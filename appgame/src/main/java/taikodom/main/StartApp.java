package taikodom.main;

import logic.*;
import taikodom.addon.AddonManager;
import taikodom.addon.AddonSettingsImpl;
import taikodom.addon.C2495fo;
import taikodom.render.loader.provider.FilePath;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class StartApp {


    static ExecutorService executorService = Executors.newSingleThreadExecutor();

    public static void main(String[] args) {
        AddonManager addonManager = new AddonManager(new WrapEngGameAddonManagerImpl(null));

        addonManager.startAddonManager();


        executorService.submit(new Runnable() {
            @Override
            public void run() {
                addonManager.step(10);
            }
        });


        while (true) {

            try {
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }


    static class WrapEngGameAddonManagerImpl implements IWrapEngGameAddonManager {

        public WrapEngGameAddonManagerImpl(Object o) {

        }

        @Override
        public IAddonSettings initAddonSetting(IAddonManager addonManager, IWrapFileXmlOrJar fileXmlOrJar, IAddonExecutor<IAddonSettings> addonExecutor) {
            AddonSettingsImpl addonSettings = new AddonSettingsImpl(
                //this.engineGame,
                addonManager, fileXmlOrJar, (C2495fo) addonExecutor);
            addonExecutor.initAddon(addonSettings);
            return addonSettings;
        }

        @Override
        public C6245ajJ getEventManager() {
            return null;
        }

        @Override
        public FilePath getRootPathRender() {
            return null;
        }

        @Override
        public FilePath getRootPath() {
            return null;
        }

        @Override
        public void setAddonManager(IAddonManager axz) {

        }

        @Override
        public boolean isPlayerNotNull() {
            return true;
        }

        @Override
        public Object getRoot() {
            return null;
        }

        @Override
        public int getScreenHeight() {
            return 0;
        }

        @Override
        public int getScreenWidth() {
            return 0;
        }
    }


}
