package taikodom.control;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

@Slf4j
public class TaskPane extends BorderPane {
    @FXML
    private BorderPane northPane;
    @FXML
    private ToggleButton pin;
    @FXML
    private Label title;
    @FXML
    private Button collapse;
    @FXML
    private TitledPane contentPane;
    @FXML
    private Button resizeButton;


    public TaskPane() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("taskpane.fxml"));
            loader.setController(this);
            loader.setRoot(this);
            loader.load();
            title.setText("set from TaskPane");
        } catch (IOException exc) {
            log.error("TaskPane:", exc);
            // handle exception
        }
    }

}
