package taikodom.addon.debugtools.taikodomversion;

import logic.IAddonSettings;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import taikodom.addon.AddonSettingsImpl;
import taikodom.addon.C2495fo;
import taikodom.addon.TaikodomAddon;
import taikodom.hoplon.taikodom.common.TaikodomVersion;

import java.awt.*;

@TaikodomAddon("taikodom.addon.debugtools.taikodomversion")
/* compiled from: a */
public class TaikodomVersionAddon implements C2495fo {
    private InternalFrame bwQ;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        AddonSettingsImpl addonSettings = (AddonSettingsImpl) addonPropertie;
        //if (addonSettings.ala() != null && addonSettings.ala().aLQ().deu()) {
        this.bwQ = addonSettings.bHv().mo16792bN("taikodomversion.xml");
        ComponentManager.getCssHolder(this.bwQ).mo13055aO(false);
        this.bwQ.mo4917cf("version").setText(TaikodomVersion.VERSION);
        this.bwQ.pack();
        Dimension preferredSize = this.bwQ.getPreferredSize();
        Dimension screenSize = addonSettings.bHv().getScreenSize();
        this.bwQ.setLocation(screenSize.width - preferredSize.width, screenSize.height - preferredSize.height);
        this.bwQ.setVisible(true);
        //}
    }

    public void stop() {
        if (this.bwQ != null) {
            this.bwQ.destroy();
        }
    }
}
