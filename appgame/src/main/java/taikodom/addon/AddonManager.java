package taikodom.addon;


import logic.*;
import logic.res.code.SoftTimer;
import logic.ui.BaseUiTegXml;
import logic.ui.IBaseUiTegXml;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.loader.provider.FilePath;

import java.io.File;
import java.io.FilenameFilter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

/* renamed from: a.fn */
/* compiled from: a */
@Slf4j
public class AddonManager implements IAddonManager, Executor {

    /* renamed from: dei */
    private final IWrapEngGameAddonManager wrapEngGameAddonManager;
    /**
     * Базовые элементы интерфейса
     */
    /* renamed from: aPy */
    private IBaseUiTegXml baseUiTegXml;
    /* renamed from: deh */
    private File folderAddons = new File("addonsTest");
    /* renamed from: dej */
    private SoftTimer softTimer = new SoftTimer();
    /**
     * Загруженные аддоны
     */
    /* renamed from: dek */
    private Map<String, IWrapFileXmlOrJar> loadAddonFile = new ConcurrentHashMap();
    /* renamed from: del */
    private C6245ajJ eventManager;
    /* renamed from: dem */
    private boolean isRunAddonManager;
    /* renamed from: den */
    private Map<String, Object> resurs = new HashMap();

    public AddonManager(IWrapEngGameAddonManager awb) {
        awb.setAddonManager(this);
        this.eventManager = awb.getEventManager();
        log.info("Starting addon manager");
        this.wrapEngGameAddonManager = awb;
        this.softTimer.addTask("Addon loader task", new AddonManagerTask(), 1000);
        this.baseUiTegXml = new BaseUiTegXml();
    }

    public Collection<IWrapFileXmlOrJar> getCloneLoadAddonFile() {
        return new ArrayList(this.loadAddonFile.values());
    }

    public IWrapEngGameAddonManager getWrapEngGameAddonManager() {
        return this.wrapEngGameAddonManager;
    }

    public SoftTimer getSoftTimer() {
        return this.softTimer;
    }


    /**
     * Работа в другом потоке
     */
    /* access modifiers changed from: protected */
    public void runAddonManagerTask() {
        if (this.isRunAddonManager && isExistsFolderAddons()) {
            HashMap tempAddonFile = new HashMap(this.loadAddonFile);//дублируем список загруженных аддонов
            for (File file : getFileCode()) {
                try {
                    //Первый запуск список пуст достаём null
                    //Последующие срезу перезапускает
                    IWrapFileXmlOrJar fileCode = (IWrapFileXmlOrJar) tempAddonFile.remove(file.getName());
                    if (fileCode == null) {
                        fileCode = loadXmlOrJar(file);
                        this.loadAddonFile.put(file.getName(), fileCode);//Список с загруженными файлами XmlOrJar
                    }
                    reLoadingAddon(fileCode);
                } catch (Exception e) {
                    log.error("Error reading file: " + file, e);
                }
            }
            //Непонятно как удаляет
            for (Object remove : tempAddonFile.keySet()) {
                this.loadAddonFile.remove(remove);
            }
        }
    }

    /* renamed from: f */
    private IWrapFileXmlOrJar loadXmlOrJar(File file) {
        String name = file.getName();
        if (name.endsWith(".xml") || (name.startsWith("dl-") && name.endsWith(".jar"))) {
            return new WrapFileXmlOrJar(this, file);
        }
        throw new IllegalArgumentException("not .js or .groovy: " + name);
    }

    /**
     * запустить загрузку аддонов
     *
     * @param fileCode
     */
    /* renamed from: a */
    private void reLoadingAddon(IWrapFileXmlOrJar fileCode) {
        try {
            fileCode.reLoadingAddon();
            // } catch (IOException e) {
            //    log.warn("Error reading addons/" + avw.getFile().getName() + " (" + e.getMessage() + ")");
        } catch (Throwable th) {
            log.warn("Error parsing addons/" + fileCode.getFile().getName(), th);
        }
    }

    /**
     * Проверяем наличеа папки
     *
     * @return
     */
    private boolean isExistsFolderAddons() {
        File folderAddons = getFolderAddons();
        if (!folderAddons.exists()) {
            log.warn("Directory '" + folderAddons + "' not found");
            return false;
        } else if (folderAddons.isDirectory()) {
            return true;
        } else {
            log.warn("'" + folderAddons + "' is not a directory");
            return false;
        }
    }

    private File[] getFileCode() {
        return getFolderAddons().listFiles(new CodeFileNameFilter());
    }

    //RELOAD_ADDONS
    public void dispose() {
        for (IWrapFileXmlOrJar dispose : this.loadAddonFile.values()) {
            dispose.dispose();
        }
        this.loadAddonFile.clear();
    }

    public void step(float second) {
        this.softTimer.step((long) (1000.0f * second), this);
    }

    public void execute(Runnable runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            log.warn("error executing timer", e);
        }
    }

    /* renamed from: b */
    public void addLoadAddonFile(String str, String... strArr) {
        for (IWrapFileXmlOrJar addonFile : this.loadAddonFile.values()) {
            addonFile.add(str, strArr);
        }
    }

    /* renamed from: y */
    public <T> T getInstanceWrapFileXmlOrJar(Class<T> cls) {
        T ret;

        for (IWrapFileXmlOrJar addonFile : this.loadAddonFile.values()) {

            ret = ((WrapFileXmlOrJar) addonFile).getIsInstanceClass(cls);

            if (ret != null) {
                if (addonFile instanceof WrapFileXmlOrJar) {
                    return ret;
                }
            }
            if (cls.isInstance(addonFile)) {
                return (T) addonFile;
            }

        }

        return null;
    }

    /* renamed from: z */
    public <T> List<T> getInstanceWrapFileXmlOrJars(Class<T> cls) {
        List<T> aS;
        ArrayList arrayList = new ArrayList();
        for (IWrapFileXmlOrJar addonFile : this.loadAddonFile.values()) {
            aS = ((WrapFileXmlOrJar) addonFile).getIsClassInstances(cls);
            if (aS != null) {
                if (addonFile instanceof WrapFileXmlOrJar) {
                    arrayList.addAll(aS);
                }
            }


            if (cls.isInstance(addonFile)) {
                arrayList.add(addonFile);
            }
        }
        return arrayList;
    }

    /**
     * Получить путь к списку аддонов taikodom.xml
     *
     * @return
     */
    public File getFolderAddons() {
        return this.folderAddons;
    }

    /**
     * Установить путь к списку аддонов taikodom.xml
     *
     * @param file
     */
    /* renamed from: g */
    public void setFolderAddons(File file) {
        this.folderAddons = file;
    }

    /**
     * Резрешить работу по чтению списка аддонов taikodom.xml
     */
    public void startAddonManager() {
        this.isRunAddonManager = true;
    }

    public boolean isPlayerNotNull() {
        return this.wrapEngGameAddonManager.isPlayerNotNull();
    }

    public C6245ajJ getEventManager() {
        return this.eventManager;
    }

    public void initAllAddonWaitPlayer() {
        for (IWrapFileXmlOrJar addonFile : this.loadAddonFile.values()) {
            addonFile.initAddonWaitPlayer();
        }
    }

    /* renamed from: n */
    public <T> T putRes(String str, T t) {
        return (T) this.resurs.put(str, t);
    }

    /* renamed from: eB */
    public <T> T getRes(String str) {
        return (T) this.resurs.get(str);
    }

    public IBaseUiTegXml getBaseUItagXML() {
        return this.baseUiTegXml;
    }

    /* renamed from: b */
    public void setBaseUItagXML(IBaseUiTegXml baseUiTegXml) {
        this.baseUiTegXml = baseUiTegXml;
    }

    public FilePath getRootPathRender() {
        return this.wrapEngGameAddonManager.getRootPathRender();
    }

    public FilePath getRootPath() {
        return this.wrapEngGameAddonManager.getRootPath();
    }

    /* renamed from: a.fn$b */
    /* compiled from: a */
    class AddonManagerTask extends WrapRunnable {
        public void run() {
            AddonManager.this.runAddonManagerTask();
        }
    }

    /* renamed from: a.fn$a */
    class CodeFileNameFilter implements FilenameFilter {
        public boolean accept(File file, String str) {
            return str.endsWith(".js") || str.endsWith(".groovy") || str.endsWith(".xml") || (str.startsWith("dl-") && str.endsWith(".jar"));
        }
    }
}

