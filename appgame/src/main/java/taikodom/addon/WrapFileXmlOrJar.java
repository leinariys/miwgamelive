package taikodom.addon;


import logic.IAddonExecutor;
import logic.IAddonSettings;
import logic.IWrapFileXmlOrJar;
import logic.res.LoaderFileXML;
import logic.res.XmlNode;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.wm */
/* compiled from: a */
@Slf4j
public class WrapFileXmlOrJar implements IWrapFileXmlOrJar {
    private final File file;
    private final AddonManager addonManager;
    /* access modifiers changed from: private */
    public WrapAddonLibraries wrapAddonLibraries = null;
    /* renamed from: Ne */
    private long fileLastModified = 0;
    private String hcF;
    private XmlNode fileXml;
    /**
     * Ожидают действие пользователя для загрузки
     */
    private final List<WrapAddonClass> addonWaitPlayer = new ArrayList();
    /**
     * Загружаются сразу не ожидая действия от пользователя
     */
    private final List<WrapAddonClass> addonNotWaitPlayer = new ArrayList();

    /**
     * @param addonManager
     * @param fileXmlOrJar XmlOrJar
     */
    public WrapFileXmlOrJar(AddonManager addonManager, File fileXmlOrJar) {
        this.addonManager = addonManager;
        this.file = fileXmlOrJar;

        if (fileXmlOrJar.getName().endsWith(".xml")) {
            try {
                this.fileXml = new LoaderFileXML().loadFileXml(new FileInputStream(fileXmlOrJar), "UTF-8");
            } catch (IOException e) {
                throw new IllegalStateException("Cannot parse descriptor", e);
            }
        }
    }

    private void load() {
        log.debug("Loading addon '" + this.file.getName() + "' ...");
        this.fileLastModified = this.file.lastModified();
        try {
            long currentTimeMillis = System.currentTimeMillis();
            start();
            log.debug(String.format("'%s' loaded in %.3fs", this.file.getName(), ((float) (System.currentTimeMillis() - currentTimeMillis)) / 1000.0f));
        } catch (Exception e) {
            log.error("Problems loading addon: " + this.file, e);
            dispose();
        }
    }

    public void start() {
        if (this.file.getName().endsWith(".xml")) {
            startReadXml();
        }
    }

    private void startReadXml() {
        boolean isWaitPlayer;
        String tempAttribute;
        try {
            this.fileXml = new LoaderFileXML().loadFileXml(new FileInputStream(this.file), "UTF-8"); //todo Убрать, есть в конструкторе
            ArrayList arrayList = new ArrayList();
            XmlNode classpath = this.fileXml.findNodeChildTag("classpath");

            if (classpath != null) {
                for (XmlNode next : classpath.getListChildrenTag()) {
                    if ("dir".equals(next.getTagName())) {
                        String attribute = next.getAttribute("path");
                        if (!attribute.endsWith("/")) {
                            tempAttribute = attribute + "/";
                        } else {
                            tempAttribute = attribute;
                        }
                        try {
                            arrayList.add(new File(this.file.getParent(), tempAttribute).toURL());
                        } catch (MalformedURLException e) {
                            throw new IllegalStateException("Cannot add classpath: " + tempAttribute, e);
                        }
                    } else if ("jar".equals(next.getTagName())) {
                        String attribute2 = next.getAttribute("path");
                        try {
                            arrayList.add(new File(this.file.getParent(), attribute2).toURL());
                        } catch (MalformedURLException e2) {
                            throw new IllegalStateException("Cannot add classpath: " + attribute2, e2);
                        }
                    }
                }
            }
            URL[] urlArr = new URL[arrayList.size()];
            arrayList.toArray(urlArr);
            this.wrapAddonLibraries = new WrapAddonLibraries(urlArr, getClass().getClassLoader());

            XmlNode mC2 = this.fileXml.findNodeChildTag("libraries");//Достаём данные из раздела libraries
            if (mC2 != null) {
                for (XmlNode next2 : mC2.getListChildrenTag()) {
                    if ("dir".equals(next2.getTagName())) {
                        String attribute3 = next2.getAttribute("path");
                        if (!attribute3.endsWith("/")) {
                            attribute3 = String.valueOf(attribute3) + "/";
                        }
                        this.wrapAddonLibraries.addAbsolutePath(new File(this.file.getParent(), attribute3).getAbsolutePath());
                    }
                }
            }

            for (XmlNode next3 : this.fileXml.findNodeChildAllTag("addon")) {//Достаём данные из раздела addon
                String attributeClass = next3.getAttribute("class");
                String waitPlayer = next3.getAttribute("waitPlayer");
                if (waitPlayer == null) {
                    isWaitPlayer = true;
                } else {
                    isWaitPlayer = !"false".equals(waitPlayer);
                }
                WrapAddonClass aVar = new WrapAddonClass();//Создание контейнер класса аддона
                aVar.className = attributeClass;
                if (isWaitPlayer) {
                    this.addonWaitPlayer.add(aVar);
                } else {
                    this.addonNotWaitPlayer.add(aVar);
                }
            }

            initAddonNotWaitPlayer();
            initAddonWaitPlayer();
        } catch (IOException e3) {
            throw new IllegalStateException("Cannot parse descriptor", e3);
        }
    }

    /**
     * Инициализация класса аддона Не требующего действия пользователя
     */
    private void initAddonNotWaitPlayer() {
        for (WrapAddonClass next : this.addonNotWaitPlayer) {
            log.debug("Starting " + next.getClass().getName());
            try {
                if (next.instanceAddonClass()) {//Проверяем инициализацию класса
                    next.addonSetting = this.addonManager.getWrapEngGameAddonManager().initAddonSetting(this.addonManager, this, next.instanceClass);
                }
            } catch (Error e) {
                log.error("Problems starting addon", e);
            } catch (Exception e2) {
                log.error("Problems starting addon", e2);
            }
        }
    }

    /**
     * Инициализация класса аддона требующих действия от пользователя
     * И перемещение иго в список addonNotWaitPlayer
     */
    public void initAddonWaitPlayer() {
        if (this.addonManager.isPlayerNotNull()) {

            Iterator<WrapAddonClass> it = this.addonWaitPlayer.iterator();
            while (it.hasNext()) {
                WrapAddonClass next = it.next();
                log.debug("Starting " + next.getClass().getName());
                try {
                    if (next.instanceAddonClass()) {
                        next.addonSetting = this.addonManager.getWrapEngGameAddonManager().initAddonSetting(this.addonManager, this, next.instanceClass);
                    }
                } catch (Exception e) {
                    log.error("Problems starting addon", e);
                } catch (Error e2) {
                    log.error("Problems starting addon", e2);
                }
                this.addonNotWaitPlayer.add(next);
                it.remove();
            }


        }
    }

    public String cHn() {
        return this.hcF;
    }

    public void stop() {
        for (WrapAddonClass wrapAddan : this.addonWaitPlayer) {
            log.debug("Stopping " + wrapAddan.getClass().getName());
            try {
                /*
                if (wrapAddan.addonSetting != null) {
                    wrapAddan.addonSetting.dispose();
                }
                */
            } catch (Exception e) {
                log.error("Problems stopping addon WaitPlayer", e);
            }
        }
        this.addonWaitPlayer.clear();

        for (WrapAddonClass wrapAddan : this.addonNotWaitPlayer) {
            log.debug("Stopping " + wrapAddan.getClass().getName());
            try {
                /*
                if (wrapAddan.addonSetting != null) {
                      wrapAddan.addonSetting.dispose();
                }
                */
            } catch (Exception e2) {
                log.error("Problems stopping addon NotWaitPlayer", e2);
            }
        }
        this.addonNotWaitPlayer.clear();

        this.wrapAddonLibraries = null;
    }

    public void dispose() {
        if (this.wrapAddonLibraries != null) {
            log.debug("Unloading addon '" + this.file.getName() + "'");
            stop();
        }
    }

    public void reLoadingAddon() {
        if (this.fileLastModified != this.file.lastModified()) {
            dispose();
            load();
        }
    }

    public File getFile() {
        return this.file;
    }

    /* renamed from: b */
    public void add(String str, String[] strArr) {
        for (WrapAddonClass next : this.addonNotWaitPlayer) {
         /*   if (next.addonSetting != null) {
                next.addonSetting.mo11844c(str, strArr);
            }*/
        }
    }

    /* renamed from: aR */
    public <T> T getIsInstanceClass(Class<T> cls) {
        for (WrapAddonClass next : this.addonNotWaitPlayer) {
          /*  if (cls.isInstance(next.instanceClass)) {
                return (T) next.instanceClass;
            }*/
        }
        return null;
    }

    /* renamed from: aS */
    public <T> List<T> getIsClassInstances(Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        for (WrapAddonClass next : this.addonNotWaitPlayer) {
          /*  if (cls.isInstance(next.instanceClass)) {
                arrayList.add(next.instanceClass);
            }*/
        }
        return arrayList;
    }


    public <T extends IAddonSettings> void mo11803a(IAddonExecutor<T> ams) {
        if (ams != null) {
            for (WrapAddonClass next : this.addonNotWaitPlayer) {
                if (next.instanceClass != null && next.instanceClass.getClass() != null && next.instanceClass.getClass().equals(ams.getClass())) {
                    if (next.addonSetting != null) {
                        next.addonSetting.dispose();
                    }
                    next.instanceClass = null;
                    next.addonSetting = null;
                    try {
                        if (next.instanceAddonClass()) {
                            next.addonSetting = this.addonManager.getWrapEngGameAddonManager().initAddonSetting(this.addonManager, this, next.instanceClass);
                            return;
                        }
                        return;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }


    public <T extends IAddonSettings> Collection<IAddonExecutor<T>> dCN() {
        ArrayList arrayList = new ArrayList();
        for (WrapAddonClass next : this.addonNotWaitPlayer) {
            if (next.instanceClass != null) {
                arrayList.add(next.instanceClass);
            }
        }
        return arrayList;
    }

    /* renamed from: a.wm$b */
    /* compiled from: a */
    static class WrapAddonLibraries extends URLClassLoader {
        private List<String> absolutePath = new ArrayList();

        public WrapAddonLibraries(URL[] urlArr, ClassLoader classLoader) {
            super(urlArr, classLoader);
        }

        /* renamed from: hZ */
        public void addAbsolutePath(String str) {
            this.absolutePath.add(str);
        }

        /* access modifiers changed from: protected */
        public String findLibrary(String str) {
            String findLibrary = super.findLibrary(str);
            if (findLibrary == null) {
                for (String file : this.absolutePath) {
                    File file2 = new File(file, System.mapLibraryName(str));
                    if (file2.exists()) {
                        return file2.getAbsolutePath();
                    }
                }
            }
            return findLibrary;
        }
    }

    /* renamed from: a.wm$a */
    private class WrapAddonClass {
        IAddonExecutor instanceClass;
        IAddonSettings addonSetting;
        String className;

        public boolean instanceAddonClass() throws IllegalAccessException, InstantiationException, ClassNotFoundException {

            if (this.instanceClass != null) {
                return true;
            }

            Class loadClass = WrapFileXmlOrJar.this.wrapAddonLibraries.loadClass(this.className);

            if (!IAddonExecutor.class.isAssignableFrom(loadClass)) {
                return false;
            }
            this.instanceClass = (IAddonExecutor) loadClass.newInstance();

            return true;
        }
    }
}
