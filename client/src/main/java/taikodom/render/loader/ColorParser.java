package taikodom.render.loader;

import com.hoplon.geometry.Color;

/* renamed from: a.Ww */
/* compiled from: a */
public class ColorParser implements IParser {
    /* renamed from: O */
    public Object mo2718O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new Color(Float.parseFloat(split[0]), Float.parseFloat(split[1]), Float.parseFloat(split[2]), Float.parseFloat(split[3]));
    }
}
