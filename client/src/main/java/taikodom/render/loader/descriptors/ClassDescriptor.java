package taikodom.render.loader.descriptors;

import java.util.Collection;

/* compiled from: a */
public abstract class ClassDescriptor {
    public abstract Object createInstance();

    public abstract String getName();

    public abstract Collection<PropertyDescriptor> getProperties();

    public abstract PropertyDescriptor getProperty(String str);
}
