package taikodom.render.loader.descriptors;

import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public abstract class PropertyDescriptor {
    public boolean isArray;
    public boolean isObject;
    public String name;
    public String propertyTypeName;

    public abstract int arrayItemCount(RenderAsset renderAsset);

    public abstract Object getAsObject(RenderAsset renderAsset);

    public abstract String getAsString(RenderAsset renderAsset);

    public abstract Object getObjectArrayItem(RenderAsset renderAsset, int i);

    public abstract boolean hasDefaultValue(RenderAsset renderAsset);

    public abstract boolean set(Object obj, int i, Object obj2);

    public abstract boolean setAsString(Object obj, String str);

    public boolean isObject() {
        return this.isObject;
    }

    public boolean isObjectArray() {
        return this.isArray && this.isObject;
    }

    public boolean isStructArray() {
        return this.isArray && !this.isObject;
    }

    public String getPropertyTypeName() {
        return this.propertyTypeName;
    }

    public String getName() {
        return this.name;
    }
}
