package taikodom.render.loader.descriptors;

import taikodom.render.loader.SceneLoader;

import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class StructUnassignedProperty extends UnassignedPropertyBase {
    private final Object object;
    public List<UnassignedPropertyBase> subProperties = new ArrayList();

    public StructUnassignedProperty(SceneLoader sceneLoader, PropertyDescriptor propertyDescriptor, int i, Object obj) {
        super(propertyDescriptor, i);
        this.object = obj;
    }

    public boolean doAssign(SceneLoader sceneLoader, Object obj) {
        for (UnassignedPropertyBase doAssign : this.subProperties) {
            doAssign.doAssign(sceneLoader, this.object);
        }
        return this.descriptor.set(obj, this.index, this.object);
    }
}
