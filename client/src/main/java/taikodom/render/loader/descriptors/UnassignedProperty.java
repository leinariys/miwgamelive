package taikodom.render.loader.descriptors;

import taikodom.render.loader.SceneLoader;

/* compiled from: a */
public abstract class UnassignedProperty {
    public abstract boolean doAssign(SceneLoader sceneLoader, Object obj);

    public abstract boolean doAssignToNewInstance(SceneLoader sceneLoader, Object obj);

    public abstract String getName();

    public abstract String getValue();
}
