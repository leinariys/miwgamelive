package taikodom.render.loader;

import taikodom.render.NotExported;

import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: a */
public abstract class RenderAsset {
    private static final AtomicInteger nextId = new AtomicInteger();
    public String name;
    private int handle;

    public RenderAsset() {
        this.handle = nextId.getAndIncrement();
        this.name = "";
    }

    public RenderAsset(RenderAsset renderAsset) {
        this.handle = renderAsset.handle;
        this.name = renderAsset.name;
    }

    public abstract RenderAsset cloneAsset();

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void invalidateHandle() {
        this.handle = nextId.getAndIncrement();
    }

    @NotExported
    public int getHandle() {
        return this.handle;
    }

    public void validate(SceneLoader sceneLoader) {
        if (this.name == null) {
            throw new RuntimeException("No 'name' property");
        }
    }

    public String toString() {
        return String.valueOf(getClass().getSimpleName()) + ": " + this.name;
    }

    public void releaseReferences() {
    }
}
