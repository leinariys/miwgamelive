package taikodom.render.loader;

import com.sun.opengl.util.texture.TextureData;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.PitchEntry;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.gui.GAim;
import taikodom.render.gui.GHudMark;
import taikodom.render.loader.descriptors.ClassDescriptor;
import taikodom.render.loader.provider.*;
import taikodom.render.scene.*;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.shaders.ShaderProgram;
import taikodom.render.shaders.parameters.*;
import taikodom.render.textures.*;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.*;

/* compiled from: a */
@Slf4j
public class SceneLoader {
    public ImageLoader imageLoader;
    public Set<String> missingIncludes = Collections.synchronizedSet(new TreeSet(String.CASE_INSENSITIVE_ORDER));
    public List<FormatProvider> providers = new ArrayList();
    public Map<String, ClassDescriptor> registeredDescriptors = new TreeMap(String.CASE_INSENSITIVE_ORDER);
    public Map<String, StockEntry> stockEntryCache = Collections.synchronizedMap(new TreeMap(String.CASE_INSENSITIVE_ORDER));
    public Map<String, StockFile> stockFiles = Collections.synchronizedMap(new TreeMap(String.CASE_INSENSITIVE_ORDER));
    private ResourceCleaner resourceCleaner;
    private Shader defaultAlphaTestShader;
    private Shader defaultBillboardShader;
    private Shader defaultBillboardShaderNoDepth;
    private Shader defaultDecalShader;
    private Shader defaultDiffuseShader;
    private Texture defaultDiffuseTexture;
    private Texture defaultEmissiveTexture;
    private Shader defaultFlatShader;
    private Shader defaultHudMarkShader;
    private Material defaultMaterial;
    private Texture defaultNormalTexture;
    private Shader defaultSpaceDustShader;
    private Shader defaultTrailShader;
    private Shader defaultVertexColorShader;
    private Shader defaultWireframeShader;

    public SceneLoader() {
        this.providers.add(new GrannyLoader(this, "gr2"));
        this.imageLoader = new ImageLoader(this, "dds", "tga", "bmp", "jpg", "gif", "png");
        this.providers.add(this.imageLoader);
        this.resourceCleaner = new ResourceCleaner();
        this.providers.add(new BinkLoader(this, "bik"));
        this.providers.add(new FlashLoader(this, "swf"));
        this.providers.add(new SoundBufferLoader(this, "wav", "mp3", "ogg"));
        this.providers.add(new ProLoader(this, "pro"));
        this.providers.add(new ShaderLoader(this, "vert", "frag"));
        registerClassDescriptor(new ReflectiveClassDescriptor("ChannelInfo", ChannelInfo.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("PannedMaterial", PannedMaterial.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RBillboard", RBillboard.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RNuclearLight", RNuclearLight.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RSceneAreaInfo", RSceneAreaInfo.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RLight", RLight.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RPanorama", RPanorama.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RSpacedust", RSpaceDust.class));//todo
        registerClassDescriptor(new ReflectiveClassDescriptor("TiledTexture", TiledTexture.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("PannedTexture", PannedTexture.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("AnimatedTexture", AnimatedTexture.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("SceneObject", SceneObject.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RParticleSystem", RParticleSystem.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RMesh", RMesh.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RModel", RModel.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RGroup", RGroup.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RLod", RLod.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("Material", Material.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("Shader", Shader.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("ShaderProgram", ShaderProgram.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("ShaderPass", ShaderPass.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RTrail", RTrail.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RSpaceDust", RSpaceDust.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("FloatParameter", FloatParameter.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("ColorParameter", ColorParameter.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("IntParameter", IntParameter.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("Vec2Parameter", Vec2fParameter.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("Vec3Parameter", Vec3fParameter.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RTerrain", RTerrain.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RShape", RShape.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RHudMark", GHudMark.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RAim", GAim.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("SPitchedSet", SPitchedSet.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("PitchEntry", PitchEntry.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("SSoundSource", SSoundSource.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("SSoundGroup", SSoundGroup.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("SSourceElement", SSourceElement.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("SMusic", SMusic.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("SMultiLayerMusic", SMultiLayerMusic.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RFlowerShot", RFlowerShot.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("RRotor", RRotor.class));
        registerClassDescriptor(new ReflectiveClassDescriptor("Socket", Socket.class));
    }

    public void registerClassDescriptor(ClassDescriptor classDescriptor) {
        this.registeredDescriptors.put(classDescriptor.getName(), classDescriptor);
    }

    public void loadFile(InputStream inputStream, String str, String str2) throws IOException {
        char c;
        log.debug("Loading " + str);
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf == -1) {
            throw new IOException();
        }
        String lowerCase = str.substring(lastIndexOf + 1).toLowerCase();
        Iterator<FormatProvider> it = this.providers.iterator();
        while (true) {
            if (!it.hasNext()) {
                c = 0;
                break;
            }
            FormatProvider next = it.next();
            if (next.check(lowerCase)) {
                try {
                    next.loadFile(inputStream, str, str2);
                    c = 1;
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException("Error loading file " + str, e);
                }
            }
        }
        if (c == 0) {
            log.error("There is no provider to the file " + str);
            throw new RuntimeException("There is no provider to the file " + str);
        } else if (c > 1) {
            log.error("There are many providers to the file " + str);
            throw new RuntimeException("There are many providers to the file " + str);
        }
    }

    public ClassDescriptor getClassDescriptor(String str) {
        return this.registeredDescriptors.get(str);
    }

    public void addInclude(StockFile stockFile, String str) {
        stockFile.includes.add(str);
        if (!this.stockFiles.containsKey(str)) {
            this.missingIncludes.add(str);
        }
    }

    public StockEntry getStockEntry(String str) {
        return this.stockEntryCache.get(str);
    }

    public ClassDescriptor getClassDescriptor(RenderAsset renderAsset) {
        return null;
    }

    public StockEntry getEntry(RenderAsset renderAsset) {
        return null;
    }

    public StockFile getStockFile(String str) {
        return this.stockFiles.get(str);
    }

    /* access modifiers changed from: package-private */
    public void addStockFile(StockFile stockFile) {
        if (stockFile.getName() == null) {
            throw new RuntimeException("Unnamed stock file");
        }
        this.missingIncludes.remove(stockFile.getName());
        this.stockFiles.put(stockFile.getName(), stockFile);
        for (StockEntry next : stockFile.entries) {
            this.stockEntryCache.put(next.getName(), next);
        }
    }

    public void addEntry(StockFile stockFile, StockEntry stockEntry) {
        stockFile.addEntry(stockEntry);
        this.stockEntryCache.put(stockEntry.getName(), stockEntry);
    }

    /* access modifiers changed from: package-private */
    public StockEntry addEntry(StockFile stockFile, String str, RenderAsset renderAsset) {
        StockEntry stockEntry = new StockEntry(str);
        stockEntry.setAsset(renderAsset);
        stockFile.addEntry(stockEntry);
        return stockEntry;
    }

    public Collection<String> allAssets() {
        return this.stockEntryCache.keySet();
    }

    public <T extends RenderAsset> T instantiateAsset(String str) {
        RenderAsset asset = getAsset(str);
        if (asset != null) {
            return (T) asset.cloneAsset();
        }
        return null;
    }

    public <T extends RenderAsset> T getAsset(String str) {
        T t;
        RenderAsset asset;
        StockEntry stockEntry = this.stockEntryCache.get(str);
        if (stockEntry == null) {
            return null;
        }
        T asset2 = (T) stockEntry.getAsset();
        if (stockEntry.isComplete() || stockEntry.completing) {
            return asset2;
        }
        stockEntry.completing = true;
        if (asset2 != null) {
            t = asset2;
        } else if (stockEntry.getClonesFrom() == null || (asset = getAsset(stockEntry.getClonesFrom())) == null) {
            return null;
        } else {
            stockEntry.setAsset(asset.cloneAsset());
            t = (T) stockEntry.getAsset();
        }
        for (int i = 0; i < stockEntry.unassignedProperties.size(); i++) {
            if (!stockEntry.unassignedProperties.get(i).doAssign(this, t)) {
            }
        }
        stockEntry.unassignedProperties.clear();
        stockEntry.completing = false;
        try {
            t.validate(this);
            return t;
        } catch (Exception e) {
            log.warn(e.toString());
            return t;
        }
    }

    public Collection<String> getMissingIncludes() {
        return this.missingIncludes;
    }

    public Texture getDefaultSelfIluminationTexture() {
        if (this.defaultEmissiveTexture == null) {
            this.defaultEmissiveTexture = new Texture();
            ByteBuffer[] byteBufferArr = {ByteBuffer.allocate(4)};
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].position(0);
            TextureData textureData = new TextureData(6408, 1, 1, 0, 6408, 5121, false, false, byteBufferArr, (TextureData.Flusher) null);
            textureData.setMipmap(false);
            this.defaultEmissiveTexture.setTextureData(0, textureData);
            this.defaultEmissiveTexture.setName("Default emissive texture");
        }
        return this.defaultEmissiveTexture;
    }

    public Texture getDefaultNormalTexture() {
        if (this.defaultNormalTexture == null) {
            this.defaultNormalTexture = new Texture();
            ByteBuffer[] byteBufferArr = {ByteBuffer.allocate(4)};
            byteBufferArr[0].put(Byte.MAX_VALUE);
            byteBufferArr[0].put(Byte.MAX_VALUE);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].position(0);
            TextureData textureData = new TextureData(6408, 1, 1, 0, 6408, 5121, false, false, byteBufferArr, (TextureData.Flusher) null);
            textureData.setMipmap(false);
            this.defaultNormalTexture.setTextureData(0, textureData);
            this.defaultNormalTexture.setName("Default normal texture");
        }
        return this.defaultNormalTexture;
    }

    public Texture getDefaultDiffuseTexture() {
        if (this.defaultDiffuseTexture == null) {
            this.defaultDiffuseTexture = new Texture();
            ByteBuffer[] byteBufferArr = {ByteBuffer.allocate(16)};
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].put((byte) 0);
            byteBufferArr[0].put((byte) -1);
            byteBufferArr[0].position(0);
            TextureData textureData = new TextureData(6408, 2, 2, 0, 6408, 5121, false, false, byteBufferArr, (TextureData.Flusher) null);
            textureData.setMipmap(false);
            this.defaultDiffuseTexture.setTextureData(0, textureData);
            this.defaultDiffuseTexture.setMagFilter(TexMagFilter.NEAREST);
            this.defaultDiffuseTexture.setMinFilter(TexMinFilter.NEAREST);
            this.defaultDiffuseTexture.setName("Default diffuse texture");
        }
        return this.defaultDiffuseTexture;
    }

    public Shader getDefaultTrailShader() {
        if (this.defaultTrailShader == null) {
            this.defaultTrailShader = new Shader();
            this.defaultTrailShader.setName("Default trail shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default trail pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setColorMask(true);
            shaderPass.setDepthMask(false);
            shaderPass.setAlphaMask(false);
            shaderPass.setColorArrayEnabled(true);
            shaderPass.setDstBlend(BlendType.ONE);
            shaderPass.setSrcBlend(BlendType.SRC_ALPHA);
            shaderPass.getRenderStates().setCullFaceEnabled(true);
            this.defaultTrailShader.addPass(shaderPass);
        }
        return this.defaultTrailShader;
    }

    public Shader getDefaultSpaceDustShader() {
        if (this.defaultSpaceDustShader == null) {
            this.defaultSpaceDustShader = new Shader();
            this.defaultSpaceDustShader.setName("Default SpaceDust shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default SpaceDust pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDepthMask(false);
            shaderPass.setColorArrayEnabled(true);
            shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            shaderPass.setSrcBlend(BlendType.ONE);
            shaderPass.getRenderStates().setCullFaceEnabled(false);
            this.defaultSpaceDustShader.addPass(shaderPass);
        }
        return this.defaultSpaceDustShader;
    }

    public Shader getDefaultBillboardShader() {
        if (this.defaultBillboardShader == null) {
            this.defaultBillboardShader = new Shader();
            this.defaultBillboardShader.setName("Default billboard shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default billboard pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            shaderPass.setSrcBlend(BlendType.ONE);
            shaderPass.setCullFaceEnabled(true);
            shaderPass.getRenderStates().setCullFaceEnabled(true);
            this.defaultBillboardShader.addPass(shaderPass);
        }
        return this.defaultBillboardShader;
    }

    public Shader getDefaultHudMarkShader() {
        if (this.defaultHudMarkShader == null) {
            this.defaultHudMarkShader = new Shader();
            this.defaultHudMarkShader.setName("Default hudmark shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default hudmark pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            shaderPass.setSrcBlend(BlendType.ONE);
            shaderPass.setCullFaceEnabled(false);
            shaderPass.getRenderStates().setCullFaceEnabled(false);
            this.defaultHudMarkShader.addPass(shaderPass);
        }
        return this.defaultHudMarkShader;
    }

    public Collection<StockEntry> getStockEntries() {
        return this.stockEntryCache.values();
    }

    public Material getDefaultMaterial() {
        if (this.defaultMaterial == null) {
            this.defaultMaterial = new Material();
            this.defaultMaterial.setName("Default material");
            this.defaultMaterial.setDiffuseTexture(getDefaultDiffuseTexture());
            this.defaultMaterial.setNormalTexture(getDefaultNormalTexture());
            this.defaultMaterial.setSelfIluminationTexture(getDefaultSelfIluminationTexture());
            this.defaultMaterial.setShader(getDefaultDiffuseShader());
        }
        return this.defaultMaterial;
    }

    public Shader getDefaultDiffuseShader() {
        if (this.defaultDiffuseShader == null) {
            this.defaultDiffuseShader = new Shader();
            this.defaultDiffuseShader.setName("Default diffuse shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default diffuse pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setCullFaceEnabled(true);
            shaderPass.getRenderStates().setCullFaceEnabled(true);
            this.defaultDiffuseShader.addPass(shaderPass);
        }
        return this.defaultDiffuseShader;
    }

    public Shader getDefaultBillboardShaderNoDepth() {
        if (this.defaultBillboardShaderNoDepth == null) {
            this.defaultBillboardShaderNoDepth = new Shader();
            this.defaultBillboardShaderNoDepth.setName("Default billboard shader no depth");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default billboard pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDepthMask(false);
            shaderPass.setDstBlend(BlendType.ONE);
            shaderPass.setSrcBlend(BlendType.ONE);
            shaderPass.setCullFaceEnabled(true);
            shaderPass.getRenderStates().setCullFaceEnabled(true);
            this.defaultBillboardShaderNoDepth.addPass(shaderPass);
        }
        return this.defaultBillboardShaderNoDepth;
    }

    public Shader getDefaultVertexColorShader() {
        if (this.defaultVertexColorShader == null) {
            this.defaultVertexColorShader = new Shader();
            this.defaultVertexColorShader.setName("Default vertex color shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default vertex color pass");
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDepthMask(false);
            shaderPass.setDepthTestEnabled(false);
            shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            shaderPass.setSrcBlend(BlendType.SRC_ALPHA);
            shaderPass.getRenderStates().setCullFaceEnabled(true);
            this.defaultVertexColorShader.addPass(shaderPass);
        }
        return this.defaultVertexColorShader;
    }

    public Shader getDefaultWireframeShader() {
        if (this.defaultWireframeShader == null) {
            this.defaultWireframeShader = new Shader();
            this.defaultWireframeShader.setName("Default wireframe shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default wireframe pass");
            shaderPass.setDepthMask(false);
            shaderPass.setDepthTestEnabled(true);
            shaderPass.setLineMode(true);
            shaderPass.getRenderStates().setCullFaceEnabled(false);
            this.defaultWireframeShader.addPass(shaderPass);
        }
        return this.defaultWireframeShader;
    }

    public Shader getDefaultDecalShader() {
        if (this.defaultDecalShader == null) {
            this.defaultDecalShader = new Shader();
            this.defaultDecalShader.setName("Default decal shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default decal pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setDepthMask(false);
            shaderPass.setDepthTestEnabled(true);
            shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
            shaderPass.setSrcBlend(BlendType.ONE);
            shaderPass.getRenderStates().setCullFaceEnabled(true);
            this.defaultDecalShader.addPass(shaderPass);
        }
        return this.defaultDecalShader;
    }

    public Shader getDefaultFlatShader() {
        if (this.defaultFlatShader == null) {
            this.defaultFlatShader = new Shader();
            this.defaultFlatShader.setName("Default flat shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default flat pass");
            ChannelInfo channelInfo = new ChannelInfo();
            channelInfo.setTexChannel(8);
            shaderPass.setChannelTextureSet(0, channelInfo);
            this.defaultFlatShader.addPass(shaderPass);
        }
        return this.defaultFlatShader;
    }

    public Shader getDefaultAlphaTestShader() {
        if (this.defaultAlphaTestShader == null) {
            this.defaultAlphaTestShader = new Shader();
            this.defaultAlphaTestShader.setName("Default alpha test shader");
            ShaderPass shaderPass = new ShaderPass();
            shaderPass.setName("Default alpha test pass");
            this.defaultAlphaTestShader.addPass(shaderPass);
            shaderPass.setBlendEnabled(true);
            shaderPass.setAlphaTestEnabled(true);
            shaderPass.setAlphaFunc(DepthFuncType.GEQUAL);
            shaderPass.setAlphaRef(1.0f);
            shaderPass.setDepthMask(false);
            shaderPass.setDepthTestEnabled(false);
            shaderPass.setSrcBlend(BlendType.DST_ALPHA);
        }
        return this.defaultAlphaTestShader;
    }

    public ImageLoader getImageLoader() {
        return this.imageLoader;
    }
}
