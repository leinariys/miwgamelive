package taikodom.render.loader.provider;

import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureIO;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.DrawContext;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;
import taikodom.render.textures.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.Semaphore;

/* compiled from: a */
@Slf4j
public class ImageLoader extends FormatProvider implements Runnable {
    public static final int[] cubeMapTargets = {34069, 34070, 34071, 34072, 34073, 34074};
    public static Semaphore processingLock = new Semaphore(1);
    static TextureData noTextureData;
    static TextureData noTextureData2;
    static long textureTimeLimit = 5000;
    private static boolean LOG_IMAGE_LOADER = false;
    private static Comparator<ImageLoaderEntry> comparator;
    private static Map<Texture, ImageLoaderEntry> currentTexturesMap;
    private static int[] ddsTargets = {1024, 2048, 4096, DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY, DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEZ};
    private static int globalMaxMipMapSize = 256;
    private static int globalMinTextureSize = 16;
    private static boolean processing = false;
    private static FilePath rootpath;
    private static Map<Texture, ImageLoaderEntry> secondaryTexturesMap;
    private static List<Texture> textureDestroyList;
    private static List<Texture> textureUpdateList;
    boolean shouldStop = false;

    public ImageLoader(SceneLoader sceneLoader, String... strArr) {
        super(sceneLoader, strArr);
        currentTexturesMap = new HashMap();
        secondaryTexturesMap = new HashMap();
        Thread thread = new Thread(this, "ImageLoader");
        thread.setDaemon(true);
        thread.start();
    }

    private static int nextPowerOfTwo(int i) {
        int i2 = 1;
        while (i2 < i) {
            i2 <<= 1;
        }
        return i2;
    }

    public static boolean addTexture(Texture texture, int i, boolean z) {
        TextureDataSource dataSource = texture.getDataSource();
        if (!(dataSource instanceof DDSDataSource)) {
            return false;
        }
        ImageLoaderEntry imageLoaderEntry = new ImageLoaderEntry();
        imageLoaderEntry.texture = texture;
        DDSDataSource dDSDataSource = (DDSDataSource) dataSource;
        if (i == -1 || i > globalMaxMipMapSize) {
            imageLoaderEntry.mipMapResolution = globalMaxMipMapSize;
        } else {
            imageLoaderEntry.mipMapResolution = nextPowerOfTwo(i);
        }
        int mipMapIDByResolution = dDSDataSource.getMipMapIDByResolution(imageLoaderEntry.mipMapResolution);
        imageLoaderEntry.mipMapLevel = mipMapIDByResolution;
        if (imageLoaderEntry.mipMapLevel == dataSource.getDegradationLevel()) {
            return false;
        }
        if (mipMapIDByResolution == 0) {
            imageLoaderEntry.mipMapResolution = dDSDataSource.getRealWidth();
        }
        imageLoaderEntry.singleLevel = z;
        synchronized (currentTexturesMap) {
            currentTexturesMap.put(texture, imageLoaderEntry);
        }
        return true;
    }

    public static void reAddTexture(Texture texture) {
        synchronized (currentTexturesMap) {
            TextureDataSource dataSource = texture.getDataSource();
            if (dataSource instanceof DDSDataSource) {
                DDSDataSource dDSDataSource = (DDSDataSource) dataSource;
                ImageLoaderEntry imageLoaderEntry = new ImageLoaderEntry();
                dataSource.setReady(true);
                imageLoaderEntry.texture = texture;
                imageLoaderEntry.singleLevel = false;
                imageLoaderEntry.mipMapLevel = dDSDataSource.getDegradationLevel();
                imageLoaderEntry.mipMapResolution = dDSDataSource.getMipMapResolution();
                currentTexturesMap.put(texture, imageLoaderEntry);
            }
        }
    }

    public static void getFullDDSTexture(Texture texture) {
        if (texture.getDataSource() instanceof DDSDataSource) {
            ImageLoaderEntry imageLoaderEntry = new ImageLoaderEntry();
            imageLoaderEntry.texture = texture;
            imageLoaderEntry.mipMapResolution = globalMaxMipMapSize;
            imageLoaderEntry.mipMapLevel = 0;
            imageLoaderEntry.singleLevel = false;
            processEntry(imageLoaderEntry);
        }
    }

    public static TextureData newTextureDataByResolution(DDSLoader dDSLoader, int i, int i2, int i3, boolean z, int i4, InputStream inputStream) {
        return newTextureData(dDSLoader, i, i2, i3, z, dDSLoader.getMipMapIDByResolution(i4), inputStream);
    }

    public static TextureData newTextureData(DDSLoader dDSLoader, int i, int i2, int i3, boolean z, int i4, InputStream inputStream) {
        DDSInfo mipMap;
        int i5;
        int i6;
        if (z) {
            mipMap = dDSLoader.getMipMaps(i, i4, dDSLoader.getNumMipMaps(), inputStream);
        } else {
            mipMap = dDSLoader.getMipMap(i, i4, inputStream);
        }
        ByteBuffer[] dataList = mipMap.getDataList();
        if (i3 == 0) {
            switch (dDSLoader.getPixelFormat()) {
                case 20:
                    i5 = 32992;
                    break;
                default:
                    i5 = 32993;
                    break;
            }
        } else {
            i5 = i3;
        }
        if (mipMap.isCompressed()) {
            switch (mipMap.getCompressionFormat()) {
                case DDSLoader.D3DFMT_DXT1 /*827611204*/:
                    i6 = 33776;
                    break;
                case DDSLoader.D3DFMT_DXT3 /*861165636*/:
                    i6 = 33778;
                    break;
                case DDSLoader.D3DFMT_DXT5 /*894720068*/:
                    i6 = 33779;
                    break;
                default:
                    throw new RuntimeException("Unsupported DDS compression format \"" + DDSLoader.getCompressionFormatName(mipMap.getCompressionFormat()) + "\"");
            }
        } else {
            i6 = i2;
        }
        if (i6 == 0) {
            switch (dDSLoader.getPixelFormat()) {
                case 20:
                    i6 = 6407;
                    break;
                default:
                    i6 = 6408;
                    break;
            }
        }
        TextureData.Flusher aVar = new C5149a();
        if (!z || dDSLoader.getNumMipMaps() <= 0) {
            TextureData textureData = new TextureData(i6, mipMap.getWidth(), mipMap.getHeight(), 0, i5, 5121, false, mipMap.isCompressed(), true, mipMap.getData(), aVar);
            textureData.setMipmap(false);
            return textureData;
        }
        TextureData textureData2 = new TextureData(i6, mipMap.getWidth(), mipMap.getHeight(), 0, i5, 5121, mipMap.isCompressed(), true, dataList, aVar);
        textureData2.setMipmap(true);
        return textureData2;
    }

    public static int getMinTextureSize() {
        return globalMinTextureSize;
    }

    public static void setMinTextureSize(int i) {
        globalMinTextureSize = i;
    }

    public static int getMaxMipMapSize() {
        return globalMaxMipMapSize;
    }

    public static void setMaxMipMapSize(int i) {
        globalMaxMipMapSize = i;
    }

    private static void processEntry(ImageLoaderEntry imageLoaderEntry) {
        TextureData[] textureDataArr;
        TextureData[] textureDataArr2;
        if (LOG_IMAGE_LOADER) {
            log.info("Requested a resolution " + imageLoaderEntry.mipMapResolution + " for " + imageLoaderEntry.texture.getName());
        }
        try {
            DDSDataSource dDSDataSource = (DDSDataSource) imageLoaderEntry.texture.getDataSource();
            InputStream openInputStream = rootpath.concat(imageLoaderEntry.texture.getDataSource().getFileName()).openInputStream();
            if (!dDSDataSource.isCubemap()) {
                textureDataArr = new TextureData[]{newTextureData(dDSDataSource.getLoader(), 0, 0, 0, true, imageLoaderEntry.mipMapLevel, openInputStream)};
                if (textureDataArr[0].getInternalFormat() == 0) {
                    textureDataArr[0].setInternalFormat(textureDataArr[0].getPixelFormat());
                    textureDataArr2 = textureDataArr;
                }
                textureDataArr2 = textureDataArr;
            } else {
                textureDataArr = new TextureData[6];
                for (int i = 0; i < 6; i++) {
                    textureDataArr[i] = newTextureData(dDSDataSource.getLoader(), ddsTargets[i], 0, 0, true, imageLoaderEntry.mipMapLevel, openInputStream);
                    if (textureDataArr[i].getInternalFormat() == 0) {
                        textureDataArr[i].setInternalFormat(textureDataArr[i].getPixelFormat());
                    }
                }
                textureDataArr2 = textureDataArr;
            }
            openInputStream.close();
            dDSDataSource.setMipMapResolution(imageLoaderEntry.mipMapResolution);
            dDSDataSource.setHeight(textureDataArr2[0].getHeight());
            dDSDataSource.setWidth(textureDataArr2[0].getWidth());
            dDSDataSource.setData(textureDataArr2);
            dDSDataSource.setDegradationLevel(imageLoaderEntry.mipMapLevel);
            dDSDataSource.setLastRequestTime(System.currentTimeMillis());
            dDSDataSource.setReady(true);
            imageLoaderEntry.texture.setOnlyUpdateNeeded(true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public static TextureData getNoTextureData() {
        C5150b bVar = new C5150b();
        if (noTextureData == null) {
            noTextureData = new TextureData(6408, 8, 8, 0, 6408, 5121, false, false, true, ByteBuffer.allocate(256), bVar);
        }
        return noTextureData;
    }

    public static void clearStack() {
        synchronized (currentTexturesMap) {
            currentTexturesMap.clear();
        }
    }

    public static void addForcedUpdateTexture(Texture texture) {
        if (textureUpdateList == null) {
            textureUpdateList = new ArrayList();
        }
        textureUpdateList.add(texture);
    }

    public static void processForcedUpdate() {
        if (textureUpdateList != null && textureUpdateList.size() > 0) {
            Texture remove = textureUpdateList.remove(0);
            if (remove.isOnlyUpdateNeeded()) {
                TextureData[] data = remove.getDataSource().getData();
                if (data.length > 1) {
                    for (int i = 0; i < data.length; i++) {
                        remove.updateImage(data[i], cubeMapTargets[i]);
                    }
                } else {
                    remove.updateImage(data[0], 0);
                }
                remove.getDataSource().clearData();
            }
        }
    }

    public static void destroyTexture(Texture texture) {
        if (textureDestroyList == null) {
            textureDestroyList = new ArrayList();
        }
        if (texture != null) {
            textureDestroyList.add(texture);
        }
    }

    public static void processDestroyList(DrawContext drawContext) {
        if (textureDestroyList != null && textureDestroyList.size() > 0) {
            Texture remove = textureDestroyList.remove(0);
            BaseTexture.addTextureMemoryUsed(-remove.getInternalTexture().getEstimatedMemorySize());
            TextureDataSource dataSource = remove.getDataSource();
            dataSource.setReady(false);
            dataSource.clearData();
            if (dataSource instanceof DDSDataSource) {
                DDSDataSource dDSDataSource = (DDSDataSource) dataSource;
                remove.setInitialized(false);
                dataSource.setWidth(globalMinTextureSize);
                dataSource.setHeight(globalMinTextureSize);
                dDSDataSource.setMipMapResolution(globalMinTextureSize);
                dataSource.setDegradationLevel(dDSDataSource.getNumMipMaps() - 1);
            } else {
                remove.setInitialized(false);
            }
            remove.updateImage(drawContext, getNoTextureData());
            remove.setOnlyUpdateNeeded(true);
        }
    }

    public void finalize() {
        this.shouldStop = true;
    }

    public void loadFile(InputStream inputStream, String str, String str2) {
        loadImage(inputStream, str, str.replaceAll("^.*[.]([^.]*)$", "$1").toLowerCase());
    }

    /* access modifiers changed from: protected */
    public void loadDDSImage(InputStream inputStream, String str, boolean z) {
        TextureData[] textureDataArr;
        TextureData[] textureDataArr2;
        int width;
        int height;
        try {
            int i = globalMinTextureSize;
            DDSLoader dDSLoader = new DDSLoader();
            dDSLoader.loadFromFile(inputStream);
            Texture texture = new Texture();
            texture.setName(str);
            if (!dDSLoader.isCubemap()) {
                textureDataArr = new TextureData[]{newTextureDataByResolution(dDSLoader, 0, 0, 0, z, i, inputStream)};
                textureDataArr[0].setMustFlipVertically(false);
                if (textureDataArr[0].getInternalFormat() == 0) {
                    textureDataArr[0].setInternalFormat(textureDataArr[0].getPixelFormat());
                    textureDataArr2 = textureDataArr;
                }
                textureDataArr2 = textureDataArr;
            } else {
                textureDataArr = new TextureData[6];
                for (int i2 = 0; i2 < 6; i2++) {
                    textureDataArr[i2] = newTextureDataByResolution(dDSLoader, ddsTargets[i2], 0, 0, z, i, inputStream);
                    textureDataArr[i2].setMustFlipVertically(false);
                    if (textureDataArr[i2].getInternalFormat() == 0) {
                        textureDataArr[i2].setInternalFormat(textureDataArr[i2].getPixelFormat());
                    }
                }
                textureDataArr2 = textureDataArr;
            }
            DDSDataSource dDSDataSource = new DDSDataSource();
            dDSDataSource.setLoader(dDSLoader);
            dDSDataSource.setFileName(str);
            dDSDataSource.setSampleData(textureDataArr2);
            if (dDSDataSource.getWidth() < i) {
                width = i;
            } else {
                width = dDSDataSource.getWidth();
            }
            dDSDataSource.setSampleWidth(width);
            if (dDSDataSource.getHeight() < i) {
                height = i;
            } else {
                height = dDSDataSource.getHeight();
            }
            dDSDataSource.setSampleHeight(height);
            dDSDataSource.setWidth(dDSDataSource.getSampleWidth());
            dDSDataSource.setHeight(dDSDataSource.getSampleHeight());
            dDSDataSource.setMipMapResolution(i);
            dDSDataSource.setDegradationLevel(dDSLoader.getMipMapIDByResolution(i));
            texture.setDataSource(dDSDataSource);
            texture.setSizeX(dDSLoader.getRealWidth());
            texture.setSizeY(dDSLoader.getRealHeight());
            StockFile stockFile = new StockFile();
            stockFile.setName(str);
            addEntry(stockFile, str, texture);
            createMaterialForTexture(str, texture, stockFile);
            addStockFile(stockFile);
        } catch (Exception e) {
            throw new RuntimeException("Error reading " + str, e);
        }
    }

    private void createMaterialForTexture(String str, Texture texture, StockFile stockFile) {
        Material material = new Material();
        material.setDiffuseTexture(texture);
        material.setName(String.valueOf(str) + "/material");
        addEntry(stockFile, material.getName(), material);
    }

    /* access modifiers changed from: protected */
    public void loadImage(InputStream inputStream, String str, String str2) {
        if ("dds".equals(str2)) {
            loadDDSImage(inputStream, str, true);
            return;
        }
        try {
            TextureData newTextureData = TextureIO.newTextureData(inputStream, true, str2);
            Texture texture = new Texture();
            texture.setName(str);
            Texture2DDataSource texture2DDataSource = new Texture2DDataSource();
            texture2DDataSource.setFileName(str);
            texture2DDataSource.setData(newTextureData);
            texture2DDataSource.setWidth(newTextureData.getWidth());
            texture2DDataSource.setHeight(newTextureData.getHeight());
            texture2DDataSource.setReady(true);
            texture.setDataSource(texture2DDataSource);
            texture.setSizeX(newTextureData.getWidth());
            texture.setSizeY(newTextureData.getHeight());
            StockFile stockFile = new StockFile();
            stockFile.setName(str);
            addEntry(stockFile, str, texture);
            createMaterialForTexture(str, texture, stockFile);
            addStockFile(stockFile);
        } catch (Exception e) {
            throw new RuntimeException("Error reading " + str, e);
        }
    }

    private void process() {
        if (!processing) {
            processing = true;
            synchronized (currentTexturesMap) {
                Map<Texture, ImageLoaderEntry> map = currentTexturesMap;
                currentTexturesMap = secondaryTexturesMap;
                secondaryTexturesMap = map;
            }
            for (ImageLoaderEntry next : secondaryTexturesMap.values()) {
                if (next.texture.isUpdatingData()) {
                    synchronized (currentTexturesMap) {
                        currentTexturesMap.put(next.texture, next);
                    }
                } else {
                    processEntry(next);
                    try {
                        Thread.sleep((long) (((float) Math.sqrt((double) next.mipMapResolution)) * 10.0f));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            secondaryTexturesMap.clear();
            processing = false;
        }
    }

    public void run() {
        while (!this.shouldStop) {
            process();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setBasePath(FilePath ain) {
        rootpath = ain;
    }

    /* renamed from: taikodom.render.loader.provider.ImageLoader$a */
    static class C5149a implements TextureData.Flusher {
        C5149a() {
        }

        public void flush() {
        }
    }

    /* renamed from: taikodom.render.loader.provider.ImageLoader$b */
    /* compiled from: a */
    static class C5150b implements TextureData.Flusher {
        C5150b() {
        }

        public void flush() {
        }
    }
}
