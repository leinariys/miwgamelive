package taikodom.render.loader.provider;

import logic.thred.LogPrinter;
import taikodom.render.textures.*;

import java.util.*;
import java.util.concurrent.Semaphore;

/* compiled from: a */
public class ResourceCleaner implements Runnable {
    static final int resourceAdaptationTime = 20000;
    static final int resourceLifeTime = 20000;
    private static final int CLEANER_INTERVAL = 5000;
    private static final int DEGRADE_MIN_TIME_PER_TEXTURE = 15000;
    private static final int FIRST_MEMORY_CAP = 40;
    private static final int FOURTH_MEMORY_CAP = 100;
    private static final float LOW_THRESHOLD_FACTOR = 1.2f;
    private static final int MAX_AGE_TO_DEGRADE = 40000;
    private static final int SECOND_MEMORY_CAP = 60;
    private static final int THIRD_MEMORY_CAP = 80;
    static Semaphore loaderLock = new Semaphore(1);
    static Stack<Texture> recentEntries = new Stack<>();
    static Thread resCleanerThread = null;
    static List<Texture> resources = new ArrayList(100);
    static int sizeBinkTextures;
    static int sizeDDSTextures;
    static int sizeGuiTextures;
    static int sizeImageTextures;
    static int sizeTotalTextures;
    private static boolean LOG_RESOURCE_CLEANER = false;
    private static Comparator<Texture> comparator = null;
    private static float deltaTime = 0.0f;
    private static float lastTime = 0.0f;
    private static LogPrinter logger = LogPrinter.m10275K(ResourceCleaner.class);
    boolean shouldStop = false;

    public ResourceCleaner() {
        resCleanerThread = new Thread(this, "ResourceCleaner");
        resCleanerThread.setDaemon(true);
        resCleanerThread.start();
    }

    public static void addTexture(Texture texture) {
        if (texture.getDataSource() != null) {
            if (texture.getDataSource() instanceof DDSDataSource) {
                texture.getDataSource().setLifeTime(20000);
            } else if (texture.getDataSource() instanceof BinkDataSource) {
                texture.getDataSource().setLifeTime(20000);
            } else if (texture.getDataSource() instanceof RGraphicsDataSource) {
                texture.getDataSource().setLifeTime(200000);
            }
            synchronized (recentEntries) {
                recentEntries.push(texture);
            }
        }
    }

    public static void cleanDDS(int i, float f) {
        int i2;
        deltaTime = ((float) System.nanoTime()) - lastTime;
        lastTime = (float) System.nanoTime();
        int i3 = 0;
        while (i3 < resources.size()) {
            try {
                Texture texture = resources.get(i3);
                TextureDataSource dataSource = resources.get(i3).getDataSource();
                if (dataSource instanceof DDSDataSource) {
                    if (dataSource.getWidth() < i) {
                        i2 = i3;
                    } else {
                        if (!texture.isUpdatingData() && texture.isInitialized()) {
                            texture.setPersistencePriority(Math.max(0.0f, texture.getPersistencePriority() - (deltaTime * 1.0E-8f)));
                            long currentTimeMillis = System.currentTimeMillis() - texture.getLastBindTime();
                            if (((float) currentTimeMillis) > (((float) dataSource.getLifeTime()) * f) + texture.getPersistencePriority()) {
                                destroyTexture(texture, dataSource);
                                if (LOG_RESOURCE_CLEANER) {
                                    logger.info("Texture " + texture.getName() + " wasnt bound in the last " + (((float) currentTimeMillis) * 0.001f) + "s. Destroying it.");
                                }
                                cleanTexture(texture);
                                resources.remove(i3);
                                i2 = i3 - 1;
                            } else if (System.currentTimeMillis() - texture.getLastDegradationTime() > 15000) {
                                texture.setLastDegradationTime(System.currentTimeMillis());
                                if (LOG_RESOURCE_CLEANER) {
                                    logger.info("Texture " + texture.getName() + " wasnt bound in the last " + (((float) currentTimeMillis) * 0.001f) + "s. Destroying it.");
                                }
                                processDDS(texture, (DDSDataSource) dataSource);
                            }
                        }
                        Thread.sleep(10);
                    }
                    i3 = i2 + 1;
                }
                i2 = i3;
                i3 = i2 + 1;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    public static void cleanGui(int i) {
        deltaTime = ((float) System.nanoTime()) - lastTime;
        lastTime = (float) System.nanoTime();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < resources.size()) {
                Texture texture = resources.get(i3);
                TextureDataSource dataSource = resources.get(i3).getDataSource();
                if ((texture.getDataSource() instanceof RGraphicsDataSource) && dataSource.getWidth() >= i) {
                    if (!texture.isUpdatingData() && texture.isInitialized()) {
                        texture.setPersistencePriority(Math.max(0.0f, texture.getPersistencePriority() - (deltaTime * 1.0E-8f)));
                        if (((float) (System.currentTimeMillis() - texture.getLastBindTime())) > ((float) dataSource.getLifeTime()) + texture.getPersistencePriority()) {
                            destroyTexture(texture, dataSource);
                            cleanTexture(texture);
                            resources.remove(i3);
                            i3--;
                        } else if (System.currentTimeMillis() - texture.getLastBindTime() < 20000) {
                            processImage(texture, dataSource);
                        }
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public static void cleanBink(int i) {
        deltaTime = ((float) System.nanoTime()) - lastTime;
        lastTime = (float) System.nanoTime();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < resources.size()) {
                Texture texture = resources.get(i3);
                TextureDataSource dataSource = resources.get(i3).getDataSource();
                if ((texture.getDataSource() instanceof BinkDataSource) && dataSource.getWidth() >= i) {
                    if (texture.isInitialized()) {
                        texture.setPersistencePriority(Math.max(0.0f, texture.getPersistencePriority() - (deltaTime * 1.0E-8f)));
                        if (((float) (System.currentTimeMillis() - texture.getLastBindTime())) > ((float) dataSource.getLifeTime()) + texture.getPersistencePriority()) {
                            destroyTexture(texture, dataSource);
                            cleanTexture(texture);
                            resources.remove(i3);
                            i3--;
                        }
                    }
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private static void destroyUnusedTextures(int i, float f) {
        int i2;
        deltaTime = ((float) System.nanoTime()) - lastTime;
        lastTime = (float) System.nanoTime();
        long currentTimeMillis = System.currentTimeMillis();
        int i3 = 0;
        while (i3 < resources.size()) {
            try {
                Texture texture = resources.get(i3);
                TextureDataSource dataSource = resources.get(i3).getDataSource();
                if (dataSource instanceof DDSDataSource) {
                    if (dataSource.getWidth() < i) {
                        i2 = i3;
                    } else {
                        long lastBindTime = currentTimeMillis - texture.getLastBindTime();
                        if (((float) lastBindTime) <= ((float) dataSource.getLifeTime()) * f) {
                            return;
                        }
                        if (texture.isUpdatingData() || !texture.isInitialized()) {
                            Thread.sleep(10);
                        } else {
                            destroyTexture(texture, dataSource);
                            if (LOG_RESOURCE_CLEANER) {
                                logger.info("Texture " + texture.getName() + " wasnt bound in the last " + (((float) lastBindTime) * 0.001f) + "s. Destroying it.");
                            }
                            cleanTexture(texture);
                            resources.remove(i3);
                            i2 = i3 - 1;
                        }
                    }
                    i3 = i2 + 1;
                }
                i2 = i3;
                i3 = i2 + 1;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    private static void cleanTexture(Texture texture) {
        texture.getDataSource().setReady(false);
        texture.getDataSource().clearData();
    }

    private static void degradeFarTextures(int i, float f) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            for (int size = resources.size() - 1; size >= 0; size--) {
                Texture texture = resources.get(size);
                TextureDataSource dataSource = resources.get(size).getDataSource();
                if ((dataSource instanceof DDSDataSource) && dataSource.getWidth() >= i) {
                    if (((float) (currentTimeMillis - texture.getLastBindTime())) < 40000.0f * f) {
                        if (currentTimeMillis - texture.getLastDegradationTime() > 15000) {
                            texture.setLastDegradationTime(currentTimeMillis);
                            if (LOG_RESOURCE_CLEANER) {
                                logger.info("Check if texture " + texture.getName() + " can be reduced to a lower resolution.");
                            }
                            processDDS(texture, (DDSDataSource) dataSource);
                        }
                        Thread.sleep(10);
                    } else {
                        return;
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void processOldUpdates() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < resources.size()) {
                Texture texture = resources.get(i2);
                if (System.currentTimeMillis() - texture.getDataSource().getLastRequestTime() > 30000 && texture.getDataSource().isReady()) {
                    cleanTexture(texture);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private static void process() {
        synchronized (recentEntries) {
            resources.addAll(recentEntries);
            recentEntries.clear();
        }
        if (comparator == null) {
            comparator = new C5151a();
        }
        Collections.sort(resources, comparator);
        sizeGuiTextures = 0;
        sizeDDSTextures = 0;
        sizeImageTextures = 0;
        sizeBinkTextures = 0;
        for (int i = 0; i < resources.size(); i++) {
            Texture texture = resources.get(i);
            if (texture.getDataSource() instanceof DDSDataSource) {
                sizeDDSTextures = texture.getMemorySize() + sizeDDSTextures;
            } else if (texture.getDataSource() instanceof RGraphicsDataSource) {
                sizeGuiTextures = texture.getMemorySize() + sizeGuiTextures;
            } else if (texture.getDataSource() instanceof Texture2DDataSource) {
                sizeImageTextures = texture.getMemorySize() + sizeImageTextures;
            } else if (texture.getDataSource() instanceof BinkDataSource) {
                sizeBinkTextures = texture.getMemorySize() + sizeBinkTextures;
            }
        }
        sizeDDSTextures /= 1000000;
        sizeGuiTextures /= 1000000;
        sizeImageTextures /= 1000000;
        sizeBinkTextures /= 1000000;
        if (sizeDDSTextures > 100) {
            destroyUnusedTextures(32, 0.5f);
            degradeFarTextures(32, 0.7f);
        } else if (sizeDDSTextures > 80) {
            destroyUnusedTextures(32, 1.0f);
            degradeFarTextures(128, 0.8f);
        } else if (sizeDDSTextures > 60) {
            destroyUnusedTextures(128, 1.0f);
            degradeFarTextures(256, 0.9f);
        } else if (sizeDDSTextures > 40) {
            destroyUnusedTextures(512, 1.0f);
            degradeFarTextures(512, 1.0f);
        }
        if (sizeGuiTextures > 80) {
            cleanGui(0);
        } else if (sizeGuiTextures > 60) {
            cleanGui(32);
        } else if (sizeGuiTextures > 40) {
            cleanGui(256);
        } else if (sizeGuiTextures > 25) {
            cleanGui(512);
        } else if (sizeGuiTextures > 15) {
            cleanGui(1024);
        }
        if (sizeBinkTextures > 30) {
            cleanBink(0);
        } else if (sizeBinkTextures > 20) {
            cleanBink(32);
        } else if (sizeBinkTextures > 10) {
            cleanBink(256);
        } else if (sizeBinkTextures > 5) {
            cleanBink(512);
        }
        processOldUpdates();
    }

    private static void destroyTexture(Texture texture, TextureDataSource textureDataSource) {
        texture.destroyInternalTexture();
    }

    private static void processImage(Texture texture, TextureDataSource textureDataSource) {
        textureDataSource.setLifeTime(200000);
    }

    private static void processDDS(Texture texture, DDSDataSource dDSDataSource) {
        texture.resetTextureArea();
        float screenArea = texture.getScreenArea();
        dDSDataSource.setActualAreaSize(screenArea);
        float f = screenArea * LOW_THRESHOLD_FACTOR;
        if (f <= ((float) ImageLoader.getMinTextureSize())) {
            f = (float) ImageLoader.getMinTextureSize();
        }
        if (dDSDataSource.getDegradationLevel() < dDSDataSource.getNumMipMaps() && f < ((float) (dDSDataSource.getMipMapResolution() >> 1))) {
            ImageLoader.addTexture(texture, dDSDataSource.getMipMapResolution() >> 1, false);
        }
    }

    private static boolean degradeDDS(Texture texture, TextureDataSource textureDataSource) {
        if (textureDataSource.getWidth() > ImageLoader.getMinTextureSize()) {
            ImageLoader.addTexture(texture, textureDataSource.getWidth() >> 1, false);
            ImageLoader.addForcedUpdateTexture(texture);
            textureDataSource.incLifeTime(20000);
            return true;
        }
        destroyTexture(texture, textureDataSource);
        return false;
    }

    public static void showTexturesStats() {
        logger.info("Num of textures: " + resources.size() + " : " + BaseTexture.getTextureMemoryUsed() + " Bytes allocated");
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i < resources.size()) {
            Texture texture = resources.get(i);
            TextureDataSource dataSource = texture.getDataSource();
            if (dataSource instanceof DDSDataSource) {
                DDSDataSource dDSDataSource = (DDSDataSource) dataSource;
                logger.info("Texture " + dDSDataSource.getFileName() + "\n\t" + " Current Resolution:" + dDSDataSource.getWidth() + " of " + dDSDataSource.getRealWidth() + " " + " LifeTime:" + (System.currentTimeMillis() - texture.getLastBindTime()) + " of " + (((float) dataSource.getLifeTime()) + texture.getPersistencePriority()) + " size:" + texture.getMemorySize() + " bytes - DDSImage");
                i5 += texture.getMemorySize();
            } else if (dataSource instanceof RGraphicsDataSource) {
                i4 += texture.getMemorySize();
            } else if (dataSource instanceof BinkDataSource) {
                i2 += texture.getMemorySize();
            } else {
                i3 += texture.getMemorySize();
            }
            i++;
            i2 = i2;
            i3 = i3;
        }
        logger.info("internal total DDS texture size: " + i5);
        logger.info("internal total RGraphics2 texture size: " + i4);
        logger.info("internal total Image texture size: " + i3);
        logger.info("internal total bink texture size: " + i2);
        logger.info("internal total texture size: " + (i5 + i4 + i3 + i2));
    }

    public void finalize() {
        this.shouldStop = true;
    }

    public void run() {
        while (!this.shouldStop) {
            process();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: taikodom.render.loader.provider.ResourceCleaner$a */
    class C5151a implements Comparator<Texture> {
        C5151a() {
        }

        /* renamed from: a */
        public int compare(Texture texture, Texture texture2) {
            long lastBindTime = texture.getLastBindTime();
            long lastBindTime2 = texture2.getLastBindTime();
            long lifeTime = lastBindTime - ((long) texture.getDataSource().getLifeTime());
            long lifeTime2 = lastBindTime2 - ((long) texture2.getDataSource().getLifeTime());
            if (lifeTime < lifeTime2) {
                return -1;
            }
            if (lifeTime > lifeTime2) {
                return 1;
            }
            return 0;
        }
    }
}
