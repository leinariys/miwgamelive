package taikodom.render.loader.provider;

import logic.res.XmlNode;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.loader.*;
import taikodom.render.loader.descriptors.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/* compiled from: a */
@Slf4j
public class ProLoader extends FormatProvider {

    public ProLoader(SceneLoader sceneLoader, String... strArr) {
        super(sceneLoader, strArr);
    }

    public void loadFile(InputStream inputStream, String str, String str2) throws IOException {
        XmlNode a = new LoaderFileXML().loadFileXml(inputStream, "utf-8");
        StockFile stockFile = new StockFile();
        stockFile.setName(str);
        XmlNode mD = a.findNodeChildTagDepth("includes");
        if (mD != null && mD.getListChildrenTag().size() > 0) {
            for (XmlNode attribute : mD.getListChildrenTag()) {
                String attribute2 = attribute.getAttribute("file");
                if (attribute2 != null) {
                    this.loader.addInclude(stockFile, attribute2);
                }
            }
        }
        XmlNode mD2 = a.findNodeChildTagDepth("objects");
        if (mD2 != null) {
            for (XmlNode next : mD2.getListChildrenTag()) {
                String name = next.getTagName();
                ClassDescriptor classDescriptor = this.loader.getClassDescriptor(name);
                if (classDescriptor == null) {
                    log.warn("no class descriptor for: [" + name + "]");
                } else if (next.getAttribute("name") == null) {
                    log.warn("attribute not found: [name]");
                } else {
                    String attribute3 = next.getAttribute("name");
                    String attribute4 = next.getAttribute("clonesFrom");
                    StockEntry stockEntry = new StockEntry(attribute3);
                    if (attribute4 != null) {
                        stockEntry.setClonesFrom(attribute4);
                    } else {
                        RenderAsset renderAsset = (RenderAsset) classDescriptor.createInstance();
                        if (renderAsset != null) {
                            stockEntry.setAsset(renderAsset);
                        }
                    }
                    parseXmlTag(stockFile, classDescriptor, next, stockEntry.unassignedProperties);
                    this.loader.addEntry(stockFile, stockEntry);
                }
            }
        }
        addStockFile(stockFile);
    }

    /* access modifiers changed from: package-private */
    public void parseXmlTag(StockFile stockFile, ClassDescriptor classDescriptor, XmlNode agy, List<UnassignedPropertyBase> list) {
        int i;
        PropertyDescriptor property;
        ObjectUnassignedProperty objectUnassignedProperty = null;
        for (Map.Entry next : agy.getAttributes().entrySet()) {
            PropertyDescriptor property2 = classDescriptor.getProperty((String) next.getKey());
            if (property2 != null) {
                if (property2.isObjectArray()) {
                    log.warn(String.valueOf(property2));
                } else if (property2.isObject()) {
                    list.add(new ObjectUnassignedProperty(property2, (String) next.getValue(), false, -1, (RenderAsset) null, false));
                } else if (property2.isObjectArray()) {
                    log.warn(String.valueOf(property2));
                } else {
                    list.add(new UnassignedPropertyBase(property2, (String) next.getValue(), -1));
                }
            }
        }
        for (XmlNode next2 : agy.getListChildrenTag()) {
            String name = next2.getTagName();
            if (next2.getAttribute("index") != null) {
                i = Integer.parseInt(next2.getAttribute("index"));
                property = classDescriptor.getProperty(String.valueOf(name) + "[]");
            } else {
                i = -1;
                property = classDescriptor.getProperty(name);
                if (property == null) {
                    property = classDescriptor.getProperty(String.valueOf(name) + "[]");
                }
            }
            if (property != null) {
                if (property.isStructArray()) {
                    ClassDescriptor classDescriptor2 = this.loader.getClassDescriptor(property.getPropertyTypeName());
                    StructUnassignedProperty structUnassignedProperty = new StructUnassignedProperty(this.loader, property, i, classDescriptor2.createInstance());
                    list.add(structUnassignedProperty);
                    for (Map.Entry next3 : next2.getAttributes().entrySet()) {
                        PropertyDescriptor property3 = classDescriptor2.getProperty((String) next3.getKey());
                        if (property3 != null) {
                            if (property3.isObjectArray()) {
                                log.warn("PropertyDescriptor is object array " + ((String) next3.getKey()));
                            } else if (property3.isObject()) {
                                log.warn("PropertyDescriptor is object " + ((String) next3.getKey()));
                                log.warn((String) next3.getKey());
                            } else if (property3.isObjectArray()) {
                                log.warn((String) next3.getKey());
                            } else {
                                structUnassignedProperty.subProperties.add(new UnassignedPropertyBase(property3, (String) next3.getValue(), -1));
                            }
                        }
                    }
                } else if (property.isObjectArray()) {
                    if (next2.getAttribute("name") != null) {
                        list.add(new ObjectUnassignedProperty(property, next2.getAttribute("name"), true, i, (RenderAsset) null, false));
                    } else {
                        for (XmlNode next4 : next2.getListChildrenTag()) {
                            ClassDescriptor classDescriptor3 = this.loader.getClassDescriptor(next4.getTagName());
                            if (classDescriptor3 != null) {
                                String attribute = next4.getAttribute("name");
                                String attribute2 = next4.getAttribute("clonesFrom");
                                if (attribute != null) {
                                    StockEntry stockEntry = new StockEntry(attribute);
                                    if (attribute2 != null) {
                                        stockEntry.setClonesFrom(attribute2);
                                    } else {
                                        RenderAsset renderAsset = (RenderAsset) classDescriptor3.createInstance();
                                        if (renderAsset != null) {
                                            stockEntry.setAsset(renderAsset);
                                        }
                                    }
                                    parseXmlTag(stockFile, classDescriptor3, next4, stockEntry.unassignedProperties);
                                    this.loader.addEntry(stockFile, stockEntry);
                                    list.add(new ObjectUnassignedProperty(property, attribute, true, i, (RenderAsset) null, false));
                                }
                                if (attribute == null) {
                                    if (attribute2 != null) {
                                        objectUnassignedProperty = new ObjectUnassignedProperty(property, attribute2, true, i, (RenderAsset) null, true);
                                    } else {
                                        RenderAsset renderAsset2 = (RenderAsset) classDescriptor3.createInstance();
                                        if (renderAsset2 != null) {
                                            objectUnassignedProperty = new ObjectUnassignedProperty(property, "", true, i, renderAsset2, false);
                                        }
                                    }
                                    parseXmlTag(stockFile, classDescriptor3, next4, objectUnassignedProperty.subProperties);
                                    list.add(objectUnassignedProperty);
                                }
                                if (i >= 0) {
                                    i++;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String getXML(StockFile stockFile) {
        XmlNode agy = new XmlNode("root");
        agy.addAttribute("xmlns", "http://scene.gametoolkit.hoplon.com/scene");
        agy.addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        agy.addAttribute("xsi:schemaLocation", "http://scene.gametoolkit.hoplon.com/scene ../scene.xsd");
        if (stockFile.getIncludes().size() > 0) {
            XmlNode k = agy.addChildrenTag(new XmlNode("includes"));
            for (String aO : stockFile.getIncludes()) {
                k.addChildrenTag(new XmlNode("include")).addAttribute("file", aO);
            }
        }
        if (stockFile.getEntries().size() > 0) {
            XmlNode k2 = agy.addChildrenTag(new XmlNode("objects"));
            for (int i = 0; i < stockFile.entryCount(); i++) {
                StockEntry entry = stockFile.getEntry(i);
                RenderAsset asset = entry.getAsset();
                XmlNode k3 = k2.addChildrenTag(new XmlNode(this.loader.getClassDescriptor(asset).getName()));
                k3.addAttribute("name", entry.getName());
                if (entry.getClonesFrom() != null) {
                    k3.addAttribute("clonesFrom", entry.getClonesFrom());
                }
                addXmlTag(k3, asset);
            }
        }
        return String.valueOf("<?xml version=\"1.0\"?>\n") + agy.toString();
    }

    /* access modifiers changed from: package-private */
    public void addXmlTag(XmlNode agy, RenderAsset renderAsset) {
        for (PropertyDescriptor next : this.loader.getClassDescriptor(renderAsset).getProperties()) {
            if (next.isObjectArray()) {
                if (next.arrayItemCount(renderAsset) > 0) {
                    int arrayItemCount = next.arrayItemCount(renderAsset);
                    for (int i = 0; i < arrayItemCount; i++) {
                        RenderAsset renderAsset2 = (RenderAsset) next.getObjectArrayItem(renderAsset, i);
                        StockEntry entry = this.loader.getEntry(renderAsset2);
                        if (entry == null && renderAsset2 != null) {
                            entry = this.loader.getStockEntry(renderAsset2.getName());
                        }
                        if (renderAsset2 != null) {
                            XmlNode k = agy.addChildrenTag(new XmlNode(next.getName()));
                            k.addAttribute("index", Integer.toString(i));
                            k.addAttribute("name", renderAsset2.getName());
                        } else if (entry != null) {
                            XmlNode k2 = agy.addChildrenTag(new XmlNode(next.getName()));
                            k2.addAttribute("index", Integer.toString(i));
                            k2.addAttribute("name", entry.getName());
                        }
                    }
                }
            } else if (next.isObject()) {
                RenderAsset renderAsset3 = (RenderAsset) next.getAsObject(renderAsset);
                StockEntry entry2 = this.loader.getEntry(renderAsset3);
                if (renderAsset3 != null) {
                    agy.addAttribute(next.getName(), renderAsset3.getName());
                } else if (entry2 != null) {
                    agy.addAttribute(next.getName(), entry2.getName());
                }
            } else if (!next.hasDefaultValue(renderAsset)) {
                agy.addAttribute(next.getName(), next.getAsString(renderAsset));
            }
        }
    }
}
