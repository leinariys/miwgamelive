package taikodom.render.loader.provider;

import java.io.OutputStream;
import java.nio.ByteBuffer;

/* renamed from: a.akK  reason: case insensitive filesystem */
/* compiled from: a */
public final class C6298akK extends OutputStream {
    private ByteBuffer fUn;

    public C6298akK() {
        this(16);
    }

    public C6298akK(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Invalid size: " + i);
        }
        this.fUn = ByteBuffer.allocateDirect(i);
    }

    public final void write(int i) {
        ensure(size() + 1);
        this.fUn.put((byte) i);
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (i2 != 0) {
            ensure(size() + i2);
            this.fUn.put(bArr, i, i2);
        }
    }

    private final void ensure(int i) {
        if (i > this.fUn.capacity()) {
            ByteBuffer allocateDirect = ByteBuffer.allocateDirect(Math.max((this.fUn.position() * 3) / 2, i));
            this.fUn.flip();
            allocateDirect.put(this.fUn);
            this.fUn = allocateDirect;
        }
    }

    public final void reset() {
        this.fUn.clear();
    }

    public final int size() {
        return this.fUn.position();
    }

    public final ByteBuffer chm() {
        return this.fUn;
    }

    public String toString() {
        return super.toString();
    }
}
