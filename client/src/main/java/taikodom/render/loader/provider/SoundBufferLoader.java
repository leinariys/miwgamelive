package taikodom.render.loader.provider;

import logic.thred.LogPrinter;
import taikodom.render.MilesSoundBuffer;
import taikodom.render.MilesSoundStream;
import taikodom.render.SoundBuffer;
import taikodom.render.graphics2d.C0559Hm;
import taikodom.render.loader.FormatProvider;
import taikodom.render.loader.SceneLoader;
import taikodom.render.loader.StockFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: a */
public class SoundBufferLoader extends FormatProvider implements Runnable {
    private static int INTERVAL_BETWEEN_ENTRIES = 10;
    private static int THREAD_INTERVAL = 200;
    private static List<SoundBuffer> loadingList = new ArrayList();
    private static LogPrinter logger = LogPrinter.m10275K(SoundBufferLoader.class);
    private static Set<SoundBuffer> toLoadList = new HashSet();
    private boolean shouldStop = false;

    public SoundBufferLoader(SceneLoader sceneLoader, String... strArr) {
        super(sceneLoader, strArr);
        Thread thread = new Thread(this, "SoundBufferLoader");
        thread.setDaemon(true);
        thread.start();
    }

    private static void loadBuffer(SoundBuffer soundBuffer) throws FileNotFoundException {
        File file = new File(soundBuffer.getFileName());
        if (!file.exists()) {
            throw new FileNotFoundException(soundBuffer.getFileName());
        }
        soundBuffer.setData(C0559Hm.m5263p(file), (int) file.length());
    }

    public static void addBufferRequest(SoundBuffer soundBuffer) {
        synchronized (toLoadList) {
            toLoadList.add(soundBuffer);
        }
    }

    public static void syncLoadBuffer(MilesSoundBuffer milesSoundBuffer) {
        if (milesSoundBuffer != null) {
            synchronized (loadingList) {
                if (milesSoundBuffer.getData() == null) {
                    try {
                        loadBuffer(milesSoundBuffer);
                    } catch (FileNotFoundException e) {
                        logger.error("Sound file not found: " + e);
                    }
                }
                loadingList.remove(milesSoundBuffer);
            }
            synchronized (toLoadList) {
                toLoadList.remove(milesSoundBuffer);
            }
        }
    }

    public void loadFile(InputStream inputStream, String str, String str2) {
        String substring = str.substring(str.lastIndexOf("."));
        StockFile stockFile = new StockFile();
        stockFile.setName(str);
        if (substring.equals(".mp3") || substring.equals(".ogg")) {
            MilesSoundStream milesSoundStream = new MilesSoundStream();
            milesSoundStream.setName(str);
            milesSoundStream.setFileName(str2);
            addEntry(stockFile, str, milesSoundStream);
        } else {
            MilesSoundBuffer milesSoundBuffer = new MilesSoundBuffer();
            milesSoundBuffer.setExtension(substring);
            milesSoundBuffer.setName(str);
            milesSoundBuffer.setFileName(str2);
            addEntry(stockFile, str, milesSoundBuffer);
        }
        addStockFile(stockFile);
    }

    public void finalize() {
        this.shouldStop = true;
    }

    public void run() {
        while (!this.shouldStop) {
            process();
            try {
                Thread.sleep((long) THREAD_INTERVAL);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void process() {
        synchronized (toLoadList) {
            loadingList.addAll(toLoadList);
            toLoadList.clear();
        }
        synchronized (loadingList) {
            for (SoundBuffer loadBuffer : loadingList) {
                try {
                    loadBuffer(loadBuffer);
                    Thread.sleep((long) INTERVAL_BETWEEN_ENTRIES);
                } catch (FileNotFoundException e) {
                    logger.error("Sound file not found./n" + e);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        synchronized (toLoadList) {
            toLoadList.removeAll(loadingList);
            loadingList.clear();
        }
    }
}
