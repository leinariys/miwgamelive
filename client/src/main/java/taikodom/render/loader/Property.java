package taikodom.render.loader;

/* compiled from: a */
public @interface Property {
    boolean editable() default true;

    String group() default "";

    String label() default "";

    String name();

    boolean persisted() default true;

    boolean writable() default true;
}
