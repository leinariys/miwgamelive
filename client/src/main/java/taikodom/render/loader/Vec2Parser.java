package taikodom.render.loader;

import game.geometry.Vector2fWrap;

/* renamed from: a.Wr */
/* compiled from: a */
public class Vec2Parser implements IParser {
    /* renamed from: O */
    public Object mo2718O(String str) {
        String[] split = str.split("[ \t\r\n]+");
        return new Vector2fWrap(Float.parseFloat(split[0]), Float.parseFloat(split[1]));
    }
}
