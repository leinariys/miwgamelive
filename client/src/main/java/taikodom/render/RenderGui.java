package taikodom.render;

import taikodom.render.camera.Camera;
import taikodom.render.gui.GuiScene;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.RenderStates;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;

import javax.media.opengl.GL;

/* compiled from: a */
public class RenderGui {
    private Camera camera = new Camera();
    private RenderStates defaultStates = new RenderStates();
    private GuiContext guiContext = new GuiContext();

    public RenderGui() {
        this.camera.setOrthogonal(true);
        this.camera.setFarPlane(1.0f);
        this.camera.setNearPlane(1.0f);
    }

    private void unbindPrimitive() {
        this.guiContext.getDc().getGl().glBindBufferARB(34962, 0);
        this.guiContext.getDc().getGl().glBindBufferARB(34963, 0);
    }

    public void render(GuiScene guiScene) {
        DrawContext dc = this.guiContext.getDc();
        GL gl = dc.getGl();
        this.guiContext.clearRecords();
        RenderStates renderStates = this.defaultStates;
        Viewport viewport = this.guiContext.viewPort;
        gl.glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
        dc.setViewport(viewport.x, viewport.y, viewport.width, viewport.height);
        guiScene.render(this.guiContext);
        this.defaultStates.blindlyUpdateGLStates(dc);
        gl.glLoadIdentity();
        this.camera.updateGLProjection(dc);
        gl.glEnableClientState(32884);
        int i = 0;
        while (i < this.guiContext.getRecords().size()) {
            try {
                TreeRecord treeRecord = this.guiContext.getRecords().get(i);
                Shader shader = treeRecord.shader;
                for (int i2 = 0; i2 < treeRecord.material.getShader().passCount(); i2++) {
                    unbindPrimitive();
                    ShaderPass pass = shader.getPass(i2);
                    dc.currentPass = pass;
                    pass.updateStates(dc, renderStates);
                    renderStates = dc.currentPass.getRenderStates();
                    renderStates.fillPrimitiveStates(dc.primitiveState);
                    renderStates.fillMaterialStates(dc.materialState);
                    dc.getGl().glMatrixMode(5890);
                    gl.glLoadIdentity();
                    treeRecord.material.bind(dc);
                    dc.getGl().glMatrixMode(5888);
                    dc.floatBuffer60Temp0.clear();
                    gl.glPushMatrix();
                    gl.glMultMatrixf(treeRecord.gpuTransform.mo13998b(dc.floatBuffer60Temp0));
                    gl.glColor4f(treeRecord.color.x, treeRecord.color.y, treeRecord.color.z, treeRecord.color.w);
                    treeRecord.primitive.bind(dc);
                    treeRecord.primitive.draw(dc);
                    pass.unbind(dc);
                    gl.glPopMatrix();
                }
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            } finally {
                dc.getGl().glMatrixMode(5890);
                gl.glLoadIdentity();
                dc.getGl().glMatrixMode(5888);
                gl.glDisableClientState(32884);
                this.defaultStates.blindlyUpdateGLStates(dc);
            }
        }
    }

    public void resize(int i, int i2, int i3, int i4) {
        this.camera.setOrthoWindow(0.0f, (float) i3, 0.0f, (float) i4);
        this.guiContext.setViewPort(i, i2, i3, i4);
    }

    public GuiContext getGuiContext() {
        return this.guiContext;
    }

    public void setGuiContext(GuiContext guiContext2) {
        this.guiContext = guiContext2;
    }
}
