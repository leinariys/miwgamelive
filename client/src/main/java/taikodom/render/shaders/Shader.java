package taikodom.render.shaders;


import lombok.extern.slf4j.Slf4j;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
@Slf4j
public class Shader extends RenderAsset {

    public boolean compiled;
    public Shader fallback;
    public int priority;
    public int quality = -1;
    public List<ShaderPass> shaderPasses = new ArrayList();
    public boolean validated;

    public boolean isCompiled() {
        return this.compiled;
    }

    public void setCompiled(boolean z) {
        this.compiled = z;
    }

    public boolean isValidated() {
        return this.validated;
    }

    public void setValidated(boolean z) {
        this.validated = z;
    }

    public Shader getFallback() {
        return this.fallback;
    }

    public void setFallback(Shader shader) {
        this.fallback = shader;
    }

    public void addPass(ShaderPass shaderPass) {
        this.shaderPasses.add(shaderPass);
    }

    public Shader getBestShader(DrawContext drawContext) {
        Shader fallback2;
        if (!this.compiled) {
            compile(drawContext);
        }
        int i = 0;
        while (true) {
            if (((this.quality == -1 || this.quality >= drawContext.getMaxShaderQuality()) && this.validated) || (fallback2 = this.getFallback()) == null) {
                return this;
            }
            i++;
            if (i == 15) {
                throw new IllegalStateException("Infinite fallback loop in " + fallback2.getName() + " shader.");
            }
            //todo
            //   this = fallback2;
        }
        //  return this;
    }

    public List<ShaderPass> getShaderPasses() {
        return this.shaderPasses;
    }

    public int getQuality() {
        return this.quality;
    }

    public void setQuality(int i) {
        this.quality = i;
    }

    public void compile(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logCompile(this);
        }
        if (!this.compiled) {
            this.compiled = true;
            this.validated = true;
            for (int i = 0; i < this.shaderPasses.size(); i++) {
                if (!this.shaderPasses.get(i).compile(drawContext)) {
                    this.validated = false;
                }
            }
            if (this.fallback != null) {
                this.fallback.compile(drawContext);
            }
        }
    }

    public ShaderPass getPass(int i) {
        return this.shaderPasses.get(i);
    }

    public void setPass(int i, ShaderPass shaderPass) {
        if (this.shaderPasses.size() == i || i == -1) {
            this.shaderPasses.add(shaderPass);
        } else {
            this.shaderPasses.set(i, shaderPass);
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public int passCount() {
        return this.shaderPasses.size();
    }

    public void clearPasses() {
        this.shaderPasses.clear();
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.shaderPasses.clear();
        this.fallback = null;
    }

    public int getPriority() {
        return this.priority;
    }

    public void setPriority(int i) {
        this.priority = i;
    }
}
