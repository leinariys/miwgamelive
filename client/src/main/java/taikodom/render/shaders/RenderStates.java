package taikodom.render.shaders;

import taikodom.render.DrawContext;
import taikodom.render.MaterialDrawingStates;
import taikodom.render.PrimitiveDrawingState;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.CullFace;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.DDSLoader;

import javax.media.opengl.GL;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class RenderStates {
    public DepthFuncType alphaFunc = DepthFuncType.GREATER;
    public boolean alphaMask = true;
    public float alphaRef = 0.0f;
    public boolean alphaTestEnabled = false;
    public boolean blendEnabled = false;
    public List<ChannelInfo> channels = new ArrayList();
    public boolean colorArrayEnabled = false;
    public boolean colorMask = true;
    public CullFace cullFace = CullFace.BACK;
    public boolean cullFaceEnabled = true;
    public DepthFuncType depthFunc = DepthFuncType.LEQUAL;
    public boolean depthMask = true;
    public float depthRangeMax = 1.0f;
    public float depthRangeMin = 0.0f;
    public boolean depthTestEnabled = true;
    public BlendType dstBlend = BlendType.ONE;
    public boolean fogEnabled = false;
    public boolean glLightingEnabled = false;
    public boolean lineMode = false;
    public float lineWidth = 1.0f;
    public boolean normalArrayEnabled = false;
    public BlendType srcBlend = BlendType.ONE;
    public boolean tangentArrayEnabled = false;
    public int tangentChannel = 0;

    public float getAlphaRef() {
        return this.alphaRef;
    }

    public void setAlphaRef(float f) {
        this.alphaRef = f;
    }

    public DepthFuncType getAlphaFunc() {
        return this.alphaFunc;
    }

    public void setAlphaFunc(DepthFuncType depthFuncType) {
        this.alphaFunc = depthFuncType;
    }

    public boolean isAlphaTestEnabled() {
        return this.alphaTestEnabled;
    }

    public void setAlphaTestEnabled(boolean z) {
        this.alphaTestEnabled = z;
    }

    public BlendType getDstBlend() {
        return this.dstBlend;
    }

    public void setDstBlend(BlendType blendType) {
        this.dstBlend = blendType;
    }

    public BlendType getSrcBlend() {
        return this.srcBlend;
    }

    public void setSrcBlend(BlendType blendType) {
        this.srcBlend = blendType;
    }

    public boolean isBlendEnabled() {
        return this.blendEnabled;
    }

    public void setBlendEnabled(boolean z) {
        this.blendEnabled = z;
    }

    public boolean isColorMask() {
        return this.colorMask;
    }

    public void setColorMask(boolean z) {
        this.colorMask = z;
    }

    public CullFace getCullFace() {
        return this.cullFace;
    }

    public void setCullFace(CullFace cullFace2) {
        this.cullFace = cullFace2;
    }

    public boolean isCullFaceEnabled() {
        return this.cullFaceEnabled;
    }

    public void setCullFaceEnabled(boolean z) {
        this.cullFaceEnabled = z;
    }

    public boolean isDepthMask() {
        return this.depthMask;
    }

    public void setDepthMask(boolean z) {
        this.depthMask = z;
    }

    public boolean isDepthTestEnabled() {
        return this.depthTestEnabled;
    }

    public void setDepthTestEnabled(boolean z) {
        this.depthTestEnabled = z;
    }

    public DepthFuncType getDepthFunc() {
        return this.depthFunc;
    }

    public void setDepthFunc(DepthFuncType depthFuncType) {
        this.depthFunc = depthFuncType;
    }

    public float getDepthRangeMin() {
        return this.depthRangeMin;
    }

    public void setDepthRangeMin(float f) {
        this.depthRangeMin = f;
    }

    public float getDepthRangeMax() {
        return this.depthRangeMax;
    }

    public void setDepthRangeMax(float f) {
        this.depthRangeMax = f;
    }

    public boolean isGlLightingEnabled() {
        return this.glLightingEnabled;
    }

    public void setGlLightingEnabled(boolean z) {
        this.glLightingEnabled = z;
        if (z) {
            this.normalArrayEnabled = true;
        }
    }

    public boolean isFogEnabled() {
        return this.fogEnabled;
    }

    public void setFogEnabled(boolean z) {
        this.fogEnabled = z;
    }

    public boolean isLineMode() {
        return this.lineMode;
    }

    public void setLineMode(boolean z) {
        this.lineMode = z;
    }

    public float getLineWidth() {
        return this.lineWidth;
    }

    public void setLineWidth(float f) {
        this.lineWidth = f;
    }

    public boolean isTangentArrayEnabled() {
        return this.tangentArrayEnabled;
    }

    public void setTangentArrayEnabled(boolean z) {
        this.tangentArrayEnabled = z;
    }

    public int getTangentChannel() {
        return this.tangentChannel;
    }

    public void setTangentChannel(int i) {
        this.tangentChannel = i;
    }

    public boolean isNormalArrayEnabled() {
        return this.normalArrayEnabled;
    }

    public void setNormalArrayEnabled(boolean z) {
        this.normalArrayEnabled = z || this.glLightingEnabled;
    }

    public boolean isColorArrayEnabled() {
        return this.colorArrayEnabled;
    }

    public void setColorArrayEnabled(boolean z) {
        this.colorArrayEnabled = z;
    }

    public List<ChannelInfo> getChannels() {
        return this.channels;
    }

    public void fillPrimitiveStates(PrimitiveDrawingState primitiveDrawingState) {
        primitiveDrawingState.useColorArray = this.colorArrayEnabled;
        primitiveDrawingState.useNormalArray = this.normalArrayEnabled;
        primitiveDrawingState.useTangentArray = this.tangentArrayEnabled;
        primitiveDrawingState.tangentChannel = this.tangentChannel;
        primitiveDrawingState.setStates(this.channels);
    }

    public void updateGLStates(DrawContext drawContext, RenderStates renderStates) {
        boolean z;
        GL gl = drawContext.getGl();
        if (renderStates.lineMode) {
            if (!this.lineMode) {
                gl.glPolygonMode(1032, 6914);
                gl.glDisable(10754);
                gl.glPolygonOffset(0.0f, 0.0f);
            }
        } else if (this.lineMode) {
            gl.glPolygonMode(1032, 6913);
            gl.glEnable(10754);
            gl.glPolygonOffset(-0.1f, 0.0f);
            gl.glLineWidth(this.lineWidth);
        }
        if (renderStates.alphaTestEnabled) {
            if (!this.alphaTestEnabled) {
                gl.glDisable(3008);
            } else if (!(renderStates.alphaFunc == this.alphaFunc && renderStates.alphaRef == this.alphaRef)) {
                gl.glAlphaFunc(this.alphaFunc.glEquivalent(), this.alphaRef);
            }
        } else if (this.alphaTestEnabled) {
            gl.glEnable(3008);
            gl.glAlphaFunc(this.alphaFunc.glEquivalent(), this.alphaRef);
        }
        if (renderStates.blendEnabled) {
            if (!this.blendEnabled) {
                gl.glDisable(3042);
            } else if (!(renderStates.dstBlend == this.dstBlend && renderStates.srcBlend == this.srcBlend)) {
                gl.glBlendFunc(this.srcBlend.glEquivalent(), this.dstBlend.glEquivalent());
            }
        } else if (this.blendEnabled) {
            gl.glEnable(3042);
            gl.glBlendFunc(this.srcBlend.glEquivalent(), this.dstBlend.glEquivalent());
        }
        if (renderStates.alphaMask) {
            if (!this.alphaMask) {
                z = true;
            }
            z = false;
        } else {
            if (this.alphaMask) {
                z = true;
            }
            z = false;
        }
        if (renderStates.colorMask) {
            if (!this.colorMask) {
                gl.glColorMask(false, false, false, this.alphaMask);
            } else if (z) {
                gl.glColorMask(true, true, true, this.alphaMask);
            }
        } else if (this.colorMask) {
            gl.glColorMask(true, true, true, this.alphaMask);
        } else if (z) {
            gl.glColorMask(false, false, false, this.alphaMask);
        }
        if (renderStates.cullFaceEnabled) {
            if (!this.cullFaceEnabled) {
                gl.glDisable(2884);
            } else if (renderStates.cullFace != this.cullFace) {
                gl.glCullFace(this.cullFace.glEquivalent());
            }
        } else if (this.cullFaceEnabled) {
            gl.glEnable(2884);
            gl.glCullFace(this.cullFace.glEquivalent());
        }
        if (renderStates.depthMask) {
            if (!this.depthMask) {
                gl.glDepthMask(false);
            }
        } else if (this.depthMask) {
            gl.glDepthMask(true);
        }
        if (renderStates.depthTestEnabled) {
            if (!this.depthTestEnabled) {
                gl.glDisable(2929);
            } else if (renderStates.depthFunc != this.depthFunc) {
                gl.glDepthFunc(this.depthFunc.glEquivalent());
            }
        } else if (this.depthTestEnabled) {
            gl.glEnable(2929);
            gl.glDepthFunc(this.depthFunc.glEquivalent());
        }
        gl.glDepthRange((double) this.depthRangeMin, (double) this.depthRangeMax);
        if (renderStates.normalArrayEnabled) {
            if (!this.normalArrayEnabled) {
                gl.glDisableClientState(32885);
            }
        } else if (this.normalArrayEnabled) {
            gl.glEnableClientState(32885);
        }
        if (renderStates.colorArrayEnabled) {
            if (!this.colorArrayEnabled) {
                gl.glDisableClientState(32886);
            }
        } else if (this.colorArrayEnabled) {
            gl.glEnableClientState(32886);
        }
        if (renderStates.tangentArrayEnabled) {
            if (!this.tangentArrayEnabled) {
                gl.glDisableVertexAttribArrayARB(renderStates.tangentChannel);
            } else if (this.tangentChannel != renderStates.tangentChannel) {
                gl.glDisableVertexAttribArrayARB(renderStates.tangentChannel);
                gl.glEnableVertexAttribArrayARB(this.tangentChannel);
            }
        } else if (this.tangentArrayEnabled) {
            gl.glEnableVertexAttribArrayARB(this.tangentChannel);
        }
        if (this.glLightingEnabled) {
            gl.glEnable(2896);
            gl.glEnable(2903);
            gl.glEnable(2977);
        } else {
            gl.glDisable(2896);
            gl.glDisable(2903);
            gl.glDisable(2977);
        }
        gl.glMatrixMode(5890);
        for (int i = 0; i < renderStates.numTextureUsed(); i++) {
            gl.glActiveTexture(renderStates.channelMapping(i) + 33984);
            gl.glClientActiveTexture(renderStates.channelMapping(i) + 33984);
            disableAutoTexCoords(gl, renderStates.getChannel(i));
            gl.glDisableClientState(32888);
            gl.glTexEnvi(8960, 8704, 8448);
            gl.glDisable(3553);
            gl.glDisable(34067);
            gl.glLoadIdentity();
        }
        for (int i2 = 0; i2 < numTextureUsed(); i2++) {
            gl.glActiveTexture(channelMapping(i2) + 33984);
            gl.glClientActiveTexture(channelMapping(i2) + 33984);
            ChannelInfo channelInfo = this.channels.get(i2);
            setupAutomaticTexGen(drawContext, gl, channelInfo);
            if (!channelInfo.isAutoCoord() && getChannel(i2).getTexCoord() != -1) {
                gl.glEnableClientState(32888);
            }
            gl.glTexEnvi(8960, 8704, texEnvMode(i2));
        }
        gl.glMatrixMode(5888);
        gl.glActiveTexture(33984);
        gl.glClientActiveTexture(33984);
        if (this.fogEnabled) {
            gl.glEnable(2912);
        } else {
            gl.glDisable(2912);
        }
    }

    private void disableAutoTexCoords(GL gl, ChannelInfo channelInfo) {
        if (channelInfo.isSTexCoordGeneration()) {
            gl.glDisable(3168);
        }
        if (channelInfo.isTTexCoordGeneration()) {
            gl.glDisable(3169);
        }
        if (channelInfo.isRTexCoordGeneration()) {
            gl.glDisable(3170);
        }
    }

    private void setupAutomaticTexGen(DrawContext drawContext, GL gl, ChannelInfo channelInfo) {
        if (channelInfo.isSTexCoordGeneration()) {
            gl.glEnable(3168);
            gl.glTexGeni(DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY, 9472, 9217);
            gl.glTexGenfv(DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY, 9473, channelInfo.getSEquation().fillBuffer(drawContext.floatBuffer60Temp0));
        }
        if (channelInfo.isTTexCoordGeneration()) {
            gl.glEnable(3169);
            gl.glTexGeni(8193, 9472, 9217);
            gl.glTexGenfv(8193, 9473, channelInfo.getTEquation().fillBuffer(drawContext.floatBuffer60Temp0));
        }
        if (channelInfo.isRTexCoordGeneration()) {
            gl.glEnable(3170);
            gl.glTexGeni(8194, 9472, 9217);
            gl.glTexGenfv(8194, 9473, channelInfo.getREquation().fillBuffer(drawContext.floatBuffer60Temp0));
        }
    }

    private int texEnvMode(int i) {
        return getChannel(i).getTexEnvMode().glEquivalent();
    }

    private int channelMapping(int i) {
        if (i >= this.channels.size()) {
            return 0;
        }
        return getChannel(i).getChannelMapping();
    }

    public void blindlyUpdateGLStates(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (this.lineMode) {
            gl.glPolygonMode(1032, 6913);
            gl.glEnable(10754);
            gl.glPolygonOffset(-0.1f, 0.0f);
            gl.glLineWidth(this.lineWidth);
        } else {
            gl.glPolygonMode(1032, 6914);
            gl.glDisable(10754);
            gl.glPolygonOffset(0.0f, 0.0f);
            gl.glLineWidth(1.0f);
        }
        if (this.alphaTestEnabled) {
            gl.glEnable(3008);
            gl.glAlphaFunc(this.alphaFunc.glEquivalent(), this.alphaRef);
        } else {
            gl.glDisable(3008);
        }
        if (this.blendEnabled) {
            gl.glEnable(3042);
            gl.glBlendFunc(this.srcBlend.glEquivalent(), this.dstBlend.glEquivalent());
        } else {
            gl.glDisable(3042);
        }
        if (this.colorMask) {
            gl.glColorMask(true, true, true, true);
        } else {
            gl.glColorMask(false, false, false, false);
        }
        if (this.cullFaceEnabled) {
            gl.glEnable(2884);
            gl.glCullFace(this.cullFace.glEquivalent());
        } else {
            gl.glDisable(2884);
        }
        gl.glDepthMask(this.depthMask);
        if (this.depthTestEnabled) {
            gl.glEnable(2929);
            gl.glDepthFunc(this.depthFunc.glEquivalent());
        } else {
            gl.glDisable(2929);
        }
        gl.glDepthRange((double) this.depthRangeMin, (double) this.depthRangeMax);
        if (this.normalArrayEnabled) {
            gl.glEnableClientState(32885);
        } else {
            gl.glDisableClientState(32885);
        }
        if (this.colorArrayEnabled) {
            gl.glEnableClientState(32886);
        } else {
            gl.glDisableClientState(32886);
        }
        if (this.tangentArrayEnabled) {
            gl.glEnableVertexAttribArrayARB(this.tangentChannel);
        } else {
            gl.glDisableVertexAttribArrayARB(this.tangentChannel);
        }
        if (this.glLightingEnabled) {
            gl.glEnable(2896);
            gl.glEnable(2903);
            gl.glEnable(2977);
        } else {
            gl.glDisable(2896);
            gl.glDisable(2903);
            gl.glDisable(2977);
        }
        gl.glMatrixMode(5890);
        for (int i = 0; i < GLSupportedCaps.getMaxTexChannels(); i++) {
            gl.glActiveTexture(33984 + i);
            gl.glClientActiveTexture(33984 + i);
            gl.glDisableClientState(32888);
            gl.glTexEnvi(8960, 8704, 8448);
            gl.glDisable(3553);
            gl.glDisable(34067);
            gl.glDisable(3168);
            gl.glDisable(3169);
            gl.glDisable(3170);
            gl.glLoadIdentity();
        }
        gl.glMatrixMode(5888);
        for (int i2 = 0; i2 < numTextureUsed(); i2++) {
            gl.glActiveTexture(channelMapping(i2) + 33984);
            gl.glClientActiveTexture(channelMapping(i2) + 33984);
            gl.glEnableClientState(32888);
            gl.glTexEnvi(8960, 8704, texEnvMode(i2));
            setupAutomaticTexGen(drawContext, gl, this.channels.get(i2));
        }
        for (int i3 = 0; i3 < numTextureUsed(); i3++) {
            gl.glActiveTexture(channelMapping(i3) + 33984);
            gl.glEnable(3553);
        }
        gl.glActiveTexture(33984);
        gl.glClientActiveTexture(33984);
        if (this.fogEnabled) {
            gl.glEnable(2912);
        } else {
            gl.glDisable(2912);
        }
    }

    public void fillMaterialStates(MaterialDrawingStates materialDrawingStates) {
        materialDrawingStates.numTextures = numTextureUsed();
        materialDrawingStates.setStates(this.channels);
    }

    public ChannelInfo getChannel(int i) {
        return this.channels.get(i);
    }

    public void setChannel(int i, ChannelInfo channelInfo) {
        if (i == -1) {
            i = this.channels.size();
        }
        while (this.channels.size() <= i) {
            this.channels.add(new ChannelInfo());
        }
        this.channels.get(i).assign(channelInfo);
    }

    /* access modifiers changed from: protected */
    public int numTextureUsed() {
        return this.channels.size();
    }

    public void updateDepthStates(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (this.depthMask) {
            gl.glDepthMask(true);
        } else {
            gl.glDepthMask(false);
        }
        if (this.depthTestEnabled) {
            gl.glEnable(2929);
            gl.glDepthFunc(this.depthFunc.glEquivalent());
        } else {
            gl.glDisable(2929);
        }
        gl.glDepthRange((double) this.depthRangeMin, (double) this.depthRangeMax);
    }

    public boolean isAlphaMask() {
        return this.alphaMask;
    }

    public void setAlphaMask(boolean z) {
        this.alphaMask = z;
    }
}
