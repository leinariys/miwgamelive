package taikodom.render.shaders;

import com.sun.opengl.util.BufferUtil;
import logic.thred.LogPrinter;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

import javax.media.opengl.GL;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

/* compiled from: a */
public class ShaderProgramObject extends RenderAsset {
    private static LogPrinter logger = LogPrinter.m10275K(ShaderProgramObject.class);
    public String name;
    private int objectId;
    private int shaderType;
    private String sourceCode;

    public ShaderProgramObject() {
        this.objectId = -1;
        this.shaderType = 0;
    }

    public ShaderProgramObject(int i, String str) {
        this.shaderType = i;
        this.sourceCode = str;
        this.objectId = -1;
    }

    static boolean checkLogInfo(GL gl, int i) {
        IntBuffer newIntBuffer = BufferUtil.newIntBuffer(1);
        gl.glGetObjectParameterivARB(i, 35716, newIntBuffer);
        int i2 = newIntBuffer.get();
        if (i2 <= 1) {
            return true;
        }
        ByteBuffer newByteBuffer = BufferUtil.newByteBuffer(i2);
        newIntBuffer.flip();
        gl.glGetInfoLogARB(i, i2, newIntBuffer, newByteBuffer);
        newByteBuffer.get(new byte[i2]);
        gl.glGetObjectParameterivARB(i, 35713, newIntBuffer);
        if (newIntBuffer.get() > 1) {
            return false;
        }
        return true;
    }

    public int getShaderType() {
        return this.shaderType;
    }

    public void setShaderType(int i) {
        this.shaderType = i;
    }

    public int getObjectId() {
        return this.objectId;
    }

    public String getSourceCode() {
        return this.sourceCode;
    }

    public void setSourceCode(String str) {
        this.sourceCode = str;
    }

    /* access modifiers changed from: package-private */
    public boolean compile(DrawContext drawContext) {
        if (this.shaderType == 0 || this.sourceCode == null || this.sourceCode.length() == 0) {
            return false;
        }
        if (drawContext.debugDraw()) {
            drawContext.logCompile(this);
        }
        GL gl = drawContext.getGl();
        int glCreateShaderObjectARB = gl.glCreateShaderObjectARB(this.shaderType);
        if (glCreateShaderObjectARB == 0) {
            return false;
        }
        gl.glShaderSourceARB(glCreateShaderObjectARB, 1, new String[]{this.sourceCode}, (int[]) null, 0);
        gl.glCompileShaderARB(glCreateShaderObjectARB);
        if (!checkLogInfo(gl, glCreateShaderObjectARB)) {
            return false;
        }
        this.objectId = glCreateShaderObjectARB;
        return true;
    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
