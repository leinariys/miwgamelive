package taikodom.render.shaders.parameters;

import taikodom.render.DrawContext;
import taikodom.render.NotExported;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public abstract class ShaderParameter extends RenderAsset {
    public int locationId = -1;
    private String parameterName = "unnamed";

    public ShaderParameter() {
    }

    public ShaderParameter(ShaderParameter shaderParameter) {
        super(shaderParameter);
        this.locationId = shaderParameter.locationId;
        this.name = shaderParameter.name;
        this.parameterName = shaderParameter.parameterName;
    }

    public abstract boolean bind(DrawContext drawContext);

    /* access modifiers changed from: protected */
    public abstract void doAssign(ShaderParameter shaderParameter);

    public String getParameterName() {
        return this.parameterName;
    }

    public void setParameterName(String str) {
        this.parameterName = str;
    }

    public void assignValue(ShaderParameter shaderParameter) {
        if (shaderParameter.locationId != -1) {
            this.locationId = shaderParameter.locationId;
        }
        doAssign(shaderParameter);
    }

    public String toString() {
        return String.valueOf(this.locationId) + ":" + this.parameterName;
    }

    @NotExported
    public int getLocationId() {
        return this.locationId;
    }

    @NotExported
    public void setLocationId(int i) {
        this.locationId = i;
    }
}
