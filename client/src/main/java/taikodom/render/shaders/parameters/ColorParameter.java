package taikodom.render.shaders.parameters;

import com.hoplon.geometry.Color;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class ColorParameter extends ShaderParameter {
    private final Color value = new Color(0.0f, 0.0f, 0.0f, 0.0f);

    public ColorParameter() {
    }

    public ColorParameter(ColorParameter colorParameter) {
        super(colorParameter);
        this.value.set(colorParameter.value);
    }

    public RenderAsset cloneAsset() {
        return new ColorParameter(this);
    }

    public Color getValue() {
        return this.value;
    }

    public void setValue(Color color) {
        this.value.set(color);
    }

    public void setValue(float f, float f2, float f3, float f4) {
        this.value.set(f, f2, f3, f4);
    }

    public boolean bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.locationId == -1) {
            return false;
        }
        drawContext.getGl().glUniform4f(this.locationId, this.value.x, this.value.y, this.value.z, this.value.w);
        return true;
    }

    /* access modifiers changed from: protected */
    public void doAssign(ShaderParameter shaderParameter) {
        setValue(((ColorParameter) shaderParameter).getValue());
    }
}
