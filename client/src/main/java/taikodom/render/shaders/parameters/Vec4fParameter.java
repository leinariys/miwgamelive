package taikodom.render.shaders.parameters;

import game.geometry.Vector4fWrap;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class Vec4fParameter extends ShaderParameter {
    private final Vector4fWrap value = new Vector4fWrap();

    public Vec4fParameter() {
    }

    public Vec4fParameter(Vec4fParameter vec4fParameter) {
        super(vec4fParameter);
        this.value.set(vec4fParameter.value);
    }

    public RenderAsset cloneAsset() {
        return new Vec4fParameter(this);
    }

    public Vector4fWrap getValue() {
        return this.value;
    }

    public void setValue(Vector4fWrap ajf) {
        this.value.set(ajf);
    }

    public void setValue(float f, float f2, float f3, float f4) {
        this.value.set(f, f2, f3, f4);
    }

    public boolean bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.locationId == -1) {
            return false;
        }
        drawContext.getGl().glUniform4f(this.locationId, this.value.x, this.value.y, this.value.z, this.value.w);
        return true;
    }

    /* access modifiers changed from: protected */
    public void doAssign(ShaderParameter shaderParameter) {
        setValue(((Vec4fParameter) shaderParameter).getValue());
    }
}
