package taikodom.render.shaders.parameters;

import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class IntParameter extends ShaderParameter {
    public int value;

    public IntParameter() {
    }

    public IntParameter(IntParameter intParameter) {
        super(intParameter);
        this.value = intParameter.value;
    }

    public RenderAsset cloneAsset() {
        return new IntParameter(this);
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int i) {
        this.value = i;
    }

    public boolean bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.locationId == -1) {
            return false;
        }
        drawContext.getGl().glUniform1i(this.locationId, this.value);
        return true;
    }

    /* access modifiers changed from: protected */
    public void doAssign(ShaderParameter shaderParameter) {
        setValue(((IntParameter) shaderParameter).getValue());
    }
}
