package taikodom.render.shaders;

import taikodom.render.DrawContext;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.CullFace;
import taikodom.render.enums.DepthFuncType;
import taikodom.render.enums.LightType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.ChannelInfo;

/* compiled from: a */
public class ShaderPass extends RenderAsset {
    public boolean compiled;
    public ShaderProgram program;
    public RenderStates renderStates = new RenderStates();
    public boolean useDiffuseCubemap;
    public LightType useLights = LightType.NONE;
    public boolean useReflectCubemap;
    public boolean useSceneAmbientColor;
    public boolean validated;

    public void releaseReferences() {
        super.releaseReferences();
        this.program = null;
    }

    public ShaderProgram getProgram() {
        return this.program;
    }

    public void setProgram(ShaderProgram shaderProgram) {
        this.program = shaderProgram;
    }

    public LightType getUseLights() {
        return this.useLights;
    }

    public void setUseLights(LightType lightType) {
        this.useLights = lightType;
    }

    public boolean getUseFixedFunctionLighting() {
        return this.renderStates.glLightingEnabled;
    }

    public void setUseFixedFunctionLighting(boolean z) {
        this.renderStates.setGlLightingEnabled(z);
    }

    public boolean getUseSceneAmbientColor() {
        return this.useSceneAmbientColor;
    }

    public void setUseSceneAmbientColor(boolean z) {
        this.useSceneAmbientColor = z;
    }

    public boolean getUseReflectCubemap() {
        return this.useReflectCubemap;
    }

    public void setUseReflectCubemap(boolean z) {
        this.useReflectCubemap = z;
    }

    public boolean getUseDiffuseCubemap() {
        return this.useDiffuseCubemap;
    }

    public void setUseDiffuseCubemap(boolean z) {
        this.useDiffuseCubemap = z;
    }

    public RenderStates getRenderStates() {
        return this.renderStates;
    }

    public boolean isCompiled() {
        return this.compiled;
    }

    public boolean isValidated() {
        return this.validated;
    }

    public void updateStates(DrawContext drawContext, RenderStates renderStates2) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.program != null) {
            this.program.bind(drawContext);
        }
        this.renderStates.updateGLStates(drawContext, renderStates2);
        drawContext.incNumShadersBind();
    }

    public void unbind(DrawContext drawContext) {
        if (this.program != null) {
            if (drawContext.debugDraw()) {
                drawContext.logUnbind(this);
            }
            this.program.unbind(drawContext);
        }
    }

    public void updateShaderProgramAttribs(DrawContext drawContext) {
        if (this.program != null) {
            this.program.updateShaderProgramAttribs(drawContext);
        }
    }

    public void updateMaterialProgramAttribs(DrawContext drawContext) {
        if (this.program != null) {
            this.program.updateMaterialProgramAttribs(drawContext);
        }
    }

    public void updatePrimitiveProgramAttribs(DrawContext drawContext) {
        if (this.program != null) {
            this.program.updatePrimitiveProgramAttribs(drawContext);
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public boolean isAlphaTestEnabled() {
        return this.renderStates.isAlphaTestEnabled();
    }

    public void setAlphaTestEnabled(boolean z) {
        this.renderStates.setAlphaTestEnabled(z);
    }

    public boolean isBlendEnabled() {
        return this.renderStates.isBlendEnabled();
    }

    public void setBlendEnabled(boolean z) {
        this.renderStates.setBlendEnabled(z);
    }

    public boolean isColorArrayEnabled() {
        return this.renderStates.isColorArrayEnabled();
    }

    public void setColorArrayEnabled(boolean z) {
        this.renderStates.setColorArrayEnabled(z);
    }

    public boolean isColorMask() {
        return this.renderStates.isColorMask();
    }

    public void setColorMask(boolean z) {
        this.renderStates.setColorMask(z);
    }

    public boolean isAlphaMask() {
        return this.renderStates.isColorMask();
    }

    public void setAlphaMask(boolean z) {
        this.renderStates.setAlphaMask(z);
    }

    public boolean isCullFaceEnabled() {
        return this.renderStates.isCullFaceEnabled();
    }

    public void setCullFaceEnabled(boolean z) {
        this.renderStates.setCullFaceEnabled(z);
    }

    public boolean isDepthMask() {
        return this.renderStates.isDepthMask();
    }

    public void setDepthMask(boolean z) {
        this.renderStates.setDepthMask(z);
    }

    public boolean isDepthTestEnabled() {
        return this.renderStates.isDepthTestEnabled();
    }

    public void setDepthTestEnabled(boolean z) {
        this.renderStates.setDepthTestEnabled(z);
    }

    public boolean isFogEnabled() {
        return this.renderStates.isFogEnabled();
    }

    public void setFogEnabled(boolean z) {
        this.renderStates.setFogEnabled(z);
    }

    public boolean isGlLightingEnabled() {
        return this.renderStates.isGlLightingEnabled();
    }

    public void setGlLightingEnabled(boolean z) {
        this.renderStates.setGlLightingEnabled(z);
    }

    public boolean isLineMode() {
        return this.renderStates.isLineMode();
    }

    public void setLineMode(boolean z) {
        this.renderStates.setLineMode(z);
    }

    public boolean isNormalArrayEnabled() {
        return this.renderStates.isNormalArrayEnabled();
    }

    public void setNormalArrayEnabled(boolean z) {
        this.renderStates.setNormalArrayEnabled(z);
    }

    public boolean isTangentArrayEnabled() {
        return this.renderStates.isTangentArrayEnabled();
    }

    public void setTangentArrayEnabled(boolean z) {
        this.renderStates.setTangentArrayEnabled(z);
    }

    public float getAlphaRef() {
        return this.renderStates.getAlphaRef();
    }

    public void setAlphaRef(float f) {
        this.renderStates.setAlphaRef(f);
    }

    public float getDepthRangeMax() {
        return this.renderStates.getDepthRangeMax();
    }

    public void setDepthRangeMax(float f) {
        this.renderStates.setDepthRangeMax(f);
    }

    public float getDepthRangeMin() {
        return this.renderStates.getDepthRangeMin();
    }

    public void setDepthRangeMin(float f) {
        this.renderStates.setDepthRangeMin(f);
    }

    public float getLineWidth() {
        return this.renderStates.getLineWidth();
    }

    public void setLineWidth(float f) {
        this.renderStates.setLineWidth(f);
    }

    public DepthFuncType getAlphaFunc() {
        return this.renderStates.getAlphaFunc();
    }

    public void setAlphaFunc(DepthFuncType depthFuncType) {
        this.renderStates.setAlphaFunc(depthFuncType);
    }

    public CullFace getCullFace() {
        return this.renderStates.getCullFace();
    }

    public void setCullFace(CullFace cullFace) {
        this.renderStates.setCullFace(cullFace);
    }

    public DepthFuncType getDepthFunc() {
        return this.renderStates.getDepthFunc();
    }

    public void setDepthFunc(DepthFuncType depthFuncType) {
        this.renderStates.setDepthFunc(depthFuncType);
    }

    public BlendType getDstBlend() {
        return this.renderStates.getDstBlend();
    }

    public void setDstBlend(BlendType blendType) {
        this.renderStates.setDstBlend(blendType);
    }

    public BlendType getSrcBlend() {
        return this.renderStates.getSrcBlend();
    }

    public void setSrcBlend(BlendType blendType) {
        this.renderStates.setSrcBlend(blendType);
    }

    public int getTangentChannel() {
        return this.renderStates.getTangentChannel();
    }

    public void setTangentChannel(int i) {
        this.renderStates.setTangentChannel(i);
    }

    public ChannelInfo getChannelTextureSet(int i) {
        return this.renderStates.getChannel(i);
    }

    public void setChannelTextureSet(int i, ChannelInfo channelInfo) {
        this.renderStates.setChannel(i, channelInfo);
    }

    public int channelsCount() {
        return this.renderStates.getChannels().size();
    }

    public boolean compile(DrawContext drawContext) {
        if (this.compiled) {
            return this.validated;
        }
        this.compiled = true;
        if (this.program != null && !this.program.compile(this, drawContext)) {
            return false;
        }
        this.validated = true;
        return true;
    }

    public void clearTexChannels() {
        this.renderStates.channels.clear();
        this.compiled = false;
        this.validated = false;
    }
}
