package taikodom.render.shaders;

import com.sun.opengl.util.BufferUtil;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.DrawContext;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;
import taikodom.render.shaders.parameters.*;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
@Slf4j
public class ShaderProgram extends RenderAsset {
    public String name;
    private boolean compiled;
    private boolean hasError;
    private List<ShaderParameter> manualParams = new ArrayList();
    private ArrayList<ShaderParameter> materialAutoParams = new ArrayList<>();
    private List<ShaderProgramObject> objects = new ArrayList();
    private ArrayList<ShaderParameter> primitiveAutoParams = new ArrayList<>();
    private int programId;
    private List<ShaderParameter> shaderAutoParams = new ArrayList();
    private int tangentChannel;
    private boolean useDiffuseCubemap;
    private boolean useNormalArray;
    private boolean useReflectCubemap;
    private boolean useTangentArray;

    static boolean checkLogInfo(GL gl, int i) {
        IntBuffer newIntBuffer = BufferUtil.newIntBuffer(1);
        gl.glGetObjectParameterivARB(i, 35714, newIntBuffer);
        int i2 = newIntBuffer.get();
        if (i2 <= 1) {
            return true;
        }
        ByteBuffer newByteBuffer = BufferUtil.newByteBuffer(i2);
        newIntBuffer.flip();
        gl.glGetInfoLogARB(i, i2, newIntBuffer, newByteBuffer);
        newByteBuffer.get(new byte[i2]);
        return false;
    }

    public void addProgramObject(ShaderProgramObject shaderProgramObject) {
        this.objects.add(shaderProgramObject);
    }

    public boolean compile(ShaderPass shaderPass, DrawContext drawContext) {
        boolean z;
        if (!GLSupportedCaps.isGlsl()) {
            this.compiled = true;
            this.hasError = true;
        }
        if (this.compiled) {
            if (shaderPass != null) {
                shaderPass.setUseDiffuseCubemap(this.useDiffuseCubemap);
                shaderPass.setUseReflectCubemap(this.useReflectCubemap);
                shaderPass.setNormalArrayEnabled(this.useNormalArray);
                shaderPass.setTangentArrayEnabled(this.useTangentArray);
                shaderPass.setTangentChannel(this.tangentChannel);
            }
            if (this.hasError) {
                z = false;
            } else {
                z = true;
            }
            return z;
        }
        if (drawContext.debugDraw()) {
            drawContext.logCompile(this);
        }
        this.compiled = true;
        this.hasError = true;
        if (this.objects.size() == 0) {
            return false;
        }
        GL gl = drawContext.getGl();
        this.programId = gl.glCreateProgramObjectARB();
        if (this.programId == 0) {
            return false;
        }
        for (ShaderProgramObject next : this.objects) {
            if (!next.compile(drawContext)) {
                return false;
            }
            gl.glAttachObjectARB(this.programId, next.getObjectId());
        }
        gl.glLinkProgram(this.programId);
        gl.glValidateProgramARB(this.programId);
        if (!checkLogInfo(gl, this.programId)) {
            return false;
        }
        findParameters(shaderPass, drawContext);
        this.compiled = true;
        this.hasError = false;
        return true;
    }

    public void findParameters(ShaderPass shaderPass, DrawContext drawContext) {
        ShaderParameter matrix4fParameter;
        if (shaderPass != null) {
            GL gl = drawContext.getGl();
            int[] iArr = new int[3];
            byte[] bArr = new byte[520];
            gl.glGetObjectParameterivARB(this.programId, 35718, iArr, 0);
            int i = iArr[0];
            for (int i2 = 0; i2 < i; i2++) {
                gl.glGetActiveUniform(this.programId, i2, bArr.length, iArr, 0, iArr, 1, iArr, 2, bArr, 0);
                int i3 = iArr[0];
                int i4 = iArr[2];
                try {
                    String str = new String(bArr, 0, i3, "utf-8");
                    GlobalShaderParameter globalShaderParameter = drawContext.sceneConfig.getGlobalShaderParameter(str);
                    if (globalShaderParameter != null) {
                        C5192a aVar = new C5192a(globalShaderParameter);
                        aVar.setLocationId(gl.glGetUniformLocation(this.programId, str));
                        aVar.setName(str);
                        aVar.setParameterName(str);
                        if (globalShaderParameter.getLayer() == 0) {
                            this.shaderAutoParams.add(aVar);
                        } else if (globalShaderParameter.getLayer() == 1) {
                            this.materialAutoParams.add(aVar);
                        } else if (globalShaderParameter.getLayer() == 2) {
                            this.primitiveAutoParams.add(aVar);
                        }
                        if ("reflectCubemap".equals(str)) {
                            this.useReflectCubemap = true;
                            shaderPass.setUseReflectCubemap(true);
                        } else if ("diffuseCubemap".equals(str)) {
                            this.useDiffuseCubemap = true;
                            shaderPass.setUseDiffuseCubemap(true);
                        }
                    } else if (!str.startsWith("gl")) {
                        switch (i4) {
                            case 5124:
                                matrix4fParameter = new IntParameter();
                                break;
                            case 5126:
                                matrix4fParameter = new FloatParameter();
                                break;
                            case 35664:
                                matrix4fParameter = new Vec2fParameter();
                                break;
                            case 35665:
                                matrix4fParameter = new Vec3fParameter();
                                break;
                            case 35666:
                                matrix4fParameter = new ColorParameter();
                                break;
                            case 35676:
                                matrix4fParameter = new Matrix4fParameter();
                                break;
                            default:
                                matrix4fParameter = null;
                                break;
                        }
                        if (matrix4fParameter != null) {
                            matrix4fParameter.setLocationId(gl.glGetUniformLocation(this.programId, str));
                            matrix4fParameter.setName(str);
                            matrix4fParameter.setParameterName(str);
                            this.manualParams.add(matrix4fParameter);
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            gl.glGetObjectParameterivARB(this.programId, 35721, iArr, 0);
            int i5 = iArr[0];
            for (int i6 = 0; i6 < i5; i6++) {
                gl.glGetActiveAttrib(this.programId, i6, bArr.length, iArr, 0, iArr, 1, iArr, 2, bArr, 0);
                try {
                    String str2 = new String(bArr, 0, iArr[0], "utf-8");
                    GlobalShaderParameter globalShaderParameter2 = drawContext.sceneConfig.getGlobalShaderParameter(str2);
                    if (globalShaderParameter2 != null) {
                        C5193b bVar = new C5193b(globalShaderParameter2);
                        bVar.setLocationId(gl.glGetAttribLocation(this.programId, str2));
                        bVar.setName(str2);
                        bVar.setParameterName(str2);
                        if (globalShaderParameter2.getLayer() == 0) {
                            this.shaderAutoParams.add(bVar);
                        } else if (globalShaderParameter2.getLayer() == 1) {
                            this.materialAutoParams.add(bVar);
                        } else if (globalShaderParameter2.getLayer() == 2) {
                            this.primitiveAutoParams.add(bVar);
                        }
                    }
                    if ("tangent".equals(str2)) {
                        this.tangentChannel = gl.glGetAttribLocation(this.programId, str2);
                        this.useTangentArray = true;
                        if (shaderPass != null) {
                            shaderPass.getRenderStates().setTangentArrayEnabled(true);
                            shaderPass.getRenderStates().setTangentChannel(this.tangentChannel);
                        }
                    } else if ("gl_Normal".equals(str2)) {
                        this.useNormalArray = true;
                        if (shaderPass != null) {
                            shaderPass.getRenderStates().setNormalArrayEnabled(true);
                        }
                    }
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                }
            }
            unbind(drawContext);
        }
    }

    public void unbind(DrawContext drawContext) {
        drawContext.getGl().glUseProgramObjectARB(0);
    }

    public void bind(DrawContext drawContext) {
        compile(drawContext.currentPass, drawContext);
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (!this.hasError) {
            drawContext.getGl().glUseProgramObjectARB(this.programId);
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    @Deprecated
    public ShaderProgramObject getShaderProgramObject(int i) {
        return this.objects.get(i);
    }

    @Deprecated
    public void setShaderProgramObject(int i, ShaderProgramObject shaderProgramObject) {
        this.objects.add(shaderProgramObject);
    }

    public int getManualParamsCount() {
        return this.manualParams.size();
    }

    public ShaderParameter getManualParameter(int i) {
        return this.manualParams.get(i);
    }

    public void updateShaderProgramAttribs(DrawContext drawContext) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.shaderAutoParams.size()) {
                this.shaderAutoParams.get(i2).bind(drawContext);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void updateMaterialProgramAttribs(DrawContext drawContext) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.materialAutoParams.size()) {
                this.materialAutoParams.get(i2).bind(drawContext);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void updatePrimitiveProgramAttribs(DrawContext drawContext) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.primitiveAutoParams.size()) {
                this.primitiveAutoParams.get(i2).bind(drawContext);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public int shaderProgramObjectCount() {
        return this.objects.size();
    }

    public void releaseReferences() {
        super.releaseReferences();
    }

    public void clear() {
        this.objects.clear();
        GLU.getCurrentGL().glDeleteObjectARB(this.programId);
        this.programId = 0;
        this.useTangentArray = false;
        this.tangentChannel = 0;
        this.compiled = false;
        this.hasError = false;
    }

    /* renamed from: taikodom.render.shaders.ShaderProgram$a */
    class C5192a extends ShaderParameter {
        private final /* synthetic */ GlobalShaderParameter bwD;

        C5192a(GlobalShaderParameter globalShaderParameter) {
            this.bwD = globalShaderParameter;
        }

        public boolean bind(DrawContext drawContext) {
            if (drawContext.debugDraw()) {
                drawContext.logBind(this);
            }
            this.bwD.bind(this.locationId, drawContext);
            return true;
        }

        public RenderAsset cloneAsset() {
            return this;
        }

        /* access modifiers changed from: protected */
        public void doAssign(ShaderParameter shaderParameter) {
        }
    }

    /* renamed from: taikodom.render.shaders.ShaderProgram$b */
    /* compiled from: a */
    class C5193b extends ShaderParameter {
        private final /* synthetic */ GlobalShaderParameter bwD;

        C5193b(GlobalShaderParameter globalShaderParameter) {
            this.bwD = globalShaderParameter;
        }

        public boolean bind(DrawContext drawContext) {
            if (drawContext.debugDraw()) {
                drawContext.logBind(this);
            }
            this.bwD.bind(this.locationId, drawContext);
            return true;
        }

        public RenderAsset cloneAsset() {
            return this;
        }

        /* access modifiers changed from: protected */
        public void doAssign(ShaderParameter shaderParameter) {
        }
    }
}
