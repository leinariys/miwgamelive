package taikodom.render.postProcessing;

import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.SceneView;
import taikodom.render.enums.BlendType;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.TreeRecordList;
import taikodom.render.shaders.*;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.vecmath.Matrix3f;
import java.util.Collections;

/* compiled from: a */
public class GlowFX extends PostProcessingFX {
    private static final int RT_RESOLUTION = 256;
    public float glowIntensity = 1.0f;
    private Shader defaultShader;
    private RenderStates defaultStates = new RenderStates();
    private float feedback = 0.75f;
    private ShaderProgram horProgram;
    private boolean initialized = false;
    private boolean isSupported = false;
    private int nFilterPasses = 4;
    private RenderTarget renderTarget;
    private RenderTarget tempTarget;
    private float texCoord = 1.0f;
    private boolean useFBO = false;
    private ShaderProgram verProgram;

    public void setSceneIntensity(float f) {
        this.glowIntensity = f;
    }

    public boolean init(RenderContext renderContext) {
        if (!GLSupportedCaps.isGlsl()) {
            this.initialized = true;
            return false;
        }
        this.useFBO = GLSupportedCaps.isFbo();
        if (this.useFBO) {
            initAsFBO(renderContext);
        } else {
            initAsPbuffer(renderContext);
        }
        this.defaultShader = new Shader();
        this.defaultShader.setName("Glow shader");
        ShaderPass shaderPass = new ShaderPass();
        shaderPass.setName("Glow shader pass");
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setTexChannel(10);
        shaderPass.setChannelTextureSet(0, channelInfo);
        this.defaultShader.addPass(shaderPass);
        createShaderPrograms(renderContext);
        this.initialized = true;
        return true;
    }

    private void initAsFBO(RenderContext renderContext) {
        this.renderTarget = new RenderTarget();
        this.renderTarget.create(256, 256, true, renderContext.getDc(), false);
        this.tempTarget = new RenderTarget();
        this.tempTarget.create(256, 256, true, renderContext.getDc(), false);
        this.texCoord = 256.0f;
    }

    private void initAsPbuffer(RenderContext renderContext) {
        this.renderTarget = new RenderTarget();
        this.renderTarget.create(256, 256, true, renderContext.getDc(), true);
    }

    private void createShaderPrograms(RenderContext renderContext) {
        String str;
        String str2;
        ShaderProgramObject shaderProgramObject = new ShaderProgramObject(35633, "void main(void){gl_Position = ftransform();gl_TexCoord[0] = gl_MultiTexCoord0;}");
        float f = 0.005859375f;
        if (this.useFBO) {
            f = 1.0f;
        }
        if (this.useFBO) {
            str = "uniform sampler2DRect tex0; float texScaler = " + f + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];" + "void main(void) " + "{ vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(0.0, -3.9, 0.0,  weight);gaussFilter[1] = vec4(0.0, -2.6, 0.0,  weight); gaussFilter[2] = vec4(0.0, -1.3, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4(0.0,  1.3, 0.0, weight); gaussFilter[5] = vec4(0.0, 2.6, 0.0, weight); gaussFilter[6] = vec4(0.0, 3.9, 0.0,  weight);  int i; " + "for (i=0;i<7;i++) " + "{ " + "color += texture2DRect(tex0,vec2(gl_TexCoord[0].x + gaussFilter[i].x * texScaler , " + "gl_TexCoord[0].y + gaussFilter[i].y * texScaler )) *  gaussFilter[i].w; " + "}  gl_FragColor = color; }";
            str2 = "uniform sampler2DRect tex0; float texScaler = " + f + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];" + "void main(void) " + "{ vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(-3.9, 0.0, 0.0,  weight);gaussFilter[1] = vec4(-2.6, 0.0, 0.0,  weight); gaussFilter[2] = vec4(-1.3, 0.0, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4( 1.3, 0.0, 0.0, weight); gaussFilter[5] = vec4(2.6, 0.0, 0.0, weight); gaussFilter[6] = vec4(3.9, 0.0, 0.0,  weight);  int i; " + "for (i=0;i<7;i++) " + "{ " + "color += texture2DRect(tex0,vec2(gl_TexCoord[0].x + gaussFilter[i].x * texScaler , " + "gl_TexCoord[0].y + gaussFilter[i].y * texScaler )) *  gaussFilter[i].w; " + "}  gl_FragColor = color; }";
        } else {
            str = "uniform sampler2D tex0; float texScaler = " + f + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];void main(void) { vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(0.0, -3.9, 0.0,  weight);gaussFilter[1] = vec4(0.0, -2.6, 0.0,  weight); gaussFilter[2] = vec4(0.0, -1.3, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4(0.0,  1.3, 0.0, weight); gaussFilter[5] = vec4(0.0, 2.6, 0.0, weight); gaussFilter[6] = vec4(0.0, 3.9, 0.0,  weight);  int i; for (i=0;i<7;i++) { color += texture2D(tex0,vec2(gl_TexCoord[0].x + gaussFilter[i].x * texScaler , gl_TexCoord[0].y + gaussFilter[i].y * texScaler )) *  gaussFilter[i].w; }  gl_FragColor = color; }";
            str2 = "uniform sampler2D tex0; float texScaler = " + f + "; " + "float weight = 1.0/7.0; vec4 gaussFilter[7];void main(void) { vec4 color = vec4(0.0,0.0,0.0,0.0);  gaussFilter[0] = vec4(-3.9, 0.0, 0.0,  weight);gaussFilter[1] = vec4(-2.6, 0.0, 0.0,  weight); gaussFilter[2] = vec4(-1.3, 0.0, 0.0, weight); gaussFilter[3] = vec4( 0.0, 0.0, 0.0, weight); gaussFilter[4] = vec4( 1.3, 0.0, 0.0, weight); gaussFilter[5] = vec4(2.6, 0.0, 0.0, weight); gaussFilter[6] = vec4(3.9, 0.0, 0.0,  weight);  int i; for (i=0;i<7;i++) { color += texture2D(tex0,vec2(gl_TexCoord[0].x + gaussFilter[i].x * texScaler , gl_TexCoord[0].y + gaussFilter[i].y * texScaler )) *  gaussFilter[i].w; }  gl_FragColor = color; }";
        }
        ShaderProgramObject shaderProgramObject2 = new ShaderProgramObject(35632, str);
        ShaderProgramObject shaderProgramObject3 = new ShaderProgramObject(35632, str2);
        this.verProgram = new ShaderProgram();
        this.verProgram.addProgramObject(shaderProgramObject);
        this.verProgram.addProgramObject(shaderProgramObject2);
        this.horProgram = new ShaderProgram();
        this.horProgram.addProgramObject(shaderProgramObject);
        this.horProgram.addProgramObject(shaderProgramObject3);
        if (!this.verProgram.compile((ShaderPass) null, renderContext.getDc())) {
            this.verProgram = null;
            this.horProgram = null;
        } else if (!this.horProgram.compile((ShaderPass) null, renderContext.getDc())) {
            this.verProgram = null;
            this.horProgram = null;
        } else {
            this.isSupported = true;
        }
    }

    public void preRender(RenderContext renderContext, SceneView sceneView) {
        if (!this.initialized && !init(renderContext)) {
            return;
        }
        if (!this.isSupported) {
            this.enabled = false;
            return;
        }
        DrawContext dc = renderContext.getDc();
        GL gl = dc.getGl();
        gl.glMatrixMode(5889);
        gl.glPushMatrix();
        gl.glMatrixMode(5888);
        gl.glPushMatrix();
        this.renderTarget.bind(renderContext.getDc());
        renderEmissiveMaterial(renderContext);
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        gl.glOrtho(ScriptRuntime.NaN, 256.0d, ScriptRuntime.NaN, 256.0d, -1.0d, 1.0d);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        gl.glEnable(3042);
        gl.glBlendFunc(1, 1);
        gl.glDisable(2929);
        if (this.useFBO) {
            this.tempTarget.bindTexture(dc);
            renderFeedback(dc, gl);
            this.renderTarget.unbind(dc);
            gl.glDisable(3042);
            for (int i = 0; i < this.nFilterPasses; i++) {
                this.renderTarget.bindTexture(dc);
                this.tempTarget.bind(dc);
                verticalBlur(renderContext);
                this.tempTarget.unbind(dc);
                this.tempTarget.bindTexture(dc);
                this.renderTarget.bind(dc);
                horizontalBlur(renderContext);
                this.renderTarget.unbind(dc);
            }
        } else {
            this.renderTarget.bindTexture(dc);
            renderFeedback(dc, gl);
            this.renderTarget.copyBufferToTexture(dc);
            gl.glDisable(3042);
            for (int i2 = 0; i2 < this.nFilterPasses; i2++) {
                verticalBlur(renderContext);
                this.renderTarget.copyBufferToTexture(dc);
                horizontalBlur(renderContext);
                this.renderTarget.copyBufferToTexture(dc);
            }
            this.renderTarget.unbind(dc);
        }
        gl.glEnable(2929);
        gl.glDisable(this.renderTarget.getTexture().getTarget());
        gl.glDisable(3042);
        gl.glMatrixMode(5889);
        gl.glPopMatrix();
        gl.glMatrixMode(5888);
        gl.glPopMatrix();
    }

    private void renderFeedback(DrawContext drawContext, GL gl) {
        gl.glColor3f(this.feedback, this.feedback, this.feedback);
        drawRTQuad(drawContext);
    }

    private void renderEmissiveMaterial(RenderContext renderContext) {
        DrawContext dc = renderContext.getDc();
        GL gl = dc.getGl();
        gl.glViewport(0, 0, 256, 256);
        gl.glEnable(3089);
        gl.glScissor(0, 0, 256, 256);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClear(17664);
        gl.glDisable(3089);
        gl.glColor4f(this.glowIntensity, this.glowIntensity, this.glowIntensity, this.glowIntensity);
        renderContext.getCamera().updateGLProjection(dc);
        renderContext.getDc().gpuCamTransform.mo13985a((Matrix3f) renderContext.getCamera().getGlobalTransform().orientation);
        dc.gpuCamTransform.mo14049p(0.0f, 0.0f, 0.0f);
        renderContext.getDc().gpuCamAffine.mo14043k(dc.gpuCamTransform);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        dc.tempBuffer0.clear();
        renderContext.getDc().gpuCamAffine.asColumnMajorBuffer(dc.tempBuffer0);
        dc.tempBuffer0.flip();
        gl.glMultMatrixf(dc.tempBuffer0);
        gl.glEnableClientState(32884);
        dc.primitiveState.setStates(Collections.EMPTY_LIST);
        TreeRecordList records = renderContext.getRecords();
        dc.primitiveState.setUseColorArray(false);
        dc.primitiveState.setUseNormalArray(false);
        dc.primitiveState.setUseTangentArray(false);
        dc.currentPass = this.defaultShader.getPass(0);
        RenderStates renderStates = this.defaultShader.getPass(0).getRenderStates();
        renderStates.fillPrimitiveStates(dc.primitiveState);
        renderStates.fillMaterialStates(dc.materialState);
        renderStates.blindlyUpdateGLStates(dc);
        Primitive primitive = null;
        Material material = null;
        for (int i = 0; i < records.size(); i++) {
            TreeRecord treeRecord = records.get(i);
            RenderStates renderStates2 = treeRecord.shader.getPass(0).getRenderStates();
            if (!renderStates2.isBlendEnabled() || renderStates2.getDstBlend() == BlendType.ONE || renderStates2.getDstBlend() == BlendType.ZERO) {
                if (renderStates2.isAlphaTestEnabled() != renderStates.isAlphaTestEnabled()) {
                    if (renderStates2.isAlphaTestEnabled()) {
                        gl.glEnable(3008);
                        gl.glAlphaFunc(renderStates2.getAlphaFunc().glEquivalent(), renderStates2.getAlphaRef());
                        renderStates = renderStates2;
                    } else {
                        gl.glDisable(3008);
                        renderStates = renderStates2;
                    }
                }
                if (renderStates2.isBlendEnabled()) {
                    gl.glEnable(3042);
                    gl.glBlendFunc(renderStates2.getSrcBlend().glEquivalent(), renderStates2.getDstBlend().glEquivalent());
                } else {
                    gl.glDisable(3042);
                }
                if (primitive == null || primitive.getHandle() != treeRecord.primitive.getHandle()) {
                    if (primitive != null && primitive.getUseVBO() && !treeRecord.primitive.getUseVBO()) {
                        unbindPrimitive(dc);
                    }
                    primitive = treeRecord.primitive;
                    primitive.bind(dc);
                }
                if (material == null || material.getHandle() != treeRecord.material.getHandle()) {
                    gl.glMatrixMode(5890);
                    treeRecord.material.bind(dc);
                    gl.glMatrixMode(5888);
                    material = treeRecord.material;
                }
                gl.glPushMatrix();
                renderStates2.updateDepthStates(dc);
                dc.tempBuffer0.clear();
                treeRecord.gpuTransform.asColumnMajorBuffer(dc.tempBuffer0);
                dc.tempBuffer0.flip();
                gl.glMultMatrixf(dc.tempBuffer0);
                primitive.draw(dc);
                gl.glPopMatrix();
            }
        }
        gl.glDisableClientState(32884);
        unbindPrimitive(dc);
        this.defaultStates.blindlyUpdateGLStates(dc);
    }

    private void unbindPrimitive(DrawContext drawContext) {
        drawContext.getGl().glBindBufferARB(34962, 0);
        drawContext.getGl().glBindBufferARB(34963, 0);
    }

    public void render(RenderContext renderContext, SceneView sceneView) {
        if (this.isSupported) {
            DrawContext dc = renderContext.getDc();
            GL gl = dc.getGl();
            gl.glColor3f(1.0f, 1.0f, 1.0f);
            gl.glPushMatrix();
            gl.glLoadIdentity();
            gl.glMatrixMode(5889);
            gl.glPushMatrix();
            gl.glLoadIdentity();
            gl.glOrtho(ScriptRuntime.NaN, (double) dc.getScreenWidth(), ScriptRuntime.NaN, (double) dc.getScreenHeight(), -1.0d, 1.0d);
            this.renderTarget.bindTexture(dc);
            gl.glBlendFunc(1, 1);
            gl.glDisable(2929);
            gl.glDepthMask(false);
            gl.glEnable(3042);
            gl.glBegin(7);
            gl.glTexCoord2f(0.0f, 0.0f);
            gl.glVertex2f((float) dc.getScreenOffsetX(), (float) dc.getScreenOffsetY());
            gl.glTexCoord2f(this.texCoord, 0.0f);
            gl.glVertex2f((float) (dc.getScreenOffsetX() + dc.getScreenWidth()), (float) dc.getScreenOffsetY());
            gl.glTexCoord2f(this.texCoord, this.texCoord);
            gl.glVertex2f((float) (dc.getScreenOffsetX() + dc.getScreenWidth()), (float) (dc.getScreenOffsetY() + dc.getScreenHeight()));
            gl.glTexCoord2f(0.0f, this.texCoord);
            gl.glVertex2f((float) dc.getScreenOffsetX(), (float) (dc.getScreenHeight() + dc.getScreenOffsetY()));
            gl.glEnd();
            gl.glDisable(this.renderTarget.getTexture().getTarget());
            gl.glDisable(3042);
            gl.glEnable(2929);
            gl.glDepthMask(true);
            gl.glPopMatrix();
            gl.glMatrixMode(5888);
            gl.glPopMatrix();
            gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            if (this.useFBO) {
                RenderTarget renderTarget2 = this.tempTarget;
                this.tempTarget = this.renderTarget;
                this.renderTarget = renderTarget2;
            }
        }
    }

    private void drawRTQuad(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        gl.glBegin(7);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(0.0f, 0.0f);
        gl.glTexCoord2f(this.texCoord, 0.0f);
        gl.glVertex2f(256.0f, 0.0f);
        gl.glTexCoord2f(this.texCoord, this.texCoord);
        gl.glVertex2f(256.0f, 256.0f);
        gl.glTexCoord2f(0.0f, this.texCoord);
        gl.glVertex2f(0.0f, 256.0f);
        gl.glEnd();
    }

    private void verticalBlur(RenderContext renderContext) {
        DrawContext dc = renderContext.getDc();
        this.verProgram.bind(dc);
        this.verProgram.updateMaterialProgramAttribs(dc);
        drawRTQuad(dc);
        this.verProgram.unbind(dc);
    }

    private void horizontalBlur(RenderContext renderContext) {
        DrawContext dc = renderContext.getDc();
        this.horProgram.bind(dc);
        this.horProgram.updateMaterialProgramAttribs(dc);
        drawRTQuad(dc);
        this.horProgram.unbind(dc);
    }

    public void releaseReferences() {
        if (this.renderTarget != null) {
            this.renderTarget.releaseReferences();
        }
        if (this.defaultShader != null) {
            this.defaultShader.releaseReferences();
        }
        if (this.verProgram != null) {
            this.verProgram.releaseReferences();
        }
        if (this.horProgram != null) {
            this.horProgram.releaseReferences();
        }
        this.renderTarget = null;
        this.horProgram = null;
        this.verProgram = null;
        this.defaultShader = null;
    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
