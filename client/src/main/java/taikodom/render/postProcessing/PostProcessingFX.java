package taikodom.render.postProcessing;

import taikodom.render.RenderContext;
import taikodom.render.SceneView;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public abstract class PostProcessingFX extends RenderAsset {
    public boolean enabled = true;

    public abstract boolean init(RenderContext renderContext);

    public abstract void preRender(RenderContext renderContext, SceneView sceneView);

    public abstract void render(RenderContext renderContext, SceneView sceneView);

    public void setSceneIntensity(float f) {
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean z) {
        this.enabled = z;
    }
}
