package taikodom.render;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.io.PrintWriter;
import java.text.AttributedCharacterIterator;
import java.util.Map;

/* compiled from: a */
public class DebugGraphics extends Graphics2D {
    private static int nextId = 0;
    private Graphics2D graphics;

    /* renamed from: id */
    private int f10329id;
    private boolean logging;
    private DebugGraphics parent = null;

    /* renamed from: pw */
    private PrintWriter f10330pw;

    public DebugGraphics(Graphics2D graphics2D) {
        int i = nextId + 1;
        nextId = i;
        this.f10329id = i;
        this.graphics = graphics2D;
        this.f10330pw = new PrintWriter(System.out, true);
        this.logging = true;
    }

    public void addRenderingHints(Map<?, ?> map) {
        if (isLogging()) {
            log("addRenderingHints", map);
        }
        this.graphics.addRenderingHints(map);
    }

    private void printId() {
        if (this.parent != null) {
            this.parent.printId();
        }
        this.f10330pw.print(this.f10329id);
        this.f10330pw.print('.');
    }

    private void log(String str, Object... objArr) {
        printId();
        this.f10330pw.print(str);
        this.f10330pw.print("(");
        if (objArr != null && objArr.length > 0) {
            for (int i = 0; i < objArr.length; i++) {
                this.f10330pw.print(objArr[i]);
                if (i + 1 < objArr.length) {
                    this.f10330pw.print(", ");
                }
            }
        }
        this.f10330pw.println(");");
        this.f10330pw.flush();
    }

    private boolean isLogging() {
        return this.logging;
    }

    public void setLogging(boolean z) {
        this.logging = z;
    }

    public void clip(Shape shape) {
        if (isLogging()) {
            log("clip", shape);
        }
        this.graphics.clip(shape);
    }

    public void draw(Shape shape) {
        if (isLogging()) {
            log("draw", shape);
        }
        this.graphics.draw(shape);
    }

    public void drawGlyphVector(GlyphVector glyphVector, float f, float f2) {
        if (isLogging()) {
            log("drawGlyphVector", glyphVector, Float.valueOf(f), Float.valueOf(f2));
        }
        this.graphics.drawGlyphVector(glyphVector, f, f2);
    }

    public boolean drawImage(Image image, AffineTransform affineTransform, ImageObserver imageObserver) {
        if (isLogging()) {
            log("drawImage", image, affineTransform, imageObserver);
        }
        boolean drawImage = this.graphics.drawImage(image, affineTransform, imageObserver);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(drawImage));
        }
        return drawImage;
    }

    public void drawImage(BufferedImage bufferedImage, BufferedImageOp bufferedImageOp, int i, int i2) {
        if (isLogging()) {
            log("drawImage", bufferedImage, bufferedImageOp, Integer.valueOf(i), Integer.valueOf(i2));
        }
        this.graphics.drawImage(bufferedImage, bufferedImageOp, i, i2);
    }

    public void drawRenderableImage(RenderableImage renderableImage, AffineTransform affineTransform) {
        if (isLogging()) {
            log("drawRenderableImage", renderableImage, affineTransform);
        }
        this.graphics.drawRenderableImage(renderableImage, affineTransform);
    }

    public void drawRenderedImage(RenderedImage renderedImage, AffineTransform affineTransform) {
        if (isLogging()) {
            log("drawRenderedImage", renderedImage, affineTransform);
        }
        this.graphics.drawRenderedImage(renderedImage, affineTransform);
    }

    public void drawString(String str, int i, int i2) {
        if (isLogging()) {
            log("drawString", str, Integer.valueOf(i), Integer.valueOf(i2));
        }
        this.graphics.drawString(str, i, i2);
    }

    public void drawString(String str, float f, float f2) {
        if (isLogging()) {
            log("drawString", str, Float.valueOf(f), Float.valueOf(f2));
        }
        this.graphics.drawString(str, f, f2);
    }

    public void drawString(AttributedCharacterIterator attributedCharacterIterator, int i, int i2) {
        if (isLogging()) {
            log("drawString", attributedCharacterIterator, Integer.valueOf(i), Integer.valueOf(i2));
        }
        this.graphics.drawString(attributedCharacterIterator, i, i2);
    }

    public void drawString(AttributedCharacterIterator attributedCharacterIterator, float f, float f2) {
        if (isLogging()) {
            log("drawString", attributedCharacterIterator, Float.valueOf(f), Float.valueOf(f2));
        }
        this.graphics.drawString(attributedCharacterIterator, f, f2);
    }

    public void fill(Shape shape) {
        if (isLogging()) {
            log("fill", shape);
        }
        this.graphics.fill(shape);
    }

    public Color getBackground() {
        if (isLogging()) {
            log("getBackground", new Object[0]);
        }
        Color background = this.graphics.getBackground();
        if (isLogging()) {
            log("  return ", background);
        }
        return background;
    }

    public void setBackground(Color color) {
        if (isLogging()) {
            log("setBackground", color);
        }
        this.graphics.setBackground(color);
    }

    public Composite getComposite() {
        if (isLogging()) {
            log("getComposite", new Object[0]);
        }
        Composite composite = this.graphics.getComposite();
        if (isLogging()) {
            log("  return ", composite);
        }
        return composite;
    }

    public void setComposite(Composite composite) {
        if (isLogging()) {
            log("setComposite", composite);
        }
        this.graphics.setComposite(composite);
    }

    public GraphicsConfiguration getDeviceConfiguration() {
        if (isLogging()) {
            log("getDeviceConfiguration", new Object[0]);
        }
        GraphicsConfiguration deviceConfiguration = this.graphics.getDeviceConfiguration();
        if (isLogging()) {
            log("  return ", deviceConfiguration);
        }
        return deviceConfiguration;
    }

    public FontRenderContext getFontRenderContext() {
        if (isLogging()) {
            log("getFontRenderContext", new Object[0]);
        }
        FontRenderContext fontRenderContext = this.graphics.getFontRenderContext();
        if (isLogging()) {
            log("  return ", fontRenderContext);
        }
        return fontRenderContext;
    }

    public Paint getPaint() {
        if (isLogging()) {
            log("getPaint", new Object[0]);
        }
        Paint paint = this.graphics.getPaint();
        if (isLogging()) {
            log("  return ", paint);
        }
        return paint;
    }

    public void setPaint(Paint paint) {
        if (isLogging()) {
            log("setPaint", paint);
        }
        this.graphics.setPaint(paint);
    }

    public Object getRenderingHint(RenderingHints.Key key) {
        if (isLogging()) {
            log("getRenderingHint", key);
        }
        Object renderingHint = this.graphics.getRenderingHint(key);
        if (isLogging()) {
            log("  return ", renderingHint);
        }
        return renderingHint;
    }

    public RenderingHints getRenderingHints() {
        if (isLogging()) {
            log("getRenderingHints", new Object[0]);
        }
        RenderingHints renderingHints = this.graphics.getRenderingHints();
        if (isLogging()) {
            log("  return ", renderingHints);
        }
        return renderingHints;
    }

    public void setRenderingHints(Map<?, ?> map) {
        if (isLogging()) {
            log("setRenderingHints", map);
        }
        this.graphics.setRenderingHints(map);
    }

    public Stroke getStroke() {
        if (isLogging()) {
            log("getStroke", new Object[0]);
        }
        Stroke stroke = this.graphics.getStroke();
        if (isLogging()) {
            log("  return ", stroke);
        }
        return stroke;
    }

    public void setStroke(Stroke stroke) {
        if (isLogging()) {
            log("setStroke", stroke);
        }
        this.graphics.setStroke(stroke);
    }

    public AffineTransform getTransform() {
        if (isLogging()) {
            log("getTransform", new Object[0]);
        }
        AffineTransform transform = this.graphics.getTransform();
        if (isLogging()) {
            log("  return ", transform);
        }
        return transform;
    }

    public void setTransform(AffineTransform affineTransform) {
        if (isLogging()) {
            log("setTransform", affineTransform);
        }
        this.graphics.setTransform(affineTransform);
    }

    public boolean hit(Rectangle rectangle, Shape shape, boolean z) {
        if (isLogging()) {
            log("hit", rectangle, shape, Boolean.valueOf(z));
        }
        boolean hit = this.graphics.hit(rectangle, shape, z);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(hit));
        }
        return hit;
    }

    public void rotate(double d) {
        if (isLogging()) {
            log("rotate", Double.valueOf(d));
        }
        this.graphics.rotate(d);
    }

    public void rotate(double d, double d2, double d3) {
        if (isLogging()) {
            log("rotate", Double.valueOf(d), Double.valueOf(d2), Double.valueOf(d3));
        }
        this.graphics.rotate(d, d2, d3);
    }

    public void scale(double d, double d2) {
        if (isLogging()) {
            log("scale", Double.valueOf(d), Double.valueOf(d2));
        }
        this.graphics.scale(d, d2);
    }

    public void setRenderingHint(RenderingHints.Key key, Object obj) {
        if (isLogging()) {
            log("setRenderingHint", key, obj);
        }
        this.graphics.setRenderingHint(key, obj);
    }

    public void shear(double d, double d2) {
        if (isLogging()) {
            log("shear", Double.valueOf(d), Double.valueOf(d2));
        }
        this.graphics.shear(d, d2);
    }

    public void transform(AffineTransform affineTransform) {
        if (isLogging()) {
            log("transform", affineTransform);
        }
        this.graphics.transform(affineTransform);
    }

    public void translate(int i, int i2) {
        if (isLogging()) {
            log("translate", Integer.valueOf(i), Integer.valueOf(i2));
        }
        this.graphics.translate(i, i2);
    }

    public void translate(double d, double d2) {
        if (isLogging()) {
            log("translate", Double.valueOf(d), Double.valueOf(d2));
        }
        this.graphics.translate(d, d2);
    }

    public void clearRect(int i, int i2, int i3, int i4) {
        if (isLogging()) {
            log("clearRect", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
        }
        this.graphics.clearRect(i, i2, i3, i4);
    }

    public void clipRect(int i, int i2, int i3, int i4) {
        if (isLogging()) {
            log("clipRect", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
        }
        this.graphics.clipRect(i, i2, i3, i4);
    }

    public void copyArea(int i, int i2, int i3, int i4, int i5, int i6) {
        if (isLogging()) {
            log("copyArea", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6));
        }
        this.graphics.copyArea(i, i2, i3, i4, i5, i6);
    }

    public Graphics create() {
        if (isLogging()) {
            log("create", new Object[0]);
        }
        Graphics2D create = (Graphics2D) this.graphics.create();
        if (isLogging()) {
            log("  return ", create);
        }
        DebugGraphics debugGraphics = new DebugGraphics(create);
        debugGraphics.parent = this;
        debugGraphics.setLogging(this.logging);
        debugGraphics.setPrintWriter(this.f10330pw);
        return debugGraphics;
    }

    public void dispose() {
        if (isLogging()) {
            log("dispose", new Object[0]);
        }
        this.graphics.dispose();
    }

    public void drawArc(int i, int i2, int i3, int i4, int i5, int i6) {
        if (isLogging()) {
            log("drawArc", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6));
        }
        this.graphics.drawArc(i, i2, i3, i4, i5, i6);
    }

    public boolean drawImage(Image image, int i, int i2, ImageObserver imageObserver) {
        if (isLogging()) {
            log("drawImage", image, Integer.valueOf(i), Integer.valueOf(i2), imageObserver);
        }
        boolean drawImage = this.graphics.drawImage(image, i, i2, imageObserver);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(drawImage));
        }
        return drawImage;
    }

    public boolean drawImage(Image image, int i, int i2, Color color, ImageObserver imageObserver) {
        if (isLogging()) {
            log("drawImage", image, Integer.valueOf(i), Integer.valueOf(i2), color, imageObserver);
        }
        boolean drawImage = this.graphics.drawImage(image, i, i2, color, imageObserver);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(drawImage));
        }
        return drawImage;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, ImageObserver imageObserver) {
        if (isLogging()) {
            log("drawImage", image, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), imageObserver);
        }
        boolean drawImage = this.graphics.drawImage(image, i, i2, i3, i4, imageObserver);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(drawImage));
        }
        return drawImage;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, Color color, ImageObserver imageObserver) {
        if (isLogging()) {
            log("drawImage", image, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), color, imageObserver);
        }
        boolean drawImage = this.graphics.drawImage(image, i, i2, i3, i4, color, imageObserver);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(drawImage));
        }
        return drawImage;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, ImageObserver imageObserver) {
        if (isLogging()) {
            log("drawImage", image, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6), Integer.valueOf(i7), Integer.valueOf(i8), imageObserver);
        }
        boolean drawImage = this.graphics.drawImage(image, i, i2, i3, i4, i5, i6, i7, i8, imageObserver);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(drawImage));
        }
        return drawImage;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Color color, ImageObserver imageObserver) {
        if (isLogging()) {
            log("drawImage", image, Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6), Integer.valueOf(i7), Integer.valueOf(i8), color, imageObserver);
        }
        boolean drawImage = this.graphics.drawImage(image, i, i2, i3, i4, i5, i6, i7, i8, color, imageObserver);
        if (isLogging()) {
            log("  return ", Boolean.valueOf(drawImage));
        }
        return drawImage;
    }

    public void drawLine(int i, int i2, int i3, int i4) {
        if (isLogging()) {
            log("drawLine", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
        }
        this.graphics.drawLine(i, i2, i3, i4);
    }

    public void drawOval(int i, int i2, int i3, int i4) {
        if (isLogging()) {
            log("drawOval", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
        }
        this.graphics.drawOval(i, i2, i3, i4);
    }

    public void drawPolygon(int[] iArr, int[] iArr2, int i) {
        if (isLogging()) {
            log("drawPolygon", iArr, iArr2, Integer.valueOf(i));
        }
        this.graphics.drawPolygon(iArr, iArr2, i);
    }

    public void drawPolyline(int[] iArr, int[] iArr2, int i) {
        if (isLogging()) {
            log("drawPolyline", iArr, iArr2, Integer.valueOf(i));
        }
        this.graphics.drawPolyline(iArr, iArr2, i);
    }

    public void drawRoundRect(int i, int i2, int i3, int i4, int i5, int i6) {
        if (isLogging()) {
            log("drawRoundRect", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6));
        }
        this.graphics.drawRoundRect(i, i2, i3, i4, i5, i6);
    }

    public void fillArc(int i, int i2, int i3, int i4, int i5, int i6) {
        if (isLogging()) {
            log("fillArc", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6));
        }
        this.graphics.fillArc(i, i2, i3, i4, i5, i6);
    }

    public void fillOval(int i, int i2, int i3, int i4) {
        if (isLogging()) {
            log("fillOval", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
        }
        this.graphics.fillOval(i, i2, i3, i4);
    }

    public void fillPolygon(int[] iArr, int[] iArr2, int i) {
        if (isLogging()) {
            log("fillPolygon", iArr, iArr2, Integer.valueOf(i));
        }
        this.graphics.fillPolygon(iArr, iArr2, i);
    }

    public void fillRect(int i, int i2, int i3, int i4) {
        if (isLogging()) {
            log("fillRect", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
        }
        this.graphics.fillRect(i, i2, i3, i4);
    }

    public void fillRoundRect(int i, int i2, int i3, int i4, int i5, int i6) {
        if (isLogging()) {
            log("fillRoundRect", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5), Integer.valueOf(i6));
        }
        this.graphics.fillRoundRect(i, i2, i3, i4, i5, i6);
    }

    public Shape getClip() {
        if (isLogging()) {
            log("getClip", new Object[0]);
        }
        Shape clip = this.graphics.getClip();
        if (isLogging()) {
            log("  return ", clip);
        }
        return clip;
    }

    public void setClip(Shape shape) {
        if (isLogging()) {
            log("setClip", shape);
        }
        this.graphics.setClip(shape);
    }

    public Rectangle getClipBounds() {
        if (isLogging()) {
            log("getClipBounds", new Object[0]);
        }
        Rectangle clipBounds = this.graphics.getClipBounds();
        if (isLogging()) {
            log("  return ", clipBounds);
        }
        return clipBounds;
    }

    public Color getColor() {
        if (isLogging()) {
            log("getColor", new Object[0]);
        }
        Color color = this.graphics.getColor();
        if (isLogging()) {
            log("  return ", color);
        }
        return color;
    }

    public void setColor(Color color) {
        if (isLogging()) {
            log("setColor", color);
        }
        this.graphics.setColor(color);
    }

    public Font getFont() {
        if (isLogging()) {
            log("getFont", new Object[0]);
        }
        Font font = this.graphics.getFont();
        if (isLogging()) {
            log("  return ", font);
        }
        return font;
    }

    public void setFont(Font font) {
        if (isLogging()) {
            log("setFont", font);
        }
        this.graphics.setFont(font);
    }

    public FontMetrics getFontMetrics(Font font) {
        if (isLogging()) {
            log("getFontMetrics", font);
        }
        FontMetrics fontMetrics = this.graphics.getFontMetrics(font);
        if (isLogging()) {
            log("  return ", fontMetrics);
        }
        return fontMetrics;
    }

    public void setClip(int i, int i2, int i3, int i4) {
        if (isLogging()) {
            log("setClip", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4));
        }
        this.graphics.setClip(i, i2, i3, i4);
    }

    public void setPaintMode() {
        if (isLogging()) {
            log("setPaintMode", new Object[0]);
        }
        this.graphics.setPaintMode();
    }

    public void setXORMode(Color color) {
        if (isLogging()) {
            log("setXORMode", color);
        }
        this.graphics.setXORMode(color);
    }

    private void setPrintWriter(PrintWriter printWriter) {
        this.f10330pw = printWriter;
    }
}
