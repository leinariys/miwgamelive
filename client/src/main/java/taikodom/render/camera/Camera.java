package taikodom.render.camera;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.DrawContext;
import taikodom.render.scene.SceneObject;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3d;
import java.nio.FloatBuffer;

/* compiled from: a */
public class Camera extends SceneObject {
    private final Matrix4fWrap projection = new Matrix4fWrap();
    public float fovY = 75.0f;
    private float aspect = 1.3333334f;
    private float bottom = -1.0f;
    private float farPlane = 400000.0f;
    private boolean isOrthogonal = false;
    private float left = -1.0f;
    private float nearPlane = 1.0f;
    private float right = 1.0f;
    private float tanHalfFovY = ((float) Math.tan((((double) this.fovY) * 3.141592653589793d) / 360.0d));
    private float top = 1.0f;

    public Camera cloneAsset() {
        return this;
    }

    public void setOrthoWindow(float f, float f2, float f3, float f4) {
        this.left = f;
        this.right = f2;
        this.bottom = f3;
        this.top = f4;
    }

    public Matrix4fWrap getProjection() {
        return this.projection;
    }

    public float getFarPlane() {
        return this.farPlane;
    }

    public void setFarPlane(float f) {
        this.farPlane = f;
    }

    public float getNearPlane() {
        return this.nearPlane;
    }

    public void setNearPlane(float f) {
        this.nearPlane = f;
    }

    public float getAspect() {
        return this.aspect;
    }

    public void setAspect(float f) {
        this.aspect = f;
    }

    public float getFovY() {
        return this.fovY;
    }

    public void setFovY(float f) {
        this.fovY = f;
        this.tanHalfFovY = (float) Math.tan((((double) f) * 3.141592653589793d) / 360.0d);
    }

    public FloatBuffer getRawData() {
        return null;
    }

    public FloatBuffer getRawOrientation() {
        return null;
    }

    public void calculateProjectionMatrix() {
        if (this.isOrthogonal) {
            this.projection.mo13983a(2.0f / (this.right - this.left), 0.0f, 0.0f, 0.0f, 0.0f, 2.0f / (this.top - this.bottom), 0.0f, 0.0f, 0.0f, 0.0f, 1.0f / (this.farPlane - this.nearPlane), 0.0f, (-(this.right + this.left)) / (this.right - this.left), (-(this.top + this.bottom)) / (this.top - this.bottom), (-this.nearPlane) / (this.farPlane + this.nearPlane), 1.0f);
        } else if (((double) getFovY()) != ScriptRuntime.NaN) {
            float f = this.nearPlane * this.tanHalfFovY;
            float f2 = -f;
            float f3 = f2 * this.aspect;
            float f4 = f * this.aspect;
            this.projection.mo13983a((2.0f * this.nearPlane) / (f4 - f3), 0.0f, 0.0f, 0.0f, 0.0f, (2.0f * this.nearPlane) / (f - f2), 0.0f, 0.0f, (f4 + f3) / (f4 - f3), (f + f2) / (f - f2), -((this.farPlane + this.nearPlane) / (this.farPlane - this.nearPlane)), -1.0f, 0.0f, 0.0f, -(((2.0f * this.farPlane) * this.nearPlane) / (this.farPlane - this.nearPlane)), 0.0f);
        }
    }

    public void updateGLProjection(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        if (!this.isOrthogonal) {
            drawContext.glu().gluPerspective((double) getFovY(), (double) getAspect(), (double) getNearPlane(), (double) getFarPlane());
        } else {
            drawContext.getGl().glOrtho((double) this.left, (double) this.right, (double) this.bottom, (double) this.top, (double) (-this.nearPlane), (double) this.farPlane);
        }
        gl.glMatrixMode(5888);
    }

    public boolean isOrthogonal() {
        return this.isOrthogonal;
    }

    public void setOrthogonal(boolean z) {
        this.isOrthogonal = z;
    }

    public float getTanHalfFovY() {
        return this.tanHalfFovY;
    }

    public void lookAt(Vec3d ajr) {
        Vec3f dfV = ajr.mo9517f((Tuple3d) getPosition()).dfX().dfV();
        Vec3f vec3f = new Vec3f(0.0f, 1.0f, 0.0f);
        Vec3f b = dfV.mo23476b(vec3f);
        b.dfP();
        vec3f.set(b.mo23476b(dfV));
        this.transform.setX(b);
        this.transform.setY(vec3f);
        this.transform.setZ(dfV.dfT());
    }
}
