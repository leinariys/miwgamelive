package taikodom.render.camera;

import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;

/* compiled from: a */
public class ViewCameraControl {
    public static final int FLAT_SHADER = 1;
    public static final int FRONT_VIEW = 2;
    public static final int LIGHT_SHADER = 0;
    public static final int PERSPECTIVE_VIEW = 0;
    public static final int SIDE_VIEW = 3;
    public static final int TOP_VIEW = 1;
    public static final int WIREFRAME_SHADER = 2;
    public Camera camera = new Camera();
    public float orthoZoom = 1.0f;
    private float camAngX = 0.0f;
    private float camAngY = 0.0f;
    private float camAngZ = 0.0f;
    private float distanceToPivot;
    private int shaderType = 1;
    private float speed = 1.0f;
    private int viewType = 0;
    private int windowHeigth = 1;
    private int windowWidth = 1;

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera camera2) {
        this.camera = camera2;
    }

    public void reset() {
        if (getViewType() == 0) {
            TransformWrap bcVar = new TransformWrap();
            this.camAngY = 45.0f;
            bcVar.mo17392z(45.0f);
            this.camAngX = -15.0f;
            this.camAngZ = 0.0f;
            bcVar.mo17391y(-15.0f);
            setCameraTransform(bcVar);
            this.camera.setPosition(40.0f, 20.0f, 40.0f);
        } else {
            this.camera.setPosition(0.0f, 0.0f, 0.0f);
            setOrthoMode(this.viewType);
        }
        this.orthoZoom = 1.0f;
        updateCameraAttribs();
    }

    /* access modifiers changed from: package-private */
    public float getOrthoZoom() {
        return this.orthoZoom;
    }

    /* access modifiers changed from: package-private */
    public int getViewType() {
        return this.viewType;
    }

    public void setPosition(Vec3d ajr) {
        this.camera.setPosition(ajr);
    }

    public void setWindowSize(int i, int i2) {
        this.windowWidth = i;
        this.windowHeigth = i2;
    }

    public void setOrthoMode(int i) {
        this.viewType = i;
        this.camera.setOrthogonal(true);
        Vec3d position = this.camera.getPosition();
        TransformWrap bcVar = new TransformWrap();
        if (this.viewType == 1) {
            bcVar.mo17391y(90.0f);
        } else if (this.viewType == 3) {
            bcVar.mo17392z(90.0f);
        }
        bcVar.setTranslation(position);
        setCameraTransform(bcVar);
        updateCameraAttribs();
    }

    public void updateCameraAttribs() {
        float f = (((float) this.windowWidth) * 1.0f) / (((float) this.windowHeigth) * 1.0f);
        this.camera.setOrthoWindow((-this.orthoZoom) * f, this.orthoZoom * f, -this.orthoZoom, this.orthoZoom);
        this.camera.setAspect(f);
    }

    public void setPerspectiveMode() {
        this.viewType = 0;
        this.camera.setOrthogonal(false);
    }

    public void zoomIn(float f) {
        if (f > 0.0f) {
            if (this.viewType == 0) {
                this.camera.setPosition(this.camera.getPosition().mo9539s((Tuple3f) this.camera.getTransform().getVectorZ().mo23510mS(this.speed * f).mo23510mS(f)));
                this.distanceToPivot -= this.speed * f;
                if (this.distanceToPivot < 1.0f) {
                    this.distanceToPivot = 1.0f;
                    return;
                }
                return;
            }
            this.orthoZoom /= (this.speed * f) + 1.0f;
            this.camera.setPosition(this.camera.getPosition().mo9539s((Tuple3f) this.camera.getTransform().getVectorZ().mo23510mS(f).mo23510mS(f)));
            updateCameraAttribs();
        }
    }

    public void zoomOut(float f) {
        if (f > 0.0f) {
            if (this.viewType == 0) {
                this.camera.setPosition(this.camera.getPosition().mo9531q(this.camera.getTransform().getVectorZ().mo23510mS(this.speed * f).mo23510mS(f)));
                return;
            }
            this.orthoZoom *= 1.0f + (this.speed * f);
            this.camera.setPosition(this.camera.getPosition().mo9531q(this.camera.getTransform().getVectorZ().mo23510mS(f).mo23510mS(f)));
            updateCameraAttribs();
        }
    }

    public void pan(int i, int i2) {
        if (this.viewType == 0) {
            Vec3f vectorX = this.camera.getTransform().getVectorX();
            Vec3f vectorY = this.camera.getTransform().getVectorY();
            Vec3f mS = vectorX.mo23510mS(((float) i) * this.speed * 0.1f);
            this.camera.setPosition(this.camera.getPosition().mo9531q(vectorY.mo23510mS(((float) i2) * this.speed * 0.1f).mo23506j(mS)));
            return;
        }
        Vec3f vectorX2 = this.camera.getTransform().getVectorX();
        Vec3f vectorY2 = this.camera.getTransform().getVectorY();
        Vec3f mS2 = vectorX2.mo23510mS(((float) i) * this.orthoZoom * 0.01f * this.speed);
        this.camera.setPosition(this.camera.getPosition().mo9531q(vectorY2.mo23510mS(((float) i2) * this.orthoZoom * 0.01f * this.speed).mo23506j(mS2)));
    }

    public void rotate(int i, int i2) {
        if (this.viewType == 0) {
            this.camAngX -= ((float) i2) * 0.2f;
            this.camAngY -= ((float) i) * 0.2f;
            TransformWrap bcVar = new TransformWrap();
            bcVar.mo17391y(this.camAngX);
            bcVar.mo17392z(this.camAngY);
            bcVar.mo17323A(this.camAngZ);
            bcVar.setTranslation(this.camera.getPosition());
            setCameraTransform(bcVar);
        }
    }

    private void setCameraTransform(TransformWrap bcVar) {
        double lengthSquared = bcVar.position.lengthSquared();
        float determinant3x3 = bcVar.determinant3x3();
        if (determinant3x3 == determinant3x3 && lengthSquared == lengthSquared && !Double.isInfinite(lengthSquared)) {
            this.camera.setTransform(bcVar);
            return;
        }
        System.out.println("Invalid camera transform: " + bcVar + " angles " + this.camAngX + ", " + this.camAngY + ", " + this.camAngZ);
        if (determinant3x3 != determinant3x3) {
            bcVar.orientation.setIdentity();
        }
        if (lengthSquared != lengthSquared || Double.isInfinite(lengthSquared)) {
            bcVar.setTranslation(this.camera.getPosition());
        }
        if (Float.isNaN(this.camAngX) || Float.isInfinite(this.camAngX)) {
            this.camAngX = 0.0f;
        }
        if (Float.isNaN(this.camAngY) || Float.isInfinite(this.camAngY)) {
            this.camAngY = 0.0f;
        }
        if (Float.isNaN(this.camAngZ) || Float.isInfinite(this.camAngZ)) {
            this.camAngZ = 0.0f;
        }
        System.out.println("Using " + bcVar);
        this.camera.setTransform(bcVar);
    }

    public float getSpeed() {
        return this.speed;
    }

    public void setSpeed(float f) {
        if (f > 0.0f) {
            this.speed = f;
        }
    }

    public int getWindowHeigth() {
        return this.windowHeigth;
    }

    public void setWindowHeigth(int i) {
        this.windowHeigth = i;
    }

    public int getWindowWidth() {
        return this.windowWidth;
    }

    public void setWindowWidth(int i) {
        this.windowWidth = i;
    }

    public Vec3f getWorldSpacePos(int i, int i2) {
        Vec3f vec3f = new Vec3f();
        float f = ((float) this.windowWidth) / ((float) this.windowHeigth);
        if (this.viewType != 0) {
            vec3f.x = ((f * this.orthoZoom) * ((float) ((i * 2) - this.windowWidth))) / ((float) this.windowWidth);
            vec3f.y = ((-this.orthoZoom) * ((float) ((i2 * 2) - this.windowHeigth))) / ((float) this.windowHeigth);
            this.camera.getTransform().multiply3x3(vec3f, vec3f);
            if (this.viewType == 1) {
                vec3f.y = 0.0f;
            } else if (this.viewType == 2) {
                vec3f.z = 0.0f;
            } else if (this.viewType == 3) {
                vec3f.x = 0.0f;
            }
        }
        return vec3f;
    }

    public int getShaderType() {
        return this.shaderType;
    }

    public void setShaderType(int i) {
        this.shaderType = i;
    }

    public void orbit(float f, float f2, Vec3f vec3f) {
        if (this.viewType == 0) {
            this.camAngX -= f2;
            this.camAngY -= f;
            Vec3f vec3f2 = new Vec3f(0.0f, 0.0f, 0.0f);
            vec3f2.x = this.distanceToPivot * ((float) Math.sin((((double) this.camAngY) * 3.141592653589793d) / 180.0d)) * ((float) Math.cos((((double) this.camAngX) * 3.141592653589793d) / 180.0d));
            vec3f2.z = this.distanceToPivot * ((float) Math.cos((((double) this.camAngY) * 3.141592653589793d) / 180.0d)) * ((float) Math.cos((((double) this.camAngX) * 3.141592653589793d) / 180.0d));
            vec3f2.y = (-this.distanceToPivot) * ((float) Math.sin((((double) this.camAngX) * 3.141592653589793d) / 180.0d));
            Vec3f i = vec3f2.mo23503i(vec3f);
            TransformWrap bcVar = new TransformWrap();
            bcVar.mo17392z(this.camAngY);
            bcVar.mo17391y(this.camAngX);
            bcVar.mo17323A(this.camAngZ);
            bcVar.setTranslation(i);
            setCameraTransform(bcVar);
        }
    }

    public void lookTo(Vec3d ajr) {
        if (this.viewType == 0) {
            Vec3d f = ajr.mo9517f((Tuple3d) this.camera.getPosition());
            this.distanceToPivot = (float) f.length();
            f.normalize();
            this.camAngX = (((float) Math.asin(f.y)) * 180.0f) / 3.1415927f;
            this.camAngY = ((((float) Math.atan2(f.x, f.z)) * 180.0f) / 3.1415927f) + 180.0f;
            TransformWrap bcVar = new TransformWrap();
            bcVar.mo17392z(this.camAngY);
            bcVar.mo17391y(this.camAngX);
            bcVar.mo17323A(this.camAngZ);
            bcVar.setTranslation(this.camera.getPosition());
            try {
                setCameraTransform(bcVar);
            } catch (Exception e) {
                System.out.println("Invalid camera transform: " + bcVar + " angles " + this.camAngX + ", " + this.camAngY + ", " + this.camAngZ + "  position: " + ajr + " camera position " + this.camera.getPosition());
            }
        }
    }

    public void orbitZoomOut(float f, Vec3f vec3f) {
        if (f > 0.0f) {
            this.distanceToPivot += this.speed * f * 0.25f;
            orbit(0.0f, 0.0f, vec3f);
        }
    }

    public void orbitZoomIn(float f, Vec3f vec3f) {
        if (f > 0.0f) {
            this.distanceToPivot -= (this.speed * f) * 0.25f;
            orbit(0.0f, 0.0f, vec3f);
        }
    }

    public float getDistanceToPivot() {
        return this.distanceToPivot;
    }

    public void setDistanceToPivot(float f) {
        this.distanceToPivot = f;
    }
}
