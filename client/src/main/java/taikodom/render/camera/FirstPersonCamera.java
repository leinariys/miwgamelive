package taikodom.render.camera;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.aLH;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.StepContext;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Tuple3d;

/* compiled from: a */
public class FirstPersonCamera extends Camera {
    private static final TransformWrap targetMatrixWithNoScale = new TransformWrap();
    private final Vec3f currentCameraOffset = new Vec3f();
    private final Vec3d currentTargetOffset = new Vec3d();
    private Vec3d cameraTarget;
    private float filterTimeConstant = 0.16f;
    private Matrix4fWrap mrot;
    private Matrix4fWrap mrot2;
    private Vec3d newPos;
    private Vec3f newX;
    private Vec3f newY;
    private Vec3f newZ;
    private SceneObject target;
    private aLH targetLSAABB;

    public void copyState(ChaseCamera chaseCamera) {
        this.currentCameraOffset.set(chaseCamera.getCameraOffset());
        this.currentTargetOffset.mo9484aA(chaseCamera.getCameraTargetOffset());
    }

    public void setTarget(SceneObject sceneObject) {
        this.target = sceneObject;
        if (sceneObject != null) {
            this.targetLSAABB = sceneObject.computeLocalSpaceAABB();
        }
    }

    public int step(StepContext stepContext) {
        if (this.target != null) {
            this.transform.mo17344b(this.target.getTransform());
        }
        super.step(stepContext);
        if (this.target != null) {
            this.target.getTransformWithNoScale(targetMatrixWithNoScale);
            this.newPos = stepContext.vec3dTemp0;
            if (this.currentCameraOffset.lengthSquared() > 1.0f) {
                float exp = (float) (1.0d - Math.exp((double) ((-stepContext.getDeltaTime()) / this.filterTimeConstant)));
                if (exp > 1.0f) {
                    exp = 1.0f;
                }
                this.newPos.set(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
                this.currentCameraOffset.scale(1.0f - exp);
                targetMatrixWithNoScale.multiply3x4(this.currentCameraOffset, this.newPos);
                this.mrot = stepContext.matrix4fTemp0;
                this.mrot2 = stepContext.matrix4fTemp1;
                this.mrot.setIdentity();
                Vec3d ajr = new Vec3d(ScriptRuntime.NaN - this.currentTargetOffset.x, ScriptRuntime.NaN - this.currentTargetOffset.y, -100.0d - this.currentTargetOffset.z);
                ajr.scale((double) exp);
                this.currentTargetOffset.add(ajr);
                this.cameraTarget = stepContext.vec3dTemp1;
                this.mrot.mo13986a((Tuple3d) this.currentTargetOffset, (Tuple3d) this.cameraTarget);
                targetMatrixWithNoScale.multiply3x4(this.cameraTarget, this.cameraTarget);
                this.newX = stepContext.vec3ftemp0;
                this.newY = stepContext.vec3ftemp1;
                this.newZ = stepContext.vec3ftemp2;
                this.newZ.set(this.newPos);
                this.newZ.mo23464A((float) this.cameraTarget.x, (float) this.cameraTarget.y, (float) this.cameraTarget.z);
                this.newZ.normalize();
                this.newY.set(targetMatrixWithNoScale.getVectorY());
                this.newX.set(this.newZ);
                this.newX.mo23483c(this.newY);
                this.newX.negate();
                this.newX.normalize();
                this.newY.set(this.newX);
                this.newY.mo23483c(this.newZ);
                this.newY.negate();
                this.newY.normalize();
                this.mrot2.mo13988aD(this.newX);
                this.mrot2.mo13989aE(this.newY);
                this.mrot2.mo13990aF(this.newZ);
                this.mrot2.setTranslation(this.newPos);
                this.mrot2.mul(this.mrot, this.mrot2);
                this.transform.set(this.mrot2);
                if (this.currentCameraOffset.lengthSquared() > this.targetLSAABB.clX() / 4.0f) {
                    this.target.setRender(true);
                } else {
                    this.target.setRender(false);
                }
            } else {
                this.target.setRender(false);
                this.transform.mo17344b(targetMatrixWithNoScale);
            }
            updateInternalGeometry((SceneObject) null);
        }
        return 0;
    }

    public Vec3f getCurrentCameraOffset() {
        return this.currentCameraOffset;
    }

    public void setCurrentCameraOffset(Vec3f vec3f) {
        this.currentCameraOffset.set(vec3f);
    }

    public Vec3d getCurrentTargetOffset() {
        return this.currentTargetOffset;
    }

    public void setCurrentTargetOffset(Vec3d ajr) {
        this.currentTargetOffset.mo9484aA(ajr);
    }

    public float getFilterTimeConstant() {
        return this.filterTimeConstant;
    }

    public void setFilterTimeConstant(float f) {
        this.filterTimeConstant = f;
    }
}
