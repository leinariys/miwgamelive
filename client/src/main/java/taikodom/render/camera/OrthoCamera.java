package taikodom.render.camera;

import javax.media.opengl.GL;

/* compiled from: a */
public class OrthoCamera {
    private float bottom = -1.0f;
    private float farPlane;
    private float left = -1.0f;
    private float nearPlane;
    private float right = 1.0f;
    private float top = 1.0f;

    public void updateGLModelview(GL gl) {
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
    }

    public void updateGLProjection(GL gl) {
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        gl.glOrtho((double) this.left, (double) this.right, (double) this.bottom, (double) this.top, (double) (-this.nearPlane), (double) this.farPlane);
        gl.glMatrixMode(5888);
    }

    public void setOrthoWindow(float f, float f2, float f3, float f4) {
        this.left = f;
        this.right = f2;
        this.bottom = f3;
        this.top = f4;
    }
}
