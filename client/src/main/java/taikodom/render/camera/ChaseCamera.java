package taikodom.render.camera;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.StepContext;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Vector3f;
import java.util.Random;

/* compiled from: a */
public class ChaseCamera extends Camera {
    private static final float FIXED_TIME_STEP = 0.1f;
    final Vec3d cameraOffset = new Vec3d();
    final Vec3d cameraTargetOffset = new Vec3d();
    int RAND_MAX;
    float filterTimeConstant = 0.25f;
    Vec3d lastPos = new Vec3d();
    Matrix4fWrap mrot2;
    Random randomFactor;
    SceneObject target;
    Matrix4fWrap transformAsMatrix4;
    private Vec3f cameraTarget;
    private float damping;
    private float defaultFovY;
    private boolean enableFov;
    private float mass;
    private float maxCameraRelativeOffset;
    private float maxCameraRotation;
    private float maxFovY;
    private float maxShake;
    private float maxSolidAngularVelocity;
    private float maxSolidLinearVelocity;
    private float minFovY;
    private Matrix4fWrap mrot;
    private Vec3f newY;
    private boolean shakeForever;
    private float shakeTime;
    private float stiffness;
    private Vec3f vetor;

    public ChaseCamera() {
        this.cameraTargetOffset.set(ScriptRuntime.NaN, 25.0d, ScriptRuntime.NaN);
        this.enableFov = true;
        this.maxSolidLinearVelocity = 100.0f;
        this.maxSolidAngularVelocity = 90.0f;
        this.maxCameraRelativeOffset = 0.25f;
        this.maxCameraRotation = 15.0f;
        this.maxShake = 0.0f;
        this.shakeTime = 0.0f;
        this.shakeForever = false;
        this.randomFactor = new Random();
        this.vetor = new Vec3f();
        this.mrot = new Matrix4fWrap();
        this.cameraTarget = new Vec3f();
        this.newY = new Vec3f();
        this.mrot2 = new Matrix4fWrap();
        this.transformAsMatrix4 = new Matrix4fWrap();
        this.defaultFovY = 75.0f;
        this.maxFovY = 100.0f;
        this.minFovY = 60.0f;
        this.firstFrame = true;
        this.stiffness = 30.0f;
        this.damping = 8.0f;
        this.mass = 1.0f;
    }

    ChaseCamera(ChaseCamera chaseCamera) {
    }

    /* access modifiers changed from: package-private */
    public Vec3d getCameraTargetOffset() {
        return this.cameraTargetOffset;
    }

    public void setCameraTargetOffset(Vec3d ajr) {
        this.cameraTargetOffset.mo9484aA(ajr);
    }

    /* access modifiers changed from: package-private */
    public float getMinFovY() {
        return this.minFovY;
    }

    /* access modifiers changed from: package-private */
    public void setMinFovY(float f) {
        this.minFovY = f;
    }

    /* access modifiers changed from: package-private */
    public float getMaxFovY() {
        return this.maxFovY;
    }

    /* access modifiers changed from: package-private */
    public void setMaxFovY(float f) {
        this.maxFovY = f;
    }

    /* access modifiers changed from: package-private */
    public float getMaxCameraRotation() {
        return this.maxCameraRotation;
    }

    /* access modifiers changed from: package-private */
    public void setMaxCameraRotation(float f) {
        this.maxCameraRotation = f;
    }

    /* access modifiers changed from: package-private */
    public float getMaxCameraRelativeOffset() {
        return this.maxCameraRelativeOffset;
    }

    /* access modifiers changed from: package-private */
    public void setMaxCameraRelativeOffset(float f) {
        this.maxCameraRelativeOffset = f;
    }

    /* access modifiers changed from: package-private */
    public float getMaxSolidAngularVelocity() {
        return this.maxSolidAngularVelocity;
    }

    /* access modifiers changed from: package-private */
    public void setMaxSolidAngularVelocity(float f) {
        this.maxSolidAngularVelocity = f;
    }

    /* access modifiers changed from: package-private */
    public float getMaxSolidLinearVelocity() {
        return this.maxSolidLinearVelocity;
    }

    /* access modifiers changed from: package-private */
    public void setMaxSolidLinearVelocity(float f) {
        if (f > 0.0f) {
            this.maxSolidLinearVelocity = f;
        }
    }

    public Camera cloneAsset() {
        return this;
    }

    public boolean isEnableFov() {
        return this.enableFov;
    }

    public void setEnableFov(boolean z) {
        this.enableFov = z;
    }

    public int step(StepContext stepContext) {
        if (this.target == null) {
            return 0;
        }
        float deltaTime = stepContext.getDeltaTime();
        Vec3d ajr = stepContext.vec3dTemp0;
        Vec3d ajr2 = stepContext.vec3dTemp1;
        Vec3f velocity = this.target.getVelocity();
        Vec3f vec3f = stepContext.vec3ftemp0;
        float length = velocity.length() / this.maxSolidLinearVelocity;
        this.target.getGlobalTransform().orientation.mo13947a((Vector3f) velocity, (Vector3f) vec3f);
        if (length > 1.0f) {
        }
        while (deltaTime > FIXED_TIME_STEP) {
            integrate(ajr, ajr2, 0.0f, FIXED_TIME_STEP);
            deltaTime -= FIXED_TIME_STEP;
        }
        integrate(ajr, ajr2, 0.0f, deltaTime);
        this.transform.mo17334a(this.target.getGlobalTransform().orientation);
        Vec3f vec3f2 = stepContext.vec3ftemp0;
        Vec3f vec3f3 = stepContext.vec3ftemp1;
        Vec3f vec3f4 = stepContext.vec3ftemp2;
        this.target.getGlobalTransform().getZ(vec3f2);
        this.target.getGlobalTransform().getY(vec3f3);
        this.target.getGlobalTransform().getX(vec3f4);
        vec3f2.normalize();
        vec3f3.normalize();
        vec3f4.normalize();
        this.transform.setX(vec3f4);
        this.transform.setY(vec3f3);
        this.transform.setZ(vec3f2);
        updateInternalGeometry((SceneObject) null);
        this.lastPos.sub(this.target.getGlobalTransform().position);
        this.lastPos.scale((double) (1.0f / deltaTime));
        this.lastPos.mo9484aA(this.target.getGlobalTransform().position);
        this.stiffness = 5.0f;
        this.damping = 1.0f;
        this.mass = FIXED_TIME_STEP;
        return 1;
    }

    private void integrate(Vec3d ajr, Vec3d ajr2, float f, float f2) {
        Vec3d ajr3 = this.transform.position;
        ajr2.x = ((double) ((this.maxCameraRelativeOffset * f) + 1.0f)) * this.cameraOffset.x;
        ajr2.y = ((double) ((this.maxCameraRelativeOffset * f) + 1.0f)) * this.cameraOffset.y;
        ajr2.z = ((double) ((this.maxCameraRelativeOffset * f) + 1.0f)) * this.cameraOffset.z;
        this.target.getGlobalTransform().mo17343b(ajr2, ajr);
        if (this.firstFrame) {
            this.firstFrame = false;
            this.transform.position.mo9484aA(ajr);
        }
        Vec3d ajr4 = new Vec3d();
        ajr4.sub(this.transform.position, ajr);
        ajr4.scale((double) (-this.stiffness));
        ajr4.add(this.velocity.mo23510mS(-this.damping));
        Vec3f dfV = ajr4.dfV();
        dfV.scale(1.0f / this.mass);
        dfV.scale(f2);
        this.velocity.add(dfV);
        ajr3.add(this.velocity.mo23510mS(f2));
    }

    public float getMaxShake() {
        return this.maxShake;
    }

    public void setMaxShake(float f) {
        this.maxShake = f;
    }

    public void setShakeTime(float f) {
        this.shakeTime = f;
    }

    public void setShakeForever(boolean z) {
        this.shakeForever = z;
    }

    public float getDefaultFovY() {
        return this.defaultFovY;
    }

    public void setDefaultFovY(float f) {
        this.defaultFovY = f;
    }

    public SceneObject getTarget() {
        return this.target;
    }

    public void setTarget(SceneObject sceneObject) {
        this.target = sceneObject;
    }

    public Vec3d getCameraOffset() {
        return this.cameraOffset;
    }

    public void setCameraOffset(Vec3d ajr) {
        this.cameraOffset.mo9484aA(ajr);
    }
}
