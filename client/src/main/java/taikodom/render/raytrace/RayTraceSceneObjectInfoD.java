package taikodom.render.raytrace;

import game.geometry.Vec3d;
import game.geometry.aDM;
import taikodom.render.scene.SceneObject;

/* compiled from: a */
public class RayTraceSceneObjectInfoD extends aDM {
    private boolean changed = false;
    private SceneObject intersectedObject = null;
    private Vec3d intersectedPoint = new Vec3d();

    public RayTraceSceneObjectInfoD(Vec3d ajr, Vec3d ajr2) {
        super(ajr, ajr2);
    }

    public Vec3d getIntersectedPoint() {
        return this.intersectedPoint;
    }

    public void setIntersectedPoint(Vec3d ajr) {
        this.intersectedPoint.mo9484aA(ajr);
    }

    public SceneObject getIntersectedObject() {
        return this.intersectedObject;
    }

    public void setIntersectedObject(SceneObject sceneObject) {
        this.intersectedObject = sceneObject;
    }

    public boolean isChanged() {
        return this.changed;
    }

    public void setChanged(boolean z) {
        this.changed = z;
    }
}
