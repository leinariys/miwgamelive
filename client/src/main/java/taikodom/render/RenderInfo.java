package taikodom.render;

import com.hoplon.geometry.Color;

/* compiled from: a */
public class RenderInfo {
    private final Color clearColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    private final Color fogColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    private boolean clearColorBuffer = true;
    private boolean clearDepthBuffer = true;
    private boolean clearStencilBuffer = true;
    private boolean drawAABBs = false;
    private boolean drawLights = false;
    private float fogEnd = 1000.0f;
    private float fogExp = 1.0f;
    private float fogStart = 1.0f;
    private int fogType = 9729;
    private boolean noShading = false;
    private boolean wireframeOnly = false;

    public void set(RenderInfo renderInfo) {
        this.clearColor.set(renderInfo.clearColor);
        this.clearColorBuffer = renderInfo.clearColorBuffer;
        this.clearDepthBuffer = renderInfo.clearDepthBuffer;
        this.clearStencilBuffer = renderInfo.clearStencilBuffer;
        this.fogType = renderInfo.fogType;
        this.fogStart = renderInfo.fogStart;
        this.fogEnd = renderInfo.fogEnd;
        this.fogExp = renderInfo.fogExp;
        this.fogColor.set(renderInfo.fogColor);
        this.wireframeOnly = renderInfo.wireframeOnly;
        this.noShading = renderInfo.noShading;
        this.drawAABBs = renderInfo.drawAABBs;
        this.drawLights = renderInfo.drawLights;
    }

    public boolean isClearColorBuffer() {
        return this.clearColorBuffer;
    }

    public void setClearColorBuffer(boolean z) {
        this.clearColorBuffer = z;
    }

    public boolean isClearDepthBuffer() {
        return this.clearDepthBuffer;
    }

    public void setClearDepthBuffer(boolean z) {
        this.clearDepthBuffer = z;
    }

    public boolean isClearStencilBuffer() {
        return this.clearStencilBuffer;
    }

    public void setClearStencilBuffer(boolean z) {
        this.clearStencilBuffer = z;
    }

    public int getFogType() {
        return this.fogType;
    }

    public void setFogType(int i) {
        this.fogType = i;
    }

    public float getFogStart() {
        return this.fogStart;
    }

    public void setFogStart(float f) {
        this.fogStart = f;
    }

    public float getFogEnd() {
        return this.fogEnd;
    }

    public void setFogEnd(float f) {
        this.fogEnd = f;
    }

    public float getFogExp() {
        return this.fogExp;
    }

    public void setFogExp(float f) {
        this.fogExp = f;
    }

    public Color getFogColor() {
        return this.fogColor;
    }

    public void setFogColor(Color color) {
        this.fogColor.set(color);
    }

    public boolean isWireframeOnly() {
        return this.wireframeOnly;
    }

    public void setWireframeOnly(boolean z) {
        this.wireframeOnly = z;
    }

    public Color getClearColor() {
        return this.clearColor;
    }

    public void setClearColor(float f, float f2, float f3, float f4) {
        this.clearColor.set(f, f2, f3, f4);
    }

    public boolean isDrawAABBs() {
        return this.drawAABBs;
    }

    public void setDrawAABBs(boolean z) {
        this.drawAABBs = z;
    }

    public boolean isNoShading() {
        return this.noShading;
    }

    public void setNoShading(boolean z) {
        this.noShading = z;
    }

    public boolean isDrawLights() {
        return this.drawLights;
    }

    public void setDrawLights(boolean z) {
        this.drawLights = z;
    }
}
