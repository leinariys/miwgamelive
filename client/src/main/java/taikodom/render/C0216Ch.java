package taikodom.render;

import game.geometry.*;
import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.Ch */
/* compiled from: a */
public class C0216Ch extends C3862vv {
    private final Vec3d cameraOffset = new Vec3d();
    private final TransformWrap cuU = new TransformWrap();
    private final aLH cuV = new aLH();
    private final Vec3d cuW = new Vec3d();
    private final aVS cuX = new aVS();

    /* renamed from: a */
    public void mo1158a(TransformWrap bcVar, Matrix4fWrap ajk) {
        this.cameraOffset.mo9484aA(bcVar.position);
        this.cameraOffset.negate();
        this.cuU.mo17344b(bcVar);
        this.cuU.position.set(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        super.mo1158a(this.cuU, ajk);
    }

    /* renamed from: a */
    public boolean mo1160a(aLH alh) {
        this.cuV.mo9867g(alh);
        this.cuV.mo9846aK(this.cameraOffset);
        return super.mo1160a(this.cuV);
    }

    /* renamed from: a */
    public boolean mo1159a(C1085Pt pt) {
        this.cuX.center.mo9497bG(pt.bny());
        this.cuX.center.add(this.cameraOffset);
        this.cuX.jcB = (double) pt.getRadius();
        return super.mo1161a(this.cuX);
    }

    /* renamed from: a */
    public boolean mo1161a(aVS avs) {
        this.cuX.mo11772b(avs);
        this.cuX.center.add(this.cameraOffset);
        return super.mo1161a(avs);
    }

    /* renamed from: v */
    public boolean mo1163v(Vec3d ajr) {
        this.cuW.mo9484aA(ajr);
        this.cuW.add(this.cameraOffset);
        return super.mo1163v(ajr);
    }

    /* renamed from: b */
    public int mo1162b(aLH alh) {
        this.cuV.mo9867g(alh);
        this.cuV.mo9846aK(this.cameraOffset);
        return super.mo1162b(this.cuV);
    }
}
