package taikodom.render.helpers;

import taikodom.render.scene.SceneObject;

import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class FindInstancesVisitor<T extends SceneObject> implements RenderVisitor {
    Class<T> findClass;
    List<T> list = new ArrayList();

    public FindInstancesVisitor(Class<T> cls) {
        this.findClass = cls;
    }

    public boolean visit(SceneObject sceneObject) {
        if (!this.findClass.isInstance(sceneObject)) {
            return true;
        }
        this.list.add(sceneObject);
        return true;
    }

    public List<T> getInstances() {
        return this.list;
    }

    public void reset() {
        this.list.clear();
    }
}
