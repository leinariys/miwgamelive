package taikodom.render.helpers;

import com.hoplon.geometry.Vec3f;
import taikodom.render.data.IntIndexData;
import taikodom.render.data.ShortIndexData;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.RLine;
import taikodom.render.scene.SceneObject;
import taikodom.render.scene.custom.RCustomMesh;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/* compiled from: a */
public class MeshUtils {
    public static final boolean NORMALS_DEBUG = false;
    public static final RLine normal = null;
    public static final RLine tan = null;
    private static final boolean SHOW_FINAL_TIMES = false;
    private static final boolean SHOW_INTERMEDIARY_TIMES = false;

    public static void sortTrianglesByDistance(Mesh mesh, Vec3f vec3f) {
        int i = 0;
        if (mesh.getIndexes().isVBO()) {
            throw new RuntimeException("Mesh is already a VBO, could not be sorted");
        }
        Vec3f vec3f2 = new Vec3f();
        Vec3f vec3f3 = new Vec3f();
        Vec3f vec3f4 = new Vec3f();
        if (mesh.getIndexes() instanceof IntIndexData) {
            IntIndexData intIndexData = (IntIndexData) mesh.getIndexes();
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < intIndexData.size(); i2 += 3) {
                C5142c cVar = new C5142c();
                cVar.gfc = intIndexData.getIndex(i2);
                cVar.gfd = intIndexData.getIndex(i2 + 1);
                cVar.gfe = intIndexData.getIndex(i2 + 2);
                mesh.getVertexData().getPosition(cVar.gfc, vec3f2);
                mesh.getVertexData().getPosition(cVar.gfc, vec3f3);
                mesh.getVertexData().getPosition(cVar.gfc, vec3f4);
                vec3f2.add(vec3f3);
                vec3f2.add(vec3f4);
                vec3f2.scale(0.333f);
                cVar.gff = Vec3f.direction(vec3f2, vec3f);
                arrayList.add(cVar);
            }
            Collections.sort(arrayList, new C5141b());
            while (i < intIndexData.size()) {
                intIndexData.setIndex(i, ((C5142c) arrayList.get(i / 3)).gfc);
                intIndexData.setIndex(i + 1, ((C5142c) arrayList.get(i / 3)).gfd);
                intIndexData.setIndex(i + 2, ((C5142c) arrayList.get(i / 3)).gfe);
                i += 3;
            }
        } else if (mesh.getIndexes() instanceof ShortIndexData) {
            ShortIndexData shortIndexData = (ShortIndexData) mesh.getIndexes();
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < shortIndexData.size(); i3 += 3) {
                C5142c cVar2 = new C5142c();
                cVar2.gfc = shortIndexData.getIndex(i3);
                cVar2.gfd = shortIndexData.getIndex(i3 + 1);
                cVar2.gfe = shortIndexData.getIndex(i3 + 2);
                mesh.getVertexData().getPosition(cVar2.gfc, vec3f2);
                mesh.getVertexData().getPosition(cVar2.gfc, vec3f3);
                mesh.getVertexData().getPosition(cVar2.gfc, vec3f4);
                vec3f2.add(vec3f3);
                vec3f2.add(vec3f4);
                vec3f2.scale(0.333f);
                cVar2.gff = Vec3f.direction(vec3f2, vec3f);
                arrayList2.add(cVar2);
            }
            Collections.sort(arrayList2, new C5140a());
            while (i < shortIndexData.size()) {
                shortIndexData.setIndex(i, ((C5142c) arrayList2.get(i / 3)).gfc);
                shortIndexData.setIndex(i + 1, ((C5142c) arrayList2.get(i / 3)).gfd);
                shortIndexData.setIndex(i + 2, ((C5142c) arrayList2.get(i / 3)).gfe);
                i += 3;
            }
        }
    }

    public static void rebuild(RCustomMesh rCustomMesh) {
        rCustomMesh.buildIndexData();
        rCustomMesh.weldParts(true);
        rCustomMesh.updateInternalGeometry((SceneObject) null);
    }

    public static void setPartVisible(RCustomMesh rCustomMesh, String str, boolean z) {
        if (z) {
            rCustomMesh.showPart(str);
        } else {
            rCustomMesh.hidePart(str);
        }
    }

    /* renamed from: taikodom.render.helpers.MeshUtils$c */
    /* compiled from: a */
    class C5142c {
        int gfc;
        int gfd;
        int gfe;
        float gff;

        C5142c() {
        }
    }

    /* renamed from: taikodom.render.helpers.MeshUtils$b */
    /* compiled from: a */
    class C5141b implements Comparator<C5142c> {
        C5141b() {
        }

        /* renamed from: a */
        public int compare(C5142c cVar, C5142c cVar2) {
            if (cVar.gff < cVar2.gff) {
                return -1;
            }
            return 1;
        }
    }

    /* renamed from: taikodom.render.helpers.MeshUtils$a */
    class C5140a implements Comparator<C5142c> {
        C5140a() {
        }

        /* renamed from: a */
        public int compare(C5142c cVar, C5142c cVar2) {
            if (cVar.gff < cVar2.gff) {
                return -1;
            }
            return 1;
        }
    }
}
