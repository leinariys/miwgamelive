package taikodom.render.helpers;

import taikodom.render.scene.SceneObject;

/* compiled from: a */
public interface RenderVisitor {
    boolean visit(SceneObject sceneObject);
}
