package taikodom.render.helpers;

import taikodom.render.scene.SceneObject;

/* compiled from: a */
public class FindByNameVisitor implements RenderVisitor {
    private String name;
    private SceneObject result = null;

    public FindByNameVisitor(String str) {
        this.name = str;
    }

    public static SceneObject runPreOrder(String str, SceneObject sceneObject) {
        FindByNameVisitor findByNameVisitor = new FindByNameVisitor(str);
        sceneObject.visitPreOrder(findByNameVisitor);
        return findByNameVisitor.getResult();
    }

    /* JADX WARNING: Removed duplicated region for block: B:1:0x000d A[LOOP:0: B:1:0x000d->B:6:0x0022, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static taikodom.render.scene.SceneObject runPreOrder(java.lang.String r3, taikodom.render.scene.Scene r4) {
        /*
            taikodom.render.helpers.FindByNameVisitor r1 = new taikodom.render.helpers.FindByNameVisitor
            r1.<init>(r3)
            java.util.Set r0 = r4.getChildren()
            java.util.Iterator r2 = r0.iterator()
        L_0x000d:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0018
        L_0x0013:
            taikodom.render.scene.SceneObject r0 = r1.getResult()
            return r0
        L_0x0018:
            java.lang.Object r0 = r2.next()
            taikodom.render.scene.SceneObject r0 = (taikodom.render.scene.SceneObject) r0
            boolean r0 = r0.visitPreOrder(r1)
            if (r0 != 0) goto L_0x000d
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.render.helpers.FindByNameVisitor.runPreOrder(java.lang.String, taikodom.render.scene.Scene):taikodom.render.scene.SceneObject");
    }

    public boolean visit(SceneObject sceneObject) {
        if (!this.name.equals(sceneObject.getName())) {
            return true;
        }
        this.result = sceneObject;
        return false;
    }

    public SceneObject getResult() {
        return this.result;
    }
}
