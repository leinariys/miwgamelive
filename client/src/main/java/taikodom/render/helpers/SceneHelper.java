package taikodom.render.helpers;

import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.ImageUtil;
import game.geometry.*;
import logic.res.FileControl;
import org.mozilla1.classfile.C0147Bi;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.camera.Camera;
import taikodom.render.data.IndexData;
import taikodom.render.data.VertexData;
import taikodom.render.scene.*;
import taikodom.render.textures.RenderTarget;

import javax.imageio.ImageIO;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class SceneHelper {

    /* renamed from: in */
    static final Vector4dWrap f10353in = new Vector4dWrap();
    static final Vector4dWrap out = new Vector4dWrap();
    static final Vec3d tempProj = new Vec3d();
    static final Vec3d[] vertsArray = new Vec3d[8];

    static {
        for (int i = 0; i < 8; i++) {
            vertsArray[i] = new Vec3d();
        }
    }

    public static <T extends SceneObject> List<T> getChildInstances(SceneObject sceneObject, Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        getInstances(sceneObject, arrayList, cls);
        return arrayList;
    }

    private static <T extends SceneObject> void getInstances(SceneObject sceneObject, List<T> list, Class<T> cls) {
        if (sceneObject != null) {
            if (cls.isInstance(sceneObject)) {
                list.add(sceneObject);
            } else if (sceneObject instanceof RLod) {
                getInstances(((RLod) sceneObject).getObjectLod0(), list, cls);
            }
            for (SceneObject instances : sceneObject.getChildren()) {
                getInstances(instances, list, cls);
            }
        }
    }

    public static <T extends SceneObject> List<T> getChildInstancesAndRemove(SceneObject sceneObject, Class<T> cls) {
        ArrayList arrayList = new ArrayList();
        getInstancesAndRemove(sceneObject, arrayList, cls);
        return arrayList;
    }

    private static <T extends SceneObject> boolean getInstancesAndRemove(SceneObject sceneObject, List<T> list, Class<T> cls) {
        boolean z = false;
        if (sceneObject != null) {
            if (cls.isInstance(sceneObject)) {
                list.add(sceneObject);
                z = true;
            } else if ((sceneObject instanceof RLod) && getInstancesAndRemove(((RLod) sceneObject).getObjectLod0(), list, cls)) {
                ((RLod) sceneObject).setObjectLod0((SceneObject) null);
            }
            ArrayList<SceneObject> arrayList = new ArrayList<>();
            for (SceneObject next : sceneObject.getChildren()) {
                if (getInstancesAndRemove(next, list, cls)) {
                    arrayList.add(next);
                }
            }
            for (SceneObject removeChild : arrayList) {
                sceneObject.removeChild(removeChild);
            }
            arrayList.clear();
        }
        return z;
    }

    public static Vec3f projectMouseIntoWorld(Vector2fWrap aka, Vector2fWrap aka2, Camera camera, float f) {
        throw new RuntimeException("Not implemented!");
    }

    public static void getAABBWindowCoords(Camera camera, Viewport viewport, aLH alh, Vector2fWrap aka, Vector2fWrap aka2) {
        aka.x = 999999.0f;
        aka.y = 999999.0f;
        aka2.x = -999999.0f;
        aka2.y = -999999.0f;
        Vec3d dim = alh.dim();
        Vec3d din = alh.din();
        vertsArray[0].mo9484aA(dim);
        vertsArray[1].mo9484aA(din);
        vertsArray[2].x = din.x;
        vertsArray[2].y = din.y;
        vertsArray[2].z = dim.z;
        vertsArray[3].x = din.x;
        vertsArray[3].y = dim.y;
        vertsArray[3].z = din.z;
        vertsArray[4].x = dim.x;
        vertsArray[4].y = din.y;
        vertsArray[4].z = din.z;
        vertsArray[5].x = din.x;
        vertsArray[5].y = dim.y;
        vertsArray[5].z = dim.z;
        vertsArray[6].x = dim.x;
        vertsArray[6].y = din.y;
        vertsArray[6].z = dim.z;
        vertsArray[7].x = dim.x;
        vertsArray[7].y = dim.y;
        vertsArray[7].z = din.z;
        for (int i = 0; i < 8; i++) {
            getWindowCoords(camera, viewport, vertsArray[i], tempProj);
            if (tempProj.z < 1.0d && tempProj.z > ScriptRuntime.NaN) {
                float f = (float) tempProj.x;
                float f2 = (float) tempProj.y;
                if (f > aka2.x) {
                    aka2.x = f;
                }
                if (f < aka.x) {
                    aka.x = f;
                }
                if (f2 > aka2.y) {
                    aka2.y = f2;
                }
                if (f2 < aka.y) {
                    aka.y = f2;
                }
            }
        }
    }

    public static void getWindowCoords(Camera camera, Viewport viewport, Vec3d ajr, Vec3d ajr2) {
        f10353in.x = ajr.x;
        f10353in.y = ajr.y;
        f10353in.z = ajr.z;
        f10353in.w = 1.0d;
        camera.getGlobalTransform().mo17341b(f10353in, out);
        camera.getProjection().mo14005c(out, f10353in);
        if (f10353in.w != ScriptRuntime.NaN) {
            f10353in.x /= f10353in.w;
            f10353in.y /= f10353in.w;
            f10353in.z /= f10353in.w;
            ajr2.x = ((double) viewport.x) + (((f10353in.x + 1.0d) * ((double) viewport.width)) / 2.0d);
            ajr2.y = ((double) viewport.y) + (((f10353in.y + 1.0d) * ((double) viewport.height)) / 2.0d);
            ajr2.z = (f10353in.z + 1.0d) / 2.0d;
        }
    }

    public static void generateMeshNormals(RMesh rMesh, RLine rLine, RLine rLine2, float f) {
        VertexData vertexData = rMesh.getMesh().getVertexData();
        IndexData indexes = rMesh.getMesh().getIndexes();
        int size = indexes.size();
        Vec3f vec3f = new Vec3f();
        Vec3f vec3f2 = new Vec3f();
        Vector4fWrap ajf = new Vector4fWrap();
        for (int i = 0; i < size; i++) {
            int index = indexes.getIndex(i);
            vertexData.getPosition(index, vec3f);
            rLine.addPoint(vec3f);
            vertexData.getNormal(index, vec3f2);
            vec3f2.scale(f);
            vec3f2.add(vec3f);
            rLine.addPoint(vec3f2);
            rLine2.addPoint(vec3f);
            vertexData.getTangents(index, ajf);
            vec3f2.set(ajf.x, ajf.y, ajf.z);
            vec3f2.scale(f);
            vec3f2.add(vec3f);
            rLine2.addPoint(vec3f2);
        }
    }

    public static void saveScreenShot(SceneView sceneView, RenderView renderView, String str, String str2, String str3, int i, int i2, boolean z) {
        RenderTarget renderTarget = new RenderTarget();
        renderTarget.create(64, 64, true, renderView.getDrawContext(), false);
        renderTarget.bind(renderView.getDrawContext());
        RenderTarget renderTarget2 = sceneView.getRenderTarget();
        sceneView.setRenderTarget((RenderTarget) null);
        ArrayList arrayList = new ArrayList(sceneView.getPostProcessingFXs());
        sceneView.getPostProcessingFXs().clear();
        Viewport viewport = new Viewport(sceneView.getViewport());
        sceneView.setViewport(0, 0, 64, 64);
        renderView.render(sceneView, ScriptRuntime.NaN, 0.0f);
        sceneView.setRenderTarget(renderTarget2);
        sceneView.setViewport(viewport);
        sceneView.getPostProcessingFXs().addAll(arrayList);
        try {
            saveScreenShotWithAlpha(str, str2, "png", 64, 64);
        } catch (IOException e) {
            e.printStackTrace();
        }
        renderTarget.unbind(renderView.getDrawContext());
        renderTarget.releaseReferences(renderView.getDrawContext().getGl());
    }

    public static void saveScreenShotWithAlpha(String str, String str2, String str3, int i, int i2) {
        new FileControl(str).getFile().mkdirs();
        FileOutputStream fileOutputStream = new FileOutputStream(new FileControl(String.valueOf(str) + C0147Bi.SEPARATOR + str2 + "." + str3).getFile());
        ImageIO.write(generateBufferedImageWithAlpha(i, i2), str3, fileOutputStream);
        fileOutputStream.close();
    }

    private static BufferedImage generateBufferedImageWithAlpha(int i, int i2) {
        BufferedImage bufferedImage = new BufferedImage(i, i2, 6);
        GL currentGL = GLU.getCurrentGL();
        C5143a aVar = new C5143a();
        aVar.mo28029a(currentGL);
        currentGL.glReadPixels(0, 0, i, i2, 6408, 5121, ByteBuffer.wrap(bufferedImage.getRaster().getDataBuffer().getData()));
        aVar.mo28030b(currentGL);
        for (int i3 = 0; i3 < i; i3++) {
            for (int i4 = 0; i4 < i2; i4++) {
                int rgb = bufferedImage.getRGB(i3, i4);
                bufferedImage.setRGB(i3, i4, ((rgb & 255) << 8) | (((-16777216 & rgb) >> 24) << 16) | (((16711680 & rgb) >> 16) << 24) | ((65280 & rgb) >> 8));
            }
        }
        ImageUtil.flipImageVertically(bufferedImage);
        return bufferedImage;
    }

    /* renamed from: taikodom.render.helpers.SceneHelper$a */
    static class C5143a {

        /* renamed from: Ur */
        int f10354Ur;

        /* renamed from: Us */
        int[] f10355Us = new int[1];
        int packAlignment;
        int packRowLength;
        int packSkipPixels;
        int packSkipRows;

        C5143a() {
        }

        /* renamed from: a */
        private static int m45838a(GL gl, int i, int[] iArr) {
            gl.glGetIntegerv(i, iArr, 0);
            return iArr[0];
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo28029a(GL gl) {
            this.packAlignment = m45838a(gl, 3333, this.f10355Us);
            this.packRowLength = m45838a(gl, 3330, this.f10355Us);
            this.packSkipRows = m45838a(gl, 3331, this.f10355Us);
            this.packSkipPixels = m45838a(gl, 3332, this.f10355Us);
            this.f10354Ur = m45838a(gl, 3328, this.f10355Us);
            gl.glPixelStorei(3333, 1);
            gl.glPixelStorei(3330, 0);
            gl.glPixelStorei(3331, 0);
            gl.glPixelStorei(3332, 0);
            gl.glPixelStorei(3328, 0);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo28030b(GL gl) {
            gl.glPixelStorei(3333, this.packAlignment);
            gl.glPixelStorei(3330, this.packRowLength);
            gl.glPixelStorei(3331, this.packSkipRows);
            gl.glPixelStorei(3332, this.packSkipPixels);
            gl.glPixelStorei(3328, this.f10354Ur);
        }
    }
}
