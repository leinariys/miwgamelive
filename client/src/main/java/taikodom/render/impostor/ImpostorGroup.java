package taikodom.render.impostor;

import game.geometry.Vec3d;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.RenderObject;
import taikodom.render.textures.Material;

import java.util.*;

/* compiled from: a */
public class ImpostorGroup extends RenderObject {
    static final ArrayList<Impostor> toBeRemoved = new ArrayList<>();
    private boolean dirty = false;
    private List<Impostor> impostors = new ArrayList();
    private ExpandableIndexData indexes;
    private Mesh mesh;
    private boolean recalculateCenter = false;
    private DynamicTextureAtlas texAtlas;
    private ExpandableVertexData vertexes;

    public ImpostorGroup() {
        initRenderStructs();
    }

    public void releaseReferences() {
        super.releaseReferences();
        removeAllImpostors();
        this.texAtlas = null;
    }

    private void initRenderStructs() {
        this.vertexes = new ExpandableVertexData(new VertexLayout(true, true, false, false, 1), 300);
        this.indexes = new ExpandableIndexData((int) 300);
        this.mesh = new Mesh();
        this.mesh.setName("ImpostorGroup_mesh");
        this.mesh.setGeometryType(GeometryType.QUADS);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setIndexes(this.indexes);
    }

    public void removeImpostor(Impostor impostor) {
        if (this.impostors.remove(impostor)) {
            this.texAtlas.pushBackSlot(impostor.slot);
        } else {
            System.out.println("Impostor removed from an wrong group!!");
        }
        this.dirty = true;
    }

    public void addImpostor(Impostor impostor) {
        if (this.impostors.size() == 0) {
            this.recalculateCenter = true;
        }
        this.impostors.add(impostor);
        this.dirty = true;
    }

    private void recalculateCenter() {
        this.recalculateCenter = false;
        float size = 1.0f / ((float) this.impostors.size());
        Vec3d ajr = new Vec3d();
        for (Impostor impostor : this.impostors) {
            ajr.add(impostor.position.mo9476Z((double) size));
        }
        setPosition(ajr);
    }

    public void regenerateMesh() {
        Collections.sort(this.impostors, new C5144a());
        this.indexes.ensure(this.impostors.size() * 4);
        this.vertexes.ensure(this.impostors.size() * 4);
        this.indexes.clear();
        if (this.recalculateCenter) {
            recalculateCenter();
        }
        int i = 0;
        for (Impostor next : this.impostors) {
            DynamicTextureAtlas.MaterialSlot slot = next.getSlot();
            next.indexStart = i;
            this.indexes.setIndex(i, i);
            this.vertexes.setPosition(i, (float) (next.getVert0().x - this.globalTransform.position.x), (float) (next.getVert0().y - this.globalTransform.position.y), (float) (next.getVert0().z - this.globalTransform.position.z));
            this.vertexes.setTexCoord(i, 0, slot.minCoordinates.x, slot.minCoordinates.y);
            this.vertexes.setColor(i, next.currentColor, next.currentColor, next.currentColor, next.currentColor);
            int i2 = i + 1;
            this.indexes.setIndex(i2, i2);
            this.vertexes.setPosition(i2, (float) (next.getVert1().x - this.globalTransform.position.x), (float) (next.getVert1().y - this.globalTransform.position.y), (float) (next.getVert1().z - this.globalTransform.position.z));
            this.vertexes.setTexCoord(i2, 0, slot.maxCoordinates.x, slot.minCoordinates.y);
            this.vertexes.setColor(i2, next.currentColor, next.currentColor, next.currentColor, next.currentColor);
            int i3 = i2 + 1;
            this.indexes.setIndex(i3, i3);
            this.vertexes.setPosition(i3, (float) (next.getVert2().x - this.globalTransform.position.x), (float) (next.getVert2().y - this.globalTransform.position.y), (float) (next.getVert2().z - this.globalTransform.position.z));
            this.vertexes.setTexCoord(i3, 0, slot.maxCoordinates.x, slot.maxCoordinates.y);
            this.vertexes.setColor(i3, next.currentColor, next.currentColor, next.currentColor, next.currentColor);
            int i4 = i3 + 1;
            this.indexes.setIndex(i4, i4);
            this.vertexes.setPosition(i4, (float) (next.getVert3().x - this.globalTransform.position.x), (float) (next.getVert3().y - this.globalTransform.position.y), (float) (next.getVert3().z - this.globalTransform.position.z));
            this.vertexes.setTexCoord(i4, 0, slot.minCoordinates.x, slot.maxCoordinates.y);
            this.vertexes.setColor(i4, next.currentColor, next.currentColor, next.currentColor, next.currentColor);
            i = i4 + 1;
        }
        this.indexes.setSize(i);
        this.dirty = false;
        this.mesh.computeLocalAabb();
        this.localAabb.mo9867g(this.mesh.getAabb());
        this.localAabb.mo9834a(this.globalTransform, this.aabbWorldSpace);
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render) {
            return false;
        }
        if (this.dirty) {
            regenerateMesh();
        }
        if (!super.render(renderContext, z)) {
            return false;
        }
        if (this.mesh.getIndexes().size() == 0) {
            return true;
        }
        if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
            renderContext.getDc().incNumImpostorsRendered();
            renderContext.addPrimitive(this.texAtlas.getAtlasMaterial(), this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
            renderContext.incMeshesRendered();
        }
        return true;
    }

    public DynamicTextureAtlas.MaterialSlot getMaterialSlot(int i) {
        return this.texAtlas.getFreeSlot(i);
    }

    public DynamicTextureAtlas getTexAtlas() {
        return this.texAtlas;
    }

    public void setTexAtlas(DynamicTextureAtlas dynamicTextureAtlas) {
        this.texAtlas = dynamicTextureAtlas;
    }

    public Material getMaterial() {
        return this.texAtlas.atlasMaterial;
    }

    public void setDirty(boolean z) {
        this.dirty = z;
    }

    public void removeInvalidImpostors(RenderContext renderContext) {
        for (Impostor next : this.impostors) {
            if (next.position.mo9491ax(renderContext.getCamera().getGlobalTransform().position) / next.getCurrentDistanceToCameraPlane() > 2.0d) {
                toBeRemoved.add(next);
            }
        }
        Iterator<Impostor> it = toBeRemoved.iterator();
        while (it.hasNext()) {
            it.next().clearData();
        }
        if (toBeRemoved.size() != 0) {
            toBeRemoved.clear();
            this.dirty = true;
        }
    }

    public void regenerateMeshColor(Impostor impostor) {
        this.vertexes.setColor(impostor.indexStart, impostor.currentColor, impostor.currentColor, impostor.currentColor, impostor.currentColor);
        this.vertexes.setColor(impostor.indexStart + 1, impostor.currentColor, impostor.currentColor, impostor.currentColor, impostor.currentColor);
        this.vertexes.setColor(impostor.indexStart + 2, impostor.currentColor, impostor.currentColor, impostor.currentColor, impostor.currentColor);
        this.vertexes.setColor(impostor.indexStart + 3, impostor.currentColor, impostor.currentColor, impostor.currentColor, impostor.currentColor);
    }

    public void setRecalculateCenter(boolean z) {
        this.recalculateCenter = z;
    }

    public int getImpostorsCount() {
        return this.impostors.size();
    }

    private void removeAllImpostors() {
        toBeRemoved.addAll(this.impostors);
        Iterator<Impostor> it = toBeRemoved.iterator();
        while (it.hasNext()) {
            it.next().clearData();
        }
        toBeRemoved.clear();
        this.impostors.clear();
    }

    /* renamed from: taikodom.render.impostor.ImpostorGroup$a */
    class C5144a implements Comparator<Impostor> {
        C5144a() {
        }

        /* renamed from: a */
        public int compare(Impostor impostor, Impostor impostor2) {
            if (impostor.getCurrentDistanceToCameraPlane() > impostor2.getCurrentDistanceToCameraPlane()) {
                return -1;
            }
            if (impostor.getCurrentDistanceToCameraPlane() < impostor2.getCurrentDistanceToCameraPlane()) {
                return 1;
            }
            return 0;
        }
    }
}
