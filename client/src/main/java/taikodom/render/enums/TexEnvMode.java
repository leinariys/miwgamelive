package taikodom.render.enums;


/* compiled from: a */
public enum TexEnvMode {
    MODULATE(8448),
    DECAL(8449),
    ADD(260),
    BLEND(3042),
    REPLACE(7681);

    private final int glEquivalent;

    private TexEnvMode(int i) {
        this.glEquivalent = i;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
