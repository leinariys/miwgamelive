package taikodom.render.enums;

/* compiled from: a */
public enum FogType {
    FOG_LINEAR(9729),
    FOG_EXP(2048),
    FOG_EXP2(2049);

    private final int glEquivalent;

    private FogType(int i) {
        this.glEquivalent = i;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
