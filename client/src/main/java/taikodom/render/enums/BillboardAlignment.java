package taikodom.render.enums;

/* compiled from: a */
public enum BillboardAlignment {
    VIEW_ALIGNED,
    AXIS_ALIGNED,
    SCREEN_ALIGNED,
    USER_ALIGNED
}
