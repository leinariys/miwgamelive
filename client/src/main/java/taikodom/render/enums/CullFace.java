package taikodom.render.enums;

/* compiled from: a */
public enum CullFace {
    FRONT(1028),
    BACK(1029),
    FRONT_AND_BACK(1032);

    private final int glEquivalent;

    private CullFace(int i) {
        this.glEquivalent = i;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
