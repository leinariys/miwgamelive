package taikodom.render.enums;

/* compiled from: a */
public enum TexMagFilter {
    NONE(0),
    NEAREST(9728),
    LINEAR(9729);

    private final int glEquivalent;

    private TexMagFilter(int i) {
        this.glEquivalent = i;
    }

    public int glEquivalent() {
        return this.glEquivalent;
    }
}
