package taikodom.render;

import taikodom.render.primitives.Billboard;
import taikodom.render.shaders.RenderStates;

import javax.media.opengl.GL;

/* compiled from: a */
public class BinkVideo implements Video {
    Billboard billboard = new Billboard();
    BinkTexture bink;
    RenderStates states = new RenderStates();
    float texAspectX;
    float texAspectY;

    public BinkVideo(BinkTexture binkTexture) {
        this.bink = binkTexture;
        this.texAspectX = ((float) binkTexture.getVideoSizeX()) / ((float) binkTexture.getSizeX());
        this.texAspectY = ((float) binkTexture.getVideoSizeY()) / ((float) binkTexture.getSizeY());
    }

    public void render(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        this.states.blindlyUpdateGLStates(drawContext);
        gl.glScalef((float) drawContext.getScreenWidth(), (float) drawContext.getScreenHeight(), 1.0f);
        this.bink.bind(drawContext);
        gl.glBegin(7);
        gl.glTexCoord2f(0.0f, this.texAspectY);
        gl.glVertex2f(0.0f, 1.0f);
        gl.glTexCoord2f(this.texAspectX, this.texAspectY);
        gl.glVertex2f(1.0f, 1.0f);
        gl.glTexCoord2f(this.texAspectX, 0.0f);
        gl.glVertex2f(1.0f, 0.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(0.0f, 0.0f);
        gl.glEnd();
        if (this.bink.isEnded()) {
            stop();
        }
    }

    public void update(DrawContext drawContext) {
    }

    public void stop() {
    }

    public void play() {
    }

    public boolean isEnded() {
        return this.bink.isEnded();
    }
}
