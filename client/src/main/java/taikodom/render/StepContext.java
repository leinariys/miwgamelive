package taikodom.render;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import taikodom.render.camera.Camera;

/* compiled from: a */
public class StepContext {
    public final Matrix3fWrap matrix3fTemp0 = new Matrix3fWrap();
    public final Matrix3fWrap matrix3fTemp1 = new Matrix3fWrap();
    public final Matrix3fWrap matrix3fTemp2 = new Matrix3fWrap();
    public final Matrix4fWrap matrix4fTemp0 = new Matrix4fWrap();
    public final Matrix4fWrap matrix4fTemp1 = new Matrix4fWrap();
    public final Matrix4fWrap matrix4fTemp2 = new Matrix4fWrap();
    public final Vec3d vec3dTemp0 = new Vec3d();
    public final Vec3d vec3dTemp1 = new Vec3d();
    public final Vec3d vec3dTemp2 = new Vec3d();
    public final Vec3f vec3ftemp0 = new Vec3f();
    public final Vec3f vec3ftemp1 = new Vec3f();
    public final Vec3f vec3ftemp2 = new Vec3f();
    private Camera camera;
    private float deltaTimeInSeconds;
    private long numObjectsStepped;
    private long numParticleSystemStepped;

    public Camera getCamera() {
        return this.camera;
    }

    public void setCamera(Camera camera2) {
        this.camera = camera2;
    }

    public float getDeltaTime() {
        return this.deltaTimeInSeconds;
    }

    public void setDeltaTime(float f) {
        this.deltaTimeInSeconds = f;
    }

    public void incObjectSteps(long j) {
        this.numObjectsStepped += j;
    }

    public void incParticleSystemStepped() {
        this.numParticleSystemStepped++;
    }

    public long getNumObjectsStepped() {
        return this.numObjectsStepped;
    }

    public void reset() {
        this.numObjectsStepped = 0;
        this.deltaTimeInSeconds = 0.0f;
        this.numParticleSystemStepped = 0;
    }

    public boolean getEditMode() {
        return false;
    }

    public long getNumParticleSystemStepped() {
        return this.numParticleSystemStepped;
    }
}
