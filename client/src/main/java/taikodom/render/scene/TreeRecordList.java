package taikodom.render.scene;

import taikodom.render.enums.BlendType;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.shaders.RenderStates;
import taikodom.render.shaders.ShaderPass;

import java.util.*;

/* compiled from: a */
public class TreeRecordList {
    private List<TreeRecord> data = new ArrayList();
    private LinkedList<TreeRecord> poolData = new LinkedList<>();

    public TreeRecord add() {
        if (this.poolData.size() > 0) {
            TreeRecord removeLast = this.poolData.removeLast();
            this.data.add(removeLast);
            return removeLast;
        }
        TreeRecord treeRecord = new TreeRecord();
        this.data.add(treeRecord);
        return treeRecord;
    }

    public void clear() {
        this.poolData.addAll(this.data);
        this.data.clear();
    }

    public TreeRecord get(int i) {
        return this.data.get(i);
    }

    public int size() {
        return this.data.size();
    }

    public void sort() {
        Collections.sort(this.data, new C5190a());
    }

    /* renamed from: taikodom.render.scene.TreeRecordList$a */
    class C5190a implements Comparator<TreeRecord> {
        C5190a() {
        }

        /* renamed from: a */
        public int compare(TreeRecord treeRecord, TreeRecord treeRecord2) {
            if ((treeRecord.entryPriority > 0 || treeRecord2.entryPriority > 0) && treeRecord.entryPriority != treeRecord2.entryPriority) {
                return treeRecord2.entryPriority - treeRecord.entryPriority;
            }
            ShaderPass pass = treeRecord.shader.getPass(0);
            ShaderPass pass2 = treeRecord2.shader.getPass(0);
            RenderStates renderStates = pass.getRenderStates();
            RenderStates renderStates2 = pass2.getRenderStates();
            if (treeRecord.shader.getHandle() == treeRecord2.shader.getHandle()) {
                if (renderStates.isBlendEnabled()) {
                    return (int) (treeRecord2.squaredDistanceToCamera - treeRecord.squaredDistanceToCamera);
                }
                if (treeRecord.entryPriority != treeRecord2.entryPriority) {
                    return treeRecord2.entryPriority - treeRecord.entryPriority;
                }
                if (treeRecord.material.getHandle() == treeRecord2.material.getHandle()) {
                    if (treeRecord.primitive.getHandle() == treeRecord2.primitive.getHandle()) {
                        return (int) (treeRecord.squaredDistanceToCamera - treeRecord2.squaredDistanceToCamera);
                    }
                    if (treeRecord.primitive.getHandle() <= treeRecord2.primitive.getHandle()) {
                        return -1;
                    }
                    return 1;
                } else if (treeRecord.material.getHandle() <= treeRecord2.material.getHandle()) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (!renderStates.isBlendEnabled() || !renderStates2.isBlendEnabled()) {
                if (renderStates.isBlendEnabled()) {
                    return 1;
                }
                if (renderStates2.isBlendEnabled()) {
                    return -1;
                }
                if (treeRecord.shader.getPriority() != treeRecord2.shader.getPriority()) {
                    return treeRecord2.shader.getPriority() - treeRecord.shader.getPriority();
                }
                if (treeRecord.entryPriority != treeRecord2.entryPriority) {
                    return treeRecord2.entryPriority - treeRecord.entryPriority;
                }
                if (treeRecord.shader.getHandle() <= treeRecord2.shader.getHandle()) {
                    return -1;
                }
                return 1;
            } else if (treeRecord.entryPriority != treeRecord2.entryPriority) {
                return treeRecord2.entryPriority - treeRecord.entryPriority;
            } else {
                if (renderStates.isDepthMask() && !renderStates2.isDepthMask()) {
                    return -1;
                }
                if (renderStates2.isDepthMask() && !renderStates.isDepthMask()) {
                    return 1;
                }
                if (renderStates.getDepthRangeMin() != renderStates2.getDepthRangeMin()) {
                    return (int) ((renderStates2.getDepthRangeMin() * 1000.0f) - (renderStates.getDepthRangeMin() * 1000.0f));
                }
                if (renderStates.getDstBlend() != renderStates2.getDstBlend()) {
                    if (renderStates.getDstBlend() == BlendType.ONE_MINUS_SRC_ALPHA) {
                        return -1;
                    }
                    if (renderStates2.getDstBlend() == BlendType.ONE_MINUS_SRC_ALPHA) {
                        return 1;
                    }
                }
                return (int) (treeRecord2.squaredDistanceToCamera - treeRecord.squaredDistanceToCamera);
            }
        }
    }
}
