package taikodom.render.scene;

import game.geometry.C1085Pt;
import taikodom.render.enums.LightType;
import taikodom.render.primitives.Light;

import java.util.*;

/* compiled from: a */
public class LightRecordList {
    private List<LightRecord> data = new ArrayList();
    private LinkedList<LightRecord> poolData = new LinkedList<>();

    public LightRecord add() {
        if (this.poolData.size() > 0) {
            LightRecord removeLast = this.poolData.removeLast();
            this.data.add(removeLast);
            return removeLast;
        }
        LightRecord lightRecord = new LightRecord();
        this.data.add(lightRecord);
        return lightRecord;
    }

    public void clear() {
        this.poolData.addAll(this.data);
        this.data.clear();
    }

    public LightRecord get(int i) {
        return this.data.get(i);
    }

    public int size() {
        return this.data.size();
    }

    public void sort() {
        Collections.sort(this.data, new C5152a());
    }

    /* compiled from: a */
    public class LightRecord {
        public final C1085Pt cameraSpaceBoundingSphere = new C1085Pt();
        public Light light;

        public LightRecord() {
        }
    }

    /* renamed from: taikodom.render.scene.LightRecordList$a */
    class C5152a implements Comparator<LightRecord> {
        C5152a() {
        }

        /* renamed from: a */
        public int compare(LightRecord lightRecord, LightRecord lightRecord2) {
            Light light = lightRecord.light;
            Light light2 = lightRecord2.light;
            if (light.getLightType() == LightType.DIRECTIONAL_LIGHT) {
                return -1;
            }
            if (light2.getLightType() == LightType.DIRECTIONAL_LIGHT) {
                return 1;
            }
            if (light.getLightType() == LightType.SPOT_LIGHT) {
                return 1;
            }
            if (light2.getLightType() == LightType.SPOT_LIGHT) {
                return -1;
            }
            if (light.getLightType() != LightType.POINT_LIGHT || light2.getLightType() != LightType.POINT_LIGHT) {
                return 0;
            }
            if (light.getCurrentDistanceToCamera() - ((double) light.getRadius()) > light2.getCurrentDistanceToCamera() - ((double) light2.getRadius())) {
                return 1;
            }
            return -1;
        }
    }
}
