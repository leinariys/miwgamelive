package taikodom.render.scene;

/* renamed from: a.uv */
/* compiled from: a */
public class C3778uv {
    private int bvY;
    private int bvZ;
    private int bwa;

    public C3778uv(int i) {
        mo22467eP(i);
    }

    /* renamed from: eP */
    public void mo22467eP(int i) {
        this.bvY = -1791900999;
        this.bvZ = 1214946999;
        this.bwa = i;
    }

    /* access modifiers changed from: package-private */
    public short ajo() {
        this.bwa = (this.bwa * this.bvY) + this.bvZ;
        return (short) (this.bwa >> 8);
    }

    /* renamed from: j */
    public float mo22468j(float f, float f2) {
        return (((float) ajo()) * 1.525902E-5f * (f2 - f)) + f;
    }
}
