package taikodom.render.scene;

import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.OcclusionQuery;

/* compiled from: a */
public class RNuclearLight extends RenderObject {
    public RenderObject defaultObject;
    public RBillboard nuclearBillboard;
    public RBillboard objectBillboard;
    public float occlusionRatio;

    public RNuclearLight() {
        this.occlusionRatio = 1.0f;
    }

    public RNuclearLight(RNuclearLight rNuclearLight) {
        super(rNuclearLight);
        this.occlusionRatio = rNuclearLight.occlusionRatio;
        if (rNuclearLight.defaultObject != null) {
            this.defaultObject = (RenderObject) smartClone(rNuclearLight.defaultObject);
        }
        if (rNuclearLight.nuclearBillboard != null) {
            this.nuclearBillboard = (RBillboard) smartClone(rNuclearLight.nuclearBillboard);
        }
        if (rNuclearLight.objectBillboard != null) {
            this.objectBillboard = (RBillboard) smartClone(rNuclearLight.objectBillboard);
        }
    }

    public RenderObject getDefaultObject() {
        return this.defaultObject;
    }

    public void setDefaultObject(RenderObject renderObject) {
        this.defaultObject = renderObject;
    }

    public RBillboard getNuclearBillboard() {
        return this.nuclearBillboard;
    }

    public void setNuclearBillboard(RBillboard rBillboard) {
        this.nuclearBillboard = rBillboard;
    }

    public RBillboard getObjectBillboard() {
        return this.objectBillboard;
    }

    public void setObjectBillboard(RBillboard rBillboard) {
        this.objectBillboard = rBillboard;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        float f = 0.0f;
        if (!this.render) {
            return false;
        }
        if (!super.render(renderContext, z)) {
            return false;
        }
        if (this.objectBillboard == null) {
            return true;
        }
        this.objectBillboard.render(renderContext, z);
        if (this.defaultObject != null) {
            this.defaultObject.render(renderContext, z);
        }
        if (this.nuclearBillboard != null && renderContext.getSceneViewQuality().getEnvFxQuality() <= 1) {
            OcclusionQuery occlusionQuery = this.objectBillboard.getOcclusionQuery();
            if (occlusionQuery == null) {
                this.objectBillboard.setOcclusionQueryEnabled(true);
            } else {
                float length = this.objectBillboard.getSize().length();
                renderContext.vec3dTemp0.sub(renderContext.getCamera().getPosition(), this.objectBillboard.getGlobalTransform().position);
                float screenHeight = (length * (((float) renderContext.getDc().getScreenHeight()) / (((float) renderContext.vec3dTemp0.length()) * (renderContext.getCamera().getTanHalfFovY() * 2.0f)))) / 1.414f;
                float result = occlusionQuery.getResult(screenHeight * screenHeight * this.occlusionRatio, renderContext.getDc().getGl());
                renderContext.vec3dTemp0.normalize();
                renderContext.vec3fTemp0.set(renderContext.vec3dTemp0);
                renderContext.getCamera().getTransform().getZ(renderContext.vec3fTemp1);
                renderContext.vec3dTemp1.negate();
                float dot = renderContext.vec3fTemp0.dot(renderContext.vec3fTemp1);
                float sin = (float) Math.sin((((double) (renderContext.getCamera().getFovY() / 2.0f)) * 3.141592653589793d) / 180.0d);
                float f2 = result * ((dot - sin) / (1.0f - sin));
                if (f2 >= 0.0f) {
                    if (f2 > 1.0f) {
                        f = 1.0f;
                    } else {
                        f = f2;
                    }
                }
                this.nuclearBillboard.setPrimitiveColor(f, f, f, 1.0f);
                this.nuclearBillboard.render(renderContext, z);
            }
        }
        renderContext.getCurrentScene().addColorIntensity(f);
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RNuclearLight(this);
    }

    public float getOcclusionRatio() {
        return this.occlusionRatio;
    }

    public void setOcclusionRatio(float f) {
        this.occlusionRatio = f;
    }

    public aLH computeWorldSpaceAABB() {
        this.aabbWorldSpace.reset();
        if (this.defaultObject != null) {
            this.defaultObject.updateInternalGeometry(this);
            this.aabbWorldSpace.mo9868h(this.defaultObject.getAabbWorldSpace());
        }
        if (this.objectBillboard != null) {
            this.objectBillboard.updateInternalGeometry(this);
            this.aabbWorldSpace.mo9868h(this.objectBillboard.getAabbWorldSpace());
        }
        if (this.nuclearBillboard != null) {
            this.nuclearBillboard.updateInternalGeometry(this);
            this.aabbWorldSpace.mo9868h(this.nuclearBillboard.getAabbWorldSpace());
        }
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }
}
