package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.*;
import gnu.trove.THashMap;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.NotExported;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.helpers.RenderVisitor;
import taikodom.render.impostor.Impostor;
import taikodom.render.loader.RenderAsset;
import taikodom.render.partitioning.RenderZone;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;

import java.util.*;

/* compiled from: a */
public class SceneObject extends RenderAsset {
    public static final int MIN_AABB_WS_LENGHT = 5;
    public final aLH aabbWorldSpace = new aLH();
    public final Vec3f angularVelocity = new Vec3f(0.0f, 0.0f, 0.0f);
    public final Vec3f desiredSize = new Vec3f(0.0f, 0.0f, 0.0f);
    public final TransformWrap globalTransform = new TransformWrap();
    public final aLH localAabb = new aLH();
    public final List<SceneObject> objects = new ArrayList();
    public final TransformWrap parentTransform = new TransformWrap();
    public final Vec3f scaling = new Vec3f(1.0f, 1.0f, 1.0f);
    public final TransformWrap transform = new TransformWrap();
    public final Vec3f velocity = new Vec3f(0.0f, 0.0f, 0.0f);
    public double aabbLSLenght;
    public double aabbWSLenght;
    public boolean allowSelection = true;
    public boolean canRemoveChildren = true;
    public Map<Object, Object> clientProperties;
    public double currentSquaredDistanceToCamera = ScriptRuntime.NaN;
    public boolean disposeWhenEmpty = false;
    public boolean disposed = false;
    public boolean firstFrame = true;
    public double maxSquaredVisibleDistance;
    public double maxVisibleDistance;
    public boolean rayTraceable;
    public boolean render = true;
    public int renderPriority = 0;
    RenderZone renderZone;
    private float impostorBlendingDistance;
    private Impostor impostorData;
    private double impostorDistance;
    private boolean impostorEnabled = false;
    private List<SceneObject> postPonedRemovalList = new ArrayList();

    public SceneObject() {
        this.localAabb.mo9825B(-0.5d, -0.5d, -0.5d);
        this.localAabb.mo9824A(0.5d, 0.5d, 0.5d);
        setMaxVisibleDistance(-1.0d);
    }

    public SceneObject(SceneObject sceneObject) {
        super(sceneObject);
        this.localAabb.mo9867g(sceneObject.localAabb);
        this.aabbLSLenght = sceneObject.aabbLSLenght;
        this.aabbWorldSpace.mo9867g(sceneObject.aabbWorldSpace);
        this.aabbWSLenght = sceneObject.aabbWSLenght;
        this.disposeWhenEmpty = sceneObject.disposeWhenEmpty;
        this.impostorDistance = sceneObject.impostorDistance;
        setImpostorEnabled(sceneObject.impostorEnabled);
        this.impostorBlendingDistance = sceneObject.impostorBlendingDistance;
        for (SceneObject smartClone : sceneObject.objects) {
            this.objects.add((SceneObject) smartClone(smartClone));
        }
        this.name = sceneObject.name;
        this.transform.mo17344b(sceneObject.transform);
        this.velocity.set(sceneObject.velocity);
        this.disposed = sceneObject.disposed;
        this.desiredSize.set(sceneObject.desiredSize);
        this.render = sceneObject.render;
        this.allowSelection = sceneObject.allowSelection;
        this.renderPriority = sceneObject.renderPriority;
        this.rayTraceable = sceneObject.rayTraceable;
        setMaxVisibleDistance(sceneObject.maxVisibleDistance);
        this.scaling.set(sceneObject.scaling);
    }

    /* access modifiers changed from: protected */
    public <T> T smartClone(T t) {
        if (t instanceof RenderAsset) {
            return (T) ((RenderAsset) t).cloneAsset();
        }
        return t;
    }

    public RenderAsset cloneAsset() {
        return new SceneObject(this);
    }

    public TransformWrap getTransform() {
        return this.transform;
    }

    public void setTransform(TransformWrap bcVar) {
        this.transform.mo17344b(bcVar);
        updateInternalGeometry((SceneObject) null);
    }

    @NotExported
    public void setTransform(Matrix4fWrap ajk) {
        this.transform.set(ajk);
        updateInternalGeometry((SceneObject) null);
    }

    public TransformWrap getGlobalTransform() {
        return this.globalTransform;
    }

    public Vec3f getVelocity() {
        return this.velocity;
    }

    public void setVelocity(Vec3f vec3f) {
        this.velocity.set(vec3f);
    }

    public void setVelocity(float f, float f2, float f3) {
        this.velocity.set(f, f2, f3);
    }

    public boolean isDisposed() {
        return this.disposed;
    }

    public Vec3f getDesiredSize() {
        return this.desiredSize;
    }

    public void setDesiredSize(Vec3f vec3f) {
        this.desiredSize.set(vec3f);
    }

    public boolean isRender() {
        return this.render;
    }

    public void setRender(boolean z) {
        this.render = z;
    }

    public boolean isVisible() {
        return this.render;
    }

    public void setVisible(boolean z) {
        this.render = z;
    }

    public boolean isAllowSelection() {
        return this.allowSelection;
    }

    public void setAllowSelection(boolean z) {
        this.allowSelection = z;
    }

    public int getRenderPriority() {
        return this.renderPriority;
    }

    public void setRenderPriority(int i) {
        this.renderPriority = i;
    }

    public boolean isRayTraceable() {
        return this.rayTraceable;
    }

    public void setRayTraceable(boolean z) {
        this.rayTraceable = z;
    }

    @NotExported
    public void setPosition(double d, double d2, double d3) {
        if (Double.isInfinite(d) || Double.isNaN(d) || Double.isInfinite(d2) || Double.isNaN(d2) || Double.isInfinite(d3) || Double.isNaN(d3)) {
            throw new RuntimeException("Not a valid position for " + getName());
        }
        this.transform.setTranslation(d, d2, d3);
        updateInternalGeometry((SceneObject) null);
    }

    public void setPosition(float f, float f2, float f3) {
        if (Float.isInfinite(f) || Float.isNaN(f) || Float.isInfinite(f2) || Float.isNaN(f2) || Float.isInfinite(f3) || Float.isNaN(f3)) {
            throw new RuntimeException("Not a valid position for " + getName());
        }
        this.transform.setTranslation((double) f, (double) f2, (double) f3);
        updateInternalGeometry((SceneObject) null);
    }

    public void setTransform(Vec3f vec3f, Quat4fWrap aoy) {
        this.transform.position.mo9497bG(vec3f);
        this.transform.orientation.set(aoy);
        updateInternalGeometry((SceneObject) null);
    }

    public void setTransform(Vec3d ajr, Quat4fWrap aoy) {
        if (Double.isInfinite(ajr.x) || Double.isNaN(ajr.x) || Double.isInfinite(ajr.y) || Double.isNaN(ajr.y) || Double.isInfinite(ajr.z) || Double.isNaN(ajr.z)) {
            throw new RuntimeException("Not a valid position for " + getName());
        }
        this.transform.position.mo9484aA(ajr);
        this.transform.orientation.set(aoy);
        updateInternalGeometry((SceneObject) null);
    }

    /* access modifiers changed from: package-private */
    public void setTransform(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        this.transform.setX(vec3f);
        this.transform.setY(vec3f2);
        this.transform.setZ(vec3f3);
        this.transform.position.mo9497bG(vec3f4);
        updateInternalGeometry((SceneObject) null);
    }

    public void setOrientation(Quat4fWrap aoy) {
        this.transform.setOrientation(aoy);
        updateInternalGeometry((SceneObject) null);
    }

    public void reset() {
    }

    public void show() {
        this.render = true;
    }

    public void hide() {
        this.render = false;
    }

    public int step(StepContext stepContext) {
        return 0;
    }

    public boolean needStep() {
        return true;
    }

    public void fillRenderQueue(RenderContext renderContext, boolean z) {
        this.currentSquaredDistanceToCamera = this.globalTransform.mo17352g(renderContext.getCamera().globalTransform);
        if (isImpostorEnabled() && renderContext.isAllowImpostors()) {
            if (this.currentSquaredDistanceToCamera > this.impostorData.getImpostorDistance() * this.impostorData.getImpostorDistance()) {
                if (z && !renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
                    return;
                }
                if (this.impostorData.isValid()) {
                    this.impostorData.updateImpostor(renderContext, this);
                    float sqrt = ((float) (Math.sqrt(this.currentSquaredDistanceToCamera) - this.impostorData.getImpostorDistance())) / this.impostorBlendingDistance;
                    if (sqrt > 1.0f) {
                        sqrt = 1.0f;
                    }
                    this.impostorData.setCurrentColor(sqrt);
                    if (sqrt < 1.0f) {
                        render(renderContext, false);
                        this.impostorData.setTransition(true);
                        return;
                    }
                    return;
                }
                renderContext.addImpostor(this);
                float sqrt2 = ((float) (Math.sqrt(this.currentSquaredDistanceToCamera) - this.impostorData.getImpostorDistance())) / this.impostorBlendingDistance;
                if (sqrt2 > 1.0f) {
                    sqrt2 = 1.0f;
                }
                this.impostorData.setCurrentColor(sqrt2);
                if (sqrt2 < 1.0f) {
                    render(renderContext, false);
                    return;
                }
                return;
            } else if (this.impostorData.isValid()) {
                this.impostorData.clearData();
            }
        }
        render(renderContext, z);
    }

    public void processRender(RenderContext renderContext, boolean z) {
        setCanRemoveChildren(false);
        synchronized (this.objects) {
            int i = 0;
            for (SceneObject next : this.objects) {
                if (next != null) {
                    if (next.isDisposed()) {
                        removeChild(next);
                    } else {
                        next.fillRenderQueue(renderContext, z);
                    }
                    i++;
                } else {
                    setCanRemoveChildren(true);
                    throw new RuntimeException("Null child " + i + " in " + this.name);
                }
            }
            setCanRemoveChildren(true);
        }
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render) {
            return false;
        }
        if (this.maxVisibleDistance > ScriptRuntime.NaN && this.currentSquaredDistanceToCamera > this.maxSquaredVisibleDistance) {
            return false;
        }
        if (this.objects.size() > 0) {
            processRender(renderContext, z);
        }
        return true;
    }

    public Vec3f getScaling() {
        return this.scaling;
    }

    public void setScaling(Vec3f vec3f) {
        this.scaling.set(vec3f);
        updateInternalGeometry((SceneObject) null);
    }

    public void setScaling(float f) {
        setScaling(new Vec3f(f, f, f));
    }

    public Vec3d getPosition() {
        return this.transform.position;
    }

    public void setPosition(Vec3d ajr) {
        if (Double.isInfinite(ajr.x) || Double.isNaN(ajr.x) || Double.isInfinite(ajr.y) || Double.isNaN(ajr.y) || Double.isInfinite(ajr.z) || Double.isNaN(ajr.z)) {
            throw new RuntimeException("Not a valid position for " + getName());
        }
        this.transform.setTranslation(ajr);
        updateInternalGeometry((SceneObject) null);
    }

    @NotExported
    public void setPosition(Vec3f vec3f) {
        if (Float.isInfinite(vec3f.x) || Float.isNaN(vec3f.x) || Float.isInfinite(vec3f.y) || Float.isNaN(vec3f.y) || Float.isInfinite(vec3f.z) || Float.isNaN(vec3f.z)) {
            throw new RuntimeException("Not a valid position for " + getName());
        }
        this.transform.setTranslation(vec3f);
        updateInternalGeometry((SceneObject) null);
    }

    public Vec3f getAngularVelocity() {
        return this.angularVelocity;
    }

    public void setAngularVelocity(Vec3f vec3f) {
        this.angularVelocity.set(vec3f);
    }

    public void updateInternalGeometry(SceneObject sceneObject) {
        if (sceneObject != null) {
            this.parentTransform.mo17344b(sceneObject.getGlobalTransform());
            this.globalTransform.mo17337a(this.parentTransform, this.transform);
        } else {
            this.globalTransform.mo17337a(this.parentTransform, this.transform);
        }
        if (this.scaling.x != 1.0f) {
            this.scaling.x = this.scaling.x;
        }
        this.globalTransform.mo17324B(this.scaling.x);
        this.globalTransform.mo17325C(this.scaling.y);
        this.globalTransform.mo17326D(this.scaling.z);
        int childCount = childCount();
        if (childCount > 0) {
            setCanRemoveChildren(false);
            for (int i = 0; i < childCount; i++) {
                SceneObject child = getChild(i);
                if (child == null || child.isDisposed()) {
                    removeChild(child);
                } else {
                    child.updateInternalGeometry(this);
                }
            }
            setCanRemoveChildren(true);
        }
        computeWorldSpaceAABB();
        if (this.renderZone != null) {
            this.renderZone.onObjectTransform(this);
        }
    }

    public aLH computeWorldSpaceAABB() {
        this.aabbWorldSpace.reset();
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        boolean z;
        this.localAabb.reset();
        if (childCount() > 0) {
            aLH alh = new aLH();
            z = true;
            for (SceneObject next : this.objects) {
                if (next.isRender() && ((next instanceof RGroup) || (next instanceof RLod) || (next instanceof RMesh) || (next instanceof RModel) || (next instanceof RShape) || (next instanceof RParticleSystem) || (next instanceof RBillboard))) {
                    aLH computeLocalSpaceAABB = next.computeLocalSpaceAABB();
                    if (computeLocalSpaceAABB.dim().x <= computeLocalSpaceAABB.din().x) {
                        computeLocalSpaceAABB.mo9834a(next.getTransform(), alh);
                        this.localAabb.mo9842aG(alh.dim());
                        this.localAabb.mo9842aG(alh.din());
                        z = false;
                    }
                }
            }
        } else {
            z = true;
        }
        if (z) {
            this.localAabb.mo9826C(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        }
        this.aabbLSLenght = this.localAabb.din().mo9491ax(this.localAabb.dim());
        return this.localAabb;
    }

    public aLH getAABB() {
        return this.localAabb;
    }

    /* access modifiers changed from: protected */
    public void addChildrenWorldSpaceAABB() {
        int childCount = childCount();
        for (int i = 0; i < childCount; i++) {
            SceneObject child = getChild(i);
            if ((child instanceof RGroup) || (child instanceof RLod) || (child instanceof RMesh) || (child instanceof RModel) || (child instanceof RShape) || (child instanceof RBillboard)) {
                this.aabbWorldSpace.mo9868h(child.getAabbWorldSpace());
            }
        }
    }

    public void removeChild(SceneObject sceneObject) {
        if (!this.canRemoveChildren) {
            this.postPonedRemovalList.add(sceneObject);
            return;
        }
        if (this.objects.remove(sceneObject) && sceneObject != null) {
            sceneObject.onRemove();
        }
        checkEmpty();
    }

    public void removeAllChildren() {
        if (!this.canRemoveChildren) {
            this.postPonedRemovalList.addAll(this.objects);
            return;
        }
        for (SceneObject onRemove : this.objects) {
            onRemove.onRemove();
        }
        this.objects.clear();
        checkEmpty();
    }

    public void addChild(SceneObject sceneObject) {
        if (sceneObject == null) {
            System.out.println("Trying to add a NULL child in object " + this.name);
        }
        this.objects.add(sceneObject);
        updateInternalGeometry((SceneObject) null);
        computeLocalSpaceAABB();
        if (this.renderZone != null && sceneObject != null) {
            sceneObject.setRenderZone(this.renderZone);
            this.renderZone.findStepableObjects(sceneObject);
        }
    }

    public SceneObject getChild(int i) {
        return this.objects.get(i);
    }

    public void setChild(int i, SceneObject sceneObject) {
        addChild(sceneObject);
        updateInternalGeometry((SceneObject) null);
        if (this.renderZone != null && sceneObject != null) {
            sceneObject.setRenderZone(this.renderZone);
            this.renderZone.findStepableObjects(sceneObject);
        }
    }

    public List<SceneObject> getChildren() {
        return this.objects;
    }

    public int childCount() {
        return this.objects.size();
    }

    public aLH getAabbWorldSpace() {
        return this.aabbWorldSpace;
    }

    public boolean isNeedToStep() {
        return false;
    }

    public RenderZone getRenderZone() {
        return this.renderZone;
    }

    public void setRenderZone(RenderZone renderZone2) {
        this.renderZone = renderZone2;
    }

    public void dispose() {
        this.disposed = true;
        if (this.impostorData != null) {
            this.impostorData.clearData();
            this.impostorData = null;
        }
    }

    public void releaseReferences() {
        super.releaseReferences();
        if (this.impostorData != null) {
            this.impostorData.clearData();
            this.impostorData = null;
        }
    }

    public void onRemove() {
        if (this.renderZone != null) {
            this.renderZone.removeChild(this);
            this.renderZone = null;
        }
        if (this.impostorData != null) {
            this.impostorData.clearData();
            this.impostorData = null;
        }
        for (SceneObject onRemove : this.objects) {
            onRemove.onRemove();
        }
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD rayTraceSceneObjectInfoD, boolean z, boolean z2) {
        rayTraceSceneObjectInfoD.setChanged(false);
        if (z2) {
            synchronized (this.objects) {
                Iterator<SceneObject> it = this.objects.iterator();
                while (true) {
                    if (it.hasNext()) {
                        it.next().rayIntersects(rayTraceSceneObjectInfoD, z, z2);
                        if (rayTraceSceneObjectInfoD.isIntersected() && rayTraceSceneObjectInfoD.isChanged()) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        return rayTraceSceneObjectInfoD;
    }

    public boolean isCanRemoveChildren() {
        return this.canRemoveChildren;
    }

    public void setCanRemoveChildren(boolean z) {
        if (!z) {
            this.canRemoveChildren = false;
            return;
        }
        this.canRemoveChildren = true;
        if (this.postPonedRemovalList.size() > 0) {
            for (SceneObject removeChild : this.postPonedRemovalList) {
                removeChild(removeChild);
            }
            this.postPonedRemovalList.clear();
        }
    }

    public SceneObject findChildByName(String str) {
        for (SceneObject next : this.objects) {
            if (next.name.equals(str)) {
                return next;
            }
        }
        return null;
    }

    public boolean hasChild(SceneObject sceneObject) {
        for (SceneObject sceneObject2 : this.objects) {
            if (sceneObject2 == sceneObject) {
                return true;
            }
        }
        return false;
    }

    public void resetRotation() {
        this.transform.orientation.setIdentity();
        updateInternalGeometry((SceneObject) null);
    }

    public void resetPosition() {
        this.transform.position.set(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        updateInternalGeometry((SceneObject) null);
    }

    public boolean isImpostorEnabled() {
        return this.impostorEnabled;
    }

    public void setImpostorEnabled(boolean z) {
        this.impostorEnabled = z;
        if (z && this.impostorData == null) {
            this.impostorData = new Impostor();
            this.impostorData.setImpostorDistance(this.impostorDistance);
        }
    }

    public double getImpostorDistance() {
        return this.impostorDistance;
    }

    public void setImpostorDistance(double d) {
        this.impostorDistance = d;
        if (this.impostorData != null) {
            this.impostorData.setImpostorDistance(d);
        }
    }

    public float getImpostorBlendingDistance() {
        return this.impostorBlendingDistance;
    }

    public void setImpostorBlendingDistance(float f) {
        this.impostorBlendingDistance = f;
    }

    public void getTransformWithNoScale(TransformWrap bcVar) {
        bcVar.mo17344b(this.transform);
    }

    public double getMaxVisibleDistance() {
        return this.maxVisibleDistance;
    }

    public void setMaxVisibleDistance(double d) {
        this.maxVisibleDistance = d;
        this.maxSquaredVisibleDistance = d * d;
        if (d < ScriptRuntime.NaN) {
            this.maxSquaredVisibleDistance = d;
        }
    }

    public boolean isDisposeWhenEmpty() {
        return this.disposeWhenEmpty;
    }

    public void setDisposeWhenEmpty(boolean z) {
        this.disposeWhenEmpty = z;
    }

    public void checkEmpty() {
        if (this.objects.size() == 0 && this.disposeWhenEmpty) {
            dispose();
        }
    }

    public boolean visitPreOrder(RenderVisitor renderVisitor) {
        if (!renderVisitor.visit(this)) {
            return false;
        }
        for (SceneObject next : this.objects) {
            next.setRenderZone(this.renderZone);
            if (!next.visitPreOrder(renderVisitor)) {
                return false;
            }
        }
        return true;
    }

    public Impostor getImpostor() {
        return this.impostorData;
    }

    public double getAabbWSLenght() {
        return this.aabbWSLenght;
    }

    public double getAabbLSLenght() {
        return this.aabbLSLenght;
    }

    public double getCurrentSquaredDistanceToCamera() {
        return this.currentSquaredDistanceToCamera;
    }

    public <T> T putClientProperty(Object obj, T t) {
        if (this.clientProperties == null) {
            if (t == null) {
                return null;
            }
            this.clientProperties = Collections.synchronizedMap(new THashMap());
        }
        if (t == null) {
            return (T) this.clientProperties.remove(obj);
        }
        return (T) this.clientProperties.put(obj, t);
    }

    public <T> T getClientProperty(Object obj) {
        if (this.clientProperties == null) {
            return null;
        }
        return (T) this.clientProperties.get(obj);
    }
}
