package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;

/* compiled from: a */
public class RayBB extends RenderObject {
    RBillboard billboard;
    float elapsedTime;
    float lifeTime;
    float range;

    public RayBB(RBillboard rBillboard, float f, float f2) {
        this.billboard = rBillboard;
        this.lifeTime = f;
        this.range = f2;
    }

    public aLH buildAABB() {
        this.billboard.computeWorldSpaceAABB();
        return this.billboard.getTransformedAABB();
    }

    public void dispose() {
        this.billboard = null;
    }

    public int step(StepContext stepContext) {
        if (this.billboard == null) {
            return 1;
        }
        this.billboard.step(stepContext);
        return 1;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (this.billboard == null) {
            return true;
        }
        this.billboard.render(renderContext, z);
        return true;
    }

    public void setRay(RBillboard rBillboard, float f) {
        this.billboard = rBillboard;
        this.lifeTime = f;
    }

    /* access modifiers changed from: package-private */
    public float getLifeTime() {
        return this.lifeTime;
    }

    /* access modifiers changed from: package-private */
    public float getElapsedTime() {
        return this.elapsedTime;
    }

    /* access modifiers changed from: package-private */
    public void decLifeTime(float f) {
        this.elapsedTime += f;
        Color primitiveColor = this.billboard.getPrimitiveColor();
        float f2 = 1.0f - (this.elapsedTime / this.lifeTime);
        primitiveColor.w = f2;
        primitiveColor.x = f2;
        primitiveColor.y = f2;
        primitiveColor.z = f2;
        this.billboard.setPrimitiveColor(primitiveColor);
    }

    /* access modifiers changed from: package-private */
    public Color getColor() {
        return this.billboard.getPrimitiveColor();
    }

    /* access modifiers changed from: package-private */
    public void setColor(Color color) {
        this.billboard.setPrimitiveColor(color);
    }

    public void trigger(Vec3d ajr, Vec3f vec3f, float f) {
        throw new RuntimeException("Must implement this");
    }
}
