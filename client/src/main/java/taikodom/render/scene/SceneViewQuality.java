package taikodom.render.scene;

/* compiled from: a */
public class SceneViewQuality {
    private int envFxQuality = 0;
    private float lodQuality = 0.0f;
    private boolean postProcessingFX = true;
    private int shaderQuality = 0;

    public SceneViewQuality() {
    }

    public SceneViewQuality(SceneViewQuality sceneViewQuality) {
        set(sceneViewQuality);
    }

    public void set(SceneViewQuality sceneViewQuality) {
        this.lodQuality = sceneViewQuality.lodQuality;
        this.envFxQuality = sceneViewQuality.envFxQuality;
        this.shaderQuality = sceneViewQuality.shaderQuality;
        this.postProcessingFX = sceneViewQuality.postProcessingFX;
    }

    public float getLodQuality() {
        return this.lodQuality;
    }

    public void setLodQuality(float f) {
        this.lodQuality = f;
    }

    public int getEnvFxQuality() {
        return this.envFxQuality;
    }

    public void setEnvFxQuality(int i) {
        this.envFxQuality = i;
    }

    public int getShaderQuality() {
        return this.shaderQuality;
    }

    public void setShaderQuality(int i) {
        this.shaderQuality = i;
    }

    public boolean isPostProcessingFX() {
        return this.postProcessingFX;
    }

    public void setPostProcessingFX(boolean z) {
        this.postProcessingFX = z;
    }
}
