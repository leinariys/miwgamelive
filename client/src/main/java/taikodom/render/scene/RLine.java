package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;

/* compiled from: a */
public class RLine extends RenderObject {
    private static final Vec3f firstPoint = new Vec3f();

    /* renamed from: p1 */
    private static final Vec3f f10363p1 = new Vec3f();

    /* renamed from: p2 */
    private static final Vec3f f10364p2 = new Vec3f();
    private ExpandableIndexData indexes = new ExpandableIndexData(100);
    private int lastIndex = 0;
    private short lineStipple;
    private float lineWidth = 1.0f;
    private Mesh mesh = new Mesh();
    private ExpandableVertexData vertexes = new ExpandableVertexData(new VertexLayout(true, false, false, false, 1), 100);

    public RLine() {
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setGeometryType(GeometryType.LINES);
    }

    public RLine(RLine rLine) {
        super(rLine);
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setGeometryType(GeometryType.LINES);
    }

    public float getLineWidth() {
        System.out.println("[Warning]LineWidth not implemented...");
        return this.lineWidth;
    }

    public void setLineWidth(float f) {
        System.out.println("[Warning]LineWidth not implemented...");
        this.lineWidth = f;
    }

    /* access modifiers changed from: package-private */
    public short getLineStipple() {
        return this.lineStipple;
    }

    /* access modifiers changed from: package-private */
    public void setLineStipple(short s) {
        this.lineStipple = s;
    }

    /* access modifiers changed from: package-private */
    public GeometryType getGeometryType() {
        return this.mesh.getGeometryType();
    }

    public void setGeometryType(GeometryType geometryType) {
        this.mesh.setGeometryType(geometryType);
    }

    /* access modifiers changed from: package-private */
    public void createCircle(Vec3f vec3f, Vec3f vec3f2, float f, float f2, float f3, float f4) {
        if (f4 >= 0.0f && f3 >= f2) {
            f10363p1.x = (float) ((Math.cos((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) vec3f2.x));
            f10363p1.y = (float) ((Math.sin((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) vec3f2.y));
            f10363p1.z = vec3f2.z;
            firstPoint.set(f10363p1);
            while (f2 < f3) {
                addPoint(f10363p1);
                f10364p2.x = (float) ((Math.cos((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) vec3f2.x));
                f10364p2.y = (float) ((Math.sin((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) vec3f2.y));
                f10364p2.z = vec3f2.z;
                addPoint(f10364p2);
                f10363p1.set(f10364p2);
                f2 += 3.0f * f4;
            }
            addPoint(f10364p2);
            addPoint(firstPoint);
        }
    }

    public void createSquare(Vec3f vec3f, Vec3f vec3f2, float f) {
        addPoint((-f) + vec3f2.x, (-f) + vec3f2.y, vec3f2.z);
        addPoint(vec3f2.x + f, (-f) + vec3f2.y, vec3f2.z);
        addPoint(vec3f2.x + f, (-f) + vec3f2.y, vec3f2.z);
        addPoint(vec3f2.x + f, vec3f2.y + f, vec3f2.z);
        addPoint(vec3f2.x + f, vec3f2.y + f, vec3f2.z);
        addPoint((-f) + vec3f2.x, vec3f2.y + f, vec3f2.z);
        addPoint((-f) + vec3f2.x, vec3f2.y + f, vec3f2.z);
        addPoint((-f) + vec3f2.x, (-f) + vec3f2.y, vec3f2.z);
    }

    public void createRectangle(Vec3f vec3f, Vec3f vec3f2, float f, float f2) {
        addPoint((-f) + vec3f2.x, (-f2) + vec3f2.y, vec3f2.z);
        addPoint(vec3f2.x + f, (-f2) + vec3f2.y, vec3f2.z);
        addPoint(vec3f2.x + f, (-f2) + vec3f2.y, vec3f2.z);
        addPoint(vec3f2.x + f, vec3f2.y + f2, vec3f2.z);
        addPoint(vec3f2.x + f, vec3f2.y + f2, vec3f2.z);
        addPoint((-f) + vec3f2.x, vec3f2.y + f2, vec3f2.z);
        addPoint((-f) + vec3f2.x, vec3f2.y + f2, vec3f2.z);
        addPoint((-f) + vec3f2.x, (-f2) + vec3f2.y, vec3f2.z);
    }

    public void createElipse(Vec3f vec3f, Vec3f vec3f2, float f, float f2) {
        float f3 = 0.0f;
        f10363p1.x = (float) ((Math.cos((double) (0.0f * 0.017453292f)) * ((double) f)) + ((double) vec3f2.x));
        f10363p1.y = (float) ((Math.sin((double) (0.0f * 0.017453292f)) * ((double) f2)) + ((double) vec3f2.y));
        f10363p1.z = vec3f2.z;
        firstPoint.set(f10363p1);
        while (f3 < 360.0f) {
            addPoint(f10363p1);
            f10364p2.x = (float) ((Math.cos((double) (f3 * 0.017453292f)) * ((double) f)) + ((double) vec3f2.x));
            f10364p2.y = (float) ((Math.sin((double) (f3 * 0.017453292f)) * ((double) f2)) + ((double) vec3f2.y));
            f10364p2.z = vec3f2.z;
            addPoint(f10364p2);
            f10363p1.set(f10364p2);
            f3 += this.lineWidth * 3.0f;
        }
        addPoint(f10364p2);
        addPoint(firstPoint);
    }

    public void setDrawLine() {
    }

    /* access modifiers changed from: package-private */
    public void setDrawBigSegments() {
    }

    /* access modifiers changed from: package-private */
    public void setDrawMediumSegments() {
    }

    /* access modifiers changed from: package-private */
    public void setDrawSmallSegments() {
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
            renderContext.addPrimitive(this.material, this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
        }
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RLine(this);
    }

    public aLH computeWorldSpaceAABB() {
        this.mesh.getAabb().mo9834a(this.globalTransform, this.aabbWorldSpace);
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        this.localAabb.mo9868h(this.mesh.getAabb());
        return this.localAabb;
    }

    public aLH getAABB() {
        if (this.mesh != null) {
            return this.mesh.getAabb();
        }
        return this.localAabb;
    }

    public void addPoint(float f, float f2, float f3) {
        this.vertexes.setPosition(this.lastIndex, f, f2, f3);
        this.indexes.setIndex(this.lastIndex, this.lastIndex);
        this.lastIndex++;
        float f4 = (float) (((double) ((this.transform.orientation.m10 * f) + (this.transform.orientation.m11 * f2) + (this.transform.orientation.m12 * f3))) + this.transform.position.y);
        float f5 = (float) (((double) ((this.transform.orientation.m20 * f) + (this.transform.orientation.m21 * f2) + (this.transform.orientation.m22 * f3))) + this.transform.position.z);
        this.mesh.getAabb().mo9826C((double) f, (double) f2, (double) f3);
        this.aabbWorldSpace.mo9826C((double) ((float) (((double) ((this.transform.orientation.m00 * f) + (this.transform.orientation.m01 * f2) + (this.transform.orientation.m02 * f3))) + this.transform.position.x)), (double) f4, (double) f5);
        this.indexes.setSize(this.lastIndex);
    }

    public void addPoint(Vec3f vec3f) {
        addPoint(vec3f.x, vec3f.y, vec3f.z);
    }

    public void createCircle(Vec3f vec3f, Vec3f vec3f2, float f) {
        createCircle(vec3f, vec3f2, f, 0.0f, 360.0f, 1.0f);
    }

    public void resetLine() {
        this.lastIndex = 0;
        this.indexes.setSize(0);
        this.mesh.getAabb().reset();
    }
}
