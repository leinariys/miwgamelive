package taikodom.render.scene;

import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.ShortIndexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.primitives.Mesh;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;

/* compiled from: a */
public class ObjectFactory {
    private static ObjectFactory instance = new ObjectFactory();

    public static ObjectFactory getDefault() {
        return instance;
    }

    public RMesh createCube(float f) {
        return createBox(f, f, f);
    }

    public Material createDefaultMaterial() {
        Material material = new Material();
        Shader shader = new Shader();
        ShaderPass shaderPass = new ShaderPass();
        shader.addPass(shaderPass);
        shaderPass.setLineMode(true);
        shaderPass.setColorArrayEnabled(true);
        shaderPass.setCullFaceEnabled(false);
        material.setShader(shader);
        return material;
    }

    public Material createDefaultMaterial0() {
        Material material = new Material();
        Shader shader = new Shader();
        ShaderPass shaderPass = new ShaderPass();
        shader.addPass(shaderPass);
        shaderPass.setLineMode(true);
        shaderPass.setColorArrayEnabled(true);
        shaderPass.setCullFaceEnabled(false);
        material.setShader(shader);
        return material;
    }

    private RMesh createBox(float f, float f2, float f3) {
        ExpandableVertexData expandableVertexData = new ExpandableVertexData(new VertexLayout(true, false, false, false, 0), 8);
        ShortIndexData shortIndexData = new ShortIndexData(36);
        float f4 = 0.5f * f;
        float f5 = 0.5f * f2;
        float f6 = 0.5f * f3;
        expandableVertexData.setPosition(0, -f4, -f5, -f6);
        expandableVertexData.setPosition(1, f4, -f5, -f6);
        expandableVertexData.setPosition(2, f4, f5, -f6);
        expandableVertexData.setPosition(3, -f4, f5, -f6);
        expandableVertexData.setPosition(4, -f4, -f5, f6);
        expandableVertexData.setPosition(5, f4, -f5, f6);
        expandableVertexData.setPosition(6, f4, f5, f6);
        expandableVertexData.setPosition(7, -f4, f5, f6);
        addQuad(shortIndexData, addQuad(shortIndexData, addQuad(shortIndexData, addQuad(shortIndexData, addQuad(shortIndexData, addQuad(shortIndexData, 0, 1, 0, 3, 2), 4, 5, 6, 7), 0, 1, 5, 4), 1, 2, 6, 5), 2, 3, 7, 6), 3, 0, 4, 7);
        Mesh mesh = new Mesh();
        mesh.setIndexes(shortIndexData);
        mesh.setVertexData(expandableVertexData);
        RMesh rMesh = new RMesh();
        rMesh.setMesh(mesh);
        return rMesh;
    }

    private int addQuad(ShortIndexData shortIndexData, int i, int i2, int i3, int i4, int i5) {
        int i6 = i + 1;
        shortIndexData.setIndex(i, i2);
        int i7 = i6 + 1;
        shortIndexData.setIndex(i6, i3);
        int i8 = i7 + 1;
        shortIndexData.setIndex(i7, i4);
        int i9 = i8 + 1;
        shortIndexData.setIndex(i8, i2);
        int i10 = i9 + 1;
        shortIndexData.setIndex(i9, i4);
        int i11 = i10 + 1;
        shortIndexData.setIndex(i10, i5);
        return i11;
    }
}
