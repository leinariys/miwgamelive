package taikodom.render.scene;

import game.geometry.C2567gu;
import game.geometry.Vec3d;
import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.enums.ShapeTypes;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.textures.BaseTexture;

import javax.vecmath.Vector3d;

/* compiled from: a */
public class RShape extends RenderObject {
    private static /* synthetic */ int[] $SWITCH_TABLE$taikodom$render$enums$ShapeTypes;
    private float finalAngle;
    private ExpandableIndexData indexes;
    private float initialAngle;
    private float innerRadius;
    private Mesh mesh;
    private float numSpiralTurns;
    private ShapeTypes shapeType;
    private float spiralHeight;
    private float spiralWidth;
    private int tesselation;
    private ExpandableVertexData vertexes;

    public RShape() {
        this.shapeType = ShapeTypes.SHAPE_DISC;
        this.tesselation = 10;
        this.spiralWidth = 0.5f;
        this.numSpiralTurns = 1.0f;
        this.spiralHeight = 0.0f;
        this.innerRadius = 0.0f;
        this.initialAngle = 0.0f;
        this.finalAngle = 360.0f;
        this.tesselation = 1;
        initRenderStructs();
    }

    public RShape(RShape rShape) {
        super(rShape);
        this.innerRadius = rShape.innerRadius;
        this.initialAngle = rShape.initialAngle;
        this.finalAngle = rShape.finalAngle;
        this.numSpiralTurns = rShape.numSpiralTurns;
        this.spiralHeight = rShape.spiralHeight;
        this.spiralWidth = rShape.spiralWidth;
        this.shapeType = rShape.shapeType;
        this.tesselation = rShape.tesselation;
        initRenderStructs();
        build();
    }

    static /* synthetic */ int[] $SWITCH_TABLE$taikodom$render$enums$ShapeTypes() {
        int[] iArr = $SWITCH_TABLE$taikodom$render$enums$ShapeTypes;
        if (iArr == null) {
            iArr = new int[ShapeTypes.values().length];
            try {
                iArr[ShapeTypes.SHAPE_CUBE.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ShapeTypes.SHAPE_DISC.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ShapeTypes.SHAPE_RECTANGLE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ShapeTypes.SHAPE_SPHERE.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ShapeTypes.SHAPE_SPIRAL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            $SWITCH_TABLE$taikodom$render$enums$ShapeTypes = iArr;
        }
        return iArr;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
            float sizeOnScreen = renderContext.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);
            for (int i = 0; i < this.material.textureCount(); i++) {
                BaseTexture texture = this.material.getTexture(i);
                if (texture != null) {
                    texture.addArea(sizeOnScreen);
                }
            }
            renderContext.addPrimitive(this.material, this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
        }
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RShape(this);
    }

    public ShapeTypes getShapeType() {
        return this.shapeType;
    }

    public void setShapeType(ShapeTypes shapeTypes) {
        this.shapeType = shapeTypes;
        build();
    }

    public int getTesselation() {
        return this.tesselation;
    }

    public void setTesselation(int i) {
        this.tesselation = i;
        build();
    }

    public float getInnerRadius() {
        return this.innerRadius;
    }

    public void setInnerRadius(float f) {
        this.innerRadius = f;
        build();
    }

    public float getInitialAngle() {
        return this.initialAngle;
    }

    public void setInitialAngle(float f) {
        this.initialAngle = f;
        if (this.finalAngle < this.initialAngle) {
            this.finalAngle = this.initialAngle;
        }
        build();
    }

    public float getFinalAngle() {
        return this.finalAngle;
    }

    public void setFinalAngle(float f) {
        this.finalAngle = f;
        if (this.initialAngle > this.finalAngle) {
            this.initialAngle = this.finalAngle;
        }
        build();
    }

    public float getNumSpiralTurns() {
        return this.numSpiralTurns;
    }

    public void setNumSpiralTurns(float f) {
        this.numSpiralTurns = f;
        build();
    }

    public float getSpiralHeight() {
        return this.spiralHeight;
    }

    public void setSpiralHeight(float f) {
        this.spiralHeight = f;
        build();
    }

    public float getSpiralWidth() {
        return this.spiralWidth;
    }

    public void setSpiralWidth(float f) {
        this.spiralWidth = f;
        build();
    }

    private void initRenderStructs() {
        this.vertexes = new ExpandableVertexData(new VertexLayout(true, true, false, true, 1), 100);
        this.indexes = new ExpandableIndexData(100);
        this.mesh = new Mesh();
        this.mesh.setName(String.valueOf(getName()) + "_mesh");
        this.mesh.setGeometryType(GeometryType.TRIANGLES);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setIndexes(this.indexes);
    }

    private void build() {
        switch ($SWITCH_TABLE$taikodom$render$enums$ShapeTypes()[this.shapeType.ordinal()]) {
            case 1:
                buildDisc();
                break;
            case 2:
                buildSphere();
                break;
            case 3:
                buildRectangle();
                break;
            case 4:
                buildCube();
                break;
            case 5:
                buildSpiral();
                break;
        }
        this.mesh.computeLocalAabb();
        computeWorldSpaceAABB();
    }

    private void buildDisc() {
        this.indexes.clear();
        this.mesh.setGeometryType(GeometryType.QUAD_STRIP);
        float f = 15.0f / ((float) this.tesselation);
        int i = 0;
        for (float f2 = this.initialAngle; f2 < this.finalAngle; f2 += f) {
            float f3 = f2 / 360.0f;
            float cos = (float) (0.5d * Math.cos(((double) f3) * 3.141592653589793d * 2.0d));
            float sin = (float) (0.5d * Math.sin(((double) f3) * 3.141592653589793d * 2.0d));
            this.indexes.setIndex(i, i);
            this.vertexes.setPosition(i, this.innerRadius * cos, this.innerRadius * sin, 0.0f);
            this.vertexes.setTexCoord(i, 0, 0.5f + (this.innerRadius * cos), 0.5f + (this.innerRadius * sin));
            this.vertexes.setNormal(i, 0.0f, 0.0f, 1.0f);
            int i2 = i + 1;
            this.indexes.setIndex(i2, i2);
            this.vertexes.setPosition(i2, cos, sin, 0.0f);
            this.vertexes.setTexCoord(i2, 0, cos + 0.5f, sin + 0.5f);
            this.vertexes.setNormal(i2, 0.0f, 0.0f, 1.0f);
            i = i2 + 1;
        }
        float f4 = this.finalAngle / 360.0f;
        float cos2 = (float) (0.5d * Math.cos(((double) f4) * 3.141592653589793d * 2.0d));
        float sin2 = (float) (0.5d * Math.sin(((double) f4) * 3.141592653589793d * 2.0d));
        this.indexes.setIndex(i, i);
        this.vertexes.setPosition(i, this.innerRadius * cos2, this.innerRadius * sin2, 0.0f);
        this.vertexes.setTexCoord(i, 0, 0.5f + (this.innerRadius * cos2), 0.5f + (this.innerRadius * sin2));
        this.vertexes.setNormal(i, 0.0f, 0.0f, 1.0f);
        int i3 = i + 1;
        this.indexes.setIndex(i3, i3);
        this.vertexes.setPosition(i3, cos2, sin2, 0.0f);
        this.vertexes.setTexCoord(i3, 0, cos2 + 0.5f, sin2 + 0.5f);
        this.vertexes.setNormal(i3, 0.0f, 0.0f, 1.0f);
    }

    private void buildSphere() {
        this.indexes.clear();
        long j = 10 * ((long) this.tesselation);
        this.mesh.setGeometryType(GeometryType.QUADS);
        int i = 0;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (((long) i3) < j) {
                float f = ((float) i3) / ((float) j);
                float f2 = ((float) (i3 + 1)) / ((float) j);
                float cos = (float) (-0.5d * Math.cos(((double) f) * 3.141592653589793d));
                float cos2 = (float) (-0.5d * Math.cos(((double) f2) * 3.141592653589793d));
                float sin = (float) Math.sin(((double) f) * 3.141592653589793d);
                float sin2 = (float) Math.sin(((double) f2) * 3.141592653589793d);
                for (int i4 = 0; ((long) i4) < j; i4++) {
                    float f3 = ((float) i4) / ((float) j);
                    float f4 = ((float) (i4 + 1)) / ((float) j);
                    float cos3 = (float) (0.5d * Math.cos(((double) f3) * 3.141592653589793d * 2.0d));
                    float cos4 = (float) (0.5d * Math.cos(((double) f4) * 3.141592653589793d * 2.0d));
                    float sin3 = (float) (0.5d * Math.sin(((double) f3) * 3.141592653589793d * 2.0d));
                    float sin4 = (float) (0.5d * Math.sin(((double) f4) * 3.141592653589793d * 2.0d));
                    this.vertexes.setPosition(i, cos3 * sin, cos, sin3 * sin);
                    this.vertexes.setNormal(i, 0.0f, 0.0f, 1.0f);
                    this.vertexes.setTexCoord(i, 0, 0.5f + cos3, 0.5f + cos);
                    this.indexes.setIndex(i, i);
                    int i5 = i + 1;
                    this.vertexes.setPosition(i5, cos3 * sin2, cos2, sin3 * sin2);
                    this.vertexes.setNormal(i5, 0.0f, 0.0f, 1.0f);
                    this.vertexes.setTexCoord(i5, 0, cos3 + 0.5f, 0.5f + cos);
                    this.indexes.setIndex(i5, i5);
                    int i6 = i5 + 1;
                    this.vertexes.setPosition(i6, cos4 * sin2, cos2, sin4 * sin2);
                    this.vertexes.setNormal(i6, 0.0f, 0.0f, 1.0f);
                    this.vertexes.setTexCoord(i6, 0, 0.5f + cos4, 0.5f + cos2);
                    this.indexes.setIndex(i6, i6);
                    int i7 = i6 + 1;
                    this.vertexes.setPosition(i7, cos4 * sin, cos, sin4 * sin);
                    this.vertexes.setNormal(i7, 0.0f, 0.0f, 1.0f);
                    this.vertexes.setTexCoord(i7, 0, 0.5f + cos4, 0.5f + cos2);
                    this.indexes.setIndex(i7, i7);
                    i = i7 + 1;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void buildRectangle() {
        this.indexes.clear();
        this.mesh.setGeometryType(GeometryType.QUADS);
        this.vertexes.setPosition(0, -0.5f, -0.5f, 0.0f);
        this.vertexes.setNormal(0, 0.0f, 0.0f, 1.0f);
        this.vertexes.setTexCoord(0, 0, 0.0f, 0.0f);
        this.indexes.setIndex(0, 0);
        this.vertexes.setPosition(1, 0.5f, -0.5f, 0.0f);
        this.vertexes.setNormal(1, 0.0f, 0.0f, 1.0f);
        this.vertexes.setTexCoord(1, 0, 1.0f, 0.0f);
        this.indexes.setIndex(1, 1);
        this.vertexes.setPosition(2, 0.5f, 0.5f, 0.0f);
        this.vertexes.setNormal(2, 0.0f, 0.0f, 1.0f);
        this.vertexes.setTexCoord(2, 0, 1.0f, 1.0f);
        this.indexes.setIndex(2, 2);
        this.vertexes.setPosition(3, -0.5f, 0.5f, 0.0f);
        this.vertexes.setNormal(3, 0.0f, 0.0f, 1.0f);
        this.vertexes.setTexCoord(3, 0, 0.0f, 1.0f);
        this.indexes.setIndex(3, 3);
    }

    private void buildCube() {
    }

    private void buildSpiral() {
        this.indexes.clear();
        this.mesh.setGeometryType(GeometryType.QUAD_STRIP);
        float f = 1.0f / (((float) this.tesselation) * 10.0f);
        int i = 0;
        for (float f2 = 0.0f; f2 <= 1.0f; f2 += f) {
            float f3 = this.numSpiralTurns * f2;
            float f4 = (1.0f - f2) * this.spiralWidth;
            float cos = (float) (0.5d * Math.cos(((double) f3) * 3.141592653589793d * 2.0d));
            float sin = (float) (0.5d * Math.sin(((double) f3) * 3.141592653589793d * 2.0d));
            this.vertexes.setPosition(i, cos * f2, sin * f2, this.spiralHeight * f2);
            this.vertexes.setNormal(i, 0.0f, 0.0f, 1.0f);
            this.vertexes.setTexCoord(i, 0, 0.5f + (cos * f2), 0.5f + (sin * f2));
            this.indexes.setIndex(i, i);
            int i2 = i + 1;
            this.vertexes.setPosition(i2, (cos * f2) + (cos * f4), (f4 * sin) + (sin * f2), this.spiralHeight * f2);
            this.vertexes.setNormal(i2, 0.0f, 0.0f, 1.0f);
            this.vertexes.setTexCoord(i2, 0, cos + 0.5f, sin + 0.5f);
            this.indexes.setIndex(i2, i2);
            i = i2 + 1;
        }
    }

    public aLH getAABB() {
        if (this.mesh != null) {
            return this.mesh.getAabb();
        }
        return this.localAabb;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        if (this.mesh != null) {
            this.localAabb.mo9868h(this.mesh.getAabb());
        }
        return this.localAabb;
    }

    public aLH computeWorldSpaceAABB() {
        if (this.mesh != null) {
            this.mesh.getAabb().mo9834a(this.globalTransform, this.aabbWorldSpace);
        } else {
            this.aabbWorldSpace.reset();
        }
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public void releaseReferences() {
        super.releaseReferences();
        this.indexes = null;
        this.vertexes = null;
        this.mesh = null;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD rayTraceSceneObjectInfoD, boolean z, boolean z2) {
        rayTraceSceneObjectInfoD.setChanged(false);
        super.rayIntersects(rayTraceSceneObjectInfoD, z, z2);
        if ((!rayTraceSceneObjectInfoD.isIntersected() || !rayTraceSceneObjectInfoD.isChanged()) && this.mesh != null) {
            Vec3d ajr = new Vec3d();
            Vec3d ajr2 = new Vec3d();
            ajr.sub(rayTraceSceneObjectInfoD.getStartPos(), this.globalTransform.position);
            ajr2.sub(rayTraceSceneObjectInfoD.getEndPos(), this.globalTransform.position);
            this.globalTransform.orientation.mo13944a((Vector3d) ajr);
            this.globalTransform.orientation.mo13944a((Vector3d) ajr2);
            C2567gu rayIntersects = this.mesh.rayIntersects(ajr.dfV(), ajr2.dfV());
            if (rayIntersects.isIntersected() && (!z || ((double) rayIntersects.mo19142vM()) < rayTraceSceneObjectInfoD.getParamIntersection())) {
                rayTraceSceneObjectInfoD.set(rayIntersects);
                rayTraceSceneObjectInfoD.setChanged(true);
                rayTraceSceneObjectInfoD.setIntersectedObject(this);
            }
        }
        return rayTraceSceneObjectInfoD;
    }
}
