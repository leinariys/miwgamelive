package taikodom.render.scene;

import game.geometry.C2567gu;
import game.geometry.Vec3d;
import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.textures.BaseTexture;

import javax.vecmath.Vector3d;

/* compiled from: a */
public class RMesh extends RenderObject {
    public Mesh mesh;
    public float minimumScreenSize;

    public RMesh() {
    }

    public RMesh(RMesh rMesh) {
        super(rMesh);
        this.mesh = (Mesh) smartClone(rMesh.mesh);
        this.minimumScreenSize = rMesh.minimumScreenSize;
    }

    public Mesh getMesh() {
        return this.mesh;
    }

    public void setMesh(Mesh mesh2) {
        this.mesh = mesh2;
        this.localAabb.mo9867g(mesh2.getAabb());
        computeWorldSpaceAABB();
    }

    public float getMinimumScreenSize() {
        return this.minimumScreenSize;
    }

    public void setMinimumScreenSize(float f) {
        this.minimumScreenSize = f;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        float sizeOnScreen = renderContext.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);
        if ((!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) && (!renderContext.isCullOutSmallObjects() || sizeOnScreen > 5.0f)) {
            if (this.material == null) {
                return false;
            }
            for (int i = 0; i < this.material.textureCount(); i++) {
                BaseTexture texture = this.material.getTexture(i);
                if (texture != null) {
                    texture.addArea(sizeOnScreen);
                }
            }
            renderContext.addPrimitive(this.material, this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
            renderContext.incMeshesRendered();
        }
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RMesh(this);
    }

    public aLH getAABB() {
        if (this.mesh != null) {
            return this.mesh.getAabb();
        }
        return this.localAabb;
    }

    public aLH computeWorldSpaceAABB() {
        if (this.mesh != null) {
            this.mesh.getAabb().mo9834a(this.globalTransform, this.aabbWorldSpace);
        }
        addChildrenWorldSpaceAABB();
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }

    public aLH computeLocalSpaceAABB() {
        super.computeLocalSpaceAABB();
        if (this.mesh != null) {
            this.localAabb.mo9868h(this.mesh.getAabb());
        }
        return this.localAabb;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD rayTraceSceneObjectInfoD, boolean z, boolean z2) {
        rayTraceSceneObjectInfoD.setChanged(false);
        super.rayIntersects(rayTraceSceneObjectInfoD, z, z2);
        if ((!rayTraceSceneObjectInfoD.isIntersected() || !rayTraceSceneObjectInfoD.isChanged()) && this.mesh != null) {
            Vec3d ajr = new Vec3d();
            Vec3d ajr2 = new Vec3d();
            ajr.sub(rayTraceSceneObjectInfoD.getStartPos(), this.globalTransform.position);
            ajr2.sub(rayTraceSceneObjectInfoD.getEndPos(), this.globalTransform.position);
            this.globalTransform.orientation.mo13944a((Vector3d) ajr);
            this.globalTransform.orientation.mo13944a((Vector3d) ajr2);
            C2567gu rayIntersects = this.mesh.rayIntersects(ajr.dfV(), ajr2.dfV());
            if (rayIntersects.isIntersected() && (!z || ((double) rayIntersects.mo19142vM()) < rayTraceSceneObjectInfoD.getParamIntersection())) {
                rayTraceSceneObjectInfoD.set(rayIntersects);
                rayTraceSceneObjectInfoD.setChanged(true);
                rayTraceSceneObjectInfoD.setIntersectedObject(this);
            }
        }
        return rayTraceSceneObjectInfoD;
    }

    public void validate(SceneLoader sceneLoader) {
        super.validate(sceneLoader);
        if (this.mesh == null) {
            throw new RuntimeException("No 'mesh' property for " + this.name);
        }
    }
}
