package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import taikodom.render.NotExported;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.Material;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector4f;
import java.util.ArrayList;

/* compiled from: a */
public class RTrail extends RenderObject {
    static final Vec3f curDir2 = new Vec3f();
    static final Vec3f lastDir3 = new Vec3f();
    private static final Vec3d cameraTranslation = new Vec3d();
    private static final Vec3f curDir = new Vec3f();
    private static final Vec3f dir = new Vec3f();
    private static final Vec3f dir2 = new Vec3f();
    private static final Vec3f lastDir = new Vec3f();
    private static final Vec3d lastPos = new Vec3d();
    private static final Vec3d localTranslation = new Vec3d();

    /* renamed from: m1 */
    private static final Vec3f f10373m1 = new Vec3f();

    /* renamed from: m2 */
    private static final Vec3f f10374m2 = new Vec3f();

    /* renamed from: m3 */
    private static final Vec3f f10375m3 = new Vec3f();
    private static final Vec3d norm0 = new Vec3d();
    private static final Vec3d norm1 = new Vec3d();
    private static final Vec3d point = new Vec3d();
    private static final Vec3f side = new Vec3f();
    private static final Vec3f spawnDirection = new Vec3f();
    private static final Vec3d spawnVect = new Vec3d();
    private static final Vec3f tempVec = new Vec3f();
    private static final Vec3f tempVect = new Vec3f();
    final Color billboardColor;
    final Vector2fWrap billboardSize;
    final ArrayList<C5242aBu> entries2;
    final ExpandableIndexData indexes;
    final Vector2fWrap texCoord1;
    final Vector2fWrap texCoord2;
    final Vector2fWrap texCoord3;
    final ExpandableVertexData vertexes;
    final Color whiteAlpha0;
    final Color whiteAlpha1;
    public int RenderedTrails;
    public int Trails;
    public int numVertexes;
    public float xSize;
    float DISTANCE_LOD_1 = 160000.0f;
    float DISTANCE_LOD_2 = 360000.0f;
    float DISTANCE_LOD_3 = 640000.0f;
    float DISTANCE_LOD_4 = 7840000.0f;
    float DISTANCE_OUT_OF_RANGE = 1.0E8f;
    float FADE_IN_RELATION = 0.25f;
    int MAX_TRAIL_SUBDIVISION = 50;
    float SCROLL_FACTOR = 10.0f;
    float TRAIL_ANGLE_THRESHOLD = 0.99f;
    float TRAIL_CUTOFF_COSINE = 0.3f;
    float TRAIL_FILTER_VALUE = 0.65f;
    int actualEntrySlot;
    Material billboardMaterial;
    float currentLod;
    int currentPoint;
    Billboard dotBillboard;
    Vec3d lastAddedPosition;
    float lastAddedTime;
    long lastTick;
    float lifeTime;
    float lifeTimeModifier;
    Mesh mesh;
    float minSpeed;
    int numEntrySlots;
    float spawnDistance;
    float squaredDistance;
    int tempVec3fCall = 0;
    float textureTileStart;
    long ticksSec = System.currentTimeMillis();
    boolean useLighting;
    boolean wasVisibleLastFrame;
    private int actualIndex;
    private Vec3d globalTranslation;
    private TransformWrap relativeTransform;
    private C5242aBu tempEntry;

    public RTrail() {
        initializeLocalVariables();
        VertexLayout vertexLayout = new VertexLayout(true, true, false, false, 1);
        this.mesh = new Mesh();
        this.indexes = new ExpandableIndexData((int) 300);
        this.vertexes = new ExpandableVertexData(vertexLayout, 500);
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.squaredDistance = 1.0f;
        this.currentLod = 1.0f;
        this.lifeTimeModifier = 1.0f;
        this.spawnDistance = 1.0f;
        this.lifeTime = 0.5f;
        this.xSize = 1.0f;
        this.minSpeed = 1.0f;
        this.dotBillboard = new Billboard();
        this.billboardSize = new Vector2fWrap(1.0f, 1.0f);
        this.billboardColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.entries2 = new ArrayList<>(300);
        this.mesh.setName("Mesh for a RTrail");
        this.wasVisibleLastFrame = false;
        this.relativeTransform = new TransformWrap();
        this.whiteAlpha1 = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.whiteAlpha0 = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.texCoord1 = new Vector2fWrap(0.0f, 0.0f);
        this.texCoord2 = new Vector2fWrap(0.5f, 0.0f);
        this.texCoord3 = new Vector2fWrap(1.0f, 0.0f);
        this.globalTranslation = new Vec3d();
        this.tempEntry = new C5242aBu();
        this.mesh.setGeometryType(GeometryType.TRIANGLE_STRIP);
        this.textureTileStart = 0.0f;
    }

    RTrail(RTrail rTrail) {
        super(rTrail);
        initializeLocalVariables();
        VertexLayout vertexLayout = new VertexLayout(true, true, false, false, 1);
        this.mesh = new Mesh();
        this.indexes = new ExpandableIndexData((int) 300);
        this.vertexes = new ExpandableVertexData(vertexLayout, 500);
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.squaredDistance = 1.0f;
        this.currentLod = 1.0f;
        this.lifeTimeModifier = 1.0f;
        this.spawnDistance = rTrail.spawnDistance;
        this.lifeTime = rTrail.lifeTime;
        this.xSize = rTrail.xSize;
        this.minSpeed = rTrail.minSpeed;
        this.dotBillboard = rTrail.dotBillboard;
        this.billboardSize = new Vector2fWrap((Vector2f) rTrail.billboardSize);
        this.billboardColor = new Color((Vector4f) rTrail.billboardColor);
        this.entries2 = new ArrayList<>(300);
        this.mesh.setName("Mesh for a RTrail");
        this.wasVisibleLastFrame = false;
        this.relativeTransform = new TransformWrap();
        this.whiteAlpha1 = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.whiteAlpha0 = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.texCoord1 = new Vector2fWrap((Vector2f) rTrail.texCoord1);
        this.texCoord2 = new Vector2fWrap((Vector2f) rTrail.texCoord2);
        this.texCoord3 = new Vector2fWrap((Vector2f) rTrail.texCoord3);
        this.globalTranslation = new Vec3d();
        this.tempEntry = new C5242aBu();
        this.mesh.setGeometryType(GeometryType.TRIANGLE_STRIP);
        this.billboardSize.x = 1.0f;
        this.billboardSize.y = 1.0f;
        this.billboardColor.x = 1.0f;
        this.billboardColor.y = 1.0f;
        this.billboardColor.z = 1.0f;
        this.textureTileStart = 0.0f;
    }

    /* access modifiers changed from: package-private */
    public void setCurrentPoints(int i) {
        this.currentPoint = i;
    }

    public int getNumEntries() {
        return this.actualEntrySlot;
    }

    /* access modifiers changed from: package-private */
    public C5242aBu getEntry(int i) {
        if (i > this.actualEntrySlot) {
            return null;
        }
        return this.entries2.get(i);
    }

    /* access modifiers changed from: package-private */
    public void addEntry(C5242aBu abu) {
        if (this.actualEntrySlot == this.entries2.size()) {
            this.entries2.add(new C5242aBu(abu));
        } else {
            this.entries2.get(this.actualEntrySlot).mo7987a(abu);
        }
        this.actualEntrySlot++;
    }

    /* access modifiers changed from: package-private */
    public void removeEntry(int i) {
        while (i < this.entries2.size() - 1) {
            this.entries2.get(i).mo7987a(this.entries2.get(i + 1));
            i++;
        }
        this.actualEntrySlot--;
    }

    /* access modifiers changed from: package-private */
    public C5242aBu getFirstEntry() {
        return this.entries2.get(0);
    }

    /* access modifiers changed from: package-private */
    public C5242aBu getLastEntry() {
        return this.entries2.get(getNumEntries() - 1);
    }

    /* access modifiers changed from: package-private */
    public int getEntriesBegin() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getEntriesEnd() {
        return getNumEntries() - 1;
    }

    /* access modifiers changed from: package-private */
    public float invSqrRoot(float f) {
        return 1.0f / ((float) Math.sqrt((double) f));
    }

    /* access modifiers changed from: package-private */
    public void fastNormalizeVec3(Vec3f vec3f) {
        float invSqrRoot = invSqrRoot((vec3f.x * vec3f.x) + (vec3f.y * vec3f.y) + (vec3f.z * vec3f.z));
        vec3f.x *= invSqrRoot;
        vec3f.y *= invSqrRoot;
        vec3f.z = invSqrRoot * vec3f.z;
    }

    /* access modifiers changed from: package-private */
    public float getInvLenght(Vec3f vec3f) {
        return invSqrRoot((vec3f.x * vec3f.x) + (vec3f.y * vec3f.y) + (vec3f.z * vec3f.z));
    }

    /* access modifiers changed from: package-private */
    public void initializeLocalVariables() {
    }

    /* access modifiers changed from: protected */
    public Object clone() {
        return new RTrail(this);
    }

    public void dispose() {
    }

    public void releaseReferences() {
        super.releaseReferences();
    }

    /* access modifiers changed from: package-private */
    public void removeDeadSegments(float f) {
        if (getNumEntries() != 0) {
            int entriesBegin = getEntriesBegin();
            while (entriesBegin < getEntriesEnd()) {
                C5242aBu entry = getEntry(entriesBegin);
                entry.mo7990lm(f);
                if (entry.getTime() < 0.0f) {
                    removeEntry(entriesBegin);
                    entriesBegin--;
                } else {
                    entry.hhr -= f;
                    if (entry.hhr < 0.0f) {
                        entry.hhr = 0.0f;
                    }
                }
                entriesBegin++;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void generateMesh(Vec3d ajr) {
        int i;
        this.indexes.clear();
        this.vertexes.data().clear();
        this.mesh.getAabb().reset();
        long numEntries = (long) getNumEntries();
        if (numEntries >= 2) {
            float f = (this.squaredDistance - this.DISTANCE_LOD_3) / (this.DISTANCE_LOD_4 - this.DISTANCE_LOD_3);
            if (f < 0.0f) {
                f = 0.0f;
            } else if (f > 1.0f) {
                f = 1.0f;
            }
            float f2 = 1.0f - f;
            if (f2 > 1.0E-4f) {
                this.textureTileStart = (float) (((double) this.textureTileStart) + Math.floor((double) Math.abs(this.textureTileStart)));
                float f3 = this.textureTileStart;
                this.globalTransform.getX(tempVect);
                float length = this.xSize * 0.5f * tempVect.length();
                this.texCoord1.y = f3;
                this.texCoord2.y = f3;
                this.texCoord3.y = f3;
                int entriesEnd = getEntriesEnd();
                int entriesBegin = getEntriesBegin();
                norm0.mo9484aA(getLastEntry().crZ());
                while (entriesEnd != entriesBegin) {
                    C5242aBu entry = getEntry(entriesEnd);
                    dir.x = (float) (entry.crZ().x - norm0.x);
                    dir.y = (float) (entry.crZ().y - norm0.y);
                    dir.z = (float) (entry.crZ().z - norm0.z);
                    dir2.x = (float) (ajr.x - norm0.x);
                    dir2.y = (float) (ajr.y - norm0.y);
                    dir2.z = (float) (ajr.z - norm0.z);
                    if (dir.lengthSquared() >= 1.0f) {
                        break;
                    }
                    entriesEnd--;
                }
                float f4 = 1.0f / this.lifeTime;
                float f5 = 1.0f / ((float) numEntries);
                dir.scale(5.0f);
                side.set(dir);
                side.mo23483c(dir2);
                side.normalize();
                if (!side.anA()) {
                    side.set(0.0f, 0.0f, 0.0f);
                }
                side.mo23511mT(length);
                this.whiteAlpha1.w = getEntry(entriesEnd).getTime() * f4;
                f10374m2.x = (float) ((norm0.x - ((double) side.x)) - this.globalTranslation.x);
                f10374m2.y = (float) ((norm0.y - ((double) side.y)) - this.globalTranslation.y);
                f10374m2.z = (float) ((norm0.z - ((double) side.z)) - this.globalTranslation.z);
                f10375m3.x = (float) ((norm0.x + ((double) side.x)) - this.globalTranslation.x);
                f10375m3.y = (float) ((norm0.y + ((double) side.y)) - this.globalTranslation.y);
                f10375m3.z = (float) ((norm0.z + ((double) side.z)) - this.globalTranslation.z);
                if (this.currentLod > 1.0f) {
                    this.mesh.getAabb().reset();
                    addPointIgnoreAABB(f10375m3, this.texCoord3, this.whiteAlpha1);
                    addPointIgnoreAABB(f10374m2, this.texCoord1, this.whiteAlpha1);
                    this.indexes.setIndex(0, 1);
                    this.indexes.setIndex(1, 0);
                    this.indexes.setSize(2);
                } else {
                    this.mesh.getAabb().reset();
                    f10373m1.x = (float) (norm0.x - this.globalTranslation.x);
                    f10373m1.y = (float) (norm0.y - this.globalTranslation.y);
                    f10373m1.z = (float) (norm0.z - this.globalTranslation.z);
                    addPointIgnoreAABB(f10375m3, this.texCoord3, this.whiteAlpha1);
                    addPointIgnoreAABB(f10373m1, this.texCoord2, this.whiteAlpha1);
                    addPointIgnoreAABB(f10374m2, this.texCoord1, this.whiteAlpha1);
                }
                this.mesh.getAabb().mo9842aG(norm0);
                int i2 = 1;
                if (this.currentLod > 3.0f) {
                    i2 = 8;
                } else if (this.currentLod > 2.0f) {
                    i2 = 5;
                } else if (this.currentLod > 1.0f) {
                    i2 = 3;
                }
                float f6 = 1.0f / (this.SCROLL_FACTOR * this.xSize);
                float f7 = 2.0f / (this.FADE_IN_RELATION * this.lifeTime);
                int entriesEnd2 = getEntriesEnd();
                this.globalTransform.getZ(tempVect);
                int i3 = 1;
                int i4 = 1;
                float f8 = length;
                while (entriesEnd2 > entriesBegin) {
                    C5242aBu entry2 = getEntry(entriesEnd2);
                    norm1.x = norm0.x * ((double) this.TRAIL_FILTER_VALUE);
                    norm1.y = norm0.y * ((double) this.TRAIL_FILTER_VALUE);
                    norm1.z = norm0.z * ((double) this.TRAIL_FILTER_VALUE);
                    norm1.x += entry2.crZ().x * ((double) (1.0f - this.TRAIL_FILTER_VALUE));
                    norm1.y += entry2.crZ().y * ((double) (1.0f - this.TRAIL_FILTER_VALUE));
                    norm1.z += entry2.crZ().z * ((double) (1.0f - this.TRAIL_FILTER_VALUE));
                    if (i4 % i2 != 0) {
                        norm0.mo9484aA(norm1);
                        i = i3;
                    } else {
                        dir.x = (float) (norm1.x - norm0.x);
                        dir.y = (float) (norm1.y - norm0.y);
                        dir.z = (float) (norm1.z - norm0.z);
                        dir2.x = (float) (ajr.x - norm0.x);
                        dir2.y = (float) (ajr.y - norm0.y);
                        dir2.z = (float) (ajr.z - norm0.z);
                        side.set(dir);
                        side.mo23483c(dir2);
                        side.normalize();
                        if (dir.length() < 0.3f) {
                            i = i3;
                        } else if (!side.anA()) {
                            i = i3;
                        } else {
                            f3 += dir.length() * f6;
                            float time = entry2.getTime() * f4;
                            f8 = (f8 * 0.95f) + ((1.0f + (4.0f * (1.0f - time))) * length * 0.05f);
                            side.mo23511mT(f8);
                            f10374m2.x = (float) ((norm1.x - ((double) side.x)) - this.globalTranslation.x);
                            f10374m2.y = (float) ((norm1.y - ((double) side.y)) - this.globalTranslation.y);
                            f10374m2.z = (float) ((norm1.z - ((double) side.z)) - this.globalTranslation.z);
                            f10375m3.x = (float) ((norm1.x + ((double) side.x)) - this.globalTranslation.x);
                            f10375m3.y = (float) ((norm1.y + ((double) side.y)) - this.globalTranslation.y);
                            f10375m3.z = (float) ((norm1.z + ((double) side.z)) - this.globalTranslation.z);
                            float f9 = (time * ((((float) numEntries) - ((float) i4)) * f5)) - (entry2.hhr * f7);
                            if (f9 > 1.0f) {
                                f9 = 1.0f;
                            } else if (f9 < 0.0f) {
                                f9 = 0.0f;
                            }
                            this.whiteAlpha0.w = this.whiteAlpha1.w;
                            this.whiteAlpha1.w = f9 * f2;
                            Vector2fWrap aka = this.texCoord1;
                            Vector2fWrap aka2 = this.texCoord2;
                            this.texCoord3.y = f3;
                            aka2.y = f3;
                            aka.y = f3;
                            if (this.currentLod > 1.0f) {
                                addPointIgnoreAABB(f10375m3, this.texCoord3, this.whiteAlpha1);
                                addPointIgnoreAABB(f10374m2, this.texCoord1, this.whiteAlpha1);
                                this.indexes.ensure(i3 * 2);
                                this.indexes.setIndex(i3 * 2, (i3 * 2) + 1);
                                this.indexes.setIndex((i3 * 2) + 1, i3 * 2);
                                this.indexes.setSize(i3 * 2);
                            } else {
                                f10373m1.x = (float) (norm1.x - this.globalTranslation.x);
                                f10373m1.y = (float) (norm1.y - this.globalTranslation.y);
                                f10373m1.z = (float) (norm1.z - this.globalTranslation.z);
                                addPointIgnoreAABB(f10375m3, this.texCoord3, this.whiteAlpha1);
                                addPointIgnoreAABB(f10373m1, this.texCoord2, this.whiteAlpha1);
                                addPointIgnoreAABB(f10374m2, this.texCoord1, this.whiteAlpha1);
                                this.indexes.ensure(i3 * 8);
                                this.indexes.setIndex((i3 * 8) - 8, (i3 * 3) - 3);
                                this.indexes.setIndex((i3 * 8) - 7, i3 * 3);
                                this.indexes.setIndex((i3 * 8) - 6, (i3 * 3) - 2);
                                this.indexes.setIndex((i3 * 8) - 5, (i3 * 3) + 1);
                                this.indexes.setIndex((i3 * 8) - 4, (i3 * 3) - 1);
                                this.indexes.setIndex((i3 * 8) - 3, (i3 * 3) + 2);
                                this.indexes.setIndex((i3 * 8) - 2, (i3 * 3) + 2);
                                this.indexes.setIndex((i3 * 8) - 1, i3 * 3);
                                this.indexes.setSize(i3 * 8);
                            }
                            this.mesh.getAabb().mo9842aG(norm1);
                            norm0.mo9484aA(norm1);
                            i = i3 + 1;
                        }
                    }
                    entriesEnd2--;
                    i4++;
                    i3 = i;
                }
                if (this.currentLod > 1.0f) {
                    setCurrentPoints((i3 - 1) * 2);
                } else {
                    setCurrentPoints((i3 - 1) * 8);
                }
                tempVect.x = ((float) this.mesh.getAabb().din().x) + this.xSize;
                tempVect.y = ((float) this.mesh.getAabb().din().y) + this.xSize;
                tempVect.z = ((float) this.mesh.getAabb().din().z) + this.xSize;
                this.mesh.getAabb().addPoint(tempVect);
                tempVect.x -= ((float) this.mesh.getAabb().dim().x) + this.xSize;
                tempVect.y -= ((float) this.mesh.getAabb().dim().y) + this.xSize;
                tempVect.z -= ((float) this.mesh.getAabb().dim().z) + this.xSize;
                this.mesh.getAabb().addPoint(tempVect);
            }
        }
    }

    private void addPointIgnoreAABB(Vec3f vec3f, Vector2fWrap aka, Color color) {
        this.vertexes.setPosition(this.actualIndex, vec3f.x, vec3f.y, vec3f.z);
        this.vertexes.setColor(this.actualIndex, color.x, color.y, color.z, color.w);
        this.vertexes.setTexCoord(this.actualIndex, 0, aka.x, aka.y);
        this.actualIndex++;
    }

    /* access modifiers changed from: package-private */
    public void setActualPoint(int i) {
        this.actualIndex = i;
    }

    /* access modifiers changed from: package-private */
    public void generateAABB() {
        this.aabbWorldSpace.reset();
        int numEntries = getNumEntries();
        if (numEntries >= 2) {
            for (int i = 0; i < numEntries; i++) {
                this.aabbWorldSpace.mo9842aG(getEntry(i).crZ());
            }
            float length = this.xSize * 0.5f * this.globalTransform.getVectorX().length() * 5.0f;
            point.x = this.aabbWorldSpace.din().x + ((double) length);
            point.y = this.aabbWorldSpace.din().y + ((double) length);
            point.z = this.aabbWorldSpace.din().z + ((double) length);
            this.aabbWorldSpace.mo9842aG(point);
            point.x = this.aabbWorldSpace.dim().x - ((double) length);
            point.y = this.aabbWorldSpace.dim().y - ((double) length);
            point.z = this.aabbWorldSpace.dim().z - ((double) length);
            this.aabbWorldSpace.mo9842aG(point);
        }
    }

    /* access modifiers changed from: package-private */
    public void checkForNewSegments(Vec3d ajr, float f) {
        int i;
        if (getNumEntries() != 0) {
            spawnDirection.x = (float) (ajr.x - getLastEntry().crZ().x);
            spawnDirection.y = (float) (ajr.y - getLastEntry().crZ().y);
            spawnDirection.z = (float) (ajr.z - getLastEntry().crZ().z);
            this.textureTileStart -= 1.0f / ((this.SCROLL_FACTOR * this.xSize) * getInvLenght(spawnDirection));
            spawnDirection.normalize();
            this.globalTransform.getZ(tempVec);
            float f2 = -spawnDirection.dot(tempVec);
            if (f2 < 0.0f) {
                f2 = 0.0f;
            }
            float clamp = clamp(f2, 0.0f, 1.0f);
            float f3 = (1.0f - (clamp * clamp)) * this.lifeTime * this.FADE_IN_RELATION;
            float f4 = 0.0f;
            tempVec.x = (float) (ajr.x - this.lastAddedPosition.x);
            tempVec.y = (float) (ajr.y - this.lastAddedPosition.y);
            tempVec.z = (float) (ajr.z - this.lastAddedPosition.z);
            float length = tempVec.length();
            tempVec.x /= length;
            tempVec.y /= length;
            tempVec.z /= length;
            if (getNumEntries() >= 2) {
                lastDir.x = (float) (this.lastAddedPosition.x - getEntry(getNumEntries() - 2).crZ().x);
                lastDir.y = (float) (this.lastAddedPosition.y - getEntry(getNumEntries() - 2).crZ().y);
                lastDir.z = (float) (this.lastAddedPosition.z - getEntry(getNumEntries() - 2).crZ().z);
                lastDir.normalize();
                f4 = lastDir.length() * tempVec.length();
            }
            float f5 = this.spawnDistance * this.currentLod;
            int floor = (int) Math.floor((double) (length / (2.0f * f5)));
            if (floor > this.MAX_TRAIL_SUBDIVISION) {
                i = this.MAX_TRAIL_SUBDIVISION;
            } else {
                i = floor;
            }
            if (i > 0) {
                float f6 = length / ((float) i);
                for (int i2 = 1; i2 < i; i2++) {
                    float f7 = (this.lifeTime * this.lifeTimeModifier) - ((((float) (i - i2)) * f) / ((float) i));
                    if (f7 > 0.0f) {
                        spawnVect.x = ajr.x - ((double) (tempVec.x * (((float) (i - i2)) * f6)));
                        spawnVect.y = ajr.y - ((double) (tempVec.y * (((float) (i - i2)) * f6)));
                        spawnVect.z = ajr.z - ((double) (tempVec.z * (((float) (i - i2)) * f6)));
                        spawn(spawnVect, f3);
                        getFirstEntry().setTime(f7 + f);
                    }
                    length -= f6;
                }
            }
            if (length > f5) {
                if (i == 0) {
                    getFirstEntry().mo7986X(this.lastAddedPosition);
                }
                spawn(ajr, f3);
                getFirstEntry().mo7991ln(f);
                this.lastAddedPosition.mo9484aA(ajr);
            } else if (f4 < this.TRAIL_ANGLE_THRESHOLD) {
                if (i == 0) {
                    getFirstEntry().mo7986X(this.lastAddedPosition);
                }
                spawn(ajr, f3);
                getFirstEntry().mo7991ln(f);
                this.lastAddedPosition.mo9484aA(ajr);
            } else {
                getLastEntry().mo7986X(ajr);
            }
        } else {
            this.textureTileStart = (float) (((double) this.textureTileStart) - (Vec3d.m15653n(this.lastAddedPosition, ajr) / ((double) (this.SCROLL_FACTOR * this.xSize))));
            spawn(ajr, 0.0f);
        }
    }

    private float clamp(float f, float f2, float f3) {
        float f4;
        if (f < f2) {
            f4 = f2;
        } else {
            f4 = f;
        }
        if (f4 > f3) {
            return f3;
        }
        return f4;
    }

    /* access modifiers changed from: package-private */
    public void spawn(Vec3d ajr, float f) {
        if (ajr != this.lastAddedPosition) {
            this.tempEntry.mo7986X(ajr);
            this.tempEntry.hhr = this.lifeTimeModifier * f;
            this.tempEntry.setTime(this.lifeTime * this.lifeTimeModifier);
            addEntry(this.tempEntry);
        }
    }

    /* access modifiers changed from: package-private */
    public void checkConsistency() {
        int i = 0;
        if (getNumEntries() >= 2 && this.currentLod <= 2.0f) {
            lastPos.mo9484aA(getEntry(0).crZ());
            int i2 = 1;
            lastDir3.x = (float) (getEntry(1).crZ().x - lastPos.x);
            lastDir3.y = (float) (getEntry(1).crZ().y - lastPos.y);
            lastDir3.z = (float) (getEntry(1).crZ().z - lastPos.z);
            lastDir3.normalize();
            lastPos.mo9484aA(getEntry(1).crZ());
            for (int i3 = 2; i3 != getNumEntries() && i >= getNumEntries() - 2; i3++) {
                if (getEntry(i3).crZ() == lastPos) {
                }
                curDir2.x = (float) (getEntry(i3).crZ().x - lastPos.x);
                curDir2.y = (float) (getEntry(i3).crZ().y - lastPos.y);
                curDir2.z = (float) (getEntry(i3).crZ().z - lastPos.z);
                curDir2.normalize();
                if (curDir2.length() * lastDir.length() < this.TRAIL_CUTOFF_COSINE) {
                    removeEntry(i2);
                    return;
                }
                lastDir.set(curDir2);
                lastPos.mo9484aA(getEntry(i3).crZ());
                i++;
                i2 = i3;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public float getLodQuality() {
        return 1.0f;
    }

    public int step(StepContext stepContext) {
        Camera camera = stepContext.getCamera();
        this.globalTransform.getTranslation(this.globalTranslation);
        camera.getGlobalTransform().getTranslation(cameraTranslation);
        if (this.lastAddedPosition == null) {
            this.lastAddedPosition = new Vec3d(this.globalTranslation);
        }
        this.relativeTransform.setIdentity();
        this.relativeTransform.setTranslation(this.globalTranslation.x, this.globalTranslation.y, this.globalTranslation.z);
        localTranslation.x = this.globalTranslation.x - cameraTranslation.x;
        localTranslation.y = this.globalTranslation.y - cameraTranslation.y;
        localTranslation.z = this.globalTranslation.z - cameraTranslation.z;
        this.squaredDistance = (getLodQuality() * 0.2f) + 1.0f;
        this.squaredDistance = (float) (((double) this.squaredDistance) * (localTranslation.lengthSquared() / ((double) this.xSize)));
        if (this.squaredDistance == 0.0f) {
            this.squaredDistance = 1.0f;
        }
        this.currentLod = 1.0f;
        if (this.squaredDistance > this.DISTANCE_LOD_4) {
            this.currentLod = 10.0f;
        } else if (this.squaredDistance > this.DISTANCE_LOD_3) {
            this.currentLod = 4.0f;
        } else if (this.squaredDistance > this.DISTANCE_LOD_2) {
            this.currentLod = 3.0f;
        } else if (this.squaredDistance > this.DISTANCE_LOD_1) {
            this.currentLod = 2.0f;
        }
        removeDeadSegments(stepContext.getDeltaTime());
        if (this.squaredDistance < this.DISTANCE_OUT_OF_RANGE) {
            checkConsistency();
            checkForNewSegments(this.globalTranslation, stepContext.getDeltaTime());
        }
        this.lastTick = System.currentTimeMillis();
        return 0;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        Camera camera = renderContext.getCamera();
        boolean z2 = false;
        if (this.currentLod >= 10.0f) {
            this.mesh.getAabb().reset();
        } else if (this.wasVisibleLastFrame) {
            generateAABB();
            z2 = true;
        } else {
            generateAABB();
            if (renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
                z2 = true;
            }
        }
        this.wasVisibleLastFrame = false;
        if (z2) {
            generateMesh(camera.getPosition());
            if (renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
                this.wasVisibleLastFrame = true;
            }
        }
        if (!z || this.wasVisibleLastFrame || z2) {
            float sizeOnScreen = renderContext.getSizeOnScreen(this.globalTransform.position, this.aabbWSLenght);
            for (int i = 0; i < this.material.textureCount(); i++) {
                BaseTexture texture = this.material.getTexture(i);
                if (texture != null) {
                    texture.addArea(sizeOnScreen);
                }
            }
            renderContext.incTrailsRendered();
            this.mesh.setGeometryType(GeometryType.TRIANGLE_STRIP);
            renderContext.addPrimitive(this.material, this.mesh, this.relativeTransform, getPrimitiveColor(), this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
            this.RenderedTrails = this.indexes.size();
            this.Trails = getNumEntries();
            this.numVertexes = this.vertexes.size();
            this.actualIndex = 0;
            this.tempVec3fCall = 0;
        } else {
            this.aabbWorldSpace.mo9867g(this.mesh.getAabb());
        }
        return true;
    }

    public void clearEntries() {
        this.lastTick = 0;
        this.entries2.clear();
    }

    public RenderAsset cloneAsset() {
        return new RTrail(this);
    }

    @NotExported
    @Deprecated
    public Material getTrailMaterial() {
        return this.material;
    }

    @NotExported
    @Deprecated
    public void setTrailMaterial(Material material) {
        this.material = material;
    }

    public float getLifeTimeModifier() {
        return this.lifeTimeModifier;
    }

    public void setLifeTimeModifier(float f) {
        this.lifeTimeModifier = f;
    }

    public float getMinSpeed() {
        return this.minSpeed;
    }

    public void setMinSpeed(float f) {
        this.minSpeed = f;
    }

    public Vector2fWrap getBillboardSize() {
        return this.billboardSize;
    }

    public void setBillboardSize(Vector2fWrap aka) {
        this.billboardSize.set(aka);
    }

    public Material getBillboardMaterial() {
        return this.billboardMaterial;
    }

    public void setBillboardMaterial(Material material) {
        this.billboardMaterial = material;
    }

    public float getTrailWidth() {
        return this.xSize;
    }

    public void setTrailWidth(float f) {
        this.xSize = f;
    }

    public float getLifeTime() {
        return this.lifeTime;
    }

    public void setLifeTime(float f) {
        this.lifeTime = f;
    }

    public boolean isUseLighting() {
        return this.useLighting;
    }

    public void setUseLighting(boolean z) {
        this.useLighting = z;
    }

    public void validate(SceneLoader sceneLoader) {
        super.validate(sceneLoader);
        if (this.material.getShader() == null) {
            this.material.setShader(sceneLoader.getDefaultTrailShader());
        }
    }

    public float getSpawnDistance() {
        return this.spawnDistance;
    }

    public void setSpawnDistance(float f) {
        this.spawnDistance = f;
    }

    public boolean isNeedToStep() {
        return true;
    }
}
