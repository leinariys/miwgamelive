package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import taikodom.render.RenderContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;

/* compiled from: a */
public class RExpandableMesh extends RenderObject {
    public int currentIdx;
    public Mesh mesh;
    public float minimumScreenSize;

    public RExpandableMesh() {
        this.currentIdx = 0;
        this.mesh = new Mesh();
        this.mesh.setGeometryType(GeometryType.TRIANGLES);
        this.mesh.setVertexData(new ExpandableVertexData(VertexLayout.defaultLayout, 10));
        this.mesh.setIndexes(new ExpandableIndexData(10));
    }

    public RExpandableMesh(RExpandableMesh rExpandableMesh) {
        super(rExpandableMesh);
        this.currentIdx = 0;
        this.mesh = (Mesh) smartClone(rExpandableMesh.mesh);
        this.minimumScreenSize = rExpandableMesh.minimumScreenSize;
    }

    public void addVertex(float f, float f2, float f3) {
        this.mesh.getVertexData().setPosition(this.mesh.getVertexData().size(), f, f2, f3);
    }

    public void setVertex(int i, float f, float f2, float f3) {
        this.mesh.getVertexData().setPosition(i, f, f2, f3);
    }

    public void setColor(int i, float f, float f2, float f3, float f4) {
        this.mesh.getVertexData().setColor(i, f, f2, f3, f4);
    }

    public void addVertexWithColor(float f, float f2, float f3, float f4, float f5, float f6, float f7) {
        int size = this.mesh.getVertexData().size();
        this.mesh.getVertexData().setPosition(size, f, f2, f3);
        this.mesh.getVertexData().setColor(size, f4, f5, f6, f7);
    }

    public void setVertexColor(int i, float f, float f2, float f3, float f4) {
        this.mesh.getVertexData().setColor(i, f, f2, f3, f4);
    }

    public void addIndex(int i) {
        this.mesh.getIndexes().setIndex(this.mesh.getIndexes().size(), i);
    }

    public void setIndex(int i, int i2) {
        this.mesh.getIndexes().setIndex(i, i2);
    }

    public Mesh getMesh() {
        return this.mesh;
    }

    public void setMesh(Mesh mesh2) {
        this.mesh = mesh2;
    }

    public float getMinimumScreenSize() {
        return this.minimumScreenSize;
    }

    public void setMinimumScreenSize(float f) {
        this.minimumScreenSize = f;
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        renderContext.addPrimitive(this.material, this.mesh, this.transform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RExpandableMesh(this);
    }

    public void addPoint(Vec3f vec3f, Vector2fWrap aka, Color color) {
        VertexData vertexData = this.mesh.getVertexData();
        vertexData.setColor(this.currentIdx, color.x, color.y, color.z, color.w);
        vertexData.setTexCoord(this.currentIdx, 0, aka.x, aka.y);
        vertexData.setPosition(this.currentIdx, vec3f.x, vec3f.y, vec3f.z);
        this.mesh.getIndexes().setIndex(this.currentIdx, this.currentIdx);
        this.currentIdx++;
    }

    public void reset() {
        this.currentIdx = 0;
    }
}
