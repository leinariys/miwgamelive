package taikodom.render.scene.custom;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;
import logic.render.granny.C0201CS;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.IndexData;
import taikodom.render.data.VertexData;
import taikodom.render.primitives.Mesh;
import taikodom.render.scene.RMesh;

import javax.vecmath.Vector3f;
import java.io.*;
import java.util.*;

/* compiled from: a */
@Slf4j
public class RCustomMesh extends RMesh {
    private static final boolean CHECK_NON_EDGE_VERTS = false;
    private static final float NORMALS_THRESHOLD = 0.8f;
    private static final boolean ORTHONORMALIZE_TANGENT_SPACE = true;
    private static final float TANGENT_THRESHOLD = 0.8f;
    private static final float VERTEX_SQUARED_DISTANCE_THRESHOLD = 1.0E-7f;
    private final List<C5191a> parts = new ArrayList();
    private final Vector2fWrap v2tmp1 = new Vector2fWrap();
    private final Vector2fWrap v2tmp2 = new Vector2fWrap();
    private final Vec3f v3tmp1 = new Vec3f();
    private final Vec3f v3tmp2 = new Vec3f();
    private final Vec3f v3tmp3 = new Vec3f();
    private final Vector4fWrap v4tmp1 = new Vector4fWrap();
    private final Vector4fWrap v4tmp2 = new Vector4fWrap();
    private final Comparator<Vec3f> vertexComparator = new C2622hg(this);
    private int indexCount = 0;
    private boolean[] usedVertexes;
    private WeldingInfo weldingInfo;

    public void addPart(Mesh mesh, boolean z) {
        C0201CS cDE = mesh.getGrannyMesh().bqM().mo17090wa(mesh.getGrannyMaterialIndex()).cDE();
        String str = "unnamed";
        if (cDE != null) {
            str = cDE.getName();
        }
        this.parts.add(new C5191a(str, mesh, z));
        this.indexCount += mesh.getIndexes().size();
        if (this.mesh == null) {
            setMesh(new Mesh(mesh));
        }
    }

    public void assembleMesh() {
        buildIndexData();
        weldParts(true);
    }

    public void buildIndexData() {
        ExpandableIndexData expandableIndexData;
        if (this.mesh == null) {
            log.error("trying to build an index data for a RCustomMesh without any mesh!!");
            return;
        }
        if (!(this.mesh.getIndexes() instanceof ExpandableIndexData)) {
            ExpandableIndexData expandableIndexData2 = new ExpandableIndexData(this.indexCount);
            expandableIndexData2.setUseVbo(false);
            expandableIndexData = expandableIndexData2;
        } else {
            expandableIndexData = (ExpandableIndexData) this.mesh.getIndexes();
        }
        expandableIndexData.clear();
        int i = 0;
        for (C5191a next : this.parts) {
            if (next.isVisible()) {
                IndexData indexes = next.getMesh().getIndexes();
                int i2 = i;
                for (int i3 = 0; i3 < indexes.size(); i3++) {
                    expandableIndexData.setIndex(i2, indexes.getIndex(i3));
                    i2++;
                }
                i = i2;
            }
        }
        expandableIndexData.compact();
        this.mesh.setIndexes(expandableIndexData);
        this.mesh.computeLocalAabb();
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x01b7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void generateWeldingInfo() {
        System.nanoTime();

        TreeMap localTreeMap1 = new TreeMap(
                this.vertexComparator);
        VertexData localVertexData = this.mesh.getVertexData();
        int i = localVertexData.size();

        int j = 0;
        for (int k = 0; k < i; k++) {
            localVertexData.getPosition(k, this.v3tmp1);

            List localObject1 = getDuplicate(localTreeMap1, this.v3tmp1);
            if (localObject1 == null) {
                localObject1 = new ArrayList();
                localTreeMap1.put(new Vec3f(this.v3tmp1), localObject1);
            }
            if (!(localObject1).contains(Integer.valueOf(k))) {
                localObject1.add(Integer.valueOf(k));
                j = Math.max(j, localObject1.size());
            }
        }
        TreeMap localTreeMap2 = new TreeMap(
                this.vertexComparator);

        Object[] localObject1 = new Object[localTreeMap1.size()];
        Object localObject2;
        for (int m = 0; m < this.parts.size(); m++) {
            localObject2 = (this.parts.get(m)).getMesh().getIndexes();

            TreeSet localObject3 = new TreeSet(this.vertexComparator);
            for (int i2 = 0; i2 < ((IndexData) localObject2).size(); i2++) {
                int i3 = ((IndexData) localObject2).getIndex(i2);
                localVertexData.getPosition(i3, this.v3tmp1);
                if (!((Set) localObject3).contains(this.v3tmp1)) {
                    Vec3f localVec3f1 = new Vec3f(this.v3tmp1);
                    ((Set) localObject3).add(localVec3f1);

                    Integer localObject5 = (Integer) localTreeMap2.get(localVec3f1);
                    if (localObject5 == null) {
                        localObject5 = Integer.valueOf(0);
                    }
                    localTreeMap2.put(localVec3f1, Integer.valueOf(((Integer) localObject5).intValue() + 1));
                }
            }
        }
        int m = 0;
        for (Object localObject3 = localTreeMap1.keySet().iterator(); ((Iterator) localObject3).hasNext(); ) {
            localObject2 = (Vec3f) ((Iterator) localObject3).next();
            Integer localObject4 = (Integer) localTreeMap2.get(localObject2);
            if (localObject4 == null) {
                localObject4 = Integer.valueOf(0);
            }
            localObject1[(m++)] = localObject4.intValue();
        }
        m = 0;
        int n = 0;
        int i1 = 0;

        Object localObject4 = new ArrayList();
        ArrayList localArrayList = new ArrayList();

        Vec3f localVec3f1 = new Vec3f();
        Vec3f localObject5 = new Vec3f();
        Vec3f localVec3f2 = new Vec3f();
        Vec3f localVec3f3 = new Vec3f();
        Vector4fWrap localaJF1 = new Vector4fWrap();
        Vector4fWrap localaJF2 = new Vector4fWrap();
        int i8;
        for (Object localObject7 = localTreeMap1.entrySet().iterator(); ((Iterator) localObject7).hasNext(); ) {
            Map.Entry localObject6 = (Map.Entry) ((Iterator) localObject7).next();

            List localList1 = (List) ((Map.Entry) localObject6).getValue();
            int i5 = localList1.size();
            if (i5 > 1) {
                int i6 = 0;
                if ((int) localObject1[m] > 1) {
                    localVertexData.getNormal(((Integer) localList1.get(0)).intValue(), localVec3f1);
                    localVertexData.getNormal(((Integer) localList1.get(1)).intValue(), (Vec3f) localObject5);
                    if (localVec3f1.dot((Vector3f) localObject5) > 0.8F) {
                        ((List) localObject4).add(localList1);
                        n = Math.max(n, i5);
                        i1 += 1 + i5;
                        i6 = 1;
                    }
                }
                if (i6 != 0) {
                    boolean[] arrayOfBoolean2 = new boolean[i5];
                    arrayOfBoolean2[0] = true;
                    localArrayList.add(arrayOfBoolean2);
                    for (i8 = 0; i8 < i5 - 1; i8++) {
                        localVertexData.getTangents(((Integer) localList1.get(i8)).intValue(), localaJF1);
                        localVec3f2.set(localaJF1.x, localaJF1.y, localaJF1.z);
                        for (int i9 = i8 + 1; i9 < i5; i9++) {
                            if (arrayOfBoolean2[i9] == false) {
                                localVertexData.getTangents(((Integer) localList1.get(i9)).intValue(),
                                        localaJF2);
                                localVec3f3.set(localaJF2.x, localaJF2.y, localaJF2.z);
                                if (localVec3f2.dot(localVec3f3) > 0.8F) {
                                    arrayOfBoolean2[i9] = true;
                                    i9 = i5;
                                }
                            }
                        }
                    }
                }
            }
            m++;
        }
        int[] localObject6 = new int[i1];
        boolean[] localObject7 = new boolean[i1];
        m = 0;
        for (int i4 = 0; i4 < ((List) localObject4).size(); i4++) {
            List localList2 = (List) ((List) localObject4).get(i4);
            boolean[] arrayOfBoolean1 = (boolean[]) localArrayList.get(i4);
            int i7 = localList2.size();

            localObject6[m] = i7;
            localObject7[m] = false;
            m++;
            for (i8 = 0; i8 < i7; i8++) {
                localObject6[m] = ((Integer) localList2.get(i8)).intValue();
                localObject7[m] = arrayOfBoolean1[i8];
                m++;
            }
        }
        this.weldingInfo = new WeldingInfo(((List) localObject4).size(),
                n, (int[]) localObject6, (boolean[]) localObject7);
    }


    public void generateWeldingInfo(File file, File file2) {
        File file3 = new File(String.valueOf(file2.getParent()) + File.separator + file2.getName().replace('.', '_') + ".weld");
        if (file3.exists()) {
            if (file3.lastModified() == file.lastModified()) {
                try {
                    FileInputStream fileInputStream = new FileInputStream(file3);
                    Object readObject = new ObjectInputStream(fileInputStream).readObject();
                    if (readObject instanceof WeldingInfo) {
                        this.weldingInfo = (WeldingInfo) readObject;
                        fileInputStream.close();
                        return;
                    }
                    fileInputStream.close();
                } catch (IOException e) {
                } catch (ClassNotFoundException e2) {
                    System.err.println("Possible serialization error in RCustomMesh.");
                }
            }
            file3.delete();
        }
        generateWeldingInfo();
        try {
            file3.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file3);
            new ObjectOutputStream(fileOutputStream).writeObject(this.weldingInfo);
            fileOutputStream.close();
            file3.setLastModified(file.lastModified());
        } catch (IOException e3) {
            System.err.println("Could not cache welding information in RCustomMesh.");
        }
    }

    private List<Integer> getDuplicate(Map<Vec3f, List<Integer>> map, Vec3f vec3f) {
        return map.get(vec3f);
    }

    public int getPartCount() {
        return this.parts.size();
    }

    public WeldingInfo getWeldingInfo() {
        return this.weldingInfo;
    }

    public void setWeldingInfo(WeldingInfo weldingInfo2) {
        this.weldingInfo = weldingInfo2;
    }

    public void hidePart(int i) {
        this.parts.get(i).setVisible(false);
    }

    public void hidePart(String str) {
        for (C5191a next : this.parts) {
            if (next.getName().equals(str)) {
                next.setVisible(false);
                return;
            }
        }
        System.err.println("Part \"" + str + "\" not found!");
    }

    public void showPart(int i) {
        this.parts.get(i).setVisible(true);
    }

    public void showPart(String str) {
        for (C5191a next : this.parts) {
            if (next.getName().equals(str)) {
                next.setVisible(true);
                return;
            }
        }
        System.err.println("Part \"" + str + "\" not found!");
    }

    public void weldParts(boolean z) {
        VertexData vertexData;
        if (this.mesh == null) {
            log.error("trying do weld a RCustomMesh without any mesh!!");
            return;
        }
        if (this.weldingInfo == null) {
            generateWeldingInfo();
        }
        VertexData vertexData2 = this.mesh.getVertexData();
        VertexData vertexData3 = this.parts.get(0).getMesh().getVertexData();
        IndexData indexes = this.mesh.getIndexes();
        if (this.usedVertexes == null || this.usedVertexes.length != vertexData2.size()) {
            this.usedVertexes = new boolean[vertexData2.size()];
            for (int i = 0; i < indexes.size(); i++) {
                this.usedVertexes[indexes.getIndex(i)] = true;
            }
        }
        int i2 = 0;
        int[] info = this.weldingInfo.getInfo();
        boolean[] weldTangents = this.weldingInfo.getWeldTangents();
        Vec3f vec3f = new Vec3f();
        Vec3f vec3f2 = new Vec3f();
        Vec3f vec3f3 = new Vec3f();
        Vec3f vec3f4 = new Vec3f();
        Vector4fWrap ajf = new Vector4fWrap();
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < this.weldingInfo.getUniqueCount()) {
                boolean z2 = false;
                int i5 = info[i2];
                int i6 = i2 + 1;
                int i7 = 0;
                while (true) {
                    if (i7 >= i5) {
                        break;
                    } else if (this.usedVertexes[info[i6 + i7]]) {
                        z2 = true;
                        break;
                    } else {
                        i7++;
                    }
                }
                if (z2) {
                    if (z) {
                        vertexData = vertexData3;
                    } else {
                        vertexData = vertexData2;
                    }
                    vertexData.getNormal(info[i6], this.v3tmp1);
                    vertexData.getTangents(info[i6], ajf);
                    this.v4tmp1.set(ajf);
                    for (int i8 = 1; i8 < i5; i8++) {
                        vertexData.getNormal(info[i6 + i8], this.v3tmp2);
                        this.v3tmp1.add(this.v3tmp2);
                        if (weldTangents[i6 + i8]) {
                            vertexData.getTangents(info[i6 + i8], this.v4tmp2);
                            this.v4tmp2.w = 0.0f;
                            this.v4tmp1.add(this.v4tmp2);
                        }
                    }
                    this.v3tmp1.cox();
                    this.v3tmp2.set(this.v4tmp1.x, this.v4tmp1.y, this.v4tmp1.z);
                    this.v3tmp2.cox();
                    this.v4tmp1.x = this.v3tmp2.x;
                    this.v4tmp1.y = this.v3tmp2.y;
                    this.v4tmp1.z = this.v3tmp2.z;
                    vec3f.set(this.v3tmp2);
                    vec3f2.set(this.v3tmp1);
                    int i9 = 0;
                    while (true) {
                        int i10 = i9;
                        if (i10 >= i5) {
                            break;
                        }
                        vertexData2.setNormal(info[i6], vec3f2.x, vec3f2.y, vec3f2.z);
                        vertexData.getTangents(info[i6], ajf);
                        if (weldTangents[i6]) {
                            vec3f3.cross(vec3f2, vec3f);
                            vec3f3.normalize();
                            vec3f4.cross(vec3f3, vec3f2);
                            vertexData2.setTangents(info[i6], vec3f4.x, vec3f4.y, vec3f4.z, ajf.w);
                        } else {
                            vec3f4.set(ajf.x, ajf.y, ajf.z);
                            vec3f3.cross(vec3f2, vec3f4);
                            vec3f3.normalize();
                            vec3f4.cross(vec3f3, vec3f2);
                            vertexData2.setTangents(info[i6], vec3f4.x, vec3f4.y, vec3f4.z, ajf.w);
                        }
                        i6++;
                        i9 = i10 + 1;
                    }
                    i2 = i6;
                } else {
                    i2 = i6 + i5;
                }
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    public void releaseReferences() {
        this.parts.clear();
        this.indexCount = 0;
        this.usedVertexes = null;
        this.weldingInfo = null;
        this.mesh = null;
        super.releaseReferences();
    }

    /* renamed from: taikodom.render.scene.custom.RCustomMesh$a */
    private class C5191a {
        private Mesh mesh;
        private String name;
        private boolean visible;

        public C5191a(String str, Mesh mesh2, boolean z) {
            this.name = str;
            this.mesh = mesh2;
            this.visible = z;
        }

        public Mesh getMesh() {
            return this.mesh;
        }

        public String getName() {
            return this.name;
        }

        public boolean isVisible() {
            return this.visible;
        }

        public void setVisible(boolean z) {
            this.visible = z;
        }
    }
}
