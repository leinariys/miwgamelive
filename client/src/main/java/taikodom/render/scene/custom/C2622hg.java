package taikodom.render.scene.custom;

import com.hoplon.geometry.Vec3f;

import java.util.Comparator;

/* renamed from: a.hg */
/* compiled from: a */
class C2622hg implements Comparator<Vec3f> {

    /* renamed from: TK */
    final /* synthetic */ RCustomMesh f8023TK;

    public C2622hg(RCustomMesh rCustomMesh) {
        this.f8023TK = rCustomMesh;
    }

    /* renamed from: d */
    public int compare(Vec3f vec3f, Vec3f vec3f2) {
        if (vec3f.x + 1.0E-7f < vec3f2.x) {
            return -1;
        }
        if (vec3f.x - 1.0E-7f > vec3f2.x) {
            return 1;
        }
        if (vec3f.y + 1.0E-7f < vec3f2.y) {
            return -1;
        }
        if (vec3f.y - 1.0E-7f > vec3f2.y) {
            return 1;
        }
        if (vec3f.z + 1.0E-7f < vec3f2.z) {
            return -1;
        }
        if (vec3f.z - 1.0E-7f > vec3f2.z) {
            return 1;
        }
        return 0;
    }
}
