package taikodom.render.scene;

import game.geometry.Vec3d;

/* renamed from: a.aBu  reason: case insensitive filesystem */
/* compiled from: a */
public class C5242aBu {
    public float hhr;
    private Vec3d grI = new Vec3d();
    private float time;

    public C5242aBu() {
    }

    public C5242aBu(C5242aBu abu) {
        mo7987a(abu);
    }

    /* renamed from: a */
    public void mo7987a(C5242aBu abu) {
        this.grI.mo9484aA(abu.grI);
        this.time = abu.time;
        this.hhr = abu.hhr;
    }

    public Vec3d crZ() {
        return this.grI;
    }

    /* renamed from: X */
    public void mo7986X(Vec3d ajr) {
        this.grI.mo9484aA(ajr);
    }

    public float getTime() {
        return this.time;
    }

    public void setTime(float f) {
        this.time = f;
    }

    /* renamed from: lm */
    public void mo7990lm(float f) {
        this.time -= f;
    }

    /* renamed from: ln */
    public void mo7991ln(float f) {
        this.time += f;
    }
}
