package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import taikodom.render.StepContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class RRotor extends SceneObject {
    private final Vec3f pos = new Vec3f();
    private final Matrix3fWrap rotationMatrix = new Matrix3fWrap();
    private final Matrix3fWrap tempMatrix = new Matrix3fWrap();
    float rotX;
    float rotY;
    float rotZ;

    public RRotor() {
    }

    public RRotor(RRotor rRotor) {
        this.rotationMatrix.set(rRotor.rotationMatrix);
        this.pos.set(rRotor.pos);
        this.rotX = rRotor.rotX;
        this.rotY = rRotor.rotY;
        this.rotZ = rRotor.rotZ;
    }

    public int step(StepContext stepContext) {
        this.rotX += stepContext.getDeltaTime() * this.angularVelocity.x;
        this.rotY += stepContext.getDeltaTime() * this.angularVelocity.y;
        this.rotZ += stepContext.getDeltaTime() * this.angularVelocity.z;
        this.rotationMatrix.setIdentity();
        this.tempMatrix.setIdentity();
        this.tempMatrix.rotateY(this.rotY);
        this.rotationMatrix.mul(this.tempMatrix);
        this.tempMatrix.setIdentity();
        this.tempMatrix.rotateX(this.rotX);
        this.rotationMatrix.mul(this.tempMatrix);
        this.tempMatrix.setIdentity();
        this.tempMatrix.rotateZ(this.rotZ);
        this.rotationMatrix.mul(this.tempMatrix);
        this.transform.getTranslation(this.pos);
        this.transform.orientation.set(this.rotationMatrix);
        this.transform.setTranslation(this.pos);
        updateInternalGeometry((SceneObject) null);
        return 0;
    }

    public RenderAsset cloneAsset() {
        return new RRotor(this);
    }

    public boolean isNeedToStep() {
        return true;
    }
}
