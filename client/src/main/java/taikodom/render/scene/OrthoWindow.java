package taikodom.render.scene;

/* compiled from: a */
public class OrthoWindow {
    public float bottom;
    public float left;
    public float right;
    public float top;

    public OrthoWindow() {
    }

    public OrthoWindow(OrthoWindow orthoWindow) {
        set(orthoWindow);
    }

    public OrthoWindow(float f, float f2, float f3, float f4) {
        this.left = f;
        this.right = f2;
        this.bottom = f3;
        this.top = f4;
    }

    public void set(OrthoWindow orthoWindow) {
        this.left = orthoWindow.left;
        this.right = orthoWindow.right;
        this.bottom = orthoWindow.bottom;
        this.top = orthoWindow.top;
    }
}
