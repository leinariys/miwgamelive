package taikodom.render.scene;

import logic.res.KeyCode;
import util.Maths;

/* compiled from: a */
public class RenderConfig {
    private final SceneViewQuality sceneViewQuality = new SceneViewQuality();
    private float anisotropicFilter = 0.0f;
    private int antiAliasing = 0;
    private int bpp = 32;
    private boolean fullscreen = false;
    private float guiContrastLevel;
    private int maxMipMapResolution = 11;
    private int minMipMapLevel = 2;
    private int refreshRate = 60;
    private int screenHeight = KeyCode.cuB;
    private int screenWidth = 1024;
    private int textureFilter = 0;
    private int textureQuality = 1024;
    private boolean useVbo = true;

    public int getScreenWidth() {
        return this.screenWidth;
    }

    public void setScreenWidth(int i) {
        this.screenWidth = i;
    }

    public int getScreenHeight() {
        return this.screenHeight;
    }

    public void setScreenHeight(int i) {
        this.screenHeight = i;
    }

    public int getBpp() {
        return this.bpp;
    }

    public void setBpp(int i) {
        this.bpp = i;
    }

    public int getTextureQuality() {
        return this.textureQuality;
    }

    public void setTextureQuality(int i) {
        this.textureQuality = Maths.doubling(i);
        if (i < 256) {
        }
    }

    public int getTextureFilter() {
        return this.textureFilter;
    }

    public void setTextureFilter(int i) {
        this.textureFilter = i;
    }

    public int getAntiAliasing() {
        return this.antiAliasing;
    }

    public void setAntiAliasing(int i) {
        this.antiAliasing = i;
    }

    public float getLodQuality() {
        return this.sceneViewQuality.getLodQuality();
    }

    public void setLodQuality(float f) {
        this.sceneViewQuality.setLodQuality(f);
    }

    public int getEnvFxQuality() {
        return this.sceneViewQuality.getEnvFxQuality();
    }

    public void setEnvFxQuality(int i) {
        this.sceneViewQuality.setEnvFxQuality(i);
    }

    public int getShaderQuality() {
        return this.sceneViewQuality.getShaderQuality();
    }

    public void setShaderQuality(int i) {
        this.sceneViewQuality.setShaderQuality(i);
    }

    public boolean isPostProcessingFX() {
        return this.sceneViewQuality.isPostProcessingFX();
    }

    public void setPostProcessingFX(boolean z) {
        this.sceneViewQuality.setPostProcessingFX(z);
    }

    public boolean isFullscreen() {
        return this.fullscreen;
    }

    public void setFullscreen(boolean z) {
        this.fullscreen = z;
    }

    public boolean isUseVbo() {
        return this.useVbo;
    }

    public void setUseVbo(boolean z) {
        this.useVbo = z;
    }

    public Float getAnisotropicFilter() {
        return Float.valueOf(this.anisotropicFilter);
    }

    public void setAnisotropicFilter(Float f) {
        this.anisotropicFilter = f.floatValue();
    }

    public void setAnisotropicFilter(float f) {
        this.anisotropicFilter = f;
    }

    public SceneViewQuality getSceneViewQuality() {
        return this.sceneViewQuality;
    }

    public int getMaxMipMapResolution() {
        return this.maxMipMapResolution;
    }

    public void setMaxMipMapResolution(int i) {
        this.maxMipMapResolution = i;
    }

    public int getMinMipMapLevel() {
        return this.minMipMapLevel;
    }

    public void setMinMipMapLevel(int i) {
        this.minMipMapLevel = i;
    }

    public float getGuiContrastLevel() {
        return this.guiContrastLevel;
    }

    public void setGuiContrastLevel(float f) {
        this.guiContrastLevel = f;
    }

    public int getRefreshRate() {
        return this.refreshRate;
    }

    public void setRefreshRate(int i) {
        this.refreshRate = i;
    }
}
