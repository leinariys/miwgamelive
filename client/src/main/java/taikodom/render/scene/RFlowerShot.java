package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import game.geometry.Vec3d;
import game.geometry.aLH;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.textures.BaseTexture;

/* compiled from: a */
public class RFlowerShot extends RenderObject {
    public static Matrix3fWrap tempMat3 = new Matrix3fWrap();
    public final Vec3f shotSize = new Vec3f(1.0f, 1.0f, 1.0f);
    public Mesh shotMesh;

    public RFlowerShot() {
    }

    public RFlowerShot(RFlowerShot rFlowerShot) {
        super(rFlowerShot);
        setShotMesh(rFlowerShot.shotMesh);
        setShotSize(rFlowerShot.shotSize);
    }

    public RenderAsset cloneAsset() {
        return new RFlowerShot(this);
    }

    public Mesh getShotMesh() {
        return this.shotMesh;
    }

    public void setShotMesh(Mesh mesh) {
        this.shotMesh = mesh;
        if (this.shotMesh != null) {
            this.localAabb.mo9867g(this.shotMesh.getAabb());
        }
    }

    public Vec3f getShotSize() {
        return this.shotSize;
    }

    public void setShotSize(Vec3f vec3f) {
        this.shotSize.set(vec3f);
        updateInternalGeometry((SceneObject) null);
    }

    public void updateInternalGeometry(SceneObject sceneObject) {
        this.globalTransform.setIdentity();
        tempMat3.m00 = this.shotSize.x;
        tempMat3.m11 = this.shotSize.y;
        tempMat3.m22 = this.shotSize.z;
        this.globalTransform.orientation.mul(this.transform.orientation, tempMat3);
        this.globalTransform.setTranslation(this.transform.position);
        this.localAabb.mo9834a(this.globalTransform, this.aabbWorldSpace);
    }

    public void setPosition(double d, double d2, double d3) {
        this.aabbWorldSpace.mo9827D(d - this.globalTransform.position.x, d2 - this.globalTransform.position.y, d3 - this.globalTransform.position.z);
        this.transform.position.set(d, d2, d3);
        this.globalTransform.position.set(d, d2, d3);
    }

    public void setPosition(Vec3d ajr) {
        setPosition(ajr.x, ajr.y, ajr.z);
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render) {
            return false;
        }
        this.currentSquaredDistanceToCamera = this.globalTransform.mo17352g(renderContext.getCamera().globalTransform);
        if (this.maxVisibleDistance > ScriptRuntime.NaN && this.currentSquaredDistanceToCamera > this.maxSquaredVisibleDistance) {
            return false;
        }
        if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
            BaseTexture baseTexture = null;
            if (this.material != null) {
                baseTexture = this.material.getDiffuseTexture();
            }
            if (baseTexture != null) {
                baseTexture.addArea(1000.0f);
            }
            renderContext.addPrimitive(this.material, this.shotMesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
            renderContext.incMeshesRendered();
        }
        return true;
    }

    public aLH getAABB() {
        if (this.shotMesh != null) {
            return this.shotMesh.getAabb();
        }
        return this.localAabb;
    }
}
