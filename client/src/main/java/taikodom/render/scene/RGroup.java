package taikodom.render.scene;

import game.geometry.aLH;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class RGroup extends SceneObject {
    public RGroup() {
    }

    public RGroup(RGroup rGroup) {
        super(rGroup);
    }

    public RenderAsset cloneAsset() {
        return new RGroup(this);
    }

    public aLH computeWorldSpaceAABB() {
        super.computeWorldSpaceAABB();
        this.aabbWorldSpace.mo9842aG(this.globalTransform.position);
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return this.aabbWorldSpace;
    }
}
