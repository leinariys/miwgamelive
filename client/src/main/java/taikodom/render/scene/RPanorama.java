package taikodom.render.scene;

import taikodom.render.RenderContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class RPanorama extends SceneObject {
    public RPanorama() {
    }

    public RPanorama(RPanorama rPanorama) {
        super(rPanorama);
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render) {
            return false;
        }
        renderContext.getCamera().getTransform().getTranslation(this.transform.position);
        updateInternalGeometry((SceneObject) null);
        if (!super.render(renderContext, z)) {
            return false;
        }
        return true;
    }

    public RenderAsset cloneAsset() {
        return new RPanorama(this);
    }
}
