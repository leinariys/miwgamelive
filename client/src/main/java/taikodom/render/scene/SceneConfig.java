package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import gnu.trove.THashMap;
import taikodom.render.DrawContext;
import taikodom.render.primitives.Light;
import taikodom.render.shaders.parameters.GlobalShaderParameter;
import taikodom.render.textures.ChannelInfo;

import javax.vecmath.Vector3f;
import java.util.List;
import java.util.Map;

/* compiled from: a */
public class SceneConfig {
    private Map<String, GlobalShaderParameter> globalShaderParameters = new THashMap();

    public SceneConfig() {
        init();
    }

    public boolean usePostProcessingFXs() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void init() {
        addGlobalShaderParameter(new C5170g(0, "elapsedTime"));
        addGlobalShaderParameter(new C5169f(0, "totalTime"));
        addGlobalShaderParameter(new C5172i(0, "cameraPositionWorldSpace"));
        addGlobalShaderParameter(new C5171h(0, "cameraTransform"));
        addGlobalShaderParameter(new C5164a(0, "cameraAffineTransform"));
        addGlobalShaderParameter(new C5165b(0, "lightPositionWorldSpace"));
        addGlobalShaderParameter(new C5166c(0, "lightDirectionWorldSpace"));
        addGlobalShaderParameter(new C5167d(0, "lightDiffuseColor"));
        addGlobalShaderParameter(new C5168e(0, "lightSpecularColor"));
        addGlobalShaderParameter(new C5182s(0, "lightInverseSquaredRadius"));
        addGlobalShaderParameter(new C5181r(0, "lightRadius"));
        addGlobalShaderParameter(new C5180q(0, "spotLightCosCutoff"));
        addGlobalShaderParameter(new C5179p(0, "spotLightExponent"));
        addGlobalShaderParameter(new C5176m(0, "mainLightDirectionWorldSpace"));
        addGlobalShaderParameter(new C5175l(0, "mainLightPositionWorldSpace"));
        addGlobalShaderParameter(new C5174k(0, "mainLightDiffuseColor"));
        addGlobalShaderParameter(new C5173j(0, "mainLightSpecularColor"));
        for (int i = 0; i < 16; i++) {
            addGlobalShaderParameter(new C5178o(1, "tex" + i, i));
        }
        addGlobalShaderParameter(new C5177n(1, "shininess"));
        addGlobalShaderParameter(new C5162C(1, "materialSpecular"));
        addGlobalShaderParameter(new C5189z(1, "reflectCubemap"));
        addGlobalShaderParameter(new C5188y(1, "diffuseCubemap"));
        addGlobalShaderParameter(new C5161B(2, "cameraPositionObjectSpace"));
        addGlobalShaderParameter(new C5160A(2, "lightPositionObjectSpace"));
        addGlobalShaderParameter(new C5185v(2, "lightDirectionObjectSpace"));
        addGlobalShaderParameter(new C5184u(2, "objectTransform"));
        addGlobalShaderParameter(new C5187x(2, "objectAffineTransform"));
        addGlobalShaderParameter(new C5186w(2, "projectTransform"));
        addGlobalShaderParameter(new C5183t(2, "ambientLightColor"));
        addGlobalShaderParameter(new C5163D(2, "objectPrimitiveColor"));
    }

    private void addGlobalShaderParameter(GlobalShaderParameter globalShaderParameter) {
        this.globalShaderParameters.put(globalShaderParameter.getName(), globalShaderParameter);
    }

    public GlobalShaderParameter getGlobalShaderParameter(String str) {
        return this.globalShaderParameters.get(str);
    }

    /* renamed from: taikodom.render.scene.SceneConfig$g */
    /* compiled from: a */
    class C5170g extends GlobalShaderParameter {
        C5170g(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.getGl().glUniform1f(i, drawContext.getElapsedTime());
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$f */
    /* compiled from: a */
    class C5169f extends GlobalShaderParameter {
        C5169f(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.getGl().glUniform1f(i, (float) drawContext.getTotalTime());
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$i */
    /* compiled from: a */
    class C5172i extends GlobalShaderParameter {
        C5172i(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.getGl().glUniform3f(i, 0.0f, 0.0f, 0.0f);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$h */
    /* compiled from: a */
    class C5171h extends GlobalShaderParameter {
        C5171h(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.floatBuffer60Temp0.clear();
            drawContext.getGl().glUniformMatrix4fv(i, 1, false, drawContext.gpuCamAffine.mo13998b(drawContext.floatBuffer60Temp0));
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$a */
    class C5164a extends GlobalShaderParameter {
        C5164a(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.floatBuffer60Temp0.clear();
            drawContext.getGl().glUniformMatrix4fv(i, 1, false, drawContext.gpuCamTransform.mo13998b(drawContext.floatBuffer60Temp0));
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$b */
    /* compiled from: a */
    class C5165b extends GlobalShaderParameter {
        C5165b(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                Vec3d position = drawContext.shaderParamContext.getLight().getPosition();
                Vec3d ajr = drawContext.gpuOffset;
                drawContext.getGl().glUniform3f(i, (float) (position.x - ajr.x), (float) (position.y - ajr.y), (float) (position.z - ajr.z));
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$c */
    /* compiled from: a */
    class C5166c extends GlobalShaderParameter {
        C5166c(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                Vec3f direction = drawContext.shaderParamContext.getLight().getDirection();
                drawContext.getGl().glUniform3f(i, -direction.x, -direction.y, -direction.z);
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$d */
    /* compiled from: a */
    class C5167d extends GlobalShaderParameter {
        C5167d(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                Color diffuseColor = drawContext.shaderParamContext.getLight().getDiffuseColor();
                drawContext.getGl().glUniform3f(i, diffuseColor.x, diffuseColor.y, diffuseColor.z);
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$e */
    /* compiled from: a */
    class C5168e extends GlobalShaderParameter {
        C5168e(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                Color specularColor = drawContext.shaderParamContext.getLight().getSpecularColor();
                drawContext.getGl().glUniform3f(i, specularColor.x, specularColor.y, specularColor.z);
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$s */
    /* compiled from: a */
    class C5182s extends GlobalShaderParameter {
        C5182s(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                float radius = drawContext.shaderParamContext.getLight().getRadius();
                drawContext.getGl().glUniform1f(i, 1.0f / (radius * radius));
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$r */
    /* compiled from: a */
    class C5181r extends GlobalShaderParameter {
        C5181r(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                drawContext.getGl().glUniform1f(i, 1.0f / drawContext.shaderParamContext.getLight().getRadius());
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$q */
    /* compiled from: a */
    class C5180q extends GlobalShaderParameter {
        C5180q(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                drawContext.getGl().glUniform1f(i, drawContext.shaderParamContext.getLight().getCosCutoff());
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$p */
    /* compiled from: a */
    class C5179p extends GlobalShaderParameter {
        C5179p(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                drawContext.getGl().glUniform1f(i, drawContext.shaderParamContext.getLight().getSpotExponent());
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$m */
    /* compiled from: a */
    class C5176m extends GlobalShaderParameter {
        C5176m(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Light mainLight = drawContext.getMainLight();
            if (mainLight != null) {
                Vec3f direction = mainLight.getDirection();
                drawContext.getGl().glUniform3f(i, -direction.x, -direction.y, -direction.z);
                return;
            }
            drawContext.getGl().glUniform3f(i, 0.7f, 0.7f, 0.0f);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$l */
    /* compiled from: a */
    class C5175l extends GlobalShaderParameter {
        C5175l(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Light mainLight = drawContext.getMainLight();
            if (mainLight != null) {
                Vec3d position = mainLight.getPosition();
                Vec3d ajr = drawContext.gpuOffset;
                drawContext.getGl().glUniform3f(i, (float) (position.x - ajr.x), (float) (position.y - ajr.y), (float) (position.z - ajr.z));
                return;
            }
            drawContext.getGl().glUniform3f(i, 0.0f, 0.0f, 0.0f);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$k */
    /* compiled from: a */
    class C5174k extends GlobalShaderParameter {
        C5174k(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Light mainLight = drawContext.getMainLight();
            if (mainLight != null) {
                Color diffuseColor = mainLight.getDiffuseColor();
                drawContext.getGl().glUniform3f(i, diffuseColor.x, diffuseColor.y, diffuseColor.z);
                return;
            }
            drawContext.getGl().glUniform3f(i, 1.0f, 1.0f, 1.0f);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$j */
    /* compiled from: a */
    class C5173j extends GlobalShaderParameter {
        C5173j(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Light mainLight = drawContext.getMainLight();
            if (mainLight != null) {
                Color specularColor = mainLight.getSpecularColor();
                drawContext.getGl().glUniform3f(i, specularColor.x, specularColor.y, specularColor.z);
                return;
            }
            drawContext.getGl().glUniform3f(i, 1.0f, 1.0f, 1.0f);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$o */
    /* compiled from: a */
    class C5178o extends GlobalShaderParameter {
        private final /* synthetic */ int cIW;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C5178o(int i, String str, int i2) {
            super(i, str);
            this.cIW = i2;
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.materialState.getStates().size() > this.cIW) {
                drawContext.getGl().glUniform1i(i, drawContext.materialState.getTexInfo(this.cIW).getChannelMapping());
            } else {
                drawContext.getGl().glUniform1i(i, 0);
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$n */
    /* compiled from: a */
    class C5177n extends GlobalShaderParameter {
        C5177n(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.getGl().glUniform1f(i, drawContext.currentRecord().material.getShininess());
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$C */
    /* compiled from: a */
    class C5162C extends GlobalShaderParameter {
        C5162C(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Color specular = drawContext.currentRecord().material.getSpecular();
            drawContext.getGl().glUniform4f(i, specular.getRed(), specular.getGreen(), specular.getBlue(), specular.getAlpha());
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$z */
    /* compiled from: a */
    class C5189z extends GlobalShaderParameter {
        C5189z(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            List<ChannelInfo> states = drawContext.materialState.getStates();
            int size = states.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (states.get(i2).getTexChannel() == 0) {
                    drawContext.getGl().glUniform1i(i, i2);
                    return;
                }
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$y */
    /* compiled from: a */
    class C5188y extends GlobalShaderParameter {
        C5188y(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            List<ChannelInfo> states = drawContext.materialState.getStates();
            int size = states.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (states.get(i2).getTexChannel() == 1) {
                    drawContext.getGl().glUniform1i(i, i2);
                    return;
                }
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$B */
    /* compiled from: a */
    class C5161B extends GlobalShaderParameter {
        C5161B(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.gpuCamTransform.getTranslation(drawContext.vec3fTemp0);
            drawContext.currentRecord().transform.mo17345b(drawContext.vec3fTemp0, drawContext.vec3fTemp0);
            drawContext.getGl().glUniform3f(i, drawContext.vec3fTemp0.x, drawContext.vec3fTemp0.y, drawContext.vec3fTemp0.z);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$A */
    /* compiled from: a */
    class C5160A extends GlobalShaderParameter {
        C5160A(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            if (drawContext.shaderParamContext.getLight() != null) {
                drawContext.currentRecord().transform.mo17333a(drawContext.shaderParamContext.getLight().getPosition(), drawContext.vec3fTemp0);
                drawContext.getGl().glUniform3f(i, drawContext.vec3fTemp0.x, drawContext.vec3fTemp0.y, drawContext.vec3fTemp0.z);
            }
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$v */
    /* compiled from: a */
    class C5185v extends GlobalShaderParameter {
        C5185v(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Vec3f vec3f = drawContext.vec3fTemp0;
            drawContext.currentRecord().transform.orientation.mo13947a((Vector3f) drawContext.shaderParamContext.getLight().getDirection(), (Vector3f) vec3f);
            vec3f.normalize();
            drawContext.getGl().glUniform3f(i, -vec3f.x, -vec3f.y, -vec3f.z);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$u */
    /* compiled from: a */
    class C5184u extends GlobalShaderParameter {
        C5184u(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.floatBuffer60Temp0.clear();
            drawContext.getGl().glUniformMatrix4fv(i, 1, false, drawContext.currentRecord().gpuTransform.mo13998b(drawContext.floatBuffer60Temp0));
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$x */
    /* compiled from: a */
    class C5187x extends GlobalShaderParameter {
        C5187x(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Matrix4fWrap ajk = drawContext.currentRecord().gpuTransform;
            drawContext.floatBuffer60Temp0.clear();
            ajk.mo13984a(drawContext.floatBuffer60Temp0);
            drawContext.floatBuffer60Temp0.flip();
            drawContext.getGl().glUniformMatrix4fv(i, 1, false, drawContext.floatBuffer60Temp0);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$w */
    /* compiled from: a */
    class C5186w extends GlobalShaderParameter {
        C5186w(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            drawContext.floatBuffer60Temp0.clear();
            drawContext.getGl().glUniformMatrix4fv(i, 1, false, drawContext.matrix4fTemp0.mo13997b(0.5f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f).mo14034h(drawContext.gpuCamAffine).mo14034h(drawContext.camProjection).mo14034h(drawContext.currentRecord().gpuTransform).mo13998b(drawContext.floatBuffer60Temp0));
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$t */
    /* compiled from: a */
    class C5183t extends GlobalShaderParameter {
        C5183t(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Color color = drawContext.currentRecord().areaInfo.ambientColor;
            drawContext.getGl().glUniform3f(i, color.x, color.y, color.z);
        }
    }

    /* renamed from: taikodom.render.scene.SceneConfig$D */
    /* compiled from: a */
    class C5163D extends GlobalShaderParameter {
        C5163D(int i, String str) {
            super(i, str);
        }

        public void bind(int i, DrawContext drawContext) {
            Color color = drawContext.currentRecord().color;
            drawContext.getGl().glUniform4fARB(i, color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha());
        }
    }
}
