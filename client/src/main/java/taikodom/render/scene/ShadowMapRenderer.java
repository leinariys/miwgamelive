package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.camera.Camera;
import taikodom.render.enums.FBOAttachTarget;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.shaders.RenderStates;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.shaders.ShaderProgram;
import taikodom.render.shaders.ShaderProgramObject;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.FrameBufferObject;
import taikodom.render.textures.Material;
import taikodom.render.textures.SimpleTexture;

import javax.media.opengl.GL;
import javax.vecmath.Matrix3f;
import javax.vecmath.Tuple3d;

/* compiled from: a */
public class ShadowMapRenderer {
    SimpleTexture depthTexture;
    FrameBufferObject frameBufferObject;
    SimpleTexture rgbaTexture;
    private float bias = 1.0E-4f;
    private RenderStates defaultStates = new RenderStates();
    private ShaderProgram depthAsColorProgram;
    private Camera lightCamera = new Camera();
    /* renamed from: rc */
    private RenderContext f10377rc = new RenderContext();
    private int shadowMapResolution = 1024;
    private boolean supported = false;

    public ShadowMapRenderer(DrawContext drawContext) {
        this.f10377rc.setDc(drawContext);
        GL gl = drawContext.getGl();
        this.depthTexture = new SimpleTexture();
        this.depthTexture.createEmptyDepth(this.shadowMapResolution, this.shadowMapResolution, 32);
        this.depthTexture.bind(drawContext);
        gl.glTexParameteri(3553, 34891, 32841);
        gl.glTexParameteri(3553, 34892, 34894);
        gl.glTexParameteri(3553, 34893, 515);
        this.rgbaTexture = new SimpleTexture();
        this.rgbaTexture.createEmptyRGBA(this.shadowMapResolution, this.shadowMapResolution, 32);
        this.frameBufferObject = new FrameBufferObject();
        this.frameBufferObject.create(drawContext, this.shadowMapResolution, this.shadowMapResolution, false);
        this.frameBufferObject.addDepthAttachTexture(drawContext, this.depthTexture, FBOAttachTarget.TEXTURE_2D);
        this.frameBufferObject.addColorAttachTexture(drawContext, this.rgbaTexture, FBOAttachTarget.TEXTURE_2D);
        createShaders(drawContext);
    }

    private void createShaders(DrawContext drawContext) {
        this.supported = true;
        ShaderProgramObject shaderProgramObject = new ShaderProgramObject(35633, "void main(void){gl_Position = ftransform();gl_TexCoord[0] = gl_MultiTexCoord0;}");
        ShaderProgramObject shaderProgramObject2 = new ShaderProgramObject(35632, "uniform sampler2D tex0; void main(){vec4 color = texture2D(tex0, gl_TexCoord[0].xy);gl_FragColor = vec4(gl_FragCoord.z,gl_FragCoord.z,gl_FragCoord.z,color.a);\t}");
        this.depthAsColorProgram = new ShaderProgram();
        this.depthAsColorProgram.addProgramObject(shaderProgramObject);
        this.depthAsColorProgram.addProgramObject(shaderProgramObject2);
        if (!this.depthAsColorProgram.compile((ShaderPass) null, drawContext)) {
            this.depthAsColorProgram = null;
            this.supported = false;
            System.out.println("Error compiling shadow shader");
        }
    }

    public SimpleTexture getDepthTexture() {
        return this.depthTexture;
    }

    public void updateShadowTexture(TransformWrap bcVar, RLight rLight, SceneObject sceneObject, DrawContext drawContext, Matrix4fWrap ajk) {
        GL gl = drawContext.getGl();
        this.frameBufferObject.bind(drawContext);
        gl.glViewport(0, 0, this.shadowMapResolution, this.shadowMapResolution);
        gl.glEnable(3089);
        gl.glScissor(0, 0, this.shadowMapResolution, this.shadowMapResolution);
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClear(16640);
        gl.glDisable(3089);
        this.defaultStates.blindlyUpdateGLStates(drawContext);
        float aabbWSLenght = (float) sceneObject.getAabbWSLenght();
        this.lightCamera.setOrthogonal(true);
        Vec3d ajr = new Vec3d(rLight.getLight().getDirection());
        ajr.scaleAdd((double) (-aabbWSLenght), sceneObject.getAabbWorldSpace().mo9875zO());
        Vec3f dfS = rLight.getLight().getDirection().dfS();
        Vec3f vec3f = new Vec3f(1.0f, 0.0f, 0.0f);
        Vec3f vec3f2 = new Vec3f();
        vec3f2.cross(dfS, vec3f);
        vec3f2.normalize();
        vec3f.cross(dfS, vec3f2);
        TransformWrap bcVar2 = new TransformWrap();
        bcVar2.setZ(dfS);
        bcVar2.setY(vec3f);
        bcVar2.setX(vec3f2);
        bcVar2.setTranslation(ajr);
        this.lightCamera.setTransform(bcVar2);
        float f = aabbWSLenght * 0.5f;
        this.lightCamera.setOrthoWindow(-f, f, -f, f);
        this.lightCamera.setNearPlane(0.0f);
        this.lightCamera.setFarPlane(2.0f * aabbWSLenght);
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setTexChannel(8);
        drawContext.primitiveState.clear();
        drawContext.materialState.clear();
        drawContext.primitiveState.states.add(channelInfo);
        drawContext.materialState.states.add(channelInfo);
        this.f10377rc.setDc(drawContext);
        this.f10377rc.setCamera(this.lightCamera);
        this.f10377rc.clearRecordsAndLights();
        this.f10377rc.getDc().gpuCamTransform.mo13985a((Matrix3f) this.f10377rc.getCamera().getGlobalTransform().orientation);
        this.f10377rc.setGpuOffset(this.lightCamera.getPosition());
        drawContext.gpuCamTransform.mo14049p(0.0f, 0.0f, 0.0f);
        this.f10377rc.getDc().gpuCamAffine.mo14043k(drawContext.gpuCamTransform);
        sceneObject.fillRenderQueue(this.f10377rc, false);
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        drawContext.tempBuffer0.clear();
        this.f10377rc.getDc().gpuCamAffine.asColumnMajorBuffer(drawContext.tempBuffer0);
        drawContext.tempBuffer0.flip();
        gl.glMultMatrixf(drawContext.tempBuffer0);
        Matrix4fWrap ajk2 = new Matrix4fWrap();
        ajk2.setIdentity();
        ajk2.mo14030f(-f, f, -f, f, 0.0f, 2.0f * aabbWSLenght);
        gl.glMatrixMode(5889);
        gl.glLoadIdentity();
        drawContext.tempBuffer0.clear();
        ajk2.asColumnMajorBuffer(drawContext.tempBuffer0);
        drawContext.tempBuffer0.flip();
        gl.glMultMatrixf(drawContext.tempBuffer0);
        gl.glMatrixMode(5888);
        this.depthAsColorProgram.bind(drawContext);
        this.depthAsColorProgram.updateMaterialProgramAttribs(drawContext);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glEnableClientState(32884);
        gl.glEnableClientState(32888);
        gl.glDisable(2884);
        gl.glCullFace(1028);
        gl.glEnable(2929);
        gl.glEnable(3553);
        gl.glAlphaFunc(516, 0.0f);
        gl.glEnable(32823);
        gl.glPolygonOffset(1.5f, 0.5f);
        drawPrimitives(drawContext);
        gl.glDisable(32823);
        gl.glPolygonOffset(0.0f, 0.0f);
        gl.glDisableClientState(32884);
        gl.glDisableClientState(32888);
        gl.glCullFace(1029);
        drawContext.primitiveState.clear();
        drawContext.materialState.clear();
        this.depthAsColorProgram.unbind(drawContext);
        gl.glDisableClientState(32884);
        this.defaultStates.blindlyUpdateGLStates(drawContext);
        FrameBufferObject.unbind(drawContext);
        Matrix4fWrap ajk3 = new Matrix4fWrap();
        ajk3.mo13983a(0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.5f, 0.5f - this.bias, 1.0f);
        ajk.setIdentity();
        ajk.set(bcVar2.orientation);
        Matrix4fWrap ajk4 = ajk;
        ajk4.setTranslation(bcVar2.position.mo9517f((Tuple3d) new Vec3d(bcVar.position)));
        ajk.invert();
        Matrix4fWrap.m22816c(ajk2, ajk, ajk);
        Matrix4fWrap.m22816c(ajk3, ajk, ajk);
        ajk2.set(ajk);
    }

    private void drawPrimitives(DrawContext drawContext) {
        Primitive primitive = null;
        GL gl = drawContext.getGl();
        int i = 0;
        Material material = null;
        while (true) {
            Primitive primitive2 = primitive;
            if (i >= this.f10377rc.getRecords().size()) {
                unbindPrimitive(drawContext);
                return;
            }
            TreeRecord treeRecord = this.f10377rc.getRecords().get(i);
            if (primitive2 == null || primitive2.getHandle() != treeRecord.primitive.getHandle()) {
                if (primitive2 != null && primitive2.getUseVBO() && !treeRecord.primitive.getUseVBO()) {
                    unbindPrimitive(drawContext);
                }
                primitive = treeRecord.primitive;
                primitive.bind(drawContext);
            } else {
                primitive = primitive2;
            }
            if (material == null || material.getHandle() != treeRecord.material.getHandle()) {
                gl.glMatrixMode(5890);
                treeRecord.material.bind(drawContext);
                material = treeRecord.material;
                gl.glMatrixMode(5888);
            }
            gl.glPushMatrix();
            drawContext.tempBuffer0.clear();
            treeRecord.gpuTransform.asColumnMajorBuffer(drawContext.tempBuffer0);
            drawContext.tempBuffer0.flip();
            gl.glMultMatrixf(drawContext.tempBuffer0);
            primitive.draw(drawContext);
            gl.glPopMatrix();
            i++;
        }
    }

    private void unbindPrimitive(DrawContext drawContext) {
        drawContext.getGl().glBindBuffer(34962, 0);
        drawContext.getGl().glBindBuffer(34963, 0);
    }

    public SimpleTexture getRgbaTexture() {
        return this.rgbaTexture;
    }

    public float getBias() {
        return this.bias;
    }

    public void setBias(float f) {
        this.bias = f;
    }
}
