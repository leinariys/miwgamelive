package taikodom.render.scene;

import game.geometry.C2567gu;
import game.geometry.Quat4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import logic.render.granny.*;
import org.lwjgl.BufferUtils;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.data.FloatMatrixData;
import taikodom.render.data.VertexData;
import taikodom.render.loader.RenderAsset;
import taikodom.render.primitives.Mesh;
import taikodom.render.raytrace.RayTraceSceneObjectInfoD;
import taikodom.render.scene.custom.RCustomMesh;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.*;

/* compiled from: a */
public class RModel extends SceneObject {
    public static final Vec3d LOCAL_AABB_EXTEND = new Vec3d(5.0d, 5.0d, 5.0d);
    private static final int GRANNY_PNG333_FLOAT_COUNT = 9;
    private static final FloatMatrixData tmp_mat_4x4 = new FloatMatrixData();
    private static final TransformWrap tmp_transform = new TransformWrap();
    private Set<C1126QZ> alreadySkinned = new HashSet();
    private List<RAnimation> animations = new ArrayList();
    private List<BoneTransform> boneTransforms = new ArrayList();
    private FloatBuffer compositeMatrixes;
    private float gameClock = 0.0f;
    private aUB grannyLocalPose = null;
    private C5975adz grannyModel = null;
    private C6676arY grannyModelInstance = null;
    private C3894wP grannyWorldPose = null;
    private boolean hasActiveAnimation = false;
    private C0938Nm lastSeconds = new C0938Nm();
    private int maxMutableVertexCount = 0;
    private FloatBuffer mutableVertexBuffer = null;
    private boolean needSkinning = false;
    private int numBones = 0;
    private FloatMatrixData placementMatrix = new FloatMatrixData(4, 4);
    private C0913NP skeleton = null;
    private List<SocketHub> socketHubs = new ArrayList();
    private List<Socket> sockets = new ArrayList();
    private List<RMesh> subMeshes = new ArrayList();
    private FloatBuffer tmpMeshBindingMatrices;
    private Map<C1126QZ, VertexData> uniqueMeshes = new HashMap();

    public RModel() {
        AdapterGrannyJNI.m4036a(this.lastSeconds);
    }

    public RModel(C5975adz adz) {
        this.grannyModel = adz;
        if (adz != null) {
            this.grannyModelInstance = AdapterGrannyJNI.m4807d(adz);
            this.skeleton = AdapterGrannyJNI.m4829e(this.grannyModelInstance);
        }
        if (this.skeleton != null) {
            setNumBones(this.skeleton.bkG());
        }
        AdapterGrannyJNI.m4036a(this.lastSeconds);
    }

    public RModel(RModel rModel) {
        super(rModel);
        setNumBones(rModel.getNumBones());
        setMaxMutableVertexCount(rModel.getMaxMutableVertexCount());
        this.grannyModel = rModel.grannyModel;
        if (this.grannyModel != null) {
            this.grannyModelInstance = AdapterGrannyJNI.m4807d(this.grannyModel);
            this.skeleton = AdapterGrannyJNI.m4829e(this.grannyModelInstance);
        }
        for (RMesh smartClone : rModel.subMeshes) {
            RMesh rMesh = (RMesh) smartClone(smartClone);
            rMesh.setMesh(new Mesh(rMesh.getMesh()));
            addSubMesh(rMesh);
        }
        for (BoneTransform add : rModel.boneTransforms) {
            this.boneTransforms.add(add);
        }
        for (Socket socket : rModel.sockets) {
            addSocket(new Socket(socket));
        }
        findUniqueGrannyMeshes();
        AdapterGrannyJNI.m4036a(this.lastSeconds);
    }

    public void addBoneRotation(String str, Quat4fWrap aoy) {
        if (this.grannyModelInstance != null) {
            IntBuffer createIntBuffer = BufferUtils.createIntBuffer(1);
            if (AdapterGrannyJNI.m4715b(this.skeleton, str, (Buffer) createIntBuffer)) {
                this.boneTransforms.add(new BoneTransform(createIntBuffer.get(0), new Quat4fWrap(aoy)));
            }
        }
    }

    public void addSubMesh(RMesh rMesh) {
        C0913NP grannySkeleton;
        if (rMesh != null) {
            this.subMeshes.add(rMesh);
            this.localAabb.mo9868h(rMesh.getAABB());
            this.localAabb.mo9833a(this.globalTransform.toQuaternion(), this.globalTransform.position, this.aabbWorldSpace);
            this.aabbLSLenght = this.localAabb.din().mo9491ax(this.localAabb.dim());
            this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
            rMesh.updateInternalGeometry(this);
            Mesh mesh = rMesh.getMesh();
            if (!(mesh == null || (grannySkeleton = mesh.getGrannySkeleton()) == null || grannySkeleton.equals(this.skeleton))) {
                mesh.getGrannyBinding();
                mesh.setGrannyBinding(AdapterGrannyJNI.m3879a(mesh.getGrannyMesh(), grannySkeleton, this.skeleton));
            }
            findUniqueGrannyMeshes();
        }
    }

    public void removeSubMesh(RMesh rMesh) {
        if (rMesh != null) {
            this.subMeshes.remove(rMesh);
            findUniqueGrannyMeshes();
            updateInternalGeometry((SceneObject) null);
        }
    }

    public boolean animate() {
        if (!this.hasActiveAnimation && this.boneTransforms.size() == 0) {
            return false;
        }
        C0938Nm nm = new C0938Nm();
        AdapterGrannyJNI.m4036a(nm);
        this.lastSeconds = nm;
        AdapterGrannyJNI.m4319a(this.grannyModelInstance, this.gameClock);
        if (this.hasActiveAnimation) {
            AdapterGrannyJNI.m4660b(this.grannyModelInstance, 0, this.numBones, this.grannyLocalPose);
        }
        for (BoneTransform next : this.boneTransforms) {
            AdapterGrannyJNI.m4160a(AdapterGrannyJNI.m3797a(this.grannyLocalPose, next.getIndex()), next.getOrientation().x, next.getOrientation().y, next.getOrientation().z, next.getOrientation().w);
        }
        AdapterGrannyJNI.m4019a(this.skeleton, 0, this.numBones, this.grannyLocalPose, (Buffer) this.placementMatrix.buffer(), this.grannyWorldPose);
        this.hasActiveAnimation = false;
        this.needSkinning = true;
        AdapterGrannyJNI.m4708b(this.grannyWorldPose, this.numBones, (Buffer) this.compositeMatrixes);
        this.localAabb.reset();
        for (int i = 0; i < this.numBones; i++) {
            AdapterGrannyJNI.m3961a(i, (Buffer) this.compositeMatrixes, (Buffer) tmp_mat_4x4.buffer());
            tmp_mat_4x4.getTransform(tmp_transform);
            this.localAabb.mo9842aG(tmp_transform.position);
        }
        this.localAabb.mo9830G(LOCAL_AABB_EXTEND.x, LOCAL_AABB_EXTEND.y, LOCAL_AABB_EXTEND.z);
        for (SocketHub next2 : this.socketHubs) {
            AdapterGrannyJNI.m3961a(next2.getBoneIndex(), (Buffer) this.compositeMatrixes, (Buffer) tmp_mat_4x4.buffer());
            tmp_mat_4x4.getTransform(tmp_transform);
            next2.setTransform(tmp_transform);
        }
        return true;
    }

    public void releaseReferences() {
        super.releaseReferences();
        removeAllAnimations();
        this.uniqueMeshes.clear();
        this.subMeshes.clear();
    }

    private boolean deformVerts() {
        AdapterGrannyJNI.m4441a(this.grannyWorldPose, this.numBones, (Buffer) this.compositeMatrixes);
        this.needSkinning = false;
        for (RMesh next : this.subMeshes) {
            Mesh mesh = next.getMesh();
            if (!(mesh == null || mesh.getGrannyMesh() == null)) {
                C1126QZ grannyMesh = mesh.getGrannyMesh();
                VertexData vertexData = mesh.getVertexData();
                C6796ato grannyBinding = mesh.getGrannyBinding();
                C6604aqE grannyDeformer = mesh.getGrannyDeformer();
                C1845ab f = AdapterGrannyJNI.m4856f(grannyBinding);
                int e = AdapterGrannyJNI.m4827e(grannyMesh);
                if (mesh.isRidig()) {
                    AdapterGrannyJNI.m4245a(f, (Buffer) this.compositeMatrixes, (Buffer) tmp_mat_4x4.buffer());
                    tmp_mat_4x4.getTransform(tmp_transform);
                    next.setTransform(tmp_transform);
                } else if (grannyDeformer == null) {
                    continue;
                } else if (this.mutableVertexBuffer == null) {
                    return false;
                } else {
                    if (!this.alreadySkinned.contains(grannyMesh)) {
                        this.alreadySkinned.add(grannyMesh);
                        C5501aLt f2 = AdapterGrannyJNI.m4852f(grannyMesh);
                        if (AdapterGrannyJNI.m4724b(grannyBinding)) {
                            int c = AdapterGrannyJNI.m4740c(grannyBinding);
                            if (this.tmpMeshBindingMatrices == null || this.tmpMeshBindingMatrices.capacity() < c * 16) {
                                this.tmpMeshBindingMatrices = BufferUtils.createFloatBuffer(c * 16);
                            }
                            AdapterGrannyJNI.m4347a(grannyBinding, this.grannyWorldPose, 0, AdapterGrannyJNI.m4740c(grannyBinding), (Buffer) this.tmpMeshBindingMatrices);
                            AdapterGrannyJNI.m4317a(grannyDeformer, (C1845ab) null, (Buffer) this.tmpMeshBindingMatrices, e, f2, (Buffer) this.mutableVertexBuffer);
                        } else {
                            AdapterGrannyJNI.m4317a(grannyDeformer, f, (Buffer) this.compositeMatrixes, e, f2, (Buffer) this.mutableVertexBuffer);
                        }
                        copyDeformedVertexes(vertexData, e);
                    }
                    if (next instanceof RCustomMesh) {
                        ((RCustomMesh) next).weldParts(false);
                    }
                }
            }
        }
        this.alreadySkinned.clear();
        return true;
    }

    private void copyDeformedVertexes(VertexData vertexData, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            vertexData.setPosition(i2, this.mutableVertexBuffer.get(i2 * 9), this.mutableVertexBuffer.get((i2 * 9) + 1), this.mutableVertexBuffer.get((i2 * 9) + 2));
            vertexData.setNormal(i2, this.mutableVertexBuffer.get((i2 * 9) + 3), this.mutableVertexBuffer.get((i2 * 9) + 3 + 1), this.mutableVertexBuffer.get((i2 * 9) + 3 + 2));
            vertexData.setTangents(i2, this.mutableVertexBuffer.get((i2 * 9) + 6), this.mutableVertexBuffer.get((i2 * 9) + 6 + 1), this.mutableVertexBuffer.get((i2 * 9) + 6 + 2));
        }
    }

    public void checkMaxMutableVertexCount(int i) {
        if (this.maxMutableVertexCount < i) {
            setMaxMutableVertexCount(i);
        }
    }

    public void clearBoneRotations() {
        this.boneTransforms.clear();
    }

    public void clearSubMeshes() {
        this.subMeshes.clear();
        this.localAabb.reset();
        this.aabbWorldSpace.reset();
        this.uniqueMeshes.clear();
    }

    public RenderAsset cloneAsset() {
        return new RModel(this);
    }

    public void setSubMesh(int i, RMesh rMesh) {
        if (rMesh != null) {
            this.subMeshes.set(i, rMesh);
            this.localAabb.mo9868h(rMesh.getAABB());
            this.localAabb.mo9833a(this.globalTransform.toQuaternion(), this.globalTransform.position, this.aabbWorldSpace);
            this.aabbLSLenght = this.localAabb.din().mo9491ax(this.localAabb.dim());
            this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
            findUniqueGrannyMeshes();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        if (this.grannyModelInstance != null) {
            AdapterGrannyJNI.m4772c(this.grannyModelInstance);
        }
        if (this.grannyWorldPose != null) {
            AdapterGrannyJNI.m4707b(this.grannyWorldPose);
        }
        if (this.grannyLocalPose != null) {
            AdapterGrannyJNI.m4210a(this.grannyLocalPose);
        }
    }

    public float getGameClock() {
        return this.gameClock;
    }

    public C5975adz getGrannyModel() {
        return this.grannyModel;
    }

    public void setGrannyModel(C5975adz adz) {
        this.grannyModel = adz;
        if (this.grannyModelInstance != null) {
            AdapterGrannyJNI.m4772c(this.grannyModelInstance);
        }
        this.grannyModelInstance = AdapterGrannyJNI.m4807d(adz);
    }

    public C6676arY getGrannyModelInstance() {
        return this.grannyModelInstance;
    }

    public int getMaxMutableVertexCount() {
        return this.maxMutableVertexCount;
    }

    public void setMaxMutableVertexCount(int i) {
        if (this.mutableVertexBuffer != null) {
            this.mutableVertexBuffer = null;
        }
        this.maxMutableVertexCount = i;
        if (i != 0) {
            this.mutableVertexBuffer = BufferUtils.createFloatBuffer(this.maxMutableVertexCount * 9);
        }
    }

    public int getNumBones() {
        return this.numBones;
    }

    public void setNumBones(int i) {
        if (i != 0) {
            this.numBones = i;
            if (this.grannyWorldPose != null) {
                AdapterGrannyJNI.m4707b(this.grannyWorldPose);
            }
            if (this.grannyLocalPose != null) {
                AdapterGrannyJNI.m4210a(this.grannyLocalPose);
            }
            this.grannyWorldPose = AdapterGrannyJNI.m4923hx(this.numBones);
            this.grannyLocalPose = AdapterGrannyJNI.m4920hu(this.numBones);
            this.compositeMatrixes = BufferUtils.createFloatBuffer(this.numBones * 16);
        }
    }

    public C0913NP getSkeleton() {
        return this.skeleton;
    }

    public List<RMesh> getSubMeshes() {
        return this.subMeshes;
    }

    public RayTraceSceneObjectInfoD rayIntersects(RayTraceSceneObjectInfoD rayTraceSceneObjectInfoD, boolean z, boolean z2) {
        rayTraceSceneObjectInfoD.setChanged(false);
        super.rayIntersects(rayTraceSceneObjectInfoD, z, z2);
        if (!rayTraceSceneObjectInfoD.isIntersected() || !rayTraceSceneObjectInfoD.isChanged()) {
            Vec3d ajr = new Vec3d();
            Vec3d ajr2 = new Vec3d();
            TransformWrap bcVar = new TransformWrap();
            this.globalTransform.mo17336a(bcVar);
            bcVar.multiply3x4(rayTraceSceneObjectInfoD.getStartPos(), ajr);
            bcVar.multiply3x4(rayTraceSceneObjectInfoD.getEndPos(), ajr2);
            C2567gu guVar = new C2567gu(ajr.dfV(), ajr2.dfV());
            for (RMesh mesh : this.subMeshes) {
                Mesh mesh2 = mesh.getMesh();
                if (mesh2 != null) {
                    mesh2.rayIntersects(guVar);
                    if (guVar.isIntersected() && (!z || ((double) guVar.mo19142vM()) < rayTraceSceneObjectInfoD.getParamIntersection())) {
                        rayTraceSceneObjectInfoD.set(guVar);
                        rayTraceSceneObjectInfoD.setChanged(true);
                        rayTraceSceneObjectInfoD.setIntersectedObject(this);
                    }
                }
            }
        }
        return rayTraceSceneObjectInfoD;
    }

    public void updateInternalGeometry(SceneObject sceneObject) {
        if (sceneObject != null) {
            this.parentTransform.mo17344b(sceneObject.getGlobalTransform());
            this.globalTransform.mo17337a(this.parentTransform, this.transform);
        } else {
            this.globalTransform.mo17337a(this.parentTransform, this.transform);
        }
        this.globalTransform.mo17324B(this.scaling.x);
        this.globalTransform.mo17325C(this.scaling.y);
        this.globalTransform.mo17326D(this.scaling.z);
        for (RMesh updateInternalGeometry : this.subMeshes) {
            updateInternalGeometry.updateInternalGeometry(this);
        }
        float f = this.scaling.x;
        float f2 = this.scaling.y;
        float f3 = this.scaling.z;
        this.scaling.set(1.0f, 1.0f, 1.0f);
        super.updateInternalGeometry(sceneObject);
        this.scaling.set(f, f2, f3);
    }

    /* access modifiers changed from: protected */
    public void addChildrenWorldSpaceAABB() {
        super.addChildrenWorldSpaceAABB();
        for (RMesh aabbWorldSpace : this.subMeshes) {
            this.aabbWorldSpace.mo9868h(aabbWorldSpace.getAabbWorldSpace());
        }
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render) {
            return false;
        }
        if (!super.render(renderContext, z)) {
            return false;
        }
        this.localAabb.mo9834a(this.globalTransform, this.aabbWorldSpace);
        if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
            if (this.needSkinning) {
                deformVerts();
            }
            for (RMesh render : this.subMeshes) {
                render.render(renderContext, false);
            }
        }
        this.aabbLSLenght = this.localAabb.din().mo9491ax(this.localAabb.dim());
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return true;
    }

    public void setHasActiveAnimation(boolean z) {
        this.hasActiveAnimation = z;
    }

    public int step(StepContext stepContext) {
        this.gameClock += stepContext.getDeltaTime();
        stepAnimations(stepContext);
        if (animate()) {
            for (int i = 0; i < this.subMeshes.size(); i++) {
                RMesh rMesh = this.subMeshes.get(i);
                if (!rMesh.getMesh().isRidig()) {
                    rMesh.getAABB().mo9867g(this.localAabb);
                    rMesh.computeLocalSpaceAABB();
                    rMesh.computeWorldSpaceAABB();
                }
                this.localAabb.mo9868h(rMesh.getAABB());
            }
        } else {
            this.localAabb.reset();
            for (int i2 = 0; i2 < this.subMeshes.size(); i2++) {
                this.localAabb.mo9868h(this.subMeshes.get(i2).getAABB());
            }
        }
        this.localAabb.mo9834a(this.globalTransform, this.aabbWorldSpace);
        this.aabbLSLenght = this.localAabb.din().mo9491ax(this.localAabb.dim());
        this.aabbWSLenght = this.aabbWorldSpace.din().mo9491ax(this.aabbWorldSpace.dim());
        return 0;
    }

    private void stepAnimations(StepContext stepContext) {
        for (int size = this.animations.size() - 1; size >= 0; size--) {
            RAnimation rAnimation = this.animations.get(size);
            rAnimation.step(stepContext);
            if (rAnimation.isDisposed()) {
                this.animations.remove(size);
            }
        }
    }

    public boolean isNeedToStep() {
        return true;
    }

    public SocketHub getHub(String str) {
        for (SocketHub next : this.socketHubs) {
            if (next.getName().equals(str)) {
                return next;
            }
        }
        SocketHub socketHub = new SocketHub(this, str);
        addChild(socketHub);
        this.socketHubs.add(socketHub);
        return socketHub;
    }

    public SocketHub getHub(int i) {
        for (SocketHub next : this.socketHubs) {
            if (next.getBoneIndex() == i) {
                return next;
            }
        }
        SocketHub socketHub = new SocketHub(this, i);
        addChild(socketHub);
        this.socketHubs.add(socketHub);
        return socketHub;
    }

    public void addSocket(Socket socket) {
        this.sockets.add(socket);
        socket.setRModel(this);
    }

    public Socket getSocket(int i) {
        return this.sockets.get(i);
    }

    public void setSocket(int i, Socket socket) {
        this.sockets.add(socket);
        socket.setRModel(this);
    }

    public void removeSocket(Socket socket) {
        if (this.sockets.remove(socket)) {
            socket.unlink();
        }
    }

    public int socketCount() {
        return this.sockets.size();
    }

    public void addAnimation(RAnimation rAnimation) {
        this.animations.add(rAnimation);
        rAnimation.setModel(this);
    }

    public RAnimation getAnimation(String str) {
        for (RAnimation next : this.animations) {
            if (next.getName().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public void removeAnimation(RAnimation rAnimation) {
        this.animations.remove(rAnimation);
    }

    public void removeAnimation(String str) {
        RAnimation animation = getAnimation(str);
        if (animation != null) {
            removeAnimation(animation);
        }
    }

    public void removeAllAnimations() {
        for (RAnimation next : this.animations) {
            next.setFadeTime(0.0f);
            next.stop();
        }
        this.animations.clear();
    }

    private void findUniqueGrannyMeshes() {
        HashSet hashSet = new HashSet();
        for (RMesh mesh : this.subMeshes) {
            Mesh mesh2 = mesh.getMesh();
            hashSet.add(mesh2.getGrannyMesh());
            if (!this.uniqueMeshes.containsKey(mesh2.getGrannyMesh())) {
                if (!mesh2.isRidig()) {
                    VertexData duplicate = mesh2.getVertexData().duplicate();
                    this.uniqueMeshes.put(mesh2.getGrannyMesh(), duplicate);
                    mesh2.setVertexData(duplicate);
                    mesh2.invalidateHandle();
                } else {
                    this.uniqueMeshes.put(mesh2.getGrannyMesh(), mesh2.getVertexData());
                }
            } else if (!mesh2.isRidig()) {
                mesh2.setVertexData(this.uniqueMeshes.get(mesh2.getGrannyMesh()));
            }
        }
        HashSet<C1126QZ> hashSet2 = new HashSet<>();
        for (C1126QZ next : this.uniqueMeshes.keySet()) {
            if (!hashSet.contains(next)) {
                hashSet2.add(next);
            }
        }
        for (C1126QZ remove : hashSet2) {
            this.uniqueMeshes.remove(remove);
        }
    }
}
