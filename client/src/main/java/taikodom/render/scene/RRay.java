package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.geometry.aLH;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.enums.BillboardAlignment;

import java.util.ArrayList;

/* compiled from: a */
public class RRay extends SceneObject {
    RBillboard billboard;
    float elapsedTime;
    float lifeTime;
    float range;
    private ArrayList<RayBB> billboardList = new ArrayList<>();

    public RRay() {
    }

    public RRay(RRay rRay) {
        super(rRay);
        this.billboard = new RBillboard(rRay.billboard);
        this.lifeTime = rRay.lifeTime;
        this.range = rRay.range;
    }

    public boolean isNeedToStep() {
        return true;
    }

    public aLH buildAABB() {
        this.billboard.computeWorldSpaceAABB();
        return this.billboard.getTransformedAABB();
    }

    public void dispose() {
        this.billboard = null;
    }

    public int step(StepContext stepContext) {
        for (int i = 0; i < this.billboardList.size(); i++) {
            RayBB rayBB = this.billboardList.get(i);
            if (rayBB.getElapsedTime() > rayBB.getLifeTime()) {
                rayBB.dispose();
                this.billboardList.remove(i);
            } else {
                rayBB.decLifeTime(stepContext.getDeltaTime());
            }
        }
        return 0;
    }

    public void releaseReferences() {
        super.releaseReferences();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.billboardList.size()) {
                this.billboardList.clear();
                super.releaseReferences();
                return;
            }
            this.billboardList.get(i2).dispose();
            i = i2 + 1;
        }
    }

    public boolean render(RenderContext renderContext, boolean z) {
        int i = 0;
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        while (true) {
            int i2 = i;
            if (i2 >= this.billboardList.size()) {
                return true;
            }
            this.billboardList.get(i2).render(renderContext, z);
            i = i2 + 1;
        }
    }

    public void setRay(RBillboard rBillboard, float f) {
        this.billboard = rBillboard;
        this.lifeTime = f;
    }

    /* access modifiers changed from: package-private */
    public float getLifeTime() {
        return this.lifeTime;
    }

    /* access modifiers changed from: package-private */
    public float getElapsedTime() {
        return this.elapsedTime;
    }

    /* access modifiers changed from: package-private */
    public void decLifeTime(float f) {
        this.elapsedTime += f;
        Color primitiveColor = this.billboard.getPrimitiveColor();
        float f2 = 1.0f - (this.elapsedTime / this.lifeTime);
        primitiveColor.w = f2;
        primitiveColor.x = f2;
        primitiveColor.y = f2;
        primitiveColor.z = f2;
        this.billboard.setPrimitiveColor(primitiveColor);
    }

    /* access modifiers changed from: package-private */
    public Color getColor() {
        return this.billboard.getPrimitiveColor();
    }

    /* access modifiers changed from: package-private */
    public void setColor(Color color) {
        this.billboard.setPrimitiveColor(color);
    }

    public void trigger(Vec3d ajr, Vec3f vec3f, float f) {
        if (this.billboard != null) {
            RBillboard rBillboard = (RBillboard) this.billboard.cloneAsset();
            rBillboard.setPosition(ajr);
            rBillboard.setAlignment(BillboardAlignment.AXIS_ALIGNED);
            rBillboard.setAlignmentDirection(vec3f);
            rBillboard.setSize(this.billboard.getSize().x, f);
            RayBB rayBB = new RayBB(rBillboard, this.lifeTime, f);
            this.billboardList.add(rayBB);
            this.aabbWorldSpace.mo9868h(rayBB.buildAABB());
        }
    }

    public RRay cloneAsset() {
        return new RRay(this);
    }
}
