package taikodom.render.scene;

import com.hoplon.geometry.Vec3f;
import taikodom.render.camera.Camera;

/* compiled from: a */
public abstract class RSimulatedCamera extends RenderObject {
    public Camera camera = new Camera();
    public Vec3f currentCameraOffset = new Vec3f();
    public float currentFovY = 75.0f;
    public float currentRotZ = 0.0f;
    public Vec3f currentTargetOffset = new Vec3f();
    public float filterTimeConstant = 0.25f;
    public SceneObject target;

    public Vec3f getCurrentCameraOffset() {
        return this.currentCameraOffset;
    }

    public void setCurrentCameraOffset(Vec3f vec3f) {
        this.currentCameraOffset = vec3f;
    }

    public SceneObject getTarget() {
        return this.target;
    }

    public void setTarget(SceneObject sceneObject) {
        this.target = sceneObject;
    }

    public float getFilterTimeConstant() {
        return this.filterTimeConstant;
    }

    public void setFilterTimeConstant(float f) {
        this.filterTimeConstant = f;
    }

    public float getCurrentRotZ() {
        return this.currentRotZ;
    }

    public void setCurrentRotZ(float f) {
        this.currentRotZ = f;
    }

    public void copyState(RSimulatedCamera rSimulatedCamera) {
        this.currentCameraOffset = rSimulatedCamera.currentCameraOffset;
        this.currentTargetOffset = rSimulatedCamera.currentTargetOffset;
        this.currentRotZ = rSimulatedCamera.currentRotZ;
    }
}
