package taikodom.render.scene;

import logic.render.granny.C0913NP;
import logic.render.granny.C5767aVz;

import java.util.List;

/* compiled from: a */
public class SocketHub extends SceneObject {
    private int boneIndex;
    private C5767aVz grannyBone;

    public SocketHub(RModel rModel, C5767aVz avz) {
        this.name = avz.getName();
        this.grannyBone = avz;
        this.boneIndex = rModel.getSkeleton().mo4229gb(this.name);
        if (this.boneIndex < 0) {
            throw new IndexOutOfBoundsException("Bone " + this.name + " not found on skeleton " + rModel.getSkeleton().getName());
        }
    }

    public SocketHub(RModel rModel, String str) {
        C0913NP skeleton = rModel.getSkeleton();
        this.boneIndex = skeleton.mo4228ga(str);
        if (this.boneIndex < 0) {
            throw new IndexOutOfBoundsException("Bone " + str + " not found on skeleton " + skeleton.getName());
        }
        this.grannyBone = skeleton.bkH().mo11989Bg(this.boneIndex);
        this.name = this.grannyBone.getName();
    }

    public SocketHub(RModel rModel, int i) {
        C0913NP skeleton = rModel.getSkeleton();
        this.boneIndex = i;
        this.grannyBone = skeleton.bkH().mo11989Bg(i);
        if (this.grannyBone == null) {
            throw new IndexOutOfBoundsException("Bone [" + i + "] not found on skeleton " + skeleton.getName());
        }
        this.name = this.grannyBone.getName();
    }

    public String getName() {
        return this.grannyBone.getName();
    }

    public List<SceneObject> getSockets() {
        return getChildren();
    }

    public void addSocket(Socket socket) {
        addChild(socket);
    }

    public void removeSocket(Socket socket) {
        removeChild(socket);
    }

    public String getBoneName() {
        return this.grannyBone.getName();
    }

    public int getBoneIndex() {
        return this.boneIndex;
    }
}
