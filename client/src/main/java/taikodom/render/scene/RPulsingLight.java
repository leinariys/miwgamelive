package taikodom.render.scene;

import com.hoplon.geometry.Color;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;

/* compiled from: a */
public class RPulsingLight extends RLight {
    private final Color finalColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private final Color initialColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    private float currentLife;
    private float finalSize;
    private float initialSize;
    private float lifeTime;

    public void trigger() {
        this.light.setRadius(this.initialSize);
        this.light.setDiffuseColor(this.initialColor);
        this.light.setSpecularColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
        this.currentLife = this.lifeTime;
        show();
    }

    /* access modifiers changed from: package-private */
    public void trigger(float f, float f2, Color color, Color color2, float f3) {
        this.initialSize = f;
        this.finalSize = f2;
        this.initialColor.set(color);
        this.finalColor.set(color2);
        this.lifeTime = f3;
        trigger();
    }

    public void setInitialColor(Color color) {
        this.initialColor.set(color);
    }

    public void setFinalColor(Color color) {
        this.finalColor.set(color);
    }

    public void setInitialSize(float f) {
        this.initialSize = f;
    }

    public void setFinalSize(float f) {
        this.finalSize = f;
    }

    public void setLifeTime(float f) {
        this.lifeTime = f;
    }

    public int step(StepContext stepContext) {
        this.currentLife -= stepContext.getDeltaTime();
        if (this.currentLife > 0.0f) {
            float f = 1.0f - (this.currentLife / this.lifeTime);
            this.light.setRadius((this.finalSize * f) + (this.initialSize * (1.0f - f)));
            this.light.setDiffuseColor(Color.interpolate(this.initialColor, this.finalColor, f));
        } else {
            hide();
        }
        super.step(stepContext);
        getAABB().mo9834a(this.globalTransform, this.aabbWorldSpace);
        this.globalTransform.getTranslation(this.light.getPosition());
        return super.step(stepContext);
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        return true;
    }

    public boolean isNeedToStep() {
        return true;
    }
}
