package taikodom.render.scene;

import game.geometry.Quat4fWrap;

/* compiled from: a */
public class BoneTransform {
    private int index;
    private Quat4fWrap orientation;

    public BoneTransform(int i, Quat4fWrap aoy) {
        this.index = i;
        this.orientation = new Quat4fWrap(aoy);
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int i) {
        this.index = i;
    }

    public Quat4fWrap getOrientation() {
        return this.orientation;
    }

    public void setOrientation(Quat4fWrap aoy) {
        this.orientation = aoy;
    }
}
