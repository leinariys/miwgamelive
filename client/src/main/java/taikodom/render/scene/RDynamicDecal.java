package taikodom.render.scene;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.*;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.RenderContext;
import taikodom.render.StepContext;
import taikodom.render.data.*;
import taikodom.render.enums.GeometryType;
import taikodom.render.helpers.FindInstancesVisitor;
import taikodom.render.primitives.Mesh;

import javax.vecmath.Vector3f;
import java.util.List;

/* compiled from: a */
public class RDynamicDecal extends RenderObject {
    static final float DECAL_EPSILON = 0.25f;
    static final int MAX_DECAL_VERTS = 256;
    final C0107BM backPlane = new C0107BM();
    final C0107BM bottomPlane = new C0107BM();
    final Vec3f decalBinormal = new Vec3f();
    final Vec3f decalCenter = new Vec3f();
    final Vec3f decalNormal = new Vec3f();
    final Vec3f decalTangent = new Vec3f();
    final C0107BM frontPlane = new C0107BM();
    final C0107BM leftPlane = new C0107BM();
    final C0107BM rightPlane = new C0107BM();
    final C0107BM topPlane = new C0107BM();
    private final Color initialColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    float currentLifeTime;
    boolean isValid = true;
    float lifeTimeInSeconds = 10.0f;
    float zOffset;
    private int decalTriangleCount = 0;
    private int decalVertexCount = 0;
    private ExpandableIndexData indexes;
    private Mesh mesh;
    private ExpandableVertexData vertexes;

    public RDynamicDecal(SceneObject sceneObject, Vec3d ajr, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, float f) {
        FindInstancesVisitor findInstancesVisitor = new FindInstancesVisitor(RMesh.class);
        sceneObject.visitPreOrder(findInstancesVisitor);
        List<RMesh> instances = findInstancesVisitor.getInstances();
        if (instances.size() == 0) {
            this.isValid = false;
            return;
        }
        sceneObject.getGlobalTransform().mo17333a(ajr, this.decalCenter);
        sceneObject.getGlobalTransform().orientation.mo13947a((Vector3f) vec3f, (Vector3f) this.decalNormal);
        sceneObject.getGlobalTransform().orientation.mo13947a((Vector3f) vec3f2, (Vector3f) this.decalTangent);
        this.decalBinormal.cross(this.decalNormal, this.decalTangent);
        float dot = this.decalCenter.dot(this.decalTangent);
        this.leftPlane.mo720c((double) this.decalTangent.x, (double) this.decalTangent.y, (double) this.decalTangent.z, (double) ((vec3f3.x * 0.5f) - dot));
        this.rightPlane.mo720c((double) (-this.decalTangent.x), (double) (-this.decalTangent.y), (double) (-this.decalTangent.z), (double) ((vec3f3.x * 0.5f) + dot));
        float dot2 = this.decalCenter.dot(this.decalBinormal);
        this.bottomPlane.mo720c((double) this.decalBinormal.x, (double) this.decalBinormal.y, (double) this.decalBinormal.z, (double) ((vec3f3.y * 0.5f) - dot2));
        this.topPlane.mo720c((double) (-this.decalBinormal.x), (double) (-this.decalBinormal.y), (double) (-this.decalBinormal.z), (double) ((vec3f3.y * 0.5f) + dot2));
        float dot3 = this.decalCenter.dot(this.decalNormal);
        this.frontPlane.mo720c((double) (-this.decalNormal.x), (double) (-this.decalNormal.y), (double) (-this.decalNormal.z), (double) (vec3f3.z + dot3));
        this.backPlane.mo720c((double) this.decalNormal.x, (double) this.decalNormal.y, (double) this.decalNormal.z, (double) (vec3f3.z - dot3));
        this.zOffset = f;
        initRenderStructs();
        for (RMesh mesh2 : instances) {
            clipMesh(mesh2.getMesh());
        }
        this.indexes.setSize(this.decalTriangleCount * 3);
        generateTexCoords(vec3f3);
    }

    private void generateTexCoords(Vec3f vec3f) {
        float f = 1.0f / vec3f.x;
        float f2 = 1.0f / vec3f.y;
        Vec3f vec3f2 = new Vec3f();
        Vec3f vec3f3 = new Vec3f();
        for (int i = 0; i < this.decalVertexCount; i++) {
            this.vertexes.getPosition(i, vec3f3);
            vec3f2.sub(vec3f3, this.decalCenter);
            this.vertexes.setTexCoord(i, 0, (vec3f2.dot(this.decalTangent) * f) + 0.5f, (vec3f2.dot(this.decalBinormal) * f2) + 0.5f);
        }
        Vec3f vec3f4 = new Vec3f();
        Vec3f vec3f5 = new Vec3f();
        Vec3f vec3f6 = new Vec3f();
        Vector2fWrap aka = new Vector2fWrap();
        Vector2fWrap aka2 = new Vector2fWrap();
        Vector2fWrap aka3 = new Vector2fWrap();
        Vec3f vec3f7 = new Vec3f();
        Vec3f vec3f8 = new Vec3f();
        Vector4fWrap ajf = new Vector4fWrap();
        for (int i2 = 0; i2 < this.decalTriangleCount * 3; i2 += 3) {
            int index = this.indexes.getIndex(i2);
            int index2 = this.indexes.getIndex(i2 + 1);
            int index3 = this.indexes.getIndex(i2 + 2);
            this.vertexes.getPosition(index, vec3f4);
            this.vertexes.getPosition(index2, vec3f5);
            this.vertexes.getPosition(index3, vec3f6);
            this.vertexes.getTexCoord(index, 1, aka);
            this.vertexes.getTexCoord(index2, 1, aka2);
            this.vertexes.getTexCoord(index3, 1, aka3);
            float f3 = vec3f5.x - vec3f4.x;
            float f4 = vec3f6.x - vec3f4.x;
            float f5 = vec3f5.y - vec3f4.y;
            float f6 = vec3f6.y - vec3f4.y;
            float f7 = vec3f5.z - vec3f4.z;
            float f8 = vec3f6.z - vec3f4.z;
            float f9 = aka2.x - aka.x;
            float f10 = aka3.x - aka.x;
            float f11 = aka2.y - aka.y;
            float f12 = aka3.y - aka.y;
            float f13 = (f9 * f12) - (f10 * f11);
            if (f13 == 0.0f) {
                vec3f7.x = 1.0f;
                vec3f8.z = 1.0f;
            } else {
                float f14 = 1.0f / f13;
                vec3f7.set(((f12 * f3) - (f11 * f4)) * f14, ((f12 * f5) - (f11 * f6)) * f14, ((f12 * f7) - (f11 * f8)) * f14);
                vec3f8.set(((f4 * f9) - (f3 * f10)) * f14, ((f9 * f6) - (f5 * f10)) * f14, ((f9 * f8) - (f10 * f7)) * f14);
                vec3f7.normalize();
                vec3f8.normalize();
            }
            ajf.set(vec3f7.x, vec3f7.y, vec3f7.z, 1.0f);
            this.vertexes.setTangents(index, ajf);
            this.vertexes.setTangents(index2, ajf);
            this.vertexes.setTangents(index3, ajf);
        }
    }

    private void clipMesh(Mesh mesh2) {
        Vec3f[] vec3fArr = new Vec3f[9];
        Vec3f[] vec3fArr2 = new Vec3f[9];
        for (int i = 0; i < 9; i++) {
            vec3fArr[i] = new Vec3f();
            vec3fArr2[i] = new Vec3f();
        }
        Vec3f vec3f = new Vec3f();
        Vec3f vec3f2 = new Vec3f();
        Vec3f vec3f3 = new Vec3f();
        Vec3f vec3f4 = new Vec3f();
        Vec3f vec3f5 = new Vec3f();
        Vec3f vec3f6 = new Vec3f();
        IndexData indexes2 = mesh2.getIndexes();
        VertexData vertexData = mesh2.getVertexData();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < indexes2.size() / 3) {
                int index = indexes2.getIndex(i3 * 3);
                int index2 = indexes2.getIndex((i3 * 3) + 1);
                int index3 = indexes2.getIndex((i3 * 3) + 2);
                vertexData.getPosition(index, vec3f);
                vertexData.getPosition(index2, vec3f2);
                vertexData.getPosition(index3, vec3f3);
                vec3f5.sub(vec3f2, vec3f);
                vec3f6.sub(vec3f3, vec3f);
                vec3f4.cross(vec3f5, vec3f6);
                if (this.decalNormal.dot(vec3f4) > DECAL_EPSILON * vec3f4.length()) {
                    vec3fArr[0].set(vec3f);
                    vec3fArr[1].set(vec3f2);
                    vec3fArr[2].set(vec3f3);
                    vertexData.getNormal(index, vec3fArr2[0]);
                    vertexData.getNormal(index2, vec3fArr2[1]);
                    vertexData.getNormal(index3, vec3fArr2[2]);
                    int clipPolygon = clipPolygon(3, vec3fArr, vec3fArr2, vec3fArr, vec3fArr2);
                    if (clipPolygon != 0 && !addPolygon(clipPolygon, vec3fArr, vec3fArr2)) {
                        return;
                    }
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private int clipPolygon(int i, Vec3f[] vec3fArr, Vec3f[] vec3fArr2, Vec3f[] vec3fArr3, Vec3f[] vec3fArr4) {
        Vec3f[] vec3fArr5 = new Vec3f[9];
        Vec3f[] vec3fArr6 = new Vec3f[9];
        for (int i2 = 0; i2 < 9; i2++) {
            vec3fArr5[i2] = new Vec3f();
            vec3fArr6[i2] = new Vec3f();
        }
        int clipPolygonAgainstPlane = clipPolygonAgainstPlane(this.leftPlane, i, vec3fArr, vec3fArr2, vec3fArr5, vec3fArr6);
        if (clipPolygonAgainstPlane == 0) {
            return clipPolygonAgainstPlane;
        }
        int clipPolygonAgainstPlane2 = clipPolygonAgainstPlane(this.rightPlane, clipPolygonAgainstPlane, vec3fArr5, vec3fArr6, vec3fArr3, vec3fArr4);
        if (clipPolygonAgainstPlane2 != 0) {
            int clipPolygonAgainstPlane3 = clipPolygonAgainstPlane(this.bottomPlane, clipPolygonAgainstPlane2, vec3fArr3, vec3fArr4, vec3fArr5, vec3fArr6);
            if (clipPolygonAgainstPlane3 == 0) {
                return clipPolygonAgainstPlane3;
            }
            clipPolygonAgainstPlane2 = clipPolygonAgainstPlane(this.topPlane, clipPolygonAgainstPlane3, vec3fArr5, vec3fArr6, vec3fArr3, vec3fArr4);
            if (clipPolygonAgainstPlane2 != 0) {
                int clipPolygonAgainstPlane4 = clipPolygonAgainstPlane(this.backPlane, clipPolygonAgainstPlane2, vec3fArr3, vec3fArr4, vec3fArr5, vec3fArr6);
                if (clipPolygonAgainstPlane4 == 0) {
                    return clipPolygonAgainstPlane4;
                }
                return clipPolygonAgainstPlane(this.frontPlane, clipPolygonAgainstPlane4, vec3fArr5, vec3fArr6, vec3fArr3, vec3fArr4);
            }
        }
        return clipPolygonAgainstPlane2;
    }

    private int clipPolygonAgainstPlane(C0107BM bm, int i, Vec3f[] vec3fArr, Vec3f[] vec3fArr2, Vec3f[] vec3fArr3, Vec3f[] vec3fArr4) {
        int i2;
        int i3;
        boolean[] zArr = new boolean[10];
        long j = 0;
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= i) {
                break;
            }
            boolean z = bm.getNormal().mo9494b((IGeometryF) vec3fArr[i5]) + bm.dnz() < ScriptRuntime.NaN;
            zArr[i5] = z;
            if (z) {
                i3 = 1;
            } else {
                i3 = 0;
            }
            j += (long) i3;
            i4 = i5 + 1;
        }
        if (j == ((long) i)) {
            return 0;
        }
        int i6 = 0;
        int i7 = 0;
        while (true) {
            int i8 = i7;
            if (i8 >= i) {
                return i6;
            }
            int i9 = i8 != 0 ? i8 - 1 : i - 1;
            if (!zArr[i8]) {
                if (zArr[i9]) {
                    Vec3f vec3f = vec3fArr[i8];
                    Vec3f vec3f2 = vec3fArr[i9];
                    Vec3d normal = bm.getNormal();
                    float b = (float) ((normal.mo9494b((IGeometryF) vec3f) + bm.dnz()) / ((normal.z * ((double) (vec3f.z - vec3f2.z))) + ((normal.x * ((double) (vec3f.x - vec3f2.x))) + (normal.y * ((double) (vec3f.y - vec3f2.y))))));
                    vec3fArr3[i6].set(vec3f.mo23510mS(1.0f - b).mo23503i(vec3f2.mo23510mS(b)));
                    vec3fArr4[i6].set(vec3fArr2[i8].mo23510mS(1.0f - b).mo23503i(vec3fArr2[i9].mo23510mS(b)));
                    i2 = i6 + 1;
                } else {
                    i2 = i6;
                }
                vec3fArr3[i2].set(vec3fArr[i8]);
                vec3fArr4[i2].set(vec3fArr2[i8]);
                i6 = i2 + 1;
            } else if (!zArr[i9]) {
                Vec3f vec3f3 = vec3fArr[i9];
                Vec3f vec3f4 = vec3fArr[i8];
                Vec3d normal2 = bm.getNormal();
                float b2 = (float) ((normal2.mo9494b((IGeometryF) vec3f3) + bm.dnz()) / ((normal2.z * ((double) (vec3f3.z - vec3f4.z))) + ((normal2.x * ((double) (vec3f3.x - vec3f4.x))) + (normal2.y * ((double) (vec3f3.y - vec3f4.y))))));
                vec3fArr3[i6].set(vec3f3.mo23510mS(1.0f - b2).mo23503i(vec3f4.mo23510mS(b2)));
                vec3fArr4[i6].set(vec3fArr2[i9].mo23510mS(1.0f - b2).mo23503i(vec3fArr2[i8].mo23510mS(b2)));
                i6++;
            }
            i7 = i8 + 1;
        }
    }

    private boolean addPolygon(int i, Vec3f[] vec3fArr, Vec3f[] vec3fArr2) {
        int i2 = this.decalVertexCount;
        if (i2 + i >= 256) {
            return false;
        }
        int i3 = this.decalTriangleCount * 3;
        this.decalTriangleCount += i - 2;
        for (int i4 = 2; i4 < i; i4++) {
            this.indexes.setIndex(i3, i2);
            this.indexes.setIndex(i3 + 1, (i2 + i4) - 1);
            this.indexes.setIndex(i3 + 2, i2 + i4);
            i3 += 3;
        }
        Vec3f vec3f = new Vec3f();
        for (int i5 = 0; i5 < i; i5++) {
            vec3f.set(0.0f, 0.0f, 0.0f);
            vec3f.scaleAdd(this.zOffset, vec3fArr2[i5], vec3fArr[i5]);
            this.vertexes.setPosition(i2, vec3f);
            this.vertexes.setNormal(i2, vec3fArr2[i5]);
            this.mesh.getAabb().addPoint(vec3fArr[i5]);
            i2++;
        }
        this.decalVertexCount = i2;
        return true;
    }

    private void initRenderStructs() {
        this.vertexes = new ExpandableVertexData(new VertexLayout(true, false, true, true, 1), 256);
        this.indexes = new ExpandableIndexData(256);
        this.indexes.clear();
        this.vertexes.data().clear();
        this.mesh = new Mesh();
        this.mesh.setName("RDynamicDecal_mesh");
        this.mesh.setGeometryType(GeometryType.TRIANGLES);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setIndexes(this.indexes);
    }

    public boolean render(RenderContext renderContext, boolean z) {
        if (!this.render || !super.render(renderContext, z)) {
            return false;
        }
        if (this.indexes.size() > 0) {
            this.mesh.getAabb().mo9834a(this.globalTransform, this.aabbWorldSpace);
            if (!z || renderContext.getFrustum().mo1160a(this.aabbWorldSpace)) {
                renderContext.addPrimitive(this.material, this.mesh, this.globalTransform, this.primitiveColor, this.occlusionQuery, this.renderPriority, this, this.aabbWorldSpace, this.currentSquaredDistanceToCamera);
                renderContext.incMeshesRendered();
            }
        }
        return true;
    }

    public boolean isNeedToStep() {
        return true;
    }

    public float getLifeTimeInSeconds() {
        return this.lifeTimeInSeconds;
    }

    public void setLifeTimeInSeconds(float f) {
        this.lifeTimeInSeconds = f;
    }

    public int step(StepContext stepContext) {
        this.currentLifeTime += stepContext.getDeltaTime();
        if (this.currentLifeTime > this.lifeTimeInSeconds) {
            dispose();
            return 1;
        }
        this.primitiveColor.scale(1.0f - (this.currentLifeTime / this.lifeTimeInSeconds), this.initialColor);
        return 0;
    }
}
