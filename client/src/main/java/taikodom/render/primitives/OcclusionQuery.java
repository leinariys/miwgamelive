package taikodom.render.primitives;

import taikodom.render.gl.GLSupportedCaps;

import javax.media.opengl.GL;

/* compiled from: a */
public class OcclusionQuery {
    private boolean initialized = false;
    private int queryId = -1;

    public void begin(GL gl) {
        if (this.initialized && this.queryId != -1) {
            gl.glBeginQuery(35092, this.queryId);
        }
    }

    public void end(GL gl) {
        if (this.initialized && this.queryId != -1) {
            gl.glEndQuery(35092);
        }
    }

    public float getResult(float f, GL gl) {
        if ((!this.initialized || this.queryId == -1 || !gl.glIsQuery(this.queryId)) && !create(gl)) {
            return 0.0f;
        }
        int[] iArr = new int[1];
        gl.glGetQueryObjectiv(this.queryId, 34918, iArr, 0);
        return ((float) iArr[0]) / f;
    }

    private boolean create(GL gl) {
        if (!this.initialized) {
            this.initialized = true;
            if (!GLSupportedCaps.isOcclusionQuery()) {
                return false;
            }
            int[] iArr = new int[1];
            gl.glGenQueries(1, iArr, 0);
            this.queryId = iArr[0];
            gl.glBeginQuery(35092, this.queryId);
            gl.glEndQuery(35092);
            if (!gl.glIsQuery(this.queryId)) {
                this.queryId = -1;
            }
            if (this.queryId == -1) {
                return false;
            }
            return true;
        } else if (this.queryId != -1) {
            return true;
        } else {
            return false;
        }
    }

    public void dispose(GL gl) {
        gl.glDeleteQueries(1, new int[]{this.queryId}, 0);
    }
}
