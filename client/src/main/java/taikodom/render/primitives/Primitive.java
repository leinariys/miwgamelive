package taikodom.render.primitives;

import com.hoplon.geometry.Vec3f;
import game.geometry.C2567gu;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public abstract class Primitive extends RenderAsset {
    public Primitive() {
    }

    public Primitive(Primitive primitive) {
        super(primitive);
    }

    public abstract void bind(DrawContext drawContext);

    public abstract void draw(DrawContext drawContext);

    public abstract boolean getUseVBO();

    public abstract C2567gu rayIntersects(C2567gu guVar);

    public C2567gu rayIntersects(Vec3f vec3f, Vec3f vec3f2) {
        return rayIntersects(new C2567gu(vec3f, vec3f2));
    }
}
