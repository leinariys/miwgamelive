package taikodom.render.primitives;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.geometry.aVS;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.camera.Camera;
import taikodom.render.enums.LightType;
import taikodom.render.loader.RenderAsset;
import util.Maths;

import javax.media.opengl.GL;
import javax.vecmath.Tuple3d;

/* compiled from: a */
public class Light extends RenderAsset {
    public final Color diffuseColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public final Vec3f direction = new Vec3f(0.0f, 0.0f, 1.0f);
    public final Vec3f globalDirection = new Vec3f(1.0f, 1.0f, 1.0f);
    public final Color primitiveColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public final Color specularColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private final aVS boundingSphere = new aVS();
    public float cutoff;
    public float exponent;
    public boolean isMainLight;
    public LightType type = LightType.POINT_LIGHT;
    int scBottom;
    int scLeft;
    int scRight;
    int scTop;
    private float cosCutoff;
    private double currentDistanceToCamera;
    private Vec3d position = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
    private float radius;
    private float spotExponent;

    public Light() {
    }

    public Light(Light light) {
        setRadius(light.radius);
        this.type = light.type;
        this.exponent = light.exponent;
        this.cutoff = light.cutoff;
        this.isMainLight = light.isMainLight;
        this.primitiveColor.set(light.primitiveColor);
        this.diffuseColor.set(light.diffuseColor);
        this.specularColor.set(light.specularColor);
        this.direction.set(light.direction);
        this.globalDirection.set(light.globalDirection);
        this.boundingSphere.mo11771ag((double) this.radius);
    }

    public Vec3d getPosition() {
        return this.position;
    }

    public void setPosition(Vec3d ajr) {
        this.position.mo9484aA(ajr);
        this.boundingSphere.mo11774h(ajr);
    }

    public float getRadius() {
        return this.radius;
    }

    public void setRadius(float f) {
        this.radius = f;
        this.boundingSphere.mo11771ag((double) f);
    }

    public float getCosCutoff() {
        return this.cosCutoff;
    }

    public float getSpotExponent() {
        return this.spotExponent;
    }

    public boolean setupScissorRectangle(RenderContext renderContext) {
        double d;
        int i;
        int i2;
        double nearPlane;
        double nearPlane2;
        DrawContext dc = renderContext.getDc();
        GL gl = dc.getGl();
        double d2 = -1.0d;
        double d3 = 1.0d;
        double d4 = 1.0d;
        if (this.type == LightType.DIRECTIONAL_LIGHT) {
            gl.glDisable(3089);
            return true;
        }
        gl.glEnable(3089);
        if (this.radius == 0.0f) {
            return false;
        }
        Vec3d ajr = renderContext.vec3dTemp0;
        ajr.sub(this.position, renderContext.getGpuOffset());
        renderContext.cameraAffineTransform.mo14000b((Tuple3d) ajr, (Tuple3d) ajr);
        Camera camera = renderContext.getCamera();
        double O = Maths.tanPi180(((double) camera.getFovY()) * 0.5d) * ((double) camera.getNearPlane());
        double aspect = O * ((double) camera.getAspect());
        Vec3d ajr2 = renderContext.vec3dTemp1;
        double d5 = ajr.z * ajr.z * (((ajr.x * ajr.x) + (ajr.z * ajr.z)) - ((double) (this.radius * this.radius)));
        if (d5 > ScriptRuntime.NaN) {
            double sqrt = Math.sqrt(d5);
            int i3 = 0;
            while (i3 < 2) {
                if (i3 == 0) {
                    ajr2.x = (((double) this.radius) * ajr.x) + sqrt;
                } else {
                    ajr2.x = (((double) this.radius) * ajr.x) - sqrt;
                }
                ajr2.x /= (ajr.x * ajr.x) + (ajr.z * ajr.z);
                ajr2.z = ((double) this.radius) - (ajr2.x * ajr.x);
                ajr2.z /= ajr.z;
                if (ajr2.x == ScriptRuntime.NaN) {
                    nearPlane2 = d3;
                } else {
                    Vec3d ajr3 = renderContext.vec3dTemp2;
                    ajr3.z = ((ajr.x * ajr.x) + (ajr.z * ajr.z)) - ((double) (this.radius * this.radius));
                    ajr3.z /= ajr.z - ((ajr2.z / ajr2.x) * ajr.x);
                    if (ajr3.z >= ScriptRuntime.NaN) {
                        nearPlane2 = d3;
                    } else {
                        ajr3.x = ((-ajr3.z) * ajr2.z) / ajr2.x;
                        nearPlane2 = (ajr2.z * ((double) camera.getNearPlane())) / (ajr2.x * aspect);
                        if (ajr3.x < ajr.x && nearPlane2 > d2) {
                            d2 = nearPlane2;
                        }
                        if (ajr3.x <= ajr.x || nearPlane2 >= d3) {
                            nearPlane2 = d3;
                        }
                    }
                }
                i3++;
                d3 = nearPlane2;
            }
        }
        ajr2.x = ScriptRuntime.NaN;
        double d6 = ajr.z * ajr.z * (((ajr.y * ajr.y) + (ajr.z * ajr.z)) - ((double) (this.radius * this.radius)));
        if (d6 > ScriptRuntime.NaN) {
            double sqrt2 = Math.sqrt(d6);
            int i4 = 0;
            d = -1.0d;
            while (i4 < 2) {
                if (i4 == 0) {
                    ajr2.y = (((double) this.radius) * ajr.y) + sqrt2;
                } else {
                    ajr2.y = (((double) this.radius) * ajr.y) - sqrt2;
                }
                ajr2.y /= (ajr.y * ajr.y) + (ajr.z * ajr.z);
                ajr2.z = ((double) this.radius) - (ajr2.y * ajr.y);
                ajr2.z /= ajr.z;
                if (ajr2.y == ScriptRuntime.NaN) {
                    nearPlane = d4;
                } else {
                    Vec3d ajr4 = renderContext.vec3dTemp2;
                    ajr4.z = ((ajr.y * ajr.y) + (ajr.z * ajr.z)) - ((double) (this.radius * this.radius));
                    ajr4.z /= ajr.z - ((ajr2.z / ajr2.y) * ajr.y);
                    if (ajr4.z >= ScriptRuntime.NaN) {
                        nearPlane = d4;
                    } else {
                        ajr4.y = ((-ajr4.z) * ajr2.z) / ajr2.y;
                        nearPlane = (ajr2.z * ((double) camera.getNearPlane())) / (ajr2.y * O);
                        if (ajr4.y < ajr.y && nearPlane > d) {
                            d = nearPlane;
                        }
                        if (ajr4.y <= ajr.y || nearPlane >= d4) {
                            nearPlane = d4;
                        }
                    }
                }
                i4++;
                d4 = nearPlane;
            }
        } else {
            d = -1.0d;
        }
        int screenWidth = (int) ((((double) dc.getScreenWidth()) * (1.0d + d2)) / 2.0d);
        int screenHeight = (int) ((((double) dc.getScreenHeight()) * (1.0d + d)) / 2.0d);
        int screenWidth2 = (int) (((d3 - d2) * ((double) dc.getScreenWidth())) / 2.0d);
        int screenHeight2 = (int) (((d4 - d) * ((double) dc.getScreenHeight())) / 2.0d);
        if (screenWidth < 0) {
            i = 0;
        } else if (screenWidth > dc.getScreenWidth() - 1) {
            return false;
        } else {
            i = screenWidth;
        }
        if (screenHeight < 0) {
            i2 = 0;
        } else if (screenHeight > dc.getScreenHeight() - 1) {
            return false;
        } else {
            i2 = screenHeight;
        }
        if (screenWidth2 < 0) {
            screenWidth2 = 0;
        } else if (i + screenWidth2 > dc.getScreenWidth() - 1) {
            screenWidth2 = (dc.getScreenWidth() - 1) - i;
        }
        if (screenHeight2 < 0) {
            screenHeight2 = 0;
        } else if (i2 + screenHeight2 > dc.getScreenHeight() - 1) {
            screenHeight2 = (dc.getScreenHeight() - 1) - i2;
        }
        this.scLeft = dc.getScreenOffsetX() + i;
        this.scRight = i + screenWidth2;
        this.scBottom = dc.getScreenOffsetY() + i2;
        this.scTop = i2 + screenHeight2;
        gl.glScissor(i + dc.getScreenOffsetX(), i2 + dc.getScreenOffsetY(), screenWidth2, screenHeight2);
        return true;
    }

    public LightType getLightType() {
        return this.type;
    }

    public aVS getBoundingSphere() {
        return this.boundingSphere;
    }

    public RenderAsset cloneAsset() {
        return new Light(this);
    }

    public LightType getType() {
        return this.type;
    }

    public void setType(LightType lightType) {
        this.type = lightType;
    }

    public Color getDiffuseColor() {
        return this.diffuseColor;
    }

    public void setDiffuseColor(Color color) {
        this.diffuseColor.set(color);
    }

    public Color getSpecularColor() {
        return this.specularColor;
    }

    public void setSpecularColor(Color color) {
        this.specularColor.set(color);
    }

    public Vec3f getDirection() {
        return this.direction;
    }

    public void setDirection(Vec3f vec3f) {
        this.direction.set(vec3f);
    }

    public Vec3f getGlobalDirection() {
        return this.globalDirection;
    }

    public void setGlobalDirection(Vec3f vec3f) {
        this.globalDirection.set(vec3f);
    }

    public float getExponent() {
        return this.exponent;
    }

    public void setExponent(float f) {
        this.exponent = f;
    }

    public float getCutoff() {
        return this.cutoff;
    }

    public void setCutoff(float f) {
        this.cutoff = f;
    }

    public Color getPrimitiveColor() {
        return this.primitiveColor;
    }

    public void setPrimitiveColor(Color color) {
        this.primitiveColor.set(color);
    }

    public boolean isMainLight() {
        return this.isMainLight;
    }

    public void setMainLight(boolean z) {
        this.isMainLight = z;
    }

    public double getCurrentDistanceToCamera() {
        return this.currentDistanceToCamera;
    }

    public void setCurrentDistanceToCamera(double d) {
        this.currentDistanceToCamera = d;
    }
}
