package taikodom.render.primitives;

import com.hoplon.geometry.Vec3f;
import game.geometry.C2567gu;
import game.geometry.aLH;
import logic.render.granny.*;
import taikodom.render.DrawContext;
import taikodom.render.data.IndexData;
import taikodom.render.data.VertexData;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;

import java.nio.Buffer;

/* compiled from: a */
public class Mesh extends Primitive implements PrimitiveBase {
    private final aLH aabb = new aLH();
    public VertexData data;
    public GeometryType geometryType = GeometryType.TRIANGLES;
    public IndexData indexes;
    private C6796ato grannyBinding;
    private C6604aqE grannyDeformer;
    private int grannyMaterialIndex;
    private C1126QZ grannyMesh;
    private C0913NP grannySkeleton;
    private boolean rigid = true;

    public Mesh() {
    }

    public Mesh(Mesh mesh) {
        super(mesh);
        this.data = mesh.data;
        this.indexes = mesh.indexes.duplicate();
        this.rigid = mesh.rigid;
        this.grannyMesh = mesh.grannyMesh;
        this.grannyBinding = mesh.grannyBinding;
        this.grannyDeformer = mesh.grannyDeformer;
        this.grannySkeleton = mesh.grannySkeleton;
    }

    public boolean isRidig() {
        return this.rigid;
    }

    public VertexData getVertexData() {
        return this.data;
    }

    public void setVertexData(VertexData vertexData) {
        this.data = vertexData;
    }

    public IndexData getIndexes() {
        return this.indexes;
    }

    public void setIndexes(IndexData indexData) {
        this.indexes = indexData;
    }

    public GeometryType getGeometryType() {
        return this.geometryType;
    }

    public void setGeometryType(GeometryType geometryType2) {
        this.geometryType = geometryType2;
    }

    public void bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        try {
            this.data.bind(drawContext);
            this.indexes.bind(drawContext);
            drawContext.incNumPrimitiveBind();
        } catch (Exception e) {
            System.err.println("Problem with mesh " + getName() + ". " + e.getMessage() + ". Its using a shader that uses missing mesh properties: shader " + (drawContext.currentPass != null ? drawContext.currentPass.getName() : "unknown"));
            throw new RuntimeException("Missing mesh properties. Need to export it again");
        }
    }

    public void draw(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logDraw(this);
        }
        if (this.indexes.size() > 0) {
            Buffer buffer = this.indexes.buffer();
            if (buffer != null) {
                drawContext.getGl().glDrawElements(this.geometryType.glEquivalent(), this.indexes.size(), this.indexes.getGLType(), buffer);
            } else {
                drawContext.getGl().glDrawElements(this.geometryType.glEquivalent(), this.indexes.size(), this.indexes.getGLType(), 0);
            }
            drawContext.incNumDrawCalls();
            drawContext.incNumTriangles(this.indexes.size() / 3);
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public C1126QZ getGrannyMesh() {
        return this.grannyMesh;
    }

    public void setGrannyMesh(C1126QZ qz) {
        this.grannyMesh = qz;
        this.rigid = AdapterGrannyJNI.m4880g(qz);
    }

    public boolean getUseVBO() {
        return this.data.isUseVBO();
    }

    public void computeLocalAabb() {
        this.aabb.reset();
        Vec3f vec3f = new Vec3f();
        if (!(this.indexes == null || this.data == null)) {
            for (int i = 0; i < this.indexes.size(); i++) {
                this.data.getPosition(this.indexes.getIndex(i), vec3f);
                this.aabb.addPoint(vec3f);
            }
        }
        if (!this.aabb.din().isValid()) {
            this.aabb.addPoint(vec3f);
        }
    }

    public aLH getAabb() {
        return this.aabb;
    }

    public C2567gu rayIntersects(C2567gu guVar) {
        if (this.indexes != null && this.data != null) {
            Vec3f[] vec3fArr = {new Vec3f(), new Vec3f(), new Vec3f()};
            aSH ash = new aSH(vec3fArr[0], vec3fArr[1], vec3fArr[2]);
            for (int i = 0; i < this.indexes.size(); i += 3) {
                this.data.getPosition(this.indexes.getIndex(i), vec3fArr[0]);
                this.data.getPosition(this.indexes.getIndex(i + 1), vec3fArr[1]);
                this.data.getPosition(this.indexes.getIndex(i + 2), vec3fArr[2]);
                ash.mo11347q(vec3fArr[0], vec3fArr[1], vec3fArr[2]);
                ash.rayIntersects(guVar);
                if (guVar.isIntersected()) {
                    break;
                }
            }
        }
        return guVar;
    }

    public C6796ato getGrannyBinding() {
        return this.grannyBinding;
    }

    public void setGrannyBinding(C6796ato ato) {
        this.grannyBinding = ato;
    }

    public C6604aqE getGrannyDeformer() {
        return this.grannyDeformer;
    }

    public void setGrannyDeformer(C6604aqE aqe) {
        this.grannyDeformer = aqe;
    }

    public int getGrannyMaterialIndex() {
        return this.grannyMaterialIndex;
    }

    public void setGrannyMaterialIndex(int i) {
        this.grannyMaterialIndex = i;
    }

    public C0913NP getGrannySkeleton() {
        return this.grannySkeleton;
    }

    public void setGrannySkeleton(C0913NP np) {
        this.grannySkeleton = np;
    }
}
