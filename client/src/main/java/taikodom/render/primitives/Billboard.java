package taikodom.render.primitives;

import com.hoplon.geometry.Vec3f;
import game.geometry.C2567gu;
import taikodom.render.DrawContext;
import taikodom.render.data.InterleavedVertexData;
import taikodom.render.data.VertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class Billboard extends Primitive implements PrimitiveBase {

    /* renamed from: p0 */
    public static final Vec3f f10359p0 = new Vec3f(-0.5f, -0.5f, 0.0f);

    /* renamed from: p1 */
    public static final Vec3f f10360p1 = new Vec3f(0.5f, -0.5f, 0.0f);

    /* renamed from: p2 */
    public static final Vec3f f10361p2 = new Vec3f(0.5f, 0.5f, 0.0f);

    /* renamed from: p3 */
    public static final Vec3f f10362p3 = new Vec3f(-0.5f, 0.5f, 0.0f);
    public VertexData data = new InterleavedVertexData(VertexLayout.defaultLayout, 4);

    public Billboard() {
        this.data.setPosition(0, f10359p0.x, f10359p0.y, f10359p0.z);
        this.data.setPosition(1, f10360p1.x, f10360p1.y, f10360p1.z);
        this.data.setPosition(2, f10361p2.x, f10361p2.y, f10361p2.z);
        this.data.setPosition(3, f10362p3.x, f10362p3.y, f10362p3.z);
        this.data.setTexCoord(0, 0, 0.0f, 0.0f);
        this.data.setTexCoord(1, 0, 1.0f, 0.0f);
        this.data.setTexCoord(2, 0, 1.0f, 1.0f);
        this.data.setTexCoord(3, 0, 0.0f, 1.0f);
        this.data.setNormal(0, 0.0f, 0.0f, 1.0f);
        this.data.setNormal(1, 0.0f, 0.0f, 1.0f);
        this.data.setNormal(2, 0.0f, 0.0f, 1.0f);
        this.data.setNormal(3, 0.0f, 0.0f, 1.0f);
        this.data.setTangents(0, 1.0f, 0.0f, 0.0f, 1.0f);
        this.data.setTangents(1, 1.0f, 0.0f, 0.0f, 1.0f);
        this.data.setTangents(2, 1.0f, 0.0f, 0.0f, 1.0f);
        this.data.setTangents(3, 1.0f, 0.0f, 0.0f, 1.0f);
        this.data.setColor(0, 1.0f, 1.0f, 1.0f, 1.0f);
        this.data.setColor(1, 1.0f, 1.0f, 1.0f, 1.0f);
        this.data.setColor(2, 1.0f, 1.0f, 1.0f, 1.0f);
        this.data.setColor(3, 1.0f, 1.0f, 1.0f, 1.0f);
    }

    public void bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        this.data.bind(drawContext);
        drawContext.incNumPrimitiveBind();
    }

    public void draw(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logDraw(this);
        }
        drawContext.getGl().glDrawArrays(7, 0, 4);
        drawContext.incNumDrawCalls();
        drawContext.incNumTriangles(2);
    }

    public boolean getUseVBO() {
        return this.data.isUseVBO();
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public C2567gu rayIntersects(C2567gu guVar) {
        aSH ash = new aSH();
        ash.mo11339bS(f10359p0);
        ash.mo11340bT(f10360p1);
        ash.mo11341bU(f10362p3);
        ash.rayIntersects(guVar);
        if (!guVar.isIntersected()) {
            ash.mo11339bS(f10359p0);
            ash.mo11340bT(f10362p3);
            ash.mo11341bU(f10360p1);
            ash.rayIntersects(guVar);
            if (!guVar.isIntersected()) {
                ash.mo11339bS(f10360p1);
                ash.mo11340bT(f10361p2);
                ash.mo11341bU(f10362p3);
                ash.rayIntersects(guVar);
                if (!guVar.isIntersected()) {
                    ash.mo11339bS(f10360p1);
                    ash.mo11340bT(f10362p3);
                    ash.mo11341bU(f10361p2);
                    ash.rayIntersects(guVar);
                }
            }
        }
        return guVar;
    }
}
