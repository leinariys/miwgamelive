package taikodom.render.primitives;

import com.hoplon.geometry.Color;
import game.geometry.Matrix4fWrap;
import game.geometry.TransformWrap;
import game.geometry.aLH;
import taikodom.render.scene.RenderObject;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;

/* compiled from: a */
public class TreeRecord {
    public final aLH cameraSpaceBoundingBox = new aLH();
    public final Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public final Matrix4fWrap gpuTransform = new Matrix4fWrap();
    public final TransformWrap transform = new TransformWrap();
    public RenderAreaInfo areaInfo;
    public int entryPriority;
    public boolean isGuiItem;
    public Material material;
    public OcclusionQuery occlusionQuery;
    public Primitive primitive;
    public RenderObject renderObject;
    public Shader shader;
    public double squaredDistanceToCamera;
}
