package taikodom.render;

import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.SoundSource;

/* compiled from: a */
public class PitchEntry extends RenderAsset {
    final float PITCH_ENTRY_GAIN_CORRECT;
    SoundBuffer buffer;
    float currentGain;
    float currentPitch;
    float finalControllerValue;
    float finalPitchValue;
    float gainMultiplier;
    float initialControllerValue;
    float initialPitchValue;
    boolean isFirst;
    boolean isLast;
    float parentGain;
    SoundSource source;

    public PitchEntry() {
        this.PITCH_ENTRY_GAIN_CORRECT = 1.5f;
        this.source = new MilesSoundSource();
        this.initialControllerValue = 0.0f;
        this.finalControllerValue = 1.0f;
        this.initialPitchValue = 1.0f;
        this.finalPitchValue = 1.0f;
        this.currentPitch = 0.0f;
        this.currentGain = 0.0f;
        this.isFirst = false;
        this.isLast = false;
        this.parentGain = 1.0f;
        this.buffer = null;
        this.gainMultiplier = 1.0f;
    }

    PitchEntry(PitchEntry pitchEntry) {
        this.PITCH_ENTRY_GAIN_CORRECT = 1.5f;
        this.source = new MilesSoundSource();
        if (pitchEntry.getSoundBuffer() != null) {
            setSoundBuffer(pitchEntry.getSoundBuffer().cloneAsset());
        } else {
            this.buffer = null;
        }
        this.initialControllerValue = pitchEntry.initialControllerValue;
        this.finalControllerValue = pitchEntry.finalControllerValue;
        this.initialPitchValue = pitchEntry.initialPitchValue;
        this.finalPitchValue = pitchEntry.finalPitchValue;
        this.currentPitch = pitchEntry.currentPitch;
        this.currentGain = pitchEntry.currentGain;
        this.isFirst = pitchEntry.isFirst;
        this.isLast = pitchEntry.isLast;
        this.parentGain = pitchEntry.parentGain;
        this.gainMultiplier = pitchEntry.gainMultiplier;
    }

    public void releaseReferences() {
        if (this.source != null) {
            this.source.releaseReferences();
        }
    }

    public float getInitialControllerValue() {
        return this.initialControllerValue;
    }

    public void setInitialControllerValue(float f) {
        this.initialControllerValue = f;
    }

    public float getFinalControllerValue() {
        return this.finalControllerValue;
    }

    public void setFinalControllerValue(float f) {
        this.finalControllerValue = f;
    }

    public float getInitialPitchValue() {
        return this.initialPitchValue;
    }

    public void setInitialPitchValue(float f) {
        this.initialPitchValue = f;
    }

    public float getFinalPitchValue() {
        return this.finalPitchValue;
    }

    public void setFinalPitchValue(float f) {
        this.finalPitchValue = f;
    }

    public boolean getFirst() {
        return this.isFirst;
    }

    public void setFirst(boolean z) {
        this.isFirst = z;
    }

    public boolean getLast() {
        return this.isLast;
    }

    public void setLast(boolean z) {
        this.isLast = z;
    }

    public void setParentGain(float f) {
        this.parentGain = f;
    }

    public SoundSource getSoundSource() {
        return this.source;
    }

    public void setSoundSource(SoundSource soundSource) {
        this.source = soundSource;
    }

    public SoundBuffer getSoundBuffer() {
        return this.buffer;
    }

    public void setSoundBuffer(SoundBuffer soundBuffer) {
        this.buffer = soundBuffer;
        this.source.setBuffer(this.buffer);
    }

    public boolean update(float f, StepContext stepContext) {
        float f2 = -1.0f;
        float f3 = this.finalControllerValue - this.initialControllerValue;
        if (f3 == 0.0f) {
            return false;
        }
        float f4 = this.currentGain;
        float f5 = this.currentPitch;
        float f6 = (f - this.initialControllerValue) / f3;
        this.currentPitch = ((this.finalPitchValue - this.initialPitchValue) * f6) + this.initialPitchValue;
        float f7 = (f6 * 2.0f) - 1.0f;
        if (f7 > 1.0f) {
            f7 = 1.0f;
        }
        if (f7 >= -1.0f) {
            f2 = f7;
        }
        this.currentGain = (float) Math.cos(((double) f2) * 1.5707963267948966d);
        this.currentGain *= 1.5f * this.gainMultiplier;
        this.currentGain = this.currentGain > 1.0f ? 1.0f : this.currentGain;
        this.currentGain = this.currentGain < 0.0f ? 0.0f : this.currentGain;
        if (this.isFirst && f2 < 0.0f) {
            this.currentGain = 1.0f;
        }
        if (this.isLast && f2 > 0.0f) {
            this.currentGain = 1.0f;
        }
        this.currentGain *= this.parentGain;
        if (this.source != null) {
            if (this.currentGain <= 0.0f) {
                this.source.stop();
            } else {
                if (!this.source.isPlaying()) {
                    this.source.play(0);
                }
                if (f4 != this.currentGain) {
                    this.source.setGain(this.currentGain);
                }
                if (f5 != this.currentPitch) {
                    this.source.setPitch(this.currentPitch);
                }
            }
            this.source.step(stepContext);
        }
        return true;
    }

    public float getGainMultiplier() {
        return this.gainMultiplier;
    }

    public void setGainMultiplier(float f) {
        this.gainMultiplier = f;
    }

    public PitchEntry cloneAsset() {
        return new PitchEntry(this);
    }
}
