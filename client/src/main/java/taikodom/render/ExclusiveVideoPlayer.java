package taikodom.render;

import org.mozilla1.javascript.ScriptRuntime;

import javax.media.opengl.GL;

/* compiled from: a */
public class ExclusiveVideoPlayer {
    boolean playing = false;
    Video video;

    public void playVideo(Video video2) {
        this.video = video2;
        this.playing = true;
    }

    public void renderFrame(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        gl.glMatrixMode(5888);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glMatrixMode(5889);
        gl.glPushMatrix();
        gl.glLoadIdentity();
        gl.glViewport(0, 0, drawContext.getScreenWidth(), drawContext.getScreenHeight());
        gl.glDisable(2929);
        gl.glOrtho(ScriptRuntime.NaN, (double) drawContext.getScreenWidth(), (double) drawContext.getScreenHeight(), ScriptRuntime.NaN, -1.0d, 1.0d);
        gl.glMatrixMode(5888);
        gl.glDisable(2884);
        this.video.update(drawContext);
        this.video.render(drawContext);
        if (this.video.isEnded()) {
            this.playing = false;
            drawContext.postSceneEvent("endVideo", this);
        }
        gl.glPopMatrix();
        gl.glMatrixMode(5889);
        gl.glPopMatrix();
        gl.glMatrixMode(5888);
    }

    public boolean isPlaying() {
        return this.playing;
    }

    public void stopVideo() {
        this.playing = false;
        this.video.stop();
    }
}
