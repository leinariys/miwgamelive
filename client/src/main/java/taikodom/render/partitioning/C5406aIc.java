package taikodom.render.partitioning;

import taikodom.render.helpers.RenderVisitor;
import taikodom.render.scene.SceneObject;

/* renamed from: a.aIc  reason: case insensitive filesystem */
/* compiled from: a */
public class C5406aIc implements RenderVisitor {
    final /* synthetic */ TkdWorld iaK;

    public C5406aIc(TkdWorld tkdWorld) {
        this.iaK = tkdWorld;
    }

    public boolean visit(SceneObject sceneObject) {
        if (!sceneObject.isNeedToStep()) {
            return true;
        }
        this.iaK.stepableObjects.add(sceneObject);
        return true;
    }
}
