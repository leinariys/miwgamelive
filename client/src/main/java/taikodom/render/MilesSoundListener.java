package taikodom.render;

import com.hoplon.geometry.Vec3f;
import logic.render.miles.AdapterMilesBridgeJNI;
import logic.render.miles.C1467Vb;
import logic.render.miles.aNZ;
import taikodom.render.loader.RenderAsset;

/* compiled from: a */
public class MilesSoundListener extends SoundListener {
    static int totalMilesSounds;
    public final Vec3f frontDir = new Vec3f();
    public final Vec3f upDir = new Vec3f();
    boolean isFadingOut;
    C1467Vb milesSample;
    Vec3f pos = new Vec3f(0.0f, 0.0f, 0.0f);
    Vec3f velocity = new Vec3f(0.0f, 0.0f, 0.0f);

    public int step(StepContext stepContext) {
        if (stepContext != null) {
            setTransform(stepContext.getCamera().getTransform());
            setVelocity(stepContext.getCamera().getVelocity());
            this.transform.getTranslation(this.pos);
            this.transform.getZ(this.frontDir);
            this.transform.getY(this.upDir);
        }
        try {
            aNZ dig = SoundMiles.getInstance().getDig();
            AdapterMilesBridgeJNI.m21853c(dig, this.velocity.x, this.velocity.y, this.velocity.z);
            AdapterMilesBridgeJNI.m21766a(dig, this.frontDir.x, this.frontDir.y, this.frontDir.z, this.upDir.x, this.upDir.y, this.upDir.z);
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public RenderAsset cloneAsset() {
        return this;
    }
}
