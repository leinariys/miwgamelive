package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.ArrayList;

/* compiled from: a */
public class RGuiScene {
    /* access modifiers changed from: private */
    public BlurFX blur;
    /* access modifiers changed from: private */
    public RenderTarget renderTarget;
    private int blendFuncDfactor;
    private int blendFuncSfactor;
    private ArrayList<DrawNode> callList = new ArrayList<>();
    private boolean colorArrayEnabled;
    private Color currentColor = Color.WHITE;
    /* renamed from: dc */
    private DrawContext f10345dc;
    private float[] fArray = new float[16];
    private ExpandableIndexData indexes = new ExpandableIndexData(128);
    private Rectangle lastClip = new Rectangle(-1073741824, -1073741824, Integer.MAX_VALUE, Integer.MAX_VALUE);
    private Rectangle lastClipGet = new Rectangle(0, 0, 0, 0);
    private int lastIdx;
    private AffineTransform lastTransform = new AffineTransform();
    private int lastVtx;
    private float lineWidth = -1.0f;
    /* renamed from: p1 */
    private Point2D f10346p1 = new Point2D.Float(0.0f, 0.0f);
    /* renamed from: p2 */
    private Point2D f10347p2 = new Point2D.Float(0.0f, 0.0f);
    private PaintContext paintContext;
    private boolean transparentPaintContext;
    private ExpandableVertexData vertexes;

    public RGuiScene(RGraphicsContext rGraphicsContext) {
        this.f10345dc = rGraphicsContext.mo27858dc();
        setVertexes(new ExpandableVertexData(new VertexLayout(true, true, false, false, 1), 50));
    }

    public void reset() {
        this.lastVtx = 0;
        this.lastIdx = 0;
        getVertexes().data().clear();
        getIndexes().buffer().clear();
        this.callList.clear();
        this.currentColor = Color.WHITE;
        this.blendFuncSfactor = 1;
        this.blendFuncDfactor = 771;
        this.colorArrayEnabled = false;
    }

    /* access modifiers changed from: package-private */
    public void add(DrawNode drawNode) {
        this.callList.add(drawNode);
    }

    /* access modifiers changed from: protected */
    public void doRender() {
        ArrayList<DrawNode> arrayList = this.callList;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            arrayList.get(i).draw(this.f10345dc, this);
        }
    }

    public int currentIndex() {
        return this.lastIdx;
    }

    public void addIndex(int i) {
    }

    public void draw() {
        draw((RenderTarget) null);
    }

    public void draw(RenderTarget renderTarget2) {
        this.renderTarget = renderTarget2;
        GL gl = this.f10345dc.getGl();
        gl.glPushAttrib(24576);
        try {
            gl.glEnable(3042);
            gl.glBlendFunc(1, 771);
            this.blendFuncSfactor = 1;
            this.blendFuncDfactor = 771;
            gl.glEnableClientState(32884);
            gl.glEnableClientState(32888);
            gl.glEnableClientState(32886);
            this.colorArrayEnabled = false;
            gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            this.currentColor = Color.WHITE;
            gl.glBindTexture(3553, 0);
            gl.glDisable(3553);
            gl.glEnable(3089);
            getVertexes().data().position(getVertexes().layout().positionOffset());
            gl.glVertexPointer(3, 5126, getVertexes().layout().stride(), getVertexes().data());
            getVertexes().data().position(getVertexes().getLayout().colorOffset());
            gl.glColorPointer(4, 5126, getVertexes().layout().stride(), getVertexes().data());
            getVertexes().data().position(getVertexes().layout().texCoordOffset());
            gl.glTexCoordPointer(2, 5126, getVertexes().layout().stride(), getVertexes().data());
            gl.glDisableClientState(32886);
            doRender();
            gl.glDisableClientState(32886);
            gl.glDisable(3089);
            gl.glDisableClientState(32884);
            gl.glDisableClientState(32888);
        } finally {
            gl.glPopAttrib();
        }
    }

    public void setClip(RGraphics2 rGraphics2) {
        rGraphics2.untransformedClipBounds(this.lastClipGet);
        if (!this.lastClip.equals(this.lastClipGet)) {
            this.lastClip.setBounds(this.lastClipGet);
            this.callList.add(new C0756Ko(this.lastClip));
        }
    }

    public void addToList(Color color, int i, int i2) {
        if (color != null && color.getAlpha() != 0) {
            PolyDraw polyDraw = new PolyDraw();
            polyDraw.count = i2;
            polyDraw.bufferPosition = i;
            add(polyDraw);
        }
    }

    public ExpandableVertexData getVertexes() {
        return this.vertexes;
    }

    public void setVertexes(ExpandableVertexData expandableVertexData) {
        this.vertexes = expandableVertexData;
    }

    public ExpandableIndexData getIndexes() {
        return this.indexes;
    }

    public int getLastVtx() {
        return this.lastVtx;
    }

    public int getLastIdx() {
        return this.lastIdx;
    }

    public void setBlendFunc(int i, int i2) {
        if (i != this.blendFuncSfactor || i2 != this.blendFuncDfactor) {
            add(new BlendFuncNode(i, i2));
            this.blendFuncSfactor = i;
            this.blendFuncDfactor = i2;
        }
    }

    public void setEnableColorArray(boolean z) {
        if (this.colorArrayEnabled != z) {
            this.colorArrayEnabled = z;
            add(new C5133b(z));
        }
    }

    public void setColor(Color color, Color color2) {
        if (color == null) {
            color = Color.WHITE;
        }
        if (!Color.WHITE.equals(color2)) {
            int alpha = (color.getAlpha() * color2.getAlpha()) / 255;
            int red = (color.getRed() * color2.getRed()) / 255;
            int green = (color.getGreen() * color2.getGreen()) / 255;
            int blue = (color.getBlue() * color2.getBlue()) / 255;
            if (this.currentColor.getAlpha() != alpha || this.currentColor.getRed() != red || this.currentColor.getGreen() != green || this.currentColor.getBlue() != blue) {
                this.currentColor = new Color(red, green, blue, alpha);
                this.callList.add(new C1082Pq(this.currentColor));
            }
        } else if (!this.currentColor.equals(color)) {
            this.currentColor = color;
            this.callList.add(new C1082Pq(color));
        }
    }

    public void addVertex(float f, float f2, float f3) {
        this.vertexes.setPosition(this.lastVtx, f, f2, f3);
        if (this.paintContext != null) {
            this.f10346p1.setLocation((double) f, (double) f2);
            this.lastTransform.transform(this.f10346p1, this.f10346p1);
            this.paintContext.getRaster((int) this.f10346p1.getX(), (int) this.f10346p1.getY(), 1, 1).getPixel(0, 0, this.fArray);
            if (this.transparentPaintContext) {
                this.vertexes.setColor(this.lastVtx, this.fArray[0] / 255.0f, this.fArray[1] / 255.0f, this.fArray[2] / 255.0f, this.fArray[3] / 255.0f);
            } else {
                this.vertexes.setColor(this.lastVtx, this.fArray[0] / 255.0f, this.fArray[1] / 255.0f, this.fArray[2], 1.0f);
            }
        } else {
            this.vertexes.setColor(this.lastVtx, ((float) this.currentColor.getRed()) / 255.0f, ((float) this.currentColor.getGreen()) / 255.0f, ((float) this.currentColor.getBlue()) / 255.0f, ((float) this.currentColor.getAlpha()) / 255.0f);
        }
        this.indexes.setIndex(this.lastIdx, this.lastVtx);
        this.lastVtx++;
        this.lastIdx++;
    }

    public void setPaintContext(PaintContext paintContext2, boolean z) {
        this.paintContext = paintContext2;
        this.transparentPaintContext = z;
    }

    public void setCurrentTexCoord(int i, float f, float f2) {
        this.vertexes.setTexCoord(this.lastVtx - 1, i, f, f2);
    }

    public void setLineWidth(float f) {
        if (this.lineWidth != f) {
            this.lineWidth = f;
            add(new C5134c(f));
        }
    }

    public void addBlur(int i, int i2, int i3, int i4) {
        setColor(Color.WHITE, Color.WHITE);
        setEnableColorArray(false);
        Rectangle rectangle = new Rectangle(this.lastClip);
        SwingUtilities.computeIntersection(i, i2, i3, i4, rectangle);
        add(new C5132a(rectangle));
    }

    /* renamed from: taikodom.render.graphics2d.RGuiScene$b */
    /* compiled from: a */
    class C5133b extends DrawNode {
        private final /* synthetic */ boolean cba;

        C5133b(boolean z) {
            this.cba = z;
        }

        public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
            if (this.cba) {
                drawContext.getGl().glEnableClientState(32886);
            } else {
                drawContext.getGl().glDisableClientState(32886);
            }
        }
    }

    /* renamed from: taikodom.render.graphics2d.RGuiScene$c */
    /* compiled from: a */
    class C5134c extends DrawNode {
        private final /* synthetic */ float cbb;

        C5134c(float f) {
            this.cbb = f;
        }

        public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
            drawContext.getGl().glLineWidth(this.cbb);
        }
    }

    /* renamed from: taikodom.render.graphics2d.RGuiScene$a */
    class C5132a extends DrawNode {
        private final /* synthetic */ Rectangle caZ;

        C5132a(Rectangle rectangle) {
            this.caZ = rectangle;
        }

        public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
            if (RGuiScene.this.blur == null) {
                RGuiScene.this.blur = new BlurFX();
            }
            drawContext.getGl();
            RGuiScene.this.blur.blur(rGuiScene.renderTarget, drawContext, this.caZ.x, this.caZ.y, this.caZ.width, this.caZ.height);
        }
    }
}
