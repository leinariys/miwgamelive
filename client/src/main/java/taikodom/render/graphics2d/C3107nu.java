package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

/* renamed from: a.nu */
/* compiled from: a */
public class C3107nu extends DrawNode {
    public int bufferPosition;
    public int count;
    public int elementType;

    public C3107nu(int i, int i2, int i3) {
        this.bufferPosition = i2;
        this.count = i3;
        this.elementType = i;
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        rGuiScene.getIndexes().buffer().position(this.bufferPosition);
        drawContext.getGl().glDrawElements(this.elementType, this.count, 5125, rGuiScene.getIndexes().buffer());
    }
}
