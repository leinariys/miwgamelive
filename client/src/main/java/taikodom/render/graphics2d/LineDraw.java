package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

import javax.media.opengl.GL;

/* compiled from: a */
public class LineDraw extends DrawNode {
    public int bufferPosition;
    public int count;
    public float lineWidth;

    public LineDraw(float f, int i, int i2) {
        this.bufferPosition = i;
        this.count = i2;
        this.lineWidth = f;
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        GL gl = drawContext.getGl();
        gl.glLineWidth(this.lineWidth * 2.0f);
        drawElements(gl, rGuiScene.getIndexes(), 1, this.bufferPosition, this.count);
    }
}
