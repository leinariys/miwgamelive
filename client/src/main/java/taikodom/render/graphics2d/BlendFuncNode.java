package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

/* compiled from: a */
public class BlendFuncNode extends DrawNode {
    private int dfactor;
    private int sfactor;

    public BlendFuncNode(int i, int i2) {
        this.sfactor = i;
        this.dfactor = i2;
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        drawContext.getGl().glBlendFunc(this.sfactor, this.dfactor);
    }
}
