package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

/* compiled from: a */
public class PolyDraw extends DrawNode {
    public int bufferPosition;
    public int count;

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        drawElements(drawContext.getGl(), rGuiScene.getIndexes(), 4, this.bufferPosition, this.count);
    }
}
