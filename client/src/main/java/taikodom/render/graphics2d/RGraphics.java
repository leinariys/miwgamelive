package taikodom.render.graphics2d;

import com.sun.opengl.util.BufferUtil;
import com.sun.opengl.util.texture.TextureIO;
import logic.thred.LogPrinter;
import taikodom.render.DrawContext;
import taikodom.render.RenderView;
import taikodom.render.textures.Texture;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.nio.DoubleBuffer;
import java.text.AttributedCharacterIterator;
import java.util.HashMap;
import java.util.Map;

/* compiled from: a */
public class RGraphics extends Graphics2D {
    public static final int BOTTOM = 8;
    public static final int LEFT = 1;
    public static final int RIGHT = 4;
    public static final int TOP = 2;
    private static LogPrinter log = LogPrinter.m10275K(RGraphics.class);
    private Color backgroundColor;
    private DoubleBuffer bbfixedTransform;
    private Rectangle clipArea;
    private boolean clippingOn;
    private Color color;

    /* renamed from: dc */
    private DrawContext f10343dc;
    private Font font;
    private Map<Font, taikodom.render.textures.Font> fontMap;
    private int fontSize;
    private taikodom.render.textures.Font graphicalFont;
    private RenderingHints hints;
    private Line2D lineClipper;
    private float lineWidth;
    private Paint paint;
    private int stepSize;
    private Stroke stroke;
    private Map<Image, Texture> textureMap;
    private AffineTransform transform;

    public RGraphics(RGraphics rGraphics) {
        this.f10343dc = rGraphics.f10343dc;
        this.backgroundColor = new Color(255, 255, 255, 255);
        this.color = new Color(255, 255, 255, 255);
        this.fontSize = 12;
        this.stepSize = 10;
        this.lineWidth = 0.5f;
        this.clippingOn = false;
        this.bbfixedTransform = rGraphics.bbfixedTransform;
        this.transform = (AffineTransform) rGraphics.transform.clone();
        applyFixedAffineTransform(this.transform);
        this.clipArea = new Rectangle(0, 0, 0, 0);
        this.clipArea.x = rGraphics.clipArea.x;
        this.clipArea.y = rGraphics.clipArea.y;
        this.clipArea.width = rGraphics.clipArea.width;
        this.clipArea.height = rGraphics.clipArea.height;
        if (this.hints != null) {
            this.hints = rGraphics.hints;
        }
        setClipping();
        this.fontMap = rGraphics.fontMap;
        this.clippingOn = rGraphics.clippingOn;
        this.font = rGraphics.font;
        this.fontSize = rGraphics.fontSize;
        this.graphicalFont = rGraphics.graphicalFont;
        this.paint = rGraphics.paint;
        this.textureMap = rGraphics.textureMap;
        this.lineClipper = rGraphics.lineClipper;
    }

    public RGraphics(DrawContext drawContext) {
        this.f10343dc = drawContext;
        this.clipArea = new Rectangle(0, 0, 0, 0);
        this.textureMap = new HashMap();
        this.fontMap = new HashMap();
        this.backgroundColor = new Color(255, 255, 255, 255);
        this.color = new Color(255, 255, 255, 255);
        this.fontSize = 12;
        this.stepSize = 10;
        this.bbfixedTransform = BufferUtil.newDoubleBuffer(16);
        this.bbfixedTransform.put(0, 1.0d);
        this.bbfixedTransform.put(5, 1.0d);
        this.bbfixedTransform.put(10, 1.0d);
        this.bbfixedTransform.put(15, 1.0d);
        this.transform = new AffineTransform();
        this.lineClipper = new Line2D.Float();
        this.lineWidth = 0.5f;
        this.clippingOn = false;
    }

    public void resetTransforms() {
        this.transform.setToIdentity();
    }

    private void setClipping() {
        if (this.clippingOn) {
            this.transform.getTranslateX();
            this.transform.getTranslateY();
            RenderView renderView = this.f10343dc.renderView;
        }
    }

    public void resetTransform() {
        applyFixedAffineTransform(this.transform);
    }

    private void setInternalTransform() {
        applyFixedAffineTransform(this.transform);
        this.f10343dc.getGl().glLoadMatrixd(this.bbfixedTransform);
        setClipping();
    }

    private void applyFixedAffineTransform(AffineTransform affineTransform) {
        this.transform = affineTransform;
        this.bbfixedTransform.put(12, affineTransform.getTranslateX());
        this.bbfixedTransform.put(13, affineTransform.getTranslateY());
        this.f10343dc.getGl().glLoadMatrixd(this.bbfixedTransform);
    }

    private void applyAffineTransform(AffineTransform affineTransform) {
        setClipping();
    }

    private void setBackColor() {
        this.f10343dc.getGl().glDisable(3553);
        this.f10343dc.getGl().glColor4d((double) (((float) this.backgroundColor.getRed()) / 255.0f), (double) (((float) this.backgroundColor.getGreen()) / 255.0f), (double) (((float) this.backgroundColor.getBlue()) / 255.0f), (double) (((float) this.backgroundColor.getAlpha()) / 255.0f));
    }

    private void setForeColor() {
        this.f10343dc.getGl().glDisable(3553);
        this.f10343dc.getGl().glDisable(2884);
        this.f10343dc.getGl().glColor4f(((float) this.color.getRed()) / 255.0f, ((float) this.color.getGreen()) / 255.0f, ((float) this.color.getBlue()) / 255.0f, ((float) this.color.getAlpha()) / 255.0f);
    }

    public void addRenderingHints(Map<?, ?> map) {
    }

    public void clip(Shape shape) {
        Rectangle bounds = shape.getBounds();
        clipRect(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    public void draw(Shape shape) {
        fillRect(shape.getBounds().x, shape.getBounds().y, shape.getBounds().width, shape.getBounds().height);
    }

    public void drawGlyphVector(GlyphVector glyphVector, float f, float f2) {
    }

    public boolean drawImage(Image image, AffineTransform affineTransform, ImageObserver imageObserver) {
        applyAffineTransform(affineTransform);
        drawImage(image, 0, 0, imageObserver);
        return false;
    }

    public void drawImage(BufferedImage bufferedImage, BufferedImageOp bufferedImageOp, int i, int i2) {
    }

    public void drawRenderableImage(RenderableImage renderableImage, AffineTransform affineTransform) {
    }

    public void drawRenderedImage(RenderedImage renderedImage, AffineTransform affineTransform) {
    }

    public void drawString(String str, int i, int i2) {
        setInternalTransform();
        drawString(str, (float) i, (float) i2);
    }

    public void drawString(String str, float f, float f2) {
        this.f10343dc.getGl().glColor4f(((float) this.color.getRed()) / 255.0f, ((float) this.color.getGreen()) / 255.0f, ((float) this.color.getBlue()) / 255.0f, 1.0f);
        if (((float) this.graphicalFont.getFontMetrics().getHeight()) + f2 >= ((float) this.clipArea.y) && f2 <= ((float) (this.clipArea.y + this.clipArea.height))) {
            this.graphicalFont.drawString(this.f10343dc, str, f, f2);
        }
    }

    public void drawString(AttributedCharacterIterator attributedCharacterIterator, int i, int i2) {
    }

    public void drawString(AttributedCharacterIterator attributedCharacterIterator, float f, float f2) {
    }

    public void fill(Shape shape) {
    }

    public Color getBackground() {
        return this.backgroundColor;
    }

    public void setBackground(Color color2) {
        this.backgroundColor = color2;
    }

    public Composite getComposite() {
        return null;
    }

    public void setComposite(Composite composite) {
    }

    public GraphicsConfiguration getDeviceConfiguration() {
        return null;
    }

    public FontRenderContext getFontRenderContext() {
        return null;
    }

    public Paint getPaint() {
        return this.paint;
    }

    public void setPaint(Paint paint2) {
        this.paint = paint2;
    }

    public Object getRenderingHint(RenderingHints.Key key) {
        if (this.hints == null) {
            return null;
        }
        return this.hints.get(key);
    }

    public RenderingHints getRenderingHints() {
        return this.hints;
    }

    public void setRenderingHints(Map<?, ?> map) {
    }

    public Stroke getStroke() {
        return this.stroke;
    }

    public void setStroke(Stroke stroke2) {
        this.lineWidth = ((BasicStroke) stroke2).getLineWidth() / 2.0f;
    }

    public AffineTransform getTransform() {
        return this.transform;
    }

    public void setTransform(AffineTransform affineTransform) {
        applyFixedAffineTransform(affineTransform);
    }

    public boolean hit(Rectangle rectangle, Shape shape, boolean z) {
        return false;
    }

    public void rotate(double d) {
    }

    public void rotate(double d, double d2, double d3) {
    }

    public void scale(double d, double d2) {
    }

    public void setRenderingHint(RenderingHints.Key key, Object obj) {
        if (this.hints == null) {
            this.hints = new RenderingHints(key, obj);
        } else {
            this.hints.put(key, obj);
        }
    }

    public void shear(double d, double d2) {
    }

    public void transform(AffineTransform affineTransform) {
    }

    public void translate(int i, int i2) {
        translate((double) i, (double) i2);
    }

    public void translate(double d, double d2) {
        this.transform.translate(d, d2);
        Rectangle rectangle = this.clipArea;
        rectangle.x = (int) (((double) rectangle.x) - d);
        Rectangle rectangle2 = this.clipArea;
        rectangle2.y = (int) (((double) rectangle2.y) - d2);
        applyFixedAffineTransform(this.transform);
    }

    public void clearRect(int i, int i2, int i3, int i4) {
        setInternalTransform();
        setBackColor();
        fillRect(i, i2, i3, i4);
    }

    public void clipRect(int i, int i2, int i3, int i4) {
        int i5 = 0;
        if (!this.clippingOn) {
            setClip(i, i2, i3, i4);
            return;
        }
        int i6 = this.clipArea.x;
        int i7 = this.clipArea.y;
        int i8 = i6 + this.clipArea.width;
        int i9 = i7 + this.clipArea.height;
        int i10 = i + i3;
        int i11 = i2 + i4;
        if (i6 >= i) {
            i = i6;
        }
        if (i7 >= i2) {
            i2 = i7;
        }
        if (i8 <= i10) {
            i10 = i8;
        }
        if (i9 <= i11) {
            i11 = i9;
        }
        int i12 = i10 - i;
        int i13 = i11 - i2;
        if (i12 < 0) {
            i12 = 0;
        }
        if (i13 >= 0) {
            i5 = i13;
        }
        this.clipArea.x = i;
        this.clipArea.y = i2;
        this.clipArea.width = i12;
        this.clipArea.height = i5;
        setClipping();
    }

    public void copyArea(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public Graphics create() {
        return new RGraphics(this);
    }

    public void dispose() {
    }

    public void drawArc(int i, int i2, int i3, int i4, int i5, int i6) {
        float f;
        boolean z;
        float f2 = (float) this.stepSize;
        boolean z2 = false;
        setForeColor();
        this.f10343dc.getGl().glDisable(2884);
        this.f10343dc.getGl().glBegin(3);
        if (i6 < 0) {
            f2 = -f2;
        }
        float f3 = (float) i5;
        while (f3 != ((float) (i5 + i6))) {
            int sin = ((int) ((Math.sin(((double) f3) * 0.017453292519943295d) * ((double) (i3 / 2))) + ((double) (i3 / 2)))) + i + 1;
            int cos = ((int) ((Math.cos(((double) f3) * 0.017453292519943295d) * ((double) (i4 / 2))) + ((double) (i4 / 2)))) + i2 + 1;
            if (i6 > 0) {
                if (f3 + f2 > ((float) (i5 + i6))) {
                    f = (float) ((int) (((float) (i5 + i6)) - f3));
                }
                f = f2;
            } else {
                if (f3 + f2 < ((float) (i5 + i6))) {
                    f = (float) ((int) (((float) (i5 + i6)) - f3));
                }
                f = f2;
            }
            int sin2 = ((int) ((Math.sin(((double) (f3 + f)) * 0.017453292519943295d) * ((double) (i3 / 2))) + ((double) (i3 / 2)))) + i + 1;
            int cos2 = ((int) ((Math.cos(((double) (f3 + f)) * 0.017453292519943295d) * ((double) (i4 / 2))) + ((double) (i4 / 2)))) + i2 + 1;
            if (this.clippingOn) {
                if (!lineClip(sin, cos, sin2, cos2, this.lineClipper)) {
                    z = true;
                    f3 += f;
                    z2 = z;
                    f2 = f;
                } else {
                    if (z2) {
                        this.f10343dc.getGl().glEnd();
                        this.f10343dc.getGl().glBegin(3);
                    }
                    sin = (int) this.lineClipper.getP1().getX();
                    cos = (int) this.lineClipper.getP1().getY();
                    sin2 = (int) this.lineClipper.getP2().getX();
                    cos2 = (int) this.lineClipper.getP2().getY();
                }
            }
            this.f10343dc.getGl().glVertex2f((float) sin, (float) cos);
            this.f10343dc.getGl().glVertex2f((float) sin2, (float) cos2);
            z = z2;
            f3 += f;
            z2 = z;
            f2 = f;
        }
        this.f10343dc.getGl().glEnd();
        this.f10343dc.getGl().glEnable(3553);
    }

    public Texture createTexture(Image image) {
        Texture texture = this.textureMap.get(image);
        if (texture != null) {
            return texture;
        }
        BufferedImage bufferedImage = new BufferedImage(image.getWidth((ImageObserver) null), image.getHeight((ImageObserver) null), 2);
        bufferedImage.getGraphics().drawImage(image, 0, 0, (ImageObserver) null);
        bufferedImage.flush();
        Texture texture2 = new Texture();
        texture2.setTextureData(0, TextureIO.newTextureData(bufferedImage, false));
        this.textureMap.put(image, texture2);
        log.info("Created texture for " + image);
        return texture2;
    }

    public boolean drawImage(Image image, int i, int i2, ImageObserver imageObserver) {
        drawImage(image, i, i2, i + image.getWidth(imageObserver), i2 + image.getHeight(imageObserver), 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), this.backgroundColor, imageObserver);
        return true;
    }

    public boolean drawImage(Image image, int i, int i2, Color color2, ImageObserver imageObserver) {
        drawImage(image, i, i2, i + image.getWidth(imageObserver), i2 + image.getHeight(imageObserver), 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), color2, imageObserver);
        return true;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, ImageObserver imageObserver) {
        drawImage(image, i, i2, i + i3, i2 + i4, 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), this.backgroundColor, imageObserver);
        return false;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, Color color2, ImageObserver imageObserver) {
        drawImage(image, i, i2, i + i3, i2 + i4, 0, 0, image.getWidth(imageObserver), image.getHeight(imageObserver), color2, imageObserver);
        return false;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, ImageObserver imageObserver) {
        drawImage(image, i, i2, i3, i4, i5, i6, i7, i8, this.backgroundColor, imageObserver);
        return true;
    }

    public boolean drawImage(Image image, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Color color2, ImageObserver imageObserver) {
        Texture texture = this.textureMap.get(image);
        if (texture == null) {
            texture = createTexture(image);
        }
        if (this.clippingOn) {
            if (this.clipArea.x > i) {
                i5 -= i - this.clipArea.x;
                i = this.clipArea.x;
            }
            if (this.clipArea.y > i2) {
                i6 -= i2 - this.clipArea.y;
                i2 = this.clipArea.y;
            }
            if (i3 > this.clipArea.width + this.clipArea.x) {
                i7 += (this.clipArea.width + this.clipArea.x) - i3;
                i3 = this.clipArea.width + this.clipArea.x;
            }
            if (i4 > this.clipArea.height + this.clipArea.y) {
                i8 += (this.clipArea.height + this.clipArea.y) - i4;
                i4 = this.clipArea.height + this.clipArea.y;
            }
            if (i3 - i < 0 || i4 - i2 < 0) {
                return false;
            }
        }
        this.f10343dc.getGl().glEnable(3042);
        this.f10343dc.getGl().glBlendFunc(770, 771);
        this.f10343dc.getGl().glPushMatrix();
        setInternalTransform();
        texture.bind(this.f10343dc);
        this.f10343dc.getGl().glBegin(4);
        this.f10343dc.getGl().glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        this.f10343dc.getGl().glTexCoord2f(((float) i7) / ((float) image.getWidth(imageObserver)), ((float) i6) / ((float) image.getHeight(imageObserver)));
        this.f10343dc.getGl().glVertex2f((float) i3, (float) i2);
        this.f10343dc.getGl().glTexCoord2f(((float) i5) / ((float) image.getWidth(imageObserver)), ((float) i6) / ((float) image.getHeight(imageObserver)));
        this.f10343dc.getGl().glVertex2f((float) i, (float) i2);
        this.f10343dc.getGl().glTexCoord2f(((float) i7) / ((float) image.getWidth(imageObserver)), ((float) i8) / ((float) image.getHeight(imageObserver)));
        this.f10343dc.getGl().glVertex2f((float) i3, (float) i4);
        this.f10343dc.getGl().glTexCoord2f(((float) i5) / ((float) image.getWidth(imageObserver)), ((float) i6) / ((float) image.getHeight(imageObserver)));
        this.f10343dc.getGl().glVertex2f((float) i, (float) i2);
        this.f10343dc.getGl().glTexCoord2f(((float) i5) / ((float) image.getWidth(imageObserver)), ((float) i8) / ((float) image.getHeight(imageObserver)));
        this.f10343dc.getGl().glVertex2f((float) i, (float) i4);
        this.f10343dc.getGl().glTexCoord2f(((float) i7) / ((float) image.getWidth(imageObserver)), ((float) i8) / ((float) image.getHeight(imageObserver)));
        this.f10343dc.getGl().glVertex2f((float) i3, (float) i4);
        this.f10343dc.getGl().glEnd();
        this.f10343dc.getGl().glPopMatrix();
        this.f10343dc.getGl().glDisable(3042);
        return true;
    }

    /* access modifiers changed from: package-private */
    public int outcode(float f, float f2) {
        int i = 0;
        if (f2 < ((float) this.clipArea.y)) {
            i = 8;
        } else if (f2 > ((float) (this.clipArea.y + this.clipArea.height))) {
            i = 2;
        }
        if (f > ((float) (this.clipArea.x + this.clipArea.width))) {
            return i | 4;
        }
        if (f < ((float) this.clipArea.x)) {
            return i | 1;
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public boolean lineClip(int i, int i2, int i3, int i4, Line2D line2D) {
        int i5;
        float f;
        float f2;
        int i6;
        int i7;
        boolean z = false;
        boolean z2 = false;
        float f3 = (float) this.clipArea.x;
        float f4 = (float) this.clipArea.y;
        float f5 = (float) (this.clipArea.x + this.clipArea.width);
        float f6 = (float) (this.clipArea.y + this.clipArea.height);
        int outcode = outcode((float) i, (float) i2);
        int outcode2 = outcode((float) i3, (float) i4);
        int i8 = i4;
        int i9 = i3;
        int i10 = i2;
        int i11 = i;
        while (true) {
            if ((outcode | outcode2) == 0) {
                z = true;
                z2 = true;
                i6 = i8;
                i7 = i9;
            } else if ((outcode & outcode2) != 0) {
                z2 = true;
                i6 = i8;
                i7 = i9;
            } else {
                if (outcode != 0) {
                    i5 = outcode;
                } else {
                    i5 = outcode2;
                }
                if ((i5 & 2) != 0) {
                    f2 = ((((float) (i9 - i11)) * (f6 - ((float) i10))) / ((float) (i8 - i10))) + ((float) i11);
                    f = f6;
                } else if ((i5 & 8) != 0) {
                    f2 = ((((float) (i9 - i11)) * (f4 - ((float) i10))) / ((float) (i8 - i10))) + ((float) i11);
                    f = f6;
                } else if ((i5 & 4) != 0) {
                    f = ((float) i10) + ((((float) (i8 - i10)) * (f5 - ((float) i11))) / ((float) (i9 - i11)));
                    f2 = f5;
                } else {
                    f = ((float) i10) + ((((float) (i8 - i10)) * (f3 - ((float) i11))) / ((float) (i9 - i11)));
                    f2 = f3;
                }
                if (i5 == outcode) {
                    i11 = (int) f2;
                    i10 = (int) f;
                    outcode = outcode((float) i11, (float) i10);
                    i6 = i8;
                    i7 = i9;
                } else {
                    int i12 = (int) f2;
                    int i13 = (int) f;
                    outcode2 = outcode((float) i12, (float) i13);
                    i6 = i13;
                    i7 = i12;
                }
            }
            if (z2) {
                break;
            }
            i8 = i6;
            i9 = i7;
        }
        if (!z) {
            return false;
        }
        line2D.setLine((double) i11, (double) i10, (double) i7, (double) i6);
        return true;
    }

    public void drawLine(int i, int i2, int i3, int i4) {
        setInternalTransform();
        setForeColor();
        float atan2 = (float) (Math.atan2((double) (i3 - i), (double) (i4 - i2)) * 57.29577951308232d);
        if (this.clippingOn) {
            if (lineClip(i, i2, i3, i4, this.lineClipper)) {
                i = (int) this.lineClipper.getP1().getX();
                i2 = (int) this.lineClipper.getP1().getY();
                i3 = (int) this.lineClipper.getP2().getX();
                i4 = (int) this.lineClipper.getP2().getY();
            } else {
                return;
            }
        }
        int i5 = i + 1;
        int i6 = i3 + 1;
        this.f10343dc.getGl().glBegin(4);
        this.f10343dc.getGl().glVertex2f((float) (((double) i5) + (Math.sin((double) ((atan2 - 90.0f) * 0.017453292f)) * ((double) this.lineWidth))), (float) (((double) i2) + (Math.cos((double) ((atan2 - 90.0f) * 0.017453292f)) * ((double) this.lineWidth))));
        this.f10343dc.getGl().glVertex2f((float) (((double) i6) + (Math.sin((double) ((atan2 - 90.0f) * 0.017453292f)) * ((double) this.lineWidth))), (float) (((double) i4) + (Math.cos((double) ((atan2 - 90.0f) * 0.017453292f)) * ((double) this.lineWidth))));
        this.f10343dc.getGl().glVertex2d((double) ((float) (((double) i5) + (Math.sin((double) ((90.0f + atan2) * 0.017453292f)) * ((double) this.lineWidth)))), (double) ((float) (((double) i2) + (Math.cos((double) ((90.0f + atan2) * 0.017453292f)) * ((double) this.lineWidth)))));
        this.f10343dc.getGl().glVertex2f((float) (((double) i6) + (Math.sin((double) ((atan2 - 90.0f) * 0.017453292f)) * ((double) this.lineWidth))), (float) (((double) i4) + (Math.cos((double) ((atan2 - 90.0f) * 0.017453292f)) * ((double) this.lineWidth))));
        this.f10343dc.getGl().glVertex2f((float) (((double) i6) + (Math.sin((double) ((90.0f + atan2) * 0.017453292f)) * ((double) this.lineWidth))), (float) (((double) i4) + (Math.cos((double) ((90.0f + atan2) * 0.017453292f)) * ((double) this.lineWidth))));
        this.f10343dc.getGl().glVertex2d((double) ((float) (((double) i5) + (Math.sin((double) ((90.0f + atan2) * 0.017453292f)) * ((double) this.lineWidth)))), (double) ((float) (((double) i2) + (Math.cos((double) ((90.0f + atan2) * 0.017453292f)) * ((double) this.lineWidth)))));
        this.f10343dc.getGl().glEnd();
    }

    public void drawOval(int i, int i2, int i3, int i4) {
        setForeColor();
        int sin = ((int) ((Math.sin(0.017453292519943295d) * ((double) (i3 / 2))) + ((double) (i3 / 2)))) + i;
        int cos = ((int) ((Math.cos(0.017453292519943295d) * ((double) (i4 / 2))) + ((double) (i4 / 2)))) + i2;
        float f = (float) this.stepSize;
        int i5 = sin;
        while (f <= 360.0f) {
            int sin2 = ((int) ((Math.sin(((double) f) * 0.017453292519943295d) * ((double) (i3 / 2))) + ((double) (i3 / 2)))) + i;
            int cos2 = ((int) ((Math.cos(((double) f) * 0.017453292519943295d) * ((double) (i4 / 2))) + ((double) (i4 / 2)))) + i2;
            drawLine(i5, cos, sin2, cos2);
            f += (float) this.stepSize;
            cos = cos2;
            i5 = sin2;
        }
    }

    public void drawPolygon(int[] iArr, int[] iArr2, int i) {
        setForeColor();
        this.f10343dc.getGl().glBegin(1);
        for (int i2 = 0; i2 < i - 1; i2++) {
            this.f10343dc.getGl().glVertex2f((float) iArr[i2], (float) iArr2[i2]);
            this.f10343dc.getGl().glVertex2f((float) iArr[i2 + 1], (float) iArr2[i2 + 1]);
        }
        this.f10343dc.getGl().glVertex2f((float) iArr[i], (float) iArr2[i]);
        this.f10343dc.getGl().glVertex2f((float) iArr[0], (float) iArr2[0]);
        this.f10343dc.getGl().glEnd();
    }

    public void drawPolyline(int[] iArr, int[] iArr2, int i) {
        this.f10343dc.getGl().glBegin(1);
        for (int i2 = 0; i2 < i - 1; i2++) {
            this.f10343dc.getGl().glVertex2f((float) iArr[i2], (float) iArr2[i2]);
            this.f10343dc.getGl().glVertex2f((float) iArr[i2 + 1], (float) iArr2[i2 + 1]);
        }
        this.f10343dc.getGl().glEnd();
    }

    public void drawRoundRect(int i, int i2, int i3, int i4, int i5, int i6) {
        drawArc(i, i2, i5, i6, 180, 90);
        drawArc(i, (i2 + i4) - i6, i5, i6, 0, -90);
        drawArc((i + i3) - i5, i2, i5, i6, 90, 90);
        drawArc((i + i3) - i5, (i2 + i4) - i6, i5, i6, 0, 90);
        setForeColor();
        drawLine((i5 / 2) + i, i2, (i + i3) - (i5 / 2), i2);
        drawLine(i, (i6 / 2) + i2, i, (i2 + i4) - (i6 / 2));
        drawLine(i + i3, (i6 / 2) + i2, i + i3, (i2 + i4) - (i6 / 2));
        drawLine((i5 / 2) + i, i2 + i4, (i + i3) - (i5 / 2), i2 + i4);
    }

    public void fillArc(int i, int i2, int i3, int i4, int i5, int i6) {
        float f = (float) this.stepSize;
        this.f10343dc.getGl().glDisable(2884);
        this.f10343dc.getGl().glBegin(4);
        if (i6 < 0) {
            f = -f;
        }
        for (float f2 = (float) i5; f2 != ((float) (i5 + i6)); f2 += f) {
            int sin = ((int) ((Math.sin(((double) f2) * 0.017453292519943295d) * ((double) (i3 / 2))) + ((double) (i3 / 2)))) + i;
            int cos = ((int) ((Math.cos(((double) f2) * 0.017453292519943295d) * ((double) (i4 / 2))) + ((double) (i4 / 2)))) + i2;
            if (i6 > 0) {
                if (f2 + f > ((float) (i5 + i6))) {
                    f = (float) ((int) (((float) (i5 + i6)) - f2));
                }
            } else if (f2 + f < ((float) (i5 + i6))) {
                f = (float) ((int) (((float) (i5 + i6)) - f2));
            }
            this.f10343dc.getGl().glVertex2f((float) ((i3 / 2) + i), (float) ((i4 / 2) + i2));
            this.f10343dc.getGl().glVertex2f((float) sin, (float) cos);
            this.f10343dc.getGl().glVertex2f((float) (((int) ((Math.sin(((double) (f2 + f)) * 0.017453292519943295d) * ((double) (i3 / 2))) + ((double) (i3 / 2)))) + i), (float) (((int) ((Math.cos(((double) (f2 + f)) * 0.017453292519943295d) * ((double) (i4 / 2))) + ((double) (i4 / 2)))) + i2));
        }
        this.f10343dc.getGl().glEnd();
        this.f10343dc.getGl().glEnable(2884);
    }

    public void fillOval(int i, int i2, int i3, int i4) {
        fillArc(i, i2, i3, i4, 0, 360);
    }

    public void fillPolygon(int[] iArr, int[] iArr2, int i) {
        setForeColor();
        this.f10343dc.getGl().glBegin(9);
        for (int i2 = 0; i2 < i - 1; i2++) {
            this.f10343dc.getGl().glVertex2f((float) iArr[i2], (float) iArr2[i2]);
            this.f10343dc.getGl().glVertex2f((float) iArr[i2 + 1], (float) iArr2[i2 + 1]);
        }
        this.f10343dc.getGl().glEnd();
    }

    public void fillRect(int i, int i2, int i3, int i4) {
        setInternalTransform();
        if (this.clipArea.x > i) {
            i = this.clipArea.x;
        }
        if (this.clipArea.y > i2) {
            i2 = this.clipArea.y;
        }
        if (i3 + i > this.clipArea.width + this.clipArea.x) {
            i3 = (this.clipArea.width + this.clipArea.x) - i;
        }
        if (i4 + i2 > this.clipArea.height + this.clipArea.y) {
            i4 = (this.clipArea.height + this.clipArea.y) - i2;
        }
        setForeColor();
        this.f10343dc.getGl().glBegin(4);
        this.f10343dc.getGl().glVertex2d((double) i, (double) i2);
        this.f10343dc.getGl().glVertex2d((double) (i + i3), (double) i2);
        this.f10343dc.getGl().glVertex2d((double) i, (double) (i2 + i4));
        this.f10343dc.getGl().glVertex2d((double) (i + i3), (double) i2);
        this.f10343dc.getGl().glVertex2d((double) (i + i3), (double) (i2 + i4));
        this.f10343dc.getGl().glVertex2d((double) i, (double) (i2 + i4));
        this.f10343dc.getGl().glEnd();
    }

    public void fillRoundRect(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public Shape getClip() {
        return getClipBounds();
    }

    public void setClip(Shape shape) {
        if (shape == null) {
            this.clippingOn = false;
            this.f10343dc.getGl().glDisable(3089);
            return;
        }
        Rectangle bounds = shape.getBounds();
        setClip(bounds.x, bounds.y, bounds.width, bounds.height);
    }

    public Rectangle getClipBounds() {
        if (!this.clippingOn) {
            return null;
        }
        return (Rectangle) this.clipArea.clone();
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color2) {
        this.color = color2;
    }

    public Font getFont() {
        return this.font;
    }

    public void setFont(Font font2) {
        this.font = font2;
        this.graphicalFont = this.fontMap.get(font2);
        if (this.graphicalFont == null) {
            this.graphicalFont = new taikodom.render.textures.Font();
            this.graphicalFont.setSize((float) font2.getSize());
            this.graphicalFont.setDc(this.f10343dc);
            this.graphicalFont.setAwtFont(font2);
            this.fontMap.put(font2, this.graphicalFont);
            log.info("Creating render font for: " + font2);
        }
    }

    public FontMetrics getFontMetrics(Font font2) {
        taikodom.render.textures.Font font3 = this.fontMap.get(font2);
        if (font3 != null) {
            return font3.getFontMetrics();
        }
        return null;
    }

    public void setClip(int i, int i2, int i3, int i4) {
        this.clipArea.x = i;
        this.clipArea.y = i2;
        this.clipArea.width = i3;
        this.clipArea.height = i4;
        this.clippingOn = true;
    }

    public void setPaintMode() {
    }

    public void setXORMode(Color color2) {
    }

    public DrawContext getDc() {
        return this.f10343dc;
    }

    public void setDc(DrawContext drawContext) {
        this.f10343dc = drawContext;
    }

    public int getFontSize() {
        return this.fontSize;
    }

    public void setFontSize(int i) {
        this.fontSize = i;
    }

    public Color getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(Color color2) {
        this.backgroundColor = color2;
    }
}
