package taikodom.render.graphics2d;

import gnu.trove.THashMap;
import gnu.trove.TObjectHashingStrategy;
import taikodom.render.DrawContext;
import taikodom.render.TexBackedImage;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.RGraphicsDataSource;
import taikodom.render.textures.Texture;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;

/* compiled from: a */
public class RGraphicsContext {

    StackList<RGraphics2> graphics = new C1682Ym(this);
    /* renamed from: dc */
    private DrawContext f10344dc;
    private Map<Font, taikodom.render.textures.Font> fontMap;
    private Map<Font, taikodom.render.textures.Font> quickFontMap;
    private boolean rendering;
    private Map<Image, Texture> textureMap;

    public RGraphicsContext(DrawContext drawContext) {
        this.f10344dc = drawContext;
        this.textureMap = new HashMap();
        this.quickFontMap = new WeakHashMap();
        this.fontMap = new THashMap(new C5131a());
    }

    public BaseTexture getTexture(Image image) {
        if (image == null) {
            return null;
        }
        int height = image.getHeight((ImageObserver) null);
        int width = image.getWidth((ImageObserver) null);
        if (height <= 0 || width <= 0) {
            return null;
        }
        if (image instanceof TexBackedImage) {
            return ((TexBackedImage) image).getTexture();
        }
        Texture texture = this.textureMap.get(image);
        if (texture != null) {
            return texture;
        }
        Texture texture2 = new Texture();
        texture2.setMagFilter(TexMagFilter.LINEAR);
        texture2.setMinFilter(TexMinFilter.LINEAR);
        RGraphicsDataSource rGraphicsDataSource = new RGraphicsDataSource();
        rGraphicsDataSource.setImage(image);
        texture2.setDataSource(rGraphicsDataSource);
        if ((Texture.isPowerOf2(rGraphicsDataSource.getWidth()) && Texture.isPowerOf2(rGraphicsDataSource.getHeight())) || Texture.haveNPOT()) {
            texture2.setTarget(3553);
        } else if (Texture.haveTexRect()) {
            texture2.setTarget(34037);
        }
        this.textureMap.put(image, texture2);
        texture2.setOnlyUpdateNeeded(true);
        return texture2;
    }

    public taikodom.render.textures.Font getFont(Font font) {
        if (font == null) {
            return null;
        }
        taikodom.render.textures.Font font2 = this.quickFontMap.get(font);
        if (font2 != null) {
            return font2;
        }
        taikodom.render.textures.Font font3 = this.fontMap.get(font);
        if (font3 == null) {
            font3 = new taikodom.render.textures.Font();
            font3.setSize((float) font.getSize());
            font3.setDc(this.f10344dc);
            font3.setAwtFont(font);
            this.fontMap.put(font, font3);
        }
        this.quickFontMap.put(font, font3);
        return font3;
    }

    /* renamed from: dc */
    public DrawContext mo27858dc() {
        return this.f10344dc;
    }

    public FontMetrics getFontMetrics(Font font) {
        return getFont(font).getFontMetrics();
    }

    public RGraphics2 createGraphics() {
        if (this.rendering) {
            return this.graphics.get();
        }
        return new RGraphics2(this);
    }

    public void startRendering() {
        this.rendering = true;
        this.graphics.push();
    }

    public void stopRendering() {
        this.rendering = false;
        this.graphics.pop();
    }

    /* renamed from: taikodom.render.graphics2d.RGraphicsContext$a */
    class C5131a implements TObjectHashingStrategy<Font> {


        C5131a() {
        }

        /* renamed from: a */
        public int computeHashCode(Font font) {
            int i = 0;
            if (font == null) {
                return 0;
            }
            Map<Object, Object> ak = C5885acN.m20331ak(font);
            int hashCode = font.hashCode();
            if (ak != null) {
                i = ak.hashCode() + 3000;
            }
            return i + hashCode;
        }

        /* renamed from: a */
        public boolean equals(Font font, Font font2) {
            if (!font.equals(font2)) {
                return false;
            }
            Map<Object, Object> ak = C5885acN.m20331ak(font);
            Map<Object, Object> ak2 = C5885acN.m20331ak(font2);
            if (ak == ak2) {
                return true;
            }
            if (ak == null || ak2 == null) {
                return false;
            }
            return ak.equals(ak2);
        }
    }
}
