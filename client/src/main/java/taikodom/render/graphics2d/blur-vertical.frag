uniform sampler2D tex0;
	
float texScaler = 1.0/1024.0;
float weight = 1.0/7.0;
vec4 gaussFilter[7];

void main(void) { 
	vec4 color = vec4(0.0,0.0,0.0,0.0);
  	gaussFilter[0] = vec4(0.0, -3.9, 0.0, weight);
	gaussFilter[1] = vec4(0.0, -2.6, 0.0, weight);
 	gaussFilter[2] = vec4(0.0, -1.3, 0.0, weight);
 	gaussFilter[3] = vec4(0.0,  0.0, 0.0, weight);
 	gaussFilter[4] = vec4(0.0,  1.3, 0.0, weight);
 	gaussFilter[5] = vec4(0.0,  2.6, 0.0, weight);
 	gaussFilter[6] = vec4(0.0,  3.9, 0.0, weight);
  	int i;
 	for (i=0; i<7;i++) { 
 		color += texture2D(tex0,
 			vec2(gl_TexCoord[0].x + gaussFilter[i].x * texScaler, 
 			gl_TexCoord[0].y + gaussFilter[i].y * texScaler )) 
 			*  gaussFilter[i].w;
	 }  
	gl_FragColor = color; 
} 
