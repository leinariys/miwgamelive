package taikodom.render.graphics2d;

import taikodom.render.DrawContext;

import java.awt.*;

/* renamed from: a.Ko */
/* compiled from: a */
public final class C0756Ko extends DrawNode {
    private int dpJ;
    private int dpK;
    private int dpL;
    private int dpM;

    public C0756Ko(Rectangle rectangle) {
        this.dpJ = rectangle.x;
        this.dpL = rectangle.y;
        this.dpM = rectangle.width;
        this.dpK = rectangle.height;
    }

    public void draw(DrawContext drawContext, RGuiScene rGuiScene) {
        drawContext.getGl().glScissor(this.dpJ, drawContext.getRenderView().getViewport().height - (this.dpK + this.dpL), this.dpM, this.dpK);
    }
}
