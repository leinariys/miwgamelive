package taikodom.render.graphics2d;

import taikodom.render.DrawContext;
import taikodom.render.data.IntIndexData;

import javax.media.opengl.GL;

/* compiled from: a */
public abstract class DrawNode {
    public abstract void draw(DrawContext drawContext, RGuiScene rGuiScene);

    /* access modifiers changed from: protected */
    public void drawElements(GL gl, IntIndexData intIndexData, int i, int i2, int i3) {
        intIndexData.buffer().position(i2);
        gl.glDrawElements(i, i3, 5125, intIndexData.buffer());
    }
}
