package taikodom.render;

import taikodom.render.textures.ChannelInfo;

import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class PrimitiveDrawingState {
    public static final int MAX_MATERIAL_TEX = 16;
    public static final int MAX_TEX_COORD_SETS = 8;
    public static final int MAX_USED_GL_CHANNELS = 8;
    public final List<ChannelInfo> states = new ArrayList();
    public int tangentChannel = 1;
    public boolean useColorArray = false;
    public boolean useNormalArray = false;
    public boolean useTangentArray = false;

    public void setStates(List<ChannelInfo> list) {
        this.states.clear();
        this.states.addAll(list);
    }

    public boolean isUseNormalArray() {
        return this.useNormalArray;
    }

    public void setUseNormalArray(boolean z) {
        this.useNormalArray = z;
    }

    public boolean isUseColorArray() {
        return this.useColorArray;
    }

    public void setUseColorArray(boolean z) {
        this.useColorArray = z;
    }

    public boolean isUseTangentArray() {
        return this.useTangentArray;
    }

    public void setUseTangentArray(boolean z) {
        this.useTangentArray = z;
    }

    public int getTangentChannel() {
        return this.tangentChannel;
    }

    public void setTangentChannel(int i) {
        this.tangentChannel = i;
    }

    public int getNumTextures() {
        return this.states.size();
    }

    public int channelMapping(int i) {
        return this.states.get(i).getChannelMapping();
    }

    public int texCoordsSets(int i) {
        return this.states.get(i).getTexCoord();
    }

    public void clear() {
        this.states.clear();
        this.useNormalArray = false;
        this.useColorArray = false;
        this.useTangentArray = false;
    }
}
