package taikodom.render;

import com.hoplon.geometry.Color;
import game.geometry.TransformWrap;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.TreeRecordList;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.Shader;
import taikodom.render.textures.Material;

import javax.vecmath.Matrix3f;

/* compiled from: a */
public class GuiContext {

    public final Viewport viewPort = new Viewport();
    /* renamed from: dc */
    DrawContext f10332dc;
    private TreeRecordList records = new TreeRecordList();

    public float getViewportWidth() {
        return (float) this.viewPort.width;
    }

    public float getViewportHeight() {
        return (float) this.viewPort.height;
    }

    public DrawContext getDc() {
        return this.f10332dc;
    }

    public void setDc(DrawContext drawContext) {
        this.f10332dc = drawContext;
    }

    public void clearRecords() {
        this.records.clear();
    }

    public TreeRecord addPrimitive(Shader shader, Material material, Primitive primitive, TransformWrap bcVar, Color color) {
        if (material == null) {
            throw new RuntimeException("Null material!");
        } else if (shader == null) {
            throw new RuntimeException("Null material!");
        } else if (primitive == null) {
            throw new RuntimeException("Null material!");
        } else {
            TreeRecord add = this.records.add();
            add.shader = shader;
            add.material = material;
            add.primitive = primitive;
            add.transform.mo17344b(bcVar);
            add.gpuTransform.mo13985a((Matrix3f) bcVar.orientation);
            add.gpuTransform.m03 = (float) bcVar.position.x;
            add.gpuTransform.m13 = (float) bcVar.position.y;
            add.gpuTransform.m23 = (float) bcVar.position.z;
            add.color.set(color);
            return add;
        }
    }

    public TreeRecordList getRecords() {
        return this.records;
    }

    public void setRecords(TreeRecordList treeRecordList) {
        this.records = treeRecordList;
    }

    public void setViewPort(int i, int i2, int i3, int i4) {
        this.viewPort.x = i;
        this.viewPort.y = i2;
        this.viewPort.width = i3;
        this.viewPort.height = i4;
    }
}
