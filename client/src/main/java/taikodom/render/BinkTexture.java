package taikodom.render;

import game.geometry.Matrix4fWrap;
import logic.render.bink.C0972OJ;
import logic.render.bink.C2427fN;
import org.lwjgl.BufferUtils;
import taikodom.render.enums.TexMagFilter;
import taikodom.render.enums.TexMinFilter;
import taikodom.render.loader.provider.ImageLoader;
import taikodom.render.loader.provider.ResourceCleaner;
import taikodom.render.textures.BinkDataSource;
import taikodom.render.textures.Texture;

import javax.media.opengl.GL;
import java.nio.Buffer;
import java.util.concurrent.Semaphore;

/* compiled from: a */
public class BinkTexture extends Texture {
    public static final long BINKALPHA = 1048576;
    public static final long BINKCOPYALL = 2147483648L;
    public static final long BINKFROMMEMORY = 67108864;
    public static final long BINKNOSKIP = 524288;
    public static final long BINKPRELOADALL = 8192;
    public static final int BINKSURFACE24R = 2;
    public static final int BINKSURFACE32RA = 6;
    Semaphore binkLock = new Semaphore(1);
    int numFrames = 0;
    private boolean copyBinkTextureToBuffer;
    private boolean isEnded;
    private boolean isPlaying;
    private short pitch;
    private Buffer pixelBuffer;
    private int pixelFormat;
    private short pixelSize;
    private int texId;
    private short videoX;
    private short videoY;

    public BinkTexture() {
    }

    public BinkTexture(BinkDataSource binkDataSource) {
        this.dataSource = binkDataSource;
        processData();
        this.copyBinkTextureToBuffer = true;
    }

    public boolean isPlaying() {
        return this.isPlaying;
    }

    private void updateTexture(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        gl.glPixelStorei(3314, this.pitch / this.pixelSize);
        gl.glPixelStorei(3317, 8);
        gl.glTexSubImage2D(this.target, 0, 0, 0, this.videoX, this.videoY, this.pixelFormat, 5121, this.pixelBuffer);
    }

    /* access modifiers changed from: package-private */
    public void processData() {
        this.videoX = (short) ((BinkDataSource) this.dataSource).getWidth();
        this.videoY = (short) ((BinkDataSource) this.dataSource).getHeight();
        this.target = 3553;
        this.width = this.videoX;
        this.height = this.videoY;
        if (!isPowerOf2(this.videoX) || !isPowerOf2(this.videoY)) {
            this.width = (short) Texture.roundToPowerOf2(this.videoX);
            this.height = (short) Texture.roundToPowerOf2(this.videoY);
        }
        int sV = (int) (((BinkDataSource) this.dataSource).getBink().mo18551sV() & BINKALPHA);
        this.pixelFormat = sV != 0 ? 6408 : 6407;
        this.pixelSize = (short) (sV != 0 ? 4 : 3);
        this.pitch = (short) (this.videoX * this.pixelSize);
        this.pixelBuffer = BufferUtils.createByteBuffer(this.pitch * this.height);
        this.bpp = this.pixelSize * 8;
    }

    private void createTexture(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        int[] iArr = new int[1];
        gl.glEnable(this.target);
        gl.glGenTextures(1, iArr, 0);
        this.texId = iArr[0];
        gl.glBindTexture(this.target, this.texId);
        gl.glTexParameteri(this.target, 10242, this.wrapS.glEquivalent());
        gl.glTexParameteri(this.target, 10243, this.wrapT.glEquivalent());
        if (this.minFilter != TexMinFilter.NONE) {
            gl.glTexParameteri(this.target, 10241, this.minFilter.glEquivalent());
        } else {
            gl.glTexParameteri(this.target, 10241, 9729);
        }
        if (this.magFilter != TexMagFilter.NONE) {
            gl.glTexParameteri(this.target, 10240, this.magFilter.glEquivalent());
        } else {
            gl.glTexParameteri(this.target, 10240, 9729);
        }
        gl.glTexImage2D(this.target, 0, this.pixelFormat, this.width, this.height, 0, this.pixelFormat, 5121, (Buffer) null);
        float f = ((float) this.videoX) / ((float) this.width);
        float f2 = ((float) this.videoY) / ((float) this.height);
        setTransform(new Matrix4fWrap(f, 0.0f, 0.0f, 0.0f, 0.0f, f2, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f - f2, 0.0f, 1.0f));
    }

    public void play() {
        C0972OJ.m7904a(((BinkDataSource) this.dataSource).getBink(), 0);
        this.isPlaying = true;
    }

    public void bind(DrawContext drawContext) {
        int i;
        try {
            this.binkLock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!this.initialized) {
            ResourceCleaner.addTexture(this);
            this.initialized = true;
        }
        setLastBindTime(System.currentTimeMillis());
        if (this.texId == 0) {
            createTexture(drawContext);
            this.isPlaying = true;
        }
        if (!this.initialized) {
            ResourceCleaner.addTexture(this);
            this.initialized = true;
        }
        GL gl = drawContext.getGl();
        gl.glEnable(this.target);
        gl.glBindTexture(this.target, this.texId);
        if (((BinkDataSource) this.dataSource).getBink() == null) {
            this.binkLock.release();
            return;
        }
        if (this.isPlaying || this.copyBinkTextureToBuffer) {
            if (C0972OJ.m7936d(((BinkDataSource) this.dataSource).getBink()) == 0 || this.copyBinkTextureToBuffer) {
                C0972OJ.m7926b(((BinkDataSource) this.dataSource).getBink());
                if (C0972OJ.m7949g(((BinkDataSource) this.dataSource).getBink()) == 0 || this.copyBinkTextureToBuffer) {
                    C2427fN bink = ((BinkDataSource) this.dataSource).getBink();
                    Buffer buffer = this.pixelBuffer;
                    short s = this.pitch;
                    long j = (long) this.videoY;
                    if (this.pixelFormat == 6408) {
                        i = 6;
                    } else {
                        i = 2;
                    }
                    if (C0972OJ.m7907a(bink, buffer, s, j, 0, 0, BINKNOSKIP | ((long) i) | BINKCOPYALL) == 0) {
                        updateTexture(drawContext);
                        this.copyBinkTextureToBuffer = false;
                    }
                }
                if (this.isPlaying) {
                    C0972OJ.m7934c(((BinkDataSource) this.dataSource).getBink());
                }
            }
            if (((BinkDataSource) this.dataSource).getBink().mo18546sQ() == ((BinkDataSource) this.dataSource).getBink().mo18545sP()) {
                this.isPlaying = false;
                this.isEnded = true;
                drawContext.postSceneEvent("endVideo", this);
            }
        }
        this.binkLock.release();
    }

    public void finalize() {
    }

    public void releaseReferences() {
        try {
            this.binkLock.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (((BinkDataSource) this.dataSource).getBink() == null) {
            this.binkLock.release();
            return;
        }
        ((BinkDataSource) this.dataSource).flush();
        this.binkLock.release();
    }

    public int getVideoSizeX() {
        return this.videoX;
    }

    public int getVideoSizeY() {
        return this.videoY;
    }

    public int getTexId() {
        return this.texId;
    }

    public int getType() {
        return this.target;
    }

    public C2427fN getBink() {
        return ((BinkDataSource) this.dataSource).getBink();
    }

    public void setBink(C2427fN fNVar) {
        ((BinkDataSource) this.dataSource).setBink(fNVar);
        processData();
        this.copyBinkTextureToBuffer = true;
    }

    public void rewind() {
        C0972OJ.m7917a(((BinkDataSource) this.dataSource).getBink(), 1, 0);
        this.copyBinkTextureToBuffer = true;
    }

    public void gotoFrame(long j) {
        C0972OJ.m7917a(((BinkDataSource) this.dataSource).getBink(), j, 0);
        this.copyBinkTextureToBuffer = true;
    }

    public void pause() {
        C0972OJ.m7904a(((BinkDataSource) this.dataSource).getBink(), 1);
        this.isPlaying = false;
    }

    public void destroyInternalTexture() {
        releaseReferences();
        this.initialized = false;
        ImageLoader.destroyTexture(this);
    }

    public int getMemorySize() {
        return this.width * this.height * this.pixelSize;
    }

    public boolean isEnded() {
        return this.isEnded;
    }
}
