package taikodom.render;

import taikodom.render.textures.ChannelInfo;

import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class MaterialDrawingStates {
    public final List<ChannelInfo> states = new ArrayList();
    public int numTextures;

    public ChannelInfo getTexInfo(int i) {
        return this.states.get(i);
    }

    public List<ChannelInfo> getStates() {
        return this.states;
    }

    public void setStates(List<ChannelInfo> list) {
        this.states.clear();
        this.states.addAll(list);
    }

    public void clear() {
        this.states.clear();
    }
}
