package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;

import javax.media.opengl.GL;
import java.nio.Buffer;
import java.nio.IntBuffer;

/* compiled from: a */
public class IntIndexData extends IndexData {
    public IntBuffer data;
    private int VBO;
    private int glType = 5125;
    private boolean vboCreated = false;

    public IntIndexData(int i) {
        this.data = BufferUtil.newIntBuffer(i);
        this.size = i;
    }

    public IntIndexData(IntIndexData intIndexData) {
        this.size = intIndexData.size;
        this.data = BufferUtil.newIntBuffer(this.size);
        intIndexData.data.clear();
        this.data.clear();
        this.data.put(intIndexData.data);
        intIndexData.data.clear();
        this.data.clear();
    }

    public void setIndex(int i, int i2) {
        this.data.put(i, i2);
    }

    public int getIndex(int i) {
        return this.data.get(i);
    }

    public int getGLType() {
        return this.glType;
    }

    public Buffer buffer() {
        if (!this.useVbo || !this.vboCreated) {
            return this.data;
        }
        return null;
    }

    public IndexData subSet(int i, int i2) {
        IntIndexData intIndexData = new IntIndexData(i2);
        this.data.position(i);
        this.data.limit(i + i2);
        intIndexData.data.put(this.data);
        intIndexData.data.clear();
        this.data.clear();
        return intIndexData;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.data.capacity(); i++) {
            sb.append(this.data.get(i)).append(", ");
        }
        return sb.toString();
    }

    public void clear() {
        this.data.clear();
        this.size = 0;
    }

    public void bind(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (!drawContext.isUseVbo()) {
            this.useVbo = false;
        }
        if (this.useVbo) {
            if (!this.vboCreated) {
                buildVBO(drawContext);
            }
            gl.glBindBufferARB(34963, this.VBO);
            return;
        }
        gl.glBindBufferARB(34963, 0);
    }

    private void buildVBO(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (this.data != null) {
            int[] iArr = new int[1];
            gl.glGenBuffersARB(1, iArr, 0);
            this.VBO = iArr[0];
            this.vboCreated = true;
            gl.glBindBufferARB(34963, iArr[0]);
            gl.glBufferDataARB(34963, this.size * 4, this.data, 35044);
            gl.glBindBufferARB(34963, 0);
            this.vboCreated = true;
            return;
        }
        this.useVbo = false;
    }

    public IndexData duplicate() {
        return new IntIndexData(this);
    }

    public boolean isVBO() {
        return this.VBO != 0;
    }
}
