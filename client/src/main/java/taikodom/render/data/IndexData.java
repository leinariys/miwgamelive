package taikodom.render.data;

import taikodom.render.DrawContext;

import java.nio.Buffer;

/* compiled from: a */
public abstract class IndexData {
    public int glType;
    public int size;
    public boolean useVbo = false;

    public abstract void bind(DrawContext drawContext);

    public abstract Buffer buffer();

    public abstract void clear();

    public abstract IndexData duplicate();

    public abstract int getGLType();

    public abstract int getIndex(int i);

    public abstract void setIndex(int i, int i2);

    public abstract IndexData subSet(int i, int i2);

    public boolean isUseVBO() {
        return this.useVbo;
    }

    public void setUseVbo(boolean z) {
        this.useVbo = z;
    }

    public boolean isVBO() {
        return false;
    }

    public int size() {
        return this.size;
    }
}
