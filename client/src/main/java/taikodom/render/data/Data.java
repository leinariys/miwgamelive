package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;

import java.nio.FloatBuffer;

/* compiled from: a */
public class Data {
    public final FloatBuffer data;

    public Data(int i) {
        this.data = BufferUtil.newFloatBuffer(i);
    }

    public Data(FloatBuffer floatBuffer) {
        this.data = floatBuffer;
    }

    public final FloatBuffer buffer() {
        return this.data;
    }

    public int byteSize() {
        return this.data.capacity() * 4;
    }

    public Data duplicate() {
        this.data.clear();
        Data data2 = new Data(this.data.capacity());
        data2.buffer().put(this.data);
        data2.buffer().clear();
        this.data.clear();
        return data2;
    }
}
