package taikodom.render.data;

import game.geometry.Vector2fWrap;

/* compiled from: a */
public class Vec2fData extends Data {
    public Vec2fData(int i) {
        super(i * 2);
    }

    public void set(int i, float f, float f2) {
        int i2 = i * 2;
        this.data.put(i2, f);
        this.data.put(i2 + 1, f2);
    }

    public void set(int i, Vector2fWrap aka) {
        int i2 = i * 2;
        this.data.put(i2, aka.x);
        this.data.put(i2 + 1, aka.y);
    }

    public void get(int i, Vector2fWrap aka) {
        int i2 = i * 2;
        aka.x = this.data.get(i2);
        aka.y = this.data.get(i2 + 1);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.data.capacity(); i += 2) {
            sb.append("(");
            sb.append(this.data.get(i)).append(", ");
            sb.append(this.data.get(i + 1));
            sb.append("), ");
        }
        return sb.toString();
    }

    public Vec2fData duplicate() {
        this.data.clear();
        Vec2fData vec2fData = new Vec2fData(this.data.capacity() / 2);
        vec2fData.buffer().put(this.data);
        vec2fData.buffer().clear();
        this.data.clear();
        return vec2fData;
    }
}
