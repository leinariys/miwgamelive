package taikodom.render.data;

import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.BufferUtil;
import game.geometry.Vector2fWrap;
import game.geometry.Vector4fWrap;
import taikodom.render.DrawContext;
import taikodom.render.PrimitiveDrawingState;

import javax.media.opengl.GL;
import java.nio.ByteBuffer;

/* compiled from: a */
public class InterleavedVertexData extends VertexData {
    public ByteBuffer data;
    public int size;
    public boolean vboCreated;
    private int VBO;
    private VertexLayout layout;
    private boolean useVBO = false;

    public InterleavedVertexData(VertexLayout vertexLayout, int i) {
        this.layout = vertexLayout;
        this.size = i;
        this.data = BufferUtil.newByteBuffer(vertexLayout.stride() * i);
    }

    public InterleavedVertexData(InterleavedVertexData interleavedVertexData) {
        this.layout = interleavedVertexData.layout;
        this.size = interleavedVertexData.size;
        this.data = BufferUtil.newByteBuffer(this.size * this.layout.stride());
        interleavedVertexData.data.clear();
        this.data.clear();
        this.data.put(interleavedVertexData.data);
        interleavedVertexData.data.clear();
        this.data.clear();
    }

    public void setPosition(int i, float f, float f2, float f3) {
        int stride = (this.layout.stride() * i) + this.layout.positionOffset();
        this.data.putFloat(stride, f);
        this.data.putFloat(stride + 4, f2);
        this.data.putFloat(stride + 8, f3);
    }

    public Vec3f getPosition(int i) {
        int stride = (this.layout.stride() * i) + this.layout.positionOffset();
        return new Vec3f(this.data.getFloat(stride), this.data.getFloat(stride + 4), this.data.getFloat(stride + 8));
    }

    public Vector2fWrap getTexCoord(int i, int i2) {
        int stride = (this.layout.stride() * i) + this.layout.texCoordOffset(i2);
        return new Vector2fWrap(this.data.getFloat(stride), this.data.getFloat(stride + 4));
    }

    public void setColor(int i, float f, float f2, float f3, float f4) {
        int stride = (this.layout.stride() * i) + this.layout.colorOffset();
        this.data.putFloat(stride, f);
        this.data.putFloat(stride + 4, f2);
        this.data.putFloat(stride + 8, f3);
        this.data.putFloat(stride + 12, f4);
    }

    public void setNormal(int i, float f, float f2, float f3) {
        int stride = (this.layout.stride() * i) + this.layout.normalsOffset();
        this.data.putFloat(stride, f);
        this.data.putFloat(stride + 4, f2);
        this.data.putFloat(stride + 8, f3);
    }

    public void setTangents(int i, float f, float f2, float f3, float f4) {
        int stride = (this.layout.stride() * i) + this.layout.tangentsOffset();
        this.data.putFloat(stride, f);
        this.data.putFloat(stride + 4, f2);
        this.data.putFloat(stride + 8, f3);
        this.data.putFloat(stride + 12, f4);
    }

    public void setTangents(int i, float f, float f2, float f3) {
        int stride = (this.layout.stride() * i) + this.layout.tangentsOffset();
        this.data.putFloat(stride, f);
        this.data.putFloat(stride + 4, f2);
        this.data.putFloat(stride + 8, f3);
    }

    public void setTexCoord(int i, int i2, float f, float f2) {
        int stride = (this.layout.stride() * i) + this.layout.texCoordOffset(i2);
        this.data.putFloat(stride, f);
        this.data.putFloat(stride + 4, f2);
    }

    public int size() {
        return this.size;
    }

    public ByteBuffer data() {
        return this.data;
    }

    public VertexLayout layout() {
        return this.layout;
    }

    public boolean isUseVBO() {
        return this.useVBO;
    }

    public void setUseVbo(boolean z) {
        this.useVBO = z;
    }

    public void bind(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        PrimitiveDrawingState primitiveDrawingState = drawContext.primitiveState;
        if (!drawContext.isUseVbo()) {
            this.useVBO = false;
        }
        if (this.useVBO) {
            if (!this.vboCreated) {
                buildVBO(drawContext);
            }
            gl.glBindBufferARB(34962, this.VBO);
            if (this.layout.positionOffset() >= 0) {
                gl.glVertexPointer(3, 5126, this.layout.stride(), (long) this.layout.positionOffset());
            }
            if (primitiveDrawingState.isUseNormalArray()) {
                if (this.layout.normalsOffset() >= 0) {
                    gl.glNormalPointer(5126, this.layout.stride(), (long) this.layout.normalsOffset());
                } else {
                    throw new RuntimeException("This mesh doesnt have normals");
                }
            }
            if (primitiveDrawingState.isUseColorArray()) {
                if (this.layout.colorOffset() >= 0) {
                    gl.glColorPointer(this.layout.colorSize(), 5126, this.layout.stride(), (long) this.layout.colorOffset());
                } else {
                    throw new RuntimeException("This mesh doesnt have colors");
                }
            }
            if (primitiveDrawingState.isUseTangentArray()) {
                if (this.layout.tangentsOffset() >= 0) {
                    gl.glVertexAttribPointer(primitiveDrawingState.getTangentChannel(), 4, 5126, false, 0, (long) this.layout.tangentsOffset());
                } else {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }
            }
            if (this.layout.texCoordCount() > 0) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= primitiveDrawingState.getNumTextures()) {
                        gl.glClientActiveTexture(33984);
                        return;
                    }
                    int channelMapping = primitiveDrawingState.channelMapping(i2);
                    int texCoordsSets = primitiveDrawingState.texCoordsSets(channelMapping);
                    gl.glClientActiveTexture(channelMapping + 33984);
                    if (this.layout.texCoordCount() > texCoordsSets) {
                        gl.glTexCoordPointer(2, 5126, this.layout.stride(), (long) this.layout.texCoordOffset(texCoordsSets));
                    } else {
                        gl.glTexCoordPointer(2, 5126, this.layout.stride(), (long) this.layout.texCoordOffset(0));
                    }
                    i = i2 + 1;
                }
            }
        } else {
            if (this.layout.positionOffset() >= 0) {
                this.data.position(this.layout.positionOffset());
                gl.glVertexPointer(3, 5126, this.layout.stride(), this.data);
            }
            if (primitiveDrawingState.isUseNormalArray()) {
                if (this.layout.normalsOffset() >= 0) {
                    this.data.position(this.layout.normalsOffset());
                    gl.glNormalPointer(5126, this.layout.stride(), this.data);
                } else {
                    throw new RuntimeException("This mesh doesnt have normals");
                }
            }
            if (primitiveDrawingState.isUseColorArray()) {
                if (this.layout.colorOffset() >= 0) {
                    this.data.position(this.layout.colorOffset());
                    gl.glColorPointer(this.layout.colorSize(), 5126, this.layout.stride(), this.data);
                } else {
                    throw new RuntimeException("This mesh doesnt have colors");
                }
            }
            if (primitiveDrawingState.isUseTangentArray()) {
                if (this.layout.tangentsOffset() >= 0) {
                    this.data.position(this.layout.tangentsOffset());
                    gl.glVertexAttribPointer(primitiveDrawingState.getTangentChannel(), 4, 5126, false, 0, this.data);
                } else {
                    throw new RuntimeException("This mesh doesnt have tangents");
                }
            }
            if (this.layout.texCoordCount() > 0) {
                for (int i3 = 0; i3 < primitiveDrawingState.getNumTextures(); i3++) {
                    int channelMapping2 = primitiveDrawingState.channelMapping(i3);
                    int texCoordsSets2 = primitiveDrawingState.texCoordsSets(channelMapping2);
                    gl.glClientActiveTexture(channelMapping2 + 33984);
                    if (this.layout.texCoordCount() > texCoordsSets2) {
                        this.data.position(this.layout.texCoordOffset(texCoordsSets2));
                    } else {
                        this.data.position(this.layout.texCoordOffset(0));
                    }
                    gl.glTexCoordPointer(2, 5126, this.layout.stride(), this.data);
                }
                gl.glClientActiveTexture(33984);
            }
        }
    }

    private void buildVBO(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        int[] iArr = new int[1];
        gl.glGenBuffersARB(1, iArr, 0);
        gl.glBindBufferARB(34962, iArr[0]);
        gl.glBufferDataARB(34962, size() * this.layout.stride(), this.data, 35044);
        this.VBO = iArr[0];
        this.vboCreated = true;
    }

    public void disposeVBO(DrawContext drawContext) {
        if (this.vboCreated) {
            drawContext.getGl().glDeleteBuffersARB(1, new int[]{this.VBO}, 0);
            this.vboCreated = false;
            this.VBO = 0;
        }
    }

    public void getPosition(int i, Vec3f vec3f) {
        int stride = (this.layout.stride() * i) + this.layout.positionOffset();
        vec3f.x = this.data.getFloat(stride);
        vec3f.y = this.data.getFloat(stride + 4);
        vec3f.z = this.data.getFloat(stride + 8);
    }

    public void getNormal(int i, Vec3f vec3f) {
        int stride = (this.layout.stride() * i) + this.layout.normalsOffset();
        vec3f.set(this.data.getFloat(stride), this.data.getFloat(stride + 4), this.data.getFloat(stride + 8));
    }

    public void getTexCoord(int i, int i2, Vector2fWrap aka) {
        int stride = (this.layout.stride() * i) + this.layout.texCoordOffset(i2);
        aka.set(this.data.getFloat(stride), this.data.getFloat(stride + 4));
    }

    public void getTangents(int i, Vector4fWrap ajf) {
        int stride = (this.layout.stride() * i) + this.layout.tangentsOffset();
        ajf.set(this.data.getFloat(stride), this.data.getFloat(stride + 4), this.data.getFloat(stride + 8), this.data.getFloat(stride + 12));
    }

    public VertexLayout getLayout() {
        return this.layout;
    }

    public VertexData duplicate() {
        return new InterleavedVertexData(this);
    }
}
