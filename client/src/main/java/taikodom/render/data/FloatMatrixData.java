package taikodom.render.data;

import game.geometry.TransformWrap;

import java.nio.FloatBuffer;

/* compiled from: a */
public class FloatMatrixData extends Data {
    private int columnCount;
    private int rowCount;

    public FloatMatrixData() {
        this(4, 4);
    }

    public FloatMatrixData(int i) {
        this((int) Math.sqrt((double) i), (int) Math.sqrt((double) i));
    }

    public FloatMatrixData(FloatBuffer floatBuffer) {
        this((int) Math.sqrt((double) floatBuffer.capacity()), (int) Math.sqrt((double) floatBuffer.capacity()), floatBuffer);
    }

    public FloatMatrixData(int i, int i2) {
        super(i * i2);
        this.rowCount = i;
        this.columnCount = i2;
        int max = Math.max(i, i2);
        for (int i3 = 0; i3 < max; i3++) {
            set(i3, i3, 1.0f);
        }
    }

    public FloatMatrixData(int i, int i2, FloatBuffer floatBuffer) {
        super(floatBuffer.capacity());
        this.rowCount = i;
        this.columnCount = i2;
        this.data.put(floatBuffer);
    }

    public float get(int i, int i2) {
        return this.data.get(index(i, i2));
    }

    private int index(int i, int i2) {
        return (Math.min(i, this.rowCount - 1) * this.columnCount) + Math.min(i2, this.columnCount - 1);
    }

    public void set(int i, int i2, float f) {
        this.data.put(index(i, i2), f);
    }

    public int getColumnCount() {
        return this.columnCount;
    }

    public int getRowCount() {
        return this.rowCount;
    }

    public TransformWrap getTransform() {
        if (this.rowCount < 4 || this.columnCount < 4) {
            throw new IllegalStateException();
        }
        TransformWrap bcVar = new TransformWrap();
        bcVar.mo17328a(get(0, 0), get(0, 1), get(0, 2), get(1, 0), get(1, 1), get(1, 2), get(2, 0), get(2, 1), get(2, 2));
        bcVar.setTranslation((double) get(3, 0), (double) get(3, 1), (double) get(3, 2));
        return bcVar;
    }

    public void setTransform(TransformWrap bcVar) {
        set(0, 0, bcVar.orientation.m00);
        set(1, 0, bcVar.orientation.m10);
        set(2, 0, bcVar.orientation.m20);
        set(0, 1, bcVar.orientation.m01);
        set(1, 1, bcVar.orientation.m11);
        set(2, 1, bcVar.orientation.m21);
        set(0, 2, bcVar.orientation.m02);
        set(1, 2, bcVar.orientation.m12);
        set(2, 2, bcVar.orientation.m22);
        set(0, 3, (float) bcVar.position.x);
        set(1, 3, (float) bcVar.position.y);
        set(2, 3, (float) bcVar.position.z);
    }

    public void getTransform(TransformWrap bcVar) {
        if (this.rowCount < 4 || this.columnCount < 4) {
            throw new IllegalStateException();
        }
        bcVar.mo17328a(get(0, 0), get(0, 1), get(0, 2), get(1, 0), get(1, 1), get(1, 2), get(2, 0), get(2, 1), get(2, 2));
        bcVar.setTranslation((double) get(3, 0), (double) get(3, 1), (double) get(3, 2));
    }

    public FloatMatrixData duplicate() {
        this.data.clear();
        FloatMatrixData floatMatrixData = new FloatMatrixData(this.rowCount, this.columnCount);
        floatMatrixData.buffer().put(this.data);
        floatMatrixData.buffer().clear();
        this.data.clear();
        return floatMatrixData;
    }
}
