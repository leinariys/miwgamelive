package taikodom.render.data;

import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;

import java.nio.ByteBuffer;

/* compiled from: a */
public class ExpandableVertexData extends InterleavedVertexData {
    public int used;
    private boolean shouldDisposeVBO;

    public ExpandableVertexData(VertexLayout vertexLayout, int i) {
        super(vertexLayout, i);
        this.used = 0;
    }

    public ExpandableVertexData(ExpandableVertexData expandableVertexData) {
        super(expandableVertexData);
        this.used = expandableVertexData.used;
    }

    public int size() {
        return this.used;
    }

    public void setColor(int i, float f, float f2, float f3, float f4) {
        ensure(i);
        super.setColor(i, f, f2, f3, f4);
    }

    public void setNormal(int i, float f, float f2, float f3) {
        ensure(i);
        super.setNormal(i, f, f2, f3);
    }

    public void setPosition(int i, float f, float f2, float f3) {
        ensure(i);
        super.setPosition(i, f, f2, f3);
    }

    public void setTangents(int i, float f, float f2, float f3, float f4) {
        ensure(i);
        super.setTangents(i, f, f2, f3, f4);
    }

    public void setTexCoord(int i, int i2, float f, float f2) {
        ensure(i);
        super.setTexCoord(i, i2, f, f2);
    }

    public void bind(DrawContext drawContext) {
        if (this.shouldDisposeVBO) {
            disposeVBO(drawContext);
            this.shouldDisposeVBO = false;
        }
        super.bind(drawContext);
    }

    public void ensure(int i) {
        if (this.size <= i) {
            if (this.vboCreated && !this.shouldDisposeVBO) {
                this.shouldDisposeVBO = true;
            }
            int max = Math.max((this.size * 3) / 2, i) + 1;
            ByteBuffer newByteBuffer = BufferUtil.newByteBuffer(layout().stride() * max);
            newByteBuffer.put(this.data);
            newByteBuffer.clear();
            this.data = newByteBuffer;
            this.size = max;
            this.used = i + 1;
        } else if (this.used <= i) {
            this.used = i + 1;
        }
    }

    public VertexData duplicate() {
        return new ExpandableVertexData(this);
    }
}
