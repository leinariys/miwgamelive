package taikodom.render.math;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import org.mozilla1.javascript.ScriptRuntime;

import java.nio.FloatBuffer;

/* compiled from: a */
public class Transform {
    public float m00 = 1.0f;
    public float m01;
    public float m02;
    public double m03;
    public float m10;
    public float m11 = 1.0f;
    public float m12;
    public double m13;
    public float m20;
    public float m21;
    public float m22 = 1.0f;
    public double m23;
    float scale = 1.0f;

    public Transform() {
    }

    public Transform(Matrix4fWrap ajk) {
        set(ajk);
    }

    public Transform(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, double d, double d2, double d3) {
        this.m00 = f;
        this.m10 = f4;
        this.m20 = f7;
        this.m01 = f2;
        this.m11 = f5;
        this.m21 = f8;
        this.m02 = f3;
        this.m12 = f6;
        this.m22 = f9;
        this.m03 = d;
        this.m13 = d2;
        this.m23 = d3;
    }

    public static void multiply(Transform transform, Transform transform2, Transform transform3) {
        float f = (transform.m00 * transform2.m00) + (transform.m01 * transform2.m10) + (transform.m02 * transform2.m20);
        float f2 = (transform.m10 * transform2.m00) + (transform.m11 * transform2.m10) + (transform.m12 * transform2.m20);
        float f3 = (transform.m20 * transform2.m00) + (transform.m21 * transform2.m10) + (transform.m22 * transform2.m20);
        float f4 = (transform.m00 * transform2.m01) + (transform.m01 * transform2.m11) + (transform.m02 * transform2.m21);
        float f5 = (transform.m10 * transform2.m01) + (transform.m11 * transform2.m11) + (transform.m12 * transform2.m21);
        float f6 = (transform.m20 * transform2.m01) + (transform.m21 * transform2.m11) + (transform.m22 * transform2.m21);
        float f7 = (transform.m00 * transform2.m02) + (transform.m01 * transform2.m12) + (transform.m02 * transform2.m22);
        float f8 = (transform.m10 * transform2.m02) + (transform.m11 * transform2.m12) + (transform.m12 * transform2.m22);
        float f9 = (transform.m20 * transform2.m02) + (transform.m21 * transform2.m12) + (transform.m22 * transform2.m22);
        double d = (((double) transform.m00) * transform2.m03) + (((double) transform.m01) * transform2.m13) + (((double) transform.m02) * transform2.m23) + transform.m03;
        double d2 = (((double) transform.m10) * transform2.m03) + (((double) transform.m11) * transform2.m13) + (((double) transform.m12) * transform2.m23) + transform.m13;
        transform3.m00 = f;
        transform3.m10 = f2;
        transform3.m20 = f3;
        transform3.m01 = f4;
        transform3.m11 = f5;
        transform3.m21 = f6;
        transform3.m02 = f7;
        transform3.m12 = f8;
        transform3.m22 = f9;
        transform3.m03 = d;
        transform3.m13 = d2;
        transform3.m23 = (((double) transform.m20) * transform2.m03) + (((double) transform.m21) * transform2.m13) + (((double) transform.m22) * transform2.m23) + transform.m23;
    }

    public static void multiply3x3(Transform transform, Transform transform2, Transform transform3) {
        float f = (transform.m00 * transform2.m00) + (transform.m01 * transform2.m10) + (transform.m02 * transform2.m20);
        float f2 = (transform.m10 * transform2.m00) + (transform.m11 * transform2.m10) + (transform.m12 * transform2.m20);
        float f3 = (transform.m20 * transform2.m00) + (transform.m21 * transform2.m10) + (transform.m22 * transform2.m20);
        float f4 = (transform.m00 * transform2.m01) + (transform.m01 * transform2.m11) + (transform.m02 * transform2.m21);
        float f5 = (transform.m10 * transform2.m01) + (transform.m11 * transform2.m11) + (transform.m12 * transform2.m21);
        float f6 = (transform.m20 * transform2.m01) + (transform.m21 * transform2.m11) + (transform.m22 * transform2.m21);
        float f7 = (transform.m00 * transform2.m02) + (transform.m01 * transform2.m12) + (transform.m02 * transform2.m22);
        float f8 = (transform.m10 * transform2.m02) + (transform.m11 * transform2.m12) + (transform.m12 * transform2.m22);
        transform3.m00 = f;
        transform3.m10 = f2;
        transform3.m20 = f3;
        transform3.m01 = f4;
        transform3.m11 = f5;
        transform3.m21 = f6;
        transform3.m02 = f7;
        transform3.m12 = f8;
        transform3.m22 = (transform.m20 * transform2.m02) + (transform.m21 * transform2.m12) + (transform.m22 * transform2.m22);
    }

    public static void multiply3x4(Transform transform, Matrix4fWrap ajk, Transform transform2) {
        float f = (transform.m00 * ajk.m00) + (transform.m01 * ajk.m10) + (transform.m02 * ajk.m20);
        float f2 = (transform.m10 * ajk.m00) + (transform.m11 * ajk.m10) + (transform.m12 * ajk.m20);
        float f3 = (transform.m20 * ajk.m00) + (transform.m21 * ajk.m10) + (transform.m22 * ajk.m20);
        float f4 = (transform.m00 * ajk.m01) + (transform.m01 * ajk.m11) + (transform.m02 * ajk.m21);
        float f5 = (transform.m10 * ajk.m01) + (transform.m11 * ajk.m11) + (transform.m12 * ajk.m21);
        float f6 = (transform.m20 * ajk.m01) + (transform.m21 * ajk.m11) + (transform.m22 * ajk.m21);
        float f7 = (transform.m00 * ajk.m02) + (transform.m01 * ajk.m12) + (transform.m02 * ajk.m22);
        float f8 = (transform.m10 * ajk.m02) + (transform.m11 * ajk.m12) + (transform.m12 * ajk.m22);
        float f9 = (transform.m20 * ajk.m02) + (transform.m21 * ajk.m12) + (transform.m22 * ajk.m22);
        double d = ((double) ((transform.m00 * ajk.m03) + (transform.m01 * ajk.m13) + (transform.m02 * ajk.m23))) + transform.m03;
        double d2 = ((double) ((transform.m10 * ajk.m03) + (transform.m11 * ajk.m13) + (transform.m12 * ajk.m23))) + transform.m13;
        transform2.m00 = f;
        transform2.m10 = f2;
        transform2.m20 = f3;
        transform2.m01 = f4;
        transform2.m11 = f5;
        transform2.m21 = f6;
        transform2.m02 = f7;
        transform2.m12 = f8;
        transform2.m22 = f9;
        transform2.m03 = d;
        transform2.m13 = d2;
        transform2.m23 = ((double) ((transform.m20 * ajk.m03) + (transform.m21 * ajk.m13) + (transform.m22 * ajk.m23))) + transform.m23;
    }

    static boolean isZero(float f) {
        return f < 1.0E-5f && f > -1.0E-5f;
    }

    public void multiply(Vec3f vec3f, Vec3f vec3f2) {
        float f = (float) (((double) (((vec3f.x * this.m10) + (vec3f.y * this.m11) + (vec3f.z * this.m12)) * this.scale)) + this.m13);
        vec3f2.z = (float) (((double) (((vec3f.x * this.m20) + (vec3f.y * this.m21) + (vec3f.z * this.m22)) * this.scale)) + this.m23);
        vec3f2.y = f;
        vec3f2.x = (float) (((double) (((vec3f.x * this.m00) + (vec3f.y * this.m01) + (vec3f.z * this.m02)) * this.scale)) + this.m03);
    }

    public void multiply(Vec3f vec3f, Vec3d ajr) {
        double d = ((double) (((vec3f.x * this.m00) + (vec3f.y * this.m01) + (vec3f.z * this.m02)) * this.scale)) + this.m03;
        double d2 = ((double) (((vec3f.x * this.m10) + (vec3f.y * this.m11) + (vec3f.z * this.m12)) * this.scale)) + this.m13;
        ajr.z = ((double) (((vec3f.x * this.m20) + (vec3f.y * this.m21) + (vec3f.z * this.m22)) * this.scale)) + this.m23;
        ajr.y = d2;
        ajr.x = d;
    }

    public void multiply3x3(Vec3f vec3f, Vec3f vec3f2) {
        float f = (vec3f.x * this.m00) + (vec3f.y * this.m01) + (vec3f.z * this.m02);
        float f2 = (vec3f.x * this.m10) + (vec3f.y * this.m11) + (vec3f.z * this.m12);
        vec3f2.z = (vec3f.x * this.m20) + (vec3f.y * this.m21) + (vec3f.z * this.m22);
        vec3f2.y = f2;
        vec3f2.x = f;
    }

    public void multiply3x3(Vec3d ajr, Vec3d ajr2) {
        double d = (ajr.x * ((double) this.m00)) + (ajr.y * ((double) this.m01)) + (ajr.z * ((double) this.m02));
        double d2 = (ajr.x * ((double) this.m10)) + (ajr.y * ((double) this.m11)) + (ajr.z * ((double) this.m12));
        ajr2.z = (ajr.x * ((double) this.m20)) + (ajr.y * ((double) this.m21)) + (ajr.z * ((double) this.m22));
        ajr2.y = d2;
        ajr2.x = d;
    }

    public void multiply3x3(Vec3f vec3f, Vec3d ajr) {
        ajr.z = (double) ((vec3f.x * this.m20) + (vec3f.y * this.m21) + (vec3f.z * this.m22));
        ajr.y = (double) ((vec3f.x * this.m10) + (vec3f.y * this.m11) + (vec3f.z * this.m12));
        ajr.x = (double) ((vec3f.x * this.m00) + (vec3f.y * this.m01) + (vec3f.z * this.m02));
    }

    public void multiplyByAffine(Vec3d ajr, Vec3d ajr2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        double d = -((this.m03 * ((double) this.m02)) + (this.m13 * ((double) this.m12)) + (this.m23 * ((double) this.m22)));
        double d2 = ajr.x;
        double d3 = ajr.y;
        double d4 = ajr.z;
        ajr2.x = (((double) f6) * d4) + (((double) f4) * d2) + (((double) f5) * d3) + (-((this.m03 * ((double) this.m00)) + (this.m13 * ((double) this.m10)) + (this.m23 * ((double) this.m20))));
        ajr2.y = (((double) f7) * d2) + (((double) f8) * d3) + (((double) f9) * d4) + (-((this.m03 * ((double) this.m01)) + (this.m13 * ((double) this.m11)) + (this.m23 * ((double) this.m21))));
        ajr2.z = (((double) f11) * d3) + (((double) f10) * d2) + (((double) f12) * d4) + d;
    }

    public void multiplyByAffine(Vec3d ajr, Vec3f vec3f) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        double d = -((this.m03 * ((double) this.m02)) + (this.m13 * ((double) this.m12)) + (this.m23 * ((double) this.m22)));
        double d2 = ajr.x;
        double d3 = ajr.y;
        double d4 = ajr.z;
        vec3f.x = (float) ((((double) f6) * d4) + (((double) f4) * d2) + (((double) f5) * d3) + (-((this.m03 * ((double) this.m00)) + (this.m13 * ((double) this.m10)) + (this.m23 * ((double) this.m20)))));
        vec3f.y = (float) ((((double) f7) * d2) + (((double) f8) * d3) + (((double) f9) * d4) + (-((this.m03 * ((double) this.m01)) + (this.m13 * ((double) this.m11)) + (this.m23 * ((double) this.m21)))));
        vec3f.z = (float) ((((double) f11) * d3) + (((double) f10) * d2) + (((double) f12) * d4) + d);
    }

    public void multiplyByAffine(Vec3f vec3f, Vec3f vec3f2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        double d = -((this.m03 * ((double) this.m02)) + (this.m13 * ((double) this.m12)) + (this.m23 * ((double) this.m22)));
        double d2 = (double) vec3f.x;
        double d3 = (double) vec3f.y;
        double d4 = (double) vec3f.z;
        vec3f2.x = (float) ((((double) f6) * d4) + (((double) f4) * d2) + (((double) f5) * d3) + (-((this.m03 * ((double) this.m00)) + (this.m13 * ((double) this.m10)) + (this.m23 * ((double) this.m20)))));
        vec3f2.y = (float) ((((double) f7) * d2) + (((double) f8) * d3) + (((double) f9) * d4) + (-((this.m03 * ((double) this.m01)) + (this.m13 * ((double) this.m11)) + (this.m23 * ((double) this.m21)))));
        vec3f2.z = (float) ((((double) f11) * d3) + (((double) f10) * d2) + (((double) f12) * d4) + d);
    }

    public void multiplyByAffine3x3(Vec3d ajr, Vec3d ajr2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        ajr2.x = (((double) f6) * d3) + (((double) f4) * d) + (((double) f5) * d2);
        ajr2.y = (((double) f7) * d) + (((double) f8) * d2) + (((double) f9) * d3);
        ajr2.z = (((double) f11) * d2) + (((double) f10) * d) + (((double) (f3 * this.m22)) * d3);
    }

    public void multiplyByAffine3x3(Vec3f vec3f, Vec3f vec3f2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        float f13 = vec3f.x;
        float f14 = vec3f.y;
        float f15 = vec3f.z;
        vec3f2.x = (f4 * f13) + (f5 * f14) + (f15 * f6);
        vec3f2.y = (f13 * f7) + (f14 * f8) + (f15 * f9);
        vec3f2.z = (f10 * f13) + (f11 * f14) + (f15 * f12);
    }

    public void getX(Vec3f vec3f) {
        vec3f.x = this.m00;
        vec3f.y = this.m10;
        vec3f.z = this.m20;
    }

    public void getY(Vec3f vec3f) {
        vec3f.x = this.m01;
        vec3f.y = this.m11;
        vec3f.z = this.m21;
    }

    public void getZ(Vec3f vec3f) {
        vec3f.x = this.m02;
        vec3f.y = this.m12;
        vec3f.z = this.m22;
    }

    public void getTranslation(Vec3d ajr) {
        ajr.x = this.m03;
        ajr.y = this.m13;
        ajr.z = this.m23;
    }

    public void getTranslation(Vec3f vec3f) {
        vec3f.x = (float) this.m03;
        vec3f.y = (float) this.m13;
        vec3f.z = (float) this.m23;
    }

    public void setX(Vec3f vec3f) {
        this.m00 = vec3f.x;
        this.m10 = vec3f.y;
        this.m20 = vec3f.z;
    }

    public void setX(float f, float f2, float f3) {
        this.m00 = f;
        this.m10 = f2;
        this.m20 = f3;
    }

    public void setY(Vec3f vec3f) {
        this.m01 = vec3f.x;
        this.m11 = vec3f.y;
        this.m21 = vec3f.z;
    }

    public void setY(float f, float f2, float f3) {
        this.m01 = f;
        this.m11 = f2;
        this.m21 = f3;
    }

    public void setZ(Vec3f vec3f) {
        this.m02 = vec3f.x;
        this.m12 = vec3f.y;
        this.m22 = vec3f.z;
    }

    public void setZ(float f, float f2, float f3) {
        this.m02 = f;
        this.m12 = f2;
        this.m22 = f3;
    }

    public void setTranslation(double d, double d2, double d3) {
        this.m03 = d;
        this.m13 = d2;
        this.m23 = d3;
    }

    public void setTranslation(Vec3d ajr) {
        this.m03 = ajr.x;
        this.m13 = ajr.y;
        this.m23 = ajr.z;
    }

    public void setTranslation(Vec3f vec3f) {
        this.m03 = (double) vec3f.x;
        this.m13 = (double) vec3f.y;
        this.m23 = (double) vec3f.z;
    }

    public boolean hasRotation() {
        float asin;
        float atan2;
        float atan22;
        float f = -this.m12;
        if (f <= -1.0f) {
            asin = -1.570796f;
        } else if (((double) f) >= 1.0d) {
            asin = 1.570796f;
        } else {
            asin = (float) Math.asin((double) f);
        }
        if (f > 0.9999f) {
            atan2 = (float) Math.atan2((double) (-this.m02), (double) this.m00);
            atan22 = 0.0f;
        } else {
            atan2 = (float) Math.atan2((double) this.m02, (double) this.m22);
            atan22 = (float) Math.atan2((double) this.m10, (double) this.m11);
        }
        if (atan2 == 0.0f && atan22 == 0.0f && asin == 0.0f) {
            return false;
        }
        return true;
    }

    public void getAffineInverse3x4(Transform transform) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        transform.m00 = this.m00 * f;
        transform.m01 = this.m10 * f2;
        transform.m02 = this.m20 * f3;
        transform.m10 = this.m01 * f;
        transform.m11 = this.m11 * f2;
        transform.m12 = this.m21 * f3;
        transform.m20 = this.m02 * f;
        transform.m21 = this.m12 * f2;
        transform.m22 = this.m22 * f3;
        double d = this.m03 * ((double) f);
        double d2 = ((double) f2) * this.m13;
        double d3 = ((double) f3) * this.m23;
        transform.m03 = -((((double) this.m00) * d) + (((double) this.m10) * d2) + (((double) this.m20) * d3));
        transform.m13 = -((((double) this.m01) * d) + (((double) this.m11) * d2) + (((double) this.m21) * d3));
        transform.m23 = -((d2 * ((double) this.m12)) + (d * ((double) this.m02)) + (d3 * ((double) this.m22)));
    }

    public float determinant3x3() {
        return (this.m00 * ((this.m11 * this.m22) - (this.m12 * this.m21))) + (this.m01 * ((this.m12 * this.m20) - (this.m10 * this.m22))) + (this.m02 * ((this.m10 * this.m21) - (this.m11 * this.m20)));
    }

    public boolean isValidRotation() {
        float abs = Math.abs(1.0f - determinant3x3());
        if (((double) abs) < 1.0E-6d || abs != abs) {
            return false;
        }
        return true;
    }

    public String toString() {
        return String.format(" %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n", new Object[]{Float.valueOf(this.m00), Float.valueOf(this.m01), Float.valueOf(this.m02), Double.valueOf(this.m03), Float.valueOf(this.m10), Float.valueOf(this.m11), Float.valueOf(this.m12), Double.valueOf(this.m13), Float.valueOf(this.m20), Float.valueOf(this.m21), Float.valueOf(this.m22), Double.valueOf(this.m23), Float.valueOf(this.scale), Float.valueOf(this.scale), Float.valueOf(this.scale), Float.valueOf(1.0f)});
    }

    public void set(Transform transform) {
        this.m00 = transform.m00;
        this.m10 = transform.m10;
        this.m20 = transform.m20;
        this.m01 = transform.m01;
        this.m11 = transform.m11;
        this.m21 = transform.m21;
        this.m02 = transform.m02;
        this.m12 = transform.m12;
        this.m22 = transform.m22;
        this.m03 = transform.m03;
        this.m13 = transform.m13;
        this.m23 = transform.m23;
    }

    public void set(Matrix4fWrap ajk) {
        this.m00 = ajk.m00;
        this.m10 = ajk.m10;
        this.m20 = ajk.m20;
        this.m01 = ajk.m01;
        this.m11 = ajk.m11;
        this.m21 = ajk.m21;
        this.m02 = ajk.m02;
        this.m12 = ajk.m12;
        this.m22 = ajk.m22;
        this.m03 = (double) ajk.m03;
        this.m13 = (double) ajk.m13;
        this.m23 = (double) ajk.m23;
    }

    public void set3x3(Matrix4fWrap ajk) {
        this.m00 = ajk.m00;
        this.m10 = ajk.m10;
        this.m20 = ajk.m20;
        this.m01 = ajk.m01;
        this.m11 = ajk.m11;
        this.m21 = ajk.m21;
        this.m02 = ajk.m02;
        this.m12 = ajk.m12;
        this.m22 = ajk.m22;
    }

    public Transform sSet3x3(Matrix4fWrap ajk) {
        set3x3(ajk);
        return this;
    }

    public Transform sSet(Transform transform) {
        set(transform);
        return this;
    }

    public void get3x3(Matrix4fWrap ajk) {
        ajk.m00 = this.m00;
        ajk.m10 = this.m10;
        ajk.m20 = this.m20;
        ajk.m01 = this.m01;
        ajk.m11 = this.m11;
        ajk.m21 = this.m21;
        ajk.m02 = this.m02;
        ajk.m12 = this.m12;
        ajk.m22 = this.m22;
    }

    public Transform sMultiply3x4(Matrix4fWrap ajk) {
        multiply3x4(this, ajk, this);
        return this;
    }

    public void multiply3x4(Vec3d ajr, Vec3d ajr2) {
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        ajr2.x = (((double) this.m00) * d) + (((double) this.m01) * d2) + (((double) this.m02) * d3) + this.m03;
        ajr2.y = (((double) this.m10) * d) + (((double) this.m11) * d2) + (((double) this.m12) * d3) + this.m13;
        ajr2.z = (d * ((double) this.m20)) + (d2 * ((double) this.m21)) + (((double) this.m22) * d3) + this.m23;
    }

    public void multiply3x4(Vec3f vec3f, Vec3d ajr) {
        double d = (double) vec3f.x;
        double d2 = (double) vec3f.y;
        double d3 = (double) vec3f.z;
        ajr.x = (((double) this.m00) * d) + (((double) this.m01) * d2) + (((double) this.m02) * d3) + this.m03;
        ajr.y = (((double) this.m10) * d) + (((double) this.m11) * d2) + (((double) this.m12) * d3) + this.m13;
        ajr.z = (d * ((double) this.m20)) + (d2 * ((double) this.m21)) + (((double) this.m22) * d3) + this.m23;
    }

    public void normalizedMultiply3x4(Vec3f vec3f, Vec3d ajr) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        float sqrt = (float) (1.0d / Math.sqrt((double) (((this.m00 * this.m00) + (this.m01 * this.m01)) + (this.m02 * this.m02))));
        float sqrt2 = (float) (1.0d / Math.sqrt((double) (((this.m10 * this.m10) + (this.m11 * this.m11)) + (this.m02 * this.m12))));
        float sqrt3 = (float) (1.0d / Math.sqrt((double) (((this.m20 * this.m20) + (this.m21 * this.m21)) + (this.m22 * this.m22))));
        ajr.x = (double) ((this.m00 * f * sqrt) + (this.m01 * f2 * sqrt2) + (this.m02 * f3 * sqrt3));
        ajr.y = (double) ((this.m10 * f * sqrt) + (this.m11 * f2 * sqrt2) + (this.m12 * f3 * sqrt3));
        ajr.z = (double) ((f * this.m20 * sqrt) + (f2 * this.m12 * sqrt2) + (this.m22 * f3 * sqrt3));
    }

    public void normalizedMultiply3x4(Vec3f vec3f, Vec3f vec3f2) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        float sqrt = (float) (1.0d / Math.sqrt((double) (((this.m00 * this.m00) + (this.m01 * this.m01)) + (this.m02 * this.m02))));
        float sqrt2 = (float) (1.0d / Math.sqrt((double) (((this.m10 * this.m10) + (this.m11 * this.m11)) + (this.m02 * this.m12))));
        float sqrt3 = (float) (1.0d / Math.sqrt((double) (((this.m20 * this.m20) + (this.m21 * this.m21)) + (this.m22 * this.m22))));
        vec3f2.x = (this.m00 * f * sqrt) + (this.m01 * f2 * sqrt2) + (this.m02 * f3 * sqrt3);
        vec3f2.y = (this.m10 * f * sqrt) + (this.m11 * f2 * sqrt2) + (this.m12 * f3 * sqrt3);
        vec3f2.z = (f * this.m20 * sqrt) + (f2 * this.m12 * sqrt2) + (this.m22 * f3 * sqrt3);
    }

    public void rotateX(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m11 = cos;
        ajk.m12 = -sin;
        ajk.m21 = sin;
        ajk.m22 = cos;
        sMultiply3x4(ajk);
    }

    public void rotateY(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m00 = cos;
        ajk.m02 = sin;
        ajk.m20 = -sin;
        ajk.m22 = cos;
        sMultiply3x4(ajk);
    }

    public void rotateZ(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m00 = cos;
        ajk.m01 = -sin;
        ajk.m10 = sin;
        ajk.m11 = cos;
        sMultiply3x4(ajk);
    }

    public void get3x3(Transform transform) {
        transform.m00 = this.m00;
        transform.m10 = this.m10;
        transform.m20 = this.m20;
        transform.m01 = this.m01;
        transform.m11 = this.m11;
        transform.m21 = this.m21;
        transform.m02 = this.m02;
        transform.m12 = this.m12;
        transform.m22 = this.m22;
    }

    public void copyTo(Transform transform) {
        transform.m00 = this.m00;
        transform.m10 = this.m10;
        transform.m20 = this.m20;
        transform.m01 = this.m01;
        transform.m11 = this.m11;
        transform.m21 = this.m21;
        transform.m02 = this.m02;
        transform.m12 = this.m12;
        transform.m22 = this.m22;
        transform.m03 = this.m03;
        transform.m13 = this.m13;
        transform.m23 = this.m23;
    }

    public void setIdentity() {
        this.m00 = 1.0f;
        this.m01 = 0.0f;
        this.m02 = 0.0f;
        this.m10 = 0.0f;
        this.m11 = 1.0f;
        this.m12 = 0.0f;
        this.m20 = 0.0f;
        this.m21 = 0.0f;
        this.m22 = 1.0f;
        this.m23 = ScriptRuntime.NaN;
        this.m13 = ScriptRuntime.NaN;
        this.m03 = ScriptRuntime.NaN;
        this.scale = 1.0f;
    }

    public void asColumnMajorBuffer(FloatBuffer floatBuffer) {
        floatBuffer.put(this.m00);
        floatBuffer.put(this.m10);
        floatBuffer.put(this.m20);
        floatBuffer.put(0.0f);
        floatBuffer.put(this.m01);
        floatBuffer.put(this.m11);
        floatBuffer.put(this.m21);
        floatBuffer.put(0.0f);
        floatBuffer.put(this.m02);
        floatBuffer.put(this.m12);
        floatBuffer.put(this.m22);
        floatBuffer.put(0.0f);
        floatBuffer.put((float) this.m03);
        floatBuffer.put((float) this.m13);
        floatBuffer.put((float) this.m23);
        floatBuffer.put(1.0f);
    }

    public void setOrientation(Quat4fWrap aoy) {
        float f = aoy.w;
        float f2 = aoy.x;
        float f3 = aoy.y;
        float f4 = aoy.z;
        double d = (double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4));
        if (!isZero(1.0f - ((float) d))) {
            double sqrt = 1.0d / Math.sqrt(d);
            f = (float) (((double) f) * sqrt);
            f2 = (float) (((double) f2) * sqrt);
            f3 = (float) (((double) f3) * sqrt);
            f4 = (float) (sqrt * ((double) f4));
        }
        float f5 = f2 * f2;
        float f6 = f2 * f3;
        float f7 = f2 * f4;
        float f8 = f2 * f;
        float f9 = f3 * f3;
        float f10 = f3 * f4;
        float f11 = f3 * f;
        float f12 = f4 * f4;
        float f13 = f4 * f;
        this.m00 = 1.0f - ((f9 + f12) * 2.0f);
        this.m01 = (f6 - f13) * 2.0f;
        this.m02 = (f7 + f11) * 2.0f;
        this.m10 = (f13 + f6) * 2.0f;
        this.m11 = 1.0f - ((f5 + f12) * 2.0f);
        this.m12 = (f10 - f8) * 2.0f;
        this.m20 = (f7 - f11) * 2.0f;
        this.m21 = (f10 + f8) * 2.0f;
        this.m22 = 1.0f - ((f5 + f9) * 2.0f);
    }

    public Quat4fWrap toQuaternion() {
        Quat4fWrap aoy = new Quat4fWrap();
        float f = this.m00 + 1.0f + this.m11 + this.m22;
        if (f > 1.0E-5f) {
            float sqrt = ((float) Math.sqrt((double) f)) * 2.0f;
            aoy.x = (this.m21 - this.m12) / sqrt;
            aoy.y = (this.m02 - this.m20) / sqrt;
            aoy.z = (this.m10 - this.m01) / sqrt;
            aoy.w = sqrt * 0.25f;
        } else if (this.m00 > this.m11 && this.m00 > this.m22) {
            float sqrt2 = ((float) Math.sqrt(((((double) this.m00) + 1.0d) - ((double) this.m11)) - ((double) this.m22))) * 2.0f;
            aoy.x = 0.25f * sqrt2;
            aoy.y = (this.m10 + this.m01) / sqrt2;
            aoy.z = (this.m02 + this.m20) / sqrt2;
            aoy.w = (this.m12 - this.m21) / sqrt2;
        } else if (this.m11 > this.m22) {
            float sqrt3 = ((float) Math.sqrt(((((double) this.m11) + 1.0d) - ((double) this.m00)) - ((double) this.m22))) * 2.0f;
            aoy.x = (this.m10 + this.m01) / sqrt3;
            aoy.y = 0.25f * sqrt3;
            aoy.z = (this.m21 + this.m12) / sqrt3;
            aoy.w = (this.m02 - this.m20) / sqrt3;
        } else {
            float sqrt4 = ((float) Math.sqrt(((((double) this.m22) + 1.0d) - ((double) this.m00)) - ((double) this.m11))) * 2.0f;
            aoy.x = (this.m02 + this.m20) / sqrt4;
            aoy.y = (this.m21 + this.m12) / sqrt4;
            aoy.z = 0.25f * sqrt4;
            aoy.w = (this.m01 - this.m10) / sqrt4;
        }
        double bhp = aoy.bhp();
        if (!isZero(1.0f - ((float) bhp))) {
            double sqrt5 = Math.sqrt(bhp);
            aoy.w = (float) (((double) aoy.w) / sqrt5);
            aoy.x = (float) (((double) aoy.x) / sqrt5);
            aoy.y = (float) (((double) aoy.y) / sqrt5);
            aoy.z = (float) (((double) aoy.z) / sqrt5);
        }
        return aoy;
    }

    public Vec3f getVectorX() {
        Vec3f vec3f = new Vec3f();
        getX(vec3f);
        return vec3f;
    }

    public Vec3f getVectorY() {
        Vec3f vec3f = new Vec3f();
        getY(vec3f);
        return vec3f;
    }

    public Vec3f getVectorZ() {
        Vec3f vec3f = new Vec3f();
        getZ(vec3f);
        return vec3f;
    }
}
