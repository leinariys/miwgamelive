package taikodom.render;

import taikodom.render.loader.provider.C3176ol;
import taikodom.render.shaders.RenderStates;
import taikodom.render.textures.DDSLoader;
import taikodom.render.textures.PBuffer;
import taikodom.render.textures.SimpleTexture;

import javax.media.opengl.GL;

/* compiled from: a */
public class FlashVideo implements Video {
    int height;
    C3176ol player;
    RenderStates states = new RenderStates();
    SimpleTexture texture;
    int width;
    private PBuffer pbuffer;

    public FlashVideo(C3176ol olVar) {
        this.player = olVar;
        olVar.mo21041aL(false);
    }

    private void createPBuffer(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (this.pbuffer != null) {
            this.pbuffer.destroy();
            this.pbuffer = null;
            this.texture.releaseReferences();
        }
        this.pbuffer = new PBuffer();
        this.pbuffer.create(drawContext, this.width, this.height);
        this.texture = new SimpleTexture();
        this.texture.createEmptyRGBA(this.width, this.height, 32);
        this.pbuffer.activate(drawContext);
        gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
        this.texture.copyFromCurrentBuffer(gl);
        this.pbuffer.deactivate(drawContext);
        this.texture.bind(drawContext);
    }

    public void render(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        if (this.pbuffer == null) {
            this.width = this.player.mo21035SC().getWidth();
            this.height = this.player.mo21035SC().getHeight();
            createPBuffer(drawContext);
        }
        float sizeX = ((float) this.width) / ((float) this.texture.getSizeX());
        float sizeY = ((float) this.height) / ((float) this.texture.getSizeY());
        this.states.blindlyUpdateGLStates(drawContext);
        if (this.player.mo21039SG()) {
            this.pbuffer.activate(drawContext);
            try {
                gl.glPushMatrix();
                gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
                gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
                this.player.setSize(this.width, this.height);
                this.player.draw();
                gl.glPopMatrix();
            } finally {
                this.texture.copyFromCurrentBuffer(gl);
                this.pbuffer.deactivate(drawContext);
            }
        }
        gl.glPushMatrix();
        gl.glScalef((float) drawContext.getScreenWidth(), (float) drawContext.getScreenHeight(), 1.0f);
        this.texture.bind(drawContext);
        gl.glBegin(7);
        gl.glTexCoord2f(0.0f, sizeY);
        gl.glVertex2f(0.0f, 1.0f);
        gl.glTexCoord2f(sizeX, sizeY);
        gl.glVertex2f(1.0f, 1.0f);
        gl.glTexCoord2f(sizeX, 0.0f);
        gl.glVertex2f(1.0f, 0.0f);
        gl.glTexCoord2f(0.0f, 0.0f);
        gl.glVertex2f(0.0f, 0.0f);
        gl.glEnd();
        gl.glPopMatrix();
    }

    public void update(DrawContext drawContext) {
    }

    public void stop() {
    }

    public void play() {
    }

    public boolean isEnded() {
        return this.player.isFinished();
    }
}
