package taikodom.render.debug;

/* compiled from: a */
public interface RenderDebug {
    public static final boolean LOG_MISSING_PROPERTIES = false;
    public static final boolean LOG_PROPERTY_DESCRIPTORS = false;
    public static final boolean LOG_SHADERS = false;
}
