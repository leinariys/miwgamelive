package taikodom.render;

import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLCapabilitiesChooser;
import javax.media.opengl.GLContext;
import javax.swing.*;
import java.awt.*;

/* compiled from: a */
public class JGLDesktop extends MyGLCanvas implements TDesktop {

    private JDesktopPane desktopPane = new JDesktopPane();

    public JGLDesktop() {
        setContentPane(this.desktopPane);
    }

    public JGLDesktop(GLCapabilities gLCapabilities, GLCapabilitiesChooser gLCapabilitiesChooser, GLContext gLContext, GraphicsDevice graphicsDevice) {
        super(gLCapabilities, gLCapabilitiesChooser, gLContext, graphicsDevice);
        setContentPane(this.desktopPane);
    }

    public JGLDesktop(GLCapabilities gLCapabilities) {
        super(gLCapabilities);
        setContentPane(this.desktopPane);
    }

    public JDesktopPane getDesktopPane() {
        return this.desktopPane;
    }

    public void setRootPane(JRootPane jRootPane) {
        super.setRootPane(jRootPane);
    }
}
