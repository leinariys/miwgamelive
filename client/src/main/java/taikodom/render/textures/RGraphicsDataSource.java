package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;
import com.sun.opengl.util.texture.TextureIO;
import taikodom.render.gl.GLSupportedCaps;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

/* compiled from: a */
public class RGraphicsDataSource extends TextureDataSource {
    static MediaTracker tracker = new MediaTracker(new Label());
    Image image;
    boolean isFlushed = false;
    TextureData[] tdata = new TextureData[1];

    public TextureData[] getData() {
        boolean z;
        boolean z2;
        boolean z3;
        BufferedImage bufferedImage;
        int i;
        int i2;
        if (this.isFlushed) {
            tracker.addImage(this.image, 0);
            try {
                tracker.waitForID(0, 10000);
                tracker.removeImage(this.image);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        int width = this.image.getWidth((ImageObserver) null);
        int height = this.image.getHeight((ImageObserver) null);
        if (Texture.isPowerOf2(width)) {
            z = false;
        } else {
            z = true;
        }
        if (Texture.isPowerOf2(height)) {
            z2 = false;
        } else {
            z2 = true;
        }
        if ((z || z2) && !GLSupportedCaps.isTextureNPOT() && !GLSupportedCaps.isTextureRectangle()) {
            if (z) {
                i = width + 1;
            } else {
                i = width;
            }
            if (z2) {
                i2 = height + 1;
            } else {
                i2 = height;
            }
            BufferedImage bufferedImage2 = new BufferedImage(i, i2, 2);
            z3 = z2;
            bufferedImage = bufferedImage2;
        } else {
            z3 = false;
            z = false;
            bufferedImage = new BufferedImage(width, height, 2);
        }
        bufferedImage.getGraphics().drawImage(this.image, 0, 0, (ImageObserver) null);
        if (z) {
            bufferedImage.getGraphics().copyArea(width - 1, 0, 1, height, 1, 0);
        }
        if (z3) {
            bufferedImage.getGraphics().copyArea(0, height - 1, width, 1, 0, 1);
        }
        this.tdata[0] = TextureIO.newTextureData(bufferedImage, false);
        bufferedImage.flush();
        this.isFlushed = true;
        this.image.flush();
        return this.tdata;
    }

    public Image getImage() {
        return this.image;
    }

    public void setImage(Image image2) {
        this.image = image2;
        this.width = image2.getWidth((ImageObserver) null);
        this.height = image2.getHeight((ImageObserver) null);
    }

    public void clearData() {
        this.tdata[0] = null;
    }
}
