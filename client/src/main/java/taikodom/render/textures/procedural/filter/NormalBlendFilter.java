package taikodom.render.textures.procedural.filter;

import taikodom.render.DrawContext;
import taikodom.render.RenderContext;
import taikodom.render.shaders.*;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.procedural.Filter;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/* compiled from: a */
public class NormalBlendFilter extends Filter {
    private ShaderProgram blendProgram;
    private Shader defaultShader;

    public NormalBlendFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, RenderContext renderContext, BaseTexture baseTexture2) {
        super(baseTexture, (Color) null, floatR, baseTexture2);
        createNormalBlendShader(renderContext);
    }

    private void createNormalBlendShader(RenderContext renderContext) {
        this.defaultShader = new Shader();
        this.defaultShader.setName("Normal blend shader");
        ShaderPass shaderPass = new ShaderPass();
        shaderPass.setName("Normal blend shader pass");
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setChannelMapping(0);
        shaderPass.setChannelTextureSet(0, channelInfo);
        channelInfo.setChannelMapping(1);
        shaderPass.setChannelTextureSet(1, channelInfo);
        this.defaultShader.addPass(shaderPass);
        ShaderProgramObject shaderProgramObject = new ShaderProgramObject(35633, "void main(void){gl_Position = ftransform();gl_TexCoord[0] = gl_MultiTexCoord0;}");
        ShaderProgramObject shaderProgramObject2 = new ShaderProgramObject(35632, "uniform sampler2D tex0; uniform sampler2D tex1; void main(void) { vec4 color0full = texture2D(tex0,gl_TexCoord[0].xy);vec4 color1full = texture2D(tex1,gl_TexCoord[0].xy);vec3 color0 = color0full.xyz*2.0 - 1.0;vec3 color1 = color1full.xyz*2.0 - 1.0;color0+=vec3(color1.x,color1.y,0.0);color0 = normalize(color0);color0 = color0*0.5 + 0.5;gl_FragColor = vec4(color0,color0full.a*color1full.a);}");
        this.blendProgram = new ShaderProgram();
        this.blendProgram.addProgramObject(shaderProgramObject);
        this.blendProgram.addProgramObject(shaderProgramObject2);
        if (!this.blendProgram.compile(this.defaultShader.getPass(0), renderContext.getDc())) {
            this.blendProgram = null;
        }
    }

    public void forceApply(DrawContext drawContext, int i) {
        GL gl = drawContext.getGl();
        gl.glEnable(i);
        gl.glDisable(3042);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        this.output.bind(33984, drawContext);
        gl.glCopyTexSubImage2D(i, 0, 0, 0, 0, 0, this.output.getSizeX(), this.output.getSizeY());
        this.mask.bind(33985, drawContext);
        drawContext.currentPass = null;
        this.blendProgram.bind(drawContext);
        RenderStates renderStates = this.defaultShader.getPass(0).getRenderStates();
        renderStates.fillPrimitiveStates(drawContext.primitiveState);
        renderStates.fillMaterialStates(drawContext.materialState);
        this.blendProgram.updateMaterialProgramAttribs(drawContext);
        for (Rectangle2D.Float floatR : this.areas) {
            gl.glBegin(7);
            gl.glTexCoord2f(floatR.x, floatR.y);
            gl.glVertex2f(floatR.x, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.y);
            gl.glVertex2f(floatR.width, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.height);
            gl.glVertex2f(floatR.width, floatR.height);
            gl.glTexCoord2f(floatR.x, floatR.height);
            gl.glVertex2f(floatR.x, floatR.height);
            gl.glEnd();
        }
        this.blendProgram.unbind(drawContext);
    }
}
