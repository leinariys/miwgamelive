package taikodom.render.textures.procedural.filter;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.procedural.Filter;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/* compiled from: a */
public class BlendFilter extends Filter {
    private float weight;

    public BlendFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, float f) {
        super(baseTexture, (Color) null, floatR);
        this.weight = f;
    }

    public void forceApply(DrawContext drawContext, int i) {
        GL gl = drawContext.getGl();
        gl.glEnable(i);
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glColor4f(1.0f, 1.0f, 1.0f, this.weight);
        this.mask.bind(drawContext);
        for (Rectangle2D.Float floatR : this.areas) {
            gl.glBegin(7);
            gl.glTexCoord2f(floatR.x, floatR.y);
            gl.glVertex2f(floatR.x, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.y);
            gl.glVertex2f(floatR.width, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.height);
            gl.glVertex2f(floatR.width, floatR.height);
            gl.glTexCoord2f(floatR.x, floatR.height);
            gl.glVertex2f(floatR.x, floatR.height);
            gl.glEnd();
        }
    }
}
