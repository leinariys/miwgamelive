package taikodom.render.textures.procedural;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public abstract class Filter {
    public static final Rectangle2D.Float FullArea = new Rectangle2D.Float(0.0f, 0.0f, 1.0f, 1.0f);
    public final List<Rectangle2D.Float> areas;
    public Color color;
    public BaseTexture mask;
    public BaseTexture output;
    private boolean active;

    public Filter(BaseTexture baseTexture, Color color2, Rectangle2D.Float floatR) {
        this.areas = new ArrayList();
        this.mask = baseTexture;
        this.color = color2;
        this.areas.add(floatR);
        this.active = true;
    }

    public Filter(BaseTexture baseTexture, Color color2, Rectangle2D.Float floatR, BaseTexture baseTexture2) {
        this(baseTexture, color2, floatR);
        this.output = baseTexture2;
    }

    public Filter(BaseTexture baseTexture, Color color2, Rectangle2D.Float floatR, BaseTexture baseTexture2, boolean z) {
        this(baseTexture, color2, floatR, baseTexture2);
        this.active = z;
    }

    public abstract void forceApply(DrawContext drawContext, int i);

    public void apply(DrawContext drawContext, int i) {
        if (this.active) {
            forceApply(drawContext, i);
        }
    }

    public List<Rectangle2D.Float> getAreas() {
        return this.areas;
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color2) {
        this.color = color2;
    }

    public BaseTexture getMask() {
        return this.mask;
    }

    public void setMask(BaseTexture baseTexture) {
        this.mask = baseTexture;
    }

    public BaseTexture getOutput() {
        return this.output;
    }

    public void setOutput(BaseTexture baseTexture) {
        this.output = baseTexture;
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean z) {
        this.active = z;
    }

    public void setColor(int i, int i2, int i3, int i4) {
        this.color = new Color(i, i2, i3, i4);
    }
}
