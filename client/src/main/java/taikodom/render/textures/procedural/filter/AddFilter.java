package taikodom.render.textures.procedural.filter;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.procedural.Filter;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/* compiled from: a */
public class AddFilter extends Filter {
    private boolean alpha;
    private boolean blue;
    private boolean green;
    private boolean red;

    public AddFilter(BaseTexture baseTexture, Rectangle2D.Float floatR) {
        super(baseTexture, (Color) null, floatR);
        this.alpha = true;
        this.green = true;
        this.blue = true;
        this.red = true;
    }

    public AddFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        super(baseTexture, (Color) null, floatR, (BaseTexture) null, z5);
        this.red = z;
        this.blue = z2;
        this.green = z3;
        this.alpha = z4;
    }

    public void forceApply(DrawContext drawContext, int i) {
        GL gl = drawContext.getGl();
        gl.glEnable(i);
        gl.glEnable(3042);
        gl.glBlendFunc(1, 1);
        gl.glColorMask(this.red, this.green, this.blue, this.alpha);
        this.mask.bind(drawContext);
        for (Rectangle2D.Float floatR : this.areas) {
            gl.glBegin(7);
            gl.glTexCoord2f(floatR.x, floatR.y);
            gl.glVertex2f(floatR.x, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.y);
            gl.glVertex2f(floatR.width, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.height);
            gl.glVertex2f(floatR.width, floatR.height);
            gl.glTexCoord2f(floatR.x, floatR.height);
            gl.glVertex2f(floatR.x, floatR.height);
            gl.glEnd();
        }
        gl.glColorMask(true, true, true, true);
        gl.glDisable(3042);
    }
}
