package taikodom.render.textures.procedural.filter;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.procedural.Filter;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/* compiled from: a */
public class PaintFilter extends Filter {
    public PaintFilter(Color color, Rectangle2D.Float floatR) {
        super((BaseTexture) null, color, floatR);
    }

    public void forceApply(DrawContext drawContext, int i) {
        GL gl = drawContext.getGl();
        gl.glDisable(3553);
        gl.glDisable(3042);
        gl.glColor4f(((float) this.color.getRed()) / 255.0f, ((float) this.color.getGreen()) / 255.0f, ((float) this.color.getBlue()) / 255.0f, ((float) this.color.getAlpha()) / 255.0f);
        for (Rectangle2D.Float floatR : this.areas) {
            gl.glBegin(7);
            gl.glVertex2f(floatR.x, floatR.y);
            gl.glVertex2f(floatR.width, floatR.y);
            gl.glVertex2f(floatR.width, floatR.height);
            gl.glVertex2f(floatR.x, floatR.height);
            gl.glEnd();
        }
    }
}
