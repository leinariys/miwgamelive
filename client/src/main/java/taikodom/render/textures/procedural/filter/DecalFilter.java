package taikodom.render.textures.procedural.filter;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.procedural.Filter;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/* compiled from: a */
public class DecalFilter extends Filter {
    private boolean printSrcAlpha;
    private Rectangle2D.Float srcArea;
    private boolean stencil;

    public DecalFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, Rectangle2D.Float floatR2) {
        this(baseTexture, Color.WHITE, floatR, floatR2, (BaseTexture) null, false, false, true);
    }

    public DecalFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, Rectangle2D.Float floatR2, boolean z) {
        this(baseTexture, Color.WHITE, floatR, floatR2, (BaseTexture) null, false, false, z);
    }

    public DecalFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, boolean z, boolean z2, boolean z3) {
        this(baseTexture, Color.WHITE, Filter.FullArea, floatR, (BaseTexture) null, z, z2, z3);
    }

    public DecalFilter(BaseTexture baseTexture, Color color, Rectangle2D.Float floatR, Rectangle2D.Float floatR2, BaseTexture baseTexture2, boolean z, boolean z2, boolean z3) {
        super(baseTexture, color, floatR2, baseTexture2, z3);
        this.srcArea = floatR;
        this.stencil = z2;
        this.printSrcAlpha = z;
    }

    public void setSourceArea(Rectangle2D.Float floatR) {
        this.srcArea = floatR;
    }

    public void forceApply(DrawContext drawContext, int i) {
        GL gl = drawContext.getGl();
        gl.glEnable(i);
        if (this.stencil) {
            gl.glEnable(3042);
            gl.glBlendFunc(770, 771);
        }
        this.mask.bind(drawContext);
        gl.glColor4f(((float) this.color.getRed()) / 255.0f, ((float) this.color.getGreen()) / 255.0f, ((float) this.color.getBlue()) / 255.0f, ((float) this.color.getAlpha()) / 255.0f);
        gl.glColorMask(true, true, true, !this.printSrcAlpha);
        for (Rectangle2D.Float floatR : this.areas) {
            gl.glBegin(7);
            gl.glTexCoord2f(this.srcArea.x, this.srcArea.y);
            gl.glVertex2f(floatR.x, floatR.y);
            gl.glTexCoord2f(this.srcArea.width, this.srcArea.y);
            gl.glVertex2f(floatR.width, floatR.y);
            gl.glTexCoord2f(this.srcArea.width, this.srcArea.height);
            gl.glVertex2f(floatR.width, floatR.height);
            gl.glTexCoord2f(this.srcArea.x, this.srcArea.height);
            gl.glVertex2f(floatR.x, floatR.height);
            gl.glEnd();
        }
        if (this.stencil) {
            gl.glDisable(3042);
        }
        if (this.printSrcAlpha) {
            gl.glColorMask(false, false, false, true);
            for (Rectangle2D.Float floatR2 : this.areas) {
                gl.glBegin(7);
                gl.glTexCoord2f(this.srcArea.x, this.srcArea.y);
                gl.glVertex2f(floatR2.x, floatR2.y);
                gl.glTexCoord2f(this.srcArea.width, this.srcArea.y);
                gl.glVertex2f(floatR2.width, floatR2.y);
                gl.glTexCoord2f(this.srcArea.width, this.srcArea.height);
                gl.glVertex2f(floatR2.width, floatR2.height);
                gl.glTexCoord2f(this.srcArea.x, this.srcArea.height);
                gl.glVertex2f(floatR2.x, floatR2.height);
                gl.glEnd();
            }
            gl.glColorMask(true, true, true, true);
        }
    }

    public boolean isStencil() {
        return this.stencil;
    }

    public void setStencil(boolean z) {
        this.stencil = z;
    }
}
