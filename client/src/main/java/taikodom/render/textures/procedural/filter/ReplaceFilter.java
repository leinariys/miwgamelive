package taikodom.render.textures.procedural.filter;

import taikodom.render.DrawContext;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.procedural.Filter;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.geom.Rectangle2D;

/* compiled from: a */
public class ReplaceFilter extends Filter {
    private boolean alphaTest;
    private boolean printSrcAlpha;
    private boolean stencil;

    public ReplaceFilter(BaseTexture baseTexture, Rectangle2D.Float floatR) {
        this(baseTexture, Color.WHITE, floatR, (BaseTexture) null, false, false, false, true);
    }

    public ReplaceFilter(BaseTexture baseTexture, Color color, Rectangle2D.Float floatR) {
        this(baseTexture, color, floatR, (BaseTexture) null, false, false, false, true);
    }

    public ReplaceFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, boolean z) {
        this(baseTexture, Color.WHITE, floatR, (BaseTexture) null, false, false, false, z);
    }

    public ReplaceFilter(BaseTexture baseTexture, Rectangle2D.Float floatR, boolean z, boolean z2) {
        this(baseTexture, Color.WHITE, floatR, (BaseTexture) null, false, false, z, z2);
    }

    public ReplaceFilter(BaseTexture baseTexture, Color color, Rectangle2D.Float floatR, BaseTexture baseTexture2, boolean z, boolean z2, boolean z3, boolean z4) {
        super(baseTexture, color, floatR, baseTexture2, z4);
        this.printSrcAlpha = z2;
        this.stencil = z3;
        this.alphaTest = z;
    }

    public void forceApply(DrawContext drawContext, int i) {
        GL gl = drawContext.getGl();
        gl.glEnable(i);
        if (this.stencil) {
            gl.glEnable(3042);
            gl.glBlendFunc(770, 771);
        }
        if (this.alphaTest) {
            gl.glEnable(3008);
            gl.glAlphaFunc(517, 0.0f);
        }
        this.mask.bind(drawContext);
        gl.glColor4f(((float) this.color.getRed()) / 255.0f, ((float) this.color.getGreen()) / 255.0f, ((float) this.color.getBlue()) / 255.0f, ((float) this.color.getAlpha()) / 255.0f);
        for (Rectangle2D.Float floatR : this.areas) {
            gl.glBegin(7);
            gl.glTexCoord2f(floatR.x, floatR.y);
            gl.glVertex2f(floatR.x, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.y);
            gl.glVertex2f(floatR.width, floatR.y);
            gl.glTexCoord2f(floatR.width, floatR.height);
            gl.glVertex2f(floatR.width, floatR.height);
            gl.glTexCoord2f(floatR.x, floatR.height);
            gl.glVertex2f(floatR.x, floatR.height);
            gl.glEnd();
        }
        if (this.stencil) {
            gl.glDisable(3042);
        }
        if (this.alphaTest) {
            gl.glDisable(3008);
        }
        if (this.printSrcAlpha) {
            gl.glColorMask(false, false, false, true);
            for (Rectangle2D.Float floatR2 : this.areas) {
                gl.glBegin(7);
                gl.glTexCoord2f(floatR2.x, floatR2.y);
                gl.glVertex2f(floatR2.x, floatR2.y);
                gl.glTexCoord2f(floatR2.width, floatR2.y);
                gl.glVertex2f(floatR2.width, floatR2.y);
                gl.glTexCoord2f(floatR2.width, floatR2.height);
                gl.glVertex2f(floatR2.width, floatR2.height);
                gl.glTexCoord2f(floatR2.x, floatR2.height);
                gl.glVertex2f(floatR2.x, floatR2.height);
                gl.glEnd();
            }
            gl.glColorMask(true, true, true, true);
        }
    }

    public boolean isStencil() {
        return this.stencil;
    }

    public void setStencil(boolean z) {
        this.stencil = z;
    }
}
