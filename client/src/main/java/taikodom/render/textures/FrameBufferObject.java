package taikodom.render.textures;

import lombok.extern.slf4j.Slf4j;
import taikodom.render.DrawContext;
import taikodom.render.enums.FBOAttachTarget;
import taikodom.render.gl.GLSupportedCaps;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
@Slf4j
public class FrameBufferObject {
    private static int MAX_ATTACH_TEXTURES = 4;
    private int depthId;
    private int fboId;
    private boolean hasDepthBuffer = false;
    private int height;
    private List<BaseTexture> textures = new ArrayList();
    private int width;

    public static void unbind(DrawContext drawContext) {
        drawContext.getGl().glBindFramebufferEXT(36160, 0);
    }

    public boolean create(DrawContext drawContext, int i, int i2, boolean z) {
        if (!GLSupportedCaps.isFbo()) {
            return false;
        }
        releaseReferences();
        this.width = i;
        this.height = i2;
        this.hasDepthBuffer = z;
        GL gl = drawContext.getGl();
        int[] iArr = new int[1];
        gl.glGenFramebuffersEXT(1, iArr, 0);
        this.fboId = iArr[0];
        if (this.hasDepthBuffer) {
            gl.glBindFramebufferEXT(36160, this.fboId);
            gl.glGenRenderbuffersEXT(1, iArr, 0);
            this.depthId = iArr[0];
            gl.glBindRenderbufferEXT(36161, this.depthId);
            gl.glRenderbufferStorageEXT(36161, 33190, this.width, this.height);
            gl.glFramebufferRenderbufferEXT(36160, 36096, 36161, this.depthId);
        }
        unbind(drawContext);
        return true;
    }

    public void releaseReferences() {
        if (this.fboId != 0) {
            GLU.getCurrentGL().glDeleteFramebuffersEXT(1, new int[]{this.fboId}, 0);
            this.fboId = 0;
        }
        if (this.depthId != 0) {
            GLU.getCurrentGL().glDeleteRenderbuffersEXT(1, new int[]{this.depthId}, 0);
            this.depthId = 0;
        }
        this.textures.clear();
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public boolean isHasDepthBuffer() {
        return this.hasDepthBuffer;
    }

    public void bind(DrawContext drawContext) {
        drawContext.getGl().glBindFramebufferEXT(36160, this.fboId);
    }

    public boolean addDepthAttachTexture(DrawContext drawContext, BaseTexture baseTexture, FBOAttachTarget fBOAttachTarget) {
        drawContext.getGl();
        if (baseTexture.getSizeX() == this.width && baseTexture.getSizeY() == this.height) {
            bind(drawContext);
            drawContext.getGl().glFramebufferTexture2DEXT(36160, 36096, fBOAttachTarget.glEquivalent(), baseTexture.getTexId(), 0);
            unbind(drawContext);
            return true;
        }
        log.error("Texture size is different from FBO size");
        return false;
    }

    public boolean addColorAttachTexture(DrawContext drawContext, BaseTexture baseTexture, FBOAttachTarget fBOAttachTarget) {
        drawContext.getGl();
        if (baseTexture.getSizeX() != this.width || baseTexture.getSizeY() != this.height) {
            log.error("Texture size is different from FBO size");
            return false;
        } else if (this.textures.size() == MAX_ATTACH_TEXTURES) {
            log.error("Max attach textures reached in FBO");
            return false;
        } else {
            bind(drawContext);
            drawContext.getGl().glFramebufferTexture2DEXT(36160, 36064 + this.textures.size(), fBOAttachTarget.glEquivalent(), baseTexture.getTexId(), 0);
            unbind(drawContext);
            this.textures.add(baseTexture);
            return true;
        }
    }

    public boolean checkStatus(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        bind(drawContext);
        int glCheckFramebufferStatusEXT = gl.glCheckFramebufferStatusEXT(36160);
        unbind(drawContext);
        if (glCheckFramebufferStatusEXT == 36053) {
            return true;
        }
        if (glCheckFramebufferStatusEXT == 36061) {
            log.error("GL_FRAMEBUFFER_UNSUPPORTED_EXT");
        } else {
            log.error("Problem creating FBO");
        }
        releaseReferences();
        return false;
    }
}
