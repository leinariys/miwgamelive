package taikodom.render.textures;

import java.awt.*;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

/* compiled from: a */
public class ProxyImage extends Image {
    private Image image;

    public ProxyImage(Image image2) {
        this.image = image2;
    }

    public Graphics getGraphics() {
        return this.image.getGraphics();
    }

    public int getHeight(ImageObserver imageObserver) {
        return this.image.getHeight(imageObserver);
    }

    public Object getProperty(String str, ImageObserver imageObserver) {
        return this.image.getProperty(str, imageObserver);
    }

    public ImageProducer getSource() {
        return this.image.getSource();
    }

    public int getWidth(ImageObserver imageObserver) {
        return this.image.getWidth(imageObserver);
    }

    public void dispose() {
        this.image = null;
    }

    public Image getImage() {
        return this.image;
    }

    public void flush() {
    }
}
