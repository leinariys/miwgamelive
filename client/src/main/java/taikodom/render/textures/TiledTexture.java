package taikodom.render.textures;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import taikodom.render.DrawContext;

/* compiled from: a */
public class TiledTexture extends Texture {
    public final Matrix4fWrap tileMatrix = new Matrix4fWrap();
    public Texture texture = null;

    public void addArea(float f) {
        if (this.texture != null) {
            this.texture.addArea(f);
        }
    }

    public Texture getTexture() {
        return this.texture;
    }

    public void setTexture(Texture texture2) {
        this.texture = texture2;
    }

    public Vec3f getTile() {
        return new Vec3f(this.tileMatrix.m00, this.tileMatrix.m11, this.tileMatrix.m22);
    }

    public void setTile(Vec3f vec3f) {
        this.tileMatrix.m00 = vec3f.x;
        this.tileMatrix.m11 = vec3f.y;
        this.tileMatrix.m22 = vec3f.z;
    }

    public void bind(DrawContext drawContext) {
        if (drawContext.debugDraw()) {
            drawContext.logBind(this);
        }
        if (this.texture != null) {
            this.texture.bind(drawContext);
            drawContext.getGl().glTexParameteri(this.texture.target, 10242, this.wrapS.glEquivalent());
            drawContext.getGl().glTexParameteri(this.texture.target, 10243, this.wrapT.glEquivalent());
        }
        drawContext.getGl().glMultMatrixf(this.tileMatrix.mo13998b(drawContext.tempBuffer0));
    }

    public void bind(int i, DrawContext drawContext) {
        if (this.texture != null) {
            this.texture.bind(i, drawContext);
            drawContext.getGl().glTexParameteri(this.texture.target, 10242, this.wrapS.glEquivalent());
            drawContext.getGl().glTexParameteri(this.texture.target, 10243, this.wrapT.glEquivalent());
        }
        drawContext.getGl().glMultMatrixf(this.tileMatrix.mo13998b(drawContext.tempBuffer0));
    }
}
