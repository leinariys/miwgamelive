package taikodom.render.textures;

import com.sun.opengl.util.BufferUtil;
import taikodom.render.DrawContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.BitmapFontFactory;

import javax.media.opengl.GL;
import java.awt.*;
import java.nio.IntBuffer;
import java.util.Vector;

/* compiled from: a */
public class Font extends RenderAsset {
    public static final String CACHE_DIRECTORY = "data/fontCache/";
    public java.awt.Font awtFont;
    public int numOfValidChars = 0;
    public float size = -1.0f;
    BitmapFont bitmapFont;
    /* renamed from: dc */
    private DrawContext f10382dc;
    private boolean discartedChars;
    private IntBuffer glyphs = IntBuffer.allocate(this.numOfValidChars / 4);

    public boolean haveDiscartedChars() {
        return this.discartedChars;
    }

    public java.awt.Font getAwtFont() {
        return this.awtFont;
    }

    public void setAwtFont(java.awt.Font font) {
        this.awtFont = font;
        this.size = (float) font.getSize();
        this.bitmapFont = BitmapFontFactory.getBitmapFont(font);
    }

    private void destroyTexture() {
    }

    public Texture getTexture() {
        return this.bitmapFont.texture;
    }

    public RenderAsset cloneAsset() {
        return this;
    }

    public void bind(DrawContext drawContext) {
        if (this.bitmapFont.isReady()) {
            this.bitmapFont.texture.bind(drawContext);
        }
    }

    public void drawString(DrawContext drawContext, String str, float f, float f2) {
        if (this.bitmapFont.isReady()) {
            bind(drawContext);
            this.glyphs.clear();
            if (this.glyphs.capacity() < str.length()) {
                this.glyphs = BufferUtil.newIntBuffer(str.length());
            }
            for (int i = 0; i < str.length(); i++) {
                this.glyphs.put(this.bitmapFont.getDisplayList(drawContext, str.codePointAt(i)));
            }
            this.glyphs.flip();
            GL gl = drawContext.getGl();
            gl.glPushMatrix();
            gl.glTranslatef(f, ((float) this.bitmapFont.getFontMetrics().getDescent()) + f2, 0.0f);
            gl.glCallLists(str.length(), 5124, this.glyphs);
            gl.glPopMatrix();
        }
    }

    public float getSize() {
        return this.size;
    }

    public void setSize(float f) {
        this.size = f;
    }

    public DrawContext getDc() {
        return this.f10382dc;
    }

    public void setDc(DrawContext drawContext) {
        this.f10382dc = drawContext;
    }

    public FontMetrics getFontMetrics() {
        return this.bitmapFont.getFontMetrics();
    }

    public void setFontMetrics(FontMetrics fontMetrics) {
        this.bitmapFont.setFontMetrics(fontMetrics);
    }

    public int getNumOfValidChars() {
        return this.numOfValidChars;
    }

    public void setNumOfValidChars(int i) {
        this.numOfValidChars = i;
    }

    public Vector<WidthData> getCharWidths() {
        return this.bitmapFont.charWidth;
    }
}
