package taikodom.render.textures;

import java.nio.ByteBuffer;

/* compiled from: a */
public class DDSInfo {
    private int compressionFormat;
    private ByteBuffer[] data;
    private int height;
    private boolean isCompressed;
    private int width;

    public DDSInfo(ByteBuffer[] byteBufferArr, int i, int i2, boolean z, int i3) {
        this.data = byteBufferArr;
        this.width = i;
        this.height = i2;
        this.isCompressed = z;
        this.compressionFormat = i3;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public ByteBuffer getData() {
        return this.data[0];
    }

    public ByteBuffer getData(int i) {
        return this.data[i];
    }

    public ByteBuffer[] getDataList() {
        return this.data;
    }

    public boolean isCompressed() {
        return this.isCompressed;
    }

    public int getCompressionFormat() {
        return this.compressionFormat;
    }

    public void setCompressionFormat(int i) {
        this.compressionFormat = i;
    }
}
