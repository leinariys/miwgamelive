package taikodom.render.textures;

import com.hoplon.geometry.Color;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.DrawContext;
import taikodom.render.gl.GLSupportedCaps;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.procedural.Filter;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;
import java.util.ArrayList;
import java.util.List;

/* compiled from: a */
public class ProceduralTexture extends BaseTexture {
    private static final boolean BENCHMARK_BAKING = false;
    private static PBuffer pbuffer;

    static {
        if (!GLSupportedCaps.isPbuffer()) {
            throw new GLException("pBuffers not supported with this graphics card");
        }
    }

    private final List<Filter> filters = new ArrayList();
    private int originalHeight;
    private int originalWidth;
    private SimpleTexture output;
    private boolean updateNeeded = true;

    ProceduralTexture() {
    }

    public ProceduralTexture(int i, int i2) {
        this.originalWidth = i;
        this.originalHeight = i2;
        this.width = Texture.roundToPowerOf2(i);
        this.height = Texture.roundToPowerOf2(i2);
    }

    public void addFilter(Filter filter) {
        this.filters.add(filter);
        this.updateNeeded = true;
    }

    /* JADX INFO: finally extract failed */
    public void bind(DrawContext drawContext) {
        GL gl = drawContext.getGl();
        drawContext.incNumTextureBind();
        if (this.output == null) {
            this.output = new SimpleTexture();
            this.output.createEmptyRGBA(this.width, this.height, 32);
            this.output.fillWithColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));
        }
        if (this.updateNeeded) {
            pbuffer = drawContext.getAuxPBuffer();
            pbuffer.activate(drawContext);
            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
            gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
            gl.glMatrixMode(5888);
            gl.glLoadIdentity();
            gl.glMatrixMode(5889);
            gl.glLoadIdentity();
            gl.glOrtho(ScriptRuntime.NaN, 1.0d, ScriptRuntime.NaN, 1.0d, -1.0d, 1.0d);
            gl.glMatrixMode(5888);
            try {
                gl.glViewport(0, 0, this.originalWidth, this.originalHeight);
                gl.glClear(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
                for (Filter apply : this.filters) {
                    apply.apply(drawContext, this.target);
                }
                this.output.copyFromCurrentBuffer(gl);
                pbuffer.deactivate(drawContext);
                drawContext.getGlContext().makeCurrent();
                gl = drawContext.getGl();
                this.updateNeeded = false;
            } catch (Throwable th) {
                this.output.copyFromCurrentBuffer(gl);
                pbuffer.deactivate(drawContext);
                throw th;
            }
        }
        gl.glEnable(this.target);
        this.output.bind(drawContext);
    }

    public void bind(int i, DrawContext drawContext) {
        drawContext.getGl().glActiveTexture(i);
        this.output.bind(drawContext);
    }

    public void clearFilters() {
        this.filters.clear();
        this.updateNeeded = true;
    }

    public RenderAsset cloneAsset() {
        return null;
    }

    public Filter getFilter(int i) {
        return this.filters.get(i);
    }

    public int getOriginalHeight() {
        return this.originalHeight;
    }

    public int getOriginalWidth() {
        return this.originalWidth;
    }

    public BaseTexture getOutput() {
        return this.output;
    }

    public void releaseReferences() {
        this.output.releaseReferences();
    }

    public void setFilter(int i, Filter filter) {
        this.filters.set(i, filter);
        this.updateNeeded = true;
    }

    public void setTarget(int i) {
        this.target = i;
    }

    public void update() {
        this.updateNeeded = true;
    }
}
