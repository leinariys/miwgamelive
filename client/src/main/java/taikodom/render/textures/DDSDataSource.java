package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;

/* compiled from: a */
public class DDSDataSource extends TextureDataSource {
    TextureData[] data;
    DDSLoader loader;
    TextureData[] sampleData;
    int sampleHeight;
    int sampleWidth;
    private int mipMapResolution;

    public TextureData getData(int i) {
        if (this.data != null) {
            return this.data[i];
        }
        return this.sampleData[i];
    }

    public int getMipMapResolution() {
        return this.mipMapResolution;
    }

    public void setMipMapResolution(int i) {
        this.mipMapResolution = i;
        if (i == 0) {
        }
    }

    public TextureData[] getData() {
        if (this.data != null) {
            return this.data;
        }
        return this.sampleData;
    }

    public void setData(TextureData[] textureDataArr) {
        this.data = textureDataArr;
    }

    public void setSampleData(TextureData[] textureDataArr) {
        this.sampleData = textureDataArr;
    }

    public void setSampleData(TextureData textureData) {
        this.sampleData = new TextureData[1];
        this.sampleData[0] = textureData;
    }

    public int getSampleWidth() {
        return this.sampleWidth;
    }

    public void setSampleWidth(int i) {
        this.sampleWidth = i;
    }

    public int getSampleHeight() {
        return this.sampleHeight;
    }

    public void setSampleHeight(int i) {
        this.sampleHeight = i;
    }

    public int getMipMapIDByResolution(int i) {
        return this.loader.getMipMapIDByResolution(i);
    }

    public boolean isCubemap() {
        return this.loader.isCubemap();
    }

    public int getNumMipMaps() {
        return this.loader.getNumMipMaps();
    }

    public DDSLoader getLoader() {
        return this.loader;
    }

    public void setLoader(DDSLoader dDSLoader) {
        if (dDSLoader != null) {
            this.loader = dDSLoader;
        }
    }

    public int getRealWidth() {
        return this.loader.getRealWidth();
    }

    public int getRealHeight() {
        return this.loader.getRealHeight();
    }

    public void clearData() {
        this.data = null;
    }
}
