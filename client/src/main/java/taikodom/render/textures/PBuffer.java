package taikodom.render.textures;

import com.sun.opengl.impl.windows.WGL;
import com.sun.opengl.impl.windows.WGLExt;
import com.sun.opengl.impl.windows.WGLExtImpl;
import com.sun.opengl.impl.windows.WindowsGLContext;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.DrawContext;
import taikodom.render.gl.GLSupportedCaps;

import javax.media.opengl.GL;
import javax.media.opengl.GLException;

/* compiled from: a */
@Slf4j
public class PBuffer {

    private boolean active = false;
    private long buffer;
    private WGLExt glExt;
    private long glRc;
    private long hdc;
    private int height;
    private long lastHdc;
    private long lastRc;
    private int width;

    public boolean create(DrawContext drawContext, int i, int i2) {
        this.glExt = new WGLExtImpl((WindowsGLContext) drawContext.getGlContext());
        if (!GLSupportedCaps.isPbuffer()) {
            return false;
        }
        int[] iArr = new int[30];
        float[] fArr = new float[1];
        int[] iArr2 = new int[256];
        iArr[0] = 8209;
        iArr[1] = 0;
        iArr[2] = 8208;
        iArr[3] = 1;
        iArr[4] = 8237;
        iArr[5] = 1;
        iArr[6] = 8215;
        iArr[7] = 8;
        iArr[8] = 8213;
        iArr[9] = 8;
        iArr[10] = 8217;
        iArr[11] = 8;
        iArr[12] = 8219;
        iArr[13] = 8;
        iArr[14] = 8226;
        iArr[15] = 24;
        iArr[16] = 8227;
        iArr[17] = 8;
        iArr[18] = 8211;
        iArr[19] = 8235;
        iArr[20] = 8195;
        iArr[21] = 8231;
        iArr[22] = 0;
        long wglGetCurrentDC = WGL.wglGetCurrentDC();
        long wglGetCurrentContext = WGL.wglGetCurrentContext();
        if (wglGetCurrentDC == 0) {
            log.error("Problem getting the Current Device Context");
            return false;
        }
        int GetPixelFormat = WGL.GetPixelFormat(wglGetCurrentDC);
        if (GetPixelFormat != 0) {
            this.buffer = this.glExt.wglCreatePbufferARB(wglGetCurrentDC, GetPixelFormat, i, i2, iArr, 23);
        } else {
            this.buffer = 0;
            log.warn("Problem trying to get game gl pixel format. Trying to find a suitable format.");
        }
        if (this.buffer == 0) {
            int[] iArr3 = new int[256];
            int[] iArr4 = new int[1];
            if (this.glExt.wglChoosePixelFormatARB(wglGetCurrentDC, iArr, 0, fArr, 0, 255, iArr3, 0, iArr4, 0)) {
                for (int i3 = 0; i3 < iArr4[0]; i3++) {
                    this.buffer = this.glExt.wglCreatePbufferARB(wglGetCurrentDC, iArr3[i3], i, i2, iArr, 23);
                    if (this.buffer != 0) {
                        break;
                    }
                }
            } else {
                throw new GLException("pbuffer creation error: wglChoosePixelFormatARB() failed");
            }
        }
        if (this.buffer == 0) {
            int glGetError = drawContext.getGl().glGetError();
            String num = Integer.toString(glGetError);
            switch (glGetError) {
                case 13:
                    num = "Invalid data";
                    break;
                case 1450:
                    num = "No resources avaliable";
                    break;
                case 2000:
                    num = "Invalid pixel format";
                    break;
            }
            throw new GLException("Pbuffer creation error: no suitable format to generate buffer found. (" + num + ").");
        }
        this.hdc = this.glExt.wglGetPbufferDCARB(this.buffer);
        if (this.hdc == 0) {
            throw new GLException("pbuffer creation error: wglGetPbufferDCARB() failed.");
        }
        this.width = i;
        this.height = i2;
        this.glRc = WGL.wglCreateContext(this.hdc);
        WGL.wglShareLists(wglGetCurrentContext, this.glRc);
        int[] iArr5 = new int[2];
        this.glExt.wglQueryPbufferARB(this.buffer, 8244, iArr5, 0);
        this.glExt.wglQueryPbufferARB(this.buffer, 8245, iArr5, 1);
        return true;
    }

    public void releaseReferences() {
        destroy();
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public void activate(DrawContext drawContext) {
        if (this.active) {
            log.warn("Trying to activate a pbuffer that's already active.");
            return;
        }
        this.active = true;
        GL gl = drawContext.getGl();
        this.lastHdc = WGL.wglGetCurrentDC();
        this.lastRc = WGL.wglGetCurrentContext();
        if (this.lastHdc == 0 || this.lastRc == 0) {
            log.warn("Activating a pbuffer in a thread without active glContext.");
        }
        WGL.wglMakeCurrent(this.hdc, this.glRc);
        gl.glClear(16640);
    }

    public void deactivate(DrawContext drawContext) {
        if (this.active) {
            this.active = false;
            WGL.wglMakeCurrent(this.lastHdc, this.lastRc);
        }
    }

    public void destroy() {
        if (this.glRc != 0) {
            WGL.wglDeleteContext(this.glRc);
            this.glExt.wglReleasePbufferDCARB(this.buffer, this.hdc);
            this.glExt.wglDestroyPbufferARB(this.buffer);
        }
    }

    public boolean isActive() {
        return this.active;
    }

    public void setActive(boolean z) {
        this.active = z;
    }
}
