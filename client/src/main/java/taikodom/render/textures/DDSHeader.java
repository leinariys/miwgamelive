package taikodom.render.textures;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

/* compiled from: a */
public class DDSHeader {
    private static final int MAGIC = 542327876;
    int alphaBitDepth;
    int backBufferCountOrDepth;
    int colorSpaceHighValue;
    int colorSpaceLowValue;
    int ddsCaps1;
    int ddsCaps2;
    int ddsCapsReserved1;
    int ddsCapsReserved2;
    int destBltColorSpaceHighValue;
    int destBltColorSpaceLowValue;
    int flags;
    int height;
    int mipMapCountOrAux;
    int pfABitMask;
    int pfBBitMask;
    int pfFlags;
    int pfFourCC;
    int pfGBitMask;
    int pfRBitMask;
    int pfRGBBitCount;
    int pfSize;
    int pitchOrLinearSize;
    int reserved1;
    int size;
    int srcBltColorSpaceHighValue;
    int srcBltColorSpaceLowValue;
    int srcOverlayColorSpaceHighValue;
    int srcOverlayColorSpaceLowValue;
    int surface;
    int textureStage;
    int width;

    private static int size() {
        return 124;
    }

    private static int pfSize() {
        return 32;
    }

    public static int writtenSize() {
        return 128;
    }

    public final int readInt(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        int read2 = inputStream.read();
        int read3 = inputStream.read();
        int read4 = inputStream.read();
        if ((read | read2 | read3 | read4) < 0) {
            throw new EOFException();
        }
        return (read << 0) + (read2 << 8) + (read3 << 16) + (read4 << 24);
    }

    /* access modifiers changed from: package-private */
    public void read(InputStream inputStream) throws IOException {
        int readInt = readInt(inputStream);
        if (readInt != MAGIC) {
            throw new IOException("Incorrect magic number 0x" + Integer.toHexString(readInt) + " (expected " + MAGIC + ")");
        }
        this.size = readInt(inputStream);
        this.flags = readInt(inputStream);
        this.height = readInt(inputStream);
        this.width = readInt(inputStream);
        this.pitchOrLinearSize = readInt(inputStream);
        this.backBufferCountOrDepth = readInt(inputStream);
        this.mipMapCountOrAux = readInt(inputStream);
        this.alphaBitDepth = readInt(inputStream);
        this.reserved1 = readInt(inputStream);
        this.surface = readInt(inputStream);
        this.colorSpaceLowValue = readInt(inputStream);
        this.colorSpaceHighValue = readInt(inputStream);
        this.destBltColorSpaceLowValue = readInt(inputStream);
        this.destBltColorSpaceHighValue = readInt(inputStream);
        this.srcOverlayColorSpaceLowValue = readInt(inputStream);
        this.srcOverlayColorSpaceHighValue = readInt(inputStream);
        this.srcBltColorSpaceLowValue = readInt(inputStream);
        this.srcBltColorSpaceHighValue = readInt(inputStream);
        this.pfSize = readInt(inputStream);
        this.pfFlags = readInt(inputStream);
        this.pfFourCC = readInt(inputStream);
        this.pfRGBBitCount = readInt(inputStream);
        this.pfRBitMask = readInt(inputStream);
        this.pfGBitMask = readInt(inputStream);
        this.pfBBitMask = readInt(inputStream);
        this.pfABitMask = readInt(inputStream);
        this.ddsCaps1 = readInt(inputStream);
        this.ddsCaps2 = readInt(inputStream);
        this.ddsCapsReserved1 = readInt(inputStream);
        this.ddsCapsReserved2 = readInt(inputStream);
        this.textureStage = readInt(inputStream);
    }

    public int getPfFourCC() {
        return this.pfFourCC;
    }

    public void setPfFourCC(int i) {
        this.pfFourCC = i;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int i) {
        this.width = i;
    }
}
