package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureIO;
import game.geometry.Vector2fWrap;
import gnu.trove.TIntShortHashMap;
import gnu.trove.TShortIntHashMap;
import taikodom.render.DrawContext;
import taikodom.render.data.Vec2fData;
import taikodom.render.loader.provider.BitmapFontFactory;

import javax.media.opengl.GL;
import java.awt.Font;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Vector;

/* compiled from: a */
public class BitmapFont {
    public Texture texture;
    public boolean textureInitialized;
    Vector<WidthData> charWidth = new Vector<>();
    /* renamed from: dc */
    DrawContext f10381dc;
    boolean discartedChars;
    Font font;
    TShortIntHashMap mapDisplayList = null;
    TIntShortHashMap mapIndex = null;
    int numOfValidChars;
    Vec2fData texCoords;
    float textureResolution;
    Vec2fData vertexes;
    private FontMetrics fontMetrics;
    private boolean ready;

    public int getDisplayList(DrawContext drawContext, int i) {
        short s = this.mapIndex.get(i);
        if (this.mapDisplayList.containsKey(s)) {
            return this.mapDisplayList.get(s);
        }
        GL gl = drawContext.getGl();
        int glGenLists = gl.glGenLists(1);
        gl.glNewList(glGenLists, 4864);
        gl.glBegin(7);
        Vector2fWrap aka = new Vector2fWrap();
        Vector2fWrap aka2 = new Vector2fWrap();
        int i2 = s * 4;
        this.vertexes.get(i2, aka);
        this.texCoords.get(i2, aka2);
        gl.glTexCoord2f(aka2.x, aka2.y);
        gl.glVertex2f(aka.x, aka.y);
        this.vertexes.get(i2 + 1, aka);
        this.texCoords.get(i2 + 1, aka2);
        gl.glTexCoord2f(aka2.x, aka2.y);
        gl.glVertex2f(aka.x, aka.y);
        this.vertexes.get(i2 + 2, aka);
        this.texCoords.get(i2 + 2, aka2);
        gl.glTexCoord2f(aka2.x, aka2.y);
        gl.glVertex2f(aka.x, aka.y);
        float f = aka.x;
        this.vertexes.get(i2 + 3, aka);
        this.texCoords.get(i2 + 3, aka2);
        gl.glTexCoord2f(aka2.x, aka2.y);
        gl.glVertex2f(aka.x, aka.y);
        gl.glEnd();
        gl.glTranslatef(f, 0.0f, 0.0f);
        gl.glEndList();
        this.mapDisplayList.put(s, glGenLists);
        return glGenLists;
    }

    public void initialize(Font font2) {
        boolean z;
        float f;
        int i;
        int i2;
        int i3;
        System.nanoTime();
        this.font = font2;
        boolean z2 = false;
        Graphics2D graphics = (Graphics2D) new BufferedImage(1, 1, 2).getGraphics();
        graphics.setColor(Color.WHITE);
        this.textureResolution = 256.0f;
        graphics.setFont(this.font.deriveFont(Math.min((float) this.font.getSize(), 50.0f)));
        BufferedImage readSnapshot = BitmapFontFactory.readSnapshot(this);
        if (readSnapshot != null) {
            z = true;
        } else {
            z = false;
        }
        FontMetrics fontMetrics2 = graphics.getFontMetrics();
        if (!z) {
            int i4 = 0;
            for (int i5 = 0; i5 < 65536; i5++) {
                if (this.font.canDisplay(i5)) {
                    i4 += (fontMetrics2.charWidth(i5) + 5) * (fontMetrics2.getHeight() + 5);
                    if (((float) i4) > this.textureResolution * this.textureResolution) {
                        if (this.textureResolution < ((float) 512)) {
                            this.textureResolution += this.textureResolution;
                        } else {
                            this.discartedChars = true;
                        }
                    }
                    this.numOfValidChars++;
                }
            }
        }
        if (!z) {
            readSnapshot = new BufferedImage((int) this.textureResolution, (int) this.textureResolution, 2);
        }
        Graphics2D graphics2 = (Graphics2D) readSnapshot.getGraphics();
        graphics2.setFont(this.font);
        Graphics2D create = (Graphics2D) graphics2.create();
        if (((float) this.font.getSize()) > 50.0f) {
            create.setFont(this.font.deriveFont(50.0f));
            z2 = true;
        }
        create.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        graphics2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        this.vertexes = new Vec2fData(this.numOfValidChars * 4);
        this.texCoords = new Vec2fData(this.numOfValidChars * 4);
        int i6 = 0;
        float f2 = 0.0f;
        float height = (float) fontMetrics2.getHeight();
        float ascent = (float) (fontMetrics2.getAscent() + fontMetrics2.getDescent() + fontMetrics2.getLeading());
        FontMetrics fontMetrics3 = create.getFontMetrics();
        if (z2) {
            f = (float) (fontMetrics3.getAscent() + fontMetrics3.getDescent() + fontMetrics3.getLeading());
        } else {
            f = ascent;
        }
        this.fontMetrics = fontMetrics2;
        Color color = new Color(255, 255, 255, 0);
        Color color2 = new Color(255, 255, 255, 160);
        Color color3 = new Color(255, 255, 255, 255);
        if (!z) {
            graphics2.setColor(color);
            graphics2.fillRect(0, 0, (int) this.textureResolution, (int) this.textureResolution);
            create.setColor(color3);
        }
        this.mapIndex = new TIntShortHashMap(this.numOfValidChars);
        this.mapDisplayList = new TShortIntHashMap(this.numOfValidChars / 4);
        int[] iArr = new int[1];
        short s = 0;
        int i7 = 0;
        while (i7 < 65536) {
            if (this.font.canDisplay(i7)) {
                if (height > this.textureResolution) {
                    break;
                }
                short s2 = (short) (s + 1);
                this.mapIndex.put(i7, s);
                if (!z) {
                    i = fontMetrics2.charWidth(i7);
                    if (z2) {
                        i2 = fontMetrics3.charWidth(i7);
                    } else {
                        i2 = i;
                    }
                    this.charWidth.add(new WidthData(i, i2));
                } else {
                    WidthData widthData = this.charWidth.get(s2 - 1);
                    i = widthData.width;
                    i2 = widthData.realWidth;
                }
                if (f2 + f > this.textureResolution) {
                    height += 5.0f + f;
                    f2 = 0.0f;
                }
                this.vertexes.set(i6, 0.0f, 0.0f);
                this.texCoords.set(i6, f2 / this.textureResolution, height / this.textureResolution);
                this.vertexes.set(i6 + 1, (float) i, 0.0f);
                this.texCoords.set(i6 + 1, (f2 / this.textureResolution) + (((float) i2) / this.textureResolution), height / this.textureResolution);
                this.vertexes.set(i6 + 2, (float) i, -ascent);
                this.texCoords.set(i6 + 2, (f2 / this.textureResolution) + (((float) i2) / this.textureResolution), (height / this.textureResolution) - (f / this.textureResolution));
                this.vertexes.set(i6 + 3, 0.0f, -ascent);
                this.texCoords.set(i6 + 3, f2 / this.textureResolution, (height / this.textureResolution) - (f / this.textureResolution));
                iArr[0] = i7;
                create.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                if (!z) {
                    create.setColor(color3);
                    create.drawString(new String(iArr, 0, 1), (int) f2, ((int) height) - fontMetrics3.getDescent());
                    create.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    create.setColor(color2);
                    create.drawString(new String(iArr, 0, 1), (int) f2, ((int) height) - fontMetrics3.getDescent());
                }
                f2 += (float) (i2 + 5);
                i3 = i6 + 4;
                s = s2;
            } else {
                i3 = i6;
            }
            i7++;
            i6 = i3;
        }
        if (!z) {
            BitmapFontFactory.writeSnapshot(readSnapshot, i6, this);
        }
        this.texture = new Texture();
        this.texture.setTextureData(0, TextureIO.newTextureData(readSnapshot, false));
        readSnapshot.flush();
        this.textureInitialized = true;
        this.ready = true;
    }

    public void destroyDisplayList() {
        if (this.mapDisplayList != null) {
            for (int glDeleteLists : this.mapDisplayList.getValues()) {
                this.f10381dc.getGl().glDeleteLists(glDeleteLists, 1);
            }
            this.mapDisplayList.clear();
            this.mapDisplayList = null;
        }
    }

    public Font getFont() {
        return this.font;
    }

    public FontMetrics getFontMetrics() {
        return this.fontMetrics;
    }

    public void setFontMetrics(FontMetrics fontMetrics2) {
        this.fontMetrics = fontMetrics2;
    }

    public float getTextureResolution() {
        return this.textureResolution;
    }

    public void setTextureResolution(int i) {
        this.textureResolution = (float) i;
    }

    public int getNumOfValidChars() {
        return this.numOfValidChars;
    }

    public void setNumOfValidChars(int i) {
        this.numOfValidChars = i;
    }

    public Vector<WidthData> getCharWidths() {
        return this.charWidth;
    }

    public void setCharWidths(Vector<WidthData> vector) {
        this.charWidth = vector;
    }

    public boolean isReady() {
        return this.ready;
    }
}
