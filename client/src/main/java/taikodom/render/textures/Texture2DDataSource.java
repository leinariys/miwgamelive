package taikodom.render.textures;

import com.sun.opengl.util.texture.TextureData;

/* compiled from: a */
public class Texture2DDataSource extends TextureDataSource {
    TextureData[] data = new TextureData[1];

    public TextureData[] getData() {
        return this.data;
    }

    public void setData(TextureData textureData) {
        this.data[0] = textureData;
    }

    public void clearData() {
        this.data[0] = null;
    }
}
