package taikodom.render;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import com.sun.opengl.util.GLUT;
import game.geometry.*;
import taikodom.render.camera.Camera;
import taikodom.render.enums.LightType;
import taikodom.render.postProcessing.PostProcessingFX;
import taikodom.render.primitives.Light;
import taikodom.render.primitives.Primitive;
import taikodom.render.primitives.TreeRecord;
import taikodom.render.scene.LightRecordList;
import taikodom.render.scene.Scene;
import taikodom.render.scene.TreeRecordList;
import taikodom.render.scene.Viewport;
import taikodom.render.shaders.RenderStates;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;
import taikodom.render.textures.DDSLoader;
import taikodom.render.textures.Material;
import taikodom.render.textures.RenderTarget;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLContext;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;
import javax.vecmath.Matrix3f;
import java.util.Collections;

/* compiled from: a */
public class RenderView {
    private static RenderTarget framebufferRenderTarget;
    private final TransformWrap camAffineTransform = new TransformWrap();
    private final Matrix4fWrap camProjection = new Matrix4fWrap();
    private final TransformWrap camTransform = new TransformWrap();
    private final Matrix4fWrap gpuCamAffine = new Matrix4fWrap();
    private final Vec3d gpuOffset = new Vec3d();
    private final Viewport viewport = new Viewport();
    GLU glu = new GLU();
    GLUT glut = new GLUT();
    GLUquadric quadric = this.glu.gluNewQuadric();
    private SceneView currentSceneView;
    private RenderStates defaultStates = new RenderStates();
    private DrawContext drawContext = new DrawContext();
    private float[] one = {1.0f, 1.0f, 1.0f, 1.0f};
    private RenderContext renderContext = new RenderContext();
    private RenderInfo renderInfo = new RenderInfo();
    private RenderTarget renderTarget;
    private float[] tempFloatArray = {0.0f, 0.0f, 0.0f, 0.0f};
    private Color white = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    private float[] zero = {0.0f, 0.0f, 0.0f, 0.0f};

    public RenderView(GLContext gLContext) {
        getDrawContext().setRenderView(this);
        getDrawContext().setGlContext(gLContext);
        setGL(gLContext.getGL());
        this.renderContext.setDc(this.drawContext);
    }

    public RenderView(GLAutoDrawable gLAutoDrawable) {
        setViewport(0, 0, gLAutoDrawable.getWidth(), gLAutoDrawable.getHeight());
        setGL(gLAutoDrawable.getGL());
        getDrawContext().setRenderView(this);
        getDrawContext().setGlContext(gLAutoDrawable.getContext());
        this.renderContext.setDc(this.drawContext);
    }

    public static RenderTarget getFramebufferRenderTarget() {
        return framebufferRenderTarget;
    }

    public static void setFramebufferRenderTarget(RenderTarget renderTarget2) {
        framebufferRenderTarget = renderTarget2;
    }

    public void setStartFrame(int i) {
        this.drawContext.setCurrentFrame((long) i);
    }

    public void render(SceneView sceneView, double d, float f) {
        setRenderInfo(sceneView.getRenderInfo());
        this.renderTarget = sceneView.getRenderTarget();
        setViewport(sceneView.getViewport());
        this.renderContext.setSceneViewQuality(sceneView.getSceneViewQuality());
        this.renderContext.setAllowImpostors(sceneView.isAllowImpostors());
        this.currentSceneView = sceneView;
        render(sceneView.getScene(), sceneView.getCamera(), d, f);
    }

    public void render(Scene scene, Camera camera, double d, float f) {
        this.drawContext.setMaxShaderQuality(this.renderContext.getSceneViewQuality().getShaderQuality());
        if (this.drawContext.debugDraw()) {
            this.drawContext.log("----------------------------");
            this.drawContext.log("render");
        }
        this.renderContext.clearRecordsAndLights();
        this.drawContext.incCurrentFrame();
        this.drawContext.setElapsedTime(f);
        this.drawContext.setTotalTime(d);
        this.renderContext.setDeltaTime(f);
        setupCamera(scene, camera);
        scene.render(this.renderContext);
        if (this.renderContext.getImpostorManager() != null && (this.currentSceneView == null || this.currentSceneView.isAllowImpostors())) {
            this.renderContext.getImpostorManager().updateImpostors(this.renderContext, this.currentSceneView);
        }
        this.renderContext.getRecords().sort();
        if (this.currentSceneView != null) {
            for (PostProcessingFX next : this.currentSceneView.getPostProcessingFXs()) {
                if (next.isEnabled()) {
                    next.setSceneIntensity(scene.getColorCorrection());
                    next.preRender(this.renderContext, this.currentSceneView);
                }
            }
        }
        if (this.renderTarget != null) {
            this.renderTarget.bind(this.drawContext);
        }
        renderEntries(camera);
        GL gl = this.drawContext.getGl();
        if (this.renderInfo.isDrawAABBs()) {
            renderAABBs();
            scene.renderAABBs(gl, this.gpuOffset);
        }
        if (this.renderInfo.isDrawLights()) {
            renderLights();
        }
        if (this.renderTarget != null) {
            this.renderTarget.copyBufferToTexture(this.drawContext);
            this.renderTarget.unbind(this.drawContext);
        }
        this.drawContext.rendering = false;
        if (this.currentSceneView != null) {
            for (PostProcessingFX next2 : this.currentSceneView.getPostProcessingFXs()) {
                if (next2.isEnabled()) {
                    next2.render(this.renderContext, this.currentSceneView);
                }
            }
        }
    }

    public void setupCamera(Scene scene, Camera camera) {
        camera.calculateProjectionMatrix();
        this.camProjection.set(camera.getProjection());
        this.camTransform.mo17344b(camera.getTransform());
        this.camTransform.getTranslation(this.gpuOffset);
        this.camTransform.mo17336a(this.camAffineTransform);
        this.renderContext.cameraTransform.set(this.camTransform.orientation);
        this.renderContext.cameraAffineTransform.mo14039j(this.renderContext.cameraTransform);
        this.renderContext.setGpuOffset(this.gpuOffset);
        this.drawContext.setGpuOffset(this.gpuOffset);
        this.renderContext.getFrustum().mo1158a(this.camTransform, this.camProjection);
        this.drawContext.gpuCamTransform.mo13985a((Matrix3f) this.camTransform.orientation);
        this.drawContext.gpuCamTransform.mo14049p(0.0f, 0.0f, 0.0f);
        this.gpuCamAffine.mo14043k(this.drawContext.gpuCamTransform);
        this.drawContext.sceneConfig = scene.getSceneConfig();
        this.drawContext.setViewport(this.viewport.x, this.viewport.y, this.viewport.width, this.viewport.height);
        camera.getTransform().getTranslation(this.drawContext.vec3dTemp0);
        this.renderContext.currentScene = scene;
        this.renderContext.currentCamera = camera;
        scene.fillRenderViewAreaInfo(this.drawContext.vec3dTemp0, this);
    }

    public void renderEntries(Camera camera) {
        int i;
        int i2;
        GL gl = this.drawContext.getGl();
        gl.glViewport(this.viewport.x, this.viewport.y, this.viewport.width, this.viewport.height);
        gl.glClearColor(getClearColor().getRed(), getClearColor().getGreen(), getClearColor().getBlue(), getClearColor().getAlpha());
        gl.glScissor(this.viewport.x, this.viewport.y, this.viewport.width, this.viewport.height);
        gl.glEnable(3089);
        int i3 = isClearColorBuffer() ? DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ : 0;
        if (isClearDepthBuffer()) {
            i = 256;
        } else {
            i = 0;
        }
        int i4 = i | i3;
        if (isClearStencilBuffer()) {
            i2 = 1024;
        } else {
            i2 = 0;
        }
        gl.glClear(i2 | i4);
        gl.glDisable(3089);
        camera.updateGLProjection(this.drawContext);
        updateGLModelview(this.drawContext);
        if (isWireframeOnly()) {
            renderTreeWireFrame();
        } else if (isNoShading()) {
            renderTreeNoShading();
        } else {
            gl.glEnableClientState(32884);
            this.drawContext.primitiveState.setStates(Collections.EMPTY_LIST);
            this.drawContext.primitiveState.setUseColorArray(false);
            this.drawContext.primitiveState.setUseNormalArray(false);
            this.drawContext.primitiveState.setUseTangentArray(false);
            gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
            renderTree();
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void renderTree() {
        /*
            r15 = this;
            taikodom.render.DrawContext r0 = r15.drawContext
            boolean r0 = r0.debugDraw()
            if (r0 == 0) goto L_0x000f
            taikodom.render.DrawContext r0 = r15.drawContext
            java.lang.String r1 = "renderTree"
            r0.log(r1)
        L_0x000f:
            taikodom.render.RenderContext r0 = r15.renderContext
            taikodom.render.scene.TreeRecordList r8 = r0.getRecords()
            r2 = 0
            taikodom.render.DrawContext r0 = r15.drawContext
            javax.media.opengl.GL r9 = r0.mo24978gl()
            taikodom.render.shaders.RenderStates r0 = r15.defaultStates
            taikodom.render.DrawContext r1 = r15.drawContext
            r15.setupGLLights(r1)
            taikodom.render.DrawContext r1 = r15.drawContext
            r15.setupGLFog(r1)
            taikodom.render.shaders.RenderStates r1 = r15.defaultStates
            taikodom.render.DrawContext r3 = r15.drawContext
            r1.blindlyUpdateGLStates(r3)
        L_0x002f:
            int r1 = r8.size()     // Catch:{ Exception -> 0x0137 }
            if (r2 < r1) goto L_0x0048
        L_0x0035:
            r1 = 32884(0x8074, float:4.608E-41)
            r9.glDisableClientState(r1)
            r15.unbindPrimitive()
            if (r0 == 0) goto L_0x012e
            taikodom.render.shaders.RenderStates r1 = r15.defaultStates
            taikodom.render.DrawContext r2 = r15.drawContext
            r1.updateGLStates(r2, r0)
        L_0x0047:
            return
        L_0x0048:
            taikodom.render.primitives.TreeRecord r1 = r8.get(r2)     // Catch:{ Exception -> 0x0137 }
            taikodom.render.shaders.Shader r10 = r1.shader     // Catch:{ Exception -> 0x0137 }
            boolean r1 = r10.isCompiled()     // Catch:{ Exception -> 0x0137 }
            if (r1 != 0) goto L_0x0059
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x0137 }
            r10.compile(r1)     // Catch:{ Exception -> 0x0137 }
        L_0x0059:
            r3 = r2
        L_0x005a:
            int r1 = r8.size()     // Catch:{ Exception -> 0x0137 }
            if (r3 >= r1) goto L_0x0068
            taikodom.render.primitives.TreeRecord r1 = r8.get(r3)     // Catch:{ Exception -> 0x0137 }
            taikodom.render.shaders.Shader r1 = r1.shader     // Catch:{ Exception -> 0x0137 }
            if (r1 == r10) goto L_0x0075
        L_0x0068:
            java.util.List r1 = r10.getShaderPasses()     // Catch:{ Exception -> 0x0137 }
            int r11 = r1.size()     // Catch:{ Exception -> 0x0137 }
            r5 = 0
        L_0x0071:
            if (r5 < r11) goto L_0x0078
            r2 = r3
            goto L_0x002f
        L_0x0075:
            int r3 = r3 + 1
            goto L_0x005a
        L_0x0078:
            r15.unbindPrimitive()     // Catch:{ Exception -> 0x0137 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x0137 }
            r4 = 0
            r1.currentMaterial = r4     // Catch:{ Exception -> 0x0137 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x0137 }
            r4 = 0
            r1.currentPrimitive = r4     // Catch:{ Exception -> 0x0137 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x0137 }
            taikodom.render.ShaderParametersRenderingContext r1 = r1.shaderParamContext     // Catch:{ Exception -> 0x0137 }
            r4 = 0
            r1.setLight(r4)     // Catch:{ Exception -> 0x0137 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x0137 }
            taikodom.render.shaders.ShaderPass r4 = r10.getPass(r5)     // Catch:{ Exception -> 0x0137 }
            r1.currentPass = r4     // Catch:{ Exception -> 0x0137 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x0137 }
            r4.updateStates(r1, r0)     // Catch:{ Exception -> 0x0137 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x0137 }
            taikodom.render.shaders.ShaderPass r1 = r1.currentPass     // Catch:{ Exception -> 0x0137 }
            taikodom.render.shaders.RenderStates r7 = r1.getRenderStates()     // Catch:{ Exception -> 0x0137 }
            taikodom.render.DrawContext r0 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.PrimitiveDrawingState r0 = r0.primitiveState     // Catch:{ Exception -> 0x00e8 }
            r7.fillPrimitiveStates(r0)     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.DrawContext r0 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.MaterialDrawingStates r0 = r0.materialState     // Catch:{ Exception -> 0x00e8 }
            r7.fillMaterialStates(r0)     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.enums.LightType r0 = r4.getUseLights()     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.enums.LightType r1 = taikodom.render.enums.LightType.NONE     // Catch:{ Exception -> 0x00e8 }
            if (r0 == r1) goto L_0x0121
            boolean r0 = r4.getUseFixedFunctionLighting()     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x00cf
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            com.hoplon.geometry.Color r6 = r15.white     // Catch:{ Exception -> 0x00e8 }
            r0 = r15
            r0.renderSubTree(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00e8 }
        L_0x00c6:
            taikodom.render.DrawContext r0 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            r4.unbind(r0)     // Catch:{ Exception -> 0x00e8 }
            int r5 = r5 + 1
            r0 = r7
            goto L_0x0071
        L_0x00cf:
            r0 = 3089(0xc11, float:4.329E-42)
            r9.glEnable(r0)     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.RenderContext r0 = r15.renderContext     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.scene.LightRecordList r12 = r0.getLights()     // Catch:{ Exception -> 0x00e8 }
            int r13 = r12.size()     // Catch:{ Exception -> 0x00e8 }
            r0 = 0
            r6 = r0
        L_0x00e0:
            if (r6 < r13) goto L_0x00f6
            r0 = 3089(0xc11, float:4.329E-42)
            r9.glDisable(r0)     // Catch:{ Exception -> 0x00e8 }
            goto L_0x00c6
        L_0x00e8:
            r1 = move-exception
            r0 = r7
        L_0x00ea:
            java.io.PrintStream r2 = java.lang.System.err
            java.lang.String r3 = "Something wrong happened on render pipeline."
            r2.println(r3)
            r1.printStackTrace()
            goto L_0x0035
        L_0x00f6:
            taikodom.render.scene.LightRecordList$LightRecord r0 = r12.get(r6)     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.primitives.Light r1 = r0.light     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.RenderContext r14 = r15.renderContext     // Catch:{ Exception -> 0x00e8 }
            boolean r1 = r1.setupScissorRectangle(r14)     // Catch:{ Exception -> 0x00e8 }
            if (r1 != 0) goto L_0x0108
        L_0x0104:
            int r0 = r6 + 1
            r6 = r0
            goto L_0x00e0
        L_0x0108:
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            r1.currentLight = r0     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.ShaderParametersRenderingContext r1 = r1.shaderParamContext     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.primitives.Light r0 = r0.light     // Catch:{ Exception -> 0x00e8 }
            r1.setLight(r0)     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.DrawContext r0 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            r4.updateShaderProgramAttribs(r0)     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            r0 = r15
            r0.renderSubTreeByLight(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x00e8 }
            goto L_0x0104
        L_0x0121:
            taikodom.render.DrawContext r0 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            r4.updateShaderProgramAttribs(r0)     // Catch:{ Exception -> 0x00e8 }
            taikodom.render.DrawContext r1 = r15.drawContext     // Catch:{ Exception -> 0x00e8 }
            r6 = 0
            r0 = r15
            r0.renderSubTree(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00e8 }
            goto L_0x00c6
        L_0x012e:
            taikodom.render.shaders.RenderStates r0 = r15.defaultStates
            taikodom.render.DrawContext r1 = r15.drawContext
            r0.blindlyUpdateGLStates(r1)
            goto L_0x0047
        L_0x0137:
            r1 = move-exception
            goto L_0x00ea
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.render.RenderView.renderTree():void");
    }

    /* access modifiers changed from: package-private */
    public void renderSphere(C1085Pt pt, GL gl) {
        gl.glPushMatrix();
        gl.glTranslatef(pt.bny().x, pt.bny().y, pt.bny().z);
        this.glu.gluQuadricDrawStyle(this.quadric, 100012);
        this.glu.gluSphere(this.quadric, 0.5d, 10, 10);
        this.glu.gluQuadricDrawStyle(this.quadric, 100011);
        this.glu.gluSphere(this.quadric, (double) pt.getRadius(), 10, 10);
        gl.glPopMatrix();
    }

    /* access modifiers changed from: package-private */
    public void renderAABB(aLH alh, GL gl) {
        Vec3d dim = alh.dim();
        Vec3d din = alh.din();
        gl.glVertex3d(dim.x, dim.y, dim.z);
        gl.glVertex3d(din.x, dim.y, dim.z);
        gl.glVertex3d(dim.x, dim.y, dim.z);
        gl.glVertex3d(dim.x, din.y, dim.z);
        gl.glVertex3d(dim.x, dim.y, dim.z);
        gl.glVertex3d(dim.x, dim.y, din.z);
        gl.glVertex3d(din.x, din.y, din.z);
        gl.glVertex3d(dim.x, din.y, din.z);
        gl.glVertex3d(din.x, din.y, din.z);
        gl.glVertex3d(din.x, dim.y, din.z);
        gl.glVertex3d(din.x, din.y, din.z);
        gl.glVertex3d(din.x, din.y, dim.z);
        gl.glVertex3d(dim.x, din.y, dim.z);
        gl.glVertex3d(din.x, din.y, dim.z);
        gl.glVertex3d(dim.x, din.y, dim.z);
        gl.glVertex3d(dim.x, din.y, din.z);
        gl.glVertex3d(dim.x, din.y, din.z);
        gl.glVertex3d(dim.x, dim.y, din.z);
        gl.glVertex3d(dim.x, dim.y, din.z);
        gl.glVertex3d(din.x, dim.y, din.z);
        gl.glVertex3d(din.x, dim.y, dim.z);
        gl.glVertex3d(din.x, dim.y, din.z);
        gl.glVertex3d(din.x, dim.y, dim.z);
        gl.glVertex3d(din.x, din.y, dim.z);
    }

    /* access modifiers changed from: package-private */
    public void renderLights() {
        GL gl = this.drawContext.getGl();
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        int size = this.renderContext.lights.size();
        for (int i = 0; i < size; i++) {
            LightRecordList.LightRecord lightRecord = this.renderContext.lights.get(i);
            Light light = lightRecord.light;
            gl.glColor3f(light.getDiffuseColor().x, light.getDiffuseColor().y, light.getDiffuseColor().z);
            renderSphere(lightRecord.cameraSpaceBoundingSphere, gl);
        }
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        gl.glDisable(3042);
        gl.glBlendFunc(1, 1);
    }

    /* access modifiers changed from: package-private */
    public void renderAABBs() {
        GL gl = this.drawContext.getGl();
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glColor3f(1.0f, 0.0f, 0.0f);
        int size = this.renderContext.getRecords().size();
        gl.glBegin(1);
        for (int i = 0; i < size; i++) {
            renderAABB(this.renderContext.getRecords().get(i).cameraSpaceBoundingBox, gl);
        }
        gl.glEnd();
        gl.glColor3f(1.0f, 1.0f, 1.0f);
        gl.glDisable(3042);
        gl.glBlendFunc(1, 1);
    }

    /* access modifiers changed from: package-private */
    public void renderTreeNoShading() {
        if (this.drawContext.debugDraw()) {
            this.drawContext.log("renderTreeWireFrame");
        }
        TreeRecordList records = this.renderContext.getRecords();
        GL gl = this.drawContext.getGl();
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glEnableClientState(32884);
        gl.glEnableClientState(32888);
        gl.glDisable(2884);
        gl.glEnable(2929);
        gl.glEnable(3553);
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setTexChannel(8);
        this.drawContext.primitiveState.clear();
        this.drawContext.materialState.clear();
        this.drawContext.primitiveState.states.add(channelInfo);
        this.drawContext.materialState.states.add(channelInfo);
        Primitive primitive = null;
        Material material = null;
        for (int i = 0; i < records.size(); i++) {
            TreeRecord treeRecord = records.get(i);
            if (primitive == null || primitive.getHandle() != treeRecord.primitive.getHandle()) {
                if (primitive != null && primitive.getUseVBO() && !treeRecord.primitive.getUseVBO()) {
                    unbindPrimitive();
                }
                primitive = treeRecord.primitive;
                primitive.bind(this.drawContext);
            }
            if (material == null || material.getHandle() != treeRecord.material.getHandle()) {
                gl.glMatrixMode(5890);
                treeRecord.material.bind(this.drawContext);
                material = treeRecord.material;
                gl.glMatrixMode(5888);
            }
            gl.glPushMatrix();
            this.drawContext.tempBuffer0.clear();
            treeRecord.gpuTransform.asColumnMajorBuffer(this.drawContext.tempBuffer0);
            this.drawContext.tempBuffer0.flip();
            gl.glMultMatrixf(this.drawContext.tempBuffer0);
            primitive.draw(this.drawContext);
            gl.glPopMatrix();
        }
        gl.glPolygonMode(1028, 6914);
        gl.glPolygonMode(1029, 6914);
        gl.glDisableClientState(32884);
        gl.glDisableClientState(32888);
        gl.glDisable(3042);
        gl.glBlendFunc(1, 1);
        gl.glEnable(2884);
        gl.glDisable(3553);
        unbindPrimitive();
        this.defaultStates.blindlyUpdateGLStates(this.drawContext);
        this.drawContext.primitiveState.clear();
        this.drawContext.materialState.clear();
    }

    /* access modifiers changed from: package-private */
    public void renderTreeWireFrame() {
        if (this.drawContext.debugDraw()) {
            this.drawContext.log("renderTreeWireFrame");
        }
        TreeRecordList records = this.renderContext.getRecords();
        GL gl = this.drawContext.getGl();
        gl.glEnable(3042);
        gl.glBlendFunc(770, 771);
        gl.glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
        gl.glPolygonMode(1028, 6913);
        gl.glPolygonMode(1029, 6913);
        gl.glEnableClientState(32884);
        gl.glDisable(2884);
        Primitive primitive = null;
        for (int i = 0; i < records.size(); i++) {
            TreeRecord treeRecord = records.get(i);
            if (primitive != treeRecord.primitive) {
                if (primitive != null && primitive.getUseVBO() && !treeRecord.primitive.getUseVBO()) {
                    unbindPrimitive();
                }
                primitive = treeRecord.primitive;
                primitive.bind(this.drawContext);
            }
            gl.glPushMatrix();
            this.drawContext.tempBuffer0.clear();
            treeRecord.gpuTransform.asColumnMajorBuffer(this.drawContext.tempBuffer0);
            this.drawContext.tempBuffer0.flip();
            gl.glMultMatrixf(this.drawContext.tempBuffer0);
            if (primitive != null) {
                primitive.draw(this.drawContext);
            }
            gl.glPopMatrix();
        }
        gl.glPolygonMode(1028, 6914);
        gl.glPolygonMode(1029, 6914);
        gl.glDisableClientState(32884);
        gl.glDisable(3042);
        gl.glBlendFunc(1, 1);
        gl.glEnable(2884);
        unbindPrimitive();
    }

    private void setupGLFog(DrawContext drawContext2) {
        drawContext2.getGl().glFogi(2917, getFogType());
        drawContext2.getGl().glFogf(2915, getFogStart());
        drawContext2.getGl().glFogf(2916, getFogEnd());
        drawContext2.getGl().glFogf(2914, getFogExp());
        drawContext2.floatBuffer60Temp0.clear();
        drawContext2.getGl().glFogfv(2918, getFogColor().fillFloatBuffer(drawContext2.floatBuffer60Temp0));
    }

    private void setupGLLights(DrawContext drawContext2) {
        GL gl = drawContext2.getGl();
        sortLights();
        int size = this.renderContext.lights.size();
        for (int i = size; i < 8; i++) {
            gl.glDisable(i + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
        }
        int i2 = 0;
        while (i2 < size && i2 < 8) {
            LightRecordList.LightRecord lightRecord = this.renderContext.lights.get(i2);
            Light light = lightRecord.light;
            this.tempFloatArray[0] = light.getDiffuseColor().x;
            this.tempFloatArray[1] = light.getDiffuseColor().y;
            this.tempFloatArray[2] = light.getDiffuseColor().z;
            this.tempFloatArray[3] = light.getDiffuseColor().w;
            gl.glLightfv(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4609, this.tempFloatArray, 0);
            gl.glLightfv(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4610, this.zero, 0);
            if (light.getLightType() == LightType.DIRECTIONAL_LIGHT) {
                this.tempFloatArray[0] = -light.getDirection().x;
                this.tempFloatArray[1] = -light.getDirection().y;
                this.tempFloatArray[2] = -light.getDirection().z;
                this.tempFloatArray[3] = 0.0f;
                gl.glLightfv(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4611, this.tempFloatArray, 0);
                gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4615, 0.0f);
                gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4616, 0.0f);
                gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4617, 0.0f);
            } else {
                Vec3f bny = lightRecord.cameraSpaceBoundingSphere.bny();
                this.tempFloatArray[0] = bny.x;
                this.tempFloatArray[1] = bny.y;
                this.tempFloatArray[2] = bny.z;
                this.tempFloatArray[3] = 1.0f;
                gl.glLightfv(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4611, this.tempFloatArray, 0);
                if (light.getLightType() == LightType.SPOT_LIGHT) {
                    gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4614, (float) ((180.0d * Math.acos((double) light.getCosCutoff())) / 3.141592653589793d));
                    this.tempFloatArray[0] = light.getDirection().x;
                    this.tempFloatArray[1] = light.getDirection().y;
                    this.tempFloatArray[2] = light.getDirection().z;
                    this.tempFloatArray[3] = 1.0f;
                    gl.glLightfv(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4612, this.tempFloatArray, 0);
                    gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4613, light.getSpotExponent());
                } else {
                    gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4614, 180.0f);
                }
                gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4615, 0.0f);
                gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4616, 1.0f / light.getRadius());
                gl.glLightf(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4617, 1.0f / (light.getRadius() * light.getRadius()));
            }
            gl.glEnable(i2 + DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ);
            i2++;
        }
    }

    private void sortLights() {
        if (this.renderContext.lights.size() > 0) {
            for (int i = 0; i < this.renderContext.lights.size(); i++) {
                LightRecordList.LightRecord lightRecord = this.renderContext.lights.get(i);
                lightRecord.light.setCurrentDistanceToCamera(lightRecord.light.getPosition().mo9491ax(this.camTransform.position));
            }
            this.renderContext.lights.sort();
        }
    }

    private void unbindPrimitive() {
        this.drawContext.getGl().glBindBufferARB(34962, 0);
        this.drawContext.getGl().glBindBufferARB(34963, 0);
    }

    public void updateGLModelview(DrawContext drawContext2) {
        GL gl = drawContext2.getGl();
        gl.glMatrixMode(5888);
        gl.glLoadIdentity();
        drawContext2.tempBuffer0.clear();
        this.gpuCamAffine.asColumnMajorBuffer(drawContext2.tempBuffer0);
        drawContext2.tempBuffer0.flip();
        gl.glMultMatrixf(drawContext2.tempBuffer0);
    }

    public void setGL(GL gl) {
        this.drawContext.setGL(gl);
    }

    private void renderSubTreeByLight(DrawContext drawContext2, int i, int i2, ShaderPass shaderPass, int i3) {
        if (drawContext2.debugDraw()) {
            drawContext2.log("renderSubTreeByLight");
        }
        TreeRecordList records = this.renderContext.getRecords();
        while (i < i2) {
            TreeRecord treeRecord = records.get(i);
            if (shaderPass.getUseLights() == drawContext2.currentLight.light.getLightType() && (drawContext2.currentLight.light.getLightType() == LightType.DIRECTIONAL_LIGHT || treeRecord.cameraSpaceBoundingBox.mo9836a(drawContext2.currentLight.cameraSpaceBoundingSphere))) {
                drawContext2.currentRecord = treeRecord;
                GL gl = drawContext2.getGl();
                if (drawContext2.currentMaterial == null || treeRecord.material.getHandle() != drawContext2.currentMaterial.getHandle()) {
                    gl.glMatrixMode(5890);
                    treeRecord.material.bind(drawContext2);
                    gl.glMatrixMode(5888);
                    drawContext2.currentMaterial = treeRecord.material;
                    shaderPass.updateMaterialProgramAttribs(drawContext2);
                    treeRecord.material.updateShaderParameters(i3, drawContext2);
                }
                if (drawContext2.currentPrimitive == null || treeRecord.primitive.getHandle() != drawContext2.currentPrimitive.getHandle()) {
                    if (drawContext2.currentPrimitive != null && drawContext2.currentPrimitive.getUseVBO() && !treeRecord.primitive.getUseVBO()) {
                        unbindPrimitive();
                    }
                    treeRecord.primitive.bind(drawContext2);
                    drawContext2.currentPrimitive = treeRecord.primitive;
                }
                gl.glPushMatrix();
                shaderPass.updatePrimitiveProgramAttribs(drawContext2);
                Color color = treeRecord.color;
                if (shaderPass.getUseSceneAmbientColor()) {
                    Color color2 = treeRecord.areaInfo.ambientColor;
                    gl.glColor4f(color.x * color2.x, color.y * color2.y, color.z * color2.z, color.w * color2.w);
                } else {
                    gl.glColor4f(color.x, color.y, color.z, color.w);
                }
                drawContext2.floatBuffer60Temp0.clear();
                gl.glMultMatrixf(treeRecord.gpuTransform.mo13998b(drawContext2.floatBuffer60Temp0));
                if (treeRecord.occlusionQuery != null) {
                    treeRecord.occlusionQuery.begin(gl);
                    drawContext2.currentPrimitive.draw(drawContext2);
                    treeRecord.occlusionQuery.end(gl);
                } else {
                    drawContext2.currentPrimitive.draw(drawContext2);
                }
                gl.glPopMatrix();
            }
            i++;
        }
        if (drawContext2.currentMaterial != null) {
            drawContext2.currentMaterial.unbind(drawContext2);
        }
    }

    private void renderSubTree(DrawContext drawContext2, int i, int i2, ShaderPass shaderPass, int i3, Color color) {
        if (drawContext2.debugDraw()) {
            drawContext2.log("renderSubTree");
        }
        GL gl = drawContext2.getGl();
        drawContext2.currentMaterial = null;
        drawContext2.currentPrimitive = null;
        TreeRecordList records = this.renderContext.getRecords();
        while (i < i2) {
            TreeRecord treeRecord = records.get(i);
            drawContext2.currentRecord = treeRecord;
            if (drawContext2.currentMaterial == null || treeRecord.material.getHandle() != drawContext2.currentMaterial.getHandle()) {
                gl.glMatrixMode(5890);
                treeRecord.material.bind(drawContext2);
                gl.glMatrixMode(5888);
                unbindAdditionalTextures(drawContext2);
                drawContext2.currentMaterial = treeRecord.material;
                shaderPass.updateMaterialProgramAttribs(drawContext2);
                treeRecord.material.updateShaderParameters(i3, drawContext2);
            }
            if (drawContext2.currentPrimitive == null || treeRecord.primitive.getHandle() != drawContext2.currentPrimitive.getHandle()) {
                if (drawContext2.currentPrimitive != null && drawContext2.currentPrimitive.getUseVBO() && !treeRecord.primitive.getUseVBO()) {
                    unbindPrimitive();
                }
                treeRecord.primitive.bind(drawContext2);
                drawContext2.currentPrimitive = treeRecord.primitive;
            }
            bindAdditionalTextures(drawContext2, shaderPass, treeRecord);
            gl.glPushMatrix();
            shaderPass.updatePrimitiveProgramAttribs(drawContext2);
            if (shaderPass.getUseFixedFunctionLighting()) {
                drawContext2.floatBuffer60Temp0.clear();
                gl.glMaterialfv(1028, 4608, treeRecord.areaInfo.ambientColor.fillFloatBuffer(drawContext2.floatBuffer60Temp0));
                gl.glMaterialfv(1028, 4608, this.one, 0);
                gl.glLightfv(DDSLoader.DDSCAPS2_CUBEMAP_POSITIVEZ, 4608, treeRecord.areaInfo.ambientColor.fillFloatBuffer(drawContext2.floatBuffer60Temp0));
            }
            Color color2 = treeRecord.color;
            if (color != null) {
                gl.glColor4f(color.x, color.y, color.z, color.w);
            } else if (!shaderPass.getUseSceneAmbientColor() || treeRecord.areaInfo == null) {
                gl.glColor4f(color2.x, color2.y, color2.z, color2.w);
            } else {
                Color color3 = treeRecord.areaInfo.ambientColor;
                gl.glColor4f(color2.x * color3.x, color2.y * color3.y, color2.z * color3.z, color2.w * color3.w);
            }
            gl.glMultMatrixf(treeRecord.gpuTransform.mo13998b(drawContext2.floatBuffer60Temp0));
            if (treeRecord.occlusionQuery != null) {
                treeRecord.occlusionQuery.begin(gl);
                drawContext2.currentPrimitive.draw(drawContext2);
                treeRecord.occlusionQuery.end(gl);
            } else {
                drawContext2.currentPrimitive.draw(drawContext2);
            }
            gl.glPopMatrix();
            i++;
        }
        if (drawContext2.currentMaterial != null) {
            drawContext2.currentMaterial.unbind(drawContext2);
        }
        unbindAdditionalTextures(drawContext2);
    }

    private void unbindAdditionalTextures(DrawContext drawContext2) {
        if (drawContext2.currentMaterial != null && drawContext2.currentMaterial.isUseFramebufferRGB()) {
            int i = 0;
            while (i < drawContext2.materialState.getStates().size()) {
                if (drawContext2.materialState.getTexInfo(i).getTexChannel() != 14) {
                    i++;
                } else if (framebufferRenderTarget != null) {
                    GL gl = drawContext2.getGl();
                    gl.glActiveTexture(i + 33984);
                    gl.glDisable(framebufferRenderTarget.getTexture().getTarget());
                    return;
                } else {
                    return;
                }
            }
        }
    }

    private void bindAdditionalTextures(DrawContext drawContext2, ShaderPass shaderPass, TreeRecord treeRecord) {
        int i = 0;
        if (shaderPass.getUseDiffuseCubemap() && treeRecord.areaInfo != null && treeRecord.areaInfo.diffuseCubemap != null) {
            int i2 = 0;
            while (true) {
                if (i2 >= drawContext2.materialState.getStates().size()) {
                    break;
                } else if (drawContext2.materialState.getTexInfo(i2).getTexChannel() == 1) {
                    treeRecord.areaInfo.diffuseCubemap.bind(i2 + 33984, drawContext2);
                    treeRecord.areaInfo.diffuseCubemap.addArea(1024.0f);
                    break;
                } else {
                    i2++;
                }
            }
        }
        if (shaderPass.getUseReflectCubemap() && treeRecord.areaInfo != null && treeRecord.areaInfo.reflectiveCubemap != null) {
            int i3 = 0;
            while (true) {
                if (i3 >= drawContext2.materialState.getStates().size()) {
                    break;
                } else if (drawContext2.materialState.getTexInfo(i3).getTexChannel() == 0) {
                    treeRecord.areaInfo.reflectiveCubemap.bind(i3 + 33984, drawContext2);
                    treeRecord.areaInfo.reflectiveCubemap.addArea(1024.0f);
                    break;
                } else {
                    i3++;
                }
            }
        }
        if (drawContext2.currentMaterial.isUseFramebufferRGB()) {
            while (i < drawContext2.materialState.getStates().size()) {
                if (drawContext2.materialState.getTexInfo(i).getTexChannel() != 14) {
                    i++;
                } else if (framebufferRenderTarget != null) {
                    framebufferRenderTarget.getTexture().bind(i + 33984, drawContext2);
                    return;
                } else {
                    return;
                }
            }
        }
    }

    public boolean projectMouseIntoWorld(Camera camera, float f, float f2, float f3, Vec3d ajr) {
        ajr.x = (double) (f / ((float) getViewport().width));
        ajr.y = (double) ((((float) getViewport().height) - f2) / ((float) getViewport().height));
        ajr.z = (double) f3;
        Matrix4fWrap ajk = new Matrix4fWrap();
        Vector4fWrap ajf = new Vector4fWrap();
        Vector4fWrap ajf2 = new Vector4fWrap();
        ajf.x = (float) ((ajr.x * 2.0d) - 1.0d);
        ajf.y = (float) ((ajr.y * 2.0d) - 1.0d);
        ajf.z = (float) ((ajr.z * 2.0d) - 1.0d);
        ajf.w = 1.0f;
        ajk.invert(camera.getProjection());
        ajk.transform(ajf, ajf2);
        if (ajf2.w == 0.0f) {
            return false;
        }
        ajr.x = (double) (ajf2.x * ajf2.w);
        ajr.y = (double) (ajf2.y * ajf2.w);
        ajr.z = (double) (ajf2.z * ajf2.w);
        camera.getTransform().mo17331a(ajr);
        return true;
    }

    public boolean isGLSLSupported() {
        return true;
    }

    public DrawContext getDrawContext() {
        return this.drawContext;
    }

    public Viewport getViewport() {
        return this.viewport;
    }

    public void setViewport(Viewport viewport2) {
        this.viewport.set(viewport2);
    }

    public void setViewport(int i, int i2, int i3, int i4) {
        this.viewport.x = i;
        this.viewport.y = i2;
        this.viewport.width = i3;
        this.viewport.height = i4;
    }

    public RenderInfo getRenderInfo() {
        return this.renderInfo;
    }

    public void setRenderInfo(RenderInfo renderInfo2) {
        this.renderInfo.set(renderInfo2);
    }

    public Color getClearColor() {
        return this.renderInfo.getClearColor();
    }

    public Color getFogColor() {
        return this.renderInfo.getFogColor();
    }

    public void setFogColor(Color color) {
        this.renderInfo.setFogColor(color);
    }

    public float getFogEnd() {
        return this.renderInfo.getFogEnd();
    }

    public void setFogEnd(float f) {
        this.renderInfo.setFogEnd(f);
    }

    public float getFogExp() {
        return this.renderInfo.getFogExp();
    }

    public void setFogExp(float f) {
        this.renderInfo.setFogExp(f);
    }

    public float getFogStart() {
        return this.renderInfo.getFogStart();
    }

    public void setFogStart(float f) {
        this.renderInfo.setFogStart(f);
    }

    public int getFogType() {
        return this.renderInfo.getFogType();
    }

    public void setFogType(int i) {
        this.renderInfo.setFogType(i);
    }

    public boolean isClearColorBuffer() {
        return this.renderInfo.isClearColorBuffer();
    }

    public void setClearColorBuffer(boolean z) {
        this.renderInfo.setClearColorBuffer(z);
    }

    public boolean isClearDepthBuffer() {
        return this.renderInfo.isClearDepthBuffer();
    }

    public void setClearDepthBuffer(boolean z) {
        this.renderInfo.setClearDepthBuffer(z);
    }

    public boolean isClearStencilBuffer() {
        return this.renderInfo.isClearStencilBuffer();
    }

    public void setClearStencilBuffer(boolean z) {
        this.renderInfo.setClearStencilBuffer(z);
    }

    public boolean isWireframeOnly() {
        return this.renderInfo.isWireframeOnly();
    }

    public void setWireframeOnly(boolean z) {
        this.renderInfo.setWireframeOnly(z);
    }

    public boolean isNoShading() {
        return this.renderInfo.isNoShading();
    }

    public RenderContext getRenderContext() {
        return this.renderContext;
    }
}
