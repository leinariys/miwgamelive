package taikodom.render;

import taikodom.render.loader.RenderAsset;

import java.nio.Buffer;

/* compiled from: a */
public abstract class SoundBuffer extends RenderAsset {
    public int bitDepth = 0;
    public int channels = 0;
    public Buffer data = null;
    public long dataLenght = 0;
    public int length = 0;
    public int sampleRate = 0;
    public float timeLenght = 1000000.0f;

    public abstract SoundBuffer cloneAsset();

    public abstract Buffer getData();

    public abstract String getFileName();

    public abstract boolean setData(Buffer buffer, int i);

    public void releaseReferences() {
    }

    public void forceSyncLoading() {
    }

    /* access modifiers changed from: package-private */
    public boolean isValid() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public float getTimeLength() {
        return 0.0f;
    }

    /* access modifiers changed from: package-private */
    public long getSampleCount() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getSampleRate() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getChannels() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getBitDepth() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getBufferId() {
        return 0;
    }
}
