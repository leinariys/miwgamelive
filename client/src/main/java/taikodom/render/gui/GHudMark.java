package taikodom.render.gui;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.IGeometryF;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.geometry.aLH;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.GuiContext;
import taikodom.render.SceneView;
import taikodom.render.StepContext;
import taikodom.render.camera.Camera;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.Material;

import javax.vecmath.Tuple3d;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: a */
public class GHudMark extends GuiSceneObject {
    private static Billboard billBoard = new Billboard();
    private final Vec3f screenPosition = new Vec3f();
    private final Vec3d tempVec = new Vec3d();
    private final Vec3f tempVec2 = new Vec3f();
    private float anim = 0.0f;
    private float areaRadius;
    private Color centerColor;
    private boolean isOffScreen;
    private int lastPx = Integer.MAX_VALUE;
    private int lastPy = Integer.MAX_VALUE;
    private Material lock;
    private int lockImgHeight;
    private int lockImgWidth;
    private Material mark;
    private int markImgHeight;
    private int markImgWidth;
    private Material markOut;
    private Material markSelected;
    private ArrayList<Vector2fWrap> marks = new ArrayList<>(4);
    private int maxLockSize;
    private float maxVisibleDistance;
    private int minLockSize;
    private Vector2fWrap offSet = new Vector2fWrap(0.0f, 0.0f);
    private Color outColor;
    private SceneObject referenceObject;
    private Color satellitesColor;
    private float screenSize;
    private Color selectedColor;
    private boolean shouldTrack;
    private boolean showSatellites;
    private boolean showWhenOffscreen = true;
    private boolean smooth = false;
    private Vector2fWrap temp = new Vector2fWrap();

    public GHudMark() {
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
    }

    public GHudMark(GHudMark gHudMark) {
        super(gHudMark);
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
        this.marks.add(this.temp);
        this.temp = new Vector2fWrap();
        setMark(gHudMark.mark);
        this.markSelected = gHudMark.markSelected;
        this.markOut = gHudMark.markOut;
        setLock(gHudMark.lock);
        this.centerColor = gHudMark.centerColor;
        this.outColor = gHudMark.outColor;
        this.satellitesColor = gHudMark.satellitesColor;
        this.selectedColor = gHudMark.selectedColor;
        this.maxLockSize = gHudMark.maxLockSize;
        this.minLockSize = gHudMark.minLockSize;
        this.areaRadius = gHudMark.areaRadius;
    }

    /* access modifiers changed from: package-private */
    public float getScreenSize(float f) {
        Camera camera = this.sceneView.getCamera();
        SceneObject sceneObject = this.referenceObject;
        Vec3d ajr = new Vec3d();
        sceneObject.getGlobalTransform().getTranslation(ajr);
        aLH aabbWorldSpace = sceneObject.getAabbWorldSpace();
        return (f / ((camera.getTanHalfFovY() * 2.0f) * ((float) camera.getPosition().mo9517f((Tuple3d) ajr).length()))) * ((float) aabbWorldSpace.din().mo9517f((Tuple3d) aabbWorldSpace.dim()).length());
    }

    public int step(StepContext stepContext) {
        if (this.referenceObject == null) {
            return 0;
        }
        double ax = this.referenceObject.getPosition().mo9491ax(this.sceneView.getCamera().getPosition());
        float f = (float) (this.sceneView.getViewport().width / 2);
        float f2 = (float) (this.sceneView.getViewport().height / 2);
        if (ax <= ((double) this.maxVisibleDistance) || this.maxVisibleDistance <= 0.0f) {
            this.anim += stepContext.getDeltaTime() * 0.001f;
            this.shouldTrack = true;
        } else {
            this.anim -= stepContext.getDeltaTime() * 0.001f;
            this.shouldTrack = false;
        }
        if (this.anim > 1.0f) {
            this.anim = 1.0f;
        }
        if (this.anim < 0.0f) {
            this.anim = 0.0f;
        }
        if (this.shouldTrack || !this.showWhenOffscreen) {
            if (this.sceneView == null) {
                return 0;
            }
            this.sceneView.getCamera().getTransform().getZ(this.tempVec2);
            this.tempVec.mo9484aA(this.sceneView.getCamera().getPosition());
            this.tempVec.mo9520g(this.referenceObject.getPosition());
            this.tempVec.normalize();
            double b = this.tempVec.mo9494b((IGeometryF) this.tempVec2);
            this.sceneView.getWindowCoords(this.referenceObject.getPosition(), this.tempVec);
            this.screenPosition.set(this.tempVec);
            this.screenPosition.mo23468E(this.offSet.x, this.offSet.y, 0.0f);
            if (this.tempVec.x < ((double) this.sceneView.getViewport().x) || this.tempVec.x > ((double) this.sceneView.getViewport().width) || this.tempVec.y < ((double) this.sceneView.getViewport().y) || this.tempVec.y > ((double) this.sceneView.getViewport().height) || b < ScriptRuntime.NaN) {
                this.isOffScreen = true;
                if (!this.showWhenOffscreen) {
                    return 0;
                }
                this.tempVec.mo9529o((double) f, (double) f2, ScriptRuntime.NaN);
                this.tempVec.normalize();
                if (b < ScriptRuntime.NaN) {
                    this.tempVec.y = -this.tempVec.y;
                    this.tempVec.x = -this.tempVec.x;
                }
                this.tempVec.scale((double) this.areaRadius);
                this.tempVec.mo9558z((double) f, (double) f2, 1.0d);
                this.screenPosition.set(this.tempVec);
                this.screenPosition.mo23468E(this.offSet.x, this.offSet.y, 0.0f);
            } else {
                this.isOffScreen = false;
            }
        } else if (this.anim == 0.0f) {
            setPosition(Math.round(this.screenPosition.x), Math.round(this.screenPosition.y));
            return 0;
        }
        this.screenSize = (float) Math.max(this.markImgWidth, this.markImgHeight);
        float f3 = ((1.0f - this.anim) * 40.0f) + (this.screenSize / 2.0f);
        float f4 = 360.0f * this.anim;
        this.temp.x = (float) (Math.sin((double) ((45.0f + f4) * 0.017453292f)) * ((double) f3));
        this.temp.y = (float) (Math.cos((double) ((45.0f + f4) * 0.017453292f)) * ((double) f3));
        this.temp.add(this.tempVec.x, this.tempVec.y);
        this.marks.get(0).set(this.temp);
        this.temp.x = (float) (Math.sin((double) ((135.0f + f4) * 0.017453292f)) * ((double) f3));
        this.temp.y = (float) (Math.cos((double) ((135.0f + f4) * 0.017453292f)) * ((double) f3));
        this.temp.add(this.tempVec.x, this.tempVec.y);
        this.marks.get(1).set(this.temp);
        this.temp.x = (float) (Math.sin((double) ((225.0f + f4) * 0.017453292f)) * ((double) f3));
        this.temp.y = (float) (Math.cos((double) ((225.0f + f4) * 0.017453292f)) * ((double) f3));
        this.temp.add(this.tempVec.x, this.tempVec.y);
        this.marks.get(2).set(this.temp);
        this.temp.x = (float) (Math.sin((double) ((315.0f + f4) * 0.017453292f)) * ((double) f3));
        this.temp.y = (float) (Math.cos((double) ((f4 + 315.0f) * 0.017453292f)) * ((double) f3));
        this.temp.add(this.tempVec.x, this.tempVec.y);
        this.marks.get(3).set(this.temp);
        setPosition(Math.round(this.screenPosition.x), Math.round(this.screenPosition.y));
        return super.step(stepContext);
    }

    public boolean isSmooth() {
        return !this.smooth;
    }

    public void setSmooth(boolean z) {
        this.smooth = z;
    }

    public void jump() {
        this.lastPx = Integer.MAX_VALUE;
        this.lastPy = Integer.MAX_VALUE;
    }

    /* access modifiers changed from: protected */
    public void setPosition(int i, int i2) {
        if (!this.smooth || (this.lastPy == i2 && this.lastPx == i)) {
            this.lastPx = i;
            this.lastPy = i2;
            this.position.set((float) this.lastPx, (float) this.lastPy);
        } else if (this.lastPx == Integer.MAX_VALUE || this.lastPy == Integer.MAX_VALUE) {
            this.lastPx = i;
            this.lastPy = i2;
            this.position.set((float) i, (float) i2);
        } else {
            int abs = Math.abs(this.lastPx - i);
            int abs2 = Math.abs(this.lastPy - i2);
            int ceil = (int) Math.ceil(((double) abs) / 2.0d);
            if (abs <= 2) {
                this.lastPx = i;
            } else if (this.lastPx > i) {
                this.lastPx -= ceil;
            } else {
                this.lastPx += ceil;
            }
            int ceil2 = (int) Math.ceil(((double) abs2) / 2.0d);
            if (abs2 <= 2) {
                this.lastPy = i2;
            } else if (this.lastPy > i2) {
                this.lastPy -= ceil2;
            } else {
                this.lastPy = ceil2 + this.lastPy;
            }
            this.position.set((float) this.lastPx, (float) this.lastPy);
        }
    }

    public void render(GuiContext guiContext) {
        float f;
        float f2;
        float f3;
        float f4;
        float f5 = this.anim * 360.0f;
        if (!this.render) {
            return;
        }
        if ((!this.isOffScreen || this.showWhenOffscreen) && this.mark != null && this.mark.getDiffuseTexture() != null) {
            guiContext.getDc().getGl();
            if (!this.isOffScreen) {
                float f6 = this.centerColor.x;
                float f7 = this.centerColor.y;
                float f8 = this.centerColor.z;
                f = this.centerColor.w;
                f2 = f8;
                f3 = f7;
                f4 = f6;
            } else {
                float f9 = this.outColor.x;
                float f10 = this.outColor.y;
                float f11 = this.outColor.z;
                f = this.outColor.w;
                f2 = f11;
                f3 = f10;
                f4 = f9;
            }
            this.markImgWidth = (int) Math.abs(this.mark.getDiffuseTexture().getTransformedWidth() * this.mark.getXScaling());
            this.markImgHeight = (int) Math.abs(this.mark.getDiffuseTexture().getTransformedHeight() * this.mark.getYScaling());
            this.globalTransform.setIdentity();
            this.globalTransform.setTranslation((double) (this.position.x + (((float) (this.markImgWidth % 2)) * 0.5f)), (double) (this.position.y + (((float) (this.markImgHeight % 2)) * 0.5f)), ScriptRuntime.NaN);
            this.globalTransform.mo17324B((float) this.markImgWidth);
            this.globalTransform.mo17325C((float) this.markImgHeight);
            this.primitiveColor.set(f4, f3, f2, f);
            guiContext.addPrimitive(this.mark.getShader(), this.mark, billBoard, this.globalTransform, this.primitiveColor);
            float f12 = 180.0f - (this.anim * 360.0f);
            if (this.showSatellites) {
                Iterator<Vector2fWrap> it = this.marks.iterator();
                float f13 = f12;
                while (it.hasNext()) {
                    Vector2fWrap next = it.next();
                    this.globalTransform.setIdentity();
                    this.globalTransform.setTranslation((double) ((int) next.x), (double) ((int) next.y), ScriptRuntime.NaN);
                    this.globalTransform.mo17324B((float) this.lockImgWidth);
                    this.globalTransform.mo17325C((float) this.lockImgHeight);
                    this.transform.setIdentity();
                    this.transform.mo17323A(f13);
                    this.globalTransform.mo17350e(this.transform);
                    f13 -= 90.0f;
                    guiContext.addPrimitive(this.lock.getShader(), this.lock, billBoard, this.globalTransform, this.satellitesColor);
                }
            }
        }
    }

    public RenderAsset cloneAsset() {
        return new GHudMark(this);
    }

    public Material getMark() {
        return this.mark;
    }

    public void setMark(Material material) {
        this.mark = material;
        if (material != null) {
            this.markImgWidth = (int) Math.abs(material.getDiffuseTexture().getTransformedWidth() * material.getXScaling());
            this.markImgHeight = (int) Math.abs(material.getDiffuseTexture().getTransformedHeight() * material.getYScaling());
        }
    }

    public void validate(SceneLoader sceneLoader) {
        this.mark.setShader(sceneLoader.getDefaultHudMarkShader());
        this.lock.setShader(sceneLoader.getDefaultHudMarkShader());
        this.markOut.setShader(sceneLoader.getDefaultHudMarkShader());
        super.validate(sceneLoader);
    }

    public int getMaxLockSize() {
        return this.maxLockSize;
    }

    public void setMaxLockSize(int i) {
        this.maxLockSize = i;
    }

    public int getMinLockSize() {
        return this.minLockSize;
    }

    public void setMinLockSize(int i) {
        this.minLockSize = i;
    }

    public float getAreaRadius() {
        return this.areaRadius;
    }

    public void setAreaRadius(float f) {
        this.areaRadius = f;
    }

    public Color getCenterColor() {
        return this.centerColor;
    }

    public void setCenterColor(Color color) {
        this.centerColor = color;
    }

    public Color getOutColor() {
        return this.outColor;
    }

    public void setOutColor(Color color) {
        this.outColor = color;
    }

    public Color getSatellitesColor() {
        return this.satellitesColor;
    }

    public void setSatellitesColor(Color color) {
        this.satellitesColor = color;
    }

    public Color getSelectedColor() {
        return this.selectedColor;
    }

    public void setSelectedColor(Color color) {
        this.selectedColor = color;
    }

    public boolean isShowWhenOffscreen() {
        return this.showWhenOffscreen;
    }

    public void setShowWhenOffscreen(boolean z) {
        this.showWhenOffscreen = z;
    }

    public float getMaxVisibleDistance() {
        return this.maxVisibleDistance;
    }

    public void setMaxVisibleDistance(float f) {
        this.maxVisibleDistance = f;
    }

    public Material getMarkSelected() {
        return this.markSelected;
    }

    public void setMarkSelected(Material material) {
        this.markSelected = material;
    }

    public Material getMarkOut() {
        return this.markOut;
    }

    public void setMarkOut(Material material) {
        this.markOut = material;
    }

    public Material getLock() {
        return this.lock;
    }

    public void setLock(Material material) {
        this.lock = material;
        if (material != null) {
            this.lockImgWidth = (int) Math.abs(material.getDiffuseTexture().getTransformedWidth() * material.getXScaling());
            this.lockImgHeight = (int) Math.abs(material.getDiffuseTexture().getTransformedHeight() * material.getYScaling());
        }
    }

    public boolean isOffScreen() {
        return this.isOffScreen;
    }

    public void setOffScreen(boolean z) {
        this.isOffScreen = z;
    }

    public boolean isShowSatellites() {
        return this.showSatellites;
    }

    public void setShowSatellites(boolean z) {
        this.showSatellites = z;
    }

    public void setTarget(SceneObject sceneObject, SceneView sceneView) {
        setSceneView(sceneView);
        setReferenceObject(sceneObject);
    }

    public SceneObject getReferenceObject() {
        return this.referenceObject;
    }

    public void setReferenceObject(SceneObject sceneObject) {
        this.referenceObject = sceneObject;
    }

    public Vec3f getScreenPosition() {
        return this.screenPosition;
    }

    public void setScreenPosition(Vec3f vec3f) {
        this.screenPosition.set(vec3f);
    }

    public float getMarkSize() {
        return this.screenSize;
    }

    public Vector2fWrap getCenterBillboardSize() {
        return null;
    }

    public void setCenterBillboardSize(Vector2fWrap aka) {
    }

    public void setOffset(float f, float f2) {
        this.offSet.set(f, f2);
    }

    public Vector2fWrap getOffset() {
        return this.offSet;
    }
}
