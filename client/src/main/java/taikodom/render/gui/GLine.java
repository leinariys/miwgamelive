package taikodom.render.gui;

import game.geometry.Vector2fWrap;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.GuiContext;
import taikodom.render.StepContext;
import taikodom.render.data.ExpandableIndexData;
import taikodom.render.data.ExpandableVertexData;
import taikodom.render.data.VertexLayout;
import taikodom.render.enums.BlendType;
import taikodom.render.enums.GeometryType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Mesh;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.ChannelInfo;

/* compiled from: a */
public class GLine extends GuiRenderObject {
    private static final Vector2fWrap firstPoint = new Vector2fWrap();

    /* renamed from: p1 */
    private static final Vector2fWrap f10350p1 = new Vector2fWrap();

    /* renamed from: p2 */
    private static final Vector2fWrap f10351p2 = new Vector2fWrap();
    float xCenter = 0.0f;
    float yCenter = 0.0f;
    private ExpandableIndexData indexes = new ExpandableIndexData(100);
    private int lastIndex = 0;
    private short lineStipple;
    private float lineWidth = 1.0f;
    private Mesh mesh = new Mesh();
    private ExpandableVertexData vertexes = new ExpandableVertexData(new VertexLayout(true, false, false, false, 1), 100);

    public GLine() {
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setGeometryType(GeometryType.LINES);
    }

    public GLine(GLine gLine) {
        this.mesh.setIndexes(this.indexes);
        this.mesh.setVertexData(this.vertexes);
        this.mesh.setGeometryType(GeometryType.LINES);
    }

    public Shader getDefaultLineShader() {
        Shader shader = new Shader();
        shader.setName("Default Line shader");
        ShaderPass shaderPass = new ShaderPass();
        shaderPass.setName("Default Line pass");
        ChannelInfo channelInfo = new ChannelInfo();
        channelInfo.setTexChannel(8);
        shaderPass.setChannelTextureSet(0, channelInfo);
        shaderPass.setBlendEnabled(true);
        shaderPass.setSrcBlend(BlendType.SRC_ALPHA);
        shaderPass.setDstBlend(BlendType.ONE_MINUS_SRC_ALPHA);
        shaderPass.setDepthMask(false);
        shaderPass.setColorArrayEnabled(false);
        shaderPass.getRenderStates().setCullFaceEnabled(true);
        shader.addPass(shaderPass);
        return shader;
    }

    /* access modifiers changed from: package-private */
    public short getLineStipple() {
        return this.lineStipple;
    }

    /* access modifiers changed from: package-private */
    public void setLineStipple(short s) {
        this.lineStipple = s;
    }

    /* access modifiers changed from: package-private */
    public GeometryType getGeometryType() {
        return this.mesh.getGeometryType();
    }

    public void setGeometryType(GeometryType geometryType) {
        this.mesh.setGeometryType(geometryType);
    }

    /* access modifiers changed from: package-private */
    public void createCircle(Vector2fWrap aka, float f, float f2, float f3, float f4) {
        if (f4 >= 0.0f && f3 >= f2) {
            f10350p1.x = (float) ((Math.cos((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) aka.x));
            f10350p1.y = (float) ((Math.sin((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) aka.y));
            firstPoint.set(f10350p1);
            while (f2 < f3) {
                addPoint(f10350p1);
                f10351p2.x = (float) ((Math.cos((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) aka.x));
                f10351p2.y = (float) ((Math.sin((double) (f2 * 0.017453292f)) * ((double) f)) + ((double) aka.y));
                addPoint(f10351p2);
                f10350p1.set(f10351p2);
                f2 += 3.0f * f4;
            }
            addPoint(f10351p2);
            addPoint(firstPoint);
        }
    }

    /* access modifiers changed from: package-private */
    public void createSquare(Vector2fWrap aka, float f) {
        addPoint((-f) + aka.x, (-f) + aka.y);
        addPoint(aka.x + f, (-f) + aka.y);
        addPoint(aka.x + f, (-f) + aka.y);
        addPoint(aka.x + f, aka.y + f);
        addPoint(aka.x + f, aka.y + f);
        addPoint((-f) + aka.x, aka.y + f);
        addPoint((-f) + aka.x, aka.y + f);
        addPoint((-f) + aka.x, (-f) + aka.y);
    }

    /* access modifiers changed from: package-private */
    public void createRectangle(Vector2fWrap aka, float f, float f2) {
        addPoint((-f) + aka.x, (-f2) + aka.y);
        addPoint(aka.x + f, (-f2) + aka.y);
        addPoint(aka.x + f, (-f2) + aka.y);
        addPoint(aka.x + f, aka.y + f2);
        addPoint(aka.x + f, aka.y + f2);
        addPoint((-f) + aka.x, aka.y + f2);
        addPoint((-f) + aka.x, aka.y + f2);
        addPoint((-f) + aka.x, (-f2) + aka.y);
    }

    /* access modifiers changed from: package-private */
    public void createElipse(Vector2fWrap aka, float f, float f2) {
        float f3 = 0.0f;
        f10350p1.x = (float) ((Math.cos((double) (0.0f * 0.017453292f)) * ((double) f)) + ((double) aka.x));
        f10350p1.y = (float) ((Math.sin((double) (0.0f * 0.017453292f)) * ((double) f2)) + ((double) aka.y));
        firstPoint.set(f10350p1);
        while (f3 < 360.0f) {
            addPoint(f10350p1);
            f10351p2.x = (float) ((Math.cos((double) (f3 * 0.017453292f)) * ((double) f)) + ((double) aka.x));
            f10351p2.y = (float) ((Math.sin((double) (f3 * 0.017453292f)) * ((double) f2)) + ((double) aka.y));
            addPoint(f10351p2);
            f10350p1.set(f10351p2);
            f3 += this.lineWidth * 3.0f;
        }
        addPoint(f10351p2);
        addPoint(firstPoint);
    }

    /* access modifiers changed from: package-private */
    public void forcedDraw(GuiContext guiContext) {
        render(guiContext);
    }

    /* access modifiers changed from: package-private */
    public void setDrawLine() {
    }

    /* access modifiers changed from: package-private */
    public void setDrawBigSegments() {
    }

    /* access modifiers changed from: package-private */
    public void setDrawMediumSegments() {
    }

    /* access modifiers changed from: package-private */
    public void setDrawSmallSegments() {
    }

    public int step(StepContext stepContext) {
        return super.step(stepContext);
    }

    public void render(GuiContext guiContext) {
        if (this.render) {
            if (this.material.getShader() == null) {
                this.material.setShader(getDefaultLineShader());
            }
            this.transform.setTranslation((double) (this.position.x + (guiContext.getViewportWidth() / 2.0f)), (double) (this.position.y + (guiContext.getViewportHeight() / 2.0f)), ScriptRuntime.NaN);
            guiContext.addPrimitive(this.material.getShader(), this.material, this.mesh, this.transform, this.primitiveColor);
        }
    }

    public RenderAsset cloneAsset() {
        return new GLine(this);
    }

    public void addPoint(float f, float f2) {
        this.vertexes.setPosition(this.lastIndex, f, f2, 0.0f);
        this.indexes.setIndex(this.lastIndex, this.lastIndex);
        this.lastIndex++;
        this.indexes.setSize(this.lastIndex);
    }

    public void addPoint(Vector2fWrap aka) {
        addPoint(aka.x, aka.y);
    }

    public void createCircle(Vector2fWrap aka, float f) {
        createCircle(aka, f, 0.0f, 360.0f, 1.0f);
    }

    public void validate(SceneLoader sceneLoader) {
        this.material.setShader(sceneLoader.getDefaultSpaceDustShader());
        super.validate(sceneLoader);
    }

    public void resetLine() {
        this.lastIndex = 0;
        this.indexes.setSize(0);
    }
}
