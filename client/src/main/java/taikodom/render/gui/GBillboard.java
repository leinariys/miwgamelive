package taikodom.render.gui;

import game.geometry.Vector2fWrap;
import org.mozilla1.javascript.ScriptRuntime;
import taikodom.render.GuiContext;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.SceneLoader;
import taikodom.render.primitives.Billboard;
import taikodom.render.shaders.Shader;

import javax.vecmath.Vector2f;

/* compiled from: a */
public class GBillboard extends GuiRenderObject {
    private static Billboard billboard = new Billboard();
    private Vector2fWrap size;

    public GBillboard() {
        this.size = new Vector2fWrap();
    }

    public GBillboard(GBillboard gBillboard) {
        this.size = new Vector2fWrap((Vector2f) gBillboard.size);
    }

    public void render(GuiContext guiContext) {
        if (this.render) {
            this.transform.setIdentity();
            this.transform.setTranslation((double) this.position.x, (double) this.position.y, ScriptRuntime.NaN);
            this.transform.mo17324B(this.size.x);
            this.transform.mo17325C(this.size.y);
            guiContext.addPrimitive(this.material.getShader(), this.material, billboard, this.transform, this.primitiveColor);
        }
    }

    public RenderAsset cloneAsset() {
        return new GBillboard(this);
    }

    public void setSize(Vector2fWrap aka) {
        this.size = aka;
    }

    public void validate(SceneLoader sceneLoader) {
        this.material.setShader(sceneLoader.getDefaultBillboardShader());
        super.validate(sceneLoader);
    }

    public void setShader(Shader shader) {
        this.material.setShader(shader);
    }
}
