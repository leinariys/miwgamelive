package taikodom.render;

/* compiled from: a */
public interface Video {
    boolean isEnded();

    void play();

    void render(DrawContext drawContext);

    void stop();

    void update(DrawContext drawContext);
}
