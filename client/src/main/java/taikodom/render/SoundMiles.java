package taikodom.render;

import logic.render.miles.AdapterMilesBridgeJNI;
import logic.render.miles.aNZ;

/* compiled from: a */
public class SoundMiles {
    private static SoundMiles hInstance;
    boolean initialized;
    MilesSoundListener m_listener;
    int maxSourceNumber;
    private long DIG_DS_FRAGMENT_CNT = 35;
    private long DIG_DS_MIX_FRAGMENT_CNT = 42;
    private long DIG_MIXER_CHANNELS = 1;
    private aNZ driver;

    public static SoundMiles getInstance() {
        if (hInstance == null) {
            hInstance = new SoundMiles();
        }
        return hInstance;
    }

    public boolean init(String str) {
        if (str != null) {
            AdapterMilesBridgeJNI.AIL_set_redist_directory(str);
        }
        AdapterMilesBridgeJNI.AIL_startup();
        this.driver = AdapterMilesBridgeJNI.m21718a((long) 44100, 16, 16, 4);
        if (this.driver == null) {
            System.out.println("SoundMiles ERROR" + AdapterMilesBridgeJNI.AIL_last_error());
            AdapterMilesBridgeJNI.AIL_shutdown();
            return false;
        }
        this.initialized = true;
        this.maxSourceNumber = AdapterMilesBridgeJNI.AIL_get_preference(this.DIG_MIXER_CHANNELS);
        return true;
    }

    public aNZ getDig() throws Exception {
        if (this.driver != null) {
            return this.driver;
        }
        if (!this.initialized) {
            throw new Exception("Trying to get miles driver before call init.");
        }
        throw new Exception("Trying to get miles driver error.");
    }

    public void prepareForStarving(int i) {
        int i2 = 8;
        int i3 = i / 8;
        if (i3 >= 8) {
            i2 = i3;
        }
        if (i2 > AdapterMilesBridgeJNI.AIL_get_preference(this.DIG_DS_FRAGMENT_CNT)) {
            i2 = AdapterMilesBridgeJNI.AIL_get_preference(this.DIG_DS_FRAGMENT_CNT) - 1;
        }
        AdapterMilesBridgeJNI.AIL_set_preference(this.DIG_DS_MIX_FRAGMENT_CNT, i2);
        AdapterMilesBridgeJNI.AIL_serve();
    }

    public void restoreFromStarving() {
        AdapterMilesBridgeJNI.AIL_set_preference(this.DIG_DS_MIX_FRAGMENT_CNT, 8);
    }

    public void shutdown() {
        if (this.driver != null) {
            this.driver = null;
            AdapterMilesBridgeJNI.m21851c(this.driver);
            AdapterMilesBridgeJNI.AIL_shutdown();
        }
    }

    public void dispose() {
        shutdown();
    }

    public void setMasterVolume(float f) {
        if (this.driver != null) {
            AdapterMilesBridgeJNI.m21762a(this.driver, f);
        }
    }

    /* access modifiers changed from: package-private */
    public void setSoundSpeed(float f) {
    }

    /* access modifiers changed from: package-private */
    public void setDopplerFactor(float f) {
        if (this.driver != null) {
            AdapterMilesBridgeJNI.m21852c(this.driver, f);
        }
    }

    /* access modifiers changed from: package-private */
    public MilesSoundListener createSoundListener() {
        if (this.m_listener == null) {
            this.m_listener = new MilesSoundListener();
        }
        return this.m_listener;
    }

    /* access modifiers changed from: package-private */
    public int getNumDevices() {
        return 1;
    }

    /* access modifiers changed from: package-private */
    public String getDeviceName(int i) {
        return "Miles Sound System";
    }

    /* access modifiers changed from: package-private */
    public boolean isInitialized() {
        return this.initialized;
    }

    public int getMaxSourceNumber() {
        return this.maxSourceNumber;
    }
}
