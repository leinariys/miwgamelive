package taikodom.render;

import com.sun.opengl.impl.Debug;
import com.sun.opengl.impl.GLContextImpl;
import com.sun.opengl.impl.GLDrawableHelper;

import javax.media.opengl.*;
import javax.swing.*;
import java.awt.*;
import java.beans.Beans;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;

/* compiled from: a */
public class MyGLCanvas extends JApplet implements GLAutoDrawable {
    private static final boolean DEBUG = Debug.debug("GLCanvas");
    /* access modifiers changed from: private */
    public static Method disableBackgroundEraseMethod;
    private static boolean disableBackgroundEraseInitialized;
    /* access modifiers changed from: private */
    public GLContextImpl context;
    /* access modifiers changed from: private */
    public C5124b displayAction;
    /* access modifiers changed from: private */
    public GLDrawable drawable;
    /* access modifiers changed from: private */
    public GLDrawableHelper drawableHelper;
    /* access modifiers changed from: private */
    public C5127e initAction;
    /* access modifiers changed from: private */
    public boolean sendReshape;
    /* access modifiers changed from: private */
    public C5129g swapBuffersAction;
    private boolean autoSwapBufferMode;
    private GraphicsConfiguration chosen;
    private C5128f destroyAction;
    private C5125c displayOnEventDispatchThreadAction;
    private GLCapabilitiesChooser glCapChooser;
    private GLCapabilities glCaps;
    private C5126d swapBuffersOnEventDispatchThreadAction;

    public MyGLCanvas() {
        this((GLCapabilities) null);
    }

    public MyGLCanvas(GLCapabilities gLCapabilities) {
        this(gLCapabilities, (GLCapabilitiesChooser) null, (GLContext) null, (GraphicsDevice) null);
    }

    public MyGLCanvas(GLCapabilities gLCapabilities, GLCapabilitiesChooser gLCapabilitiesChooser, GLContext gLContext, GraphicsDevice graphicsDevice) {
        this.drawableHelper = new GLDrawableHelper();
        this.autoSwapBufferMode = true;
        this.sendReshape = false;
        this.initAction = new C5127e();
        this.displayAction = new C5124b();
        this.swapBuffersAction = new C5129g();
        this.displayOnEventDispatchThreadAction = new C5125c();
        this.swapBuffersOnEventDispatchThreadAction = new C5126d();
        this.destroyAction = new C5128f();
        this.chosen = chooseGraphicsConfiguration(gLCapabilities, gLCapabilitiesChooser, graphicsDevice);
        if (this.chosen != null) {
            this.glCapChooser = gLCapabilitiesChooser;
            this.glCaps = gLCapabilities;
        }
        if (!Beans.isDesignTime()) {
            this.drawable = GLDrawableFactory.getFactory().getGLDrawable(this, gLCapabilities, gLCapabilitiesChooser);
            this.context = this.drawable.createContext(gLContext);
            this.context.setSynchronized(true);
        }
    }

    private static GraphicsConfiguration chooseGraphicsConfiguration(GLCapabilities gLCapabilities, GLCapabilitiesChooser gLCapabilitiesChooser, GraphicsDevice graphicsDevice) {
        if (Beans.isDesignTime()) {
            return null;
        }
        AWTGraphicsConfiguration chooseGraphicsConfiguration = GLDrawableFactory.getFactory().chooseGraphicsConfiguration(gLCapabilities, gLCapabilitiesChooser, new AWTGraphicsDevice(graphicsDevice));
        if (chooseGraphicsConfiguration == null) {
            return null;
        }
        return chooseGraphicsConfiguration.getGraphicsConfiguration();
    }

    public GraphicsConfiguration getGraphicsConfiguration() {
        GraphicsConfiguration chooseGraphicsConfiguration;
        GraphicsConfiguration graphicsConfiguration = MyGLCanvas.super.getGraphicsConfiguration();
        if (graphicsConfiguration != null && this.chosen != null && !this.chosen.equals(graphicsConfiguration)) {
            if (!this.chosen.getDevice().getIDstring().equals(graphicsConfiguration.getDevice().getIDstring()) && (chooseGraphicsConfiguration = chooseGraphicsConfiguration(this.glCaps, this.glCapChooser, graphicsConfiguration.getDevice())) != null) {
                this.chosen = chooseGraphicsConfiguration;
            }
            return this.chosen;
        } else if (graphicsConfiguration == null) {
            return this.chosen;
        } else {
            return graphicsConfiguration;
        }
    }

    public GLContext createContext(GLContext gLContext) {
        return this.drawable.createContext(gLContext);
    }

    public void setRealized(boolean z) {
    }

    public void display() {
        maybeDoSingleThreadedWorkaround(this.displayOnEventDispatchThreadAction, this.displayAction);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0023, code lost:
        r0 = getClass().getName();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void paint(java.awt.Graphics r11) {
        /*
            r10 = this;
            r2 = 0
            r8 = 4611686018427387904(0x4000000000000000, double:2.0)
            boolean r0 = java.beans.Beans.isDesignTime()
            if (r0 == 0) goto L_0x005e
            java.awt.Color r0 = java.awt.Color.BLACK
            r11.setColor(r0)
            int r0 = r10.getWidth()
            int r1 = r10.getHeight()
            r11.fillRect(r2, r2, r0, r1)
            java.awt.FontMetrics r1 = r11.getFontMetrics()
            java.lang.String r0 = r10.getName()
            if (r0 != 0) goto L_0x0039
            java.lang.Class r0 = r10.getClass()
            java.lang.String r0 = r0.getName()
            r2 = 46
            int r2 = r0.lastIndexOf(r2)
            if (r2 < 0) goto L_0x0039
            int r2 = r2 + 1
            java.lang.String r0 = r0.substring(r2)
        L_0x0039:
            java.awt.geom.Rectangle2D r1 = r1.getStringBounds(r0, r11)
            java.awt.Color r2 = java.awt.Color.WHITE
            r11.setColor(r2)
            int r2 = r10.getWidth()
            double r2 = (double) r2
            double r4 = r1.getWidth()
            double r2 = r2 - r4
            double r2 = r2 / r8
            int r2 = (int) r2
            int r3 = r10.getHeight()
            double r4 = (double) r3
            double r6 = r1.getHeight()
            double r4 = r4 + r6
            double r4 = r4 / r8
            int r1 = (int) r4
            r11.drawString(r0, r2, r1)
        L_0x005d:
            return
        L_0x005e:
            r10.display()
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.render.MyGLCanvas.paint(java.awt.Graphics):void");
    }

    public void paintComponents(Graphics graphics) {
    }

    public void paintAll(Graphics graphics) {
    }

    public void addNotify() {
        MyGLCanvas.super.addNotify();
        if (!Beans.isDesignTime()) {
            disableBackgroundErase();
            this.drawable.setRealized(true);
        }
        if (DEBUG) {
            System.err.println("GLCanvas.addNotify()");
        }
    }

    public void removeNotify() {
        String str;
        if (Beans.isDesignTime()) {
            MyGLCanvas.super.removeNotify();
            return;
        }
        try {
            if (!Threading.isSingleThreaded() || Threading.isOpenGLThread()) {
                this.destroyAction.run();
            } else if (Thread.holdsLock(getTreeLock())) {
                this.destroyAction.run();
            } else {
                Threading.invokeOnOpenGLThread(this.destroyAction);
            }
        } finally {
            this.drawable.setRealized(false);
            MyGLCanvas.super.removeNotify();
            if (DEBUG) {
                str = "GLCanvas.removeNotify()";
                System.err.println(str);
            }
        }
    }

    public void reshape(int i, int i2, int i3, int i4) {
        MyGLCanvas.super.reshape(i, i2, i3, i4);
        this.sendReshape = true;
    }

    public void update(Graphics graphics) {
        paint(graphics);
    }

    public void addGLEventListener(GLEventListener gLEventListener) {
        this.drawableHelper.addGLEventListener(gLEventListener);
    }

    public void removeGLEventListener(GLEventListener gLEventListener) {
        this.drawableHelper.removeGLEventListener(gLEventListener);
    }

    public GLContext getContext() {
        return this.context;
    }

    public GL getGL() {
        if (Beans.isDesignTime()) {
            return null;
        }
        return getContext().getGL();
    }

    public void setGL(GL gl) {
        if (!Beans.isDesignTime()) {
            getContext().setGL(gl);
        }
    }

    public boolean getAutoSwapBufferMode() {
        return this.drawableHelper.getAutoSwapBufferMode();
    }

    public void setAutoSwapBufferMode(boolean z) {
        this.drawableHelper.setAutoSwapBufferMode(z);
    }

    public void swapBuffers() {
        maybeDoSingleThreadedWorkaround(this.swapBuffersOnEventDispatchThreadAction, this.swapBuffersAction);
    }

    public GLCapabilities getChosenGLCapabilities() {
        if (this.drawable == null) {
            return null;
        }
        return this.drawable.getChosenGLCapabilities();
    }

    private void maybeDoSingleThreadedWorkaround(Runnable runnable, Runnable runnable2) {
        if (!Threading.isSingleThreaded() || Threading.isOpenGLThread()) {
            this.drawableHelper.invokeGL(this.drawable, this.context, runnable2, this.initAction);
        } else {
            Threading.invokeOnOpenGLThread(runnable);
        }
    }

    private void disableBackgroundErase() {
        if (!disableBackgroundEraseInitialized) {
            try {
                AccessController.doPrivileged(new C5123a());
            } catch (Exception e) {
            }
            disableBackgroundEraseInitialized = true;
        }
        if (disableBackgroundEraseMethod != null) {
            try {
                disableBackgroundEraseMethod.invoke(getToolkit(), new Object[]{this});
            } catch (Exception e2) {
            }
        }
    }

    public GLDrawable getDrawable() {
        return this.drawable;
    }

    /* renamed from: taikodom.render.MyGLCanvas$e */
    /* compiled from: a */
    class C5127e implements Runnable {
        C5127e() {
        }

        public void run() {
            MyGLCanvas.this.drawableHelper.init(MyGLCanvas.this);
        }
    }

    /* renamed from: taikodom.render.MyGLCanvas$b */
    /* compiled from: a */
    class C5124b implements Runnable {
        C5124b() {
        }

        public void run() {
            if (MyGLCanvas.this.sendReshape) {
                int width = MyGLCanvas.this.getWidth();
                int height = MyGLCanvas.this.getHeight();
                MyGLCanvas.this.getGL().glViewport(0, 0, width, height);
                MyGLCanvas.this.drawableHelper.reshape(MyGLCanvas.this, 0, 0, width, height);
                MyGLCanvas.this.sendReshape = false;
            }
            MyGLCanvas.this.drawableHelper.display(MyGLCanvas.this);
        }
    }

    /* renamed from: taikodom.render.MyGLCanvas$g */
    /* compiled from: a */
    class C5129g implements Runnable {
        C5129g() {
        }

        public void run() {
            MyGLCanvas.this.drawable.swapBuffers();
        }
    }

    /* renamed from: taikodom.render.MyGLCanvas$c */
    /* compiled from: a */
    class C5125c implements Runnable {
        C5125c() {
        }

        public void run() {
            MyGLCanvas.this.drawableHelper.invokeGL(MyGLCanvas.this.drawable, MyGLCanvas.this.context, MyGLCanvas.this.displayAction, MyGLCanvas.this.initAction);
        }
    }

    /* renamed from: taikodom.render.MyGLCanvas$d */
    /* compiled from: a */
    class C5126d implements Runnable {
        C5126d() {
        }

        public void run() {
            MyGLCanvas.this.drawableHelper.invokeGL(MyGLCanvas.this.drawable, MyGLCanvas.this.context, MyGLCanvas.this.swapBuffersAction, MyGLCanvas.this.initAction);
        }
    }

    /* renamed from: taikodom.render.MyGLCanvas$f */
    /* compiled from: a */
    class C5128f implements Runnable {
        C5128f() {
        }

        public void run() {
            if (GLContext.getCurrent() == MyGLCanvas.this.context) {
                MyGLCanvas.this.context.release();
            }
            MyGLCanvas.this.context.destroy();
        }
    }

    /* renamed from: taikodom.render.MyGLCanvas$a */
    class C5123a implements PrivilegedAction {
        C5123a() {
        }

        public Object run() {
            try {
                MyGLCanvas.disableBackgroundEraseMethod = MyGLCanvas.this.getToolkit().getClass().getDeclaredMethod("disableBackgroundErase", new Class[]{Canvas.class});
                MyGLCanvas.disableBackgroundEraseMethod.setAccessible(true);
                return null;
            } catch (Exception e) {
                return null;
            }
        }
    }
}
