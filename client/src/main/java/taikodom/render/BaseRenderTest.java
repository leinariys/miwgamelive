package taikodom.render;

import com.hoplon.geometry.Color;
import com.sun.opengl.util.Animator;
import game.geometry.Vec3d;
import logic.res.FileControl;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.C0394FT;
import taikodom.render.camera.ViewCameraControl;
import taikodom.render.gl.GLWrapper;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.scene.RSceneAreaInfo;
import taikodom.render.scene.Scene;

import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import java.awt.*;
import java.awt.event.*;

/* compiled from: a */
public abstract class BaseRenderTest extends Frame implements GLEventListener {
    public final GLCanvas canvas;
    public long before;
    public Vec3d cameraLookAt = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
    public Vec3d cameraPosition = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 1.0d);
    public int dragPanSpeed = 20;
    public FileSceneLoader loader;
    public float mouseWheelStep = 2.0f;
    /* renamed from: rv */
    public RenderView f10328rv;
    public Scene scene;
    public SceneView sceneView;
    public long start = System.currentTimeMillis();
    public StepContext stepContext = new StepContext();
    public C0394FT timer = new C0394FT();
    public int translationStep = (((int) this.mouseWheelStep) * 10);
    public ViewCameraControl vcc;
    private Point moving = null;

    public BaseRenderTest() {
        setLayout(new BorderLayout());
        setSize(800, 600);
        setLocation(40, 40);
        setVisible(true);
        GLCapabilities gLCapabilities = new GLCapabilities();
        gLCapabilities.setDoubleBuffered(true);
        gLCapabilities.setHardwareAccelerated(true);
        gLCapabilities.setSampleBuffers(true);
        gLCapabilities.setNumSamples(2);
        this.canvas = new GLCanvas(gLCapabilities);
        this.canvas.enableInputMethods(true);
        this.canvas.addGLEventListener(this);
        this.canvas.addKeyListener(new C5119b());
        this.canvas.addMouseMotionListener(new C5118a());
        this.canvas.addMouseListener(new C5122e());
        this.canvas.addMouseWheelListener(new C5121d());
        add(this.canvas, "Center");
        new Animator(this.canvas).start();
        addWindowListener(new C5120c());
        this.canvas.setVisible(true);
    }

    /* access modifiers changed from: protected */
    public abstract void loadAssets();

    /* access modifiers changed from: protected */
    public void onKeyPressed(char c) {
        if (c == 'w') {
            this.vcc.zoomIn(this.mouseWheelStep / 2.0f);
        } else if (c == 's') {
            this.vcc.zoomOut(this.mouseWheelStep / 2.0f);
        } else if (c == 'a') {
            this.vcc.pan(this.translationStep, 0);
        } else if (c == 'd') {
            this.vcc.pan(-this.translationStep, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void onKeyReleased(char c) {
    }

    /* access modifiers changed from: protected */
    public void onMouseMove(MouseEvent mouseEvent) {
    }

    /* access modifiers changed from: protected */
    public void onMouseDragged(MouseEvent mouseEvent) {
        if (this.moving != null) {
            int x = mouseEvent.getX() - this.moving.x;
            int y = mouseEvent.getY() - this.moving.y;
            if (mouseEvent.getModifiers() == 16) {
                this.vcc.rotate(x, y);
            } else if (mouseEvent.getModifiers() == 8) {
                this.vcc.pan(x * this.dragPanSpeed, y * this.dragPanSpeed);
            }
            this.moving.setLocation(mouseEvent.getX(), mouseEvent.getY());
        }
    }

    /* access modifiers changed from: protected */
    public void onMouseReleased(MouseEvent mouseEvent) {
        this.moving = null;
    }

    /* access modifiers changed from: protected */
    public void onMousePressed(MouseEvent mouseEvent) {
        this.moving = new Point(mouseEvent.getX(), mouseEvent.getY());
    }

    public void display(GLAutoDrawable gLAutoDrawable) {
        long nanoTime = System.nanoTime();
        float f = ((float) (nanoTime - this.before)) * 1.0E-9f;
        step(f);
        this.f10328rv.render(this.sceneView, (double) (((float) (System.currentTimeMillis() - this.start)) * 0.001f), f);
        this.before = nanoTime;
        this.timer.update();
    }

    public void step(float f) {
        this.stepContext.reset();
        this.stepContext.setCamera(this.sceneView.getCamera());
        this.stepContext.setDeltaTime(f);
        this.scene.step(this.stepContext);
    }

    public void displayChanged(GLAutoDrawable gLAutoDrawable, boolean z, boolean z2) {
    }

    public void init(GLAutoDrawable gLAutoDrawable) {
        this.f10328rv = new RenderView(gLAutoDrawable.getContext());
        this.f10328rv.setViewport(0, 0, gLAutoDrawable.getWidth(), gLAutoDrawable.getHeight());
        this.sceneView = new SceneView();
        this.f10328rv.setWireframeOnly(false);
        this.sceneView.setAllowImpostors(true);
        gLAutoDrawable.getGL();
        GLWrapper gLWrapper = new GLWrapper(this.canvas.getGL());
        this.canvas.setGL(gLWrapper);
        this.f10328rv.setGL(gLWrapper);
        this.f10328rv.getDrawContext().setRenderView(this.f10328rv);
        this.scene = this.sceneView.getScene();
        try {
            System.out.println("resourceDir: " + "./resource-test");
            this.loader = new FileSceneLoader((FilePath) new FileControl("./resource-test"));
            System.loadLibrary("mss32");
            System.loadLibrary("binkw32");
            System.loadLibrary("granny2");
            System.loadLibrary("utaikodom.render");
            this.vcc = new ViewCameraControl();
            this.vcc.setCamera(this.sceneView.getCamera());
            this.vcc.setPosition(this.cameraPosition);
            this.vcc.lookTo(this.cameraLookAt);
            RSceneAreaInfo rSceneAreaInfo = new RSceneAreaInfo();
            rSceneAreaInfo.setAmbientColor(new Color(0.2f, 0.2f, 0.2f, 1.0f));
            rSceneAreaInfo.setFogColor(new Color(0.2f, 0.2f, 0.2f, 1.0f));
            loadAssets();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void reshape(GLAutoDrawable gLAutoDrawable, int i, int i2, int i3, int i4) {
        if (this.vcc.getCamera() != null) {
            this.vcc.getCamera().setAspect(((float) i3) / ((float) i4));
        }
        this.sceneView.setViewport(i, i2, i3, i4);
    }

    public void setSize(int i, int i2) {
        Insets insets = getInsets();
        BaseRenderTest.super.setSize(insets.left + i + insets.right, insets.top + i2 + insets.bottom);
        if (this.canvas != null) {
            this.canvas.setLocation(insets.left, insets.top);
            this.canvas.setSize(i, i2);
        }
    }

    public void setSize(Dimension dimension) {
        setSize(dimension.width, dimension.height);
    }

    public void onMouseWheel(int i) {
        if (i < 0) {
            this.vcc.zoomIn(((float) (-i)) * this.mouseWheelStep);
        } else {
            this.vcc.zoomOut(((float) i) * this.mouseWheelStep);
        }
    }

    /* renamed from: taikodom.render.BaseRenderTest$b */
    /* compiled from: a */
    class C5119b implements KeyListener {
        C5119b() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            BaseRenderTest.this.onKeyPressed(keyEvent.getKeyChar());
        }

        public void keyReleased(KeyEvent keyEvent) {
            BaseRenderTest.this.onKeyReleased(keyEvent.getKeyChar());
        }

        public void keyTyped(KeyEvent keyEvent) {
        }
    }

    /* renamed from: taikodom.render.BaseRenderTest$a */
    class C5118a implements MouseMotionListener {
        C5118a() {
        }

        public void mouseDragged(MouseEvent mouseEvent) {
            BaseRenderTest.this.onMouseDragged(mouseEvent);
        }

        public void mouseMoved(MouseEvent mouseEvent) {
            BaseRenderTest.this.onMouseMove(mouseEvent);
        }
    }

    /* renamed from: taikodom.render.BaseRenderTest$e */
    /* compiled from: a */
    class C5122e implements MouseListener {
        C5122e() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
        }

        public void mouseEntered(MouseEvent mouseEvent) {
        }

        public void mouseExited(MouseEvent mouseEvent) {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            BaseRenderTest.this.onMousePressed(mouseEvent);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            BaseRenderTest.this.onMouseReleased(mouseEvent);
        }
    }

    /* renamed from: taikodom.render.BaseRenderTest$d */
    /* compiled from: a */
    class C5121d implements MouseWheelListener {
        C5121d() {
        }

        public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
            BaseRenderTest.this.onMouseWheel(mouseWheelEvent.getWheelRotation());
        }
    }

    /* renamed from: taikodom.render.BaseRenderTest$c */
    /* compiled from: a */
    class C5120c extends WindowAdapter {
        C5120c() {
        }

        public void windowClosing(WindowEvent windowEvent) {
            System.exit(0);
        }

        public void windowClosed(WindowEvent windowEvent) {
            System.exit(0);
        }
    }
}
