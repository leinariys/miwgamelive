package taikodom.render.gl;

import javax.media.opengl.GL;
import java.nio.*;

/* renamed from: taikodom.render.gl.GLWrapperBase */
/* compiled from: a */
public class GLWrapperBase implements GL {

    /* renamed from: gl */
    public GL f10342gl;

    public GLWrapperBase(GL gl) {
        this.f10342gl = gl;
    }

    public Object getPlatformGLExtensions() {
        return this.f10342gl.getPlatformGLExtensions();
    }

    public void glAccum(int i, float f) {
        this.f10342gl.glAccum(i, f);
    }

    public void glActiveStencilFaceEXT(int i) {
        this.f10342gl.glActiveStencilFaceEXT(i);
    }

    public void glActiveTexture(int i) {
        this.f10342gl.glActiveTexture(i);
    }

    public void glActiveVaryingNV(int i, byte[] bArr, int i2) {
        this.f10342gl.glActiveVaryingNV(i, bArr, i2);
    }

    public void glActiveVaryingNV(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glActiveVaryingNV(i, byteBuffer);
    }

    public ByteBuffer glAllocateMemoryNV(int i, float f, float f2, float f3) {
        return this.f10342gl.glAllocateMemoryNV(i, f, f2, f3);
    }

    public void glAlphaFragmentOp1ATI(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glAlphaFragmentOp1ATI(i, i2, i3, i4, i5, i6);
    }

    public void glAlphaFragmentOp2ATI(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        this.f10342gl.glAlphaFragmentOp2ATI(i, i2, i3, i4, i5, i6, i7, i8, i9);
    }

    public void glAlphaFragmentOp3ATI(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12) {
        this.f10342gl.glAlphaFragmentOp3ATI(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12);
    }

    public void glAlphaFunc(int i, float f) {
        this.f10342gl.glAlphaFunc(i, f);
    }

    public void glApplyTextureEXT(int i) {
        this.f10342gl.glApplyTextureEXT(i);
    }

    public boolean glAreProgramsResidentNV(int i, int[] iArr, int i2, byte[] bArr, int i3) {
        return this.f10342gl.glAreProgramsResidentNV(i, iArr, i2, bArr, i3);
    }

    public boolean glAreProgramsResidentNV(int i, IntBuffer intBuffer, ByteBuffer byteBuffer) {
        return this.f10342gl.glAreProgramsResidentNV(i, intBuffer, byteBuffer);
    }

    public boolean glAreTexturesResident(int i, int[] iArr, int i2, byte[] bArr, int i3) {
        return this.f10342gl.glAreTexturesResident(i, iArr, i2, bArr, i3);
    }

    public boolean glAreTexturesResident(int i, IntBuffer intBuffer, ByteBuffer byteBuffer) {
        return this.f10342gl.glAreTexturesResident(i, intBuffer, byteBuffer);
    }

    public void glArrayElement(int i) {
        this.f10342gl.glArrayElement(i);
    }

    public void glArrayObjectATI(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glArrayObjectATI(i, i2, i3, i4, i5, i6);
    }

    public void glAsyncMarkerSGIX(int i) {
        this.f10342gl.glAsyncMarkerSGIX(i);
    }

    public void glAttachObjectARB(int i, int i2) {
        this.f10342gl.glAttachObjectARB(i, i2);
    }

    public void glAttachShader(int i, int i2) {
        this.f10342gl.glAttachShader(i, i2);
    }

    public void glBegin(int i) {
        this.f10342gl.glBegin(i);
    }

    public void glBeginFragmentShaderATI() {
        this.f10342gl.glBeginFragmentShaderATI();
    }

    public void glBeginOcclusionQueryNV(int i) {
        this.f10342gl.glBeginOcclusionQueryNV(i);
    }

    public void glBeginQuery(int i, int i2) {
        this.f10342gl.glBeginQuery(i, i2);
    }

    public void glBeginQueryARB(int i, int i2) {
        this.f10342gl.glBeginQueryARB(i, i2);
    }

    public void glBeginTransformFeedbackNV(int i) {
        this.f10342gl.glBeginTransformFeedbackNV(i);
    }

    public void glBeginVertexShaderEXT() {
        this.f10342gl.glBeginVertexShaderEXT();
    }

    public void glBindAttribLocation(int i, int i2, String str) {
        this.f10342gl.glBindAttribLocation(i, i2, str);
    }

    public void glBindAttribLocationARB(int i, int i2, String str) {
        this.f10342gl.glBindAttribLocationARB(i, i2, str);
    }

    public void glBindBuffer(int i, int i2) {
        this.f10342gl.glBindBuffer(i, i2);
    }

    public void glBindBufferARB(int i, int i2) {
        this.f10342gl.glBindBufferARB(i, i2);
    }

    public void glBindBufferBaseNV(int i, int i2, int i3) {
        this.f10342gl.glBindBufferBaseNV(i, i2, i3);
    }

    public void glBindBufferOffsetNV(int i, int i2, int i3, int i4) {
        this.f10342gl.glBindBufferOffsetNV(i, i2, i3, i4);
    }

    public void glBindBufferRangeNV(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glBindBufferRangeNV(i, i2, i3, i4, i5);
    }

    public void glBindFragDataLocationEXT(int i, int i2, ByteBuffer byteBuffer) {
        this.f10342gl.glBindFragDataLocationEXT(i, i2, byteBuffer);
    }

    public void glBindFragDataLocationEXT(int i, int i2, byte[] bArr, int i3) {
        this.f10342gl.glBindFragDataLocationEXT(i, i2, bArr, i3);
    }

    public void glBindFragmentShaderATI(int i) {
        this.f10342gl.glBindFragmentShaderATI(i);
    }

    public void glBindFramebufferEXT(int i, int i2) {
        this.f10342gl.glBindFramebufferEXT(i, i2);
    }

    public int glBindLightParameterEXT(int i, int i2) {
        return this.f10342gl.glBindLightParameterEXT(i, i2);
    }

    public int glBindMaterialParameterEXT(int i, int i2) {
        return this.f10342gl.glBindMaterialParameterEXT(i, i2);
    }

    public int glBindParameterEXT(int i) {
        return this.f10342gl.glBindParameterEXT(i);
    }

    public void glBindProgramARB(int i, int i2) {
        this.f10342gl.glBindProgramARB(i, i2);
    }

    public void glBindProgramNV(int i, int i2) {
        this.f10342gl.glBindProgramNV(i, i2);
    }

    public void glBindRenderbufferEXT(int i, int i2) {
        this.f10342gl.glBindRenderbufferEXT(i, i2);
    }

    public int glBindTexGenParameterEXT(int i, int i2, int i3) {
        return this.f10342gl.glBindTexGenParameterEXT(i, i2, i3);
    }

    public void glBindTexture(int i, int i2) {
        this.f10342gl.glBindTexture(i, i2);
    }

    public int glBindTextureUnitParameterEXT(int i, int i2) {
        return this.f10342gl.glBindTextureUnitParameterEXT(i, i2);
    }

    public void glBindVertexArrayAPPLE(int i) {
        this.f10342gl.glBindVertexArrayAPPLE(i);
    }

    public void glBindVertexShaderEXT(int i) {
        this.f10342gl.glBindVertexShaderEXT(i);
    }

    public void glBitmap(int i, int i2, float f, float f2, float f3, float f4, long j) {
        this.f10342gl.glBitmap(i, i2, f, f2, f3, f4, j);
    }

    public void glBitmap(int i, int i2, float f, float f2, float f3, float f4, ByteBuffer byteBuffer) {
        this.f10342gl.glBitmap(i, i2, f, f2, f3, f4, byteBuffer);
    }

    public void glBitmap(int i, int i2, float f, float f2, float f3, float f4, byte[] bArr, int i3) {
        this.f10342gl.glBitmap(i, i2, f, f2, f3, f4, bArr, i3);
    }

    public void glBlendColor(float f, float f2, float f3, float f4) {
        this.f10342gl.glBlendColor(f, f2, f3, f4);
    }

    public void glBlendEquation(int i) {
        this.f10342gl.glBlendEquation(i);
    }

    public void glBlendEquationSeparate(int i, int i2) {
        this.f10342gl.glBlendEquationSeparate(i, i2);
    }

    public void glBlendEquationSeparateEXT(int i, int i2) {
        this.f10342gl.glBlendEquationSeparateEXT(i, i2);
    }

    public void glBlendFunc(int i, int i2) {
        this.f10342gl.glBlendFunc(i, i2);
    }

    public void glBlendFuncSeparate(int i, int i2, int i3, int i4) {
        this.f10342gl.glBlendFuncSeparate(i, i2, i3, i4);
    }

    public void glBlendFuncSeparateEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glBlendFuncSeparateEXT(i, i2, i3, i4);
    }

    public void glBlendFuncSeparateINGR(int i, int i2, int i3, int i4) {
        this.f10342gl.glBlendFuncSeparateINGR(i, i2, i3, i4);
    }

    public void glBlitFramebufferEXT(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f10342gl.glBlitFramebufferEXT(i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
    }

    public void glBufferData(int i, int i2, Buffer buffer, int i3) {
        this.f10342gl.glBufferData(i, i2, buffer, i3);
    }

    public void glBufferDataARB(int i, int i2, Buffer buffer, int i3) {
        this.f10342gl.glBufferDataARB(i, i2, buffer, i3);
    }

    public void glBufferParameteriAPPLE(int i, int i2, int i3) {
        this.f10342gl.glBufferParameteriAPPLE(i, i2, i3);
    }

    public int glBufferRegionEnabled() {
        return this.f10342gl.glBufferRegionEnabled();
    }

    public void glBufferSubData(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glBufferSubData(i, i2, i3, buffer);
    }

    public void glBufferSubDataARB(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glBufferSubDataARB(i, i2, i3, buffer);
    }

    public void glCallList(int i) {
        this.f10342gl.glCallList(i);
    }

    public void glCallLists(int i, int i2, Buffer buffer) {
        this.f10342gl.glCallLists(i, i2, buffer);
    }

    public int glCheckFramebufferStatusEXT(int i) {
        return this.f10342gl.glCheckFramebufferStatusEXT(i);
    }

    public void glClampColorARB(int i, int i2) {
        this.f10342gl.glClampColorARB(i, i2);
    }

    public void glClear(int i) {
        this.f10342gl.glClear(i);
    }

    public void glClearAccum(float f, float f2, float f3, float f4) {
        this.f10342gl.glClearAccum(f, f2, f3, f4);
    }

    public void glClearColor(float f, float f2, float f3, float f4) {
        this.f10342gl.glClearColor(f, f2, f3, f4);
    }

    public void glClearColorIiEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glClearColorIiEXT(i, i2, i3, i4);
    }

    public void glClearColorIuiEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glClearColorIuiEXT(i, i2, i3, i4);
    }

    public void glClearDepth(double d) {
        this.f10342gl.glClearDepth(d);
    }

    public void glClearDepthdNV(double d) {
        this.f10342gl.glClearDepthdNV(d);
    }

    public void glClearIndex(float f) {
        this.f10342gl.glClearIndex(f);
    }

    public void glClearStencil(int i) {
        this.f10342gl.glClearStencil(i);
    }

    public void glClientActiveTexture(int i) {
        this.f10342gl.glClientActiveTexture(i);
    }

    public void glClientActiveVertexStreamATI(int i) {
        this.f10342gl.glClientActiveVertexStreamATI(i);
    }

    public void glClipPlane(int i, double[] dArr, int i2) {
        this.f10342gl.glClipPlane(i, dArr, i2);
    }

    public void glClipPlane(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glClipPlane(i, doubleBuffer);
    }

    public void glColor3b(byte b, byte b2, byte b3) {
        this.f10342gl.glColor3b(b, b2, b3);
    }

    public void glColor3bv(ByteBuffer byteBuffer) {
        this.f10342gl.glColor3bv(byteBuffer);
    }

    public void glColor3bv(byte[] bArr, int i) {
        this.f10342gl.glColor3bv(bArr, i);
    }

    public void glColor3d(double d, double d2, double d3) {
        this.f10342gl.glColor3d(d, d2, d3);
    }

    public void glColor3dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glColor3dv(doubleBuffer);
    }

    public void glColor3dv(double[] dArr, int i) {
        this.f10342gl.glColor3dv(dArr, i);
    }

    public void glColor3f(float f, float f2, float f3) {
        this.f10342gl.glColor3f(f, f2, f3);
    }

    public void glColor3fVertex3fSUN(float f, float f2, float f3, float f4, float f5, float f6) {
        this.f10342gl.glColor3fVertex3fSUN(f, f2, f3, f4, f5, f6);
    }

    public void glColor3fVertex3fvSUN(float[] fArr, int i, float[] fArr2, int i2) {
        this.f10342gl.glColor3fVertex3fvSUN(fArr, i, fArr2, i2);
    }

    public void glColor3fVertex3fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glColor3fVertex3fvSUN(floatBuffer, floatBuffer2);
    }

    public void glColor3fv(float[] fArr, int i) {
        this.f10342gl.glColor3fv(fArr, i);
    }

    public void glColor3fv(FloatBuffer floatBuffer) {
        this.f10342gl.glColor3fv(floatBuffer);
    }

    public void glColor3hNV(short s, short s2, short s3) {
        this.f10342gl.glColor3hNV(s, s2, s3);
    }

    public void glColor3hvNV(short[] sArr, int i) {
        this.f10342gl.glColor3hvNV(sArr, i);
    }

    public void glColor3hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glColor3hvNV(shortBuffer);
    }

    public void glColor3i(int i, int i2, int i3) {
        this.f10342gl.glColor3i(i, i2, i3);
    }

    public void glColor3iv(int[] iArr, int i) {
        this.f10342gl.glColor3iv(iArr, i);
    }

    public void glColor3iv(IntBuffer intBuffer) {
        this.f10342gl.glColor3iv(intBuffer);
    }

    public void glColor3s(short s, short s2, short s3) {
        this.f10342gl.glColor3s(s, s2, s3);
    }

    public void glColor3sv(ShortBuffer shortBuffer) {
        this.f10342gl.glColor3sv(shortBuffer);
    }

    public void glColor3sv(short[] sArr, int i) {
        this.f10342gl.glColor3sv(sArr, i);
    }

    public void glColor3ub(byte b, byte b2, byte b3) {
        this.f10342gl.glColor3ub(b, b2, b3);
    }

    public void glColor3ubv(ByteBuffer byteBuffer) {
        this.f10342gl.glColor3ubv(byteBuffer);
    }

    public void glColor3ubv(byte[] bArr, int i) {
        this.f10342gl.glColor3ubv(bArr, i);
    }

    public void glColor3ui(int i, int i2, int i3) {
        this.f10342gl.glColor3ui(i, i2, i3);
    }

    public void glColor3uiv(int[] iArr, int i) {
        this.f10342gl.glColor3uiv(iArr, i);
    }

    public void glColor3uiv(IntBuffer intBuffer) {
        this.f10342gl.glColor3uiv(intBuffer);
    }

    public void glColor3us(short s, short s2, short s3) {
        this.f10342gl.glColor3us(s, s2, s3);
    }

    public void glColor3usv(ShortBuffer shortBuffer) {
        this.f10342gl.glColor3usv(shortBuffer);
    }

    public void glColor3usv(short[] sArr, int i) {
        this.f10342gl.glColor3usv(sArr, i);
    }

    public void glColor4b(byte b, byte b2, byte b3, byte b4) {
        this.f10342gl.glColor4b(b, b2, b3, b4);
    }

    public void glColor4bv(ByteBuffer byteBuffer) {
        this.f10342gl.glColor4bv(byteBuffer);
    }

    public void glColor4bv(byte[] bArr, int i) {
        this.f10342gl.glColor4bv(bArr, i);
    }

    public void glColor4d(double d, double d2, double d3, double d4) {
        this.f10342gl.glColor4d(d, d2, d3, d4);
    }

    public void glColor4dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glColor4dv(doubleBuffer);
    }

    public void glColor4dv(double[] dArr, int i) {
        this.f10342gl.glColor4dv(dArr, i);
    }

    public void glColor4f(float f, float f2, float f3, float f4) {
        this.f10342gl.glColor4f(f, f2, f3, f4);
    }

    public void glColor4fNormal3fVertex3fSUN(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        this.f10342gl.glColor4fNormal3fVertex3fSUN(f, f2, f3, f4, f5, f6, f7, f8, f9, f10);
    }

    public void glColor4fNormal3fVertex3fvSUN(float[] fArr, int i, float[] fArr2, int i2, float[] fArr3, int i3) {
        this.f10342gl.glColor4fNormal3fVertex3fvSUN(fArr, i, fArr2, i2, fArr3, i3);
    }

    public void glColor4fNormal3fVertex3fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3) {
        this.f10342gl.glColor4fNormal3fVertex3fvSUN(floatBuffer, floatBuffer2, floatBuffer3);
    }

    public void glColor4fv(float[] fArr, int i) {
        this.f10342gl.glColor4fv(fArr, i);
    }

    public void glColor4fv(FloatBuffer floatBuffer) {
        this.f10342gl.glColor4fv(floatBuffer);
    }

    public void glColor4hNV(short s, short s2, short s3, short s4) {
        this.f10342gl.glColor4hNV(s, s2, s3, s4);
    }

    public void glColor4hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glColor4hvNV(shortBuffer);
    }

    public void glColor4hvNV(short[] sArr, int i) {
        this.f10342gl.glColor4hvNV(sArr, i);
    }

    public void glColor4i(int i, int i2, int i3, int i4) {
        this.f10342gl.glColor4i(i, i2, i3, i4);
    }

    public void glColor4iv(IntBuffer intBuffer) {
        this.f10342gl.glColor4iv(intBuffer);
    }

    public void glColor4iv(int[] iArr, int i) {
        this.f10342gl.glColor4iv(iArr, i);
    }

    public void glColor4s(short s, short s2, short s3, short s4) {
        this.f10342gl.glColor4s(s, s2, s3, s4);
    }

    public void glColor4sv(ShortBuffer shortBuffer) {
        this.f10342gl.glColor4sv(shortBuffer);
    }

    public void glColor4sv(short[] sArr, int i) {
        this.f10342gl.glColor4sv(sArr, i);
    }

    public void glColor4ub(byte b, byte b2, byte b3, byte b4) {
        this.f10342gl.glColor4ub(b, b2, b3, b4);
    }

    public void glColor4ubVertex2fSUN(byte b, byte b2, byte b3, byte b4, float f, float f2) {
        this.f10342gl.glColor4ubVertex2fSUN(b, b2, b3, b4, f, f2);
    }

    public void glColor4ubVertex2fvSUN(ByteBuffer byteBuffer, FloatBuffer floatBuffer) {
        this.f10342gl.glColor4ubVertex2fvSUN(byteBuffer, floatBuffer);
    }

    public void glColor4ubVertex2fvSUN(byte[] bArr, int i, float[] fArr, int i2) {
        this.f10342gl.glColor4ubVertex2fvSUN(bArr, i, fArr, i2);
    }

    public void glColor4ubVertex3fSUN(byte b, byte b2, byte b3, byte b4, float f, float f2, float f3) {
        this.f10342gl.glColor4ubVertex3fSUN(b, b2, b3, b4, f, f2, f3);
    }

    public void glColor4ubVertex3fvSUN(ByteBuffer byteBuffer, FloatBuffer floatBuffer) {
        this.f10342gl.glColor4ubVertex3fvSUN(byteBuffer, floatBuffer);
    }

    public void glColor4ubVertex3fvSUN(byte[] bArr, int i, float[] fArr, int i2) {
        this.f10342gl.glColor4ubVertex3fvSUN(bArr, i, fArr, i2);
    }

    public void glColor4ubv(ByteBuffer byteBuffer) {
        this.f10342gl.glColor4ubv(byteBuffer);
    }

    public void glColor4ubv(byte[] bArr, int i) {
        this.f10342gl.glColor4ubv(bArr, i);
    }

    public void glColor4ui(int i, int i2, int i3, int i4) {
        this.f10342gl.glColor4ui(i, i2, i3, i4);
    }

    public void glColor4uiv(int[] iArr, int i) {
        this.f10342gl.glColor4uiv(iArr, i);
    }

    public void glColor4uiv(IntBuffer intBuffer) {
        this.f10342gl.glColor4uiv(intBuffer);
    }

    public void glColor4us(short s, short s2, short s3, short s4) {
        this.f10342gl.glColor4us(s, s2, s3, s4);
    }

    public void glColor4usv(short[] sArr, int i) {
        this.f10342gl.glColor4usv(sArr, i);
    }

    public void glColor4usv(ShortBuffer shortBuffer) {
        this.f10342gl.glColor4usv(shortBuffer);
    }

    public void glColorFragmentOp1ATI(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.f10342gl.glColorFragmentOp1ATI(i, i2, i3, i4, i5, i6, i7);
    }

    public void glColorFragmentOp2ATI(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.f10342gl.glColorFragmentOp2ATI(i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
    }

    public void glColorFragmentOp3ATI(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13) {
        this.f10342gl.glColorFragmentOp3ATI(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13);
    }

    public void glColorMask(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f10342gl.glColorMask(z, z2, z3, z4);
    }

    public void glColorMaskIndexedEXT(int i, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f10342gl.glColorMaskIndexedEXT(i, z, z2, z3, z4);
    }

    public void glColorMaterial(int i, int i2) {
        this.f10342gl.glColorMaterial(i, i2);
    }

    public void glColorPointer(int i, int i2, int i3, long j) {
        this.f10342gl.glColorPointer(i, i2, i3, j);
    }

    public void glColorPointer(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glColorPointer(i, i2, i3, buffer);
    }

    public void glColorSubTable(int i, int i2, int i3, int i4, int i5, long j) {
        this.f10342gl.glColorSubTable(i, i2, i3, i4, i5, j);
    }

    public void glColorSubTable(int i, int i2, int i3, int i4, int i5, Buffer buffer) {
        this.f10342gl.glColorSubTable(i, i2, i3, i4, i5, buffer);
    }

    public void glColorTable(int i, int i2, int i3, int i4, int i5, long j) {
        this.f10342gl.glColorTable(i, i2, i3, i4, i5, j);
    }

    public void glColorTable(int i, int i2, int i3, int i4, int i5, Buffer buffer) {
        this.f10342gl.glColorTable(i, i2, i3, i4, i5, buffer);
    }

    public void glColorTableEXT(int i, int i2, int i3, int i4, int i5, Buffer buffer) {
        this.f10342gl.glColorTableEXT(i, i2, i3, i4, i5, buffer);
    }

    public void glColorTableParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glColorTableParameterfv(i, i2, floatBuffer);
    }

    public void glColorTableParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glColorTableParameterfv(i, i2, fArr, i3);
    }

    public void glColorTableParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glColorTableParameteriv(i, i2, iArr, i3);
    }

    public void glColorTableParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glColorTableParameteriv(i, i2, intBuffer);
    }

    public void glCombinerInputNV(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glCombinerInputNV(i, i2, i3, i4, i5, i6);
    }

    public void glCombinerOutputNV(int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z, boolean z2, boolean z3) {
        this.f10342gl.glCombinerOutputNV(i, i2, i3, i4, i5, i6, i7, z, z2, z3);
    }

    public void glCombinerParameterfNV(int i, float f) {
        this.f10342gl.glCombinerParameterfNV(i, f);
    }

    public void glCombinerParameterfvNV(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glCombinerParameterfvNV(i, floatBuffer);
    }

    public void glCombinerParameterfvNV(int i, float[] fArr, int i2) {
        this.f10342gl.glCombinerParameterfvNV(i, fArr, i2);
    }

    public void glCombinerParameteriNV(int i, int i2) {
        this.f10342gl.glCombinerParameteriNV(i, i2);
    }

    public void glCombinerParameterivNV(int i, int[] iArr, int i2) {
        this.f10342gl.glCombinerParameterivNV(i, iArr, i2);
    }

    public void glCombinerParameterivNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glCombinerParameterivNV(i, intBuffer);
    }

    public void glCombinerStageParameterfvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glCombinerStageParameterfvNV(i, i2, floatBuffer);
    }

    public void glCombinerStageParameterfvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glCombinerStageParameterfvNV(i, i2, fArr, i3);
    }

    public void glCompileShader(int i) {
        this.f10342gl.glCompileShader(i);
    }

    public void glCompileShaderARB(int i) {
        this.f10342gl.glCompileShaderARB(i);
    }

    public void glCompressedTexImage1D(int i, int i2, int i3, int i4, int i5, int i6, long j) {
        this.f10342gl.glCompressedTexImage1D(i, i2, i3, i4, i5, i6, j);
    }

    public void glCompressedTexImage1D(int i, int i2, int i3, int i4, int i5, int i6, Buffer buffer) {
        this.f10342gl.glCompressedTexImage1D(i, i2, i3, i4, i5, i6, buffer);
    }

    public void glCompressedTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, Buffer buffer) {
        this.f10342gl.glCompressedTexImage2D(i, i2, i3, i4, i5, i6, i7, buffer);
    }

    public void glCompressedTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, long j) {
        this.f10342gl.glCompressedTexImage2D(i, i2, i3, i4, i5, i6, i7, j);
    }

    public void glCompressedTexImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j) {
        this.f10342gl.glCompressedTexImage3D(i, i2, i3, i4, i5, i6, i7, i8, j);
    }

    public void glCompressedTexImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        this.f10342gl.glCompressedTexImage3D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
    }

    public void glCompressedTexSubImage1D(int i, int i2, int i3, int i4, int i5, int i6, long j) {
        this.f10342gl.glCompressedTexSubImage1D(i, i2, i3, i4, i5, i6, j);
    }

    public void glCompressedTexSubImage1D(int i, int i2, int i3, int i4, int i5, int i6, Buffer buffer) {
        this.f10342gl.glCompressedTexSubImage1D(i, i2, i3, i4, i5, i6, buffer);
    }

    public void glCompressedTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        this.f10342gl.glCompressedTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
    }

    public void glCompressedTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j) {
        this.f10342gl.glCompressedTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8, j);
    }

    public void glCompressedTexSubImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, long j) {
        this.f10342gl.glCompressedTexSubImage3D(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, j);
    }

    public void glCompressedTexSubImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, Buffer buffer) {
        this.f10342gl.glCompressedTexSubImage3D(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, buffer);
    }

    public void glConvolutionFilter1D(int i, int i2, int i3, int i4, int i5, Buffer buffer) {
        this.f10342gl.glConvolutionFilter1D(i, i2, i3, i4, i5, buffer);
    }

    public void glConvolutionFilter1D(int i, int i2, int i3, int i4, int i5, long j) {
        this.f10342gl.glConvolutionFilter1D(i, i2, i3, i4, i5, j);
    }

    public void glConvolutionFilter2D(int i, int i2, int i3, int i4, int i5, int i6, Buffer buffer) {
        this.f10342gl.glConvolutionFilter2D(i, i2, i3, i4, i5, i6, buffer);
    }

    public void glConvolutionFilter2D(int i, int i2, int i3, int i4, int i5, int i6, long j) {
        this.f10342gl.glConvolutionFilter2D(i, i2, i3, i4, i5, i6, j);
    }

    public void glConvolutionParameterf(int i, int i2, float f) {
        this.f10342gl.glConvolutionParameterf(i, i2, f);
    }

    public void glConvolutionParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glConvolutionParameterfv(i, i2, floatBuffer);
    }

    public void glConvolutionParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glConvolutionParameterfv(i, i2, fArr, i3);
    }

    public void glConvolutionParameteri(int i, int i2, int i3) {
        this.f10342gl.glConvolutionParameteri(i, i2, i3);
    }

    public void glConvolutionParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glConvolutionParameteriv(i, i2, intBuffer);
    }

    public void glConvolutionParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glConvolutionParameteriv(i, i2, iArr, i3);
    }

    public void glCopyColorSubTable(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glCopyColorSubTable(i, i2, i3, i4, i5);
    }

    public void glCopyColorTable(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glCopyColorTable(i, i2, i3, i4, i5);
    }

    public void glCopyConvolutionFilter1D(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glCopyConvolutionFilter1D(i, i2, i3, i4, i5);
    }

    public void glCopyConvolutionFilter2D(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glCopyConvolutionFilter2D(i, i2, i3, i4, i5, i6);
    }

    public void glCopyPixels(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glCopyPixels(i, i2, i3, i4, i5);
    }

    public void glCopyTexImage1D(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.f10342gl.glCopyTexImage1D(i, i2, i3, i4, i5, i6, i7);
    }

    public void glCopyTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f10342gl.glCopyTexImage2D(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void glCopyTexSubImage1D(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glCopyTexSubImage1D(i, i2, i3, i4, i5, i6);
    }

    public void glCopyTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.f10342gl.glCopyTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void glCopyTexSubImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        this.f10342gl.glCopyTexSubImage3D(i, i2, i3, i4, i5, i6, i7, i8, i9);
    }

    public int glCreateProgram() {
        return this.f10342gl.glCreateProgram();
    }

    public int glCreateProgramObjectARB() {
        return this.f10342gl.glCreateProgramObjectARB();
    }

    public int glCreateShader(int i) {
        return this.f10342gl.glCreateShader(i);
    }

    public int glCreateShaderObjectARB(int i) {
        return this.f10342gl.glCreateShaderObjectARB(i);
    }

    public void glCullFace(int i) {
        this.f10342gl.glCullFace(i);
    }

    public void glCullParameterdvEXT(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glCullParameterdvEXT(i, doubleBuffer);
    }

    public void glCullParameterdvEXT(int i, double[] dArr, int i2) {
        this.f10342gl.glCullParameterdvEXT(i, dArr, i2);
    }

    public void glCullParameterfvEXT(int i, float[] fArr, int i2) {
        this.f10342gl.glCullParameterfvEXT(i, fArr, i2);
    }

    public void glCullParameterfvEXT(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glCullParameterfvEXT(i, floatBuffer);
    }

    public void glCurrentPaletteMatrixARB(int i) {
        this.f10342gl.glCurrentPaletteMatrixARB(i);
    }

    public void glDeformSGIX(int i) {
        this.f10342gl.glDeformSGIX(i);
    }

    public void glDeformationMap3dSGIX(int i, double d, double d2, int i2, int i3, double d3, double d4, int i4, int i5, double d5, double d6, int i6, int i7, double[] dArr, int i8) {
        this.f10342gl.glDeformationMap3dSGIX(i, d, d2, i2, i3, d3, d4, i4, i5, d5, d6, i6, i7, dArr, i8);
    }

    public void glDeformationMap3dSGIX(int i, double d, double d2, int i2, int i3, double d3, double d4, int i4, int i5, double d5, double d6, int i6, int i7, DoubleBuffer doubleBuffer) {
        this.f10342gl.glDeformationMap3dSGIX(i, d, d2, i2, i3, d3, d4, i4, i5, d5, d6, i6, i7, doubleBuffer);
    }

    public void glDeformationMap3fSGIX(int i, float f, float f2, int i2, int i3, float f3, float f4, int i4, int i5, float f5, float f6, int i6, int i7, float[] fArr, int i8) {
        this.f10342gl.glDeformationMap3fSGIX(i, f, f2, i2, i3, f3, f4, i4, i5, f5, f6, i6, i7, fArr, i8);
    }

    public void glDeformationMap3fSGIX(int i, float f, float f2, int i2, int i3, float f3, float f4, int i4, int i5, float f5, float f6, int i6, int i7, FloatBuffer floatBuffer) {
        this.f10342gl.glDeformationMap3fSGIX(i, f, f2, i2, i3, f3, f4, i4, i5, f5, f6, i6, i7, floatBuffer);
    }

    public void glDeleteAsyncMarkersSGIX(int i, int i2) {
        this.f10342gl.glDeleteAsyncMarkersSGIX(i, i2);
    }

    public void glDeleteBufferRegion(int i) {
        this.f10342gl.glDeleteBufferRegion(i);
    }

    public void glDeleteBuffers(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteBuffers(i, intBuffer);
    }

    public void glDeleteBuffers(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteBuffers(i, iArr, i2);
    }

    public void glDeleteBuffersARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteBuffersARB(i, intBuffer);
    }

    public void glDeleteBuffersARB(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteBuffersARB(i, iArr, i2);
    }

    public void glDeleteFencesAPPLE(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteFencesAPPLE(i, iArr, i2);
    }

    public void glDeleteFencesAPPLE(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteFencesAPPLE(i, intBuffer);
    }

    public void glDeleteFencesNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteFencesNV(i, intBuffer);
    }

    public void glDeleteFencesNV(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteFencesNV(i, iArr, i2);
    }

    public void glDeleteFragmentShaderATI(int i) {
        this.f10342gl.glDeleteFragmentShaderATI(i);
    }

    public void glDeleteFramebuffersEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteFramebuffersEXT(i, intBuffer);
    }

    public void glDeleteFramebuffersEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteFramebuffersEXT(i, iArr, i2);
    }

    public void glDeleteLists(int i, int i2) {
        this.f10342gl.glDeleteLists(i, i2);
    }

    public void glDeleteObjectARB(int i) {
        this.f10342gl.glDeleteObjectARB(i);
    }

    public void glDeleteOcclusionQueriesNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteOcclusionQueriesNV(i, intBuffer);
    }

    public void glDeleteOcclusionQueriesNV(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteOcclusionQueriesNV(i, iArr, i2);
    }

    public void glDeleteProgram(int i) {
        this.f10342gl.glDeleteProgram(i);
    }

    public void glDeleteProgramsARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteProgramsARB(i, intBuffer);
    }

    public void glDeleteProgramsARB(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteProgramsARB(i, iArr, i2);
    }

    public void glDeleteProgramsNV(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteProgramsNV(i, iArr, i2);
    }

    public void glDeleteProgramsNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteProgramsNV(i, intBuffer);
    }

    public void glDeleteQueries(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteQueries(i, iArr, i2);
    }

    public void glDeleteQueries(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteQueries(i, intBuffer);
    }

    public void glDeleteQueriesARB(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteQueriesARB(i, iArr, i2);
    }

    public void glDeleteQueriesARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteQueriesARB(i, intBuffer);
    }

    public void glDeleteRenderbuffersEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteRenderbuffersEXT(i, iArr, i2);
    }

    public void glDeleteRenderbuffersEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteRenderbuffersEXT(i, intBuffer);
    }

    public void glDeleteShader(int i) {
        this.f10342gl.glDeleteShader(i);
    }

    public void glDeleteTextures(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteTextures(i, iArr, i2);
    }

    public void glDeleteTextures(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteTextures(i, intBuffer);
    }

    public void glDeleteVertexArraysAPPLE(int i, IntBuffer intBuffer) {
        this.f10342gl.glDeleteVertexArraysAPPLE(i, intBuffer);
    }

    public void glDeleteVertexArraysAPPLE(int i, int[] iArr, int i2) {
        this.f10342gl.glDeleteVertexArraysAPPLE(i, iArr, i2);
    }

    public void glDeleteVertexShaderEXT(int i) {
        this.f10342gl.glDeleteVertexShaderEXT(i);
    }

    public void glDepthBoundsEXT(double d, double d2) {
        this.f10342gl.glDepthBoundsEXT(d, d2);
    }

    public void glDepthBoundsdNV(double d, double d2) {
        this.f10342gl.glDepthBoundsdNV(d, d2);
    }

    public void glDepthFunc(int i) {
        this.f10342gl.glDepthFunc(i);
    }

    public void glDepthMask(boolean z) {
        this.f10342gl.glDepthMask(z);
    }

    public void glDepthRange(double d, double d2) {
        this.f10342gl.glDepthRange(d, d2);
    }

    public void glDepthRangedNV(double d, double d2) {
        this.f10342gl.glDepthRangedNV(d, d2);
    }

    public void glDetachObjectARB(int i, int i2) {
        this.f10342gl.glDetachObjectARB(i, i2);
    }

    public void glDetachShader(int i, int i2) {
        this.f10342gl.glDetachShader(i, i2);
    }

    public void glDetailTexFuncSGIS(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glDetailTexFuncSGIS(i, i2, floatBuffer);
    }

    public void glDetailTexFuncSGIS(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glDetailTexFuncSGIS(i, i2, fArr, i3);
    }

    public void glDisable(int i) {
        this.f10342gl.glDisable(i);
    }

    public void glDisableClientState(int i) {
        this.f10342gl.glDisableClientState(i);
    }

    public void glDisableIndexedEXT(int i, int i2) {
        this.f10342gl.glDisableIndexedEXT(i, i2);
    }

    public void glDisableVariantClientStateEXT(int i) {
        this.f10342gl.glDisableVariantClientStateEXT(i);
    }

    public void glDisableVertexAttribAPPLE(int i, int i2) {
        this.f10342gl.glDisableVertexAttribAPPLE(i, i2);
    }

    public void glDisableVertexAttribArray(int i) {
        this.f10342gl.glDisableVertexAttribArray(i);
    }

    public void glDisableVertexAttribArrayARB(int i) {
        this.f10342gl.glDisableVertexAttribArrayARB(i);
    }

    public void glDrawArrays(int i, int i2, int i3) {
        this.f10342gl.glDrawArrays(i, i2, i3);
    }

    public void glDrawArraysInstancedEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glDrawArraysInstancedEXT(i, i2, i3, i4);
    }

    public void glDrawBuffer(int i) {
        this.f10342gl.glDrawBuffer(i);
    }

    public void glDrawBufferRegion(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.f10342gl.glDrawBufferRegion(i, i2, i3, i4, i5, i6, i7);
    }

    public void glDrawBuffers(int i, int[] iArr, int i2) {
        this.f10342gl.glDrawBuffers(i, iArr, i2);
    }

    public void glDrawBuffers(int i, IntBuffer intBuffer) {
        this.f10342gl.glDrawBuffers(i, intBuffer);
    }

    public void glDrawBuffersARB(int i, int[] iArr, int i2) {
        this.f10342gl.glDrawBuffersARB(i, iArr, i2);
    }

    public void glDrawBuffersARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glDrawBuffersARB(i, intBuffer);
    }

    public void glDrawBuffersATI(int i, int[] iArr, int i2) {
        this.f10342gl.glDrawBuffersATI(i, iArr, i2);
    }

    public void glDrawBuffersATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glDrawBuffersATI(i, intBuffer);
    }

    public void glDrawElementArrayAPPLE(int i, int i2, int i3) {
        this.f10342gl.glDrawElementArrayAPPLE(i, i2, i3);
    }

    public void glDrawElementArrayATI(int i, int i2) {
        this.f10342gl.glDrawElementArrayATI(i, i2);
    }

    public void glDrawElements(int i, int i2, int i3, long j) {
        this.f10342gl.glDrawElements(i, i2, i3, j);
    }

    public void glDrawElements(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glDrawElements(i, i2, i3, buffer);
    }

    public void glDrawElementsInstancedEXT(int i, int i2, int i3, Buffer buffer, int i4) {
        this.f10342gl.glDrawElementsInstancedEXT(i, i2, i3, buffer, i4);
    }

    public void glDrawMeshArraysSUN(int i, int i2, int i3, int i4) {
        this.f10342gl.glDrawMeshArraysSUN(i, i2, i3, i4);
    }

    public void glDrawPixels(int i, int i2, int i3, int i4, Buffer buffer) {
        this.f10342gl.glDrawPixels(i, i2, i3, i4, buffer);
    }

    public void glDrawPixels(int i, int i2, int i3, int i4, long j) {
        this.f10342gl.glDrawPixels(i, i2, i3, i4, j);
    }

    public void glDrawRangeElementArrayAPPLE(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glDrawRangeElementArrayAPPLE(i, i2, i3, i4, i5);
    }

    public void glDrawRangeElementArrayATI(int i, int i2, int i3, int i4) {
        this.f10342gl.glDrawRangeElementArrayATI(i, i2, i3, i4);
    }

    public void glDrawRangeElements(int i, int i2, int i3, int i4, int i5, Buffer buffer) {
        this.f10342gl.glDrawRangeElements(i, i2, i3, i4, i5, buffer);
    }

    public void glDrawRangeElements(int i, int i2, int i3, int i4, int i5, long j) {
        this.f10342gl.glDrawRangeElements(i, i2, i3, i4, i5, j);
    }

    public void glEdgeFlag(boolean z) {
        this.f10342gl.glEdgeFlag(z);
    }

    public void glEdgeFlagPointer(int i, Buffer buffer) {
        this.f10342gl.glEdgeFlagPointer(i, buffer);
    }

    public void glEdgeFlagPointer(int i, long j) {
        this.f10342gl.glEdgeFlagPointer(i, j);
    }

    public void glEdgeFlagv(byte[] bArr, int i) {
        this.f10342gl.glEdgeFlagv(bArr, i);
    }

    public void glEdgeFlagv(ByteBuffer byteBuffer) {
        this.f10342gl.glEdgeFlagv(byteBuffer);
    }

    public void glElementPointerAPPLE(int i, Buffer buffer) {
        this.f10342gl.glElementPointerAPPLE(i, buffer);
    }

    public void glElementPointerATI(int i, Buffer buffer) {
        this.f10342gl.glElementPointerATI(i, buffer);
    }

    public void glElementPointerATI(int i, long j) {
        this.f10342gl.glElementPointerATI(i, j);
    }

    public void glEnable(int i) {
        this.f10342gl.glEnable(i);
    }

    public void glEnableClientState(int i) {
        this.f10342gl.glEnableClientState(i);
    }

    public void glEnableIndexedEXT(int i, int i2) {
        this.f10342gl.glEnableIndexedEXT(i, i2);
    }

    public void glEnableVariantClientStateEXT(int i) {
        this.f10342gl.glEnableVariantClientStateEXT(i);
    }

    public void glEnableVertexAttribAPPLE(int i, int i2) {
        this.f10342gl.glEnableVertexAttribAPPLE(i, i2);
    }

    public void glEnableVertexAttribArray(int i) {
        this.f10342gl.glEnableVertexAttribArray(i);
    }

    public void glEnableVertexAttribArrayARB(int i) {
        this.f10342gl.glEnableVertexAttribArrayARB(i);
    }

    public void glEnd() {
        this.f10342gl.glEnd();
    }

    public void glEndFragmentShaderATI() {
        this.f10342gl.glEndFragmentShaderATI();
    }

    public void glEndList() {
        this.f10342gl.glEndList();
    }

    public void glEndOcclusionQueryNV() {
        this.f10342gl.glEndOcclusionQueryNV();
    }

    public void glEndQuery(int i) {
        this.f10342gl.glEndQuery(i);
    }

    public void glEndQueryARB(int i) {
        this.f10342gl.glEndQueryARB(i);
    }

    public void glEndTransformFeedbackNV() {
        this.f10342gl.glEndTransformFeedbackNV();
    }

    public void glEndVertexShaderEXT() {
        this.f10342gl.glEndVertexShaderEXT();
    }

    public void glEvalCoord1d(double d) {
        this.f10342gl.glEvalCoord1d(d);
    }

    public void glEvalCoord1dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glEvalCoord1dv(doubleBuffer);
    }

    public void glEvalCoord1dv(double[] dArr, int i) {
        this.f10342gl.glEvalCoord1dv(dArr, i);
    }

    public void glEvalCoord1f(float f) {
        this.f10342gl.glEvalCoord1f(f);
    }

    public void glEvalCoord1fv(FloatBuffer floatBuffer) {
        this.f10342gl.glEvalCoord1fv(floatBuffer);
    }

    public void glEvalCoord1fv(float[] fArr, int i) {
        this.f10342gl.glEvalCoord1fv(fArr, i);
    }

    public void glEvalCoord2d(double d, double d2) {
        this.f10342gl.glEvalCoord2d(d, d2);
    }

    public void glEvalCoord2dv(double[] dArr, int i) {
        this.f10342gl.glEvalCoord2dv(dArr, i);
    }

    public void glEvalCoord2dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glEvalCoord2dv(doubleBuffer);
    }

    public void glEvalCoord2f(float f, float f2) {
        this.f10342gl.glEvalCoord2f(f, f2);
    }

    public void glEvalCoord2fv(float[] fArr, int i) {
        this.f10342gl.glEvalCoord2fv(fArr, i);
    }

    public void glEvalCoord2fv(FloatBuffer floatBuffer) {
        this.f10342gl.glEvalCoord2fv(floatBuffer);
    }

    public void glEvalMapsNV(int i, int i2) {
        this.f10342gl.glEvalMapsNV(i, i2);
    }

    public void glEvalMesh1(int i, int i2, int i3) {
        this.f10342gl.glEvalMesh1(i, i2, i3);
    }

    public void glEvalMesh2(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glEvalMesh2(i, i2, i3, i4, i5);
    }

    public void glEvalPoint1(int i) {
        this.f10342gl.glEvalPoint1(i);
    }

    public void glEvalPoint2(int i, int i2) {
        this.f10342gl.glEvalPoint2(i, i2);
    }

    public void glExecuteProgramNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glExecuteProgramNV(i, i2, floatBuffer);
    }

    public void glExecuteProgramNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glExecuteProgramNV(i, i2, fArr, i3);
    }

    public void glExtractComponentEXT(int i, int i2, int i3) {
        this.f10342gl.glExtractComponentEXT(i, i2, i3);
    }

    public void glFeedbackBuffer(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glFeedbackBuffer(i, i2, floatBuffer);
    }

    public void glFinalCombinerInputNV(int i, int i2, int i3, int i4) {
        this.f10342gl.glFinalCombinerInputNV(i, i2, i3, i4);
    }

    public void glFinish() {
        this.f10342gl.glFinish();
    }

    public int glFinishAsyncSGIX(IntBuffer intBuffer) {
        return this.f10342gl.glFinishAsyncSGIX(intBuffer);
    }

    public int glFinishAsyncSGIX(int[] iArr, int i) {
        return this.f10342gl.glFinishAsyncSGIX(iArr, i);
    }

    public void glFinishFenceAPPLE(int i) {
        this.f10342gl.glFinishFenceAPPLE(i);
    }

    public void glFinishFenceNV(int i) {
        this.f10342gl.glFinishFenceNV(i);
    }

    public void glFinishObjectAPPLE(int i, int i2) {
        this.f10342gl.glFinishObjectAPPLE(i, i2);
    }

    public void glFinishRenderAPPLE() {
        this.f10342gl.glFinishRenderAPPLE();
    }

    public void glFinishTextureSUNX() {
        this.f10342gl.glFinishTextureSUNX();
    }

    public void glFlush() {
        this.f10342gl.glFlush();
    }

    public void glFlushMappedBufferRangeAPPLE(int i, int i2, int i3) {
        this.f10342gl.glFlushMappedBufferRangeAPPLE(i, i2, i3);
    }

    public void glFlushPixelDataRangeNV(int i) {
        this.f10342gl.glFlushPixelDataRangeNV(i);
    }

    public void glFlushRasterSGIX() {
        this.f10342gl.glFlushRasterSGIX();
    }

    public void glFlushRenderAPPLE() {
        this.f10342gl.glFlushRenderAPPLE();
    }

    public void glFlushVertexArrayRangeAPPLE(int i, Buffer buffer) {
        this.f10342gl.glFlushVertexArrayRangeAPPLE(i, buffer);
    }

    public void glFlushVertexArrayRangeNV() {
        this.f10342gl.glFlushVertexArrayRangeNV();
    }

    public void glFogCoordPointer(int i, int i2, Buffer buffer) {
        this.f10342gl.glFogCoordPointer(i, i2, buffer);
    }

    public void glFogCoordPointer(int i, int i2, long j) {
        this.f10342gl.glFogCoordPointer(i, i2, j);
    }

    public void glFogCoordPointerEXT(int i, int i2, Buffer buffer) {
        this.f10342gl.glFogCoordPointerEXT(i, i2, buffer);
    }

    public void glFogCoordPointerEXT(int i, int i2, long j) {
        this.f10342gl.glFogCoordPointerEXT(i, i2, j);
    }

    public void glFogCoordd(double d) {
        this.f10342gl.glFogCoordd(d);
    }

    public void glFogCoorddEXT(double d) {
        this.f10342gl.glFogCoorddEXT(d);
    }

    public void glFogCoorddv(double[] dArr, int i) {
        this.f10342gl.glFogCoorddv(dArr, i);
    }

    public void glFogCoorddv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glFogCoorddv(doubleBuffer);
    }

    public void glFogCoorddvEXT(DoubleBuffer doubleBuffer) {
        this.f10342gl.glFogCoorddvEXT(doubleBuffer);
    }

    public void glFogCoorddvEXT(double[] dArr, int i) {
        this.f10342gl.glFogCoorddvEXT(dArr, i);
    }

    public void glFogCoordf(float f) {
        this.f10342gl.glFogCoordf(f);
    }

    public void glFogCoordfEXT(float f) {
        this.f10342gl.glFogCoordfEXT(f);
    }

    public void glFogCoordfv(FloatBuffer floatBuffer) {
        this.f10342gl.glFogCoordfv(floatBuffer);
    }

    public void glFogCoordfv(float[] fArr, int i) {
        this.f10342gl.glFogCoordfv(fArr, i);
    }

    public void glFogCoordfvEXT(FloatBuffer floatBuffer) {
        this.f10342gl.glFogCoordfvEXT(floatBuffer);
    }

    public void glFogCoordfvEXT(float[] fArr, int i) {
        this.f10342gl.glFogCoordfvEXT(fArr, i);
    }

    public void glFogCoordhNV(short s) {
        this.f10342gl.glFogCoordhNV(s);
    }

    public void glFogCoordhvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glFogCoordhvNV(shortBuffer);
    }

    public void glFogCoordhvNV(short[] sArr, int i) {
        this.f10342gl.glFogCoordhvNV(sArr, i);
    }

    public void glFogFuncSGIS(int i, float[] fArr, int i2) {
        this.f10342gl.glFogFuncSGIS(i, fArr, i2);
    }

    public void glFogFuncSGIS(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glFogFuncSGIS(i, floatBuffer);
    }

    public void glFogf(int i, float f) {
        this.f10342gl.glFogf(i, f);
    }

    public void glFogfv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glFogfv(i, floatBuffer);
    }

    public void glFogfv(int i, float[] fArr, int i2) {
        this.f10342gl.glFogfv(i, fArr, i2);
    }

    public void glFogi(int i, int i2) {
        this.f10342gl.glFogi(i, i2);
    }

    public void glFogiv(int i, int[] iArr, int i2) {
        this.f10342gl.glFogiv(i, iArr, i2);
    }

    public void glFogiv(int i, IntBuffer intBuffer) {
        this.f10342gl.glFogiv(i, intBuffer);
    }

    public void glFragmentColorMaterialSGIX(int i, int i2) {
        this.f10342gl.glFragmentColorMaterialSGIX(i, i2);
    }

    public void glFragmentLightModelfSGIX(int i, float f) {
        this.f10342gl.glFragmentLightModelfSGIX(i, f);
    }

    public void glFragmentLightModelfvSGIX(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glFragmentLightModelfvSGIX(i, floatBuffer);
    }

    public void glFragmentLightModelfvSGIX(int i, float[] fArr, int i2) {
        this.f10342gl.glFragmentLightModelfvSGIX(i, fArr, i2);
    }

    public void glFragmentLightModeliSGIX(int i, int i2) {
        this.f10342gl.glFragmentLightModeliSGIX(i, i2);
    }

    public void glFragmentLightModelivSGIX(int i, IntBuffer intBuffer) {
        this.f10342gl.glFragmentLightModelivSGIX(i, intBuffer);
    }

    public void glFragmentLightModelivSGIX(int i, int[] iArr, int i2) {
        this.f10342gl.glFragmentLightModelivSGIX(i, iArr, i2);
    }

    public void glFragmentLightfSGIX(int i, int i2, float f) {
        this.f10342gl.glFragmentLightfSGIX(i, i2, f);
    }

    public void glFragmentLightfvSGIX(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glFragmentLightfvSGIX(i, i2, floatBuffer);
    }

    public void glFragmentLightfvSGIX(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glFragmentLightfvSGIX(i, i2, fArr, i3);
    }

    public void glFragmentLightiSGIX(int i, int i2, int i3) {
        this.f10342gl.glFragmentLightiSGIX(i, i2, i3);
    }

    public void glFragmentLightivSGIX(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glFragmentLightivSGIX(i, i2, iArr, i3);
    }

    public void glFragmentLightivSGIX(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glFragmentLightivSGIX(i, i2, intBuffer);
    }

    public void glFragmentMaterialfSGIX(int i, int i2, float f) {
        this.f10342gl.glFragmentMaterialfSGIX(i, i2, f);
    }

    public void glFragmentMaterialfvSGIX(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glFragmentMaterialfvSGIX(i, i2, floatBuffer);
    }

    public void glFragmentMaterialfvSGIX(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glFragmentMaterialfvSGIX(i, i2, fArr, i3);
    }

    public void glFragmentMaterialiSGIX(int i, int i2, int i3) {
        this.f10342gl.glFragmentMaterialiSGIX(i, i2, i3);
    }

    public void glFragmentMaterialivSGIX(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glFragmentMaterialivSGIX(i, i2, intBuffer);
    }

    public void glFragmentMaterialivSGIX(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glFragmentMaterialivSGIX(i, i2, iArr, i3);
    }

    public void glFrameZoomSGIX(int i) {
        this.f10342gl.glFrameZoomSGIX(i);
    }

    public void glFramebufferRenderbufferEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glFramebufferRenderbufferEXT(i, i2, i3, i4);
    }

    public void glFramebufferTexture1DEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glFramebufferTexture1DEXT(i, i2, i3, i4, i5);
    }

    public void glFramebufferTexture2DEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glFramebufferTexture2DEXT(i, i2, i3, i4, i5);
    }

    public void glFramebufferTexture3DEXT(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glFramebufferTexture3DEXT(i, i2, i3, i4, i5, i6);
    }

    public void glFramebufferTextureEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glFramebufferTextureEXT(i, i2, i3, i4);
    }

    public void glFramebufferTextureFaceEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glFramebufferTextureFaceEXT(i, i2, i3, i4, i5);
    }

    public void glFramebufferTextureLayerEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glFramebufferTextureLayerEXT(i, i2, i3, i4, i5);
    }

    public void glFreeObjectBufferATI(int i) {
        this.f10342gl.glFreeObjectBufferATI(i);
    }

    public void glFrontFace(int i) {
        this.f10342gl.glFrontFace(i);
    }

    public void glFrustum(double d, double d2, double d3, double d4, double d5, double d6) {
        this.f10342gl.glFrustum(d, d2, d3, d4, d5, d6);
    }

    public int glGenAsyncMarkersSGIX(int i) {
        return this.f10342gl.glGenAsyncMarkersSGIX(i);
    }

    public void glGenBuffers(int i, int[] iArr, int i2) {
        this.f10342gl.glGenBuffers(i, iArr, i2);
    }

    public void glGenBuffers(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenBuffers(i, intBuffer);
    }

    public void glGenBuffersARB(int i, int[] iArr, int i2) {
        this.f10342gl.glGenBuffersARB(i, iArr, i2);
    }

    public void glGenBuffersARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenBuffersARB(i, intBuffer);
    }

    public void glGenFencesAPPLE(int i, int[] iArr, int i2) {
        this.f10342gl.glGenFencesAPPLE(i, iArr, i2);
    }

    public void glGenFencesAPPLE(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenFencesAPPLE(i, intBuffer);
    }

    public void glGenFencesNV(int i, int[] iArr, int i2) {
        this.f10342gl.glGenFencesNV(i, iArr, i2);
    }

    public void glGenFencesNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenFencesNV(i, intBuffer);
    }

    public int glGenFragmentShadersATI(int i) {
        return this.f10342gl.glGenFragmentShadersATI(i);
    }

    public void glGenFramebuffersEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenFramebuffersEXT(i, intBuffer);
    }

    public void glGenFramebuffersEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glGenFramebuffersEXT(i, iArr, i2);
    }

    public int glGenLists(int i) {
        return this.f10342gl.glGenLists(i);
    }

    public void glGenOcclusionQueriesNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenOcclusionQueriesNV(i, intBuffer);
    }

    public void glGenOcclusionQueriesNV(int i, int[] iArr, int i2) {
        this.f10342gl.glGenOcclusionQueriesNV(i, iArr, i2);
    }

    public void glGenProgramsARB(int i, int[] iArr, int i2) {
        this.f10342gl.glGenProgramsARB(i, iArr, i2);
    }

    public void glGenProgramsARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenProgramsARB(i, intBuffer);
    }

    public void glGenProgramsNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenProgramsNV(i, intBuffer);
    }

    public void glGenProgramsNV(int i, int[] iArr, int i2) {
        this.f10342gl.glGenProgramsNV(i, iArr, i2);
    }

    public void glGenQueries(int i, int[] iArr, int i2) {
        this.f10342gl.glGenQueries(i, iArr, i2);
    }

    public void glGenQueries(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenQueries(i, intBuffer);
    }

    public void glGenQueriesARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenQueriesARB(i, intBuffer);
    }

    public void glGenQueriesARB(int i, int[] iArr, int i2) {
        this.f10342gl.glGenQueriesARB(i, iArr, i2);
    }

    public void glGenRenderbuffersEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glGenRenderbuffersEXT(i, iArr, i2);
    }

    public void glGenRenderbuffersEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenRenderbuffersEXT(i, intBuffer);
    }

    public int glGenSymbolsEXT(int i, int i2, int i3, int i4) {
        return this.f10342gl.glGenSymbolsEXT(i, i2, i3, i4);
    }

    public void glGenTextures(int i, int[] iArr, int i2) {
        this.f10342gl.glGenTextures(i, iArr, i2);
    }

    public void glGenTextures(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenTextures(i, intBuffer);
    }

    public void glGenVertexArraysAPPLE(int i, int[] iArr, int i2) {
        this.f10342gl.glGenVertexArraysAPPLE(i, iArr, i2);
    }

    public void glGenVertexArraysAPPLE(int i, IntBuffer intBuffer) {
        this.f10342gl.glGenVertexArraysAPPLE(i, intBuffer);
    }

    public int glGenVertexShadersEXT(int i) {
        return this.f10342gl.glGenVertexShadersEXT(i);
    }

    public void glGenerateMipmapEXT(int i) {
        this.f10342gl.glGenerateMipmapEXT(i);
    }

    public void glGetActiveAttrib(int i, int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5, int[] iArr3, int i6, byte[] bArr, int i7) {
        this.f10342gl.glGetActiveAttrib(i, i2, i3, iArr, i4, iArr2, i5, iArr3, i6, bArr, i7);
    }

    public void glGetActiveAttrib(int i, int i2, int i3, IntBuffer intBuffer, IntBuffer intBuffer2, IntBuffer intBuffer3, ByteBuffer byteBuffer) {
        this.f10342gl.glGetActiveAttrib(i, i2, i3, intBuffer, intBuffer2, intBuffer3, byteBuffer);
    }

    public void glGetActiveAttribARB(int i, int i2, int i3, IntBuffer intBuffer, IntBuffer intBuffer2, IntBuffer intBuffer3, ByteBuffer byteBuffer) {
        this.f10342gl.glGetActiveAttribARB(i, i2, i3, intBuffer, intBuffer2, intBuffer3, byteBuffer);
    }

    public void glGetActiveAttribARB(int i, int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5, int[] iArr3, int i6, byte[] bArr, int i7) {
        this.f10342gl.glGetActiveAttribARB(i, i2, i3, iArr, i4, iArr2, i5, iArr3, i6, bArr, i7);
    }

    public void glGetActiveUniform(int i, int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5, int[] iArr3, int i6, byte[] bArr, int i7) {
        this.f10342gl.glGetActiveUniform(i, i2, i3, iArr, i4, iArr2, i5, iArr3, i6, bArr, i7);
    }

    public void glGetActiveUniform(int i, int i2, int i3, IntBuffer intBuffer, IntBuffer intBuffer2, IntBuffer intBuffer3, ByteBuffer byteBuffer) {
        this.f10342gl.glGetActiveUniform(i, i2, i3, intBuffer, intBuffer2, intBuffer3, byteBuffer);
    }

    public void glGetActiveUniformARB(int i, int i2, int i3, IntBuffer intBuffer, IntBuffer intBuffer2, IntBuffer intBuffer3, ByteBuffer byteBuffer) {
        this.f10342gl.glGetActiveUniformARB(i, i2, i3, intBuffer, intBuffer2, intBuffer3, byteBuffer);
    }

    public void glGetActiveUniformARB(int i, int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5, int[] iArr3, int i6, byte[] bArr, int i7) {
        this.f10342gl.glGetActiveUniformARB(i, i2, i3, iArr, i4, iArr2, i5, iArr3, i6, bArr, i7);
    }

    public void glGetActiveVaryingNV(int i, int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5, int[] iArr3, int i6, byte[] bArr, int i7) {
        this.f10342gl.glGetActiveVaryingNV(i, i2, i3, iArr, i4, iArr2, i5, iArr3, i6, bArr, i7);
    }

    public void glGetActiveVaryingNV(int i, int i2, int i3, IntBuffer intBuffer, IntBuffer intBuffer2, IntBuffer intBuffer3, ByteBuffer byteBuffer) {
        this.f10342gl.glGetActiveVaryingNV(i, i2, i3, intBuffer, intBuffer2, intBuffer3, byteBuffer);
    }

    public void glGetArrayObjectfvATI(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetArrayObjectfvATI(i, i2, fArr, i3);
    }

    public void glGetArrayObjectfvATI(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetArrayObjectfvATI(i, i2, floatBuffer);
    }

    public void glGetArrayObjectivATI(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetArrayObjectivATI(i, i2, iArr, i3);
    }

    public void glGetArrayObjectivATI(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetArrayObjectivATI(i, i2, intBuffer);
    }

    public void glGetAttachedObjectsARB(int i, int i2, int[] iArr, int i3, int[] iArr2, int i4) {
        this.f10342gl.glGetAttachedObjectsARB(i, i2, iArr, i3, iArr2, i4);
    }

    public void glGetAttachedObjectsARB(int i, int i2, IntBuffer intBuffer, IntBuffer intBuffer2) {
        this.f10342gl.glGetAttachedObjectsARB(i, i2, intBuffer, intBuffer2);
    }

    public void glGetAttachedShaders(int i, int i2, int[] iArr, int i3, int[] iArr2, int i4) {
        this.f10342gl.glGetAttachedShaders(i, i2, iArr, i3, iArr2, i4);
    }

    public void glGetAttachedShaders(int i, int i2, IntBuffer intBuffer, IntBuffer intBuffer2) {
        this.f10342gl.glGetAttachedShaders(i, i2, intBuffer, intBuffer2);
    }

    public int glGetAttribLocation(int i, String str) {
        return this.f10342gl.glGetAttribLocation(i, str);
    }

    public int glGetAttribLocationARB(int i, String str) {
        return this.f10342gl.glGetAttribLocationARB(i, str);
    }

    public void glGetBooleanIndexedvEXT(int i, int i2, byte[] bArr, int i3) {
        this.f10342gl.glGetBooleanIndexedvEXT(i, i2, bArr, i3);
    }

    public void glGetBooleanIndexedvEXT(int i, int i2, ByteBuffer byteBuffer) {
        this.f10342gl.glGetBooleanIndexedvEXT(i, i2, byteBuffer);
    }

    public void glGetBooleanv(int i, byte[] bArr, int i2) {
        this.f10342gl.glGetBooleanv(i, bArr, i2);
    }

    public void glGetBooleanv(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glGetBooleanv(i, byteBuffer);
    }

    public void glGetBufferParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetBufferParameteriv(i, i2, iArr, i3);
    }

    public void glGetBufferParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetBufferParameteriv(i, i2, intBuffer);
    }

    public void glGetBufferParameterivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetBufferParameterivARB(i, i2, iArr, i3);
    }

    public void glGetBufferParameterivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetBufferParameterivARB(i, i2, intBuffer);
    }

    public void glGetBufferSubData(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glGetBufferSubData(i, i2, i3, buffer);
    }

    public void glGetBufferSubDataARB(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glGetBufferSubDataARB(i, i2, i3, buffer);
    }

    public void glGetClipPlane(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetClipPlane(i, doubleBuffer);
    }

    public void glGetClipPlane(int i, double[] dArr, int i2) {
        this.f10342gl.glGetClipPlane(i, dArr, i2);
    }

    public void glGetColorTable(int i, int i2, int i3, long j) {
        this.f10342gl.glGetColorTable(i, i2, i3, j);
    }

    public void glGetColorTable(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glGetColorTable(i, i2, i3, buffer);
    }

    public void glGetColorTableEXT(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glGetColorTableEXT(i, i2, i3, buffer);
    }

    public void glGetColorTableParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetColorTableParameterfv(i, i2, floatBuffer);
    }

    public void glGetColorTableParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetColorTableParameterfv(i, i2, fArr, i3);
    }

    public void glGetColorTableParameterfvEXT(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetColorTableParameterfvEXT(i, i2, floatBuffer);
    }

    public void glGetColorTableParameterfvEXT(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetColorTableParameterfvEXT(i, i2, fArr, i3);
    }

    public void glGetColorTableParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetColorTableParameteriv(i, i2, intBuffer);
    }

    public void glGetColorTableParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetColorTableParameteriv(i, i2, iArr, i3);
    }

    public void glGetColorTableParameterivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetColorTableParameterivEXT(i, i2, iArr, i3);
    }

    public void glGetColorTableParameterivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetColorTableParameterivEXT(i, i2, intBuffer);
    }

    public void glGetCombinerInputParameterfvNV(int i, int i2, int i3, int i4, float[] fArr, int i5) {
        this.f10342gl.glGetCombinerInputParameterfvNV(i, i2, i3, i4, fArr, i5);
    }

    public void glGetCombinerInputParameterfvNV(int i, int i2, int i3, int i4, FloatBuffer floatBuffer) {
        this.f10342gl.glGetCombinerInputParameterfvNV(i, i2, i3, i4, floatBuffer);
    }

    public void glGetCombinerInputParameterivNV(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        this.f10342gl.glGetCombinerInputParameterivNV(i, i2, i3, i4, iArr, i5);
    }

    public void glGetCombinerInputParameterivNV(int i, int i2, int i3, int i4, IntBuffer intBuffer) {
        this.f10342gl.glGetCombinerInputParameterivNV(i, i2, i3, i4, intBuffer);
    }

    public void glGetCombinerOutputParameterfvNV(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glGetCombinerOutputParameterfvNV(i, i2, i3, fArr, i4);
    }

    public void glGetCombinerOutputParameterfvNV(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glGetCombinerOutputParameterfvNV(i, i2, i3, floatBuffer);
    }

    public void glGetCombinerOutputParameterivNV(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glGetCombinerOutputParameterivNV(i, i2, i3, iArr, i4);
    }

    public void glGetCombinerOutputParameterivNV(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glGetCombinerOutputParameterivNV(i, i2, i3, intBuffer);
    }

    public void glGetCombinerStageParameterfvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetCombinerStageParameterfvNV(i, i2, fArr, i3);
    }

    public void glGetCombinerStageParameterfvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetCombinerStageParameterfvNV(i, i2, floatBuffer);
    }

    public void glGetCompressedTexImage(int i, int i2, Buffer buffer) {
        this.f10342gl.glGetCompressedTexImage(i, i2, buffer);
    }

    public void glGetCompressedTexImage(int i, int i2, long j) {
        this.f10342gl.glGetCompressedTexImage(i, i2, j);
    }

    public void glGetConvolutionFilter(int i, int i2, int i3, long j) {
        this.f10342gl.glGetConvolutionFilter(i, i2, i3, j);
    }

    public void glGetConvolutionFilter(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glGetConvolutionFilter(i, i2, i3, buffer);
    }

    public void glGetConvolutionParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetConvolutionParameterfv(i, i2, fArr, i3);
    }

    public void glGetConvolutionParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetConvolutionParameterfv(i, i2, floatBuffer);
    }

    public void glGetConvolutionParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetConvolutionParameteriv(i, i2, iArr, i3);
    }

    public void glGetConvolutionParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetConvolutionParameteriv(i, i2, intBuffer);
    }

    public void glGetDetailTexFuncSGIS(int i, float[] fArr, int i2) {
        this.f10342gl.glGetDetailTexFuncSGIS(i, fArr, i2);
    }

    public void glGetDetailTexFuncSGIS(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glGetDetailTexFuncSGIS(i, floatBuffer);
    }

    public void glGetDoublev(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetDoublev(i, doubleBuffer);
    }

    public void glGetDoublev(int i, double[] dArr, int i2) {
        this.f10342gl.glGetDoublev(i, dArr, i2);
    }

    public int glGetError() {
        return this.f10342gl.glGetError();
    }

    public void glGetFenceivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetFenceivNV(i, i2, intBuffer);
    }

    public void glGetFenceivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetFenceivNV(i, i2, iArr, i3);
    }

    public void glGetFinalCombinerInputParameterfvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetFinalCombinerInputParameterfvNV(i, i2, floatBuffer);
    }

    public void glGetFinalCombinerInputParameterfvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetFinalCombinerInputParameterfvNV(i, i2, fArr, i3);
    }

    public void glGetFinalCombinerInputParameterivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetFinalCombinerInputParameterivNV(i, i2, iArr, i3);
    }

    public void glGetFinalCombinerInputParameterivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetFinalCombinerInputParameterivNV(i, i2, intBuffer);
    }

    public void glGetFloatv(int i, float[] fArr, int i2) {
        this.f10342gl.glGetFloatv(i, fArr, i2);
    }

    public void glGetFloatv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glGetFloatv(i, floatBuffer);
    }

    public void glGetFogFuncSGIS(float[] fArr, int i) {
        this.f10342gl.glGetFogFuncSGIS(fArr, i);
    }

    public void glGetFogFuncSGIS(FloatBuffer floatBuffer) {
        this.f10342gl.glGetFogFuncSGIS(floatBuffer);
    }

    public int glGetFragDataLocationEXT(int i, byte[] bArr, int i2) {
        return this.f10342gl.glGetFragDataLocationEXT(i, bArr, i2);
    }

    public int glGetFragDataLocationEXT(int i, ByteBuffer byteBuffer) {
        return this.f10342gl.glGetFragDataLocationEXT(i, byteBuffer);
    }

    public void glGetFragmentLightfvSGIX(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetFragmentLightfvSGIX(i, i2, fArr, i3);
    }

    public void glGetFragmentLightfvSGIX(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetFragmentLightfvSGIX(i, i2, floatBuffer);
    }

    public void glGetFragmentLightivSGIX(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetFragmentLightivSGIX(i, i2, intBuffer);
    }

    public void glGetFragmentLightivSGIX(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetFragmentLightivSGIX(i, i2, iArr, i3);
    }

    public void glGetFragmentMaterialfvSGIX(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetFragmentMaterialfvSGIX(i, i2, fArr, i3);
    }

    public void glGetFragmentMaterialfvSGIX(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetFragmentMaterialfvSGIX(i, i2, floatBuffer);
    }

    public void glGetFragmentMaterialivSGIX(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetFragmentMaterialivSGIX(i, i2, iArr, i3);
    }

    public void glGetFragmentMaterialivSGIX(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetFragmentMaterialivSGIX(i, i2, intBuffer);
    }

    public void glGetFramebufferAttachmentParameterivEXT(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glGetFramebufferAttachmentParameterivEXT(i, i2, i3, iArr, i4);
    }

    public void glGetFramebufferAttachmentParameterivEXT(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glGetFramebufferAttachmentParameterivEXT(i, i2, i3, intBuffer);
    }

    public int glGetHandleARB(int i) {
        return this.f10342gl.glGetHandleARB(i);
    }

    public void glGetHistogram(int i, boolean z, int i2, int i3, Buffer buffer) {
        this.f10342gl.glGetHistogram(i, z, i2, i3, buffer);
    }

    public void glGetHistogram(int i, boolean z, int i2, int i3, long j) {
        this.f10342gl.glGetHistogram(i, z, i2, i3, j);
    }

    public void glGetHistogramParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetHistogramParameterfv(i, i2, fArr, i3);
    }

    public void glGetHistogramParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetHistogramParameterfv(i, i2, floatBuffer);
    }

    public void glGetHistogramParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetHistogramParameteriv(i, i2, iArr, i3);
    }

    public void glGetHistogramParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetHistogramParameteriv(i, i2, intBuffer);
    }

    public void glGetImageTransformParameterfvHP(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetImageTransformParameterfvHP(i, i2, fArr, i3);
    }

    public void glGetImageTransformParameterfvHP(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetImageTransformParameterfvHP(i, i2, floatBuffer);
    }

    public void glGetImageTransformParameterivHP(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetImageTransformParameterivHP(i, i2, iArr, i3);
    }

    public void glGetImageTransformParameterivHP(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetImageTransformParameterivHP(i, i2, intBuffer);
    }

    public void glGetInfoLogARB(int i, int i2, int[] iArr, int i3, byte[] bArr, int i4) {
        this.f10342gl.glGetInfoLogARB(i, i2, iArr, i3, bArr, i4);
    }

    public void glGetInfoLogARB(int i, int i2, IntBuffer intBuffer, ByteBuffer byteBuffer) {
        this.f10342gl.glGetInfoLogARB(i, i2, intBuffer, byteBuffer);
    }

    public int glGetInstrumentsSGIX() {
        return this.f10342gl.glGetInstrumentsSGIX();
    }

    public void glGetIntegerIndexedvEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetIntegerIndexedvEXT(i, i2, intBuffer);
    }

    public void glGetIntegerIndexedvEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetIntegerIndexedvEXT(i, i2, iArr, i3);
    }

    public void glGetIntegerv(int i, int[] iArr, int i2) {
        this.f10342gl.glGetIntegerv(i, iArr, i2);
    }

    public void glGetIntegerv(int i, IntBuffer intBuffer) {
        this.f10342gl.glGetIntegerv(i, intBuffer);
    }

    public void glGetInvariantBooleanvEXT(int i, int i2, byte[] bArr, int i3) {
        this.f10342gl.glGetInvariantBooleanvEXT(i, i2, bArr, i3);
    }

    public void glGetInvariantBooleanvEXT(int i, int i2, ByteBuffer byteBuffer) {
        this.f10342gl.glGetInvariantBooleanvEXT(i, i2, byteBuffer);
    }

    public void glGetInvariantFloatvEXT(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetInvariantFloatvEXT(i, i2, floatBuffer);
    }

    public void glGetInvariantFloatvEXT(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetInvariantFloatvEXT(i, i2, fArr, i3);
    }

    public void glGetInvariantIntegervEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetInvariantIntegervEXT(i, i2, intBuffer);
    }

    public void glGetInvariantIntegervEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetInvariantIntegervEXT(i, i2, iArr, i3);
    }

    public void glGetLightfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetLightfv(i, i2, floatBuffer);
    }

    public void glGetLightfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetLightfv(i, i2, fArr, i3);
    }

    public void glGetLightiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetLightiv(i, i2, iArr, i3);
    }

    public void glGetLightiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetLightiv(i, i2, intBuffer);
    }

    public void glGetListParameterfvSGIX(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetListParameterfvSGIX(i, i2, fArr, i3);
    }

    public void glGetListParameterfvSGIX(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetListParameterfvSGIX(i, i2, floatBuffer);
    }

    public void glGetListParameterivSGIX(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetListParameterivSGIX(i, i2, iArr, i3);
    }

    public void glGetListParameterivSGIX(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetListParameterivSGIX(i, i2, intBuffer);
    }

    public void glGetLocalConstantBooleanvEXT(int i, int i2, byte[] bArr, int i3) {
        this.f10342gl.glGetLocalConstantBooleanvEXT(i, i2, bArr, i3);
    }

    public void glGetLocalConstantBooleanvEXT(int i, int i2, ByteBuffer byteBuffer) {
        this.f10342gl.glGetLocalConstantBooleanvEXT(i, i2, byteBuffer);
    }

    public void glGetLocalConstantFloatvEXT(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetLocalConstantFloatvEXT(i, i2, fArr, i3);
    }

    public void glGetLocalConstantFloatvEXT(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetLocalConstantFloatvEXT(i, i2, floatBuffer);
    }

    public void glGetLocalConstantIntegervEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetLocalConstantIntegervEXT(i, i2, intBuffer);
    }

    public void glGetLocalConstantIntegervEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetLocalConstantIntegervEXT(i, i2, iArr, i3);
    }

    public void glGetMapAttribParameterfvNV(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glGetMapAttribParameterfvNV(i, i2, i3, floatBuffer);
    }

    public void glGetMapAttribParameterfvNV(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glGetMapAttribParameterfvNV(i, i2, i3, fArr, i4);
    }

    public void glGetMapAttribParameterivNV(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glGetMapAttribParameterivNV(i, i2, i3, iArr, i4);
    }

    public void glGetMapAttribParameterivNV(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glGetMapAttribParameterivNV(i, i2, i3, intBuffer);
    }

    public void glGetMapControlPointsNV(int i, int i2, int i3, int i4, int i5, boolean z, Buffer buffer) {
        this.f10342gl.glGetMapControlPointsNV(i, i2, i3, i4, i5, z, buffer);
    }

    public void glGetMapParameterfvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetMapParameterfvNV(i, i2, floatBuffer);
    }

    public void glGetMapParameterfvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetMapParameterfvNV(i, i2, fArr, i3);
    }

    public void glGetMapParameterivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetMapParameterivNV(i, i2, intBuffer);
    }

    public void glGetMapParameterivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetMapParameterivNV(i, i2, iArr, i3);
    }

    public void glGetMapdv(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glGetMapdv(i, i2, dArr, i3);
    }

    public void glGetMapdv(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetMapdv(i, i2, doubleBuffer);
    }

    public void glGetMapfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetMapfv(i, i2, fArr, i3);
    }

    public void glGetMapfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetMapfv(i, i2, floatBuffer);
    }

    public void glGetMapiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetMapiv(i, i2, iArr, i3);
    }

    public void glGetMapiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetMapiv(i, i2, intBuffer);
    }

    public void glGetMaterialfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetMaterialfv(i, i2, floatBuffer);
    }

    public void glGetMaterialfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetMaterialfv(i, i2, fArr, i3);
    }

    public void glGetMaterialiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetMaterialiv(i, i2, intBuffer);
    }

    public void glGetMaterialiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetMaterialiv(i, i2, iArr, i3);
    }

    public void glGetMinmax(int i, boolean z, int i2, int i3, long j) {
        this.f10342gl.glGetMinmax(i, z, i2, i3, j);
    }

    public void glGetMinmax(int i, boolean z, int i2, int i3, Buffer buffer) {
        this.f10342gl.glGetMinmax(i, z, i2, i3, buffer);
    }

    public void glGetMinmaxParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetMinmaxParameterfv(i, i2, fArr, i3);
    }

    public void glGetMinmaxParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetMinmaxParameterfv(i, i2, floatBuffer);
    }

    public void glGetMinmaxParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetMinmaxParameteriv(i, i2, iArr, i3);
    }

    public void glGetMinmaxParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetMinmaxParameteriv(i, i2, intBuffer);
    }

    public void glGetObjectBufferfvATI(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetObjectBufferfvATI(i, i2, floatBuffer);
    }

    public void glGetObjectBufferfvATI(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetObjectBufferfvATI(i, i2, fArr, i3);
    }

    public void glGetObjectBufferivATI(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetObjectBufferivATI(i, i2, intBuffer);
    }

    public void glGetObjectBufferivATI(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetObjectBufferivATI(i, i2, iArr, i3);
    }

    public void glGetObjectParameterfvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetObjectParameterfvARB(i, i2, fArr, i3);
    }

    public void glGetObjectParameterfvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetObjectParameterfvARB(i, i2, floatBuffer);
    }

    public void glGetObjectParameterivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetObjectParameterivARB(i, i2, iArr, i3);
    }

    public void glGetObjectParameterivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetObjectParameterivARB(i, i2, intBuffer);
    }

    public void glGetOcclusionQueryivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetOcclusionQueryivNV(i, i2, iArr, i3);
    }

    public void glGetOcclusionQueryivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetOcclusionQueryivNV(i, i2, intBuffer);
    }

    public void glGetOcclusionQueryuivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetOcclusionQueryuivNV(i, i2, iArr, i3);
    }

    public void glGetOcclusionQueryuivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetOcclusionQueryuivNV(i, i2, intBuffer);
    }

    public void glGetPixelMapfv(int i, long j) {
        this.f10342gl.glGetPixelMapfv(i, j);
    }

    public void glGetPixelMapfv(int i, float[] fArr, int i2) {
        this.f10342gl.glGetPixelMapfv(i, fArr, i2);
    }

    public void glGetPixelMapfv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glGetPixelMapfv(i, floatBuffer);
    }

    public void glGetPixelMapuiv(int i, IntBuffer intBuffer) {
        this.f10342gl.glGetPixelMapuiv(i, intBuffer);
    }

    public void glGetPixelMapuiv(int i, int[] iArr, int i2) {
        this.f10342gl.glGetPixelMapuiv(i, iArr, i2);
    }

    public void glGetPixelMapuiv(int i, long j) {
        this.f10342gl.glGetPixelMapuiv(i, j);
    }

    public void glGetPixelMapusv(int i, long j) {
        this.f10342gl.glGetPixelMapusv(i, j);
    }

    public void glGetPixelMapusv(int i, short[] sArr, int i2) {
        this.f10342gl.glGetPixelMapusv(i, sArr, i2);
    }

    public void glGetPixelMapusv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glGetPixelMapusv(i, shortBuffer);
    }

    public void glGetPixelTexGenParameterfvSGIS(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glGetPixelTexGenParameterfvSGIS(i, floatBuffer);
    }

    public void glGetPixelTexGenParameterfvSGIS(int i, float[] fArr, int i2) {
        this.f10342gl.glGetPixelTexGenParameterfvSGIS(i, fArr, i2);
    }

    public void glGetPixelTexGenParameterivSGIS(int i, IntBuffer intBuffer) {
        this.f10342gl.glGetPixelTexGenParameterivSGIS(i, intBuffer);
    }

    public void glGetPixelTexGenParameterivSGIS(int i, int[] iArr, int i2) {
        this.f10342gl.glGetPixelTexGenParameterivSGIS(i, iArr, i2);
    }

    public void glGetPolygonStipple(long j) {
        this.f10342gl.glGetPolygonStipple(j);
    }

    public void glGetPolygonStipple(byte[] bArr, int i) {
        this.f10342gl.glGetPolygonStipple(bArr, i);
    }

    public void glGetPolygonStipple(ByteBuffer byteBuffer) {
        this.f10342gl.glGetPolygonStipple(byteBuffer);
    }

    public void glGetProgramEnvParameterIivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetProgramEnvParameterIivNV(i, i2, intBuffer);
    }

    public void glGetProgramEnvParameterIivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetProgramEnvParameterIivNV(i, i2, iArr, i3);
    }

    public void glGetProgramEnvParameterIuivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetProgramEnvParameterIuivNV(i, i2, iArr, i3);
    }

    public void glGetProgramEnvParameterIuivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetProgramEnvParameterIuivNV(i, i2, intBuffer);
    }

    public void glGetProgramEnvParameterdvARB(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glGetProgramEnvParameterdvARB(i, i2, dArr, i3);
    }

    public void glGetProgramEnvParameterdvARB(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetProgramEnvParameterdvARB(i, i2, doubleBuffer);
    }

    public void glGetProgramEnvParameterfvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetProgramEnvParameterfvARB(i, i2, floatBuffer);
    }

    public void glGetProgramEnvParameterfvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetProgramEnvParameterfvARB(i, i2, fArr, i3);
    }

    public void glGetProgramInfoLog(int i, int i2, IntBuffer intBuffer, ByteBuffer byteBuffer) {
        this.f10342gl.glGetProgramInfoLog(i, i2, intBuffer, byteBuffer);
    }

    public void glGetProgramInfoLog(int i, int i2, int[] iArr, int i3, byte[] bArr, int i4) {
        this.f10342gl.glGetProgramInfoLog(i, i2, iArr, i3, bArr, i4);
    }

    public void glGetProgramLocalParameterIivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetProgramLocalParameterIivNV(i, i2, iArr, i3);
    }

    public void glGetProgramLocalParameterIivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetProgramLocalParameterIivNV(i, i2, intBuffer);
    }

    public void glGetProgramLocalParameterIuivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetProgramLocalParameterIuivNV(i, i2, iArr, i3);
    }

    public void glGetProgramLocalParameterIuivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetProgramLocalParameterIuivNV(i, i2, intBuffer);
    }

    public void glGetProgramLocalParameterdvARB(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glGetProgramLocalParameterdvARB(i, i2, dArr, i3);
    }

    public void glGetProgramLocalParameterdvARB(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetProgramLocalParameterdvARB(i, i2, doubleBuffer);
    }

    public void glGetProgramLocalParameterfvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetProgramLocalParameterfvARB(i, i2, fArr, i3);
    }

    public void glGetProgramLocalParameterfvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetProgramLocalParameterfvARB(i, i2, floatBuffer);
    }

    public void glGetProgramNamedParameterdvNV(int i, int i2, String str, double[] dArr, int i3) {
        this.f10342gl.glGetProgramNamedParameterdvNV(i, i2, str, dArr, i3);
    }

    public void glGetProgramNamedParameterdvNV(int i, int i2, String str, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetProgramNamedParameterdvNV(i, i2, str, doubleBuffer);
    }

    public void glGetProgramNamedParameterfvNV(int i, int i2, String str, float[] fArr, int i3) {
        this.f10342gl.glGetProgramNamedParameterfvNV(i, i2, str, fArr, i3);
    }

    public void glGetProgramNamedParameterfvNV(int i, int i2, String str, FloatBuffer floatBuffer) {
        this.f10342gl.glGetProgramNamedParameterfvNV(i, i2, str, floatBuffer);
    }

    public void glGetProgramParameterdvNV(int i, int i2, int i3, double[] dArr, int i4) {
        this.f10342gl.glGetProgramParameterdvNV(i, i2, i3, dArr, i4);
    }

    public void glGetProgramParameterdvNV(int i, int i2, int i3, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetProgramParameterdvNV(i, i2, i3, doubleBuffer);
    }

    public void glGetProgramParameterfvNV(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glGetProgramParameterfvNV(i, i2, i3, floatBuffer);
    }

    public void glGetProgramParameterfvNV(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glGetProgramParameterfvNV(i, i2, i3, fArr, i4);
    }

    public void glGetProgramStringARB(int i, int i2, Buffer buffer) {
        this.f10342gl.glGetProgramStringARB(i, i2, buffer);
    }

    public void glGetProgramStringNV(int i, int i2, byte[] bArr, int i3) {
        this.f10342gl.glGetProgramStringNV(i, i2, bArr, i3);
    }

    public void glGetProgramStringNV(int i, int i2, ByteBuffer byteBuffer) {
        this.f10342gl.glGetProgramStringNV(i, i2, byteBuffer);
    }

    public void glGetProgramiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetProgramiv(i, i2, iArr, i3);
    }

    public void glGetProgramiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetProgramiv(i, i2, intBuffer);
    }

    public void glGetProgramivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetProgramivARB(i, i2, iArr, i3);
    }

    public void glGetProgramivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetProgramivARB(i, i2, intBuffer);
    }

    public void glGetProgramivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetProgramivNV(i, i2, intBuffer);
    }

    public void glGetProgramivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetProgramivNV(i, i2, iArr, i3);
    }

    public void glGetQueryObjecti64vEXT(int i, int i2, LongBuffer longBuffer) {
        this.f10342gl.glGetQueryObjecti64vEXT(i, i2, longBuffer);
    }

    public void glGetQueryObjecti64vEXT(int i, int i2, long[] jArr, int i3) {
        this.f10342gl.glGetQueryObjecti64vEXT(i, i2, jArr, i3);
    }

    public void glGetQueryObjectiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetQueryObjectiv(i, i2, intBuffer);
    }

    public void glGetQueryObjectiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetQueryObjectiv(i, i2, iArr, i3);
    }

    public void glGetQueryObjectivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetQueryObjectivARB(i, i2, iArr, i3);
    }

    public void glGetQueryObjectivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetQueryObjectivARB(i, i2, intBuffer);
    }

    public void glGetQueryObjectui64vEXT(int i, int i2, long[] jArr, int i3) {
        this.f10342gl.glGetQueryObjectui64vEXT(i, i2, jArr, i3);
    }

    public void glGetQueryObjectui64vEXT(int i, int i2, LongBuffer longBuffer) {
        this.f10342gl.glGetQueryObjectui64vEXT(i, i2, longBuffer);
    }

    public void glGetQueryObjectuiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetQueryObjectuiv(i, i2, iArr, i3);
    }

    public void glGetQueryObjectuiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetQueryObjectuiv(i, i2, intBuffer);
    }

    public void glGetQueryObjectuivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetQueryObjectuivARB(i, i2, iArr, i3);
    }

    public void glGetQueryObjectuivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetQueryObjectuivARB(i, i2, intBuffer);
    }

    public void glGetQueryiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetQueryiv(i, i2, iArr, i3);
    }

    public void glGetQueryiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetQueryiv(i, i2, intBuffer);
    }

    public void glGetQueryivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetQueryivARB(i, i2, iArr, i3);
    }

    public void glGetQueryivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetQueryivARB(i, i2, intBuffer);
    }

    public void glGetRenderbufferParameterivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetRenderbufferParameterivEXT(i, i2, iArr, i3);
    }

    public void glGetRenderbufferParameterivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetRenderbufferParameterivEXT(i, i2, intBuffer);
    }

    public void glGetSeparableFilter(int i, int i2, int i3, Buffer buffer, Buffer buffer2, Buffer buffer3) {
        this.f10342gl.glGetSeparableFilter(i, i2, i3, buffer, buffer2, buffer3);
    }

    public void glGetSeparableFilter(int i, int i2, int i3, long j, long j2, long j3) {
        this.f10342gl.glGetSeparableFilter(i, i2, i3, j, j2, j3);
    }

    public void glGetShaderInfoLog(int i, int i2, int[] iArr, int i3, byte[] bArr, int i4) {
        this.f10342gl.glGetShaderInfoLog(i, i2, iArr, i3, bArr, i4);
    }

    public void glGetShaderInfoLog(int i, int i2, IntBuffer intBuffer, ByteBuffer byteBuffer) {
        this.f10342gl.glGetShaderInfoLog(i, i2, intBuffer, byteBuffer);
    }

    public void glGetShaderSource(int i, int i2, IntBuffer intBuffer, ByteBuffer byteBuffer) {
        this.f10342gl.glGetShaderSource(i, i2, intBuffer, byteBuffer);
    }

    public void glGetShaderSource(int i, int i2, int[] iArr, int i3, byte[] bArr, int i4) {
        this.f10342gl.glGetShaderSource(i, i2, iArr, i3, bArr, i4);
    }

    public void glGetShaderSourceARB(int i, int i2, int[] iArr, int i3, byte[] bArr, int i4) {
        this.f10342gl.glGetShaderSourceARB(i, i2, iArr, i3, bArr, i4);
    }

    public void glGetShaderSourceARB(int i, int i2, IntBuffer intBuffer, ByteBuffer byteBuffer) {
        this.f10342gl.glGetShaderSourceARB(i, i2, intBuffer, byteBuffer);
    }

    public void glGetShaderiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetShaderiv(i, i2, intBuffer);
    }

    public void glGetShaderiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetShaderiv(i, i2, iArr, i3);
    }

    public void glGetSharpenTexFuncSGIS(int i, float[] fArr, int i2) {
        this.f10342gl.glGetSharpenTexFuncSGIS(i, fArr, i2);
    }

    public void glGetSharpenTexFuncSGIS(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glGetSharpenTexFuncSGIS(i, floatBuffer);
    }

    public String glGetString(int i) {
        return this.f10342gl.glGetString(i);
    }

    public void glGetTexBumpParameterfvATI(int i, float[] fArr, int i2) {
        this.f10342gl.glGetTexBumpParameterfvATI(i, fArr, i2);
    }

    public void glGetTexBumpParameterfvATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glGetTexBumpParameterfvATI(i, floatBuffer);
    }

    public void glGetTexBumpParameterivATI(int i, int[] iArr, int i2) {
        this.f10342gl.glGetTexBumpParameterivATI(i, iArr, i2);
    }

    public void glGetTexBumpParameterivATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glGetTexBumpParameterivATI(i, intBuffer);
    }

    public void glGetTexEnvfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetTexEnvfv(i, i2, floatBuffer);
    }

    public void glGetTexEnvfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetTexEnvfv(i, i2, fArr, i3);
    }

    public void glGetTexEnviv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetTexEnviv(i, i2, intBuffer);
    }

    public void glGetTexEnviv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetTexEnviv(i, i2, iArr, i3);
    }

    public void glGetTexFilterFuncSGIS(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetTexFilterFuncSGIS(i, i2, floatBuffer);
    }

    public void glGetTexFilterFuncSGIS(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetTexFilterFuncSGIS(i, i2, fArr, i3);
    }

    public void glGetTexGendv(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glGetTexGendv(i, i2, dArr, i3);
    }

    public void glGetTexGendv(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetTexGendv(i, i2, doubleBuffer);
    }

    public void glGetTexGenfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetTexGenfv(i, i2, fArr, i3);
    }

    public void glGetTexGenfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetTexGenfv(i, i2, floatBuffer);
    }

    public void glGetTexGeniv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetTexGeniv(i, i2, iArr, i3);
    }

    public void glGetTexGeniv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetTexGeniv(i, i2, intBuffer);
    }

    public void glGetTexImage(int i, int i2, int i3, int i4, Buffer buffer) {
        this.f10342gl.glGetTexImage(i, i2, i3, i4, buffer);
    }

    public void glGetTexImage(int i, int i2, int i3, int i4, long j) {
        this.f10342gl.glGetTexImage(i, i2, i3, i4, j);
    }

    public void glGetTexLevelParameterfv(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glGetTexLevelParameterfv(i, i2, i3, fArr, i4);
    }

    public void glGetTexLevelParameterfv(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glGetTexLevelParameterfv(i, i2, i3, floatBuffer);
    }

    public void glGetTexLevelParameteriv(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glGetTexLevelParameteriv(i, i2, i3, intBuffer);
    }

    public void glGetTexLevelParameteriv(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glGetTexLevelParameteriv(i, i2, i3, iArr, i4);
    }

    public void glGetTexParameterIivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetTexParameterIivEXT(i, i2, iArr, i3);
    }

    public void glGetTexParameterIivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetTexParameterIivEXT(i, i2, intBuffer);
    }

    public void glGetTexParameterIuivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetTexParameterIuivEXT(i, i2, intBuffer);
    }

    public void glGetTexParameterIuivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetTexParameterIuivEXT(i, i2, iArr, i3);
    }

    public void glGetTexParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetTexParameterfv(i, i2, fArr, i3);
    }

    public void glGetTexParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetTexParameterfv(i, i2, floatBuffer);
    }

    public void glGetTexParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetTexParameteriv(i, i2, iArr, i3);
    }

    public void glGetTexParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetTexParameteriv(i, i2, intBuffer);
    }

    public void glGetTrackMatrixivNV(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glGetTrackMatrixivNV(i, i2, i3, iArr, i4);
    }

    public void glGetTrackMatrixivNV(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glGetTrackMatrixivNV(i, i2, i3, intBuffer);
    }

    public void glGetTransformFeedbackVaryingNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetTransformFeedbackVaryingNV(i, i2, iArr, i3);
    }

    public void glGetTransformFeedbackVaryingNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetTransformFeedbackVaryingNV(i, i2, intBuffer);
    }

    public int glGetUniformBufferSizeEXT(int i, int i2) {
        return this.f10342gl.glGetUniformBufferSizeEXT(i, i2);
    }

    public int glGetUniformLocation(int i, String str) {
        return this.f10342gl.glGetUniformLocation(i, str);
    }

    public int glGetUniformLocationARB(int i, String str) {
        return this.f10342gl.glGetUniformLocationARB(i, str);
    }

    public int glGetUniformOffsetEXT(int i, int i2) {
        return this.f10342gl.glGetUniformOffsetEXT(i, i2);
    }

    public void glGetUniformfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetUniformfv(i, i2, fArr, i3);
    }

    public void glGetUniformfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetUniformfv(i, i2, floatBuffer);
    }

    public void glGetUniformfvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetUniformfvARB(i, i2, fArr, i3);
    }

    public void glGetUniformfvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetUniformfvARB(i, i2, floatBuffer);
    }

    public void glGetUniformiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetUniformiv(i, i2, iArr, i3);
    }

    public void glGetUniformiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetUniformiv(i, i2, intBuffer);
    }

    public void glGetUniformivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetUniformivARB(i, i2, iArr, i3);
    }

    public void glGetUniformivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetUniformivARB(i, i2, intBuffer);
    }

    public void glGetUniformuivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetUniformuivEXT(i, i2, iArr, i3);
    }

    public void glGetUniformuivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetUniformuivEXT(i, i2, intBuffer);
    }

    public void glGetVariantArrayObjectfvATI(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetVariantArrayObjectfvATI(i, i2, floatBuffer);
    }

    public void glGetVariantArrayObjectfvATI(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetVariantArrayObjectfvATI(i, i2, fArr, i3);
    }

    public void glGetVariantArrayObjectivATI(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVariantArrayObjectivATI(i, i2, iArr, i3);
    }

    public void glGetVariantArrayObjectivATI(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVariantArrayObjectivATI(i, i2, intBuffer);
    }

    public void glGetVariantBooleanvEXT(int i, int i2, ByteBuffer byteBuffer) {
        this.f10342gl.glGetVariantBooleanvEXT(i, i2, byteBuffer);
    }

    public void glGetVariantBooleanvEXT(int i, int i2, byte[] bArr, int i3) {
        this.f10342gl.glGetVariantBooleanvEXT(i, i2, bArr, i3);
    }

    public void glGetVariantFloatvEXT(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetVariantFloatvEXT(i, i2, fArr, i3);
    }

    public void glGetVariantFloatvEXT(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetVariantFloatvEXT(i, i2, floatBuffer);
    }

    public void glGetVariantIntegervEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVariantIntegervEXT(i, i2, intBuffer);
    }

    public void glGetVariantIntegervEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVariantIntegervEXT(i, i2, iArr, i3);
    }

    public int glGetVaryingLocationNV(int i, byte[] bArr, int i2) {
        return this.f10342gl.glGetVaryingLocationNV(i, bArr, i2);
    }

    public int glGetVaryingLocationNV(int i, ByteBuffer byteBuffer) {
        return this.f10342gl.glGetVaryingLocationNV(i, byteBuffer);
    }

    public void glGetVertexAttribArrayObjectfvATI(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetVertexAttribArrayObjectfvATI(i, i2, fArr, i3);
    }

    public void glGetVertexAttribArrayObjectfvATI(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetVertexAttribArrayObjectfvATI(i, i2, floatBuffer);
    }

    public void glGetVertexAttribArrayObjectivATI(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVertexAttribArrayObjectivATI(i, i2, iArr, i3);
    }

    public void glGetVertexAttribArrayObjectivATI(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVertexAttribArrayObjectivATI(i, i2, intBuffer);
    }

    public void glGetVertexAttribIivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVertexAttribIivEXT(i, i2, iArr, i3);
    }

    public void glGetVertexAttribIivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVertexAttribIivEXT(i, i2, intBuffer);
    }

    public void glGetVertexAttribIuivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVertexAttribIuivEXT(i, i2, iArr, i3);
    }

    public void glGetVertexAttribIuivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVertexAttribIuivEXT(i, i2, intBuffer);
    }

    public void glGetVertexAttribdv(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetVertexAttribdv(i, i2, doubleBuffer);
    }

    public void glGetVertexAttribdv(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glGetVertexAttribdv(i, i2, dArr, i3);
    }

    public void glGetVertexAttribdvARB(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetVertexAttribdvARB(i, i2, doubleBuffer);
    }

    public void glGetVertexAttribdvARB(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glGetVertexAttribdvARB(i, i2, dArr, i3);
    }

    public void glGetVertexAttribdvNV(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glGetVertexAttribdvNV(i, i2, dArr, i3);
    }

    public void glGetVertexAttribdvNV(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glGetVertexAttribdvNV(i, i2, doubleBuffer);
    }

    public void glGetVertexAttribfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetVertexAttribfv(i, i2, fArr, i3);
    }

    public void glGetVertexAttribfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetVertexAttribfv(i, i2, floatBuffer);
    }

    public void glGetVertexAttribfvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetVertexAttribfvARB(i, i2, fArr, i3);
    }

    public void glGetVertexAttribfvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetVertexAttribfvARB(i, i2, floatBuffer);
    }

    public void glGetVertexAttribfvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glGetVertexAttribfvNV(i, i2, fArr, i3);
    }

    public void glGetVertexAttribfvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glGetVertexAttribfvNV(i, i2, floatBuffer);
    }

    public void glGetVertexAttribiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVertexAttribiv(i, i2, iArr, i3);
    }

    public void glGetVertexAttribiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVertexAttribiv(i, i2, intBuffer);
    }

    public void glGetVertexAttribivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVertexAttribivARB(i, i2, intBuffer);
    }

    public void glGetVertexAttribivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVertexAttribivARB(i, i2, iArr, i3);
    }

    public void glGetVertexAttribivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glGetVertexAttribivNV(i, i2, iArr, i3);
    }

    public void glGetVertexAttribivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glGetVertexAttribivNV(i, i2, intBuffer);
    }

    public void glGlobalAlphaFactorbSUN(byte b) {
        this.f10342gl.glGlobalAlphaFactorbSUN(b);
    }

    public void glGlobalAlphaFactordSUN(double d) {
        this.f10342gl.glGlobalAlphaFactordSUN(d);
    }

    public void glGlobalAlphaFactorfSUN(float f) {
        this.f10342gl.glGlobalAlphaFactorfSUN(f);
    }

    public void glGlobalAlphaFactoriSUN(int i) {
        this.f10342gl.glGlobalAlphaFactoriSUN(i);
    }

    public void glGlobalAlphaFactorsSUN(short s) {
        this.f10342gl.glGlobalAlphaFactorsSUN(s);
    }

    public void glGlobalAlphaFactorubSUN(byte b) {
        this.f10342gl.glGlobalAlphaFactorubSUN(b);
    }

    public void glGlobalAlphaFactoruiSUN(int i) {
        this.f10342gl.glGlobalAlphaFactoruiSUN(i);
    }

    public void glGlobalAlphaFactorusSUN(short s) {
        this.f10342gl.glGlobalAlphaFactorusSUN(s);
    }

    public void glHint(int i, int i2) {
        this.f10342gl.glHint(i, i2);
    }

    public void glHintPGI(int i, int i2) {
        this.f10342gl.glHintPGI(i, i2);
    }

    public void glHistogram(int i, int i2, int i3, boolean z) {
        this.f10342gl.glHistogram(i, i2, i3, z);
    }

    public void glIglooInterfaceSGIX(int i, Buffer buffer) {
        this.f10342gl.glIglooInterfaceSGIX(i, buffer);
    }

    public void glImageTransformParameterfHP(int i, int i2, float f) {
        this.f10342gl.glImageTransformParameterfHP(i, i2, f);
    }

    public void glImageTransformParameterfvHP(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glImageTransformParameterfvHP(i, i2, floatBuffer);
    }

    public void glImageTransformParameterfvHP(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glImageTransformParameterfvHP(i, i2, fArr, i3);
    }

    public void glImageTransformParameteriHP(int i, int i2, int i3) {
        this.f10342gl.glImageTransformParameteriHP(i, i2, i3);
    }

    public void glImageTransformParameterivHP(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glImageTransformParameterivHP(i, i2, iArr, i3);
    }

    public void glImageTransformParameterivHP(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glImageTransformParameterivHP(i, i2, intBuffer);
    }

    public void glIndexFuncEXT(int i, float f) {
        this.f10342gl.glIndexFuncEXT(i, f);
    }

    public void glIndexMask(int i) {
        this.f10342gl.glIndexMask(i);
    }

    public void glIndexMaterialEXT(int i, int i2) {
        this.f10342gl.glIndexMaterialEXT(i, i2);
    }

    public void glIndexPointer(int i, int i2, Buffer buffer) {
        this.f10342gl.glIndexPointer(i, i2, buffer);
    }

    public void glIndexd(double d) {
        this.f10342gl.glIndexd(d);
    }

    public void glIndexdv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glIndexdv(doubleBuffer);
    }

    public void glIndexdv(double[] dArr, int i) {
        this.f10342gl.glIndexdv(dArr, i);
    }

    public void glIndexf(float f) {
        this.f10342gl.glIndexf(f);
    }

    public void glIndexfv(FloatBuffer floatBuffer) {
        this.f10342gl.glIndexfv(floatBuffer);
    }

    public void glIndexfv(float[] fArr, int i) {
        this.f10342gl.glIndexfv(fArr, i);
    }

    public void glIndexi(int i) {
        this.f10342gl.glIndexi(i);
    }

    public void glIndexiv(IntBuffer intBuffer) {
        this.f10342gl.glIndexiv(intBuffer);
    }

    public void glIndexiv(int[] iArr, int i) {
        this.f10342gl.glIndexiv(iArr, i);
    }

    public void glIndexs(short s) {
        this.f10342gl.glIndexs(s);
    }

    public void glIndexsv(short[] sArr, int i) {
        this.f10342gl.glIndexsv(sArr, i);
    }

    public void glIndexsv(ShortBuffer shortBuffer) {
        this.f10342gl.glIndexsv(shortBuffer);
    }

    public void glIndexub(byte b) {
        this.f10342gl.glIndexub(b);
    }

    public void glIndexubv(byte[] bArr, int i) {
        this.f10342gl.glIndexubv(bArr, i);
    }

    public void glIndexubv(ByteBuffer byteBuffer) {
        this.f10342gl.glIndexubv(byteBuffer);
    }

    public void glInitNames() {
        this.f10342gl.glInitNames();
    }

    public void glInsertComponentEXT(int i, int i2, int i3) {
        this.f10342gl.glInsertComponentEXT(i, i2, i3);
    }

    public void glInstrumentsBufferSGIX(int i, int[] iArr, int i2) {
        this.f10342gl.glInstrumentsBufferSGIX(i, iArr, i2);
    }

    public void glInstrumentsBufferSGIX(int i, IntBuffer intBuffer) {
        this.f10342gl.glInstrumentsBufferSGIX(i, intBuffer);
    }

    public void glInterleavedArrays(int i, int i2, Buffer buffer) {
        this.f10342gl.glInterleavedArrays(i, i2, buffer);
    }

    public void glInterleavedArrays(int i, int i2, long j) {
        this.f10342gl.glInterleavedArrays(i, i2, j);
    }

    public boolean glIsAsyncMarkerSGIX(int i) {
        return this.f10342gl.glIsAsyncMarkerSGIX(i);
    }

    public boolean glIsBuffer(int i) {
        return this.f10342gl.glIsBuffer(i);
    }

    public boolean glIsBufferARB(int i) {
        return this.f10342gl.glIsBufferARB(i);
    }

    public boolean glIsEnabled(int i) {
        return this.f10342gl.glIsEnabled(i);
    }

    public boolean glIsEnabledIndexedEXT(int i, int i2) {
        return this.f10342gl.glIsEnabledIndexedEXT(i, i2);
    }

    public boolean glIsFenceAPPLE(int i) {
        return this.f10342gl.glIsFenceAPPLE(i);
    }

    public boolean glIsFenceNV(int i) {
        return this.f10342gl.glIsFenceNV(i);
    }

    public boolean glIsFramebufferEXT(int i) {
        return this.f10342gl.glIsFramebufferEXT(i);
    }

    public boolean glIsList(int i) {
        return this.f10342gl.glIsList(i);
    }

    public boolean glIsObjectBufferATI(int i) {
        return this.f10342gl.glIsObjectBufferATI(i);
    }

    public boolean glIsOcclusionQueryNV(int i) {
        return this.f10342gl.glIsOcclusionQueryNV(i);
    }

    public boolean glIsProgram(int i) {
        return this.f10342gl.glIsProgram(i);
    }

    public boolean glIsProgramARB(int i) {
        return this.f10342gl.glIsProgramARB(i);
    }

    public boolean glIsProgramNV(int i) {
        return this.f10342gl.glIsProgramNV(i);
    }

    public boolean glIsQuery(int i) {
        return this.f10342gl.glIsQuery(i);
    }

    public boolean glIsQueryARB(int i) {
        return this.f10342gl.glIsQueryARB(i);
    }

    public boolean glIsRenderbufferEXT(int i) {
        return this.f10342gl.glIsRenderbufferEXT(i);
    }

    public boolean glIsShader(int i) {
        return this.f10342gl.glIsShader(i);
    }

    public boolean glIsTexture(int i) {
        return this.f10342gl.glIsTexture(i);
    }

    public boolean glIsVariantEnabledEXT(int i, int i2) {
        return this.f10342gl.glIsVariantEnabledEXT(i, i2);
    }

    public boolean glIsVertexArrayAPPLE(int i) {
        return this.f10342gl.glIsVertexArrayAPPLE(i);
    }

    public boolean glIsVertexAttribEnabledAPPLE(int i, int i2) {
        return this.f10342gl.glIsVertexAttribEnabledAPPLE(i, i2);
    }

    public void glLightEnviSGIX(int i, int i2) {
        this.f10342gl.glLightEnviSGIX(i, i2);
    }

    public void glLightModelf(int i, float f) {
        this.f10342gl.glLightModelf(i, f);
    }

    public void glLightModelfv(int i, float[] fArr, int i2) {
        this.f10342gl.glLightModelfv(i, fArr, i2);
    }

    public void glLightModelfv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glLightModelfv(i, floatBuffer);
    }

    public void glLightModeli(int i, int i2) {
        this.f10342gl.glLightModeli(i, i2);
    }

    public void glLightModeliv(int i, IntBuffer intBuffer) {
        this.f10342gl.glLightModeliv(i, intBuffer);
    }

    public void glLightModeliv(int i, int[] iArr, int i2) {
        this.f10342gl.glLightModeliv(i, iArr, i2);
    }

    public void glLightf(int i, int i2, float f) {
        this.f10342gl.glLightf(i, i2, f);
    }

    public void glLightfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glLightfv(i, i2, floatBuffer);
    }

    public void glLightfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glLightfv(i, i2, fArr, i3);
    }

    public void glLighti(int i, int i2, int i3) {
        this.f10342gl.glLighti(i, i2, i3);
    }

    public void glLightiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glLightiv(i, i2, iArr, i3);
    }

    public void glLightiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glLightiv(i, i2, intBuffer);
    }

    public void glLineStipple(int i, short s) {
        this.f10342gl.glLineStipple(i, s);
    }

    public void glLineWidth(float f) {
        this.f10342gl.glLineWidth(f);
    }

    public void glLinkProgram(int i) {
        this.f10342gl.glLinkProgram(i);
    }

    public void glLinkProgramARB(int i) {
        this.f10342gl.glLinkProgramARB(i);
    }

    public void glListBase(int i) {
        this.f10342gl.glListBase(i);
    }

    public void glListParameterfSGIX(int i, int i2, float f) {
        this.f10342gl.glListParameterfSGIX(i, i2, f);
    }

    public void glListParameterfvSGIX(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glListParameterfvSGIX(i, i2, fArr, i3);
    }

    public void glListParameterfvSGIX(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glListParameterfvSGIX(i, i2, floatBuffer);
    }

    public void glListParameteriSGIX(int i, int i2, int i3) {
        this.f10342gl.glListParameteriSGIX(i, i2, i3);
    }

    public void glListParameterivSGIX(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glListParameterivSGIX(i, i2, intBuffer);
    }

    public void glListParameterivSGIX(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glListParameterivSGIX(i, i2, iArr, i3);
    }

    public void glLoadIdentity() {
        this.f10342gl.glLoadIdentity();
    }

    public void glLoadIdentityDeformationMapSGIX(int i) {
        this.f10342gl.glLoadIdentityDeformationMapSGIX(i);
    }

    public void glLoadMatrixd(double[] dArr, int i) {
        this.f10342gl.glLoadMatrixd(dArr, i);
    }

    public void glLoadMatrixd(DoubleBuffer doubleBuffer) {
        this.f10342gl.glLoadMatrixd(doubleBuffer);
    }

    public void glLoadMatrixf(float[] fArr, int i) {
        this.f10342gl.glLoadMatrixf(fArr, i);
    }

    public void glLoadMatrixf(FloatBuffer floatBuffer) {
        this.f10342gl.glLoadMatrixf(floatBuffer);
    }

    public void glLoadName(int i) {
        this.f10342gl.glLoadName(i);
    }

    public void glLoadProgramNV(int i, int i2, int i3, String str) {
        this.f10342gl.glLoadProgramNV(i, i2, i3, str);
    }

    public void glLoadTransposeMatrixd(double[] dArr, int i) {
        this.f10342gl.glLoadTransposeMatrixd(dArr, i);
    }

    public void glLoadTransposeMatrixd(DoubleBuffer doubleBuffer) {
        this.f10342gl.glLoadTransposeMatrixd(doubleBuffer);
    }

    public void glLoadTransposeMatrixf(FloatBuffer floatBuffer) {
        this.f10342gl.glLoadTransposeMatrixf(floatBuffer);
    }

    public void glLoadTransposeMatrixf(float[] fArr, int i) {
        this.f10342gl.glLoadTransposeMatrixf(fArr, i);
    }

    public void glLockArraysEXT(int i, int i2) {
        this.f10342gl.glLockArraysEXT(i, i2);
    }

    public void glLogicOp(int i) {
        this.f10342gl.glLogicOp(i);
    }

    public void glMap1d(int i, double d, double d2, int i2, int i3, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMap1d(i, d, d2, i2, i3, doubleBuffer);
    }

    public void glMap1d(int i, double d, double d2, int i2, int i3, double[] dArr, int i4) {
        this.f10342gl.glMap1d(i, d, d2, i2, i3, dArr, i4);
    }

    public void glMap1f(int i, float f, float f2, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glMap1f(i, f, f2, i2, i3, fArr, i4);
    }

    public void glMap1f(int i, float f, float f2, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glMap1f(i, f, f2, i2, i3, floatBuffer);
    }

    public void glMap2d(int i, double d, double d2, int i2, int i3, double d3, double d4, int i4, int i5, double[] dArr, int i6) {
        this.f10342gl.glMap2d(i, d, d2, i2, i3, d3, d4, i4, i5, dArr, i6);
    }

    public void glMap2d(int i, double d, double d2, int i2, int i3, double d3, double d4, int i4, int i5, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMap2d(i, d, d2, i2, i3, d3, d4, i4, i5, doubleBuffer);
    }

    public void glMap2f(int i, float f, float f2, int i2, int i3, float f3, float f4, int i4, int i5, FloatBuffer floatBuffer) {
        this.f10342gl.glMap2f(i, f, f2, i2, i3, f3, f4, i4, i5, floatBuffer);
    }

    public void glMap2f(int i, float f, float f2, int i2, int i3, float f3, float f4, int i4, int i5, float[] fArr, int i6) {
        this.f10342gl.glMap2f(i, f, f2, i2, i3, f3, f4, i4, i5, fArr, i6);
    }

    public ByteBuffer glMapBuffer(int i, int i2) {
        return this.f10342gl.glMapBuffer(i, i2);
    }

    public ByteBuffer glMapBufferARB(int i, int i2) {
        return this.f10342gl.glMapBufferARB(i, i2);
    }

    public void glMapControlPointsNV(int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z, Buffer buffer) {
        this.f10342gl.glMapControlPointsNV(i, i2, i3, i4, i5, i6, i7, z, buffer);
    }

    public void glMapGrid1d(int i, double d, double d2) {
        this.f10342gl.glMapGrid1d(i, d, d2);
    }

    public void glMapGrid1f(int i, float f, float f2) {
        this.f10342gl.glMapGrid1f(i, f, f2);
    }

    public void glMapGrid2d(int i, double d, double d2, int i2, double d3, double d4) {
        this.f10342gl.glMapGrid2d(i, d, d2, i2, d3, d4);
    }

    public void glMapGrid2f(int i, float f, float f2, int i2, float f3, float f4) {
        this.f10342gl.glMapGrid2f(i, f, f2, i2, f3, f4);
    }

    public void glMapParameterfvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glMapParameterfvNV(i, i2, floatBuffer);
    }

    public void glMapParameterfvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glMapParameterfvNV(i, i2, fArr, i3);
    }

    public void glMapParameterivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glMapParameterivNV(i, i2, iArr, i3);
    }

    public void glMapParameterivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glMapParameterivNV(i, i2, intBuffer);
    }

    public void glMapVertexAttrib1dAPPLE(int i, int i2, double d, double d2, int i3, int i4, double[] dArr, int i5) {
        this.f10342gl.glMapVertexAttrib1dAPPLE(i, i2, d, d2, i3, i4, dArr, i5);
    }

    public void glMapVertexAttrib1dAPPLE(int i, int i2, double d, double d2, int i3, int i4, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMapVertexAttrib1dAPPLE(i, i2, d, d2, i3, i4, doubleBuffer);
    }

    public void glMapVertexAttrib1fAPPLE(int i, int i2, float f, float f2, int i3, int i4, FloatBuffer floatBuffer) {
        this.f10342gl.glMapVertexAttrib1fAPPLE(i, i2, f, f2, i3, i4, floatBuffer);
    }

    public void glMapVertexAttrib1fAPPLE(int i, int i2, float f, float f2, int i3, int i4, float[] fArr, int i5) {
        this.f10342gl.glMapVertexAttrib1fAPPLE(i, i2, f, f2, i3, i4, fArr, i5);
    }

    public void glMapVertexAttrib2dAPPLE(int i, int i2, double d, double d2, int i3, int i4, double d3, double d4, int i5, int i6, double[] dArr, int i7) {
        this.f10342gl.glMapVertexAttrib2dAPPLE(i, i2, d, d2, i3, i4, d3, d4, i5, i6, dArr, i7);
    }

    public void glMapVertexAttrib2dAPPLE(int i, int i2, double d, double d2, int i3, int i4, double d3, double d4, int i5, int i6, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMapVertexAttrib2dAPPLE(i, i2, d, d2, i3, i4, d3, d4, i5, i6, doubleBuffer);
    }

    public void glMapVertexAttrib2fAPPLE(int i, int i2, float f, float f2, int i3, int i4, float f3, float f4, int i5, int i6, FloatBuffer floatBuffer) {
        this.f10342gl.glMapVertexAttrib2fAPPLE(i, i2, f, f2, i3, i4, f3, f4, i5, i6, floatBuffer);
    }

    public void glMapVertexAttrib2fAPPLE(int i, int i2, float f, float f2, int i3, int i4, float f3, float f4, int i5, int i6, float[] fArr, int i7) {
        this.f10342gl.glMapVertexAttrib2fAPPLE(i, i2, f, f2, i3, i4, f3, f4, i5, i6, fArr, i7);
    }

    public void glMaterialf(int i, int i2, float f) {
        this.f10342gl.glMaterialf(i, i2, f);
    }

    public void glMaterialfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glMaterialfv(i, i2, floatBuffer);
    }

    public void glMaterialfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glMaterialfv(i, i2, fArr, i3);
    }

    public void glMateriali(int i, int i2, int i3) {
        this.f10342gl.glMateriali(i, i2, i3);
    }

    public void glMaterialiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glMaterialiv(i, i2, iArr, i3);
    }

    public void glMaterialiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glMaterialiv(i, i2, intBuffer);
    }

    public void glMatrixIndexPointerARB(int i, int i2, int i3, long j) {
        this.f10342gl.glMatrixIndexPointerARB(i, i2, i3, j);
    }

    public void glMatrixIndexPointerARB(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glMatrixIndexPointerARB(i, i2, i3, buffer);
    }

    public void glMatrixIndexubvARB(int i, byte[] bArr, int i2) {
        this.f10342gl.glMatrixIndexubvARB(i, bArr, i2);
    }

    public void glMatrixIndexubvARB(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glMatrixIndexubvARB(i, byteBuffer);
    }

    public void glMatrixIndexuivARB(int i, int[] iArr, int i2) {
        this.f10342gl.glMatrixIndexuivARB(i, iArr, i2);
    }

    public void glMatrixIndexuivARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glMatrixIndexuivARB(i, intBuffer);
    }

    public void glMatrixIndexusvARB(int i, short[] sArr, int i2) {
        this.f10342gl.glMatrixIndexusvARB(i, sArr, i2);
    }

    public void glMatrixIndexusvARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMatrixIndexusvARB(i, shortBuffer);
    }

    public void glMatrixMode(int i) {
        this.f10342gl.glMatrixMode(i);
    }

    public void glMinmax(int i, int i2, boolean z) {
        this.f10342gl.glMinmax(i, i2, z);
    }

    public void glMultMatrixd(DoubleBuffer doubleBuffer) {
        this.f10342gl.glMultMatrixd(doubleBuffer);
    }

    public void glMultMatrixd(double[] dArr, int i) {
        this.f10342gl.glMultMatrixd(dArr, i);
    }

    public void glMultMatrixf(float[] fArr, int i) {
        this.f10342gl.glMultMatrixf(fArr, i);
    }

    public void glMultMatrixf(FloatBuffer floatBuffer) {
        this.f10342gl.glMultMatrixf(floatBuffer);
    }

    public void glMultTransposeMatrixd(double[] dArr, int i) {
        this.f10342gl.glMultTransposeMatrixd(dArr, i);
    }

    public void glMultTransposeMatrixd(DoubleBuffer doubleBuffer) {
        this.f10342gl.glMultTransposeMatrixd(doubleBuffer);
    }

    public void glMultTransposeMatrixf(float[] fArr, int i) {
        this.f10342gl.glMultTransposeMatrixf(fArr, i);
    }

    public void glMultTransposeMatrixf(FloatBuffer floatBuffer) {
        this.f10342gl.glMultTransposeMatrixf(floatBuffer);
    }

    public void glMultiDrawArrays(int i, int[] iArr, int i2, int[] iArr2, int i3, int i4) {
        this.f10342gl.glMultiDrawArrays(i, iArr, i2, iArr2, i3, i4);
    }

    public void glMultiDrawArrays(int i, IntBuffer intBuffer, IntBuffer intBuffer2, int i2) {
        this.f10342gl.glMultiDrawArrays(i, intBuffer, intBuffer2, i2);
    }

    public void glMultiDrawArraysEXT(int i, int[] iArr, int i2, int[] iArr2, int i3, int i4) {
        this.f10342gl.glMultiDrawArraysEXT(i, iArr, i2, iArr2, i3, i4);
    }

    public void glMultiDrawArraysEXT(int i, IntBuffer intBuffer, IntBuffer intBuffer2, int i2) {
        this.f10342gl.glMultiDrawArraysEXT(i, intBuffer, intBuffer2, i2);
    }

    public void glMultiDrawElementArrayAPPLE(int i, int[] iArr, int i2, int[] iArr2, int i3, int i4) {
        this.f10342gl.glMultiDrawElementArrayAPPLE(i, iArr, i2, iArr2, i3, i4);
    }

    public void glMultiDrawElementArrayAPPLE(int i, IntBuffer intBuffer, IntBuffer intBuffer2, int i2) {
        this.f10342gl.glMultiDrawElementArrayAPPLE(i, intBuffer, intBuffer2, i2);
    }

    public void glMultiDrawElements(int i, IntBuffer intBuffer, int i2, Buffer[] bufferArr, int i3) {
        this.f10342gl.glMultiDrawElements(i, intBuffer, i2, bufferArr, i3);
    }

    public void glMultiDrawElements(int i, int[] iArr, int i2, int i3, Buffer[] bufferArr, int i4) {
        this.f10342gl.glMultiDrawElements(i, iArr, i2, i3, bufferArr, i4);
    }

    public void glMultiDrawElementsEXT(int i, int[] iArr, int i2, int i3, Buffer[] bufferArr, int i4) {
        this.f10342gl.glMultiDrawElementsEXT(i, iArr, i2, i3, bufferArr, i4);
    }

    public void glMultiDrawElementsEXT(int i, IntBuffer intBuffer, int i2, Buffer[] bufferArr, int i3) {
        this.f10342gl.glMultiDrawElementsEXT(i, intBuffer, i2, bufferArr, i3);
    }

    public void glMultiDrawRangeElementArrayAPPLE(int i, int i2, int i3, IntBuffer intBuffer, IntBuffer intBuffer2, int i4) {
        this.f10342gl.glMultiDrawRangeElementArrayAPPLE(i, i2, i3, intBuffer, intBuffer2, i4);
    }

    public void glMultiDrawRangeElementArrayAPPLE(int i, int i2, int i3, int[] iArr, int i4, int[] iArr2, int i5, int i6) {
        this.f10342gl.glMultiDrawRangeElementArrayAPPLE(i, i2, i3, iArr, i4, iArr2, i5, i6);
    }

    public void glMultiModeDrawArraysIBM(int[] iArr, int i, int[] iArr2, int i2, int[] iArr3, int i3, int i4, int i5) {
        this.f10342gl.glMultiModeDrawArraysIBM(iArr, i, iArr2, i2, iArr3, i3, i4, i5);
    }

    public void glMultiModeDrawArraysIBM(IntBuffer intBuffer, IntBuffer intBuffer2, IntBuffer intBuffer3, int i, int i2) {
        this.f10342gl.glMultiModeDrawArraysIBM(intBuffer, intBuffer2, intBuffer3, i, i2);
    }

    public void glMultiModeDrawElementsIBM(IntBuffer intBuffer, IntBuffer intBuffer2, int i, Buffer[] bufferArr, int i2, int i3) {
        this.f10342gl.glMultiModeDrawElementsIBM(intBuffer, intBuffer2, i, bufferArr, i2, i3);
    }

    public void glMultiModeDrawElementsIBM(int[] iArr, int i, int[] iArr2, int i2, int i3, Buffer[] bufferArr, int i4, int i5) {
        this.f10342gl.glMultiModeDrawElementsIBM(iArr, i, iArr2, i2, i3, bufferArr, i4, i5);
    }

    public void glMultiTexCoord1d(int i, double d) {
        this.f10342gl.glMultiTexCoord1d(i, d);
    }

    public void glMultiTexCoord1dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMultiTexCoord1dv(i, doubleBuffer);
    }

    public void glMultiTexCoord1dv(int i, double[] dArr, int i2) {
        this.f10342gl.glMultiTexCoord1dv(i, dArr, i2);
    }

    public void glMultiTexCoord1f(int i, float f) {
        this.f10342gl.glMultiTexCoord1f(i, f);
    }

    public void glMultiTexCoord1fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glMultiTexCoord1fv(i, floatBuffer);
    }

    public void glMultiTexCoord1fv(int i, float[] fArr, int i2) {
        this.f10342gl.glMultiTexCoord1fv(i, fArr, i2);
    }

    public void glMultiTexCoord1hNV(int i, short s) {
        this.f10342gl.glMultiTexCoord1hNV(i, s);
    }

    public void glMultiTexCoord1hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord1hvNV(i, shortBuffer);
    }

    public void glMultiTexCoord1hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord1hvNV(i, sArr, i2);
    }

    public void glMultiTexCoord1i(int i, int i2) {
        this.f10342gl.glMultiTexCoord1i(i, i2);
    }

    public void glMultiTexCoord1iv(int i, IntBuffer intBuffer) {
        this.f10342gl.glMultiTexCoord1iv(i, intBuffer);
    }

    public void glMultiTexCoord1iv(int i, int[] iArr, int i2) {
        this.f10342gl.glMultiTexCoord1iv(i, iArr, i2);
    }

    public void glMultiTexCoord1s(int i, short s) {
        this.f10342gl.glMultiTexCoord1s(i, s);
    }

    public void glMultiTexCoord1sv(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord1sv(i, sArr, i2);
    }

    public void glMultiTexCoord1sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord1sv(i, shortBuffer);
    }

    public void glMultiTexCoord2d(int i, double d, double d2) {
        this.f10342gl.glMultiTexCoord2d(i, d, d2);
    }

    public void glMultiTexCoord2dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMultiTexCoord2dv(i, doubleBuffer);
    }

    public void glMultiTexCoord2dv(int i, double[] dArr, int i2) {
        this.f10342gl.glMultiTexCoord2dv(i, dArr, i2);
    }

    public void glMultiTexCoord2f(int i, float f, float f2) {
        this.f10342gl.glMultiTexCoord2f(i, f, f2);
    }

    public void glMultiTexCoord2fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glMultiTexCoord2fv(i, floatBuffer);
    }

    public void glMultiTexCoord2fv(int i, float[] fArr, int i2) {
        this.f10342gl.glMultiTexCoord2fv(i, fArr, i2);
    }

    public void glMultiTexCoord2hNV(int i, short s, short s2) {
        this.f10342gl.glMultiTexCoord2hNV(i, s, s2);
    }

    public void glMultiTexCoord2hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord2hvNV(i, sArr, i2);
    }

    public void glMultiTexCoord2hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord2hvNV(i, shortBuffer);
    }

    public void glMultiTexCoord2i(int i, int i2, int i3) {
        this.f10342gl.glMultiTexCoord2i(i, i2, i3);
    }

    public void glMultiTexCoord2iv(int i, IntBuffer intBuffer) {
        this.f10342gl.glMultiTexCoord2iv(i, intBuffer);
    }

    public void glMultiTexCoord2iv(int i, int[] iArr, int i2) {
        this.f10342gl.glMultiTexCoord2iv(i, iArr, i2);
    }

    public void glMultiTexCoord2s(int i, short s, short s2) {
        this.f10342gl.glMultiTexCoord2s(i, s, s2);
    }

    public void glMultiTexCoord2sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord2sv(i, shortBuffer);
    }

    public void glMultiTexCoord2sv(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord2sv(i, sArr, i2);
    }

    public void glMultiTexCoord3d(int i, double d, double d2, double d3) {
        this.f10342gl.glMultiTexCoord3d(i, d, d2, d3);
    }

    public void glMultiTexCoord3dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMultiTexCoord3dv(i, doubleBuffer);
    }

    public void glMultiTexCoord3dv(int i, double[] dArr, int i2) {
        this.f10342gl.glMultiTexCoord3dv(i, dArr, i2);
    }

    public void glMultiTexCoord3f(int i, float f, float f2, float f3) {
        this.f10342gl.glMultiTexCoord3f(i, f, f2, f3);
    }

    public void glMultiTexCoord3fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glMultiTexCoord3fv(i, floatBuffer);
    }

    public void glMultiTexCoord3fv(int i, float[] fArr, int i2) {
        this.f10342gl.glMultiTexCoord3fv(i, fArr, i2);
    }

    public void glMultiTexCoord3hNV(int i, short s, short s2, short s3) {
        this.f10342gl.glMultiTexCoord3hNV(i, s, s2, s3);
    }

    public void glMultiTexCoord3hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord3hvNV(i, shortBuffer);
    }

    public void glMultiTexCoord3hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord3hvNV(i, sArr, i2);
    }

    public void glMultiTexCoord3i(int i, int i2, int i3, int i4) {
        this.f10342gl.glMultiTexCoord3i(i, i2, i3, i4);
    }

    public void glMultiTexCoord3iv(int i, int[] iArr, int i2) {
        this.f10342gl.glMultiTexCoord3iv(i, iArr, i2);
    }

    public void glMultiTexCoord3iv(int i, IntBuffer intBuffer) {
        this.f10342gl.glMultiTexCoord3iv(i, intBuffer);
    }

    public void glMultiTexCoord3s(int i, short s, short s2, short s3) {
        this.f10342gl.glMultiTexCoord3s(i, s, s2, s3);
    }

    public void glMultiTexCoord3sv(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord3sv(i, sArr, i2);
    }

    public void glMultiTexCoord3sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord3sv(i, shortBuffer);
    }

    public void glMultiTexCoord4d(int i, double d, double d2, double d3, double d4) {
        this.f10342gl.glMultiTexCoord4d(i, d, d2, d3, d4);
    }

    public void glMultiTexCoord4dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glMultiTexCoord4dv(i, doubleBuffer);
    }

    public void glMultiTexCoord4dv(int i, double[] dArr, int i2) {
        this.f10342gl.glMultiTexCoord4dv(i, dArr, i2);
    }

    public void glMultiTexCoord4f(int i, float f, float f2, float f3, float f4) {
        this.f10342gl.glMultiTexCoord4f(i, f, f2, f3, f4);
    }

    public void glMultiTexCoord4fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glMultiTexCoord4fv(i, floatBuffer);
    }

    public void glMultiTexCoord4fv(int i, float[] fArr, int i2) {
        this.f10342gl.glMultiTexCoord4fv(i, fArr, i2);
    }

    public void glMultiTexCoord4hNV(int i, short s, short s2, short s3, short s4) {
        this.f10342gl.glMultiTexCoord4hNV(i, s, s2, s3, s4);
    }

    public void glMultiTexCoord4hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord4hvNV(i, shortBuffer);
    }

    public void glMultiTexCoord4hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord4hvNV(i, sArr, i2);
    }

    public void glMultiTexCoord4i(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glMultiTexCoord4i(i, i2, i3, i4, i5);
    }

    public void glMultiTexCoord4iv(int i, int[] iArr, int i2) {
        this.f10342gl.glMultiTexCoord4iv(i, iArr, i2);
    }

    public void glMultiTexCoord4iv(int i, IntBuffer intBuffer) {
        this.f10342gl.glMultiTexCoord4iv(i, intBuffer);
    }

    public void glMultiTexCoord4s(int i, short s, short s2, short s3, short s4) {
        this.f10342gl.glMultiTexCoord4s(i, s, s2, s3, s4);
    }

    public void glMultiTexCoord4sv(int i, short[] sArr, int i2) {
        this.f10342gl.glMultiTexCoord4sv(i, sArr, i2);
    }

    public void glMultiTexCoord4sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glMultiTexCoord4sv(i, shortBuffer);
    }

    public int glNewBufferRegion(int i) {
        return this.f10342gl.glNewBufferRegion(i);
    }

    public void glNewList(int i, int i2) {
        this.f10342gl.glNewList(i, i2);
    }

    public int glNewObjectBufferATI(int i, Buffer buffer, int i2) {
        return this.f10342gl.glNewObjectBufferATI(i, buffer, i2);
    }

    public void glNormal3b(byte b, byte b2, byte b3) {
        this.f10342gl.glNormal3b(b, b2, b3);
    }

    public void glNormal3bv(ByteBuffer byteBuffer) {
        this.f10342gl.glNormal3bv(byteBuffer);
    }

    public void glNormal3bv(byte[] bArr, int i) {
        this.f10342gl.glNormal3bv(bArr, i);
    }

    public void glNormal3d(double d, double d2, double d3) {
        this.f10342gl.glNormal3d(d, d2, d3);
    }

    public void glNormal3dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glNormal3dv(doubleBuffer);
    }

    public void glNormal3dv(double[] dArr, int i) {
        this.f10342gl.glNormal3dv(dArr, i);
    }

    public void glNormal3f(float f, float f2, float f3) {
        this.f10342gl.glNormal3f(f, f2, f3);
    }

    public void glNormal3fVertex3fSUN(float f, float f2, float f3, float f4, float f5, float f6) {
        this.f10342gl.glNormal3fVertex3fSUN(f, f2, f3, f4, f5, f6);
    }

    public void glNormal3fVertex3fvSUN(float[] fArr, int i, float[] fArr2, int i2) {
        this.f10342gl.glNormal3fVertex3fvSUN(fArr, i, fArr2, i2);
    }

    public void glNormal3fVertex3fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glNormal3fVertex3fvSUN(floatBuffer, floatBuffer2);
    }

    public void glNormal3fv(FloatBuffer floatBuffer) {
        this.f10342gl.glNormal3fv(floatBuffer);
    }

    public void glNormal3fv(float[] fArr, int i) {
        this.f10342gl.glNormal3fv(fArr, i);
    }

    public void glNormal3hNV(short s, short s2, short s3) {
        this.f10342gl.glNormal3hNV(s, s2, s3);
    }

    public void glNormal3hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glNormal3hvNV(shortBuffer);
    }

    public void glNormal3hvNV(short[] sArr, int i) {
        this.f10342gl.glNormal3hvNV(sArr, i);
    }

    public void glNormal3i(int i, int i2, int i3) {
        this.f10342gl.glNormal3i(i, i2, i3);
    }

    public void glNormal3iv(IntBuffer intBuffer) {
        this.f10342gl.glNormal3iv(intBuffer);
    }

    public void glNormal3iv(int[] iArr, int i) {
        this.f10342gl.glNormal3iv(iArr, i);
    }

    public void glNormal3s(short s, short s2, short s3) {
        this.f10342gl.glNormal3s(s, s2, s3);
    }

    public void glNormal3sv(ShortBuffer shortBuffer) {
        this.f10342gl.glNormal3sv(shortBuffer);
    }

    public void glNormal3sv(short[] sArr, int i) {
        this.f10342gl.glNormal3sv(sArr, i);
    }

    public void glNormalPointer(int i, int i2, long j) {
        this.f10342gl.glNormalPointer(i, i2, j);
    }

    public void glNormalPointer(int i, int i2, Buffer buffer) {
        this.f10342gl.glNormalPointer(i, i2, buffer);
    }

    public void glNormalStream3bATI(int i, byte b, byte b2, byte b3) {
        this.f10342gl.glNormalStream3bATI(i, b, b2, b3);
    }

    public void glNormalStream3bvATI(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glNormalStream3bvATI(i, byteBuffer);
    }

    public void glNormalStream3bvATI(int i, byte[] bArr, int i2) {
        this.f10342gl.glNormalStream3bvATI(i, bArr, i2);
    }

    public void glNormalStream3dATI(int i, double d, double d2, double d3) {
        this.f10342gl.glNormalStream3dATI(i, d, d2, d3);
    }

    public void glNormalStream3dvATI(int i, double[] dArr, int i2) {
        this.f10342gl.glNormalStream3dvATI(i, dArr, i2);
    }

    public void glNormalStream3dvATI(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glNormalStream3dvATI(i, doubleBuffer);
    }

    public void glNormalStream3fATI(int i, float f, float f2, float f3) {
        this.f10342gl.glNormalStream3fATI(i, f, f2, f3);
    }

    public void glNormalStream3fvATI(int i, float[] fArr, int i2) {
        this.f10342gl.glNormalStream3fvATI(i, fArr, i2);
    }

    public void glNormalStream3fvATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glNormalStream3fvATI(i, floatBuffer);
    }

    public void glNormalStream3iATI(int i, int i2, int i3, int i4) {
        this.f10342gl.glNormalStream3iATI(i, i2, i3, i4);
    }

    public void glNormalStream3ivATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glNormalStream3ivATI(i, intBuffer);
    }

    public void glNormalStream3ivATI(int i, int[] iArr, int i2) {
        this.f10342gl.glNormalStream3ivATI(i, iArr, i2);
    }

    public void glNormalStream3sATI(int i, short s, short s2, short s3) {
        this.f10342gl.glNormalStream3sATI(i, s, s2, s3);
    }

    public void glNormalStream3svATI(int i, short[] sArr, int i2) {
        this.f10342gl.glNormalStream3svATI(i, sArr, i2);
    }

    public void glNormalStream3svATI(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glNormalStream3svATI(i, shortBuffer);
    }

    public void glOrtho(double d, double d2, double d3, double d4, double d5, double d6) {
        this.f10342gl.glOrtho(d, d2, d3, d4, d5, d6);
    }

    public void glPNTrianglesfATI(int i, float f) {
        this.f10342gl.glPNTrianglesfATI(i, f);
    }

    public void glPNTrianglesiATI(int i, int i2) {
        this.f10342gl.glPNTrianglesiATI(i, i2);
    }

    public void glPassTexCoordATI(int i, int i2, int i3) {
        this.f10342gl.glPassTexCoordATI(i, i2, i3);
    }

    public void glPassThrough(float f) {
        this.f10342gl.glPassThrough(f);
    }

    public void glPixelDataRangeNV(int i, int i2, Buffer buffer) {
        this.f10342gl.glPixelDataRangeNV(i, i2, buffer);
    }

    public void glPixelMapfv(int i, int i2, long j) {
        this.f10342gl.glPixelMapfv(i, i2, j);
    }

    public void glPixelMapfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glPixelMapfv(i, i2, fArr, i3);
    }

    public void glPixelMapfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glPixelMapfv(i, i2, floatBuffer);
    }

    public void glPixelMapuiv(int i, int i2, long j) {
        this.f10342gl.glPixelMapuiv(i, i2, j);
    }

    public void glPixelMapuiv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glPixelMapuiv(i, i2, iArr, i3);
    }

    public void glPixelMapuiv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glPixelMapuiv(i, i2, intBuffer);
    }

    public void glPixelMapusv(int i, int i2, long j) {
        this.f10342gl.glPixelMapusv(i, i2, j);
    }

    public void glPixelMapusv(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glPixelMapusv(i, i2, sArr, i3);
    }

    public void glPixelMapusv(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glPixelMapusv(i, i2, shortBuffer);
    }

    public void glPixelStoref(int i, float f) {
        this.f10342gl.glPixelStoref(i, f);
    }

    public void glPixelStorei(int i, int i2) {
        this.f10342gl.glPixelStorei(i, i2);
    }

    public void glPixelTexGenParameterfSGIS(int i, float f) {
        this.f10342gl.glPixelTexGenParameterfSGIS(i, f);
    }

    public void glPixelTexGenParameterfvSGIS(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glPixelTexGenParameterfvSGIS(i, floatBuffer);
    }

    public void glPixelTexGenParameterfvSGIS(int i, float[] fArr, int i2) {
        this.f10342gl.glPixelTexGenParameterfvSGIS(i, fArr, i2);
    }

    public void glPixelTexGenParameteriSGIS(int i, int i2) {
        this.f10342gl.glPixelTexGenParameteriSGIS(i, i2);
    }

    public void glPixelTexGenParameterivSGIS(int i, int[] iArr, int i2) {
        this.f10342gl.glPixelTexGenParameterivSGIS(i, iArr, i2);
    }

    public void glPixelTexGenParameterivSGIS(int i, IntBuffer intBuffer) {
        this.f10342gl.glPixelTexGenParameterivSGIS(i, intBuffer);
    }

    public void glPixelTexGenSGIX(int i) {
        this.f10342gl.glPixelTexGenSGIX(i);
    }

    public void glPixelTransferf(int i, float f) {
        this.f10342gl.glPixelTransferf(i, f);
    }

    public void glPixelTransferi(int i, int i2) {
        this.f10342gl.glPixelTransferi(i, i2);
    }

    public void glPixelTransformParameterfEXT(int i, int i2, float f) {
        this.f10342gl.glPixelTransformParameterfEXT(i, i2, f);
    }

    public void glPixelTransformParameterfvEXT(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glPixelTransformParameterfvEXT(i, i2, floatBuffer);
    }

    public void glPixelTransformParameterfvEXT(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glPixelTransformParameterfvEXT(i, i2, fArr, i3);
    }

    public void glPixelTransformParameteriEXT(int i, int i2, int i3) {
        this.f10342gl.glPixelTransformParameteriEXT(i, i2, i3);
    }

    public void glPixelTransformParameterivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glPixelTransformParameterivEXT(i, i2, intBuffer);
    }

    public void glPixelTransformParameterivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glPixelTransformParameterivEXT(i, i2, iArr, i3);
    }

    public void glPixelZoom(float f, float f2) {
        this.f10342gl.glPixelZoom(f, f2);
    }

    public void glPointParameterf(int i, float f) {
        this.f10342gl.glPointParameterf(i, f);
    }

    public void glPointParameterfARB(int i, float f) {
        this.f10342gl.glPointParameterfARB(i, f);
    }

    public void glPointParameterfEXT(int i, float f) {
        this.f10342gl.glPointParameterfEXT(i, f);
    }

    public void glPointParameterfSGIS(int i, float f) {
        this.f10342gl.glPointParameterfSGIS(i, f);
    }

    public void glPointParameterfv(int i, float[] fArr, int i2) {
        this.f10342gl.glPointParameterfv(i, fArr, i2);
    }

    public void glPointParameterfv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glPointParameterfv(i, floatBuffer);
    }

    public void glPointParameterfvARB(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glPointParameterfvARB(i, floatBuffer);
    }

    public void glPointParameterfvARB(int i, float[] fArr, int i2) {
        this.f10342gl.glPointParameterfvARB(i, fArr, i2);
    }

    public void glPointParameterfvEXT(int i, float[] fArr, int i2) {
        this.f10342gl.glPointParameterfvEXT(i, fArr, i2);
    }

    public void glPointParameterfvEXT(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glPointParameterfvEXT(i, floatBuffer);
    }

    public void glPointParameterfvSGIS(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glPointParameterfvSGIS(i, floatBuffer);
    }

    public void glPointParameterfvSGIS(int i, float[] fArr, int i2) {
        this.f10342gl.glPointParameterfvSGIS(i, fArr, i2);
    }

    public void glPointParameteri(int i, int i2) {
        this.f10342gl.glPointParameteri(i, i2);
    }

    public void glPointParameteriNV(int i, int i2) {
        this.f10342gl.glPointParameteriNV(i, i2);
    }

    public void glPointParameteriv(int i, int[] iArr, int i2) {
        this.f10342gl.glPointParameteriv(i, iArr, i2);
    }

    public void glPointParameteriv(int i, IntBuffer intBuffer) {
        this.f10342gl.glPointParameteriv(i, intBuffer);
    }

    public void glPointParameterivNV(int i, int[] iArr, int i2) {
        this.f10342gl.glPointParameterivNV(i, iArr, i2);
    }

    public void glPointParameterivNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glPointParameterivNV(i, intBuffer);
    }

    public void glPointSize(float f) {
        this.f10342gl.glPointSize(f);
    }

    public int glPollAsyncSGIX(IntBuffer intBuffer) {
        return this.f10342gl.glPollAsyncSGIX(intBuffer);
    }

    public int glPollAsyncSGIX(int[] iArr, int i) {
        return this.f10342gl.glPollAsyncSGIX(iArr, i);
    }

    public int glPollInstrumentsSGIX(int[] iArr, int i) {
        return this.f10342gl.glPollInstrumentsSGIX(iArr, i);
    }

    public int glPollInstrumentsSGIX(IntBuffer intBuffer) {
        return this.f10342gl.glPollInstrumentsSGIX(intBuffer);
    }

    public void glPolygonMode(int i, int i2) {
        this.f10342gl.glPolygonMode(i, i2);
    }

    public void glPolygonOffset(float f, float f2) {
        this.f10342gl.glPolygonOffset(f, f2);
    }

    public void glPolygonStipple(byte[] bArr, int i) {
        this.f10342gl.glPolygonStipple(bArr, i);
    }

    public void glPolygonStipple(ByteBuffer byteBuffer) {
        this.f10342gl.glPolygonStipple(byteBuffer);
    }

    public void glPolygonStipple(long j) {
        this.f10342gl.glPolygonStipple(j);
    }

    public void glPopAttrib() {
        this.f10342gl.glPopAttrib();
    }

    public void glPopClientAttrib() {
        this.f10342gl.glPopClientAttrib();
    }

    public void glPopMatrix() {
        this.f10342gl.glPopMatrix();
    }

    public void glPopName() {
        this.f10342gl.glPopName();
    }

    public void glPrimitiveRestartIndexNV(int i) {
        this.f10342gl.glPrimitiveRestartIndexNV(i);
    }

    public void glPrimitiveRestartNV() {
        this.f10342gl.glPrimitiveRestartNV();
    }

    public void glPrioritizeTextures(int i, int[] iArr, int i2, float[] fArr, int i3) {
        this.f10342gl.glPrioritizeTextures(i, iArr, i2, fArr, i3);
    }

    public void glPrioritizeTextures(int i, IntBuffer intBuffer, FloatBuffer floatBuffer) {
        this.f10342gl.glPrioritizeTextures(i, intBuffer, floatBuffer);
    }

    public void glProgramBufferParametersIivNV(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        this.f10342gl.glProgramBufferParametersIivNV(i, i2, i3, i4, iArr, i5);
    }

    public void glProgramBufferParametersIivNV(int i, int i2, int i3, int i4, IntBuffer intBuffer) {
        this.f10342gl.glProgramBufferParametersIivNV(i, i2, i3, i4, intBuffer);
    }

    public void glProgramBufferParametersIuivNV(int i, int i2, int i3, int i4, IntBuffer intBuffer) {
        this.f10342gl.glProgramBufferParametersIuivNV(i, i2, i3, i4, intBuffer);
    }

    public void glProgramBufferParametersIuivNV(int i, int i2, int i3, int i4, int[] iArr, int i5) {
        this.f10342gl.glProgramBufferParametersIuivNV(i, i2, i3, i4, iArr, i5);
    }

    public void glProgramBufferParametersfvNV(int i, int i2, int i3, int i4, float[] fArr, int i5) {
        this.f10342gl.glProgramBufferParametersfvNV(i, i2, i3, i4, fArr, i5);
    }

    public void glProgramBufferParametersfvNV(int i, int i2, int i3, int i4, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramBufferParametersfvNV(i, i2, i3, i4, floatBuffer);
    }

    public void glProgramEnvParameter4dARB(int i, int i2, double d, double d2, double d3, double d4) {
        this.f10342gl.glProgramEnvParameter4dARB(i, i2, d, d2, d3, d4);
    }

    public void glProgramEnvParameter4dvARB(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glProgramEnvParameter4dvARB(i, i2, dArr, i3);
    }

    public void glProgramEnvParameter4dvARB(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glProgramEnvParameter4dvARB(i, i2, doubleBuffer);
    }

    public void glProgramEnvParameter4fARB(int i, int i2, float f, float f2, float f3, float f4) {
        this.f10342gl.glProgramEnvParameter4fARB(i, i2, f, f2, f3, f4);
    }

    public void glProgramEnvParameter4fvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramEnvParameter4fvARB(i, i2, floatBuffer);
    }

    public void glProgramEnvParameter4fvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glProgramEnvParameter4fvARB(i, i2, fArr, i3);
    }

    public void glProgramEnvParameterI4iNV(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glProgramEnvParameterI4iNV(i, i2, i3, i4, i5, i6);
    }

    public void glProgramEnvParameterI4ivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glProgramEnvParameterI4ivNV(i, i2, intBuffer);
    }

    public void glProgramEnvParameterI4ivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glProgramEnvParameterI4ivNV(i, i2, iArr, i3);
    }

    public void glProgramEnvParameterI4uiNV(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glProgramEnvParameterI4uiNV(i, i2, i3, i4, i5, i6);
    }

    public void glProgramEnvParameterI4uivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glProgramEnvParameterI4uivNV(i, i2, intBuffer);
    }

    public void glProgramEnvParameterI4uivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glProgramEnvParameterI4uivNV(i, i2, iArr, i3);
    }

    public void glProgramEnvParameters4fvEXT(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glProgramEnvParameters4fvEXT(i, i2, i3, fArr, i4);
    }

    public void glProgramEnvParameters4fvEXT(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramEnvParameters4fvEXT(i, i2, i3, floatBuffer);
    }

    public void glProgramEnvParametersI4ivNV(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glProgramEnvParametersI4ivNV(i, i2, i3, iArr, i4);
    }

    public void glProgramEnvParametersI4ivNV(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glProgramEnvParametersI4ivNV(i, i2, i3, intBuffer);
    }

    public void glProgramEnvParametersI4uivNV(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glProgramEnvParametersI4uivNV(i, i2, i3, intBuffer);
    }

    public void glProgramEnvParametersI4uivNV(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glProgramEnvParametersI4uivNV(i, i2, i3, iArr, i4);
    }

    public void glProgramLocalParameter4dARB(int i, int i2, double d, double d2, double d3, double d4) {
        this.f10342gl.glProgramLocalParameter4dARB(i, i2, d, d2, d3, d4);
    }

    public void glProgramLocalParameter4dvARB(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glProgramLocalParameter4dvARB(i, i2, doubleBuffer);
    }

    public void glProgramLocalParameter4dvARB(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glProgramLocalParameter4dvARB(i, i2, dArr, i3);
    }

    public void glProgramLocalParameter4fARB(int i, int i2, float f, float f2, float f3, float f4) {
        this.f10342gl.glProgramLocalParameter4fARB(i, i2, f, f2, f3, f4);
    }

    public void glProgramLocalParameter4fvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glProgramLocalParameter4fvARB(i, i2, fArr, i3);
    }

    public void glProgramLocalParameter4fvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramLocalParameter4fvARB(i, i2, floatBuffer);
    }

    public void glProgramLocalParameterI4iNV(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glProgramLocalParameterI4iNV(i, i2, i3, i4, i5, i6);
    }

    public void glProgramLocalParameterI4ivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glProgramLocalParameterI4ivNV(i, i2, intBuffer);
    }

    public void glProgramLocalParameterI4ivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glProgramLocalParameterI4ivNV(i, i2, iArr, i3);
    }

    public void glProgramLocalParameterI4uiNV(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glProgramLocalParameterI4uiNV(i, i2, i3, i4, i5, i6);
    }

    public void glProgramLocalParameterI4uivNV(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glProgramLocalParameterI4uivNV(i, i2, intBuffer);
    }

    public void glProgramLocalParameterI4uivNV(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glProgramLocalParameterI4uivNV(i, i2, iArr, i3);
    }

    public void glProgramLocalParameters4fvEXT(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glProgramLocalParameters4fvEXT(i, i2, i3, fArr, i4);
    }

    public void glProgramLocalParameters4fvEXT(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramLocalParameters4fvEXT(i, i2, i3, floatBuffer);
    }

    public void glProgramLocalParametersI4ivNV(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glProgramLocalParametersI4ivNV(i, i2, i3, intBuffer);
    }

    public void glProgramLocalParametersI4ivNV(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glProgramLocalParametersI4ivNV(i, i2, i3, iArr, i4);
    }

    public void glProgramLocalParametersI4uivNV(int i, int i2, int i3, int[] iArr, int i4) {
        this.f10342gl.glProgramLocalParametersI4uivNV(i, i2, i3, iArr, i4);
    }

    public void glProgramLocalParametersI4uivNV(int i, int i2, int i3, IntBuffer intBuffer) {
        this.f10342gl.glProgramLocalParametersI4uivNV(i, i2, i3, intBuffer);
    }

    public void glProgramNamedParameter4dNV(int i, int i2, String str, double d, double d2, double d3, double d4) {
        this.f10342gl.glProgramNamedParameter4dNV(i, i2, str, d, d2, d3, d4);
    }

    public void glProgramNamedParameter4dvNV(int i, int i2, String str, double[] dArr, int i3) {
        this.f10342gl.glProgramNamedParameter4dvNV(i, i2, str, dArr, i3);
    }

    public void glProgramNamedParameter4dvNV(int i, int i2, String str, DoubleBuffer doubleBuffer) {
        this.f10342gl.glProgramNamedParameter4dvNV(i, i2, str, doubleBuffer);
    }

    public void glProgramNamedParameter4fNV(int i, int i2, String str, float f, float f2, float f3, float f4) {
        this.f10342gl.glProgramNamedParameter4fNV(i, i2, str, f, f2, f3, f4);
    }

    public void glProgramNamedParameter4fvNV(int i, int i2, String str, float[] fArr, int i3) {
        this.f10342gl.glProgramNamedParameter4fvNV(i, i2, str, fArr, i3);
    }

    public void glProgramNamedParameter4fvNV(int i, int i2, String str, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramNamedParameter4fvNV(i, i2, str, floatBuffer);
    }

    public void glProgramParameter4dNV(int i, int i2, double d, double d2, double d3, double d4) {
        this.f10342gl.glProgramParameter4dNV(i, i2, d, d2, d3, d4);
    }

    public void glProgramParameter4dvNV(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glProgramParameter4dvNV(i, i2, dArr, i3);
    }

    public void glProgramParameter4dvNV(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glProgramParameter4dvNV(i, i2, doubleBuffer);
    }

    public void glProgramParameter4fNV(int i, int i2, float f, float f2, float f3, float f4) {
        this.f10342gl.glProgramParameter4fNV(i, i2, f, f2, f3, f4);
    }

    public void glProgramParameter4fvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glProgramParameter4fvNV(i, i2, fArr, i3);
    }

    public void glProgramParameter4fvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramParameter4fvNV(i, i2, floatBuffer);
    }

    public void glProgramParameteriEXT(int i, int i2, int i3) {
        this.f10342gl.glProgramParameteriEXT(i, i2, i3);
    }

    public void glProgramParameters4dvNV(int i, int i2, int i3, DoubleBuffer doubleBuffer) {
        this.f10342gl.glProgramParameters4dvNV(i, i2, i3, doubleBuffer);
    }

    public void glProgramParameters4dvNV(int i, int i2, int i3, double[] dArr, int i4) {
        this.f10342gl.glProgramParameters4dvNV(i, i2, i3, dArr, i4);
    }

    public void glProgramParameters4fvNV(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glProgramParameters4fvNV(i, i2, i3, fArr, i4);
    }

    public void glProgramParameters4fvNV(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glProgramParameters4fvNV(i, i2, i3, floatBuffer);
    }

    public void glProgramStringARB(int i, int i2, int i3, String str) {
        this.f10342gl.glProgramStringARB(i, i2, i3, str);
    }

    public void glProgramVertexLimitNV(int i, int i2) {
        this.f10342gl.glProgramVertexLimitNV(i, i2);
    }

    public void glPushAttrib(int i) {
        this.f10342gl.glPushAttrib(i);
    }

    public void glPushClientAttrib(int i) {
        this.f10342gl.glPushClientAttrib(i);
    }

    public void glPushMatrix() {
        this.f10342gl.glPushMatrix();
    }

    public void glPushName(int i) {
        this.f10342gl.glPushName(i);
    }

    public void glRasterPos2d(double d, double d2) {
        this.f10342gl.glRasterPos2d(d, d2);
    }

    public void glRasterPos2dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glRasterPos2dv(doubleBuffer);
    }

    public void glRasterPos2dv(double[] dArr, int i) {
        this.f10342gl.glRasterPos2dv(dArr, i);
    }

    public void glRasterPos2f(float f, float f2) {
        this.f10342gl.glRasterPos2f(f, f2);
    }

    public void glRasterPos2fv(float[] fArr, int i) {
        this.f10342gl.glRasterPos2fv(fArr, i);
    }

    public void glRasterPos2fv(FloatBuffer floatBuffer) {
        this.f10342gl.glRasterPos2fv(floatBuffer);
    }

    public void glRasterPos2i(int i, int i2) {
        this.f10342gl.glRasterPos2i(i, i2);
    }

    public void glRasterPos2iv(IntBuffer intBuffer) {
        this.f10342gl.glRasterPos2iv(intBuffer);
    }

    public void glRasterPos2iv(int[] iArr, int i) {
        this.f10342gl.glRasterPos2iv(iArr, i);
    }

    public void glRasterPos2s(short s, short s2) {
        this.f10342gl.glRasterPos2s(s, s2);
    }

    public void glRasterPos2sv(ShortBuffer shortBuffer) {
        this.f10342gl.glRasterPos2sv(shortBuffer);
    }

    public void glRasterPos2sv(short[] sArr, int i) {
        this.f10342gl.glRasterPos2sv(sArr, i);
    }

    public void glRasterPos3d(double d, double d2, double d3) {
        this.f10342gl.glRasterPos3d(d, d2, d3);
    }

    public void glRasterPos3dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glRasterPos3dv(doubleBuffer);
    }

    public void glRasterPos3dv(double[] dArr, int i) {
        this.f10342gl.glRasterPos3dv(dArr, i);
    }

    public void glRasterPos3f(float f, float f2, float f3) {
        this.f10342gl.glRasterPos3f(f, f2, f3);
    }

    public void glRasterPos3fv(FloatBuffer floatBuffer) {
        this.f10342gl.glRasterPos3fv(floatBuffer);
    }

    public void glRasterPos3fv(float[] fArr, int i) {
        this.f10342gl.glRasterPos3fv(fArr, i);
    }

    public void glRasterPos3i(int i, int i2, int i3) {
        this.f10342gl.glRasterPos3i(i, i2, i3);
    }

    public void glRasterPos3iv(int[] iArr, int i) {
        this.f10342gl.glRasterPos3iv(iArr, i);
    }

    public void glRasterPos3iv(IntBuffer intBuffer) {
        this.f10342gl.glRasterPos3iv(intBuffer);
    }

    public void glRasterPos3s(short s, short s2, short s3) {
        this.f10342gl.glRasterPos3s(s, s2, s3);
    }

    public void glRasterPos3sv(ShortBuffer shortBuffer) {
        this.f10342gl.glRasterPos3sv(shortBuffer);
    }

    public void glRasterPos3sv(short[] sArr, int i) {
        this.f10342gl.glRasterPos3sv(sArr, i);
    }

    public void glRasterPos4d(double d, double d2, double d3, double d4) {
        this.f10342gl.glRasterPos4d(d, d2, d3, d4);
    }

    public void glRasterPos4dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glRasterPos4dv(doubleBuffer);
    }

    public void glRasterPos4dv(double[] dArr, int i) {
        this.f10342gl.glRasterPos4dv(dArr, i);
    }

    public void glRasterPos4f(float f, float f2, float f3, float f4) {
        this.f10342gl.glRasterPos4f(f, f2, f3, f4);
    }

    public void glRasterPos4fv(float[] fArr, int i) {
        this.f10342gl.glRasterPos4fv(fArr, i);
    }

    public void glRasterPos4fv(FloatBuffer floatBuffer) {
        this.f10342gl.glRasterPos4fv(floatBuffer);
    }

    public void glRasterPos4i(int i, int i2, int i3, int i4) {
        this.f10342gl.glRasterPos4i(i, i2, i3, i4);
    }

    public void glRasterPos4iv(IntBuffer intBuffer) {
        this.f10342gl.glRasterPos4iv(intBuffer);
    }

    public void glRasterPos4iv(int[] iArr, int i) {
        this.f10342gl.glRasterPos4iv(iArr, i);
    }

    public void glRasterPos4s(short s, short s2, short s3, short s4) {
        this.f10342gl.glRasterPos4s(s, s2, s3, s4);
    }

    public void glRasterPos4sv(ShortBuffer shortBuffer) {
        this.f10342gl.glRasterPos4sv(shortBuffer);
    }

    public void glRasterPos4sv(short[] sArr, int i) {
        this.f10342gl.glRasterPos4sv(sArr, i);
    }

    public void glReadBuffer(int i) {
        this.f10342gl.glReadBuffer(i);
    }

    public void glReadBufferRegion(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glReadBufferRegion(i, i2, i3, i4, i5);
    }

    public void glReadInstrumentsSGIX(int i) {
        this.f10342gl.glReadInstrumentsSGIX(i);
    }

    public void glReadPixels(int i, int i2, int i3, int i4, int i5, int i6, Buffer buffer) {
        this.f10342gl.glReadPixels(i, i2, i3, i4, i5, i6, buffer);
    }

    public void glReadPixels(int i, int i2, int i3, int i4, int i5, int i6, long j) {
        this.f10342gl.glReadPixels(i, i2, i3, i4, i5, i6, j);
    }

    public void glRectd(double d, double d2, double d3, double d4) {
        this.f10342gl.glRectd(d, d2, d3, d4);
    }

    public void glRectdv(double[] dArr, int i, double[] dArr2, int i2) {
        this.f10342gl.glRectdv(dArr, i, dArr2, i2);
    }

    public void glRectdv(DoubleBuffer doubleBuffer, DoubleBuffer doubleBuffer2) {
        this.f10342gl.glRectdv(doubleBuffer, doubleBuffer2);
    }

    public void glRectf(float f, float f2, float f3, float f4) {
        this.f10342gl.glRectf(f, f2, f3, f4);
    }

    public void glRectfv(FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glRectfv(floatBuffer, floatBuffer2);
    }

    public void glRectfv(float[] fArr, int i, float[] fArr2, int i2) {
        this.f10342gl.glRectfv(fArr, i, fArr2, i2);
    }

    public void glRecti(int i, int i2, int i3, int i4) {
        this.f10342gl.glRecti(i, i2, i3, i4);
    }

    public void glRectiv(IntBuffer intBuffer, IntBuffer intBuffer2) {
        this.f10342gl.glRectiv(intBuffer, intBuffer2);
    }

    public void glRectiv(int[] iArr, int i, int[] iArr2, int i2) {
        this.f10342gl.glRectiv(iArr, i, iArr2, i2);
    }

    public void glRects(short s, short s2, short s3, short s4) {
        this.f10342gl.glRects(s, s2, s3, s4);
    }

    public void glRectsv(short[] sArr, int i, short[] sArr2, int i2) {
        this.f10342gl.glRectsv(sArr, i, sArr2, i2);
    }

    public void glRectsv(ShortBuffer shortBuffer, ShortBuffer shortBuffer2) {
        this.f10342gl.glRectsv(shortBuffer, shortBuffer2);
    }

    public void glReferencePlaneSGIX(DoubleBuffer doubleBuffer) {
        this.f10342gl.glReferencePlaneSGIX(doubleBuffer);
    }

    public void glReferencePlaneSGIX(double[] dArr, int i) {
        this.f10342gl.glReferencePlaneSGIX(dArr, i);
    }

    public int glRenderMode(int i) {
        return this.f10342gl.glRenderMode(i);
    }

    public void glRenderbufferStorageEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glRenderbufferStorageEXT(i, i2, i3, i4);
    }

    public void glRenderbufferStorageMultisampleCoverageNV(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glRenderbufferStorageMultisampleCoverageNV(i, i2, i3, i4, i5, i6);
    }

    public void glRenderbufferStorageMultisampleEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glRenderbufferStorageMultisampleEXT(i, i2, i3, i4, i5);
    }

    public void glReplacementCodeuiColor3fVertex3fSUN(int i, float f, float f2, float f3, float f4, float f5, float f6) {
        this.f10342gl.glReplacementCodeuiColor3fVertex3fSUN(i, f, f2, f3, f4, f5, f6);
    }

    public void glReplacementCodeuiColor3fVertex3fvSUN(IntBuffer intBuffer, FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glReplacementCodeuiColor3fVertex3fvSUN(intBuffer, floatBuffer, floatBuffer2);
    }

    public void glReplacementCodeuiColor3fVertex3fvSUN(int[] iArr, int i, float[] fArr, int i2, float[] fArr2, int i3) {
        this.f10342gl.glReplacementCodeuiColor3fVertex3fvSUN(iArr, i, fArr, i2, fArr2, i3);
    }

    public void glReplacementCodeuiColor4fNormal3fVertex3fSUN(int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10) {
        this.f10342gl.glReplacementCodeuiColor4fNormal3fVertex3fSUN(i, f, f2, f3, f4, f5, f6, f7, f8, f9, f10);
    }

    public void glReplacementCodeuiColor4fNormal3fVertex3fvSUN(IntBuffer intBuffer, FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3) {
        this.f10342gl.glReplacementCodeuiColor4fNormal3fVertex3fvSUN(intBuffer, floatBuffer, floatBuffer2, floatBuffer3);
    }

    public void glReplacementCodeuiColor4fNormal3fVertex3fvSUN(int[] iArr, int i, float[] fArr, int i2, float[] fArr2, int i3, float[] fArr3, int i4) {
        this.f10342gl.glReplacementCodeuiColor4fNormal3fVertex3fvSUN(iArr, i, fArr, i2, fArr2, i3, fArr3, i4);
    }

    public void glReplacementCodeuiColor4ubVertex3fSUN(int i, byte b, byte b2, byte b3, byte b4, float f, float f2, float f3) {
        this.f10342gl.glReplacementCodeuiColor4ubVertex3fSUN(i, b, b2, b3, b4, f, f2, f3);
    }

    public void glReplacementCodeuiColor4ubVertex3fvSUN(int[] iArr, int i, byte[] bArr, int i2, float[] fArr, int i3) {
        this.f10342gl.glReplacementCodeuiColor4ubVertex3fvSUN(iArr, i, bArr, i2, fArr, i3);
    }

    public void glReplacementCodeuiColor4ubVertex3fvSUN(IntBuffer intBuffer, ByteBuffer byteBuffer, FloatBuffer floatBuffer) {
        this.f10342gl.glReplacementCodeuiColor4ubVertex3fvSUN(intBuffer, byteBuffer, floatBuffer);
    }

    public void glReplacementCodeuiNormal3fVertex3fSUN(int i, float f, float f2, float f3, float f4, float f5, float f6) {
        this.f10342gl.glReplacementCodeuiNormal3fVertex3fSUN(i, f, f2, f3, f4, f5, f6);
    }

    public void glReplacementCodeuiNormal3fVertex3fvSUN(IntBuffer intBuffer, FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glReplacementCodeuiNormal3fVertex3fvSUN(intBuffer, floatBuffer, floatBuffer2);
    }

    public void glReplacementCodeuiNormal3fVertex3fvSUN(int[] iArr, int i, float[] fArr, int i2, float[] fArr2, int i3) {
        this.f10342gl.glReplacementCodeuiNormal3fVertex3fvSUN(iArr, i, fArr, i2, fArr2, i3);
    }

    public void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN(int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12) {
        this.f10342gl.glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fSUN(i, f, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12);
    }

    public void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(IntBuffer intBuffer, FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3, FloatBuffer floatBuffer4) {
        this.f10342gl.glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(intBuffer, floatBuffer, floatBuffer2, floatBuffer3, floatBuffer4);
    }

    public void glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(int[] iArr, int i, float[] fArr, int i2, float[] fArr2, int i3, float[] fArr3, int i4, float[] fArr4, int i5) {
        this.f10342gl.glReplacementCodeuiTexCoord2fColor4fNormal3fVertex3fvSUN(iArr, i, fArr, i2, fArr2, i3, fArr3, i4, fArr4, i5);
    }

    public void glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN(int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        this.f10342gl.glReplacementCodeuiTexCoord2fNormal3fVertex3fSUN(i, f, f2, f3, f4, f5, f6, f7, f8);
    }

    public void glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(IntBuffer intBuffer, FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3) {
        this.f10342gl.glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(intBuffer, floatBuffer, floatBuffer2, floatBuffer3);
    }

    public void glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(int[] iArr, int i, float[] fArr, int i2, float[] fArr2, int i3, float[] fArr3, int i4) {
        this.f10342gl.glReplacementCodeuiTexCoord2fNormal3fVertex3fvSUN(iArr, i, fArr, i2, fArr2, i3, fArr3, i4);
    }

    public void glReplacementCodeuiTexCoord2fVertex3fSUN(int i, float f, float f2, float f3, float f4, float f5) {
        this.f10342gl.glReplacementCodeuiTexCoord2fVertex3fSUN(i, f, f2, f3, f4, f5);
    }

    public void glReplacementCodeuiTexCoord2fVertex3fvSUN(IntBuffer intBuffer, FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glReplacementCodeuiTexCoord2fVertex3fvSUN(intBuffer, floatBuffer, floatBuffer2);
    }

    public void glReplacementCodeuiTexCoord2fVertex3fvSUN(int[] iArr, int i, float[] fArr, int i2, float[] fArr2, int i3) {
        this.f10342gl.glReplacementCodeuiTexCoord2fVertex3fvSUN(iArr, i, fArr, i2, fArr2, i3);
    }

    public void glReplacementCodeuiVertex3fSUN(int i, float f, float f2, float f3) {
        this.f10342gl.glReplacementCodeuiVertex3fSUN(i, f, f2, f3);
    }

    public void glReplacementCodeuiVertex3fvSUN(IntBuffer intBuffer, FloatBuffer floatBuffer) {
        this.f10342gl.glReplacementCodeuiVertex3fvSUN(intBuffer, floatBuffer);
    }

    public void glReplacementCodeuiVertex3fvSUN(int[] iArr, int i, float[] fArr, int i2) {
        this.f10342gl.glReplacementCodeuiVertex3fvSUN(iArr, i, fArr, i2);
    }

    public void glRequestResidentProgramsNV(int i, int[] iArr, int i2) {
        this.f10342gl.glRequestResidentProgramsNV(i, iArr, i2);
    }

    public void glRequestResidentProgramsNV(int i, IntBuffer intBuffer) {
        this.f10342gl.glRequestResidentProgramsNV(i, intBuffer);
    }

    public void glResetHistogram(int i) {
        this.f10342gl.glResetHistogram(i);
    }

    public void glResetMinmax(int i) {
        this.f10342gl.glResetMinmax(i);
    }

    public void glResizeBuffersMESA() {
        this.f10342gl.glResizeBuffersMESA();
    }

    public void glRotated(double d, double d2, double d3, double d4) {
        this.f10342gl.glRotated(d, d2, d3, d4);
    }

    public void glRotatef(float f, float f2, float f3, float f4) {
        this.f10342gl.glRotatef(f, f2, f3, f4);
    }

    public void glSampleCoverage(float f, boolean z) {
        this.f10342gl.glSampleCoverage(f, z);
    }

    public void glSampleMapATI(int i, int i2, int i3) {
        this.f10342gl.glSampleMapATI(i, i2, i3);
    }

    public void glSampleMaskEXT(float f, boolean z) {
        this.f10342gl.glSampleMaskEXT(f, z);
    }

    public void glSampleMaskSGIS(float f, boolean z) {
        this.f10342gl.glSampleMaskSGIS(f, z);
    }

    public void glSamplePatternEXT(int i) {
        this.f10342gl.glSamplePatternEXT(i);
    }

    public void glSamplePatternSGIS(int i) {
        this.f10342gl.glSamplePatternSGIS(i);
    }

    public void glScaled(double d, double d2, double d3) {
        this.f10342gl.glScaled(d, d2, d3);
    }

    public void glScalef(float f, float f2, float f3) {
        this.f10342gl.glScalef(f, f2, f3);
    }

    public void glScissor(int i, int i2, int i3, int i4) {
        this.f10342gl.glScissor(i, i2, i3, i4);
    }

    public void glSecondaryColor3b(byte b, byte b2, byte b3) {
        this.f10342gl.glSecondaryColor3b(b, b2, b3);
    }

    public void glSecondaryColor3bEXT(byte b, byte b2, byte b3) {
        this.f10342gl.glSecondaryColor3bEXT(b, b2, b3);
    }

    public void glSecondaryColor3bv(byte[] bArr, int i) {
        this.f10342gl.glSecondaryColor3bv(bArr, i);
    }

    public void glSecondaryColor3bv(ByteBuffer byteBuffer) {
        this.f10342gl.glSecondaryColor3bv(byteBuffer);
    }

    public void glSecondaryColor3bvEXT(ByteBuffer byteBuffer) {
        this.f10342gl.glSecondaryColor3bvEXT(byteBuffer);
    }

    public void glSecondaryColor3bvEXT(byte[] bArr, int i) {
        this.f10342gl.glSecondaryColor3bvEXT(bArr, i);
    }

    public void glSecondaryColor3d(double d, double d2, double d3) {
        this.f10342gl.glSecondaryColor3d(d, d2, d3);
    }

    public void glSecondaryColor3dEXT(double d, double d2, double d3) {
        this.f10342gl.glSecondaryColor3dEXT(d, d2, d3);
    }

    public void glSecondaryColor3dv(double[] dArr, int i) {
        this.f10342gl.glSecondaryColor3dv(dArr, i);
    }

    public void glSecondaryColor3dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glSecondaryColor3dv(doubleBuffer);
    }

    public void glSecondaryColor3dvEXT(double[] dArr, int i) {
        this.f10342gl.glSecondaryColor3dvEXT(dArr, i);
    }

    public void glSecondaryColor3dvEXT(DoubleBuffer doubleBuffer) {
        this.f10342gl.glSecondaryColor3dvEXT(doubleBuffer);
    }

    public void glSecondaryColor3f(float f, float f2, float f3) {
        this.f10342gl.glSecondaryColor3f(f, f2, f3);
    }

    public void glSecondaryColor3fEXT(float f, float f2, float f3) {
        this.f10342gl.glSecondaryColor3fEXT(f, f2, f3);
    }

    public void glSecondaryColor3fv(FloatBuffer floatBuffer) {
        this.f10342gl.glSecondaryColor3fv(floatBuffer);
    }

    public void glSecondaryColor3fv(float[] fArr, int i) {
        this.f10342gl.glSecondaryColor3fv(fArr, i);
    }

    public void glSecondaryColor3fvEXT(float[] fArr, int i) {
        this.f10342gl.glSecondaryColor3fvEXT(fArr, i);
    }

    public void glSecondaryColor3fvEXT(FloatBuffer floatBuffer) {
        this.f10342gl.glSecondaryColor3fvEXT(floatBuffer);
    }

    public void glSecondaryColor3hNV(short s, short s2, short s3) {
        this.f10342gl.glSecondaryColor3hNV(s, s2, s3);
    }

    public void glSecondaryColor3hvNV(short[] sArr, int i) {
        this.f10342gl.glSecondaryColor3hvNV(sArr, i);
    }

    public void glSecondaryColor3hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glSecondaryColor3hvNV(shortBuffer);
    }

    public void glSecondaryColor3i(int i, int i2, int i3) {
        this.f10342gl.glSecondaryColor3i(i, i2, i3);
    }

    public void glSecondaryColor3iEXT(int i, int i2, int i3) {
        this.f10342gl.glSecondaryColor3iEXT(i, i2, i3);
    }

    public void glSecondaryColor3iv(int[] iArr, int i) {
        this.f10342gl.glSecondaryColor3iv(iArr, i);
    }

    public void glSecondaryColor3iv(IntBuffer intBuffer) {
        this.f10342gl.glSecondaryColor3iv(intBuffer);
    }

    public void glSecondaryColor3ivEXT(IntBuffer intBuffer) {
        this.f10342gl.glSecondaryColor3ivEXT(intBuffer);
    }

    public void glSecondaryColor3ivEXT(int[] iArr, int i) {
        this.f10342gl.glSecondaryColor3ivEXT(iArr, i);
    }

    public void glSecondaryColor3s(short s, short s2, short s3) {
        this.f10342gl.glSecondaryColor3s(s, s2, s3);
    }

    public void glSecondaryColor3sEXT(short s, short s2, short s3) {
        this.f10342gl.glSecondaryColor3sEXT(s, s2, s3);
    }

    public void glSecondaryColor3sv(ShortBuffer shortBuffer) {
        this.f10342gl.glSecondaryColor3sv(shortBuffer);
    }

    public void glSecondaryColor3sv(short[] sArr, int i) {
        this.f10342gl.glSecondaryColor3sv(sArr, i);
    }

    public void glSecondaryColor3svEXT(ShortBuffer shortBuffer) {
        this.f10342gl.glSecondaryColor3svEXT(shortBuffer);
    }

    public void glSecondaryColor3svEXT(short[] sArr, int i) {
        this.f10342gl.glSecondaryColor3svEXT(sArr, i);
    }

    public void glSecondaryColor3ub(byte b, byte b2, byte b3) {
        this.f10342gl.glSecondaryColor3ub(b, b2, b3);
    }

    public void glSecondaryColor3ubEXT(byte b, byte b2, byte b3) {
        this.f10342gl.glSecondaryColor3ubEXT(b, b2, b3);
    }

    public void glSecondaryColor3ubv(byte[] bArr, int i) {
        this.f10342gl.glSecondaryColor3ubv(bArr, i);
    }

    public void glSecondaryColor3ubv(ByteBuffer byteBuffer) {
        this.f10342gl.glSecondaryColor3ubv(byteBuffer);
    }

    public void glSecondaryColor3ubvEXT(byte[] bArr, int i) {
        this.f10342gl.glSecondaryColor3ubvEXT(bArr, i);
    }

    public void glSecondaryColor3ubvEXT(ByteBuffer byteBuffer) {
        this.f10342gl.glSecondaryColor3ubvEXT(byteBuffer);
    }

    public void glSecondaryColor3ui(int i, int i2, int i3) {
        this.f10342gl.glSecondaryColor3ui(i, i2, i3);
    }

    public void glSecondaryColor3uiEXT(int i, int i2, int i3) {
        this.f10342gl.glSecondaryColor3uiEXT(i, i2, i3);
    }

    public void glSecondaryColor3uiv(IntBuffer intBuffer) {
        this.f10342gl.glSecondaryColor3uiv(intBuffer);
    }

    public void glSecondaryColor3uiv(int[] iArr, int i) {
        this.f10342gl.glSecondaryColor3uiv(iArr, i);
    }

    public void glSecondaryColor3uivEXT(int[] iArr, int i) {
        this.f10342gl.glSecondaryColor3uivEXT(iArr, i);
    }

    public void glSecondaryColor3uivEXT(IntBuffer intBuffer) {
        this.f10342gl.glSecondaryColor3uivEXT(intBuffer);
    }

    public void glSecondaryColor3us(short s, short s2, short s3) {
        this.f10342gl.glSecondaryColor3us(s, s2, s3);
    }

    public void glSecondaryColor3usEXT(short s, short s2, short s3) {
        this.f10342gl.glSecondaryColor3usEXT(s, s2, s3);
    }

    public void glSecondaryColor3usv(ShortBuffer shortBuffer) {
        this.f10342gl.glSecondaryColor3usv(shortBuffer);
    }

    public void glSecondaryColor3usv(short[] sArr, int i) {
        this.f10342gl.glSecondaryColor3usv(sArr, i);
    }

    public void glSecondaryColor3usvEXT(short[] sArr, int i) {
        this.f10342gl.glSecondaryColor3usvEXT(sArr, i);
    }

    public void glSecondaryColor3usvEXT(ShortBuffer shortBuffer) {
        this.f10342gl.glSecondaryColor3usvEXT(shortBuffer);
    }

    public void glSecondaryColorPointer(int i, int i2, int i3, long j) {
        this.f10342gl.glSecondaryColorPointer(i, i2, i3, j);
    }

    public void glSecondaryColorPointer(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glSecondaryColorPointer(i, i2, i3, buffer);
    }

    public void glSecondaryColorPointerEXT(int i, int i2, int i3, long j) {
        this.f10342gl.glSecondaryColorPointerEXT(i, i2, i3, j);
    }

    public void glSecondaryColorPointerEXT(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glSecondaryColorPointerEXT(i, i2, i3, buffer);
    }

    public void glSelectBuffer(int i, IntBuffer intBuffer) {
        this.f10342gl.glSelectBuffer(i, intBuffer);
    }

    public void glSeparableFilter2D(int i, int i2, int i3, int i4, int i5, int i6, long j, long j2) {
        this.f10342gl.glSeparableFilter2D(i, i2, i3, i4, i5, i6, j, j2);
    }

    public void glSeparableFilter2D(int i, int i2, int i3, int i4, int i5, int i6, Buffer buffer, Buffer buffer2) {
        this.f10342gl.glSeparableFilter2D(i, i2, i3, i4, i5, i6, buffer, buffer2);
    }

    public void glSetFenceAPPLE(int i) {
        this.f10342gl.glSetFenceAPPLE(i);
    }

    public void glSetFenceNV(int i, int i2) {
        this.f10342gl.glSetFenceNV(i, i2);
    }

    public void glSetFragmentShaderConstantATI(int i, float[] fArr, int i2) {
        this.f10342gl.glSetFragmentShaderConstantATI(i, fArr, i2);
    }

    public void glSetFragmentShaderConstantATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glSetFragmentShaderConstantATI(i, floatBuffer);
    }

    public void glSetInvariantEXT(int i, int i2, Buffer buffer) {
        this.f10342gl.glSetInvariantEXT(i, i2, buffer);
    }

    public void glSetLocalConstantEXT(int i, int i2, Buffer buffer) {
        this.f10342gl.glSetLocalConstantEXT(i, i2, buffer);
    }

    public void glShadeModel(int i) {
        this.f10342gl.glShadeModel(i);
    }

    public void glShaderOp1EXT(int i, int i2, int i3) {
        this.f10342gl.glShaderOp1EXT(i, i2, i3);
    }

    public void glShaderOp2EXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glShaderOp2EXT(i, i2, i3, i4);
    }

    public void glShaderOp3EXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glShaderOp3EXT(i, i2, i3, i4, i5);
    }

    public void glShaderSource(int i, int i2, String[] strArr, int[] iArr, int i3) {
        this.f10342gl.glShaderSource(i, i2, strArr, iArr, i3);
    }

    public void glShaderSource(int i, int i2, String[] strArr, IntBuffer intBuffer) {
        this.f10342gl.glShaderSource(i, i2, strArr, intBuffer);
    }

    public void glShaderSourceARB(int i, int i2, String[] strArr, IntBuffer intBuffer) {
        this.f10342gl.glShaderSourceARB(i, i2, strArr, intBuffer);
    }

    public void glShaderSourceARB(int i, int i2, String[] strArr, int[] iArr, int i3) {
        this.f10342gl.glShaderSourceARB(i, i2, strArr, iArr, i3);
    }

    public void glSharpenTexFuncSGIS(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glSharpenTexFuncSGIS(i, i2, floatBuffer);
    }

    public void glSharpenTexFuncSGIS(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glSharpenTexFuncSGIS(i, i2, fArr, i3);
    }

    public void glSpriteParameterfSGIX(int i, float f) {
        this.f10342gl.glSpriteParameterfSGIX(i, f);
    }

    public void glSpriteParameterfvSGIX(int i, float[] fArr, int i2) {
        this.f10342gl.glSpriteParameterfvSGIX(i, fArr, i2);
    }

    public void glSpriteParameterfvSGIX(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glSpriteParameterfvSGIX(i, floatBuffer);
    }

    public void glSpriteParameteriSGIX(int i, int i2) {
        this.f10342gl.glSpriteParameteriSGIX(i, i2);
    }

    public void glSpriteParameterivSGIX(int i, int[] iArr, int i2) {
        this.f10342gl.glSpriteParameterivSGIX(i, iArr, i2);
    }

    public void glSpriteParameterivSGIX(int i, IntBuffer intBuffer) {
        this.f10342gl.glSpriteParameterivSGIX(i, intBuffer);
    }

    public void glStartInstrumentsSGIX() {
        this.f10342gl.glStartInstrumentsSGIX();
    }

    public void glStencilClearTagEXT(int i, int i2) {
        this.f10342gl.glStencilClearTagEXT(i, i2);
    }

    public void glStencilFunc(int i, int i2, int i3) {
        this.f10342gl.glStencilFunc(i, i2, i3);
    }

    public void glStencilFuncSeparate(int i, int i2, int i3, int i4) {
        this.f10342gl.glStencilFuncSeparate(i, i2, i3, i4);
    }

    public void glStencilFuncSeparateATI(int i, int i2, int i3, int i4) {
        this.f10342gl.glStencilFuncSeparateATI(i, i2, i3, i4);
    }

    public void glStencilMask(int i) {
        this.f10342gl.glStencilMask(i);
    }

    public void glStencilMaskSeparate(int i, int i2) {
        this.f10342gl.glStencilMaskSeparate(i, i2);
    }

    public void glStencilOp(int i, int i2, int i3) {
        this.f10342gl.glStencilOp(i, i2, i3);
    }

    public void glStencilOpSeparate(int i, int i2, int i3, int i4) {
        this.f10342gl.glStencilOpSeparate(i, i2, i3, i4);
    }

    public void glStencilOpSeparateATI(int i, int i2, int i3, int i4) {
        this.f10342gl.glStencilOpSeparateATI(i, i2, i3, i4);
    }

    public void glStopInstrumentsSGIX(int i) {
        this.f10342gl.glStopInstrumentsSGIX(i);
    }

    public void glStringMarkerGREMEDY(int i, Buffer buffer) {
        this.f10342gl.glStringMarkerGREMEDY(i, buffer);
    }

    public void glSwapAPPLE() {
        this.f10342gl.glSwapAPPLE();
    }

    public void glSwizzleEXT(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glSwizzleEXT(i, i2, i3, i4, i5, i6);
    }

    public void glTagSampleBufferSGIX() {
        this.f10342gl.glTagSampleBufferSGIX();
    }

    public void glTbufferMask3DFX(int i) {
        this.f10342gl.glTbufferMask3DFX(i);
    }

    public boolean glTestFenceAPPLE(int i) {
        return this.f10342gl.glTestFenceAPPLE(i);
    }

    public boolean glTestFenceNV(int i) {
        return this.f10342gl.glTestFenceNV(i);
    }

    public boolean glTestObjectAPPLE(int i, int i2) {
        return this.f10342gl.glTestObjectAPPLE(i, i2);
    }

    public void glTexBufferEXT(int i, int i2, int i3) {
        this.f10342gl.glTexBufferEXT(i, i2, i3);
    }

    public void glTexBumpParameterfvATI(int i, float[] fArr, int i2) {
        this.f10342gl.glTexBumpParameterfvATI(i, fArr, i2);
    }

    public void glTexBumpParameterfvATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glTexBumpParameterfvATI(i, floatBuffer);
    }

    public void glTexBumpParameterivATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glTexBumpParameterivATI(i, intBuffer);
    }

    public void glTexBumpParameterivATI(int i, int[] iArr, int i2) {
        this.f10342gl.glTexBumpParameterivATI(i, iArr, i2);
    }

    public void glTexCoord1d(double d) {
        this.f10342gl.glTexCoord1d(d);
    }

    public void glTexCoord1dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glTexCoord1dv(doubleBuffer);
    }

    public void glTexCoord1dv(double[] dArr, int i) {
        this.f10342gl.glTexCoord1dv(dArr, i);
    }

    public void glTexCoord1f(float f) {
        this.f10342gl.glTexCoord1f(f);
    }

    public void glTexCoord1fv(FloatBuffer floatBuffer) {
        this.f10342gl.glTexCoord1fv(floatBuffer);
    }

    public void glTexCoord1fv(float[] fArr, int i) {
        this.f10342gl.glTexCoord1fv(fArr, i);
    }

    public void glTexCoord1hNV(short s) {
        this.f10342gl.glTexCoord1hNV(s);
    }

    public void glTexCoord1hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord1hvNV(shortBuffer);
    }

    public void glTexCoord1hvNV(short[] sArr, int i) {
        this.f10342gl.glTexCoord1hvNV(sArr, i);
    }

    public void glTexCoord1i(int i) {
        this.f10342gl.glTexCoord1i(i);
    }

    public void glTexCoord1iv(IntBuffer intBuffer) {
        this.f10342gl.glTexCoord1iv(intBuffer);
    }

    public void glTexCoord1iv(int[] iArr, int i) {
        this.f10342gl.glTexCoord1iv(iArr, i);
    }

    public void glTexCoord1s(short s) {
        this.f10342gl.glTexCoord1s(s);
    }

    public void glTexCoord1sv(short[] sArr, int i) {
        this.f10342gl.glTexCoord1sv(sArr, i);
    }

    public void glTexCoord1sv(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord1sv(shortBuffer);
    }

    public void glTexCoord2d(double d, double d2) {
        this.f10342gl.glTexCoord2d(d, d2);
    }

    public void glTexCoord2dv(double[] dArr, int i) {
        this.f10342gl.glTexCoord2dv(dArr, i);
    }

    public void glTexCoord2dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glTexCoord2dv(doubleBuffer);
    }

    public void glTexCoord2f(float f, float f2) {
        this.f10342gl.glTexCoord2f(f, f2);
    }

    public void glTexCoord2fColor3fVertex3fSUN(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        this.f10342gl.glTexCoord2fColor3fVertex3fSUN(f, f2, f3, f4, f5, f6, f7, f8);
    }

    public void glTexCoord2fColor3fVertex3fvSUN(float[] fArr, int i, float[] fArr2, int i2, float[] fArr3, int i3) {
        this.f10342gl.glTexCoord2fColor3fVertex3fvSUN(fArr, i, fArr2, i2, fArr3, i3);
    }

    public void glTexCoord2fColor3fVertex3fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3) {
        this.f10342gl.glTexCoord2fColor3fVertex3fvSUN(floatBuffer, floatBuffer2, floatBuffer3);
    }

    public void glTexCoord2fColor4fNormal3fVertex3fSUN(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12) {
        this.f10342gl.glTexCoord2fColor4fNormal3fVertex3fSUN(f, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12);
    }

    public void glTexCoord2fColor4fNormal3fVertex3fvSUN(float[] fArr, int i, float[] fArr2, int i2, float[] fArr3, int i3, float[] fArr4, int i4) {
        this.f10342gl.glTexCoord2fColor4fNormal3fVertex3fvSUN(fArr, i, fArr2, i2, fArr3, i3, fArr4, i4);
    }

    public void glTexCoord2fColor4fNormal3fVertex3fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3, FloatBuffer floatBuffer4) {
        this.f10342gl.glTexCoord2fColor4fNormal3fVertex3fvSUN(floatBuffer, floatBuffer2, floatBuffer3, floatBuffer4);
    }

    public void glTexCoord2fColor4ubVertex3fSUN(float f, float f2, byte b, byte b2, byte b3, byte b4, float f3, float f4, float f5) {
        this.f10342gl.glTexCoord2fColor4ubVertex3fSUN(f, f2, b, b2, b3, b4, f3, f4, f5);
    }

    public void glTexCoord2fColor4ubVertex3fvSUN(FloatBuffer floatBuffer, ByteBuffer byteBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glTexCoord2fColor4ubVertex3fvSUN(floatBuffer, byteBuffer, floatBuffer2);
    }

    public void glTexCoord2fColor4ubVertex3fvSUN(float[] fArr, int i, byte[] bArr, int i2, float[] fArr2, int i3) {
        this.f10342gl.glTexCoord2fColor4ubVertex3fvSUN(fArr, i, bArr, i2, fArr2, i3);
    }

    public void glTexCoord2fNormal3fVertex3fSUN(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        this.f10342gl.glTexCoord2fNormal3fVertex3fSUN(f, f2, f3, f4, f5, f6, f7, f8);
    }

    public void glTexCoord2fNormal3fVertex3fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3) {
        this.f10342gl.glTexCoord2fNormal3fVertex3fvSUN(floatBuffer, floatBuffer2, floatBuffer3);
    }

    public void glTexCoord2fNormal3fVertex3fvSUN(float[] fArr, int i, float[] fArr2, int i2, float[] fArr3, int i3) {
        this.f10342gl.glTexCoord2fNormal3fVertex3fvSUN(fArr, i, fArr2, i2, fArr3, i3);
    }

    public void glTexCoord2fVertex3fSUN(float f, float f2, float f3, float f4, float f5) {
        this.f10342gl.glTexCoord2fVertex3fSUN(f, f2, f3, f4, f5);
    }

    public void glTexCoord2fVertex3fvSUN(float[] fArr, int i, float[] fArr2, int i2) {
        this.f10342gl.glTexCoord2fVertex3fvSUN(fArr, i, fArr2, i2);
    }

    public void glTexCoord2fVertex3fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glTexCoord2fVertex3fvSUN(floatBuffer, floatBuffer2);
    }

    public void glTexCoord2fv(FloatBuffer floatBuffer) {
        this.f10342gl.glTexCoord2fv(floatBuffer);
    }

    public void glTexCoord2fv(float[] fArr, int i) {
        this.f10342gl.glTexCoord2fv(fArr, i);
    }

    public void glTexCoord2hNV(short s, short s2) {
        this.f10342gl.glTexCoord2hNV(s, s2);
    }

    public void glTexCoord2hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord2hvNV(shortBuffer);
    }

    public void glTexCoord2hvNV(short[] sArr, int i) {
        this.f10342gl.glTexCoord2hvNV(sArr, i);
    }

    public void glTexCoord2i(int i, int i2) {
        this.f10342gl.glTexCoord2i(i, i2);
    }

    public void glTexCoord2iv(IntBuffer intBuffer) {
        this.f10342gl.glTexCoord2iv(intBuffer);
    }

    public void glTexCoord2iv(int[] iArr, int i) {
        this.f10342gl.glTexCoord2iv(iArr, i);
    }

    public void glTexCoord2s(short s, short s2) {
        this.f10342gl.glTexCoord2s(s, s2);
    }

    public void glTexCoord2sv(short[] sArr, int i) {
        this.f10342gl.glTexCoord2sv(sArr, i);
    }

    public void glTexCoord2sv(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord2sv(shortBuffer);
    }

    public void glTexCoord3d(double d, double d2, double d3) {
        this.f10342gl.glTexCoord3d(d, d2, d3);
    }

    public void glTexCoord3dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glTexCoord3dv(doubleBuffer);
    }

    public void glTexCoord3dv(double[] dArr, int i) {
        this.f10342gl.glTexCoord3dv(dArr, i);
    }

    public void glTexCoord3f(float f, float f2, float f3) {
        this.f10342gl.glTexCoord3f(f, f2, f3);
    }

    public void glTexCoord3fv(FloatBuffer floatBuffer) {
        this.f10342gl.glTexCoord3fv(floatBuffer);
    }

    public void glTexCoord3fv(float[] fArr, int i) {
        this.f10342gl.glTexCoord3fv(fArr, i);
    }

    public void glTexCoord3hNV(short s, short s2, short s3) {
        this.f10342gl.glTexCoord3hNV(s, s2, s3);
    }

    public void glTexCoord3hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord3hvNV(shortBuffer);
    }

    public void glTexCoord3hvNV(short[] sArr, int i) {
        this.f10342gl.glTexCoord3hvNV(sArr, i);
    }

    public void glTexCoord3i(int i, int i2, int i3) {
        this.f10342gl.glTexCoord3i(i, i2, i3);
    }

    public void glTexCoord3iv(int[] iArr, int i) {
        this.f10342gl.glTexCoord3iv(iArr, i);
    }

    public void glTexCoord3iv(IntBuffer intBuffer) {
        this.f10342gl.glTexCoord3iv(intBuffer);
    }

    public void glTexCoord3s(short s, short s2, short s3) {
        this.f10342gl.glTexCoord3s(s, s2, s3);
    }

    public void glTexCoord3sv(short[] sArr, int i) {
        this.f10342gl.glTexCoord3sv(sArr, i);
    }

    public void glTexCoord3sv(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord3sv(shortBuffer);
    }

    public void glTexCoord4d(double d, double d2, double d3, double d4) {
        this.f10342gl.glTexCoord4d(d, d2, d3, d4);
    }

    public void glTexCoord4dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glTexCoord4dv(doubleBuffer);
    }

    public void glTexCoord4dv(double[] dArr, int i) {
        this.f10342gl.glTexCoord4dv(dArr, i);
    }

    public void glTexCoord4f(float f, float f2, float f3, float f4) {
        this.f10342gl.glTexCoord4f(f, f2, f3, f4);
    }

    public void glTexCoord4fColor4fNormal3fVertex4fSUN(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15) {
        this.f10342gl.glTexCoord4fColor4fNormal3fVertex4fSUN(f, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15);
    }

    public void glTexCoord4fColor4fNormal3fVertex4fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2, FloatBuffer floatBuffer3, FloatBuffer floatBuffer4) {
        this.f10342gl.glTexCoord4fColor4fNormal3fVertex4fvSUN(floatBuffer, floatBuffer2, floatBuffer3, floatBuffer4);
    }

    public void glTexCoord4fColor4fNormal3fVertex4fvSUN(float[] fArr, int i, float[] fArr2, int i2, float[] fArr3, int i3, float[] fArr4, int i4) {
        this.f10342gl.glTexCoord4fColor4fNormal3fVertex4fvSUN(fArr, i, fArr2, i2, fArr3, i3, fArr4, i4);
    }

    public void glTexCoord4fVertex4fSUN(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        this.f10342gl.glTexCoord4fVertex4fSUN(f, f2, f3, f4, f5, f6, f7, f8);
    }

    public void glTexCoord4fVertex4fvSUN(FloatBuffer floatBuffer, FloatBuffer floatBuffer2) {
        this.f10342gl.glTexCoord4fVertex4fvSUN(floatBuffer, floatBuffer2);
    }

    public void glTexCoord4fVertex4fvSUN(float[] fArr, int i, float[] fArr2, int i2) {
        this.f10342gl.glTexCoord4fVertex4fvSUN(fArr, i, fArr2, i2);
    }

    public void glTexCoord4fv(float[] fArr, int i) {
        this.f10342gl.glTexCoord4fv(fArr, i);
    }

    public void glTexCoord4fv(FloatBuffer floatBuffer) {
        this.f10342gl.glTexCoord4fv(floatBuffer);
    }

    public void glTexCoord4hNV(short s, short s2, short s3, short s4) {
        this.f10342gl.glTexCoord4hNV(s, s2, s3, s4);
    }

    public void glTexCoord4hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord4hvNV(shortBuffer);
    }

    public void glTexCoord4hvNV(short[] sArr, int i) {
        this.f10342gl.glTexCoord4hvNV(sArr, i);
    }

    public void glTexCoord4i(int i, int i2, int i3, int i4) {
        this.f10342gl.glTexCoord4i(i, i2, i3, i4);
    }

    public void glTexCoord4iv(int[] iArr, int i) {
        this.f10342gl.glTexCoord4iv(iArr, i);
    }

    public void glTexCoord4iv(IntBuffer intBuffer) {
        this.f10342gl.glTexCoord4iv(intBuffer);
    }

    public void glTexCoord4s(short s, short s2, short s3, short s4) {
        this.f10342gl.glTexCoord4s(s, s2, s3, s4);
    }

    public void glTexCoord4sv(short[] sArr, int i) {
        this.f10342gl.glTexCoord4sv(sArr, i);
    }

    public void glTexCoord4sv(ShortBuffer shortBuffer) {
        this.f10342gl.glTexCoord4sv(shortBuffer);
    }

    public void glTexCoordPointer(int i, int i2, int i3, long j) {
        this.f10342gl.glTexCoordPointer(i, i2, i3, j);
    }

    public void glTexCoordPointer(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glTexCoordPointer(i, i2, i3, buffer);
    }

    public void glTexEnvf(int i, int i2, float f) {
        this.f10342gl.glTexEnvf(i, i2, f);
    }

    public void glTexEnvfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glTexEnvfv(i, i2, floatBuffer);
    }

    public void glTexEnvfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glTexEnvfv(i, i2, fArr, i3);
    }

    public void glTexEnvi(int i, int i2, int i3) {
        this.f10342gl.glTexEnvi(i, i2, i3);
    }

    public void glTexEnviv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glTexEnviv(i, i2, intBuffer);
    }

    public void glTexEnviv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glTexEnviv(i, i2, iArr, i3);
    }

    public void glTexFilterFuncSGIS(int i, int i2, int i3, FloatBuffer floatBuffer) {
        this.f10342gl.glTexFilterFuncSGIS(i, i2, i3, floatBuffer);
    }

    public void glTexFilterFuncSGIS(int i, int i2, int i3, float[] fArr, int i4) {
        this.f10342gl.glTexFilterFuncSGIS(i, i2, i3, fArr, i4);
    }

    public void glTexGend(int i, int i2, double d) {
        this.f10342gl.glTexGend(i, i2, d);
    }

    public void glTexGendv(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glTexGendv(i, i2, dArr, i3);
    }

    public void glTexGendv(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glTexGendv(i, i2, doubleBuffer);
    }

    public void glTexGenf(int i, int i2, float f) {
        this.f10342gl.glTexGenf(i, i2, f);
    }

    public void glTexGenfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glTexGenfv(i, i2, floatBuffer);
    }

    public void glTexGenfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glTexGenfv(i, i2, fArr, i3);
    }

    public void glTexGeni(int i, int i2, int i3) {
        this.f10342gl.glTexGeni(i, i2, i3);
    }

    public void glTexGeniv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glTexGeniv(i, i2, iArr, i3);
    }

    public void glTexGeniv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glTexGeniv(i, i2, intBuffer);
    }

    public void glTexImage1D(int i, int i2, int i3, int i4, int i5, int i6, int i7, long j) {
        this.f10342gl.glTexImage1D(i, i2, i3, i4, i5, i6, i7, j);
    }

    public void glTexImage1D(int i, int i2, int i3, int i4, int i5, int i6, int i7, Buffer buffer) {
        this.f10342gl.glTexImage1D(i, i2, i3, i4, i5, i6, i7, buffer);
    }

    public void glTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j) {
        this.f10342gl.glTexImage2D(i, i2, i3, i4, i5, i6, i7, i8, j);
    }

    public void glTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        this.f10342gl.glTexImage2D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
    }

    public void glTexImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, long j) {
        this.f10342gl.glTexImage3D(i, i2, i3, i4, i5, i6, i7, i8, i9, j);
    }

    public void glTexImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, Buffer buffer) {
        this.f10342gl.glTexImage3D(i, i2, i3, i4, i5, i6, i7, i8, i9, buffer);
    }

    public void glTexImage4DSGIS(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, Buffer buffer) {
        this.f10342gl.glTexImage4DSGIS(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, buffer);
    }

    public void glTexParameterIivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glTexParameterIivEXT(i, i2, intBuffer);
    }

    public void glTexParameterIivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glTexParameterIivEXT(i, i2, iArr, i3);
    }

    public void glTexParameterIuivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glTexParameterIuivEXT(i, i2, iArr, i3);
    }

    public void glTexParameterIuivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glTexParameterIuivEXT(i, i2, intBuffer);
    }

    public void glTexParameterf(int i, int i2, float f) {
        this.f10342gl.glTexParameterf(i, i2, f);
    }

    public void glTexParameterfv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glTexParameterfv(i, i2, fArr, i3);
    }

    public void glTexParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glTexParameterfv(i, i2, floatBuffer);
    }

    public void glTexParameteri(int i, int i2, int i3) {
        this.f10342gl.glTexParameteri(i, i2, i3);
    }

    public void glTexParameteriv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glTexParameteriv(i, i2, intBuffer);
    }

    public void glTexParameteriv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glTexParameteriv(i, i2, iArr, i3);
    }

    public void glTexSubImage1D(int i, int i2, int i3, int i4, int i5, int i6, long j) {
        this.f10342gl.glTexSubImage1D(i, i2, i3, i4, i5, i6, j);
    }

    public void glTexSubImage1D(int i, int i2, int i3, int i4, int i5, int i6, Buffer buffer) {
        this.f10342gl.glTexSubImage1D(i, i2, i3, i4, i5, i6, buffer);
    }

    public void glTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j) {
        this.f10342gl.glTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8, j);
    }

    public void glTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        this.f10342gl.glTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
    }

    public void glTexSubImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, long j) {
        this.f10342gl.glTexSubImage3D(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, j);
    }

    public void glTexSubImage3D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, Buffer buffer) {
        this.f10342gl.glTexSubImage3D(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, buffer);
    }

    public void glTexSubImage4DSGIS(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, Buffer buffer) {
        this.f10342gl.glTexSubImage4DSGIS(i, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, buffer);
    }

    public void glTextureColorMaskSGIS(boolean z, boolean z2, boolean z3, boolean z4) {
        this.f10342gl.glTextureColorMaskSGIS(z, z2, z3, z4);
    }

    public void glTextureLightEXT(int i) {
        this.f10342gl.glTextureLightEXT(i);
    }

    public void glTextureMaterialEXT(int i, int i2) {
        this.f10342gl.glTextureMaterialEXT(i, i2);
    }

    public void glTextureNormalEXT(int i) {
        this.f10342gl.glTextureNormalEXT(i);
    }

    public void glTextureRangeAPPLE(int i, int i2, Buffer buffer) {
        this.f10342gl.glTextureRangeAPPLE(i, i2, buffer);
    }

    public void glTrackMatrixNV(int i, int i2, int i3, int i4) {
        this.f10342gl.glTrackMatrixNV(i, i2, i3, i4);
    }

    public void glTransformFeedbackAttribsNV(int i, int[] iArr, int i2, int i3) {
        this.f10342gl.glTransformFeedbackAttribsNV(i, iArr, i2, i3);
    }

    public void glTransformFeedbackAttribsNV(int i, IntBuffer intBuffer, int i2) {
        this.f10342gl.glTransformFeedbackAttribsNV(i, intBuffer, i2);
    }

    public void glTransformFeedbackVaryingsNV(int i, int i2, int[] iArr, int i3, int i4) {
        this.f10342gl.glTransformFeedbackVaryingsNV(i, i2, iArr, i3, i4);
    }

    public void glTransformFeedbackVaryingsNV(int i, int i2, IntBuffer intBuffer, int i3) {
        this.f10342gl.glTransformFeedbackVaryingsNV(i, i2, intBuffer, i3);
    }

    public void glTranslated(double d, double d2, double d3) {
        this.f10342gl.glTranslated(d, d2, d3);
    }

    public void glTranslatef(float f, float f2, float f3) {
        this.f10342gl.glTranslatef(f, f2, f3);
    }

    public void glUniform1f(int i, float f) {
        this.f10342gl.glUniform1f(i, f);
    }

    public void glUniform1fARB(int i, float f) {
        this.f10342gl.glUniform1fARB(i, f);
    }

    public void glUniform1fv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform1fv(i, i2, floatBuffer);
    }

    public void glUniform1fv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform1fv(i, i2, fArr, i3);
    }

    public void glUniform1fvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform1fvARB(i, i2, fArr, i3);
    }

    public void glUniform1fvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform1fvARB(i, i2, floatBuffer);
    }

    public void glUniform1i(int i, int i2) {
        this.f10342gl.glUniform1i(i, i2);
    }

    public void glUniform1iARB(int i, int i2) {
        this.f10342gl.glUniform1iARB(i, i2);
    }

    public void glUniform1iv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform1iv(i, i2, intBuffer);
    }

    public void glUniform1iv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform1iv(i, i2, iArr, i3);
    }

    public void glUniform1ivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform1ivARB(i, i2, intBuffer);
    }

    public void glUniform1ivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform1ivARB(i, i2, iArr, i3);
    }

    public void glUniform1uiEXT(int i, int i2) {
        this.f10342gl.glUniform1uiEXT(i, i2);
    }

    public void glUniform1uivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform1uivEXT(i, i2, iArr, i3);
    }

    public void glUniform1uivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform1uivEXT(i, i2, intBuffer);
    }

    public void glUniform2f(int i, float f, float f2) {
        this.f10342gl.glUniform2f(i, f, f2);
    }

    public void glUniform2fARB(int i, float f, float f2) {
        this.f10342gl.glUniform2fARB(i, f, f2);
    }

    public void glUniform2fv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform2fv(i, i2, fArr, i3);
    }

    public void glUniform2fv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform2fv(i, i2, floatBuffer);
    }

    public void glUniform2fvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform2fvARB(i, i2, floatBuffer);
    }

    public void glUniform2fvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform2fvARB(i, i2, fArr, i3);
    }

    public void glUniform2i(int i, int i2, int i3) {
        this.f10342gl.glUniform2i(i, i2, i3);
    }

    public void glUniform2iARB(int i, int i2, int i3) {
        this.f10342gl.glUniform2iARB(i, i2, i3);
    }

    public void glUniform2iv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform2iv(i, i2, intBuffer);
    }

    public void glUniform2iv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform2iv(i, i2, iArr, i3);
    }

    public void glUniform2ivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform2ivARB(i, i2, iArr, i3);
    }

    public void glUniform2ivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform2ivARB(i, i2, intBuffer);
    }

    public void glUniform2uiEXT(int i, int i2, int i3) {
        this.f10342gl.glUniform2uiEXT(i, i2, i3);
    }

    public void glUniform2uivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform2uivEXT(i, i2, iArr, i3);
    }

    public void glUniform2uivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform2uivEXT(i, i2, intBuffer);
    }

    public void glUniform3f(int i, float f, float f2, float f3) {
        this.f10342gl.glUniform3f(i, f, f2, f3);
    }

    public void glUniform3fARB(int i, float f, float f2, float f3) {
        this.f10342gl.glUniform3fARB(i, f, f2, f3);
    }

    public void glUniform3fv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform3fv(i, i2, floatBuffer);
    }

    public void glUniform3fv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform3fv(i, i2, fArr, i3);
    }

    public void glUniform3fvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform3fvARB(i, i2, fArr, i3);
    }

    public void glUniform3fvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform3fvARB(i, i2, floatBuffer);
    }

    public void glUniform3i(int i, int i2, int i3, int i4) {
        this.f10342gl.glUniform3i(i, i2, i3, i4);
    }

    public void glUniform3iARB(int i, int i2, int i3, int i4) {
        this.f10342gl.glUniform3iARB(i, i2, i3, i4);
    }

    public void glUniform3iv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform3iv(i, i2, intBuffer);
    }

    public void glUniform3iv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform3iv(i, i2, iArr, i3);
    }

    public void glUniform3ivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform3ivARB(i, i2, intBuffer);
    }

    public void glUniform3ivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform3ivARB(i, i2, iArr, i3);
    }

    public void glUniform3uiEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glUniform3uiEXT(i, i2, i3, i4);
    }

    public void glUniform3uivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform3uivEXT(i, i2, intBuffer);
    }

    public void glUniform3uivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform3uivEXT(i, i2, iArr, i3);
    }

    public void glUniform4f(int i, float f, float f2, float f3, float f4) {
        this.f10342gl.glUniform4f(i, f, f2, f3, f4);
    }

    public void glUniform4fARB(int i, float f, float f2, float f3, float f4) {
        this.f10342gl.glUniform4fARB(i, f, f2, f3, f4);
    }

    public void glUniform4fv(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform4fv(i, i2, fArr, i3);
    }

    public void glUniform4fv(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform4fv(i, i2, floatBuffer);
    }

    public void glUniform4fvARB(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glUniform4fvARB(i, i2, floatBuffer);
    }

    public void glUniform4fvARB(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glUniform4fvARB(i, i2, fArr, i3);
    }

    public void glUniform4i(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glUniform4i(i, i2, i3, i4, i5);
    }

    public void glUniform4iARB(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glUniform4iARB(i, i2, i3, i4, i5);
    }

    public void glUniform4iv(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform4iv(i, i2, iArr, i3);
    }

    public void glUniform4iv(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform4iv(i, i2, intBuffer);
    }

    public void glUniform4ivARB(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform4ivARB(i, i2, iArr, i3);
    }

    public void glUniform4ivARB(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform4ivARB(i, i2, intBuffer);
    }

    public void glUniform4uiEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glUniform4uiEXT(i, i2, i3, i4, i5);
    }

    public void glUniform4uivEXT(int i, int i2, int[] iArr, int i3) {
        this.f10342gl.glUniform4uivEXT(i, i2, iArr, i3);
    }

    public void glUniform4uivEXT(int i, int i2, IntBuffer intBuffer) {
        this.f10342gl.glUniform4uivEXT(i, i2, intBuffer);
    }

    public void glUniformBufferEXT(int i, int i2, int i3) {
        this.f10342gl.glUniformBufferEXT(i, i2, i3);
    }

    public void glUniformMatrix2fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix2fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix2fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix2fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix2fvARB(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix2fvARB(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix2fvARB(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix2fvARB(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix2x3fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix2x3fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix2x3fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix2x3fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix2x4fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix2x4fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix2x4fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix2x4fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix3fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix3fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix3fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix3fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix3fvARB(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix3fvARB(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix3fvARB(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix3fvARB(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix3x2fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix3x2fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix3x2fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix3x2fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix3x4fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix3x4fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix3x4fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix3x4fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix4fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix4fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix4fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix4fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix4fvARB(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix4fvARB(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix4fvARB(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix4fvARB(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix4x2fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix4x2fv(i, i2, z, fArr, i3);
    }

    public void glUniformMatrix4x2fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix4x2fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix4x3fv(int i, int i2, boolean z, FloatBuffer floatBuffer) {
        this.f10342gl.glUniformMatrix4x3fv(i, i2, z, floatBuffer);
    }

    public void glUniformMatrix4x3fv(int i, int i2, boolean z, float[] fArr, int i3) {
        this.f10342gl.glUniformMatrix4x3fv(i, i2, z, fArr, i3);
    }

    public void glUnlockArraysEXT() {
        this.f10342gl.glUnlockArraysEXT();
    }

    public boolean glUnmapBuffer(int i) {
        return this.f10342gl.glUnmapBuffer(i);
    }

    public boolean glUnmapBufferARB(int i) {
        return this.f10342gl.glUnmapBufferARB(i);
    }

    public void glUpdateObjectBufferATI(int i, int i2, int i3, Buffer buffer, int i4) {
        this.f10342gl.glUpdateObjectBufferATI(i, i2, i3, buffer, i4);
    }

    public void glUseProgram(int i) {
        this.f10342gl.glUseProgram(i);
    }

    public void glUseProgramObjectARB(int i) {
        this.f10342gl.glUseProgramObjectARB(i);
    }

    public void glValidateProgram(int i) {
        this.f10342gl.glValidateProgram(i);
    }

    public void glValidateProgramARB(int i) {
        this.f10342gl.glValidateProgramARB(i);
    }

    public void glVariantArrayObjectATI(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glVariantArrayObjectATI(i, i2, i3, i4, i5);
    }

    public void glVariantPointerEXT(int i, int i2, int i3, long j) {
        this.f10342gl.glVariantPointerEXT(i, i2, i3, j);
    }

    public void glVariantPointerEXT(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glVariantPointerEXT(i, i2, i3, buffer);
    }

    public void glVariantbvEXT(int i, byte[] bArr, int i2) {
        this.f10342gl.glVariantbvEXT(i, bArr, i2);
    }

    public void glVariantbvEXT(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVariantbvEXT(i, byteBuffer);
    }

    public void glVariantdvEXT(int i, double[] dArr, int i2) {
        this.f10342gl.glVariantdvEXT(i, dArr, i2);
    }

    public void glVariantdvEXT(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVariantdvEXT(i, doubleBuffer);
    }

    public void glVariantfvEXT(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVariantfvEXT(i, floatBuffer);
    }

    public void glVariantfvEXT(int i, float[] fArr, int i2) {
        this.f10342gl.glVariantfvEXT(i, fArr, i2);
    }

    public void glVariantivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVariantivEXT(i, iArr, i2);
    }

    public void glVariantivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVariantivEXT(i, intBuffer);
    }

    public void glVariantsvEXT(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVariantsvEXT(i, shortBuffer);
    }

    public void glVariantsvEXT(int i, short[] sArr, int i2) {
        this.f10342gl.glVariantsvEXT(i, sArr, i2);
    }

    public void glVariantubvEXT(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVariantubvEXT(i, byteBuffer);
    }

    public void glVariantubvEXT(int i, byte[] bArr, int i2) {
        this.f10342gl.glVariantubvEXT(i, bArr, i2);
    }

    public void glVariantuivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVariantuivEXT(i, iArr, i2);
    }

    public void glVariantuivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVariantuivEXT(i, intBuffer);
    }

    public void glVariantusvEXT(int i, short[] sArr, int i2) {
        this.f10342gl.glVariantusvEXT(i, sArr, i2);
    }

    public void glVariantusvEXT(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVariantusvEXT(i, shortBuffer);
    }

    public void glVertex2d(double d, double d2) {
        this.f10342gl.glVertex2d(d, d2);
    }

    public void glVertex2dv(double[] dArr, int i) {
        this.f10342gl.glVertex2dv(dArr, i);
    }

    public void glVertex2dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertex2dv(doubleBuffer);
    }

    public void glVertex2f(float f, float f2) {
        this.f10342gl.glVertex2f(f, f2);
    }

    public void glVertex2fv(FloatBuffer floatBuffer) {
        this.f10342gl.glVertex2fv(floatBuffer);
    }

    public void glVertex2fv(float[] fArr, int i) {
        this.f10342gl.glVertex2fv(fArr, i);
    }

    public void glVertex2hNV(short s, short s2) {
        this.f10342gl.glVertex2hNV(s, s2);
    }

    public void glVertex2hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glVertex2hvNV(shortBuffer);
    }

    public void glVertex2hvNV(short[] sArr, int i) {
        this.f10342gl.glVertex2hvNV(sArr, i);
    }

    public void glVertex2i(int i, int i2) {
        this.f10342gl.glVertex2i(i, i2);
    }

    public void glVertex2iv(int[] iArr, int i) {
        this.f10342gl.glVertex2iv(iArr, i);
    }

    public void glVertex2iv(IntBuffer intBuffer) {
        this.f10342gl.glVertex2iv(intBuffer);
    }

    public void glVertex2s(short s, short s2) {
        this.f10342gl.glVertex2s(s, s2);
    }

    public void glVertex2sv(short[] sArr, int i) {
        this.f10342gl.glVertex2sv(sArr, i);
    }

    public void glVertex2sv(ShortBuffer shortBuffer) {
        this.f10342gl.glVertex2sv(shortBuffer);
    }

    public void glVertex3d(double d, double d2, double d3) {
        this.f10342gl.glVertex3d(d, d2, d3);
    }

    public void glVertex3dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertex3dv(doubleBuffer);
    }

    public void glVertex3dv(double[] dArr, int i) {
        this.f10342gl.glVertex3dv(dArr, i);
    }

    public void glVertex3f(float f, float f2, float f3) {
        this.f10342gl.glVertex3f(f, f2, f3);
    }

    public void glVertex3fv(FloatBuffer floatBuffer) {
        this.f10342gl.glVertex3fv(floatBuffer);
    }

    public void glVertex3fv(float[] fArr, int i) {
        this.f10342gl.glVertex3fv(fArr, i);
    }

    public void glVertex3hNV(short s, short s2, short s3) {
        this.f10342gl.glVertex3hNV(s, s2, s3);
    }

    public void glVertex3hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glVertex3hvNV(shortBuffer);
    }

    public void glVertex3hvNV(short[] sArr, int i) {
        this.f10342gl.glVertex3hvNV(sArr, i);
    }

    public void glVertex3i(int i, int i2, int i3) {
        this.f10342gl.glVertex3i(i, i2, i3);
    }

    public void glVertex3iv(int[] iArr, int i) {
        this.f10342gl.glVertex3iv(iArr, i);
    }

    public void glVertex3iv(IntBuffer intBuffer) {
        this.f10342gl.glVertex3iv(intBuffer);
    }

    public void glVertex3s(short s, short s2, short s3) {
        this.f10342gl.glVertex3s(s, s2, s3);
    }

    public void glVertex3sv(ShortBuffer shortBuffer) {
        this.f10342gl.glVertex3sv(shortBuffer);
    }

    public void glVertex3sv(short[] sArr, int i) {
        this.f10342gl.glVertex3sv(sArr, i);
    }

    public void glVertex4d(double d, double d2, double d3, double d4) {
        this.f10342gl.glVertex4d(d, d2, d3, d4);
    }

    public void glVertex4dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertex4dv(doubleBuffer);
    }

    public void glVertex4dv(double[] dArr, int i) {
        this.f10342gl.glVertex4dv(dArr, i);
    }

    public void glVertex4f(float f, float f2, float f3, float f4) {
        this.f10342gl.glVertex4f(f, f2, f3, f4);
    }

    public void glVertex4fv(FloatBuffer floatBuffer) {
        this.f10342gl.glVertex4fv(floatBuffer);
    }

    public void glVertex4fv(float[] fArr, int i) {
        this.f10342gl.glVertex4fv(fArr, i);
    }

    public void glVertex4hNV(short s, short s2, short s3, short s4) {
        this.f10342gl.glVertex4hNV(s, s2, s3, s4);
    }

    public void glVertex4hvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glVertex4hvNV(shortBuffer);
    }

    public void glVertex4hvNV(short[] sArr, int i) {
        this.f10342gl.glVertex4hvNV(sArr, i);
    }

    public void glVertex4i(int i, int i2, int i3, int i4) {
        this.f10342gl.glVertex4i(i, i2, i3, i4);
    }

    public void glVertex4iv(IntBuffer intBuffer) {
        this.f10342gl.glVertex4iv(intBuffer);
    }

    public void glVertex4iv(int[] iArr, int i) {
        this.f10342gl.glVertex4iv(iArr, i);
    }

    public void glVertex4s(short s, short s2, short s3, short s4) {
        this.f10342gl.glVertex4s(s, s2, s3, s4);
    }

    public void glVertex4sv(ShortBuffer shortBuffer) {
        this.f10342gl.glVertex4sv(shortBuffer);
    }

    public void glVertex4sv(short[] sArr, int i) {
        this.f10342gl.glVertex4sv(sArr, i);
    }

    public void glVertexArrayParameteriAPPLE(int i, int i2) {
        this.f10342gl.glVertexArrayParameteriAPPLE(i, i2);
    }

    public void glVertexArrayRangeAPPLE(int i, Buffer buffer) {
        this.f10342gl.glVertexArrayRangeAPPLE(i, buffer);
    }

    public void glVertexArrayRangeNV(int i, Buffer buffer) {
        this.f10342gl.glVertexArrayRangeNV(i, buffer);
    }

    public void glVertexAttrib1d(int i, double d) {
        this.f10342gl.glVertexAttrib1d(i, d);
    }

    public void glVertexAttrib1dARB(int i, double d) {
        this.f10342gl.glVertexAttrib1dARB(i, d);
    }

    public void glVertexAttrib1dNV(int i, double d) {
        this.f10342gl.glVertexAttrib1dNV(i, d);
    }

    public void glVertexAttrib1dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib1dv(i, doubleBuffer);
    }

    public void glVertexAttrib1dv(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib1dv(i, dArr, i2);
    }

    public void glVertexAttrib1dvARB(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib1dvARB(i, dArr, i2);
    }

    public void glVertexAttrib1dvARB(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib1dvARB(i, doubleBuffer);
    }

    public void glVertexAttrib1dvNV(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib1dvNV(i, dArr, i2);
    }

    public void glVertexAttrib1dvNV(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib1dvNV(i, doubleBuffer);
    }

    public void glVertexAttrib1f(int i, float f) {
        this.f10342gl.glVertexAttrib1f(i, f);
    }

    public void glVertexAttrib1fARB(int i, float f) {
        this.f10342gl.glVertexAttrib1fARB(i, f);
    }

    public void glVertexAttrib1fNV(int i, float f) {
        this.f10342gl.glVertexAttrib1fNV(i, f);
    }

    public void glVertexAttrib1fv(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib1fv(i, fArr, i2);
    }

    public void glVertexAttrib1fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib1fv(i, floatBuffer);
    }

    public void glVertexAttrib1fvARB(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib1fvARB(i, fArr, i2);
    }

    public void glVertexAttrib1fvARB(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib1fvARB(i, floatBuffer);
    }

    public void glVertexAttrib1fvNV(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib1fvNV(i, fArr, i2);
    }

    public void glVertexAttrib1fvNV(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib1fvNV(i, floatBuffer);
    }

    public void glVertexAttrib1hNV(int i, short s) {
        this.f10342gl.glVertexAttrib1hNV(i, s);
    }

    public void glVertexAttrib1hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib1hvNV(i, shortBuffer);
    }

    public void glVertexAttrib1hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib1hvNV(i, sArr, i2);
    }

    public void glVertexAttrib1s(int i, short s) {
        this.f10342gl.glVertexAttrib1s(i, s);
    }

    public void glVertexAttrib1sARB(int i, short s) {
        this.f10342gl.glVertexAttrib1sARB(i, s);
    }

    public void glVertexAttrib1sNV(int i, short s) {
        this.f10342gl.glVertexAttrib1sNV(i, s);
    }

    public void glVertexAttrib1sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib1sv(i, shortBuffer);
    }

    public void glVertexAttrib1sv(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib1sv(i, sArr, i2);
    }

    public void glVertexAttrib1svARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib1svARB(i, shortBuffer);
    }

    public void glVertexAttrib1svARB(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib1svARB(i, sArr, i2);
    }

    public void glVertexAttrib1svNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib1svNV(i, sArr, i2);
    }

    public void glVertexAttrib1svNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib1svNV(i, shortBuffer);
    }

    public void glVertexAttrib2d(int i, double d, double d2) {
        this.f10342gl.glVertexAttrib2d(i, d, d2);
    }

    public void glVertexAttrib2dARB(int i, double d, double d2) {
        this.f10342gl.glVertexAttrib2dARB(i, d, d2);
    }

    public void glVertexAttrib2dNV(int i, double d, double d2) {
        this.f10342gl.glVertexAttrib2dNV(i, d, d2);
    }

    public void glVertexAttrib2dv(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib2dv(i, dArr, i2);
    }

    public void glVertexAttrib2dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib2dv(i, doubleBuffer);
    }

    public void glVertexAttrib2dvARB(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib2dvARB(i, dArr, i2);
    }

    public void glVertexAttrib2dvARB(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib2dvARB(i, doubleBuffer);
    }

    public void glVertexAttrib2dvNV(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib2dvNV(i, dArr, i2);
    }

    public void glVertexAttrib2dvNV(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib2dvNV(i, doubleBuffer);
    }

    public void glVertexAttrib2f(int i, float f, float f2) {
        this.f10342gl.glVertexAttrib2f(i, f, f2);
    }

    public void glVertexAttrib2fARB(int i, float f, float f2) {
        this.f10342gl.glVertexAttrib2fARB(i, f, f2);
    }

    public void glVertexAttrib2fNV(int i, float f, float f2) {
        this.f10342gl.glVertexAttrib2fNV(i, f, f2);
    }

    public void glVertexAttrib2fv(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib2fv(i, fArr, i2);
    }

    public void glVertexAttrib2fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib2fv(i, floatBuffer);
    }

    public void glVertexAttrib2fvARB(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib2fvARB(i, fArr, i2);
    }

    public void glVertexAttrib2fvARB(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib2fvARB(i, floatBuffer);
    }

    public void glVertexAttrib2fvNV(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib2fvNV(i, fArr, i2);
    }

    public void glVertexAttrib2fvNV(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib2fvNV(i, floatBuffer);
    }

    public void glVertexAttrib2hNV(int i, short s, short s2) {
        this.f10342gl.glVertexAttrib2hNV(i, s, s2);
    }

    public void glVertexAttrib2hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib2hvNV(i, sArr, i2);
    }

    public void glVertexAttrib2hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib2hvNV(i, shortBuffer);
    }

    public void glVertexAttrib2s(int i, short s, short s2) {
        this.f10342gl.glVertexAttrib2s(i, s, s2);
    }

    public void glVertexAttrib2sARB(int i, short s, short s2) {
        this.f10342gl.glVertexAttrib2sARB(i, s, s2);
    }

    public void glVertexAttrib2sNV(int i, short s, short s2) {
        this.f10342gl.glVertexAttrib2sNV(i, s, s2);
    }

    public void glVertexAttrib2sv(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib2sv(i, sArr, i2);
    }

    public void glVertexAttrib2sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib2sv(i, shortBuffer);
    }

    public void glVertexAttrib2svARB(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib2svARB(i, sArr, i2);
    }

    public void glVertexAttrib2svARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib2svARB(i, shortBuffer);
    }

    public void glVertexAttrib2svNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib2svNV(i, sArr, i2);
    }

    public void glVertexAttrib2svNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib2svNV(i, shortBuffer);
    }

    public void glVertexAttrib3d(int i, double d, double d2, double d3) {
        this.f10342gl.glVertexAttrib3d(i, d, d2, d3);
    }

    public void glVertexAttrib3dARB(int i, double d, double d2, double d3) {
        this.f10342gl.glVertexAttrib3dARB(i, d, d2, d3);
    }

    public void glVertexAttrib3dNV(int i, double d, double d2, double d3) {
        this.f10342gl.glVertexAttrib3dNV(i, d, d2, d3);
    }

    public void glVertexAttrib3dv(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib3dv(i, dArr, i2);
    }

    public void glVertexAttrib3dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib3dv(i, doubleBuffer);
    }

    public void glVertexAttrib3dvARB(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib3dvARB(i, dArr, i2);
    }

    public void glVertexAttrib3dvARB(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib3dvARB(i, doubleBuffer);
    }

    public void glVertexAttrib3dvNV(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib3dvNV(i, dArr, i2);
    }

    public void glVertexAttrib3dvNV(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib3dvNV(i, doubleBuffer);
    }

    public void glVertexAttrib3f(int i, float f, float f2, float f3) {
        this.f10342gl.glVertexAttrib3f(i, f, f2, f3);
    }

    public void glVertexAttrib3fARB(int i, float f, float f2, float f3) {
        this.f10342gl.glVertexAttrib3fARB(i, f, f2, f3);
    }

    public void glVertexAttrib3fNV(int i, float f, float f2, float f3) {
        this.f10342gl.glVertexAttrib3fNV(i, f, f2, f3);
    }

    public void glVertexAttrib3fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib3fv(i, floatBuffer);
    }

    public void glVertexAttrib3fv(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib3fv(i, fArr, i2);
    }

    public void glVertexAttrib3fvARB(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib3fvARB(i, fArr, i2);
    }

    public void glVertexAttrib3fvARB(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib3fvARB(i, floatBuffer);
    }

    public void glVertexAttrib3fvNV(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib3fvNV(i, fArr, i2);
    }

    public void glVertexAttrib3fvNV(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib3fvNV(i, floatBuffer);
    }

    public void glVertexAttrib3hNV(int i, short s, short s2, short s3) {
        this.f10342gl.glVertexAttrib3hNV(i, s, s2, s3);
    }

    public void glVertexAttrib3hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib3hvNV(i, shortBuffer);
    }

    public void glVertexAttrib3hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib3hvNV(i, sArr, i2);
    }

    public void glVertexAttrib3s(int i, short s, short s2, short s3) {
        this.f10342gl.glVertexAttrib3s(i, s, s2, s3);
    }

    public void glVertexAttrib3sARB(int i, short s, short s2, short s3) {
        this.f10342gl.glVertexAttrib3sARB(i, s, s2, s3);
    }

    public void glVertexAttrib3sNV(int i, short s, short s2, short s3) {
        this.f10342gl.glVertexAttrib3sNV(i, s, s2, s3);
    }

    public void glVertexAttrib3sv(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib3sv(i, sArr, i2);
    }

    public void glVertexAttrib3sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib3sv(i, shortBuffer);
    }

    public void glVertexAttrib3svARB(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib3svARB(i, sArr, i2);
    }

    public void glVertexAttrib3svARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib3svARB(i, shortBuffer);
    }

    public void glVertexAttrib3svNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib3svNV(i, sArr, i2);
    }

    public void glVertexAttrib3svNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib3svNV(i, shortBuffer);
    }

    public void glVertexAttrib4Nbv(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4Nbv(i, byteBuffer);
    }

    public void glVertexAttrib4Nbv(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4Nbv(i, bArr, i2);
    }

    public void glVertexAttrib4NbvARB(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4NbvARB(i, byteBuffer);
    }

    public void glVertexAttrib4NbvARB(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4NbvARB(i, bArr, i2);
    }

    public void glVertexAttrib4Niv(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4Niv(i, iArr, i2);
    }

    public void glVertexAttrib4Niv(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4Niv(i, intBuffer);
    }

    public void glVertexAttrib4NivARB(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4NivARB(i, iArr, i2);
    }

    public void glVertexAttrib4NivARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4NivARB(i, intBuffer);
    }

    public void glVertexAttrib4Nsv(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4Nsv(i, sArr, i2);
    }

    public void glVertexAttrib4Nsv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4Nsv(i, shortBuffer);
    }

    public void glVertexAttrib4NsvARB(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4NsvARB(i, sArr, i2);
    }

    public void glVertexAttrib4NsvARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4NsvARB(i, shortBuffer);
    }

    public void glVertexAttrib4Nub(int i, byte b, byte b2, byte b3, byte b4) {
        this.f10342gl.glVertexAttrib4Nub(i, b, b2, b3, b4);
    }

    public void glVertexAttrib4NubARB(int i, byte b, byte b2, byte b3, byte b4) {
        this.f10342gl.glVertexAttrib4NubARB(i, b, b2, b3, b4);
    }

    public void glVertexAttrib4Nubv(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4Nubv(i, bArr, i2);
    }

    public void glVertexAttrib4Nubv(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4Nubv(i, byteBuffer);
    }

    public void glVertexAttrib4NubvARB(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4NubvARB(i, byteBuffer);
    }

    public void glVertexAttrib4NubvARB(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4NubvARB(i, bArr, i2);
    }

    public void glVertexAttrib4Nuiv(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4Nuiv(i, iArr, i2);
    }

    public void glVertexAttrib4Nuiv(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4Nuiv(i, intBuffer);
    }

    public void glVertexAttrib4NuivARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4NuivARB(i, intBuffer);
    }

    public void glVertexAttrib4NuivARB(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4NuivARB(i, iArr, i2);
    }

    public void glVertexAttrib4Nusv(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4Nusv(i, sArr, i2);
    }

    public void glVertexAttrib4Nusv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4Nusv(i, shortBuffer);
    }

    public void glVertexAttrib4NusvARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4NusvARB(i, shortBuffer);
    }

    public void glVertexAttrib4NusvARB(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4NusvARB(i, sArr, i2);
    }

    public void glVertexAttrib4bv(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4bv(i, bArr, i2);
    }

    public void glVertexAttrib4bv(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4bv(i, byteBuffer);
    }

    public void glVertexAttrib4bvARB(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4bvARB(i, bArr, i2);
    }

    public void glVertexAttrib4bvARB(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4bvARB(i, byteBuffer);
    }

    public void glVertexAttrib4d(int i, double d, double d2, double d3, double d4) {
        this.f10342gl.glVertexAttrib4d(i, d, d2, d3, d4);
    }

    public void glVertexAttrib4dARB(int i, double d, double d2, double d3, double d4) {
        this.f10342gl.glVertexAttrib4dARB(i, d, d2, d3, d4);
    }

    public void glVertexAttrib4dNV(int i, double d, double d2, double d3, double d4) {
        this.f10342gl.glVertexAttrib4dNV(i, d, d2, d3, d4);
    }

    public void glVertexAttrib4dv(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib4dv(i, doubleBuffer);
    }

    public void glVertexAttrib4dv(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib4dv(i, dArr, i2);
    }

    public void glVertexAttrib4dvARB(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib4dvARB(i, doubleBuffer);
    }

    public void glVertexAttrib4dvARB(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib4dvARB(i, dArr, i2);
    }

    public void glVertexAttrib4dvNV(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttrib4dvNV(i, doubleBuffer);
    }

    public void glVertexAttrib4dvNV(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexAttrib4dvNV(i, dArr, i2);
    }

    public void glVertexAttrib4f(int i, float f, float f2, float f3, float f4) {
        this.f10342gl.glVertexAttrib4f(i, f, f2, f3, f4);
    }

    public void glVertexAttrib4fARB(int i, float f, float f2, float f3, float f4) {
        this.f10342gl.glVertexAttrib4fARB(i, f, f2, f3, f4);
    }

    public void glVertexAttrib4fNV(int i, float f, float f2, float f3, float f4) {
        this.f10342gl.glVertexAttrib4fNV(i, f, f2, f3, f4);
    }

    public void glVertexAttrib4fv(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib4fv(i, fArr, i2);
    }

    public void glVertexAttrib4fv(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib4fv(i, floatBuffer);
    }

    public void glVertexAttrib4fvARB(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib4fvARB(i, floatBuffer);
    }

    public void glVertexAttrib4fvARB(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib4fvARB(i, fArr, i2);
    }

    public void glVertexAttrib4fvNV(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexAttrib4fvNV(i, fArr, i2);
    }

    public void glVertexAttrib4fvNV(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttrib4fvNV(i, floatBuffer);
    }

    public void glVertexAttrib4hNV(int i, short s, short s2, short s3, short s4) {
        this.f10342gl.glVertexAttrib4hNV(i, s, s2, s3, s4);
    }

    public void glVertexAttrib4hvNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4hvNV(i, shortBuffer);
    }

    public void glVertexAttrib4hvNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4hvNV(i, sArr, i2);
    }

    public void glVertexAttrib4iv(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4iv(i, intBuffer);
    }

    public void glVertexAttrib4iv(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4iv(i, iArr, i2);
    }

    public void glVertexAttrib4ivARB(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4ivARB(i, iArr, i2);
    }

    public void glVertexAttrib4ivARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4ivARB(i, intBuffer);
    }

    public void glVertexAttrib4s(int i, short s, short s2, short s3, short s4) {
        this.f10342gl.glVertexAttrib4s(i, s, s2, s3, s4);
    }

    public void glVertexAttrib4sARB(int i, short s, short s2, short s3, short s4) {
        this.f10342gl.glVertexAttrib4sARB(i, s, s2, s3, s4);
    }

    public void glVertexAttrib4sNV(int i, short s, short s2, short s3, short s4) {
        this.f10342gl.glVertexAttrib4sNV(i, s, s2, s3, s4);
    }

    public void glVertexAttrib4sv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4sv(i, shortBuffer);
    }

    public void glVertexAttrib4sv(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4sv(i, sArr, i2);
    }

    public void glVertexAttrib4svARB(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4svARB(i, sArr, i2);
    }

    public void glVertexAttrib4svARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4svARB(i, shortBuffer);
    }

    public void glVertexAttrib4svNV(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4svNV(i, sArr, i2);
    }

    public void glVertexAttrib4svNV(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4svNV(i, shortBuffer);
    }

    public void glVertexAttrib4ubNV(int i, byte b, byte b2, byte b3, byte b4) {
        this.f10342gl.glVertexAttrib4ubNV(i, b, b2, b3, b4);
    }

    public void glVertexAttrib4ubv(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4ubv(i, byteBuffer);
    }

    public void glVertexAttrib4ubv(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4ubv(i, bArr, i2);
    }

    public void glVertexAttrib4ubvARB(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4ubvARB(i, bArr, i2);
    }

    public void glVertexAttrib4ubvARB(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4ubvARB(i, byteBuffer);
    }

    public void glVertexAttrib4ubvNV(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttrib4ubvNV(i, bArr, i2);
    }

    public void glVertexAttrib4ubvNV(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttrib4ubvNV(i, byteBuffer);
    }

    public void glVertexAttrib4uiv(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4uiv(i, iArr, i2);
    }

    public void glVertexAttrib4uiv(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4uiv(i, intBuffer);
    }

    public void glVertexAttrib4uivARB(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttrib4uivARB(i, iArr, i2);
    }

    public void glVertexAttrib4uivARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttrib4uivARB(i, intBuffer);
    }

    public void glVertexAttrib4usv(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4usv(i, sArr, i2);
    }

    public void glVertexAttrib4usv(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4usv(i, shortBuffer);
    }

    public void glVertexAttrib4usvARB(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttrib4usvARB(i, sArr, i2);
    }

    public void glVertexAttrib4usvARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttrib4usvARB(i, shortBuffer);
    }

    public void glVertexAttribArrayObjectATI(int i, int i2, int i3, boolean z, int i4, int i5, int i6) {
        this.f10342gl.glVertexAttribArrayObjectATI(i, i2, i3, z, i4, i5, i6);
    }

    public void glVertexAttribI1iEXT(int i, int i2) {
        this.f10342gl.glVertexAttribI1iEXT(i, i2);
    }

    public void glVertexAttribI1ivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI1ivEXT(i, intBuffer);
    }

    public void glVertexAttribI1ivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI1ivEXT(i, iArr, i2);
    }

    public void glVertexAttribI1uiEXT(int i, int i2) {
        this.f10342gl.glVertexAttribI1uiEXT(i, i2);
    }

    public void glVertexAttribI1uivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI1uivEXT(i, iArr, i2);
    }

    public void glVertexAttribI1uivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI1uivEXT(i, intBuffer);
    }

    public void glVertexAttribI2iEXT(int i, int i2, int i3) {
        this.f10342gl.glVertexAttribI2iEXT(i, i2, i3);
    }

    public void glVertexAttribI2ivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI2ivEXT(i, intBuffer);
    }

    public void glVertexAttribI2ivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI2ivEXT(i, iArr, i2);
    }

    public void glVertexAttribI2uiEXT(int i, int i2, int i3) {
        this.f10342gl.glVertexAttribI2uiEXT(i, i2, i3);
    }

    public void glVertexAttribI2uivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI2uivEXT(i, intBuffer);
    }

    public void glVertexAttribI2uivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI2uivEXT(i, iArr, i2);
    }

    public void glVertexAttribI3iEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glVertexAttribI3iEXT(i, i2, i3, i4);
    }

    public void glVertexAttribI3ivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI3ivEXT(i, intBuffer);
    }

    public void glVertexAttribI3ivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI3ivEXT(i, iArr, i2);
    }

    public void glVertexAttribI3uiEXT(int i, int i2, int i3, int i4) {
        this.f10342gl.glVertexAttribI3uiEXT(i, i2, i3, i4);
    }

    public void glVertexAttribI3uivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI3uivEXT(i, intBuffer);
    }

    public void glVertexAttribI3uivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI3uivEXT(i, iArr, i2);
    }

    public void glVertexAttribI4bvEXT(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttribI4bvEXT(i, bArr, i2);
    }

    public void glVertexAttribI4bvEXT(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttribI4bvEXT(i, byteBuffer);
    }

    public void glVertexAttribI4iEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glVertexAttribI4iEXT(i, i2, i3, i4, i5);
    }

    public void glVertexAttribI4ivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI4ivEXT(i, iArr, i2);
    }

    public void glVertexAttribI4ivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI4ivEXT(i, intBuffer);
    }

    public void glVertexAttribI4svEXT(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribI4svEXT(i, shortBuffer);
    }

    public void glVertexAttribI4svEXT(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttribI4svEXT(i, sArr, i2);
    }

    public void glVertexAttribI4ubvEXT(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttribI4ubvEXT(i, byteBuffer);
    }

    public void glVertexAttribI4ubvEXT(int i, byte[] bArr, int i2) {
        this.f10342gl.glVertexAttribI4ubvEXT(i, bArr, i2);
    }

    public void glVertexAttribI4uiEXT(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glVertexAttribI4uiEXT(i, i2, i3, i4, i5);
    }

    public void glVertexAttribI4uivEXT(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexAttribI4uivEXT(i, iArr, i2);
    }

    public void glVertexAttribI4uivEXT(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexAttribI4uivEXT(i, intBuffer);
    }

    public void glVertexAttribI4usvEXT(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexAttribI4usvEXT(i, sArr, i2);
    }

    public void glVertexAttribI4usvEXT(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribI4usvEXT(i, shortBuffer);
    }

    public void glVertexAttribIPointerEXT(int i, int i2, int i3, int i4, Buffer buffer) {
        this.f10342gl.glVertexAttribIPointerEXT(i, i2, i3, i4, buffer);
    }

    public void glVertexAttribPointer(int i, int i2, int i3, boolean z, int i4, Buffer buffer) {
        this.f10342gl.glVertexAttribPointer(i, i2, i3, z, i4, buffer);
    }

    public void glVertexAttribPointer(int i, int i2, int i3, boolean z, int i4, long j) {
        this.f10342gl.glVertexAttribPointer(i, i2, i3, z, i4, j);
    }

    public void glVertexAttribPointerARB(int i, int i2, int i3, boolean z, int i4, long j) {
        this.f10342gl.glVertexAttribPointerARB(i, i2, i3, z, i4, j);
    }

    public void glVertexAttribPointerARB(int i, int i2, int i3, boolean z, int i4, Buffer buffer) {
        this.f10342gl.glVertexAttribPointerARB(i, i2, i3, z, i4, buffer);
    }

    public void glVertexAttribPointerNV(int i, int i2, int i3, int i4, Buffer buffer) {
        this.f10342gl.glVertexAttribPointerNV(i, i2, i3, i4, buffer);
    }

    public void glVertexAttribPointerNV(int i, int i2, int i3, int i4, long j) {
        this.f10342gl.glVertexAttribPointerNV(i, i2, i3, i4, j);
    }

    public void glVertexAttribs1dvNV(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glVertexAttribs1dvNV(i, i2, dArr, i3);
    }

    public void glVertexAttribs1dvNV(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttribs1dvNV(i, i2, doubleBuffer);
    }

    public void glVertexAttribs1fvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glVertexAttribs1fvNV(i, i2, fArr, i3);
    }

    public void glVertexAttribs1fvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttribs1fvNV(i, i2, floatBuffer);
    }

    public void glVertexAttribs1hvNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs1hvNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs1hvNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs1hvNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs1svNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs1svNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs1svNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs1svNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs2dvNV(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glVertexAttribs2dvNV(i, i2, dArr, i3);
    }

    public void glVertexAttribs2dvNV(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttribs2dvNV(i, i2, doubleBuffer);
    }

    public void glVertexAttribs2fvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glVertexAttribs2fvNV(i, i2, fArr, i3);
    }

    public void glVertexAttribs2fvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttribs2fvNV(i, i2, floatBuffer);
    }

    public void glVertexAttribs2hvNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs2hvNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs2hvNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs2hvNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs2svNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs2svNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs2svNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs2svNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs3dvNV(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glVertexAttribs3dvNV(i, i2, dArr, i3);
    }

    public void glVertexAttribs3dvNV(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttribs3dvNV(i, i2, doubleBuffer);
    }

    public void glVertexAttribs3fvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glVertexAttribs3fvNV(i, i2, fArr, i3);
    }

    public void glVertexAttribs3fvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttribs3fvNV(i, i2, floatBuffer);
    }

    public void glVertexAttribs3hvNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs3hvNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs3hvNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs3hvNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs3svNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs3svNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs3svNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs3svNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs4dvNV(int i, int i2, double[] dArr, int i3) {
        this.f10342gl.glVertexAttribs4dvNV(i, i2, dArr, i3);
    }

    public void glVertexAttribs4dvNV(int i, int i2, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexAttribs4dvNV(i, i2, doubleBuffer);
    }

    public void glVertexAttribs4fvNV(int i, int i2, float[] fArr, int i3) {
        this.f10342gl.glVertexAttribs4fvNV(i, i2, fArr, i3);
    }

    public void glVertexAttribs4fvNV(int i, int i2, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexAttribs4fvNV(i, i2, floatBuffer);
    }

    public void glVertexAttribs4hvNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs4hvNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs4hvNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs4hvNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs4svNV(int i, int i2, short[] sArr, int i3) {
        this.f10342gl.glVertexAttribs4svNV(i, i2, sArr, i3);
    }

    public void glVertexAttribs4svNV(int i, int i2, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexAttribs4svNV(i, i2, shortBuffer);
    }

    public void glVertexAttribs4ubvNV(int i, int i2, ByteBuffer byteBuffer) {
        this.f10342gl.glVertexAttribs4ubvNV(i, i2, byteBuffer);
    }

    public void glVertexAttribs4ubvNV(int i, int i2, byte[] bArr, int i3) {
        this.f10342gl.glVertexAttribs4ubvNV(i, i2, bArr, i3);
    }

    public void glVertexBlendARB(int i) {
        this.f10342gl.glVertexBlendARB(i);
    }

    public void glVertexBlendEnvfATI(int i, float f) {
        this.f10342gl.glVertexBlendEnvfATI(i, f);
    }

    public void glVertexBlendEnviATI(int i, int i2) {
        this.f10342gl.glVertexBlendEnviATI(i, i2);
    }

    public void glVertexPointer(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glVertexPointer(i, i2, i3, buffer);
    }

    public void glVertexPointer(int i, int i2, int i3, long j) {
        this.f10342gl.glVertexPointer(i, i2, i3, j);
    }

    public void glVertexStream1dATI(int i, double d) {
        this.f10342gl.glVertexStream1dATI(i, d);
    }

    public void glVertexStream1dvATI(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexStream1dvATI(i, doubleBuffer);
    }

    public void glVertexStream1dvATI(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexStream1dvATI(i, dArr, i2);
    }

    public void glVertexStream1fATI(int i, float f) {
        this.f10342gl.glVertexStream1fATI(i, f);
    }

    public void glVertexStream1fvATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexStream1fvATI(i, floatBuffer);
    }

    public void glVertexStream1fvATI(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexStream1fvATI(i, fArr, i2);
    }

    public void glVertexStream1iATI(int i, int i2) {
        this.f10342gl.glVertexStream1iATI(i, i2);
    }

    public void glVertexStream1ivATI(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexStream1ivATI(i, iArr, i2);
    }

    public void glVertexStream1ivATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexStream1ivATI(i, intBuffer);
    }

    public void glVertexStream1sATI(int i, short s) {
        this.f10342gl.glVertexStream1sATI(i, s);
    }

    public void glVertexStream1svATI(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexStream1svATI(i, shortBuffer);
    }

    public void glVertexStream1svATI(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexStream1svATI(i, sArr, i2);
    }

    public void glVertexStream2dATI(int i, double d, double d2) {
        this.f10342gl.glVertexStream2dATI(i, d, d2);
    }

    public void glVertexStream2dvATI(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexStream2dvATI(i, doubleBuffer);
    }

    public void glVertexStream2dvATI(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexStream2dvATI(i, dArr, i2);
    }

    public void glVertexStream2fATI(int i, float f, float f2) {
        this.f10342gl.glVertexStream2fATI(i, f, f2);
    }

    public void glVertexStream2fvATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexStream2fvATI(i, floatBuffer);
    }

    public void glVertexStream2fvATI(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexStream2fvATI(i, fArr, i2);
    }

    public void glVertexStream2iATI(int i, int i2, int i3) {
        this.f10342gl.glVertexStream2iATI(i, i2, i3);
    }

    public void glVertexStream2ivATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexStream2ivATI(i, intBuffer);
    }

    public void glVertexStream2ivATI(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexStream2ivATI(i, iArr, i2);
    }

    public void glVertexStream2sATI(int i, short s, short s2) {
        this.f10342gl.glVertexStream2sATI(i, s, s2);
    }

    public void glVertexStream2svATI(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexStream2svATI(i, sArr, i2);
    }

    public void glVertexStream2svATI(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexStream2svATI(i, shortBuffer);
    }

    public void glVertexStream3dATI(int i, double d, double d2, double d3) {
        this.f10342gl.glVertexStream3dATI(i, d, d2, d3);
    }

    public void glVertexStream3dvATI(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexStream3dvATI(i, doubleBuffer);
    }

    public void glVertexStream3dvATI(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexStream3dvATI(i, dArr, i2);
    }

    public void glVertexStream3fATI(int i, float f, float f2, float f3) {
        this.f10342gl.glVertexStream3fATI(i, f, f2, f3);
    }

    public void glVertexStream3fvATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexStream3fvATI(i, floatBuffer);
    }

    public void glVertexStream3fvATI(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexStream3fvATI(i, fArr, i2);
    }

    public void glVertexStream3iATI(int i, int i2, int i3, int i4) {
        this.f10342gl.glVertexStream3iATI(i, i2, i3, i4);
    }

    public void glVertexStream3ivATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexStream3ivATI(i, intBuffer);
    }

    public void glVertexStream3ivATI(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexStream3ivATI(i, iArr, i2);
    }

    public void glVertexStream3sATI(int i, short s, short s2, short s3) {
        this.f10342gl.glVertexStream3sATI(i, s, s2, s3);
    }

    public void glVertexStream3svATI(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexStream3svATI(i, sArr, i2);
    }

    public void glVertexStream3svATI(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexStream3svATI(i, shortBuffer);
    }

    public void glVertexStream4dATI(int i, double d, double d2, double d3, double d4) {
        this.f10342gl.glVertexStream4dATI(i, d, d2, d3, d4);
    }

    public void glVertexStream4dvATI(int i, double[] dArr, int i2) {
        this.f10342gl.glVertexStream4dvATI(i, dArr, i2);
    }

    public void glVertexStream4dvATI(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glVertexStream4dvATI(i, doubleBuffer);
    }

    public void glVertexStream4fATI(int i, float f, float f2, float f3, float f4) {
        this.f10342gl.glVertexStream4fATI(i, f, f2, f3, f4);
    }

    public void glVertexStream4fvATI(int i, float[] fArr, int i2) {
        this.f10342gl.glVertexStream4fvATI(i, fArr, i2);
    }

    public void glVertexStream4fvATI(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glVertexStream4fvATI(i, floatBuffer);
    }

    public void glVertexStream4iATI(int i, int i2, int i3, int i4, int i5) {
        this.f10342gl.glVertexStream4iATI(i, i2, i3, i4, i5);
    }

    public void glVertexStream4ivATI(int i, IntBuffer intBuffer) {
        this.f10342gl.glVertexStream4ivATI(i, intBuffer);
    }

    public void glVertexStream4ivATI(int i, int[] iArr, int i2) {
        this.f10342gl.glVertexStream4ivATI(i, iArr, i2);
    }

    public void glVertexStream4sATI(int i, short s, short s2, short s3, short s4) {
        this.f10342gl.glVertexStream4sATI(i, s, s2, s3, s4);
    }

    public void glVertexStream4svATI(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glVertexStream4svATI(i, shortBuffer);
    }

    public void glVertexStream4svATI(int i, short[] sArr, int i2) {
        this.f10342gl.glVertexStream4svATI(i, sArr, i2);
    }

    public void glVertexWeightPointerEXT(int i, int i2, int i3, long j) {
        this.f10342gl.glVertexWeightPointerEXT(i, i2, i3, j);
    }

    public void glVertexWeightPointerEXT(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glVertexWeightPointerEXT(i, i2, i3, buffer);
    }

    public void glVertexWeightfEXT(float f) {
        this.f10342gl.glVertexWeightfEXT(f);
    }

    public void glVertexWeightfvEXT(float[] fArr, int i) {
        this.f10342gl.glVertexWeightfvEXT(fArr, i);
    }

    public void glVertexWeightfvEXT(FloatBuffer floatBuffer) {
        this.f10342gl.glVertexWeightfvEXT(floatBuffer);
    }

    public void glVertexWeighthNV(short s) {
        this.f10342gl.glVertexWeighthNV(s);
    }

    public void glVertexWeighthvNV(ShortBuffer shortBuffer) {
        this.f10342gl.glVertexWeighthvNV(shortBuffer);
    }

    public void glVertexWeighthvNV(short[] sArr, int i) {
        this.f10342gl.glVertexWeighthvNV(sArr, i);
    }

    public void glViewport(int i, int i2, int i3, int i4) {
        this.f10342gl.glViewport(i, i2, i3, i4);
    }

    public void glWeightPointerARB(int i, int i2, int i3, Buffer buffer) {
        this.f10342gl.glWeightPointerARB(i, i2, i3, buffer);
    }

    public void glWeightPointerARB(int i, int i2, int i3, long j) {
        this.f10342gl.glWeightPointerARB(i, i2, i3, j);
    }

    public void glWeightbvARB(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glWeightbvARB(i, byteBuffer);
    }

    public void glWeightbvARB(int i, byte[] bArr, int i2) {
        this.f10342gl.glWeightbvARB(i, bArr, i2);
    }

    public void glWeightdvARB(int i, DoubleBuffer doubleBuffer) {
        this.f10342gl.glWeightdvARB(i, doubleBuffer);
    }

    public void glWeightdvARB(int i, double[] dArr, int i2) {
        this.f10342gl.glWeightdvARB(i, dArr, i2);
    }

    public void glWeightfvARB(int i, float[] fArr, int i2) {
        this.f10342gl.glWeightfvARB(i, fArr, i2);
    }

    public void glWeightfvARB(int i, FloatBuffer floatBuffer) {
        this.f10342gl.glWeightfvARB(i, floatBuffer);
    }

    public void glWeightivARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glWeightivARB(i, intBuffer);
    }

    public void glWeightivARB(int i, int[] iArr, int i2) {
        this.f10342gl.glWeightivARB(i, iArr, i2);
    }

    public void glWeightsvARB(int i, short[] sArr, int i2) {
        this.f10342gl.glWeightsvARB(i, sArr, i2);
    }

    public void glWeightsvARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glWeightsvARB(i, shortBuffer);
    }

    public void glWeightubvARB(int i, ByteBuffer byteBuffer) {
        this.f10342gl.glWeightubvARB(i, byteBuffer);
    }

    public void glWeightubvARB(int i, byte[] bArr, int i2) {
        this.f10342gl.glWeightubvARB(i, bArr, i2);
    }

    public void glWeightuivARB(int i, int[] iArr, int i2) {
        this.f10342gl.glWeightuivARB(i, iArr, i2);
    }

    public void glWeightuivARB(int i, IntBuffer intBuffer) {
        this.f10342gl.glWeightuivARB(i, intBuffer);
    }

    public void glWeightusvARB(int i, ShortBuffer shortBuffer) {
        this.f10342gl.glWeightusvARB(i, shortBuffer);
    }

    public void glWeightusvARB(int i, short[] sArr, int i2) {
        this.f10342gl.glWeightusvARB(i, sArr, i2);
    }

    public void glWindowPos2d(double d, double d2) {
        this.f10342gl.glWindowPos2d(d, d2);
    }

    public void glWindowPos2dARB(double d, double d2) {
        this.f10342gl.glWindowPos2dARB(d, d2);
    }

    public void glWindowPos2dMESA(double d, double d2) {
        this.f10342gl.glWindowPos2dMESA(d, d2);
    }

    public void glWindowPos2dv(double[] dArr, int i) {
        this.f10342gl.glWindowPos2dv(dArr, i);
    }

    public void glWindowPos2dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glWindowPos2dv(doubleBuffer);
    }

    public void glWindowPos2dvARB(double[] dArr, int i) {
        this.f10342gl.glWindowPos2dvARB(dArr, i);
    }

    public void glWindowPos2dvARB(DoubleBuffer doubleBuffer) {
        this.f10342gl.glWindowPos2dvARB(doubleBuffer);
    }

    public void glWindowPos2dvMESA(double[] dArr, int i) {
        this.f10342gl.glWindowPos2dvMESA(dArr, i);
    }

    public void glWindowPos2dvMESA(DoubleBuffer doubleBuffer) {
        this.f10342gl.glWindowPos2dvMESA(doubleBuffer);
    }

    public void glWindowPos2f(float f, float f2) {
        this.f10342gl.glWindowPos2f(f, f2);
    }

    public void glWindowPos2fARB(float f, float f2) {
        this.f10342gl.glWindowPos2fARB(f, f2);
    }

    public void glWindowPos2fMESA(float f, float f2) {
        this.f10342gl.glWindowPos2fMESA(f, f2);
    }

    public void glWindowPos2fv(FloatBuffer floatBuffer) {
        this.f10342gl.glWindowPos2fv(floatBuffer);
    }

    public void glWindowPos2fv(float[] fArr, int i) {
        this.f10342gl.glWindowPos2fv(fArr, i);
    }

    public void glWindowPos2fvARB(FloatBuffer floatBuffer) {
        this.f10342gl.glWindowPos2fvARB(floatBuffer);
    }

    public void glWindowPos2fvARB(float[] fArr, int i) {
        this.f10342gl.glWindowPos2fvARB(fArr, i);
    }

    public void glWindowPos2fvMESA(FloatBuffer floatBuffer) {
        this.f10342gl.glWindowPos2fvMESA(floatBuffer);
    }

    public void glWindowPos2fvMESA(float[] fArr, int i) {
        this.f10342gl.glWindowPos2fvMESA(fArr, i);
    }

    public void glWindowPos2i(int i, int i2) {
        this.f10342gl.glWindowPos2i(i, i2);
    }

    public void glWindowPos2iARB(int i, int i2) {
        this.f10342gl.glWindowPos2iARB(i, i2);
    }

    public void glWindowPos2iMESA(int i, int i2) {
        this.f10342gl.glWindowPos2iMESA(i, i2);
    }

    public void glWindowPos2iv(int[] iArr, int i) {
        this.f10342gl.glWindowPos2iv(iArr, i);
    }

    public void glWindowPos2iv(IntBuffer intBuffer) {
        this.f10342gl.glWindowPos2iv(intBuffer);
    }

    public void glWindowPos2ivARB(int[] iArr, int i) {
        this.f10342gl.glWindowPos2ivARB(iArr, i);
    }

    public void glWindowPos2ivARB(IntBuffer intBuffer) {
        this.f10342gl.glWindowPos2ivARB(intBuffer);
    }

    public void glWindowPos2ivMESA(IntBuffer intBuffer) {
        this.f10342gl.glWindowPos2ivMESA(intBuffer);
    }

    public void glWindowPos2ivMESA(int[] iArr, int i) {
        this.f10342gl.glWindowPos2ivMESA(iArr, i);
    }

    public void glWindowPos2s(short s, short s2) {
        this.f10342gl.glWindowPos2s(s, s2);
    }

    public void glWindowPos2sARB(short s, short s2) {
        this.f10342gl.glWindowPos2sARB(s, s2);
    }

    public void glWindowPos2sMESA(short s, short s2) {
        this.f10342gl.glWindowPos2sMESA(s, s2);
    }

    public void glWindowPos2sv(short[] sArr, int i) {
        this.f10342gl.glWindowPos2sv(sArr, i);
    }

    public void glWindowPos2sv(ShortBuffer shortBuffer) {
        this.f10342gl.glWindowPos2sv(shortBuffer);
    }

    public void glWindowPos2svARB(short[] sArr, int i) {
        this.f10342gl.glWindowPos2svARB(sArr, i);
    }

    public void glWindowPos2svARB(ShortBuffer shortBuffer) {
        this.f10342gl.glWindowPos2svARB(shortBuffer);
    }

    public void glWindowPos2svMESA(short[] sArr, int i) {
        this.f10342gl.glWindowPos2svMESA(sArr, i);
    }

    public void glWindowPos2svMESA(ShortBuffer shortBuffer) {
        this.f10342gl.glWindowPos2svMESA(shortBuffer);
    }

    public void glWindowPos3d(double d, double d2, double d3) {
        this.f10342gl.glWindowPos3d(d, d2, d3);
    }

    public void glWindowPos3dARB(double d, double d2, double d3) {
        this.f10342gl.glWindowPos3dARB(d, d2, d3);
    }

    public void glWindowPos3dMESA(double d, double d2, double d3) {
        this.f10342gl.glWindowPos3dMESA(d, d2, d3);
    }

    public void glWindowPos3dv(double[] dArr, int i) {
        this.f10342gl.glWindowPos3dv(dArr, i);
    }

    public void glWindowPos3dv(DoubleBuffer doubleBuffer) {
        this.f10342gl.glWindowPos3dv(doubleBuffer);
    }

    public void glWindowPos3dvARB(DoubleBuffer doubleBuffer) {
        this.f10342gl.glWindowPos3dvARB(doubleBuffer);
    }

    public void glWindowPos3dvARB(double[] dArr, int i) {
        this.f10342gl.glWindowPos3dvARB(dArr, i);
    }

    public void glWindowPos3dvMESA(DoubleBuffer doubleBuffer) {
        this.f10342gl.glWindowPos3dvMESA(doubleBuffer);
    }

    public void glWindowPos3dvMESA(double[] dArr, int i) {
        this.f10342gl.glWindowPos3dvMESA(dArr, i);
    }

    public void glWindowPos3f(float f, float f2, float f3) {
        this.f10342gl.glWindowPos3f(f, f2, f3);
    }

    public void glWindowPos3fARB(float f, float f2, float f3) {
        this.f10342gl.glWindowPos3fARB(f, f2, f3);
    }

    public void glWindowPos3fMESA(float f, float f2, float f3) {
        this.f10342gl.glWindowPos3fMESA(f, f2, f3);
    }

    public void glWindowPos3fv(float[] fArr, int i) {
        this.f10342gl.glWindowPos3fv(fArr, i);
    }

    public void glWindowPos3fv(FloatBuffer floatBuffer) {
        this.f10342gl.glWindowPos3fv(floatBuffer);
    }

    public void glWindowPos3fvARB(FloatBuffer floatBuffer) {
        this.f10342gl.glWindowPos3fvARB(floatBuffer);
    }

    public void glWindowPos3fvARB(float[] fArr, int i) {
        this.f10342gl.glWindowPos3fvARB(fArr, i);
    }

    public void glWindowPos3fvMESA(FloatBuffer floatBuffer) {
        this.f10342gl.glWindowPos3fvMESA(floatBuffer);
    }

    public void glWindowPos3fvMESA(float[] fArr, int i) {
        this.f10342gl.glWindowPos3fvMESA(fArr, i);
    }

    public void glWindowPos3i(int i, int i2, int i3) {
        this.f10342gl.glWindowPos3i(i, i2, i3);
    }

    public void glWindowPos3iARB(int i, int i2, int i3) {
        this.f10342gl.glWindowPos3iARB(i, i2, i3);
    }

    public void glWindowPos3iMESA(int i, int i2, int i3) {
        this.f10342gl.glWindowPos3iMESA(i, i2, i3);
    }

    public void glWindowPos3iv(int[] iArr, int i) {
        this.f10342gl.glWindowPos3iv(iArr, i);
    }

    public void glWindowPos3iv(IntBuffer intBuffer) {
        this.f10342gl.glWindowPos3iv(intBuffer);
    }

    public void glWindowPos3ivARB(int[] iArr, int i) {
        this.f10342gl.glWindowPos3ivARB(iArr, i);
    }

    public void glWindowPos3ivARB(IntBuffer intBuffer) {
        this.f10342gl.glWindowPos3ivARB(intBuffer);
    }

    public void glWindowPos3ivMESA(int[] iArr, int i) {
        this.f10342gl.glWindowPos3ivMESA(iArr, i);
    }

    public void glWindowPos3ivMESA(IntBuffer intBuffer) {
        this.f10342gl.glWindowPos3ivMESA(intBuffer);
    }

    public void glWindowPos3s(short s, short s2, short s3) {
        this.f10342gl.glWindowPos3s(s, s2, s3);
    }

    public void glWindowPos3sARB(short s, short s2, short s3) {
        this.f10342gl.glWindowPos3sARB(s, s2, s3);
    }

    public void glWindowPos3sMESA(short s, short s2, short s3) {
        this.f10342gl.glWindowPos3sMESA(s, s2, s3);
    }

    public void glWindowPos3sv(short[] sArr, int i) {
        this.f10342gl.glWindowPos3sv(sArr, i);
    }

    public void glWindowPos3sv(ShortBuffer shortBuffer) {
        this.f10342gl.glWindowPos3sv(shortBuffer);
    }

    public void glWindowPos3svARB(short[] sArr, int i) {
        this.f10342gl.glWindowPos3svARB(sArr, i);
    }

    public void glWindowPos3svARB(ShortBuffer shortBuffer) {
        this.f10342gl.glWindowPos3svARB(shortBuffer);
    }

    public void glWindowPos3svMESA(short[] sArr, int i) {
        this.f10342gl.glWindowPos3svMESA(sArr, i);
    }

    public void glWindowPos3svMESA(ShortBuffer shortBuffer) {
        this.f10342gl.glWindowPos3svMESA(shortBuffer);
    }

    public void glWindowPos4dMESA(double d, double d2, double d3, double d4) {
        this.f10342gl.glWindowPos4dMESA(d, d2, d3, d4);
    }

    public void glWindowPos4dvMESA(double[] dArr, int i) {
        this.f10342gl.glWindowPos4dvMESA(dArr, i);
    }

    public void glWindowPos4dvMESA(DoubleBuffer doubleBuffer) {
        this.f10342gl.glWindowPos4dvMESA(doubleBuffer);
    }

    public void glWindowPos4fMESA(float f, float f2, float f3, float f4) {
        this.f10342gl.glWindowPos4fMESA(f, f2, f3, f4);
    }

    public void glWindowPos4fvMESA(float[] fArr, int i) {
        this.f10342gl.glWindowPos4fvMESA(fArr, i);
    }

    public void glWindowPos4fvMESA(FloatBuffer floatBuffer) {
        this.f10342gl.glWindowPos4fvMESA(floatBuffer);
    }

    public void glWindowPos4iMESA(int i, int i2, int i3, int i4) {
        this.f10342gl.glWindowPos4iMESA(i, i2, i3, i4);
    }

    public void glWindowPos4ivMESA(IntBuffer intBuffer) {
        this.f10342gl.glWindowPos4ivMESA(intBuffer);
    }

    public void glWindowPos4ivMESA(int[] iArr, int i) {
        this.f10342gl.glWindowPos4ivMESA(iArr, i);
    }

    public void glWindowPos4sMESA(short s, short s2, short s3, short s4) {
        this.f10342gl.glWindowPos4sMESA(s, s2, s3, s4);
    }

    public void glWindowPos4svMESA(short[] sArr, int i) {
        this.f10342gl.glWindowPos4svMESA(sArr, i);
    }

    public void glWindowPos4svMESA(ShortBuffer shortBuffer) {
        this.f10342gl.glWindowPos4svMESA(shortBuffer);
    }

    public void glWriteMaskEXT(int i, int i2, int i3, int i4, int i5, int i6) {
        this.f10342gl.glWriteMaskEXT(i, i2, i3, i4, i5, i6);
    }

    public boolean isExtensionAvailable(String str) {
        return this.f10342gl.isExtensionAvailable(str);
    }

    public boolean isFunctionAvailable(String str) {
        return this.f10342gl.isFunctionAvailable(str);
    }

    public void setSwapInterval(int i) {
        this.f10342gl.setSwapInterval(i);
    }

    public Object getExtension(String str) {
        return this.f10342gl.getExtension(str);
    }
}
