package taikodom.render.gl;

import logic.thred.LogPrinter;

import javax.media.opengl.GL;
import java.nio.Buffer;

/* renamed from: taikodom.render.gl.GLWrapper */
/* compiled from: a */
public class GLWrapper extends GLWrapperBase {
    private static int MAX_USED_GL_CHANNELS = 8;
    private static LogPrinter logger = LogPrinter.m10275K(GLWrapper.class);

    public GLWrapper(GL gl) {
        super(gl);
        logger.info("Detecting video cards capabilities:");
        GLSupportedCaps.setVertexShader(gl.isExtensionAvailable("GL_ARB_vertex_shader"));
        GLSupportedCaps.setGlsl(gl.isExtensionAvailable("GL_ARB_shading_language_100"));
        GLSupportedCaps.setMultitexture(gl.isExtensionAvailable("GL_ARB_multitexture"));
        GLSupportedCaps.setVbo(gl.isExtensionAvailable("GL_ARB_vertex_buffer_object"));
        GLSupportedCaps.setTextureRectangle(gl.isExtensionAvailable("GL_ARB_texture_rectangle"));
        GLSupportedCaps.setTextureNPOT(gl.isExtensionAvailable("GL_ARB_texture_non_power_of_two"));
        GLSupportedCaps.setPbuffer(gl.isExtensionAvailable("GL_ARB_pbuffer"));
        GLSupportedCaps.setOcclusionQuery(gl.isExtensionAvailable("GL_ARB_occlusion_query"));
        int[] iArr = new int[1];
        gl.glGetIntegerv(34018, iArr, 0);
        GLSupportedCaps.setMaxTexChannels(iArr[0] < MAX_USED_GL_CHANNELS ? iArr[0] : MAX_USED_GL_CHANNELS);
        GLSupportedCaps.setVertexAttrib(true);
        try {
            gl.glDisableVertexAttribArrayARB(0);
        } catch (Exception e) {
            GLSupportedCaps.setVertexAttrib(false);
        }
        if (GLSupportedCaps.isOcclusionQuery()) {
            try {
                int[] iArr2 = new int[1];
                gl.glGenQueries(1, iArr2, 0);
                gl.glDeleteQueries(1, iArr2, 0);
            } catch (Exception e2) {
                GLSupportedCaps.setOcclusionQuery(false);
            }
        }
        try {
            gl.glBindBuffer(34962, 0);
        } catch (Exception e3) {
            GLSupportedCaps.setVbo(false);
        }
        GLSupportedCaps.setFbo(true);
        try {
            gl.glBindRenderbufferEXT(36160, 0);
        } catch (Exception e4) {
            GLSupportedCaps.setFbo(false);
        }
        if (GLSupportedCaps.isGlsl()) {
            try {
                gl.glLinkProgram(0);
            } catch (Exception e5) {
                GLSupportedCaps.setGlsl(false);
            }
        }
        GLSupportedCaps.setDds(gl.isExtensionAvailable("GL_ARB_texture_compression"));
        GLSupportedCaps.setDrawRangeElements(gl.isExtensionAvailable("GL_EXT_draw_range_elements"));
        logger.info("Vendor: " + gl.glGetString(7936));
        logger.info("Version: " + gl.glGetString(7938));
        logger.info("Renderer: " + gl.glGetString(7937));
        if (GLSupportedCaps.isGlsl()) {
            logger.info("GLSL supported");
        } else {
            logger.info("GLSL unsupported");
        }
        if (GLSupportedCaps.isMultitexture()) {
            logger.info("Multitexture supported");
        } else {
            logger.info("Multitexture unsupported");
        }
        if (GLSupportedCaps.isVbo()) {
            logger.info("VBO supported");
        } else {
            logger.info("VBO unsupported");
        }
        if (GLSupportedCaps.isDds()) {
            logger.info("Compressed texture supported");
        } else {
            logger.info("Compressed texture unsupported");
        }
        if (GLSupportedCaps.isDrawRangeElements()) {
            logger.info("Draw range elements supported");
        } else {
            logger.info("Draw range elements unsupported");
        }
        if (GLSupportedCaps.isPbuffer()) {
            logger.info("PBuffer supported");
        } else {
            logger.info("PBuffer unsupported");
        }
        if (GLSupportedCaps.isFbo()) {
            logger.info("Frame buffer object supported");
        } else {
            logger.info("Frame buffer object unsupported");
        }
        if (GLSupportedCaps.isTextureRectangle()) {
            logger.info("Texture rectangle supported");
        } else {
            logger.info("Texture rectangle unsupported");
        }
        if (GLSupportedCaps.isTextureNPOT()) {
            logger.info("Texture NPOT supported");
        } else {
            logger.info("Texture NPOT unsupported");
        }
    }

    public static LogPrinter getLogger() {
        return logger;
    }

    public void glEnableVertexAttribArray(int i) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glEnableVertexAttribArray(i);
        }
    }

    public void glDisableVertexAttribArray(int i) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glDisableVertexAttribArray(i);
        }
    }

    public void glEnableVertexAttribArrayARB(int i) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glEnableVertexAttribArrayARB(i);
        }
    }

    public void glDisableVertexAttribArrayARB(int i) {
        if (GLSupportedCaps.isVertexAttrib() && GLSupportedCaps.isVertexShader()) {
            super.glDisableVertexAttribArrayARB(i);
        }
    }

    public int glCreateShaderObjectARB(int i) {
        if (GLSupportedCaps.isGlsl()) {
            return super.glCreateShaderObjectARB(i);
        }
        return 0;
    }

    public void glBindBuffer(int i, int i2) {
        if (GLSupportedCaps.isVbo()) {
            super.glBindBuffer(i, i2);
        }
    }

    public void glBindBufferARB(int i, int i2) {
        if (GLSupportedCaps.isVbo()) {
            super.glBindBufferARB(i, i2);
        }
    }

    public void glBufferData(int i, int i2, Buffer buffer, int i3) {
        if (GLSupportedCaps.isVbo()) {
            super.glBufferData(i, i2, buffer, i3);
        }
    }

    public int glCreateProgramObjectARB() {
        if (GLSupportedCaps.isGlsl()) {
            return super.glCreateProgramObjectARB();
        }
        return 0;
    }

    public void glBindFramebufferEXT(int i, int i2) {
        if (GLSupportedCaps.isFbo()) {
            super.glBindFramebufferEXT(i, i2);
        }
    }

    public void glUseProgramObjectARB(int i) {
        if (GLSupportedCaps.isGlsl()) {
            super.glUseProgramObjectARB(i);
        }
    }

    public void glLinkProgram(int i) {
        if (GLSupportedCaps.isGlsl()) {
            super.glLinkProgram(i);
        }
    }
}
