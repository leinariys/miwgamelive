package taikodom.infra.comm.tcpnio;

import game.network.channel.client.HandlerClientState;
import game.network.message.C6115agj;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

/* compiled from: a */
public class NIOBandwidthMeter implements NIOBandwidthMeterMBean {
    public static final long gpi = 10000;
    private HandlerClientState gpg;
    private ObjectName gph = null;
    private C6115agj gpj = new C6115agj(10000);
    private C6115agj gpk = new C6115agj(10000);
    private C6115agj gpl = new C6115agj(10000);
    private C6115agj gpm = new C6115agj(10000);
    private C6115agj gpn = new C6115agj(10000);
    private C6115agj gpo = new C6115agj(10000);

    public NIOBandwidthMeter(HandlerClientState lz) {
        this.gpg = lz;
        cqY();
    }

    public void cqY() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            try {
                this.gph = new ObjectName("Network:name=NIO-" + this.gpg.getId());
                if (!platformMBeanServer.isRegistered(this.gph)) {
                    platformMBeanServer.registerMBean(this, this.gph);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void cqZ() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            try {
                if (platformMBeanServer.isRegistered(this.gph)) {
                    platformMBeanServer.unregisterMBean(this.gph);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void dump() {
        System.out.printf("%6.0f %6.0f %6.0f %6.0f %6.0f %6.0f\n", new Object[]{Float.valueOf(getCompressedBytesSentPerSecond()), Float.valueOf(getUncompressedBytesSentPerSecond()), Float.valueOf(getCompressedBytesReceivedPerSecond()), Float.valueOf(getUncompressedBytesReceivedPerSecond()), Float.valueOf(getSentMessageCountPerSecond()), Float.valueOf(getReceivedMessageCountPerSecond())});
    }

    public float getCompressedBytesReceivedPerSecond() {
        return this.gpj.mo13456hf((long) this.gpg.getMetricReadByte());
    }

    public float getCompressedBytesSentPerSecond() {
        return this.gpk.mo13456hf((long) this.gpg.getMetricWriteByte());
    }

    public float getReceivedMessageCountPerSecond() {
        return this.gpn.mo13456hf((long) this.gpg.getCountReadMessage());
    }

    public float getSentMessageCountPerSecond() {
        return this.gpo.mo13456hf((long) this.gpg.getCountWriteMessage());
    }

    public float getUncompressedBytesReceivedPerSecond() {
        return this.gpl.mo13456hf((long) this.gpg.getByteLength());
    }

    public float getUncompressedBytesSentPerSecond() {
        return this.gpm.mo13456hf((long) this.gpg.getAllOriginalByteLength());
    }
}
