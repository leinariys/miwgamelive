package taikodom.infra.comm.tcpnio;

/* compiled from: a */
public interface NIOBandwidthMeterMBean {
    float getCompressedBytesReceivedPerSecond();

    float getCompressedBytesSentPerSecond();

    float getReceivedMessageCountPerSecond();

    float getSentMessageCountPerSecond();

    float getUncompressedBytesReceivedPerSecond();

    float getUncompressedBytesSentPerSecond();
}
