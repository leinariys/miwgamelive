package taikodom.infra.script;

/* compiled from: a */
public interface MeasureMBean {
    float getAvgExecuteTime();

    float getAvgInfraMethodsCalled();

    float getAvgObjectsChangedInTransaction();

    float getAvgObjectsCreatedInTransaction();

    float getAvgObjectsTouchedInPostponed();

    float getAvgObjectsTouchedInTransaction();

    float getAvgReadLocksInTransaction();

    float getAvgReexecutePerc();

    float getAvgTransactionTime();

    float getAvgTransactionalGets();

    float getAvgTransactionalGetsInPostponed();

    float getAvgTransactionalMethodsCalled();

    float getAvgTransactionalSets();

    float getAvgTransactionalSetsInPostponed();

    float getAvgWriteLocksInTransaction();

    String getClassName();

    int getExecuteCount();

    long getExecuteTime();

    int getInfraMethodsCalled();

    String getName();

    int getObjectsChangedInTransaction();

    int getObjectsCreatedInTransaction();

    int getObjectsTouchedInPostponed();

    int getObjectsTouchedInTransaction();

    float getPercUpTime();

    int getReadLocksInTransaction();

    int getTransactionCount();

    long getTransactionReexecuteCount();

    long getTransactionTime();

    int getTransactionalGets();

    int getTransactionalGetsInPostponed();

    int getTransactionalMethodsCalled();

    int getTransactionalSets();

    int getTransactionalSetsInPostponed();

    int getWriteLocksInTransaction();
}
