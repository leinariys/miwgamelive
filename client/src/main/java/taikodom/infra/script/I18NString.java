package taikodom.infra.script;

import gnu.trove.THashMap;
import p001a.C2562gp;
import p001a.C6129agx;

import java.io.Externalizable;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.*;

/* compiled from: a */
public final class I18NString implements Externalizable, Serializable, Cloneable {
    public static final String DEFAULT_LOCATION = "en";

    private static String currentLocation = System.getProperty("user.language");
    private Map<String, String> map = new THashMap(1);

    public I18NString() {
    }

    public I18NString(String str) {
        set(DEFAULT_LOCATION, str);
    }

    public static final String getCurrentLocation() {
        return currentLocation;
    }

    /* renamed from: kS */
    public static void setCurrentLocation(String str) {
        currentLocation = str;
    }

    public String get() {
        return get(currentLocation);
    }

    public String get(String str) {
        String str2 = this.map.get(str);
        if (str2 != null) {
            return str2;
        }
        String str3 = this.map.get(DEFAULT_LOCATION);
        if (str3 != null) {
            return str3;
        }
        Iterator<Map.Entry<String, String>> it = this.map.entrySet().iterator();
        if (it.hasNext()) {
            return (String) it.next().getValue();
        }
        return null;
    }

    public void set(String str, String str2) {
        if (str != null && str2 != null) {
            this.map.put(str.intern(), str2.intern());
        }
    }

    public Map<String, String> getMap() {
        return this.map;
    }

    public String toString() {
        return get();
    }

    public Object clone() {
        I18NString i18NString = new I18NString();
        i18NString.map = new HashMap(this.map);
        return i18NString;
    }

    /* JADX WARNING: type inference failed for: r0v4, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void readExternal(java.io.ObjectInput r6) {
        /*
            r5 = this;
            short r3 = r6.readShort()
            r0 = 0
            r2 = r0
        L_0x0006:
            if (r2 < r3) goto L_0x0009
            return
        L_0x0009:
            a.gp r0 = p001a.C2562gp.f7767PX
            java.lang.Object r0 = r0.mo2362b((java.io.ObjectInput) r6)
            java.lang.String r0 = (java.lang.String) r0
            a.gp r1 = p001a.C2562gp.f7767PX
            java.lang.Object r1 = r1.mo2362b((java.io.ObjectInput) r6)
            java.lang.String r1 = (java.lang.String) r1
            if (r0 == 0) goto L_0x002a
            if (r1 == 0) goto L_0x002a
            java.util.Map<java.lang.String, java.lang.String> r4 = r5.map
            java.lang.String r0 = r0.intern()
            java.lang.String r1 = r1.intern()
            r4.put(r0, r1)
        L_0x002a:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.infra.game.script.I18NString.readExternal(java.io.ObjectInput):void");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        Map<String, String> map2;
        Map<String, String> map3 = this.map;
        if (C6129agx.m22158d(objectOutput)) {
            map2 = new TreeMap<>(map3);
        } else {
            map2 = map3;
        }
        Set<Map.Entry<String, String>> entrySet = map2.entrySet();
        objectOutput.writeShort(map2.size());
        for (Map.Entry next : entrySet) {
            C2562gp.f7767PX.serializeData(objectOutput, next.getKey());
            C2562gp.f7767PX.serializeData(objectOutput, next.getValue());
        }
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof I18NString) {
            return this.map.equals(((I18NString) obj).map);
        }
        return super.equals(obj);
    }
}
