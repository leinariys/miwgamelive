package taikodom.infra.script.repository.jdbm.jmx;

import p001a.WraperDbFileImpl;
import p001a.aRG;

/* compiled from: a */
public class JDBMConsole implements aRG, JDBMConsoleMBean {
    private final WraperDbFileImpl fQr;
    private int fQs = 0;
    private long fQt = System.currentTimeMillis();

    public JDBMConsole(WraperDbFileImpl abr) {
        this.fQr = abr;
    }

    public float getCommits_perSecond() {
        long currentTimeMillis = System.currentTimeMillis();
        this.fQt = currentTimeMillis;
        return ((float) this.fQr.dqn()) / ((float) (((double) (currentTimeMillis - this.fQt)) / 1000.0d));
    }

    public float getTransactionQueueSize() {
        return (float) this.fQr.dqm();
    }
}
