package taikodom.infra.script.ai.behaviours;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C7036azx;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.TM */
/* compiled from: a */
public class AIBehaviourConfig extends TaikodomObject implements C1616Xf {
    public static final C5663aRz _f_dodgeAngle = null;
    public static final C5663aRz _f_dodgeDistance = null;
    public static final C5663aRz _f_dodgeReactionTime = null;
    public static final C5663aRz _f_pursuitDistance = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm ekP = null;
    public static final C2491fm ekQ = null;
    public static final C2491fm ekR = null;
    public static final C2491fm ekS = null;
    public static final C2491fm ekT = null;
    public static final C2491fm ekU = null;
    public static final C2491fm ekV = null;
    public static final C2491fm ekW = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3c2d9a6240eb2c5f77c9d02aac0edc68", aum = 2)
    private static float dodgeAngle;
    @C0064Am(aul = "167f845364ffb2d380a720526d8c9a65", aum = 1)
    private static float dodgeDistance;
    @C0064Am(aul = "c4bfeb3118a5bfb03bc9c86fb4b3dabb", aum = 0)
    private static float dodgeReactionTime;
    @C0064Am(aul = "c3ede6e57ecc12156836e63070a9acda", aum = 3)
    private static float pursuitDistance;

    static {
        m9939V();
    }

    public AIBehaviourConfig() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AIBehaviourConfig(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9939V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(AIBehaviourConfig.class, "c4bfeb3118a5bfb03bc9c86fb4b3dabb", i);
        _f_dodgeReactionTime = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AIBehaviourConfig.class, "167f845364ffb2d380a720526d8c9a65", i2);
        _f_dodgeDistance = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AIBehaviourConfig.class, "3c2d9a6240eb2c5f77c9d02aac0edc68", i3);
        _f_dodgeAngle = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AIBehaviourConfig.class, "c3ede6e57ecc12156836e63070a9acda", i4);
        _f_pursuitDistance = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(AIBehaviourConfig.class, "6c34faa568b90c44faa3aeea9e6faee2", i6);
        ekP = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(AIBehaviourConfig.class, "8c44734dae543503244f2fc8a9f99e81", i7);
        ekQ = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(AIBehaviourConfig.class, "f7f3a8929620b5edbd81dda6da90d129", i8);
        ekR = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(AIBehaviourConfig.class, "0e7e532b8e4be837f4f0269349ee8f81", i9);
        ekS = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(AIBehaviourConfig.class, "cb3200b3d63a23caea451ee69fc2a07f", i10);
        ekT = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(AIBehaviourConfig.class, "b84f843cc9f3a3cbf2e5aa2b5d66a6a0", i11);
        ekU = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(AIBehaviourConfig.class, "949e742c5ce169f397b72e204125779b", i12);
        ekV = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(AIBehaviourConfig.class, "e14db577d7a94a0dd0e44098e7eb38bc", i13);
        ekW = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AIBehaviourConfig.class, C7036azx.class, _m_fields, _m_methods);
    }

    private float bwe() {
        return bFf().mo5608dq().mo3211m(_f_dodgeReactionTime);
    }

    private float bwf() {
        return bFf().mo5608dq().mo3211m(_f_dodgeDistance);
    }

    private float bwg() {
        return bFf().mo5608dq().mo3211m(_f_dodgeAngle);
    }

    private float bwh() {
        return bFf().mo5608dq().mo3211m(_f_pursuitDistance);
    }

    /* renamed from: ht */
    private void m9942ht(float f) {
        bFf().mo5608dq().mo3150a(_f_dodgeReactionTime, f);
    }

    /* renamed from: hu */
    private void m9943hu(float f) {
        bFf().mo5608dq().mo3150a(_f_dodgeDistance, f);
    }

    /* renamed from: hv */
    private void m9944hv(float f) {
        bFf().mo5608dq().mo3150a(_f_dodgeAngle, f);
    }

    /* renamed from: hw */
    private void m9945hw(float f) {
        bFf().mo5608dq().mo3150a(_f_pursuitDistance, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C7036azx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Float(bwi());
            case 1:
                m9946hx(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(bwk());
            case 3:
                m9947hz(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Float(bwm());
            case 5:
                m9940hB(((Float) args[0]).floatValue());
                return null;
            case 6:
                return new Float(bwo());
            case 7:
                m9941hD(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Reaction time (s)")
    public float bwj() {
        switch (bFf().mo6893i(ekP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekP, new Object[0]));
                break;
        }
        return bwi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Distance to start dodging")
    public float bwl() {
        switch (bFf().mo6893i(ekR)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekR, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekR, new Object[0]));
                break;
        }
        return bwk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (degrees)")
    public float bwn() {
        switch (bFf().mo6893i(ekT)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekT, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekT, new Object[0]));
                break;
        }
        return bwm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pursuit - Desired distance to target")
    public float bwp() {
        switch (bFf().mo6893i(ekV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekV, new Object[0]));
                break;
        }
        return bwo();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Distance to start dodging")
    /* renamed from: hA */
    public void mo5661hA(float f) {
        switch (bFf().mo6893i(ekS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekS, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekS, new Object[]{new Float(f)}));
                break;
        }
        m9947hz(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (degrees)")
    /* renamed from: hC */
    public void mo5662hC(float f) {
        switch (bFf().mo6893i(ekU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekU, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekU, new Object[]{new Float(f)}));
                break;
        }
        m9940hB(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pursuit - Desired distance to target")
    /* renamed from: hE */
    public void mo5663hE(float f) {
        switch (bFf().mo6893i(ekW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekW, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekW, new Object[]{new Float(f)}));
                break;
        }
        m9941hD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Reaction time (s)")
    /* renamed from: hy */
    public void mo5664hy(float f) {
        switch (bFf().mo6893i(ekQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekQ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekQ, new Object[]{new Float(f)}));
                break;
        }
        m9946hx(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m9942ht(1.0f);
        m9943hu(2000.0f);
        m9944hv(12.0f);
        m9945hw(200.0f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Reaction time (s)")
    @C0064Am(aul = "6c34faa568b90c44faa3aeea9e6faee2", aum = 0)
    private float bwi() {
        return bwe();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Reaction time (s)")
    @C0064Am(aul = "8c44734dae543503244f2fc8a9f99e81", aum = 0)
    /* renamed from: hx */
    private void m9946hx(float f) {
        m9942ht(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Distance to start dodging")
    @C0064Am(aul = "f7f3a8929620b5edbd81dda6da90d129", aum = 0)
    private float bwk() {
        return bwf();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Distance to start dodging")
    @C0064Am(aul = "0e7e532b8e4be837f4f0269349ee8f81", aum = 0)
    /* renamed from: hz */
    private void m9947hz(float f) {
        m9943hu(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (degrees)")
    @C0064Am(aul = "cb3200b3d63a23caea451ee69fc2a07f", aum = 0)
    private float bwm() {
        return bwg();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (degrees)")
    @C0064Am(aul = "b84f843cc9f3a3cbf2e5aa2b5d66a6a0", aum = 0)
    /* renamed from: hB */
    private void m9940hB(float f) {
        m9944hv(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pursuit - Desired distance to target")
    @C0064Am(aul = "949e742c5ce169f397b72e204125779b", aum = 0)
    private float bwo() {
        return bwh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pursuit - Desired distance to target")
    @C0064Am(aul = "e14db577d7a94a0dd0e44098e7eb38bc", aum = 0)
    /* renamed from: hD */
    private void m9941hD(float f) {
        m9945hw(f);
    }
}
