package taikodom.infra.script.ai.behaviours;

/* renamed from: taikodom.infra.game.script.ai.behaviours.AimQuality */
/* compiled from: a */
public enum AimQuality {
    FIRE_ONLY_ON_TARGET,
    FIRE_NEAR_TARGET,
    FIRE_PREDICTING_MOVEMENT
}
