package taikodom.infra.script;

/* compiled from: a */
public class Measure implements MeasureMBean {
    public String className;
    public int enA;
    public long enB;
    public long enC;
    public float enH;
    public float enI;
    public float enJ;
    public float enK;
    public float ene;
    public float enf;
    public float eng;
    public float enh;
    public float eni;
    public float enj;
    public float enk;
    public float enl;
    public float enm;
    public float enn;
    public float eno;
    public int enp;
    public long enq;
    public int enr;
    public int ens;
    public int ent;
    public float enu;
    public int env;
    public int enw;
    public int enx;
    public int eny;
    public int enz;
    public String name;
    private int enD;
    private int enE;
    private int enF;
    private int enG;

    public float getAvgObjectsChangedInTransaction() {
        return this.enH;
    }

    public void setAvgObjectsChangedInTransaction(float f) {
        this.enH = f;
    }

    public float getAvgReadLocksInTransaction() {
        return this.enI;
    }

    public void setAvgReadLocksInTransaction(float f) {
        this.enI = f;
    }

    public float getAvgWriteLocksInTransaction() {
        return this.enJ;
    }

    public void setAvgWriteLocksInTransaction(float f) {
        this.enJ = f;
    }

    public float getAvgObjectsCreatedInTransaction() {
        return this.enK;
    }

    public void setAvgObjectsCreatedInTransaction(float f) {
        this.enK = f;
    }

    public float getAvgExecuteTime() {
        return this.ene;
    }

    public void setAvgExecuteTime(float f) {
        this.ene = f;
    }

    public float getAvgInfraMethodsCalled() {
        return this.enf;
    }

    public void setAvgInfraMethodsCalled(float f) {
        this.enf = f;
    }

    public float getAvgObjectsTouchedInPostponed() {
        return this.eng;
    }

    public void setAvgObjectsTouchedInPostponed(float f) {
        this.eng = f;
    }

    public float getAvgObjectsTouchedInTransaction() {
        return this.enh;
    }

    public void setAvgObjectsTouchedInTransaction(float f) {
        this.enh = f;
    }

    public float getAvgReexecutePerc() {
        return this.eni;
    }

    public void setAvgReexecutePerc(float f) {
        this.eni = f;
    }

    public float getAvgTransactionalGets() {
        return this.enj;
    }

    public void setAvgTransactionalGets(float f) {
        this.enj = f;
    }

    public float getAvgTransactionalGetsInPostponed() {
        return this.enk;
    }

    public void setAvgTransactionalGetsInPostponed(float f) {
        this.enk = f;
    }

    public float getAvgTransactionalMethodsCalled() {
        return this.enl;
    }

    public void setAvgTransactionalMethodsCalled(float f) {
        this.enl = f;
    }

    public float getAvgTransactionalSets() {
        return this.enm;
    }

    public void setAvgTransactionalSets(float f) {
        this.enm = f;
    }

    public float getAvgTransactionalSetsInPostponed() {
        return this.enn;
    }

    public void setAvgTransactionalSetsInPostponed(float f) {
        this.enn = f;
    }

    public float getAvgTransactionTime() {
        return this.eno;
    }

    public void setAvgTransactionTime(float f) {
        this.eno = f;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String str) {
        this.className = str;
    }

    public int getExecuteCount() {
        return this.enp;
    }

    public void setExecuteCount(int i) {
        this.enp = i;
    }

    public long getExecuteTime() {
        return this.enq;
    }

    public void setExecuteTime(long j) {
        this.enq = j;
    }

    public int getInfraMethodsCalled() {
        return this.enr;
    }

    public void setInfraMethodsCalled(int i) {
        this.enr = i;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    public int getObjectsTouchedInPostponed() {
        return this.ens;
    }

    public void setObjectsTouchedInPostponed(int i) {
        this.ens = i;
    }

    public int getObjectsTouchedInTransaction() {
        return this.ent;
    }

    public void setObjectsTouchedInTransaction(int i) {
        this.ent = i;
    }

    public float getPercUpTime() {
        return this.enu;
    }

    public void setPercUpTime(float f) {
        this.enu = f;
    }

    public int getTransactionalGets() {
        return this.env;
    }

    public void setTransactionalGets(int i) {
        this.env = i;
    }

    public int getTransactionalGetsInPostponed() {
        return this.enw;
    }

    public void setTransactionalGetsInPostponed(int i) {
        this.enw = i;
    }

    public int getTransactionalMethodsCalled() {
        return this.enx;
    }

    public void setTransactionalMethodsCalled(int i) {
        this.enx = i;
    }

    public int getTransactionalSets() {
        return this.eny;
    }

    public void setTransactionalSets(int i) {
        this.eny = i;
    }

    public int getTransactionalSetsInPostponed() {
        return this.enz;
    }

    public void setTransactionalSetsInPostponed(int i) {
        this.enz = i;
    }

    public int getTransactionCount() {
        return this.enA;
    }

    public void setTransactionCount(int i) {
        this.enA = i;
    }

    public long getTransactionReexecuteCount() {
        return this.enB;
    }

    public void setTransactionReexecuteCount(long j) {
        this.enB = j;
    }

    public long getTransactionTime() {
        return this.enC;
    }

    public void setTransactionTime(long j) {
        this.enC = j;
    }

    public int getObjectsChangedInTransaction() {
        return this.enG;
    }

    public void setObjectsChangedInTransaction(int i) {
        this.enG = i;
    }

    public int getReadLocksInTransaction() {
        return this.enF;
    }

    public void setReadLocksInTransaction(int i) {
        this.enF = i;
    }

    public int getWriteLocksInTransaction() {
        return this.enE;
    }

    public void setWriteLocksInTransaction(int i) {
        this.enE = i;
    }

    public int getObjectsCreatedInTransaction() {
        return this.enD;
    }

    public void setObjectsCreatedInTransaction(int i) {
        this.enD = i;
    }
}
