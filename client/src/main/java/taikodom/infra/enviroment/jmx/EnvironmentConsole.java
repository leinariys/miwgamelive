package taikodom.infra.enviroment.jmx;

import game.engine.DataGameEvent;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.Measure;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.*;

/* compiled from: a */
public class EnvironmentConsole implements C5516aMi, EnvironmentConsoleMBean {
    private final DataGameEvent cVF;
    long cVG;
    long cVH;
    ThreadMXBean cVI;
    Map<Long, Long> cVJ;
    int cVK;
    long cVL = System.currentTimeMillis();
    long cVM = System.currentTimeMillis();
    private long cVN;
    private long cVO = System.currentTimeMillis();
    private long cVP = 0;
    private long cVQ = System.currentTimeMillis();
    private float cVR = 0.0f;

    public EnvironmentConsole(DataGameEvent jz) {
        this.cVF = jz;
        this.cVG = 0;
        this.cVH = -1;
        this.cVI = ManagementFactory.getThreadMXBean();
        this.cVJ = new HashMap();
        if (this.cVI.isThreadCpuTimeSupported()) {
            this.cVI.setThreadCpuTimeEnabled(true);
        } else {
            System.err.println("CPU Time not supported!");
        }
        this.cVK = ManagementFactory.getOperatingSystemMXBean().getAvailableProcessors();
    }

    public float getTrans_rerunPerSecond() {
        long currentTimeMillis = System.currentTimeMillis();
        float andSet = ((float) (this.cVF.bGr().hap.getAndSet(0) + 0)) / (((float) (currentTimeMillis - this.cVM)) / 1000.0f);
        this.cVM = currentTimeMillis;
        return andSet;
    }

    public long getTrans_averageTime() {
        azC bGr = this.cVF.bGr();
        long andSet = bGr.ham.getAndSet(0);
        long andSet2 = bGr.han.getAndSet(0);
        if (andSet2 != 0) {
            this.cVG = andSet / andSet2;
        }
        return this.cVG;
    }

    public float getTrans_perSecond() {
        long currentTimeMillis = System.currentTimeMillis();
        long j = currentTimeMillis - this.cVL;
        if (j == 0) {
            j = 1;
        }
        float andSet = ((float) this.cVF.bGr().haw.getAndSet(0)) / (((float) j) / 1000.0f);
        this.cVL = currentTimeMillis;
        return andSet;
    }

    public long getTrans_maxTime() {
        long j = this.cVF.bGr().hav.get();
        if (j != Long.MIN_VALUE) {
            return j;
        }
        return 0;
    }

    public long getTrans_minTime() {
        long j = this.cVF.bGr().hau.get();
        if (j != Long.MAX_VALUE) {
            return j;
        }
        return 0;
    }

    public double getTrans_stdDeviation() {
        azC bGr = this.cVF.bGr();
        long j = bGr.har.get() + 0;
        long j2 = bGr.haq.get() + 0;
        return Math.sqrt((((double) (bGr.has.get() + 0)) - (((double) (j * j)) / ((double) j2))) / ((double) j2));
    }

    public float getDataReq_perSecond() {
        long currentTimeMillis = System.currentTimeMillis();
        float andSet = ((float) (this.cVF.bGr().hat.getAndSet(0) + 0)) / (((float) (currentTimeMillis - this.cVN)) / 1000.0f);
        this.cVN = currentTimeMillis;
        return andSet;
    }

    public float getCPU_usage() {
        long j;
        if (this.cVH == -1) {
            this.cVH = System.nanoTime();
            return 0.0f;
        }
        long[] allThreadIds = this.cVI.getAllThreadIds();
        long j2 = 0;
        int length = allThreadIds.length;
        int i = 0;
        while (i < length) {
            long j3 = allThreadIds[i];
            long threadCpuTime = this.cVI.getThreadCpuTime(j3);
            if (threadCpuTime == -1) {
                this.cVJ.remove(Long.valueOf(j3));
                j = j2;
            } else {
                Long l = this.cVJ.get(Long.valueOf(j3));
                if (l != null) {
                    j = (threadCpuTime - l.longValue()) + j2;
                    this.cVJ.put(Long.valueOf(j3), Long.valueOf(threadCpuTime));
                } else {
                    this.cVJ.put(Long.valueOf(j3), new Long(0));
                    j = j2;
                }
            }
            i++;
            j2 = j;
        }
        long nanoTime = System.nanoTime();
        long j4 = nanoTime - this.cVH;
        this.cVH = nanoTime;
        float f = ((((float) j2) * 100.0f) / ((float) j4)) / ((float) this.cVK);
        if (f > 100.0f) {
            return 100.0f;
        }
        if (f < 0.0f) {
            return 0.0f;
        }
        return f;
    }

    public SortedMap<String, Measure> getTransactionalMethods() {
        List<Measure> transactionalData = getTransactionalData();
        TreeMap treeMap = new TreeMap();
        for (Measure next : transactionalData) {
            treeMap.put(String.valueOf(next.getClassName()) + "." + next.getName(), next);
        }
        return treeMap;
    }

    public String getTransactionalReport() {
        return getRealTransactionalReport();
    }

    public String getRealTransactionalReport() {
        try {
            List<Measure> transactionalData = getTransactionalData();
            long j = 0;
            C3776uu uuVar = new C3776uu();
            StringBuilder sb = new StringBuilder();
            long j2 = 0;
            for (Measure next : transactionalData) {
                j += next.enC;
                j2 += (long) next.enA;
                sb.append(uuVar.mo22465c("method", String.valueOf(next.getClassName()) + "." + next.getName(), "tran. time", Long.valueOf(next.enC), "tran. count", Integer.valueOf(next.enA), "avg tran. time", Float.valueOf(next.eno), "perc of up time", Float.valueOf(next.enu), "exec. count", Integer.valueOf(next.enp), "exec. time", Long.valueOf(next.enq), "avg exec. time", Float.valueOf(next.ene), "tran. reexec. count", Long.valueOf(next.enB), "avg reexec. perc.", Float.valueOf(next.eni), "avg objs touched in tran.", Float.valueOf(next.enh), "avg objs created in tran.", Float.valueOf(next.enK), "avg write locks tran.", Float.valueOf(next.enJ), "avg read locks in tran.", Float.valueOf(next.enI), "avg objs changed in tran.", Float.valueOf(next.enH), "avg tran. methods calls", Float.valueOf(next.enl), "avg tran. gets", Float.valueOf(next.enj), "avg tran. sets", Float.valueOf(next.enm), "avg objs touched in postponed", Float.valueOf(next.eng), "avg tran. sets in postponed", Float.valueOf(next.enn), "avg tran. gets in postponed", Float.valueOf(next.enk), "avg infra Methods calls", Float.valueOf(next.enf))).append("\n");
            }
            sb.append(uuVar.mo22465c("method", "----> Total", "tran. time", Long.valueOf(j), "tran. count", Long.valueOf(j2), "avg tran. time", Float.valueOf(((float) j) / ((float) j2)), "perc of up time", Float.valueOf(100.0f * (((float) j) / (((float) getUpTimeInMillis()) * 1000.0f))))).append("\n");
            sb.append("Obs. Times in micro seconds").append("\r\n");
            sb.append("Up time: " + getUpTime()).append("\r\n");
            return sb.toString();
        } catch (RuntimeException e) {
            e.printStackTrace();
            return e.toString();
        }
    }

    private List<Measure> getTransactionalData() {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry next : C1720ZS.eRE.entrySet()) {
            if (next.getValue() instanceof C2491fm) {
                C2491fm fmVar = (C2491fm) next.getValue();
                if (fmVar.getTransactionCount() > 0) {
                    Measure measure = new Measure();
                    measure.name = fmVar.name();
                    measure.className = fmVar.mo4752hD().getName();
                    measure.enC = fmVar.getTransactionTime();
                    measure.enu = (((float) measure.enC) / (((float) getUpTimeInMillis()) * 1000.0f)) * 100.0f;
                    measure.enA = fmVar.getTransactionCount();
                    measure.eno = ((float) measure.enC) / ((float) measure.enA);
                    measure.enp = fmVar.getExecuteCount();
                    measure.enq = fmVar.getExecuteTime();
                    measure.ene = ((float) measure.enq) / ((float) measure.enp);
                    measure.enB = (long) (measure.enp - measure.enA);
                    measure.eni = (((float) (measure.enp - measure.enA)) * 100.0f) / ((float) measure.enA);
                    measure.ent = fmVar.getObjectsTouchedInTransaction();
                    measure.enx = fmVar.getTransactionalMethodsCalled();
                    measure.env = fmVar.getTransactionalGets();
                    measure.eny = fmVar.getTransactionalSets();
                    measure.ens = fmVar.getObjectsTouchedInPostponed();
                    measure.enz = fmVar.getTransactionalSetsInPostponed();
                    measure.enw = fmVar.getTransactionalGetsInPostponed();
                    measure.enr = fmVar.getInfraMethodsCalled();
                    measure.enh = ((float) fmVar.getObjectsTouchedInTransaction()) / ((float) measure.enA);
                    measure.enl = ((float) fmVar.getTransactionalMethodsCalled()) / ((float) measure.enA);
                    measure.enj = ((float) fmVar.getTransactionalGets()) / ((float) measure.enA);
                    measure.enm = ((float) fmVar.getTransactionalSets()) / ((float) measure.enA);
                    measure.eng = ((float) fmVar.getObjectsTouchedInPostponed()) / ((float) measure.enA);
                    measure.enn = ((float) fmVar.getTransactionalSetsInPostponed()) / ((float) measure.enA);
                    measure.enk = ((float) fmVar.getTransactionalGetsInPostponed()) / ((float) measure.enA);
                    measure.enf = ((float) fmVar.getInfraMethodsCalled()) / ((float) measure.enA);
                    measure.enK = ((float) fmVar.getObjectsCreatedInTransaction()) / ((float) measure.enA);
                    measure.enJ = ((float) fmVar.getWriteLocksInTransaction()) / ((float) measure.enA);
                    measure.enI = ((float) fmVar.getReadLocksInTransaction()) / ((float) measure.enA);
                    measure.enH = ((float) fmVar.getObjectsChangedInTransaction()) / ((float) measure.enA);
                    arrayList.add(measure);
                }
            }
        }
        Collections.sort(arrayList, new C5117a());
        return arrayList;
    }

    public String getUpTime() {
        long currentTimeMillis = System.currentTimeMillis() - this.cVF.eKo;
        String format = String.format("%2d %02d:%02d:%02d ", new Object[]{Long.valueOf(currentTimeMillis / 86400000), Long.valueOf((currentTimeMillis / 3600000) % 24), Long.valueOf((currentTimeMillis / 60000) % 60), Long.valueOf((currentTimeMillis / 1000) % 60)});
        System.out.println(format);
        return format;
    }

    public long getUpTimeInMillis() {
        return System.currentTimeMillis() - this.cVF.eKo;
    }

    public int getScriptObjectCount() {
        return this.cVF.getScriptObjectCount();
    }

    public long getPullCount() {
        long j = 0;
        Iterator<Long> it = C1875af.m21423dx().values().iterator();
        while (true) {
            long j2 = j;
            if (!it.hasNext()) {
                return j2;
            }
            j = it.next().longValue() + j2;
        }
    }

    public float getPulls_perSecond() {
        long pullCount = getPullCount();
        long j = pullCount - this.cVP;
        this.cVP = pullCount;
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = currentTimeMillis - this.cVO;
        if (j2 == 0) {
            j2 = 1;
        }
        this.cVO = currentTimeMillis;
        return ((float) j) / (((float) j2) / 1000.0f);
    }

    public float getWriteLocks_perSecond() {
        long currentTimeMillis = System.currentTimeMillis();
        long j = currentTimeMillis - this.cVQ;
        long j2 = j == 0 ? 1 : j;
        this.cVQ = currentTimeMillis;
        float f = 0.0f;
        Iterator<Measure> it = getTransactionalData().iterator();
        while (true) {
            float f2 = f;
            if (!it.hasNext()) {
                this.cVR = f2;
                return (f2 - this.cVR) / (((float) j2) / 1000.0f);
            }
            Measure next = it.next();
            f = (((float) next.enA) * next.enJ) + f2;
        }
    }

    public int getPendingRequestsCount() {
        return this.cVF.bHg();
    }

    /* renamed from: taikodom.infra.enviroment.jmx.EnvironmentConsole$a */
    class C5117a implements Comparator<Measure> {
        C5117a() {
        }

        /* renamed from: a */
        public int compare(Measure measure, Measure measure2) {
            long j = measure.enC - measure2.enC;
            return -(j > 0 ? 1 : j == 0 ? 0 : -1);
        }
    }
}
