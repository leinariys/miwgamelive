package taikodom.addon.splashscreen;

import game.engine.C6082agC;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.aUU;
import logic.IAddonSettings;
import logic.aVH;
import logic.render.IEngineGraphics;
import logic.ui.Panel;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.BinkTexture;
import taikodom.render.BinkVideo;
import taikodom.render.ExclusiveVideoPlayer;
import taikodom.render.Video;
import taikodom.render.gui.GBillboard;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@TaikodomAddon("taikodom.addon.splashscreen")
/* compiled from: a */
public class SplashScreen implements C2495fo {
    private Panel ijO;
    private C6124ags<ExclusiveVideoPlayer> ijP;
    private aVH<C3689to> ijQ;
    private Image ijR;
    private GBillboard ijS;

    /* renamed from: kj */
    private IAddonProperties f10272kj;

    /* renamed from: lV */
    private IEngineGraphics f10273lV;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10272kj = addonPropertie;
        this.f10273lV = addonPropertie.ale();
        for (aUU next : this.f10272kj.aVU().mo13967b(aUU.class)) {
            if (next.dAD() == 0) {
                this.f10272kj.aVU().mo13974g(next);
                open();
                return;
            }
        }
    }

    private void open() {
        C6082agC.bWV().aRS();
        this.ijR = this.f10272kj.bHv().adz().getImage("/data/gui/imageset/Splash_Screen.bik");
        ((BinkTexture) this.ijR.getTexture()).play();
        this.ijO = new Panel();
        this.f10272kj.bHv().addInRootPane(this.ijO);
        this.ijO.setLocation(0, 0);
        this.ijO.setSize(this.f10272kj.bHv().getScreenWidth(), this.f10272kj.bHv().getScreenHeight());
        this.ijO.setVisible(true);
        BinkVideo binkVideo = new BinkVideo((BinkTexture) this.ijR.getTexture());
        this.f10273lV = this.f10272kj.ale();
        this.f10273lV.mo3065eI(1000);
        this.f10273lV.mo3006a((Video) binkVideo);
        this.ijQ = new C5003b();
        this.ijP = new C5004c();
        this.ijO.addMouseListener(new C5002a());
        this.f10272kj.aVU().mo13966a("endVideo", ExclusiveVideoPlayer.class, this.ijP);
        this.f10272kj.aVU().mo13965a(C3689to.class, this.ijQ);
    }

    /* access modifiers changed from: protected */
    public void stopVideo() {
        this.f10272kj.aVU().mo13975h(new aUU(1));
        this.f10273lV.aeD();
        stop();
    }

    public void stop() {
        C6082agC.bWV().aRR();
        if (this.ijO != null) {
            this.ijO.getParent().remove(this.ijO);
            this.ijO = null;
        }
        if (this.ijR != null) {
            ((BinkTexture) this.ijR.getTexture()).releaseReferences();
        }
        if (this.f10273lV != null) {
            this.f10273lV.aex();
        }
        this.f10272kj.aVU().mo13964a(this.ijP);
        this.f10272kj.aVU().mo13964a(this.ijQ);
    }

    /* renamed from: taikodom.addon.splashscreen.SplashScreen$b */
    /* compiled from: a */
    class C5003b extends C6124ags<C3689to> {
        C5003b() {
        }

        /* renamed from: b */
        public boolean updateInfo(C3689to toVar) {
            SplashScreen.this.stopVideo();
            return false;
        }
    }

    /* renamed from: taikodom.addon.splashscreen.SplashScreen$c */
    /* compiled from: a */
    class C5004c extends C6124ags<ExclusiveVideoPlayer> {
        C5004c() {
        }

        /* renamed from: b */
        public boolean updateInfo(ExclusiveVideoPlayer exclusiveVideoPlayer) {
            SplashScreen.this.stopVideo();
            return false;
        }
    }

    /* renamed from: taikodom.addon.splashscreen.SplashScreen$a */
    class C5002a extends MouseAdapter {
        C5002a() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            SplashScreen.this.stopVideo();
        }
    }
}
