package taikodom.addon.characterselection;

import com.hoplon.geometry.Color;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.network.message.externalizable.C0939Nn;
import game.script.avatar.AvatarManager;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.player.Player;
import game.script.ship.ShipType;
import game.script.ship.Station;
import gnu.trove.THashMap;
import logic.C6961axa;
import logic.WrapRunnable;
import logic.aWa;
import logic.render.IEngineGraphics;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import logic.ui.*;
import logic.ui.item.InternalFrame;
import logic.ui.item.TextField;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.RenderView;
import taikodom.render.SceneView;
import taikodom.render.camera.OrbitalCamera;
import taikodom.render.gui.GBillboard;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.*;
import taikodom.render.textures.Material;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@TaikodomAddon("taikodom.addon.characterselection")
/* renamed from: a.nY */
/* compiled from: a */
public class C3058nY implements aWa {
    /* access modifiers changed from: private */
    public static final LogPrinter logger = LogPrinter.setClass(C3058nY.class);
    private static final double gnI = 3.5d;
    private static final double gnJ = 0.9d;
    private static final int gnK = 1;
    private static final double gnL = 0.01d;
    private static final double gnM = 0.98d;
    private static final C6811auD ilk = new C6811auD(C3667tV.AVATAR_SPIN_LOOP);
    private static final C6811auD ill = new C6811auD(C3667tV.AVATAR_SPIN_LOOP, true);
    /* renamed from: qw */
    private static final String f8741qw = "avatarCreation.xml";
    private static /* synthetic */ int[] ilN = null;
    /* renamed from: kj */
    public final IAddonProperties f8743kj;
    private final Map<C0849MO.C0850a, String> iiM = new THashMap();
    /* access modifiers changed from: private */
    public RLight bOF;
    /* access modifiers changed from: private */
    public AvatarManager cKJ;
    /* access modifiers changed from: private */
    public RenderTask gtc;
    /* access modifiers changed from: private */
    public C3068j ilD = C3068j.NO_TASK;
    /* access modifiers changed from: private */
    public RModel ilE;
    /* access modifiers changed from: private */
    public C2211co ilF;
    /* access modifiers changed from: private */
    public OrbitalCamera ilK;
    /* access modifiers changed from: private */
    public RGroup ilL;
    /* access modifiers changed from: private */
    public SceneObject ilM;
    /* access modifiers changed from: private */
    public JToggleButton iln;
    /* access modifiers changed from: private */
    public JToggleButton ilo;
    /* access modifiers changed from: private */
    public JToggleButton ilp;
    /* access modifiers changed from: private */
    public JToggleButton ilq;
    /* access modifiers changed from: private */
    public JToggleButton ilr;
    /* access modifiers changed from: private */
    public AvatarAppearanceType ilv;
    /* access modifiers changed from: private */
    public AvatarAppearanceType ilx;
    /* access modifiers changed from: private */
    public C3135oL ilz;
    /* renamed from: lV */
    public IEngineGraphics f8744lV;
    /* renamed from: nM */
    public InternalFrame f8745nM;
    /* access modifiers changed from: private */
    public SceneView sceneView;
    /* renamed from: P */
    private Player f8742P;
    private float aSB;
    private GBillboard aja;
    private Timer gnS;
    private TextField ilA;
    private ActionListener ilB;
    private FileSceneLoader ilC;
    private C3084x ilG;
    private Timer ilH;
    private GBillboard ilI;
    private Timer ilJ;
    private boolean ilm = false;
    private ButtonGroup ils;
    /* access modifiers changed from: private */
    private ButtonGroup ilt;
    /* access modifiers changed from: private */
    private List<AvatarAppearanceType> ilu;
    /* access modifiers changed from: private */
    private AvatarAppearanceType ilw;
    private AvatarAppearanceType ily;

    public C3058nY(IAddonProperties vWVar) {
        this.f8743kj = vWVar;
        C6961axa bHv = this.f8743kj.bHv();
        this.iiM.put(C0849MO.C0850a.NAME_IS_NULL, this.f8743kj.translate("null-name"));
        this.iiM.put(C0849MO.C0850a.TOO_LONG, this.f8743kj.translate("Name is too long"));
        this.iiM.put(C0849MO.C0850a.TOO_SHORT, this.f8743kj.translate("Name is too short"));
        this.iiM.put(C0849MO.C0850a.INVALID_CHARACTER, this.f8743kj.translate("Name has an invalid character"));
        this.iiM.put(C0849MO.C0850a.TOO_MANY_SPACES, this.f8743kj.translate("Name has too many whitespaces"));
        this.iiM.put(C0849MO.C0850a.FORBIDDEN, this.f8743kj.translate("Name is forbidden"));
        this.iiM.put(C0849MO.C0850a.RESERVED, this.f8743kj.translate("Name is reserver to another player"));
        this.iiM.put(C0849MO.C0850a.BELONGS_TO_USER, this.f8743kj.translate("Name already in use by another player"));
        this.iiM.put(C0849MO.C0850a.BELONGS_TO_NPC, this.f8743kj.translate("Name already in use"));
        this.iiM.put(C0849MO.C0850a.BELONGS_TO_CORP, this.f8743kj.translate("Name already in use"));
        this.iiM.put(C0849MO.C0850a.BELONGS_TO_THING, this.f8743kj.translate("Name already in use"));
        this.iiM.put(C0849MO.C0850a.ALREADY_NAMED, this.f8743kj.translate("You have a name already"));
        this.f8744lV = this.f8743kj.ale();
        m36229a(bHv);
        this.f8745nM.setLocation(0, 0);
        this.f8745nM.setSize(bHv.getScreenSize());
        this.ilC = this.f8744lV.aej();
        if (m36251iL()) {
            dhQ();
        }
        this.cKJ = this.f8743kj.ala().aLa();
        if (this.cKJ != null) {
            this.ilz = new C3135oL(this.f8743kj, this.cKJ, this.f8745nM);
            this.ilu = this.cKJ.aFG();
            m36255jN(this.iln.isSelected());
            if (this.ilq.isSelected()) {
                m36263q(this.ilx);
            }
        } else {
            this.ilu = new ArrayList();
        }
        this.gnS = new Timer(20, new C3065g());
        this.ilF = new C2211co(this.f8743kj, this.f8745nM.mo4916ce("profession"));
        this.ilF.mo17687c(new C3067i());
        m36231a(C3084x.Creating);
        Player dL = this.f8743kj.getPlayer();
        if (dL != null) {
            boolean isUnnamed = dL.isUnnamed();
            this.ilA.setEnabled(isUnnamed);
            this.ilA.setFocusable(isUnnamed);
        }
        open();
    }

    static /* synthetic */ int[] dhS() {
        int[] iArr = ilN;
        if (iArr == null) {
            iArr = new int[C3084x.values().length];
            try {
                iArr[C3084x.Choosing.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C3084x.Creating.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            ilN = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        destroy();
        super.finalize();
    }

    /* access modifiers changed from: private */
    /* renamed from: U */
    public void m36227U(double d) {
        if (this.ilz != null && this.ilK != null) {
            double d2 = this.ilK.getDistance().z;
            double zoomFactor = d2 - ((this.ilK.getZoomFactor() + (0.3d * d2)) * d);
            if ((d <= ScriptRuntime.NaN || zoomFactor >= gnJ) && (d >= ScriptRuntime.NaN || zoomFactor <= gnI)) {
                if (this.ilm) {
                    this.f8743kj.aVU().mo13975h(ilk);
                }
                this.ilK.setDistance(new Vec3d(this.ilK.getDistance().x, (1.0d - ((zoomFactor - gnJ) / 2.6d)) * ((double) this.ilz.getHeight()) * 0.4699999988079071d * gnM, zoomFactor));
                return;
            }
            this.f8743kj.aVU().mo13975h(ill);
        }
    }

    /* renamed from: Fa */
    private void m36225Fa() {
        C3066h hVar = new C3066h();
        C3062d dVar = new C3062d();
        C3061c cVar = new C3061c();
        C3064f fVar = new C3064f();
        C3063e eVar = new C3063e();
        C3060b bVar = new C3060b();
        C3059a aVar = new C3059a();
        C3079t tVar = new C3079t();
        C3077r rVar = new C3077r();
        C3076q qVar = new C3076q();
        this.f8745nM.mo4913cb("confirm").addActionListener(dVar);
        this.f8745nM.mo4913cb("cancel").addActionListener(cVar);
        this.f8745nM.mo4913cb("random").addActionListener(fVar);
        this.f8745nM.mo4913cb("zoomIn").addMouseListener(aVar);
        this.f8745nM.mo4913cb("zoomOut").addMouseListener(tVar);
        this.f8745nM.mo4913cb("rotateCCW").addMouseListener(rVar);
        this.f8745nM.mo4913cb("rotateCW").addMouseListener(qVar);
        this.ilA.addKeyListener(hVar);
        this.iln.addActionListener(eVar);
        this.ilo.addActionListener(eVar);
        this.ilp.addActionListener(bVar);
        this.ilq.addActionListener(bVar);
        this.ilr.addActionListener(bVar);
        this.f8745nM.mo4915cd("heightSlider").addAdjustmentListener(new C3069k());
    }

    /* renamed from: iM */
    private void m36252iM() {
        this.ilA = this.f8745nM.mo4920ci("characterName");
        this.iln = this.f8745nM.mo4915cd("selectMale");
        this.ilo = this.f8745nM.mo4915cd("selectFemale");
        this.ilp = this.f8745nM.mo4915cd("selectBodySlim");
        this.ilq = this.f8745nM.mo4915cd("selectBodyNormal");
        this.ilr = this.f8745nM.mo4915cd("selectBodyLarge");
    }

    /* access modifiers changed from: private */
    public void cancel() {
        if (this.ilG.equals(C3084x.Choosing)) {
            m36231a(C3084x.Creating);
        } else {
            this.ilB.actionPerformed(new ActionEvent(this, 1, "cancel"));
        }
    }

    /* access modifiers changed from: private */
    public void dhG() {
        this.ilv = null;
        if (!(this.sceneView == null || this.ilL == null)) {
            this.sceneView.getScene().removeChild(this.ilL);
            this.ilL = null;
            this.ilE = null;
        }
        if (this.ilz != null) {
            this.ilz.clear();
            this.ilz = null;
        }
    }

    private void open() {
        dhJ();
        this.f8743kj.alf().mo7960a("Avatar starting fade task", new C3078s(), 750);
    }

    /* access modifiers changed from: private */
    public void dhH() {
        this.aSB = 1.0f;
        this.ilJ = new Timer(10, new C3072m());
        this.ilJ.setInitialDelay(0);
        this.ilJ.start();
    }

    /* access modifiers changed from: protected */
    public void dhI() {
        Color primitiveColor = this.ilI.getPrimitiveColor();
        primitiveColor.w = this.aSB;
        this.ilI.setPrimitiveColor(primitiveColor);
        this.aSB -= 0.005f;
        if (this.aSB < 0.0f) {
            this.ilJ.stop();
            this.f8743kj.getEngineGame().getEngineGraphics().aeh().removeChild(this.ilI);
            this.ilI.setRender(false);
            this.ilI.dispose();
            this.ilI = null;
            this.f8745nM.setVisible(true);
        }
    }

    private void dhJ() {
        IEngineGraphics ale = this.f8743kj.getEngineGame().getEngineGraphics();
        if (ale != null) {
            int aet = ale.aet();
            int aes = ale.aes();
            this.ilI = new GBillboard();
            this.ilI.setPrimitiveColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));
            this.ilI.setSize(new Vector2fWrap((float) (aes * 2), (float) (aet * 2)));
            this.ilI.setPosition(new Vector2fWrap((float) (aes / 2), (float) (aet / 2)));
            this.ilI.setName("fadeInBillBoard");
            this.ilI.setRender(true);
            IEngineGraphics ale2 = this.f8743kj.getEngineGame().getEngineGraphics();
            if (ale2 != null) {
                Material material = new Material();
                material.setShader(ale2.aej().getDefaultVertexColorShader());
                this.ilI.setMaterial(material);
                ale2.aeh().addChild(this.ilI);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: NI */
    public void m36226NI() {
        if (this.ilG.equals(C3084x.Creating)) {
            m36231a(C3084x.Choosing);
            return;
        }
        C3579sb Rn = this.ilF.mo17686Rn();
        if (Rn == null) {
            this.f8743kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.WARNING, this.f8743kj.translate("Select a career"), this.f8743kj.translate("Ok")));
            return;
        }
        String trim = dhO().trim();
        this.f8742P = null;
        try {
            this.f8742P = this.f8743kj.getPlayer();
            if (this.f8742P == null) {
                this.f8742P = this.f8743kj.getEngineGame().getUser().mo8478lJ(trim);
            } else if (this.f8742P.isUnnamed()) {
                this.f8742P.setName(trim);
            }
            this.ilz.mo20962aY(this.f8742P);
            this.f8742P.mo14355b(Rn, 0);
            if (m36251iL()) {
                Station xh = Rn.mo21962xh();
                if (0 >= Rn.aaA().size()) {
                    throw new IllegalArgumentException("Wrong ship index. Should be less than " + Rn.aaA().size());
                }
                this.f8742P.mo14352b(xh, (ShipType) Rn.aaA().get(0), (List<WeaponType>) Rn.aaE(), (MissionTemplate) null);
            }
            dhK();
        } catch (C0849MO e) {
            this.f8743kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.iiM.get(e.bhF()), new Object[0]));
        } catch (IllegalStateException e2) {
            this.f8743kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f8743kj.translate("You have reached the maximum number of characters"), new Object[0]));
        }
    }

    private void dhK() {
        IEngineGraphics ale = this.f8743kj.getEngineGame().getEngineGraphics();
        if (ale != null) {
            int aet = ale.aet();
            int aes = ale.aes();
            this.aja = new GBillboard();
            this.aja.setPrimitiveColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
            this.aja.setSize(new Vector2fWrap((float) (aes * 2), (float) (aet * 2)));
            this.aja.setPosition(new Vector2fWrap((float) (aes / 2), (float) (aet / 2)));
            this.aja.setName("fadeOutBillBoard");
            this.aja.setRender(true);
            IEngineGraphics ale2 = this.f8743kj.getEngineGame().getEngineGraphics();
            if (ale2 != null) {
                Material material = new Material();
                material.setShader(ale2.aej().getDefaultVertexColorShader());
                this.aja.setMaterial(material);
                ale2.aeh().addChild(this.aja);
                IBaseUiTegXml.initBaseUItegXML().mo13709a((RenderTask) new C3073n());
                this.f8745nM.destroy();
                this.aSB = 0.0f;
                this.ilH = new Timer(10, new C3074o());
                this.ilH.setInitialDelay(0);
                this.ilH.start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void dhL() {
        Color primitiveColor = this.aja.getPrimitiveColor();
        primitiveColor.w = this.aSB;
        this.aja.setPrimitiveColor(primitiveColor);
        this.aSB = (float) (((double) this.aSB) + gnL);
        if (this.aSB > 1.0f) {
            this.ilH.stop();
            this.f8743kj.getEngineGame().getEngineGraphics().aeh().removeChild(this.aja);
            this.aja.setRender(false);
            this.aja.dispose();
            this.aja = null;
            dhM();
        }
    }

    private void dhM() {
        this.ilB.actionPerformed(new ActionEvent(this, 0, "confirmed"));
    }

    /* renamed from: iL */
    private boolean m36251iL() {
        return this.f8743kj != null && this.f8743kj.ala().aLS().mo22273iL();
    }

    /* renamed from: a */
    private void m36231a(C3084x xVar) {
        Container ce = this.f8745nM.mo4916ce("leftColumn");
        Container ce2 = this.f8745nM.mo4916ce("rightColumn");
        switch (dhS()[xVar.ordinal()]) {
            case 1:
                ce.getLayout().show(ce, "CREATE");
                ce2.getLayout().show(ce2, "CREATE");
                this.ilF.clearSelection();
                if (this.ilz != null) {
                    this.ilz.mo2035r(this.cKJ.aGa());
                }
                this.f8745nM.mo4913cb("confirm").setText(this.f8743kj.translate("Next"));
                this.f8745nM.mo4913cb("random").setVisible(true);
                this.f8745nM.mo4915cd("heightSlider").setVisible(true);
                this.f8745nM.mo4913cb("cancel").setText(this.f8743kj.translate("Exit"));
                if (!m36251iL()) {
                    this.f8745nM.mo4913cb("cancel").setVisible(false);
                    break;
                }
                break;
            case 2:
                ce.getLayout().show(ce, "CHOOSE");
                ce2.getLayout().show(ce2, "CHOOSE");
                this.f8745nM.mo4913cb("confirm").setText(this.f8743kj.translate("Accept"));
                this.f8745nM.mo4913cb("random").setVisible(false);
                this.f8745nM.mo4915cd("heightSlider").setVisible(false);
                this.f8745nM.mo4913cb("cancel").setText(this.f8743kj.translate("Back"));
                if (!m36251iL()) {
                    this.f8745nM.mo4913cb("cancel").setVisible(true);
                    break;
                }
                break;
        }
        this.ilG = xVar;
    }

    public void dhN() {
        Scene scene;
        logger.trace("releaseResources():Begin");
        if (this.sceneView != null) {
            if (!(this.ilL == null || (scene = this.sceneView.getScene()) == null)) {
                scene.removeChild(this.ilL);
                this.ilE.clearSubMeshes();
            }
            if (m36251iL()) {
                this.sceneView.setEnabled(false);
                this.f8743kj.ale().aea().setEnabled(true);
            }
            this.f8743kj.ale().mo3046b(this.sceneView);
            this.sceneView = null;
        }
        logger.trace("releaseResources():End");
    }

    /* renamed from: a */
    private void m36229a(C6961axa axa) {
        this.f8745nM = (InternalFrame) axa.mo16789b(C3058nY.class, f8741qw);
        if (!m36251iL()) {
            this.sceneView = this.f8744lV.mo3052bY("avatarSceneView");
        }
        m36252iM();
        m36225Fa();
        this.f8745nM.mo4915cd("shininessSlider").setEnabled(false);
        this.f8745nM.mo4915cd("faceLengthSlider").setEnabled(false);
        this.f8745nM.mo4915cd("faceWidthSlider").setEnabled(false);
        this.f8745nM.mo4915cd("eyePositionSlider").setEnabled(false);
        this.f8745nM.mo4915cd("chinDepthSlider").setEnabled(false);
        this.f8745nM.mo4915cd("noseSizeSlider").setEnabled(false);
        this.f8745nM.mo4915cd("mouthPositionSlider").setEnabled(false);
        this.f8745nM.mo4915cd("featuresWeightSlider").setEnabled(false);
        this.ils = new ButtonGroup();
        this.ils.add(this.iln);
        this.ils.add(this.ilo);
        this.iln.setSelected(true);
        this.ilt = new ButtonGroup();
        this.ilt.add(this.ilp);
        this.ilt.add(this.ilq);
        this.ilt.add(this.ilr);
        this.ilp.setEnabled(false);
        this.ilr.setEnabled(false);
        this.ilA.requestFocus();
        this.f8745nM.addComponentListener(new C3075p());
        Component cd = this.f8745nM.mo4915cd("centerColumn");
        cd.addMouseMotionListener((MouseMotionListener) null);
        C3071l lVar = new C3071l();
        cd.addMouseListener(lVar);
        cd.addMouseMotionListener(lVar);
        cd.addMouseWheelListener(lVar);
        ComponentManager.getCssHolder(cd).mo13053a((ComponentCheck) new C3083w());
        this.f8743kj.aVU().mo13975h(new C2240dG(C3667tV.CAREER_FIGHTER));
        this.f8743kj.aVU().mo13975h(new C2240dG(C3667tV.CAREER_BOMBER));
        this.f8743kj.aVU().mo13975h(new C2240dG(C3667tV.CAREER_EXPLORER));
        this.f8743kj.aVU().mo13975h(new C2240dG(C3667tV.CAREER_FREIGHTER));
    }

    public String dhO() {
        return this.ilA.getText();
    }

    public C2698il dhP() {
        return this.f8745nM;
    }

    /* renamed from: jM */
    private List<AvatarAppearanceType> m36254jM(boolean z) {
        ArrayList arrayList = new ArrayList();
        for (AvatarAppearanceType next : this.ilu) {
            if (z) {
                if (!next.mo5531bC()) {
                    arrayList.add(next);
                }
            } else if (next.mo5531bC()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    /* renamed from: jN */
    public void m36255jN(boolean z) {
        List<AvatarAppearanceType> jM = m36254jM(z);
        if (jM != null) {
            this.ily = null;
            this.ilx = jM.get(0);
            this.ilw = null;
        }
    }

    /* access modifiers changed from: private */
    public void randomize() {
        if (this.ilz != null && this.ilv != null) {
            this.ilz.randomize();
        }
    }

    /* renamed from: g */
    public void mo20777g(ActionListener actionListener) {
        this.ilB = actionListener;
    }

    /* access modifiers changed from: private */
    /* renamed from: q */
    public synchronized void m36263q(AvatarAppearanceType t) {
        if (t != null) {
            if (!t.equals(this.ilv) && this.gtc == null) {
                if (this.ilv == null) {
                    this.ilv = t;
                    this.ilz.mo20965e(t);
                    this.ilz.randomize();
                } else {
                    this.ilv = t;
                    C3135oL.C3138b bei = this.ilz.bei();
                    this.ilz.mo20965e(t);
                    this.ilz.mo20960a(bei);
                }
                this.gtc = new C3081v(t);
                this.f8744lV.mo3007a(this.gtc);
            }
        }
    }

    /* renamed from: ne */
    public void mo20778ne(String str) {
        this.ilA.setText(str);
        this.ilA.requestFocus();
    }

    private void dhQ() {
        this.f8743kj.ale().aea().setEnabled(false);
        this.sceneView = new SceneView();
        this.sceneView.setViewport(0, 0, this.f8743kj.bHv().getScreenWidth(), this.f8743kj.bHv().getScreenHeight());
        this.sceneView.setClearColorBuffer(true);
        this.sceneView.setClearDepthBuffer(true);
        this.sceneView.setClearStencilBuffer(true);
        this.sceneView.getCamera().setFovY(40.0f);
        this.f8743kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/nod_avt_scene.pro", "nod_avt_scene", (Scene) null, new C3080u(), "AvatarCreationWindow");
    }

    /* access modifiers changed from: private */
    public void dhR() {
        if (this.ilM != null && this.ilK != null) {
            this.ilM.setPosition((double) ScriptRuntime.NaN, ((double) (this.ilz.getHeight() * 0.5f)) * gnM, (double) ScriptRuntime.NaN);
            m36227U(ScriptRuntime.NaN);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: q */
    public void m36264q(SceneObject sceneObject) {
        this.ilK = new OrbitalCamera();
        this.ilK.setTarget(sceneObject);
        this.ilK.setAspect(((float) this.f8745nM.getWidth()) / ((float) this.f8745nM.getHeight()));
        this.ilK.setDistance(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, gnI));
        this.ilK.setNearPlane(0.01f);
        this.ilK.setFovY(40.0f);
        this.sceneView.setCamera(this.ilK);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m36230a(C3068j jVar) {
        this.ilD = jVar;
        this.gnS.setInitialDelay(1);
        this.gnS.start();
        this.ilm = true;
    }

    /* access modifiers changed from: private */
    public void cqs() {
        this.ilD = C3068j.NO_TASK;
        this.gnS.stop();
        this.f8743kj.aVU().mo13975h(ill);
        this.ilm = false;
    }

    /* access modifiers changed from: private */
    /* renamed from: yA */
    public void m36273yA(int i) {
        if (this.ilE != null) {
            if (this.ilm) {
                this.f8743kj.aVU().mo13975h(ilk);
            }
            TransformWrap transform = this.ilE.getTransform();
            transform.mo17392z((float) i);
            this.ilE.setTransform(transform);
        }
    }

    /* renamed from: dL */
    public Player mo20769dL() {
        return this.f8742P;
    }

    public void destroy() {
        dhG();
        this.ilz = null;
        if (this.f8745nM != null) {
            this.f8745nM.destroy();
            this.f8745nM = null;
        }
    }

    /* renamed from: a.nY$j */
    /* compiled from: a */
    private enum C3068j {
        NO_TASK,
        ROTATE_CCW,
        ROTATE_CW,
        ZOOM_IN,
        ZOOM_OUT
    }

    /* renamed from: a.nY$x */
    /* compiled from: a */
    private enum C3084x {
        Creating,
        Choosing
    }

    /* renamed from: a.nY$g */
    /* compiled from: a */
    class C3065g implements ActionListener {
        private static /* synthetic */ int[] aMD;

        C3065g() {
        }

        /* renamed from: Si */
        static /* synthetic */ int[] m36278Si() {
            int[] iArr = aMD;
            if (iArr == null) {
                iArr = new int[C3068j.values().length];
                try {
                    iArr[C3068j.NO_TASK.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C3068j.ROTATE_CCW.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C3068j.ROTATE_CW.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C3068j.ZOOM_IN.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C3068j.ZOOM_OUT.ordinal()] = 5;
                } catch (NoSuchFieldError e5) {
                }
                aMD = iArr;
            }
            return iArr;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            switch (m36278Si()[C3058nY.this.ilD.ordinal()]) {
                case 2:
                    C3058nY.this.m36273yA(1);
                    return;
                case 3:
                    C3058nY.this.m36273yA(-1);
                    return;
                case 4:
                    C3058nY.this.m36227U(C3058nY.gnL);
                    return;
                case 5:
                    C3058nY.this.m36227U(-0.01d);
                    return;
                default:
                    C3058nY.this.cqs();
                    return;
            }
        }
    }

    /* renamed from: a.nY$i */
    /* compiled from: a */
    class C3067i implements ActionListener {
        String[] aMM = {"none", "low", "medium", "high"};

        C3067i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3579sb Rn = C3058nY.this.ilF.mo17686Rn();
            if (Rn == null) {
                C3058nY.this.ilz.mo2035r(C3058nY.this.cKJ.aGa());
                C3058nY.this.f8745nM.mo4915cd("career-description").setText("");
                C3058nY.this.f8745nM.mo4915cd("carrer-logo").setVisible(false);
                IComponentManager e = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("speed-rate"));
                e.setAttribute("class", this.aMM[0]);
                e.mo13045Vk();
                e.getCurrentComponent().invalidate();
                IComponentManager e2 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("armor-rate"));
                e2.setAttribute("class", this.aMM[0]);
                e2.mo13045Vk();
                e2.getCurrentComponent().invalidate();
                IComponentManager e3 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("cargo-rate"));
                e3.setAttribute("class", this.aMM[0]);
                e3.mo13045Vk();
                e3.getCurrentComponent().invalidate();
                IComponentManager e4 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("attack-rate"));
                e4.setAttribute("class", this.aMM[0]);
                e4.mo13045Vk();
                e4.getCurrentComponent().invalidate();
                return;
            }
            C3058nY.this.ilz.mo2035r(Rn.aay());
            C3058nY.this.f8745nM.mo4915cd("career-description").setText(Rn.mo21961vW().get());
            IComponentManager e5 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("carrer-logo"));
            e5.setAttribute("class", Rn.aaK());
            e5.mo13063q(2, 2);
            e5.getCurrentComponent().setVisible(true);
            e5.getCurrentComponent().validate();
            e5.mo13045Vk();
            IComponentManager e6 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("speed-rate"));
            e6.setAttribute("class", this.aMM[Rn.aaM()]);
            e6.mo13045Vk();
            e6.getCurrentComponent().invalidate();
            e6.getCurrentComponent().validate();
            IComponentManager e7 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("armor-rate"));
            e7.setAttribute("class", this.aMM[Rn.aaO()]);
            e7.mo13045Vk();
            e7.getCurrentComponent().invalidate();
            IComponentManager e8 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("cargo-rate"));
            e8.setAttribute("class", this.aMM[Rn.aaQ()]);
            e8.mo13045Vk();
            e8.getCurrentComponent().invalidate();
            IComponentManager e9 = ComponentManager.getCssHolder(C3058nY.this.f8745nM.mo4915cd("attack-rate"));
            e9.setAttribute("class", this.aMM[Rn.aaS()]);
            e9.mo13045Vk();
            e9.getCurrentComponent().invalidate();
        }
    }

    /* renamed from: a.nY$h */
    /* compiled from: a */
    class C3066h extends KeyAdapter {
        C3066h() {
        }

        public void keyTyped(KeyEvent keyEvent) {
            switch (keyEvent.getKeyCode()) {
                case 10:
                    C3058nY.this.m36226NI();
                    return;
                case 27:
                    return;
                default:
                    if (!C3004mr.m35866cW(keyEvent.getKeyChar())) {
                        keyEvent.consume();
                        return;
                    }
                    return;
            }
        }
    }

    /* renamed from: a.nY$d */
    /* compiled from: a */
    class C3062d implements ActionListener {
        C3062d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3058nY.this.m36226NI();
        }
    }

    /* renamed from: a.nY$c */
    /* compiled from: a */
    class C3061c implements ActionListener {
        C3061c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3058nY.this.cancel();
        }
    }

    /* renamed from: a.nY$f */
    /* compiled from: a */
    class C3064f implements ActionListener {
        C3064f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3058nY.this.randomize();
        }
    }

    /* renamed from: a.nY$e */
    /* compiled from: a */
    class C3063e implements ActionListener {
        C3063e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            boolean z = false;
            if (C3058nY.this.ilv != null) {
                boolean z2 = !C3058nY.this.ilv.mo5531bC();
                if (actionEvent.getSource().equals(C3058nY.this.ilo)) {
                    if (!z2) {
                        return;
                    }
                } else if (!actionEvent.getSource().equals(C3058nY.this.iln)) {
                    z = z2;
                } else if (!z2) {
                    z = true;
                } else {
                    return;
                }
            } else {
                z = true;
            }
            C3058nY.this.m36255jN(z);
            C3058nY.this.ilq.setSelected(true);
            C3058nY.this.m36263q(C3058nY.this.ilx);
        }
    }

    /* renamed from: a.nY$b */
    /* compiled from: a */
    class C3060b implements ActionListener {
        C3060b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!actionEvent.getSource().equals(C3058nY.this.ilp) && !actionEvent.getSource().equals(C3058nY.this.ilr)) {
                C3058nY.this.m36263q(C3058nY.this.ilx);
            }
        }
    }

    /* renamed from: a.nY$a */
    class C3059a extends MouseAdapter {
        C3059a() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            C3058nY.this.m36230a(C3068j.ZOOM_IN);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            C3058nY.this.cqs();
        }
    }

    /* renamed from: a.nY$t */
    /* compiled from: a */
    class C3079t extends MouseAdapter {
        C3079t() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            C3058nY.this.m36230a(C3068j.ZOOM_OUT);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            C3058nY.this.cqs();
        }
    }

    /* renamed from: a.nY$r */
    /* compiled from: a */
    class C3077r extends MouseAdapter {
        C3077r() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            C3058nY.this.m36230a(C3068j.ROTATE_CCW);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            C3058nY.this.cqs();
        }
    }

    /* renamed from: a.nY$q */
    /* compiled from: a */
    class C3076q extends MouseAdapter {
        C3076q() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            C3058nY.this.m36230a(C3068j.ROTATE_CW);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            C3058nY.this.cqs();
        }
    }

    /* renamed from: a.nY$k */
    /* compiled from: a */
    class C3069k implements AdjustmentListener {
        C3069k() {
        }

        public void adjustmentValueChanged(AdjustmentEvent adjustmentEvent) {
            C3058nY.this.f8744lV.mo3007a((RenderTask) new C3070a());
        }

        /* renamed from: a.nY$k$a */
        class C3070a implements RenderTask {
            C3070a() {
            }

            public void run(RenderView renderView) {
                if (C3058nY.this.ilz != null) {
                    C3058nY.this.ilz.mo20967yf();
                    C3058nY.this.dhR();
                }
            }
        }
    }

    /* renamed from: a.nY$s */
    /* compiled from: a */
    class C3078s extends WrapRunnable {
        C3078s() {
        }

        public void run() {
            C3058nY.this.dhH();
        }
    }

    /* renamed from: a.nY$m */
    /* compiled from: a */
    class C3072m implements ActionListener {
        C3072m() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3058nY.this.dhI();
        }
    }

    /* renamed from: a.nY$n */
    /* compiled from: a */
    class C3073n implements RenderTask {
        C3073n() {
        }

        public void run(RenderView renderView) {
            C3058nY.this.dhN();
        }
    }

    /* renamed from: a.nY$o */
    /* compiled from: a */
    class C3074o implements ActionListener {
        C3074o() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3058nY.this.dhL();
        }
    }

    /* renamed from: a.nY$p */
    /* compiled from: a */
    class C3075p extends ComponentAdapter {
        C3075p() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C3058nY.super.componentHidden(componentEvent);
            C3058nY.this.dhG();
        }
    }

    /* renamed from: a.nY$l */
    /* compiled from: a */
    class C3071l extends MouseAdapter {
        Point gqX;

        C3071l() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 1) {
                this.gqX = mouseEvent.getPoint();
            }
        }

        public void mouseDragged(MouseEvent mouseEvent) {
            Point point = mouseEvent.getPoint();
            if (this.gqX != null) {
                int i = point.x - this.gqX.x;
                int i2 = point.y;
                int i3 = this.gqX.y;
                if (i < 0) {
                    C3058nY.this.m36273yA(i * 1);
                } else if (i > 0) {
                    C3058nY.this.m36273yA(i * 1);
                }
            }
            this.gqX = point;
        }

        public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
            if (mouseWheelEvent.getWheelRotation() < 0) {
                C3058nY.this.m36227U(0.05d);
            } else {
                C3058nY.this.m36227U(-0.05d);
            }
        }
    }

    /* renamed from: a.nY$w */
    /* compiled from: a */
    class C3083w implements ComponentCheck {
        C3083w() {
        }

        /* renamed from: i */
        public boolean isContains(Component component) {
            return true;
        }
    }

    /* renamed from: a.nY$v */
    /* compiled from: a */
    class C3081v implements RenderTask {
        private final /* synthetic */ AvatarAppearanceType grH;

        C3081v(AvatarAppearanceType t) {
            this.grH = t;
        }

        public void run(RenderView renderView) {
            C3058nY.logger.trace("Scene Unload");
            if (C3058nY.this.ilL != null) {
                C3058nY.this.sceneView.getScene().removeChild(C3058nY.this.ilL);
            }
            C3058nY.logger.trace("Scene Load");
            C3058nY.this.ilz.mo2027b((C0907NJ) new C3082a(this.grH));
        }

        /* renamed from: a.nY$v$a */
        class C3082a implements C0907NJ {
            private final /* synthetic */ AvatarAppearanceType grH;

            C3082a(AvatarAppearanceType t) {
                this.grH = t;
            }

            /* renamed from: a */
            public void mo968a(RenderAsset renderAsset) {
                boolean z = true;
                C3058nY.logger.trace("Scene Loaded");
                if (!(renderAsset instanceof RModel)) {
                    C3058nY.this.ilo.setEnabled(true);
                    C3058nY.this.iln.setEnabled(true);
                    C3058nY.logger.trace("Load Failed");
                    return;
                }
                C3058nY.logger.trace("Rotating model");
                C3058nY.this.ilE = (RModel) renderAsset;
                C3058nY.this.ilE.getTransform().mo17392z(180.0f);
                C3058nY.logger.trace("Attatching model to new RGroup");
                C3058nY.this.ilL = new RGroup();
                C3058nY.this.ilL.addChild(C3058nY.this.ilE);
                C3058nY.this.ilz.mo20961aC(C3058nY.this.ilz.mo7725cJ());
                C3058nY.logger.trace("Adding group to scene");
                C3058nY.this.sceneView.getScene().addChild(C3058nY.this.ilL);
                C3058nY.this.ilM = new SceneObject();
                if (C3058nY.this.ilK == null) {
                    C3058nY.this.m36264q(C3058nY.this.ilM);
                } else {
                    C3058nY.this.ilK.setTarget(C3058nY.this.ilM);
                }
                C3058nY.this.dhR();
                C3058nY.logger.trace("Scene Done");
                C3058nY.this.gtc = null;
                C3058nY.this.ilo.setSelected(this.grH.mo5531bC());
                JToggleButton l = C3058nY.this.iln;
                if (this.grH.mo5531bC()) {
                    z = false;
                }
                l.setSelected(z);
            }
        }
    }

    /* renamed from: a.nY$u */
    /* compiled from: a */
    class C3080u implements C0907NJ {
        C3080u() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            C3058nY.this.sceneView.getScene().addChild((SceneObject) renderAsset);
            C3058nY.this.f8743kj.ale().mo3005a(C3058nY.this.sceneView);
            C3058nY.this.sceneView.setEnabled(true);
            C3058nY.this.bOF = (RLight) ((SceneObject) renderAsset).getChild(0).getChild(1);
        }
    }
}
