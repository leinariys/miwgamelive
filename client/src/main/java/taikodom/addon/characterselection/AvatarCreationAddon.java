package taikodom.addon.characterselection;

import game.network.message.externalizable.C3131oI;
import logic.IAddonSettings;
import logic.aaa.C2327eB;
import p001a.C0696Jv;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.characterselection")
/* compiled from: a */
public class AvatarCreationAddon implements C2495fo {
    /* access modifiers changed from: private */
    public C3058nY dtP;
    /* access modifiers changed from: private */

    /* renamed from: kj */
    public IAddonProperties f9754kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9754kj = addonPropertie;
        this.f9754kj.aVU().mo13965a(C3131oI.class, new C4159b());
    }

    public void stop() {
        if (this.dtP != null) {
            this.dtP.destroy();
            this.dtP = null;
        }
        this.f9754kj.aVU().mo13975h(new C2327eB(true));
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        if (this.dtP != null) {
            this.dtP.destroy();
            this.dtP = null;
        }
        this.f9754kj.aVU().mo13975h(new C2327eB(true));
    }

    /* access modifiers changed from: private */
    public void bfq() {
        if (this.dtP == null) {
            this.f9754kj.aVU().mo13975h(new C2327eB(false));
            this.dtP = new C3058nY(this.f9754kj);
            this.dtP.mo20777g((ActionListener) new C4158a());
        }
    }

    /* renamed from: taikodom.addon.characterselection.AvatarCreationAddon$b */
    /* compiled from: a */
    class C4159b extends C6124ags<C3131oI> {
        C4159b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3131oI oIVar) {
            if (C3131oI.C3132a.OPEN_AVATAR_CREATION.equals(oIVar.mo20956Us())) {
                Timer timer = new Timer(1500, new C4160a());
                timer.setRepeats(false);
                timer.start();
            }
            return false;
        }

        /* renamed from: taikodom.addon.characterselection.AvatarCreationAddon$b$a */
        class C4160a implements ActionListener {
            C4160a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                AvatarCreationAddon.this.f9754kj.aVU().mo13975h(new C0696Jv(1, false));
                AvatarCreationAddon.this.f9754kj.aVU().mo13975h(new C0696Jv(2, false));
                AvatarCreationAddon.this.bfq();
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.AvatarCreationAddon$a */
    class C4158a implements ActionListener {
        C4158a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (actionEvent.getID() != 1) {
                if (AvatarCreationAddon.this.dtP != null) {
                    AvatarCreationAddon.this.dtP = null;
                }
                AvatarCreationAddon.this.f9754kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.CREATED_AVATAR));
            }
        }
    }
}
