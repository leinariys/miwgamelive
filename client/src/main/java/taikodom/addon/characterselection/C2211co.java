package taikodom.addon.characterselection;

import game.network.message.serializable.C3438ra;
import game.script.progression.ProgressionCareer;
import logic.aWa;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.item.Repeater;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.characterselection")
/* renamed from: a.co */
/* compiled from: a */
public class C2211co implements aWa {
    private static final String hCU = "career-chooser-panel.xml";
    /* access modifiers changed from: private */
    public ActionListener fHW;
    /* access modifiers changed from: private */
    public ButtonGroup hCV;
    /* access modifiers changed from: private */
    public ProgressionCareer hCW;

    /* renamed from: kj */
    private IAddonProperties f6246kj;

    /* renamed from: qx */
    private C2698il f6247qx;

    public C2211co(IAddonProperties vWVar, C2698il ilVar) {
        this.f6246kj = vWVar;
        this.f6247qx = (C2698il) this.f6246kj.bHv().mo16779a(getClass(), hCU, ilVar);
        m28634iJ();
        this.f6247qx.setVisible(true);
    }

    /* renamed from: c */
    public void mo17687c(ActionListener actionListener) {
        this.fHW = actionListener;
    }

    /* renamed from: iJ */
    private void m28634iJ() {
        Repeater cd = this.f6247qx.mo4915cd("career-list");
        this.hCV = new ButtonGroup();
        cd.mo22250a(new C2212a(this, (C2212a) null));
        C3438ra<ProgressionCareer> aD = this.f6246kj.ala().aJk().mo15612aD();
        this.f6247qx.mo4915cd("career-list-container").getLayout().setRows(aD.size());
        for (ProgressionCareer G : aD) {
            cd.mo22248G(G);
        }
    }

    private void destroy() {
        if (this.f6247qx != null) {
            this.f6247qx.destroy();
            this.f6247qx = null;
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        destroy();
        super.finalize();
    }

    /* renamed from: Rn */
    public ProgressionCareer mo17686Rn() {
        return this.hCW;
    }

    public void clearSelection() {
        this.hCW = null;
        this.hCV.clearSelection();
        if (this.fHW != null) {
            this.fHW.actionPerformed(new ActionEvent(this, 0, IComponentManager.selected));
        }
    }

    /* renamed from: a.co$a */
    private class C2212a implements Repeater.C3671a<ProgressionCareer> {


        private C2212a() {
        }

        /* synthetic */ C2212a(C2211co coVar, C2212a aVar) {
            this();
        }

        /* renamed from: a */
        public void mo843a(ProgressionCareer sbVar, Component component) {
            JToggleButton jToggleButton = (JToggleButton) component;
            ComponentManager.getCssHolder(jToggleButton).setAttribute("class", "iconbutton " + sbVar.aaK());
            jToggleButton.setText(sbVar.mo21959rP().get());
            jToggleButton.addActionListener(new C2213a(sbVar));
            C2211co.this.hCV.add(jToggleButton);
        }

        /* renamed from: a.co$a$a */
        class C2213a implements ActionListener {

            /* renamed from: rw */
            private final /* synthetic */ ProgressionCareer f6249rw;

            C2213a(ProgressionCareer sbVar) {
                this.f6249rw = sbVar;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                C2211co.this.hCW = this.f6249rw;
                if (C2211co.this.fHW != null) {
                    C2211co.this.fHW.actionPerformed(new ActionEvent(this, 0, IComponentManager.selected));
                }
            }
        }
    }
}
