package taikodom.addon.characterselection;

import game.engine.C6082agC;
import game.geometry.Vec3d;
import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C3002mq;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.aUU;
import game.network.message.serializable.C3438ra;
import game.script.PlayerController;
import game.script.corporation.Corporation;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.nursery.NurseryDefaults;
import game.script.player.Player;
import game.script.player.User;
import game.script.progression.ProgressionCareer;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.aaa.C1506WA;
import logic.aaa.C2733jJ;
import logic.aaa.aIQ;
import logic.aaa.aPY;
import logic.render.IEngineGraphics;
import logic.res.ConfigIniKeyValue;
import logic.res.ConfigManager;
import logic.res.KeyCode;
import logic.res.sound.C0907NJ;
import logic.ui.C2698il;
import logic.ui.IBaseUiTegXml;
import logic.ui.Panel;
import logic.ui.item.Component3d;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.infra.script.I18NString;
import taikodom.render.ExclusiveVideoPlayer;
import taikodom.render.FlashVideo;
import taikodom.render.RenderView;
import taikodom.render.Video;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.C3176ol;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@TaikodomAddon("taikodom.addon.characterselection")
/* compiled from: a */
public class CharacterSelectionAddon implements C2495fo {
    private static final String dtA = "deleteChar.xml";
    private static final String dtB = "data/movies/intro.swf";
    private static final String dtx = "characterselection.xml";
    private static final String dty = "background.xml";
    private static final String dtz = "characterSheet.xml";
    /* access modifiers changed from: private */
    public C2698il dtD;
    /* access modifiers changed from: private */
    public C2698il dtE;
    /* access modifiers changed from: private */
    public C2698il dtG;
    /* access modifiers changed from: private */
    public Component3d dtI;
    /* access modifiers changed from: private */
    public Map<Player, JButton> dtK;
    /* access modifiers changed from: private */
    public Player dtM;
    /* access modifiers changed from: private */
    public C3058nY dtP;
    /* access modifiers changed from: private */
    public boolean dua = false;
    /* access modifiers changed from: private */
    public C3176ol duf;
    /* renamed from: kj */
    public IAddonProperties f9755kj;
    /* renamed from: lV */
    public IEngineGraphics f9756lV;
    private C3428rT<aUU> dtC;
    private C2698il dtF;
    private Repeater.C3671a<Player> dtH;
    private Repeater<Player> dtJ;
    private User dtL;
    private C6124ags<aPY> dtN;
    private InternalFrame dtO;
    private JButton dtQ;
    private JButton dtR;
    private JLabel dtS;
    private JLabel dtT;
    private JLabel dtU;
    private JLabel dtV;
    private JLabel dtW;
    private JButton dtX;
    private Panel dtY;
    private JLabel dtZ;
    private Panel dub;
    private C0907NJ duc;
    private C3428rT<ExclusiveVideoPlayer> dud;
    /* access modifiers changed from: private */
    private MouseAdapter due;
    /* access modifiers changed from: private */
    private boolean dug = false;
    /* renamed from: wz */
    private C3428rT<C3689to> f9757wz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9755kj = addonPropertie;
        this.f9756lV = addonPropertie.ale();
        m42834iG();
        this.dtH = new C4166f();
        this.f9755kj.aVU().mo13965a(aUU.class, this.dtC);
        this.f9755kj.aVU().mo13965a(aPY.class, this.dtN);
    }

    /* renamed from: iG */
    private void m42834iG() {
        this.dtC = new C4165e();
        this.dtN = new C4164d();
        this.duc = new C4163c();
        this.due = new C4162b();
        this.f9757wz = new C4161a();
        this.dud = new C4169g();
    }

    private void bff() {
        int i;
        try {
            i = this.f9755kj.getEngineGame().getConfigManager().getSection(ConfigIniKeyValue.LAST_PLAYED_CHARACTER.getValue()).mo7199b(ConfigIniKeyValue.LAST_PLAYED_CHARACTER);
        } catch (Exception e) {
            i = -1;
        }
        this.dtJ.clear();
        this.dtK.clear();
        C3438ra<Player> cSQ = this.dtL.cSQ();
        Player aku = null;
        for (Player aku2 : cSQ) {
            this.dtJ.mo22248G(aku2);
            if (m42828eV(aku2.mo8317Ej()) == i) {
                aku = aku2;
            }
        }
        this.dtJ.doLayout();
        if (aku != null) {
            m42816aZ(aku);
            bfo();
        } else if (this.dtK.size() > 0) {
            m42816aZ((Player) cSQ.get(0));
            bfo();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m42832i(User adk) {
        this.dtL = adk;
        if (m42835iL() || adk.cSQ().size() != 0) {
            if (this.dtG == null) {
                this.dtG = (C2698il) this.f9755kj.bHv().mo16794bQ(dtA);
            }
            if (this.dtE == null) {
                this.dtE = (C2698il) this.f9755kj.bHv().mo16794bQ(dtx);
                this.dtJ = this.dtE.mo4915cd("charRepeater");
                this.dtJ.mo22250a(this.dtH);
                this.dtK = new HashMap();
            }
            if (this.dtF == null) {
                this.dtF = (C2698il) this.f9755kj.bHv().mo16794bQ(dtz);
            }
            m42836iM();
            this.dtE.setVisible(true);
            this.dtE.setSize(this.dtE.getPreferredSize().width, 488);
            this.dtE.setLocation(80, 80);
            this.dtE.pack();
            this.dtF.setVisible(false);
            this.dtF.setSize(this.dtF.getPreferredSize());
            this.dtF.setLocation(this.dtE.getSize().width + 80, 80);
            this.dtF.pack();
            this.dtQ.addActionListener(new C4170h());
            this.dtG.setVisible(true);
            this.dtG.setLocation(10, this.f9755kj.bHv().getScreenHeight() - 100);
            this.dtR.addActionListener(new C4171i());
            this.dtG.pack();
            bff();
            if (this.dtM == null) {
                this.dtF.setVisible(false);
            }
            this.dtX.setText(this.f9755kj.translate("Enter Taikodom"));
            this.dtX.addActionListener(new C4175m());
            this.dtX.addKeyListener(new C4174l());
            if (this.dtD == null) {
                this.dtD = (C2698il) this.f9755kj.bHv().mo16794bQ(dty);
            }
            this.dtD.setVisible(true);
            this.dtD.setLocation(0, 0);
            this.dtD.setSize(this.f9755kj.bHv().getScreenSize());
            this.dtD.pack();
            return;
        }
        bfg();
    }

    private void bfg() {
        try {
            ((User) C3582se.m38985a(this.dtL, (C6144ahM<?>) new C4179q())).cSK();
        } catch (IllegalStateException e) {
            this.f9755kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f9755kj.translate("You have reached the maximum number of characters"), new Object[0]));
        }
    }

    /* access modifiers changed from: private */
    public void bfh() {
        this.dua = true;
        destroy();
        C6082agC.bWV().aRS();
        this.dub = new Panel();
        this.f9755kj.bHv().addInRootPane(this.dub);
        this.dub.setLocation(0, 0);
        this.dub.setSize(this.f9755kj.bHv().getScreenWidth(), this.f9755kj.bHv().getScreenHeight());
        this.dub.setVisible(true);
        this.dub.addMouseListener(this.due);
        this.f9755kj.aVU().mo13965a(C3689to.class, this.f9757wz);
        this.f9755kj.aVU().mo13966a("endVideo", ExclusiveVideoPlayer.class, this.dud);
        String eH = m42827eH(dtB);
        this.f9755kj.alg().mo4995a(eH, String.valueOf(eH) + "/player", (Scene) null, this.duc, getClass().getSimpleName());
    }

    /* renamed from: eH */
    private String m42827eH(String str) {
        String cGs = I18NString.getCurrentLocation();
        if (cGs == null) {
            return str;
        }
        String replaceAll = str.replaceAll("([.][^.]+)$", "_" + cGs + "$1");
        if (this.f9755kj.ale().getRootPath().concat(replaceAll).exists()) {
            return replaceAll;
        }
        return str;
    }

    /* renamed from: iM */
    private void m42836iM() {
        this.dtR = this.dtG.mo4913cb("deleteCharButton");
        this.dtY = this.dtF.mo4915cd("info-box");
        this.dtS = this.dtF.mo4917cf("playerName");
        this.dtT = this.dtF.mo4917cf("locationName");
        this.dtU = this.dtF.mo4917cf("cloningCenterName");
        this.dtV = this.dtF.mo4917cf("corporationName");
        this.dtW = this.dtF.mo4917cf("creditsAmount");
        this.dtZ = this.dtF.mo4917cf("activeShipName");
        this.dtI = this.dtF.mo4915cd("ship-image");
        this.dtX = this.dtF.mo4913cb("enterTaikodom");
        this.dtQ = this.dtE.mo4913cb("createNewChar");
    }

    private void destroy() {
        if (this.dtD != null) {
            this.dtD.setVisible(false);
            this.dtD.destroy();
            this.dtD = null;
        }
        if (this.dtE != null) {
            this.dtE.setVisible(false);
            this.dtE.destroy();
            this.dtE = null;
        }
        if (this.dtO != null) {
            this.dtO.setVisible(false);
            this.dtO.destroy();
            this.dtO = null;
        }
        if (this.dtF != null) {
            this.dtF.setVisible(false);
            if (this.dtI != null) {
                this.dtI.destroy();
                this.dtI = null;
            }
            this.dtF.destroy();
            this.dtF = null;
        }
        if (this.dtG != null) {
            this.dtG.setVisible(false);
            this.dtG.destroy();
            this.dtG = null;
        }
    }

    public void stop() {
        destroy();
        this.f9755kj.aVU().mo13964a(this.dtC);
        if (this.dtN != null) {
            this.f9755kj.aVU().mo13964a(this.dtN);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: aZ */
    public void m42816aZ(Player aku) {
        this.dtM = aku;
        for (JButton selected : this.dtK.values()) {
            selected.setSelected(false);
        }
        if (aku != null) {
            this.dtK.get(aku).setSelected(true);
            this.dtK.get(aku).requestFocus();
        }
    }

    /* access modifiers changed from: private */
    public void bfi() {
        if (!m42835iL()) {
            bfg();
        } else if (this.dtL.cSQ().size() >= 6) {
            ((MessageDialogAddon) this.f9755kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.ERROR, this.f9755kj.translate("You have reached the maximum number of characters"), this.f9755kj.translate("Ok"));
        } else {
            bfp();
        }
    }

    /* access modifiers changed from: private */
    public void bfj() {
        String str;
        if (this.dtM == null) {
            ((MessageDialogAddon) this.f9755kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.ERROR, String.valueOf(this.f9755kj.translate("No player selected")) + ".", this.f9755kj.translate("OK"));
            return;
        }
        String name = this.dtM.getName();
        if (this.dtM.isUnnamed()) {
            str = this.f9755kj.translate("<Tutorial Pending>");
        } else {
            str = name;
        }
        ((MessageDialogAddon) this.f9755kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f9755kj.translate("$Delete the player %s?", str), new Object[0]), (aEP) new C4178p(), this.f9755kj.translate("Cancel"), this.f9755kj.translate("Ok"));
    }

    /* access modifiers changed from: private */
    public void bfk() {
        if (this.dtM != null) {
            Corporation bYd = this.dtM.bYd();
            if (bYd == null || !bYd.mo10726m(this.dtM)) {
                bfl();
                return;
            }
            ((MessageDialogAddon) this.f9755kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f9755kj.translate("deleteCEOMessage"), bYd.getName()), (aEP) new C4177o(), this.f9755kj.translate("Cancel"), this.f9755kj.translate("Ok"));
        }
    }

    /* access modifiers changed from: private */
    public void bfl() {
        this.dtF.setVisible(false);
        this.dtL.mo8467ch(this.dtM);
        m42816aZ((Player) null);
        bff();
    }

    private void bfm() {
        ConfigManager bhb = this.f9755kj.getEngineGame().getConfigManager();
        bhb.getSection(ConfigIniKeyValue.LAST_PLAYED_CHARACTER.getValue()).mo7202b(ConfigIniKeyValue.LAST_PLAYED_CHARACTER, Integer.valueOf(m42828eV(this.dtM.mo8317Ej())));
        try {
            bhb.loadConfigIni("config/user.ini");
        } catch (IOException e) {
            this.f9755kj.getLog().error("User config options could not be saved", e);
        }
    }

    /* renamed from: eV */
    private int m42828eV(long j) {
        return String.valueOf(j).hashCode();
    }

    /* access modifiers changed from: private */
    /* renamed from: cX */
    public void m42822cX(boolean z) {
        if (this.dtM == null) {
            this.f9755kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.INFO, String.valueOf(this.f9755kj.translate("No player selected")) + ".", new Object[0]));
        } else if (z) {
            this.f9755kj.aVU().mo13975h(new C3002mq(0, new C4176n(z)));
        } else {
            m42823cY(z);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: cY */
    public void m42823cY(boolean z) {
        this.dtL.ala().getUserConnection().mo15383bL(this.dtM);
        bfm();
        if (m42835iL()) {
            if (this.dtI != null) {
                this.dtI.destroy();
                this.dtI = null;
            }
            if (this.dtF != null) {
                this.dtF.setVisible(false);
            }
            if (this.dtG != null) {
                this.dtG.setVisible(false);
            }
            if (this.dtE != null) {
                this.dtE.setVisible(false);
            }
        } else {
            destroy();
        }
        if (this.dtM.dxZ() == null) {
            if (!m42835iL()) {
                NurseryDefaults xF = this.f9755kj.ala().aJe().mo19006xF();
                this.dtM.mo14351b(xF.bZC(), xF.bZa(), xF.bZc(), (MissionTemplate) null);
                if (z) {
                    bfn();
                } else {
                    m42824cZ(true);
                }
            } else {
                this.f9755kj.aVU().mo13975h(new aPY());
            }
        } else if (z) {
            bfn();
        } else {
            m42824cZ(false);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: cZ */
    public void m42824cZ(boolean z) {
        this.f9755kj.aVU().mo13975h(new C3002mq(0, new C4183u(), z));
    }

    /* access modifiers changed from: private */
    public void bfn() {
        boolean z = false;
        this.f9755kj.aVU().mo13975h(new aIQ(0.5f));
        this.f9755kj.getEngineGame().initAllAddonWaitPlayer();
        PlayerController dxc = this.dtM.dxc();
        if (!this.dtM.bQB()) {
            if (dxc.aYa() != null) {
                dxc.mo22061al().mo656qV();
            }
            dxc.cOi();
            this.dtM.mo14419f((C1506WA) new C3002mq(0));
        } else {
            dxc.cOI();
            z = true;
        }
        this.f9755kj.aVU().mo13975h(new aIQ(0.85f));
        this.dtM.mo14429ks(true);
        this.dtM.dyt();
        this.f9755kj.aVU().mo13975h(new aIQ(0.9f));
        this.f9755kj.aVU().mo13975h(new aUU(5));
        this.f9755kj.aVU().mo13975h(new aIQ(0.95f));
        dxc.cPp();
        dxc.cPx();
        if (z) {
            this.f9755kj.aVU().mo13975h(new C3002mq(1));
        }
        bfs();
    }

    /* access modifiers changed from: private */
    public void bfo() {
        boolean z;
        this.dtF.setVisible(true);
        if (this.dtM.isUnnamed()) {
            this.dtS.setText(this.f9755kj.translate("<Tutorial Pending>"));
        } else {
            this.dtS.setText(this.dtM.getName());
        }
        this.dtW.setText("T$ " + String.format("%,d", new Object[]{Long.valueOf(this.dtM.mo5156Qw().bSs())}));
        this.dtY.setVisible(true);
        if (this.dtM.dxZ() == null) {
            z = true;
        } else {
            z = false;
        }
        if (z) {
            this.dtT.setText(this.f9755kj.translate("$None"));
        } else if (this.dtM.bQB()) {
            String name = ((Station) this.dtM.bhE()).getName();
            this.dtT.setText(String.valueOf(name) + " - " + ((Station) this.dtM.bhE()).azW().mo21665ke().get());
        } else {
            this.dtT.setText(((Ship) this.dtM.bhE()).azW().mo21665ke().get());
        }
        if (z) {
            this.dtU.setText(this.f9755kj.translate("$None"));
            this.dtZ.setText(this.f9755kj.translate("$None"));
            this.dtI.setVisible(false);
        } else {
            this.dtU.setText(this.dtM.dxZ().djk().getName());
            Ship bQx = this.dtM.bQx();
            if (bQx != null) {
                this.dtZ.setText(bQx.agH().mo19891ke().get());
                Asset Nu = bQx.agH().mo4151Nu();
                C4182t tVar = new C4182t();
                this.f9755kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/default_scene.pro", "default_scene", (Scene) null, new C4181s(), "CharacterSelectionAddon");
                this.f9755kj.getEngineGame().getLoaderTrail().mo4995a(Nu.getFile(), Nu.getHandle(), (Scene) null, tVar, "CharacterSelectionAddon");
            } else {
                this.dtZ.setText(this.f9755kj.translate("$None"));
            }
        }
        if (this.dtM.bYd() != null) {
            this.dtV.setText(this.dtM.bYd().getName());
            return;
        }
        this.dtV.setText(this.f9755kj.translate("$None"));
    }

    private void bfp() {
        if (this.dtP == null) {
            bfq();
        }
        this.dtP.dhP().setVisible(true);
        this.dtD.setVisible(false);
        this.dtE.setVisible(false);
        this.dtF.setVisible(false);
        this.dtG.setVisible(false);
    }

    private void bfq() {
        this.dtP = new C3058nY(this.f9755kj);
        this.dtP.mo20777g((ActionListener) new C4180r());
    }

    /* access modifiers changed from: private */
    public void bfr() {
        if (this.duf != null && this.dub != null && !this.dug) {
            this.dug = true;
            this.f9756lV.aeD();
            this.dub.setVisible(false);
            this.dub.destroy();
            this.dub = null;
            C6082agC.bWV().aRR();
            IBaseUiTegXml.initBaseUItegXML().mo13709a((RenderTask) new C4172j());
            this.f9755kj.aVU().mo13964a(this.f9757wz);
            this.f9755kj.aVU().mo13964a(this.dud);
            m42822cX(false);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: iL */
    public boolean m42835iL() {
        return this.f9755kj != null && this.f9755kj.ala().aLS().mo22273iL();
    }

    private void bfs() {
        if (m42835iL()) {
            destroy();
            Timer timer = new Timer(C1298TD.hFl, new C4173k());
            timer.setRepeats(false);
            timer.start();
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$f */
    /* compiled from: a */
    class C4166f implements Repeater.C3671a<Player> {
        C4166f() {
        }

        /* renamed from: a */
        public void mo843a(Player aku, Component component) {
            JButton jButton = (JButton) component;
            CharacterSelectionAddon.this.dtK.put(aku, jButton);
            if (aku.isUnnamed()) {
                jButton.setText(CharacterSelectionAddon.this.f9755kj.translate("<Tutorial Pending>"));
            } else {
                jButton.setText(aku.getName());
            }
            jButton.addMouseListener(new C4168b(aku));
            jButton.addKeyListener(new C4167a());
        }

        /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$f$b */
        /* compiled from: a */
        class C4168b extends MouseAdapter {
            private final /* synthetic */ Player agw;

            C4168b(Player aku) {
                this.agw = aku;
            }

            public void mouseClicked(MouseEvent mouseEvent) {
                if (mouseEvent.getClickCount() == 2) {
                    CharacterSelectionAddon.this.m42816aZ(this.agw);
                    CharacterSelectionAddon.this.m42822cX(true);
                } else if (mouseEvent.getClickCount() == 1) {
                    CharacterSelectionAddon.this.m42816aZ(this.agw);
                    CharacterSelectionAddon.this.bfo();
                }
            }
        }

        /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$f$a */
        class C4167a extends KeyAdapter {
            C4167a() {
            }

            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == 10) {
                    CharacterSelectionAddon.this.bfo();
                }
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$e */
    /* compiled from: a */
    class C4165e extends C3428rT<aUU> {
        C4165e() {
        }

        /* renamed from: a */
        public void mo321b(aUU auu) {
            switch (auu.dAD()) {
                case 2:
                    CharacterSelectionAddon.this.m42832i(CharacterSelectionAddon.this.f9755kj.getEngineGame().getUser());
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$d */
    /* compiled from: a */
    class C4164d extends C6124ags<aPY> {
        C4164d() {
        }

        /* renamed from: a */
        public boolean updateInfo(aPY apy) {
            ProgressionCareer Rn = apy.mo10808Rn();
            if (Rn != null) {
                CharacterSelectionAddon.this.dtM.mo14355b(Rn, 0);
                if (CharacterSelectionAddon.this.m42835iL()) {
                    Station xh = Rn.mo21962xh();
                    if (0 >= Rn.aaA().size()) {
                        throw new IllegalArgumentException("Wrong ship index. Should be less than " + Rn.aaA().size());
                    }
                    CharacterSelectionAddon.this.dtM.mo14352b(xh, (ShipType) Rn.aaA().get(0), (List<WeaponType>) Rn.aaE(), (MissionTemplate) null);
                }
                CharacterSelectionAddon.this.m42824cZ(false);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$c */
    /* compiled from: a */
    class C4163c implements C0907NJ {
        C4163c() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                CharacterSelectionAddon.this.duf = (C3176ol) renderAsset;
                CharacterSelectionAddon.this.f9756lV.mo3006a((Video) new FlashVideo(CharacterSelectionAddon.this.duf));
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$b */
    /* compiled from: a */
    class C4162b extends MouseAdapter {
        C4162b() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            CharacterSelectionAddon.this.bfr();
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$a */
    class C4161a extends C3428rT<C3689to> {
        C4161a() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            toVar.adC();
            CharacterSelectionAddon.this.bfr();
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$g */
    /* compiled from: a */
    class C4169g extends C3428rT<ExclusiveVideoPlayer> {
        C4169g() {
        }

        /* renamed from: a */
        public void mo321b(ExclusiveVideoPlayer exclusiveVideoPlayer) {
            CharacterSelectionAddon.this.bfr();
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$h */
    /* compiled from: a */
    class C4170h implements ActionListener {
        C4170h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CharacterSelectionAddon.this.bfi();
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$i */
    /* compiled from: a */
    class C4171i implements ActionListener {
        C4171i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CharacterSelectionAddon.this.bfj();
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$m */
    /* compiled from: a */
    class C4175m implements ActionListener {
        C4175m() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CharacterSelectionAddon.this.m42822cX(true);
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$l */
    /* compiled from: a */
    class C4174l extends KeyAdapter {
        C4174l() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 10) {
                CharacterSelectionAddon.this.m42822cX(true);
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$q */
    /* compiled from: a */
    class C4179q extends C0947Nt<Player> {
        C4179q() {
        }

        /* renamed from: U */
        public void mo4278E(Player aku) {
            CharacterSelectionAddon.this.dtM = aku;
            if (!CharacterSelectionAddon.this.dua) {
                CharacterSelectionAddon.this.bfh();
            }
        }

        /* renamed from: c */
        public void mo4279c(Throwable th) {
            CharacterSelectionAddon.this.f9755kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, CharacterSelectionAddon.this.f9755kj.translate("You have reached the maximum number of characters"), new Object[0]));
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$p */
    /* compiled from: a */
    class C4178p extends aEP {
        C4178p() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                CharacterSelectionAddon.this.bfk();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.WARNING;
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$o */
    /* compiled from: a */
    class C4177o extends aEP {
        C4177o() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                CharacterSelectionAddon.this.bfl();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.WARNING;
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$n */
    /* compiled from: a */
    class C4176n implements C3002mq.C3003a {
        private final /* synthetic */ boolean aYc;

        C4176n(boolean z) {
            this.aYc = z;
        }

        public void execute() {
            CharacterSelectionAddon.this.m42823cY(this.aYc);
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$u */
    /* compiled from: a */
    class C4183u implements C3002mq.C3003a {
        C4183u() {
        }

        public void execute() {
            CharacterSelectionAddon.this.bfn();
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$t */
    /* compiled from: a */
    class C4182t implements C0907NJ {
        C4182t() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (CharacterSelectionAddon.this.dtI != null) {
                SceneObject sceneObject = (SceneObject) renderAsset;
                CharacterSelectionAddon.this.dtI.getScene().addChild(sceneObject);
                CharacterSelectionAddon.this.dtI.mo16902c(sceneObject, 30.0f);
                CharacterSelectionAddon.this.dtI.csq().setDistance(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 0.699999988079071d * sceneObject.getAabbWSLenght()));
                CharacterSelectionAddon.this.dtI.setVisible(true);
                CharacterSelectionAddon.this.dtI.getSceneView().getSceneViewQuality().setShaderQuality(CharacterSelectionAddon.this.f9755kj.ale().getShaderQuality());
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$s */
    /* compiled from: a */
    class C4181s implements C0907NJ {
        C4181s() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (CharacterSelectionAddon.this.dtI != null) {
                CharacterSelectionAddon.this.dtI.getScene().removeAllChildren();
                CharacterSelectionAddon.this.dtI.getScene().addChild((SceneObject) renderAsset);
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$r */
    /* compiled from: a */
    class C4180r implements ActionListener {
        C4180r() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (actionEvent.getID() == 1) {
                CharacterSelectionAddon.this.dtD.setVisible(true);
                CharacterSelectionAddon.this.dtE.setVisible(true);
                CharacterSelectionAddon.this.dtG.setVisible(true);
            } else {
                CharacterSelectionAddon.this.dtM = CharacterSelectionAddon.this.dtP.mo20769dL();
                CharacterSelectionAddon.this.m42822cX(false);
            }
            if (CharacterSelectionAddon.this.dtP != null) {
                CharacterSelectionAddon.this.dtP.dhN();
                CharacterSelectionAddon.this.dtP.destroy();
                CharacterSelectionAddon.this.dtP = null;
            }
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$j */
    /* compiled from: a */
    class C4172j implements RenderTask {
        C4172j() {
        }

        public void run(RenderView renderView) {
            CharacterSelectionAddon.this.duf.stop();
            CharacterSelectionAddon.this.duf.destroy();
            CharacterSelectionAddon.this.duf = null;
        }
    }

    /* renamed from: taikodom.addon.characterselection.CharacterSelectionAddon$k */
    /* compiled from: a */
    class C4173k implements ActionListener {
        C4173k() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Date date = new Date(CharacterSelectionAddon.this.f9755kj.getEngineGame().getTaikodom().cVr());
            int year = date.getYear() + KeyCode.csX;
            Date date2 = new Date(year, date.getMonth(), date.getDate());
            CharacterSelectionAddon.this.f9755kj.getPlayer().mo14419f((C1506WA) new C2733jJ(C5956adg.format(CharacterSelectionAddon.this.f9755kj.translate("DATE (ER YEAR)"), date2, Integer.valueOf((year - 2073) + 1900)), false, new Object[0]));
        }
    }
}
