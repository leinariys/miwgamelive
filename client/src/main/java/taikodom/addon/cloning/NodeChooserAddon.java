package taikodom.addon.cloning;

import game.network.message.externalizable.aSC;
import game.network.message.externalizable.aUU;
import game.script.cloning.CloningDefaults;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.nls.NLSCloning;
import game.script.nls.NLSManager;
import game.script.npc.NPC;
import game.script.resource.Asset;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.aaa.C6382alq;
import logic.swing.C5378aHa;
import logic.ui.Panel;
import logic.ui.item.InternalFrame;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.cloning")
/* compiled from: a */
public class NodeChooserAddon implements C2495fo {
    private static final String ipt = "nodeChooser.xml";
    /* renamed from: kj */
    public IAddonProperties f9808kj;
    /* renamed from: nM */
    public InternalFrame f9809nM;
    /* access modifiers changed from: private */
    private C3428rT<C6382alq> fdc;
    /* access modifiers changed from: private */
    private C3428rT<aUU> ipu;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9808kj = addonPropertie;
        this.fdc = new C4231c();
        this.ipu = new C4229a();
        this.f9808kj.aVU().mo13965a(C6382alq.class, this.fdc);
        this.f9808kj.aVU().mo13965a(aUU.class, this.ipu);
    }

    /* access modifiers changed from: private */
    /* renamed from: Im */
    public void m42989Im() {
        this.f9809nM = this.f9808kj.bHv().mo16792bN(ipt);
        this.f9809nM.setClosable(false);
    }

    /* access modifiers changed from: private */
    /* renamed from: iJ */
    public void m42996iJ() {
        CloningDefaults xd = this.f9808kj.ala().aJe().mo19010xd();
        NLSCloning ach = (NLSCloning) this.f9808kj.ala().aIY().mo6310c(NLSManager.C1472a.CLONING);
        m42991a(this.f9809nM.mo4915cd("station1"), xd.bSK(), xd.bSU(), xd.bSY(), xd.bTc(), xd.bTg(), xd.bSO(), ach.cQm().get(), this.f9808kj.translate("nodeOption1"));
        m42991a(this.f9809nM.mo4915cd("station2"), xd.bSM(), xd.bSW(), xd.bTa(), xd.bTe(), xd.bTi(), xd.bSQ(), ach.cQo().get(), this.f9808kj.translate("nodeOption2"));
        this.f9809nM.pack();
        this.f9809nM.setSize(560, 520);
        this.f9809nM.setLocation((this.f9808kj.bHv().getScreenSize().width - this.f9809nM.getSize().width) / 2, (this.f9808kj.bHv().getScreenSize().height - this.f9809nM.getSize().height) / 2);
    }

    /* renamed from: a */
    private void m42991a(Panel aco, Station bf, MissionTemplate avh, NPC auf, ShipType ng, WeaponType apt, Asset tCVar, String str, String str2) {
        if (tCVar != null) {
            aco.mo4915cd("stationImg").mo16824a(C5378aHa.NEUTRAL, tCVar.getHandle());
        }
        aco.mo4915cd("description").setText(str);
        aco.mo4917cf("nodeName").setText(str2);
        aco.mo4913cb("choose").addActionListener(new C4230b(bf, avh, ng, apt, auf));
    }

    /* access modifiers changed from: private */
    public void destroy() {
        if (this.f9809nM != null) {
            this.f9809nM.destroy();
            this.f9809nM = null;
        }
    }

    public void stop() {
        destroy();
        this.f9808kj.aVU().mo13964a(this.fdc);
        this.f9808kj.aVU().mo13964a(this.ipu);
    }

    /* renamed from: taikodom.addon.cloning.NodeChooserAddon$c */
    /* compiled from: a */
    class C4231c extends C3428rT<C6382alq> {
        C4231c() {
        }

        /* renamed from: a */
        public void mo321b(C6382alq alq) {
            if (NodeChooserAddon.this.f9809nM == null) {
                NodeChooserAddon.this.m42989Im();
                NodeChooserAddon.this.m42996iJ();
            }
            NodeChooserAddon.this.f9809nM.setVisible(true);
        }
    }

    /* renamed from: taikodom.addon.cloning.NodeChooserAddon$a */
    class C4229a extends C3428rT<aUU> {
        C4229a() {
        }

        /* renamed from: a */
        public void mo321b(aUU auu) {
            if (auu.dAD() == 5) {
                NodeChooserAddon.this.destroy();
            }
        }
    }

    /* renamed from: taikodom.addon.cloning.NodeChooserAddon$b */
    /* compiled from: a */
    class C4230b implements ActionListener {
        private final /* synthetic */ Station aSn;
        private final /* synthetic */ MissionTemplate aSo;
        private final /* synthetic */ ShipType aSp;
        private final /* synthetic */ WeaponType aSq;
        private final /* synthetic */ NPC aSr;

        C4230b(Station bf, MissionTemplate avh, ShipType ng, WeaponType apt, NPC auf) {
            this.aSn = bf;
            this.aSo = avh;
            this.aSp = ng;
            this.aSq = apt;
            this.aSr = auf;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NodeChooserAddon.this.f9809nM.setVisible(false);
            NodeChooserAddon.this.f9808kj.aVU().mo13975h(new aSC(this.aSn, this.aSo, this.aSp, this.aSq));
            NodeChooserAddon.this.f9808kj.getPlayer().bQG().mo9430x(this.aSr);
        }
    }
}
