package taikodom.addon.cloning;

import game.network.message.externalizable.C0939Nn;
import game.script.Actor;
import game.script.cloning.CloningInfo;
import game.script.player.Player;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.aaa.C5425aIv;
import logic.aaa.C5783aaP;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.data.link.C1059PV;
import logic.res.code.C5663aRz;
import logic.ui.item.Table;
import logic.ui.item.TaskPane;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.cloning")
/* compiled from: a */
public class CloningCenterAddon implements C2495fo {
    private static final String hKQ = "cloning.xml";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9806P;
    /* access modifiers changed from: private */
    public aGC aCC;
    /* access modifiers changed from: private */
    public Station hKT;
    /* access modifiers changed from: private */
    public JSpinner hKV;
    /* access modifiers changed from: private */
    public JLabel hKW;
    /* renamed from: kj */
    public IAddonProperties f9807kj;
    private aSZ aCF;
    private C3428rT<C5783aaP> aoN;
    private TaskPane cQI;
    private Table ehG;
    private C6124ags<C5425aIv> hKR;
    private C6200aiQ<aDJ> hKS;
    private AbstractTableModel hKU;
    /* access modifiers changed from: private */
    private JButton hmE;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9807kj = addonPropertie;
        this.f9806P = this.f9807kj.getPlayer();
        if (this.f9806P != null) {
            m42972iG();
            m42962Im();
            cZu();
            this.aCC = new C4219b(this.f9807kj.translate("Cloning Center"), "cloning");
            this.aCC.mo15602jH(this.f9807kj.translate("cloningExplanationMessage"));
            this.aCC.mo15603jI("CLONING_CENTER_WINDOW");
            this.f9807kj.mo2322a("cloning", (Action) this.aCC);
            this.aCF = new aSZ(this.cQI, this.aCC, 400, 2);
            this.f9807kj.aVU().publish(this.aCF);
            this.f9807kj.mo2317P(this);
            SwingUtilities.invokeLater(new C4228i());
        }
    }

    /* renamed from: iG */
    private void m42972iG() {
        this.hKS = new C4217a();
        this.hKR = new C4227h();
        this.f9807kj.aVU().mo13965a(C5425aIv.class, this.hKR);
        this.aoN = new C4226g();
        this.f9807kj.aVU().mo13965a(C5783aaP.class, this.aoN);
    }

    @C2602hR(mo19255zf = "CLONING_CENTER_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo23626al(boolean z) {
        if (z) {
            refresh();
        }
    }

    /* renamed from: Im */
    private void m42962Im() {
        this.cQI = (TaskPane) this.f9807kj.bHv().mo16794bQ(hKQ);
        this.hKU = new C4225f();
        this.hKV = this.cQI.mo4915cd("cloneQuant");
        this.hKW = this.cQI.mo4917cf("cost");
        this.hmE = this.cQI.mo4915cd("buy");
        this.ehG = this.cQI.mo4915cd("cloneStationTable");
        m42961Fa();
    }

    public void stop() {
        if (this.f9806P != null) {
            this.f9806P.dxZ().mo8326b(C1059PV.dSC, (C6200aiQ<?>) this.hKS);
        }
        this.f9807kj.aVU().mo13964a(this.aoN);
        this.f9807kj.aVU().mo13964a(this.hKR);
    }

    /* access modifiers changed from: private */
    public void refresh() {
        Station cZu = cZu();
        if (cZu == null) {
            this.f9807kj.aVU().mo13975h(new C0269DS(this.aCF, false));
            return;
        }
        int bSS = this.f9807kj.ala().aJe().mo19010xd().bSS();
        this.hKV.getModel().setValue(1);
        if (cZu.azL() != null) {
            this.hKW.setText(String.valueOf(cZu.azL().mo20141JL()));
        }
        if (this.f9806P.dxZ().mo5646aw(cZu) >= bSS) {
            this.hKV.setEnabled(false);
            this.hKW.setEnabled(false);
            this.hmE.setEnabled(false);
        } else {
            this.hKV.setEnabled(true);
            this.hKW.setEnabled(true);
            this.hmE.setEnabled(true);
        }
        this.ehG.clear();
        this.ehG.getModel().fireTableDataChanged();
        C5616aQe.m17578a(this.ehG, (Insets) null, true, false);
        this.f9807kj.aVU().mo13975h(new C0269DS(this.aCF, true));
    }

    /* access modifiers changed from: private */
    public Station cZu() {
        if (this.hKT == null) {
            Actor bhE = this.f9806P.bhE();
            if (bhE == null || !(bhE instanceof Station)) {
                return null;
            }
            this.hKT = (Station) bhE;
        }
        return this.hKT;
    }

    /* renamed from: Fa */
    private void m42961Fa() {
        this.hKV.setModel(new C4224e(1, 1, 5, 1));
        this.hmE.addActionListener(new C4221d());
        this.ehG.setModel(this.hKU);
        this.f9806P.dxZ().mo8320a(C1059PV.dSC, (C6200aiQ<?>) this.hKS);
    }

    /* access modifiers changed from: private */
    public void cZv() {
        ((MessageDialogAddon) this.f9807kj.mo11830U(MessageDialogAddon.class)).mo24307a(this.f9807kj.translate("All clones at this Station are spent. Do you want to purchase more clones?"), (aEP) new C4220c(), this.f9807kj.translate("No"), this.f9807kj.translate("Yes"));
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$b */
    /* compiled from: a */
    class C4219b extends aGC {
        private static final long serialVersionUID = -8176113614152805221L;

        C4219b(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            Station a = CloningCenterAddon.this.cZu();
            if (a == null) {
                return false;
            }
            return a.mo628b(C0437GE.CLONING, CloningCenterAddon.this.f9806P);
        }

        public void setVisible(boolean z) {
            if (z) {
                CloningCenterAddon.this.refresh();
            }
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$i */
    /* compiled from: a */
    class C4228i implements Runnable {
        C4228i() {
        }

        public void run() {
            CloningCenterAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$a */
    class C4217a implements C6200aiQ<aDJ> {
        C4217a() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            SwingUtilities.invokeLater(new C4218a());
        }

        /* renamed from: taikodom.addon.cloning.CloningCenterAddon$a$a */
        class C4218a implements Runnable {
            C4218a() {
            }

            public void run() {
                CloningCenterAddon.this.refresh();
            }
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$h */
    /* compiled from: a */
    class C4227h extends C6124ags<C5425aIv> {
        C4227h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5425aIv aiv) {
            CloningCenterAddon.this.cZv();
            return false;
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$g */
    /* compiled from: a */
    class C4226g extends C3428rT<C5783aaP> {
        C4226g() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (!aap.bMO().equals(C5783aaP.C1841a.DOCKED)) {
                CloningCenterAddon.this.hKT = null;
            }
            CloningCenterAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$f */
    /* compiled from: a */
    class C4225f extends AbstractTableModel {
        private static final long serialVersionUID = 8502497131160841109L;
        private List<Station> gob = new ArrayList();

        C4225f() {
        }

        public Class<?> getColumnClass(int i) {
            if (i == 0) {
                return Boolean.class;
            }
            return String.class;
        }

        public int getColumnCount() {
            return 4;
        }

        public int getRowCount() {
            return this.gob.size();
        }

        public Object getValueAt(int i, int i2) {
            switch (i2) {
                case 0:
                    return new Boolean(this.gob.get(i) == CloningCenterAddon.this.f9807kj.getPlayer().dxZ().djk());
                case 1:
                    return this.gob.get(i).getName();
                case 2:
                    return this.gob.get(i).azW().mo21665ke().get();
                case 3:
                    Station bf = this.gob.get(i);
                    if (bf == CloningCenterAddon.this.f9807kj.getPlayer().dxZ().mo5652xh()) {
                        return "-";
                    }
                    return Integer.valueOf(CloningCenterAddon.this.f9807kj.getPlayer().dxZ().mo5646aw(bf));
                default:
                    return null;
            }
        }

        public void fireTableDataChanged() {
            this.gob.clear();
            this.gob.add(CloningCenterAddon.this.f9807kj.getPlayer().dxZ().mo5652xh());
            this.gob.addAll(CloningCenterAddon.this.f9807kj.getPlayer().dxZ().djm());
            CloningCenterAddon.super.fireTableDataChanged();
        }

        public boolean isCellEditable(int i, int i2) {
            return i2 == 0;
        }

        public void setValueAt(Object obj, int i, int i2) {
            if (i2 == 0 && ((Boolean) obj).booleanValue()) {
                Station bf = this.gob.get(i);
                if (CloningCenterAddon.this.f9807kj.getPlayer().bhE() == bf) {
                    CloningCenterAddon.this.f9807kj.getPlayer().dxZ().mo5645au(bf);
                    fireTableCellUpdated(i, i2);
                    return;
                }
                CloningCenterAddon.this.f9807kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.INFO, CloningCenterAddon.this.f9807kj.translate("cloneInDockedStationMessage"), new Object[0]));
            }
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$e */
    /* compiled from: a */
    class C4224e extends SpinnerNumberModel {
        private static final long serialVersionUID = -3300049198990503185L;

        C4224e(int i, int i2, int i3, int i4) {
            super(i, i2, i3, i4);
        }

        public Object getNextValue() {
            Object nextValue = CloningCenterAddon.super.getNextValue();
            if (nextValue == null) {
                return null;
            }
            Station a = CloningCenterAddon.this.cZu();
            if (a == null) {
                return null;
            }
            int min = Math.min(CloningCenterAddon.this.f9807kj.ala().aJe().mo19010xd().bSS() - CloningCenterAddon.this.f9806P.dxZ().mo5646aw(a), ((Integer) nextValue).intValue());
            CloningCenterAddon.this.hKW.setText(String.valueOf(a.azL().mo20141JL() * ((long) min)));
            return Integer.valueOf(min);
        }

        public Object getPreviousValue() {
            Object previousValue = CloningCenterAddon.super.getPreviousValue();
            if (previousValue == null) {
                return previousValue;
            }
            Station a = CloningCenterAddon.this.cZu();
            if (a == null) {
                return null;
            }
            int intValue = ((Integer) previousValue).intValue();
            CloningCenterAddon.this.hKW.setText(String.valueOf(a.azL().mo20141JL() * ((long) intValue)));
            return Integer.valueOf(intValue);
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$d */
    /* compiled from: a */
    class C4221d implements ActionListener {
        C4221d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            try {
                Station a = CloningCenterAddon.this.cZu();
                if (a != null) {
                    ((CloningInfo) C3582se.m38985a(CloningCenterAddon.this.f9806P.dxZ(), (C6144ahM<?>) new C4222a())).mo5648b(a, ((Integer) CloningCenterAddon.this.hKV.getValue()).intValue());
                }
            } catch (CloningInfo.C1318a e) {
                e.printStackTrace();
            }
        }

        /* renamed from: taikodom.addon.cloning.CloningCenterAddon$d$a */
        class C4222a implements C6144ahM<Void> {
            C4222a() {
            }

            /* renamed from: a */
            public void mo1931n(Void voidR) {
                SwingUtilities.invokeLater(new C4223a());
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                if (th instanceof CloningInfo.C1318a) {
                    CloningCenterAddon.this.f9807kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, CloningCenterAddon.this.f9807kj.translate("NO_MONEY"), new Object[0]));
                }
            }

            /* renamed from: taikodom.addon.cloning.CloningCenterAddon$d$a$a */
            class C4223a implements Runnable {
                C4223a() {
                }

                public void run() {
                    CloningCenterAddon.this.refresh();
                }
            }
        }
    }

    /* renamed from: taikodom.addon.cloning.CloningCenterAddon$c */
    /* compiled from: a */
    class C4220c extends aEP {
        C4220c() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                CloningCenterAddon.this.aCC.mo15605u(true);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }
}
