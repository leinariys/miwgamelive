package taikodom.addon.npcmessage;

import game.network.message.externalizable.C3002mq;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5311aEl;
import game.script.npc.NPCType;
import game.script.resource.Asset;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.item.C2830kk;
import logic.ui.item.InternalFrame;
import logic.ui.item.Scrollable;
import logic.ui.item.TextArea;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.loading.LoadingAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.npcmessage")
/* compiled from: a */
public class NPCMessageAddon extends C6124ags<C3689to> implements C2495fo {
    private static final int aWO = 35;
    private static final int fvL = 3000;
    private static final String fvM = "npc-message.xml";
    /* access modifiers changed from: private */
    public C0454GJ fvO;
    /* access modifiers changed from: private */
    public Component fvQ;
    /* access modifiers changed from: private */
    public C5311aEl fvR;
    /* access modifiers changed from: private */
    public Scrollable fvS;
    /* access modifiers changed from: private */
    public TextArea fvT;
    /* access modifiers changed from: private */
    public Timer fvY;
    /* access modifiers changed from: private */
    public Timer fvZ;
    /* access modifiers changed from: private */
    public C5990aeO fwb;
    /* access modifiers changed from: private */
    public WrapRunnable fwd;
    /* renamed from: kj */
    public IAddonProperties f10170kj;
    /* renamed from: nM */
    public InternalFrame f10171nM;
    /* access modifiers changed from: private */
    public String text;
    private NPCType bhT;
    private List<C5311aEl> fvN;
    private C0454GJ fvP;
    private C6124ags<C5311aEl> fvU;
    private JLabel fvV;
    private JLabel fvW;
    /* access modifiers changed from: private */
    private C0454GJ fvX;
    /* access modifiers changed from: private */
    private boolean fwa;
    private C6124ags<C3002mq> fwc;

    /* renamed from: b */
    private void m44960b(C5311aEl ael) {
        ael.cXv();
        this.fvN.add(ael);
    }

    /* renamed from: iM */
    private void m44975iM() {
        this.fvQ = this.f10171nM.mo4915cd("mainPanel");
        this.fvV = this.f10171nM.mo4917cf("avatar");
        this.fvW = this.f10171nM.mo4917cf("name");
        this.fvS = this.f10171nM.mo4915cd("msgScroll");
        this.fvT = this.f10171nM.mo4915cd("msg");
    }

    private void bGg() {
        this.fvP = new C4834d();
        this.fvX = new C4833c();
        this.fvO = new C4838h();
    }

    /* renamed from: iG */
    private void m44974iG() {
        this.fvU = new C4837g();
        this.fwc = new C4836f();
    }

    /* access modifiers changed from: protected */
    public void bVP() {
        if (this.fvR != null) {
            this.f10170kj.alf().addTask("NPC Message Loading Task", this.fwd, 1000);
        }
    }

    /* access modifiers changed from: private */
    public void bVQ() {
        if (this.fvR != null) {
            if (this.fvY != null && this.fvY.isRunning()) {
                this.fvY.stop();
            }
            if (this.fvZ != null && this.fvZ.isRunning()) {
                this.fvZ.stop();
            }
            this.f10170kj.aVU().mo13975h(new C0208CZ(C3667tV.MESS_NPC_CHAT_LOOP, true));
            if (this.fwb != null) {
                this.f10170kj.aVU().mo13975h(new C0208CZ(this.fwb, true));
            }
            this.fwb = null;
            this.fvP.mo9a((JComponent) null);
        }
    }

    private void bVR() {
        this.fvZ = new Timer(fvL, new C4835e());
        this.fvY = new Timer(35, new C4832b());
    }

    /* renamed from: b */
    public boolean updateInfo(C3689to toVar) {
        this.f10171nM.close();
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m44962c(C5311aEl ael) {
        if (this.fvN.size() > 0) {
            m44960b(ael);
        } else if (this.f10171nM.isVisible()) {
            m44960b(ael);
        } else {
            m44966d(ael);
        }
    }

    /* access modifiers changed from: private */
    public void bVS() {
        if (this.fvY.isRunning()) {
            this.fvY.stop();
        }
        new aDX(this.f10171nM, "[0..-195] dur 500", C2830kk.asP, this.fvP);
    }

    /* renamed from: d */
    private void m44966d(C5311aEl ael) {
        if (this.fwa) {
            this.fvR = ael;
        } else {
            m44968e(ael);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ei */
    public void m44969ei(boolean z) {
        this.fwa = z;
    }

    /* access modifiers changed from: private */
    public void bVT() {
        if (this.fvN.size() > 0) {
            C5311aEl ael = this.fvN.get(0);
            this.fvN.remove(ael);
            if (ael.cGZ()) {
                bVT();
            } else {
                m44966d(ael);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m44968e(C5311aEl ael) {
        this.bhT = ael.cXu();
        this.text = C5956adg.format(ael.cXt().get(), this.f10170kj.getPlayer().getName());
        this.fvR = ael;
        this.fvQ.setVisible(false);
        String bTI = this.bhT.bTI();
        if (bTI == null || bTI.length() == 0) {
            this.fvV.setVisible(false);
        } else {
            this.fvV.setIcon(new ImageIcon(this.f10170kj.bHv().adz().getImage("res://data/gui/imageset/imageset_avatar/" + bTI)));
            this.fvV.setVisible(true);
        }
        this.fvW.setText(this.bhT.mo11470ke().get());
        this.fvT.setText("");
        this.f10171nM.setSize(100, 130);
        this.f10171nM.setLocation((this.f10170kj.bHv().getScreenWidth() / 2) - 145, -100);
        this.f10171nM.setAlwaysOnTop(true);
        this.f10171nM.setVisible(true);
        if (this.fwb != null) {
            this.f10170kj.aVU().mo13975h(new C0208CZ(this.fwb, true));
        }
        Asset abx = ael.abx();
        if (!(abx == null || abx.getHandle() == null)) {
            this.f10170kj.aVU().mo13975h(new C0208CZ(abx, false));
            this.fwb = abx;
        }
        new aDX(this.f10171nM, "[-195..66] dur 500", C2830kk.asP, this.fvX);
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10170kj = addonPropertie;
        this.fvN = new ArrayList();
        this.f10171nM = this.f10170kj.bHv().mo16792bN(fvM);
        this.f10171nM.setVisible(false);
        m44974iG();
        bVR();
        bGg();
        m44975iM();
        bVU();
        this.f10170kj.aVU().mo13965a(C5311aEl.class, this.fvU);
        this.f10170kj.aVU().mo13965a(C3002mq.class, this.fwc);
    }

    private void bVU() {
        this.fwd = new C4831a();
    }

    /* access modifiers changed from: private */
    public void bVV() {
        this.fvY.start();
        if (this.fwb == null) {
            this.f10170kj.aVU().mo13975h(new C0208CZ(C3667tV.MESS_NPC_CHAT_LOOP));
        }
    }

    public void stop() {
        this.f10170kj.aVU().mo13964a(this.fvU);
        this.f10170kj.aVU().mo13964a(this.fwc);
        if (this.fwd != null) {
            this.f10170kj.alf().mo7963a(this.fwd);
        }
        this.f10171nM.destroy();
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$d */
    /* compiled from: a */
    class C4834d implements C0454GJ {
        C4834d() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            NPCMessageAddon.this.f10171nM.setVisible(false);
            if (NPCMessageAddon.this.fvR != null) {
                if (NPCMessageAddon.this.fvR.cXr() != null) {
                    NPCMessageAddon.this.fvR.cXr().actionPerformed((ActionEvent) null);
                }
                NPCMessageAddon.this.fvR = null;
                NPCMessageAddon.this.bVT();
            }
        }
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$c */
    /* compiled from: a */
    class C4833c implements C0454GJ {
        C4833c() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            NPCMessageAddon.this.f10171nM.mo20876fW(true);
            NPCMessageAddon.this.fvQ.setVisible(true);
            new aDX(NPCMessageAddon.this.f10171nM, "[100..290] dur 500", C2830kk.asR, NPCMessageAddon.this.fvO);
        }
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$h */
    /* compiled from: a */
    class C4838h implements C0454GJ {
        C4838h() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            NPCMessageAddon.this.bVV();
        }
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$g */
    /* compiled from: a */
    class C4837g extends C6124ags<C5311aEl> {
        C4837g() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5311aEl ael) {
            NPCMessageAddon.this.m44962c(ael);
            return true;
        }
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$f */
    /* compiled from: a */
    class C4836f extends C6124ags<C3002mq> {
        C4836f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3002mq mqVar) {
            switch (mqVar.aiH()) {
                case 0:
                    NPCMessageAddon.this.m44969ei(true);
                    NPCMessageAddon.this.bVQ();
                    break;
                case 1:
                    NPCMessageAddon.this.m44969ei(false);
                    NPCMessageAddon.this.bVP();
                    break;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$e */
    /* compiled from: a */
    class C4835e implements ActionListener {
        C4835e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NPCMessageAddon.this.f10170kj.aVU().mo13975h(new C0208CZ(C3667tV.MESS_NPC_CHAT_LOOP, true));
            NPCMessageAddon.this.fvZ.stop();
            NPCMessageAddon.this.bVS();
        }
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$b */
    /* compiled from: a */
    class C4832b implements ActionListener {
        C4832b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (NPCMessageAddon.this.text.length() > 0) {
                NPCMessageAddon.this.fvT.setText(String.valueOf(NPCMessageAddon.this.fvT.getText()) + NPCMessageAddon.this.text.charAt(0));
                NPCMessageAddon.this.fvS.getVerticalScrollBar().setValue(NPCMessageAddon.this.fvT.getHeight() - NPCMessageAddon.this.fvT.getVisibleRect().height);
                NPCMessageAddon.this.text = NPCMessageAddon.this.text.substring(1);
                return;
            }
            NPCMessageAddon.this.f10170kj.aVU().mo13975h(new C0208CZ(C3667tV.MESS_NPC_CHAT_LOOP, true));
            NPCMessageAddon.this.fwb = null;
            NPCMessageAddon.this.fvY.stop();
            NPCMessageAddon.this.fvZ.setRepeats(false);
            NPCMessageAddon.this.fvZ.start();
        }
    }

    /* renamed from: taikodom.addon.npcmessage.NPCMessageAddon$a */
    class C4831a extends WrapRunnable {
        C4831a() {
        }

        public void run() {
            if (!((LoadingAddon) NPCMessageAddon.this.f10170kj.mo11830U(LoadingAddon.class)).isVisible()) {
                NPCMessageAddon.this.m44968e(NPCMessageAddon.this.fvR);
                NPCMessageAddon.this.f10170kj.alf().mo7963a(NPCMessageAddon.this.fwd);
            }
        }
    }
}
