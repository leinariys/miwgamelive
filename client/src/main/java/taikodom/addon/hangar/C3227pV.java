package taikodom.addon.hangar;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C3131oI;
import game.network.message.externalizable.C3689to;
import game.script.player.Player;
import logic.aaa.C2123cF;
import logic.aaa.C3949xF;
import logic.aaa.C5783aaP;
import logic.baa.C6200aiQ;
import logic.data.link.C5386aHi;
import logic.res.code.C5663aRz;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.item.C2830kk;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.addon.neo.preferences.GUIPrefAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.hangar")
/* renamed from: a.pV */
/* compiled from: a */
public class C3227pV {
    private static final String bwW = "undock_insurance_confirmation";
    private static /* synthetic */ int[] bxa;
    /* renamed from: SZ */
    public aDX f8826SZ;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public C3228a bwP;
    /* access modifiers changed from: private */
    public JButton bwR;
    /* access modifiers changed from: private */
    public C6124ags<C3131oI> bwZ;
    /* renamed from: kj */
    public IAddonProperties f8827kj;
    /* access modifiers changed from: private */
    public InternalFrame bwQ = this.f8827kj.bHv().mo16795c(HangarAddon.class, "infopanel.xml");
    /* renamed from: P */
    private Player f8825P = this.f8827kj.getPlayer();
    private C3428rT<C3949xF> bwS;
    private C6200aiQ<Player> bwT;
    private C3428rT<C5783aaP> bwU;
    private ActionListener bwV;
    private int bwX;
    /* access modifiers changed from: private */
    private int bwY;

    public C3227pV(IAddonProperties vWVar, Dimension dimension) {
        this.f8827kj = vWVar;
        this.bwQ.pack();
        this.bwX = dimension.height - this.bwQ.getHeight();
        this.bwY = dimension.height;
        this.bwQ.setLocation((dimension.width - this.bwQ.getWidth()) / 2, this.bwX);
        this.bwQ.moveToFront();
        this.bwR = this.bwQ.mo4913cb("undockButton");
        m37121Fa();
    }

    static /* synthetic */ int[] ajN() {
        int[] iArr = bxa;
        if (iArr == null) {
            iArr = new int[C3228a.values().length];
            try {
                iArr[C3228a.COMBAT.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C3228a.DOCKED.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C3228a.NAVIGATION.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            bxa = iArr;
        }
        return iArr;
    }

    /* renamed from: iL */
    private boolean m37138iL() {
        return (this.f8827kj == null || this.f8827kj.ala() == null || !this.f8827kj.ala().aLS().mo22273iL()) ? false : true;
    }

    /* renamed from: Fa */
    private void m37121Fa() {
        this.bwV = new C3239j();
        this.bwR.addActionListener(this.bwV);
        this.bwS = new C3237h();
        this.bwT = new C3238i();
        this.bwU = new C3234f();
        if (!m37138iL()) {
            this.bwZ = new C3235g();
            this.f8827kj.aVU().mo13965a(C3131oI.class, this.bwZ);
        }
        this.f8827kj.aVU().mo13965a(C3949xF.class, this.bwS);
        this.f8827kj.aVU().mo13965a(C5783aaP.class, this.bwU);
        if (this.f8825P != null) {
            this.f8825P.mo8320a(C5386aHi.hWr, (C6200aiQ<?>) this.bwT);
        }
    }

    public void init() {
        if (this.f8825P.bQB()) {
            m37123a(C3228a.DOCKED);
        } else if (this.f8827kj.alb().anX()) {
            m37123a(C3228a.COMBAT);
        } else {
            m37123a(C3228a.NAVIGATION);
        }
    }

    public void destroy() {
        if (this.bwZ != null) {
            this.f8827kj.aVU().mo13964a(this.bwZ);
            this.bwZ = null;
        }
        if (this.bwQ != null) {
            this.bwQ.destroy();
            this.bwQ = null;
            this.f8827kj.aVU().mo13964a(this.bwS);
            if (this.f8825P != null) {
                this.f8825P.mo8326b(C5386aHi.hWr, (C6200aiQ<?>) this.bwT);
            }
        }
    }

    public Rectangle getBounds() {
        return new Rectangle(this.bwQ.getX(), this.bwQ.getY(), this.bwQ.getWidth(), this.bwQ.getHeight());
    }

    public void setVisible(boolean z) {
        if (z) {
            init();
            this.bwQ.setVisible(true);
            if (this.f8826SZ != null) {
                this.f8826SZ.kill();
            }
            this.f8826SZ = new aDX(this.bwQ, "[" + this.bwY + ".." + this.bwX + "] dur 500 strong_out", C2830kk.asP, new C3232d());
            return;
        }
        if (this.f8826SZ != null) {
            this.f8826SZ.kill();
        }
        this.f8826SZ = new aDX(this.bwQ, "[" + this.bwQ.getY() + ".." + this.bwY + "] dur 500 strong_in;", C2830kk.asP, new C3233e());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m37123a(C3228a aVar) {
        boolean z = true;
        this.bwP = aVar;
        switch (ajN()[aVar.ordinal()]) {
            case 1:
                this.bwR.setText(this.f8827kj.mo11834a((Class<?>) C3227pV.class, "Combat Mode", new Object[0]));
                this.bwR.setEnabled(true);
                return;
            case 2:
                this.bwR.setText(this.f8827kj.mo11834a((Class<?>) C3227pV.class, "Combat Mode Active", new Object[0]));
                this.bwR.setEnabled(false);
                return;
            case 3:
                this.bwR.setText(this.f8827kj.mo11834a((Class<?>) C3227pV.class, "Undock", new Object[0]));
                JButton jButton = this.bwR;
                if (this.f8825P.bQx() == null) {
                    z = false;
                }
                jButton.setEnabled(z);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void ajL() {
        this.f8827kj.alb().cOW();
    }

    /* access modifiers changed from: private */
    public void ajM() {
        PlayerController alb = this.f8827kj.alb();
        if (alb.aYa() == null) {
            this.f8827kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f8827kj.mo11834a((Class<?>) C3227pV.class, "TRYING_TO_UNDOCK_WITHOUT_AN_ACTIVE_SHIP", new Object[0]), new Object[0]));
        } else {
            m37134e(alb);
        }
    }

    /* renamed from: e */
    private void m37134e(PlayerController tVar) {
        this.bwR.setEnabled(false);
        ((PlayerController) C3582se.m38985a(tVar, (C6144ahM<?>) new C3229b(tVar))).cPr();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m37129a(PlayerController tVar, Boolean bool) {
        if (!bool.booleanValue() || "true".equals(GUIPrefAddon.m44898f("hangar", bwW, ""))) {
            m37136f(tVar);
            return;
        }
        ((MessageDialogAddon) this.f8827kj.mo11830U(MessageDialogAddon.class)).mo24307a(this.f8827kj.translate("insuranceMessage"), (aEP) new C3231c(tVar), this.f8827kj.translate("No"), this.f8827kj.translate("Yes"));
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m37136f(PlayerController tVar) {
        this.bwR.setEnabled(false);
        tVar.ajM();
        if (aMU.MISSION_1_TAKEN.equals(this.f8827kj.getPlayer().cXm())) {
            this.f8827kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.FIRST_LAUNCH));
        }
        this.f8827kj.aVU().mo13975h(new C3689to(C3689to.C3690a.SHIP_LAUNCH));
    }

    /* renamed from: bw */
    public void mo21162bw(boolean z) {
        m37123a(z ? C3228a.DOCKED : C3228a.COMBAT);
        setVisible(z);
    }

    /* renamed from: a.pV$a */
    private enum C3228a {
        NAVIGATION,
        COMBAT,
        DOCKED
    }

    /* renamed from: a.pV$j */
    /* compiled from: a */
    class C3239j implements ActionListener {
        private static /* synthetic */ int[] bxa;

        C3239j() {
        }

        static /* synthetic */ int[] ajN() {
            int[] iArr = bxa;
            if (iArr == null) {
                iArr = new int[C3228a.values().length];
                try {
                    iArr[C3228a.COMBAT.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C3228a.DOCKED.ordinal()] = 3;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C3228a.NAVIGATION.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                bxa = iArr;
            }
            return iArr;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            switch (ajN()[C3227pV.this.bwP.ordinal()]) {
                case 1:
                    C3227pV.this.ajL();
                    return;
                case 3:
                    C3227pV.this.ajM();
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: a.pV$h */
    /* compiled from: a */
    class C3237h extends C3428rT<C3949xF> {
        C3237h() {
        }

        /* renamed from: a */
        public void mo321b(C3949xF xFVar) {
            C3228a aVar;
            C3227pV pVVar = C3227pV.this;
            if (xFVar.anX()) {
                aVar = C3228a.COMBAT;
            } else {
                aVar = C3228a.NAVIGATION;
            }
            pVVar.m37123a(aVar);
        }
    }

    /* renamed from: a.pV$i */
    /* compiled from: a */
    class C3238i implements C6200aiQ<Player> {
        C3238i() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            C3227pV.this.bwR.setEnabled(obj != null);
        }
    }

    /* renamed from: a.pV$f */
    /* compiled from: a */
    class C3234f extends C3428rT<C5783aaP> {
        C3234f() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (aap.bMO() == C5783aaP.C1841a.FLYING) {
                C3227pV.this.init();
            }
        }
    }

    /* renamed from: a.pV$g */
    /* compiled from: a */
    class C3235g extends C6124ags<C3131oI> {
        C3235g() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3131oI oIVar) {
            if (aMU.MISSION_1_TAKEN.equals(C3227pV.this.f8827kj.getPlayer().cXm()) && C3131oI.C3132a.HIGHLIGHT_HANGAR.equals(oIVar.mo20956Us())) {
                Timer timer = new Timer(1500, new C3236a());
                timer.setRepeats(false);
                timer.start();
                C3227pV.this.f8827kj.aVU().mo13964a(C3227pV.this.bwZ);
                C3227pV.this.bwZ = null;
            }
            return false;
        }

        /* renamed from: a.pV$g$a */
        class C3236a implements ActionListener {
            C3236a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                C3227pV.this.f8827kj.aVU().mo13975h(new C2123cF(C3227pV.this.bwR));
            }
        }
    }

    /* renamed from: a.pV$d */
    /* compiled from: a */
    class C3232d implements C0454GJ {
        C3232d() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C3227pV.this.f8826SZ = null;
        }
    }

    /* renamed from: a.pV$e */
    /* compiled from: a */
    class C3233e implements C0454GJ {
        C3233e() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C3227pV.this.f8826SZ = null;
            C3227pV.this.bwQ.setVisible(false);
        }
    }

    /* renamed from: a.pV$b */
    /* compiled from: a */
    class C3229b implements C6144ahM<Boolean> {
        private final /* synthetic */ PlayerController eZC;

        C3229b(PlayerController tVar) {
            this.eZC = tVar;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            SwingUtilities.invokeLater(new C3230a(this.eZC, bool));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            C3227pV.this.bwR.setEnabled(true);
        }

        /* renamed from: a.pV$b$a */
        class C3230a implements Runnable {
            private final /* synthetic */ PlayerController eZC;
            private final /* synthetic */ Boolean eZD;

            C3230a(PlayerController tVar, Boolean bool) {
                this.eZC = tVar;
                this.eZD = bool;
            }

            public void run() {
                C3227pV.this.m37129a(this.eZC, this.eZD);
                C3227pV.this.bwR.setEnabled(true);
            }
        }
    }

    /* renamed from: a.pV$c */
    /* compiled from: a */
    class C3231c extends aEP {
        private final /* synthetic */ PlayerController eZC;

        C3231c(PlayerController tVar) {
            this.eZC = tVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                if (z) {
                    GUIPrefAddon.m44897e("hangar", C3227pV.bwW, "true");
                }
                C3227pV.this.m37136f(this.eZC);
            }
        }

        /* access modifiers changed from: protected */
        public String cyE() {
            return C3227pV.this.f8827kj.translate("Never show this message again");
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.WARNING;
        }
    }
}
