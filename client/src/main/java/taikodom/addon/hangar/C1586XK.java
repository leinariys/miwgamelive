package taikodom.addon.hangar;

import game.network.message.externalizable.C1749Zq;
import game.script.Actor;
import game.script.hangar.Bay;
import game.script.hangar.Hangar;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ShipItem;
import game.script.player.Player;
import game.script.ship.Station;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.C2698il;
import logic.ui.C3810vI;
import logic.ui.aRU;
import logic.ui.item.C2830kk;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import logic.ui.item.Scrollable;
import p001a.C6124ags;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.TItemPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@TaikodomAddon("taikodom.addon.hangar")
/* renamed from: a.XK */
/* compiled from: a */
public class C1586XK {
    /* access modifiers changed from: private */

    /* renamed from: SZ */
    public aDX f2112SZ;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public C0454GJ atu;
    /* access modifiers changed from: private */
    public Hangar cXJ;
    /* access modifiers changed from: private */
    public InternalFrame eJB;
    /* access modifiers changed from: private */
    public Repeater<Bay> eJD;
    private int bwY;
    private C6124ags<C1749Zq> eJC;
    private C2698il eJE;
    private Point eJF;
    private C0454GJ eJG;
    private C0454GJ eJH;
    /* renamed from: kj */
    private IAddonProperties f2113kj;
    /* renamed from: P */
    public Player f2111P = this.f2113kj.getPlayer();

    public C1586XK(IAddonProperties vWVar, Rectangle rectangle) {
        this.f2113kj = vWVar;
        if (this.f2111P.bhE() instanceof Station) {
            this.cXJ = this.f2111P.mo14346aD((Station) this.f2111P.bhE());
        }
        m11476qb();
        update();
        bGg();
        this.eJF = new Point(rectangle.x + aRU.CENTER.mo11143t(rectangle.width, this.eJB.getWidth()), C3810vI.BOTTOM.mo16934t(rectangle.y, this.eJB.getHeight()) - 2);
        this.bwY = this.f2113kj.bHv().getScreenHeight();
        this.eJB.setLocation(this.eJF);
        m11463Fa();
    }

    private void bGg() {
        this.eJG = new C1593e();
        this.eJH = new C1592d();
    }

    /* renamed from: qb */
    private void m11476qb() {
        this.eJB = this.f2113kj.bHv().mo16795c(HangarAddon.class, "hangarview.xml");
        this.eJB.pack();
        this.eJB.moveToFront();
        this.eJE = this.eJB.mo4916ce("hangarShips");
        this.eJD = (Repeater<Bay>) this.eJB.mo4919ch("baysRepeater");
        this.eJD.mo22250a(new C1587a());
    }

    /* renamed from: Fa */
    private void m11463Fa() {
        this.eJC = new C1591c();
        this.f2113kj.aVU().mo13965a(C1749Zq.class, this.eJC);
    }

    public void init() {
        m11472dH(true);
        this.eJB.moveToBack();
    }

    public void destroy() {
        if (this.eJB != null) {
            this.eJB.destroy();
            this.eJB = null;
        }
        this.f2113kj.aVU().mo13968b(C1749Zq.class, this.eJC);
    }

    /* renamed from: dH */
    private void m11472dH(boolean z) {
        if (z) {
            this.eJE.setSize(this.eJE.getWidth(), this.eJE.getPreferredSize().height);
        } else {
            this.eJE.setSize(this.eJE.getWidth(), 0);
        }
        this.eJE.setVisible(z);
        this.eJB.moveToBack();
    }

    /* renamed from: b */
    public void mo6711b(boolean z, C0454GJ gj) {
        this.atu = gj;
        update();
        this.eJB.setEnabled(false);
        if (z) {
            this.eJB.setVisible(true);
            if (this.f2112SZ != null) {
                this.f2112SZ.kill();
            }
            this.f2112SZ = new aDX(this.eJB, "[" + this.bwY + ".." + this.eJF.y + "] dur 500 strong_out", C2830kk.asP, this.eJG);
            return;
        }
        if (this.f2112SZ != null) {
            this.f2112SZ.kill();
        }
        this.f2112SZ = new aDX(this.eJB, "[" + this.eJB.getY() + ".." + this.bwY + "] dur 500 strong_in;", C2830kk.asP, this.eJH);
    }

    public Rectangle getBounds() {
        return this.eJB.getBounds();
    }

    /* access modifiers changed from: private */
    public Bay bGh() {
        for (Bay ajm : this.cXJ.mo20056KU()) {
            if (!ajm.isEmpty()) {
                return ajm;
            }
        }
        return null;
    }

    /* renamed from: bw */
    public void mo6712bw(boolean z) {
        if (z) {
            mo6711b(z, (C0454GJ) null);
        }
    }

    /* access modifiers changed from: private */
    public void update() {
        Actor bhE = this.f2111P.bhE();
        if (bhE instanceof Station) {
            this.cXJ = this.f2111P.mo14346aD((Station) bhE);
            SwingUtilities.invokeLater(new C1590b(bhE));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m11470c(Bay ajm) {
        if (ajm != null) {
            this.f2111P.mo12634P(ajm.mo9625al());
        }
    }

    /* renamed from: a.XK$e */
    /* compiled from: a */
    class C1593e implements C0454GJ {
        C1593e() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C1586XK.this.eJB.setEnabled(true);
            C1586XK.this.f2112SZ = null;
            if (C1586XK.this.atu != null) {
                C1586XK.this.atu.mo9a(C1586XK.this.eJB);
                C1586XK.this.atu = null;
            }
        }
    }

    /* renamed from: a.XK$d */
    /* compiled from: a */
    class C1592d implements C0454GJ {
        C1592d() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C1586XK.this.f2112SZ = null;
            C1586XK.this.eJB.setVisible(false);
            if (C1586XK.this.atu != null) {
                C1586XK.this.atu.mo9a(C1586XK.this.eJB);
                C1586XK.this.atu = null;
            }
        }
    }

    /* renamed from: a.XK$a */
    class C1587a implements Repeater.C3671a<Bay> {
        C1587a() {
        }

        /* renamed from: a */
        public void mo843a(Bay ajm, Component component) {
            TItemPanel tItemPanel = new TItemPanel();
            Scrollable cd = tItemPanel.mo4915cd("itemListScrollable");
            cd.setHorizontalScrollBarPolicy(31);
            cd.setVerticalScrollBarPolicy(21);
            tItemPanel.mo23646a((TItemPanel.C4236e) new C1588a());
            tItemPanel.mo23647b((ItemLocation) ajm);
            ((JComponent) component).add(tItemPanel);
            ((JComponent) component).addMouseListener(new C1589b(ajm));
        }

        /* renamed from: a.XK$a$a */
        class C1588a implements TItemPanel.C4236e {
            C1588a() {
            }

            /* renamed from: H */
            public void mo6717H(Item auq) {
                ShipItem arq = (ShipItem) auq;
                if (C1586XK.this.f2111P.bQx() != arq.mo11127al()) {
                    C1586XK.this.f2111P.mo12634P(arq.mo11127al());
                }
            }
        }

        /* renamed from: a.XK$a$b */
        /* compiled from: a */
        class C1589b extends MouseAdapter {
            private final /* synthetic */ Bay hxG;

            C1589b(Bay ajm) {
                this.hxG = ajm;
            }

            public void mouseReleased(MouseEvent mouseEvent) {
                C1586XK.this.m11470c(this.hxG);
            }
        }
    }

    /* renamed from: a.XK$c */
    /* compiled from: a */
    class C1591c extends C6124ags<C1749Zq> {
        C1591c() {
        }

        /* renamed from: a */
        public boolean updateInfo(C1749Zq zq) {
            C1586XK.this.update();
            if (C1586XK.this.f2111P.bQx() != null) {
                return false;
            }
            C1586XK.this.m11470c(C1586XK.this.bGh());
            return false;
        }
    }

    /* renamed from: a.XK$b */
    /* compiled from: a */
    class C1590b implements Runnable {
        private final /* synthetic */ Actor hYT;

        C1590b(Actor cr) {
            this.hYT = cr;
        }

        public void run() {
            C1586XK.this.eJD.clear();
            if (this.hYT instanceof Station) {
                for (Bay G : C1586XK.this.cXJ.mo20056KU()) {
                    C1586XK.this.eJD.mo22248G(G);
                }
            }
        }
    }
}
