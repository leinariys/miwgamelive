package taikodom.addon.hangar;

import game.network.message.externalizable.C3131oI;
import logic.IAddonSettings;
import logic.aaa.C2327eB;
import logic.aaa.C5783aaP;
import logic.swing.C0454GJ;
import p001a.C3428rT;
import p001a.aMU;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import javax.swing.*;
import java.awt.*;

@TaikodomAddon("taikodom.addon.hangar")
/* compiled from: a */
public class HangarAddon implements C2495fo {
    public static final int gfJ = -2;
    /* access modifiers changed from: private */
    public C3428rT<C3131oI> ccC;
    /* access modifiers changed from: private */
    public boolean gfP;
    /* renamed from: kj */
    public IAddonProperties f9892kj;
    private C3428rT<C5783aaP> gfK;
    private C1586XK gfL;
    private C3227pV gfM;
    private C3428rT<C2327eB> gfN;
    /* access modifiers changed from: private */
    private C3428rT<HudVisibilityAddon.C4534d> gfO;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9892kj = addonPropertie;
        m43569Im();
        setVisible(false);
        if (!m43578iL()) {
            if (aMU.MISSION_1_TAKEN.ordinal() <= this.f9892kj.getPlayer().cXm().ordinal()) {
                this.gfP = false;
                initialize();
                return;
            }
            this.gfP = true;
            this.ccC = new C4428e();
            this.f9892kj.aVU().mo13965a(C3131oI.class, this.ccC);
            return;
        }
        this.gfP = false;
        initialize();
    }

    /* access modifiers changed from: protected */
    public void bli() {
        this.gfN = new C4427d();
        this.gfO = new C4425b();
        this.gfK = new C4424a();
        this.f9892kj.aVU().mo13965a(C5783aaP.class, this.gfK);
        this.f9892kj.aVU().mo13965a(C2327eB.class, this.gfN);
        this.f9892kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.gfO);
    }

    public void stop() {
        if (this.ccC != null) {
            this.f9892kj.aVU().mo13964a(this.ccC);
            this.ccC = null;
        }
        if (this.gfM != null) {
            this.gfM.destroy();
        }
        if (this.gfL != null) {
            this.gfL.destroy();
        }
        if (this.f9892kj != null) {
            this.f9892kj.aVU().mo13964a(this.gfK);
            this.f9892kj.aVU().mo13964a(this.gfN);
            this.f9892kj.aVU().mo13964a(this.gfO);
        }
    }

    /* renamed from: Im */
    private void m43569Im() {
        this.gfM = new C3227pV(this.f9892kj, this.f9892kj.bHv().getScreenSize());
        this.gfL = new C1586XK(this.f9892kj, this.gfM.getBounds());
    }

    /* access modifiers changed from: private */
    public void initialize() {
        this.gfM.init();
        this.gfL.init();
        setVisible(true);
        bli();
    }

    /* access modifiers changed from: private */
    /* renamed from: bw */
    public void m43575bw(boolean z) {
        this.gfM.mo21162bw(z);
        this.gfL.mo6712bw(z);
    }

    /* access modifiers changed from: private */
    public void setVisible(boolean z) {
        C4426c cVar;
        this.gfM.setVisible(z && this.f9892kj.getPlayer().bQB());
        if (this.f9892kj.getPlayer().bQB()) {
            if (!z || !this.gfP) {
                cVar = null;
            } else {
                cVar = new C4426c();
            }
            this.gfL.mo6711b(z, cVar);
            return;
        }
        this.gfL.mo6711b(false, (C0454GJ) null);
    }

    public Rectangle getBounds() {
        Rectangle bounds = this.gfM.getBounds();
        Rectangle bounds2 = this.gfL.getBounds();
        return new Rectangle(bounds.x, bounds2.y, bounds.width, bounds.height + bounds.height + 2);
    }

    /* renamed from: iL */
    private boolean m43578iL() {
        return this.f9892kj != null && this.f9892kj.ala().aLS().mo22273iL();
    }

    /* renamed from: taikodom.addon.hangar.HangarAddon$e */
    /* compiled from: a */
    class C4428e extends C3428rT<C3131oI> {
        C4428e() {
        }

        /* renamed from: b */
        public void mo321b(C3131oI oIVar) {
            if (C3131oI.C3132a.UNLOCK_HANGAR.equals(oIVar.mo20956Us())) {
                HangarAddon.this.initialize();
                HangarAddon.this.f9892kj.aVU().mo13964a(HangarAddon.this.ccC);
                HangarAddon.this.ccC = null;
            }
        }
    }

    /* renamed from: taikodom.addon.hangar.HangarAddon$d */
    /* compiled from: a */
    class C4427d extends C3428rT<C2327eB> {
        C4427d() {
        }

        /* renamed from: a */
        public void mo321b(C2327eB eBVar) {
            HangarAddon.this.setVisible(eBVar.isVisible());
        }
    }

    /* renamed from: taikodom.addon.hangar.HangarAddon$b */
    /* compiled from: a */
    class C4425b extends C3428rT<HudVisibilityAddon.C4534d> {
        C4425b() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            HangarAddon.this.setVisible(dVar.isVisible());
        }
    }

    /* renamed from: taikodom.addon.hangar.HangarAddon$a */
    class C4424a extends C3428rT<C5783aaP> {
        private boolean dDf;

        C4424a() {
            this.dDf = HangarAddon.this.f9892kj.getPlayer().bQB();
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (aap.bMO() == C5783aaP.C1841a.ONI) {
                HangarAddon.this.setVisible(false);
            } else if (aap.bMO() == C5783aaP.C1841a.DOCKED) {
                HangarAddon.this.m43575bw(true);
                this.dDf = true;
            } else if (aap.bMO() == C5783aaP.C1841a.FLYING && this.dDf) {
                HangarAddon.this.m43575bw(false);
                this.dDf = false;
            }
        }
    }

    /* renamed from: taikodom.addon.hangar.HangarAddon$c */
    /* compiled from: a */
    class C4426c implements C0454GJ {
        C4426c() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            HangarAddon.this.f9892kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.HIGHLIGHT_HANGAR));
            HangarAddon.this.gfP = false;
        }
    }
}
