package taikodom.addon.profileview;

import logic.IAddonSettings;
import logic.aPE;
import logic.aaa.C7016azd;
import logic.render.IEngineGraphics;
import logic.ui.item.InternalFrame;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.DrawContext;
import taikodom.render.MilesSoundBuffer;
import taikodom.render.RenderContext;
import taikodom.render.RenderView;
import taikodom.render.scene.SoundSource;
import taikodom.render.textures.BaseTexture;

import javax.swing.*;

@TaikodomAddon("taikodom.addon.profileview")
/* compiled from: a */
public class ProfileViewAddon implements C2495fo {
    /* access modifiers changed from: private */
    public JLabel aKA;
    /* access modifiers changed from: private */
    public JLabel aKB;
    /* access modifiers changed from: private */
    public JLabel aKC;
    /* access modifiers changed from: private */
    public boolean aKE = false;
    /* access modifiers changed from: private */
    public JLabel aKp;
    /* access modifiers changed from: private */
    public JLabel aKq;
    /* access modifiers changed from: private */
    public JLabel aKr;
    /* access modifiers changed from: private */
    public JLabel aKs;
    /* access modifiers changed from: private */
    public JLabel aKt;
    /* access modifiers changed from: private */
    public JLabel aKu;
    /* access modifiers changed from: private */
    public JLabel aKv;
    /* access modifiers changed from: private */
    public JLabel aKw;
    /* access modifiers changed from: private */
    public JLabel aKx;
    /* access modifiers changed from: private */
    public JLabel aKy;
    /* access modifiers changed from: private */
    public JLabel aKz;
    /* renamed from: kj */
    public IAddonProperties f10217kj;
    /* access modifiers changed from: private */
    /* renamed from: nM */
    public InternalFrame f10218nM;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public RenderView renderView;
    private aPE aKD;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10217kj = addonPropertie;
        init();
        this.f10217kj.aVU().mo13965a(C7016azd.class, new C4912b());
    }

    public void init() {
        this.renderView = this.f10217kj.ale().getRenderView();
    }

    private void hide() {
        if (this.f10218nM != null) {
            this.f10218nM.close();
            this.f10218nM = null;
            this.aKE = false;
        }
    }

    /* access modifiers changed from: private */
    public void show() {
        if (this.f10218nM != null) {
            hide();
            return;
        }
        this.f10217kj.bHv().mo16790b((Object) this, "profileview.css");
        this.f10218nM = this.f10217kj.bHv().mo16792bN("profileview.xml");
        this.f10218nM.pack();
        this.f10218nM.setFocusable(false);
        this.f10218nM.setLocation(this.f10217kj.bHv().getScreenWidth() - this.f10218nM.getWidth(), (this.f10217kj.bHv().getScreenHeight() - this.f10218nM.getHeight()) / 2);
        this.f10218nM.setVisible(true);
        this.f10218nM.setFocusable(false);
        m45201QX();
        IEngineGraphics ale = this.f10217kj.getEngineGame().getEngineGraphics();
        this.aKE = true;
        this.aKD = new C4911a(ale);
        this.f10217kj.mo11833a(1000, this.aKD);
    }

    /* renamed from: QX */
    private void m45201QX() {
        this.aKp = this.f10218nM.mo4917cf("numVisibleObjects");
        this.aKq = this.f10218nM.mo4917cf("numTris");
        this.aKr = this.f10218nM.mo4917cf("numDrawCalls");
        this.aKs = this.f10218nM.mo4917cf("numPrimitiveBinds");
        this.aKt = this.f10218nM.mo4917cf("numObjectsStepped");
        this.aKu = this.f10218nM.mo4917cf("numMaterialBinds");
        this.aKv = this.f10218nM.mo4917cf("numTexBinds");
        this.aKw = this.f10218nM.mo4917cf("numShaderBinds");
        this.aKx = this.f10218nM.mo4917cf("vramTexture");
        this.aKy = this.f10218nM.mo4917cf("numImpostorsRendered");
        this.aKz = this.f10218nM.mo4917cf("numImpostorsUpdated");
        this.aKA = this.f10218nM.mo4917cf("soundMemory");
        this.aKB = this.f10218nM.mo4917cf("numSoundSamples");
        this.aKC = this.f10218nM.mo4917cf("numActors");
    }

    public void stop() {
        this.aKE = false;
    }

    /* renamed from: taikodom.addon.profileview.ProfileViewAddon$b */
    /* compiled from: a */
    class C4912b extends C3428rT<C7016azd> {
        C4912b() {
        }

        /* renamed from: a */
        public void mo321b(C7016azd azd) {
            ProfileViewAddon.this.show();
        }
    }

    /* renamed from: taikodom.addon.profileview.ProfileViewAddon$a */
    class C4911a implements aPE {
        private final /* synthetic */ IEngineGraphics eQX;

        C4911a(IEngineGraphics abd) {
            this.eQX = abd;
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            RenderContext renderContext = ProfileViewAddon.this.renderView.getRenderContext();
            DrawContext drawContext = ProfileViewAddon.this.renderView.getDrawContext();
            ProfileViewAddon.this.aKp.setText(new StringBuilder().append(renderContext.getNumVisibleObjects()).toString());
            ProfileViewAddon.this.aKq.setText(new StringBuilder().append(drawContext.getNumTriangles()).toString());
            ProfileViewAddon.this.aKr.setText(new StringBuilder().append(drawContext.getNumDrawCalls()).toString());
            ProfileViewAddon.this.aKs.setText(new StringBuilder().append(drawContext.getNumPrimitiveBind()).toString());
            ProfileViewAddon.this.aKt.setText(new StringBuilder().append(this.eQX.aer().getNumObjectsStepped()).toString());
            ProfileViewAddon.this.aKu.setText(new StringBuilder().append(drawContext.getNumMaterarialsBind()).toString());
            ProfileViewAddon.this.aKv.setText(new StringBuilder().append(drawContext.getNumTextureBind()).toString());
            ProfileViewAddon.this.aKy.setText(new StringBuilder().append(drawContext.getNumImpostorsRendered()).toString());
            ProfileViewAddon.this.aKz.setText(new StringBuilder().append(drawContext.getNumImpostorsUpdated()).toString());
            ProfileViewAddon.this.aKw.setText(new StringBuilder().append(drawContext.getNumShadersBind()).toString());
            ProfileViewAddon.this.aKx.setText(new StringBuilder().append(BaseTexture.getTextureMemoryUsed()).toString());
            ProfileViewAddon.this.aKA.setText(new StringBuilder().append(MilesSoundBuffer.getTotalBytes()).toString());
            ProfileViewAddon.this.aKB.setText(new StringBuilder().append(SoundSource.getNumSoundSources()).toString());
            if (ProfileViewAddon.this.f10217kj.ala().aLW().dvO() != null) {
                ProfileViewAddon.this.aKC.setText(new StringBuilder().append(ProfileViewAddon.this.f10217kj.ala().aLW().dvO().cKv().size()).toString());
            }
            if (ProfileViewAddon.this.f10218nM != null) {
                ProfileViewAddon.this.f10218nM.validate();
            }
            return ProfileViewAddon.this.aKE;
        }
    }
}
