package taikodom.addon.help;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.network.message.externalizable.C6018aeq;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.res.C2084bq;
import logic.res.LoaderFileXML;
import logic.res.XmlNode;
import logic.ui.C1276Sr;
import logic.ui.C2698il;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import p001a.C3428rT;
import p001a.C5956adg;
import p001a.C6622aqW;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.help")
/* compiled from: a */
public class HelpAddon implements C2495fo {
    /* access modifiers changed from: private */
    public C3428rT<C5783aaP> aoN;
    /* access modifiers changed from: private */
    public int dDA;
    /* access modifiers changed from: private */
    public C1276Sr dDD;
    /* access modifiers changed from: private */
    public ComponentAdapter dDE;
    /* renamed from: kj */
    public IAddonProperties f9893kj;
    /* renamed from: nM */
    public InternalFrame f9894nM;
    /* renamed from: nP */
    public C3428rT<C3689to> f9895nP;
    /* access modifiers changed from: private */
    private Repeater<C4439j> dDB;
    /* access modifiers changed from: private */
    private C4438i dDC;
    /* access modifiers changed from: private */
    private C6622aqW dcE;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9893kj = addonPropertie;
        this.f9893kj.bHv().mo16790b((Object) this, "help.css");
        this.dcE = new C4431c("help", this.f9893kj.translate("tooltip.Help"), "help");
        this.dcE.mo15602jH(this.f9893kj.translate("This is the help window, use it if you are lost."));
        this.aoN = new C4432d();
        this.f9895nP = new C4436g();
        this.f9893kj.mo2322a("help", (Action) this.dcE);
        this.f9893kj.aVU().publish(new C5344aFs(this.dcE, 700, true));
    }

    public void open() {
        C6245ajJ aVU = this.f9893kj.aVU();
        if (this.f9894nM == null) {
            m43590Im();
        } else if (this.f9894nM.isVisible()) {
            this.f9894nM.setVisible(false);
            aVU.mo13974g(new C6018aeq("help", ""));
            return;
        }
        this.f9894nM.pack();
        this.f9894nM.center();
        aVU.mo13965a(C3689to.class, this.f9895nP);
        aVU.mo13965a(C5783aaP.class, this.aoN);
        aVU.mo13975h(new C1532WW(C1532WW.C1533a.OPENED, HelpAddon.class));
        this.f9894nM.setSize(this.f9894nM.getPreferredSize());
        this.f9894nM.pack();
        this.f9894nM.doLayout();
        this.f9894nM.setVisible(true);
    }

    private void setText(String str) {
        this.f9894nM.mo4917cf("text").setText(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: lT */
    public void m43601lT(int i) {
        JPanel jPanel = (C2698il) this.f9894nM.mo4915cd("contents");
        if (jPanel instanceof JPanel) {
            jPanel.removeAll();
        }
        this.dDD = null;
        setText(C5956adg.format("{0}/{1}", Integer.valueOf(i + 1), Integer.valueOf(C4439j.fKs.size())));
        this.dDB.get(C4439j.fKs.get(this.dDA)).get(0).setSelected(false);
        C4439j jVar = C4439j.fKs.get(i);
        this.dDD = this.f9893kj.bHv().mo16780a(jVar.hmP, (C2698il) jPanel);
        if (this.dDD instanceof JComponent) {
            this.dDD.setSize(this.dDD.getPreferredSize());
        }
        if (this.dDD instanceof JPanel) {
            this.dDD.doLayout();
        }
        this.dDB.get(jVar);
        if (jPanel instanceof JPanel) {
            jPanel.doLayout();
        }
        this.dDA = i;
    }

    /* renamed from: d */
    private final C4438i m43598d(XmlNode agy) {
        C4438i iVar = new C4438i(agy.getAttribute("title"));
        for (XmlNode next : agy.getListChildrenTag()) {
            if ("section".equals(next.getTagName())) {
                iVar.fKr.add(m43598d(next));
            } else if ("page".equals(next.getTagName())) {
                iVar.fKs.add(new C4439j(iVar, next.getAttribute("title"), next.getAttribute("src")));
            }
        }
        return iVar;
    }

    /* renamed from: Im */
    private void m43590Im() {
        this.f9894nM = this.f9893kj.bHv().mo16792bN("help.xml");
        this.dDE = new C4437h();
        this.f9894nM.addComponentListener(this.dDE);
        this.dDC = m43598d(biy());
        this.dDB = this.f9894nM.mo4915cd("index");
        this.dDB.mo22250a(new C4433e());
        for (C4438i iVar : this.dDC.fKr) {
            for (C4439j G : iVar.fKs) {
                this.dDB.mo22248G(G);
            }
        }
        this.f9894nM.doLayout();
        this.f9894nM.mo4913cb("nextButton").setText(this.f9893kj.translate("Next"));
        this.f9894nM.mo4913cb("previousButton").setText(this.f9893kj.translate("Previous"));
        this.f9894nM.mo4913cb("previousButton").addActionListener(new C4435f());
        this.f9894nM.mo4913cb("nextButton").addActionListener(new C4430b());
        m43601lT(0);
    }

    public void stop() {
        this.dDD = null;
        if (this.f9894nM != null) {
            this.f9894nM.destroy();
            this.f9894nM = null;
        }
        C4439j.fKs.clear();
    }

    public XmlNode biy() {
        InputStream inputStream = null;
        LoaderFileXML oMVar = new LoaderFileXML();
        try {
            inputStream = this.f9893kj.mo11846f(HelpAddon.class, "help-index.xml");
            if (inputStream == null) {
                throw new IllegalArgumentException("xmlFile argument stream is null");
            }
            XmlNode node = oMVar.loadFileXml(inputStream, "UTF-8");
            mo9022a(new C4429a(),node);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
            return node;
        } catch (IOException e2) {
            throw new IllegalArgumentException("Problems reading xmlFile stream", e2);
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                }
            }
            throw th;
        }
    }

    //todo перенес из XmlNode
    /* renamed from: a */
    public void mo9022a(C2084bq bqVar, XmlNode node) {
        bqVar.mo17466a(node);
        if (node.getListChildrenTag().size() > 0) {
            for (XmlNode xmlNode  : node.getListChildrenTag() ){
                mo9022a(bqVar,xmlNode);
            }
        }
    }


    /* renamed from: taikodom.addon.help.HelpAddon$j */
    /* compiled from: a */
    private static final class C4439j {
        static final List<C4439j> fKs = new ArrayList();
        final C4438i hmO;
        final String hmP;
        final int index = fKs.size();
        final String title;

        C4439j(C4438i iVar, String str, String str2) {
            this.hmO = iVar;
            this.title = str;
            this.hmP = str2;
            fKs.add(this);
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$i */
    /* compiled from: a */
    private static final class C4438i {
        static int total = 0;
        final List<C4438i> fKr = new ArrayList();
        final List<C4439j> fKs = new ArrayList();
        final String title;

        public C4438i(String str) {
            this.title = str;
            total++;
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$c */
    /* compiled from: a */
    class C4431c extends C6622aqW {
        private static final long serialVersionUID = 6611059777489110048L;

        C4431c(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            HelpAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$d */
    /* compiled from: a */
    class C4432d extends C3428rT<C5783aaP> {
        C4432d() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (HelpAddon.this.f9894nM != null && HelpAddon.this.f9894nM.isVisible()) {
                HelpAddon.this.f9894nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$g */
    /* compiled from: a */
    class C4436g extends C3428rT<C3689to> {
        C4436g() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (HelpAddon.this.f9894nM != null && HelpAddon.this.f9894nM.isVisible()) {
                HelpAddon.this.f9894nM.setVisible(false);
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$h */
    /* compiled from: a */
    class C4437h extends ComponentAdapter {
        C4437h() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C6245ajJ aVU = HelpAddon.this.f9893kj.aVU();
            aVU.mo13964a(HelpAddon.this.aoN);
            aVU.mo13964a(HelpAddon.this.f9895nP);
            aVU.mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            HelpAddon.this.f9894nM.removeComponentListener(HelpAddon.this.dDE);
            HelpAddon.this.f9894nM = null;
            C4439j.fKs.clear();
            HelpAddon.this.dDD = null;
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$e */
    /* compiled from: a */
    class C4433e implements Repeater.C3671a<C4439j> {
        C4433e() {
        }

        /* renamed from: a */
        public void mo843a(C4439j jVar, Component component) {
            if (component instanceof JToggleButton) {
                JToggleButton jToggleButton = (JToggleButton) component;
                jToggleButton.setText(jVar.title);
                jToggleButton.addActionListener(new C4434a(jVar));
            }
        }

        /* renamed from: taikodom.addon.help.HelpAddon$e$a */
        class C4434a implements ActionListener {
            private final /* synthetic */ C4439j fWn;

            C4434a(C4439j jVar) {
                this.fWn = jVar;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (((JToggleButton) actionEvent.getSource()).isSelected()) {
                    HelpAddon.this.m43601lT(this.fWn.index - 1);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$f */
    /* compiled from: a */
    class C4435f implements ActionListener {
        C4435f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (HelpAddon.this.dDA - 1 < 0) {
                HelpAddon.this.m43601lT(C4439j.fKs.size() - 1);
            } else {
                HelpAddon.this.m43601lT(HelpAddon.this.dDA - 1);
            }
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$b */
    /* compiled from: a */
    class C4430b implements ActionListener {
        C4430b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (HelpAddon.this.dDA + 1 >= C4439j.fKs.size()) {
                HelpAddon.this.m43601lT(0);
            } else {
                HelpAddon.this.m43601lT(HelpAddon.this.dDA + 1);
            }
        }
    }

    /* renamed from: taikodom.addon.help.HelpAddon$a */
    class C4429a implements C2084bq {
        C4429a() {
        }

        /* renamed from: a */
        public void mo17466a(XmlNode xmlNode) {
            String attribute = xmlNode.getAttribute("title");
            if (attribute != null) {
                xmlNode.addAttribute("title", HelpAddon.this.f9893kj.mo11834a((Class<?>) HelpAddon.class, attribute, new Object[0]));
            }
        }
    }
}
