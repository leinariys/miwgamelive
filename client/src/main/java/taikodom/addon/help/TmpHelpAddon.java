package taikodom.addon.help;

import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.aaa.aNS;
import logic.ui.item.EditorPane;
import logic.ui.item.InternalFrame;
import p001a.C2602hR;
import p001a.C3428rT;
import p001a.C5673aSj;
import p001a.C6622aqW;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.infra.script.I18NString;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Stack;

@TaikodomAddon("taikodom.addon.help")
/* compiled from: a */
public class TmpHelpAddon implements C2495fo {
    /* access modifiers changed from: private */
    public EditorPane fVm;
    /* renamed from: kj */
    public IAddonProperties f9896kj;
    /* renamed from: nM */
    public InternalFrame f9897nM;
    private C3428rT<C5783aaP> aoN;
    private C6622aqW atn;
    private Stack<String> fVn;
    /* access modifiers changed from: private */
    private String fVo;
    /* access modifiers changed from: private */
    private C3428rT<aNS> fVp;
    /* renamed from: nP */
    private C3428rT<C3689to> f9898nP;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9896kj = addonPropertie;
        this.aoN = new C4448h();
        this.f9898nP = new C4443c();
        this.fVp = new C4442b();
        this.f9896kj.aVU().mo13965a(aNS.class, this.fVp);
        this.atn = new C4445e("help", this.f9896kj.translate("Help"), "help");
        this.atn.mo15602jH(this.f9896kj.translate("This is the help window, use it whenever you are lost."));
        this.atn.mo15603jI("HELP_WINDOW");
        this.f9896kj.aVU().publish(new C5344aFs(this.atn, 909, true));
        this.f9896kj.mo2317P(this);
    }

    public void open() {
        C6245ajJ aVU = this.f9896kj.aVU();
        if (this.f9897nM == null) {
            this.f9897nM = this.f9896kj.bHv().mo16792bN("tmp-help.xml");
        } else if (this.f9897nM.isVisible()) {
            this.f9897nM.setVisible(false);
            return;
        }
        m43618iM();
        aVU.mo13965a(C3689to.class, this.f9898nP);
        aVU.mo13965a(C5783aaP.class, this.aoN);
        this.f9897nM.center();
        this.f9897nM.setVisible(true);
    }

    /* renamed from: iM */
    private void m43618iM() {
        this.fVn = new Stack<>();
        this.fVm = this.f9897nM.mo4915cd("help-msg");
        this.fVm.addHyperlinkListener(new C4444d());
        this.f9897nM.mo4913cb("index").addActionListener(new C4447g());
        this.f9897nM.mo4913cb("back").addActionListener(new C4446f());
        chI();
    }

    /* access modifiers changed from: protected */
    public void chH() {
        if (!this.fVn.empty()) {
            mo23889jm(this.fVn.pop());
        }
    }

    /* access modifiers changed from: private */
    public void chI() {
        m43619jj(chJ());
        m43620jl(chJ());
    }

    private String chJ() {
        return "http://www.taikodom.com.br/help_in_game?locale=" + I18NString.getCurrentLocation();
    }

    /* renamed from: jj */
    private void m43619jj(String str) {
        C4440a aVar = new C4440a("Help page", str);
        aVar.setDaemon(true);
        aVar.start();
    }

    /* access modifiers changed from: private */
    public void chK() {
        this.fVm.setCaretPosition(0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: jk */
    public void mo23888jk(String str) {
        mo23889jm(str);
        m43620jl(str);
    }

    /* renamed from: jl */
    private void m43620jl(String str) {
        if (str.startsWith("http://")) {
            if (this.fVo != null) {
                this.fVn.push(this.fVo);
            }
            this.fVo = str;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: jm */
    public void mo23889jm(String str) {
        if (str.startsWith("http://")) {
            m43619jj(str);
        } else {
            this.fVm.scrollToReference(str);
        }
    }

    public void stop() {
        if (this.f9897nM != null) {
            this.f9897nM.destroy();
            this.f9897nM = null;
            this.f9896kj.aVU().mo13964a(this.fVp);
            this.f9896kj.aVU().mo13964a(this.f9898nP);
            this.f9896kj.aVU().mo13964a(this.aoN);
        }
    }

    @C2602hR(mo19255zf = "HELP_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo23886al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$h */
    /* compiled from: a */
    class C4448h extends C3428rT<C5783aaP> {
        C4448h() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (TmpHelpAddon.this.f9897nM != null && TmpHelpAddon.this.f9897nM.isVisible()) {
                TmpHelpAddon.this.f9897nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$c */
    /* compiled from: a */
    class C4443c extends C3428rT<C3689to> {
        C4443c() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (TmpHelpAddon.this.f9897nM != null && TmpHelpAddon.this.f9897nM.isVisible()) {
                TmpHelpAddon.this.f9897nM.setVisible(false);
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$b */
    /* compiled from: a */
    class C4442b extends C3428rT<aNS> {
        C4442b() {
        }

        /* renamed from: a */
        public void mo321b(aNS ans) {
            TmpHelpAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$e */
    /* compiled from: a */
    class C4445e extends C6622aqW {


        C4445e(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return TmpHelpAddon.this.f9897nM != null && TmpHelpAddon.this.f9897nM.isVisible();
        }

        public boolean isEnabled() {
            return TmpHelpAddon.this.f9896kj.getPlayer() != null;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            TmpHelpAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$d */
    /* compiled from: a */
    class C4444d implements HyperlinkListener {
        C4444d() {
        }

        public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
            if (HyperlinkEvent.EventType.ACTIVATED.equals(hyperlinkEvent.getEventType())) {
                TmpHelpAddon.this.mo23888jk(hyperlinkEvent.getDescription());
            }
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$g */
    /* compiled from: a */
    class C4447g implements ActionListener {
        C4447g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            TmpHelpAddon.this.chI();
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$f */
    /* compiled from: a */
    class C4446f implements ActionListener {
        C4446f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            TmpHelpAddon.this.chH();
        }
    }

    /* renamed from: taikodom.addon.help.TmpHelpAddon$a */
    class C4440a extends Thread {
        private final /* synthetic */ String gxM;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4440a(String str, String str2) {
            super(str);
            this.gxM = str2;
        }

        public void run() {
            SwingUtilities.invokeLater(new C4441a(C5673aSj.m18227a(this.gxM, (String) null, new File("data/web-cache/"))));
        }

        /* renamed from: taikodom.addon.help.TmpHelpAddon$a$a */
        class C4441a implements Runnable {
            private final /* synthetic */ String eXf;

            C4441a(String str) {
                this.eXf = str;
            }

            public void run() {
                if (TmpHelpAddon.this.fVm != null) {
                    TmpHelpAddon.this.fVm.setText(this.eXf);
                    TmpHelpAddon.this.chK();
                }
            }
        }
    }
}
