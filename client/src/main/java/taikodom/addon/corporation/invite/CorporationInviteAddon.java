package taikodom.addon.corporation.invite;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C3689to;
import game.script.corporation.CorporationCreationStatus;
import game.script.corporation.CorporationInvitationStatus;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.IAddonSettings;
import logic.aaa.C1606XV;
import logic.aaa.C5783aaP;
import logic.baa.C5473aKr;
import logic.baa.C6200aiQ;
import logic.data.link.C0511HG;
import logic.data.link.aAI;
import logic.res.code.C5663aRz;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.item.InternalFrame;
import logic.ui.item.Picture;
import logic.ui.item.Repeater;
import p001a.C1052PQ;
import p001a.C3428rT;
import p001a.C3582se;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.*;

@TaikodomAddon("taikodom.addon.corporation.invite")
/* compiled from: a */
public class CorporationInviteAddon implements C2495fo {
    private static final String aDD = "corporation-invite.xml";
    private static final String hIN = "icon_corp_invite_acepted";
    private static final String hIO = "icon_corp_invite_declined";
    private static final String hIP = "icon_corp_invite_waiting";
    private static final String hsU = "corporation-invite.css";
    /* access modifiers changed from: private */
    public int hIQ;
    /* access modifiers changed from: private */
    public Map<Player, Boolean> hIR;
    /* access modifiers changed from: private */
    public List<Asset> hIS;
    /* access modifiers changed from: private */
    public Map<String, Picture> hIU;
    /* access modifiers changed from: private */
    public int hIV;
    /* access modifiers changed from: private */
    public JButton hIY;
    /* access modifiers changed from: private */
    public JButton hIZ;
    /* access modifiers changed from: private */
    public C6200aiQ<CorporationInvitationStatus> hJb;
    /* renamed from: kj */
    public IAddonProperties f9850kj;
    /* renamed from: nM */
    public InternalFrame f9851nM;
    private C3428rT<C5783aaP> aoN;
    private Picture hIT;
    private JButton hIW;
    private JButton hIX;
    private Repeater<CorporationInvitationStatus> hJa;
    private C3428rT<C1606XV> hJc;
    private C5473aKr<CorporationCreationStatus, Object> hJd;
    /* access modifiers changed from: private */
    private ComponentListener hJe;
    /* access modifiers changed from: private */
    private ComponentAdapter hJf;
    /* renamed from: wz */
    private C3428rT<C3689to> f9852wz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9850kj = addonPropertie;
        this.f9850kj.bHv().mo16790b((Object) this, hsU);
        this.f9850kj.mo2317P(this);
        this.hJc = new C4333d();
        this.hJe = new C4338i();
        this.f9850kj.aVU().mo13965a(C1606XV.class, this.hJc);
    }

    public void stop() {
        if (this.f9851nM != null) {
            this.f9851nM.removeComponentListener(this.hJf);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: Im */
    public void m43272Im() {
        this.f9851nM = this.f9850kj.bHv().mo16792bN(aDD);
        this.hIS = this.f9850kj.ala().aJe().mo19009xb().awh();
        this.hIW = this.f9851nM.mo4913cb("button-up");
        this.hIX = this.f9851nM.mo4913cb("button-down");
        this.hJa = this.f9851nM.mo4915cd("invite-status-repeater");
        this.hIY = this.f9851nM.mo4913cb("create-corp-button");
        this.hIY.setEnabled(false);
        this.hIZ = this.f9851nM.mo4913cb("invite-button");
        this.hIZ.setEnabled(false);
        this.hJa = this.f9851nM.mo4915cd("invite-status-repeater");
        this.hIT = this.f9851nM.mo4915cd("corp-picture");
        this.f9851nM.mo4917cf("value-label").setText(String.valueOf(this.f9850kj.translate("T$ ")) + this.f9850kj.ala().aIS().bQe());
        m43294xN(0);
        this.hIQ = 0;
        this.f9851nM.setVisible(true);
        this.hIR = new HashMap();
        this.hIU = new HashMap();
        m43286iG();
        m43271Fa();
        blJ();
    }

    /* access modifiers changed from: private */
    /* renamed from: gq */
    public void m43283gq() {
        if (this.f9851nM != null) {
            this.f9851nM.destroy();
            this.f9851nM = null;
        }
        cYk();
    }

    /* access modifiers changed from: private */
    public void cYk() {
        this.f9850kj.getPlayer().dxA();
        if (this.hIR != null) {
            this.hIR.clear();
        }
        this.hIR = null;
        this.f9850kj.aVU().mo13964a(this.f9852wz);
        this.f9850kj.aVU().mo13964a(this.aoN);
    }

    /* renamed from: iG */
    private void m43286iG() {
        this.hJb = new C4339j();
        this.hJd = new C4340k();
        this.f9852wz = new C4341l();
        this.hJf = new C4334e();
        this.aoN = new C4335f();
    }

    /* renamed from: Fa */
    private void m43271Fa() {
        this.f9851nM.addComponentListener(this.hJe);
        this.hIW.addActionListener(new C4336g());
        this.hIX.addActionListener(new C4337h());
        this.hIY.addActionListener(new C4325a());
        this.hIZ.addActionListener(new C4328b());
        this.f9851nM.addComponentListener(this.hJf);
        this.f9850kj.aVU().mo13965a(C3689to.class, this.f9852wz);
        this.f9850kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f9850kj.getPlayer().dxC().mo8319a(aAI.hfk, (C5473aKr<?, ?>) this.hJd);
    }

    private void blJ() {
        this.hJa.mo22250a(new C4331c());
        for (CorporationInvitationStatus G : this.f9850kj.getPlayer().dxC().mo17309ge()) {
            this.hJa.mo22248G(G);
        }
    }

    /* renamed from: xN */
    private void m43294xN(int i) {
        this.hIV = i;
        this.hIT.mo16824a(C5378aHa.CORP_IMAGES, this.hIS.get(i).getHandle().concat("_big"));
    }

    /* access modifiers changed from: private */
    public void cYl() {
        int i;
        int i2 = this.hIV + 1;
        this.hIV = i2;
        if (i2 < this.hIS.size()) {
            i = this.hIV;
        } else {
            i = 0;
        }
        this.hIV = i;
        m43294xN(this.hIV);
    }

    /* access modifiers changed from: private */
    public void cYm() {
        int i;
        int i2 = this.hIV - 1;
        this.hIV = i2;
        if (i2 < 0) {
            i = this.hIS.size() - 1;
        } else {
            i = this.hIV;
        }
        this.hIV = i;
        m43294xN(this.hIV);
    }

    /* access modifiers changed from: private */
    /* renamed from: n */
    public void m43292n(Set<CorporationInvitationStatus> set) {
        this.hIR.clear();
        this.hJa.clear();
        for (CorporationInvitationStatus G : set) {
            this.hJa.mo22248G(G);
        }
        this.hJa.doLayout();
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$d */
    /* compiled from: a */
    class C4333d extends C3428rT<C1606XV> {
        C4333d() {
        }

        /* renamed from: a */
        public void mo321b(C1606XV xv) {
            CorporationInviteAddon.this.m43272Im();
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$i */
    /* compiled from: a */
    class C4338i extends ComponentAdapter {
        C4338i() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            CorporationInviteAddon.this.f9851nM = null;
            CorporationInviteAddon.this.cYk();
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$j */
    /* compiled from: a */
    class C4339j implements C6200aiQ<CorporationInvitationStatus> {
        private static /* synthetic */ int[] gJy;

        C4339j() {
        }

        static /* synthetic */ int[] cAz() {
            int[] iArr = gJy;
            if (iArr == null) {
                iArr = new int[CorporationInvitationStatus.C1962a.values().length];
                try {
                    iArr[CorporationInvitationStatus.C1962a.ACCEPTED.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[CorporationInvitationStatus.C1962a.REFUSED.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[CorporationInvitationStatus.C1962a.UNSENT.ordinal()] = 1;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[CorporationInvitationStatus.C1962a.WAITING.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                gJy = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo1143a(CorporationInvitationStatus aqVar, C5663aRz arz, Object obj) {
            Picture axm = (Picture) CorporationInviteAddon.this.hIU.get(aqVar.mo15539dL().getName());
            switch (cAz()[aqVar.mo15534LM().ordinal()]) {
                case 2:
                    axm.mo16824a(C5378aHa.CORP_INV_IMAGES, CorporationInviteAddon.hIP);
                    return;
                case 3:
                    axm.mo16824a(C5378aHa.CORP_INV_IMAGES, CorporationInviteAddon.hIN);
                    CorporationInviteAddon corporationInviteAddon = CorporationInviteAddon.this;
                    int d = corporationInviteAddon.hIQ + 1;
                    corporationInviteAddon.hIQ = d;
                    if (d >= 5) {
                        CorporationInviteAddon.this.hIY.setEnabled(true);
                        return;
                    }
                    return;
                case 4:
                    axm.mo16824a(C5378aHa.CORP_INV_IMAGES, CorporationInviteAddon.hIO);
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$k */
    /* compiled from: a */
    class C4340k implements C5473aKr<CorporationCreationStatus, Object> {
        C4340k() {
        }

        /* renamed from: a */
        public void mo1102b(CorporationCreationStatus baVar, C5663aRz arz, Object[] objArr) {
            CorporationInviteAddon.this.m43292n(baVar.mo17309ge());
        }

        /* renamed from: b */
        public void mo1101a(CorporationCreationStatus baVar, C5663aRz arz, Object[] objArr) {
            CorporationInviteAddon.this.m43292n(baVar.mo17309ge());
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$l */
    /* compiled from: a */
    class C4341l extends C3428rT<C3689to> {
        C4341l() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (CorporationInviteAddon.this.f9851nM != null && CorporationInviteAddon.this.f9851nM.isVisible()) {
                CorporationInviteAddon.this.m43283gq();
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$e */
    /* compiled from: a */
    class C4334e extends ComponentAdapter {
        C4334e() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            CorporationInviteAddon.this.cYk();
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$f */
    /* compiled from: a */
    class C4335f extends C3428rT<C5783aaP> {
        C4335f() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (aap.bMO().equals(C5783aaP.C1841a.ONI) && CorporationInviteAddon.this.f9851nM != null && CorporationInviteAddon.this.f9851nM.isVisible()) {
                CorporationInviteAddon.this.f9851nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$g */
    /* compiled from: a */
    class C4336g implements ActionListener {
        C4336g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CorporationInviteAddon.this.cYl();
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$h */
    /* compiled from: a */
    class C4337h implements ActionListener {
        C4337h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CorporationInviteAddon.this.cYm();
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$a */
    class C4325a implements ActionListener {
        C4325a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            try {
                ((Player) C3582se.m38985a(CorporationInviteAddon.this.f9850kj.getPlayer(), (C6144ahM<?>) new C4326a())).mo14373d(CorporationInviteAddon.this.f9851nM.mo4920ci("corp-name-textfield").getText(), (Asset) CorporationInviteAddon.this.hIS.get(CorporationInviteAddon.this.hIV));
            } catch (C1052PQ e) {
                e.printStackTrace();
            }
        }

        /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$a$a */
        class C4326a implements C6144ahM {
            private static /* synthetic */ int[] gXk;

            C4326a() {
            }

            static /* synthetic */ int[] cEu() {
                int[] iArr = gXk;
                if (iArr == null) {
                    iArr = new int[C1052PQ.C1053a.values().length];
                    try {
                        iArr[C1052PQ.C1053a.CORPORATION_ALREADY_CREATED.ordinal()] = 4;
                    } catch (NoSuchFieldError e) {
                    }
                    try {
                        iArr[C1052PQ.C1053a.INVALID_NAME.ordinal()] = 1;
                    } catch (NoSuchFieldError e2) {
                    }
                    try {
                        iArr[C1052PQ.C1053a.NAME_IN_USE.ordinal()] = 2;
                    } catch (NoSuchFieldError e3) {
                    }
                    try {
                        iArr[C1052PQ.C1053a.NAME_RESERVED.ordinal()] = 7;
                    } catch (NoSuchFieldError e4) {
                    }
                    try {
                        iArr[C1052PQ.C1053a.NO_ENOUGH_BALANCE.ordinal()] = 3;
                    } catch (NoSuchFieldError e5) {
                    }
                    try {
                        iArr[C1052PQ.C1053a.NO_ENOUGH_MEMBERS.ordinal()] = 5;
                    } catch (NoSuchFieldError e6) {
                    }
                    try {
                        iArr[C1052PQ.C1053a.NO_INVITATION_STATUS.ordinal()] = 6;
                    } catch (NoSuchFieldError e7) {
                    }
                    gXk = iArr;
                }
                return iArr;
            }

            /* renamed from: n */
            public void mo1931n(Object obj) {
                SwingUtilities.invokeLater(new C4327a());
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                String translate;
                if (th instanceof C1052PQ) {
                    C1052PQ pq = (C1052PQ) th;
                    switch (cEu()[pq.bxI().ordinal()]) {
                        case 1:
                            translate = CorporationInviteAddon.this.f9850kj.translate("Invalid name");
                            break;
                        case 2:
                            translate = CorporationInviteAddon.this.f9850kj.translate("This name is already in use. Please choose another one");
                            break;
                        case 3:
                            translate = CorporationInviteAddon.this.f9850kj.translate("You have no enough balance");
                            break;
                        case 4:
                            translate = CorporationInviteAddon.this.f9850kj.translate("Corporation is already created");
                            break;
                        case 5:
                            translate = CorporationInviteAddon.this.f9850kj.translate("Not enough members to create a Corporation");
                            break;
                        case 6:
                            translate = CorporationInviteAddon.this.f9850kj.translate("You are not allowed to create a Corporation now. Please try again later");
                            break;
                        case 7:
                            translate = CorporationInviteAddon.this.f9850kj.translate("This name is reserved. Please choose another one");
                            break;
                        default:
                            throw new IllegalArgumentException("Unknown / unhandled reason code: " + pq.bxI());
                    }
                    CorporationInviteAddon.this.f9850kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, translate, new Object[0]));
                }
            }

            /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$a$a$a */
            class C4327a implements Runnable {
                C4327a() {
                }

                public void run() {
                    CorporationInviteAddon.this.m43283gq();
                }
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$b */
    /* compiled from: a */
    class C4328b implements ActionListener {
        C4328b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ArrayList arrayList = new ArrayList();
            for (Map.Entry entry : CorporationInviteAddon.this.hIR.entrySet()) {
                if (((Boolean) entry.getValue()).booleanValue()) {
                    arrayList.add((Player) entry.getKey());
                }
            }
            ((Player) C3582se.m38985a(CorporationInviteAddon.this.f9850kj.getPlayer(), (C6144ahM<?>) new C4329a())).mo14340T(arrayList);
        }

        /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$b$a */
        class C4329a implements C6144ahM<Boolean> {
            C4329a() {
            }

            /* renamed from: a */
            public void mo1931n(Boolean bool) {
                if (bool.booleanValue()) {
                    SwingUtilities.invokeLater(new C4330a());
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }

            /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$b$a$a */
            class C4330a implements Runnable {
                C4330a() {
                }

                public void run() {
                    CorporationInviteAddon.this.hIZ.setEnabled(false);
                    CorporationInviteAddon.this.hIY.setEnabled(true);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$c */
    /* compiled from: a */
    class C4331c implements Repeater.C3671a<CorporationInvitationStatus> {
        C4331c() {
        }

        /* renamed from: a */
        public void mo843a(CorporationInvitationStatus aqVar, Component component) {
            String str;
            C2698il ilVar = (C2698il) component;
            Player dL = aqVar.mo15539dL();
            JCheckBox cd = ilVar.mo4915cd("invite-checkbox");
            cd.setText(aqVar.mo15539dL().getName());
            cd.addActionListener(new C4332a(cd, dL));
            CorporationInviteAddon.this.hIR.put(dL, Boolean.FALSE);
            if (aqVar.mo15534LM() == CorporationInvitationStatus.C1962a.UNSENT) {
                cd.setEnabled(true);
            } else {
                cd.setEnabled(false);
            }
            Picture cd2 = ilVar.mo4915cd("invitation-status");
            if (aqVar.mo15534LM() == CorporationInvitationStatus.C1962a.ACCEPTED) {
                str = CorporationInviteAddon.hIN;
            } else if (aqVar.mo15534LM() == CorporationInvitationStatus.C1962a.REFUSED) {
                str = CorporationInviteAddon.hIO;
            } else {
                str = CorporationInviteAddon.hIP;
            }
            cd2.mo16824a(C5378aHa.CORP_INV_IMAGES, str);
            CorporationInviteAddon.this.hIU.put(aqVar.mo15539dL().getName(), cd2);
            aqVar.mo8320a(C0511HG.dcD, (C6200aiQ<?>) CorporationInviteAddon.this.hJb);
        }

        /* renamed from: taikodom.addon.corporation.invite.CorporationInviteAddon$c$a */
        class C4332a implements ActionListener {
            private final /* synthetic */ Player agw;
            private final /* synthetic */ JCheckBox fYv;

            C4332a(JCheckBox jCheckBox, Player aku) {
                this.fYv = jCheckBox;
                this.agw = aku;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                boolean z;
                if (this.fYv.isSelected()) {
                    CorporationInviteAddon.this.hIR.put(this.agw, Boolean.TRUE);
                } else {
                    CorporationInviteAddon.this.hIR.put(this.agw, Boolean.FALSE);
                }
                Iterator it = CorporationInviteAddon.this.hIR.values().iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((Boolean) it.next()).booleanValue()) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                CorporationInviteAddon.this.hIZ.setEnabled(z);
            }
        }
    }
}
