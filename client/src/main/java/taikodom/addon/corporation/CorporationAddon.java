package taikodom.addon.corporation;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.network.message.externalizable.aTX;
import game.script.corporation.Corporation;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.aaa.C0502Gy;
import logic.aaa.C5783aaP;
import logic.baa.C6200aiQ;
import logic.data.link.C1092Px;
import logic.res.code.C5663aRz;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.corporation")
/* compiled from: a */
public class CorporationAddon implements C2495fo {
    private static final String dZV = "corporation.window";
    private static final String dZW = "corporation.xml";
    private static final String dZX = "corporation-commands.css";
    private static final String dZY = "roles";
    private static final String dZZ = "infos";
    private static final String eaa = "members";
    /* access modifiers changed from: private */
    public C1023Ov eab;
    /* renamed from: kj */
    public IAddonProperties f9848kj;
    /* renamed from: nM */
    public InternalFrame f9849nM;
    private C6200aiQ<Player> aCE;
    private C3428rT<C5783aaP> aoN;
    private C3428rT<C3689to> dHH;
    private C6622aqW dcE;
    private C1023Ov eac;
    /* access modifiers changed from: private */
    private C1023Ov ead;
    /* access modifiers changed from: private */
    private C3428rT<C0502Gy> eae;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9848kj = addonPropertie;
        this.aoN = new C4322e();
        this.dHH = new C4321d();
        this.eae = new C4320c();
        this.f9848kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f9848kj.aVU().mo13965a(C3689to.class, this.dHH);
        this.f9848kj.aVU().mo13965a(C0502Gy.class, this.eae);
        this.aCE = new C4319b();
        this.dcE = new C4324g("corporation", this.f9848kj.translate("Corporation"), "corporation");
        this.dcE.mo15602jH(this.f9848kj.translate("Check how your corporation comrades are doing, their rank and CEO controls."));
        this.dcE.mo15603jI("CORPORATION_WINDOW");
        this.dcE.putValue("ShortDescription", this.f9848kj.translate("Corporation"));
        this.f9848kj.mo2322a("corp", (Action) this.dcE);
        this.f9848kj.getPlayer().mo8320a(C1092Px.dQN, (C6200aiQ<?>) this.aCE);
        this.f9848kj.aVU().publish(new C5344aFs(this.dcE, 500, true));
        this.f9848kj.aVU().publish(new aTX(this.dcE, 200, dZX, this));
        this.f9848kj.mo2317P(this);
    }

    @C2602hR(mo19255zf = "CORPORATION_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo23764al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: a */
    private void m43247a(Player aku, Corporation aPVar) {
        this.f9849nM = this.f9848kj.bHv().mo16792bN(dZW);
        this.f9849nM.addComponentListener(new C0962O(dZV));
        if (aPVar.mo961QW()) {
            this.f9849nM.mo4911Kk();
            ((Corporation) C3582se.m38985a(aPVar, (C6144ahM<?>) new C4317a(aku, aPVar))).mo10704QU();
            return;
        }
        m43251b(aku, aPVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m43251b(Player aku, Corporation aPVar) {
        this.eab = new C3554sY(this.f9848kj, this.f9849nM.mo4915cd(eaa), aku, aPVar);
        this.eac = new C3198pA(this.f9848kj, this.f9849nM.mo4915cd(dZZ), aku, aPVar);
        this.ead = new C1307TH(this.f9848kj, this.f9849nM.mo4915cd(dZY), aku, aPVar);
        blJ();
        m43245Fa();
        refresh();
    }

    public void stop() {
        if (this.f9848kj.ala() != null) {
            this.f9848kj.ala().getPlayer().mo8326b(C1092Px.dQN, (C6200aiQ<?>) this.aCE);
        }
        if (this.f9849nM != null) {
            removeListeners();
        }
    }

    private void blJ() {
        this.eab.blJ();
        this.eac.blJ();
        this.ead.blJ();
    }

    /* renamed from: Fa */
    private void m43245Fa() {
        this.f9849nM.addComponentListener(new C4323f());
        this.eab.mo4555Fa();
        this.eac.mo4555Fa();
        this.ead.mo4555Fa();
    }

    /* access modifiers changed from: private */
    public void removeListeners() {
        this.eab.removeListeners();
        this.eac.removeListeners();
        this.ead.removeListeners();
    }

    private void refresh() {
        this.eab.refresh();
        this.eac.refresh();
        this.ead.refresh();
        this.f9849nM.doLayout();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0016, code lost:
        r0 = r4.f9848kj.ala().aPC();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void open() {
        /*
            r4 = this;
            r3 = 1
            a.vW r0 = r4.f9848kj
            a.DN r0 = r0.ala()
            if (r0 == 0) goto L_0x0015
            a.vW r0 = r4.f9848kj
            a.DN r0 = r0.ala()
            a.akU r0 = r0.aPC()
            if (r0 != 0) goto L_0x0016
        L_0x0015:
            return
        L_0x0016:
            a.vW r0 = r4.f9848kj
            a.DN r0 = r0.ala()
            a.akU r0 = r0.aPC()
            a.aP r1 = r0.bYd()
            if (r1 == 0) goto L_0x0015
            a.nx r2 = r4.f9849nM
            if (r2 != 0) goto L_0x0033
            r4.m43247a((p001a.C6308akU) r0, (p001a.C1814aP) r1)
            a.nx r0 = r4.f9849nM
            r0.setVisible(r3)
            goto L_0x0015
        L_0x0033:
            a.nx r0 = r4.f9849nM
            boolean r0 = r0.isVisible()
            if (r0 == 0) goto L_0x0042
            a.nx r0 = r4.f9849nM
            r1 = 0
            r0.setVisible(r1)
            goto L_0x0015
        L_0x0042:
            r4.refresh()
            a.nx r0 = r4.f9849nM
            r0.setVisible(r3)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.corporation.CorporationAddon.open():void");
    }

    /* renamed from: taikodom.addon.corporation.CorporationAddon$e */
    /* compiled from: a */
    class C4322e extends C3428rT<C5783aaP> {
        C4322e() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (CorporationAddon.this.f9849nM != null && CorporationAddon.this.f9849nM.isVisible()) {
                CorporationAddon.this.f9849nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.CorporationAddon$d */
    /* compiled from: a */
    class C4321d extends C3428rT<C3689to> {
        C4321d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (CorporationAddon.this.f9849nM != null && CorporationAddon.this.f9849nM.isVisible()) {
                CorporationAddon.this.f9849nM.setVisible(false);
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.CorporationAddon$c */
    /* compiled from: a */
    class C4320c extends C3428rT<C0502Gy> {
        C4320c() {
        }

        /* renamed from: b */
        public void mo321b(C0502Gy gy) {
            if (CorporationAddon.this.f9849nM != null && CorporationAddon.this.f9849nM.isVisible() && CorporationAddon.this.eab != null) {
                CorporationAddon.this.eab.refresh();
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.CorporationAddon$b */
    /* compiled from: a */
    class C4319b implements C6200aiQ<Player> {
        C4319b() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            if (CorporationAddon.this.f9849nM != null) {
                CorporationAddon.this.f9849nM.destroy();
                CorporationAddon.this.f9849nM = null;
                CorporationAddon.this.removeListeners();
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.CorporationAddon$g */
    /* compiled from: a */
    class C4324g extends C6622aqW {


        C4324g(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            CorporationAddon.this.open();
        }

        public boolean isEnabled() {
            return CorporationAddon.this.f9848kj.ala().getPlayer().dxE();
        }

        public boolean isActive() {
            return CorporationAddon.this.f9849nM != null && (CorporationAddon.this.f9849nM.isVisible() || CorporationAddon.this.f9849nM.isAnimating());
        }
    }

    /* renamed from: taikodom.addon.corporation.CorporationAddon$a */
    class C4317a implements C6144ahM<Void> {
        private final /* synthetic */ Corporation aTb;
        private final /* synthetic */ Player agw;

        C4317a(Player aku, Corporation aPVar) {
            this.agw = aku;
            this.aTb = aPVar;
        }

        /* renamed from: a */
        public void mo1931n(Void voidR) {
            SwingUtilities.invokeLater(new C4318a(this.agw, this.aTb));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            CorporationAddon.this.f9849nM.mo4912Kl();
        }

        /* renamed from: taikodom.addon.corporation.CorporationAddon$a$a */
        class C4318a implements Runnable {
            private final /* synthetic */ Corporation aTb;
            private final /* synthetic */ Player agw;

            C4318a(Player aku, Corporation aPVar) {
                this.agw = aku;
                this.aTb = aPVar;
            }

            public void run() {
                CorporationAddon.this.f9849nM.mo4912Kl();
                CorporationAddon.this.m43251b(this.agw, this.aTb);
            }
        }
    }

    /* renamed from: taikodom.addon.corporation.CorporationAddon$f */
    /* compiled from: a */
    class C4323f extends ComponentAdapter {
        C4323f() {
        }

        public void componentShown(ComponentEvent componentEvent) {
            CorporationAddon.this.f9848kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, CorporationAddon.this));
        }

        public void componentHidden(ComponentEvent componentEvent) {
            CorporationAddon.this.f9848kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, CorporationAddon.this));
        }
    }
}
