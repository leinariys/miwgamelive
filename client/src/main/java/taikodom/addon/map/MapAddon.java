package taikodom.addon.map;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.network.message.externalizable.aTX;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.infra.script.I18NString;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;

@TaikodomAddon("taikodom.addon.map")
/* compiled from: a */
public class MapAddon implements C2495fo {
    /* access modifiers changed from: private */
    public int currentIndex;
    /* access modifiers changed from: private */
    public ArrayList<String> gYg;
    /* renamed from: kj */
    public IAddonProperties f10100kj;
    /* renamed from: nM */
    public InternalFrame f10101nM;
    /* access modifiers changed from: private */
    /* renamed from: wz */
    public C3428rT<C3689to> f10102wz;
    /* access modifiers changed from: private */
    private C3428rT<C5783aaP> aoN;
    /* access modifiers changed from: private */
    private C6622aqW dcE;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10100kj = addonPropertie;
        this.dcE = new C4698a("map", this.f10100kj.mo11832V("tooltip", "Map"), "map");
        this.dcE.mo15602jH(this.f10100kj.translate("This is your map, you can check all the gate connections between nodes and stellar systems."));
        this.dcE.mo15603jI("MAP_WINDOW");
        this.aoN = new C4699b();
        this.f10102wz = new C4700c();
        this.f10100kj.mo2322a("map", (Action) this.dcE);
        this.f10100kj.aVU().publish(new C5344aFs(this.dcE, 600, true));
        this.f10100kj.aVU().publish(new aTX(this.dcE, 500, "map-commands.css", this));
        this.f10100kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f10100kj.mo2317P(this);
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f10101nM == null) {
            m44522Im();
        } else if (this.f10101nM.isVisible()) {
            this.f10101nM.setVisible(false);
            this.f10100kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            return;
        }
        cFr();
        cFs().addActionListener(new C4701d());
        cFt().addActionListener(new C4702e());
        this.f10101nM.setSize(600, 500);
        this.f10101nM.setLocation((this.f10100kj.bHv().getScreenWidth() - this.f10101nM.getSize().width) / 2, (this.f10100kj.bHv().getScreenHeight() - this.f10101nM.getSize().height) / 2);
        this.f10101nM.pack();
        this.f10100kj.aVU().mo13965a(C3689to.class, this.f10102wz);
        this.f10101nM.setVisible(true);
        this.f10100kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, getClass()));
    }

    /* access modifiers changed from: private */
    public void cFr() {
        setImage(this.f10100kj.bHv().adz().getImage(this.gYg.get(this.currentIndex)));
        setText(C5956adg.format(this.f10100kj.translate("P. {0}/{1}"), Integer.valueOf(this.currentIndex + 1), Integer.valueOf(this.gYg.size())));
    }

    private void setText(String str) {
        this.f10101nM.mo4917cf("text").setText(str);
    }

    private void setImage(Image image) {
        this.f10101nM.mo4917cf("picture").setIcon(new ImageIcon(image));
        this.f10101nM.mo4917cf("picture").setVerticalTextPosition(0);
        this.f10101nM.mo4917cf("picture").setHorizontalTextPosition(0);
    }

    private JButton cFs() {
        return this.f10101nM.mo4913cb("previousButton");
    }

    private JButton cFt() {
        return this.f10101nM.mo4913cb("nextButton");
    }

    /* renamed from: Im */
    private void m44522Im() {
        this.f10101nM = this.f10100kj.bHv().mo16792bN("map.xml");
        this.f10101nM.addComponentListener(new C0962O("map"));
        this.f10101nM.addComponentListener(new C4703f());
        this.gYg = new ArrayList<>();
        this.currentIndex = 0;
        this.f10101nM.setTitle(this.f10100kj.translate("Map"));
        this.gYg.add("imageset_map_01_" + I18NString.getCurrentLocation() + "/map_hack_01");
        cFt().setText(this.f10100kj.translate("Next"));
        cFs().setText(this.f10100kj.translate("Previous"));
        cFt().setEnabled(false);
        cFs().setEnabled(false);
    }

    public void stop() {
        this.f10100kj.aVU().mo13964a(this.aoN);
    }

    @C2602hR(mo19255zf = "MAP_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24284al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: taikodom.addon.map.MapAddon$a */
    class C4698a extends C6622aqW {
        private static final long serialVersionUID = 3017187601122546126L;

        C4698a(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return MapAddon.this.f10101nM != null && (MapAddon.this.f10101nM.isVisible() || MapAddon.this.f10101nM.isAnimating());
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            MapAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.map.MapAddon$b */
    /* compiled from: a */
    class C4699b extends C3428rT<C5783aaP> {
        C4699b() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (MapAddon.this.f10101nM != null && MapAddon.this.f10101nM.isVisible()) {
                MapAddon.this.f10101nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.map.MapAddon$c */
    /* compiled from: a */
    class C4700c extends C3428rT<C3689to> {
        C4700c() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (MapAddon.this.f10101nM != null && MapAddon.this.f10101nM.isVisible()) {
                MapAddon.this.f10101nM.setVisible(false);
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.map.MapAddon$d */
    /* compiled from: a */
    class C4701d implements ActionListener {
        C4701d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MapAddon mapAddon = MapAddon.this;
            int c = mapAddon.currentIndex - 1;
            mapAddon.currentIndex = c;
            if (c < 0) {
                MapAddon.this.currentIndex = MapAddon.this.gYg.size() - 1;
            }
            MapAddon.this.cFr();
        }
    }

    /* renamed from: taikodom.addon.map.MapAddon$e */
    /* compiled from: a */
    class C4702e implements ActionListener {
        C4702e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MapAddon mapAddon = MapAddon.this;
            int c = mapAddon.currentIndex + 1;
            mapAddon.currentIndex = c;
            if (c >= MapAddon.this.gYg.size()) {
                MapAddon.this.currentIndex = 0;
            }
            MapAddon.this.cFr();
        }
    }

    /* renamed from: taikodom.addon.map.MapAddon$f */
    /* compiled from: a */
    class C4703f extends ComponentAdapter {
        C4703f() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            MapAddon.this.f10100kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            MapAddon.this.f10100kj.aVU().mo13964a(MapAddon.this.f10102wz);
            MapAddon.this.f10101nM = null;
        }
    }
}
