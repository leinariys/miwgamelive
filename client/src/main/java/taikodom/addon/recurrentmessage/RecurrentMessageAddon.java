package taikodom.addon.recurrentmessage;

import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5543aNj;
import game.network.message.externalizable.C6559apL;
import logic.IAddonSettings;
import logic.swing.C0454GJ;
import logic.thred.LogPrinter;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import logic.ui.item.TextArea;
import p001a.C3332qO;
import p001a.C3428rT;
import p001a.C5905ach;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@TaikodomAddon("taikodom.addon.recurrentmessage")
/* compiled from: a */
public class RecurrentMessageAddon implements C2495fo {
    private static final int giM = 10000;
    private static final int giN = 15000;
    private static final LogPrinter log = LogPrinter.setClass(RecurrentMessageAddon.class);
    private static /* synthetic */ int[] dzw = null;
    /* access modifiers changed from: private */
    public Timer fZe;
    /* access modifiers changed from: private */
    public C5543aNj giO;
    /* access modifiers changed from: private */
    public C4955k giP = C4955k.IDLE;
    /* access modifiers changed from: private */
    public TextArea giR;
    /* access modifiers changed from: private */
    public int giS;
    /* access modifiers changed from: private */
    public List<C5543aNj> giV = new ArrayList();
    /* access modifiers changed from: private */
    public C3332qO giW;
    /* access modifiers changed from: private */
    public C0454GJ giX;
    /* access modifiers changed from: private */
    public C5905ach giY;
    /* renamed from: kj */
    public IAddonProperties f10231kj;
    /* renamed from: nM */
    public InternalFrame f10232nM;
    private C0454GJ fvP;
    private C0454GJ fvX;
    private MouseAdapter giQ;
    private C3428rT<C6559apL> giT;
    /* access modifiers changed from: private */
    private C6124ags<C5543aNj> giU;
    /* access modifiers changed from: private */
    private ActionListener giZ;
    /* renamed from: nP */
    private C3428rT<C3689to> f10233nP;

    static /* synthetic */ int[] bgU() {
        int[] iArr = dzw;
        if (iArr == null) {
            iArr = new int[C4955k.values().length];
            try {
                iArr[C4955k.ANIMATING_TEXT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C4955k.HIDE_WINDOW_ANIMATION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C4955k.IDLE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C4955k.SHOW_WINDOW_ANIMATION.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C4955k.WAITING_DISMISS.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[C4955k.WAITING_REMINDER.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            dzw = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m45323b(C5543aNj anj) {
        String str;
        LogPrinter ur = log;
        StringBuilder append = new StringBuilder(">>> ADDED ").append(anj.getHandle());
        if (anj.getHandle() == null || anj.getHandle().isEmpty()) {
            str = " - " + anj.djM().get();
        } else {
            str = "";
        }
        ur.debug(append.append(str).append("\n").toString());
        String handle = anj.getHandle();
        if (handle != null) {
            for (C5543aNj handle2 : this.giV) {
                if (handle.equals(handle2.getHandle())) {
                    return;
                }
            }
        }
        this.giV.add(anj);
        switch (bgU()[this.giP.ordinal()]) {
            case 3:
            case 6:
                this.fZe.stop();
                this.giP = C4955k.SHOW_WINDOW_ANIMATION;
                this.giS = 0;
                this.giO = this.giV.get(0);
                this.giY.mo12679a(this.giZ);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void coc() {
        this.giP = C4955k.IDLE;
        if (this.giV.size() <= 0) {
            return;
        }
        if (this.giS >= this.giV.size()) {
            cof();
            return;
        }
        this.giP = C4955k.SHOW_WINDOW_ANIMATION;
        this.giO = this.giV.get(this.giS);
        this.giY.mo12679a(this.giZ);
    }

    private void bGg() {
        this.giX = new C4944a();
        this.fvP = new C4945b();
        this.fvX = new C4946c();
        this.giZ = new C4948d();
    }

    /* access modifiers changed from: private */
    public void cod() {
        this.giP = C4955k.WAITING_DISMISS;
        this.giY.mo12679a((ActionListener) new C4949e());
    }

    /* renamed from: iG */
    private void m45336iG() {
        this.f10233nP = new C4950f();
        this.giQ = new C4953i();
        this.giU = new C4952h();
        this.giT = new C4951g();
    }

    private void bVR() {
        this.fZe = new Timer(giM, new C4954j());
    }

    /* access modifiers changed from: private */
    public void coe() {
        switch (bgU()[this.giP.ordinal()]) {
            case 1:
                this.giW.stop();
                m45316Ia();
                return;
            case 4:
                this.f10232nM.mo20876fW(false);
                this.giS = this.giV.size();
                cof();
                return;
            case 5:
                this.fZe.stop();
                m45316Ia();
                return;
            case 6:
                this.fZe.stop();
                coc();
                return;
            default:
                return;
        }
    }

    private void cof() {
        this.giP = C4955k.WAITING_REMINDER;
        this.giO = null;
        this.giS = 0;
        this.fZe.setDelay(giN);
        this.fZe.setRepeats(false);
        this.fZe.restart();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m45324b(C6559apL apl) {
        String handle = apl.getHandle();
        log.debug("<<< REMOVED " + handle + "\n");
        if (handle == null || handle.isEmpty()) {
            this.giV.clear();
            this.giS = 0;
            this.giO = null;
            coe();
        }
        ArrayList<C5543aNj> arrayList = new ArrayList<>();
        for (C5543aNj next : this.giV) {
            String handle2 = next.getHandle();
            if (handle2 != null && handle2.equals(handle)) {
                arrayList.add(next);
                if (next == this.giO) {
                    coe();
                }
            }
        }
        for (C5543aNj remove : arrayList) {
            this.giV.remove(remove);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m45328c(C5543aNj anj) {
        Image image;
        if (anj != null) {
            this.giR.setText(anj.djM().get());
            if (!(anj.djL() == null || anj.djL().getFile() == null || (image = this.f10231kj.bHv().adz().getImage(anj.djL().getFile())) == null)) {
                ComponentManager.getCssHolder(this.f10232nM).mo13049Vr().mo369a(image);
            }
            this.giR.setVisible(true);
            this.f10232nM.pack();
            this.f10232nM.setLocation((this.f10231kj.bHv().getScreenWidth() - this.f10232nM.getWidth()) / 2, 194);
            this.giR.setVisible(false);
            m45342xK();
        }
    }

    /* renamed from: xK */
    private void m45342xK() {
        this.f10232nM.mo20868b(true, this.fvX);
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10231kj = addonPropertie;
        this.f10232nM = this.f10231kj.bHv().mo16792bN("recurrentMessage.xml");
        this.f10232nM.setAlwaysOnTop(true);
        this.giR = this.f10232nM.mo4915cd("header");
        m45336iG();
        bGg();
        bVR();
        this.f10232nM.addMouseListener(this.giQ);
        this.giR.addMouseListener(this.giQ);
        this.f10231kj.aVU().mo13965a(C3689to.class, this.f10233nP);
        this.f10231kj.aVU().mo13965a(C5543aNj.class, this.giU);
        this.f10231kj.aVU().mo13965a(C6559apL.class, this.giT);
        this.giY = C5905ach.m20567b((IAddonSettings) this.f10231kj);
        Collection<C6559apL> b = this.f10231kj.aVU().mo13967b(C6559apL.class);
        for (C6559apL handle : b) {
            if (handle.getHandle() == null) {
                return;
            }
        }
        for (C5543aNj b2 : this.f10231kj.aVU().mo13967b(C5543aNj.class)) {
            m45323b(b2);
        }
        for (C6559apL b3 : b) {
            m45324b(b3);
        }
    }

    public void stop() {
        this.f10231kj.aVU().mo13964a(this.giT);
        this.f10231kj.aVU().mo13964a(this.giU);
        this.f10231kj.aVU().mo13964a(this.f10233nP);
    }

    /* access modifiers changed from: private */
    /* renamed from: Ia */
    public void m45316Ia() {
        this.f10232nM.mo20868b(false, this.fvP);
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$k */
    /* compiled from: a */
    private enum C4955k {
        ANIMATING_TEXT,
        HIDE_WINDOW_ANIMATION,
        IDLE,
        SHOW_WINDOW_ANIMATION,
        WAITING_DISMISS,
        WAITING_REMINDER
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$a */
    class C4944a implements C0454GJ {
        C4944a() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            if (C4955k.ANIMATING_TEXT.equals(RecurrentMessageAddon.this.giP)) {
                RecurrentMessageAddon.this.cod();
            }
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$b */
    /* compiled from: a */
    class C4945b implements C0454GJ {
        C4945b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            RecurrentMessageAddon recurrentMessageAddon = RecurrentMessageAddon.this;
            recurrentMessageAddon.giS = recurrentMessageAddon.giS + 1;
            if (RecurrentMessageAddon.this.giO.getHandle() == null || RecurrentMessageAddon.this.giO.getHandle().isEmpty()) {
                RecurrentMessageAddon.this.giV.remove(RecurrentMessageAddon.this.giO);
                RecurrentMessageAddon recurrentMessageAddon2 = RecurrentMessageAddon.this;
                recurrentMessageAddon2.giS = recurrentMessageAddon2.giS - 1;
            }
            RecurrentMessageAddon.this.giO = null;
            RecurrentMessageAddon.this.coc();
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$c */
    /* compiled from: a */
    class C4946c implements C0454GJ {
        C4946c() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            if (RecurrentMessageAddon.this.giY.isActive()) {
                RecurrentMessageAddon.this.coe();
            } else if (C4955k.SHOW_WINDOW_ANIMATION.equals(RecurrentMessageAddon.this.giP)) {
                RecurrentMessageAddon.this.giP = C4955k.ANIMATING_TEXT;
                RecurrentMessageAddon.this.giY.mo12679a((ActionListener) new C4947a());
            } else {
                throw new IllegalStateException();
            }
        }

        /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$c$a */
        class C4947a implements ActionListener {
            C4947a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (RecurrentMessageAddon.this.giW == null) {
                    RecurrentMessageAddon.this.giW = new C3332qO(RecurrentMessageAddon.this.giR, RecurrentMessageAddon.this.giX, RecurrentMessageAddon.this.f10232nM, RecurrentMessageAddon.this.f10231kj);
                } else {
                    RecurrentMessageAddon.this.giW.mo21333a(RecurrentMessageAddon.this.giR, RecurrentMessageAddon.this.giX);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$d */
    /* compiled from: a */
    class C4948d implements ActionListener {
        C4948d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            RecurrentMessageAddon.this.m45328c(RecurrentMessageAddon.this.giO);
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$e */
    /* compiled from: a */
    class C4949e implements ActionListener {
        C4949e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            RecurrentMessageAddon.this.fZe.setDelay(RecurrentMessageAddon.giM);
            RecurrentMessageAddon.this.fZe.setRepeats(false);
            RecurrentMessageAddon.this.fZe.restart();
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$f */
    /* compiled from: a */
    class C4950f extends C3428rT<C3689to> {
        C4950f() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (toVar.adE().equals(C3689to.C3690a.SHIP_LAUNCH) && RecurrentMessageAddon.this.f10232nM != null && RecurrentMessageAddon.this.f10232nM.isVisible()) {
                RecurrentMessageAddon.this.m45316Ia();
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$i */
    /* compiled from: a */
    class C4953i extends MouseAdapter {
        C4953i() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            if (RecurrentMessageAddon.this.giO != null) {
                if (RecurrentMessageAddon.this.giW.isRunning()) {
                    RecurrentMessageAddon.this.giW.stop();
                    RecurrentMessageAddon.this.giW.clear();
                }
                RecurrentMessageAddon.this.m45316Ia();
            }
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$h */
    /* compiled from: a */
    class C4952h extends C6124ags<C5543aNj> {
        C4952h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5543aNj anj) {
            RecurrentMessageAddon.this.m45323b(anj);
            return false;
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$g */
    /* compiled from: a */
    class C4951g extends C3428rT<C6559apL> {
        C4951g() {
        }

        /* renamed from: a */
        public void mo321b(C6559apL apl) {
            RecurrentMessageAddon.this.m45324b(apl);
        }
    }

    /* renamed from: taikodom.addon.recurrentmessage.RecurrentMessageAddon$j */
    /* compiled from: a */
    class C4954j implements ActionListener {
        private static /* synthetic */ int[] dzw;

        C4954j() {
        }

        static /* synthetic */ int[] bgU() {
            int[] iArr = dzw;
            if (iArr == null) {
                iArr = new int[C4955k.values().length];
                try {
                    iArr[C4955k.ANIMATING_TEXT.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C4955k.HIDE_WINDOW_ANIMATION.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C4955k.IDLE.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C4955k.SHOW_WINDOW_ANIMATION.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C4955k.WAITING_DISMISS.ordinal()] = 5;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[C4955k.WAITING_REMINDER.ordinal()] = 6;
                } catch (NoSuchFieldError e6) {
                }
                dzw = iArr;
            }
            return iArr;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            switch (bgU()[RecurrentMessageAddon.this.giP.ordinal()]) {
                case 5:
                    RecurrentMessageAddon.this.giP = C4955k.HIDE_WINDOW_ANIMATION;
                    RecurrentMessageAddon.this.m45316Ia();
                    return;
                case 6:
                    RecurrentMessageAddon.this.coc();
                    return;
                default:
                    return;
            }
        }
    }
}
