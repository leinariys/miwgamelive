package taikodom.addon.input;

import game.engine.C5452aJw;
import game.network.message.externalizable.C5667aSd;
import game.script.PlayerController;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.res.ConfigManager;
import logic.res.ConfigManagerSection;
import logic.res.KeyCodeName;
import logic.ui.ComponentCheck;
import logic.ui.ComponentManager;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@TaikodomAddon("taikodom.addon.input")
/* compiled from: a */
public class CommandTranslatorAddon implements C2495fo {

    /* renamed from: JJ */
    private static final String f9957JJ = "input";
    /* renamed from: JL */
    public int f9959JL = -1;
    /* access modifiers changed from: private */
    /* renamed from: JO */
    public Set<Integer> f9962JO = new HashSet();
    /* renamed from: JP */
    public String f9963JP;
    /* renamed from: JQ */
    public C5212aAq f9964JQ;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public boolean disabled;
    /* access modifiers changed from: private */
    /* renamed from: kj */
    public IAddonProperties f9965kj;
    /* access modifiers changed from: private */
    /* renamed from: JK */
    private ConfigManager f9958JK;
    /* renamed from: JM */
    private Map<String, String> f9960JM = new HashMap();
    /* access modifiers changed from: private */
    /* renamed from: JN */
    private Map<String, String[]> f9961JN = new HashMap();

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9965kj = addonPropertie;
        this.f9959JL = 1;
        this.f9964JQ = new C5212aAq(this.f9965kj);
        this.f9958JK = this.f9965kj.getEngineGame().getConfigManager();
        mo24074pk();
        m44051pl();
        this.disabled = false;
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(this.f9964JQ);
        JRootPane rootPane = this.f9965kj.getEngineGame().getEngineGraphics().aee().getRootPane();
        C4545a aVar = new C4545a();
        if (ComponentManager.getCssHolder(rootPane.getContentPane()) == null) {
            rootPane.getContentPane().updateUI();
        }
        if (ComponentManager.getCssHolder(rootPane.getLayeredPane()) == null) {
            new ComponentManager("layeredPane", rootPane.getLayeredPane());
        }
        ComponentManager.getCssHolder(rootPane.getContentPane()).mo13053a((ComponentCheck) aVar);
        ComponentManager.getCssHolder(rootPane.getLayeredPane()).mo13053a((ComponentCheck) aVar);
        C4547c cVar = new C4547c(rootPane);
        C4546b bVar = new C4546b(rootPane);
        rootPane.addMouseListener(new C4549e(rootPane));
        rootPane.addMouseWheelListener(bVar);
        rootPane.addMouseMotionListener(cVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public void m44050h(Object obj) {
        this.f9965kj.aVU().mo13975h(obj);
    }

    public void stop() {
    }

    /* renamed from: pk */
    public void mo24074pk() {
        this.f9961JN.clear();
        for (aPP app : aPP.values()) {
            ConfigManagerSection yr = null;
            try {
                if (app == aPP.ALL) {
                    yr = this.f9958JK.getSection(f9957JJ);
                } else {
                    yr = this.f9958JK.getSection("input." + app.name());
                }
            } catch (C5452aJw e) {
            }
            if (yr != null) {
                for (Map.Entry next : yr.getOptions().entrySet()) {
                    String str = (String) next.getKey();
                    String[] split = String.valueOf(((ayQ) next.getValue()).getValue()).split("[, \t;:]+");
                    if (this.f9961JN.get(str) != null) {
                        System.err.println("** Key " + str + " is already mapped ***");
                        for (String b : this.f9961JN.get(str)) {
                            split = (String[]) C0550He.m5236b(split, b);
                        }
                    }
                    if (split != null && split.length > 0) {
                        this.f9961JN.put(str, split);
                        int length = split.length;
                        for (int i = 0; i < length; i++) {
                            this.f9965kj.aVU().mo2869a(String.valueOf(split[i]) + "_KEY", (Object) str);
                        }
                    }
                }
            }
        }
    }

    public void enable() {
        this.disabled = false;
    }

    public void disable() {
        this.disabled = true;
    }

    /* renamed from: ae */
    public String mo24070ae(String str) {
        return this.f9960JM.get(str);
    }

    /* renamed from: pl */
    private void m44051pl() {
        this.f9965kj.aVU().mo13965a(C2989mg.class, new C4548d());
        this.f9965kj.aVU().mo13965a(C3010mu.class, new C4551g());
        this.f9965kj.aVU().mo13965a(C0963OA.class, new C4550f());
        this.f9965kj.aVU().mo13965a(C0366Et.class, new C4552h());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44041a(String str, boolean z, C2776jq jqVar) {
        String[] strArr;
        if (str != null && (strArr = this.f9961JN.get(str)) != null && strArr.length > 0) {
            for (String str2 : strArr) {
                this.f9965kj.aVU().mo13972d(str2, new C5667aSd(str2, jqVar, z));
            }
        }
    }

    /* renamed from: af */
    public String mo24071af(String str) {
        for (Map.Entry next : this.f9961JN.entrySet()) {
            String[] strArr = (String[]) next.getValue();
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    if (strArr[i].equals(str)) {
                        String[] split = ((String) next.getKey()).split("_");
                        if (split.length == 1) {
                            return split[0];
                        }
                        String str2 = "";
                        for (int i2 = 0; i2 < split.length; i2++) {
                            str2 = String.valueOf(str2) + split[i2];
                            if (i2 < split.length - 1) {
                                str2 = String.valueOf(str2) + "+";
                            }
                        }
                        return str2;
                    }
                    i++;
                }
            }
        }
        return "";
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$a */
    class C4545a implements ComponentCheck {

        /* renamed from: i */
        public boolean isContains(Component component) {
            PlayerController alb;
            Player dL;
            IAddonProperties a = CommandTranslatorAddon.this.f9965kj;
            if (a == null || (alb = a.alb()) == null || !alb.anX() || (dL = alb.mo22135dL()) == null || dL.bQB()) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$c */
    /* compiled from: a */
    class C4547c extends MouseMotionAdapter {
        private final /* synthetic */ JRootPane czX;
        private int czY;
        private int czZ;

        C4547c(JRootPane jRootPane) {
            this.czX = jRootPane;
        }

        public void mouseDragged(MouseEvent mouseEvent) {
            float width = (float) this.czX.getWidth();
            float height = (float) this.czX.getHeight();
            int x = mouseEvent.getX();
            int y = mouseEvent.getY();
            int i = x - this.czY;
            int i2 = y - this.czZ;
            this.czY = x;
            this.czZ = y;
            CommandTranslatorAddon.this.m44050h(new aIU(((float) x) / width, ((float) y) / height, ((float) i) / width, ((float) i2) / height, x, y, i, i2, C0927Nb.m7694lQ(mouseEvent.getModifiersEx())));
        }

        public void mouseMoved(MouseEvent mouseEvent) {
            float width = (float) this.czX.getWidth();
            float height = (float) this.czX.getHeight();
            int x = mouseEvent.getX();
            int y = mouseEvent.getY();
            int i = x - this.czY;
            int i2 = y - this.czZ;
            this.czY = x;
            this.czZ = y;
            if (((float) x) != width / 2.0f || ((float) y) != height / 2.0f) {
                CommandTranslatorAddon.this.m44050h(new aIU(((float) x) / width, ((float) y) / height, ((float) i) / width, ((float) i2) / height, x, y, i, i2, C0927Nb.m7694lQ(mouseEvent.getModifiersEx())));
            }
        }
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$b */
    /* compiled from: a */
    class C4546b implements MouseWheelListener {
        private final /* synthetic */ JRootPane czX;

        C4546b(JRootPane jRootPane) {
            this.czX = jRootPane;
        }

        public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
            int i;
            float width = (float) this.czX.getWidth();
            float height = (float) this.czX.getHeight();
            if (mouseWheelEvent.getScrollType() == 0) {
                CommandTranslatorAddon commandTranslatorAddon = CommandTranslatorAddon.this;
                if (mouseWheelEvent.getWheelRotation() < 0) {
                    i = 3;
                } else {
                    i = 4;
                }
                commandTranslatorAddon.m44050h(new C3010mu(i, true, ((float) mouseWheelEvent.getX()) / width, ((float) mouseWheelEvent.getY()) / height, mouseWheelEvent.getX(), mouseWheelEvent.getY(), C0927Nb.m7694lQ(mouseWheelEvent.getModifiersEx())));
            }
        }
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$e */
    /* compiled from: a */
    class C4549e extends MouseAdapter {
        private final /* synthetic */ JRootPane czX;

        C4549e(JRootPane jRootPane) {
            this.czX = jRootPane;
        }

        public void mousePressed(MouseEvent mouseEvent) {
            float width = (float) this.czX.getWidth();
            float height = (float) this.czX.getHeight();
            this.czX.requestFocus();
            CommandTranslatorAddon.this.m44050h(new C3010mu(C2756ja.m34042ca(mouseEvent.getButton()), true, ((float) mouseEvent.getX()) / width, ((float) mouseEvent.getY()) / height, mouseEvent.getX(), mouseEvent.getY(), C0927Nb.m7694lQ(mouseEvent.getModifiersEx())));
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            float width = (float) this.czX.getWidth();
            float height = (float) this.czX.getHeight();
            CommandTranslatorAddon.this.m44050h(new C3010mu(C2756ja.m34042ca(mouseEvent.getButton()), false, ((float) mouseEvent.getX()) / width, ((float) mouseEvent.getY()) / height, mouseEvent.getX(), mouseEvent.getY(), C0927Nb.m7694lQ(mouseEvent.getModifiersEx())));
            if (mouseEvent.getModifiersEx() != 0) {
                CommandTranslatorAddon.this.m44050h(new C3010mu(C2756ja.m34042ca(mouseEvent.getButton()), false, ((float) mouseEvent.getX()) / width, ((float) mouseEvent.getY()) / height, mouseEvent.getX(), mouseEvent.getY(), 0));
            }
        }
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$d */
    /* compiled from: a */
    class C4548d extends C6124ags<C2989mg> {
        C4548d() {
        }

        /* renamed from: c */
        public boolean updateInfo(C2989mg mgVar) {
            if (!CommandTranslatorAddon.this.disabled) {
                String ls = KeyCodeName.m6645ls(mgVar.mo20584Nh());
                String ls2 = KeyCodeName.m6645ls(CommandTranslatorAddon.this.f9959JL & (mgVar.getModifiers() | CommandTranslatorAddon.this.f9964JQ.cHM()));
                if (!ls2.equals("UNDEFINED") && !ls2.equals(ls)) {
                    ls = String.valueOf(ls2) + "_" + ls;
                }
                boolean isPressed = mgVar.isPressed();
                if (!isPressed) {
                    CommandTranslatorAddon.this.f9962JO.remove(Integer.valueOf(mgVar.getUnicode()));
                } else if (!CommandTranslatorAddon.this.f9962JO.contains(Integer.valueOf(mgVar.getUnicode()))) {
                    CommandTranslatorAddon.this.f9962JO.add(Integer.valueOf(mgVar.getUnicode()));
                }
                CommandTranslatorAddon.this.m44041a(ls, isPressed, mgVar);
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$g */
    /* compiled from: a */
    class C4551g extends C6124ags<C3010mu> {
        C4551g() {
        }

        /* renamed from: c */
        public boolean updateInfo(C3010mu muVar) {
            if (!CommandTranslatorAddon.this.disabled) {
                String bf = C3517rq.m38646bf(muVar.getButton());
                String ls = KeyCodeName.m6645ls(CommandTranslatorAddon.this.f9959JL & (muVar.getModifiers() | CommandTranslatorAddon.this.f9964JQ.cHM()));
                if (!ls.equals("UNDEFINED")) {
                    bf = String.valueOf(ls) + "_" + bf;
                }
                CommandTranslatorAddon.this.m44041a(bf, muVar.isPressed(), muVar);
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$f */
    /* compiled from: a */
    class C4550f extends C6124ags<C0963OA> {
        C4550f() {
        }

        /* renamed from: b */
        public boolean updateInfo(C0963OA oa) {
            if (!CommandTranslatorAddon.this.disabled) {
                CommandTranslatorAddon.this.m44041a(C3942xA.m40808bf(oa.getButton()), oa.isPressed(), oa);
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.input.CommandTranslatorAddon$h */
    /* compiled from: a */
    class C4552h extends C6124ags<C0366Et> {
        C4552h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0366Et et) {
            if (!CommandTranslatorAddon.this.disabled) {
                String bf = C1108QK.m8806bf(et.getButton());
                if (!(bf == CommandTranslatorAddon.this.f9963JP || CommandTranslatorAddon.this.f9963JP == "UNDEFINED")) {
                    CommandTranslatorAddon.this.m44041a(CommandTranslatorAddon.this.f9963JP, false, et);
                }
                CommandTranslatorAddon.this.f9963JP = bf;
                if (bf != "UNDEFINED") {
                    CommandTranslatorAddon.this.m44041a(bf, true, et);
                }
            }
            return true;
        }
    }
}
