package taikodom.addon.input;

import com.hoplon.geometry.Vec3f;
import game.engine.C6082agC;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.network.message.externalizable.C6475anf;
import game.script.Actor;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.aaa.C1506WA;
import logic.aaa.C2061bk;
import logic.aaa.C3949xF;
import logic.aaa.C7016azd;
import logic.render.IEngineGraphics;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.loader.provider.ResourceCleaner;

import javax.vecmath.Tuple3d;
import javax.vecmath.Vector3f;

@TaikodomAddon("taikodom.addon.input")
/* compiled from: a */
public class InputAddon implements C5575aOp, C2495fo, C3274pr {

    private final C6124ags<C3949xF> glE = new aUG(this);
    PlayerController glC;
    /* renamed from: P */
    private Player f9966P;
    private boolean glD;
    /* renamed from: kj */
    private IAddonProperties f9967kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9967kj = addonPropertie;
        this.f9967kj.mo2317P(this);
        this.f9966P = this.f9967kj.ala().getPlayer();
        this.glC = this.f9966P.dxc();
        this.f9967kj.aVU().mo13965a(C3949xF.class, this.glE);
    }

    public void stop() {
        this.f9967kj.mo2318Q(this);
        this.f9967kj.aVU().mo13964a(this.glE);
    }

    @C2602hR(mo19255zf = "INTERACT")
    /* renamed from: fp */
    public void mo24108fp(boolean z) {
        if (z) {
            this.f9967kj.alb().cPh();
        }
    }

    @C2602hR(mo19255zf = "SHUTDOWN")
    /* renamed from: fq */
    public void mo24109fq(boolean z) {
        if (this.f9966P.bQx().ahb() || this.glC.cPf().mo22169P()) {
            this.f9967kj.getPlayer().dxc().mo22144ik(false);
        } else if (z) {
            mo24102fN(true);
            mo24116fx(false);
            this.glC.mo22152lJ(0.0f);
            this.f9967kj.getPlayer().mo14348am("shutdown", "");
        }
    }

    @C2602hR(mo19255zf = "ALIGN")
    /* renamed from: fr */
    public void mo24110fr(boolean z) {
        if (z && !this.glC.cPf().mo22169P()) {
            Actor agB = this.f9966P.bQx().agB();
            if ((agB instanceof Pawn) && agB != this.f9966P.bQx()) {
                Pawn avi = (Pawn) agB;
                this.glC.mo22152lJ(avi.aSj());
                this.glC.aYa().cLd().mo2472kQ().bll().mo15242f(avi.cLd().mo2472kQ().bll());
            }
        }
    }

    @C2602hR(mo19255zf = "SHOWTEXTURESSTATES")
    /* renamed from: fs */
    public void mo24111fs(boolean z) {
        if (z) {
            ResourceCleaner.showTexturesStats();
        }
    }

    @C2602hR(mo19255zf = "YAW_LEFT")
    /* renamed from: ft */
    public void mo24112ft(boolean z) {
        int i;
        if (!this.glC.cPf().mo22169P() && !this.glC.anX()) {
            if (z) {
                this.glC.mo22142h(C6483ann.YAW_LEFT);
                this.f9967kj.getPlayer().mo14348am("yaw.left", "");
            } else if (this.glC.cNC() != C6483ann.YAW_LEFT) {
                return;
            }
            mo24102fN(true);
            PlayerController tVar = this.glC;
            if (z) {
                i = -1;
            } else {
                i = 0;
            }
            tVar.mo22151lH((float) i);
        }
    }

    @C2602hR(mo19255zf = "YAW_RIGHT")
    /* renamed from: fu */
    public void mo24113fu(boolean z) {
        int i = 1;
        if (!this.glC.cPf().mo22169P() && !this.glC.anX()) {
            if (z) {
                this.glC.mo22142h(C6483ann.YAW_RIGHT);
                this.f9967kj.getPlayer().mo14348am("yaw.right", "");
            } else if (this.glC.cNC() != C6483ann.YAW_RIGHT) {
                return;
            }
            mo24102fN(true);
            PlayerController tVar = this.glC;
            if (!z) {
                i = 0;
            }
            tVar.mo22151lH((float) i);
        }
    }

    @C2602hR(mo19255zf = "DEACCELERATE")
    /* renamed from: fv */
    public void mo24114fv(boolean z) {
        if (!this.glC.cPf().mo22169P() && !this.glC.cPf().mo22163D()) {
            mo24102fN(true);
            this.glC.mo22140fv(z);
        }
    }

    @C2602hR(mo19255zf = "MICROJUMP")
    /* renamed from: fw */
    public void mo24115fw(boolean z) {
        Actor agB;
        Vec3d d;
        if (z) {
            if ((this.f9967kj.ala().aLS().adq() || this.f9967kj.getPlayer().bOx().cTc().ceo()) && (agB = ((Ship) this.glC.aYa()).agB()) != null) {
                float outerSphereRadius = 500.0f + agB.mo958IL().getOuterSphereRadius() + this.glC.aYa().mo958IL().getOuterSphereRadius();
                if (this.glC.aYa().mo1011bv(agB) < outerSphereRadius) {
                    Vec3d ajr = new Vec3d((Math.random() - 0.5d) * ((double) outerSphereRadius), (Math.random() - 0.5d) * ((double) outerSphereRadius), (Math.random() - 0.5d) * ((double) outerSphereRadius));
                    ajr.normalize();
                    ajr.scale((double) outerSphereRadius);
                    d = agB.getPosition().mo9504d((Tuple3d) ajr);
                } else {
                    Vec3d f = this.glC.aYa().getPosition().mo9517f((Tuple3d) agB.getPosition());
                    f.dfX().mo9487aa((double) outerSphereRadius);
                    d = agB.getPosition().mo9504d((Tuple3d) f);
                }
                Quat4fWrap aoy = new Quat4fWrap(this.glC.aYa().getOrientation());
                aoy.mo15243g(d, agB.getPosition());
                this.glC.aYa().mo978ah(d);
                this.glC.aYa().mo1082j(aoy);
            }
        }
    }

    @C2602hR(mo19255zf = "ACCELERATE")
    /* renamed from: fx */
    public void mo24116fx(boolean z) {
        if (!this.glC.cPf().mo22169P() && !this.glC.cPf().mo22163D()) {
            mo24102fN(true);
            this.glC.mo22141fx(z);
        }
    }

    @C2602hR(mo19255zf = "ROLL_CW")
    /* renamed from: fy */
    public void mo24117fy(boolean z) {
        int i = 1;
        if (!this.glC.cPf().mo22169P() && !this.glC.cNM()) {
            if (z) {
                this.glC.mo22139f(C6483ann.ROLL_CW);
                this.f9967kj.getPlayer().mo14348am("roll.CW", "");
            } else if (this.glC.cNA() != C6483ann.ROLL_CW) {
                return;
            }
            mo24102fN(true);
            PlayerController tVar = this.glC;
            if (!z) {
                i = 0;
            }
            tVar.mo22150lF((float) i);
        }
    }

    @C2602hR(mo19255zf = "ROLL_CCW")
    /* renamed from: fz */
    public void mo24118fz(boolean z) {
        int i;
        if (!this.glC.cPf().mo22169P() && !this.glC.cNM()) {
            if (z) {
                this.glC.mo22139f(C6483ann.ROLL_CCW);
                this.f9967kj.getPlayer().mo14348am("roll.CCW", "");
            } else if (this.glC.cNA() != C6483ann.ROLL_CCW) {
                return;
            }
            mo24102fN(true);
            PlayerController tVar = this.glC;
            if (z) {
                i = -1;
            } else {
                i = 0;
            }
            tVar.mo22150lF((float) i);
        }
    }

    @C2602hR(mo19255zf = "PITCH_UP")
    /* renamed from: fA */
    public void mo24089fA(boolean z) {
        int i = 1;
        if (!this.glC.cPf().mo22169P() && !this.glC.anX()) {
            if (z) {
                this.glC.mo22134d(C6483ann.PITCH_UP);
                this.f9967kj.getPlayer().mo14348am("pitch.up", "");
            } else if (this.glC.cNw() != C6483ann.PITCH_UP) {
                return;
            }
            mo24102fN(true);
            PlayerController tVar = this.glC;
            if (!z) {
                i = 0;
            }
            tVar.mo22149lD((float) i);
        }
    }

    @C2602hR(mo19255zf = "PITCH_DOWN")
    /* renamed from: fB */
    public void mo24090fB(boolean z) {
        int i;
        if (!this.glC.cPf().mo22169P() && !this.glC.anX()) {
            if (z) {
                this.glC.mo22134d(C6483ann.PITCH_DOWN);
                this.f9967kj.getPlayer().mo14348am("pitch.down", "");
            } else if (this.glC.cNw() != C6483ann.PITCH_DOWN) {
                return;
            }
            mo24102fN(true);
            PlayerController tVar = this.glC;
            if (z) {
                i = -1;
            } else {
                i = 0;
            }
            tVar.mo22149lD((float) i);
        }
    }

    @C2602hR(mo19255zf = "ZOOM_IN")
    /* renamed from: fC */
    public void mo24091fC(boolean z) {
        if (z && this.glC.cNM()) {
            this.f9966P.mo14419f((C1506WA) new C2061bk(1.0f));
            this.f9966P.mo14348am("orbitalCamera.zoom", "in");
        }
    }

    @C2602hR(mo19255zf = "ZOOM_OUT")
    /* renamed from: fD */
    public void mo24092fD(boolean z) {
        if (z && this.glC.cNM()) {
            this.f9966P.mo14419f((C1506WA) new C2061bk(-1.0f));
            this.f9966P.mo14348am("orbitalCamera.zoom", "out");
        }
    }

    @C2602hR(mo19255zf = "SHOW_RENDER_INFO")
    /* renamed from: fE */
    public void mo24093fE(boolean z) {
        if (z) {
            this.f9967kj.aVU().mo13975h(new C7016azd());
        }
    }

    @C2602hR(mo19255zf = "ACTIVATE_MOUSE")
    /* renamed from: fF */
    public void mo24094fF(boolean z) {
        if (!this.f9967kj.getPlayer().bQB()) {
            this.glC.cOW();
            this.glD = !this.glC.anX();
        }
    }

    @C2602hR(mo19255zf = "TOGGLE_MOUSE")
    /* renamed from: fG */
    public void mo24095fG(boolean z) {
        if (!this.f9967kj.getPlayer().bQB() && z && this.glC != null) {
            cpo();
        }
    }

    /* renamed from: fH */
    public void mo24096fH(boolean z) {
        if (!this.f9967kj.getPlayer().bQB() && this.glC != null) {
            cpo();
        }
    }

    private void cpo() {
        this.glC.cOW();
        this.glD = !this.glC.anX();
    }

    @C2602hR(mo19255zf = "CAMERA_ORBITAL")
    @ClientOnly
    /* renamed from: fI */
    public void mo24097fI(boolean z) {
        boolean z2 = true;
        if (this.f9966P.bQx() != null && !this.f9966P.bQx().isDead() && z && this.f9966P.cXm() != aMU.CREATED_PLAYER && this.f9966P.cXm() != aMU.SEEN_ONI_INITIALIZATION && this.f9966P.cXm() != aMU.CREATED_AVATAR) {
            cpp();
            this.glC.cOa();
            this.glC.cOK();
            if (this.glC.cNM()) {
                C6082agC.bWV().aRS();
                if (!this.f9966P.bQB()) {
                    if (!this.f9966P.dxc().anX()) {
                        C6082agC.bWV().aRR();
                    }
                    if (this.glC.anX()) {
                        z2 = false;
                    }
                    this.glD = z2;
                }
            } else if (this.f9967kj.getPlayer().bQB()) {
                C6082agC.bWV().aRR();
            } else if (this.glC.anX()) {
                C6082agC.bWV().aRT();
                mo24102fN(true);
            } else {
                C6082agC.bWV().aRR();
            }
        }
    }

    @C2602hR(mo19255zf = "CHANGE_CAMERA")
    /* renamed from: fJ */
    public void mo24098fJ(boolean z) {
        if (!this.glC.cPf().mo22169P() && z && this.glC.anX() && !this.glC.cNM()) {
            this.f9967kj.aVU().mo13975h(new C6709asF());
        }
    }

    @C2602hR(mo19255zf = "AUTO_PILOT")
    /* renamed from: fK */
    public void mo24099fK(boolean z) {
        if (!this.f9966P.bQB() && z && this.glC.mo22061al().agB() != null) {
            if (this.glC.anX()) {
                cpo();
            }
            this.glC.mo22137e(this.glC.mo22061al().agB(), false);
        }
    }

    @C2602hR(mo19255zf = "ORBIT_AUTO_PILOT")
    /* renamed from: fL */
    public void mo24100fL(boolean z) {
        if (!this.f9966P.bQB() && z && !this.glC.anX() && this.f9967kj.getPlayer().bQx().agB() != null) {
            this.glC.mo22077bV(this.glC.mo22061al().agB());
        }
    }

    @C2602hR(mo19255zf = "INTERACT_AUTO_PILOT")
    /* renamed from: fM */
    public void mo24101fM(boolean z) {
        if (!this.f9966P.bQB() && z && this.f9967kj.getPlayer().bQx().agB() != null) {
            if (this.glC.anX()) {
                mo24108fp(z);
                return;
            }
            this.glC.mo22137e(this.glC.mo22061al().agB(), true);
        }
    }

    @C2602hR(mo19255zf = "KILL_AUTO_PILOT")
    /* renamed from: fN */
    public void mo24102fN(boolean z) {
        if (z) {
            this.glC.cPd();
        }
    }

    @C2602hR(mo19255zf = "MOVE_TO")
    /* renamed from: fO */
    public void mo24103fO(boolean z) {
        if (!z || this.glC.cPf().mo22168N() || this.glC.anX()) {
        }
    }

    /* renamed from: c */
    public void mo21230c(aIU aiu) {
        if (this.glC.cNM() || (this.f9966P.bQx() != null && this.f9966P.bQx().isDead())) {
            if (!this.glD || this.f9966P.bQB()) {
                mo24087d(aiu);
            }
        } else if (this.glC.anX()) {
            mo24084a(aiu);
        }
    }

    /* renamed from: b */
    public void mo10677b(C6009aeh aeh) {
        if (this.glC.cNM() || (this.f9966P.bQx() != null && this.f9966P.bQx().isDead())) {
            mo24088d(aeh);
        } else if (this.glC.anX()) {
            mo24085c(aeh);
        }
    }

    /* renamed from: a */
    public void mo24084a(aIU aiu) {
        boolean z;
        boolean z2;
        if (!this.f9966P.bQB() && !this.f9967kj.bHv().adB()) {
            int screenHeight = (this.f9967kj.bHv().getScreenHeight() / 2) - 60;
            int i = screenHeight - 30;
            Vec3f vec3f = new Vec3f();
            int screenWidth = this.f9967kj.bHv().getScreenWidth() / 2;
            int screenHeight2 = this.f9967kj.bHv().getScreenHeight() / 2;
            float screenX = (float) (aiu.getScreenX() - screenWidth);
            float screenY = (float) (aiu.getScreenY() - screenHeight2);
            Vector2fWrap aka = new Vector2fWrap(screenX, screenY);
            float length = aka.length();
            if (length > 30.0f) {
                Vector2fWrap aka2 = new Vector2fWrap(screenX, screenY);
                if (length >= ((float) screenHeight)) {
                    aka.normalize();
                    aka.scale((float) screenHeight);
                    aka2.normalize();
                    aka2.scale((float) i);
                    z2 = true;
                } else {
                    aka2.normalize();
                    aka2.scale(length - 30.0f);
                    z2 = false;
                }
                float f = aka2.x / ((float) i);
                this.glC.mo22149lD((-aka2.y) / ((float) i));
                this.glC.mo22151lH(f);
                z = z2;
            } else {
                this.glC.mo22149lD(0.0f);
                this.glC.mo22151lH(0.0f);
                z = false;
            }
            Vec3f vec3f2 = new Vec3f();
            vec3f2.x = aka.x + ((float) screenWidth);
            vec3f2.y = ((float) screenHeight2) - aka.y;
            vec3f2.z = 1.0f;
            Vec3d screenPosToWorldPos = this.f9967kj.ale().aea().getScreenPosToWorldPos(vec3f2);
            Ship bQx = this.f9967kj.getPlayer().bQx();
            screenPosToWorldPos.sub(bQx.cLG().position);
            screenPosToWorldPos.normalize();
            vec3f.set(screenPosToWorldPos);
            bQx.cLG().orientation.mo13946a((Vector3f) vec3f);
            if (this.glC.aYa() instanceof Ship) {
                ((Ship) this.glC.aYa()).mo18254E(vec3f);
            }
            int i2 = ((int) aka.x) + screenWidth;
            int i3 = ((int) aka.y) + screenHeight2;
            this.f9967kj.bHv().mo16803s(i2, i3);
            IEngineGraphics ale = this.glC.ald().ale();
            if (ale != null && z) {
                ale.mo3002a(i2, i3, false);
            }
        }
    }

    public void cpp() {
        IEngineGraphics ale = this.f9967kj.ale();
        int screenWidth = this.f9967kj.bHv().getScreenWidth() / 2;
        int screenHeight = this.f9967kj.bHv().getScreenHeight() / 2;
        if (ale != null) {
            ale.mo3002a(screenWidth, screenHeight, true);
            Ship bQx = this.f9967kj.getPlayer().bQx();
            Vec3f vec3f = new Vec3f(0.0f, 0.0f, -1.0f);
            if (bQx != null) {
                bQx.mo18254E(vec3f);
            }
        }
    }

    /* renamed from: d */
    public void mo24087d(aIU aiu) {
        this.f9967kj.aVU().mo13975h(new C6475anf((float) aiu.getScreenX(), (float) aiu.getScreenY()));
        cpp();
    }

    /* renamed from: c */
    public void mo24085c(C6009aeh aeh) {
        switch (aeh.getAxis()) {
            case 0:
                this.glC.mo22151lH(aeh.getValue());
                return;
            case 1:
                this.glC.mo22149lD(aeh.getValue());
                return;
            case 2:
                this.glC.mo22150lF(aeh.getValue());
                return;
            case 3:
                this.glC.mo22153lL(-aeh.getValue());
                return;
            default:
                return;
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo24088d(p001a.C6009aeh r8) {
        /*
            r7 = this;
            r6 = 1073741824(0x40000000, float:2.0)
            r4 = 4580687790476533049(0x3f91df46a2529d39, double:0.017453292519943295)
            r1 = 0
            a.akU r0 = r7.f9966P
            boolean r0 = r0.bQB()
            if (r0 != 0) goto L_0x0018
            a.t r0 = r7.glC
            boolean r0 = r0.cNM()
            if (r0 != 0) goto L_0x0019
        L_0x0018:
            return
        L_0x0019:
            int r0 = r8.getAxis()
            switch(r0) {
                case 0: goto L_0x0042;
                case 1: goto L_0x0048;
                case 2: goto L_0x004e;
                default: goto L_0x0020;
            }
        L_0x0020:
            r0 = r1
            r2 = r1
        L_0x0022:
            int r3 = (r2 > r1 ? 1 : (r2 == r1 ? 0 : -1))
            if (r3 != 0) goto L_0x002a
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 == 0) goto L_0x0018
        L_0x002a:
            double r2 = (double) r2
            double r2 = r2 * r4
            float r1 = (float) r2
            float r1 = r1 * r6
            double r2 = (double) r0
            double r2 = r2 * r4
            float r0 = (float) r2
            float r0 = r0 * r6
            a.vW r2 = r7.f9967kj
            a.ajJ r2 = r2.aVU()
            a.anf r3 = new a.anf
            r4 = 1
            r3.<init>(r1, r0, r4)
            r2.mo13975h(r3)
            goto L_0x0018
        L_0x0042:
            float r2 = r8.getValue()
            r0 = r1
            goto L_0x0022
        L_0x0048:
            float r0 = r8.getValue()
            r2 = r1
            goto L_0x0022
        L_0x004e:
            a.akU r0 = r7.f9966P
            a.bk r2 = new a.bk
            float r3 = r8.getValue()
            r2.<init>(r3)
            r0.mo14419f((logic.aaa.C1506WA) r2)
            float r0 = r8.getValue()
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x0070
            a.akU r0 = r7.f9966P
            java.lang.String r2 = "orbitalCamera.zoom"
            java.lang.String r3 = "out"
            r0.mo14348am(r2, r3)
            r0 = r1
            r2 = r1
            goto L_0x0022
        L_0x0070:
            a.akU r0 = r7.f9966P
            java.lang.String r2 = "orbitalCamera.zoom"
            java.lang.String r3 = "in"
            r0.mo14348am(r2, r3)
            goto L_0x0020
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.input.InputAddon.mo24088d(a.aeh):void");
    }

    @C2602hR(mo19255zf = "PHYSICS_DEBUG")
    /* renamed from: fP */
    public void mo24104fP(boolean z) {
    }

    @C2602hR(mo19255zf = "TOGGLE_INERTIAL_FLIGHT")
    /* renamed from: fQ */
    public void mo24105fQ(boolean z) {
        Ship bQx = this.f9967kj.ala().getPlayer().bQx();
        if (bQx != null && bQx.bae()) {
            bQx.mo18307bv(z);
        }
    }

    @C2602hR(mo19255zf = "SCREENSHOT")
    /* renamed from: fR */
    public void mo24106fR(boolean z) {
        if (z) {
            this.f9967kj.ale().aez();
        }
    }

    @C2602hR(mo19255zf = "SCREENSHOT_HIGHRES")
    /* renamed from: fS */
    public void mo24107fS(boolean z) {
        if (z) {
            this.f9967kj.ale().aeA();
        }
    }
}
