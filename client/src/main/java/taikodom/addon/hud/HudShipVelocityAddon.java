package taikodom.addon.hud;

import game.network.message.externalizable.C2651i;
import game.network.message.externalizable.C6018aeq;
import game.network.message.externalizable.aEE;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C1278St;
import logic.aaa.C2946lw;
import logic.aaa.C2996mm;
import logic.aaa.C5783aaP;
import logic.ui.Panel;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

@TaikodomAddon("taikodom.addon.hud")
/* compiled from: a */
public class HudShipVelocityAddon implements C2495fo {

    /* renamed from: DV */
    public Panel f9931DV;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public Panel bvl;
    /* access modifiers changed from: private */
    public Panel bvn;
    /* access modifiers changed from: private */
    public JLabel bvo;
    /* access modifiers changed from: private */
    public DecimalFormat bvp;
    /* access modifiers changed from: private */
    public WrapRunnable bvu;
    /* renamed from: kj */
    public IAddonProperties f9932kj;
    /* renamed from: Br */
    private C3428rT<C2651i> f9930Br = new C2923lq(this);
    private Panel bvm;
    private Map<Panel, Float> bvq = new HashMap();
    private Map<Panel, Float> bvr = new HashMap();
    private C3428rT<HudVisibilityAddon.C4534d> bvs;
    private C3428rT<C2946lw> bvt;
    private C3428rT<C2996mm> bvv;
    private C3428rT<C1278St> bvw;
    private C6124ags<C6018aeq> bvx;
    private C3428rT<aEE> bvy;
    /* access modifiers changed from: private */
    private C3428rT<C5783aaP> bvz = new C2924lr(this);

    /* renamed from: a */
    private void m43799a(Panel aco, float f) {
        Float f2 = this.bvr.get(aco);
        if (f2 != null) {
            m43800a(aco, f2.floatValue(), f);
        } else {
            m43800a(aco, 0.0f, f);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public float m43796a(Panel aco) {
        Float f = this.bvq.get(aco);
        if (f != null) {
            return f.floatValue();
        }
        return -1.0f;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m43805b(Panel aco, float f) {
        Float f2 = this.bvq.get(aco);
        if (f2 != null) {
            m43800a(aco, f, f2.floatValue());
        } else {
            m43800a(aco, f, 0.0f);
        }
    }

    /* renamed from: a */
    private void m43800a(Panel aco, float f, float f2) {
        this.bvq.put(aco, Float.valueOf(f2));
        if (this.bvr != null && aco != null) {
            if (this.bvr.get(aco) == null || this.bvr.get(aco).floatValue() != f) {
                this.bvr.put(aco, Float.valueOf(f));
                int height = aco.getHeight();
                int width = aco.getWidth();
                int i = (int) ((((float) width) * f) / f2);
                if (i > width) {
                    i = width;
                } else if (i < 0) {
                    i = 0;
                }
                aco.mo8241d(i - width, 0, width, height);
                aco.repaint(150);
            }
        }
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9932kj = addonPropertie;
        this.bvp = new DecimalFormat("#,###,##0");
        open();
        this.bvs = new C4493g();
        this.bvy = new C4490d();
        this.bvt = new C4489c();
        this.bvv = new C4492f();
        this.bvw = new C4491e();
        this.bvx = new C4488b();
        this.f9932kj.aVU().mo13965a(C6018aeq.class, this.bvx);
        this.f9932kj.aVU().mo13965a(aEE.class, this.bvy);
        this.f9932kj.aVU().mo13965a(C1278St.class, this.bvw);
        this.f9932kj.aVU().mo13965a(C2996mm.class, this.bvv);
        this.f9932kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.bvs);
        this.f9932kj.aVU().mo13965a(C2946lw.class, this.bvt);
    }

    private void open() {
        if (this.f9931DV == null) {
            this.f9931DV = (Panel) this.f9932kj.bHv().mo16794bQ("hudShipVelocity.xml");
            this.bvl = this.f9931DV.mo4915cd("velocityBar");
            this.bvn = this.f9931DV.mo4915cd("overVelocityBar");
            this.bvo = this.f9931DV.mo4917cf("speedLabel");
            this.bvm = this.f9931DV.mo4915cd("desiredVelocityBar");
            this.f9931DV.pack();
            this.f9931DV.doLayout();
            this.f9931DV.setLocation((this.f9932kj.ale().aes() / 2) - (this.f9931DV.getSize().width / 2), ((this.f9932kj.ale().aet() / 2) - (this.f9931DV.getSize().height / 2)) - 210);
        }
        Ship bQx = this.f9932kj.ala().getPlayer().bQx();
        if (bQx != null) {
            mo23972b(bQx);
        }
        this.f9932kj.aVU().mo13965a(C2651i.class, this.f9930Br);
        this.f9932kj.aVU().mo13965a(C5783aaP.class, this.bvz);
        this.f9931DV.pack();
        this.f9931DV.doLayout();
        this.f9931DV.setVisible(bQx != null && bQx.bae());
    }

    public void stop() {
        ajl();
    }

    /* renamed from: b */
    public void mo23972b(Ship fAVar) {
        m43813u(fAVar);
        ajl();
        ajn();
        this.f9931DV.pack();
        this.f9931DV.doLayout();
    }

    /* access modifiers changed from: private */
    /* renamed from: t */
    public void m43812t(Ship fAVar) {
        float ra;
        if (fAVar.ahO() != null) {
            ra = fAVar.ahO().mo2572ra() - m43796a(this.bvl);
        } else {
            ra = fAVar.mo1091ra() - m43796a(this.bvl);
        }
        if (ra > 0.0f) {
            m43799a(this.bvn, ra);
        }
        m43805b(this.bvn, 0.0f);
    }

    /* renamed from: u */
    private void m43813u(Ship fAVar) {
        m43799a(this.bvl, fAVar.mo1091ra());
        m43799a(this.bvm, fAVar.mo1091ra());
        m43799a(this.bvn, fAVar.mo1091ra());
    }

    /* access modifiers changed from: private */
    public void ajl() {
        if (this.bvu != null) {
            this.bvu.cancel();
            this.bvu = null;
        }
    }

    public C6622aqW ajm() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: dO */
    public void mo23973dO(float f) {
        m43805b(this.bvm, Math.abs(f));
    }

    private void ajn() {
        Player aPC;
        Ship bQx;
        if (this.f9932kj.ala() != null && (aPC = this.f9932kj.ala().getPlayer()) != null && (bQx = aPC.bQx()) != null) {
            this.bvu = new C4487a(bQx);
            if (aPC.bQx() != null && aPC.bQx().bae()) {
                this.f9932kj.alf().addTask("speed task", this.bvu, 100);
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipVelocityAddon$g */
    /* compiled from: a */
    class C4493g extends C3428rT<HudVisibilityAddon.C4534d> {
        C4493g() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            if (!HudShipVelocityAddon.this.f9932kj.getPlayer().bQB()) {
                HudShipVelocityAddon.this.f9931DV.setVisible(dVar.isVisible());
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipVelocityAddon$d */
    /* compiled from: a */
    class C4490d extends C3428rT<aEE> {
        C4490d() {
        }

        /* renamed from: a */
        public void mo321b(aEE aee) {
            if (!HudShipVelocityAddon.this.f9932kj.getPlayer().bQB()) {
                HudShipVelocityAddon.this.f9931DV.setVisible(!aee.cXT());
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipVelocityAddon$c */
    /* compiled from: a */
    class C4489c extends C3428rT<C2946lw> {
        C4489c() {
        }

        /* renamed from: a */
        public void mo321b(C2946lw lwVar) {
            HudShipVelocityAddon.this.mo23973dO(lwVar.mo20444LV());
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipVelocityAddon$f */
    /* compiled from: a */
    class C4492f extends C3428rT<C2996mm> {
        C4492f() {
        }

        /* renamed from: a */
        public void mo321b(C2996mm mmVar) {
            HudShipVelocityAddon.this.ajl();
            HudShipVelocityAddon.this.f9931DV.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipVelocityAddon$e */
    /* compiled from: a */
    class C4491e extends C3428rT<C1278St> {
        C4491e() {
        }

        /* renamed from: a */
        public void mo321b(C1278St st) {
            HudShipVelocityAddon.this.m43812t(HudShipVelocityAddon.this.f9932kj.getPlayer().bQx());
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipVelocityAddon$b */
    /* compiled from: a */
    class C4488b extends C6124ags<C6018aeq> {
        C4488b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6018aeq aeq) {
            aeq.getEvent().equals("command");
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipVelocityAddon$a */
    class C4487a extends WrapRunnable {

        /* renamed from: Bn */
        private final /* synthetic */ Ship f9933Bn;

        C4487a(Ship fAVar) {
            this.f9933Bn = fAVar;
        }

        public void run() {
            float length = (-Math.signum(this.f9933Bn.mo1090qZ().z)) * this.f9933Bn.mo1090qZ().length();
            HudShipVelocityAddon.this.bvo.setText(String.valueOf(HudShipVelocityAddon.this.bvp.format((double) length)) + " m/s");
            if (Math.abs(length) > HudShipVelocityAddon.this.m43796a(HudShipVelocityAddon.this.bvl)) {
                float abs = Math.abs(length) - HudShipVelocityAddon.this.m43796a(HudShipVelocityAddon.this.bvl);
                length = HudShipVelocityAddon.this.m43796a(HudShipVelocityAddon.this.bvl);
                HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvn, abs);
            } else {
                HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvn, 0.0f);
            }
            if (length == 0.0f || length == 0.0f) {
                HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvl, 0.0f);
            } else if (length > 0.0f) {
                HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvl, length);
            } else {
                HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvl, length);
            }
            if (this.f9933Bn.ahO() != null && this.f9933Bn.ahO().aXW()) {
                float aSj = (((this.f9933Bn.aSj() / this.f9933Bn.mo1091ra()) * this.f9933Bn.mo1092rb()) * 100.0f) / this.f9933Bn.mo1092rb();
                HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvn, 0.0f);
                if (aSj == 0.0f || aSj == 0.0f) {
                    HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvl, 0.0f);
                    aSj = 0.0f;
                } else if (aSj > 0.0f) {
                    HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvl, aSj);
                } else {
                    HudShipVelocityAddon.this.m43805b(HudShipVelocityAddon.this.bvl, aSj);
                }
                HudShipVelocityAddon.this.bvo.setText(String.valueOf(HudShipVelocityAddon.this.bvp.format((double) aSj)) + "%");
            }
        }
    }
}
