package taikodom.addon.hud;

import com.hoplon.geometry.Vec3f;
import game.engine.IEngineGame;
import game.network.message.externalizable.C2814kV;
import game.network.message.externalizable.C5667aSd;
import game.script.Actor;
import game.script.Character;
import game.script.mission.MissionTrigger;
import game.script.player.Player;
import game.script.ship.Scenery;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.simulation.Space;
import game.script.space.Asteroid;
import game.script.space.Gate;
import game.script.space.Loot;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.*;
import logic.baa.C4068yr;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@TaikodomAddon("taikodom.addon.hud")
/* compiled from: a */
public class HudSelectionInputAddon implements C2495fo {
    private static final float biJ = 150000.0f;
    private static final float biK = 10.0f;
    /* access modifiers changed from: private */
    public boolean allowSelection;
    /* access modifiers changed from: private */
    public boolean biN;
    /* access modifiers changed from: private */
    public Actor biP;
    /* access modifiers changed from: private */
    public Actor biQ;
    /* access modifiers changed from: private */
    public Actor biU;
    /* renamed from: kj */
    public IAddonProperties f9921kj;
    /* renamed from: Bu */
    private C6124ags<C5783aaP> f9920Bu;
    private List<Actor> biL;
    private long biM;
    private int biO;
    private C6124ags<C5893acV> biR;
    private C6124ags<HudVisibilityAddon.C4534d> biS;
    private C6124ags<C2041bS> biT;
    /* access modifiers changed from: private */
    private C6124ags<C2814kV> biV = new C0714KK(this);

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9921kj = addonPropertie;
        abX();
        this.f9921kj.mo2317P(this);
        this.biM = 0;
        this.biR = new C4479d();
        this.f9920Bu = new C4477b();
        this.biS = new C4478c();
        this.biT = new C4480e();
        this.f9921kj.aVU().mo13965a(C2814kV.class, this.biV);
        this.f9921kj.aVU().mo13965a(C5893acV.class, this.biR);
        this.f9921kj.aVU().mo13965a(C5783aaP.class, this.f9920Bu);
        this.f9921kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.biS);
        this.f9921kj.aVU().mo13965a(C2041bS.class, this.biT);
        if (!this.f9921kj.getPlayer().bQB()) {
            this.allowSelection = true;
        }
    }

    @C2602hR(mo19255zf = "NEXT_TARGET")
    /* renamed from: ba */
    private void m43749ba(boolean z) {
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            this.biO = abU();
            if (this.biO >= 0) {
                m43752x(this.biL.get(this.biO));
                this.f9921kj.getPlayer().mo14348am("target.next", "");
            }
        }
    }

    @C2602hR(mo19255zf = "SELECT_ATTACKER")
    /* renamed from: bb */
    public void mo23950bb(boolean z) {
        if (z) {
            m43752x(this.biU);
        }
    }

    @C2602hR(mo19255zf = "SELECT_NEAREST_TARGET")
    /* renamed from: a */
    public void mo23945a(C5667aSd asd) {
        if (!(asd.dud() instanceof C2989mg)) {
            System.out.println("Invalid assign of " + asd.duc());
        } else if (asd.isPressed() && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            if (!this.biL.isEmpty()) {
                Ship al = this.f9921kj.getEngineGame().alb().mo22061al();
                Actor cr = this.biL.get(0);
                Vec3f aB = cr.getPosition().mo9485aB(al.getPosition());
                Vec3f aT = al.getOrientation().mo15209aT(new Vec3f(0.0f, 0.0f, -1.0f));
                aB.normalize();
                aT.normalize();
                float dot = aT.dot(aB);
                Actor cr2 = cr;
                for (int i = 1; i < this.biL.size(); i++) {
                    Actor cr3 = this.biL.get(i);
                    Vec3f aB2 = cr3.getPosition().mo9485aB(al.getPosition());
                    aB2.normalize();
                    float dot2 = aT.dot(aB2);
                    if (dot2 > dot && dot2 > 0.0f) {
                        dot = dot2;
                        cr2 = cr3;
                    }
                }
                m43752x(cr2);
            }
        }
    }

    @C2602hR(mo19255zf = "TARGET_OF_TARGET")
    /* renamed from: bc */
    public void mo23951bc(boolean z) {
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            Actor agB = this.f9921kj.getEngineGame().alb().mo22061al().agB();
            if (agB != null && (agB instanceof Ship)) {
                Ship fAVar = (Ship) agB;
                if (fAVar.agB() != null) {
                    m43752x(fAVar.agB());
                }
            }
        }
    }

    @C2602hR(mo19255zf = "NEXT_THREAT")
    /* renamed from: bd */
    public void mo23952bd(boolean z) {
        boolean z2;
        boolean z3;
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            Ship al = this.f9921kj.getEngineGame().alb().mo22061al();
            for (Actor next : this.biL) {
                if (next != this.biP && (next instanceof Ship)) {
                    Ship fAVar = (Ship) next;
                    Character agj = fAVar.agj();
                    if (al.agj().azN() != agj.azN()) {
                        m43752x(fAVar);
                        return;
                    }
                    Player aku = (Player) al.agj();
                    if (!aku.dxk() || !(agj instanceof Player)) {
                        z2 = false;
                    } else {
                        z2 = aku.dxi().mo2426aV((Player) agj);
                    }
                    if (aku.dxE()) {
                        z3 = aku.bYd().mo10697QA().contains(agj);
                    } else {
                        z3 = false;
                    }
                    if (!z2 && !z3) {
                        m43752x(fAVar);
                        return;
                    }
                }
            }
        }
    }

    @C2602hR(mo19255zf = "NEXT_INTEREST")
    /* renamed from: be */
    public void mo23953be(boolean z) {
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            for (Actor next : this.biL) {
                if ((next instanceof C0286Dh) && next != this.biP) {
                    m43752x(next);
                    return;
                }
            }
        }
    }

    @C2602hR(mo19255zf = "NEXT_LOOT")
    /* renamed from: bf */
    public void mo23954bf(boolean z) {
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            Loot ael = null;
            boolean z2 = false;
            for (Actor next : this.biL) {
                if (next instanceof Loot) {
                    if (ael == null) {
                        ael = (Loot) next;
                    }
                    if (next == this.biP) {
                        z2 = true;
                    } else if (z2) {
                        m43752x(next);
                        return;
                    }
                }
            }
            if (ael != null && this.biP != ael) {
                m43752x(ael);
            }
        }
    }

    @C2602hR(mo19255zf = "NEXT_TURRET")
    /* renamed from: bg */
    public void mo23955bg(boolean z) {
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            Actor agB = this.f9921kj.getEngineGame().alb().mo22061al().agB();
            if (agB != null && (agB instanceof Ship)) {
                Ship fAVar = (Ship) agB;
                if (!fAVar.agF().isEmpty()) {
                    for (Turret jq : fAVar.agF()) {
                        if (jq.isAlive()) {
                            m43752x(jq);
                            return;
                        }
                    }
                }
            }
        }
    }

    @C2602hR(mo19255zf = "PREV_TARGET")
    /* renamed from: bh */
    public void mo23956bh(boolean z) {
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            this.biO = abV();
            if (this.biO >= 0 && this.biO < this.biL.size()) {
                m43752x(this.biL.get(this.biO));
                this.f9921kj.getPlayer().mo14348am("target.previous", "");
            }
        }
    }

    @C2602hR(mo19255zf = "NEAREST_TARGET")
    /* renamed from: bi */
    public void mo23957bi(boolean z) {
        if (z && !this.f9921kj.getPlayer().bQB()) {
            if (!this.biN) {
                abX();
            }
            abW();
            aca();
            m43749ba(true);
            this.f9921kj.getPlayer().mo14348am("target.nearest", "");
        }
    }

    public void stop() {
        this.f9921kj.aVU().mo13968b(C5893acV.class, this.biR);
        this.f9921kj.mo2318Q(this);
    }

    private int abU() {
        if (this.biL.size() == 0) {
            this.biN = false;
            return -1;
        }
        abW();
        if (this.biO >= 0) {
            return (this.biO + 1) % this.biL.size();
        }
        return 0;
    }

    private int abV() {
        if (this.biL.size() == 0) {
            this.biN = false;
            return -1;
        }
        abW();
        if (this.biO <= 0) {
            return this.biL.size() - 1;
        }
        return this.biO - 1;
    }

    /* renamed from: O */
    public boolean mo23944O(Actor cr) {
        return ((cr instanceof C0286Dh) || (cr instanceof Ship) || (cr instanceof MissionTrigger) || (cr instanceof Asteroid)) && !(cr instanceof Turret);
    }

    public void abW() {
        if (((float) (System.currentTimeMillis() - this.biM)) > 10000.0f) {
            abX();
        }
    }

    private void abX() {
        Space Zc;
        if (this.f9921kj.getEngineGame().alb() != null) {
            if (this.biL == null) {
                this.biL = new ArrayList();
            } else {
                this.biL.clear();
            }
            Ship al = this.f9921kj.getEngineGame().alb().mo22061al();
            if (al != null && (Zc = al.mo965Zc()) != null) {
                this.biL = Zc.mo1900c(al.getPosition(), (float) biJ);
                int indexOf = this.biL.indexOf(al);
                if (indexOf != -1) {
                    this.biL.remove(indexOf);
                }
                for (int size = this.biL.size() - 1; size >= 0; size--) {
                    Actor cr = this.biL.get(size);
                    if (!mo23944O(cr) || !al.mo18256T(cr)) {
                        this.biL.remove(size);
                    }
                }
                Collections.sort(this.biL, new C4481f(this, (C4481f) null));
                this.biO = this.biL.indexOf(this.biP);
                this.biM = System.currentTimeMillis();
                this.biN = true;
            }
        }
    }

    public boolean abY() {
        return this.allowSelection || this.f9921kj.alb().cPf().mo22170R() || this.f9921kj.alb().cPf().mo22163D() || this.f9921kj.alb().cPf().mo22169P();
    }

    /* access modifiers changed from: private */
    /* renamed from: x */
    public void m43752x(Actor cr) {
        Ship al;
        Actor cr2;
        Pawn aZS;
        IEngineGame ald = this.f9921kj.getEngineGame();
        if (abY() && (al = ald.mo4089dL().dxc().mo22061al()) != null && al.mo18256T(cr) && cr != al && cr != al.agB()) {
            if (this.biP != null) {
                abZ();
            }
            PlayerController alb = ald.alb();
            if (cr != null) {
                if ((cr instanceof Turret) && (aZS = ((Turret) cr).aZS()) != null) {
                    cr = aZS;
                }
                float bx = al.mo1013bx(cr);
                if (cr.aZY() > 0.0f && cr.aZY() < bx) {
                    alb.mo22135dL().mo14419f((C1506WA) new C2733jJ(this.f9921kj.translate("Out of sensors range"), false, new Object[0]));
                    cr = null;
                }
                String str = "";
                if (cr instanceof Asteroid) {
                    str = "asteroid";
                } else if (cr instanceof Ship) {
                    str = "ship";
                } else if (cr instanceof Station) {
                    str = "station";
                } else if (cr instanceof Gate) {
                    str = "gate";
                }
                this.f9921kj.getPlayer().mo14348am("target.selection", str);
                cr2 = cr;
            } else {
                cr2 = cr;
            }
            this.biP = cr2;
            if (this.biP != null && this.biP.equals(al)) {
                this.biP = null;
            }
            if (!al.bae()) {
                return;
            }
            if (cr2 == null || cr2.bae()) {
                al.mo18260Z(cr2);
                Player aPC = this.f9921kj.getEngineGame().alb().getPlayer();
                aPC.bQG().mo20272ce(this.biP);
                if (cr2 instanceof C4068yr) {
                    aPC.dyl().mo11981e((C4068yr) cr2);
                }
                if ((cr2 instanceof Ship) && (((Ship) cr2).agj() instanceof C4068yr)) {
                    aPC.dyl().mo11981e((C4068yr) ((Ship) cr2).agj());
                }
                if (cr2 instanceof Ship) {
                    aPC.dyl().mo11981e((C4068yr) ((Ship) cr2).agH());
                }
                if (cr2 instanceof Gate) {
                    aPC.dyl().mo11981e((C4068yr) ((Gate) cr2).mo18391rR());
                }
                if (cr2 instanceof Scenery) {
                    aPC.dyl().mo11981e((C4068yr) ((Scenery) cr2).mo5913rR());
                }
            }
        }
    }

    public void abZ() {
        this.biP = null;
        this.f9921kj.getPlayer().mo14419f((C1506WA) new C3551sV(this.biP));
    }

    public void aca() {
        this.biO = -1;
    }

    /* renamed from: taikodom.addon.hud.HudSelectionInputAddon$d */
    /* compiled from: a */
    class C4479d extends C6124ags<C5893acV> {
        C4479d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5893acV acv) {
            HudSelectionInputAddon.this.m43752x(acv.mo12618ha());
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectionInputAddon$b */
    /* compiled from: a */
    class C4477b extends C6124ags<C5783aaP> {
        C4477b() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            if (aap.bMO() == C5783aaP.C1841a.FLYING) {
                HudSelectionInputAddon.this.biN = true;
                HudSelectionInputAddon.this.allowSelection = true;
                HudSelectionInputAddon.this.biQ = null;
            } else {
                HudSelectionInputAddon.this.m43752x((Actor) null);
                HudSelectionInputAddon.this.allowSelection = false;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectionInputAddon$c */
    /* compiled from: a */
    class C4478c extends C6124ags<HudVisibilityAddon.C4534d> {
        C4478c() {
        }

        /* renamed from: b */
        public boolean updateInfo(HudVisibilityAddon.C4534d dVar) {
            if (dVar.isVisible()) {
                HudSelectionInputAddon.this.m43752x(HudSelectionInputAddon.this.biQ);
                return false;
            }
            HudSelectionInputAddon.this.biQ = HudSelectionInputAddon.this.biP;
            HudSelectionInputAddon.this.m43752x((Actor) null);
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectionInputAddon$e */
    /* compiled from: a */
    class C4480e extends C6124ags<C2041bS> {
        C4480e() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2041bS bSVar) {
            HudSelectionInputAddon.this.biN = false;
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectionInputAddon$a */
    public class C4476a extends WrapRunnable {
        public C4476a() {
        }

        public void run() {
            Ship bQx;
            Actor agB;
            if (HudSelectionInputAddon.this.f9921kj.getPlayer() != null && (bQx = HudSelectionInputAddon.this.f9921kj.getPlayer().bQx()) != null && (agB = bQx.agB()) != null && !bQx.mo18256T(agB)) {
                HudSelectionInputAddon.this.m43752x((Actor) null);
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectionInputAddon$f */
    /* compiled from: a */
    private class C4481f<A extends Actor> implements Comparator<Actor> {
        private C4481f() {
        }

        /* synthetic */ C4481f(HudSelectionInputAddon hudSelectionInputAddon, C4481f fVar) {
            this();
        }

        /* renamed from: b */
        public int compare(Actor cr, Actor cr2) {
            Ship bQx;
            Player dL = HudSelectionInputAddon.this.f9921kj.getEngineGame().mo4089dL();
            if (dL == null || (bQx = dL.bQx()) == null) {
                return 0;
            }
            if (bQx.mo1013bx(cr) < bQx.mo1013bx(cr2)) {
                return -1;
            }
            return 1;
        }
    }
}
