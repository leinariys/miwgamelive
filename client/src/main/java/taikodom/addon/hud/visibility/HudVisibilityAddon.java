package taikodom.addon.hud.visibility;

import logic.IAddonSettings;
import logic.aaa.C2996mm;
import logic.aaa.C5783aaP;
import logic.render.GUIModule;
import p001a.C2602hR;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.hud.visibility")
/* compiled from: a */
public class HudVisibilityAddon implements C2495fo {

    /* access modifiers changed from: private */
    public C4534d iew;
    /* renamed from: kj */
    public IAddonProperties f9951kj;
    /* renamed from: Bu */
    private C6124ags<C5783aaP> f9950Bu;
    private C6124ags<C5783aaP> iex;
    /* access modifiers changed from: private */
    private C6124ags<C2996mm> iey;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9951kj = addonPropertie;
        this.iew = new C4534d();
        this.iew.visible = true;
        this.iex = new C4533c();
        this.iey = new C4532b();
        this.f9950Bu = new C4531a();
        this.f9951kj.aVU().mo13965a(C5783aaP.class, this.f9950Bu);
        this.f9951kj.aVU().mo13965a(C2996mm.class, this.iey);
        this.f9951kj.aVU().mo13965a(C5783aaP.class, this.iex);
        this.f9951kj.aVU().publish(this.iew);
        this.f9951kj.mo2317P(this);
    }

    public void stop() {
        this.f9951kj.aVU().mo13964a(this.iey);
        this.f9951kj.aVU().mo13974g(this.iew);
        this.f9951kj.aVU().mo13964a(this.iex);
        this.f9951kj.mo2318Q(this);
    }

    /* renamed from: jH */
    public void mo24031jH(boolean z) {
        if (z && !this.f9951kj.getPlayer().bQB()) {
            this.iew.visible = !this.iew.visible;
            this.f9951kj.aVU().publish(this.iew);
            this.f9951kj.getPlayer().mo14348am("HUD.toogle", String.valueOf(this.iew.visible));
        }
    }

    @C2602hR(mo19255zf = "HIDE_HUD")
    /* renamed from: jI */
    public void mo24032jI(boolean z) {
        boolean z2 = true;
        if (z && !this.f9951kj.getPlayer().bQB()) {
            mo24031jH(true);
            GUIModule bhd = this.f9951kj.getEngineGame().getGuiModule();
            if (this.f9951kj.getEngineGame().getGuiModule().ddJ()) {
                z2 = false;
            }
            bhd.mo2534jA(z2);
        }
    }

    /* renamed from: taikodom.addon.hud.visibility.HudVisibilityAddon$c */
    /* compiled from: a */
    class C4533c extends C6124ags<C5783aaP> {

        /* renamed from: Zn */
        private static /* synthetic */ int[] f9952Zn;

        C4533c() {
        }

        /* renamed from: BK */
        static /* synthetic */ int[] m43993BK() {
            int[] iArr = f9952Zn;
            if (iArr == null) {
                iArr = new int[C5783aaP.C1841a.values().length];
                try {
                    iArr[C5783aaP.C1841a.DOCKED.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5783aaP.C1841a.FLYING.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5783aaP.C1841a.JUMPING.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5783aaP.C1841a.ONI.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                f9952Zn = iArr;
            }
            return iArr;
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            switch (m43993BK()[aap.bMO().ordinal()]) {
                case 1:
                    HudVisibilityAddon.this.iew.visible = true;
                    return false;
                case 2:
                    HudVisibilityAddon.this.f9951kj.aVU().publish(HudVisibilityAddon.this.iew);
                    return false;
                default:
                    return false;
            }
        }
    }

    /* renamed from: taikodom.addon.hud.visibility.HudVisibilityAddon$b */
    /* compiled from: a */
    class C4532b extends C6124ags<C2996mm> {
        C4532b() {
        }

        /* renamed from: b */
        public boolean updateInfo(C2996mm mmVar) {
            HudVisibilityAddon.this.mo24031jH(true);
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.visibility.HudVisibilityAddon$a */
    class C4531a extends C6124ags<C5783aaP> {
        C4531a() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            if (!C5783aaP.C1841a.DOCKED.equals(aap.bMO()) || HudVisibilityAddon.this.f9951kj.getEngineGame().getGuiModule().ddJ()) {
                return false;
            }
            HudVisibilityAddon.this.f9951kj.getEngineGame().getGuiModule().mo2534jA(true);
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.visibility.HudVisibilityAddon$d */
    /* compiled from: a */
    public class C4534d {
        /* access modifiers changed from: private */
        public boolean visible;

        public C4534d() {
        }

        public boolean isVisible() {
            return this.visible;
        }
    }
}
