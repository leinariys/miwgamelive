package taikodom.addon.hud.beacon;

import logic.IAddonSettings;
import logic.aVH;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.mission.BeaconManagerAddon;
import taikodom.render.gui.GHudMark;

import java.util.Collection;

@TaikodomAddon("taikodom.addon.hud.beacon")
/* compiled from: a */
public class HudBeaconOverlayAddon implements C2495fo {
    private aVH<BeaconManagerAddon.C4765a> gml;
    private GHudMark izq;

    /* renamed from: kj */
    private IAddonProperties f9937kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9937kj = addonPropertie;
        this.gml = new C4503a();
        this.f9937kj.aVU().mo13965a(BeaconManagerAddon.C4765a.class, this.gml);
        Collection<BeaconManagerAddon.C4765a> b = this.f9937kj.aVU().mo13967b(BeaconManagerAddon.C4765a.class);
        if (b.size() > 0) {
            this.gml.mo2004k("", (BeaconManagerAddon.C4765a) b.toArray()[0]);
        }
    }

    public void stop() {
        this.f9937kj.aVU().mo13968b(BeaconManagerAddon.C4765a.class, this.gml);
    }

    /* renamed from: taikodom.addon.hud.beacon.HudBeaconOverlayAddon$a */
    class C4503a implements aVH<BeaconManagerAddon.C4765a> {
        C4503a() {
        }

        /* renamed from: a */
        public void mo2004k(String str, BeaconManagerAddon.C4765a aVar) {
        }

        /* renamed from: b */
        public void mo2003f(String str, BeaconManagerAddon.C4765a aVar) {
        }
    }
}
