package taikodom.addon.hud;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.network.message.externalizable.C0813Lk;
import game.script.Actor;
import game.script.citizenship.CitizenshipType;
import game.script.player.Player;
import game.script.ship.Outpost;
import game.script.ship.Scenery;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Gate;
import game.script.space.Loot;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C3551sV;
import logic.aaa.C5783aaP;
import logic.swing.C5378aHa;
import logic.ui.C1276Sr;
import logic.ui.Panel;
import logic.ui.item.Picture;
import logic.ui.item.Progress;
import logic.ui.item.Repeater;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import javax.swing.*;
import java.awt.*;

@TaikodomAddon("taikodom.addon.hud")
/* compiled from: a */
public class HudSelectedTargetAddon implements C2495fo {
    private static final String amI = "hud_target_selection_dock_left";
    private static final String amJ = "hud_target_selection_dock_right";
    private static final String amK = "hud_target_selection_cannon_left";
    private static final String amL = "hud_target_selection_launcher_right";
    /* access modifiers changed from: private */

    /* renamed from: Bo */
    public Progress f9916Bo;
    /* access modifiers changed from: private */

    /* renamed from: Bp */
    public Progress f9917Bp;
    /* access modifiers changed from: private */

    /* renamed from: DV */
    public Panel f9918DV;
    /* access modifiers changed from: private */
    public JLabel amC;
    /* access modifiers changed from: private */
    public Picture amD;
    /* access modifiers changed from: private */
    public Picture amE;
    /* access modifiers changed from: private */
    public Actor amM;
    /* access modifiers changed from: private */
    public boolean amO = false;
    /* access modifiers changed from: private */
    public Actor ams;
    /* access modifiers changed from: private */
    public JLabel amu;
    /* access modifiers changed from: private */
    public JProgressBar amx;
    /* renamed from: kj */
    public IAddonProperties f9919kj;
    C4466a amR = new C4466a(this, (C4466a) null);
    C4467b amS = new C4467b(this, (C4467b) null);
    WrapRunnable amT = new C5691aTb(this);
    int amt;
    int width;
    private C3428rT<HudVisibilityAddon.C4534d> amA;
    private ImageIcon amB;
    private C1276Sr amF;
    private C3428rT<C5296aDw> amG;
    private C3428rT<C5334aFi> amH;
    private Picture amN;
    private Repeater<CitizenshipType> amP;
    private JLabel amQ;
    private C6124ags<C3551sV> amr;
    private JLabel amv;
    private JLabel amw;
    /* access modifiers changed from: private */
    private C6124ags<C5783aaP> amy;
    private C4468c amz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9919kj = addonPropertie;
        this.amr = new C4470d();
        this.width = this.f9919kj.ale().aes();
        this.amt = this.f9919kj.ale().aet();
        this.amy = new C4473g();
        this.amA = new C4474h();
        this.amH = new C4471e();
        this.amG = new C4472f();
        this.f9919kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.amA);
        this.f9919kj.aVU().mo13965a(C3551sV.class, this.amr);
        this.f9919kj.aVU().mo13965a(C5783aaP.class, this.amy);
        this.f9919kj.aVU().mo13965a(C5334aFi.class, this.amH);
        this.f9919kj.aVU().mo13965a(C5296aDw.class, this.amG);
        open();
    }

    /* access modifiers changed from: protected */
    public void hide() {
        this.f9918DV.setVisible(false);
    }

    /* access modifiers changed from: protected */
    public void show() {
        if (!this.amO && this.ams != null) {
            this.f9918DV.setVisible(true);
        }
    }

    private void open() {
        if (this.f9918DV == null) {
            this.f9918DV = (Panel) this.f9919kj.bHv().mo16794bQ("selectedTarget.xml");
            this.f9918DV.setLocation(0, 0);
            this.amC = this.f9918DV.mo4917cf("centerImage");
            this.amB = new ImageIcon();
            this.amB.setImage(this.f9919kj.bHv().adz().getImage(String.valueOf(C5378aHa.HUD.getPath()) + "hud_target_selection_normal"));
            this.amC.setIcon(this.amB);
            this.amC.setVisible(true);
            this.amu = this.f9918DV.mo4917cf("distance");
            this.amu.setVisible(true);
            this.amN = this.f9918DV.mo4915cd("corporationImage");
            this.amN.setVisible(false);
            this.amP = this.f9918DV.mo4915cd("affiliationRepeater");
            this.amP.mo22250a(new C4475i());
            this.amv = this.f9918DV.mo4917cf("name");
            this.amQ = this.f9918DV.mo4917cf("rank");
            this.amz = new C4468c();
            this.f9917Bp = this.f9918DV.mo4918cg("shield");
            this.f9917Bp.setVisible(true);
            this.f9917Bp.setValue(0);
            this.f9916Bo = this.f9918DV.mo4918cg("hull");
            this.f9916Bo.setVisible(true);
            this.f9916Bo.setValue(0);
            this.amD = this.f9918DV.mo4915cd("leftImage");
            this.amE = this.f9918DV.mo4915cd("rightImage");
            this.amF = this.f9918DV.mo4915cd("centerStuff");
            this.amF.setVisible(true);
            this.amD.setVisible(true);
            this.amE.setVisible(true);
            this.f9918DV.setFocusable(false);
            this.f9918DV.setLocation(-1000, -1000);
            this.f9918DV.pack();
        }
        this.f9918DV.setVisible(true);
        this.amw = this.f9918DV.mo4915cd("lockingLabel");
        this.amx = this.f9918DV.mo4918cg("lock-progress");
        mo23926aG("");
    }

    /* renamed from: aG */
    public void mo23926aG(String str) {
        if (!this.amO && this.amw != null) {
            if (str == null || str.length() <= 0) {
                this.amw.setText("");
                this.amw.setVisible(false);
                this.amx.setVisible(false);
            } else {
                this.amw.setText(str);
                if (!this.amw.isVisible()) {
                    this.amw.setVisible(true);
                }
            }
            this.f9918DV.pack();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: x */
    public void m43724x(Actor cr) {
        String aOQ;
        String aOQ2;
        Ship bQx = this.f9919kj.getPlayer().bQx();
        if (cr == null && bQx != null && bQx.agB() != null) {
            m43704Hi();
        } else if (this.f9919kj.getPlayer().bQB() || this.f9918DV == null) {
            m43704Hi();
        } else {
            this.amM = null;
            this.ams = cr;
            if (this.amO) {
                m43704Hi();
                return;
            }
            this.f9918DV.setVisible(false);
            if (this.ams == null) {
                m43704Hi();
                return;
            }
            m43705Hj();
            if (this.ams instanceof Scenery) {
                this.amM = null;
                this.f9918DV.setVisible(false);
                this.amv.setText(this.ams.ahQ());
                this.amD.setIcon((Icon) null);
                this.amE.setIcon((Icon) null);
                this.f9916Bo.setVisible(false);
                this.f9917Bp.setVisible(false);
                mo23926aG("");
                return;
            }
            if (this.ams instanceof aDA) {
                aDA ada = (aDA) this.ams;
                if (ada.mo8288zv() != null) {
                    this.f9917Bp.setVisible(true);
                    this.f9917Bp.mo17398E(ada.mo8288zv().mo19089hh());
                } else {
                    this.f9917Bp.setVisible(false);
                }
                this.f9916Bo.setVisible(true);
                this.f9916Bo.mo17398E(ada.mo8287zt().mo19934hh());
            } else {
                this.f9917Bp.setVisible(false);
                this.f9916Bo.setVisible(false);
            }
            this.amP.clear();
            this.amN.setImage((Image) null);
            this.amN.setVisible(false);
            this.amQ.setText("");
            if (this.ams instanceof Ship) {
                Ship fAVar = (Ship) this.ams;
                this.amQ.setText("[ " + this.f9919kj.translate("Rank") + " " + fAVar.agj().bQO().mo17147lt() + " ]");
                if (fAVar.agj() instanceof Player) {
                    Player aku = (Player) fAVar.agj();
                    if (!(aku.dxy().bFK() == null || (aOQ2 = aku.dxy().bFK().aOQ()) == null || aOQ2.length() <= 0)) {
                        this.amN.mo16824a(C5378aHa.CORP_IMAGES, aOQ2);
                        this.amN.setVisible(true);
                    }
                    if (aku.dxy().bqx().size() > 0) {
                        for (CitizenshipType adl : aku.dxy().bqx()) {
                            if (adl.bRH() != null) {
                                this.amP.mo22248G(adl);
                            }
                        }
                    }
                }
            }
            if (this.ams instanceof Outpost) {
                Outpost qZVar = (Outpost) this.ams;
                if (!(qZVar.bYd() == null || qZVar.bYd().mo10706Qu() == null || (aOQ = qZVar.bYd().mo10706Qu().aOQ()) == null || aOQ.length() <= 0)) {
                    this.amN.mo16824a(C5378aHa.CORP_IMAGES, aOQ);
                    this.amN.setVisible(true);
                }
            }
            this.amv.setText(this.ams.ahQ());
            mo23926aG("");
            this.amD.setIcon((Icon) null);
            this.amE.setIcon((Icon) null);
        }
    }

    /* renamed from: Hi */
    private void m43704Hi() {
        this.amR.cancel();
        this.amS.cancel();
        this.amT.cancel();
    }

    /* renamed from: Hj */
    private void m43705Hj() {
        this.f9919kj.alf().addTask("UpdateTargetPositiontask", this.amS, 50);
        this.f9919kj.alf().addTask("UpdateTargetPositiontask", this.amR, 10);
        this.f9919kj.alf().addTask("UpdateTargetHealthTask", this.amT, 33);
    }

    public void stop() {
        m43704Hi();
        this.f9919kj.aVU().mo13968b(C3551sV.class, this.amr);
        this.amz.destroy();
    }

    /* access modifiers changed from: private */
    /* renamed from: Hk */
    public Vector2fWrap m43706Hk() {
        int i = this.amC.getSize().width / 2;
        int i2 = this.amC.getSize().height / 2;
        Container container = this.amC;
        while (container != this.f9918DV && container != null) {
            i += container.getLocation().x;
            i2 += container.getLocation().y;
            container = container.getParent();
        }
        return new Vector2fWrap((float) i, (float) i2);
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$d */
    /* compiled from: a */
    class C4470d extends C6124ags<C3551sV> {
        C4470d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3551sV sVVar) {
            HudSelectedTargetAddon.this.m43724x(sVVar.acv());
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$g */
    /* compiled from: a */
    class C4473g extends C6124ags<C5783aaP> {
        C4473g() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            HudSelectedTargetAddon.this.f9918DV.setVisible(false);
            if (HudSelectedTargetAddon.this.f9919kj.getPlayer() == null || HudSelectedTargetAddon.this.f9919kj.getPlayer().bQx() == null || HudSelectedTargetAddon.this.f9919kj.getPlayer().bQx().agB() == null) {
                HudSelectedTargetAddon.this.m43724x((Actor) null);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$h */
    /* compiled from: a */
    class C4474h extends C3428rT<HudVisibilityAddon.C4534d> {
        C4474h() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            HudSelectedTargetAddon.this.amO = !dVar.isVisible();
            if (HudSelectedTargetAddon.this.amO) {
                HudSelectedTargetAddon.this.hide();
            } else {
                HudSelectedTargetAddon.this.show();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$e */
    /* compiled from: a */
    class C4471e extends C3428rT<C5334aFi> {
        C4471e() {
        }

        /* renamed from: a */
        public void mo321b(C5334aFi afi) {
            if (afi.cHf()) {
                HudSelectedTargetAddon.this.amD.mo16824a(C5378aHa.HUD, HudSelectedTargetAddon.amK);
            } else {
                HudSelectedTargetAddon.this.amD.setIcon((Icon) null);
            }
            HudSelectedTargetAddon.this.f9918DV.pack();
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$f */
    /* compiled from: a */
    class C4472f extends C3428rT<C5296aDw> {
        C4472f() {
        }

        /* renamed from: a */
        public void mo321b(C5296aDw adw) {
            if (adw.cHf()) {
                HudSelectedTargetAddon.this.amE.mo16824a(C5378aHa.HUD, HudSelectedTargetAddon.amL);
            } else {
                HudSelectedTargetAddon.this.amE.setIcon((Icon) null);
            }
            HudSelectedTargetAddon.this.f9918DV.pack();
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$i */
    /* compiled from: a */
    class C4475i implements Repeater.C3671a<CitizenshipType> {
        C4475i() {
        }

        /* renamed from: a */
        public void mo843a(CitizenshipType adl, Component component) {
            Picture axm = (Picture) component;
            axm.setImage(HudSelectedTargetAddon.this.f9919kj.bHv().adz().getImage(String.valueOf(C5378aHa.CITIZENSHIP.getPath()) + adl.bRH().getHandle()).getScaledInstance(15, 15, 4));
            axm.setToolTipText(adl.mo12809rP().get());
            axm.setSize(10, 10);
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$b */
    /* compiled from: a */
    private class C4467b extends WrapRunnable {
        private C4467b() {
        }

        /* synthetic */ C4467b(HudSelectedTargetAddon hudSelectedTargetAddon, C4467b bVar) {
            this();
        }

        public void updateInfo() {
            String str;
            if (HudSelectedTargetAddon.this.f9919kj == null || HudSelectedTargetAddon.this.f9919kj.getPlayer() == null) {
                System.err.println(String.valueOf(getClass().getSimpleName()) + " > Addon trying to access context when it is not available anymore.");
                return;
            }
            Ship al = HudSelectedTargetAddon.this.f9919kj.getPlayer().dxc().mo22061al();
            if (al != null) {
                float bv = al.mo1011bv(HudSelectedTargetAddon.this.ams);
                if (bv < 10000.0f) {
                    str = String.valueOf(Math.round(bv)) + " m";
                } else {
                    str = String.valueOf(Math.round(((double) bv) / 1000.0d)) + " km";
                }
                HudSelectedTargetAddon.this.amu.setText(str);
                if (!(HudSelectedTargetAddon.this.ams instanceof C0286Dh)) {
                    return;
                }
                if (bv * bv <= PlayerController.m39266c((C0286Dh) HudSelectedTargetAddon.this.ams)) {
                    if (HudSelectedTargetAddon.this.amM != HudSelectedTargetAddon.this.ams) {
                        HudSelectedTargetAddon.this.amM = HudSelectedTargetAddon.this.ams;
                        String af = HudSelectedTargetAddon.this.f9919kj.mo2324af("INTERACT_AUTO_PILOT");
                        if (HudSelectedTargetAddon.this.ams instanceof Station) {
                            HudSelectedTargetAddon.this.mo23926aG(HudSelectedTargetAddon.this.f9919kj.translate("Press [%s] to dock", af));
                        } else if (HudSelectedTargetAddon.this.ams instanceof Gate) {
                            HudSelectedTargetAddon.this.mo23926aG(HudSelectedTargetAddon.this.f9919kj.translate("Press [%s] to jump", af));
                        } else if (HudSelectedTargetAddon.this.ams instanceof Loot) {
                            HudSelectedTargetAddon.this.mo23926aG(HudSelectedTargetAddon.this.f9919kj.translate("Press [%s] to loot", af));
                        } else {
                            HudSelectedTargetAddon.this.mo23926aG(HudSelectedTargetAddon.this.f9919kj.translate("Press [%s] to interact", af));
                        }
                        HudSelectedTargetAddon.this.amD.mo16824a(C5378aHa.HUD, HudSelectedTargetAddon.amI);
                        HudSelectedTargetAddon.this.amE.mo16824a(C5378aHa.HUD, HudSelectedTargetAddon.amJ);
                        HudSelectedTargetAddon.this.f9918DV.pack();
                    }
                } else if (HudSelectedTargetAddon.this.amM != null) {
                    HudSelectedTargetAddon.this.amM = null;
                    HudSelectedTargetAddon.this.amD.setIcon((Icon) null);
                    HudSelectedTargetAddon.this.amE.setIcon((Icon) null);
                    HudSelectedTargetAddon.this.mo23926aG("");
                    HudSelectedTargetAddon.this.f9918DV.pack();
                }
            }
        }

        public void run() {
            if (!HudSelectedTargetAddon.this.amO && HudSelectedTargetAddon.this.ams != null && HudSelectedTargetAddon.this.ams.cHg() != null && HudSelectedTargetAddon.this.amC != null && HudSelectedTargetAddon.this.f9919kj.ale().adY() != null) {
                updateInfo();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$a */
    private class C4466a extends WrapRunnable {
        private C4466a() {
        }

        /* synthetic */ C4466a(HudSelectedTargetAddon hudSelectedTargetAddon, C4466a aVar) {
            this();
        }

        public void run() {
            boolean z;
            if (!HudSelectedTargetAddon.this.amO && HudSelectedTargetAddon.this.ams != null && HudSelectedTargetAddon.this.ams.cHg() != null && HudSelectedTargetAddon.this.amC != null && HudSelectedTargetAddon.this.f9919kj.ale().adY() != null) {
                Vec3d ajr = new Vec3d();
                C6017aep cMI = HudSelectedTargetAddon.this.ams.cMI();
                if (HudSelectedTargetAddon.this.ams.cMI() == null || HudSelectedTargetAddon.this.ams.cMI().bUs() == null || HudSelectedTargetAddon.this.ams.cMI().bUs().getReferenceObject() != null) {
                    if (cMI == null || !cMI.bUq() || !cMI.isVisible() || !cMI.isValid() || cMI.isOffScreen() || cMI.bUs().getMark() == null) {
                        HudSelectedTargetAddon.this.f9919kj.ale().aea().getWindowCoords(HudSelectedTargetAddon.this.ams.cHg().getPosition(), ajr);
                        if (ajr.x < ScriptRuntime.NaN || ajr.y < ScriptRuntime.NaN || ajr.z > 1.0d || ajr.x > ((double) HudSelectedTargetAddon.this.f9919kj.ale().aes()) || ajr.y > ((double) HudSelectedTargetAddon.this.f9919kj.ale().aet())) {
                            ajr.mo9532q((double) (((float) HudSelectedTargetAddon.this.f9919kj.ale().aes()) / 2.0f), (double) (HudSelectedTargetAddon.this.f9919kj.ale().aet() / 2), ScriptRuntime.NaN);
                            double d = ajr.z;
                            ajr.z = ScriptRuntime.NaN;
                            ajr.normalize();
                            ajr.scale((double) (d > 1.0d ? -250 : 250));
                            ajr.z = d;
                            ajr.mo9475E(((float) HudSelectedTargetAddon.this.f9919kj.ale().aes()) / 2.0f, (float) (HudSelectedTargetAddon.this.f9919kj.ale().aet() / 2), 0.0f);
                        }
                        z = true;
                    } else {
                        ajr.mo9497bG(cMI.getScreenPosition());
                        if (!cMI.bUs().isOffScreen() || cMI.bUs().isShowWhenOffscreen()) {
                            z = true;
                        } else {
                            z = false;
                        }
                    }
                    Vec3f dfV = ajr.dfV();
                    if (z) {
                        Vector2fWrap l = HudSelectedTargetAddon.this.m43706Hk();
                        HudSelectedTargetAddon.this.f9918DV.setLocation((int) (dfV.x - l.x), HudSelectedTargetAddon.this.f9919kj.ale().aet() - ((int) (l.y + dfV.y)));
                        if (!HudSelectedTargetAddon.this.f9918DV.isVisible()) {
                            HudSelectedTargetAddon.this.f9918DV.setVisible(true);
                        }
                    } else if (HudSelectedTargetAddon.this.f9918DV.isVisible()) {
                        HudSelectedTargetAddon.this.f9918DV.setVisible(false);
                    }
                    HudSelectedTargetAddon.this.f9918DV.pack();
                }
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$c */
    /* compiled from: a */
    class C4468c extends WrapRunnable {
        long dEB;
        private float aSB = 0.0f;
        private C3428rT<C0813Lk> dEA = new C4469a();
        private boolean dEC = true;
        private boolean dED;
        private long startTime;

        public C4468c() {
            HudSelectedTargetAddon.this.f9919kj.aVU().mo13965a(C0813Lk.class, this.dEA);
        }

        /* renamed from: ff */
        public void mo23933ff(long j) {
            this.dEB = 1000 * j;
        }

        public void destroy() {
            cancel();
            HudSelectedTargetAddon.this.f9919kj.aVU().mo13968b(C0813Lk.class, this.dEA);
            HudSelectedTargetAddon.this.f9919kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_LOCKING_LOOP, true));
            HudSelectedTargetAddon.this.f9919kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_LOCKED, true));
            HudSelectedTargetAddon.this.amx = null;
        }

        public void cancel() {
            super.cancel();
            this.dED = true;
            HudSelectedTargetAddon.this.mo23926aG("");
            HudSelectedTargetAddon.this.f9919kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_LOCKING_LOOP, true));
            HudSelectedTargetAddon.this.f9919kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_LOCKED, true));
        }

        public void stop() {
            cancel();
        }

        public void start() {
            this.startTime = HudSelectedTargetAddon.this.f9919kj.ala().cVr();
            HudSelectedTargetAddon.this.f9919kj.alf().addTask("MissileLock", this, 33);
            HudSelectedTargetAddon.this.mo23926aG(HudSelectedTargetAddon.this.f9919kj.translate("LOCKING"));
            HudSelectedTargetAddon.this.amx.setMinimum(0);
            HudSelectedTargetAddon.this.amx.setMaximum(360);
            HudSelectedTargetAddon.this.amx.setValue(0);
            HudSelectedTargetAddon.this.amx.setVisible(true);
            HudSelectedTargetAddon.this.f9919kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_LOCKING_LOOP));
            this.dED = false;
        }

        public void run() {
            if (HudSelectedTargetAddon.this.amO || HudSelectedTargetAddon.this.ams == null || HudSelectedTargetAddon.this.ams.cMI() == null) {
                stop();
                return;
            }
            float cVr = ((float) (HudSelectedTargetAddon.this.f9919kj.ala().cVr() - this.startTime)) / ((float) this.dEB);
            if (cVr >= 1.0f) {
                HudSelectedTargetAddon.this.amx.setValue(360);
                if (!this.dEC) {
                    this.aSB += 0.05f;
                    if (this.aSB > 1.0f) {
                        this.dEC = true;
                    }
                } else {
                    this.aSB -= 89.6f;
                    if (this.aSB < 0.1f) {
                        this.dEC = false;
                    }
                }
                if (!this.dED) {
                    HudSelectedTargetAddon.this.mo23926aG(HudSelectedTargetAddon.this.f9919kj.translate("LOCKED"));
                    HudSelectedTargetAddon.this.f9919kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_LOCKING_LOOP, true));
                    HudSelectedTargetAddon.this.f9919kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_LOCKED));
                    this.dED = true;
                    HudSelectedTargetAddon.this.f9919kj.getPlayer().mo14348am("targetLocked", "");
                    return;
                }
                return;
            }
            HudSelectedTargetAddon.this.amx.setValue((int) (cVr * 360.0f));
        }

        /* renamed from: taikodom.addon.hud.HudSelectedTargetAddon$c$a */
        class C4469a extends C3428rT<C0813Lk> {
            C4469a() {
            }

            /* renamed from: a */
            public void mo321b(C0813Lk lk) {
                if (lk.isLocked()) {
                    C4468c.this.mo23933ff(lk.bfc().ayD());
                    C4468c.this.start();
                    return;
                }
                C4468c.this.stop();
            }
        }
    }
}
