package taikodom.addon.hud.autopilot;

import game.network.message.externalizable.C4135zz;
import game.network.message.externalizable.C6128agw;
import logic.IAddonSettings;
import logic.aaa.C2733jJ;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.C2698il;
import logic.ui.item.C2830kk;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.neo.lowerbar.NeoLowerBar;

import javax.swing.*;

@TaikodomAddon("taikodom.addon.hud.autopilot")
/* compiled from: a */
public class AutoPilotFeedbackAddon implements C2495fo {
    private static final String iMG = "autopilotfeedback.xml";
    /* access modifiers changed from: private */
    public C4135zz.C4136a iMI;
    /* renamed from: kj */
    public IAddonProperties f9936kj;
    private C2698il gFM;
    /* access modifiers changed from: private */
    private C3428rT<C4135zz> iMH;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9936kj = addonPropertie;
        this.iMH = new C4501a();
        this.f9936kj.aVU().mo13965a(C4135zz.class, this.iMH);
    }

    public void stop() {
        this.f9936kj.aVU().mo13964a(this.iMH);
        this.iMH = null;
    }

    /* access modifiers changed from: private */
    public void duk() {
        if (this.gFM != null) {
            this.gFM.destroy();
        }
        this.gFM = (C2698il) this.f9936kj.bHv().mo16794bQ(iMG);
        this.gFM.mo4917cf("message").setText(this.f9936kj.translate("AUTO_PILOT_ENGAGED_FEEDBACK_MESSAGE"));
        this.gFM.mo4917cf("message").setVisible(true);
        this.gFM.validate();
        this.gFM.pack();
        this.gFM.setLocation((this.f9936kj.bHv().getScreenWidth() - this.gFM.getWidth()) / 2, (this.f9936kj.bHv().getScreenHeight() - ((NeoLowerBar) this.f9936kj.mo11830U(NeoLowerBar.class)).getHeight()) - this.gFM.getHeight());
        new aDX(this.gFM.mo4917cf("message"), "[255..100] dur 1000; [100..255] dur 1000", C2830kk.asS, new C4502b());
    }

    /* access modifiers changed from: private */
    public void dul() {
        this.gFM.setVisible(false);
        this.gFM.destroy();
        this.gFM = null;
    }

    /* renamed from: taikodom.addon.hud.autopilot.AutoPilotFeedbackAddon$a */
    class C4501a extends C3428rT<C4135zz> {
        private static /* synthetic */ int[] gfn;

        C4501a() {
        }

        static /* synthetic */ int[] clY() {
            int[] iArr = gfn;
            if (iArr == null) {
                iArr = new int[C4135zz.C4136a.values().length];
                try {
                    iArr[C4135zz.C4136a.DIRECTION_CHANGE.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C4135zz.C4136a.FOLLOW.ordinal()] = 5;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C4135zz.C4136a.GO_TO.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C4135zz.C4136a.GO_TO_INTERACT.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C4135zz.C4136a.LAST.ordinal()] = 1;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[C4135zz.C4136a.LOST_TARGET.ordinal()] = 7;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[C4135zz.C4136a.ORBIT.ordinal()] = 6;
                } catch (NoSuchFieldError e7) {
                }
                gfn = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo321b(C4135zz zzVar) {
            if (zzVar.arT() == C4135zz.C4136a.DIRECTION_CHANGE || (!zzVar.arU() && AutoPilotFeedbackAddon.this.iMI == C4135zz.C4136a.DIRECTION_CHANGE)) {
                AutoPilotFeedbackAddon.this.iMI = zzVar.arT();
                return;
            }
            if (zzVar.arU()) {
                switch (clY()[zzVar.arT().ordinal()]) {
                    case 3:
                        AutoPilotFeedbackAddon.this.f9936kj.aVU().mo13975h(new C2733jJ(AutoPilotFeedbackAddon.this.f9936kj.translate("AUTO_PILOT_GO_TO_START_MESSAGE"), false, false, C6128agw.C1890a.WHITE, new Object[0]));
                        break;
                    case 4:
                        AutoPilotFeedbackAddon.this.f9936kj.aVU().mo13975h(new C2733jJ(AutoPilotFeedbackAddon.this.f9936kj.translate("AUTO_PILOT_GO_TO_INTERACT_START_MESSAGE"), false, false, C6128agw.C1890a.WHITE, new Object[0]));
                        break;
                    case 5:
                        AutoPilotFeedbackAddon.this.f9936kj.aVU().mo13975h(new C2733jJ(AutoPilotFeedbackAddon.this.f9936kj.translate("AUTO_PILOT_FOLLOW_START_MESSAGE"), false, false, C6128agw.C1890a.WHITE, new Object[0]));
                        break;
                    case 7:
                        AutoPilotFeedbackAddon.this.f9936kj.aVU().mo13975h(new C2733jJ(AutoPilotFeedbackAddon.this.f9936kj.translate("AUTO_PILOT_LOST_TARGET_MESSAGE"), false, false, C6128agw.C1890a.WHITE, new Object[0]));
                        break;
                }
            } else {
                AutoPilotFeedbackAddon.this.f9936kj.aVU().mo13975h(new C2733jJ(AutoPilotFeedbackAddon.this.f9936kj.translate("AUTO_PILOT_FINISHED_MESSAGE"), false, false, C6128agw.C1890a.WHITE, new Object[0]));
            }
            AutoPilotFeedbackAddon.this.iMI = zzVar.arT();
            if (zzVar.arU()) {
                AutoPilotFeedbackAddon.this.duk();
            } else {
                AutoPilotFeedbackAddon.this.dul();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.autopilot.AutoPilotFeedbackAddon$b */
    /* compiled from: a */
    class C4502b implements C0454GJ {
        C4502b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            if (jComponent.isVisible()) {
                new aDX(jComponent, "[255..100] dur 1000; [100..255] dur 1000", C2830kk.asS, this);
            }
        }
    }
}
