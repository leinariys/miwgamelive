package taikodom.addon.hud.monitor;

import logic.IAddonSettings;
import logic.aaa.C4086zH;
import logic.aaa.C5685aSv;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.item.TaskPane;
import p001a.C5577aOr;
import p001a.aGC;
import p001a.aSZ;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.hud.monitor")
/* compiled from: a */
public class MonitorAddon implements C2495fo {
    /* access modifiers changed from: private */
    public boolean eoS;
    /* renamed from: kj */
    public IAddonProperties f9945kj;
    private aSZ aCF;
    private TaskPane aCG;
    /* access modifiers changed from: private */
    private C4086zH ihS;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9945kj = addonPropertie;
        this.aCG = (TaskPane) this.f9945kj.bHv().mo16794bQ("monitor.xml");
        this.ihS = new C4086zH("monitor", this.aCG, "monitor-content");
        this.ihS.mo23270a((C4086zH.C4087a) new C4522b());
        this.f9945kj.aVU().publish(this.ihS);
        IComponentManager e = ComponentManager.getCssHolder(this.ihS.dug());
        e.setAttribute("class", "chattabpane");
        e.mo13045Vk();
        this.aCF = new aSZ(this.aCG, new C4521a("Monitor", "Monitor"), 999, 2);
        this.f9945kj.aVU().publish(this.aCF);
    }

    /* access modifiers changed from: protected */
    /* renamed from: m */
    public void mo24026m(boolean z) {
        this.eoS = z;
        for (C5685aSv next : this.ihS.duf()) {
            if (next.dut() instanceof C5577aOr) {
                ((C5577aOr) next.dut()).mo10684m(z);
            }
        }
    }

    public void stop() {
        this.f9945kj.aVU().mo13974g(this.aCF);
        this.f9945kj.aVU().mo13974g(this.ihS);
    }

    /* renamed from: taikodom.addon.hud.monitor.MonitorAddon$b */
    /* compiled from: a */
    class C4522b implements C4086zH.C4087a {
        C4522b() {
        }

        /* renamed from: df */
        public void mo23280df() {
            MonitorAddon.this.mo24026m(MonitorAddon.this.eoS);
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.MonitorAddon$a */
    class C4521a extends aGC {


        C4521a(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
        }

        public boolean isEnabled() {
            return !MonitorAddon.this.f9945kj.getPlayer().bQB();
        }

        /* renamed from: m */
        public void mo8924m(boolean z) {
            MonitorAddon.this.mo24026m(z);
        }
    }
}
