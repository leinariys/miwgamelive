package taikodom.addon.hud.monitor;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.geometry.aLH;
import game.script.Actor;
import game.script.Character;
import game.script.citizenship.CitizenshipType;
import game.script.corporation.CorporationLogo;
import game.script.item.ItemTypeCategory;
import game.script.player.Player;
import game.script.ship.*;
import game.script.space.Advertise;
import game.script.space.Gate;
import game.script.space.Loot;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C3551sV;
import logic.aaa.C5685aSv;
import logic.aaa.C5783aaP;
import logic.res.sound.C0907NJ;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.Component3d;
import logic.ui.item.Picture;
import logic.ui.item.Progress;
import logic.ui.item.Repeater;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;
import taikodom.render.SceneView;
import taikodom.render.camera.Camera;
import taikodom.render.enums.LightType;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RLight;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.swing.*;
import javax.vecmath.Quat4f;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

@TaikodomAddon("taikodom.addon.hud.monitor")
/* compiled from: a */
public class TargetInfoAddon implements C5577aOr, C2495fo {

    /* access modifiers changed from: private */
    public boolean amO = false;
    /* access modifiers changed from: private */
    public C2698il aoL;
    /* access modifiers changed from: private */
    public Actor bOC;
    /* access modifiers changed from: private */
    public SceneObject bOD;
    /* access modifiers changed from: private */
    public Component3d bOE;
    /* access modifiers changed from: private */
    public RLight bOF;
    /* renamed from: kj */
    public IAddonProperties f9949kj;
    /* renamed from: P */
    private Player f9948P;
    private C5685aSv aoK;
    private C3428rT<C3551sV> bOB;
    private Panel bOG;
    private DecimalFormat bOH;
    private Progress.C2065a bOI;
    private Progress.C2065a bOJ;
    private Repeater<C4524b> bOK;
    private boolean bOL = false;
    private boolean bOM = false;
    private ActionListener bON = new aKE(this);
    private ActionListener bOO = new aKF(this);
    private WrapRunnable bOP = new aKH(this);
    /* access modifiers changed from: private */
    private C3428rT<C5783aaP> bvz;

    /* access modifiers changed from: private */
    public void arm() {
        if (this.bOC != null && !(this.bOC instanceof Advertise) && this.bOD != null) {
            this.bOD.setOrientation(this.f9948P.bQx().getOrientation().mo15256zd().mo15233d((Quat4f) this.bOC.getOrientation()));
        }
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9949kj = addonPropertie;
        this.bOB = new C4527e();
        this.bvz = new C4528f();
        this.f9949kj.aVU().mo13965a(C3551sV.class, this.bOB);
        this.f9949kj.aVU().mo13965a(C5783aaP.class, this.bvz);
        this.f9949kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, new C4529g());
        this.bOH = new DecimalFormat("0.00");
        this.bOF = new RLight();
        this.bOF.setType(LightType.DIRECTIONAL_LIGHT);
        this.bOF.setDirection(new Vec3f(0.0f, 0.0f, -1.0f));
        this.f9948P = this.f9949kj.getPlayer();
        this.bOI = new C4530h();
        this.bOJ = new C4525c();
        this.aoK = new C5685aSv("monitor", this.f9949kj, this, true);
        aru();
        this.f9949kj.aVU().publish(this.aoK);
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public String m43964k(float f, float f2) {
        float f3 = 0.0f;
        if (f2 > 0.0f) {
            f3 = 100.0f * (f / f2);
        }
        return String.valueOf(this.bOH.format((double) f3)) + "%";
    }

    /* renamed from: xL */
    private void m43967xL() {
        this.bOK = this.aoL.mo4919ch("infoRep");
        this.bOK.mo22250a(new C4526d());
        this.bOE = this.aoL.mo4915cd("targetScene");
        this.bOG = this.aoL.mo4915cd("bottom-box");
        this.aoL.mo4918cg("hullProgress").mo17403a(this.bOJ);
        this.aoL.mo4918cg("shieldProgress").mo17403a(this.bOI);
    }

    /* access modifiers changed from: private */
    public void arn() {
        Ship bQx = this.f9948P.bQx();
        if (bQx == null || this.f9948P.bQB() || bQx.agB() == null) {
            this.bOC = null;
            aru();
            return;
        }
        this.bOC = bQx.agB();
        if (this.bOC == null) {
            aru();
        } else if (!this.amO) {
            m43966xK();
        }
    }

    private void aro() {
        JButton cb = this.aoL.mo4913cb("gotoButton");
        if (!this.bOL) {
            cb.addActionListener(this.bON);
            this.bOL = true;
        }
        cb.setVisible(true);
        cb.setEnabled(true);
        if (this.bOC instanceof Ship) {
            cb.setText(this.f9949kj.translate("Follow"));
        } else {
            cb.setText(this.f9949kj.translate("Go To"));
        }
        JButton cb2 = this.aoL.mo4913cb("interactButton");
        if (this.bOC instanceof C0286Dh) {
            if (!this.bOM) {
                cb2.addActionListener(this.bOO);
                this.bOM = true;
            }
            cb2.setVisible(true);
            cb2.setEnabled(true);
            if (this.bOC instanceof Station) {
                cb2.setText(this.f9949kj.translate("Dock"));
            } else if (this.bOC instanceof Gate) {
                cb2.setText(this.f9949kj.translate("Jump"));
            } else if (this.bOC instanceof Loot) {
                cb2.setText(this.f9949kj.translate("Loot"));
            } else {
                cb2.setText(this.f9949kj.translate("Interact"));
            }
        } else {
            cb2.setVisible(false);
            cb2.setEnabled(false);
            cb2.setText("");
        }
        this.aoL.validate();
    }

    private void arp() {
        JButton cb = this.aoL.mo4913cb("gotoButton");
        cb.setVisible(false);
        cb.setEnabled(false);
        cb.setText("");
        JButton cb2 = this.aoL.mo4913cb("interactButton");
        cb2.setVisible(false);
        cb2.setEnabled(false);
        cb2.setText("");
        this.aoL.validate();
    }

    private void arq() {
        this.bOK.clear();
        if ((this.bOC instanceof aDA) && (this.bOC instanceof Ship)) {
            Ship fAVar = (Ship) this.bOC;
            if (fAVar.agj() instanceof Player) {
                Player aku = (Player) fAVar.agj();
                m43953ac(aku);
                for (CitizenshipType a : aku.dxy().bqx()) {
                    m43950a(a);
                }
            }
            m43968y(fAVar);
            this.aoL.mo4916ce("repeaterBox").validate();
        }
    }

    /* renamed from: ac */
    private void m43953ac(Player aku) {
        if (aku != null) {
            this.bOK.mo22248G(new C4524b(this, C5378aHa.CORP_IMAGES, aku.dxy().bFK(), (C4524b) null));
        }
    }

    /* renamed from: a */
    private void m43950a(CitizenshipType adl) {
        if (adl.bRH() != null) {
            this.bOK.mo22248G(new C4524b(this, C5378aHa.ICONOGRAPHY, adl, (C4524b) null));
        }
    }

    /* renamed from: y */
    private void m43968y(Ship fAVar) {
        this.bOK.mo22248G(new C4524b(this, C5378aHa.ICONOGRAPHY, fAVar.agH().mo19866HC(), (C4524b) null));
        this.bOK.mo22248G(new C4524b(this, C5378aHa.ITEMS, fAVar.agH(), (C4524b) null));
    }

    /* access modifiers changed from: private */
    public void arr() {
        if (this.bOC != null) {
            this.aoL.mo4917cf("targetDistance").setText(m43958eu(this.f9948P.bQx().mo1011bv(this.bOC)));
            Component cd = this.aoL.mo4915cd("integrity");
            if (this.bOC instanceof aDA) {
                cd.setVisible(true);
            } else {
                cd.setVisible(false);
            }
        }
    }

    /* renamed from: xK */
    private void m43966xK() {
        this.aoL.mo4917cf("targetName").setText(String.valueOf(this.bOC.ahQ()) + " ");
        if (this.bOC instanceof Ship) {
            Character agj = ((Ship) this.bOC).agj();
            if (agj.bQO().mo17142Rn() != null) {
                this.aoL.mo4917cf("rank").setText("[ " + agj.bQO().mo17142Rn().mo21959rP().get() + " - " + this.f9949kj.translate("Rank") + " " + agj.bQO().mo17147lt() + " ]");
            } else {
                this.aoL.mo4917cf("rank").setText("[ " + this.f9949kj.translate("Rank") + " " + agj.bQO().mo17147lt() + " ]");
            }
        } else {
            this.aoL.mo4917cf("rank").setText("");
        }
        this.aoL.setVisible(true);
        this.aoL.mo4915cd("integrity").setVisible(true);
        aro();
        ars();
        arq();
        arr();
        this.f9949kj.alf().addTask("update object task", this.bOP, 33);
        this.bOE.mo16910kA(0.033f);
        this.bOE.getScene().removeAllChildren();
        this.bOE.setVisible(false);
        this.bOE.mo16909gf(false);
    }

    private void ars() {
        this.f9949kj.alg().mo4995a(this.bOC.mo649ir(), this.bOC.mo648ip(), (Scene) null, new C4523a(), "MonitorAddon");
        art();
    }

    private void art() {
        SceneView sceneView = this.bOE.getSceneView();
        if (sceneView != null) {
            sceneView.getCamera().setAspect(2.0f);
        }
    }

    /* renamed from: eu */
    private String m43958eu(float f) {
        if (f < 10000.0f) {
            return String.valueOf(Math.round(f)) + " m";
        }
        return String.valueOf(Math.round(((double) f) / 1000.0d)) + " km";
    }

    public void stop() {
        if (this.aoL != null && this.aoL.isVisible()) {
            aru();
        }
        this.f9949kj.aVU().mo13974g(this.aoK);
        this.f9949kj.aVU().mo13968b(C3551sV.class, this.bOB);
        this.f9949kj.aVU().mo13968b(C5783aaP.class, this.bvz);
    }

    /* renamed from: Io */
    public String mo5290Io() {
        return "targetInfo.xml";
    }

    /* renamed from: a */
    public void mo5291a(C2698il ilVar) {
        this.aoL = ilVar;
        m43967xL();
    }

    public String getTitle() {
        return this.f9949kj.translate("Target");
    }

    public void hide() {
        aru();
    }

    public void show() {
        if (!this.amO) {
            this.bOE.setVisible(false);
            m43966xK();
        }
    }

    public void update() {
    }

    /* renamed from: m */
    public void mo10684m(boolean z) {
        this.bOG.setVisible(!z);
        this.bOE.csr();
    }

    /* access modifiers changed from: private */
    public void aru() {
        this.bOP.cancel();
        if (this.aoL != null) {
            arp();
            this.aoL.mo4917cf("targetName").setText(this.f9949kj.translate("no-target-selected-name"));
            this.aoL.mo4917cf("rank").setText("");
            this.aoL.mo4917cf("targetDistance").setText("");
            this.aoL.mo4915cd("integrity").setVisible(false);
            this.bOK.clear();
            this.bOE.getScene().removeAllChildren();
            this.bOE.setVisible(false);
            this.aoL.setVisible(true);
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$b */
    /* compiled from: a */
    private class C4524b {
        C5378aHa gMQ;
        Object object;

        /* synthetic */ C4524b(TargetInfoAddon targetInfoAddon, C5378aHa aha, Object obj, C4524b bVar) {
            this(aha, obj);
        }

        private C4524b(C5378aHa aha, Object obj) {
            this.gMQ = aha;
            this.object = obj;
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$e */
    /* compiled from: a */
    class C4527e extends C3428rT<C3551sV> {
        C4527e() {
        }

        /* renamed from: b */
        public void mo321b(C3551sV sVVar) {
            TargetInfoAddon.this.arn();
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$f */
    /* compiled from: a */
    class C4528f extends C3428rT<C5783aaP> {
        C4528f() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            TargetInfoAddon.this.arn();
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$g */
    /* compiled from: a */
    class C4529g extends C3428rT<HudVisibilityAddon.C4534d> {
        C4529g() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            TargetInfoAddon.this.amO = !dVar.isVisible();
            if (TargetInfoAddon.this.amO) {
                TargetInfoAddon.this.aru();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$h */
    /* compiled from: a */
    class C4530h implements Progress.C2065a {
        C4530h() {
        }

        public float getMaximum() {
            if (!(TargetInfoAddon.this.bOC instanceof aDA)) {
                return 0.0f;
            }
            Shield zv = ((aDA) TargetInfoAddon.this.bOC).mo8288zv();
            if (zv == null) {
                return 0.0f;
            }
            return zv.mo19089hh();
        }

        public float getMinimum() {
            return 0.0f;
        }

        public String getText() {
            if (!(TargetInfoAddon.this.bOC instanceof aDA)) {
                return null;
            }
            Shield zv = ((aDA) TargetInfoAddon.this.bOC).mo8288zv();
            if (zv == null) {
                return null;
            }
            return TargetInfoAddon.this.m43964k(zv.mo19079Tx(), zv.mo19089hh());
        }

        public float getValue() {
            if (!(TargetInfoAddon.this.bOC instanceof aDA)) {
                return 0.0f;
            }
            Shield zv = ((aDA) TargetInfoAddon.this.bOC).mo8288zv();
            if (zv == null) {
                return 0.0f;
            }
            return zv.mo19079Tx();
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$c */
    /* compiled from: a */
    class C4525c implements Progress.C2065a {
        C4525c() {
        }

        public float getMaximum() {
            if (TargetInfoAddon.this.bOC instanceof aDA) {
                return ((aDA) TargetInfoAddon.this.bOC).mo8287zt().mo19934hh();
            }
            return 0.0f;
        }

        public float getMinimum() {
            return 0.0f;
        }

        public String getText() {
            if (!(TargetInfoAddon.this.bOC instanceof aDA)) {
                return null;
            }
            Hull zt = ((aDA) TargetInfoAddon.this.bOC).mo8287zt();
            return TargetInfoAddon.this.m43964k(zt.mo19923Tx(), zt.mo19934hh());
        }

        public float getValue() {
            if (TargetInfoAddon.this.bOC instanceof aDA) {
                return ((aDA) TargetInfoAddon.this.bOC).mo8287zt().mo19923Tx();
            }
            return 0.0f;
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$d */
    /* compiled from: a */
    class C4526d implements Repeater.C3671a<C4524b> {
        C4526d() {
        }

        /* renamed from: a */
        public void mo843a(C4524b bVar, Component component) {
            Picture axm = (Picture) component;
            if (bVar.object instanceof CorporationLogo) {
                axm.mo16824a(C5378aHa.CORP_IMAGES, ((CorporationLogo) bVar.object).aOQ());
            }
            if (bVar.object instanceof ShipType) {
                ShipType ng = (ShipType) bVar.object;
                String handle = ng.anN().getHandle();
                if (!handle.equals("ico_non_npc")) {
                    axm.mo16824a(C5378aHa.ITEMS, handle);
                    axm.setToolTipText(ng.mo19891ke() + " - " + ng.mo19893vW().get());
                }
            }
            if (bVar.object instanceof ItemTypeCategory) {
                ItemTypeCategory aai = (ItemTypeCategory) bVar.object;
                axm.mo16824a(C5378aHa.ICONOGRAPHY, aai.mo12100sK().getHandle());
                axm.setToolTipText(aai.mo12246ke().get());
            }
            if (bVar.object instanceof CitizenshipType) {
                CitizenshipType adl = (CitizenshipType) bVar.object;
                axm.setImage(TargetInfoAddon.this.f9949kj.bHv().adz().getImage(String.valueOf(C5378aHa.CITIZENSHIP.getPath()) + adl.bRH().getHandle()).getScaledInstance(30, 30, 4));
                axm.setToolTipText(adl.mo12809rP().get());
            }
        }
    }

    /* renamed from: taikodom.addon.hud.monitor.TargetInfoAddon$a */
    class C4523a implements C0907NJ {
        C4523a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (TargetInfoAddon.this.aoL != null && TargetInfoAddon.this.bOE != null && TargetInfoAddon.this.bOE.getScene() != null && TargetInfoAddon.this.bOC != null) {
                TargetInfoAddon.this.bOE.setVisible(false);
                if (renderAsset instanceof SceneObject) {
                    TargetInfoAddon.this.bOE.getScene().removeAllChildren();
                    TargetInfoAddon.this.bOE.getScene().addSceneAreaInfo(TargetInfoAddon.this.f9949kj.ale().adZ().getSceneAreaInfo(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN)));
                    TargetInfoAddon.this.bOE.getScene().addChild(TargetInfoAddon.this.bOF);
                    TargetInfoAddon.this.bOD = (SceneObject) renderAsset;
                    TargetInfoAddon.this.bOE.getScene().addChild(TargetInfoAddon.this.bOD);
                    aLH computeLocalSpaceAABB = TargetInfoAddon.this.bOD.computeLocalSpaceAABB();
                    Camera camera = TargetInfoAddon.this.bOE.getSceneView().getCamera();
                    camera.setPosition(new Vec3f(0.0f, 0.0f, ((float) (((double) computeLocalSpaceAABB.length()) / (Math.tan((double) ((float) ((((double) (camera.getFovY() / 2.0f)) * 3.141592653589793d) / 180.0d))) * 256.0d))) * 128.0f));
                    TargetInfoAddon.this.arm();
                    TargetInfoAddon.this.bOE.getSceneView().getSceneViewQuality().setShaderQuality(TargetInfoAddon.this.f9949kj.ale().getShaderQuality());
                    TargetInfoAddon.this.bOE.setVisible(true);
                }
            }
        }
    }
}
