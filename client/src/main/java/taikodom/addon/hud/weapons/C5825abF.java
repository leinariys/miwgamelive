package taikodom.addon.hud.weapons;

import game.script.item.Clip;
import game.script.item.EnergyWeapon;
import game.script.item.ProjectileWeapon;
import game.script.item.Weapon;
import logic.WrapRunnable;
import logic.aWa;
import logic.baa.C6200aiQ;
import logic.data.link.C6489ant;
import logic.res.code.C5663aRz;
import logic.swing.C0454GJ;
import logic.swing.C5378aHa;
import logic.swing.aDX;
import logic.ui.C1276Sr;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.item.C2830kk;
import logic.ui.item.Repeater;
import p001a.C1633Xu;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@TaikodomAddon("taikodom.addon.hud.weapons")
/* renamed from: a.abF  reason: case insensitive filesystem */
/* compiled from: a */
public class C5825abF implements aWa {
    private static final int hWP = 2000;
    private static final int hWQ = 500;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public final C2698il aoL;
    /* access modifiers changed from: private */
    public final IAddonProperties fVq;
    /* access modifiers changed from: private */
    public final List<Weapon> hWR = new ArrayList();
    /* access modifiers changed from: private */
    public final C1847b hWT = new C1847b(this, (C1847b) null);
    private final Repeater<Weapon> bOK;
    private final C1846a hWS = new C1846a(this, (C1846a) null);
    /* renamed from: SZ */
    public aDX f4126SZ;
    /* access modifiers changed from: private */
    public ActionListener hWV;
    private Weapon hWU;
    private ActionListener hWW;
    private Weapon hWX;
    private boolean visible;

    public C5825abF(IAddonProperties vWVar, String str) {
        this.fVq = vWVar;
        this.aoL = (C2698il) vWVar.bHv().mo16789b(C5825abF.class, "weapon-list.xml");
        this.aoL.addMouseListener(new C1852g());
        this.bOK = this.aoL.mo4915cd("cell-repeater");
        this.bOK.mo22250a(new C1850e(str));
        this.visible = this.aoL.isVisible();
    }

    /* renamed from: y */
    public void mo12417y(Weapon adv) {
        this.hWR.add(adv);
        List<Component> G = this.bOK.mo22248G(adv);
        C1851f fVar = new C1851f(adv);
        for (Component addMouseListener : G) {
            addMouseListener.addMouseListener(fVar);
        }
        this.aoL.pack();
    }

    /* renamed from: z */
    public void mo12418z(Weapon adv) {
        this.hWR.remove(adv);
        this.bOK.mo22249H(adv);
    }

    public void select(int i) {
        if (i < this.hWR.size()) {
            mo12401B(this.hWR.get(i));
        }
    }

    public Weapon ddh() {
        if (this.hWR.isEmpty()) {
            return null;
        }
        int indexOf = this.hWU == null ? 0 : this.hWR.indexOf(this.hWU) + 1;
        if (indexOf >= this.hWR.size()) {
            select(0);
        } else {
            select(indexOf);
        }
        return ddj();
    }

    public Weapon ddi() {
        if (this.hWR.isEmpty()) {
            return null;
        }
        int indexOf = this.hWU == null ? 0 : this.hWR.indexOf(this.hWU) - 1;
        if (indexOf < 0) {
            select(this.hWR.size() - 1);
        } else {
            select(indexOf);
        }
        return ddj();
    }

    public Weapon ddj() {
        return this.hWU;
    }

    /* renamed from: A */
    public int mo12400A(Weapon adv) {
        return this.hWR.indexOf(adv);
    }

    /* renamed from: e */
    public void mo12408e(ActionListener actionListener) {
        this.hWV = actionListener;
    }

    /* renamed from: f */
    public void mo12409f(ActionListener actionListener) {
        this.hWW = actionListener;
    }

    /* renamed from: B */
    public void mo12401B(Weapon adv) {
        if (adv != null) {
            if (!adv.equals(this.hWU) && this.hWU != null) {
                for (Component e : this.bOK.get(this.hWU)) {
                    ComponentManager.getCssHolder(e).mo13063q(4, 4);
                }
            }
            for (Component e2 : this.bOK.get(adv)) {
                ComponentManager.getCssHolder(e2).mo13063q(4, 2);
            }
            this.hWU = adv;
            if (this.hWW != null) {
                this.hWW.actionPerformed((ActionEvent) null);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: C */
    public void m19895C(Weapon adv) {
        for (Component e : this.bOK.get(adv)) {
            ComponentManager.getCssHolder(e).mo13063q(4, 16);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: D */
    public void m19896D(Weapon adv) {
        if (adv == null || !adv.equals(this.hWX)) {
            if (this.hWX != null) {
                List<Component> list = this.bOK.get(this.hWX);
                if (this.hWX.equals(this.hWU)) {
                    for (Component e : list) {
                        ComponentManager.getCssHolder(e).mo13063q(4, 130);
                    }
                } else {
                    for (Component e2 : list) {
                        ComponentManager.getCssHolder(e2).mo13063q(4, 128);
                    }
                }
            }
            if (adv != null) {
                List<Component> list2 = this.bOK.get(adv);
                if (!adv.equals(this.hWU)) {
                    for (Component e3 : list2) {
                        ComponentManager.getCssHolder(e3).mo13063q(4, 128);
                    }
                } else {
                    for (Component e4 : list2) {
                        ComponentManager.getCssHolder(e4).mo13063q(4, 130);
                    }
                }
                this.hWX = adv;
                this.aoL.pack();
            }
        }
    }

    public void clear() {
        this.hWX = null;
        this.hWU = null;
        this.hWT.cancel();
        this.hWT.clear();
        this.hWR.clear();
        this.bOK.clear();
    }

    public void pack() {
        this.aoL.pack();
    }

    /* renamed from: aE */
    public void mo12403aE(int i, int i2) {
        this.aoL.setLocation(i, i2);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.hWS.cancel();
        this.aoL.destroy();
        super.finalize();
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void setVisible(boolean z) {
        this.hWS.cancel();
        if (z) {
            if (this.fVq.alb().anX()) {
                this.fVq.alf().mo7960a("WeaponList - hide task", this.hWS, 2000);
            }
            if (isVisible() != z) {
                this.visible = true;
                this.aoL.setVisible(true);
                this.hWT.cAE();
                C1848c cVar = new C1848c();
                if (this.f4126SZ != null) {
                    this.f4126SZ.kill();
                    this.f4126SZ = new aDX(this.aoL, "[" + this.aoL.getLocation().y + ".." + 0 + "] dur 250 strong_out", C2830kk.asP, cVar, true);
                    return;
                }
                this.f4126SZ = new aDX(this.aoL, "[" + (-this.aoL.getHeight()) + ".." + 0 + "] dur 500 strong_out", C2830kk.asP, cVar, true);
            }
        } else if (isVisible() != z) {
            this.visible = false;
            C1849d dVar = new C1849d();
            if (this.f4126SZ != null) {
                this.f4126SZ.kill();
                this.f4126SZ = new aDX(this.aoL, "[" + this.aoL.getLocation().y + ".." + (-this.aoL.getHeight()) + "] dur 250 strong_in", C2830kk.asP, dVar, true);
                return;
            }
            this.f4126SZ = new aDX(this.aoL, "[0.." + (-this.aoL.getHeight()) + "] dur 500 strong_in", C2830kk.asP, dVar, true);
        }
    }

    public int width() {
        return this.aoL.getWidth();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m19903a(Weapon adv, C1276Sr sr) {
        if (sr instanceof C2698il) {
            ((C2698il) sr).mo4915cd("weapon-category").mo16824a(C5378aHa.ICONOGRAPHY, adv.aqz().mo19866HC().mo12100sK().getHandle());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m19904a(Weapon adv, C1276Sr sr, boolean z) {
        if (sr instanceof C2698il) {
            C2698il ilVar = (C2698il) sr;
            ilVar.mo4917cf("name").setText(adv.aqz().mo19891ke().get());
            JLabel cf = ilVar.mo4917cf("ammo-name");
            if (!z) {
                ((ProjectileWeapon) adv).mo8320a(C6489ant.gdu, (C6200aiQ<?>) new C1853h(cf));
            }
            cf.setVisible(!z);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m19905a(String str, Weapon adv, C2698il ilVar) {
        ilVar.mo4915cd("weapon-icon").mo16824a(C5378aHa.ITEMS, adv.aqz().anN().getHandle());
        String af = this.fVq.mo2324af(String.valueOf(str) + (mo12400A(adv) + 1));
        ilVar.mo4917cf("weapon-index").setVisible(af != null && !af.equals(""));
        ilVar.mo4917cf("weapon-index").setText(af);
    }

    /* renamed from: a */
    public void mo12402a(Weapon adv, JLabel jLabel) {
        int aqh = adv.aqh();
        if (m19907a(jLabel, aqh)) {
            jLabel.setText(new StringBuilder().append(aqh).toString());
        } else {
            jLabel.setText(this.fVq.mo11834a((Class<?>) WeaponsControlAddon.class, "empty", new Object[0]));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m19907a(JLabel jLabel, int i) {
        if (i <= 0) {
            ComponentManager.getCssHolder(jLabel).setAttribute("class", "empty");
            return false;
        }
        ComponentManager.getCssHolder(jLabel).setAttribute("class", "ready");
        return true;
    }

    public boolean isEmpty() {
        return this.hWR.isEmpty();
    }

    /* renamed from: a.abF$g */
    /* compiled from: a */
    class C1852g extends MouseAdapter {
        C1852g() {
        }

        public void mouseExited(MouseEvent mouseEvent) {
            C5825abF.this.m19896D((Weapon) null);
        }
    }

    /* renamed from: a.abF$e */
    /* compiled from: a */
    class C1850e implements Repeater.C3671a<Weapon> {
        private final /* synthetic */ String iwE;

        C1850e(String str) {
            this.iwE = str;
        }

        /* renamed from: a */
        public void mo843a(Weapon adv, Component component) {
            C1276Sr sr = (C1276Sr) component;
            C5825abF.this.m19905a(this.iwE, adv, (C2698il) sr);
            C5825abF.this.m19904a(adv, sr, adv instanceof EnergyWeapon);
            C2698il ilVar = (C2698il) component;
            ilVar.mo4917cf("range").setText(C1633Xu.m11724e((double) adv.mo12940in(), 1000.0d));
            C5825abF.this.hWT.mo12423e(ilVar.mo4917cf("ammo-count"));
            C5825abF.this.m19903a(adv, sr);
        }
    }

    /* renamed from: a.abF$f */
    /* compiled from: a */
    class C1851f extends MouseAdapter {
        private final /* synthetic */ Weapon iwF;

        C1851f(Weapon adv) {
            this.iwF = adv;
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            C5825abF.this.m19896D(this.iwF);
        }

        public void mousePressed(MouseEvent mouseEvent) {
            C5825abF.this.m19895C(this.iwF);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            C5825abF.this.mo12401B(this.iwF);
            if (C5825abF.this.hWV != null) {
                C5825abF.this.hWV.actionPerformed((ActionEvent) null);
            }
        }
    }

    /* renamed from: a.abF$c */
    /* compiled from: a */
    class C1848c implements C0454GJ {
        C1848c() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C5825abF.this.f4126SZ = null;
        }
    }

    /* renamed from: a.abF$d */
    /* compiled from: a */
    class C1849d implements C0454GJ {
        C1849d() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C5825abF.this.f4126SZ = null;
            C5825abF.this.aoL.setVisible(false);
        }
    }

    /* renamed from: a.abF$h */
    /* compiled from: a */
    class C1853h implements C6200aiQ<ProjectileWeapon> {
        private final /* synthetic */ JLabel iwH;

        C1853h(JLabel jLabel) {
            this.iwH = jLabel;
        }

        /* renamed from: a */
        public void mo1143a(ProjectileWeapon sg, C5663aRz arz, Object obj) {
            int Iq;
            Clip vo = (Clip) obj;
            C5825abF abf = C5825abF.this;
            JLabel jLabel = this.iwH;
            if (vo == null) {
                Iq = 0;
            } else {
                Iq = vo.mo302Iq();
            }
            if (abf.m19907a(jLabel, Iq)) {
                this.iwH.setText(vo.bAJ().mo19891ke().get());
            } else {
                this.iwH.setText(C5825abF.this.fVq.mo11834a((Class<?>) WeaponsControlAddon.class, "No ammo loaded", new Object[0]));
            }
        }
    }

    /* renamed from: a.abF$a */
    private class C1846a extends WrapRunnable {
        private C1846a() {
        }

        /* synthetic */ C1846a(C5825abF abf, C1846a aVar) {
            this();
        }

        public void run() {
            C5825abF.this.setVisible(false);
        }
    }

    /* renamed from: a.abF$b */
    /* compiled from: a */
    private class C1847b extends WrapRunnable {
        final List<JLabel> list;
        private boolean gLi;

        private C1847b() {
            this.list = new LinkedList();
            this.gLi = false;
        }

        /* synthetic */ C1847b(C5825abF abf, C1847b bVar) {
            this();
        }

        public void cAE() {
            if (!this.gLi) {
                C5825abF.this.hWT.run();
                this.gLi = true;
                C5825abF.this.fVq.alf().addTask("WeaponList - UpdateTask", C5825abF.this.hWT, 500);
            }
        }

        public void cancel() {
            this.gLi = false;
            super.cancel();
        }

        /* renamed from: e */
        public void mo12423e(JLabel jLabel) {
            this.list.add(jLabel);
        }

        public void clear() {
            this.list.clear();
        }

        public void run() {
            if (C5825abF.this.isVisible()) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < this.list.size()) {
                        C5825abF.this.mo12402a((Weapon) C5825abF.this.hWR.get(i2), this.list.get(i2));
                        i = i2 + 1;
                    } else {
                        return;
                    }
                }
            } else {
                cancel();
            }
        }
    }
}
