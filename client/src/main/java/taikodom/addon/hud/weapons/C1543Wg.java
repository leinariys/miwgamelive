package taikodom.addon.hud.weapons;

import game.script.Actor;
import game.script.item.*;
import game.script.ship.Ship;
import logic.C6961axa;
import logic.aPE;
import logic.aWa;
import logic.baa.C6200aiQ;
import logic.data.link.C6489ant;
import logic.res.code.C5663aRz;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.item.Picture;
import p001a.C1633Xu;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@TaikodomAddon("taikodom.addon.hud.weapons")
/* renamed from: a.Wg */
/* compiled from: a */
public class C1543Wg implements aWa {
    /* access modifiers changed from: private */
    public final IAddonProperties fVq;
    /* access modifiers changed from: private */
    public final JLabel hXL;
    /* access modifiers changed from: private */
    public final JLabel hXN;
    /* access modifiers changed from: private */
    public final Picture hXO;
    private final C6961axa eLY;
    private final C2698il aoL = ((C2698il) this.eLY.mo16789b(C1543Wg.class, "weapon-info.xml"));
    private final JLabel hXM;
    /* access modifiers changed from: private */
    public ActionListener hWV;
    /* access modifiers changed from: private */
    public float range;
    private C1544a hXP;

    public C1543Wg(IAddonProperties vWVar, String str) {
        this.fVq = vWVar;
        this.eLY = vWVar.bHv();
        this.aoL.setFocusable(!vWVar.alb().anX());
        this.hXL = this.aoL.mo4917cf("ammo-count");
        this.hXM = this.aoL.mo4917cf("name");
        this.hXN = this.aoL.mo4917cf("range");
        this.hXO = this.aoL.mo4915cd("ammo-icon");
        this.aoL.addMouseListener(new C1546b());
    }

    /* renamed from: a */
    public void mo6607a(String str, Weapon adv) {
        if (this.hXP != null) {
            this.hXP.cancel();
            this.hXP = null;
        }
        this.aoL.mo4917cf("weapon-index").setVisible(str != null && !str.equals(""));
        this.aoL.mo4917cf("weapon-index").setText(str);
        WeaponType aqz = adv.aqz();
        this.aoL.mo4915cd("weapon-icon").mo16824a(C5378aHa.ITEMS, aqz.anN().getHandle());
        this.aoL.mo4915cd("weapon-category").mo16824a(C5378aHa.ICONOGRAPHY, aqz.mo19866HC().mo12100sK().getHandle());
        this.range = adv.mo12940in();
        this.hXM.setText(aqz.mo19891ke().get());
        this.hXN.setText(C1633Xu.m11724e((double) this.range, 1000.0d));
        this.hXP = new C1544a(adv);
        this.fVq.mo11833a(100, (aPE) this.hXP);
        this.aoL.pack();
    }

    public void center() {
    }

    public void pack() {
        this.aoL.pack();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.hXP.cancel();
        this.hXP = null;
        this.aoL.destroy();
        super.finalize();
    }

    /* renamed from: aE */
    public void mo6608aE(int i, int i2) {
        this.aoL.setLocation(i, i2);
    }

    public int width() {
        return this.aoL.getWidth();
    }

    public Point ddy() {
        return this.aoL.getLocation();
    }

    public boolean isVisible() {
        return this.aoL.isVisible();
    }

    public void setVisible(boolean z) {
        this.aoL.setVisible(z);
        if (this.hXP != null && z) {
            this.fVq.mo11833a(100, (aPE) this.hXP);
        }
    }

    /* renamed from: e */
    public void mo6611e(ActionListener actionListener) {
        this.hWV = actionListener;
    }

    /* renamed from: jz */
    public void mo6614jz(boolean z) {
        this.aoL.setFocusable(!z);
    }

    /* renamed from: a.Wg$b */
    /* compiled from: a */
    class C1546b extends MouseAdapter {
        C1546b() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (C1543Wg.this.hWV != null) {
                C1543Wg.this.hWV.actionPerformed((ActionEvent) null);
            }
        }
    }

    /* renamed from: a.Wg$a */
    public class C1544a implements aPE {
        private final float exa;
        /* renamed from: oX */
        private final Weapon f2051oX;
        private boolean api = false;
        private int exb;
        private C6200aiQ<ProjectileWeapon> exc;

        public C1544a(Weapon adv) {
            this.f2051oX = adv;
            this.exa = C1543Wg.this.range * C1543Wg.this.range;
            if (adv instanceof ProjectileWeapon) {
                ProjectileWeapon sg = (ProjectileWeapon) adv;
                this.exc = new C1545a(sg);
                sg.mo8320a(C6489ant.gdu, (C6200aiQ<?>) this.exc);
                return;
            }
            this.exc = null;
            C1543Wg.this.hXO.setVisible(false);
        }

        /* access modifiers changed from: private */
        public void cancel() {
            if (this.exc != null) {
                this.f2051oX.mo8326b(C6489ant.gdu, (C6200aiQ<?>) this.exc);
            }
            this.api = true;
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            String str;
            if (this.api || !C1543Wg.this.isVisible()) {
                return false;
            }
            int aqh = this.f2051oX.aqh();
            IComponentManager e = ComponentManager.getCssHolder(C1543Wg.this.hXL);
            if (aqh <= 0) {
                e.setAttribute("class", "empty");
                C1543Wg.this.hXL.setText(C1543Wg.this.fVq.mo11834a((Class<?>) WeaponsControlAddon.class, "empty", new Object[0]));
                if (this.f2051oX instanceof ProjectileWeapon) {
                    C1543Wg.this.hXO.cCy();
                }
                return true;
            }
            if (this.f2051oX instanceof ProjectileWeapon) {
                if (((double) ((ProjectileWeapon) this.f2051oX).buj().cZX()) * 0.05d > ((double) aqh)) {
                    str = "low";
                }
                str = "ready";
            } else {
                if ((this.f2051oX instanceof EnergyWeapon) && ((double) ((EnergyWeapon) this.f2051oX).aqp().bVX()) * 0.05d > ((double) aqh)) {
                    str = "low";
                }
                str = "ready";
            }
            e.setAttribute("class", str);
            if (this.exb != aqh) {
                this.exb = aqh;
                C1543Wg.this.hXL.setText(new StringBuilder().append(aqh).toString());
                C1543Wg.this.hXL.repaint();
                Ship al = this.f2051oX.mo7855al();
                Actor agB = al.agB();
                if (agB == null) {
                    ComponentManager.getCssHolder(C1543Wg.this.hXN).setAttribute("class", "");
                } else if (al.mo1001bB(agB) <= this.exa) {
                    ComponentManager.getCssHolder(C1543Wg.this.hXN).setAttribute("class", "inrange");
                } else {
                    ComponentManager.getCssHolder(C1543Wg.this.hXN).setAttribute("class", "offrange");
                }
                C1543Wg.this.hXN.repaint();
            }
            return true;
        }

        /* renamed from: a.Wg$a$a */
        class C1545a implements C6200aiQ<ProjectileWeapon> {
            private final /* synthetic */ ProjectileWeapon fOV;

            C1545a(ProjectileWeapon sg) {
                this.fOV = sg;
            }

            /* renamed from: a */
            public void mo1143a(ProjectileWeapon sg, C5663aRz arz, Object obj) {
                Clip vo = (Clip) obj;
                if (obj == null || vo.mo302Iq() <= 0) {
                    C1543Wg.this.hXO.setVisible(false);
                    return;
                }
                C1543Wg.this.hXO.mo16824a(C5378aHa.ICONOGRAPHY, this.fOV.aqt());
                C1543Wg.this.hXO.setVisible(true);
            }
        }
    }
}
