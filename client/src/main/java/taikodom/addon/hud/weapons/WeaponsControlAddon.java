package taikodom.addon.hud.weapons;

import game.network.message.externalizable.C3689to;
import game.script.item.Weapon;
import game.script.mines.MineThrowerWeapon;
import game.script.missile.MissileWeapon;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.*;
import logic.ui.aRU;
import p001a.C2602hR;
import p001a.C3428rT;
import p001a.C3904wY;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.hud.weapons")
/* compiled from: a */
public class WeaponsControlAddon implements C2495fo {
    /* access modifiers changed from: private */
    public C1543Wg hQb;
    /* access modifiers changed from: private */
    public C1543Wg hQc;
    /* access modifiers changed from: private */
    public C5825abF hQd;
    /* access modifiers changed from: private */
    public C5825abF hQe;
    /* renamed from: kj */
    public IAddonProperties f9953kj;
    private C3428rT<C5783aaP> bvz;
    /* access modifiers changed from: private */
    private C3428rT<HudVisibilityAddon.C4534d> dJB;
    /* renamed from: nP */
    private C3428rT<C3689to> f9954nP;

    /* renamed from: wA */
    private C3428rT<C3949xF> f9955wA;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9953kj = addonPropertie;
        this.f9953kj.bHv().mo16790b((Object) this, "weapon-info.css");
        this.hQb = new C1543Wg(addonPropertie, "left");
        this.hQb.mo6611e((ActionListener) new C4535a());
        this.hQd = new C5825abF(addonPropertie, "SLOT");
        this.hQd.mo12408e((ActionListener) new C4536b());
        this.hQd.mo12409f(new C4537c());
        this.hQc = new C1543Wg(addonPropertie, "right");
        this.hQc.mo6611e((ActionListener) new C4538d());
        this.hQe = new C5825abF(addonPropertie, "SEC_SLOT");
        this.hQe.mo12408e((ActionListener) new C4539e());
        this.hQe.mo12409f(new C4543i());
        this.dJB = new C4542h();
        this.f9954nP = new C4541g();
        this.f9955wA = new C4540f();
        this.bvz = new C4544j();
        this.f9953kj.mo2317P(this);
        C6245ajJ aVU = this.f9953kj.aVU();
        aVU.mo13965a(HudVisibilityAddon.C4534d.class, this.dJB);
        aVU.mo13965a(C5783aaP.class, this.bvz);
        aVU.mo13965a(C3689to.class, this.f9954nP);
        aVU.mo13965a(C3949xF.class, this.f9955wA);
        boolean z = !this.f9953kj.getPlayer().bQB();
        if (z) {
            dat();
        }
        setVisible(z);
    }

    /* access modifiers changed from: private */
    public void setVisible(boolean z) {
        boolean z2;
        boolean z3;
        boolean z4 = true;
        this.hQe.setVisible(z && !this.hQe.isEmpty());
        C5825abF abf = this.hQd;
        if (!z || this.hQd.isEmpty()) {
            z2 = false;
        } else {
            z2 = true;
        }
        abf.setVisible(z2);
        C1543Wg wg = this.hQb;
        if (!z || this.hQd.isEmpty()) {
            z3 = false;
        } else {
            z3 = true;
        }
        wg.setVisible(z3);
        C1543Wg wg2 = this.hQc;
        if (!z || this.hQe.isEmpty()) {
            z4 = false;
        }
        wg2.setVisible(z4);
        this.hQb.pack();
        this.hQb.mo6608aE(0, 0);
        this.hQc.pack();
        this.hQc.mo6608aE(aRU.RIGHT.mo11143t(this.f9953kj.bHv().getScreenWidth(), this.hQc.width()), 0);
        this.hQd.pack();
        this.hQd.mo12403aE(this.hQb.width(), 0);
        this.hQe.pack();
        this.hQe.mo12403aE(this.hQc.ddy().x - this.hQe.width(), 0);
    }

    @C2602hR(mo19255zf = "SHOOT_CANNON")
    /* renamed from: jq */
    public void mo24051jq(boolean z) {
        PlayerController alb = this.f9953kj.alb();
        if (!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX()) {
            Player dL = this.f9953kj.getPlayer();
            if (!z) {
                dL.bQx().mo18250A(C3904wY.CANNON);
            } else if (this.hQd.isVisible() || this.hQe.isVisible()) {
                this.hQd.setVisible(false);
                this.hQe.setVisible(false);
            } else {
                dL.bQx().mo18331y(C3904wY.CANNON);
                if (dL.bQx().mo18313e(C3904wY.CANNON) != null && !dL.bQx().mo18322i(C3904wY.CANNON)) {
                    dL.mo14419f((C1506WA) new C2733jJ(this.f9953kj.translate("Empty weapon"), true, new Object[0]));
                    dL.mo14419f((C1506WA) new C6831auX(dL.bQx().mo18313e(C3904wY.CANNON)));
                }
            }
        }
    }

    @C2602hR(mo19255zf = "SHOOT_LAUNCHER")
    /* renamed from: jr */
    public void mo24052jr(boolean z) {
        PlayerController alb = this.f9953kj.alb();
        if (!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX() && !alb.cNM()) {
            Player dL = this.f9953kj.getPlayer();
            if (!z) {
                dL.bQx().mo18250A(C3904wY.LAUNCHER);
            } else if (this.hQd.isVisible() || this.hQe.isVisible()) {
                this.hQd.setVisible(false);
                this.hQe.setVisible(false);
                return;
            } else {
                dL.bQx().mo18331y(C3904wY.LAUNCHER);
            }
            if (dL.bQx().mo18313e(C3904wY.LAUNCHER) != null && !dL.bQx().mo18322i(C3904wY.LAUNCHER)) {
                dL.mo14419f((C1506WA) new C2733jJ(this.f9953kj.translate("Empty weapon"), true, new Object[0]));
                dL.mo14419f((C1506WA) new C6831auX(dL.bQx().mo18313e(C3904wY.LAUNCHER)));
            }
        }
    }

    @C2602hR(mo19255zf = "NEXT_CANNON")
    /* renamed from: js */
    public void mo24053js(boolean z) {
        PlayerController alb = this.f9953kj.alb();
        if (!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX() && !alb.cNM() && !z) {
            if (!this.hQd.isVisible()) {
                this.hQd.setVisible(true);
                return;
            }
            this.hQd.ddh();
            this.f9953kj.aVU().mo13972d("cannon.select.next", "");
        }
    }

    @C2602hR(mo19255zf = "PREV_CANNON")
    /* renamed from: jt */
    public void mo24054jt(boolean z) {
        PlayerController alb = this.f9953kj.alb();
        if (!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX() && !alb.cNM() && z) {
            if (!this.hQd.isVisible()) {
                this.hQd.setVisible(true);
                return;
            }
            this.hQd.ddi();
            this.f9953kj.aVU().mo13972d("cannon.select.prev", "");
        }
    }

    @C2602hR(mo19255zf = "NEXT_LAUNCHER")
    /* renamed from: ju */
    public void mo24055ju(boolean z) {
        PlayerController alb = this.f9953kj.alb();
        if (!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX() && !alb.cNM() && !z) {
            if (!this.hQe.isVisible()) {
                this.hQe.setVisible(true);
                return;
            }
            this.hQe.ddh();
            this.f9953kj.aVU().mo13972d("cannon.select.next", "");
        }
    }

    @C2602hR(mo19255zf = "PREV_LAUNCHER")
    /* renamed from: jv */
    public void mo24056jv(boolean z) {
        PlayerController alb = this.f9953kj.alb();
        if (!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX() && !alb.cNM() && z) {
            if (!this.hQe.isVisible()) {
                this.hQe.setVisible(true);
                return;
            }
            this.hQe.ddi();
            this.f9953kj.aVU().mo13972d("cannon.select.prev", "");
        }
    }

    @C2602hR(mo19255zf = "SLOT1")
    /* renamed from: jk */
    public void mo24045jk(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQd.select(0);
        }
    }

    @C2602hR(mo19255zf = "SLOT2")
    /* renamed from: jl */
    public void mo24046jl(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQd.select(1);
        }
    }

    @C2602hR(mo19255zf = "SLOT3")
    /* renamed from: jm */
    public void mo24047jm(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQd.select(2);
        }
    }

    @C2602hR(mo19255zf = "SLOT4")
    /* renamed from: jn */
    public void mo24048jn(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQd.select(3);
        }
    }

    @C2602hR(mo19255zf = "SLOT5")
    /* renamed from: jo */
    public void mo24049jo(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQd.select(4);
        }
    }

    @C2602hR(mo19255zf = "SLOT6")
    /* renamed from: jp */
    public void mo24050jp(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQd.select(5);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT1")
    /* renamed from: hJ */
    public void mo24037hJ(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(0);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT2")
    /* renamed from: hK */
    public void mo24038hK(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(1);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT3")
    /* renamed from: hL */
    public void mo24039hL(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(2);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT4")
    /* renamed from: hM */
    public void mo24040hM(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(3);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT5")
    /* renamed from: hN */
    public void mo24041hN(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(4);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT6")
    /* renamed from: hO */
    public void mo24042hO(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(5);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT7")
    /* renamed from: hP */
    public void mo24043hP(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(6);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT8")
    /* renamed from: hQ */
    public void mo24044hQ(boolean z) {
        if (z && !this.f9953kj.getPlayer().bQB()) {
            this.hQe.select(7);
        }
    }

    public void stop() {
        this.hQb = null;
        this.hQc = null;
        this.hQd = null;
        this.hQe = null;
        this.f9953kj.mo2318Q(this);
        this.f9953kj = null;
    }

    /* access modifiers changed from: private */
    public void dat() {
        Ship bQx = this.f9953kj.getPlayer().bQx();
        if (bQx != null) {
            this.hQd.clear();
            this.hQe.clear();
            for (Weapon next : bQx.agL()) {
                if ((next instanceof MineThrowerWeapon) || (next instanceof MissileWeapon)) {
                    this.hQe.mo12417y(next);
                } else {
                    this.hQd.mo12417y(next);
                }
            }
            Weapon afR = bQx.afR();
            if (afR != null) {
                this.hQd.mo12401B(afR);
            } else if (!this.hQd.isEmpty()) {
                this.hQd.select(0);
            }
            Weapon afT = bQx.afT();
            if (afT != null) {
                this.hQe.mo12401B(afT);
            } else if (!this.hQe.isEmpty()) {
                this.hQe.select(0);
            }
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$a */
    class C4535a implements ActionListener {
        C4535a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            WeaponsControlAddon.this.hQd.setVisible(true);
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$b */
    /* compiled from: a */
    class C4536b implements ActionListener {
        C4536b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            WeaponsControlAddon.this.hQd.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$c */
    /* compiled from: a */
    class C4537c implements ActionListener {
        C4537c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            WeaponsControlAddon.this.hQd.setVisible(true);
            Weapon ddj = WeaponsControlAddon.this.hQd.ddj();
            WeaponsControlAddon.this.hQb.mo6607a(WeaponsControlAddon.this.f9953kj.mo2324af("SLOT" + (WeaponsControlAddon.this.hQd.mo12400A(ddj) + 1)), ddj);
            Player dL = WeaponsControlAddon.this.f9953kj.getPlayer();
            if (dL.bQx() != null && dL.bQx().afR() != ddj) {
                if (ddj != null) {
                    ddj.bai();
                    WeaponsControlAddon.this.f9953kj.aVU().mo13972d("laucher.select", ddj.aqz().getHandle());
                }
                dL.bQx().mo18328o(ddj);
            }
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$d */
    /* compiled from: a */
    class C4538d implements ActionListener {
        C4538d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            WeaponsControlAddon.this.hQe.setVisible(true);
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$e */
    /* compiled from: a */
    class C4539e implements ActionListener {
        C4539e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            WeaponsControlAddon.this.hQe.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$i */
    /* compiled from: a */
    class C4543i implements ActionListener {
        C4543i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            WeaponsControlAddon.this.hQe.setVisible(true);
            Weapon ddj = WeaponsControlAddon.this.hQe.ddj();
            WeaponsControlAddon.this.hQc.mo6607a(WeaponsControlAddon.this.f9953kj.mo2324af("SEC_SLOT" + (WeaponsControlAddon.this.hQe.mo12400A(ddj) + 1)), ddj);
            Player dL = WeaponsControlAddon.this.f9953kj.getPlayer();
            if (dL.bQx() != null && dL.bQx().afT() != ddj) {
                if (ddj != null) {
                    ddj.bai();
                    WeaponsControlAddon.this.f9953kj.aVU().mo13972d("launcher.select", ddj.aqz().getHandle());
                }
                dL.bQx().mo18329q(ddj);
            }
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$h */
    /* compiled from: a */
    class C4542h extends C3428rT<HudVisibilityAddon.C4534d> {
        C4542h() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            WeaponsControlAddon.this.setVisible(dVar.isVisible());
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$g */
    /* compiled from: a */
    class C4541g extends C3428rT<C3689to> {
        C4541g() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (WeaponsControlAddon.this.hQe.isVisible() || WeaponsControlAddon.this.hQd.isVisible()) {
                toVar.adC();
                WeaponsControlAddon.this.hQe.setVisible(false);
                WeaponsControlAddon.this.hQd.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$f */
    /* compiled from: a */
    class C4540f extends C3428rT<C3949xF> {
        C4540f() {
        }

        /* renamed from: a */
        public void mo321b(C3949xF xFVar) {
            WeaponsControlAddon.this.hQc.mo6614jz(xFVar.anX());
            WeaponsControlAddon.this.hQb.mo6614jz(xFVar.anX());
            WeaponsControlAddon.this.hQe.setVisible(false);
            WeaponsControlAddon.this.hQd.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.hud.weapons.WeaponsControlAddon$j */
    /* compiled from: a */
    class C4544j extends C3428rT<C5783aaP> {
        C4544j() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            boolean equals = aap.bMO().equals(C5783aaP.C1841a.FLYING);
            if (equals) {
                WeaponsControlAddon.this.dat();
            }
            WeaponsControlAddon.this.setVisible(equals);
        }
    }
}
