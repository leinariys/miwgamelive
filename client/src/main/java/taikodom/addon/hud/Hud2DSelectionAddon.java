package taikodom.addon.hud;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Vector2fWrap;
import game.network.message.externalizable.C5667aSd;
import game.network.message.externalizable.C6128agw;
import game.script.Actor;
import game.script.item.Shot;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.simulation.Space;
import game.script.space.Asteroid;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.*;
import logic.render.IEngineGraphics;
import logic.thred.LogPrinter;
import logic.ui.C1276Sr;
import logic.ui.C2698il;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;
import taikodom.addon.hudmarkmanager.HudMarkManagerAddon;
import taikodom.render.gui.GLine;
import taikodom.render.scene.SceneObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TaikodomAddon("taikodom.addon.hud")
/* compiled from: a */
public class Hud2DSelectionAddon implements C2495fo {
    private static final LogPrinter cZy = LogPrinter.m10275K(Hud2DSelectionAddon.class);
    private static final float gTu = 300.0f;
    private static final float gTv = 250.0f;
    private static final float gTw = 150.0f;
    /* access modifiers changed from: private */
    public static float gTx = gTv;
    private static LogPrinter log = LogPrinter.m10275K(Hud2DSelectionAddon.class);
    /* access modifiers changed from: private */
    public C1494W gTC;
    /* access modifiers changed from: private */
    public GLine gTD;
    /* access modifiers changed from: private */
    public C4457a gTE;
    /* access modifiers changed from: private */
    public List<C6017aep> gTy = new ArrayList();
    /* renamed from: kj */
    public IAddonProperties f9904kj;
    public boolean allowSelection;
    /* renamed from: Bu */
    private C6124ags<C5783aaP> f9903Bu;
    private C6124ags<C3551sV> amr;
    private C6124ags<HudVisibilityAddon.C4534d> bKq;
    private C6124ags<C3853vo> bLI;
    private C6124ags<C6898avm> gTA;
    private C6124ags<C2041bS> gTB;
    private C6124ags<C3030nE> gTF;
    private boolean gTG = false;
    /* access modifiers changed from: private */
    private WrapRunnable gTz;
    private boolean visible;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9904kj = addonPropertie;
        this.gTA = new C4465i();
        this.gTB = new C4464h();
        this.bKq = new C4461e();
        this.gTF = new C4460d();
        this.f9903Bu = new C4459c();
        this.gTz = new C4458b();
        this.f9904kj.alf().addTask("HudSelectionUpdater", this.gTz, 5000);
        this.visible = true;
        this.f9904kj.aVU().mo13965a(C2041bS.class, this.gTB);
        this.f9904kj.aVU().mo13965a(C6898avm.class, this.gTA);
        this.f9904kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.bKq);
        this.f9904kj.aVU().mo13965a(C5783aaP.class, this.f9903Bu);
        this.f9904kj.aVU().mo13965a(C3030nE.class, this.gTF);
        this.allowSelection = !this.f9904kj.alb().anX();
        this.gTD = new GLine();
        if (this.f9904kj.getPlayer().bQB()) {
            this.gTD.setRender(false);
        } else {
            this.gTD.setRender(true);
        }
        this.gTD.setPrimitiveColor(new Color(0.5f, 0.5f, 0.5f, 0.1f));
        this.gTD.createCircle(new Vector2fWrap(0.0f, 0.0f), gTx);
        C5916acs.getSingolton().getEngineGraphics().aeh().addChild(this.gTD);
        Ship bQx = this.f9904kj.getPlayer().bQx();
        if (bQx != null && bQx.agp() > bQx.agH().agp()) {
            gTx = gTu;
        } else if (bQx == null || bQx.agp() >= bQx.agH().agp()) {
            gTx = gTv;
        } else {
            gTx = gTw;
        }
        this.gTE = new C4457a(this, (C4457a) null);
        if (!this.f9904kj.getPlayer().bQB()) {
            cDy();
        }
        m43680iJ();
        this.bLI = new C4463g();
        this.gTC = new C1494W(this.f9904kj);
        this.f9904kj.aVU().mo13965a(C3853vo.class, this.bLI);
        this.f9904kj.mo2317P(this);
        this.amr = new C4462f();
        this.f9904kj.aVU().mo13965a(C3551sV.class, this.amr);
        this.f9904kj.bHv().mo16790b((Object) this, "hudRadar.css");
        C2698il ilVar = (C2698il) this.f9904kj.bHv().mo16794bQ("hudRadar.xml");
        ilVar.pack();
        this.gTC.mo6460a((C1276Sr) ilVar);
        this.f9904kj.aVU().mo13975h(new C5526aMs());
    }

    /* renamed from: iJ */
    private void m43680iJ() {
        Ship bQx;
        Space Zc;
        if (!this.f9904kj.getPlayer().bQB() && (bQx = this.f9904kj.getPlayer().bQx()) != null && (Zc = bQx.mo965Zc()) != null) {
            for (Actor cMI : Zc.mo1900c(bQx.getPosition(), 100000.0f)) {
                m43663a(cMI.cMI());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void cDx() {
        for (C6017aep hide : this.gTy) {
            hide.hide();
        }
        this.gTy.clear();
        this.gTD.setRender(false);
    }

    /* access modifiers changed from: protected */
    public void cDy() {
        this.gTD.setRender(true);
        this.f9904kj.ale().aeh().addChild(this.gTD);
    }

    public void stop() {
        this.gTz.cancel();
        if (this.gTC != null) {
            this.gTC.destroy();
            this.gTC = null;
        }
        if (this.gTD != null) {
            this.gTD.dispose();
        }
        if (this.gTE != null) {
            this.gTE.cancel();
        }
    }

    @C2602hR(mo19255zf = "SELECT_CLICKED_TARGET")
    /* renamed from: c */
    public void mo23909c(C5667aSd asd) {
        Actor cr;
        Actor cr2;
        C6017aep ak;
        if (asd.isPressed() && !this.f9904kj.getPlayer().bQB()) {
            C3010mu muVar = (C3010mu) asd.dud();
            int screenX = muVar.getScreenX();
            int aet = this.f9904kj.ale().aet() - muVar.getScreenY();
            if (this.gTC == null || !this.gTC.isVisible()) {
                cr = null;
            } else {
                cr = this.gTC.mo6463c(screenX, aet);
            }
            if (cr == null && (ak = m43667ak(screenX, aet)) != null && ak.isVisible()) {
                cr = ak.mo13167ha();
            }
            if (cr == null) {
                cr2 = mo23912d(muVar);
            } else {
                cr2 = cr;
            }
            if (cr2 != null && !this.f9904kj.getPlayer().bQx().mo18256T(cr2)) {
                this.f9904kj.getPlayer().mo14419f((C1506WA) new C2733jJ(this.f9904kj.translate("Out of sensors range"), false, C6128agw.C1890a.RED));
                cr2 = null;
            }
            this.f9904kj.aVU().mo13975h(new C5893acV(cr2));
        }
    }

    /* renamed from: d */
    public Actor mo23912d(C3010mu muVar) {
        IEngineGraphics ale = this.f9904kj.getEngineGame().getEngineGraphics();
        SceneObject intersectedObject = ale.aea().rayTraceMouseOverScene(new Vector2fWrap((float) muVar.getScreenX(), (float) (ale.aet() - muVar.getScreenY()))).getIntersectedObject();
        if (intersectedObject != null) {
            return (Actor) intersectedObject.getClientProperty("actor");
        }
        return null;
    }

    private boolean abY() {
        return this.f9904kj.alb().anX();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43663a(C6017aep aep) {
        if (aep != null && !aep.mo13167ha().equals(this.f9904kj.getPlayer().bQx()) && aep.isValid()) {
            if (aep.bUs() != null) {
                if (!this.f9904kj.getPlayer().bQx().mo18256T(aep.mo13167ha())) {
                    aep.hide();
                } else if (this.visible) {
                    aep.show();
                }
                if (m43671bd(aep.mo13167ha())) {
                    aep.hide();
                }
                this.gTy.add(aep);
                if (this.visible) {
                    m43664a(aep, true);
                }
            } else if (aep.mo13167ha() != null) {
                log.error("Trying to add a HudMark without created mark inside (Asset:" + aep.mo13167ha().mo647iA() + " Actor:" + aep.mo13167ha().getName() + ").");
            } else {
                log.error("Trying to add a HudMark without created mark and actor inside.");
            }
        }
    }

    /* renamed from: bd */
    private boolean m43671bd(Actor cr) {
        return (cr instanceof Asteroid) || ((cr instanceof Ship) && cr.isStatic()) || cr == this.f9904kj.getPlayer().bQx();
    }

    /* renamed from: ak */
    private C6017aep m43667ak(int i, int i2) {
        for (C6017aep next : this.gTy) {
            if (next.isValid() && next.bUq()) {
                if (!next.isOffScreen() || !abY()) {
                    Vec3f screenPosition = next.getScreenPosition();
                    int round = Math.round(screenPosition.y);
                    int round2 = Math.round(screenPosition.x);
                    int round3 = Math.round(next.getMarkSize()) / 2;
                    if (i >= round2 - round3 && i <= round2 + round3 && i2 >= round - round3 && i2 <= round + round3) {
                        return next;
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void cDz() {
        Player aku;
        Ship bQx;
        if (this.visible) {
            try {
                aku = this.f9904kj.getPlayer();
            } catch (Exception e) {
                this.f9904kj.getLog().warn("The game doesn't have a valid player");
                e.printStackTrace();
                aku = null;
            }
            if (aku != null && (bQx = aku.bQx()) != null) {
                for (C6017aep next : this.gTy) {
                    if (!bQx.mo18256T(next.mo13167ha()) || m43671bd(next.mo13167ha())) {
                        next.hide();
                    } else {
                        next.show();
                    }
                    m43664a(next, false);
                }
            }
        }
    }

    /* renamed from: a */
    private void m43664a(C6017aep aep, boolean z) {
        Color color;
        Actor ha = aep.mo13167ha();
        if (ha.cMI() != null && (color = ha.getColor()) != null) {
            if (!color.equals(aep.getPrimitiveColor()) || z) {
                aep.setPrimitiveColor(color);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m43669b(C6017aep aep) {
        if (this.gTy.contains(aep)) {
            aep.hide();
            this.gTy.remove(aep);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: be */
    public void m43672be(Actor cr) {
        C6017aep aep;
        Iterator<C6017aep> it = this.gTy.iterator();
        while (true) {
            if (it.hasNext()) {
                aep = it.next();
                if (aep.mo13167ha() == cr) {
                    break;
                }
            } else {
                aep = null;
                break;
            }
        }
        this.gTy.remove(aep);
    }

    /* access modifiers changed from: private */
    public void cDA() {
        for (C6017aep next : this.gTy) {
            if (!((HudMarkManagerAddon) this.f9904kj.mo11830U(HudMarkManagerAddon.class)).mo24069e(((HudMarkManagerAddon) this.f9904kj.mo11830U(HudMarkManagerAddon.class)).mo24067a(this.f9904kj.getPlayer(), next.mo13167ha()))) {
                next.bUj();
            } else {
                next.bUk();
            }
        }
    }

    /* access modifiers changed from: private */
    public void hide() {
        for (C6017aep hide : this.gTy) {
            hide.hide();
        }
        this.visible = false;
        this.gTC.hide();
    }

    /* access modifiers changed from: private */
    public void show() {
        this.visible = true;
        cDz();
    }

    private void cDB() {
        for (C6017aep next : this.gTy) {
            try {
                next.bUs().setShowWhenOffscreen(true);
            } catch (NullPointerException e) {
                log.error("NullPointerException on mark.getMark() for actor mark '" + next.mo13167ha().getClass().getName() + "'");
            }
        }
        this.visible = true;
        this.gTD.setRender(true);
    }

    private void cDC() {
        this.visible = false;
        for (C6017aep next : this.gTy) {
            try {
                next.bUs().setShowWhenOffscreen(false);
            } catch (NullPointerException e) {
                log.error("NullPointerException on mark.getMark() for actor mark '" + next.mo13167ha().getClass().getName() + "'");
            }
        }
    }

    @C2602hR(mo19255zf = "TOGGLE_RADAR")
    /* renamed from: ht */
    public void mo23915ht(boolean z) {
        if (z && !this.f9904kj.getPlayer().bQB()) {
            this.gTG = !this.gTG;
            if (this.gTG) {
                this.f9904kj.aVU().mo13975h(new C2733jJ(this.f9904kj.translate("Auto-Radar switch ON"), false, new Object[0]));
                this.gTC.show();
                return;
            }
            this.f9904kj.aVU().mo13975h(new C2733jJ(this.f9904kj.translate("Auto-Radar switch OFF"), false, new Object[0]));
            this.gTC.hide();
        }
    }

    @C2602hR(mo19255zf = "RADAR_IN")
    /* renamed from: d */
    public void mo23913d(C5667aSd asd) {
        if (this.gTG) {
            C2776jq dud = asd.dud();
            if (!(dud instanceof C2989mg) || ((C2989mg) dud).isPressed()) {
                this.gTC.mo6466fQ();
            }
        }
    }

    @C2602hR(mo19255zf = "RADAR_OUT")
    /* renamed from: e */
    public void mo23914e(C5667aSd asd) {
        if (this.gTG) {
            C2776jq dud = asd.dud();
            if (!(dud instanceof C2989mg) || ((C2989mg) dud).isPressed()) {
                this.gTC.mo6467fR();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$i */
    /* compiled from: a */
    class C4465i extends C6124ags<C6898avm> {
        C4465i() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6898avm avm) {
            if (!(avm.mo16628ha() instanceof Shot)) {
                Hud2DSelectionAddon.this.m43663a(avm.mo16628ha().cMI());
                if (Hud2DSelectionAddon.this.gTC != null) {
                    Hud2DSelectionAddon.this.gTC.mo6462b(avm.mo16628ha());
                }
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$h */
    /* compiled from: a */
    class C4464h extends C6124ags<C2041bS> {
        C4464h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2041bS bSVar) {
            if (!(bSVar.mo17298ha() instanceof Shot)) {
                C6017aep cMI = bSVar.mo17298ha().cMI();
                if (cMI != null) {
                    Hud2DSelectionAddon.this.m43669b(cMI);
                } else {
                    Hud2DSelectionAddon.this.m43672be(bSVar.mo17298ha());
                }
                if (cMI != null) {
                    cMI.hide();
                }
                if (Hud2DSelectionAddon.this.gTC != null) {
                    Hud2DSelectionAddon.this.gTC.mo6464c(bSVar.mo17298ha());
                }
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$e */
    /* compiled from: a */
    class C4461e extends C6124ags<HudVisibilityAddon.C4534d> {
        C4461e() {
        }

        /* renamed from: b */
        public boolean updateInfo(HudVisibilityAddon.C4534d dVar) {
            if (!Hud2DSelectionAddon.this.f9904kj.getPlayer().bQB()) {
                if (Hud2DSelectionAddon.this.gTC != null) {
                    Hud2DSelectionAddon.this.gTC.mo6473r(dVar.isVisible());
                }
                if (dVar.isVisible()) {
                    Hud2DSelectionAddon.this.show();
                    if (Hud2DSelectionAddon.this.gTD != null) {
                        Hud2DSelectionAddon.this.gTD.setRender(true);
                    }
                } else {
                    if (Hud2DSelectionAddon.this.gTD != null) {
                        Hud2DSelectionAddon.this.gTD.setRender(false);
                    }
                    Hud2DSelectionAddon.this.hide();
                }
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$d */
    /* compiled from: a */
    class C4460d extends C6124ags<C3030nE> {
        C4460d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3030nE nEVar) {
            Hud2DSelectionAddon.this.cDA();
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$c */
    /* compiled from: a */
    class C4459c extends C6124ags<C5783aaP> {
        C4459c() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            if (aap.bMO() == C5783aaP.C1841a.DOCKED) {
                Hud2DSelectionAddon.this.cDx();
            } else {
                Hud2DSelectionAddon.this.cDy();
            }
            if (Hud2DSelectionAddon.this.gTC == null) {
                return false;
            }
            Hud2DSelectionAddon.this.gTC.mo6461a(aap.bMO());
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$b */
    /* compiled from: a */
    class C4458b extends WrapRunnable {
        C4458b() {
        }

        public void run() {
            Hud2DSelectionAddon.this.cDz();
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$g */
    /* compiled from: a */
    class C4463g extends C6124ags<C3853vo> {
        C4463g() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3853vo voVar) {
            Ship bQx = Hud2DSelectionAddon.this.f9904kj.getPlayer().bQx();
            if (bQx.agp() > bQx.agH().agp()) {
                if (Hud2DSelectionAddon.gTx == Hud2DSelectionAddon.gTu) {
                    return false;
                }
                Hud2DSelectionAddon.this.gTE.mo23916c(Hud2DSelectionAddon.gTx, Hud2DSelectionAddon.gTu);
                Hud2DSelectionAddon.gTx = Hud2DSelectionAddon.gTu;
                return false;
            } else if (bQx.agp() < bQx.agH().agp()) {
                if (Hud2DSelectionAddon.gTx == Hud2DSelectionAddon.gTw) {
                    return false;
                }
                Hud2DSelectionAddon.this.gTE.mo23916c(Hud2DSelectionAddon.gTx, Hud2DSelectionAddon.gTw);
                Hud2DSelectionAddon.gTx = Hud2DSelectionAddon.gTw;
                return false;
            } else if (Hud2DSelectionAddon.gTx == Hud2DSelectionAddon.gTv) {
                return false;
            } else {
                Hud2DSelectionAddon.this.gTE.mo23916c(Hud2DSelectionAddon.gTx, Hud2DSelectionAddon.gTv);
                Hud2DSelectionAddon.gTx = Hud2DSelectionAddon.gTv;
                return false;
            }
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$f */
    /* compiled from: a */
    class C4462f extends C6124ags<C3551sV> {
        C4462f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3551sV sVVar) {
            Hud2DSelectionAddon.this.gTC.mo6470g(sVVar.acv());
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.Hud2DSelectionAddon$a */
    private class C4457a extends WrapRunnable {

        /* renamed from: CZ */
        private float f9905CZ;

        /* renamed from: Da */
        private float f9906Da;
        private float start;

        private C4457a() {
        }

        /* synthetic */ C4457a(Hud2DSelectionAddon hud2DSelectionAddon, C4457a aVar) {
            this();
        }

        /* renamed from: c */
        public void mo23916c(float f, float f2) {
            this.start = f;
            this.f9905CZ = f2;
            Hud2DSelectionAddon.this.gTD.setPrimitiveColor(new Color(0.0f, 0.5f, 0.5f, 0.5f));
            this.f9906Da = this.start;
            Hud2DSelectionAddon.this.f9904kj.alf().addTask("radar anim", this, 50);
        }

        public void run() {
            double sin = Math.sin((double) ((this.f9905CZ - this.f9906Da) / Math.max(this.f9905CZ, this.start)));
            this.f9906Da = (float) (((double) this.f9906Da) + (25.0d * sin));
            if (Math.abs(sin) < 0.009999999776482582d) {
                Hud2DSelectionAddon.this.gTD.resetLine();
                Hud2DSelectionAddon.this.gTD.createCircle(new Vector2fWrap(0.0f, 0.0f), this.f9905CZ);
                for (C6017aep areaRadius : Hud2DSelectionAddon.this.gTy) {
                    areaRadius.setAreaRadius(this.f9905CZ);
                }
                cancel();
                Hud2DSelectionAddon.this.gTD.setPrimitiveColor(new Color(0.5f, 0.5f, 0.5f, 0.1f));
            }
            Hud2DSelectionAddon.this.gTD.resetLine();
            Hud2DSelectionAddon.this.gTD.createCircle(new Vector2fWrap(0.0f, 0.0f), this.f9906Da);
            for (C6017aep areaRadius2 : Hud2DSelectionAddon.this.gTy) {
                areaRadius2.setAreaRadius(this.f9906Da);
            }
        }
    }
}
