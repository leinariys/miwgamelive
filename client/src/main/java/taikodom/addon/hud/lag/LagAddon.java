package taikodom.addon.hud.lag;

import game.script.PingScript;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aVH;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import logic.ui.item.Picture;
import p001a.C0947Nt;
import p001a.C3582se;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.*;

@TaikodomAddon("taikodom.addon.hud.lag")
/* compiled from: a */
public class LagAddon implements C2495fo {
    private static final int MEDIUM = 150;
    private static final int byy = 400;
    /* access modifiers changed from: private */
    public long byu = 0;
    /* access modifiers changed from: private */
    public long byv = 0;
    /* access modifiers changed from: private */
    public String byw = "";
    /* access modifiers changed from: private */
    public String byx;
    /* renamed from: kj */
    public IAddonProperties f9944kj;
    private InternalFrame bwQ;
    private Picture bys;
    /* access modifiers changed from: private */
    private Picture byt;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9944kj = addonPropertie;
        this.bwQ = this.f9944kj.bHv().mo16792bN("lag.xml");
        this.bys = this.bwQ.mo4915cd("lag");
        this.byt = this.bwQ.mo4915cd("slag");
        this.bwQ.pack();
        Dimension preferredSize = this.bwQ.getPreferredSize();
        Dimension screenSize = this.f9944kj.bHv().getScreenSize();
        this.bwQ.setLocation(screenSize.width - preferredSize.width, screenSize.height - preferredSize.height);
        ComponentManager.getCssHolder(this.bwQ).mo13055aO(false);
        this.bwQ.setVisible(true);
        this.f9944kj.alf().addTask("Ping task", new C4519b(), 5000);
        this.f9944kj.aVU().mo13966a("rtt", Long.class, new C4520c());
        this.bwQ.setVisible(false);
    }

    public void stop() {
        if (this.bwQ != null) {
            this.bwQ.destroy();
        }
    }

    /* access modifiers changed from: protected */
    public void akp() {
        if (this.byu < 150) {
            ComponentManager.getCssHolder(this.bys).mo13047Vp().setColorMultiplier(new Color(0, 255, 0));
        } else if (this.byu < 400) {
            ComponentManager.getCssHolder(this.bys).mo13047Vp().setColorMultiplier(new Color(255, 255, 0));
        } else {
            ComponentManager.getCssHolder(this.bys).mo13047Vp().setColorMultiplier(new Color(255, 0, 0));
        }
        this.bys.repaint();
    }

    /* access modifiers changed from: protected */
    public void akq() {
        if (this.byv < 150) {
            ComponentManager.getCssHolder(this.byt).mo13047Vp().setColorMultiplier(new Color(0, 255, 0));
        } else if (this.byv < 400) {
            ComponentManager.getCssHolder(this.byt).mo13047Vp().setColorMultiplier(new Color(255, 255, 0));
        } else {
            ComponentManager.getCssHolder(this.byt).mo13047Vp().setColorMultiplier(new Color(255, 0, 0));
        }
        this.byt.repaint();
    }

    /* access modifiers changed from: protected */
    public void akr() {
        ((PingScript) C3582se.m38985a(this.f9944kj.ala().aJI(), (C6144ahM<?>) new C4518a(System.currentTimeMillis()))).mo21681cO(this.byv);
    }

    public String aks() {
        return this.byw;
    }

    /* renamed from: taikodom.addon.hud.lag.LagAddon$b */
    /* compiled from: a */
    class C4519b extends WrapRunnable {
        C4519b() {
        }

        public void run() {
            LagAddon.this.akr();
        }
    }

    /* renamed from: taikodom.addon.hud.lag.LagAddon$c */
    /* compiled from: a */
    class C4520c implements aVH<Long> {
        C4520c() {
        }

        /* renamed from: a */
        public void mo2004k(String str, Long l) {
            LagAddon.this.byv = l.longValue();
            LagAddon.this.byx = "ping: " + l + " ms";
        }

        /* renamed from: b */
        public void mo2003f(String str, Long l) {
        }
    }

    /* renamed from: taikodom.addon.hud.lag.LagAddon$a */
    class C4518a extends C0947Nt<PingScript> {
        private final /* synthetic */ long bwv;

        C4518a(long j) {
            this.bwv = j;
        }

        /* renamed from: a */
        public void mo4278E(PingScript rWVar) {
            LagAddon.this.byu = System.currentTimeMillis() - this.bwv;
            LagAddon.this.byw = String.valueOf(LagAddon.this.f9944kj.translate("server ping:")) + LagAddon.this.byu + " ms";
            if (LagAddon.this.byx != null) {
                LagAddon.this.byw = String.valueOf(LagAddon.this.byx) + ", " + LagAddon.this.byw;
            }
            LagAddon.this.akp();
            LagAddon.this.akq();
        }

        /* renamed from: c */
        public void mo4279c(Throwable th) {
        }
    }
}
