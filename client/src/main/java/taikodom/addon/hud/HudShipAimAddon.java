package taikodom.addon.hud;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aEE;
import game.script.Actor;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C3551sV;
import logic.aaa.C5623aQl;
import logic.aaa.C5783aaP;
import logic.render.IEngineGraphics;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;
import taikodom.render.SceneView;
import taikodom.render.gui.GAim;
import taikodom.render.gui.GuiScene;

@TaikodomAddon("taikodom.addon.hud")
/* compiled from: a */
public class HudShipAimAddon implements C2495fo {

    /* access modifiers changed from: private */
    public boolean amO = false;
    /* access modifiers changed from: private */
    public GAim bKo;
    /* access modifiers changed from: private */
    public GAim bKp;
    /* access modifiers changed from: private */
    public Actor bKs;
    public boolean isLocked = false;
    /* renamed from: kj */
    public IAddonProperties f9929kj;
    C6296akI.C1925a bKt;
    /* renamed from: Bu */
    private C6124ags<C5783aaP> f9928Bu = new C3091nf(this);
    private C6124ags<HudVisibilityAddon.C4534d> bKq;
    private C6124ags<aEE> bKr;
    private C6124ags<C3551sV> bKu = new C3093nh(this);
    /* access modifiers changed from: private */
    private C6124ags<C5623aQl> bKv = new C3092ng(this);

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9929kj = addonPropertie;
        this.f9929kj.aVU().mo13965a(C5783aaP.class, this.f9928Bu);
        this.f9929kj.aVU().mo13965a(C3551sV.class, this.bKu);
        this.f9929kj.aVU().mo13965a(C5623aQl.class, this.bKv);
        this.f9929kj.aVU().mo13965a(C5623aQl.class, this.bKv);
        this.bKt = new C4484c();
        this.f9929kj.ale().mo3004a(this.bKt);
        this.bKq = new C4485d();
        this.bKr = new C4482a();
        this.f9929kj.aVU().mo13965a(aEE.class, this.bKr);
        this.f9929kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.bKq);
        if (this.f9929kj.getPlayer() != null && !this.f9929kj.getPlayer().bQB()) {
            m43783fP();
        }
    }

    /* access modifiers changed from: protected */
    public void show() {
        if (!this.amO && this.f9929kj.getPlayer() != null && this.f9929kj.getPlayer().bQx() != null) {
            if (this.f9929kj.getPlayer().bQx().agB() == null || !(this.f9929kj.getPlayer().bQx().agB() instanceof Ship)) {
                apI();
            } else {
                apK();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: fP */
    public void m43783fP() {
        if (!apH()) {
            this.f9929kj.alf().addTask("A", new C4483b(), 100);
        }
        this.f9929kj.alf().addTask("AimUpdateTask", new C4486e(), 10);
    }

    /* access modifiers changed from: private */
    public boolean apH() {
        if (this.f9929kj.ala() == null) {
            return false;
        }
        Ship bQx = this.f9929kj.ala().getPlayer().bQx();
        if (bQx == null) {
            return false;
        }
        IEngineGraphics ale = this.f9929kj.ale();
        GuiScene aeh = ale.aeh();
        if (this.bKo == null) {
            this.bKo = (GAim) ale.mo3047bT("hud_target_aim");
            aeh.addChild(this.bKo);
        } else {
            aeh.removeChild(this.bKo);
            aeh.addChild(this.bKo);
        }
        if (this.bKp == null) {
            this.bKp = (GAim) ale.mo3047bT("hud_infinity_aim");
            aeh.addChild(this.bKp);
        } else {
            aeh.removeChild(this.bKp);
            aeh.addChild(this.bKp);
        }
        if (bQx.cHg() == null) {
            return false;
        }
        SceneView aea = this.f9929kj.ale().aea();
        if (this.bKo != null) {
            this.bKo.setShip(bQx.cHg(), aea);
            this.bKo.setTarget(bQx.cHg());
            this.bKo.setProjectionDistance(200000.0f);
        }
        if (this.bKp != null) {
            this.bKp.setShip(bQx.cHg(), aea);
            this.bKp.setProjectionDistance(200000.0f);
        }
        apI();
        return true;
    }

    public void stop() {
        m43782fO();
        this.f9929kj.ale().mo3044b(this.bKt);
        this.f9929kj.aVU().mo13968b(aEE.class, this.bKr);
        this.f9929kj.aVU().mo13968b(HudVisibilityAddon.C4534d.class, this.bKq);
    }

    /* access modifiers changed from: private */
    /* renamed from: fO */
    public void m43782fO() {
        if (this.bKo != null) {
            this.bKo.dispose();
            this.bKo = null;
        }
        if (this.bKp != null) {
            this.bKp.dispose();
            this.bKp = null;
        }
    }

    public C6622aqW ajm() {
        return null;
    }

    /* access modifiers changed from: private */
    public void apI() {
        if (this.bKp != null) {
            this.bKp.setRender(true);
        }
        if (this.bKo != null) {
            this.bKo.setRender(false);
        }
        this.f9929kj.aVU().mo13975h(new C1000Ok(C1000Ok.C1001a.STOP));
    }

    /* access modifiers changed from: private */
    public void apJ() {
        if (this.bKp != null) {
            this.bKp.setRender(false);
        }
        if (this.bKo != null) {
            this.bKo.setRender(false);
        }
        this.f9929kj.aVU().mo13975h(new C1000Ok(C1000Ok.C1001a.STOP));
    }

    /* access modifiers changed from: private */
    public void apK() {
        if (this.bKo != null) {
            this.bKo.setRender(true);
        }
        if (this.bKp != null) {
            this.bKp.setRender(false);
        }
        this.f9929kj.aVU().mo13975h(new C1000Ok(C1000Ok.C1001a.START));
    }

    /* renamed from: taikodom.addon.hud.HudShipAimAddon$c */
    /* compiled from: a */
    class C4484c implements C6296akI.C1925a {
        C4484c() {
        }

        /* renamed from: a */
        public void mo1968a(float f, long j) {
            if (HudShipAimAddon.this.bKo != null) {
                if (!HudShipAimAddon.this.isLocked) {
                    HudShipAimAddon.this.bKo.setPrimitiveColor(1.0f, 1.0f, 1.0f, 1.0f);
                } else if (HudShipAimAddon.this.bKs == null || HudShipAimAddon.this.bKs.cLd() == null || System.currentTimeMillis() - HudShipAimAddon.this.bKs.cLd().aSq() <= 1000) {
                    HudShipAimAddon.this.bKo.setPrimitiveColor(1.0f, 0.0f, 0.0f, 1.0f);
                } else {
                    HudShipAimAddon.this.bKo.setPrimitiveColor(1.0f, 0.5f, 0.0f, 1.0f);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipAimAddon$d */
    /* compiled from: a */
    class C4485d extends C6124ags<HudVisibilityAddon.C4534d> {
        C4485d() {
        }

        /* renamed from: b */
        public boolean updateInfo(HudVisibilityAddon.C4534d dVar) {
            HudShipAimAddon.this.amO = !dVar.isVisible();
            if (HudShipAimAddon.this.amO) {
                HudShipAimAddon.this.apJ();
            } else {
                HudShipAimAddon.this.show();
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipAimAddon$a */
    class C4482a extends C6124ags<aEE> {
        C4482a() {
        }

        /* renamed from: b */
        public boolean updateInfo(aEE aee) {
            if (!HudShipAimAddon.this.f9929kj.getPlayer().bQB()) {
                if (aee.cXT()) {
                    HudShipAimAddon.this.apJ();
                } else if (HudShipAimAddon.this.f9929kj.getPlayer().bQx().agB() == null || !(HudShipAimAddon.this.f9929kj.getPlayer().bQx().agB() instanceof Ship)) {
                    HudShipAimAddon.this.apI();
                } else {
                    HudShipAimAddon.this.apK();
                }
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipAimAddon$b */
    /* compiled from: a */
    class C4483b extends WrapRunnable {
        C4483b() {
        }

        public void run() {
            Ship bQx;
            Taikodom ala = HudShipAimAddon.this.f9929kj.ala();
            if (ala != null && (bQx = ala.aPC().bQx()) != null && bQx.cHg() != null && HudShipAimAddon.this.apH()) {
                cancel();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.HudShipAimAddon$e */
    /* compiled from: a */
    public class C4486e extends WrapRunnable {
        public C4486e() {
        }

        public void run() {
            if (HudShipAimAddon.this.f9929kj.getPlayer() != null) {
                if (HudShipAimAddon.this.f9929kj.getPlayer().bQB()) {
                    cancel();
                } else if (HudShipAimAddon.this.f9929kj.getPlayer().bQx() != null && HudShipAimAddon.this.bKp != null && HudShipAimAddon.this.bKo != null) {
                    Vec3f aip = HudShipAimAddon.this.f9929kj.getPlayer().bQx().aip();
                    HudShipAimAddon.this.bKp.setZTransform(aip);
                    HudShipAimAddon.this.bKo.setZTransform(aip);
                }
            }
        }
    }
}
