package taikodom.addon.hud.buff;

import game.network.message.externalizable.C2651i;
import game.network.message.externalizable.C5344aFs;
import game.script.item.Component;
import game.script.item.Module;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.categories.Bomber;
import game.script.ship.categories.Explorer;
import game.script.ship.categories.Fighter;
import game.script.ship.categories.Freighter;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.C0019AI;
import logic.aaa.C1399UW;
import logic.aaa.C6484ano;
import logic.aaa.aIP;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.util.HashMap;
import java.util.Map;

@TaikodomAddon("taikodom.addon.hud.buff")
/* compiled from: a */
public class BuffBarAddon implements C2495fo {
    /* access modifiers changed from: private */
    public Map<Module, C4506b> gtW = new HashMap();
    /* access modifiers changed from: private */
    public C4504a gtZ;
    /* renamed from: kj */
    public IAddonProperties f9938kj;
    private C6124ags<C2651i> gtS;
    private C6124ags<C6484ano> gtT;
    private C6124ags<C0019AI> gtU;
    private Module[] gtV = new Module[8];
    private Map<Module, C4506b> gtX = new HashMap();
    private C4508c gtY;
    /* access modifiers changed from: private */
    private C5344aFs gua;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9938kj = addonPropertie;
        ctA();
        this.gtS = new C4513g();
        this.gtT = new C4514h();
        this.gtU = new C4515i();
        C6245ajJ aVU = this.f9938kj.aVU();
        aVU.mo13965a(C6484ano.class, this.gtT);
        aVU.mo13965a(C0019AI.class, this.gtU);
        aVU.mo13965a(C2651i.class, this.gtS);
        this.f9938kj.mo2317P(this);
    }

    /* access modifiers changed from: private */
    public void ctA() {
        ctC();
        ctB();
    }

    /* renamed from: a */
    private void m43880a(Module yAVar, int i) {
        this.gtV[i] = yAVar;
        if (this.gtW.containsKey(yAVar)) {
            System.err.println("HUDBAR - booger, something here does not seem right");
            return;
        }
        if (!this.gtX.containsKey(yAVar)) {
            C4506b bVar = new C4506b(yAVar, "css-dunno", this.f9938kj.mo2324af("MODULE" + (i + 1)), yAVar.cJS().mo19891ke().get());
            this.gtW.put(yAVar, bVar);
            this.f9938kj.aVU().publish(new C5344aFs(bVar, i + 2, false));
        } else {
            this.gtW.put(yAVar, this.gtX.remove(yAVar));
        }
        System.out.println("module added: " + yAVar);
    }

    public void ctB() {
        this.gtV = new Module[8];
        this.gtX.putAll(this.gtW);
        this.gtW.clear();
        if (this.f9938kj.getPlayer() != null && this.f9938kj.getPlayer().bQx() != null) {
            int i = 0;
            for (Component abl : this.f9938kj.getPlayer().bQx().agn()) {
                if (abl instanceof Module) {
                    m43880a((Module) abl, i);
                    i++;
                }
            }
        }
    }

    public void ctC() {
        if (this.f9938kj != null && this.f9938kj.getPlayer() != null) {
            if (this.gtY == null) {
                this.gtY = new C4508c("css-dunno", this.f9938kj.translate("Cruise Speed"), this.f9938kj.mo2324af("SPECIAL" + 1));
                this.f9938kj.aVU().publish(new C5344aFs(this.gtY, 0, false));
            }
            m43884ul(1);
        }
    }

    /* renamed from: ul */
    private void m43884ul(int i) {
        Ship bQx = this.f9938kj.getPlayer().bQx();
        if (bQx != null) {
            String str = "";
            if (bQx instanceof Fighter) {
                str = this.f9938kj.translate("fighter.special");
            } else if (bQx instanceof Bomber) {
                str = this.f9938kj.translate("bomber.special");
            } else if (bQx instanceof Explorer) {
                str = this.f9938kj.translate("explorer.special");
            } else if (bQx instanceof Freighter) {
                str = this.f9938kj.translate("freighter.special");
            }
            if (this.gua != null) {
                i = this.gua.getOrder();
                this.f9938kj.aVU().mo13974g(this.gua);
                this.gua = null;
            }
            if (this.gtZ == null) {
                this.gtZ = new C4504a("css-dunno", str, this.f9938kj.mo2324af("SPECIAL" + (i + 1)));
                this.gua = new C5344aFs(this.gtZ, i, false);
                this.f9938kj.aVU().publish(this.gua);
            }
        }
    }

    public void stop() {
        C6245ajJ aVU = this.f9938kj.aVU();
        aVU.mo13968b(C2651i.class, this.gtS);
        aVU.mo13968b(C6484ano.class, this.gtT);
        aVU.mo13968b(C0019AI.class, this.gtU);
        this.f9938kj.mo2318Q(this);
    }

    @C2602hR(mo19255zf = "SPECIAL1_ON", mo19256zg = "modules")
    /* renamed from: gh */
    public void mo23995gh(boolean z) {
        if (!this.f9938kj.getPlayer().bQB() && z) {
            if (this.f9938kj.alb().cPb()) {
                this.f9938kj.alb().cPd();
            }
            Ship bQx = this.f9938kj.getPlayer().bQx();
            if (!bQx.ahb()) {
                bQx.mo18307bv(false);
            }
            this.f9938kj.getPlayer().dxc().mo22144ik(true);
        }
    }

    @C2602hR(mo19255zf = "SPECIAL1_OFF", mo19256zg = "modules")
    /* renamed from: gi */
    public void mo23996gi(boolean z) {
        if (!this.f9938kj.getPlayer().bQB() && z) {
            if (this.f9938kj.alb().cPb()) {
                this.f9938kj.alb().cPd();
            }
            this.f9938kj.getPlayer().dxc().mo22144ik(false);
        }
    }

    @C2602hR(mo19255zf = "SPECIAL1", mo19256zg = "modules")
    /* renamed from: gj */
    public void mo23997gj(boolean z) {
        if (!this.f9938kj.getPlayer().bQB() && z) {
            if (this.f9938kj.alb().cPb()) {
                this.f9938kj.alb().cPd();
            }
            Ship bQx = this.f9938kj.getPlayer().bQx();
            if (!bQx.ahb()) {
                bQx.mo18307bv(false);
            }
            this.f9938kj.getPlayer().dxc().cNf();
            if (bQx.afL().acZ().equals(C6809auB.C1996a.DEACTIVE)) {
                this.f9938kj.aVU().mo13975h(new aIP(C6809auB.C1996a.WARMUP));
            } else if (bQx.afL().acZ().equals(C6809auB.C1996a.ACTIVE)) {
                this.f9938kj.aVU().mo13975h(new aIP(C6809auB.C1996a.COOLDOWN));
            }
            this.f9938kj.aVU().mo13975h(new C1399UW(new C4510d()));
        }
    }

    @C2602hR(mo19255zf = "SPECIAL2", mo19256zg = "modules")
    /* renamed from: gk */
    public void mo23998gk(boolean z) {
        if (!this.f9938kj.getPlayer().bQB()) {
            Ship bQx = this.f9938kj.getPlayer().bQx();
            if (bQx instanceof Fighter) {
                if (this.f9938kj.alb().cPb()) {
                    this.f9938kj.alb().cPd();
                }
                Fighter pFVar = (Fighter) bQx;
                if (z) {
                    pFVar.activate();
                } else {
                    pFVar.deactivate();
                }
            } else if (z) {
                if (this.f9938kj.alb().cPb()) {
                    this.f9938kj.alb().cPd();
                }
                bQx.activate();
                this.f9938kj.aVU().mo13975h(new C1399UW(new C4511e(bQx)));
            }
        }
    }

    @C2602hR(mo19255zf = "MODULE1", mo19256zg = "modules")
    /* renamed from: gl */
    public void mo23999gl(boolean z) {
        mo24007p("MODULE1", z);
    }

    @C2602hR(mo19255zf = "MODULE2", mo19256zg = "modules")
    /* renamed from: gm */
    public void mo24000gm(boolean z) {
        mo24007p("MODULE2", z);
    }

    @C2602hR(mo19255zf = "MODULE3", mo19256zg = "modules")
    /* renamed from: gn */
    public void mo24001gn(boolean z) {
        mo24007p("MODULE3", z);
    }

    @C2602hR(mo19255zf = "MODULE4", mo19256zg = "modules")
    /* renamed from: go */
    public void mo24002go(boolean z) {
        mo24007p("MODULE4", z);
    }

    @C2602hR(mo19255zf = "MODULE5", mo19256zg = "modules")
    /* renamed from: gp */
    public void mo24003gp(boolean z) {
        mo24007p("MODULE5", z);
    }

    @C2602hR(mo19255zf = "MODULE6", mo19256zg = "modules")
    /* renamed from: gq */
    public void mo24004gq(boolean z) {
        mo24007p("MODULE6", z);
    }

    @C2602hR(mo19255zf = "MODULE7", mo19256zg = "modules")
    /* renamed from: gr */
    public void mo24005gr(boolean z) {
        mo24007p("MODULE7", z);
    }

    @C2602hR(mo19255zf = "MODULE8", mo19256zg = "modules")
    /* renamed from: gs */
    public void mo24006gs(boolean z) {
        mo24007p("MODULE8", z);
    }

    /* renamed from: p */
    public void mo24007p(String str, boolean z) {
        int intValue;
        Module yAVar;
        if (str.length() >= 6 && str.substring(0, 6).equals("MODULE") && new Integer(str.substring(6, 7)).intValue() - 1 >= 0 && !this.f9938kj.getPlayer().bQB() && (yAVar = this.gtV[intValue]) != null && z) {
            if (this.f9938kj.alb().cPb()) {
                this.f9938kj.alb().cPd();
            }
            yAVar.activate();
            this.f9938kj.aVU().mo13975h(new C1399UW(new C4512f(yAVar)));
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$j */
    /* compiled from: a */
    public abstract class C4516j extends C6622aqW {
        public C4516j(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return false;
        }

        public boolean isEnabled() {
            Player aPC = BuffBarAddon.this.f9938kj.ala().getPlayer();
            if (aPC.bQx() == null || aPC.bQB()) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$b */
    /* compiled from: a */
    public class C4506b extends C4516j {

        /* access modifiers changed from: private */
        public Module dhG;

        public C4506b(Module yAVar, String str, String str2, String str3) {
            super(str, str2, str3);
            this.dhG = yAVar;
        }

        public boolean isEnabled() {
            return super.isEnabled() && BuffBarAddon.this.gtW.containsKey(this.dhG);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            this.dhG.activate();
            BuffBarAddon.this.f9938kj.aVU().mo13975h(new C1399UW(new C4507a()));
        }

        /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$b$a */
        class C4507a implements C0375FA {
            C4507a() {
            }

            /* renamed from: kT */
            public void mo2065kT() {
                C4506b.this.dhG.abort();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$c */
    /* compiled from: a */
    public class C4508c extends C4516j {


        public C4508c(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isEnabled() {
            return super.isEnabled() && BuffBarAddon.this.f9938kj.ala().getPlayer().bQx().afL() != null;
        }

        public boolean isActive() {
            Ship bQx = BuffBarAddon.this.f9938kj.getPlayer().bQx();
            if (bQx != null) {
                return bQx.agZ();
            }
            return false;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            Ship bQx = BuffBarAddon.this.f9938kj.ala().getPlayer().bQx();
            ((PlayerController) bQx.mo2998hb()).cNf();
            BuffBarAddon.this.f9938kj.aVU().mo13975h(new C1399UW(new C4509a(bQx)));
        }

        /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$c$a */
        class C4509a implements C0375FA {

            /* renamed from: Bn */
            private final /* synthetic */ Ship f9941Bn;

            C4509a(Ship fAVar) {
                this.f9941Bn = fAVar;
            }

            /* renamed from: kT */
            public void mo2065kT() {
                this.f9941Bn.afC();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$a */
    public class C4504a extends C4516j {


        public C4504a(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isEnabled() {
            BuffBarAddon.this.f9938kj.ala().getPlayer();
            return super.isEnabled();
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            Ship bQx = BuffBarAddon.this.f9938kj.ala().getPlayer().bQx();
            bQx.activate();
            BuffBarAddon.this.f9938kj.aVU().mo13975h(new C1399UW(new C4505a(bQx)));
        }

        /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$a$a */
        class C4505a implements C0375FA {

            /* renamed from: Bn */
            private final /* synthetic */ Ship f9940Bn;

            C4505a(Ship fAVar) {
                this.f9940Bn = fAVar;
            }

            /* renamed from: kT */
            public void mo2065kT() {
                this.f9940Bn.activate();
            }
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$g */
    /* compiled from: a */
    class C4513g extends C6124ags<C2651i> {
        C4513g() {
        }

        /* renamed from: b */
        public boolean updateInfo(C2651i iVar) {
            BuffBarAddon.this.gtZ = null;
            BuffBarAddon.this.ctA();
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$h */
    /* compiled from: a */
    class C4514h extends C6124ags<C6484ano> {
        C4514h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6484ano ano) {
            if (!(ano.ciQ instanceof Module)) {
                return false;
            }
            BuffBarAddon.this.ctA();
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$i */
    /* compiled from: a */
    class C4515i extends C6124ags<C0019AI> {
        C4515i() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0019AI ai) {
            if (!(ai.ciQ instanceof Module)) {
                return false;
            }
            BuffBarAddon.this.ctA();
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$d */
    /* compiled from: a */
    class C4510d implements C0375FA {
        C4510d() {
        }

        /* renamed from: kT */
        public void mo2065kT() {
            BuffBarAddon.this.f9938kj.getPlayer().bQx().afL().abort();
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$e */
    /* compiled from: a */
    class C4511e implements C0375FA {

        /* renamed from: Bn */
        private final /* synthetic */ Ship f9942Bn;

        C4511e(Ship fAVar) {
            this.f9942Bn = fAVar;
        }

        /* renamed from: kT */
        public void mo2065kT() {
            this.f9942Bn.abort();
        }
    }

    /* renamed from: taikodom.addon.hud.buff.BuffBarAddon$f */
    /* compiled from: a */
    class C4512f implements C0375FA {
        private final /* synthetic */ Module idi;

        C4512f(Module yAVar) {
            this.idi = yAVar;
        }

        /* renamed from: kT */
        public void mo2065kT() {
            this.idi.abort();
        }
    }
}
