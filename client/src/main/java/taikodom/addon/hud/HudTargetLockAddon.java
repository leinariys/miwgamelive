package taikodom.addon.hud;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.C0813Lk;
import game.script.Actor;
import game.script.item.Weapon;
import game.script.missile.MissileWeapon;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.*;
import p001a.C6124ags;
import p001a.aTL;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.hud")
/* compiled from: a */
public class HudTargetLockAddon implements C2495fo {

    /* access modifiers changed from: private */
    public Actor ams;
    /* access modifiers changed from: private */
    public float gYi = 3.7f;
    /* access modifiers changed from: private */
    public boolean gYj;
    /* access modifiers changed from: private */
    public boolean gYk;
    /* access modifiers changed from: private */
    public boolean gYp = false;
    /* access modifiers changed from: private */
    public boolean gYq = false;
    /* access modifiers changed from: private */
    public Weapon gYr;
    /* access modifiers changed from: private */
    public Weapon gYs;
    /* renamed from: kj */
    public IAddonProperties f9935kj;
    public Vec3f gYh = new Vec3f(0.0f, 0.0f, 0.0f);
    WrapRunnable gYu = new aTL(this);
    /* renamed from: Bv */
    private C6124ags<C2996mm> f9934Bv;
    private C6124ags<C3551sV> amr;
    private C6124ags<C2208cm> gYl;
    private C6124ags<C2028bG> gYm;
    private C6124ags<C6831auX> gYn;
    private C6124ags<C0797LY> gYo;
    /* access modifiers changed from: private */
    private C6124ags<C5783aaP> gYt;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9935kj = addonPropertie;
        this.amr = new C4494a();
        this.gYm = new C4496c();
        this.gYn = new C4495b();
        this.gYl = new C4498e();
        this.gYo = new C4497d();
        this.f9934Bv = new C4500g();
        this.gYt = new C4499f();
        this.f9935kj.aVU().mo13965a(C5783aaP.class, this.gYt);
        this.f9935kj.aVU().mo13965a(C2996mm.class, this.f9934Bv);
        this.f9935kj.aVU().mo13965a(C0797LY.class, this.gYo);
        this.f9935kj.aVU().mo13965a(C6831auX.class, this.gYn);
        this.f9935kj.aVU().mo13965a(C3551sV.class, this.amr);
        this.f9935kj.aVU().mo13965a(C2208cm.class, this.gYl);
        this.f9935kj.aVU().mo13965a(C2028bG.class, this.gYm);
        this.f9935kj.alf().addTask("UpdateDistanceTask", this.gYu, 50);
    }

    public void stop() {
        this.gYu.cancel();
        this.f9935kj.aVU().mo13964a(this.gYt);
        this.f9935kj.aVU().mo13964a(this.f9934Bv);
        this.f9935kj.aVU().mo13964a(this.gYo);
        this.f9935kj.aVU().mo13964a(this.gYn);
        this.f9935kj.aVU().mo13964a(this.amr);
        this.f9935kj.aVU().mo13964a(this.gYl);
        this.f9935kj.aVU().mo13964a(this.gYm);
    }

    /* access modifiers changed from: protected */
    public void cFu() {
        if (this.f9935kj.getPlayer() != null) {
            Ship bQx = this.f9935kj.getPlayer().bQx();
            if (bQx != null) {
                mo23982v(bQx.afT());
                m43845u(bQx.afR());
                return;
            }
            m43845u((Weapon) null);
            mo23982v((Weapon) null);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: u */
    public void m43845u(Weapon adv) {
        this.gYs = adv;
        this.gYj = false;
        this.gYp = false;
        this.f9935kj.aVU().mo13975h(new C5623aQl(false));
    }

    /* access modifiers changed from: protected */
    /* renamed from: v */
    public void mo23982v(Weapon adv) {
        this.gYr = adv;
        if (adv != null && (adv instanceof MissileWeapon)) {
            this.f9935kj.aVU().mo13975h(new C0813Lk(false, (MissileWeapon) adv));
        }
        this.gYq = false;
        this.gYk = false;
    }

    /* renamed from: taikodom.addon.hud.HudTargetLockAddon$a */
    class C4494a extends C6124ags<C3551sV> {
        C4494a() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3551sV sVVar) {
            HudTargetLockAddon.this.ams = sVVar.acv();
            HudTargetLockAddon.this.cFu();
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudTargetLockAddon$c */
    /* compiled from: a */
    class C4496c extends C6124ags<C2028bG> {
        C4496c() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2028bG bGVar) {
            HudTargetLockAddon.this.mo23982v(bGVar.mo17251gB());
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudTargetLockAddon$b */
    /* compiled from: a */
    class C4495b extends C6124ags<C6831auX> {
        C4495b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6831auX aux) {
            HudTargetLockAddon.this.mo23982v(aux.afT());
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudTargetLockAddon$e */
    /* compiled from: a */
    class C4498e extends C6124ags<C2208cm> {
        C4498e() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2208cm cmVar) {
            HudTargetLockAddon.this.m43845u(cmVar.mo17679gB());
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudTargetLockAddon$d */
    /* compiled from: a */
    class C4497d extends C6124ags<C0797LY> {
        C4497d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0797LY ly) {
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudTargetLockAddon$g */
    /* compiled from: a */
    class C4500g extends C6124ags<C2996mm> {
        C4500g() {
        }

        /* renamed from: b */
        public boolean updateInfo(C2996mm mmVar) {
            HudTargetLockAddon.this.cFu();
            return false;
        }
    }

    /* renamed from: taikodom.addon.hud.HudTargetLockAddon$f */
    /* compiled from: a */
    class C4499f extends C6124ags<C5783aaP> {
        C4499f() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            HudTargetLockAddon.this.cFu();
            return false;
        }
    }
}
