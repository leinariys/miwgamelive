package taikodom.addon.hud.ghost;

import com.hoplon.geometry.Color;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.serializable.C6400amI;
import game.script.item.Weapon;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.render.IEngineGraphics;
import p001a.C1000Ok;
import p001a.C1583XH;
import p001a.C6124ags;
import p001a.C6901avp;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.gui.GHudMark;
import taikodom.render.gui.GuiScene;
import taikodom.render.scene.SceneObject;

@TaikodomAddon("taikodom.addon.hud.ghost")
/* compiled from: a */
public class HudGhostAddon extends WrapRunnable implements C2495fo {
    /* access modifiers changed from: private */
    public C1583XH hbF;
    /* renamed from: kj */
    public IAddonProperties f9943kj;
    private Ship bMA;
    private Ship hbA;
    private C6901avp hbB;
    private C6901avp hbC;
    private C6400amI hbD = new C6400amI();
    private boolean hbE;
    private SceneObject hbG;
    private GHudMark hbH;
    /* access modifiers changed from: private */
    private C6124ags<C1000Ok> hbz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9943kj = addonPropertie;
        refresh();
        this.hbD = new C6400amI();
        this.hbG = new SceneObject();
        this.hbF = new C1583XH();
        this.hbz = new C4517a();
        this.f9943kj.aVU().mo13965a(C1000Ok.class, this.hbz);
    }

    public void stop() {
        this.f9943kj.aVU().mo13964a(this.hbz);
        cancel();
        dispose();
    }

    public void cHe() {
        IEngineGraphics ale = this.f9943kj.ale();
        GuiScene aeh = ale.aeh();
        if (this.hbH == null) {
            this.hbH = (GHudMark) ale.mo3047bT("target_aimprediction");
            this.hbH.setShowWhenOffscreen(false);
            this.hbH.setSmooth(true);
            Color color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            Color color2 = new Color(0.5f, 0.5f, 0.5f, 1.0f);
            this.hbH.setCenterColor(color);
            this.hbH.setOutColor(color2);
            if (this.hbH != null) {
                aeh.addChild(this.hbH);
            } else {
                return;
            }
        } else {
            aeh.removeChild(this.hbH);
            aeh.addChild(this.hbH);
        }
        this.hbH.setTarget(this.hbG, ale.aea());
        this.hbH.setRender(false);
    }

    /* access modifiers changed from: private */
    public void setRender(boolean z) {
        if (this.hbH != null) {
            if (z && !this.hbH.isRender()) {
                this.hbH.jump();
            }
            this.hbH.setRender(z);
        }
    }

    public boolean cHf() {
        return this.hbE;
    }

    public void refresh() {
        Ship bQx;
        Ship fAVar;
        Player dL = this.f9943kj.getPlayer();
        if (dL != null && (bQx = dL.bQx()) != null && (bQx.agB() instanceof Ship)) {
            this.hbA = bQx;
            if (bQx.agB() instanceof Ship) {
                fAVar = (Ship) bQx.agB();
            } else {
                fAVar = null;
            }
            this.bMA = fAVar;
            this.hbE = false;
            setRender(false);
        }
    }

    public SceneObject cHg() {
        return this.hbG;
    }

    public void run() {
        boolean z;
        if (this.bMA != null && this.hbA != null) {
            this.hbB = this.bMA.ahO();
            this.hbC = this.hbA.ahO();
            if (this.hbB == null || this.hbC == null || this.hbB.isDisposed() || this.hbC.isDisposed() || this.bMA.isDead() || this.hbA.isDead() || !this.bMA.cLZ() || !this.hbA.cLZ() || !this.bMA.bae() || !this.hbA.bae()) {
                setRender(false);
                return;
            }
            Weapon afR = this.hbA.afR();
            if (afR == null) {
                setRender(false);
                return;
            }
            float in = afR.mo12940in();
            float ax = (float) this.hbB.getPosition().mo9491ax(this.hbC.getPosition());
            if (in > ax) {
                z = true;
            } else {
                z = false;
            }
            m43919hB(z);
            if (!this.hbE) {
                setRender(false);
                return;
            }
            ((C6400amI) this.hbB.mo2472kQ()).mo14818b(ax / afR.getSpeed(), this.hbD);
            Vec3d blk = this.hbD.blk();
            Quat4fWrap bll = this.hbD.bll();
            if (!bll.anA() || !blk.anA()) {
                setRender(false);
                return;
            }
            this.hbF.mo6702H(this.bMA);
            this.hbF.mo6703b(this.hbD);
            this.hbG.setTransform(blk, bll);
            if (this.hbB.mo2571qZ().anA()) {
                this.hbG.setVelocity(this.hbB.mo2571qZ());
            }
            if (this.hbB.aSf().anA()) {
                this.hbG.setAngularVelocity(this.hbB.aSf());
            }
            setRender(true);
        }
    }

    /* renamed from: hB */
    private void m43919hB(boolean z) {
        if (this.hbH != null) {
            if (!this.hbE && z) {
                setRender(true);
            } else if (this.hbE && !z) {
                setRender(false);
            }
            this.hbE = z;
        }
    }

    public void dispose() {
        if (this.hbH != null) {
            this.hbH.setRender(false);
            this.hbH.dispose();
            this.hbH = null;
        }
    }

    /* renamed from: taikodom.addon.hud.ghost.HudGhostAddon$a */
    class C4517a extends C6124ags<C1000Ok> {
        private static /* synthetic */ int[] elR;

        C4517a() {
        }

        static /* synthetic */ int[] bwH() {
            int[] iArr = elR;
            if (iArr == null) {
                iArr = new int[C1000Ok.C1001a.values().length];
                try {
                    iArr[C1000Ok.C1001a.START.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C1000Ok.C1001a.STOP.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                elR = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public boolean updateInfo(C1000Ok ok) {
            switch (bwH()[ok.blh().ordinal()]) {
                case 1:
                    HudGhostAddon.this.cHe();
                    HudGhostAddon.this.refresh();
                    HudGhostAddon.this.f9943kj.aVU().publish(HudGhostAddon.this.hbF);
                    HudGhostAddon.this.f9943kj.alf().addTask("Prediction Task", HudGhostAddon.this, 10);
                    break;
                case 2:
                    HudGhostAddon.this.setRender(false);
                    HudGhostAddon.this.cancel();
                    HudGhostAddon.this.f9943kj.aVU().mo13974g(HudGhostAddon.this.hbF);
                    break;
            }
            return false;
        }
    }
}
