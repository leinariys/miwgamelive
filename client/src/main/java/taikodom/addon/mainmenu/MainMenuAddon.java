package taikodom.addon.mainmenu;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationRoleInfo;
import game.script.player.Player;
import game.script.progression.CharacterEvolution;
import game.script.progression.CharacterProgression;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.*;
import logic.baa.C0211Cc;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.data.link.C1092Px;
import logic.data.link.C1643YC;
import logic.data.link.aEX;
import logic.res.code.C5663aRz;
import logic.swing.C0454GJ;
import logic.swing.C5378aHa;
import logic.swing.aDX;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.item.C2830kk;
import logic.ui.item.Picture;
import p001a.C0160Br;
import p001a.C3663tS;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;

@TaikodomAddon("taikodom.addon.mainmenu")
/* compiled from: a */
public class MainMenuAddon implements C2495fo {
    private static final String hvA = "corp-role-image";
    private static final String hvB = "corp-role-name";
    private static final String hvC = "balance";
    private static final String hvr = "negative-balance";
    private static final String hvs = "positive-balance";
    private static final String hvt = "credit";
    private static final String hvu = "transition";
    private static final String hvw = "mainmenu.xml";
    private static final String hvx = "player-name";
    private static final String hvy = "corporation-image";
    private static final String hvz = "corporation-name";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player player;
    /* access modifiers changed from: private */
    public JLabel gnU;
    /* access modifiers changed from: private */
    public aDX hvO;
    /* access modifiers changed from: private */
    public C4680a hvv;
    /* renamed from: kj */
    public IAddonProperties addonProperties;
    private C2698il aoL;
    private C6124ags<C1274Sq> cUd = new C4687g();
    private C0454GJ clN = new C4696p();
    private C6124ags<C3949xF> glE = new C4689i();
    private C0211Cc gnN;
    private C6200aiQ<aDJ> hvD = new C4683c();
    private C6124ags<HudVisibilityAddon.C4534d> hvE = new C4690j();
    private C6124ags<C2327eB> hvF = new C4685e();
    private C6124ags<C1532WW> hvG = new C4686f();
    private C6124ags<C5344aFs> hvH = new C4688h();
    private C6124ags<C3164oa> hvI;
    private C6124ags<C0880Ml> hvJ;
    private C6124ags<C0502Gy> hvK = new C4682b();
    private C6124ags<C3689to> hvL = new C4684d();
    private C0160Br hvM;
    /* access modifiers changed from: private */
    private C0160Br hvN;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.addonProperties = addonPropertie;
        this.player = this.addonProperties.getPlayer();
        this.aoL = (C2698il) this.addonProperties.bHv().mo16794bQ(hvw);
        this.aoL.mo4917cf(hvx).setText(this.player.getName());
        this.gnU = this.aoL.mo4917cf(hvC);
        this.gnN = new C4697q(this.player.mo5156Qw(), C1643YC.eMu);
        this.gnN.mo1144a(new C4692l());
        this.hvM = new C0160Br(this.addonProperties, true);
        this.hvN = new C0160Br(this.addonProperties, false);
        if (this.addonProperties.getPlayer().bQB() || this.addonProperties.alb().anX()) {
            setVisible(true);
        } else {
            setVisible(false);
        }
        cPT();
        cPV();
        bQS();
        ayK();
        cPU();
        C6245ajJ aVU = this.addonProperties.aVU();
        aVU.mo13965a(C3949xF.class, this.glE);
        aVU.mo13965a(HudVisibilityAddon.C4534d.class, this.hvE);
        aVU.mo13965a(C2327eB.class, this.hvF);
        aVU.mo13965a(C1532WW.class, this.hvG);
        aVU.mo13965a(C1274Sq.class, this.cUd);
        aVU.mo13965a(C5344aFs.class, this.hvH);
        aVU.mo13965a(C3689to.class, this.hvL);
        aVU.mo13965a(C3164oa.class, this.hvI);
        aVU.mo13965a(C0880Ml.class, this.hvJ);
        aVU.mo13965a(C0502Gy.class, this.hvK);
        this.player.mo8320a(C1092Px.dQN, (C6200aiQ<?>) this.hvD);
        for (C5344aFs k : aVU.mo13967b(C5344aFs.class)) {
            this.hvH.updateInfo(k);
        }
    }

    private void cPT() {
        CharacterEvolution Rp;
        CharacterProgression ly = this.player.mo12659ly();
        if (ly != null && (Rp = ly.mo20722Rp()) != null) {
            m44477a(Rp, "CR", aEX.bgt);
            m44477a(Rp, "MP", aEX.ghA);
            m44477a(Rp, "FP", aEX.hIi);
            this.aoL.mo4915cd("progressionPanel").setVisible(true);
        }
    }

    /* renamed from: a */
    private void m44477a(CharacterEvolution eqVar, String str, C5663aRz arz) {
        new C4693m(eqVar, arz, str).mo1144a(new C4694n(this.aoL.mo4917cf("progression" + str)));
    }

    private void cPU() {
        IComponentManager e = ComponentManager.getCssHolder(this.gnU);
        e.setAttribute("class", hvr);
        Color color = e.mo13047Vp().getColor();
        e.setAttribute("class", hvt);
        Color color2 = e.mo13047Vp().getColor();
        e.setAttribute("class", hvu);
        Color color3 = e.mo13047Vp().getColor();
        this.hvI = new C4695o(e, color3, color2);
        this.hvJ = new C4691k(e, color3, color);
    }

    public void stop() {
        this.player.mo8326b(C1092Px.dQN, (C6200aiQ<?>) this.hvD);
        C6245ajJ aVU = this.addonProperties.aVU();
        aVU.mo13964a(this.glE);
        aVU.mo13964a(this.hvE);
        aVU.mo13964a(this.hvF);
        aVU.mo13964a(this.hvG);
        aVU.mo13964a(this.cUd);
        aVU.mo13964a(this.hvH);
        aVU.mo13964a(this.hvL);
        aVU.mo13968b(C3164oa.class, this.hvI);
        aVU.mo13968b(C0880Ml.class, this.hvJ);
        this.hvM.destroy();
        this.hvN.destroy();
        this.gnU = null;
        this.aoL.destroy();
        this.aoL = null;
    }

    /* access modifiers changed from: private */
    public void cPV() {
        Picture cd = this.aoL.mo4915cd(hvy);
        Picture cd2 = this.aoL.mo4915cd(hvA);
        JLabel cf = this.aoL.mo4917cf(hvz);
        JLabel cf2 = this.aoL.mo4917cf(hvB);
        if (this.player.dxE()) {
            Corporation bYd = this.player.bYd();
            cf.setText(bYd.getName());
            cd.mo16824a(C5378aHa.CORP_IMAGES, bYd.mo10706Qu().aOQ());
            CorporationRoleInfo I = bYd.mo10695I(this.player);
            cf2.setText(I.getRoleName());
            cd2.mo16824a(C5378aHa.ICONOGRAPHY, I.mo12098sK().getHandle());
            cd.setVisible(true);
            cd2.setVisible(true);
            cf.setVisible(true);
            cf2.setVisible(true);
        } else {
            cd.setVisible(false);
            cd2.setVisible(false);
            cf.setVisible(false);
            cf2.setVisible(false);
        }
        ayK();
    }

    /* access modifiers changed from: private */
    public void bQS() {
        if (this.player.bQB()) {
            setVisible(true);
        } else {
            Collection<HudVisibilityAddon.C4534d> b = this.addonProperties.aVU().mo13967b(HudVisibilityAddon.C4534d.class);
            if (b == null || b.isEmpty() || b.iterator().next().isVisible()) {
                setVisible(!this.player.dxc().anX());
            } else {
                setVisible(false);
            }
        }
        ayJ();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44482b(C5344aFs afs) {
        if (afs.cYK()) {
            this.hvM.mo838a(afs.getAction(), afs.getOrder());
        } else {
            this.hvN.mo838a(afs.getAction(), afs.getOrder());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m44485c(C5344aFs afs) {
        if (afs.cYK()) {
            this.hvM.mo837a(afs.getAction());
        } else {
            this.hvN.mo837a(afs.getAction());
        }
    }

    /* access modifiers changed from: private */
    public void ayJ() {
        this.hvM.ayJ();
        this.hvN.ayJ();
    }

    private void ayK() {
        JComponent jComponent = this.aoL;
        jComponent.setVisible(true);
        Dimension preferredSize = jComponent.getPreferredSize();
        jComponent.setSize(preferredSize);
        jComponent.setLocation((this.addonProperties.bHv().getScreenWidth() - preferredSize.width) / 2, 0);
        jComponent.validate();
        jComponent.doLayout();
    }

    /* access modifiers changed from: private */
    public void setVisible(boolean z) {
        int height = this.aoL.getHeight();
        int i = this.aoL.getLocation().y;
        if (z) {
            if (this.hvO != null) {
                this.hvO.kill();
                this.hvO = new aDX(this.aoL, "[" + i + ".." + 0 + "] dur 250 strong_out", C2830kk.asP, this.clN);
            } else {
                this.hvO = new aDX(this.aoL, "[" + (-height) + ".." + 0 + "] dur 500 strong_out", C2830kk.asP, this.clN);
            }
        } else if (this.hvO != null) {
            this.hvO.kill();
            this.hvO = new aDX(this.aoL, "[" + i + ".." + (-height) + "] dur 250 strong_in", C2830kk.asP, this.clN);
        } else {
            this.hvO = new aDX(this.aoL, "[0.." + (-height) + "] dur 500 strong_in", C2830kk.asP, this.clN);
        }
        this.hvN.setVisible(z);
        this.hvM.setVisible(z);
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$a */
    private class C4680a extends aDX {

        /* renamed from: UA */
        private static final String f10099UA = "[255..0] dur 333;[1..255] dur 333";

        public C4680a(JComponent jComponent, Color color, Color color2, String str, int i) {
            super(jComponent, f10099UA, new C2830kk.C2831a(color, color2), new C4681a(MainMenuAddon.this, i, color, color2, str));
        }

        /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$a$a */
        class C4681a implements C0454GJ {
            private final /* synthetic */ int bfA;
            private final /* synthetic */ MainMenuAddon ezn;
            private final /* synthetic */ Color ezo;
            private final /* synthetic */ Color ezp;
            private final /* synthetic */ String ezq;

            C4681a(MainMenuAddon mainMenuAddon, int i, Color color, Color color2, String str) {
                this.ezn = mainMenuAddon;
                this.bfA = i;
                this.ezo = color;
                this.ezp = color2;
                this.ezq = str;
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                if (this.bfA - 1 > 0) {
                    new C4680a(jComponent, this.ezo, this.ezp, this.ezq, this.bfA - 1);
                    return;
                }
                ComponentManager.getCssHolder(jComponent).setAttribute("class", this.ezq);
                this.ezn.hvv = null;
            }
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$i */
    /* compiled from: a */
    class C4689i extends C6124ags<C3949xF> {
        C4689i() {
        }

        /* renamed from: b */
        public boolean updateInfo(C3949xF xFVar) {
            Collection<HudVisibilityAddon.C4534d> b = MainMenuAddon.this.addonProperties.aVU().mo13967b(HudVisibilityAddon.C4534d.class);
            if (b == null || b.isEmpty() || b.iterator().next().isVisible()) {
                MainMenuAddon.this.setVisible(!xFVar.anX());
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$j */
    /* compiled from: a */
    class C4690j extends C6124ags<HudVisibilityAddon.C4534d> {
        C4690j() {
        }

        /* renamed from: b */
        public boolean updateInfo(HudVisibilityAddon.C4534d dVar) {
            MainMenuAddon.this.setVisible(dVar.isVisible());
            return false;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$e */
    /* compiled from: a */
    class C4685e extends C6124ags<C2327eB> {
        C4685e() {
        }

        /* renamed from: b */
        public boolean updateInfo(C2327eB eBVar) {
            MainMenuAddon.this.setVisible(eBVar.isVisible());
            return false;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$f */
    /* compiled from: a */
    class C4686f extends C6124ags<C1532WW> {
        C4686f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C1532WW ww) {
            MainMenuAddon.this.ayJ();
            return true;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$g */
    /* compiled from: a */
    class C4687g extends C6124ags<C1274Sq> {
        C4687g() {
        }

        /* renamed from: b */
        public boolean updateInfo(C1274Sq sq) {
            MainMenuAddon.this.bQS();
            return false;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$h */
    /* compiled from: a */
    class C4688h extends C6124ags<C5344aFs> {
        C4688h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5344aFs afs) {
            MainMenuAddon.this.m44482b(afs);
            return false;
        }

        /* renamed from: a */
        public void mo2003f(String str, C5344aFs afs) {
            MainMenuAddon.this.m44485c(afs);
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$d */
    /* compiled from: a */
    class C4684d extends C6124ags<C3689to> {
        C4684d() {
        }

        /* renamed from: b */
        public boolean updateInfo(C3689to toVar) {
            if (toVar.adE() == C3689to.C3690a.SHIP_LAUNCH) {
                MainMenuAddon.this.setVisible(false);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$c */
    /* compiled from: a */
    class C4683c implements C6200aiQ<aDJ> {
        C4683c() {
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            MainMenuAddon.this.cPV();
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$b */
    /* compiled from: a */
    class C4682b extends C6124ags<C0502Gy> {
        C4682b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0502Gy gy) {
            MainMenuAddon.this.cPV();
            return false;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$p */
    /* compiled from: a */
    class C4696p implements C0454GJ {
        C4696p() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            MainMenuAddon.this.hvO = null;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$q */
    /* compiled from: a */
    class C4697q extends C0211Cc {
        C4697q(aDJ adj, C5663aRz arz) {
            super(adj, arz);
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public Object mo1146d(Object obj) {
            if (!(obj instanceof Long)) {
                return super.mo1146d(obj);
            }
            return String.format("%s %,d", new Object[]{"T$", Long.valueOf(((Long) obj).longValue())});
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$l */
    /* compiled from: a */
    class C4692l implements C3663tS {
        C4692l() {
        }

        /* renamed from: e */
        public void mo22231e(Object obj) {
            if (MainMenuAddon.this.gnU != null) {
                MainMenuAddon.this.gnU.setText(obj == null ? "" : obj.toString());
            }
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$m */
    /* compiled from: a */
    class C4693m extends C0211Cc {
        private final /* synthetic */ String gJn;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4693m(aDJ adj, C5663aRz arz, String str) {
            super(adj, arz);
            this.gJn = str;
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public Object mo1146d(Object obj) {
            return String.format("%s: %s", new Object[]{this.gJn, String.valueOf(obj)});
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$n */
    /* compiled from: a */
    class C4694n implements C3663tS {
        private final /* synthetic */ JLabel gJq;

        C4694n(JLabel jLabel) {
            this.gJq = jLabel;
        }

        /* renamed from: e */
        public void mo22231e(Object obj) {
            if (this.gJq != null) {
                this.gJq.setText(obj == null ? "" : obj.toString());
            }
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$o */
    /* compiled from: a */
    class C4695o extends C6124ags<C3164oa> {
        private final /* synthetic */ Color ezo;
        private final /* synthetic */ IComponentManager gJl;
        private final /* synthetic */ Color gJr;

        C4695o(IComponentManager aek, Color color, Color color2) {
            this.gJl = aek;
            this.ezo = color;
            this.gJr = color2;
        }

        /* renamed from: a */
        public boolean updateInfo(C3164oa oaVar) {
            String str;
            if (MainMenuAddon.this.hvv != null) {
                MainMenuAddon.this.hvv.kill();
                MainMenuAddon.this.hvv = null;
            }
            this.gJl.setAttribute("class", MainMenuAddon.hvu);
            MainMenuAddon mainMenuAddon = MainMenuAddon.this;
            MainMenuAddon mainMenuAddon2 = MainMenuAddon.this;
            JLabel e = MainMenuAddon.this.gnU;
            Color color = this.ezo == null ? Color.WHITE : this.ezo;
            Color color2 = this.gJr == null ? Color.YELLOW : this.gJr;
            if (MainMenuAddon.this.player.mo5156Qw().bSs() < 0) {
                str = MainMenuAddon.hvr;
            } else {
                str = MainMenuAddon.hvs;
            }
            mainMenuAddon.hvv = new C4680a(e, color, color2, str, 2);
            return false;
        }
    }

    /* renamed from: taikodom.addon.mainmenu.MainMenuAddon$k */
    /* compiled from: a */
    class C4691k extends C6124ags<C0880Ml> {
        private final /* synthetic */ Color ezo;
        private final /* synthetic */ IComponentManager gJl;
        private final /* synthetic */ Color gJm;

        C4691k(IComponentManager aek, Color color, Color color2) {
            this.gJl = aek;
            this.ezo = color;
            this.gJm = color2;
        }

        /* renamed from: a */
        public boolean updateInfo(C0880Ml ml) {
            String str;
            if (MainMenuAddon.this.hvv != null) {
                MainMenuAddon.this.hvv.kill();
                MainMenuAddon.this.hvv = null;
            }
            this.gJl.setAttribute("class", MainMenuAddon.hvu);
            MainMenuAddon mainMenuAddon = MainMenuAddon.this;
            MainMenuAddon mainMenuAddon2 = MainMenuAddon.this;
            JLabel e = MainMenuAddon.this.gnU;
            Color color = this.ezo == null ? Color.WHITE : this.ezo;
            Color color2 = this.gJm == null ? Color.GREEN : this.gJm;
            if (MainMenuAddon.this.player.mo5156Qw().bSs() < 0) {
                str = MainMenuAddon.hvr;
            } else {
                str = MainMenuAddon.hvs;
            }
            mainMenuAddon.hvv = new C4680a(e, color, color2, str, 2);
            return false;
        }
    }
}
