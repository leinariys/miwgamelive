package taikodom.addon;

import game.engine.IEngineGame;
import game.script.PlayerController;
import game.script.Taikodom;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.baa.aDJ;
import logic.render.IEngineGraphics;
import logic.res.ILoaderTrail;
import logic.res.code.C5663aRz;
import logic.res.code.SoftTimer;
import org.slf4j.Logger;

import javax.swing.*;

/* renamed from: a.vW */
/* compiled from: a */
public interface IAddonProperties extends IAddonSettings {
    /* renamed from: a */
    <T extends aDJ> T mo2319a(T t, C6144ahM<?> ahm);

    /* renamed from: a */
    void mo2321a(aDJ adj, C5663aRz arz, Action action);

    /* renamed from: a */
    void mo2322a(String str, Action action);

    /* renamed from: af */
    String mo2324af(String str);

    Taikodom ala();

    PlayerController alb();

    Logger getLog();

    IEngineGame getEngineGame();

    IEngineGraphics ale();

    SoftTimer alf();

    ILoaderTrail alg();

    /* renamed from: dL */
    Player getPlayer();
}
