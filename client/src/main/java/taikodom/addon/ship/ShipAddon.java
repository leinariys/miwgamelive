package taikodom.addon.ship;

import game.network.message.externalizable.C3131oI;
import game.network.message.externalizable.C3689to;
import game.script.item.Component;
import game.script.player.Player;
import game.script.ship.SectorCategory;
import game.script.ship.Ship;
import game.script.storage.Storage;
import logic.IAddonSettings;
import logic.aaa.C2123cF;
import logic.aaa.C2327eB;
import logic.aaa.C5783aaP;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.data.link.C5386aHi;
import logic.res.code.C5663aRz;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.PropertiesUiFromCss;
import logic.ui.item.C2830kk;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.Collection;

@TaikodomAddon("taikodom.addon.ship")
/* compiled from: a */
public class ShipAddon implements C2495fo {
    private static final String iyf = "ship.xml";
    private static final String iyg = "sensPanel.xml";
    /* access modifiers changed from: private */

    /* renamed from: DV */
    public Panel f10262DV;
    /* access modifiers changed from: private */
    public boolean ccv;
    /* access modifiers changed from: private */
    public JToggleButton iyh;
    /* access modifiers changed from: private */
    public Panel iyi;
    /* access modifiers changed from: private */
    public boolean iyq;
    /* renamed from: kj */
    public IAddonProperties f10264kj;
    /* access modifiers changed from: private */
    public boolean visible = false;
    /* renamed from: SZ */
    public aDX f10263SZ;
    private C3428rT<C5783aaP> aoN;
    private C6200aiQ<Player> bwT;
    private JLabel iyj;
    private C0956Nx iyk;
    private C2664iH iyl;
    private C2459fd iym;
    private C5775aaH iyn;
    /* access modifiers changed from: private */
    private C3428rT<C2327eB> iyo;
    private C3428rT<C6060afg> iyp;
    /* renamed from: wz */
    private C3428rT<C3689to> f10265wz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10264kj = addonPropertie;
        this.f10262DV = (Panel) this.f10264kj.bHv().mo16794bQ(iyf);
        this.iyi = (Panel) this.f10264kj.bHv().mo16794bQ(iyg);
        if (m45438iL()) {
            dna();
            this.ccv = true;
        } else {
            this.ccv = aMU.BOUGHT_MISSION_ITEM.ordinal() <= this.f10264kj.getPlayer().cXm().ordinal();
            if (this.ccv) {
                dna();
            } else {
                this.f10264kj.aVU().mo13965a(C3131oI.class, new C4989j());
            }
        }
        this.f10264kj.mo2317P(this);
    }

    /* access modifiers changed from: private */
    public void dna() {
        initComponents();
        dnb();
        m45437iG();
        m45422Fa();
        this.f10262DV.setSize(330, this.f10264kj.bHv().getScreenHeight());
        this.f10262DV.center();
        this.iyi.setSize(200, 200);
        this.iyi.center();
        dnj();
    }

    /* renamed from: iG */
    private void m45437iG() {
        this.aoN = new C4988i();
        this.f10265wz = new C4985f();
        this.bwT = new C4984e();
        this.iyo = new C4987h();
        this.iyp = new C4986g();
    }

    /* access modifiers changed from: private */
    /* renamed from: av */
    public void m45427av(Ship fAVar) {
        this.iyj.setText(fAVar.agH().mo19891ke().get());
        this.iyk.mo4307M(fAVar);
        this.iyl.mo4307M(fAVar);
        this.iym.mo4307M(fAVar);
        this.iyn.mo4307M(fAVar);
        if (this.iyh.isSelected()) {
            dnc();
            akN();
        }
    }

    private void dnb() {
        PropertiesUiFromCss.m461f(this.iyh).setColorMultiplier(new Color(0, 0, 0, 0));
        if (this.f10264kj.getPlayer().bQx() != null) {
            m45427av(this.f10264kj.getPlayer().bQx());
        }
        this.iyq = true;
    }

    private void initComponents() {
        this.iyk = new C0956Nx(this.f10264kj, this.f10262DV.mo4916ce("shipDescr"));
        this.iyk.setVisible(false);
        this.iyl = new C2664iH(this.f10264kj, this.f10262DV.mo4916ce("shipTurrets"));
        this.iyl.setVisible(false);
        this.iym = new C2459fd(this.f10264kj, this.f10262DV.mo4916ce("shipEquip"));
        this.iym.setVisible(false);
        this.iyn = new C5775aaH(this.f10264kj, this.f10262DV.mo4916ce("shipCargo"));
        this.iyn.setVisible(false);
        this.iyh = this.f10262DV.mo4915cd("openButton");
        this.iyj = this.f10262DV.mo4917cf("shipLabel");
    }

    public void dnc() {
        this.visible = false;
        this.iyk.setVisible(false);
        this.iyl.setVisible(false);
        this.iym.setVisible(false);
        this.iyn.setVisible(false);
    }

    public void dnd() {
        this.iyl.setVisible(true);
        new aDX(this.iyl.cFB(), "[0..255] dur 300", C2830kk.asS, (C0454GJ) null);
    }

    public void dne() {
        this.iyn.setVisible(true);
        new aDX(this.iyn.cFB(), "[0..255] dur 300", C2830kk.asS, (C0454GJ) null);
    }

    public void dnf() {
        this.iym.setVisible(true);
        new aDX(this.iym.cFB(), "[0..255] dur 300", C2830kk.asS, (C0454GJ) null);
    }

    public void dng() {
        this.iyk.setVisible(true);
        new aDX(this.iyk.cFB(), "[0..255] dur 300", C2830kk.asS, (C0454GJ) null);
    }

    /* access modifiers changed from: private */
    public void akN() {
        if (this.ccv) {
            if (!m45438iL()) {
                Player dL = this.f10264kj.getPlayer();
                if (dL == null) {
                    return;
                }
                if (aMU.BOUGHT_MISSION_ITEM.equals(dL.cXm()) && dL.bQB() && dL.bQx() != null && !dL.bQx().afI().isEmpty()) {
                    ((Player) C3582se.m38985a(dL, (C6144ahM<?>) new C4992l())).dxU();
                }
            }
            dng();
            if (this.iyl.cFC().agF().size() > 0) {
                dnd();
            }
            dnf();
            dne();
            this.visible = true;
            ComponentManager.getCssHolder(this.iyh).mo13049Vr().setColorMultiplier(new Color(1, 1, 1, 1));
        }
    }

    /* renamed from: iL */
    private boolean m45438iL() {
        return this.f10264kj != null && this.f10264kj.ala().aLS().mo22273iL();
    }

    /* renamed from: Fa */
    private void m45422Fa() {
        this.f10264kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f10264kj.aVU().mo13965a(C3689to.class, this.f10265wz);
        this.f10264kj.aVU().mo13965a(C2327eB.class, this.iyo);
        this.f10264kj.aVU().mo13965a(C6060afg.class, this.iyp);
        this.f10264kj.getPlayer().mo8320a(C5386aHi.hWr, (C6200aiQ<?>) this.bwT);
        this.iyi.setTransferHandler(new C4993m());
        dnh();
    }

    private void dnh() {
        this.iyh.addActionListener(new C4991k());
        this.iyh.addMouseListener(new C4983d());
        this.iyi.addMouseListener(new C4982c());
    }

    /* access modifiers changed from: private */
    /* renamed from: o */
    public void m45441o(SectorCategory fMVar) {
        this.iym.mo18743p(fMVar);
    }

    public void stop() {
        removeListeners();
    }

    private void removeListeners() {
        this.f10264kj.getPlayer().mo8326b(C5386aHi.hWr, (C6200aiQ<?>) this.bwT);
        this.f10264kj.aVU().mo13968b(C5783aaP.class, this.aoN);
        this.f10264kj.aVU().mo13968b(C3689to.class, this.f10265wz);
        this.f10264kj.aVU().mo13968b(C2327eB.class, this.iyo);
        this.f10264kj.aVU().mo13968b(C6060afg.class, this.iyp);
    }

    /* access modifiers changed from: private */
    public void dni() {
        if (this.f10263SZ != null) {
            this.f10263SZ.kill();
        }
        PropertiesUiFromCss f = PropertiesUiFromCss.m461f(this.iyh);
        Color colorMultiplier = f.getColorMultiplier();
        if (colorMultiplier != null) {
            this.f10263SZ = new aDX(this.iyh, "[" + colorMultiplier.getAlpha() + "..255] dur 500", C2830kk.asS, new C4981b());
            return;
        }
        f.setColorMultiplier(new Color(1, 1, 1, 1));
    }

    /* access modifiers changed from: private */
    public void dnj() {
        if (this.f10263SZ != null) {
            this.f10263SZ.kill();
        }
        PropertiesUiFromCss f = PropertiesUiFromCss.m461f(this.iyh);
        Color colorMultiplier = f.getColorMultiplier();
        if (colorMultiplier != null) {
            this.f10263SZ = new aDX(this.iyh, "[" + colorMultiplier.getAlpha() + "..0] dur 500", C2830kk.asS, new C4980a());
            return;
        }
        f.setColorMultiplier(new Color(0, 0, 0, 0));
    }

    @C2602hR(mo19255zf = "SHIP_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24583al(boolean z) {
        if (!this.ccv || !z) {
            return;
        }
        if (!this.visible) {
            akN();
            this.iyh.setSelected(true);
            dni();
            return;
        }
        dnc();
        this.iyh.setSelected(false);
        dnj();
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$j */
    /* compiled from: a */
    class C4989j extends C3428rT<C3131oI> {
        C4989j() {
        }

        /* renamed from: b */
        public void mo321b(C3131oI oIVar) {
            if (C3131oI.C3132a.HIGHLIGHT_SHIP_WINDOW.equals(oIVar.mo20956Us())) {
                ShipAddon.this.ccv = true;
                if (ShipAddon.this.iyh == null) {
                    ShipAddon.this.dna();
                }
                C5905ach.m20567b((IAddonSettings) ShipAddon.this.f10264kj).mo12679a((ActionListener) new C4990a());
                ShipAddon.this.f10264kj.aVU().mo13964a(this);
            }
        }

        /* renamed from: taikodom.addon.ship.ShipAddon$j$a */
        class C4990a implements ActionListener {
            C4990a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (ShipAddon.this.f10264kj.getPlayer().bQB()) {
                    ShipAddon.this.f10262DV.setVisible(true);
                    ShipAddon.this.dnj();
                    ShipAddon.this.f10264kj.aVU().mo13975h(new C2123cF(ShipAddon.this.iyh));
                    return;
                }
                ShipAddon.this.f10262DV.setVisible(false);
                ShipAddon.this.iyh.setSelected(false);
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$i */
    /* compiled from: a */
    class C4988i extends C3428rT<C5783aaP> {
        C4988i() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (!aap.bMO().equals(C5783aaP.C1841a.DOCKED)) {
                ShipAddon.this.iyi.setVisible(false);
            }
            if (ShipAddon.this.f10262DV != null && ShipAddon.this.f10262DV.isVisible()) {
                ShipAddon.this.dnc();
                ShipAddon.this.iyi.setVisible(false);
                ShipAddon.this.iyh.setSelected(false);
                ShipAddon.this.f10262DV.setVisible(false);
            }
            if (ShipAddon.this.ccv && aap.bMO() == C5783aaP.C1841a.DOCKED) {
                ShipAddon.this.f10262DV.setVisible(true);
                ShipAddon.this.iyi.setVisible(true);
                ShipAddon.this.iyq = true;
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$f */
    /* compiled from: a */
    class C4985f extends C3428rT<C3689to> {
        C4985f() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (ShipAddon.this.visible) {
                toVar.adC();
                ShipAddon.this.dnc();
                ShipAddon.this.iyh.setSelected(false);
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$e */
    /* compiled from: a */
    class C4984e implements C6200aiQ<Player> {
        C4984e() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            if (obj != null && ShipAddon.this.ccv) {
                ShipAddon.this.f10262DV.setVisible(true);
            }
            if (ShipAddon.this.f10262DV != null && ShipAddon.this.f10262DV.isVisible()) {
                if (obj != null) {
                    ShipAddon.this.m45427av((Ship) obj);
                } else if (ShipAddon.this.f10262DV.isVisible()) {
                    ShipAddon.this.f10262DV.setVisible(false);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$h */
    /* compiled from: a */
    class C4987h extends C3428rT<C2327eB> {
        C4987h() {
        }

        /* renamed from: a */
        public void mo321b(C2327eB eBVar) {
            if (!eBVar.isVisible()) {
                ShipAddon.this.f10262DV.setVisible(false);
                ShipAddon.this.iyi.setVisible(false);
                ShipAddon.this.iyq = false;
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$g */
    /* compiled from: a */
    class C4986g extends C3428rT<C6060afg> {
        C4986g() {
        }

        /* renamed from: a */
        public void mo321b(C6060afg afg) {
            boolean z = true;
            if (afg.bQB() && afg.bVt() == 0 && ShipAddon.this.iyq && ShipAddon.this.ccv) {
                ShipAddon.this.f10262DV.setVisible(!ShipAddon.this.f10262DV.isVisible());
                Panel f = ShipAddon.this.iyi;
                if (ShipAddon.this.iyi.isVisible()) {
                    z = false;
                }
                f.setVisible(z);
                if (!ShipAddon.this.f10262DV.isVisible()) {
                    ShipAddon.this.iyh.setSelected(false);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$l */
    /* compiled from: a */
    class C4992l implements C6144ahM<Storage> {
        C4992l() {
        }

        /* renamed from: e */
        public void mo1931n(Storage qzVar) {
            if (qzVar.mo21543eT().equals(ShipAddon.this.f10264kj.ala().aJe().mo19006xF().bZE())) {
                ShipAddon.this.f10264kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.OPENED_SHIP_WINDOW));
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$m */
    /* compiled from: a */
    class C4993m extends TransferHandler {
        private static final long serialVersionUID = 12302208190838447L;

        C4993m() {
        }

        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            if (ShipAddon.this.iyh.isSelected()) {
                return false;
            }
            ShipAddon.this.akN();
            try {
                for (aDJ adj : (Collection) transferSupport.getTransferable().getTransferData(C2125cH.f6026vP)) {
                    if (adj instanceof Component) {
                        ShipAddon.this.m45441o(((Component) adj).cJS().cXV());
                    }
                }
            } catch (UnsupportedFlavorException | IOException e) {
            }
            ShipAddon.this.iyh.setSelected(true);
            return false;
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$k */
    /* compiled from: a */
    class C4991k implements ActionListener {
        C4991k() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (ShipAddon.this.iyh.isSelected()) {
                ShipAddon.this.akN();
            } else {
                ShipAddon.this.dnc();
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$d */
    /* compiled from: a */
    class C4983d extends MouseAdapter {
        C4983d() {
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            if (!ShipAddon.this.iyh.isSelected() && ShipAddon.this.f10262DV.isVisible()) {
                ShipAddon.this.dni();
            }
        }

        public void mouseExited(MouseEvent mouseEvent) {
            if (!ShipAddon.this.iyh.isSelected() && ShipAddon.this.f10262DV.isVisible()) {
                ShipAddon.this.dnj();
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$c */
    /* compiled from: a */
    class C4982c extends MouseAdapter {
        C4982c() {
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            if (!ShipAddon.this.iyh.isSelected() && ShipAddon.this.f10262DV.isVisible()) {
                ShipAddon.this.dni();
            }
        }

        public void mouseExited(MouseEvent mouseEvent) {
            if (!ShipAddon.this.iyh.isSelected() && ShipAddon.this.f10262DV.isVisible()) {
                ShipAddon.this.dnj();
            }
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$b */
    /* compiled from: a */
    class C4981b implements C0454GJ {
        C4981b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            ShipAddon.this.f10263SZ = null;
        }
    }

    /* renamed from: taikodom.addon.ship.ShipAddon$a */
    class C4980a implements C0454GJ {
        C4980a() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            ShipAddon.this.f10263SZ = null;
        }
    }
}
