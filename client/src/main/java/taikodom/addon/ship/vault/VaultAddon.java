package taikodom.addon.ship.vault;

import game.network.message.externalizable.C2651i;
import game.script.item.ItemLocation;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.categories.Freighter;
import logic.IAddonSettings;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.Progress;
import logic.ui.item.TaskPane;
import logic.ui.item.TextField;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.TItemPanel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@TaikodomAddon("taikodom.addon.ship.vault")
/* compiled from: a */
public class VaultAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f10269IU;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10270P;
    /* access modifiers changed from: private */
    public aSZ aCF;
    /* access modifiers changed from: private */
    public TItemPanel aZK;
    /* access modifiers changed from: private */
    public ItemLocation bLj;
    /* access modifiers changed from: private */
    public Ship coW;
    /* access modifiers changed from: private */
    public Panel coX;
    /* access modifiers changed from: private */
    public Panel coZ;
    /* renamed from: kj */
    public IAddonProperties f10271kj;
    private TaskPane aCG;
    private aGC coU;
    private C3428rT<C2651i> coV;
    private Progress coY;
    /* access modifiers changed from: private */
    private TextField cpa;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10271kj = addonPropertie;
        this.f10270P = this.f10271kj.getPlayer();
        if (this.f10270P != null) {
            this.f10269IU = C3987xk.bFK;
            this.coU = new C5001d(this.f10271kj.translate("Vault"), "vault");
            this.coU.mo15602jH(this.f10271kj.translate("This is the protected storage area inside your freighter."));
            this.coU.mo15603jI("VAULT_WINDOW");
            m45484Im();
            this.aCF = new aSZ(this.aCG, this.coU, 600, 2);
            this.f10271kj.aVU().publish(this.aCF);
            this.coV = new C4998a();
            this.f10271kj.aVU().mo13965a(C2651i.class, this.coV);
            this.coW = this.f10270P.bQx();
            if (this.coW instanceof Freighter) {
                open();
            }
        }
    }

    public void stop() {
        if (this.aCG != null) {
            this.aCG.destroy();
            this.aCG = null;
        }
        this.f10271kj.aVU().mo13968b(C2651i.class, this.coV);
        this.f10271kj.aVU().mo13974g(this.aCF);
    }

    /* renamed from: Im */
    private void m45484Im() {
        this.aCG = (TaskPane) this.f10271kj.bHv().mo16794bQ("vault.xml");
        this.coX = (Panel) this.aCG.mo4916ce("capacity-panel");
        this.coY = this.aCG.mo4918cg("capacity");
        this.coY.mo17403a(new C4999b());
        this.aZK = new TItemPanel();
        this.aCG.mo4915cd("items-container").add(this.aZK);
        this.coZ = (Panel) this.aCG.mo4916ce("search-panel");
        this.cpa = this.aCG.mo4920ci("search-field");
        this.cpa.addKeyListener(new C5000c());
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f10270P.bQx() != null && (this.f10270P.bQx() instanceof Freighter)) {
            this.cpa.setText("");
            this.coW = this.f10270P.bQx();
            if (this.coW instanceof Freighter) {
                this.bLj = ((Freighter) this.coW).aZy();
                m45493e(this.bLj);
            } else {
                this.coW = null;
            }
            this.aZK.mo23647b(this.bLj);
        }
    }

    /* access modifiers changed from: private */
    public void close() {
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m45493e(ItemLocation aag) {
        this.coY.mo17398E(aag.mo2696wE());
        this.coY.setValue(aag.aRB());
    }

    @C2602hR(mo19255zf = "VAULT_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24605al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: taikodom.addon.ship.vault.VaultAddon$d */
    /* compiled from: a */
    class C5001d extends aGC {


        C5001d(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            return !VaultAddon.this.f10270P.bQB() && VaultAddon.this.f10270P.bQx() != null && (VaultAddon.this.f10270P.bQx() instanceof Freighter);
        }

        public void setVisible(boolean z) {
            if (z) {
                VaultAddon.this.open();
            } else {
                VaultAddon.this.close();
            }
        }

        /* renamed from: m */
        public void mo8924m(boolean z) {
            if (z) {
                VaultAddon.this.coZ.setVisible(false);
                ComponentManager.getCssHolder(VaultAddon.this.coX).setAttribute("class", "empty");
                return;
            }
            VaultAddon.this.coZ.setVisible(true);
            ComponentManager.getCssHolder(VaultAddon.this.coX).setAttribute("class", "verticalbox");
        }
    }

    /* renamed from: taikodom.addon.ship.vault.VaultAddon$a */
    class C4998a extends C3428rT<C2651i> {
        C4998a() {
        }

        /* renamed from: a */
        public void mo321b(C2651i iVar) {
            if (VaultAddon.this.coW != iVar.mo19441a()) {
                VaultAddon.this.coW = iVar.mo19441a();
                if (iVar.mo19441a() == null) {
                    VaultAddon.this.f10271kj.aVU().mo13975h(new C0269DS(VaultAddon.this.aCF, false));
                } else if (VaultAddon.this.coW instanceof Freighter) {
                    VaultAddon.this.bLj = ((Freighter) VaultAddon.this.coW).aZy();
                    if (VaultAddon.this.aZK != null) {
                        VaultAddon.this.aZK.mo23647b(VaultAddon.this.bLj);
                    }
                    VaultAddon.this.m45493e(VaultAddon.this.bLj);
                    VaultAddon.this.f10271kj.aVU().mo13975h(new C0269DS(VaultAddon.this.aCF, true));
                } else {
                    VaultAddon.this.f10271kj.aVU().mo13975h(new C0269DS(VaultAddon.this.aCF, false));
                }
            }
        }
    }

    /* renamed from: taikodom.addon.ship.vault.VaultAddon$b */
    /* compiled from: a */
    class C4999b implements Progress.C2065a {
        C4999b() {
        }

        public String getText() {
            if (VaultAddon.this.bLj == null) {
                return null;
            }
            return String.valueOf(VaultAddon.this.f10269IU.mo22980dY(VaultAddon.this.bLj.aRB())) + C0147Bi.SEPARATOR + VaultAddon.this.f10269IU.mo22980dY(VaultAddon.this.bLj.mo2696wE());
        }

        public float getValue() {
            if (VaultAddon.this.bLj == null) {
                return 0.0f;
            }
            return VaultAddon.this.bLj.aRB();
        }

        public float getMaximum() {
            if (VaultAddon.this.bLj == null) {
                return 0.0f;
            }
            return VaultAddon.this.bLj.mo2696wE();
        }

        public float getMinimum() {
            return 0.0f;
        }
    }

    /* renamed from: taikodom.addon.ship.vault.VaultAddon$c */
    /* compiled from: a */
    class C5000c extends KeyAdapter {
        C5000c() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (VaultAddon.this.aZK != null) {
                VaultAddon.this.aZK.bCU().mo2165O(keyEvent.getComponent().getText());
            }
        }
    }
}
