package taikodom.addon.ship.container;

import game.network.message.externalizable.C2651i;
import game.script.item.ItemLocation;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.Progress;
import logic.ui.item.TaskPane;
import logic.ui.item.TextField;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.TItemPanel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@TaikodomAddon("taikodom.addon.ship.container")
/* compiled from: a */
public class ContainerAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f10266IU;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10267P;
    /* access modifiers changed from: private */
    public aSZ aCF;
    /* access modifiers changed from: private */
    public TItemPanel aZK;
    /* access modifiers changed from: private */
    public ItemLocation bLj;
    /* access modifiers changed from: private */
    public Ship coW;
    /* access modifiers changed from: private */
    public Panel coX;
    /* access modifiers changed from: private */
    public Panel coZ;
    /* renamed from: kj */
    public IAddonProperties f10268kj;
    private TaskPane aCG;
    private aGC coU;
    private C3428rT<C2651i> coV;
    private Progress coY;
    /* access modifiers changed from: private */
    private TextField cpa;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10268kj = addonPropertie;
        this.f10267P = this.f10268kj.getPlayer();
        if (this.f10267P != null) {
            this.f10266IU = C3987xk.bFK;
            this.coU = new C4997d(this.f10268kj.translate("Container"), "container");
            this.coU.mo15602jH(this.f10268kj.translate("This is the container of your ship."));
            m45463Im();
            this.aCF = new aSZ(this.aCG, this.coU, 550, 2);
            this.f10268kj.aVU().publish(this.aCF);
            this.coV = new C4996c();
            this.f10268kj.aVU().mo13965a(C2651i.class, this.coV);
            this.coW = this.f10267P.bQx();
            if (this.f10267P != null && !this.f10267P.bQB()) {
                open();
            }
        }
    }

    public void stop() {
        if (this.aCG != null) {
            this.aCG.destroy();
            this.aCG = null;
        }
        this.f10268kj.aVU().mo13968b(C2651i.class, this.coV);
        this.f10268kj.aVU().mo13974g(this.aCF);
    }

    /* renamed from: Im */
    private void m45463Im() {
        this.aCG = (TaskPane) this.f10268kj.bHv().mo16794bQ("container.xml");
        this.coX = (Panel) this.aCG.mo4916ce("capacity-panel");
        this.coY = this.coX.mo4918cg("capacity");
        this.coY.mo17403a(new C4995b());
        this.aZK = new TItemPanel();
        this.aCG.mo4915cd("items-container").add(this.aZK);
        this.coZ = (Panel) this.aCG.mo4916ce("search-panel");
        this.cpa = this.coZ.mo4920ci("search-field");
        this.cpa.addKeyListener(new C4994a());
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f10267P.bQx() != null) {
            this.cpa.setText("");
            this.coW = this.f10267P.bQx();
            this.bLj = this.coW.afI();
            m45472e(this.bLj);
            this.aZK.mo23647b(this.bLj);
        }
    }

    /* access modifiers changed from: private */
    public void close() {
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m45472e(ItemLocation aag) {
        this.coY.mo17398E(aag.mo2696wE());
        this.coY.setValue(aag.aRB());
    }

    /* renamed from: taikodom.addon.ship.container.ContainerAddon$d */
    /* compiled from: a */
    class C4997d extends aGC {
        private static final long serialVersionUID = 2095831255727452386L;

        C4997d(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            return ContainerAddon.this.f10267P.bQx() != null && !ContainerAddon.this.f10267P.bQB();
        }

        public void setVisible(boolean z) {
            if (z) {
                ContainerAddon.this.open();
            } else {
                ContainerAddon.this.close();
            }
        }

        /* renamed from: m */
        public void mo8924m(boolean z) {
            if (z) {
                ContainerAddon.this.coZ.setVisible(false);
                ComponentManager.getCssHolder(ContainerAddon.this.coX).setAttribute("class", "empty");
                return;
            }
            ContainerAddon.this.coZ.setVisible(true);
            ComponentManager.getCssHolder(ContainerAddon.this.coX).setAttribute("class", "verticalbox");
        }
    }

    /* renamed from: taikodom.addon.ship.container.ContainerAddon$c */
    /* compiled from: a */
    class C4996c extends C3428rT<C2651i> {
        C4996c() {
        }

        /* renamed from: a */
        public void mo321b(C2651i iVar) {
            if (ContainerAddon.this.coW != iVar.mo19441a()) {
                ContainerAddon.this.coW = iVar.mo19441a();
                if (iVar.mo19441a() == null) {
                    ContainerAddon.this.f10268kj.aVU().mo13975h(new C0269DS(ContainerAddon.this.aCF, false));
                    return;
                }
                ContainerAddon.this.bLj = ContainerAddon.this.coW.afI();
                if (ContainerAddon.this.aZK != null) {
                    ContainerAddon.this.aZK.mo23647b(ContainerAddon.this.bLj);
                }
                ContainerAddon.this.m45472e(ContainerAddon.this.bLj);
                ContainerAddon.this.f10268kj.aVU().mo13975h(new C0269DS(ContainerAddon.this.aCF, true));
            }
        }
    }

    /* renamed from: taikodom.addon.ship.container.ContainerAddon$b */
    /* compiled from: a */
    class C4995b implements Progress.C2065a {
        C4995b() {
        }

        public String getText() {
            if (ContainerAddon.this.bLj == null) {
                return null;
            }
            return String.valueOf(ContainerAddon.this.f10266IU.mo22980dY(ContainerAddon.this.bLj.aRB())) + C0147Bi.SEPARATOR + ContainerAddon.this.f10266IU.mo22980dY(ContainerAddon.this.bLj.mo2696wE());
        }

        public float getValue() {
            if (ContainerAddon.this.bLj == null) {
                return 0.0f;
            }
            return ContainerAddon.this.bLj.aRB();
        }

        public float getMaximum() {
            if (ContainerAddon.this.bLj == null) {
                return 0.0f;
            }
            return ContainerAddon.this.bLj.mo2696wE();
        }

        public float getMinimum() {
            return 0.0f;
        }
    }

    /* renamed from: taikodom.addon.ship.container.ContainerAddon$a */
    class C4994a extends KeyAdapter {
        C4994a() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (ContainerAddon.this.aZK != null) {
                ContainerAddon.this.aZK.bCU().mo2165O(keyEvent.getComponent().getText());
            }
        }
    }
}
