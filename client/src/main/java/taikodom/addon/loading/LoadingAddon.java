package taikodom.addon.loading;

import game.network.message.externalizable.C3002mq;
import game.network.message.externalizable.C3689to;
import game.script.nls.NLSManager;
import game.script.nls.NLSResourceLoader;
import game.view.TipString;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C6958awx;
import logic.aaa.aIQ;
import logic.swing.C0454GJ;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import logic.ui.item.Progress;
import logic.ui.item.TextArea;
import p001a.C3428rT;
import p001a.C3987xk;
import p001a.C6124ags;
import p001a.aJP;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.util.List;
import java.util.Random;

@TaikodomAddon("taikodom.addon.loading")
/* compiled from: a */
public class LoadingAddon implements C2495fo {
    private static final Random RANDOM = new Random(System.currentTimeMillis());

    /* renamed from: Va */
    private static final String f10009Va = "tela";

    /* renamed from: Vb */
    private static final int f10010Vb = 5;

    /* renamed from: qw */
    private static final String f10011qw = "loading.xml";
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f10012IU;
    /* access modifiers changed from: private */

    /* renamed from: Vc */
    public InternalFrame f10013Vc;
    /* access modifiers changed from: private */

    /* renamed from: Vd */
    public Progress f10014Vd;
    /* renamed from: Vi */
    public C4634g f10019Vi;
    /* renamed from: Vj */
    public C4635h f10020Vj = new C4635h(this, (C4635h) null);
    /* renamed from: kj */
    public IAddonProperties f10021kj;
    /* renamed from: Ve */
    private TextArea f10015Ve;
    /* access modifiers changed from: private */
    /* renamed from: Vf */
    private C3428rT<C3002mq> f10016Vf;
    /* access modifiers changed from: private */
    /* renamed from: Vg */
    private C6124ags<aIQ> f10017Vg;
    /* access modifiers changed from: private */
    /* renamed from: Vh */
    private List<TipString> f10018Vh;
    private boolean running = false;

    /* renamed from: wz */
    private C3428rT<C3689to> f10022wz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10021kj = addonPropertie;
        this.f10012IU = C3987xk.bFK;
        m44374iG();
        this.f10021kj.aVU().mo13965a(C3002mq.class, this.f10016Vf);
        this.f10021kj.aVU().mo13965a(C3689to.class, this.f10022wz);
        this.f10021kj.aVU().mo13965a(aIQ.class, this.f10017Vg);
    }

    /* renamed from: iG */
    private void m44374iG() {
        this.f10016Vf = new C4631d();
        this.f10017Vg = new C4630c();
        this.f10022wz = new C4633f();
    }

    /* renamed from: yC */
    private void m44375yC() {
        this.f10013Vc = this.f10021kj.bHv().mo16792bN(f10011qw);
        this.f10013Vc.setSize(this.f10021kj.bHv().getScreenSize());
        this.f10015Ve = this.f10013Vc.mo4915cd("tipContent");
        this.f10014Vd = this.f10013Vc.mo4915cd("progress");
        this.f10014Vd.mo17403a(new C4632e());
        this.f10019Vi = new C4634g(this, (C4634g) null);
        this.f10013Vc.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44366a(C3002mq.C3003a aVar, boolean z) {
        if (!this.running) {
            this.f10020Vj.chA();
            this.running = true;
            if (this.f10013Vc == null) {
                m44375yC();
            }
            if (this.f10018Vh == null) {
                this.f10018Vh = ((NLSResourceLoader) this.f10021kj.ala().aIY().mo6310c(NLSManager.C1472a.RESOURCELOADER)).drp();
            }
            this.f10021kj.alf().addTask("RefreshTask", this.f10020Vj, 70);
            this.f10021kj.alg().mo4992a((aJP) this.f10019Vi);
            this.f10015Ve.setText(this.f10018Vh.get(RANDOM.nextInt(this.f10018Vh.size())).get());
            ComponentManager.getCssHolder(this.f10013Vc).mo13049Vr().mo369a(this.f10021kj.bHv().adz().getImage(String.valueOf(C5378aHa.LOADING.getPath()) + f10009Va + String.format("%02d", new Object[]{Integer.valueOf(RANDOM.nextInt(4) + 1)})));
            this.f10013Vc.setVisible(true);
            if (z) {
                this.f10013Vc.mo4915cd("tipPanel").setVisible(false);
            } else {
                this.f10015Ve.requestFocus();
            }
            this.f10013Vc.moveToFront();
            if (aVar != null) {
                new Thread(new C4629b(aVar)).start();
            }
        }
    }

    public void stop() {
        if (this.f10013Vc != null) {
            this.f10013Vc.destroy();
            this.f10013Vc = null;
        }
        this.f10021kj.aVU().mo13964a(this.f10016Vf);
        this.f10021kj.aVU().mo13964a(this.f10017Vg);
        this.f10021kj.alg().mo5000b((aJP) this.f10019Vi);
        this.f10021kj.alf().mo7963a(this.f10020Vj);
    }

    /* access modifiers changed from: private */
    public void close() {
        this.running = false;
        if (this.f10013Vc != null) {
            this.f10021kj.alg().mo5000b((aJP) this.f10019Vi);
            this.f10021kj.alf().mo7963a(this.f10020Vj);
            this.f10013Vc.mo20868b(false, (C0454GJ) new C4628a());
        }
    }

    public boolean isVisible() {
        if (this.f10013Vc == null) {
            return false;
        }
        return this.f10013Vc.isVisible();
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$d */
    /* compiled from: a */
    class C4631d extends C3428rT<C3002mq> {
        C4631d() {
        }

        /* renamed from: b */
        public void mo321b(C3002mq mqVar) {
            switch (mqVar.aiH()) {
                case 0:
                    LoadingAddon.this.m44366a(mqVar.aiI(), mqVar.aiJ());
                    LoadingAddon.this.f10019Vi.dtt = false;
                    return;
                case 1:
                    LoadingAddon.this.f10020Vj.done();
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$c */
    /* compiled from: a */
    class C4630c extends C6124ags<aIQ> {
        C4630c() {
        }

        /* renamed from: a */
        public boolean updateInfo(aIQ aiq) {
            LoadingAddon.this.f10020Vj.mo24209hp(aiq.getLevel());
            return false;
        }
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$f */
    /* compiled from: a */
    class C4633f extends C3428rT<C3689to> {
        C4633f() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (LoadingAddon.this.isVisible()) {
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$e */
    /* compiled from: a */
    class C4632e implements Progress.C2065a {
        C4632e() {
        }

        public float getMaximum() {
            return 100.0f;
        }

        public float getMinimum() {
            return 0.0f;
        }

        public String getText() {
            return String.valueOf(String.valueOf(LoadingAddon.this.f10012IU.format((long) Math.round(LoadingAddon.this.f10020Vj.bpP() * 100.0f)))) + " %";
        }

        public float getValue() {
            return LoadingAddon.this.f10020Vj.bpP() * 100.0f;
        }
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$b */
    /* compiled from: a */
    class C4629b implements Runnable {
        private final /* synthetic */ C3002mq.C3003a btc;

        C4629b(C3002mq.C3003a aVar) {
            this.btc = aVar;
        }

        public void run() {
            this.btc.execute();
        }
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$a */
    class C4628a implements C0454GJ {
        C4628a() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            LoadingAddon.this.f10021kj.aVU().mo13975h(new C6958awx());
        }
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$g */
    /* compiled from: a */
    private class C4634g implements aJP {
        /* access modifiers changed from: private */
        public boolean dtt;
        private float fRG;
        private boolean fRH;

        private C4634g() {
            this.fRG = 0.0f;
            this.dtt = false;
            this.fRH = false;
        }

        /* synthetic */ C4634g(LoadingAddon loadingAddon, C4634g gVar) {
            this();
        }

        private void finish() {
        }

        /* renamed from: ad */
        public void mo9474ad(int i, int i2) {
            float f;
            if (LoadingAddon.this.f10013Vc != null) {
                if (i == 0) {
                    f = 0.0f;
                } else {
                    f = 1.0f - (((float) i2) / ((float) i));
                }
                if (f > this.fRG) {
                    this.fRG = f;
                }
                if (this.fRG > 0.9f) {
                    this.fRG = 0.9f;
                }
                if (i2 == 0) {
                    this.fRH = true;
                    if (!this.dtt) {
                        finish();
                    }
                }
            }
        }
    }

    /* renamed from: taikodom.addon.loading.LoadingAddon$h */
    /* compiled from: a */
    private class C4635h extends WrapRunnable {
        private static final float fUN = 1.0f;
        private float dWb;
        private float dWc;
        private int dWd;

        private C4635h() {
            this.dWb = 0.0f;
            this.dWc = 0.0f;
            this.dWd = 0;
        }

        /* synthetic */ C4635h(LoadingAddon loadingAddon, C4635h hVar) {
            this();
        }

        public void run() {
            this.dWc = Math.min(chz() + this.dWc, this.dWb);
            LoadingAddon.this.f10014Vd.repaint(50);
            if (this.dWb == 1.0f && this.dWc == this.dWb) {
                this.dWd++;
                if (this.dWd == 5) {
                    LoadingAddon.this.close();
                }
            }
        }

        private float chz() {
            if (this.dWb == 1.0f) {
                return 0.025f;
            }
            return 0.01f;
        }

        /* access modifiers changed from: package-private */
        public float bpP() {
            return this.dWc;
        }

        /* access modifiers changed from: package-private */
        public void chA() {
            this.dWb = 0.0f;
            this.dWc = 0.0f;
            this.dWd = 0;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: hp */
        public void mo24209hp(float f) {
            if (f > this.dWb && f <= 1.0f) {
                this.dWb = f;
            }
        }

        /* access modifiers changed from: package-private */
        public void done() {
            mo24209hp(1.0f);
        }
    }
}
