package taikodom.addon.newmarket;

import game.network.message.externalizable.C0939Nn;
import game.script.hangar.Bay;
import game.script.item.BulkItemType;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.newmarket.BuyOrderInfo;
import game.script.newmarket.CommercialOrderInfo;
import game.script.newmarket.ListContainer;
import game.script.newmarket.SellOrderInfo;
import game.script.player.Player;
import game.script.player.PlayerAssistant;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.aaa.C0773LB;
import logic.baa.C4068yr;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.market.C1279Su;
import taikodom.addon.market.C2734jK;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.addon.pda.PdaAddon;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

@TaikodomAddon("taikodom.addon.newmarket")
/* renamed from: a.eF */
/* compiled from: a */
public class C2331eF {
    private static /* synthetic */ int[] dEx;
    private static /* synthetic */ int[] dEy;
    private static /* synthetic */ int[] dEz;
    /* access modifiers changed from: private */
    public C2495fo apa;
    /* access modifiers changed from: private */
    public C3442re dEs;
    /* access modifiers changed from: private */
    public C2092bw dEt;
    /* access modifiers changed from: private */
    public List<BuyOrderInfo> dqM;
    /* access modifiers changed from: private */
    public List<SellOrderInfo> dqN;
    /* renamed from: kj */
    public IAddonProperties f6688kj;
    /* renamed from: P */
    private Player f6687P = this.f6688kj.getPlayer();
    /* access modifiers changed from: private */
    public PlayerAssistant dqK = this.f6687P.dyl();
    private C0440GH dEu;
    private boolean dEv;
    /* access modifiers changed from: private */
    private boolean dEw;

    public C2331eF(IAddonProperties vWVar, C2495fo foVar) {
        this.f6688kj = vWVar;
        this.dEs = new C3442re(this.f6688kj, this, foVar);
        this.apa = foVar;
        this.dqM = new ArrayList();
        this.dqN = new ArrayList();
        this.f6688kj.aVU().mo13965a(C0773LB.class, new C2335c());
    }

    static /* synthetic */ int[] bjl() {
        int[] iArr = dEx;
        if (iArr == null) {
            iArr = new int[C6172aho.C1894a.values().length];
            try {
                iArr[C6172aho.C1894a.EQUIPPED_SHIP.ordinal()] = 7;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C6172aho.C1894a.INVALID_CALL_PLAYER.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C6172aho.C1894a.INVALID_ITEM.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C6172aho.C1894a.INVALID_LOCATION.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C6172aho.C1894a.INVALID_PLAYER_INPUT.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[C6172aho.C1894a.ITEM_BOUNDED.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[C6172aho.C1894a.NO_BALANCE.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            dEx = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] bjm() {
        int[] iArr = dEy;
        if (iArr == null) {
            iArr = new int[C3402rH.C3403a.values().length];
            try {
                iArr[C3402rH.C3403a.AGENT_ON_DIFERENT_STATION.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C3402rH.C3403a.DIFERENT_TYPE.ordinal()] = 7;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C3402rH.C3403a.EQUIPPED_SHIP.ordinal()] = 15;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C3402rH.C3403a.HANGAR_EMPTY.ordinal()] = 11;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C3402rH.C3403a.INVALID_CALL_PLAYER.ordinal()] = 10;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[C3402rH.C3403a.INVALID_LOCATION.ordinal()] = 12;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[C3402rH.C3403a.ITEM_ADDITION.ordinal()] = 14;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[C3402rH.C3403a.ITEM_BOUNDED.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[C3402rH.C3403a.MINIMAL_AMOUNT_NOT_REACHED.ordinal()] = 2;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[C3402rH.C3403a.NOT_ENOUGH_AMOUNT_ON_ORDER.ordinal()] = 3;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[C3402rH.C3403a.NO_BALANCE.ordinal()] = 5;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[C3402rH.C3403a.NO_FOUNDS.ordinal()] = 1;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[C3402rH.C3403a.NO_STORAGE.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[C3402rH.C3403a.TRANSACTON_ALREADY_CANCELED.ordinal()] = 13;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[C3402rH.C3403a.UNABLE_TO_TRANSFER.ordinal()] = 6;
            } catch (NoSuchFieldError e15) {
            }
            dEy = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] bjn() {
        int[] iArr = dEz;
        if (iArr == null) {
            iArr = new int[C3902wX.C3903a.values().length];
            try {
                iArr[C3902wX.C3903a.NO_STORAGE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            dEz = iArr;
        }
        return iArr;
    }

    public void bjf() {
        ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2336d())).dBC();
        ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2332a())).dBE();
    }

    /* renamed from: v */
    private C4068yr m29421v(ItemType jCVar) {
        return ((PdaAddon) this.f6688kj.mo11830U(PdaAddon.class)).mo24484aa(jCVar);
    }

    private void bjg() {
        if (this.dEv && this.dEw) {
            this.dEv = false;
            this.dEw = false;
            ArrayList arrayList = new ArrayList();
            for (BuyOrderInfo eP : this.dqM) {
                C4068yr v = m29421v(eP.mo8620eP());
                if (v != null && !this.f6687P.dyl().dBQ().containsKey(v)) {
                    arrayList.add(v);
                }
            }
            for (SellOrderInfo eP2 : this.dqN) {
                C4068yr v2 = m29421v(eP2.mo8620eP());
                if (v2 != null && !this.f6687P.dyl().dBQ().containsKey(v2)) {
                    arrayList.add(v2);
                }
            }
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2333b())).mo11957V(arrayList);
        }
    }

    /* access modifiers changed from: private */
    public void bjh() {
        this.dEv = true;
        bjg();
    }

    /* access modifiers changed from: private */
    public void bji() {
        this.dEw = true;
        bjg();
    }

    public void bjj() {
        if (this.f6687P.bQB()) {
            bio().aRY().mo4911Kk();
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2337e(this))).dBM();
            return;
        }
        mo17978eR(this.f6688kj.translate("Buy order outside station not allowed."));
    }

    public void bjk() {
        if (this.f6687P.bQB()) {
            this.dEu = new C0440GH(this.f6688kj, this, this.apa);
        } else {
            mo17978eR(this.f6688kj.translate("Sell order outside station not allowed."));
        }
    }

    /* renamed from: a */
    public void mo17964a(ItemType jCVar, int i, int i2, long j, long j2) {
        try {
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2339f())).mo11982f(this.f6688kj.getPlayer(), jCVar, i, i2, j, j2);
        } catch (C6172aho e) {
            e.printStackTrace();
        }
    }

    public void dispose() {
    }

    /* access modifiers changed from: protected */
    public List<BuyOrderInfo> bde() {
        return this.dqM;
    }

    /* access modifiers changed from: protected */
    public List<SellOrderInfo> bdf() {
        return this.dqN;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m29406a(C6172aho.C1894a aVar) {
        switch (bjl()[aVar.ordinal()]) {
            case 1:
                mo17978eR(this.f6688kj.translate("Operação inválida. Tente novamente."));
                return;
            case 2:
                mo17978eR(this.f6688kj.translate("Saldo insuficiente."));
                return;
            case 3:
                mo17978eR(this.f6688kj.translate("Usuário inválido. Tente novamente."));
                return;
            case 4:
                mo17978eR(this.f6688kj.translate("Item inválido."));
                return;
            case 5:
                mo17978eR(this.f6688kj.translate("Localização inválida."));
                return;
            case 6:
                mo17978eR(this.f6688kj.translate("Este item não pode ser vendido."));
                return;
            case 7:
                mo17978eR(this.f6688kj.translate("Naves equipadas não podem ser vendidas."));
                return;
            default:
                this.f6688kj.getLog().error("Unknown Error.");
                return;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m29411a(C3402rH rHVar) {
        switch (bjm()[rHVar.mo21573YJ().ordinal()]) {
            case 1:
                mo17978eR(this.f6688kj.translate("Saldo insuficiente."));
                return;
            case 2:
                mo17978eR(this.f6688kj.translate("Valor mínimo exigido não alcançado."));
                return;
            case 3:
                mo17978eR(this.f6688kj.translate("Saldo insuficiente para a transação."));
                return;
            case 4:
                mo17978eR(this.f6688kj.translate("Esta transação não pode ser efetuada nesta estação."));
                return;
            case 5:
                mo17978eR(this.f6688kj.translate("Saldo insuficiente."));
                return;
            case 6:
                mo17978eR(this.f6688kj.translate("Não é possivel transterir o item. Favor checar alocação de espaço."));
                return;
            case 7:
                mo17978eR(this.f6688kj.translate("Tipo de item diferente do esperado."));
                return;
            case 8:
                mo17978eR(this.f6688kj.translate("Este item não pode ser vendido."));
                return;
            case 9:
                mo17978eR(this.f6688kj.translate("Local de armazenamento inexistente ou insuficiente."));
                return;
            case 10:
                mo17978eR(this.f6688kj.translate("Usuário inválido. Tente novamente."));
                return;
            case 11:
                mo17978eR(this.f6688kj.translate("Hangar completamente ocupado."));
                return;
            case 12:
                mo17978eR(this.f6688kj.translate("Localização inválida."));
                return;
            case 13:
                mo17978eR(this.f6688kj.translate("Transação foi cancelada pelo possuidor."));
                return;
            case 14:
                mo17978eR(rHVar.mo21574YK().get());
                return;
            case 15:
                mo17978eR(this.f6688kj.translate("Naves equipadas não podem ser vendidas."));
                return;
            default:
                this.f6688kj.getLog().error("Unknown error.");
                return;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m29412a(C3902wX.C3903a aVar) {
        switch (bjn()[aVar.ordinal()]) {
            case 1:
                mo17978eR(this.f6688kj.translate("Local de armazenamento inexistente ou insuficiente."));
                return;
            default:
                this.f6688kj.getLog().error("Unknown error.");
                return;
        }
    }

    /* renamed from: a */
    public void mo17963a(Item auq, int i, long j, long j2) {
        try {
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2340g())).mo11965c(auq, i, j, j2);
        } catch (C6172aho e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public void mo17967a(SellOrderInfo xTVar, C0314EF ef) {
        if (!this.f6687P.bQB()) {
            mo17978eR(this.f6688kj.translate("Sell order outside station not allowed."));
        } else if (this.f6687P.bhE() != xTVar.mo8622eT()) {
            mo17978eR(this.f6688kj.translate("Item not found in station."));
        } else {
            new C1279Su(this.f6688kj, this, xTVar, ef, this.apa);
        }
    }

    /* renamed from: b */
    public void mo17968b(BuyOrderInfo sfVar) {
        boolean z;
        if (!(this.f6687P.bhE() instanceof Station)) {
            mo17978eR(this.f6688kj.translate("Buy order outside station not allowed."));
        } else if (this.f6687P.bhE() != sfVar.mo8622eT()) {
            mo17978eR(this.f6688kj.translate("Item not found in station."));
        } else {
            if (m29413a((ItemLocation) this.f6687P.dxU(), sfVar) || m29413a((ItemLocation) this.f6687P.bQx().afI(), sfVar)) {
                z = true;
            } else if (sfVar.mo8620eP() instanceof ShipType) {
                Iterator it = this.f6687P.mo14346aD((Station) this.f6687P.bhE()).mo20056KU().iterator();
                while (true) {
                    if (it.hasNext()) {
                        Bay ajm = (Bay) it.next();
                        if (!ajm.isEmpty() && this.f6687P.bQx() != ajm.mo9625al() && m29413a((ItemLocation) ajm, sfVar)) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
            } else {
                z = false;
            }
            if (!z) {
                mo17978eR(this.f6688kj.translate("Item not found."));
            }
        }
    }

    /* renamed from: a */
    private boolean m29413a(ItemLocation aag, BuyOrderInfo sfVar) {
        int i;
        Iterator<Item> iterator = aag.getIterator();
        Item auq = null;
        int i2 = 0;
        boolean z = false;
        while (iterator.hasNext()) {
            Item next = iterator.next();
            if (sfVar.mo8620eP() == next.bAP()) {
                i2 += next.mo302Iq();
                auq = next;
                z = true;
            }
        }
        if (z) {
            if (!(sfVar.mo8620eP() instanceof BulkItemType)) {
                i = 1;
            } else {
                i = i2;
            }
            new C2734jK(this.f6688kj, this, sfVar, auq, i, this.apa, aag);
        }
        return z;
    }

    /* renamed from: a */
    public void mo17965a(BuyOrderInfo sfVar, Item auq) {
        try {
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2341h(sfVar, auq))).mo11966c(sfVar, auq);
        } catch (C3402rH e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public void mo17966a(SellOrderInfo xTVar, int i, C0314EF ef) {
        try {
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2342i(xTVar, i))).mo11967c(xTVar, i, ef);
        } catch (C3402rH e) {
            e.printStackTrace();
        }
    }

    /* renamed from: ZA */
    public Set<Station> mo17962ZA() {
        return this.dqK.mo11959ZA();
    }

    /* renamed from: c */
    public void mo17976c(BuyOrderInfo sfVar) {
        this.dqK.mo11980e(sfVar);
        bde().remove(sfVar);
        this.dEs.bWw();
    }

    /* renamed from: b */
    public void mo17969b(SellOrderInfo xTVar) {
        try {
            ((PlayerAssistant) C3582se.m38985a(this.dqK, (C6144ahM<?>) new C2343j(xTVar))).mo11969d(xTVar);
        } catch (C3902wX e) {
            e.printStackTrace();
        }
    }

    /* renamed from: eR */
    public void mo17978eR(String str) {
        ((MessageDialogAddon) this.f6688kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, str, this.f6688kj.translate("Ok"));
    }

    public C3442re bio() {
        return this.dEs;
    }

    /* renamed from: a.eF$c */
    /* compiled from: a */
    class C2335c extends C6124ags<C0773LB> {
        C2335c() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0773LB lb) {
            CommercialOrderInfo bfN = lb.bfN();
            if (bfN instanceof BuyOrderInfo) {
                C2331eF.this.dqM.remove(bfN);
            } else {
                C2331eF.this.dqN.remove(bfN);
            }
            C2331eF.this.dEs.bWw();
            return false;
        }
    }

    /* renamed from: a.eF$d */
    /* compiled from: a */
    class C2336d implements C6144ahM<Boolean> {
        C2336d() {
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                C2331eF.this.dqM.clear();
                for (ListContainer list : C2331eF.this.dqK.dBI().values()) {
                    for (BuyOrderInfo add : list.getList()) {
                        C2331eF.this.dqM.add(add);
                    }
                }
            }
            C2331eF.this.bjh();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.eF$a */
    class C2332a implements C6144ahM<Boolean> {
        C2332a() {
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                C2331eF.this.dqN.clear();
                for (ListContainer list : C2331eF.this.dqK.dBK().values()) {
                    for (SellOrderInfo add : list.getList()) {
                        C2331eF.this.dqN.add(add);
                    }
                }
            }
            C2331eF.this.bji();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.eF$b */
    /* compiled from: a */
    class C2333b implements C6144ahM<Void> {
        C2333b() {
        }

        /* renamed from: a */
        public void mo1931n(Void voidR) {
            SwingUtilities.invokeLater(new C2334a());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: a.eF$b$a */
        class C2334a implements Runnable {
            C2334a() {
            }

            public void run() {
                C2331eF.this.dEs.bWw();
                C2331eF.this.dEs.bWx();
            }
        }
    }

    /* renamed from: a.eF$e */
    /* compiled from: a */
    class C2337e implements C6144ahM<List<ItemType>> {

        /* renamed from: IH */
        private final /* synthetic */ C2331eF f6694IH;

        C2337e(C2331eF eFVar) {
            this.f6694IH = eFVar;
        }

        /* renamed from: b */
        public void mo1931n(List<ItemType> list) {
            C2331eF.this.bio().aRY().mo4912Kl();
            SwingUtilities.invokeLater(new C2338a(this.f6694IH, list));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: a.eF$e$a */
        class C2338a implements Runnable {

            /* renamed from: IH */
            private final /* synthetic */ C2331eF f6695IH;
            private final /* synthetic */ List aef;

            C2338a(C2331eF eFVar, List list) {
                this.f6695IH = eFVar;
                this.aef = list;
            }

            public void run() {
                C2331eF.this.dEt = new C2092bw(C2331eF.this.f6688kj, this.f6695IH, this.aef, C2331eF.this.apa);
            }
        }
    }

    /* renamed from: a.eF$f */
    /* compiled from: a */
    class C2339f implements C6144ahM<BuyOrderInfo> {
        C2339f() {
        }

        /* renamed from: a */
        public void mo1931n(BuyOrderInfo sfVar) {
            if (sfVar != null) {
                C2331eF.this.bde().add(sfVar);
                C2331eF.this.dEs.bWw();
                C2331eF.this.dEs.bWx();
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            if (th instanceof C6172aho) {
                C2331eF.this.m29406a(((C6172aho) th).cVC());
            }
        }
    }

    /* renamed from: a.eF$g */
    /* compiled from: a */
    class C2340g implements C6144ahM<SellOrderInfo> {
        C2340g() {
        }

        /* renamed from: a */
        public void mo1931n(SellOrderInfo xTVar) {
            if (xTVar != null) {
                C2331eF.this.bdf().add(xTVar);
                C2331eF.this.dEs.bWw();
                C2331eF.this.dEs.bWx();
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            if (th instanceof C6172aho) {
                C2331eF.this.m29406a(((C6172aho) th).cVC());
            }
        }
    }

    /* renamed from: a.eF$h */
    /* compiled from: a */
    class C2341h implements C6144ahM<Boolean> {

        /* renamed from: IX */
        private final /* synthetic */ BuyOrderInfo f6699IX;

        /* renamed from: IY */
        private final /* synthetic */ Item f6700IY;

        C2341h(BuyOrderInfo sfVar, Item auq) {
            this.f6699IX = sfVar;
            this.f6700IY = auq;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            this.f6699IX.mo8613K(this.f6699IX.dBu() + this.f6700IY.mo302Iq());
            if (this.f6699IX.mo8627ff() <= 0) {
                C2331eF.this.dqM.remove(this.f6699IX);
                C2331eF.this.dEs.bWw();
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            if (th instanceof C3402rH) {
                C2331eF.this.m29411a((C3402rH) th);
            } else {
                C2331eF.this.mo17978eR(C2331eF.this.f6688kj.translate("Um erro inesperado ocorreu. Sua transação não pode ser concluída."));
            }
        }
    }

    /* renamed from: a.eF$i */
    /* compiled from: a */
    class C2342i implements C6144ahM<Boolean> {

        /* renamed from: IZ */
        private final /* synthetic */ SellOrderInfo f6702IZ;

        /* renamed from: Ja */
        private final /* synthetic */ int f6703Ja;

        C2342i(SellOrderInfo xTVar, int i) {
            this.f6702IZ = xTVar;
            this.f6703Ja = i;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            this.f6702IZ.mo8613K(this.f6702IZ.dBu() + this.f6703Ja);
            if (this.f6702IZ.mo8627ff() <= 0) {
                C2331eF.this.dqN.remove(this.f6702IZ);
                C2331eF.this.dEs.bWw();
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            if (th instanceof C3402rH) {
                C2331eF.this.m29411a((C3402rH) th);
            } else {
                C2331eF.this.mo17978eR(C2331eF.this.f6688kj.translate("Um erro inesperado ocorreu. Sua transação não pode ser concluída."));
            }
        }
    }

    /* renamed from: a.eF$j */
    /* compiled from: a */
    class C2343j implements C6144ahM<Boolean> {

        /* renamed from: IZ */
        private final /* synthetic */ SellOrderInfo f6705IZ;

        C2343j(SellOrderInfo xTVar) {
            this.f6705IZ = xTVar;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                C2331eF.this.bdf().remove(this.f6705IZ);
                C2331eF.this.dEs.bWw();
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            if (th instanceof C3902wX) {
                C2331eF.this.m29412a(((C3902wX) th).bjI());
            } else {
                C2331eF.this.mo17978eR(C2331eF.this.f6688kj.translate("Um erro inesperado ocorreu. Sua transação não pode ser concluída."));
            }
        }
    }
}
