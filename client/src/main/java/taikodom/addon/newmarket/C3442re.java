package taikodom.addon.newmarket;

import game.network.message.externalizable.*;
import game.script.Actor;
import game.script.hangar.Bay;
import game.script.item.ComponentType;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import game.script.newmarket.BuyOrderInfo;
import game.script.newmarket.CommercialOrderInfo;
import game.script.newmarket.SellOrderInfo;
import game.script.newmarket.store.Store;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.Station;
import game.script.space.Node;
import game.script.space.StellarSystem;
import game.script.storage.Storage;
import logic.aaa.C2123cF;
import logic.aaa.C5783aaP;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.*;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.addon.neo.oninet.OniNetAddon;

import javax.swing.Timer;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.*;

@TaikodomAddon("taikodom.addon.newmarket")
/* renamed from: a.re */
/* compiled from: a */
public class C3442re {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9058P;
    /* access modifiers changed from: private */
    public C5923acz akV;
    /* access modifiers changed from: private */
    public C5923acz akW;
    /* access modifiers changed from: private */
    public JTextField akY;
    /* access modifiers changed from: private */
    public C2331eF aoM;
    /* access modifiers changed from: private */
    public C2495fo apa;
    /* access modifiers changed from: private */
    public C3428rT<C3131oI> bCD;
    /* access modifiers changed from: private */
    public C5923acz fwU;
    /* access modifiers changed from: private */
    public C5923acz fwV;
    /* access modifiers changed from: private */
    public Table fwW;
    /* access modifiers changed from: private */
    public Table fwX;
    /* access modifiers changed from: private */
    public ActionListener fxA;
    /* access modifiers changed from: private */
    public JComboBox fxc;
    /* access modifiers changed from: private */
    public JTextField fxe;
    /* access modifiers changed from: private */
    public Table fxf;
    /* access modifiers changed from: private */
    public Table fxg;
    /* access modifiers changed from: private */
    public JCheckBox fxj;
    /* access modifiers changed from: private */
    public JCheckBox fxk;
    /* access modifiers changed from: private */
    public Storage fxq;
    /* access modifiers changed from: private */
    public aTX fxs;
    /* access modifiers changed from: private */
    public boolean fxz = false;
    /* renamed from: kj */
    public IAddonProperties f9059kj;
    /* renamed from: nM */
    public InternalFrame f9060nM;
    private JCheckBox akX;
    private C3428rT<C5783aaP> aoN;
    private C6622aqW dcE;
    private JButton fwY;
    private JButton fwZ;
    private JButton fxa;
    private JButton fxb;
    private JCheckBox fxd;
    private JButton fxh;
    private JButton fxi;
    private JCheckBox fxl;
    private RowFilter fxm;
    private RowFilter fxn;
    private RowFilter fxo;
    private RowFilter fxp;
    private JButton fxr;
    private C2122cE fxt;
    private C2122cE fxu;
    private C2122cE fxv;
    private C2122cE fxw;
    /* access modifiers changed from: private */
    private C2122cE fxx;
    /* access modifiers changed from: private */
    private C2122cE fxy;
    /* renamed from: wz */
    private C3428rT<C3689to> f9061wz;

    public C3442re(IAddonProperties vWVar, C2331eF eFVar, C2495fo foVar) {
        this.f9059kj = vWVar;
        this.aoM = eFVar;
        this.f9058P = this.f9059kj.getEngineGame().mo4089dL();
        this.apa = foVar;
        this.dcE = new C3483h("market", this.f9059kj.translate("Market"), "market");
        this.dcE.mo15602jH(this.f9059kj.translate("This is the market, where you can find equipments and goods to buy and sell."));
        this.dcE.mo15603jI("MARKET_WINDOW");
        m38528iG();
        this.fxs = new aTX(this.dcE, 899, "market-commands.css", this);
        this.f9059kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f9059kj.aVU().mo13965a(C3689to.class, this.f9061wz);
        this.f9059kj.aVU().publish(new C5344aFs(this.dcE, 999, true));
        this.f9059kj.aVU().publish(this.fxs);
        this.f9059kj.mo2317P(this);
        if (!m38530iL() && this.f9058P.cXm().ordinal() < aMU.BOUGHT_MISSION_ITEM.ordinal()) {
            this.fxz = true;
        }
    }

    /* renamed from: he */
    public static C3666tU m38526he(long j) {
        if (((float) j) < C3666tU.DISPOSED.getLifeTime()) {
            return C3666tU.DISPOSED;
        }
        if (((float) j) < C3666tU.VERY_SHORT.getLifeTime()) {
            return C3666tU.VERY_SHORT;
        }
        if (((float) j) < C3666tU.SHORT.getLifeTime()) {
            return C3666tU.SHORT;
        }
        if (((float) j) < C3666tU.AVERAGE.getLifeTime()) {
            return C3666tU.AVERAGE;
        }
        if (((float) j) < C3666tU.LONG.getLifeTime()) {
            return C3666tU.LONG;
        }
        return C3666tU.VERY_LONG;
    }

    /* renamed from: iG */
    private void m38528iG() {
        this.f9061wz = new C3484i();
        this.aoN = new C3485j();
        if (!m38530iL() && aMU.MISSION_2_TAKEN.ordinal() >= this.f9059kj.getPlayer().cXm().ordinal() && this.bCD == null) {
            this.bCD = new C3475b();
            this.f9059kj.aVU().mo13965a(C3131oI.class, this.bCD);
        }
    }

    private void bWg() {
        this.fxq = null;
        if (this.f9058P.bQB()) {
            ((Station) C3582se.m38985a((Station) this.f9058P.bhE(), (C6144ahM<?>) new C3478c())).mo602am(this.f9058P);
        }
        if (!isCreated()) {
            m38494Im();
        } else if (isVisible()) {
            hide();
            this.f9059kj.aVU().mo13974g(new C6018aeq("market", ""));
            this.f9059kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            return;
        }
        m38546xK();
        if (!m38530iL() && aMU.MISSION_2_TAKEN.equals(this.f9059kj.getPlayer().cXm())) {
            this.f9059kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.OPENED_MARKET));
        }
        m38529iJ();
        this.f9060nM.center();
        this.aoM.bjf();
        bWw();
        bWx();
    }

    /* renamed from: iJ */
    private void m38529iJ() {
        this.fxc.removeAllItems();
        this.fxc.addItem(new C3469a(C0314EF.CARGO_HOLD, this.f9059kj.translate("Cargo Hold")));
        this.fxc.addItem(new C3469a(C0314EF.STORAGE, this.f9059kj.translate("Storage")));
        this.fxc.addItem(new C3469a(C0314EF.HANGAR, this.f9059kj.translate("Hangar")));
        this.akY.setText("");
        this.fxe.setText("");
    }

    private void bWh() {
        this.akV.bOW();
        ((DefaultMutableTreeNode) this.akV.getModel().getRoot()).setUserObject(this.f9059kj.translate("Items"));
        HashMap hashMap = new HashMap();
        m38505a((Map<ItemTypeCategory, Set<ItemTypeCategory>>) hashMap, (List<? extends CommercialOrderInfo>) this.aoM.bde());
        m38505a((Map<ItemTypeCategory, Set<ItemTypeCategory>>) hashMap, (List<? extends CommercialOrderInfo>) this.aoM.bdf());
        ArrayList<ItemTypeCategory> arrayList = new ArrayList<>(hashMap.keySet());
        C3479d dVar = new C3479d();
        Collections.sort(arrayList, dVar);
        for (ItemTypeCategory aai : arrayList) {
            C2482fg fgVar = new C2482fg(aai);
            this.akV.mo12740a(fgVar);
            ArrayList<ItemTypeCategory> arrayList2 = new ArrayList<>((Collection) hashMap.get(aai));
            Collections.sort(arrayList2, dVar);
            for (ItemTypeCategory fgVar2 : arrayList2) {
                fgVar.add(new C2482fg(fgVar2));
            }
        }
        this.akV.expandPath(this.akV.bOY());
        this.akV.setSelectionPath(this.akV.bOY());
    }

    private void bWi() {
        this.fwU.bOW();
        ((DefaultMutableTreeNode) this.fwU.getModel().getRoot()).setUserObject(this.f9059kj.translate("My Items"));
        HashMap hashMap = new HashMap();
        m38512b((Map<ItemTypeCategory, Set<ItemTypeCategory>>) hashMap, (List<? extends CommercialOrderInfo>) this.aoM.bde());
        m38512b((Map<ItemTypeCategory, Set<ItemTypeCategory>>) hashMap, (List<? extends CommercialOrderInfo>) this.aoM.bdf());
        ArrayList<ItemTypeCategory> arrayList = new ArrayList<>(hashMap.keySet());
        C3480e eVar = new C3480e();
        Collections.sort(arrayList, eVar);
        for (ItemTypeCategory aai : arrayList) {
            C2482fg fgVar = new C2482fg(aai);
            this.fwU.mo12740a(fgVar);
            ArrayList<ItemTypeCategory> arrayList2 = new ArrayList<>((Collection) hashMap.get(aai));
            Collections.sort(arrayList2, eVar);
            for (ItemTypeCategory fgVar2 : arrayList2) {
                fgVar.add(new C2482fg(fgVar2));
            }
        }
        this.fwU.expandPath(this.fwU.bOY());
        this.fwU.setSelectionPath(this.fwU.bOY());
    }

    private void bWj() {
        this.akW.bOW();
        ((DefaultMutableTreeNode) this.akW.getModel().getRoot()).setUserObject(this.f9059kj.translate("Stelar System"));
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        for (Station next : this.aoM.mo17962ZA()) {
            if (next.azJ() != null) {
                StellarSystem Nc = next.azW().mo21606Nc();
                if (!hashMap.containsKey(Nc)) {
                    C1196Re re = new C1196Re(Nc);
                    this.akW.mo12740a(re);
                    hashMap.put(Nc, re);
                }
                if (!hashMap2.containsKey(next.azW())) {
                    C0583IE ie = new C0583IE(next.azW());
                    ((DefaultMutableTreeNode) hashMap.get(Nc)).add(ie);
                    hashMap2.put(next.azW(), ie);
                }
                ((DefaultMutableTreeNode) hashMap2.get(next.azW())).add(new aRV(next));
            }
        }
        this.akW.expandAll();
        m38498a(this.akW, this.f9058P.bhE());
    }

    /* renamed from: a */
    private void m38498a(C5923acz acz, Actor cr) {
        TreeModel model = acz.getModel();
        if (cr instanceof Station) {
            int childCount = model.getChildCount(model.getRoot());
            int i = 0;
            for (int i2 = 0; i2 < childCount; i2++) {
                i++;
                Object child = model.getChild(model.getRoot(), i2);
                int childCount2 = model.getChildCount(child);
                for (int i3 = 0; i3 < childCount2; i3++) {
                    Object child2 = model.getChild(child, i3);
                    int childCount3 = model.getChildCount(child2);
                    int i4 = 0;
                    i++;
                    while (i4 < childCount3) {
                        i++;
                        Object child3 = model.getChild(child2, i4);
                        if (!(child3 instanceof aRV) || ((aRV) child3).getUserObject() != cr) {
                            i4++;
                        } else {
                            acz.setSelectionPath(acz.getPathForRow(i));
                            this.akW.scrollPathToVisible(this.akW.getPathForRow(i));
                            return;
                        }
                    }
                }
            }
        }
        acz.setSelectionPath(acz.bOY());
    }

    private void bWk() {
        this.fwV.bOW();
        ((DefaultMutableTreeNode) this.fwV.getModel().getRoot()).setUserObject(this.f9059kj.translate("Stelar System"));
        HashMap hashMap = new HashMap();
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        m38506a(hashMap, hashMap2, hashMap3, this.aoM.bde());
        m38506a(hashMap, hashMap2, hashMap3, this.aoM.bdf());
        this.fwV.expandAll();
        this.fwV.setSelectionPath(this.fwV.bOY());
    }

    /* renamed from: a */
    private void m38506a(Map<StellarSystem, DefaultMutableTreeNode> map, Map<Node, DefaultMutableTreeNode> map2, Map<Station, DefaultMutableTreeNode> map3, List<? extends CommercialOrderInfo> list) {
        for (CommercialOrderInfo aeu : list) {
            if (aeu.abi() == this.f9058P) {
                StellarSystem Nc = aeu.mo8622eT().azW().mo21606Nc();
                if (!map.containsKey(Nc)) {
                    C1196Re re = new C1196Re(Nc);
                    this.fwV.mo12740a(re);
                    map.put(Nc, re);
                }
                if (!map2.containsKey(aeu.mo8622eT().azW())) {
                    C0583IE ie = new C0583IE(aeu.mo8622eT().azW());
                    map.get(Nc).add(ie);
                    map2.put(aeu.mo8622eT().azW(), ie);
                }
                if (!map3.containsKey(aeu.mo8622eT())) {
                    aRV arv = new aRV(aeu.mo8622eT());
                    map2.get(aeu.mo8622eT().azW()).add(arv);
                    map3.put(aeu.mo8622eT(), arv);
                }
            }
        }
    }

    /* renamed from: a */
    private void m38505a(Map<ItemTypeCategory, Set<ItemTypeCategory>> map, List<? extends CommercialOrderInfo> list) {
        for (CommercialOrderInfo eP : list) {
            ItemTypeCategory HC = eP.mo8620eP().mo19866HC();
            if (HC != null) {
                ItemTypeCategory bJS = HC.bJS();
                if (!map.containsKey(bJS)) {
                    map.put(bJS, new HashSet());
                }
                map.get(bJS).add(HC);
            }
        }
    }

    /* renamed from: b */
    private void m38512b(Map<ItemTypeCategory, Set<ItemTypeCategory>> map, List<? extends CommercialOrderInfo> list) {
        for (CommercialOrderInfo aeu : list) {
            ItemTypeCategory HC = aeu.mo8620eP().mo19866HC();
            if (HC != null && aeu.abi() == this.f9058P) {
                ItemTypeCategory bJS = HC.bJS();
                if (!map.containsKey(bJS)) {
                    map.put(bJS, new HashSet());
                }
                map.get(bJS).add(HC);
            }
        }
    }

    private boolean isCreated() {
        return this.f9060nM != null;
    }

    /* renamed from: xK */
    private void m38546xK() {
        this.f9060nM.setVisible(true);
    }

    /* renamed from: Im */
    private void m38494Im() {
        this.f9060nM = this.f9059kj.bHv().mo16795c(this.apa.getClass(), "market.xml");
        this.f9060nM.addComponentListener(new C0962O("market"));
        this.akY = this.f9060nM.mo4915cd("searchTextField");
        this.fxj = this.f9060nM.mo4915cd("hideIcons");
        this.akV = this.f9060nM.mo4915cd("itemsTree");
        this.akW = this.f9060nM.mo4915cd("stationsTree");
        this.fwW = this.f9060nM.mo4915cd("buyOrderTable");
        this.fxb = this.f9060nM.mo4913cb("buyOrderButton");
        this.akX = this.f9060nM.mo4915cd("onlyUsablesOnBuyTable");
        this.akX.setVisible(false);
        this.fxd = this.f9060nM.mo4915cd("filterMyPossessions");
        this.fxd.setSelected(true);
        this.fwX = this.f9060nM.mo4915cd("sellOrderTable");
        this.fxc = this.f9060nM.mo4914cc("sellOrderDestiny");
        this.fxa = this.f9060nM.mo4913cb("sellOrderButton");
        this.fxl = this.f9060nM.mo4915cd("onlyUsablesOnSellTable");
        this.fxl.setVisible(false);
        this.fxe = this.f9060nM.mo4915cd("mySearchTextField");
        this.fxk = this.f9060nM.mo4915cd("myHideIconsCheckBox");
        this.fwU = this.f9060nM.mo4915cd("myItemsTree");
        this.fwV = this.f9060nM.mo4915cd("myStationsTree");
        this.fxf = this.f9060nM.mo4915cd("myBuyOrderTable");
        this.fwY = this.f9060nM.mo4913cb("newMyBuyOrderButton");
        this.fxi = this.f9060nM.mo4913cb("cancelMyBuyOrderButton");
        this.fxg = this.f9060nM.mo4915cd("mySellOrderTable");
        this.fwZ = this.f9060nM.mo4913cb("newMySellOrderButton");
        this.fxh = this.f9060nM.mo4913cb("cancelMySellOrderButton");
        this.fxr = this.f9060nM.mo4913cb("updateButton");
        m38547xL();
    }

    /* renamed from: xL */
    private void m38547xL() {
        this.f9060nM.addComponentListener(new C3481f());
        this.fxr.addActionListener(new C3482g());
        this.fxd.addActionListener(new C3474ae());
        this.fxj.addActionListener(new C3472ac());
        this.fxk.addActionListener(new C3473ad());
        this.fxi.addActionListener(new C3470aa());
        this.fxh.addActionListener(new C3471ab());
        this.fwY.addActionListener(new C3468Z());
        this.fwZ.addActionListener(new C3467Y());
        this.fxb.addActionListener(new C3466X());
        this.fxa.addActionListener(new C3465W());
        this.fxm = new C3464V();
        this.fxn = new C3462T();
        bWl();
        bWq();
        bWo();
        this.akY.getDocument().addDocumentListener(new C3463U());
        this.akX.addActionListener(new C3458P());
        this.fxl.addActionListener(new C3459Q());
        this.fxo = new C3460R();
        this.fxp = new C3461S();
        bWs();
        bWt();
        this.fxe.getDocument().addDocumentListener(new C3455M());
        C3454L l = new C3454L();
        this.akW.setCellRenderer(l);
        this.akV.setCellRenderer(new C3457O());
        this.fwV.setCellRenderer(l);
        this.fwU.setCellRenderer(new C3456N());
        C3443A a = new C3443A();
        this.akW.addTreeSelectionListener(a);
        this.akV.addTreeSelectionListener(a);
        C3501z zVar = new C3501z();
        this.fwV.addTreeSelectionListener(zVar);
        this.fwU.addTreeSelectionListener(zVar);
    }

    private void bWl() {
        this.fxt = new C3500y();
        this.fxu = new C3447E();
        this.fxv = new C3446D();
        this.fxw = new C3445C();
        this.fxx = new C3444B();
        this.fxy = new C3498w();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m38510a(RowFilter.Entry entry, JTree jTree) {
        boolean z = true;
        try {
            Object value = entry.getValue(2);
            CommercialOrderInfo aeu = (CommercialOrderInfo) entry.getValue(0);
            if (aeu == null) {
                return false;
            }
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
            if (defaultMutableTreeNode != null) {
                boolean equals = value.equals(defaultMutableTreeNode.toString());
                if (!defaultMutableTreeNode.isRoot()) {
                    if (defaultMutableTreeNode.getUserObject() == null) {
                        z = equals;
                    } else if (defaultMutableTreeNode.getUserObject() instanceof StellarSystem) {
                        z = aeu.mo8622eT().azW().mo21606Nc() == defaultMutableTreeNode.getUserObject();
                    } else if (!defaultMutableTreeNode.isLeaf()) {
                        if (aeu.mo8622eT().azW() != defaultMutableTreeNode.getUserObject()) {
                            z = false;
                        }
                    } else if (aeu.mo8622eT() != defaultMutableTreeNode.getUserObject()) {
                        z = false;
                    }
                }
            }
            return z;
        } catch (RuntimeException e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m38517b(RowFilter.Entry entry, JTree jTree) {
        boolean z = true;
        CommercialOrderInfo aeu = (CommercialOrderInfo) entry.getValue(0);
        if (aeu == null) {
            return false;
        }
        DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
        if (defaultMutableTreeNode != null) {
            if (!defaultMutableTreeNode.isRoot()) {
                if (defaultMutableTreeNode.getUserObject() != null) {
                    if (!defaultMutableTreeNode.isLeaf()) {
                        z = aeu.mo8620eP().mo19866HC().bJS() == defaultMutableTreeNode.getUserObject();
                    } else if (aeu.mo8620eP().mo19866HC() != defaultMutableTreeNode.getUserObject()) {
                        z = false;
                    }
                }
            }
            return z;
        }
        z = false;
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m38516b(RowFilter.Entry entry, JTextField jTextField) {
        return ((CommercialOrderInfo) entry.getValue(0)).mo8620eP().mo19891ke().get().toLowerCase().contains(jTextField.getText().toLowerCase());
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m38515b(RowFilter.Entry entry) {
        if (!this.fxd.isSelected() || this.fxq == null) {
            return true;
        }
        CommercialOrderInfo aeu = (CommercialOrderInfo) entry.getValue(0);
        Iterator<Item> iterator = this.fxq.getIterator();
        while (iterator.hasNext()) {
            Item next = iterator.next();
            if (next.bAP() == aeu.mo8620eP()) {
                return next.aNP();
            }
        }
        if (this.f9058P.bQx() != null) {
            Iterator<Item> iterator2 = this.f9058P.bQx().afI().getIterator();
            while (iterator2.hasNext()) {
                Item next2 = iterator2.next();
                if (next2.bAP() == aeu.mo8620eP()) {
                    return next2.aNP();
                }
            }
        }
        for (Bay ajm : this.f9058P.mo14346aD(this.fxq.mo21543eT()).mo20056KU()) {
            if (!ajm.isEmpty() && ajm.mo9625al() != this.f9058P.bQx() && ajm.mo9625al().agH() == aeu.mo8620eP()) {
                return ajm.mo9625al().agr().aNP();
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public boolean m38519c(RowFilter.Entry entry) {
        boolean z = true;
        Object value = entry.getValue(0);
        if (value instanceof BuyOrderInfo) {
            return ((BuyOrderInfo) value).abg() == this.f9058P;
        }
        if (!(value instanceof SellOrderInfo)) {
            return false;
        }
        if (((SellOrderInfo) value).apa() != this.f9058P) {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void bWm() {
        if (this.fwW.getSelectedRow() == -1) {
            mo21705eR(this.f9059kj.translate("Necessario selecionar item de compra"));
        } else {
            this.aoM.mo17968b((BuyOrderInfo) this.fwW.getModel().getValueAt(this.fwW.convertRowIndexToModel(this.fwW.getSelectedRow()), -1));
        }
    }

    /* access modifiers changed from: private */
    public void bWn() {
        if (this.fwX.getSelectedRow() == -1) {
            mo21705eR(this.f9059kj.translate("First select item to buy."));
            return;
        }
        this.aoM.mo17967a((SellOrderInfo) this.fwX.getModel().getValueAt(this.fwX.convertRowIndexToModel(this.fwX.getSelectedRow()), -1), (C0314EF) ((C3469a) this.fxc.getSelectedItem()).getObject());
    }

    private void bWo() {
        this.fwW.setAutoCreateRowSorter(true);
        this.fwW.setModel(new C3499x());
        this.fwW.getRowSorter().setComparator(0, new C3497v());
        this.fwW.getRowSorter().setComparator(3, new C3490o());
        this.fwW.getColumnModel().getColumn(0).setCellRenderer(new C3486k(this.fxj));
        this.fwW.getColumnModel().getColumn(1).setCellRenderer(this.fxt);
        this.fwW.getColumnModel().getColumn(2).setCellRenderer(this.fxu);
        this.fwW.getColumnModel().getColumn(3).setCellRenderer(this.fxx);
        this.fwW.getColumnModel().getColumn(4).setCellRenderer(this.fxw);
        this.fwW.getColumnModel().getColumn(5).setCellRenderer(this.fxy);
        bWp();
        this.fwW.getSelectionModel().addListSelectionListener(new C3489n());
        this.fwW.addMouseListener(new C3492q());
        for (int i = 0; i < 6; i++) {
            TableColumn column = this.fwW.getColumnModel().getColumn(i);
            int preferredWidth = column.getPreferredWidth();
            switch (i) {
                case 0:
                    preferredWidth = 160;
                    break;
                case 1:
                case 2:
                    preferredWidth = 90;
                    break;
                case 3:
                case 4:
                    preferredWidth = 60;
                    break;
                case 5:
                case 6:
                    preferredWidth = 40;
                    break;
            }
            column.setPreferredWidth(preferredWidth);
        }
        this.fwW.getColumnModel().addColumnModelListener(new C3491p());
    }

    /* access modifiers changed from: private */
    public void bWp() {
        boolean z;
        if (this.fwW.getSelectedRow() != -1) {
            if (this.f9058P.bhE() == ((BuyOrderInfo) this.fwW.getModel().getValueAt(this.fwW.convertRowIndexToModel(this.fwW.getSelectedRow()), -1)).mo8622eT() && aMU.FINISHED_NURSERY.compareTo(this.f9058P.cXm()) <= 0) {
                z = true;
                this.fxb.setEnabled(z);
            }
        }
        z = false;
        this.fxb.setEnabled(z);
    }

    private void bWq() {
        this.fwX.setAutoCreateRowSorter(true);
        this.fwX.setModel(new C3494s());
        this.fwX.getRowSorter().setComparator(0, new C3493r());
        this.fwX.getRowSorter().setComparator(3, new C3496u());
        this.fwX.getColumnModel().getColumn(0).setCellRenderer(new C3486k(this.fxj));
        this.fwX.getColumnModel().getColumn(1).setCellRenderer(this.fxt);
        this.fwX.getColumnModel().getColumn(2).setCellRenderer(this.fxu);
        this.fwX.getColumnModel().getColumn(3).setCellRenderer(this.fxx);
        this.fwX.getColumnModel().getColumn(4).setCellRenderer(this.fxw);
        this.fwX.getColumnModel().getColumn(5).setCellRenderer(this.fxy);
        this.fwX.addMouseListener(new C3495t());
        bWr();
        this.fwX.getSelectionModel().addListSelectionListener(new C3487l());
        for (int i = 0; i < 6; i++) {
            TableColumn column = this.fwX.getColumnModel().getColumn(i);
            int preferredWidth = column.getPreferredWidth();
            switch (i) {
                case 0:
                    preferredWidth = 160;
                    break;
                case 1:
                case 2:
                    preferredWidth = 90;
                    break;
                case 3:
                case 4:
                    preferredWidth = 60;
                    break;
                case 5:
                case 6:
                    preferredWidth = 40;
                    break;
            }
            column.setPreferredWidth(preferredWidth);
        }
        this.fwX.getColumnModel().addColumnModelListener(new C3488m());
    }

    /* access modifiers changed from: private */
    public void bWr() {
        boolean z = false;
        if (this.fwX.getSelectedRow() != -1 && this.f9058P.bQB()) {
            if (this.f9058P.bhE() == ((SellOrderInfo) this.fwX.getModel().getValueAt(this.fwX.convertRowIndexToModel(this.fwX.getSelectedRow()), -1)).mo8622eT()) {
                z = !this.fxz;
            }
        }
        this.fxa.setEnabled(z);
    }

    private void bWs() {
        this.fxg.setAutoCreateRowSorter(true);
        this.fxg.setModel(new C3448F());
        this.fxg.getRowSorter().setComparator(0, new C3453K());
        this.fxg.getRowSorter().setComparator(2, new C3452J());
        this.fxg.getColumnModel().getColumn(0).setCellRenderer(new C3486k(this.fxk));
        this.fxg.getColumnModel().getColumn(1).setCellRenderer(this.fxu);
        this.fxg.getColumnModel().getColumn(2).setCellRenderer(this.fxv);
        this.fxg.getColumnModel().getColumn(3).setCellRenderer(this.fxw);
    }

    private void bWt() {
        this.fxf.setAutoCreateRowSorter(true);
        this.fxf.setModel(new C3451I());
        this.fxf.getRowSorter().setComparator(0, new C3450H());
        this.fxf.getRowSorter().setComparator(2, new C3449G());
        this.fxf.getColumnModel().getColumn(0).setCellRenderer(new C3486k(this.fxk));
        this.fxf.getColumnModel().getColumn(1).setCellRenderer(this.fxu);
        this.fxf.getColumnModel().getColumn(2).setCellRenderer(this.fxv);
        this.fxf.getColumnModel().getColumn(3).setCellRenderer(this.fxw);
    }

    /* access modifiers changed from: private */
    public void bWu() {
        this.fwW.getRowSorter().setRowFilter(this.fxm);
        this.fwX.getRowSorter().setRowFilter(this.fxn);
    }

    /* access modifiers changed from: private */
    public void bWv() {
        this.fxf.getRowSorter().setRowFilter(this.fxo);
        this.fxg.getRowSorter().setRowFilter(this.fxp);
    }

    /* access modifiers changed from: private */
    /* renamed from: dn */
    public String m38521dn(long j) {
        if (j < 30000) {
            return this.f9059kj.translate("Updated");
        }
        long j2 = j / 3600000;
        return String.valueOf((j2 - (j2 % 24)) / 24) + " " + this.f9059kj.translate("days");
    }

    private void hide() {
        this.f9060nM.setVisible(false);
    }

    private boolean isVisible() {
        return this.f9060nM != null && this.f9060nM.isVisible();
    }

    public void bWw() {
        if (this.f9060nM != null) {
            this.fwW.getModel().fireTableDataChanged();
            this.fwX.getModel().fireTableDataChanged();
            this.fxf.getModel().fireTableDataChanged();
            this.fxg.getModel().fireTableDataChanged();
        }
    }

    public void bWx() {
        if (this.f9060nM != null) {
            bWh();
            bWi();
            bWj();
            bWk();
        }
    }

    /* renamed from: a */
    private Color m38495a(CommercialOrderInfo aeu, Player aku) {
        Ship al;
        if (aku.bQx() != null && aku.bQx().afI().mo7616U(aeu.mo8620eP())) {
            return Color.GREEN;
        }
        if (this.fxq != null && this.fxq.mo7616U(aeu.mo8620eP())) {
            return Color.ORANGE;
        }
        if (aku.bhE() instanceof Station) {
            for (Bay ajm : aku.mo14346aD((Station) aku.bhE()).mo20056KU()) {
                if (!ajm.isEmpty() && aku.bQx() != (al = ajm.mo9625al()) && aeu.mo8620eP() == al.agH()) {
                    return Color.ORANGE;
                }
            }
        }
        return Color.RED;
    }

    /* renamed from: eR */
    public void mo21705eR(String str) {
        ((MessageDialogAddon) this.f9059kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, str, this.f9059kj.translate("Ok"));
    }

    public void bWy() {
        bWw();
        bWx();
    }

    public void bWz() {
        this.f9059kj.translate("Disposed");
        this.f9059kj.translate("Very Short");
        this.f9059kj.translate("Short");
        this.f9059kj.translate("Average");
        this.f9059kj.translate("Long");
        this.f9059kj.translate("Very Long");
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.dcE.isEnabled()) {
            bWg();
        }
    }

    @C2602hR(mo19255zf = "MARKET_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo21700al(boolean z) {
        if (z) {
            open();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: iL */
    public boolean m38530iL() {
        return this.f9059kj != null && this.f9059kj.ala().aLS().mo22273iL();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m38497a(int i, ActionListener actionListener) {
        Timer timer = new Timer(i, actionListener);
        timer.setRepeats(false);
        timer.start();
    }

    /* access modifiers changed from: private */
    public void bWA() {
        Object selectedItem = this.fxc.getSelectedItem();
        if (selectedItem instanceof C3469a) {
            if (C0314EF.CARGO_HOLD.equals((C0314EF) ((C3469a) selectedItem).getObject())) {
                this.f9059kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.SELECTED_DESTINATION_CONTAINER));
                this.f9059kj.aVU().mo13975h(new C2123cF(this.fxa));
                this.fxz = false;
            } else {
                this.fxz = true;
            }
            bWr();
        }
    }

    public InternalFrame aRY() {
        return this.f9060nM;
    }

    /* renamed from: a.re$a */
    private class C3469a<T> {
        private String name;
        private T object;

        public C3469a(T t, String str) {
            this.name = str;
            this.object = t;
        }

        public T getObject() {
            return this.object;
        }

        public String toString() {
            return this.name;
        }
    }

    /* renamed from: a.re$h */
    /* compiled from: a */
    class C3483h extends C6622aqW {


        C3483h(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return C3442re.this.f9060nM != null && (C3442re.this.f9060nM.isVisible() || C3442re.this.f9060nM.isAnimating());
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            C3442re.this.open();
        }
    }

    /* renamed from: a.re$i */
    /* compiled from: a */
    class C3484i extends C3428rT<C3689to> {
        C3484i() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C3442re.this.f9060nM != null && C3442re.this.f9060nM.isVisible()) {
                toVar.adC();
                C3442re.this.f9060nM.setVisible(false);
            }
        }
    }

    /* renamed from: a.re$j */
    /* compiled from: a */
    class C3485j extends C3428rT<C5783aaP> {

        /* renamed from: Zn */
        private static /* synthetic */ int[] f9062Zn;

        C3485j() {
        }

        /* renamed from: BK */
        static /* synthetic */ int[] m38574BK() {
            int[] iArr = f9062Zn;
            if (iArr == null) {
                iArr = new int[C5783aaP.C1841a.values().length];
                try {
                    iArr[C5783aaP.C1841a.DOCKED.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5783aaP.C1841a.FLYING.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5783aaP.C1841a.JUMPING.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5783aaP.C1841a.ONI.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                f9062Zn = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            switch (m38574BK()[aap.bMO().ordinal()]) {
                case 4:
                    if (C3442re.this.f9060nM != null && C3442re.this.f9060nM.isVisible()) {
                        C3442re.this.f9060nM.setVisible(false);
                        return;
                    }
                    return;
                default:
                    if (C3442re.this.f9060nM != null && C3442re.this.f9060nM.isVisible()) {
                        C3442re.this.f9060nM.setVisible(false);
                        return;
                    }
                    return;
            }
        }
    }

    /* renamed from: a.re$b */
    /* compiled from: a */
    class C3475b extends C3428rT<C3131oI> {
        C3475b() {
        }

        /* renamed from: b */
        public void mo321b(C3131oI oIVar) {
            if (aMU.MISSION_2_TAKEN.ordinal() < C3442re.this.f9059kj.getPlayer().cXm().ordinal()) {
                C3442re.this.f9059kj.aVU().mo13964a(C3442re.this.bCD);
                C3442re.this.bCD = null;
            } else if (C3131oI.C3132a.HIGHLIGHT_MARKET.equals(oIVar.mo20956Us())) {
                AbstractButton c = ((OniNetAddon) C3442re.this.f9059kj.mo11830U(OniNetAddon.class)).mo24394c(C3442re.this.fxs);
                if (c != null) {
                    C3442re.this.m38497a(2000, (ActionListener) new C3476a(c));
                }
            } else if (C3131oI.C3132a.HIGHLIGHT_MISSION_ITEM.equals(oIVar.mo20956Us())) {
                C3442re.this.f9059kj.aVU().mo13975h(new C2123cF(C3442re.this.f9060nM.mo4915cd("sellOrderScroll")));
            } else if (C3131oI.C3132a.HIGHLIGHT_DESTINATION_CONTAINER.equals(oIVar.mo20956Us())) {
                C3442re.this.f9059kj.aVU().mo13975h(new C2123cF(C3442re.this.fxc));
                C3442re.this.bWA();
                C3442re.this.fxA = new C3477b();
                C3442re.this.fxc.addActionListener(C3442re.this.fxA);
            }
        }

        /* renamed from: a.re$b$a */
        class C3476a implements ActionListener {
            private final /* synthetic */ AbstractButton btI;

            C3476a(AbstractButton abstractButton) {
                this.btI = abstractButton;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                C3442re.this.f9059kj.aVU().mo13975h(new C2123cF(this.btI));
            }
        }

        /* renamed from: a.re$b$b */
        /* compiled from: a */
        class C3477b implements ActionListener {
            C3477b() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (C3442re.this.f9058P.cXm().ordinal() >= aMU.BOUGHT_MISSION_ITEM.ordinal()) {
                    C3442re.this.fxc.removeActionListener(this);
                    C3442re.this.fxA = null;
                    C3442re.this.fxz = false;
                    C3442re.this.bWr();
                }
                C3442re.this.bWA();
            }
        }
    }

    /* renamed from: a.re$c */
    /* compiled from: a */
    class C3478c implements C6144ahM<Storage> {
        C3478c() {
        }

        /* renamed from: e */
        public void mo1931n(Storage qzVar) {
            C3442re.this.fxq = qzVar;
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.re$d */
    /* compiled from: a */
    class C3479d implements Comparator<ItemTypeCategory> {
        C3479d() {
        }

        /* renamed from: a */
        public int compare(ItemTypeCategory aai, ItemTypeCategory aai2) {
            return aai.mo12246ke().get().compareTo(aai2.mo12246ke().get());
        }
    }

    /* renamed from: a.re$e */
    /* compiled from: a */
    class C3480e implements Comparator<ItemTypeCategory> {
        C3480e() {
        }

        /* renamed from: a */
        public int compare(ItemTypeCategory aai, ItemTypeCategory aai2) {
            return aai.mo12246ke().get().compareTo(aai2.mo12246ke().get());
        }
    }

    /* renamed from: a.re$f */
    /* compiled from: a */
    class C3481f extends ComponentAdapter {
        C3481f() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C3442re.this.f9059kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, C3442re.this.apa));
        }
    }

    /* renamed from: a.re$g */
    /* compiled from: a */
    class C3482g implements ActionListener {
        C3482g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3442re.this.aoM.bjf();
        }
    }

    /* renamed from: a.re$ae */
    /* compiled from: a */
    class C3474ae implements ActionListener {
        C3474ae() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3442re.this.bWw();
        }
    }

    /* renamed from: a.re$ac */
    /* compiled from: a */
    class C3472ac implements ActionListener {
        C3472ac() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            String str;
            int i;
            String str2;
            int i2 = 50;
            IComponentManager e = ComponentManager.getCssHolder(C3442re.this.fwW);
            if (C3442re.this.fxj.isSelected()) {
                str = "rowHeightNoIcons";
            } else {
                str = "rowHeight";
            }
            String attribute = e.getAttribute(str);
            Table n = C3442re.this.fwW;
            if (attribute != null) {
                i = Integer.parseInt(attribute);
            } else {
                i = 50;
            }
            n.setRowHeight(i);
            IComponentManager e2 = ComponentManager.getCssHolder(C3442re.this.fwX);
            if (C3442re.this.fxj.isSelected()) {
                str2 = "rowHeightNoIcons";
            } else {
                str2 = "rowHeight";
            }
            String attribute2 = e2.getAttribute(str2);
            Table p = C3442re.this.fwX;
            if (attribute2 != null) {
                i2 = Integer.parseInt(attribute2);
            }
            p.setRowHeight(i2);
        }
    }

    /* renamed from: a.re$ad */
    /* compiled from: a */
    class C3473ad implements ActionListener {
        C3473ad() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            String str;
            int i;
            String str2;
            int i2 = 50;
            IComponentManager e = ComponentManager.getCssHolder(C3442re.this.fxf);
            if (C3442re.this.fxk.isSelected()) {
                str = "rowHeightNoIcons";
            } else {
                str = "rowHeight";
            }
            String attribute = e.getAttribute(str);
            Table q = C3442re.this.fxf;
            if (attribute != null) {
                i = Integer.parseInt(attribute);
            } else {
                i = 50;
            }
            q.setRowHeight(i);
            IComponentManager e2 = ComponentManager.getCssHolder(C3442re.this.fxg);
            if (C3442re.this.fxk.isSelected()) {
                str2 = "rowHeightNoIcons";
            } else {
                str2 = "rowHeight";
            }
            String attribute2 = e2.getAttribute(str2);
            Table s = C3442re.this.fxg;
            if (attribute2 != null) {
                i2 = Integer.parseInt(attribute2);
            }
            s.setRowHeight(i2);
        }
    }

    /* renamed from: a.re$aa */
    /* compiled from: a */
    class C3470aa implements ActionListener {
        C3470aa() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C3442re.this.fxf.getSelectedRow() == -1) {
                C3442re.this.mo21705eR(C3442re.this.f9059kj.translate("Necessario selecionar item."));
            } else if (C3442re.this.f9058P.bhE() instanceof Station) {
                C3442re.this.aoM.mo17976c((BuyOrderInfo) C3442re.this.fxf.getModel().getValueAt(C3442re.this.fxf.convertRowIndexToModel(C3442re.this.fxf.getSelectedRow()), -1));
            }
        }
    }

    /* renamed from: a.re$ab */
    /* compiled from: a */
    class C3471ab implements ActionListener {
        C3471ab() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C3442re.this.fxg.getSelectedRow() != -1) {
                C3442re.this.aoM.mo17969b((SellOrderInfo) C3442re.this.fxg.getModel().getValueAt(C3442re.this.fxg.convertRowIndexToModel(C3442re.this.fxg.getSelectedRow()), -1));
            } else {
                C3442re.this.mo21705eR(C3442re.this.f9059kj.translate("Necessario selecionar item."));
            }
        }
    }

    /* renamed from: a.re$Z */
    /* compiled from: a */
    class C3468Z implements ActionListener {
        C3468Z() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3442re.this.aoM.bjj();
        }
    }

    /* renamed from: a.re$Y */
    /* compiled from: a */
    class C3467Y implements ActionListener {
        C3467Y() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3442re.this.aoM.bjk();
        }
    }

    /* renamed from: a.re$X */
    /* compiled from: a */
    class C3466X implements ActionListener {
        C3466X() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3442re.this.bWm();
        }
    }

    /* renamed from: a.re$W */
    /* compiled from: a */
    class C3465W implements ActionListener {
        C3465W() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!C3442re.this.m38530iL() && C3442re.this.fxA != null) {
                C3442re.this.fxc.removeActionListener(C3442re.this.fxA);
                C3442re.this.fxA = null;
                C3442re.this.fxz = false;
                C3442re.this.bWA();
            }
            C3442re.this.bWn();
        }
    }

    /* renamed from: a.re$V */
    /* compiled from: a */
    class C3464V extends RowFilter {
        C3464V() {
        }

        public boolean include(RowFilter.Entry entry) {
            return C3442re.this.m38510a(entry, (JTree) C3442re.this.akW) && C3442re.this.m38517b(entry, (JTree) C3442re.this.akV) && C3442re.this.m38516b(entry, C3442re.this.akY) && C3442re.this.m38515b(entry);
        }
    }

    /* renamed from: a.re$T */
    /* compiled from: a */
    class C3462T extends RowFilter {
        C3462T() {
        }

        public boolean include(RowFilter.Entry entry) {
            return C3442re.this.m38510a(entry, (JTree) C3442re.this.akW) && C3442re.this.m38517b(entry, (JTree) C3442re.this.akV) && C3442re.this.m38516b(entry, C3442re.this.akY);
        }
    }

    /* renamed from: a.re$U */
    /* compiled from: a */
    class C3463U implements DocumentListener {
        C3463U() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
            C3442re.this.bWu();
        }

        public void insertUpdate(DocumentEvent documentEvent) {
            C3442re.this.bWu();
        }

        public void removeUpdate(DocumentEvent documentEvent) {
            C3442re.this.bWu();
        }
    }

    /* renamed from: a.re$P */
    /* compiled from: a */
    class C3458P implements ActionListener {
        C3458P() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3442re.this.bWu();
        }
    }

    /* renamed from: a.re$Q */
    /* compiled from: a */
    class C3459Q implements ActionListener {
        C3459Q() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3442re.this.bWu();
        }
    }

    /* renamed from: a.re$S */
    /* compiled from: a */
    class C3461S extends RowFilter {
        C3461S() {
        }

        public boolean include(RowFilter.Entry entry) {
            return C3442re.this.m38519c(entry) && C3442re.this.m38510a(entry, (JTree) C3442re.this.fwV) && C3442re.this.m38517b(entry, (JTree) C3442re.this.fwU) && C3442re.this.m38516b(entry, C3442re.this.fxe);
        }
    }

    /* renamed from: a.re$M */
    /* compiled from: a */
    class C3455M implements DocumentListener {
        C3455M() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
            C3442re.this.bWv();
        }

        public void insertUpdate(DocumentEvent documentEvent) {
            C3442re.this.bWv();
        }

        public void removeUpdate(DocumentEvent documentEvent) {
            C3442re.this.bWv();
        }
    }

    /* renamed from: a.re$L */
    /* compiled from: a */
    class C3454L extends C0037AU {
        C3454L() {
        }

        /* renamed from: a */
        public Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
            Panel nR;
            JLabel cf;
            if (z3) {
                nR = aur.mo11631nR("stationsTreeLeaf");
                cf = nR.mo4917cf("stationsTreeNameLeaf");
            } else if (z2) {
                nR = aur.mo11631nR("stationsTreeParentExpanded");
                cf = nR.mo4917cf("stationsTreeNameParentExpanded");
            } else {
                nR = aur.mo11631nR("stationsTreeParent");
                cf = nR.mo4917cf("stationsTreeNameParent");
            }
            cf.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: a.re$O */
    /* compiled from: a */
    class C3457O extends C0037AU {
        C3457O() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:16:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0093  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.awt.Component mo307a(aUR r8, java.lang.Object r9, boolean r10, boolean r11, boolean r12, int r13, boolean r14) {
            /*
                r7 = this;
                r3 = 0
                if (r12 == 0) goto L_0x0025
                java.lang.String r0 = "itemsTreeLeaf"
                java.awt.Component r0 = r8.mo11631nR(r0)
                a.aCo r0 = (logic.ui.C5262aCo) r0
                java.lang.String r1 = "itemsTreeNameLeaf"
                javax.swing.JLabel r1 = r0.mo4917cf(r1)
                r2 = r0
            L_0x0012:
                java.lang.String r0 = java.lang.String.valueOf(r9)
                r1.setText(r0)
                javax.swing.tree.DefaultMutableTreeNode r9 = (javax.swing.tree.DefaultMutableTreeNode) r9
                boolean r0 = r9.isRoot()
                if (r0 == 0) goto L_0x0047
                r1.setIcon(r3)
            L_0x0024:
                return r2
            L_0x0025:
                if (r11 == 0) goto L_0x0037
                java.lang.String r0 = "itemsTreeParentExpanded"
                java.awt.Component r0 = r8.mo11631nR(r0)
                a.aCo r0 = (logic.ui.C5262aCo) r0
                java.lang.String r1 = "itemsTreeNameParentExpanded"
                javax.swing.JLabel r1 = r0.mo4917cf(r1)
                r2 = r0
                goto L_0x0012
            L_0x0037:
                java.lang.String r0 = "itemsTreeParent"
                java.awt.Component r0 = r8.mo11631nR(r0)
                a.aCo r0 = (logic.ui.C5262aCo) r0
                java.lang.String r1 = "itemsTreeNameParent"
                javax.swing.JLabel r1 = r0.mo4917cf(r1)
                r2 = r0
                goto L_0x0012
            L_0x0047:
                java.lang.Object r0 = r9.getUserObject()
                boolean r4 = r0 instanceof p001a.C5802aai
                if (r4 == 0) goto L_0x0097
                a.aai r0 = (p001a.C5802aai) r0
                a.tC r4 = r0.mo12100sK()
                if (r4 == 0) goto L_0x0097
                a.re r4 = taikodom.addon.newmarket.C3442re.this
                a.vW r4 = r4.f9059kj
                a.axa r4 = r4.bHv()
                a.azB r4 = r4.adz()
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                a.aHa r6 = logic.swing.C5378aHa.ICONOGRAPHY
                java.lang.String r6 = r6.getPath()
                java.lang.String r6 = java.lang.String.valueOf(r6)
                r5.<init>(r6)
                a.tC r0 = r0.mo12100sK()
                java.lang.String r0 = r0.getHandle()
                java.lang.StringBuilder r0 = r5.append(r0)
                java.lang.String r0 = r0.toString()
                java.awt.Image r0 = r4.getImage(r0)
            L_0x0088:
                if (r0 == 0) goto L_0x0093
                javax.swing.ImageIcon r3 = new javax.swing.ImageIcon
                r3.<init>(r0)
                r1.setIcon(r3)
                goto L_0x0024
            L_0x0093:
                r1.setIcon(r3)
                goto L_0x0024
            L_0x0097:
                r0 = r3
                goto L_0x0088
            */
            throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.newmarket.C3442re.C3457O.mo307a(a.aUR, java.lang.Object, boolean, boolean, boolean, int, boolean):java.awt.Component");
        }
    }

    /* renamed from: a.re$N */
    /* compiled from: a */
    class C3456N extends C0037AU {
        C3456N() {
        }

        /* renamed from: a */
        public Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
            JLabel cf;
            Component component;
            Image image;
            if (z3) {
                Component component2 = (Panel) aur.mo11631nR("myItemsTreeLeaf");
                cf = component2.mo4917cf("myItemsTreeNameLeaf");
                component = component2;
            } else if (z2) {
                Component component3 = (Panel) aur.mo11631nR("myItemsTreeParentExpanded");
                cf = component3.mo4917cf("myItemsTreeNameParentExpanded");
                component = component3;
            } else {
                Component component4 = (Panel) aur.mo11631nR("myItemsTreeParent");
                cf = component4.mo4917cf("myItemsTreeNameParent");
                component = component4;
            }
            cf.setText(String.valueOf(obj));
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) obj;
            if (defaultMutableTreeNode.isRoot()) {
                cf.setIcon((Icon) null);
            } else {
                Object userObject = defaultMutableTreeNode.getUserObject();
                if (userObject instanceof ItemTypeCategory) {
                    image = C3442re.this.f9059kj.bHv().adz().getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + ((ItemTypeCategory) userObject).mo12100sK().getHandle());
                } else {
                    image = null;
                }
                if (image != null) {
                    cf.setIcon(new ImageIcon(image));
                }
            }
            return component;
        }
    }

    /* renamed from: a.re$A */
    /* compiled from: a */
    class C3443A implements TreeSelectionListener {
        C3443A() {
        }

        public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
            C3442re.this.bWu();
        }
    }

    /* renamed from: a.re$z */
    /* compiled from: a */
    class C3501z implements TreeSelectionListener {
        C3501z() {
        }

        public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
            C3442re.this.bWv();
        }
    }

    /* renamed from: a.re$k */
    /* compiled from: a */
    private class C3486k extends C2122cE {

        /* renamed from: nQ */
        private final JCheckBox f9063nQ;

        public C3486k(JCheckBox jCheckBox) {
            this.f9063nQ = jCheckBox;
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            String str;
            CommercialOrderInfo aeu = (CommercialOrderInfo) obj;
            Panel nR = aur.mo11631nR("itemView");
            if (aeu == null) {
                C3442re.this.bWu();
            } else {
                String str2 = aeu.mo8620eP().mo19891ke().get();
                LabeledIcon cd = nR.mo4915cd("itemIcon");
                if (!this.f9063nQ.isSelected()) {
                    ItemType eP = aeu.mo8620eP();
                    Picture a = cd.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
                    cd.mo2605a((Icon) new C2539gY(eP));
                    cd.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, String.valueOf(aeu.mo8627ff()));
                    if (eP instanceof ComponentType) {
                        ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) eP).cuJ());
                        a.setVisible(true);
                    } else {
                        a.setVisible(false);
                    }
                    cd.setVisible(true);
                    str = str2;
                } else {
                    cd.setVisible(false);
                    str = "[" + aeu.mo8627ff() + "] = " + str2;
                }
                nR.mo4917cf("itemName").setText(str);
            }
            return nR;
        }
    }

    /* renamed from: a.re$y */
    /* compiled from: a */
    class C3500y extends C2122cE {
        C3500y() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("ownerView");
            if (obj instanceof String) {
                nR.setText(C3442re.this.f9059kj.translate("Unknown"));
            } else if (obj instanceof Player) {
                nR.setText(((Player) obj).getName());
            } else if (obj instanceof Store) {
                nR.setText(((Store) obj).dCt().getName());
            }
            return nR;
        }
    }

    /* renamed from: a.re$E */
    /* compiled from: a */
    class C3447E extends C2122cE {
        C3447E() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("stationView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: a.re$D */
    /* compiled from: a */
    class C3446D extends C2122cE {
        C3446D() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("priceView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: a.re$C */
    /* compiled from: a */
    class C3445C extends C2122cE {
        C3445C() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("expirationView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: a.re$B */
    /* compiled from: a */
    class C3444B extends C2122cE {
        C3444B() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("unityView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: a.re$w */
    /* compiled from: a */
    class C3498w extends C2122cE {
        C3498w() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("ageView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: a.re$x */
    /* compiled from: a */
    class C3499x extends AbstractTableModel {
        private static final long serialVersionUID = 5940082737392553002L;

        C3499x() {
        }

        public int getColumnCount() {
            return 6;
        }

        public int getRowCount() {
            return C3442re.this.aoM.bde().size();
        }

        public Object getValueAt(int i, int i2) {
            BuyOrderInfo sfVar = C3442re.this.aoM.bde().get(i);
            if (sfVar.isDisposed()) {
                return null;
            }
            C0847MM abg = sfVar.abg();
            switch (i2) {
                case 1:
                    return abg == null ? "?" : abg;
                case 2:
                    return sfVar.mo8622eT().getName();
                case 3:
                    return new Long(sfVar.mo8621eR());
                case 4:
                    return C3442re.this.f9059kj.translate(C3442re.m38526he(sfVar.getTimeout()).aiP());
                case 5:
                    return C3442re.this.m38521dn(sfVar.dBs());
                default:
                    return sfVar;
            }
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return C3442re.this.f9059kj.translate("Item");
                case 1:
                    return C3442re.this.f9059kj.translate("Buyer");
                case 2:
                    return C3442re.this.f9059kj.translate("Station");
                case 3:
                    return C3442re.this.f9059kj.translate("Price (T$)");
                case 4:
                    return C3442re.this.f9059kj.translate("Life Time");
                case 5:
                    return C3442re.this.f9059kj.translate("Age");
                default:
                    return "$POST_A_TICKET_PLEASE";
            }
        }
    }

    /* renamed from: a.re$v */
    /* compiled from: a */
    class C3497v implements Comparator<BuyOrderInfo> {
        C3497v() {
        }

        /* renamed from: a */
        public int compare(BuyOrderInfo sfVar, BuyOrderInfo sfVar2) {
            return sfVar.mo8620eP().mo19891ke().get().compareTo(sfVar2.mo8620eP().mo19891ke().get());
        }
    }

    /* renamed from: a.re$o */
    /* compiled from: a */
    class C3490o implements Comparator<Long> {
        C3490o() {
        }

        /* renamed from: a */
        public int compare(Long l, Long l2) {
            return (int) (l.longValue() - l2.longValue());
        }
    }

    /* renamed from: a.re$n */
    /* compiled from: a */
    class C3489n implements ListSelectionListener {
        C3489n() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting()) {
                C3442re.this.bWp();
            }
        }
    }

    /* renamed from: a.re$q */
    /* compiled from: a */
    class C3492q extends MouseAdapter {
        C3492q() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 1 && mouseEvent.getClickCount() == 2) {
                C3442re.this.bWm();
            }
        }
    }

    /* renamed from: a.re$p */
    /* compiled from: a */
    class C3491p implements TableColumnModelListener {
        C3491p() {
        }

        public void columnAdded(TableColumnModelEvent tableColumnModelEvent) {
        }

        public void columnMarginChanged(ChangeEvent changeEvent) {
            for (int i = 0; i < 6; i++) {
                C3442re.this.fwX.getColumnModel().getColumn(i).setPreferredWidth(C3442re.this.fwW.getColumnModel().getColumn(i).getPreferredWidth());
            }
        }

        public void columnMoved(TableColumnModelEvent tableColumnModelEvent) {
            tableColumnModelEvent.getFromIndex();
            tableColumnModelEvent.getToIndex();
        }

        public void columnRemoved(TableColumnModelEvent tableColumnModelEvent) {
        }

        public void columnSelectionChanged(ListSelectionEvent listSelectionEvent) {
        }
    }

    /* renamed from: a.re$s */
    /* compiled from: a */
    class C3494s extends AbstractTableModel {
        private static final long serialVersionUID = -5385349074778581747L;

        C3494s() {
        }

        public int getColumnCount() {
            return 6;
        }

        public int getRowCount() {
            return C3442re.this.aoM.bdf().size();
        }

        public Object getValueAt(int i, int i2) {
            SellOrderInfo xTVar = C3442re.this.aoM.bdf().get(i);
            C0847MM apa = xTVar.apa();
            switch (i2) {
                case 1:
                    return apa == null ? "?" : apa;
                case 2:
                    return xTVar.mo8622eT().getName();
                case 3:
                    return new Long(xTVar.mo8621eR());
                case 4:
                    return C3442re.this.f9059kj.translate(C3442re.m38526he(xTVar.getTimeout()).aiP());
                case 5:
                    return C3442re.this.m38521dn(xTVar.dBs());
                default:
                    return xTVar;
            }
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return C3442re.this.f9059kj.translate("Item");
                case 1:
                    return C3442re.this.f9059kj.translate("Seller");
                case 2:
                    return C3442re.this.f9059kj.translate("Station");
                case 3:
                    return C3442re.this.f9059kj.translate("Price (T$)");
                case 4:
                    return C3442re.this.f9059kj.translate("Life Time");
                case 5:
                    return C3442re.this.f9059kj.translate("Age");
                default:
                    return "$POST_A_TICKET_PLEASE";
            }
        }
    }

    /* renamed from: a.re$r */
    /* compiled from: a */
    class C3493r implements Comparator<SellOrderInfo> {
        C3493r() {
        }

        /* renamed from: a */
        public int compare(SellOrderInfo xTVar, SellOrderInfo xTVar2) {
            return xTVar.mo8620eP().mo19891ke().get().compareTo(xTVar2.mo8620eP().mo19891ke().get());
        }
    }

    /* renamed from: a.re$u */
    /* compiled from: a */
    class C3496u implements Comparator<Long> {
        C3496u() {
        }

        /* renamed from: a */
        public int compare(Long l, Long l2) {
            return (int) (l.longValue() - l2.longValue());
        }
    }

    /* renamed from: a.re$t */
    /* compiled from: a */
    class C3495t extends MouseAdapter {
        C3495t() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 1 && mouseEvent.getClickCount() == 2) {
                C3442re.this.bWn();
            }
        }
    }

    /* renamed from: a.re$l */
    /* compiled from: a */
    class C3487l implements ListSelectionListener {
        C3487l() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting()) {
                C3442re.this.bWr();
                if (C3442re.this.fwX.getSelectedRow() != -1) {
                    SellOrderInfo xTVar = (SellOrderInfo) C3442re.this.fwX.getModel().getValueAt(C3442re.this.fwX.convertRowIndexToModel(C3442re.this.fwX.getSelectedRow()), -1);
                    if (!C3442re.this.m38530iL() && aMU.MISSION_2_TAKEN.equals(C3442re.this.f9058P.cXm()) && C3442re.this.f9059kj.ala().aJe().mo19006xF().bZi().equals(xTVar.mo8620eP())) {
                        C3442re.this.f9059kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.SELECTED_MISSION_ITEM));
                    }
                    int i = 1;
                    int i2 = 0;
                    for (int i3 = 0; i3 < C3442re.this.fxc.getItemCount(); i3++) {
                        C0314EF ef = (C0314EF) ((C3469a) C3442re.this.fxc.getItemAt(i3)).getObject();
                        if (ef == C0314EF.HANGAR) {
                            i2 = i3;
                        } else if (ef == C0314EF.STORAGE) {
                            i = i3;
                        }
                    }
                    if (xTVar.mo8620eP() instanceof ShipType) {
                        C3442re.this.fxc.setSelectedIndex(i2);
                    } else if (((C3469a) C3442re.this.fxc.getSelectedItem()).getObject() == C0314EF.HANGAR) {
                        C3442re.this.fxc.setSelectedIndex(i);
                    }
                }
            }
        }
    }

    /* renamed from: a.re$m */
    /* compiled from: a */
    class C3488m implements TableColumnModelListener {
        C3488m() {
        }

        public void columnAdded(TableColumnModelEvent tableColumnModelEvent) {
        }

        public void columnMarginChanged(ChangeEvent changeEvent) {
            for (int i = 0; i < 6; i++) {
                C3442re.this.fwW.getColumnModel().getColumn(i).setPreferredWidth(C3442re.this.fwX.getColumnModel().getColumn(i).getPreferredWidth());
            }
        }

        public void columnMoved(TableColumnModelEvent tableColumnModelEvent) {
            int fromIndex = tableColumnModelEvent.getFromIndex();
            int toIndex = tableColumnModelEvent.getToIndex();
            if (fromIndex != toIndex) {
                C3442re.this.fwW.moveColumn(fromIndex, toIndex);
            }
        }

        public void columnRemoved(TableColumnModelEvent tableColumnModelEvent) {
        }

        public void columnSelectionChanged(ListSelectionEvent listSelectionEvent) {
        }
    }

    /* renamed from: a.re$F */
    /* compiled from: a */
    class C3448F extends AbstractTableModel {
        private static final long serialVersionUID = -2967953929747230012L;

        C3448F() {
        }

        public int getColumnCount() {
            return 4;
        }

        public int getRowCount() {
            return C3442re.this.aoM.bdf().size();
        }

        public Object getValueAt(int i, int i2) {
            SellOrderInfo xTVar = C3442re.this.aoM.bdf().get(i);
            switch (i2) {
                case 1:
                    return xTVar.mo8622eT().getName();
                case 2:
                    return Long.valueOf(xTVar.mo8621eR());
                case 3:
                    return C3442re.this.f9059kj.translate(C3442re.m38526he(xTVar.getTimeout()).aiP());
                default:
                    return xTVar;
            }
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return C3442re.this.f9059kj.translate("Item");
                case 1:
                    return C3442re.this.f9059kj.translate("Station");
                case 2:
                    return C3442re.this.f9059kj.translate("Price (T$)");
                case 3:
                    return C3442re.this.f9059kj.translate("Life Time");
                default:
                    return "$POST_A_TICKET_PLEASE";
            }
        }
    }

    /* renamed from: a.re$K */
    /* compiled from: a */
    class C3453K implements Comparator<SellOrderInfo> {
        C3453K() {
        }

        /* renamed from: a */
        public int compare(SellOrderInfo xTVar, SellOrderInfo xTVar2) {
            return xTVar.mo8620eP().mo19891ke().get().compareTo(xTVar2.mo8620eP().mo19891ke().get());
        }
    }

    /* renamed from: a.re$J */
    /* compiled from: a */
    class C3452J implements Comparator<Long> {
        C3452J() {
        }

        /* renamed from: a */
        public int compare(Long l, Long l2) {
            return (int) (l.longValue() - l2.longValue());
        }
    }

    /* renamed from: a.re$I */
    /* compiled from: a */
    class C3451I extends AbstractTableModel {
        private static final long serialVersionUID = 3790400381368120204L;

        C3451I() {
        }

        public int getColumnCount() {
            return 4;
        }

        public int getRowCount() {
            return C3442re.this.aoM.bde().size();
        }

        public Object getValueAt(int i, int i2) {
            BuyOrderInfo sfVar = C3442re.this.aoM.bde().get(i);
            switch (i2) {
                case 1:
                    return sfVar.mo8622eT().getName();
                case 2:
                    return Long.valueOf(sfVar.mo8621eR());
                case 3:
                    return C3442re.this.f9059kj.translate(C3442re.m38526he(sfVar.getTimeout()).aiP());
                default:
                    return sfVar;
            }
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return C3442re.this.f9059kj.translate("Item");
                case 1:
                    return C3442re.this.f9059kj.translate("Station");
                case 2:
                    return C3442re.this.f9059kj.translate("Price (T$)");
                case 3:
                    return C3442re.this.f9059kj.translate("Life Time");
                default:
                    return "$POST_A_TICKET_PLEASE";
            }
        }
    }

    /* renamed from: a.re$H */
    /* compiled from: a */
    class C3450H implements Comparator<BuyOrderInfo> {
        C3450H() {
        }

        /* renamed from: a */
        public int compare(BuyOrderInfo sfVar, BuyOrderInfo sfVar2) {
            return sfVar.mo8620eP().mo19891ke().get().compareTo(sfVar2.mo8620eP().mo19891ke().get());
        }
    }

    /* renamed from: a.re$G */
    /* compiled from: a */
    class C3449G implements Comparator<Long> {
        C3449G() {
        }

        /* renamed from: a */
        public int compare(Long l, Long l2) {
            return (int) (l.longValue() - l2.longValue());
        }
    }
}
