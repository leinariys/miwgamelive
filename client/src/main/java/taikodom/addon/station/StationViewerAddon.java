package taikodom.addon.station;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.script.Actor;
import game.script.corporation.Corporation;
import game.script.resource.Asset;
import game.script.ship.Outpost;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.render.GUIModule;
import logic.res.sound.C0907NJ;
import logic.swing.C5378aHa;
import logic.ui.item.Component3d;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.station")
/* compiled from: a */
public class StationViewerAddon extends C6124ags<C3689to> implements C2495fo {
    /* access modifiers changed from: private */
    public boolean huM;
    /* renamed from: kj */
    public IAddonProperties f10274kj;
    /* renamed from: nM */
    public InternalFrame f10275nM;
    /* access modifiers changed from: private */
    private C3428rT<C5783aaP> aoN;
    /* access modifiers changed from: private */
    private C6622aqW atn;
    /* renamed from: wz */
    private C3428rT<C3689to> f10276wz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10274kj = addonPropertie;
        this.f10274kj.bHv().mo16790b((Object) this, "station.css");
        String U = addonPropertie.mo11831U("auto-open", "stationviewer");
        this.huM = U != null ? Boolean.parseBoolean(U) : false;
        this.aoN = new C5011g();
        this.f10276wz = new C5010f();
        this.atn = new C5008d("station", this.f10274kj.translate("Station"), "station");
        this.atn.mo15602jH(this.f10274kj.translate("Here you can check some history about the station you are in and how beautiful it looks from outside."));
        this.atn.mo15603jI("STATION_WINDOW");
        this.f10274kj.mo2322a("station", (Action) this.atn);
        this.f10274kj.aVU().publish(new C5344aFs(this.atn, 707, false));
        this.f10274kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        if (this.huM) {
            open();
        }
        this.f10274kj.mo2317P(this);
    }

    public void stop() {
        this.f10274kj.mo11845e("auto-open", Boolean.valueOf(this.huM).toString(), "stationviewer");
        if (this.f10275nM != null) {
            destroyWindow();
        }
        this.f10274kj.aVU().mo13964a(this.aoN);
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f10274kj.getPlayer().bhE() instanceof Station) {
            if (this.f10275nM == null) {
                this.f10275nM = this.f10274kj.bHv().mo16792bN("station.xml");
                this.f10275nM.addComponentListener(new C5007c());
                this.f10275nM.mo4915cd("auto-open").addActionListener(new C5006b());
            } else if (this.f10275nM.isVisible()) {
                this.f10275nM.close();
                return;
            }
            Actor bhE = this.f10274kj.getPlayer().bhE();
            this.f10275nM.mo4917cf("station-name").setText(bhE.getName());
            if (bhE instanceof Station) {
                this.f10275nM.mo4915cd("station-description").setText(((Station) bhE).mo662vW().get());
                if (!(bhE instanceof Outpost) || ((Outpost) bhE).bYd() == null) {
                    this.f10275nM.mo4916ce("corporation-panel").setVisible(false);
                } else {
                    Corporation bYd = ((Outpost) bhE).bYd();
                    if (bYd != null) {
                        this.f10275nM.mo4917cf("corporation-name").setText(bYd.getName());
                        this.f10275nM.mo4915cd("corporation-logo").mo16824a(C5378aHa.CORP_IMAGES, bYd.mo10706Qu().aOQ());
                        this.f10275nM.mo4915cd("corporation-panel").setVisible(true);
                    }
                }
                Asset Nu = ((Station) bhE).azP().mo3521Nu();
                C5005a aVar = new C5005a();
                this.f10274kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/default_scene.pro", "default_scene", (Scene) null, new C5009e(), "StationView");
                this.f10274kj.getEngineGame().getLoaderTrail().mo4995a(Nu.getFile(), Nu.getHandle(), (Scene) null, aVar, "StationView");
                this.f10275nM.validate();
            }
            this.f10274kj.aVU().mo13965a(C3689to.class, this.f10276wz);
            this.f10275nM.mo4915cd("auto-open").setSelected(this.huM);
            this.f10275nM.validate();
            this.f10274kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, this));
            GUIModule bhd = C5916acs.getSingolton().getGuiModule();
            this.f10275nM.setLocation((bhd.getScreenWidth() / 2) - (this.f10275nM.getWidth() / 2), ((bhd.getScreenHeight() / 2) - (this.f10275nM.getHeight() / 2)) - 60);
            this.f10275nM.setVisible(true);
            this.f10275nM.requestFocus();
        }
    }

    /* renamed from: b */
    public boolean updateInfo(C3689to toVar) {
        this.f10275nM.close();
        return false;
    }

    /* access modifiers changed from: private */
    public void destroyWindow() {
        if (this.f10275nM != null) {
            this.f10275nM.mo4915cd("station-image").destroy();
            this.f10275nM.destroy();
            this.f10275nM = null;
        }
    }

    @C2602hR(mo19255zf = "STATION_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24612al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: taikodom.addon.station.StationViewerAddon$g */
    /* compiled from: a */
    class C5011g extends C3428rT<C5783aaP> {
        C5011g() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (aap.bMO() == C5783aaP.C1841a.DOCKED && StationViewerAddon.this.huM) {
                StationViewerAddon.this.open();
            } else if (StationViewerAddon.this.f10275nM != null && StationViewerAddon.this.f10275nM.isVisible()) {
                StationViewerAddon.this.f10275nM.close();
            }
        }
    }

    /* renamed from: taikodom.addon.station.StationViewerAddon$f */
    /* compiled from: a */
    class C5010f extends C3428rT<C3689to> {
        C5010f() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (StationViewerAddon.this.f10275nM != null && StationViewerAddon.this.f10275nM.isVisible()) {
                StationViewerAddon.this.f10275nM.close();
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.station.StationViewerAddon$d */
    /* compiled from: a */
    class C5008d extends C6622aqW {
        private static final long serialVersionUID = 1285127957770615974L;

        C5008d(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return StationViewerAddon.this.f10275nM != null && StationViewerAddon.this.f10275nM.isVisible();
        }

        public boolean isEnabled() {
            return StationViewerAddon.this.f10274kj.getPlayer().bhE() instanceof Station;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            StationViewerAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.station.StationViewerAddon$c */
    /* compiled from: a */
    class C5007c extends ComponentAdapter {
        C5007c() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            StationViewerAddon.this.f10274kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, this));
            StationViewerAddon.this.destroyWindow();
        }
    }

    /* renamed from: taikodom.addon.station.StationViewerAddon$b */
    /* compiled from: a */
    class C5006b implements ActionListener {
        C5006b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            StationViewerAddon.this.huM = StationViewerAddon.this.f10275nM.mo4915cd("auto-open").isSelected();
        }
    }

    /* renamed from: taikodom.addon.station.StationViewerAddon$a */
    class C5005a implements C0907NJ {
        C5005a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            Component3d cd;
            if (StationViewerAddon.this.f10275nM != null && (cd = StationViewerAddon.this.f10275nM.mo4915cd("station-image")) != null) {
                cd.getSceneView().getSceneViewQuality().setShaderQuality(StationViewerAddon.this.f10274kj.ale().getShaderQuality());
                SceneObject sceneObject = (SceneObject) renderAsset;
                cd.getScene().addChild(sceneObject);
                cd.mo16902c(sceneObject, 30.0f);
                cd.setVisible(true);
            }
        }
    }

    /* renamed from: taikodom.addon.station.StationViewerAddon$e */
    /* compiled from: a */
    class C5009e implements C0907NJ {
        C5009e() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            Component3d cd;
            if (StationViewerAddon.this.f10275nM != null && (cd = StationViewerAddon.this.f10275nM.mo4915cd("station-image")) != null) {
                cd.getSceneView().getSceneViewQuality().setShaderQuality(StationViewerAddon.this.f10274kj.ale().getShaderQuality());
                cd.getScene().removeAllChildren();
                cd.getScene().addChild((SceneObject) renderAsset);
            }
        }
    }
}
