package taikodom.addon.tooltip;

import game.script.progression.ProgressionAbility;
import game.script.progression.ProgressionCellType;
import logic.swing.C3940wz;
import logic.ui.Panel;
import logic.ui.item.ListJ;
import logic.ui.item.aUR;
import p001a.C2302dn;
import p001a.C5517aMj;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;

@TaikodomAddon("taikodom.addon.tooltip")
/* renamed from: a.eO */
/* compiled from: a */
public class C2353eO extends C3940wz {

    /* renamed from: DU */
    public static final Object f6741DU = "DATA";
    /* renamed from: kj */
    private final IAddonProperties f6743kj;
    /* renamed from: DV */
    private Panel f6742DV;

    public C2353eO(IAddonProperties vWVar) {
        this.f6743kj = vWVar;
    }

    /* renamed from: b */
    public Component mo2675b(Component component) {
        if (this.f6742DV == null) {
            this.f6742DV = (Panel) this.f6743kj.bHv().mo16789b(TooltipAddon.class, "progressionCellTooltip.xml");
        }
        ProgressionCellType kgVar = (ProgressionCellType) ((JComponent) component).getClientProperty(f6741DU);
        m29524a(kgVar);
        m29525b(kgVar);
        return this.f6742DV;
    }

    /* renamed from: a */
    private void m29524a(ProgressionCellType kgVar) {
        if (kgVar.mo20103Gc().size() > 0) {
            ListJ cd = this.f6742DV.mo4915cd("abilitiesList");
            cd.setModel(new C2302dn(kgVar.mo20103Gc()));
            cd.setCellRenderer(new C2354a());
            this.f6742DV.mo4915cd("panelAbilities").setVisible(true);
            return;
        }
        this.f6742DV.mo4915cd("panelAbilities").setVisible(false);
    }

    /* renamed from: b */
    private void m29525b(ProgressionCellType kgVar) {
        boolean z;
        boolean z2 = true;
        JLabel cf = this.f6742DV.mo4917cf("pmRequirement");
        if (kgVar.mo20108Jz() > 0) {
            cf.setText(String.valueOf(this.f6743kj.translate("Merit Points:")) + kgVar.mo20108Jz());
            cf.setVisible(true);
            z = true;
        } else {
            cf.setVisible(false);
            z = false;
        }
        JLabel cf2 = this.f6742DV.mo4917cf("crRequirement");
        if (kgVar.mo20107Jx() > 0) {
            cf2.setText(String.valueOf(this.f6743kj.translate("Rank:")) + kgVar.mo20107Jx());
            cf2.setVisible(true);
            z = true;
        } else {
            cf2.setVisible(false);
        }
        JLabel cf3 = this.f6742DV.mo4917cf("abilityRequirement");
        if (kgVar.mo20104JB() != null) {
            cf3.setText(String.valueOf(this.f6743kj.translate("Ability:")) + kgVar.mo20104JB().mo3732rP().get());
            cf3.setVisible(true);
        } else {
            cf3.setVisible(false);
            z2 = z;
        }
        this.f6742DV.mo4915cd("panelRequirements").setVisible(z2);
    }

    /* renamed from: a.eO$a */
    class C2354a extends C5517aMj {
        C2354a() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            Panel dAx = aur.dAx();
            dAx.mo4915cd("abilityName").setText(((ProgressionAbility) obj).mo3734vW().get());
            return dAx;
        }
    }
}
