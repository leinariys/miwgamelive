package taikodom.addon.tooltip;

import game.network.message.serializable.C3438ra;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.item.ShipItem;
import game.script.ship.*;
import logic.baa.aDJ;
import logic.ui.Panel;
import logic.ui.item.Repeater;
import p001a.C2974mT;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;

@TaikodomAddon("taikodom.addon.tooltip")
        /* renamed from: a.abs  reason: case insensitive filesystem */
        /* compiled from: a */
class C5864abs extends C2974mT {
    private Repeater<ShipSectorType> eYc;
    private Repeater.C3671a<ShipSectorType> eYd = new C1856a();
    private JLabel eYe;
    private JLabel eYf;
    private JLabel eYg;
    private JLabel eYh;
    private JLabel eYi;
    private JLabel eYj;
    private JLabel eYk;
    private JLabel eYl;

    /* renamed from: kj */
    private IAddonProperties f4164kj;

    public C5864abs(Class<? extends aDJ> cls, Panel aco) {
        super(cls, aco);
        m20097c(aco);
    }

    /* renamed from: c */
    private void m20097c(Panel aco) {
        this.eYe = aco.mo4917cf("maxVelocity");
        this.eYf = aco.mo4917cf("acceleration");
        this.eYg = aco.mo4917cf("maneuver");
        this.eYh = aco.mo4917cf("container");
        this.eYi = aco.mo4917cf("hull");
        this.eYj = aco.mo4917cf("shield");
        this.eYk = aco.mo4917cf("turrets");
        this.eYl = aco.mo4917cf("turretsTitle");
    }

    /* renamed from: a */
    public void mo1169a(IAddonProperties vWVar, ItemType jCVar, Item auq) {
        ShipType ng;
        float ra;
        float rb;
        float VH;
        float wE;
        float hh;
        float hh2;
        Ship fAVar = null;
        this.f4164kj = vWVar;
        if (ShipType.class.isAssignableFrom(jCVar.getClass())) {
            ng = (ShipType) jCVar;
        } else {
            ng = null;
        }
        if (auq instanceof ShipItem) {
            fAVar = ((ShipItem) auq).mo11127al();
            if (ng == null) {
                ng = fAVar.agH();
            }
        }
        if (ng != null) {
            if (fAVar != null) {
                ra = fAVar.mo1091ra();
                rb = fAVar.mo1092rb();
                VH = fAVar.mo963VH();
                wE = fAVar.afI().mo2696wE();
                hh = fAVar.mo8287zt().mo19934hh();
                hh2 = fAVar.mo8288zv().mo19089hh();
            } else {
                ra = ng.mo4217ra();
                rb = ng.mo4218rb();
                VH = ng.mo4153VH();
                wE = ng.mo4156Wb().mo11172wE();
                hh = ng.mo4155VT().mo22727hh();
                hh2 = ((ShipStructureType) ng.agt().get(0)).mo9731VX().mo15373hh();
            }
            this.aGi.setVisible(true);
            C6120ago.m22120a(this.eYe, ng.mo4217ra(), ra);
            C6120ago.m22120a(this.eYf, ng.mo4218rb(), rb);
            C6120ago.m22120a(this.eYg, ng.mo4153VH(), VH);
            C6120ago.m22120a(this.eYh, ng.mo4156Wb().mo11172wE(), wE);
            C6120ago.m22120a(this.eYi, ng.mo4155VT().mo22727hh(), hh);
            C6120ago.m22120a(this.eYj, ((ShipStructureType) ng.agt().get(0)).mo9731VX().mo15373hh(), hh2);
            int size = ng.mo4157Wl().size();
            boolean z = size > 0;
            this.eYl.setVisible(z);
            this.eYk.setVisible(z);
            this.eYk.setText(String.valueOf(size));
            C3438ra<ShipSectorType> bVG = ((ShipStructureType) ng.agt().get(0)).bVG();
            if (this.eYc == null) {
                this.eYc = this.aGi.mo4915cd("shipSectorsRepeater");
                this.eYc.mo22250a(this.eYd);
            }
            this.eYc.clear();
            if (!bVG.isEmpty()) {
                for (ShipSectorType G : bVG) {
                    this.eYc.mo22248G(G);
                }
            }
        }
    }

    /* renamed from: a.abs$a */
    class C1856a implements Repeater.C3671a<ShipSectorType> {
        C1856a() {
        }

        /* renamed from: a */
        public void mo843a(ShipSectorType aux, Component component) {
            Panel aco = (Panel) component;
            SectorCategory bmW = aux.bmW();
            String str = "?";
            if (bmW != null) {
                str = bmW.mo18438ke().get();
            } else {
                System.err.println("Sector '" + aux.getHandle() + "' has no sector category");
            }
            aco.mo4917cf("sectorName").setText(str);
            aco.mo4917cf("sectorInfo").setText(String.valueOf(aux.dAF()));
        }
    }
}
