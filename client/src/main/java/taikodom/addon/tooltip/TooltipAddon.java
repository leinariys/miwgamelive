package taikodom.addon.tooltip;

import logic.IAddonSettings;
import logic.swing.C3940wz;
import p001a.C0564Ho;
import p001a.C0725KR;
import p001a.aEV;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.tooltip")
/* compiled from: a */
public class TooltipAddon implements C2495fo {

    /* renamed from: kj */
    private IAddonProperties f10287kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10287kj = addonPropertie;
        C3940wz.m40775a("itemTooltipProvider", (C3940wz) new C6120ago(this.f10287kj));
        C3940wz.m40775a("citizenImprovTooltipProvider", (C3940wz) new C0564Ho(this.f10287kj));
        C3940wz.m40775a("npcTooltipProvider", (C3940wz) new aEV(this.f10287kj));
        C3940wz.m40775a("progressionCellTooltipProvider", (C3940wz) new C2353eO(this.f10287kj));
        C3940wz.m40775a("progressionLineTooltipProvider", (C3940wz) new C6838aue(this.f10287kj));
        C3940wz.m40775a("windowTooltipProvider", (C3940wz) new C0725KR(this.f10287kj));
    }

    public void stop() {
    }
}
