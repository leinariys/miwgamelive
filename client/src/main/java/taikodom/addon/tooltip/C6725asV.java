package taikodom.addon.tooltip;

import game.script.damage.DamageType;
import game.script.item.*;
import game.script.mines.MineType;
import game.script.missile.MissileType;
import logic.baa.aDJ;
import logic.ui.Panel;
import logic.ui.item.Repeater;
import p001a.C2974mT;
import p001a.C3987xk;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.Component;
import java.util.Map;

@TaikodomAddon("taikodom.addon.tooltip")
        /* renamed from: a.asV  reason: case insensitive filesystem */
        /* compiled from: a */
class C6725asV extends C2974mT {
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f5282IU = C3987xk.bFK;
    /* access modifiers changed from: private */
    public ClipType gxz;
    private Repeater<Map.Entry<DamageType, Float>> eUW;
    private Repeater.C3671a<Map.Entry<DamageType, Float>> eUX = new C1986a();
    private JLabel eVg;
    private JPanel gxy;

    public C6725asV(Class<? extends aDJ> cls, Panel aco) {
        super(cls, aco);
        m25705c(aco);
    }

    /* renamed from: c */
    private void m25705c(Panel aco) {
        this.gxy = aco.mo4915cd("clipBoxPanel");
        this.eVg = aco.mo4917cf("ammoCount");
    }

    /* renamed from: a */
    public void mo1169a(IAddonProperties vWVar, ItemType jCVar, Item auq) {
        this.gxz = null;
        if (ClipBoxType.class.isAssignableFrom(jCVar.getClass())) {
            ClipBoxType aty = (ClipBoxType) jCVar;
            this.gxz = aty.dvZ();
            this.gxy.setVisible(true);
            this.eVg.setText(String.valueOf(this.f5282IU.format((long) aty.dvX())));
        } else {
            this.gxy.setVisible(false);
            if (ClipType.class.isAssignableFrom(jCVar.getClass())) {
                this.gxz = (ClipType) jCVar;
            } else if (auq instanceof Clip) {
                this.gxz = ((Clip) auq).bAJ();
            }
        }
        if (this.gxz != null) {
            this.aGi.setVisible(true);
            if (this.eUW == null) {
                this.eUW = this.aGi.mo4915cd("damageTypesRepeater");
                this.eUW.mo22250a(this.eUX);
            }
            this.eUW.clear();
            for (Map.Entry entry : this.gxz.bpA().entrySet()) {
                if (((DamageType) entry.getKey()).getHandle().equals("dmg_non_hull") || ((DamageType) entry.getKey()).getHandle().equals("dmg_non_shield") || Math.abs(((Float) entry.getValue()).floatValue()) > 1.0E-4f) {
                    this.eUW.mo22248G(entry);
                }
            }
        }
    }

    /* renamed from: a.asV$a */
    class C1986a implements Repeater.C3671a<Map.Entry<DamageType, Float>> {
        C1986a() {
        }

        /* renamed from: a */
        public void mo843a(Map.Entry<DamageType, Float> entry, Component component) {
            String str;
            Panel aco = (Panel) component;
            aco.mo4917cf("damageTypeName").setText(entry.getKey().aQm().get());
            if (entry.getValue().floatValue() < 0.0f || (C6725asV.this.gxz instanceof MineType) || (C6725asV.this.gxz instanceof MissileType)) {
                str = "";
            } else {
                str = "+";
            }
            aco.mo4917cf("damageTypeAmout").setText(String.valueOf(str) + C6725asV.this.f5282IU.mo22980dY(entry.getValue().floatValue()));
        }
    }
}
