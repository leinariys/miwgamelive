package taikodom.addon.tooltip;

import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.item.buff.amplifier.*;
import logic.baa.aDJ;
import logic.ui.Panel;
import logic.ui.item.Repeater;
import p001a.C2974mT;
import p001a.C3892wO;
import p001a.C3987xk;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.tooltip")
        /* renamed from: a.Ct */
        /* compiled from: a */
class C0229Ct extends C2974mT {
    private static final float fTx = 1.0E-4f;
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f361IU = C3987xk.bFK;
    /* access modifiers changed from: private */
    public int fTB;
    private List<AmplifierType> fTA;
    private Repeater<C0231b> fTy;
    private Repeater.C3671a<C0231b> fTz = new C0230a();

    public C0229Ct(Class<? extends aDJ> cls, Panel aco) {
        super(cls, aco);
    }

    /* renamed from: a */
    public void mo1169a(IAddonProperties vWVar, ItemType jCVar, Item auq) {
        AmplifierType iDVar;
        if (AmplifierType.class.isAssignableFrom(jCVar.getClass())) {
            iDVar = (AmplifierType) jCVar;
        } else if (auq instanceof Amplifier) {
            iDVar = (AmplifierType) ((Amplifier) auq).cJS();
        } else {
            iDVar = null;
        }
        if (iDVar != null) {
            if (this.fTy == null) {
                this.fTy = this.aGi.mo4915cd("amplifierBonusesRepeater");
                this.fTy.mo22250a(this.fTz);
                this.fTA = new ArrayList();
            }
            this.fTy.clear();
            this.fTA.clear();
            if (iDVar instanceof ComposedAmplifierType) {
                for (AmplifierType add : ((ComposedAmplifierType) jCVar).dfJ()) {
                    this.fTA.add(add);
                }
            } else {
                this.fTA.add(iDVar);
            }
            this.fTB = 0;
            for (AmplifierType next : this.fTA) {
                if (next instanceof CargoHoldAmplifierType) {
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Container:"), ((CargoHoldAmplifierType) next).mo19057vk()));
                } else if (next instanceof HullAmplifierType) {
                    HullAmplifierType bu = (HullAmplifierType) next;
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Hull:"), bu.aBc()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Armor:"), bu.aBe()));
                } else if (next instanceof ShieldAmplifierType) {
                    ShieldAmplifierType eo = (ShieldAmplifierType) next;
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Shield:"), eo.aBc()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Shield damage reduction:"), eo.aBe()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Shield recovery time:"), eo.aOX()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Regen:"), eo.aOV()));
                } else if (next instanceof ShipAmplifierType) {
                    ShipAmplifierType aat = (ShipAmplifierType) next;
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Maneuver:"), aat.mo7780yP()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Acceleration:"), aat.mo7779yN()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Velocity:"), aat.mo7778yL()));
                } else if (next instanceof WeaponAmplifierType) {
                    WeaponAmplifierType db = (WeaponAmplifierType) next;
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Shield damage:"), db.aGE()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Hull damage:"), db.aGG()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Range:"), db.aGC()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Fire rate:"), db.aGy()));
                } else if (next instanceof CruiseSpeedAmplifierType) {
                    CruiseSpeedAmplifierType acw = (CruiseSpeedAmplifierType) next;
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Maneuver:"), acw.mo12630yP()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Acceleration:"), acw.mo12629yN()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Cruise speed:"), acw.mo12628yL()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Cruise speed cool-down:"), acw.bLz()));
                    this.fTy.mo22248G(new C0231b(vWVar.translate("Warmup:"), acw.bLD()));
                }
            }
            if (this.fTB > 0) {
                this.aGi.setVisible(true);
            }
        }
    }

    /* renamed from: a.Ct$b */
    /* compiled from: a */
    private class C0231b {
        private final String eaJ;
        private final C3892wO eaK;

        public C0231b(String str, C3892wO wOVar) {
            this.eaJ = str;
            this.eaK = wOVar;
        }

        public String brC() {
            return this.eaJ;
        }

        public C3892wO brD() {
            return this.eaK;
        }
    }

    /* renamed from: a.Ct$a */
    class C0230a implements Repeater.C3671a<C0231b> {
        C0230a() {
        }

        /* renamed from: a */
        public void mo843a(C0231b bVar, Component component) {
            float value = bVar.brD().getValue();
            if (Math.abs(value) <= 1.0E-4f) {
                component.setVisible(false);
                return;
            }
            C0229Ct ct = C0229Ct.this;
            ct.fTB = ct.fTB + 1;
            ((Panel) component).mo4917cf("bonusName").setText(bVar.brC());
            ((Panel) component).mo4917cf("bonusAmout").setText(String.valueOf(value > 0.0f ? "+" : "") + C0229Ct.this.f361IU.mo22980dY(value) + (bVar.brD().coZ().equals(C3892wO.C3893a.PERCENT) ? "%" : ""));
        }
    }
}
