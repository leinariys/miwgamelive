package taikodom.addon.tooltip;

import game.script.damage.DamageType;
import game.script.item.*;
import game.script.mines.MineThrowerWeaponType;
import game.script.mines.MineType;
import game.script.missile.MissileType;
import game.script.missile.MissileWeaponType;
import logic.baa.aDJ;
import logic.ui.Panel;
import logic.ui.item.Repeater;
import p001a.C2974mT;
import p001a.C3987xk;
import p001a.C5956adg;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.Component;
import java.util.Map;
import java.util.Set;

@TaikodomAddon("taikodom.addon.tooltip")
        /* renamed from: a.aaw  reason: case insensitive filesystem */
        /* compiled from: a */
class C5816aaw extends C2974mT {
    private static final C3987xk eVl = C3987xk.bFK;
    /* access modifiers changed from: private */
    public String eUY;
    /* access modifiers changed from: private */
    public WeaponType eUZ;
    private Repeater<Map.Entry<DamageType, Float>> eUW;
    private Repeater.C3671a<Map.Entry<DamageType, Float>> eUX = new C1844a();
    private Panel eVa;
    private JLabel eVb;
    private JLabel eVc;
    private JLabel eVd;
    private JLabel eVe;
    private JLabel eVf;
    private JLabel eVg;
    private JLabel eVh;
    private JLabel eVi;
    private JLabel eVj;
    private JLabel eVk;

    public C5816aaw(Class<? extends aDJ> cls, Panel aco) {
        super(cls, aco);
        m19841c(aco);
    }

    /* renamed from: c */
    private void m19841c(Panel aco) {
        this.eVb = aco.mo4917cf("fireRate");
        this.eVi = aco.mo4917cf("projectileSpeedTitle");
        this.eVh = aco.mo4917cf("projectileSpeed");
        this.eVc = aco.mo4917cf("range");
        this.eVd = aco.mo4917cf("ammoTypeTitle");
        this.eVe = aco.mo4917cf("ammoType");
        this.eVf = aco.mo4917cf("ammoCountTitle");
        this.eVg = aco.mo4917cf("ammoCount");
        this.eVk = aco.mo4917cf("ammoCapacityTitle");
        this.eVj = aco.mo4917cf("ammoCapacity");
    }

    /* renamed from: a */
    public void mo1169a(IAddonProperties vWVar, ItemType jCVar, Item auq) {
        float ml;
        float in;
        Set<Map.Entry<DamageType, Float>> entrySet;
        ProjectileWeapon sg = null;
        this.eUZ = null;
        if (WeaponType.class.isAssignableFrom(jCVar.getClass())) {
            this.eUZ = (WeaponType) jCVar;
        }
        if (auq instanceof Weapon) {
            Weapon adv = (Weapon) auq;
            if (this.eUZ == null) {
                this.eUZ = adv.aqz();
            }
            sg = adv;
        }
        if (this.eUZ != null) {
            if (sg != null) {
                ml = sg.mo12944ml();
                in = sg.mo12940in();
            } else {
                ml = this.eUZ.mo10793ml();
                in = this.eUZ.mo10790in();
            }
            this.eUY = "";
            this.aGi.setVisible(true);
            C6120ago.m22120a(this.eVb, this.eUZ.mo10793ml(), ml);
            C6120ago.m22120a(this.eVc, this.eUZ.mo10790in(), in);
            this.eVd.setVisible(false);
            this.eVe.setVisible(false);
            this.eVf.setVisible(false);
            this.eVg.setVisible(false);
            this.eVi.setText(vWVar.translate("Projectile speed:"));
            if (sg != null && ProjectileWeapon.class.isAssignableFrom(sg.getClass())) {
                ProjectileWeapon sg2 = sg;
                Clip bup = sg2.bup();
                if (bup != null) {
                    this.eVd.setVisible(true);
                    this.eVe.setVisible(true);
                    this.eVe.setText(bup.bAJ().mo19891ke().get());
                    if (bup.bAJ() instanceof MineType) {
                        this.eVi.setText(vWVar.translate("Duration:"));
                        this.eVh.setText(String.valueOf(((MineType) bup.bAJ()).getLifeTime()));
                    } else if (bup.bAJ() instanceof MissileType) {
                        this.eVh.setText(eVl.mo22980dY(((MissileType) bup.bAJ()).mo11869ra()));
                    } else {
                        C6120ago.m22120a(this.eVh, sg2.buj().getSpeed(), sg2.getSpeed());
                    }
                }
                this.eVf.setVisible(true);
                this.eVg.setVisible(true);
                this.eVg.setText(String.valueOf(sg2.aqh()));
            } else if (sg == null || !EnergyWeapon.class.isAssignableFrom(sg.getClass())) {
                this.eVh.setText(eVl.mo22980dY(this.eUZ.getSpeed()));
            } else {
                EnergyWeapon yGVar = (EnergyWeapon) sg;
                C6120ago.m22120a(this.eVh, yGVar.aqp().getSpeed(), yGVar.getSpeed());
            }
            if ((this.eUZ instanceof MineThrowerWeaponType) || (this.eUZ instanceof MissileWeaponType)) {
                this.eVk.setVisible(true);
                this.eVj.setVisible(true);
                this.eVj.setText(String.valueOf(((ProjectileWeaponType) this.eUZ).cZX()));
            } else {
                this.eVk.setVisible(false);
                this.eVj.setVisible(false);
            }
            if (this.eUZ instanceof ScatterWeaponType) {
                this.eUY = String.valueOf(String.valueOf(((ScatterWeaponType) this.eUZ).diJ())) + "x ";
            } else if (this.eUZ.aGI()) {
                this.eUY = "2x ";
            }
            if (this.eUW == null) {
                this.eUW = this.aGi.mo4915cd("damageTypesRepeater");
                this.eUW.mo22250a(this.eUX);
                this.eVa = this.aGi.mo4915cd("weaponDamageAttributes");
            }
            this.eUW.clear();
            if (sg != null) {
                entrySet = sg.mo5372il().cPK().entrySet();
            } else {
                entrySet = this.eUZ.bpA().entrySet();
            }
            boolean z = false;
            for (Map.Entry next : entrySet) {
                if (((this.eUZ instanceof ProjectileWeaponType) || Math.abs(((Float) next.getValue()).floatValue()) > 1.0E-4f) && (((DamageType) next.getKey()).getHandle().equals("dmg_non_hull") || ((DamageType) next.getKey()).getHandle().equals("dmg_non_shield") || Math.abs(((Float) next.getValue()).floatValue()) > 1.0E-4f)) {
                    this.eUW.mo22248G(next);
                    z = true;
                }
            }
            this.eVa.setVisible(z);
        }
    }

    /* renamed from: a.aaw$a */
    class C1844a implements Repeater.C3671a<Map.Entry<DamageType, Float>> {
        C1844a() {
        }

        /* renamed from: a */
        public void mo843a(Map.Entry<DamageType, Float> entry, Component component) {
            DamageType key = entry.getKey();
            Panel aco = (Panel) component;
            JLabel cf = aco.mo4917cf("damageTypeName");
            cf.setText(C5956adg.format(cf.getText(), key.aQm().get()));
            JLabel cf2 = aco.mo4917cf("damageTypeAmout");
            C6120ago.m22120a(cf2, ((Float) C5816aaw.this.eUZ.bpA().get(key)).floatValue(), entry.getValue().floatValue());
            if (!"".equals(C5816aaw.this.eUY)) {
                cf2.setText(String.valueOf(C5816aaw.this.eUY) + cf2.getText());
            }
        }
    }
}
