package taikodom.addon.tooltip;

import game.script.item.*;
import game.script.itembilling.ItemBillingOrder;
import game.script.newmarket.CommercialOrderInfo;
import game.script.ship.ShipType;
import logic.swing.C3940wz;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.C5923acz;
import logic.ui.item.ListJ;
import logic.ui.item.Progress;
import logic.ui.item.Table;
import p001a.*;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.Component;
import java.awt.*;
import java.util.ArrayList;

@TaikodomAddon("taikodom.addon.tooltip")
/* renamed from: a.ago  reason: case insensitive filesystem */
/* compiled from: a */
public class C6120ago extends C3940wz {

    /* renamed from: DU */
    public static final Object f4532DU = "DATA";
    public static final float fxQ = 1.0E-4f;
    public static final float fxR = 1.0E-6f;
    private static final C3987xk eVl = C3987xk.bFK;
    final java.util.List fxS = new ArrayList();
    /* renamed from: DV */
    private final Panel f4533DV;
    /* renamed from: kj */
    private final IAddonProperties f4535kj;
    private Item aoR;
    /* renamed from: cw */
    private ItemType f4534cw;
    private JLabel fxT;
    private JLabel fxU;

    public C6120ago(IAddonProperties vWVar) {
        this.f4535kj = vWVar;
        this.f4533DV = (Panel) vWVar.bHv().mo16789b(TooltipAddon.class, "itemTooltip.xml");
        m22121iM();
        bWE();
    }

    /* renamed from: a */
    public static void m22120a(JLabel jLabel, float f, float f2) {
        float f3 = f2 - f;
        IComponentManager e = ComponentManager.getCssHolder(jLabel);
        if (f3 > 1.0E-6f) {
            e.setAttribute("class", "amplified");
        } else if (f3 < -1.0E-6f) {
            e.setAttribute("class", "reduced");
        } else {
            e.setAttribute("class", "neutral");
        }
        jLabel.setText(String.valueOf(eVl.mo22980dY(f2)));
    }

    /* renamed from: iM */
    private void m22121iM() {
        this.fxT = this.f4533DV.mo4917cf("slotSizeTitle");
        this.fxU = this.f4533DV.mo4917cf("slotSize");
    }

    /* renamed from: b */
    public Component mo2675b(Component component) {
        m22122m(component);
        if (this.f4534cw == null) {
            return null;
        }
        if (this.f4534cw.mo19866HC().mo12100sK() != null) {
            this.f4533DV.mo4915cd("typeIcon").mo16824a(C5378aHa.ICONOGRAPHY, this.f4534cw.mo19866HC().mo12100sK().getHandle());
        }
        this.f4533DV.mo4917cf("itemName").setText(this.f4534cw.mo19891ke().get());
        this.f4533DV.mo4917cf("itemType").setText(this.f4534cw.mo19866HC().mo12246ke().get());
        this.f4533DV.mo4915cd("itemDescription").setText(this.f4534cw.mo19893vW().get());
        if (this.f4534cw instanceof ComponentType) {
            this.fxT.setVisible(true);
            this.fxU.setVisible(true);
            this.fxU.setText(String.valueOf(((ComponentType) this.f4534cw).cuH().ordinal() + 1));
        } else {
            this.fxT.setVisible(false);
            this.fxU.setVisible(false);
        }
        this.f4533DV.mo4917cf("unitVolume").setText(String.valueOf(eVl.mo22980dY(this.f4534cw.mo19865HA())));
        JLabel cf = this.f4533DV.mo4917cf("totalVolumeTitle");
        JLabel cf2 = this.f4533DV.mo4917cf("totalVolume");
        if (this.aoR instanceof BulkItem) {
            cf.setVisible(true);
            cf2.setVisible(true);
            cf2.setText(String.valueOf(eVl.mo22980dY(((BulkItem) this.aoR).getVolume())));
        } else if (this.aoR instanceof BagItem) {
            cf.setVisible(true);
            cf2.setVisible(true);
            cf2.setText(String.valueOf(eVl.mo22980dY(((BagItem) this.aoR).aoG().aRB())));
        } else {
            cf.setVisible(false);
            cf2.setVisible(false);
        }
        Panel cd = this.f4533DV.mo4915cd("durability");
        if (this.aoR == null || !this.aoR.cxn()) {
            cd.setVisible(false);
        } else {
            Progress cg = this.f4533DV.mo4918cg("durabilityProgress");
            cg.setMinimum(0);
            cg.mo17398E((float) this.aoR.cxu());
            float cxu = (float) this.aoR.cxu();
            if (this.aoR.isBound()) {
                cxu = (float) (this.aoR.cxu() - this.aoR.cxs());
            }
            this.f4533DV.mo4917cf("boundLabel").setVisible(this.aoR.isBound());
            cg.setValue(Float.valueOf(eVl.mo22980dY(cxu)).floatValue());
            cg.setVisible(true);
            cd.setVisible(true);
        }
        boolean z = false;
        for (C2974mT next : this.fxS) {
            if (z || !next.mo20558h(this.f4534cw.getClass())) {
                next.mo20556Pz();
            } else {
                next.mo1169a(this.f4535kj, this.f4534cw, this.aoR);
                z = true;
            }
        }
        this.f4533DV.pack();
        return this.f4533DV;
    }

    private void bWE() {
        this.fxS.add(new C5864abs(ShipType.class, this.f4533DV.mo4915cd("shipAttributes")));
        this.fxS.add(new C5816aaw(WeaponType.class, this.f4533DV.mo4915cd("weaponAttributes")));
        C6725asV asv = new C6725asV(ClipType.class, this.f4533DV.mo4915cd("ammoAttributes"));
        asv.mo20557g(ClipBoxType.class);
        this.fxS.add(asv);
        this.fxS.add(new C0229Ct(AmplifierType.class, this.f4533DV.mo4915cd("amplifierAttributes")));
        this.fxS.add(new C6056afc(BlueprintType.class, this.f4533DV.mo4915cd("POAttributes")));
    }

    /* renamed from: m */
    private void m22122m(Component component) {
        Object obj;
        TreePath pathForLocation;
        DefaultMutableTreeNode defaultMutableTreeNode;
        int rowAtPoint;
        int locationToIndex;
        if (component instanceof ListJ) {
            ListJ aan = (ListJ) component;
            Point point = aan.cHt().getPoint();
            if (!(point == null || (locationToIndex = aan.locationToIndex(point)) == -1 || !aan.getCellBounds(locationToIndex, locationToIndex + 1).contains(point))) {
                obj = aan.getModel().getElementAt(locationToIndex);
            }
            obj = null;
        } else if (component instanceof Table) {
            Table pzVar = (Table) component;
            Point mousePosition = pzVar.getMousePosition(true);
            if (!(mousePosition == null || (rowAtPoint = pzVar.rowAtPoint(mousePosition)) == -1)) {
                obj = pzVar.getValueAt(rowAtPoint, 0);
            }
            obj = null;
        } else if (component instanceof C5923acz) {
            C5923acz acz = (C5923acz) component;
            Point mousePosition2 = acz.getMousePosition(true);
            if (!(mousePosition2 == null || (pathForLocation = acz.getPathForLocation(mousePosition2.x, mousePosition2.y)) == null || (defaultMutableTreeNode = (DefaultMutableTreeNode) pathForLocation.getLastPathComponent()) == null)) {
                obj = defaultMutableTreeNode.getUserObject();
            }
            obj = null;
        } else {
            if (component instanceof Panel) {
                obj = ((Panel) component).getClientProperty(f4532DU);
            }
            obj = null;
        }
        this.aoR = null;
        this.f4534cw = null;
        if (obj instanceof Item) {
            this.aoR = (Item) obj;
            this.f4534cw = this.aoR.bAP();
        } else if (obj instanceof ItemType) {
            this.f4534cw = (ItemType) obj;
        } else if (obj instanceof ItemTypeTableItem) {
            this.f4534cw = ((ItemTypeTableItem) obj).mo23385az();
        } else if (obj instanceof C2539gY) {
            this.f4534cw = (ItemType) ((C2539gY) obj).getData();
        } else if (obj instanceof C1447VJ) {
            this.aoR = ((C1447VJ) obj).mo5990XH();
            this.f4534cw = this.aoR.bAP();
        } else if (obj instanceof CommercialOrderInfo) {
            this.f4534cw = ((CommercialOrderInfo) obj).mo8620eP();
        } else if (obj instanceof ItemBillingOrder) {
            this.f4534cw = ((ItemBillingOrder) obj).mo22367az();
        } else if (obj instanceof C2459fd.C2479f) {
            C2459fd.C2479f fVar = (C2459fd.C2479f) obj;
            if (fVar.bHV() != null) {
                this.aoR = fVar.bHV();
                this.f4534cw = fVar.bHV().cJS();
            }
        }
    }
}
