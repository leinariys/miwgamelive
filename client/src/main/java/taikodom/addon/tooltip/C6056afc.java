package taikodom.addon.tooltip;

import game.script.crafting.CraftingRecipe;
import game.script.item.*;
import logic.baa.aDJ;
import logic.ui.Panel;
import logic.ui.item.Repeater;
import p001a.C2539gY;
import p001a.C2974mT;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.Component;

@TaikodomAddon("taikodom.addon.tooltip")
        /* renamed from: a.afc  reason: case insensitive filesystem */
        /* compiled from: a */
class C6056afc extends C2974mT {
    /* access modifiers changed from: private */
    public Repeater.C3671a<CraftingRecipe> fsI = new C1877a();
    private Repeater<IngredientsCategory> fsG;
    private Repeater.C3671a<IngredientsCategory> fsH = new C1878b();
    private JLabel fsJ;
    private JLabel fsK;
    private JLabel fsL;

    public C6056afc(Class<? extends aDJ> cls, Panel aco) {
        super(cls, aco);
        m21569iM();
    }

    /* renamed from: a */
    public void mo1169a(IAddonProperties vWVar, ItemType jCVar, Item auq) {
        BlueprintType mr;
        if (BlueprintType.class.isAssignableFrom(jCVar.getClass())) {
            mr = (BlueprintType) jCVar;
        } else if (auq instanceof Blueprint) {
            mr = (BlueprintType) ((Blueprint) auq).bAP();
        } else {
            mr = null;
        }
        if (mr != null) {
            this.aGi.setVisible(true);
            if (this.fsG == null) {
                this.fsG = this.aGi.mo4915cd("ingredientsCategoriesRepeater");
                this.fsG.mo22250a(this.fsH);
            }
            this.fsG.clear();
            for (IngredientsCategory G : mr.bhW()) {
                this.fsG.mo22248G(G);
            }
            CraftingRecipe bia = mr.bia();
            if (bia == null || bia.mo554az() == null) {
                this.aGi.setVisible(false);
                return;
            }
            ItemType az = bia.mo554az();
            C2539gY gYVar = new C2539gY(az);
            gYVar.mo19026bo(bia.getAmount());
            this.fsJ.setIcon(gYVar);
            this.fsK.setText(az.mo19891ke().get());
            this.fsL.setText(az.mo19866HC().mo12246ke().get());
        }
    }

    /* renamed from: iM */
    private void m21569iM() {
        this.fsJ = this.aGi.mo4917cf("resultIcon");
        this.fsK = this.aGi.mo4917cf("resultName");
        this.fsL = this.aGi.mo4917cf("resultType");
    }

    /* renamed from: a.afc$a */
    class C1877a implements Repeater.C3671a<CraftingRecipe> {
        C1877a() {
        }

        /* renamed from: a */
        public void mo843a(CraftingRecipe b, Component component) {
            Panel aco = (Panel) component;
            ItemType az = b.mo554az();
            C2539gY gYVar = new C2539gY(az);
            gYVar.mo19026bo(b.getAmount());
            aco.mo4917cf("ingredientIcon").setIcon(gYVar);
            aco.mo4917cf("ingredientIcon").setText(az.mo19891ke().get());
        }
    }

    /* renamed from: a.afc$b */
    /* compiled from: a */
    class C1878b implements Repeater.C3671a<IngredientsCategory> {
        C1878b() {
        }

        /* renamed from: a */
        public void mo843a(IngredientsCategory xVVar, Component component) {
            Panel aco = (Panel) component;
            aco.mo4917cf("categoryTitle").setText(xVVar.mo22930ke().get());
            Repeater<?> ch = aco.mo4919ch("ingredients");
            ch.mo22250a((Repeater.C3671a<?>) C6056afc.this.fsI);
            for (CraftingRecipe G : xVVar.apd()) {
                ch.mo22248G(G);
            }
        }
    }
}
