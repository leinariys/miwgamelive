package taikodom.addon.tooltip;

import game.script.progression.ProgressionLine;
import logic.swing.C3940wz;
import logic.ui.Panel;
import logic.ui.item.TextArea;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;

@TaikodomAddon("taikodom.addon.tooltip")
/* renamed from: a.aue  reason: case insensitive filesystem */
/* compiled from: a */
public class C6838aue extends C3940wz {

    /* renamed from: DU */
    public static final Object f5414DU = "DATA";

    /* renamed from: DV */
    private Panel f5415DV;
    private JLabel titleLabel;

    /* renamed from: wI */
    private TextArea f5416wI;

    public C6838aue(IAddonProperties vWVar) {
        if (this.f5415DV == null) {
            this.f5415DV = (Panel) vWVar.bHv().mo16789b(TooltipAddon.class, "progressionLineTooltip.xml");
        }
        this.titleLabel = this.f5415DV.mo4917cf("title");
        this.f5416wI = this.f5415DV.mo4915cd("description");
    }

    /* renamed from: b */
    public Component mo2675b(Component component) {
        ProgressionLine afy = (ProgressionLine) ((JComponent) component).getClientProperty(f5414DU);
        this.titleLabel.setText(afy.cZK().mo19151rP().get());
        this.f5416wI.setText(afy.cZK().mo19154vW().get());
        this.f5415DV.pack();
        return this.f5415DV;
    }
}
