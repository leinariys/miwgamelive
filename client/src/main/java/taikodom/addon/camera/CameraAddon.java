package taikodom.addon.camera;

import game.engine.C6082agC;
import game.geometry.Vec3d;
import game.network.message.externalizable.C6475anf;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aVH;
import logic.aaa.C2061bk;
import logic.aaa.C2235dB;
import logic.aaa.aRX;
import logic.bbb.C6348alI;
import logic.render.IEngineGraphics;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.camera.Camera;
import taikodom.render.camera.OrbitalCamera;
import taikodom.render.scene.SceneObject;

@TaikodomAddon("taikodom.addon.camera")
/* compiled from: a */
public class CameraAddon implements C2495fo {
    private static final Log log = LogPrinter.setClass(CameraAddon.class);
    /* access modifiers changed from: private */
    public C2688ib fjQ;
    /* access modifiers changed from: private */
    public aQT fjR;
    /* access modifiers changed from: private */
    public C5744aVc fjS;
    /* access modifiers changed from: private */
    public C6267ajf fjT;
    /* access modifiers changed from: private */
    public Camera fjU;
    /* access modifiers changed from: private */
    public C0254DH fjV;
    /* access modifiers changed from: private */
    public C0254DH fjW;
    /* access modifiers changed from: private */
    public C0254DH fjX;
    /* access modifiers changed from: private */
    public float fjY;
    /* access modifiers changed from: private */
    public float fjZ;
    /* access modifiers changed from: private */
    public OrbitalCamera fka;
    /* access modifiers changed from: private */
    public int fkb;
    /* access modifiers changed from: private */
    public int fkc;
    /* access modifiers changed from: private */
    public int fkm = 90;
    /* access modifiers changed from: private */
    public int fkn = 90;
    /* access modifiers changed from: private */
    public float fko = 8000.0f;
    /* access modifiers changed from: private */
    public float fkp = 4000.0f;
    /* access modifiers changed from: private */
    public C3173oj.C3174a fkq = C3173oj.C3174a.DOCKED;
    /* access modifiers changed from: private */
    public C3173oj.C3174a fkr = C3173oj.C3174a.DOCKED;
    /* renamed from: kj */
    public IAddonProperties f9751kj;
    /* renamed from: lU */
    public Ship f9752lU;
    /* renamed from: lV */
    public IEngineGraphics f9753lV;
    /* access modifiers changed from: private */
    public int screenHeight;
    /* access modifiers changed from: private */
    public int screenWidth;
    private Vec3d ajm;
    private aVH<C2061bk> fkd;
    private aVH<C6475anf> fke;
    private aVH<C6060afg> fkf;
    private aVH<C6709asF> fkg;
    private aVH<C0688Jo> fkh;
    /* access modifiers changed from: private */
    private aVH<aRX> fki;
    /* access modifiers changed from: private */
    private aVH<C2640ht> fkj;
    /* access modifiers changed from: private */
    private WrapRunnable fkk;
    private float fkl = 55.0f;
    private C3173oj fks;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9751kj = addonPropertie;
        this.f9753lV = this.f9751kj.ale();
        this.fks = new C3173oj();
        this.fks.mo21031a(this.fkr);
        this.f9751kj.aVU().publish(this.fks);
        this.screenHeight = this.f9753lV.aet();
        this.fkb = this.screenHeight / 2;
        this.screenWidth = this.f9753lV.aes();
        this.fkc = this.screenWidth / 2;
        this.fjU = new Camera();
        this.fjU.setFovY(this.fkl);
        this.fjU.setAspect(this.f9753lV.aeq());
        this.fka = new OrbitalCamera();
        this.fka.setFovY(this.fkl);
        this.fka.setAspect(this.f9753lV.aeq());
        this.fjQ = new C2688ib(this.fjU);
        this.fjR = new aQT(this.fjU);
        this.fjS = new C5744aVc(this.fjU);
        this.fkh = new C4150a();
        this.fkg = new C4151b();
        this.fkf = new C4152c();
        this.fkj = new C4153d();
        this.fki = new C4154e();
        bRu();
        this.f9751kj.aVU().mo13965a(C6709asF.class, this.fkg);
        this.f9751kj.aVU().mo13965a(C6060afg.class, this.fkf);
        this.f9751kj.aVU().mo13965a(aRX.class, this.fki);
        this.f9751kj.aVU().mo13965a(C2640ht.class, this.fkj);
        this.f9751kj.aVU().mo13965a(C0688Jo.class, this.fkh);
    }

    private void bRu() {
        this.ajm = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 300.0d);
        this.fjV = new C0254DH();
        this.fjW = new C0254DH();
        this.fjX = new C0254DH();
        if (this.fkk == null) {
            this.fkk = this.f9751kj.getEngineGame().alf().addTask("Smooth orbital camera task", new C4155f(), 10);
        }
        this.fkd = new C4157h();
        this.fke = new C4156g();
        this.f9751kj.aVU().mo13965a(C2061bk.class, this.fkd);
        this.f9751kj.aVU().mo13965a(C6475anf.class, this.fke);
    }

    public void stop() {
        this.f9751kj.aVU().mo13964a(this.fkg);
        this.f9751kj.aVU().mo13964a(this.fkf);
        this.f9751kj.aVU().mo13964a(this.fki);
        this.f9751kj.aVU().mo13964a(this.fke);
        this.f9751kj.aVU().mo13964a(this.fkd);
        this.f9751kj.aVU().mo13964a(this.fkj);
        this.f9751kj.aVU().mo13964a(this.fkh);
        this.f9751kj.aVU().mo13974g(this.fks);
    }

    /* access modifiers changed from: private */
    public void bRv() {
        SceneObject cfO;
        if (this.f9752lU != null) {
            cfO = this.f9752lU.cHg();
            if (this.fkr == C3173oj.C3174a.ORBITAL) {
                this.fjT.mo2472kQ().mo4490IQ().mo17344b(this.f9752lU.ahO().mo2439IQ());
                if (cfO != null) {
                    cfO.hide();
                }
            }
        } else {
            cfO = this.fjS.cfO();
        }
        if (cfO != null) {
            this.fkq = this.fkr;
            this.fkr = C3173oj.C3174a.FIRST_PERSON;
            this.fks.mo21031a(C3173oj.C3174a.FIRST_PERSON);
            this.f9751kj.getPlayer().mo14348am("visualizationMode", "true");
            this.fjR.mo11822m(cfO);
            this.fjR.clearState();
            this.fjT.mo2450a((C3735uM) this.fjR);
            this.f9753lV.aea().setCamera(this.fjU);
        }
    }

    /* access modifiers changed from: private */
    public void bRw() {
        if (this.fjT == null || this.fjT.aRh() == null || this.f9752lU == null) {
            log.error("Invalid camera solid");
            return;
        }
        this.fjT.mo2472kQ().mo4490IQ().mo17344b(this.f9752lU.ahO().mo2439IQ());
        if (this.fkr == C3173oj.C3174a.FIRST_PERSON) {
            this.fjT.mo2472kQ().mo4490IQ().mo17344b(this.f9753lV.aea().getCamera().getGlobalTransform());
        }
        this.fjU.setTransform(this.f9753lV.aea().getCamera().getTransform());
        this.fkq = this.fkr;
        this.fkr = C3173oj.C3174a.CHASE;
        this.fks.mo21031a(C3173oj.C3174a.CHASE);
        this.f9753lV.aea().setCamera(this.fjU);
        SceneObject cHg = this.f9752lU.cHg();
        if (cHg != null) {
            cHg.show();
        }
        this.fjT.mo2450a((C3735uM) this.fjQ);
        this.f9751kj.getPlayer().mo14348am("visualizationMode", "false");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m42752a(Vec3d ajr, SceneObject sceneObject) {
        this.fkq = this.fkr;
        this.fkr = C3173oj.C3174a.ORBITAL;
        this.fks.mo21031a(C3173oj.C3174a.ORBITAL);
        if (ajr != null) {
            this.fjS.setDistance(ajr);
            this.fka.setDistance(ajr);
        }
        this.fjV.mo1268eU((float) this.fjS.getDistance().z);
        if (sceneObject != null) {
            sceneObject.getAabbLSLenght();
            this.fjS.mo11822m(sceneObject);
            this.fka.setTarget(sceneObject);
            sceneObject.show();
        }
        if (this.fjT != null) {
            this.fjT.mo2450a((C3735uM) this.fjS);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m42758b(Vec3d ajr, SceneObject sceneObject) {
        this.fkq = this.fkr;
        this.fkr = C3173oj.C3174a.DOCKED;
        this.fks.mo21031a(C3173oj.C3174a.DOCKED);
        if (ajr != null) {
            this.fka.setDistance(ajr);
        }
        this.fjV.mo1268eU((float) this.fka.getDistance().z);
        this.fka.setTarget(sceneObject);
        this.fka.setZoomFactor(sceneObject.getAabbWSLenght());
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$a */
    class C4150a extends C6124ags<C0688Jo> {
        C4150a() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0688Jo jo) {
            CameraAddon.this.fjQ.mo19645W(jo.aXW());
            return false;
        }
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$b */
    /* compiled from: a */
    class C4151b extends C6124ags<C6709asF> {
        C4151b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6709asF asf) {
            if (CameraAddon.this.fkr == C3173oj.C3174a.CHASE) {
                CameraAddon.this.bRv();
                return false;
            } else if (CameraAddon.this.fkr != C3173oj.C3174a.FIRST_PERSON) {
                return false;
            } else {
                CameraAddon.this.bRw();
                return false;
            }
        }
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$c */
    /* compiled from: a */
    class C4152c extends C6124ags<C6060afg> {
        C4152c() {
        }

        /* renamed from: b */
        public boolean updateInfo(C6060afg afg) {
            if (afg.bQB()) {
                CameraAddon.this.f9753lV.aea().setCamera(CameraAddon.this.fka);
            }
            CameraAddon.this.fjS.setZoomFactor((double) afg.bVr());
            CameraAddon.this.fka.setZoomFactor((double) afg.bVr());
            if (CameraAddon.this.fkr != C3173oj.C3174a.ORBITAL || afg.bVt() == 1) {
                Vec3d bVs = afg.bVs();
                SceneObject target = afg.getTarget();
                if ((!afg.bQB() || CameraAddon.this.fkr == C3173oj.C3174a.DOCKED) && afg.bVt() != -1) {
                    CameraAddon.this.m42752a(bVs, target);
                } else {
                    float random = ((float) (((Math.random() * 2.0d) - 1.0d) * ((double) CameraAddon.this.fkn) * 2.0d)) + CameraAddon.this.fka.getYaw();
                    CameraAddon.this.fjX.mo1268eU(CameraAddon.this.fka.getYaw());
                    CameraAddon.this.fjX.mo1269eV(random);
                    float f = (-((float) (Math.random() + 1.0d))) * 10.0f;
                    CameraAddon.this.fjY = -random;
                    CameraAddon.this.fjZ = -f;
                    CameraAddon.this.fka.setPitch(f);
                    CameraAddon.this.fjW.mo1268eU(f);
                    CameraAddon.this.fjW.mo1269eV(f);
                    CameraAddon.this.m42758b(bVs, target);
                }
            } else if (afg.bQB()) {
                CameraAddon.this.m42758b(afg.bVs(), afg.getTarget());
            } else if (CameraAddon.this.fkq == C3173oj.C3174a.CHASE) {
                CameraAddon.this.bRw();
            } else {
                CameraAddon.this.bRv();
            }
            Player dL = CameraAddon.this.f9751kj.getPlayer();
            PlayerController dxc = dL.dxc();
            if (CameraAddon.this.fkr == C3173oj.C3174a.ORBITAL) {
                C6082agC.bWV().aRS();
                if (dL.bQB()) {
                    return false;
                }
                if (!dxc.anX()) {
                    C6082agC.bWV().aRR();
                    return false;
                }
                C6082agC.bWV().aRS();
                return false;
            } else if (dL.bQB()) {
                C6082agC.bWV().aRR();
                return false;
            } else if (dxc.anX()) {
                C6082agC.bWV().aRT();
                dxc.cPd();
                return false;
            } else {
                C6082agC.bWV().aRR();
                return false;
            }
        }
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$d */
    /* compiled from: a */
    class C4153d extends C6124ags<C2640ht> {
        C4153d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2640ht htVar) {
            double aabbWSLenght = htVar.mo19420ak().getAabbWSLenght();
            CameraAddon.this.fjQ.mo19651j(new Vec3d(ScriptRuntime.NaN, aabbWSLenght / 8.0d, (-aabbWSLenght) / 2.0d));
            CameraAddon.this.fjQ.mo19650i(new Vec3d(ScriptRuntime.NaN, aabbWSLenght / 5.0d, aabbWSLenght));
            CameraAddon.this.fjQ.mo19652k(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, aabbWSLenght / 2.0d));
            CameraAddon.this.fjS.setDistance(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, (double) (htVar.mo19420ak().getAABB().dit() * 2.0f)));
            CameraAddon.this.fjV.mo1268eU((float) CameraAddon.this.fjS.getDistance().z);
            if (CameraAddon.this.fkr == C3173oj.C3174a.FIRST_PERSON) {
                CameraAddon.this.bRv();
                return false;
            }
            CameraAddon.this.bRw();
            return false;
        }
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$e */
    /* compiled from: a */
    class C4154e extends C6124ags<aRX> {
        C4154e() {
        }

        /* renamed from: a */
        public boolean updateInfo(aRX arx) {
            if (arx.dtY() != null) {
                CameraAddon.this.f9752lU = CameraAddon.this.f9751kj.getPlayer().bQx();
                CameraAddon.this.fjQ.mo14133jM(CameraAddon.this.f9752lU.mo963VH());
                CameraAddon.this.fjQ.mo14132jL(CameraAddon.this.f9752lU.mo1091ra());
                CameraAddon.this.fjQ.mo19645W(false);
                CameraAddon.this.fjR.mo14133jM(CameraAddon.this.f9752lU.mo963VH());
                CameraAddon.this.fjR.mo14132jL(CameraAddon.this.f9752lU.mo1091ra());
                CameraAddon.this.fjQ.mo19649f(CameraAddon.this.f9752lU);
                CameraAddon.this.fjR.mo14128b(arx.dtY());
                CameraAddon.this.fjQ.mo14128b(arx.dtY());
                CameraAddon.this.fjS.mo14128b(arx.dtY());
                if (CameraAddon.this.f9752lU.afR() != null) {
                    CameraAddon.this.fjQ.mo19646aD(CameraAddon.this.f9752lU.afR().mo12940in());
                } else {
                    CameraAddon.this.fjQ.mo19646aD(800.0f);
                }
                CameraAddon.this.fjT = new C6267ajf(new C6348alI(), CameraAddon.this.fjU);
                CameraAddon.this.fjT.mo14161jD(1000.0f);
                CameraAddon.this.fjT.mo2450a((C3735uM) CameraAddon.this.fjQ);
                CameraAddon.this.fjT.mo2472kQ().mo4490IQ().position.mo9484aA(arx.dtY().getPosition());
                C1355Tk cKn = CameraAddon.this.f9752lU.mo965Zc().cKn();
                CameraAddon.this.fjT.mo2449a(cKn.mo5725a(cKn.bvh().currentTimeMillis(), (C2235dB) CameraAddon.this.fjT, 2));
                C3387qy<C0461GN> a = cKn.mo13314a((C0461GN) CameraAddon.this.fjT);
                a.mo2339kO();
                CameraAddon.this.fjT.mo14160a(a);
                CameraAddon.this.fjT.aRh().mo3841kO();
                CameraAddon.this.f9753lV.aea().setCamera(CameraAddon.this.fjU);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$f */
    /* compiled from: a */
    class C4155f extends WrapRunnable {
        float dQp = 0.0f;
        float dQq = 0.0f;
        float eSg = 0.0f;

        C4155f() {
        }

        public void run() {
            float f = 10.0f;
            float aHd = CameraAddon.this.fjW.aHd() % 360.0f;
            float aHd2 = CameraAddon.this.fjX.aHd() % 360.0f;
            float aHd3 = CameraAddon.this.fjV.aHd();
            if (aHd != this.dQp) {
                if (CameraAddon.this.fkq != C3173oj.C3174a.DOCKED || aHd <= 10.0f) {
                    f = aHd;
                } else {
                    CameraAddon.this.fjW.mo1269eV(10.0f);
                    CameraAddon.this.fjW.mo1268eU(10.0f);
                    CameraAddon.this.fjZ = -10.0f;
                }
                CameraAddon.this.fjS.setPitch(f);
                CameraAddon.this.fka.setPitch(f);
                this.dQp = f;
            }
            if (aHd2 != this.dQq) {
                CameraAddon.this.fjS.setYaw(aHd2);
                CameraAddon.this.fka.setYaw(aHd2);
                this.dQq = aHd2;
            }
            if (aHd3 != this.eSg) {
                CameraAddon.this.fjS.setDistance(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, (double) aHd3));
                CameraAddon.this.fka.setDistance(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, (double) aHd3));
                this.eSg = aHd3;
            }
        }
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$h */
    /* compiled from: a */
    class C4157h extends C6124ags<C2061bk> {
        C4157h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2061bk bkVar) {
            float s;
            double d = CameraAddon.this.fjS.getDistance().z;
            double zoomFactor = CameraAddon.this.fjS.getZoomFactor();
            double amount = d - (((0.3d * d) + zoomFactor) * ((double) bkVar.getAmount()));
            if (amount < zoomFactor * 0.8d) {
                amount = (double) ((float) (zoomFactor * 0.8d));
            }
            if (CameraAddon.this.f9751kj.getPlayer().bQB()) {
                s = CameraAddon.this.fkp;
            } else {
                s = CameraAddon.this.fko;
            }
            if (amount > ((double) s)) {
                amount = (double) s;
            }
            CameraAddon.this.fjV.mo1269eV((float) amount);
            return false;
        }
    }

    /* renamed from: taikodom.addon.camera.CameraAddon$g */
    /* compiled from: a */
    class C4156g extends C6124ags<C6475anf> {
        C4156g() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6475anf anf) {
            float cks;
            float ckt;
            if (anf.isAbsolute()) {
                cks = anf.cks();
                ckt = anf.ckt();
            } else {
                cks = (anf.cks() - ((float) CameraAddon.this.fkc)) / ((float) CameraAddon.this.screenWidth);
                ckt = (anf.ckt() - ((float) CameraAddon.this.fkb)) / ((float) CameraAddon.this.screenHeight);
            }
            if (cks != 0.0f) {
                CameraAddon cameraAddon = CameraAddon.this;
                cameraAddon.fjY = (cks * ((float) CameraAddon.this.fkn)) + cameraAddon.fjY;
                CameraAddon.this.fjX.mo1269eV(-CameraAddon.this.fjY);
            }
            if (ckt == 0.0f) {
                return false;
            }
            float h = ckt * ((float) CameraAddon.this.fkn);
            if (CameraAddon.this.fjZ + h > ((float) CameraAddon.this.fkm)) {
                CameraAddon.this.fjZ = (float) CameraAddon.this.fkm;
            } else if (CameraAddon.this.fjZ + h < ((float) (-CameraAddon.this.fkm))) {
                CameraAddon.this.fjZ = (float) (-CameraAddon.this.fkm);
            } else {
                CameraAddon cameraAddon2 = CameraAddon.this;
                cameraAddon2.fjZ = h + cameraAddon2.fjZ;
            }
            CameraAddon.this.fjW.mo1269eV(-CameraAddon.this.fjZ);
            return false;
        }
    }
}
