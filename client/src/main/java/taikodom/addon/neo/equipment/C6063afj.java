package taikodom.addon.neo.equipment;

import game.script.item.Weapon;
import game.script.ship.Station;
import p001a.C3357qd;
import p001a.C3904wY;
import p001a.aGC;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.neo.equipment")
/* renamed from: a.afj  reason: case insensitive filesystem */
/* compiled from: a */
public class C6063afj extends C3357qd {
    private Weapon gSR;

    public C6063afj(IAddonProperties vWVar) {
        super(vWVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public void mo1305u(Object obj) {
        if (obj != null && this.f394P != null && this.f394P.bQx() != null) {
            if (!(obj instanceof Weapon)) {
                System.err.println("ERROR: LAUNCHER BAR 0 " + obj);
                return;
            }
            this.gSR = (Weapon) obj;
            Weapon afT = this.f394P.bQx().afT();
            if (afT != null) {
                afT.bai();
            }
            this.f394P.bQx().mo18329q(this.gSR);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: qc */
    public C3904wY mo1299qc() {
        return C3904wY.LAUNCHER;
    }

    /* renamed from: qd */
    public aGC mo1300qd() {
        return new C1879a(this.f395kj.translate("Launchers"), "launchers");
    }

    /* access modifiers changed from: protected */
    public String getTitle() {
        return this.f395kj.translate("Launchers");
    }

    /* access modifiers changed from: protected */
    /* renamed from: qe */
    public String mo1301qe() {
        return "SEC_SLOT";
    }

    /* renamed from: a.afj$a */
    class C1879a extends aGC {


        C1879a(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
            C6063afj.this.setVisible(z);
        }

        public boolean isEnabled() {
            if (C6063afj.this.f394P == null || C6063afj.this.f394P.bQx() == null || C6063afj.this.f394P.bQx().mo18253C(C3904wY.LAUNCHER) == null || C6063afj.this.f394P.bQB() || (C6063afj.this.f394P.bhE() instanceof Station)) {
                return false;
            }
            return true;
        }
    }
}
