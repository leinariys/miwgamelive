package taikodom.addon.neo.equipment;

import game.script.item.Weapon;
import logic.WrapRunnable;
import p001a.C3357qd;
import p001a.C3904wY;
import p001a.C5567aOh;
import p001a.aGC;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.neo.equipment")
/* renamed from: a.Xt */
/* compiled from: a */
public class C1631Xt extends C3357qd {
    private Weapon eIZ;
    private WrapRunnable eJa = new C5567aOh(this);

    public C1631Xt(IAddonProperties vWVar) {
        super(vWVar);
        this.f395kj.alf().addTask("AmmoUpdateTask", this.eJa, 100);
    }

    public void destroy() {
        super.destroy();
        this.eJa.cancel();
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public void mo1305u(Object obj) {
        if (obj != null && this.f394P != null && this.f394P.bQx() != null) {
            if (!(obj instanceof Weapon)) {
                System.err.println("ERROR: CANNONS BAR 0 " + obj);
                return;
            }
            this.eIZ = (Weapon) obj;
            Weapon afR = this.f394P.bQx().afR();
            if (afR != null) {
                afR.bai();
            }
            this.f394P.bQx().mo18328o(this.eIZ);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: qc */
    public C3904wY mo1299qc() {
        return C3904wY.CANNON;
    }

    /* renamed from: qd */
    public aGC mo1300qd() {
        return new C1632a(this.f395kj.translate("Cannons"), "cannons");
    }

    /* access modifiers changed from: protected */
    public String getTitle() {
        return this.f395kj.translate("Cannons");
    }

    /* access modifiers changed from: protected */
    /* renamed from: qe */
    public String mo1301qe() {
        return "SLOT";
    }

    /* renamed from: a.Xt$a */
    class C1632a extends aGC {


        C1632a(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
            C1631Xt.this.setVisible(z);
        }

        public boolean isEnabled() {
            if (C1631Xt.this.f394P == null || C1631Xt.this.f394P.bQx() == null || C1631Xt.this.f394P.bQx().mo18253C(C3904wY.CANNON) == null || C1631Xt.this.f394P.bQB()) {
                return false;
            }
            return true;
        }
    }
}
