package taikodom.addon.neo.equipment;

import game.script.item.Amplifier;
import game.script.item.Component;
import game.script.item.ItemType;
import game.script.item.Weapon;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.ui.item.LabeledIcon;
import logic.ui.item.ListJ;
import logic.ui.item.TaskPane;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.input.CommandTranslatorAddon;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;

@TaikodomAddon("taikodom.addon.neo.equipment")
/* renamed from: a.DL */
/* compiled from: a */
public abstract class C0258DL {
    private static final String hvY = "equipmentbar.xml";
    /* access modifiers changed from: private */
    public ListJ hvZ;
    /* access modifiers changed from: private */
    public Object hwa;
    public TaskPane bgR;
    /* renamed from: kj */
    public IAddonProperties f395kj;
    /* renamed from: P */
    public Player f394P = this.f395kj.getPlayer();

    public C0258DL(IAddonProperties vWVar) {
        this.f395kj = vWVar;
        mo1298qb();
    }

    /* access modifiers changed from: protected */
    public abstract String getTitle();

    /* access modifiers changed from: protected */
    public abstract TransferHandler getTransferHandler();

    /* access modifiers changed from: protected */
    /* renamed from: qc */
    public abstract C3904wY mo1299qc();

    /* renamed from: qd */
    public abstract aGC mo1300qd();

    /* access modifiers changed from: protected */
    /* renamed from: qe */
    public abstract String mo1301qe();

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public abstract void mo1305u(Object obj);

    public TaskPane cQf() {
        return this.bgR;
    }

    /* access modifiers changed from: protected */
    /* renamed from: qb */
    public void mo1298qb() {
        this.bgR = (TaskPane) this.f395kj.bHv().mo16794bQ(hvY);
        this.bgR.setTitle(getTitle());
        this.hvZ = this.bgR.mo4915cd("equipment-list");
        this.hvZ.setCellRenderer(new C0259a());
        this.hvZ.addListSelectionListener(new C0260b());
        this.hvZ.setTransferHandler(getTransferHandler());
        this.hvZ.setDragEnabled(false);
        this.hvZ.setDropMode(DropMode.ON);
    }

    public void destroy() {
        this.bgR.destroy();
        this.bgR = null;
        this.hvZ = null;
        this.hwa = null;
    }

    /* access modifiers changed from: protected */
    public void repaint() {
        if (this.hvZ != null && this.hvZ.isVisible()) {
            this.hvZ.repaint();
        }
    }

    public void refresh() {
        if (this.hvZ != null) {
            Object obj = this.hwa;
            DefaultListModel model = this.hvZ.getModel();
            model.clear();
            Ship bQx = this.f395kj.getPlayer().bQx();
            if (bQx != null) {
                for (Component addElement : bQx.mo18317g(mo1299qc())) {
                    model.addElement(addElement);
                }
                if (!model.contains(obj)) {
                    mo1306xj(0);
                } else {
                    this.hvZ.setSelectedValue(obj, true);
                }
            }
        }
    }

    public void setVisible(boolean z) {
        if (z) {
            refresh();
        }
    }

    public void next() {
        int i;
        if (this.hvZ != null) {
            int selectedIndex = this.hvZ.getSelectedIndex();
            if (this.hvZ.getModel().getSize() <= selectedIndex + 1) {
                i = 0;
            } else {
                i = selectedIndex + 1;
            }
            this.hvZ.setSelectedIndex(i);
        }
    }

    public void cQg() {
        int i;
        if (this.hvZ != null) {
            int selectedIndex = this.hvZ.getSelectedIndex();
            if (-1 >= selectedIndex - 1) {
                i = this.hvZ.getModel().getSize() - 1;
            } else {
                i = selectedIndex - 1;
            }
            this.hvZ.setSelectedIndex(i);
        }
    }

    /* renamed from: xj */
    public void mo1306xj(int i) {
        if (this.hvZ != null && i >= 0 && i < this.hvZ.getModel().getSize()) {
            SwingUtilities.invokeLater(new C0261c(this.hvZ, i));
        }
    }

    public void cQh() {
        if (this.hvZ != null) {
            this.hvZ.getSelectionModel().clearSelection();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public java.awt.Component mo1290b(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
        LabeledIcon component = aur.mo11630nQ("equipment").getComponent();
        if (!(obj instanceof Component)) {
            component.mo2602a(LabeledIcon.C0530a.NORTH_EAST, "ERROR");
        } else {
            if (obj instanceof Weapon) {
                Weapon adv = (Weapon) obj;
                component.mo2605a((Icon) new C2539gY((ItemType) adv.aqz()));
                component.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, String.valueOf(adv.aqh()));
                component.mo2602a(LabeledIcon.C0530a.NORTH_WEST, String.valueOf(((CommandTranslatorAddon) this.f395kj.mo11830U(CommandTranslatorAddon.class)).mo24071af(String.valueOf(mo1301qe()) + (i + 1))));
            } else if (obj instanceof Amplifier) {
                component.mo2605a((Icon) new C2539gY((ItemType) ((Amplifier) obj).cJS()));
                component.mo2602a(LabeledIcon.C0530a.NORTH_WEST, String.valueOf(((CommandTranslatorAddon) this.f395kj.mo11830U(CommandTranslatorAddon.class)).mo24071af(String.valueOf(mo1301qe()) + (i + 1))));
            } else {
                component.mo2602a(LabeledIcon.C0530a.NORTH_EAST, "ERROR");
            }
            component.setPreferredSize(new Dimension(64, 64));
        }
        return component;
    }

    /* renamed from: a.DL$a */
    class C0259a extends C5517aMj {
        C0259a() {
        }

        /* renamed from: a */
        public java.awt.Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            return C0258DL.this.mo1290b(aur, jList, obj, i, z, z2);
        }
    }

    /* renamed from: a.DL$b */
    /* compiled from: a */
    class C0260b implements ListSelectionListener {
        C0260b() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            Object selectedValue;
            if (!listSelectionEvent.getValueIsAdjusting() && C0258DL.this.hwa != (selectedValue = C0258DL.this.hvZ.getSelectedValue())) {
                C0258DL.this.hwa = selectedValue;
                C0258DL.this.mo1305u(selectedValue);
                C0258DL.this.f395kj.aVU().mo13975h(new C6811auD(C3667tV.WEAPON_CHANGE));
            }
        }
    }

    /* renamed from: a.DL$c */
    /* compiled from: a */
    class C0261c implements Runnable {
        private final /* synthetic */ JList cIV;
        private final /* synthetic */ int cIW;

        C0261c(JList jList, int i) {
            this.cIV = jList;
            this.cIW = i;
        }

        public void run() {
            this.cIV.setSelectedIndex(this.cIW);
        }
    }
}
