package taikodom.addon.neo.equipment;

import game.script.item.Component;
import game.script.ship.Station;
import p001a.C3904wY;
import p001a.aGC;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;

@TaikodomAddon("taikodom.addon.neo.equipment")
/* renamed from: a.fq */
/* compiled from: a */
public class C2497fq extends C0258DL {
    public C2497fq(IAddonProperties vWVar) {
        super(vWVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: qb */
    public void mo1298qb() {
        super.mo1298qb();
        this.bgR.collapse();
    }

    /* access modifiers changed from: protected */
    public TransferHandler getTransferHandler() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public void mo1305u(Object obj) {
        if (obj != null && this.f394P != null && this.f394P.bQx() != null) {
            cQh();
            if (!(obj instanceof Component)) {
                System.err.println("ERROR: SPECIALS BAR 0 " + obj);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: qc */
    public C3904wY mo1299qc() {
        return C3904wY.AMPLIFIER;
    }

    /* renamed from: qd */
    public aGC mo1300qd() {
        return new C2498a(this.f395kj.translate("Amplifiers"), "amplifiers");
    }

    /* access modifiers changed from: protected */
    public String getTitle() {
        return this.f395kj.translate("Amplifiers");
    }

    /* access modifiers changed from: protected */
    /* renamed from: qe */
    public String mo1301qe() {
        return "MODULE";
    }

    /* renamed from: a.fq$a */
    class C2498a extends aGC {


        C2498a(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
            C2497fq.this.setVisible(z);
        }

        public boolean isEnabled() {
            if (C2497fq.this.f394P == null || C2497fq.this.f394P.bQx() == null || C2497fq.this.f394P.bQx().mo18253C(C3904wY.AMPLIFIER) == null || C2497fq.this.f394P.bQB() || (C2497fq.this.f394P.bhE() instanceof Station)) {
                return false;
            }
            return true;
        }
    }
}
