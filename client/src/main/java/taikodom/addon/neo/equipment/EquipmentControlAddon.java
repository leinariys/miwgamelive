package taikodom.addon.neo.equipment;

import game.network.message.externalizable.C2651i;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import p001a.C3428rT;
import p001a.aSZ;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.neo.equipment")
/* compiled from: a */
public class EquipmentControlAddon implements C2495fo {
    /* access modifiers changed from: private */
    public C6063afj hJC;
    /* access modifiers changed from: private */
    public C2497fq hJF;
    /* access modifiers changed from: private */
    public C1631Xt hJz;
    private C3428rT<C5783aaP> bvz;
    private aSZ hJA;
    private C2899lb hJB;
    private aSZ hJD;
    private aAW hJE;
    private aSZ hJG;
    private C7029azq hJH;
    private C3428rT<C2651i> hJI;
    /* renamed from: kj */
    private IAddonProperties f10143kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10143kj = addonPropertie;
        Player dL = this.f10143kj.getPlayer();
        if (dL != null) {
            this.hJI = new C4775a(dL);
            this.f10143kj.aVU().mo13965a(C2651i.class, this.hJI);
            if (dL.bQx() != null) {
                init();
            }
        }
    }

    /* access modifiers changed from: private */
    public void init() {
        this.hJz = new C1631Xt(this.f10143kj);
        this.hJB = new C2899lb(this.f10143kj, this.hJz);
        this.hJA = new aSZ(this.hJz.cQf(), this.hJz.mo1300qd(), 0, 2);
        this.hJC = new C6063afj(this.f10143kj);
        this.hJE = new aAW(this.f10143kj, this.hJC);
        this.hJD = new aSZ(this.hJC.cQf(), this.hJC.mo1300qd(), 1, 2);
        this.hJF = new C2497fq(this.f10143kj);
        this.hJH = new C7029azq(this.f10143kj, this.hJF);
        this.hJG = new aSZ(this.hJF.cQf(), this.hJF.mo1300qd(), 2, 2);
        this.bvz = new C4776b();
        this.f10143kj.mo2317P(this.hJB);
        this.f10143kj.mo2317P(this.hJE);
        this.f10143kj.mo2317P(this.hJH);
        this.f10143kj.aVU().publish(this.hJA);
        this.f10143kj.aVU().publish(this.hJD);
        this.f10143kj.aVU().publish(this.hJG);
        this.f10143kj.aVU().mo13965a(C5783aaP.class, this.bvz);
    }

    public void stop() {
        cleanUp();
        this.f10143kj.aVU().mo13964a(this.hJI);
    }

    /* access modifiers changed from: private */
    public void cleanUp() {
        if (this.hJB != null) {
            this.hJB.stop();
            this.f10143kj.mo2318Q(this.hJB);
            this.hJB = null;
        }
        if (this.hJE != null) {
            this.hJE.stop();
            this.f10143kj.mo2318Q(this.hJE);
            this.hJE = null;
        }
        if (this.hJH != null) {
            this.hJH.stop();
            this.f10143kj.mo2318Q(this.hJH);
            this.hJH = null;
        }
        if (this.bvz != null) {
            this.f10143kj.aVU().mo13964a(this.bvz);
            this.bvz = null;
        }
        if (this.hJA != null) {
            this.f10143kj.aVU().mo13974g(this.hJA);
        }
        if (this.hJD != null) {
            this.f10143kj.aVU().mo13974g(this.hJD);
        }
        if (this.hJG != null) {
            this.f10143kj.aVU().mo13974g(this.hJG);
        }
        if (this.hJz != null) {
            this.hJz.destroy();
            this.hJz = null;
        }
        if (this.hJC != null) {
            this.hJC.destroy();
            this.hJC = null;
        }
        if (this.hJF != null) {
            this.hJF.destroy();
            this.hJF = null;
        }
    }

    /* renamed from: taikodom.addon.neo.equipment.EquipmentControlAddon$a */
    class C4775a extends C3428rT<C2651i> {
        private final /* synthetic */ Player agw;

        C4775a(Player aku) {
            this.agw = aku;
        }

        /* renamed from: a */
        public void mo321b(C2651i iVar) {
            EquipmentControlAddon.this.cleanUp();
            if (this.agw.bQx() != null) {
                EquipmentControlAddon.this.init();
            }
        }
    }

    /* renamed from: taikodom.addon.neo.equipment.EquipmentControlAddon$b */
    /* compiled from: a */
    class C4776b extends C3428rT<C5783aaP> {
        C4776b() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (aap.bMO().equals(C5783aaP.C1841a.FLYING)) {
                EquipmentControlAddon.this.hJC.refresh();
                EquipmentControlAddon.this.hJz.refresh();
                EquipmentControlAddon.this.hJF.refresh();
            }
        }
    }
}
