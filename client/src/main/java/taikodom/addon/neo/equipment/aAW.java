package taikodom.addon.neo.equipment;

import game.script.PlayerController;
import game.script.player.Player;
import logic.aaa.C1506WA;
import logic.aaa.C2733jJ;
import logic.aaa.C6831auX;
import p001a.C2602hR;
import p001a.C3904wY;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.neo.equipment")
/* renamed from: a.aAW */
/* compiled from: a */
public class aAW {
    private C6063afj hfv;

    /* renamed from: kj */
    private IAddonProperties f2328kj;

    public aAW(IAddonProperties vWVar, C6063afj afj) {
        this.f2328kj = vWVar;
        this.hfv = afj;
    }

    public void stop() {
    }

    @C2602hR(mo19255zf = "SHOOT_LAUNCHER")
    /* renamed from: hG */
    public void mo7638hG(boolean z) {
        PlayerController alb = this.f2328kj.alb();
        if (!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX() && !alb.cNM()) {
            Player dL = this.f2328kj.getPlayer();
            if (z) {
                dL.bQx().mo18331y(C3904wY.LAUNCHER);
            } else {
                dL.bQx().mo18250A(C3904wY.LAUNCHER);
            }
            if (dL.bQx().mo18313e(C3904wY.LAUNCHER) != null && !dL.bQx().mo18322i(C3904wY.LAUNCHER)) {
                dL.mo14419f((C1506WA) new C2733jJ(this.f2328kj.translate("Empty weapon"), true, new Object[0]));
                dL.mo14419f((C1506WA) new C6831auX(dL.bQx().mo18313e(C3904wY.LAUNCHER)));
            }
        }
    }

    @C2602hR(mo19255zf = "NEXT_LAUNCHER")
    /* renamed from: hH */
    public void mo7639hH(boolean z) {
        PlayerController alb = this.f2328kj.alb();
        if (z && !alb.cPf().mo22170R() && !alb.cPf().mo22163D() && !alb.cNM()) {
            this.hfv.next();
            this.f2328kj.aVU().mo13972d("cannon.select.next", "");
        }
    }

    @C2602hR(mo19255zf = "PREV_LAUNCHER")
    /* renamed from: hI */
    public void mo7640hI(boolean z) {
        PlayerController alb = this.f2328kj.alb();
        if (z && !alb.cPf().mo22170R() && !alb.cPf().mo22163D() && !alb.cNM()) {
            this.hfv.cQg();
            this.f2328kj.aVU().mo13972d("cannon.select.prev", "");
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT1")
    /* renamed from: hJ */
    public void mo7641hJ(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(0);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT2")
    /* renamed from: hK */
    public void mo7642hK(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(1);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT3")
    /* renamed from: hL */
    public void mo7643hL(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(2);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT4")
    /* renamed from: hM */
    public void mo7644hM(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(3);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT5")
    /* renamed from: hN */
    public void mo7645hN(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(4);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT6")
    /* renamed from: hO */
    public void mo7646hO(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(5);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT7")
    /* renamed from: hP */
    public void mo7647hP(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(6);
        }
    }

    @C2602hR(mo19255zf = "SEC_SLOT8")
    /* renamed from: hQ */
    public void mo7648hQ(boolean z) {
        if (z && !this.f2328kj.getPlayer().bQB()) {
            this.hfv.mo1306xj(7);
        }
    }
}
