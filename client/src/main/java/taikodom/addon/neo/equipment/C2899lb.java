package taikodom.addon.neo.equipment;

import game.script.player.Player;
import logic.aaa.*;
import p001a.C2602hR;
import p001a.C3904wY;
import p001a.C6124ags;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.neo.equipment")
/* renamed from: a.lb */
/* compiled from: a */
public class C2899lb {
    /* access modifiers changed from: private */
    public boolean hNR = false;
    /* renamed from: kj */
    public IAddonProperties f8559kj;
    private C1631Xt hNO;
    private C6124ags<C3949xF> hNP = new C2901b();
    /* access modifiers changed from: private */
    private C6124ags<C2208cm> hNQ = new C2903c();

    public C2899lb(IAddonProperties vWVar, C1631Xt xt) {
        this.f8559kj = vWVar;
        this.hNO = xt;
        this.f8559kj.aVU().mo13965a(C2208cm.class, this.hNQ);
        this.f8559kj.aVU().mo13965a(C3949xF.class, this.hNP);
    }

    public void stop() {
        this.f8559kj.aVU().mo13964a(this.hNQ);
        this.f8559kj.aVU().mo13964a(this.hNP);
    }

    @C2602hR(mo19255zf = "SHOOT_CANNON")
    /* renamed from: ji */
    public void mo20301ji(boolean z) {
        this.f8559kj.getEngineGame().mo4091e(new C2900a(z));
    }

    /* access modifiers changed from: private */
    /* renamed from: jj */
    public void m35120jj(boolean z) {
        PlayerController alb = this.f8559kj.alb();
        if ((!alb.cPf().mo22170R() && !alb.cPf().mo22163D() && alb.anX()) || !z) {
            this.hNR = z;
            Player dL = this.f8559kj.getPlayer();
            if (z) {
                dL.bQx().mo18331y(C3904wY.CANNON);
                if (dL.bQx().mo18313e(C3904wY.CANNON) != null && !dL.bQx().mo18322i(C3904wY.CANNON)) {
                    dL.mo14419f((C1506WA) new C2733jJ(this.f8559kj.translate("Empty weapon"), true, new Object[0]));
                    dL.mo14419f((C1506WA) new C0797LY(dL.bQx().mo18313e(C3904wY.CANNON)));
                    return;
                }
                return;
            }
            dL.bQx().mo18250A(C3904wY.CANNON);
        }
    }

    @C2602hR(mo19255zf = "NEXT_CANNON")
    /* renamed from: hH */
    public void mo20299hH(boolean z) {
        PlayerController alb = this.f8559kj.alb();
        if (z && !alb.cPf().mo22170R() && !alb.cPf().mo22163D() && !alb.cNM()) {
            this.hNO.next();
            this.f8559kj.aVU().mo13972d("cannon.select.next", "");
        }
    }

    @C2602hR(mo19255zf = "PREV_CANNON")
    /* renamed from: hI */
    public void mo20300hI(boolean z) {
        PlayerController alb = this.f8559kj.alb();
        if (z && !alb.cPf().mo22170R() && !alb.cPf().mo22163D() && !alb.cNM()) {
            this.hNO.cQg();
            this.f8559kj.aVU().mo13972d("cannon.select.prev", "");
        }
    }

    @C2602hR(mo19255zf = "SLOT1")
    /* renamed from: jk */
    public void mo20302jk(boolean z) {
        if (z && !this.f8559kj.getPlayer().bQB()) {
            this.hNO.mo1306xj(0);
        }
    }

    @C2602hR(mo19255zf = "SLOT2")
    /* renamed from: jl */
    public void mo20303jl(boolean z) {
        if (z && !this.f8559kj.getPlayer().bQB()) {
            this.hNO.mo1306xj(1);
        }
    }

    @C2602hR(mo19255zf = "SLOT3")
    /* renamed from: jm */
    public void mo20304jm(boolean z) {
        if (z && !this.f8559kj.getPlayer().bQB()) {
            this.hNO.mo1306xj(2);
        }
    }

    @C2602hR(mo19255zf = "SLOT4")
    /* renamed from: jn */
    public void mo20305jn(boolean z) {
        if (z && !this.f8559kj.getPlayer().bQB()) {
            this.hNO.mo1306xj(3);
        }
    }

    @C2602hR(mo19255zf = "SLOT5")
    /* renamed from: jo */
    public void mo20306jo(boolean z) {
        if (z && !this.f8559kj.getPlayer().bQB()) {
            this.hNO.mo1306xj(4);
        }
    }

    @C2602hR(mo19255zf = "SLOT6")
    /* renamed from: jp */
    public void mo20307jp(boolean z) {
        if (z && !this.f8559kj.getPlayer().bQB()) {
            this.hNO.mo1306xj(5);
        }
    }

    /* renamed from: a.lb$c */
    /* compiled from: a */
    class C2903c extends C6124ags<C2208cm> {
        C2903c() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2208cm cmVar) {
            if (!C2899lb.this.hNR) {
                return false;
            }
            C2899lb.this.mo20301ji(true);
            return false;
        }
    }

    /* renamed from: a.lb$b */
    /* compiled from: a */
    class C2901b extends C6124ags<C3949xF> {
        C2901b() {
        }

        /* renamed from: b */
        public boolean updateInfo(C3949xF xFVar) {
            if (xFVar.anX()) {
                return false;
            }
            C2899lb.this.f8559kj.getEngineGame().mo4091e(new C2902a());
            return false;
        }

        /* renamed from: a.lb$b$a */
        class C2902a implements Runnable {
            C2902a() {
            }

            public void run() {
                C2899lb.this.mo20301ji(false);
            }
        }
    }

    /* renamed from: a.lb$a */
    class C2900a implements Runnable {

        /* renamed from: xi */
        private final /* synthetic */ boolean f8560xi;

        C2900a(boolean z) {
            this.f8560xi = z;
        }

        public void run() {
            C2899lb.this.m35120jj(this.f8560xi);
        }
    }
}
