package taikodom.addon.neo.preferences;

import game.network.message.externalizable.C5268aCu;
import logic.IAddonSettings;
import logic.ui.item.InternalFrame;
import logic.ui.item.TaskPane;
import logic.ui.item.TaskPaneContainer;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@TaikodomAddon("taikodom.addon.neo.preferences")
/* compiled from: a */
public class GUIPrefAddon implements C2495fo {
    private static final String SEPARATOR = ".";
    private static Dimension cUE;
    private static Map<String, String> cUF = new HashMap();
    private static String filename;

    /* renamed from: kj */
    private static IAddonProperties f10157kj;
    private C6124ags<C5268aCu> cUG;

    /* renamed from: y */
    private static String m44899y(String str, String str2) {
        return f10157kj.mo11831U(String.valueOf(str) + SEPARATOR + str2, filename);
    }

    /* renamed from: e */
    public static void m44897e(String str, String str2, String str3) {
        if (str != null && str2 != null && str2.length() != 0 && str3 != null && str3.length() != 0) {
            cUF.put(String.valueOf(str) + SEPARATOR + str2, str3);
        }
    }

    /* renamed from: a */
    public static void m44891a(String str, String[] strArr, String[] strArr2) {
        if (str != null && strArr != null && strArr.length != 0 && strArr2 != null && strArr2.length != 0 && strArr.length == strArr2.length) {
            for (int i = 0; i < strArr.length; i++) {
                m44897e(str, strArr[i], strArr2[i]);
            }
        }
    }

    /* renamed from: b */
    public static void m44895b(String str, int i, String str2) {
        if ((i & 32) != 0) {
            m44897e(str, C4817c.C, str2);
        } else if ((i & 8) != 0) {
            m44897e(str, C4817c.HEIGHT, str2);
        } else if ((i & 64) != 0) {
            m44897e(str, C4817c.O, str2);
        } else if ((i & 16) != 0) {
            m44897e(str, C4817c.P, str2);
        } else if ((i & 4) != 0) {
            m44897e(str, C4817c.WIDTH, str2);
        } else if ((i & 1) != 0) {
            m44897e(str, C4817c.X, str2);
        } else if ((i & 2) != 0) {
            m44897e(str, C4817c.Y, str2);
        }
    }

    /* renamed from: a */
    public static String[] m44892a(String str, String[] strArr) {
        String[] strArr2 = new String[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr2[i] = m44898f(str, strArr[i], "");
        }
        return strArr2;
    }

    /* renamed from: b */
    public static String[] m44896b(String str, String[] strArr, String[] strArr2) {
        if (str == null || strArr == null || strArr.length == 0 || strArr2 == null || strArr2.length == 0 || strArr.length != strArr2.length) {
            return null;
        }
        String[] strArr3 = new String[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            strArr3[i] = m44898f(str, strArr[i], strArr2[i]);
        }
        return strArr3;
    }

    /* renamed from: f */
    public static String m44898f(String str, String str2, String str3) {
        String str4 = cUF.get(String.valueOf(str) + SEPARATOR + str2);
        if (str4 == null) {
            str4 = m44899y(str, str2);
            if (str4 == null) {
                return str3;
            }
            m44891a(str, new String[]{str2}, new String[]{str4});
        }
        return str4;
    }

    /* renamed from: a */
    public static Integer m44886a(String str, String str2, Integer num) {
        String f = m44898f(str, str2, "NaN");
        if ("NaN".equals(f)) {
            return num;
        }
        try {
            return Integer.valueOf(Integer.parseInt(f));
        } catch (NumberFormatException e) {
            return num;
        }
    }

    /* renamed from: a */
    public static void m44887a(InternalFrame nxVar, String str) {
        m44888a(nxVar, str, 15);
    }

    /* renamed from: a */
    public static void m44888a(InternalFrame nxVar, String str, int i) {
        if (nxVar != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if ((i & 1) != 0) {
                arrayList.add(C4817c.X);
                arrayList2.add(String.valueOf(nxVar.getX()));
            }
            if ((i & 2) != 0) {
                arrayList.add(C4817c.Y);
                arrayList2.add(String.valueOf(nxVar.getY()));
            }
            if ((i & 4) != 0) {
                arrayList.add(C4817c.WIDTH);
                arrayList2.add(String.valueOf(nxVar.getWidth()));
            }
            if ((i & 8) != 0) {
                arrayList.add(C4817c.HEIGHT);
                arrayList2.add(String.valueOf(nxVar.getHeight()));
            }
            if (arrayList.size() != 0) {
                m44891a(str, (String[]) arrayList.toArray(new String[0]), (String[]) arrayList2.toArray(new String[0]));
            }
        }
    }

    /* renamed from: b */
    public static void m44893b(InternalFrame nxVar, String str) {
        m44894b(nxVar, str, 15);
    }

    /* renamed from: b */
    public static void m44894b(InternalFrame nxVar, String str, int i) {
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4 = null;
        if (nxVar != null) {
            if ((i & 1) != 0) {
                num = m44886a(str, C4817c.X, (Integer) null);
            } else {
                num = null;
            }
            if ((i & 2) != 0) {
                num2 = m44886a(str, C4817c.Y, (Integer) null);
            } else {
                num2 = null;
            }
            if ((i & 4) != 0) {
                num3 = m44886a(str, C4817c.WIDTH, (Integer) null);
            } else {
                num3 = null;
            }
            if ((i & 8) != 0) {
                num4 = m44886a(str, C4817c.HEIGHT, (Integer) null);
            }
            if (!(num3 == null || num4 == null)) {
                nxVar.setSize(num3.intValue(), num4.intValue());
            }
            if (num == null || num2 == null) {
                nxVar.center();
                return;
            }
            if (num.intValue() < 0) {
                num = 0;
            } else if (num.intValue() > cUE.width) {
                num = Integer.valueOf(cUE.width - num3.intValue());
            }
            if (num2.intValue() < 0) {
                num2 = 0;
            } else if (num2.intValue() > cUE.height) {
                num2 = Integer.valueOf(cUE.height - num4.intValue());
            }
            nxVar.setLocation(num.intValue(), num2.intValue());
        }
    }

    /* renamed from: a */
    public static void m44889a(TaskPaneContainer xxVar, TaskPane bpVar, String str) {
        m44890a(xxVar, bpVar, str, 56);
    }

    /* renamed from: a */
    public static void m44890a(TaskPaneContainer xxVar, TaskPane bpVar, String str, int i) {
        int i2;
        int i3;
        int i4;
        if ((i & 8) != 0) {
            i2 = m44886a(str, C4817c.HEIGHT, -1).intValue();
        } else {
            i2 = -1;
        }
        if ((i & 32) != 0) {
            i3 = m44886a(str, C4817c.C, (Integer) 0).intValue();
        } else {
            i3 = 0;
        }
        if ((i & 16) != 0) {
            i4 = m44886a(str, C4817c.P, (Integer) 0).intValue();
        } else {
            i4 = 0;
        }
        if (i2 != -1) {
            Dimension dimension = new Dimension(bpVar.getWidth(), i2);
            bpVar.setPreferredSize(dimension);
            bpVar.setSize(dimension);
        }
        if (i3 != 0) {
            bpVar.collapse();
            bpVar.mo17429aw(false);
        } else if (i4 != 0) {
            bpVar.expand();
            bpVar.mo17429aw(true);
        }
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        f10157kj = addonPropertie;
        if (f10157kj.getPlayer() != null) {
            cUE = f10157kj.bHv().getScreenSize();
            filename = String.valueOf(String.valueOf(f10157kj.getPlayer().cWm())) + "-guiprefs";
            this.cUG = new C4816b();
            f10157kj.aVU().mo13965a(C5268aCu.class, this.cUG);
        }
    }

    public void stop() {
        aPg();
        f10157kj.aVU().mo13964a(this.cUG);
    }

    private void aPg() {
        for (String next : cUF.keySet()) {
            f10157kj.mo11845e(next, cUF.get(next), filename);
        }
    }

    /* renamed from: taikodom.addon.neo.preferences.GUIPrefAddon$a */
    public interface C4815a {
        public static final int HEIGHT = 8;
        public static final int WIDTH = 4;
        public static final int aCA = 32;
        public static final int aCB = 64;
        public static final int aCx = 1;
        public static final int aCy = 2;
        public static final int aCz = 16;
    }

    /* renamed from: taikodom.addon.neo.preferences.GUIPrefAddon$c */
    /* compiled from: a */
    public interface C4817c {
        public static final String HEIGHT = "h";
        public static final String WIDTH = "w";
        public static final String X = "x";
        public static final String Y = "y";
        public static final String P = "p";
        public static final String C = "c";
        public static final String O = "o";
    }

    /* renamed from: taikodom.addon.neo.preferences.GUIPrefAddon$b */
    /* compiled from: a */
    class C4816b extends C6124ags<C5268aCu> {
        C4816b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5268aCu acu) {
            GUIPrefAddon.this.stop();
            return false;
        }
    }
}
