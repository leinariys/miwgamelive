package taikodom.addon.neo.oninet;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3131oI;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.aTX;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aVH;
import logic.aaa.C0929Nd;
import logic.aaa.C1274Sq;
import logic.aaa.C2327eB;
import logic.aaa.C3949xF;
import p001a.C3428rT;
import p001a.aMU;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import javax.swing.*;

@TaikodomAddon("taikodom.addon.neo.oninet")
/* compiled from: a */
public class OniNetAddon implements C2495fo {
    /* access modifiers changed from: private */
    public C3428rT<C3131oI> ccC;
    /* access modifiers changed from: private */
    public C0852MQ ccu;
    /* access modifiers changed from: private */
    public boolean ccv;
    /* renamed from: kj */
    public IAddonProperties f10151kj;
    private C3428rT<HudVisibilityAddon.C4534d> bLC;
    private aVH<C0929Nd> ccA;
    private C3428rT<C1532WW> ccB;
    private C3428rT<aTX> ccw;
    private C3428rT<C3949xF> ccx;
    private C3428rT<C1274Sq> ccy;
    /* access modifiers changed from: private */
    private C3428rT<C2327eB> ccz;
    /* renamed from: nP */
    private C3428rT<C3689to> f10152nP;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        boolean z = true;
        this.f10151kj = addonPropertie;
        this.ccu = new C0852MQ(this.f10151kj);
        this.ccA = new C4805g();
        this.ccw = new C4804f();
        this.ccx = new C4803e();
        this.ccy = new C4802d();
        this.bLC = new C4801c();
        this.ccz = new C4800b();
        this.ccB = new C4799a();
        this.f10152nP = new C4807i();
        C6245ajJ aVU = this.f10151kj.aVU();
        aVU.mo13965a(C0929Nd.class, this.ccA);
        aVU.mo13965a(aTX.class, this.ccw);
        aVU.mo13965a(C3949xF.class, this.ccx);
        aVU.mo13965a(C1274Sq.class, this.ccy);
        aVU.mo13965a(HudVisibilityAddon.C4534d.class, this.bLC);
        aVU.mo13965a(C2327eB.class, this.ccz);
        aVU.mo13965a(C1532WW.class, this.ccB);
        aVU.mo13965a(C3689to.class, this.f10152nP);
        if (!m44841iL()) {
            this.ccv = !aMU.CREATED_PLAYER.equals(this.f10151kj.getPlayer().cXm());
            if (!this.ccv) {
                this.ccC = new C4806h();
                this.f10151kj.aVU().mo13965a(C3131oI.class, this.ccC);
            }
        } else {
            this.ccv = true;
        }
        for (aTX k : this.f10151kj.aVU().mo13967b(aTX.class)) {
            this.ccw.updateInfo(k);
        }
        if (this.f10151kj.getPlayer().bQB()) {
            this.ccu.setVisible(this.ccv);
            return;
        }
        C0852MQ mq = this.ccu;
        if (!this.ccv || this.f10151kj.alb().anX()) {
            z = false;
        }
        mq.setVisible(z);
    }

    /* renamed from: iL */
    private boolean m44841iL() {
        return (this.f10151kj == null || this.f10151kj.ala() == null || !this.f10151kj.ala().aLS().mo22273iL()) ? false : true;
    }

    public void stop() {
        C6245ajJ aVU = this.f10151kj.aVU();
        if (this.ccC != null) {
            aVU.mo13964a(this.ccC);
            this.ccC = null;
        }
        aVU.mo13964a(this.ccA);
        aVU.mo13964a(this.ccw);
        aVU.mo13964a(this.ccx);
        aVU.mo13964a(this.ccy);
        aVU.mo13964a(this.bLC);
        aVU.mo13964a(this.ccz);
        aVU.mo13964a(this.ccB);
        aVU.mo13964a(this.f10152nP);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44832a(aTX atx) {
        this.ccu.mo3936a(atx);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44836b(aTX atx) {
        this.ccu.mo3937a(atx.getAction());
    }

    /* renamed from: c */
    public AbstractButton mo24394c(aTX atx) {
        return this.ccu.mo3940c(atx);
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$g */
    /* compiled from: a */
    class C4805g implements aVH<C0929Nd> {
        C4805g() {
        }

        /* renamed from: a */
        public void mo2004k(String str, C0929Nd nd) {
            OniNetAddon.this.ccu.mo3939b(nd);
        }

        /* renamed from: b */
        public void mo2003f(String str, C0929Nd nd) {
            OniNetAddon.this.ccu.mo3941c(nd);
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$f */
    /* compiled from: a */
    class C4804f extends C3428rT<aTX> {
        C4804f() {
        }

        /* renamed from: d */
        public void mo321b(aTX atx) {
            OniNetAddon.this.m44832a(atx);
        }

        /* renamed from: a */
        public void mo2003f(String str, aTX atx) {
            OniNetAddon.this.m44836b(atx);
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$e */
    /* compiled from: a */
    class C4803e extends C3428rT<C3949xF> {
        C4803e() {
        }

        /* renamed from: a */
        public void mo321b(C3949xF xFVar) {
            OniNetAddon.this.ccu.setVisible(OniNetAddon.this.ccv && !xFVar.anX());
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$d */
    /* compiled from: a */
    class C4802d extends C3428rT<C1274Sq> {
        private static /* synthetic */ int[] hxI;

        C4802d() {
        }

        static /* synthetic */ int[] cRQ() {
            int[] iArr = hxI;
            if (iArr == null) {
                iArr = new int[C1274Sq.C1275a.values().length];
                try {
                    iArr[C1274Sq.C1275a.DOCK.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C1274Sq.C1275a.ENTER_GATE.ordinal()] = 3;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C1274Sq.C1275a.EXIT_GATE.ordinal()] = 4;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C1274Sq.C1275a.UNDOCK.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                hxI = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo321b(C1274Sq sq) {
            boolean z = false;
            switch (cRQ()[sq.fMR.ordinal()]) {
                case 1:
                    OniNetAddon.this.ccu.setVisible(OniNetAddon.this.ccv);
                    return;
                case 2:
                case 4:
                    C0852MQ a = OniNetAddon.this.ccu;
                    if (OniNetAddon.this.ccv && !OniNetAddon.this.f10151kj.alb().anX()) {
                        z = true;
                    }
                    a.setVisible(z);
                    return;
                case 3:
                    OniNetAddon.this.ccu.setVisible(false);
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$c */
    /* compiled from: a */
    class C4801c extends C3428rT<HudVisibilityAddon.C4534d> {
        C4801c() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            OniNetAddon.this.ccu.setVisible(OniNetAddon.this.ccv && dVar.isVisible());
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$b */
    /* compiled from: a */
    class C4800b extends C3428rT<C2327eB> {
        C4800b() {
        }

        /* renamed from: a */
        public void mo321b(C2327eB eBVar) {
            OniNetAddon.this.ccu.setVisible(eBVar.isVisible());
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$a */
    class C4799a extends C3428rT<C1532WW> {
        C4799a() {
        }

        /* renamed from: b */
        public void mo321b(C1532WW ww) {
            OniNetAddon.this.ccu.ayJ();
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$i */
    /* compiled from: a */
    class C4807i extends C3428rT<C3689to> {
        C4807i() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (toVar.adE() == C3689to.C3690a.SHIP_LAUNCH) {
                OniNetAddon.this.ccu.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetAddon$h */
    /* compiled from: a */
    class C4806h extends C3428rT<C3131oI> {
        C4806h() {
        }

        /* renamed from: b */
        public void mo321b(C3131oI oIVar) {
            boolean z = true;
            boolean equals = C3131oI.C3132a.SHIP_INSTALLED.equals(oIVar.mo20956Us());
            if (C3131oI.C3132a.ONI_CHECKLIST_PERSONAL_MENU.equals(oIVar.mo20956Us()) || equals) {
                OniNetAddon.this.ccv = true;
                if (OniNetAddon.this.f10151kj.getPlayer().bQB()) {
                    OniNetAddon.this.ccu.setVisible(OniNetAddon.this.ccv);
                } else {
                    C0852MQ a = OniNetAddon.this.ccu;
                    if (!OniNetAddon.this.ccv || OniNetAddon.this.f10151kj.alb().anX()) {
                        z = false;
                    }
                    a.setVisible(z);
                }
                if (equals) {
                    OniNetAddon.this.f10151kj.aVU().mo13964a(OniNetAddon.this.ccC);
                }
                OniNetAddon.this.ccC = null;
            }
        }
    }
}
