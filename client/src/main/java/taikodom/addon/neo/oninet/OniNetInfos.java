package taikodom.addon.neo.oninet;

import game.script.player.Player;
import game.script.progression.CharacterEvolution;
import game.script.space.Node;
import logic.IAddonSettings;
import logic.aaa.*;
import logic.baa.C6200aiQ;
import logic.data.link.aEX;
import logic.res.code.C5663aRz;
import logic.ui.Panel;
import logic.ui.item.Progress;
import p001a.C0696Jv;
import p001a.C5956adg;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.neo.sidebar.SideBarManagerAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.neo.oninet")
/* compiled from: a */
public class OniNetInfos implements C2495fo {
    private static final String XML = "oninetinfos.xml";
    /* renamed from: kj */
    public IAddonProperties f10156kj;
    /* renamed from: Bu */
    private C6124ags<C5783aaP> f10153Bu;
    /* renamed from: P */
    private Player f10154P;
    /* renamed from: Ta */
    private Panel f10155Ta;
    private Progress fgO;
    private JLabel fgP;
    private JLabel fgQ;
    private JButton fgR;
    private JButton fgS;
    private C6124ags<C3164oa> fgT;
    private C6124ags<C0880Ml> fgU;
    private C6124ags<C1274Sq> fgV;
    private C0929Nd fgW;
    /* access modifiers changed from: private */
    private C6200aiQ<CharacterEvolution> fgX;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10156kj = addonPropertie;
        this.f10154P = this.f10156kj.getPlayer();
        if (this.f10154P != null) {
            m44868Im();
            m44873iM();
            m44867Fa();
            bQP();
            this.fgW = new C0929Nd(this, this.f10155Ta, 1);
            this.f10156kj.aVU().publish(this.fgW);
        }
    }

    /* renamed from: Fa */
    private void m44867Fa() {
        this.fgT = new C4809b();
        this.fgU = new C4808a();
        this.fgV = new C4812e();
        this.f10153Bu = new C4811d();
        this.f10156kj.aVU().mo13965a(C1274Sq.class, this.fgV);
        this.f10156kj.aVU().mo13965a(C5783aaP.class, this.f10153Bu);
        this.f10156kj.aVU().mo13965a(C3164oa.class, this.fgT);
        this.f10156kj.aVU().mo13965a(C0880Ml.class, this.fgU);
        this.fgR.addActionListener(new C4814g());
        this.fgS.addActionListener(new C4813f());
        this.fgX = new C4810c();
        this.f10154P.mo12638Rp().mo8320a(aEX.ghA, (C6200aiQ<?>) this.fgX);
    }

    private void removeListeners() {
        this.f10156kj.aVU().mo13964a(this.fgT);
        this.f10156kj.aVU().mo13964a(this.fgU);
        this.f10156kj.aVU().mo13964a(this.fgV);
        this.f10156kj.aVU().mo13964a(this.f10153Bu);
    }

    /* renamed from: iM */
    private void m44873iM() {
        this.fgP = this.f10155Ta.mo4917cf("location");
        this.fgQ = this.f10155Ta.mo4917cf("balance");
        this.fgO = this.f10155Ta.mo4918cg("merit-bar");
        this.fgR = this.f10155Ta.mo4913cb("toggle-mode");
        this.fgS = this.f10155Ta.mo4913cb("toggle-help");
    }

    /* renamed from: Im */
    private void m44868Im() {
        this.f10155Ta = (Panel) this.f10156kj.bHv().mo16794bQ(XML);
        this.f10155Ta.setSize(this.f10155Ta.getPreferredSize());
        this.f10155Ta.validate();
    }

    private void bQP() {
        bQS();
        bQR();
        bQQ();
    }

    /* access modifiers changed from: private */
    public void bQQ() {
        long rS = this.f10156kj.ala().aJk().cqL().mo14477rS(this.f10156kj.getPlayer().mo12658lt() - 1);
        long rS2 = this.f10156kj.ala().aJk().cqL().mo14477rS(this.f10156kj.getPlayer().mo12658lt()) - rS;
        long en = this.f10156kj.getPlayer().mo12655en() - rS;
        this.fgO.setMinimum(0);
        this.fgO.mo17398E((float) rS2);
        this.fgO.setValue((float) en);
        this.fgO.setToolTipText(String.valueOf(en) + " / " + rS2);
    }

    /* access modifiers changed from: private */
    public void bQR() {
        this.fgQ.setText(C5956adg.format("T$ {0}", Long.valueOf(this.f10154P.mo5156Qw().bSs())));
    }

    /* access modifiers changed from: private */
    public void bQS() {
        Node azW = this.f10154P.bhE().azW();
        this.fgP.setText(String.valueOf(azW.mo21665ke().get()) + " - " + azW.mo21606Nc().mo3250ke().get());
    }

    public void stop() {
        this.f10156kj.aVU().mo13974g(this.fgW);
        removeListeners();
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetInfos$b */
    /* compiled from: a */
    class C4809b extends C6124ags<C3164oa> {
        C4809b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3164oa oaVar) {
            OniNetInfos.this.bQR();
            return false;
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetInfos$a */
    class C4808a extends C6124ags<C0880Ml> {
        C4808a() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0880Ml ml) {
            OniNetInfos.this.bQR();
            return false;
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetInfos$e */
    /* compiled from: a */
    class C4812e extends C6124ags<C1274Sq> {
        C4812e() {
        }

        /* renamed from: b */
        public boolean updateInfo(C1274Sq sq) {
            if (sq.fMR != C1274Sq.C1275a.EXIT_GATE) {
                return false;
            }
            OniNetInfos.this.bQS();
            return false;
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetInfos$d */
    /* compiled from: a */
    class C4811d extends C6124ags<C5783aaP> {
        C4811d() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            if (aap.bMO() != C5783aaP.C1841a.FLYING && aap.bMO() != C5783aaP.C1841a.DOCKED) {
                return false;
            }
            OniNetInfos.this.bQS();
            return false;
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetInfos$g */
    /* compiled from: a */
    class C4814g implements ActionListener {
        C4814g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            OniNetInfos.this.f10156kj.aVU().mo13975h(new C0696Jv(3, !((SideBarManagerAddon) OniNetInfos.this.f10156kj.mo11830U(SideBarManagerAddon.class)).mo24414nD(3)));
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetInfos$f */
    /* compiled from: a */
    class C4813f implements ActionListener {
        C4813f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            OniNetInfos.this.f10156kj.aVU().mo13975h(new aNS());
        }
    }

    /* renamed from: taikodom.addon.neo.oninet.OniNetInfos$c */
    /* compiled from: a */
    class C4810c implements C6200aiQ<CharacterEvolution> {
        C4810c() {
        }

        /* renamed from: a */
        public void mo1143a(CharacterEvolution eqVar, C5663aRz arz, Object obj) {
            OniNetInfos.this.bQQ();
        }
    }
}
