package taikodom.addon.neo.oninet;

import game.network.message.externalizable.aTX;
import logic.aaa.C0929Nd;
import logic.swing.C0454GJ;
import logic.swing.C3940wz;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import logic.ui.item.Repeater;
import p001a.C6622aqW;
import p001a.aMU;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.*;

@TaikodomAddon("taikodom.addon.neo.oninet")
/* renamed from: a.MQ */
/* compiled from: a */
public class C0852MQ {
    private static final String clI = "button-caption";
    private static final String hOU = "oninet.xml";
    private static final String hOV = "rep-button";
    /* access modifiers changed from: private */

    /* renamed from: SZ */
    public aDX f1102SZ;
    /* access modifiers changed from: private */
    public Map<AbstractButton, Action> clL = new HashMap();
    /* renamed from: kj */
    public IAddonProperties f1104kj;
    /* renamed from: Ta */
    private Panel f1103Ta;
    private Repeater.C3671a<aTX> buV;
    private Repeater<aTX> clJ;
    private List<aTX> clK;
    private C0454GJ clN;
    private boolean dirty;
    /* access modifiers changed from: private */
    private ArrayList<C0929Nd> hOW = new ArrayList<>();

    public C0852MQ(IAddonProperties vWVar) {
        this.f1104kj = vWVar;
        this.f1103Ta = (Panel) this.f1104kj.bHv().mo16794bQ(hOU);
        this.buV = new C0853a();
        this.clN = new C0856c();
        this.clK = new ArrayList();
        this.clJ = this.f1103Ta.mo4919ch(hOV);
        this.clJ.mo22250a(this.buV);
        cZU();
    }

    /* renamed from: b */
    public void mo3939b(C0929Nd nd) {
        this.hOW.add(nd);
        cpN();
    }

    /* renamed from: c */
    public void mo3941c(C0929Nd nd) {
        this.hOW.remove(nd);
        cpN();
    }

    private void cpN() {
        int i = 0;
        Component component = (Panel) this.f1103Ta.mo4915cd("buttons");
        for (Component component2 : this.f1103Ta.getComponents()) {
            if (component2 != component) {
                this.f1103Ta.remove(component2);
            }
        }
        Collections.sort(this.hOW);
        Dimension preferredSize = this.f1103Ta.getPreferredSize();
        int screenWidth = this.f1104kj.bHv().getScreenWidth();
        int height = component.getHeight();
        Iterator<C0929Nd> it = this.hOW.iterator();
        int i2 = height;
        while (it.hasNext()) {
            Panel bim = it.next().bim();
            if (bim.isVisible()) {
                this.f1103Ta.add(bim);
                int width = bim.getWidth();
                bim.setLocation((screenWidth / 2) - (width / 2), i2);
                int height2 = bim.getHeight() + i2;
                preferredSize.width = Math.max(preferredSize.width, width);
                preferredSize.height = height2;
                i += 10;
                i2 = height2;
            }
        }
        preferredSize.height += i;
        this.f1103Ta.setPreferredSize(preferredSize);
        cZU();
    }

    private void cZU() {
        Point location = this.f1103Ta.getLocation();
        Dimension preferredSize = this.f1103Ta.getPreferredSize();
        this.f1103Ta.setSize(preferredSize);
        this.f1103Ta.setLocation((this.f1104kj.bHv().getScreenWidth() / 2) - (preferredSize.width / 2), location.y);
        this.f1103Ta.validate();
    }

    /* renamed from: a */
    public void mo3936a(aTX atx) {
        this.clK.add(atx);
        ayI();
    }

    /* renamed from: a */
    public void mo3937a(Action action) {
        Iterator<aTX> it = this.clK.iterator();
        while (true) {
            if (it.hasNext()) {
                if (it.next().getAction() == action) {
                    it.remove();
                    break;
                }
            } else {
                break;
            }
        }
        ayI();
    }

    private void ayI() {
        Collections.sort(this.clK, new C0855b());
        this.clL.clear();
        this.clJ.clear();
        for (aTX G : this.clK) {
            this.clJ.mo22248G(G);
        }
        ayJ();
    }

    public void ayJ() {
        if (this.f1104kj.ala() == null || !this.f1103Ta.isVisible()) {
            this.dirty = true;
            return;
        }
        for (Map.Entry next : this.clL.entrySet()) {
            AbstractButton abstractButton = (AbstractButton) next.getKey();
            Action action = (Action) next.getValue();
            abstractButton.setSelected(Boolean.TRUE.equals(action.getValue("isActive")));
            abstractButton.setVisible(true);
            abstractButton.setEnabled(action.isEnabled());
            abstractButton.setFocusable(false);
        }
        cZU();
        this.f1103Ta.validate();
        this.dirty = false;
    }

    private int cZV() {
        if (m7007iL() || this.f1104kj.getPlayer().cXm().ordinal() > aMU.CREATED_AVATAR.ordinal()) {
            return this.f1103Ta.mo4915cd("buttons").getHeight();
        }
        return this.f1103Ta.getHeight();
    }

    public void setVisible(boolean z) {
        this.f1103Ta.setVisible(true);
        int cZV = cZV();
        int i = this.f1103Ta.getLocation().y;
        int i2 = -cZV;
        if (z) {
            if (this.dirty) {
                ayJ();
            }
            if (this.f1102SZ != null) {
                this.f1102SZ.kill();
                this.f1102SZ = new aDX(this.f1103Ta, "[" + i + ".." + 0 + "] dur 250 strong_out", C2830kk.asP, this.clN, true);
                return;
            }
            this.f1102SZ = new aDX(this.f1103Ta, "[" + i + ".." + 0 + "] dur 500 strong_out", C2830kk.asP, this.clN, true);
        } else if (this.f1102SZ != null) {
            this.f1102SZ.kill();
            this.f1102SZ = new aDX(this.f1103Ta, "[" + i + ".." + i2 + "] dur 250 strong_in", C2830kk.asP, this.clN, true);
        } else {
            this.f1102SZ = new aDX(this.f1103Ta, "[" + i + ".." + i2 + "] dur 500 strong_in", C2830kk.asP, this.clN, true);
        }
    }

    public void destroy() {
        if (this.f1103Ta != null) {
            this.f1103Ta.destroy();
            this.f1103Ta = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: iL */
    public boolean m7007iL() {
        return (this.f1104kj == null || this.f1104kj.ala() == null || !this.f1104kj.ala().aLS().mo22273iL()) ? false : true;
    }

    /* renamed from: c */
    public AbstractButton mo3940c(aTX atx) {
        if (atx == null || !this.clK.contains(atx)) {
            return null;
        }
        for (AbstractButton abstractButton : ((Panel) this.f1103Ta.mo4916ce("buttons")).getComponents()) {
            if (abstractButton instanceof AbstractButton) {
                AbstractButton abstractButton2 = abstractButton;
                if (abstractButton2.getClientProperty("model").equals(atx)) {
                    return abstractButton2;
                }
            }
        }
        return null;
    }

    /* renamed from: a.MQ$a */
    class C0853a implements Repeater.C3671a<aTX> {
        C0853a() {
        }

        /* renamed from: a */
        public void mo843a(aTX atx, Component component) {
            if (component instanceof AbstractButton) {
                AbstractButton abstractButton = (AbstractButton) component;
                Action action = atx.getAction();
                abstractButton.addActionListener(new C0854a(action));
                if (component.getName().equals(C0852MQ.clI)) {
                    abstractButton.setText((String) atx.getAction().getValue("ShortDescription"));
                    C0852MQ.this.clL.put(abstractButton, action);
                    if (!C0852MQ.this.m7007iL()) {
                        C6622aqW action2 = atx.getAction();
                        abstractButton.putClientProperty("command", action2);
                        abstractButton.setToolTipText(action2.crH());
                        C3940wz.m40777a((JComponent) abstractButton, "windowTooltipProvider");
                        abstractButton.putClientProperty("model", atx);
                    }
                }
                IComponentManager e = ComponentManager.getCssHolder(component);
                e.mo13054a(C0852MQ.this.f1104kj.bHv().mo16783a(atx.dzo(), atx.dzn()));
                e.mo13045Vk();
            }
        }

        /* renamed from: a.MQ$a$a */
        class C0854a implements ActionListener {
            private final /* synthetic */ Action dCF;

            C0854a(Action action) {
                this.dCF = action;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                this.dCF.actionPerformed(actionEvent);
            }
        }
    }

    /* renamed from: a.MQ$c */
    /* compiled from: a */
    class C0856c implements C0454GJ {
        C0856c() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C0852MQ.this.f1102SZ = null;
        }
    }

    /* renamed from: a.MQ$b */
    /* compiled from: a */
    class C0855b implements Comparator<aTX> {
        C0855b() {
        }

        /* renamed from: a */
        public int compare(aTX atx, aTX atx2) {
            return atx.getOrder() - atx2.getOrder();
        }
    }
}
