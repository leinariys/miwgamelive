package taikodom.addon.neo.lowerbar;

import game.network.message.externalizable.C2651i;
import game.network.message.externalizable.C2814kV;
import game.network.message.externalizable.C6556apI;
import game.script.ship.Shield;
import game.script.ship.Ship;
import logic.WrapRunnable;
import logic.aaa.C2996mm;
import logic.aaa.C5783aaP;
import logic.ui.C2698il;
import logic.ui.item.Progress;
import p001a.*;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.neo.lowerbar")
/* renamed from: a.ed */
/* compiled from: a */
public class C2379ed {

    /* renamed from: Bp */
    public Progress f7007Bp;
    /* access modifiers changed from: private */
    /* renamed from: Bq */
    public WrapRunnable f7008Bq;
    /* access modifiers changed from: private */
    /* renamed from: kj */
    public IAddonProperties f7014kj;
    /* renamed from: Bo */
    private Progress f7006Bo;
    /* renamed from: Br */
    private C3428rT<C2651i> f7009Br = new C5300aEa(this);
    /* renamed from: Bs */
    private C3428rT<C2814kV> f7010Bs = new aDY(this);
    /* renamed from: Bt */
    private C3428rT<C6556apI> f7011Bt = new C5319aEt(this);
    /* renamed from: Bu */
    private C6124ags<C5783aaP> f7012Bu = new C5320aEu(this);
    /* access modifiers changed from: private */
    /* renamed from: Bv */
    private C6124ags<C2996mm> f7013Bv = new C5318aEs(this);

    public C2379ed(IAddonProperties vWVar, C2698il ilVar) {
        this.f7014kj = vWVar;
        this.f7006Bo = ilVar.mo4918cg("hull");
        this.f7007Bp = ilVar.mo4918cg("shield");
        this.f7014kj.aVU().mo13965a(C2651i.class, this.f7009Br);
        this.f7014kj.aVU().mo13965a(C2814kV.class, this.f7010Bs);
        this.f7014kj.aVU().mo13965a(C6556apI.class, this.f7011Bt);
        this.f7014kj.aVU().mo13965a(C5783aaP.class, this.f7012Bu);
        this.f7014kj.aVU().mo13965a(C2996mm.class, this.f7013Bv);
        Ship bQx = this.f7014kj.ala().getPlayer().bQx();
        if (bQx != null) {
            m29960b(bQx);
        }
    }

    public void destroy() {
        m29965kV();
        this.f7014kj.aVU().mo13964a(this.f7009Br);
        this.f7014kj.aVU().mo13964a(this.f7010Bs);
        this.f7014kj.aVU().mo13964a(this.f7011Bt);
        this.f7014kj.aVU().mo13964a(this.f7012Bu);
        this.f7014kj.aVU().mo13964a(this.f7013Bv);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m29957a(Ship fAVar) {
        if (fAVar != null) {
            this.f7006Bo.setValue(fAVar.mo8287zt().mo19923Tx());
            m29951N(fAVar.mo8288zv().mo19079Tx());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m29960b(Ship fAVar) {
        m29962c(fAVar);
        m29965kV();
        m29964kU();
    }

    /* renamed from: c */
    private void m29962c(Ship fAVar) {
        if (fAVar.mo8288zv().mo19089hh() > 0.0f) {
            m29953a(this.f7007Bp, fAVar.mo8288zv().mo19079Tx(), Math.max(fAVar.mo8288zv().mo19089hh(), fAVar.mo8288zv().mo19078Tv()));
        } else {
            m29953a(this.f7007Bp, 0.0f, 1.0f);
        }
        if (fAVar.mo8287zt().mo19934hh() > 0.0f) {
            m29953a(this.f7006Bo, fAVar.mo8287zt().mo19923Tx(), fAVar.mo8287zt().mo19934hh());
        } else {
            m29953a(this.f7006Bo, 0.0f, 1.0f);
        }
    }

    /* renamed from: a */
    private void m29953a(Progress bnVar, float f, float f2) {
        bnVar.mo17398E(f2);
        bnVar.setValue(f);
    }

    /* renamed from: kU */
    private void m29964kU() {
        Ship bQx = this.f7014kj.ala().getPlayer().bQx();
        if (bQx != null) {
            this.f7008Bq = new C2380a(bQx);
            if (bQx.bae()) {
                this.f7014kj.alf().addTask("shield task", this.f7008Bq, 80);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: kV */
    public void m29965kV() {
        if (this.f7008Bq != null) {
            this.f7008Bq.cancel();
            this.f7008Bq = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: N */
    public void m29951N(float f) {
        int value = this.f7007Bp.getValue();
        this.f7007Bp.setValue(f);
        int value2 = this.f7007Bp.getValue();
        if (value == value2) {
            return;
        }
        if (value2 <= this.f7007Bp.getMinimum()) {
            this.f7014kj.aVU().mo13975h(new C6811auD(C3667tV.SHIELD_EMPTY));
        } else {
            this.f7007Bp.getMaximum();
        }
    }

    /* renamed from: a.ed$a */
    class C2380a extends WrapRunnable {

        /* renamed from: Bn */
        private final /* synthetic */ Ship f7015Bn;
        private long hFx;
        private long hFy;

        C2380a(Ship fAVar) {
            this.f7015Bn = fAVar;
        }

        public void run() {
            if (C2379ed.this.f7014kj.ala() == null) {
                cancel();
                return;
            }
            Shield zv = this.f7015Bn.mo8288zv();
            if (zv.mo19079Tx() <= zv.mo19089hh()) {
                float Tx = zv.mo19079Tx();
                C2379ed.this.m29951N(Tx);
                C2379ed.this.f7007Bp.validate();
                if (Tx <= 0.0f) {
                    long currentTimeMillis = System.currentTimeMillis();
                    this.hFx = this.hFy - currentTimeMillis;
                    if (this.hFx > 2000) {
                        this.hFy = currentTimeMillis;
                    }
                }
            }
        }
    }
}
