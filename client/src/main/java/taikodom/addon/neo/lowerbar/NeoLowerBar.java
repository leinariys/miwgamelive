package taikodom.addon.neo.lowerbar;

import game.network.message.externalizable.*;
import game.script.nls.NLSConsoleAlert;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.ship.ShieldAdapter;
import game.script.ship.Ship;
import game.script.ship.hazardshield.HazardShield;
import game.script.ship.speedBoost.SpeedBoost;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aVH;
import logic.aaa.*;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import logic.ui.item.Picture;
import logic.ui.item.Progress;
import logic.ui.item.TextField;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;
import taikodom.addon.input.CommandTranslatorAddon;

import javax.swing.*;
import java.awt.event.*;

@TaikodomAddon("taikodom.addon.neo.lowerbar")
/* compiled from: a */
public class NeoLowerBar implements C2495fo {
    private static final String bLu = "SPECIAL1_ON";
    private static final String bLv = "SPECIAL1_OFF";
    private static final String bLw = "SHOW_OBJECTS";
    private static /* synthetic */ int[] bLQ = null;
    /* access modifiers changed from: private */
    /* renamed from: DV */
    public Panel f10144DV;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10145P;
    /* access modifiers changed from: private */

    /* renamed from: SZ */
    public aDX f10146SZ;
    /* access modifiers changed from: private */
    public int aYk;
    /* access modifiers changed from: private */
    public JLabel bLB;
    /* access modifiers changed from: private */
    public C0758Kq bLD;
    /* access modifiers changed from: private */
    public TextField bLK;
    /* access modifiers changed from: private */
    public String bLL = "";
    /* access modifiers changed from: private */
    public String bLM = "[0..255] dur 250";
    /* access modifiers changed from: private */
    public String bLN = "[255..0] dur 250";
    /* access modifiers changed from: private */
    public JToggleButton bLx;
    /* access modifiers changed from: private */
    public JToggleButton bLy;
    /* renamed from: kj */
    public IAddonProperties f10147kj;
    private C3428rT<C2783jw> aYn;
    private C3428rT<C1274Sq> aYo;
    private C2379ed bLA;
    private C3428rT<HudVisibilityAddon.C4534d> bLC;
    private C3428rT<aNH> bLE;
    private Picture bLF;
    private Picture bLG;
    private C3428rT<C2922lp> bLH;
    private C6124ags<C3853vo> bLI;
    private Panel bLJ;
    private C4777a bLO;
    private C3428rT<aIP> bLP;
    private JButton bLz;
    private C3428rT<C3949xF> bwS;
    private C3428rT<C5783aaP> bwU;
    private int bwX;
    /* access modifiers changed from: private */
    private int bwY;

    static /* synthetic */ int[] apX() {
        int[] iArr = bLQ;
        if (iArr == null) {
            iArr = new int[C4789k.values().length];
            try {
                iArr[C4789k.COMBAT.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C4789k.NAVIGATION.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            bLQ = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10147kj = addonPropertie;
        this.f10145P = this.f10147kj.getPlayer();
        this.aYk = 0;
        m44793qb();
        m44787iG();
        this.f10147kj.mo2317P(this);
    }

    public void stop() {
        removeListeners();
        this.bLA.destroy();
        this.f10144DV.destroy();
    }

    /* renamed from: qb */
    private void m44793qb() {
        this.f10144DV = (Panel) this.f10147kj.bHv().mo16794bQ("lowerbar.xml");
        this.bLJ = this.f10144DV.mo4915cd("quickchat");
        this.bLK = this.bLJ.mo4915cd("chatfield");
        this.bLK.addActionListener(new C4782d());
        this.bLK.addKeyListener(new C4781c());
        this.bLK.addFocusListener(new C4780b());
        this.bLB = this.f10144DV.mo4915cd("missile-warning");
        this.bLx = this.f10144DV.mo4915cd("cruise-speed");
        this.bLx.addActionListener(new C4787i());
        this.bLy = this.f10144DV.mo4915cd("radar-list");
        this.bLy.addActionListener(new C4788j());
        this.bLz = this.f10144DV.mo4913cb("mode-change");
        this.bLz.addActionListener(new C4785g());
        this.bLA = new C2379ed(this.f10147kj, this.f10144DV.mo4916ce("integrity-info"));
        this.bLD = new C0758Kq(this.f10144DV.mo4919ch("barsRepeater"));
        this.f10144DV.pack();
        this.bwY = this.f10147kj.bHv().getScreenHeight();
        this.bwX = this.bwY - this.f10144DV.getHeight();
        this.f10144DV.setLocation((this.f10147kj.bHv().getScreenWidth() / 2) - (this.f10144DV.getWidth() / 2), this.bwX);
        this.bLF = this.f10144DV.mo4915cd("radarKey");
        this.bLG = this.f10144DV.mo4915cd("cruiseKey");
        apU();
        setVisible(true);
    }

    /* renamed from: iG */
    private void m44787iG() {
        this.bwS = new C4786h();
        this.bwU = new C4783e();
        this.bLC = new C4784f();
        this.aYo = new C4794p();
        this.aYn = new C4791m();
        this.bLH = new C4790l();
        this.bLE = new C4793o();
        this.bLP = new C4792n();
        this.bLO = new C4777a();
        this.bLI = new C4796r();
        C6245ajJ aVU = this.f10147kj.aVU();
        aVU.mo13965a(C2783jw.class, this.aYn);
        aVU.mo13965a(C1274Sq.class, this.aYo);
        aVU.mo13965a(C3949xF.class, this.bwS);
        aVU.mo13965a(C5783aaP.class, this.bwU);
        aVU.mo13965a(HudVisibilityAddon.C4534d.class, this.bLC);
        aVU.mo13965a(C2922lp.class, this.bLH);
        aVU.mo13965a(aNH.class, this.bLE);
        aVU.mo13965a(C3853vo.class, this.bLI);
        aVU.mo13965a(aIP.class, this.bLP);
        aVU.mo13965a(C5873acB.class, this.bLO);
    }

    /* access modifiers changed from: protected */
    public void apU() {
        this.bLF.setText(((CommandTranslatorAddon) this.f10147kj.mo11830U(CommandTranslatorAddon.class)).mo24071af(bLw));
        this.bLG.setText(((CommandTranslatorAddon) this.f10147kj.mo11830U(CommandTranslatorAddon.class)).mo24071af("SPECIAL1"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ya */
    public void mo24366Ya() {
        this.f10147kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_PURSUIT_LOOP, true));
        this.bLB.setVisible(false);
        this.bLL = "";
    }

    /* access modifiers changed from: private */
    public void apV() {
        new aDX(this.bLB, this.bLL, C2830kk.asS, new C4795q());
    }

    /* access modifiers changed from: private */
    /* renamed from: XZ */
    public void m44768XZ() {
        this.bLB.setText(((NLSConsoleAlert) this.f10147kj.ala().aIY().mo6310c(NLSManager.C1472a.CONSOLEALERT)).bcs().get());
        this.bLB.setVisible(true);
        this.bLL = this.bLN;
        apV();
        this.f10147kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_PURSUIT_LOOP));
    }

    private void removeListeners() {
        C6245ajJ aVU = this.f10147kj.aVU();
        aVU.mo13968b(C2783jw.class, this.aYn);
        aVU.mo13968b(C1274Sq.class, this.aYo);
        aVU.mo13968b(C3949xF.class, this.bwS);
        aVU.mo13968b(C5783aaP.class, this.bwU);
        aVU.mo13968b(HudVisibilityAddon.C4534d.class, this.bLC);
        aVU.mo13968b(C2922lp.class, this.bLH);
        aVU.mo13968b(aNH.class, this.bLE);
        aVU.mo13968b(C3853vo.class, this.bLI);
        aVU.mo13964a(this.bLO);
    }

    public void setVisible(boolean z) {
        if (!z || this.f10145P.bQB()) {
            if (this.f10146SZ != null) {
                this.f10146SZ.kill();
            }
            this.f10146SZ = new aDX(this.f10144DV, "[" + this.f10144DV.getY() + ".." + this.bwY + "] dur 500 strong_in;", C2830kk.asP, new C4797s());
            return;
        }
        if (this.f10147kj.alb().anX()) {
            m44770a(C4789k.COMBAT);
        } else {
            m44770a(C4789k.NAVIGATION);
        }
        this.f10144DV.setVisible(true);
        if (this.f10146SZ != null) {
            this.f10146SZ.kill();
        }
        this.f10146SZ = new aDX(this.f10144DV, "[" + this.bwY + ".." + this.bwX + "] dur 500 strong_out", C2830kk.asP, new C4798t());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44770a(C4789k kVar) {
        switch (apX()[kVar.ordinal()]) {
            case 1:
                this.bLz.setText(this.f10147kj.translate("Activate Combat Mode"));
                ComponentManager.getCssHolder(this.bLz).setAttribute("class", "navigation");
                ComponentManager.getCssHolder(this.bLz).mo13045Vk();
                this.bLz.setEnabled(true);
                return;
            case 2:
                this.bLz.setText(this.f10147kj.translate("Combat Mode Active"));
                ComponentManager.getCssHolder(this.bLz).setAttribute("class", "combat");
                ComponentManager.getCssHolder(this.bLz).mo13045Vk();
                this.bLz.setEnabled(false);
                return;
            default:
                return;
        }
    }

    /* renamed from: bH */
    public void mo24369bH(boolean z) {
        this.bLx.setEnabled(z);
    }

    public int getHeight() {
        return this.f10144DV.getHeight();
    }

    @C2602hR(mo19255zf = "REQUEST_CHAT")
    /* renamed from: bI */
    public void mo24370bI(boolean z) {
        if (z && this.f10145P.dxc().anX()) {
            m44778bJ(true);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: cP */
    public void m44780cP(String str) {
        this.f10147kj.aVU().mo13975h(new C5950ada(str));
    }

    /* access modifiers changed from: private */
    /* renamed from: bJ */
    public void m44778bJ(boolean z) {
        this.f10145P.dxc().mo22059aR(false);
        this.bLJ.setVisible(z);
        this.bLK.setText("");
        if (z) {
            this.bLK.requestFocus();
            this.bLJ.setSize(this.bLJ.getPreferredSize());
            return;
        }
        this.bLJ.setSize(0, 0);
    }

    public C0758Kq apW() {
        return this.bLD;
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$k */
    /* compiled from: a */
    private enum C4789k {
        NAVIGATION,
        COMBAT
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$d */
    /* compiled from: a */
    class C4782d implements ActionListener {
        C4782d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NeoLowerBar.this.m44780cP(NeoLowerBar.this.bLK.getText());
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$c */
    /* compiled from: a */
    class C4781c extends KeyAdapter {
        C4781c() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 27) {
                NeoLowerBar.this.m44778bJ(false);
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$b */
    /* compiled from: a */
    class C4780b extends FocusAdapter {
        C4780b() {
        }

        public void focusLost(FocusEvent focusEvent) {
            NeoLowerBar.this.m44778bJ(false);
            NeoLowerBar.super.focusLost(focusEvent);
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$i */
    /* compiled from: a */
    class C4787i implements ActionListener {
        C4787i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Ship bQx = NeoLowerBar.this.f10145P.bQx();
            if (bQx != null && !NeoLowerBar.this.f10145P.bQB()) {
                if (bQx.ahh()) {
                    NeoLowerBar.this.bLx.setSelected(true);
                } else if (bQx.ahd()) {
                    NeoLowerBar.this.bLx.setSelected(false);
                } else if (NeoLowerBar.this.bLx.isSelected()) {
                    NeoLowerBar.this.f10147kj.aVU().mo13972d(NeoLowerBar.bLu, new C5667aSd(NeoLowerBar.bLu, true));
                } else {
                    NeoLowerBar.this.f10147kj.aVU().mo13972d(NeoLowerBar.bLv, new C5667aSd(NeoLowerBar.bLv, true));
                }
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$j */
    /* compiled from: a */
    class C4788j implements ActionListener {
        C4788j() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (NeoLowerBar.this.f10145P.bQx() != null && !NeoLowerBar.this.f10145P.bQB()) {
                if (NeoLowerBar.this.bLy.isSelected()) {
                    NeoLowerBar.this.f10147kj.aVU().mo13972d(NeoLowerBar.bLw, new C5667aSd(NeoLowerBar.bLw, true));
                } else {
                    NeoLowerBar.this.f10147kj.aVU().mo13972d(NeoLowerBar.bLw, new C5667aSd(NeoLowerBar.bLw, false));
                }
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$g */
    /* compiled from: a */
    class C4785g implements ActionListener {
        C4785g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NeoLowerBar.this.f10147kj.alb().cOW();
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$h */
    /* compiled from: a */
    class C4786h extends C3428rT<C3949xF> {
        C4786h() {
        }

        /* renamed from: a */
        public void mo321b(C3949xF xFVar) {
            C4789k kVar;
            NeoLowerBar neoLowerBar = NeoLowerBar.this;
            if (xFVar.anX()) {
                kVar = C4789k.COMBAT;
            } else {
                kVar = C4789k.NAVIGATION;
            }
            neoLowerBar.m44770a(kVar);
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$e */
    /* compiled from: a */
    class C4783e extends C3428rT<C5783aaP> {

        /* renamed from: Zn */
        private static /* synthetic */ int[] f10150Zn;

        C4783e() {
        }

        /* renamed from: BK */
        static /* synthetic */ int[] m44809BK() {
            int[] iArr = f10150Zn;
            if (iArr == null) {
                iArr = new int[C5783aaP.C1841a.values().length];
                try {
                    iArr[C5783aaP.C1841a.DOCKED.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5783aaP.C1841a.FLYING.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5783aaP.C1841a.JUMPING.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5783aaP.C1841a.ONI.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                f10150Zn = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            switch (m44809BK()[aap.bMO().ordinal()]) {
                case 1:
                case 3:
                    NeoLowerBar.this.setVisible(false);
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$f */
    /* compiled from: a */
    class C4784f extends C3428rT<HudVisibilityAddon.C4534d> {
        C4784f() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            NeoLowerBar.this.setVisible(dVar.isVisible());
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$p */
    /* compiled from: a */
    class C4794p extends C3428rT<C1274Sq> {
        C4794p() {
        }

        /* renamed from: a */
        public void mo321b(C1274Sq sq) {
            if (sq.fMR == C1274Sq.C1275a.DOCK || sq.fMR == C1274Sq.C1275a.ENTER_GATE) {
                NeoLowerBar.this.aYk = 0;
                NeoLowerBar.this.mo24366Ya();
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$m */
    /* compiled from: a */
    class C4791m extends C3428rT<C2783jw> {
        C4791m() {
        }

        /* renamed from: a */
        public void mo321b(C2783jw jwVar) {
            if (jwVar.fXd == C2783jw.C2784a.SPAWNED) {
                NeoLowerBar neoLowerBar = NeoLowerBar.this;
                neoLowerBar.aYk = neoLowerBar.aYk + 1;
                if (!NeoLowerBar.this.bLB.isVisible()) {
                    NeoLowerBar.this.m44768XZ();
                }
            } else if (jwVar.fXd == C2783jw.C2784a.UNSPAWNED && NeoLowerBar.this.aYk > 0) {
                NeoLowerBar neoLowerBar2 = NeoLowerBar.this;
                neoLowerBar2.aYk = neoLowerBar2.aYk - 1;
                if (NeoLowerBar.this.aYk <= 0) {
                    NeoLowerBar.this.mo24366Ya();
                }
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$l */
    /* compiled from: a */
    class C4790l extends C3428rT<C2922lp> {
        C4790l() {
        }

        /* renamed from: a */
        public void mo321b(C2922lp lpVar) {
            NeoLowerBar.this.apU();
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$o */
    /* compiled from: a */
    class C4793o extends C3428rT<aNH> {
        C4793o() {
        }

        /* renamed from: a */
        public void mo321b(aNH anh) {
            NeoLowerBar.this.bLy.setSelected(anh.isVisible());
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$n */
    /* compiled from: a */
    class C4792n extends C3428rT<aIP> {
        C4792n() {
        }

        /* renamed from: a */
        public void mo321b(aIP aip) {
            if (C6809auB.C1996a.WARMUP.equals(aip.acZ())) {
                NeoLowerBar.this.bLD.mo3679a(new C0122BY(true, (int) (NeoLowerBar.this.f10145P.bQx().afL().anr() * 1000.0f), NeoLowerBar.this.f10147kj.mo11834a((Class<?>) NeoLowerBar.class, "Warming up", new Object[0]), NeoLowerBar.this));
                NeoLowerBar.this.bLx.setSelected(true);
                NeoLowerBar.this.mo24369bH(false);
            } else if (C6809auB.C1996a.COOLDOWN.equals(aip.acZ())) {
                NeoLowerBar.this.bLD.mo3679a(new C0122BY(false, (int) (NeoLowerBar.this.f10145P.bQx().afL().ant() * 1000.0f), NeoLowerBar.this.f10147kj.mo11834a((Class<?>) NeoLowerBar.class, "Cooling down", new Object[0]), NeoLowerBar.this));
                NeoLowerBar.this.bLx.setSelected(false);
                NeoLowerBar.this.mo24369bH(false);
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$r */
    /* compiled from: a */
    class C4796r extends C6124ags<C3853vo> {
        private boolean gZb = false;

        C4796r() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3853vo voVar) {
            boolean z;
            ShieldAdapter ase = NeoLowerBar.this.f10147kj.getPlayer().bQx().mo8288zv().mo5353TJ().ase();
            while (true) {
                if (ase == null) {
                    z = false;
                    break;
                } else if (ase instanceof HazardShield.ShieldBlock) {
                    z = true;
                    break;
                } else {
                    ase = (ShieldAdapter) ase.cwL();
                }
            }
            if (z) {
                NeoLowerBar.this.f10147kj.aVU().mo13975h(new C6811auD(C3667tV.SPECIAL_HAZARD_LOOP));
                this.gZb = true;
            } else {
                if (this.gZb) {
                    NeoLowerBar.this.f10147kj.aVU().mo13975h(new C6811auD(C3667tV.SPECIAL_HAZARD_LOOP, true));
                }
                this.gZb = false;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$q */
    /* compiled from: a */
    class C4795q implements C0454GJ {
        C4795q() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            if (NeoLowerBar.this.bLL.equals(NeoLowerBar.this.bLN)) {
                NeoLowerBar.this.bLL = NeoLowerBar.this.bLM;
            } else if (NeoLowerBar.this.bLL.equals(NeoLowerBar.this.bLM)) {
                NeoLowerBar.this.bLL = NeoLowerBar.this.bLN;
            }
            if (!NeoLowerBar.this.bLL.equals("")) {
                NeoLowerBar.this.apV();
            }
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$t */
    /* compiled from: a */
    class C4798t implements C0454GJ {
        C4798t() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            NeoLowerBar.this.f10146SZ = null;
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$s */
    /* compiled from: a */
    class C4797s implements C0454GJ {
        C4797s() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            NeoLowerBar.this.f10146SZ = null;
            NeoLowerBar.this.f10144DV.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$a */
    public class C4777a extends WrapRunnable implements aVH<C5873acB> {
        /* access modifiers changed from: private */
        public SpeedBoost iow;
        /* access modifiers changed from: private */
        public C4778a iox;
        private long eAl;
        private boolean iot;
        private boolean iou;
        private long iov;
        private float time;

        public C4777a() {
        }

        /* renamed from: a */
        public void mo2003f(String str, C5873acB acb) {
        }

        /* renamed from: b */
        public void mo2004k(String str, C5873acB acb) {
            if (this.iox == null) {
                this.iox = new C4778a();
            }
            this.iow = acb.bPc();
            if (acb.bPb() == SpeedBoost.C2286b.COOLDOWN) {
                if (this.iox.f10149Vd != null) {
                    ComponentManager.getCssHolder(this.iox.f10149Vd).setAttribute("class", "hull");
                }
                this.iot = true;
                this.iox.setValue(1.0f);
                NeoLowerBar.this.f10147kj.aVU().mo13975h(new C6811auD(C3667tV.SPECIAL_BOOST_EMPTY));
                this.eAl = System.currentTimeMillis();
            } else {
                if (this.iox.f10149Vd != null) {
                    ComponentManager.getCssHolder(this.iox.f10149Vd).setAttribute("class", "shield");
                }
                if (acb.bPb() == SpeedBoost.C2286b.CONSUMING) {
                    this.iou = true;
                    this.time = this.iow.deW();
                    this.iov = System.currentTimeMillis();
                } else {
                    this.iou = false;
                    this.time = this.iow.deW();
                    this.iov = System.currentTimeMillis();
                }
                if (this.iot) {
                    this.iot = false;
                }
            }
            NeoLowerBar.this.f10147kj.alf().addTask("superSpeed", this, 30);
            this.iox.dmT();
        }

        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            float f = ((float) (currentTimeMillis - this.iov)) / 1000.0f;
            try {
                if (!this.iot) {
                    this.iox.setValue(this.iow.deY());
                    if (!this.iou) {
                        float max = Math.max(this.time + f, 0.0f);
                        if (this.iow.deW() >= this.iow.deO().mo16301jt() && max >= this.iow.mo17891jt()) {
                            this.iox.dmU();
                            NeoLowerBar.this.f10147kj.aVU().mo13975h(new C6811auD(C3667tV.SPECIAL_BOOST_FULL));
                            cancel();
                            return;
                        }
                        return;
                    }
                    return;
                }
                long j = currentTimeMillis - this.eAl;
                long ant = ((long) this.iow.ant()) * 1000;
                this.iox.setValue(Math.max(0.0f, ((float) (ant - j)) / ((float) ant)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$a$a */
        public class C4778a extends C0758Kq.C0759a {

            /* renamed from: DV */
            Panel f10148DV;

            /* renamed from: Vd */
            Progress f10149Vd;

            public C4778a() {
                super(NeoLowerBar.this.f10147kj.translate("Afterburner"));
            }

            /* renamed from: b */
            public void mo749b(Panel aco) {
                if (this.f10148DV != null) {
                    this.f10148DV.setVisible(false);
                    this.f10148DV.destroy();
                    this.f10148DV = null;
                }
                this.f10148DV = aco;
                this.f10149Vd = this.f10148DV.mo4918cg("progress");
                ComponentManager.getCssHolder(this.f10149Vd).setAttribute("class", "shield");
                this.f10148DV.setAlpha(0);
                this.f10148DV.setVisible(true);
                new aDX(this.f10148DV, "[0..255] dur 250", C2830kk.asS, (C0454GJ) null);
                if (C4777a.this.iow != null) {
                    setValue(C4777a.this.iow.deY());
                }
            }

            /* access modifiers changed from: package-private */
            public void dmT() {
                if (this.f10148DV == null) {
                    NeoLowerBar.this.bLD.mo3679a(this);
                }
            }

            /* access modifiers changed from: package-private */
            public void dmU() {
                if (this.f10148DV != null) {
                    new aDX(this.f10148DV, "[255..0] dur 250", C2830kk.asS, new C4779a());
                    C4777a.this.iox = null;
                }
            }

            /* access modifiers changed from: package-private */
            public void setValue(float f) {
                if (this.f10149Vd != null) {
                    this.f10149Vd.setValue(100.0f * f);
                }
            }

            /* renamed from: taikodom.addon.neo.lowerbar.NeoLowerBar$a$a$a */
            class C4779a implements C0454GJ {
                C4779a() {
                }

                /* renamed from: a */
                public void mo9a(JComponent jComponent) {
                    NeoLowerBar.this.bLD.mo3680b(C4778a.this);
                }
            }
        }
    }
}
