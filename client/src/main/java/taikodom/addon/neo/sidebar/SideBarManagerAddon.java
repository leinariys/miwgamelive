package taikodom.addon.neo.sidebar;

import game.network.message.externalizable.C3131oI;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.C1274Sq;
import logic.aaa.C2327eB;
import logic.aaa.C3949xF;
import logic.swing.C0454GJ;
import logic.swing.C2740jL;
import logic.ui.item.C2830kk;
import logic.ui.item.C2877lL;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;

@TaikodomAddon("taikodom.addon.neo.sidebar")
/* compiled from: a */
public class SideBarManagerAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: kj */
    public static IAddonProperties f10158kj;
    /* access modifiers changed from: private */
    public boolean eoR = false;
    private C3428rT<aSZ> eoI;
    private C3428rT<C0269DS> eoJ;
    private C3428rT<C0696Jv> eoK;
    private C2925ls eoL;
    private C2925ls eoM;
    private C2877lL eoN;
    private C2877lL eoO;
    private C2877lL eoP;
    private C2877lL eoQ;
    private boolean eoS;

    /* renamed from: iL */
    public static boolean m44918iL() {
        return f10158kj != null && f10158kj.ala().aLS().mo22273iL();
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        f10158kj = addonPropertie;
        this.eoS = f10158kj.getPlayer().bQB();
        bxR();
        m44904Fa();
        for (aSZ k : f10158kj.aVU().mo13967b(aSZ.class)) {
            this.eoI.updateInfo(k);
        }
        bxT();
    }

    private void bxR() {
        bxS();
        this.eoL = new C2925ls(f10158kj, true, this.eoN, this.eoO);
        this.eoM = new C2925ls(f10158kj, false, this.eoP, this.eoQ);
    }

    private void bxS() {
        this.eoN = new C4818a();
        this.eoO = new C4819b();
        this.eoP = new C4826h();
        this.eoQ = new C4827i();
    }

    /* renamed from: Fa */
    private void m44904Fa() {
        C6245ajJ aVU = f10158kj.aVU();
        this.eoI = new C4824f();
        this.eoK = new C4825g();
        this.eoJ = new C4822d();
        aVU.mo13965a(aSZ.class, this.eoI);
        aVU.mo13965a(C0696Jv.class, this.eoK);
        aVU.mo13965a(C0269DS.class, this.eoJ);
        aVU.mo13965a(C3949xF.class, new C4823e());
        aVU.mo13965a(C1274Sq.class, new C4821c());
        aVU.mo13965a(C2327eB.class, new C4829j());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44905a(C1274Sq.C1275a aVar) {
        boolean z = false;
        boolean z2 = aVar == C1274Sq.C1275a.DOCK || aVar == C1274Sq.C1275a.UNDOCK || aVar == C1274Sq.C1275a.EXIT_GATE;
        m44916d(3, z2);
        if (z2) {
            if (aVar == C1274Sq.C1275a.UNDOCK || (aVar == C1274Sq.C1275a.EXIT_GATE && this.eoR)) {
                z = true;
            }
            m44917e(3, z);
        }
    }

    public void stop() {
        this.eoL.clear();
        this.eoM.clear();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44912b(aSZ asz) {
        if (asz != null) {
            if (asz.aMP() == 1) {
                this.eoL.mo20415a(asz.mo11389qd(), asz.aMO(), asz.getOrder());
            } else {
                this.eoM.mo20415a(asz.mo11389qd(), asz.aMO(), asz.getOrder());
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m44915c(aSZ asz) {
        if (asz != null) {
            if (asz.aMP() == 1) {
                this.eoL.mo20416a(asz.aMO());
            }
            if (asz.aMP() == 2) {
                this.eoM.mo20416a(asz.aMO());
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44911b(C0269DS ds) {
        if (ds.aMP() == 1) {
            this.eoL.mo20417a(ds.aMO(), ds.isVisible());
        }
        if (ds.aMP() == 2) {
            this.eoM.mo20417a(ds.aMO(), ds.isVisible());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m44916d(int i, boolean z) {
        if ((i & 1) != 0) {
            this.eoL.setVisible(z);
        }
        if ((i & 2) != 0) {
            this.eoM.setVisible(z);
        }
    }

    /* renamed from: nD */
    public boolean mo24414nD(int i) {
        boolean z = i != 0;
        if ((i & 1) != 0) {
            z &= this.eoL.isVisible();
        }
        if ((i & 2) != 0) {
            return z & this.eoM.isVisible();
        }
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m44917e(int i, boolean z) {
        if (this.eoS != z) {
            if ((i & 1) != 0) {
                this.eoL.mo20420m(z);
            }
            if ((i & 2) != 0) {
                this.eoM.mo20420m(z);
            }
            this.eoS = z;
        }
    }

    private void bxT() {
        if (m44918iL()) {
            m44916d(3, true);
        } else if (aMU.CREATED_PLAYER.equals(f10158kj.getPlayer().cXm())) {
            m44916d(3, false);
            f10158kj.aVU().mo13965a(C3131oI.class, new C4830k());
        } else {
            m44916d(3, true);
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$a */
    class C4818a extends C2877lL {
        C4818a() {
        }

        /* renamed from: iT */
        public void mo20216iT() {
            getComponent().show(true);
        }

        /* renamed from: iU */
        public C2740jL<Component> mo20217iU() {
            return C2830kk.asO;
        }

        /* renamed from: iV */
        public String mo20218iV() {
            return "[" + getComponent().getX() + "..0] dur 500 strong_out";
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$b */
    /* compiled from: a */
    class C4819b extends C2877lL {
        C4819b() {
        }

        /* renamed from: iU */
        public C2740jL<Component> mo20217iU() {
            return C2830kk.asO;
        }

        /* renamed from: iV */
        public String mo20218iV() {
            return "[" + getComponent().getX() + "..-" + getComponent().getWidth() + "] dur 500 strong_out";
        }

        /* renamed from: iW */
        public C0454GJ mo20219iW() {
            return new C4820a();
        }

        /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$b$a */
        class C4820a implements C0454GJ {
            C4820a() {
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                C4819b.this.getComponent().show(false);
            }
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$h */
    /* compiled from: a */
    class C4826h extends C2877lL {
        C4826h() {
        }

        /* renamed from: iT */
        public void mo20216iT() {
            getComponent().show(true);
        }

        /* renamed from: iU */
        public C2740jL<Component> mo20217iU() {
            return C2830kk.asO;
        }

        /* renamed from: iV */
        public String mo20218iV() {
            return "[" + SideBarManagerAddon.f10158kj.bHv().getScreenWidth() + ".." + (SideBarManagerAddon.f10158kj.bHv().getScreenWidth() - getComponent().getWidth()) + "] dur 500 strong_out";
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$i */
    /* compiled from: a */
    class C4827i extends C2877lL {
        C4827i() {
        }

        /* renamed from: iU */
        public C2740jL<Component> mo20217iU() {
            return C2830kk.asO;
        }

        /* renamed from: iV */
        public String mo20218iV() {
            return "[" + getComponent().getX() + ".." + (getComponent().getX() + getComponent().getWidth()) + "] dur 500 strong_out";
        }

        /* renamed from: iW */
        public C0454GJ mo20219iW() {
            return new C4828a();
        }

        /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$i$a */
        class C4828a implements C0454GJ {
            C4828a() {
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                C4827i.this.getComponent().show(false);
            }
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$f */
    /* compiled from: a */
    class C4824f extends C3428rT<aSZ> {
        C4824f() {
        }

        /* renamed from: a */
        public void mo321b(aSZ asz) {
            SideBarManagerAddon.this.m44912b(asz);
        }

        /* renamed from: a */
        public void mo2003f(String str, aSZ asz) {
            SideBarManagerAddon.this.m44915c(asz);
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$g */
    /* compiled from: a */
    class C4825g extends C3428rT<C0696Jv> {
        C4825g() {
        }

        /* renamed from: a */
        public void mo321b(C0696Jv jv) {
            SideBarManagerAddon.this.m44916d(jv.aMP(), jv.isVisible());
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$d */
    /* compiled from: a */
    class C4822d extends C3428rT<C0269DS> {
        C4822d() {
        }

        /* renamed from: a */
        public void mo321b(C0269DS ds) {
            SideBarManagerAddon.this.m44911b(ds);
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$e */
    /* compiled from: a */
    class C4823e extends C3428rT<C3949xF> {
        C4823e() {
        }

        /* renamed from: a */
        public void mo321b(C3949xF xFVar) {
            SideBarManagerAddon.this.m44917e(3, xFVar.anX());
            SideBarManagerAddon.this.eoR = xFVar.anX();
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$c */
    /* compiled from: a */
    class C4821c extends C3428rT<C1274Sq> {
        C4821c() {
        }

        /* renamed from: a */
        public void mo321b(C1274Sq sq) {
            if (sq.fMR == C1274Sq.C1275a.ENTER_GATE || sq.fMR == C1274Sq.C1275a.EXIT_GATE) {
                SideBarManagerAddon.this.m44916d(3, sq.fMR == C1274Sq.C1275a.EXIT_GATE);
            } else if (SideBarManagerAddon.m44918iL()) {
                SideBarManagerAddon.this.m44905a(sq.fMR);
            } else if (!aMU.CREATED_PLAYER.equals(SideBarManagerAddon.f10158kj.getPlayer().cXm())) {
                SideBarManagerAddon.this.m44905a(sq.fMR);
            }
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$j */
    /* compiled from: a */
    class C4829j extends C3428rT<C2327eB> {
        C4829j() {
        }

        /* renamed from: a */
        public void mo321b(C2327eB eBVar) {
            SideBarManagerAddon.this.m44916d(3, eBVar.isVisible());
        }
    }

    /* renamed from: taikodom.addon.neo.sidebar.SideBarManagerAddon$k */
    /* compiled from: a */
    class C4830k extends C3428rT<C3131oI> {
        C4830k() {
        }

        /* renamed from: b */
        public void mo321b(C3131oI oIVar) {
            if (C3131oI.C3132a.ONI_CHECKLIST_LEFT_SIDE_MENU.equals(oIVar.mo20956Us())) {
                SideBarManagerAddon.this.m44916d(1, true);
            } else if (C3131oI.C3132a.ONI_CHECKLIST_RIGHT_SIDE_MENU.equals(oIVar.mo20956Us())) {
                SideBarManagerAddon.this.m44916d(2, true);
            }
        }
    }
}
