package taikodom.addon.credits;

import game.network.message.externalizable.C3689to;
import logic.IAddonSettings;
import logic.aaa.C5477aKv;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import logic.ui.item.EditorPane;
import logic.ui.item.InternalFrame;
import p001a.C3428rT;
import p001a.C5673aSj;
import taikodom.addon.C2495fo;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

@TaikodomAddon("taikodom.addon.credits")
/* compiled from: a */
public class CreditsAddon implements C2495fo {

    /* renamed from: SW */
    private static final String f9853SW = "credits.xml";
    /* renamed from: SY */
    public EditorPane f9855SY;
    /* access modifiers changed from: private */
    /* renamed from: Ta */
    public Panel f9857Ta;
    /* renamed from: kj */
    public IAddonSettings f9858kj;
    /* access modifiers changed from: private */
    /* renamed from: SX */
    private C3428rT<C5477aKv> f9854SX;
    /* access modifiers changed from: private */
    /* renamed from: SZ */
    private aDX f9856SZ;
    /* renamed from: nM */
    private InternalFrame f9859nM;

    /* renamed from: wz */
    private C3428rT<C3689to> f9860wz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9858kj = addonPropertie;
        this.f9854SX = new C4344c();
        this.f9860wz = new C4345d();
        this.f9858kj.aVU().mo13965a(C5477aKv.class, this.f9854SX);
        this.f9858kj.aVU().mo13965a(C3689to.class, this.f9860wz);
    }

    /* access modifiers changed from: private */
    /* renamed from: xK */
    public void m43327xK() {
        if (this.f9859nM != null) {
            close();
            return;
        }
        this.f9859nM = this.f9858kj.bHv().mo16792bN(f9853SW);
        this.f9855SY = this.f9859nM.mo4915cd("credits_text");
        m43328xL();
        this.f9859nM.setPreferredSize(this.f9858kj.bHv().getScreenSize());
        this.f9859nM.setVisible(true);
        this.f9859nM.pack();
        this.f9859nM.center();
    }

    /* renamed from: xL */
    private void m43328xL() {
        this.f9857Ta = this.f9859nM.mo4915cd("credits_container");
        this.f9857Ta.setMaximumSize(new Dimension(this.f9858kj.bHv().getScreenWidth(), Integer.MAX_VALUE));
        C4342a aVar = new C4342a("Init credits panel");
        aVar.setDaemon(true);
        aVar.start();
        this.f9859nM.mo4913cb("close").addActionListener(new C4343b());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23790a(Dimension dimension) {
        int screenHeight = this.f9858kj.bHv().getScreenHeight() - this.f9857Ta.getParent().getInsets().bottom;
        this.f9856SZ = new aDX(this.f9855SY, "[" + screenHeight + ".." + (-dimension.height) + "] dur " + ((dimension.height / 50) * 2000), C2830kk.asP, new C4346e());
    }

    /* access modifiers changed from: private */
    /* renamed from: xM */
    public String m43329xM() {
        return C5673aSj.m18227a("http://www.taikodom.com.br/credits_in_game/", (String) null, new File("data/web-cache/"));
    }

    /* access modifiers changed from: private */
    public void close() {
        if (this.f9859nM != null) {
            if (this.f9856SZ != null) {
                this.f9856SZ.kill();
            }
            this.f9859nM.destroy();
            this.f9859nM = null;
        }
    }

    public void stop() {
        close();
        this.f9858kj.aVU().mo13964a(this.f9854SX);
        this.f9858kj.aVU().mo13964a(this.f9860wz);
    }

    /* renamed from: taikodom.addon.credits.CreditsAddon$c */
    /* compiled from: a */
    class C4344c extends C3428rT<C5477aKv> {
        C4344c() {
        }

        /* renamed from: a */
        public void mo321b(C5477aKv akv) {
            CreditsAddon.this.m43327xK();
        }
    }

    /* renamed from: taikodom.addon.credits.CreditsAddon$d */
    /* compiled from: a */
    class C4345d extends C3428rT<C3689to> {
        C4345d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            CreditsAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.credits.CreditsAddon$a */
    class C4342a extends Thread {
        C4342a(String str) {
            super(str);
        }

        public void run() {
            CreditsAddon.this.f9855SY.mo8862e(CreditsAddon.class.getResource("creditsContent.css"));
            CreditsAddon.this.f9855SY.setText(CreditsAddon.this.m43329xM());
            Dimension preferredSize = CreditsAddon.this.f9855SY.getPreferredSize();
            CreditsAddon.this.f9855SY.setLocation((CreditsAddon.this.f9857Ta.getWidth() - preferredSize.width) / 2, CreditsAddon.this.f9858kj.bHv().getScreenHeight() - CreditsAddon.this.f9857Ta.getParent().getInsets().bottom);
            CreditsAddon.this.f9855SY.setSize(preferredSize);
            CreditsAddon.this.f9855SY.setMinimumSize(preferredSize);
            CreditsAddon.this.f9855SY.setVisible(true);
            CreditsAddon.this.mo23790a(preferredSize);
        }
    }

    /* renamed from: taikodom.addon.credits.CreditsAddon$b */
    /* compiled from: a */
    class C4343b implements ActionListener {
        C4343b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CreditsAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.credits.CreditsAddon$e */
    /* compiled from: a */
    class C4346e implements C0454GJ {
        C4346e() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            CreditsAddon.this.close();
        }
    }
}
