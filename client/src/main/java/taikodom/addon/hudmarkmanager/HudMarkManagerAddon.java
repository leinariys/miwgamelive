package taikodom.addon.hudmarkmanager;

import game.script.Actor;
import game.script.Character;
import game.script.mission.MissionTrigger;
import game.script.npc.NPC;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Gate;
import game.script.space.Loot;
import logic.IAddonSettings;
import logic.aaa.C1506WA;
import logic.aaa.C3030nE;
import p001a.C0308E;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.util.HashMap;

@TaikodomAddon("taikodom.addon.hudmarkmanager")
/* compiled from: a */
public class HudMarkManagerAddon implements C2495fo {
    private HashMap<C0308E, Boolean> ioj = new HashMap<>();

    /* renamed from: kj */
    private IAddonProperties f9956kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9956kj = addonPropertie;
    }

    public void stop() {
    }

    /* renamed from: d */
    public void mo24068d(C0308E e) {
        boolean valueOf;
        Boolean bool = this.ioj.get(e);
        if (bool == null) {
            valueOf = false;
        } else {
            valueOf = Boolean.valueOf(!bool.booleanValue());
        }
        this.ioj.put(e, valueOf);
        this.f9956kj.getPlayer().mo14419f((C1506WA) new C3030nE());
    }

    /* renamed from: e */
    public boolean mo24069e(C0308E e) {
        if (this.ioj.containsKey(e)) {
            return this.ioj.get(e).booleanValue();
        }
        return true;
    }

    /* renamed from: a */
    public C0308E mo24067a(Player aku, Actor cr) {
        if (cr instanceof MissionTrigger) {
            return C0308E.WAYPOINT;
        }
        if (cr instanceof Station) {
            return C0308E.STATION;
        }
        if (cr instanceof Gate) {
            return C0308E.PORTAL;
        }
        if (cr instanceof Ship) {
            Character agj = ((Ship) cr).agj();
            if (agj instanceof NPC) {
                return C0308E.NPC;
            }
            if (agj instanceof Player) {
                return C0308E.PLAYER;
            }
        }
        if (cr instanceof Loot) {
            return C0308E.LOOT;
        }
        return C0308E.NONE;
    }
}
