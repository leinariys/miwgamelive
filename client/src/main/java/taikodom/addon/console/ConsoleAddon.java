package taikodom.addon.console;

import game.script.ai.npc.ScriptableAIController;
import logic.IAddonSettings;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.item.C2830kk;
import logic.ui.item.InternalFrame;
import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.JavaScriptException;
import org.mozilla1.javascript.Scriptable;
import org.mozilla1.javascript.Undefined;
import p001a.C0965OC;
import p001a.C2602hR;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import java.util.Stack;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@TaikodomAddon("taikodom.addon.console")
/* compiled from: a */
public class ConsoleAddon implements C2495fo {

    public final ExecutorService fVr = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public int fNi = -1;
    /* renamed from: TR */
    public Scriptable f9834TR;
    public ThreadLocal<Context> daB = new C0965OC(this);
    public JTextArea consoleOutput;
    public JTextArea consoleInput;
    public LinkedList<String> fNh = new LinkedList<>();
    public IAddonProperties fVq;
    /* renamed from: nM */
    public InternalFrame f9835nM;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.fVq = addonPropertie;
        addonPropertie.mo2317P(this);
        int screenHeight = addonPropertie.bHv().getScreenHeight();
        int screenWidth = addonPropertie.bHv().getScreenWidth();
        this.f9835nM = addonPropertie.bHv().mo16792bN("console.xml");
        this.f9835nM.setBounds(0, 0, screenWidth, screenHeight / 2);
        this.f9835nM.getUI().setNorthPane((JComponent) null);
        this.f9835nM.setVisible(false);
        this.f9835nM.setAlwaysOnTop(true);
        this.consoleOutput = this.f9835nM.mo4915cd("console");
        this.consoleOutput.addKeyListener(new C4304a());
        this.consoleInput = this.f9835nM.mo4915cd("input");
        this.consoleInput.addKeyListener(new C4306c());
        this.consoleOutput.setText(welcomeMessage());
    }

    /* access modifiers changed from: protected */
    /* renamed from: BM */
    public String welcomeMessage() {
        return "Консоль альфа\nвведите help() для команд, введите objectList() для открытых объектов.\n\n";
    }

    /* access modifiers changed from: protected */
    /* renamed from: iP */
    public void mo23741iP(String str) {
        Stack stack = new Stack();
        for (char c : str.toCharArray()) {
            if (c == '{' || c == '[' || c == '(') {
                stack.push(1);
            }
            if ((c == '}' || c == ']' || c == ')') && !stack.isEmpty()) {
                stack.pop();
            }
        }
        if (stack.isEmpty()) {
            mo23742iQ(str);
            mo23735aF(str);
        }
    }

    /* renamed from: iQ */
    public void mo23742iQ(String str) {
        mo23743m(str, false);
    }

    /* renamed from: m */
    public void mo23743m(String str, boolean z) {
        Document document = this.consoleOutput.getDocument();
        String str2 = String.valueOf(str.trim()) + "\n";
        if (z) {
            str2 = ">" + str.trim() + "\n";
        }
        if (document != null) {
            try {
                document.insertString(document.getLength(), str2, (AttributeSet) null);
                this.consoleOutput.setCaretPosition(document.getLength());
            } catch (BadLocationException e) {
            }
        }
    }

    public void chL() {
        this.consoleOutput.setText(welcomeMessage());
    }

    @C2602hR(mo19255zf = "TOOGLE_CONSOLE")
    /* renamed from: aa */
    public void mo23736aa(boolean z) {
        if (!z) {
            chM();
        }
    }

    /* access modifiers changed from: protected */
    public void chM() {
        if (this.f9835nM.isVisible()) {
            chN();
        } else {
            chO();
        }
    }

    /* access modifiers changed from: protected */
    public void chN() {
        int screenHeight = this.fVq.bHv().getScreenHeight() / 2;
        new aDX(this.f9835nM, "[" + screenHeight + "..0] dur 500", C2830kk.asQ, new C4307d());
        int y = this.f9835nM.getY();
        if (y > 0) {
            new aDX(this.f9835nM, "[" + y + ".." + (screenHeight + y) + "] dur 500", C2830kk.asP, (C0454GJ) null);
        }
    }

    /* access modifiers changed from: protected */
    public void chO() {
        int screenHeight = this.fVq.bHv().getScreenHeight() / 2;
        this.f9835nM.setVisible(true);
        new aDX(this.f9835nM, "[0.." + screenHeight + "] dur 500", C2830kk.asQ, new C4305b());
        int y = this.f9835nM.getY();
        if (y > screenHeight) {
            new aDX(this.f9835nM, "[" + y + ".." + (y - screenHeight) + "] dur 500", C2830kk.asP, (C0454GJ) null);
        }
    }

    /* access modifiers changed from: private */
    public Context aRv() {
        Context bkP = Context.call();
        bkP.setOptimizationLevel(0);
        return bkP;
    }

    /* access modifiers changed from: protected */
    /* renamed from: BL */
    public void mo23732BL() {
        if (this.f9834TR == null) {
            Context lhVar = this.daB.get();
            this.f9834TR = lhVar.initStandardObjects();
            try {
                lhVar.evaluateReader(this.f9834TR, (Reader) new InputStreamReader(ScriptableAIController.class.getClassLoader().getResourceAsStream("taikodom/addon/console/console.js")), "<Console>", 1, (Object) null);
            } catch (JavaScriptException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            mo23744v("console", this);
            mo23744v("window", this.f9835nM);
            mo23734Oh();
        }
    }

    /* renamed from: aF */
    public void mo23735aF(String str) {
        if (!str.trim().isEmpty()) {
            this.fVr.execute(new C4308e());
            this.fNh.add(0, str);
            if (this.fNh.size() > 1000) {
                this.fNh = (LinkedList) this.fNh.subList(0, 1000);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: Oh */
    public void mo23734Oh() {
        mo23744v("_input", this.fVq.getEngineGame().getConfigManager().getSection("input"));
        Context lhVar = this.daB.get();
        try {
            lhVar.evaluateReader(this.f9834TR, (Reader) new InputStreamReader(getClass().getClassLoader().getResourceAsStream("taikodom/addon/console/input.js")), "<input>", 0, (Object) null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: v */
    public void mo23744v(String str, Object obj) {
        mo23732BL();
        this.f9834TR.put(str, this.f9834TR, obj);
    }

    public void stop() {
        if (this.f9835nM != null) {
            this.f9835nM.destroy();
        }
    }

    /* renamed from: taikodom.addon.console.ConsoleAddon$a */
    class C4304a implements KeyListener {
        C4304a() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            ConsoleAddon.this.consoleInput.requestFocus();
        }

        public void keyReleased(KeyEvent keyEvent) {
        }

        public void keyTyped(KeyEvent keyEvent) {
        }
    }

    /* renamed from: taikodom.addon.console.ConsoleAddon$c */
    /* compiled from: a */
    class C4306c implements KeyListener {
        C4306c() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 10) {
                ConsoleAddon.this.mo23741iP(ConsoleAddon.this.consoleInput.getText());
                ConsoleAddon.this.fNi = -1;
            } else if (keyEvent.getKeyCode() == 38) {
                if ((ConsoleAddon.this.consoleInput.getCaretPosition() == 0 || !ConsoleAddon.this.consoleInput.getText().contains("\n")) && ConsoleAddon.this.fNh.size() - 1 > ConsoleAddon.this.fNi) {
                    JTextArea jTextArea = ConsoleAddon.this.consoleInput;
                    LinkedList<String> linkedList = ConsoleAddon.this.fNh;
                    ConsoleAddon consoleAddon = ConsoleAddon.this;
                    int b = consoleAddon.fNi + 1;
                    consoleAddon.fNi = b;
                    jTextArea.setText(linkedList.get(b));
                }
            } else if (keyEvent.getKeyCode() == 40 && !ConsoleAddon.this.consoleInput.getText().contains("\n") && ConsoleAddon.this.fNi > 0) {
                JTextArea jTextArea2 = ConsoleAddon.this.consoleInput;
                LinkedList<String> linkedList2 = ConsoleAddon.this.fNh;
                ConsoleAddon consoleAddon2 = ConsoleAddon.this;
                int b2 = consoleAddon2.fNi - 1;
                consoleAddon2.fNi = b2;
                jTextArea2.setText(linkedList2.get(b2));
            }
        }

        public void keyReleased(KeyEvent keyEvent) {
        }

        public void keyTyped(KeyEvent keyEvent) {
        }
    }

    /* renamed from: taikodom.addon.console.ConsoleAddon$d */
    /* compiled from: a */
    class C4307d implements C0454GJ {
        C4307d() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            ConsoleAddon.this.f9835nM.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.console.ConsoleAddon$b */
    /* compiled from: a */
    class C4305b implements C0454GJ {
        C4305b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            ConsoleAddon.this.f9835nM.toFront();
            ConsoleAddon.this.consoleInput.requestFocus();
        }
    }

    /* renamed from: taikodom.addon.console.ConsoleAddon$e */
    /* compiled from: a */
    class C4308e implements Runnable {
        C4308e() {
        }

        public void run() {
            Context lhVar = ConsoleAddon.this.daB.get();
            ConsoleAddon.this.mo23732BL();
            try {
                Object a = lhVar.evaluateString(ConsoleAddon.this.f9834TR, ConsoleAddon.this.consoleInput.getText(), "<Console>", 1, (Object) null);
                if (!(a instanceof Undefined)) {
                    ConsoleAddon.this.mo23742iQ(a.toString());
                }
            } catch (Exception e) {
                ConsoleAddon.this.mo23743m(e.getMessage(), true);
            }
            ConsoleAddon.this.consoleInput.setText("");
        }
    }
}
