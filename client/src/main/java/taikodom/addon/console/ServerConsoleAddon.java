package taikodom.addon.console;

import game.server.IServerConsole;
import game.server.ServerConsoleConnect;
import logic.IAddonSettings;
import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.JavaScriptException;
import org.mozilla1.javascript.Undefined;
import p001a.C2602hR;
import taikodom.addon.TaikodomAddon;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;

@TaikodomAddon("taikodom.addon.console")
/* compiled from: a */
public class ServerConsoleAddon extends ConsoleAddon implements IServerConsole {
    /* access modifiers changed from: private */
    public ServerConsoleConnect aec = new ServerConsoleConnect();

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        super.initAddon(addonPropertie);
        this.aec.setServerConsole((IServerConsole) this);
        int height = this.f9835nM.getHeight();
        this.f9835nM.setBounds(0, height, this.f9835nM.getWidth(), height);
    }

    /* access modifiers changed from: protected */
    /* renamed from: BL */
    public void mo23732BL() {
        if (this.f9834TR == null) {
            Context lhVar = (Context) this.daB.get();
            this.f9834TR = lhVar.initStandardObjects();
            try {
                lhVar.evaluateReader(this.f9834TR, (Reader) new InputStreamReader(ServerConsoleAddon.class.getClassLoader().getResourceAsStream("taikodom/addon/console/serverconsole.js")), "<ServerConsole>", 1, (Object) null);
            } catch (JavaScriptException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            mo23744v("_sconsole", this.aec);
        }
    }

    @C2602hR(mo19255zf = "TOOGLE_SCONSOLE")
    /* renamed from: aa */
    public void mo23736aa(boolean z) {
        if (!z) {
            chM();
        }
    }

    /* renamed from: aE */
    public void printTerminal(String str) {
        mo23742iQ(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: BM */
    public String welcomeMessage() {
        return "Taikodom server console 0.1 alpha\ntype 'help()' or '>?' for commands\nuse 'server' or 's' object for server call\n\n";
    }

    /* renamed from: aF */
    public void mo23735aF(String str) {
        if (str.startsWith(">")) {
            this.aec.mo2660cP(str.replaceFirst(">\\s*", ""));
            this.consoleInput.setText("");
        } else {
            this.fVr.execute(new C4309a());
        }
        this.fNh.add(0, str.trim());
        if (this.fNh.size() > 1000) {
            this.fNh = (LinkedList) this.fNh.subList(0, 1000);
        }
    }

    /* renamed from: BN */
    public void clearConsole() {
        chL();
    }

    public void stop() {
        if (this.f9835nM != null) {
            this.f9835nM.destroy();
        }
    }

    /* renamed from: taikodom.addon.console.ServerConsoleAddon$a */
    class C4309a implements Runnable {
        C4309a() {
        }

        public void run() {
            try {
                Object ev = ServerConsoleAddon.this.aec.mo2662ev(ServerConsoleAddon.this.consoleInput.getText());
                if (!(ev instanceof Undefined) && ev != null) {
                    ServerConsoleAddon.this.mo23743m(ev.toString(), true);
                }
            } catch (Exception e) {
                ServerConsoleAddon.this.mo23743m(e.getMessage(), true);
            }
            ServerConsoleAddon.this.consoleInput.setText("");
        }
    }
}
