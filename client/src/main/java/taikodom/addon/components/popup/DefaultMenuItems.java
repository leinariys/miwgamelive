package taikodom.addon.components.popup;

import game.network.message.externalizable.aQH;
import game.script.Actor;
import game.script.hangar.Bay;
import game.script.hangar.Hangar;
import game.script.item.BulkItem;
import game.script.item.Component;
import game.script.item.Item;
import game.script.item.ShipItem;
import game.script.player.Player;
import game.script.ship.CargoHold;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.storage.Storage;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.C2055be;
import logic.baa.C4033yO;
import p001a.C3582se;
import p001a.C5566aOg;
import p001a.C6704asA;
import p001a.aFH;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.chat.ChatAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.List;

@TaikodomAddon("taikodom.addon.components.popup")
/* compiled from: a */
public class DefaultMenuItems implements C2495fo {
    /* access modifiers changed from: private */

    public static final int duH = 1;
    public static final int duI = 2;
    public static final int duJ = 4;
    public static final int duK = 8;
    public static final int duL = 16;
    public static final int duM = 32;
    /* renamed from: P */
    public static Player f9810P = null;
    /* access modifiers changed from: private */
    public static C6245ajJ del = null;
    /* access modifiers changed from: private */
    /* renamed from: kj */
    public static IAddonProperties f9811kj;

    /* renamed from: a */
    public static JMenuItem m43024a(C4033yO yOVar) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Info"));
        if (yOVar == null) {
            return null;
        }
        jMenuItem.addActionListener(new C4252j(yOVar));
        return jMenuItem;
    }

    /* renamed from: w */
    public static JMenuItem m43038w(Item auq) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Sell"));
        jMenuItem.setEnabled(false);
        return jMenuItem;
    }

    /* renamed from: q */
    public static JMenuItem m43037q(List<BulkItem> list) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Merge"));
        if (list == null || list.size() <= 0) {
            jMenuItem.setEnabled(false);
        } else {
            jMenuItem.addActionListener(new C4251i(list));
        }
        return jMenuItem;
    }

    /* renamed from: a */
    public static JMenuItem m43022a(BulkItem atv) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Split"));
        if (atv == null || atv.mo302Iq() <= 1) {
            jMenuItem.setEnabled(false);
        } else {
            jMenuItem.addActionListener(new C4245e(atv));
        }
        return jMenuItem;
    }

    /* renamed from: a */
    private static JMenuItem m43025a(Item[] auqArr, int i) {
        Storage dxU = f9810P.dxU();
        if (dxU == null || auqArr == null || auqArr[0].bNh().equals(dxU)) {
            return null;
        }
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Storage"));
        if (dxU.aRB() + ((float) i) <= dxU.mo2696wE()) {
            jMenuItem.addActionListener(new C4248g(auqArr, dxU));
            return jMenuItem;
        }
        jMenuItem.setEnabled(false);
        return jMenuItem;
    }

    /* renamed from: a */
    private static JMenuItem m43023a(Item auq, Actor cr) {
        Bay ajm;
        if (!(cr instanceof Station)) {
            return null;
        }
        Hangar aD = f9810P.mo14346aD((Station) cr);
        if (aD == null) {
            return null;
        }
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Hangar"));
        Iterator it = aD.mo20056KU().iterator();
        while (true) {
            if (it.hasNext()) {
                ajm = (Bay) it.next();
                if (ajm.isEmpty()) {
                    break;
                }
            } else {
                ajm = null;
                break;
            }
        }
        if (ajm != null) {
            jMenuItem.addActionListener(new C4247f(ajm, auq));
        } else {
            jMenuItem.setEnabled(false);
        }
        return jMenuItem;
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0037  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static javax.swing.JMenuItem m43027b(Item r3, Actor r4) {
        /*
            javax.swing.JMenu r1 = new javax.swing.JMenu
            a.vW r0 = f9811kj
            java.lang.String r2 = "Ship"
            java.lang.String r0 = r0.translate(r2)
            r1.<init>(r0)
            a.akU r0 = f9810P
            boolean r0 = r0.bQB()
            if (r0 == 0) goto L_0x0031
            boolean r0 = r4 instanceof p001a.C0096BF
            if (r0 != 0) goto L_0x001b
            r0 = 0
        L_0x001a:
            return r0
        L_0x001b:
            a.akU r0 = f9810P
            a.BF r4 = (p001a.C0096BF) r4
            a.kR r0 = r0.mo14346aD(r4)
            a.ra r0 = r0.mo20056KU()
            java.util.Iterator r2 = r0.iterator()
        L_0x002b:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0037
        L_0x0031:
            r0 = 0
            r1.setEnabled(r0)
            r0 = r1
            goto L_0x001a
        L_0x0037:
            java.lang.Object r0 = r2.next()
            a.aJm r0 = (p001a.C5442aJm) r0
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x002b
            goto L_0x002b
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.components.popup.DefaultMenuItems.m43027b(a.auq, a.CR):javax.swing.JMenuItem");
    }

    /* renamed from: ba */
    private static JMenuItem m43028ba(Player aku) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("send private message"));
        jMenuItem.setEnabled(false);
        ((Player) C3582se.m38985a(aku, (C6144ahM<?>) new C4239b(jMenuItem, aku))).dxa();
        return jMenuItem;
    }

    /* renamed from: bb */
    private static JMenuItem m43029bb(Player aku) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Invite to party"));
        jMenuItem.setEnabled(false);
        ((Player) C3582se.m38985a(aku, (C6144ahM<?>) new C4237a(jMenuItem, aku))).dxa();
        return jMenuItem;
    }

    /* renamed from: bc */
    private static JMenuItem m43030bc(Player aku) {
        if (f9810P == null || !f9810P.dxE()) {
            return null;
        }
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Invite to Corporation"));
        if (!f9810P.bYd().mo10718b(f9810P, C6704asA.HIRE)) {
            jMenuItem.setEnabled(false);
            return jMenuItem;
        }
        jMenuItem.addActionListener(new C4250h(aku));
        return jMenuItem;
    }

    /* renamed from: bd */
    private static JMenuItem m43031bd(Player aku) {
        if (aku == null) {
            return null;
        }
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Invite to trade"));
        jMenuItem.setEnabled(false);
        ((Player) C3582se.m38985a(aku, (C6144ahM<?>) new C4241c(jMenuItem, aku))).dxa();
        return jMenuItem;
    }

    /* renamed from: be */
    private static JMenuItem m43032be(Player aku) {
        JMenuItem jMenuItem = new JMenuItem();
        if (!f9810P.dxu().mo8514ct(aku)) {
            jMenuItem.setText(f9811kj.translate("Block"));
            jMenuItem.addActionListener(new C4256n(aku));
        } else {
            jMenuItem.setText(f9811kj.translate("Unblock"));
            jMenuItem.addActionListener(new C4253k(aku));
        }
        return jMenuItem;
    }

    /* renamed from: bf */
    private static JMenuItem m43033bf(Player aku) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Transfer party leadership"));
        jMenuItem.addActionListener(new C4254l(aku));
        return jMenuItem;
    }

    /* renamed from: bg */
    private static JMenuItem m43034bg(Player aku) {
        JMenuItem jMenuItem = new JMenuItem(f9811kj.translate("Kick from party"));
        jMenuItem.addActionListener(new C4255m(aku));
        return jMenuItem;
    }

    /* renamed from: a */
    public static JMenu m43020a(int i, Item[] auqArr) {
        JMenu a;
        JMenu b;
        JMenu x;
        JMenuItem b2;
        JMenuItem a2;
        JMenuItem a3;
        JMenu jMenu = new JMenu(f9811kj.translate("Send to..."));
        if (f9811kj == null || f9810P == null || i == 0 || auqArr == null || auqArr.length <= 0) {
            return null;
        }
        Actor bhE = f9810P.bhE();
        int i2 = 0;
        for (Item volume : auqArr) {
            i2 = (int) (((float) i2) + volume.getVolume());
        }
        if (!((i & 1) == 0 || !f9810P.bQB() || (a3 = m43025a(auqArr, i2)) == null)) {
            jMenu.add(a3);
        }
        if ((i & 2) != 0 && f9810P.bQB() && auqArr.length == 1 && (auqArr[0] instanceof ShipItem) && (a2 = m43023a(auqArr[0], bhE)) != null) {
            jMenu.add(a2);
        }
        if ((i & 4) != 0 && auqArr.length == 1 && (auqArr[0] instanceof Component) && (b2 = m43027b(auqArr[0], bhE)) != null) {
            jMenu.add(b2);
        }
        if (!((i & 8) == 0 || auqArr.length != 1 || (x = m43039x(auqArr[0])) == null)) {
            jMenu.add(x);
        }
        if (!((i & 32) == 0 || (b = m43026b(auqArr)) == null)) {
            jMenu.add(b);
        }
        if (!((i & 16) == 0 || !f9810P.bQB() || (a = m43021a(auqArr, i2, bhE)) == null)) {
            jMenu.add(a);
        }
        return jMenu;
    }

    /* renamed from: x */
    private static JMenu m43039x(Item auq) {
        JMenu jMenu = new JMenu(f9811kj.translate("Weapon"));
        jMenu.setEnabled(false);
        return jMenu;
    }

    /* renamed from: b */
    private static JMenu m43026b(Item[] auqArr) {
        JMenu jMenu = new JMenu(f9811kj.translate("Weapon"));
        jMenu.setEnabled(false);
        return jMenu;
    }

    /* renamed from: a */
    private static JMenu m43021a(Item[] auqArr, int i, Actor cr) {
        boolean z;
        CargoHold afI;
        if (auqArr == null || auqArr.length <= 0 || !(cr instanceof Station)) {
            return null;
        }
        JMenu jMenu = new JMenu(f9811kj.translate("Container"));
        Hangar aD = f9810P.mo14346aD((Station) cr);
        if (aD == null) {
            return null;
        }
        for (Bay ajm : aD.mo20056KU()) {
            if (!ajm.isEmpty()) {
                Ship al = ajm.mo9625al();
                if (!auqArr[0].bNh().equals(al.afI()) && (afI = al.afI()) != null) {
                    JMenuItem jMenuItem = new JMenuItem(al.agH().mo19891ke().get());
                    if (afI.aRB() + ((float) i) <= afI.mo2696wE()) {
                        jMenuItem.addActionListener(new C4243d(auqArr, afI));
                    } else {
                        jMenuItem.setEnabled(false);
                    }
                    jMenu.add(jMenuItem);
                }
            }
        }
        if (jMenu.getItemCount() > 0) {
            z = true;
        } else {
            z = false;
        }
        jMenu.setEnabled(z);
        return jMenu;
    }

    /* renamed from: bh */
    public static JPopupMenu m43035bh(Player aku) {
        JPopupMenu jPopupMenu = new JPopupMenu();
        if (aku == null) {
            return null;
        }
        jPopupMenu.add(m43028ba(aku));
        jPopupMenu.add(m43029bb(aku));
        JMenuItem bc = m43030bc(aku);
        if (bc != null) {
            jPopupMenu.add(bc);
        }
        jPopupMenu.add(m43031bd(aku));
        jPopupMenu.add(m43032be(aku));
        return jPopupMenu;
    }

    /* renamed from: bi */
    public static JPopupMenu m43036bi(Player aku) {
        if (f9810P.equals(aku) || f9810P.dxi() == null || !f9810P.equals(f9810P.dxi().aQU())) {
            return null;
        }
        JPopupMenu jPopupMenu = new JPopupMenu();
        jPopupMenu.add(m43034bg(aku));
        jPopupMenu.add(m43033bf(aku));
        return jPopupMenu;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        f9811kj = addonPropertie;
        f9810P = f9811kj.getPlayer();
        del = f9811kj.aVU();
    }

    public void stop() {
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$j */
    /* compiled from: a */
    class C4252j implements ActionListener {
        private final /* synthetic */ C4033yO dvz;

        C4252j(C4033yO yOVar) {
            this.dvz = yOVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            DefaultMenuItems.del.mo13975h(new C2055be(this.dvz));
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$i */
    /* compiled from: a */
    class C4251i implements ActionListener {
        private final /* synthetic */ List dvx;

        C4251i(List list) {
            this.dvx = list;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            BulkItem atv = (BulkItem) this.dvx.get(0);
            int i = 1;
            while (true) {
                int i2 = i;
                if (i2 < this.dvx.size()) {
                    atv.mo11562c((BulkItem) this.dvx.get(i2));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$e */
    /* compiled from: a */
    class C4245e implements ActionListener {
        private final /* synthetic */ BulkItem cTk;

        C4245e(BulkItem atv) {
            this.cTk = atv;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((MessageDialogAddon) DefaultMenuItems.f9811kj.mo11830U(MessageDialogAddon.class)).mo24308a((String) null, (aFH) new C4246a(this.cTk));
        }

        /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$e$a */
        class C4246a extends aFH {
            private final /* synthetic */ BulkItem cTk;

            C4246a(BulkItem atv) {
                this.cTk = atv;
            }

            public int getValue() {
                return 1;
            }

            public int getMinimum() {
                return 1;
            }

            public int getMaximum() {
                return this.cTk.mo302Iq() - 1;
            }

            /* renamed from: a */
            public void mo8775a(boolean z, int i) {
                if (z) {
                    this.cTk.mo11560AQ(i);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$g */
    /* compiled from: a */
    class C4248g implements ActionListener {
        private final /* synthetic */ Item[] dvs;
        private final /* synthetic */ Storage dvt;

        C4248g(Item[] auqArr, Storage qzVar) {
            this.dvs = auqArr;
            this.dvt = qzVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int i = 0;
            while (true) {
                try {
                    int i2 = i;
                    if (i2 < this.dvs.length) {
                        ((Storage) C3582se.m38985a(this.dvt, (C6144ahM<?>) new C4249a())).mo2693s(this.dvs[i2]);
                        i = i2 + 1;
                    } else {
                        return;
                    }
                } catch (Exception e) {
                    return;
                }
            }
        }

        /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$g$a */
        class C4249a implements C6144ahM<Item> {
            C4249a() {
            }

            /* renamed from: o */
            public void mo1931n(Item auq) {
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$f */
    /* compiled from: a */
    class C4247f implements ActionListener {

        /* renamed from: IY */
        private final /* synthetic */ Item f9820IY;
        private final /* synthetic */ Bay dvm;

        C4247f(Bay ajm, Item auq) {
            this.dvm = ajm;
            this.f9820IY = auq;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            try {
                this.dvm.mo9626as(((ShipItem) this.f9820IY).mo11127al());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$b */
    /* compiled from: a */
    class C4239b implements C6144ahM<Boolean> {

        /* renamed from: A */
        private final /* synthetic */ Player f9815A;
        private final /* synthetic */ JMenuItem dvl;

        C4239b(JMenuItem jMenuItem, Player aku) {
            this.dvl = jMenuItem;
            this.f9815A = aku;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            this.dvl.setEnabled(bool.booleanValue());
            if (bool.booleanValue()) {
                this.dvl.addActionListener(new C4240a(this.f9815A));
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$b$a */
        class C4240a extends AbstractAction {


            /* renamed from: A */
            private final /* synthetic */ Player f9816A;

            C4240a(Player aku) {
                this.f9816A = aku;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                ChatAddon chatAddon = (ChatAddon) DefaultMenuItems.f9811kj.mo11830U(ChatAddon.class);
                if (chatAddon != null) {
                    chatAddon.mo23581ke(this.f9816A.getName());
                }
            }
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$a */
    class C4237a implements C6144ahM<Boolean> {

        /* renamed from: A */
        private final /* synthetic */ Player f9812A;
        private final /* synthetic */ JMenuItem dvj;

        C4237a(JMenuItem jMenuItem, Player aku) {
            this.dvj = jMenuItem;
            this.f9812A = aku;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            this.dvj.setEnabled(bool.booleanValue());
            if (bool.booleanValue()) {
                this.dvj.addActionListener(new C4238a(this.f9812A));
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$a$a */
        class C4238a extends AbstractAction {


            /* renamed from: A */
            private final /* synthetic */ Player f9813A;

            C4238a(Player aku) {
                this.f9813A = aku;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                DefaultMenuItems.f9810P.dwU().mo21447mN(this.f9813A.getName());
            }
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$h */
    /* compiled from: a */
    class C4250h extends AbstractAction {


        /* renamed from: A */
        private final /* synthetic */ Player f9821A;

        C4250h(Player aku) {
            this.f9821A = aku;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            DefaultMenuItems.f9810P.bYd().mo10709aP(this.f9821A.getName());
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$c */
    /* compiled from: a */
    class C4241c implements C6144ahM<Boolean> {

        /* renamed from: A */
        private final /* synthetic */ Player f9818A;
        private final /* synthetic */ JMenuItem dvw;

        C4241c(JMenuItem jMenuItem, Player aku) {
            this.dvw = jMenuItem;
            this.f9818A = aku;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            this.dvw.setEnabled(bool.booleanValue());
            if (bool.booleanValue()) {
                this.dvw.addActionListener(new C4242a(this.f9818A));
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$c$a */
        class C4242a implements ActionListener {

            /* renamed from: A */
            private final /* synthetic */ Player f9819A;

            C4242a(Player aku) {
                this.f9819A = aku;
            }

            @C5566aOg
            public void actionPerformed(ActionEvent actionEvent) {
                DefaultMenuItems.f9810P.dwU().mo21450mT(this.f9819A.getName());
            }
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$n */
    /* compiled from: a */
    class C4256n implements ActionListener {

        /* renamed from: A */
        private final /* synthetic */ Player f9823A;

        C4256n(Player aku) {
            this.f9823A = aku;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            DefaultMenuItems.del.mo13975h(new aQH(2, this.f9823A.getName()));
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$k */
    /* compiled from: a */
    class C4253k implements ActionListener {

        /* renamed from: A */
        private final /* synthetic */ Player f9822A;

        C4253k(Player aku) {
            this.f9822A = aku;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            DefaultMenuItems.del.mo13975h(new aQH(3, this.f9822A.getName()));
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$l */
    /* compiled from: a */
    class C4254l implements ActionListener {
        private final /* synthetic */ Player itF;

        C4254l(Player aku) {
            this.itF = aku;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            DefaultMenuItems.f9810P.dwU().mo21449mR(this.itF.getName());
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$m */
    /* compiled from: a */
    class C4255m implements ActionListener {
        private final /* synthetic */ Player itF;

        C4255m(Player aku) {
            this.itF = aku;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            DefaultMenuItems.f9810P.dwU().mo21448mP(this.itF.getName());
        }
    }

    /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$d */
    /* compiled from: a */
    class C4243d implements ActionListener {
        private final /* synthetic */ Item[] dvs;
        private final /* synthetic */ CargoHold itG;

        C4243d(Item[] auqArr, CargoHold wIVar) {
            this.dvs = auqArr;
            this.itG = wIVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int i = 0;
            while (true) {
                try {
                    int i2 = i;
                    if (i2 < this.dvs.length) {
                        ((CargoHold) C3582se.m38985a(this.itG, (C6144ahM<?>) new C4244a())).mo2693s(this.dvs[i2]);
                        i = i2 + 1;
                    } else {
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            }
        }

        /* renamed from: taikodom.addon.components.popup.DefaultMenuItems$d$a */
        class C4244a implements C6144ahM<Item> {
            C4244a() {
            }

            /* renamed from: o */
            public void mo1931n(Item auq) {
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                th.printStackTrace();
            }
        }
    }
}
