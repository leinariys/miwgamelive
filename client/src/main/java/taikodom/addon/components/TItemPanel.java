package taikodom.addon.components;

import game.script.item.Component;
import game.script.item.*;
import logic.baa.C4033yO;
import logic.baa.C4068yr;
import logic.baa.C5473aKr;
import logic.baa.aDJ;
import logic.data.link.C2743jO;
import logic.res.code.C5663aRz;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.Panel;
import logic.ui.item.LabeledIcon;
import logic.ui.item.Picture;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.components.popup.DefaultMenuItems;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: a */
public class TItemPanel extends Panel {

    /* access modifiers changed from: private */
    public List<C1447VJ> evM = new ArrayList();
    /* access modifiers changed from: private */
    public C4236e evP;
    /* access modifiers changed from: private */
    public JList list;
    private ItemLocation bLj;
    private JScrollPane evL;
    private aGL evN;
    private C3016mz evO = new C3016mz(this);
    private MouseAdapter evQ = new C4232a();
    private C5473aKr<?, ?> evR;
    private JTable table;

    public TItemPanel() {
        Panel d = IBaseUiTegXml.initBaseUItegXML().createJComponent(TItemPanel.class, "itemPanel.xml");
        this.evL = d.mo4915cd("itemListScrollable");
        this.table = d.mo4915cd("itemTable");
        this.list = d.mo4915cd("itemList");
        this.list.setCellRenderer(new C4234c());
        this.list.setModel(new DefaultListModel());
        this.list.setVisibleRowCount(-1);
        this.list.setDragEnabled(true);
        this.list.setTransferHandler(this.evO);
        this.list.setDropMode(DropMode.ON_OR_INSERT);
        this.list.addMouseListener(this.evQ);
        setLayout(new BorderLayout());
        add((java.awt.Component) this.evL, (Object) "Center");
        this.evR = new C4235d();
    }

    /* access modifiers changed from: private */
    public void refresh() {
        DefaultListModel model = this.list.getModel();
        model.clear();
        for (C1447VJ next : this.evM) {
            if (this.evN == null || this.evN.evaluate(next.mo5990XH())) {
                model.addElement(next);
            }
        }
    }

    public JList getList() {
        return this.list;
    }

    public JTable getTable() {
        return this.table;
    }

    /* renamed from: b */
    public void mo23647b(ItemLocation aag) {
        this.evM.clear();
        if (this.bLj != null) {
            this.bLj.mo8325b(C2743jO.apm, this.evR);
        }
        this.bLj = aag;
        if (this.bLj != null) {
            this.bLj.mo8319a(C2743jO.apm, this.evR);
            for (Item vj : this.bLj.aRz()) {
                this.evM.add(new C1447VJ(vj));
            }
            refresh();
        }
        this.evO.mo20634b(this.bLj);
    }

    public C0400FZ<Item> bCU() {
        if (this.evN == null) {
            this.evN = new aGL();
            this.evN.mo2166a(new C4233b());
        }
        return this.evN;
    }

    /* renamed from: a */
    public void mo23646a(C4236e eVar) {
        this.evP = eVar;
    }

    public void bCV() {
        this.evP = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43004a(Item auq, int i, int i2) {
        JPopupMenu jPopupMenu;
        JPopupMenu r = mo12148r(auq);
        if (auq instanceof BulkItem) {
            if (r == null) {
                jPopupMenu = new JPopupMenu();
            } else {
                jPopupMenu = r;
            }
            JMenuItem jMenuItem = null;
            if (auq.bAP() instanceof C4068yr) {
                jMenuItem = DefaultMenuItems.m43024a((C4033yO) auq.bAP());
            }
            if (jMenuItem != null) {
                jPopupMenu.add(jMenuItem, 0);
                jPopupMenu.addSeparator();
            }
            JMenuItem a = DefaultMenuItems.m43022a((BulkItem) auq);
            if (a != null) {
                jPopupMenu.add(a);
            }
        } else {
            jPopupMenu = r;
        }
        if (jPopupMenu != null) {
            jPopupMenu.show(this.list, i, i2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m43009c(Object[] objArr, int i, int i2) {
        BulkItemType amh;
        boolean z = false;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i3 = 0; i3 < objArr.length; i3++) {
            if (objArr[i3] instanceof C1447VJ) {
                arrayList2.add(objArr[i3].mo5990XH());
            }
        }
        JPopupMenu jPopupMenu = new JPopupMenu();
        BulkItemType amh2 = null;
        int length = objArr.length;
        int i4 = 0;
        while (true) {
            if (i4 < length) {
                Item XH = objArr[i4].mo5990XH();
                if (!(XH instanceof BulkItem)) {
                    break;
                }
                BulkItem atv = (BulkItem) XH;
                if (amh2 != null) {
                    if (atv.bAP() != amh2) {
                        break;
                    }
                    amh = amh2;
                } else {
                    amh = (BulkItemType) atv.bAP();
                }
                arrayList.add((BulkItem) XH);
                i4++;
                amh2 = amh;
            } else {
                z = true;
                break;
            }
        }
        if (z) {
            if (jPopupMenu.getComponentCount() > 0) {
                jPopupMenu.add(new JSeparator());
            }
            JMenuItem q = DefaultMenuItems.m43037q(arrayList);
            if (q != null) {
                jPopupMenu.add(q);
            }
        }
        if (jPopupMenu.getComponentCount() > 0) {
            jPopupMenu.show(this.list, i, i2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public JPopupMenu mo12148r(Item auq) {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public JPopupMenu mo12147a(Item[] auqArr) {
        if (auqArr == null || auqArr.length <= 0) {
            return null;
        }
        return mo12148r(auqArr[0]);
    }

    public String getElementName() {
        return "item-panel";
    }

    /* renamed from: taikodom.addon.components.TItemPanel$e */
    /* compiled from: a */
    public interface C4236e {
        /* renamed from: H */
        void mo6717H(Item auq);
    }

    /* renamed from: taikodom.addon.components.TItemPanel$a */
    class C4232a extends MouseAdapter {
        C4232a() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            Object selectedValue;
            if (TItemPanel.this.evP != null && (selectedValue = TItemPanel.this.list.getSelectedValue()) != null) {
                TItemPanel.this.evP.mo6717H(((C1447VJ) selectedValue).mo5990XH());
            }
        }

        public void mousePressed(MouseEvent mouseEvent) {
            boolean z;
            boolean z2 = true;
            int locationToIndex = TItemPanel.this.list.locationToIndex(mouseEvent.getPoint());
            int[] selectedIndices = TItemPanel.this.list.getSelectedIndices();
            int length = selectedIndices.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = false;
                    break;
                } else if (locationToIndex == selectedIndices[i]) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                if ((mouseEvent.getModifiersEx() & 128) != 128) {
                    z2 = false;
                }
                if (!z2) {
                    TItemPanel.this.list.setSelectedIndex(locationToIndex);
                }
            }
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            Object[] selectedValues;
            if (mouseEvent.isPopupTrigger() && (selectedValues = TItemPanel.this.list.getSelectedValues()) != null && selectedValues.length != 0) {
                if (selectedValues.length == 1) {
                    TItemPanel.this.m43004a(((C1447VJ) selectedValues[0]).mo5990XH(), mouseEvent.getX(), mouseEvent.getY());
                } else {
                    TItemPanel.this.m43009c(selectedValues, mouseEvent.getX(), mouseEvent.getY());
                }
            }
        }
    }

    /* renamed from: taikodom.addon.components.TItemPanel$c */
    /* compiled from: a */
    class C4234c extends C5517aMj {
        C4234c() {
        }

        /* renamed from: a */
        public java.awt.Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            String str;
            LabeledIcon nR = aur.mo11631nR("IconOnly");
            Picture a = nR.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
            C1447VJ vj = (C1447VJ) obj;
            nR.mo2605a(vj.getIcon());
            Item XH = vj.mo5990XH();
            if (XH instanceof Component) {
                ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((Component) XH).cJS().cuJ());
                a.setVisible(true);
                LabeledIcon.C0530a aVar = LabeledIcon.C0530a.SOUTH_EAST;
                if (XH instanceof ProjectileWeapon) {
                    str = String.valueOf(((ProjectileWeapon) XH).aqh());
                } else {
                    str = "";
                }
                nR.mo2602a(aVar, str);
            } else {
                if (XH instanceof BulkItem) {
                    nR.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, String.valueOf(XH.mo302Iq()));
                } else {
                    nR.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, "");
                }
                a.setVisible(false);
            }
            return nR;
        }
    }

    /* renamed from: taikodom.addon.components.TItemPanel$d */
    /* compiled from: a */
    class C4235d implements C5473aKr<aDJ, Object> {
        C4235d() {
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            boolean z;
            int length = objArr.length;
            int i = 0;
            boolean z2 = false;
            while (i < length) {
                Item auq = objArr[i];
                if (!TItemPanel.this.evM.contains(auq)) {
                    TItemPanel.this.evM.add(new C1447VJ(auq));
                    z = true;
                } else {
                    z = z2;
                }
                i++;
                z2 = z;
            }
            if (z2) {
                TItemPanel.this.refresh();
            }
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            boolean z;
            int length = objArr.length;
            int i = 0;
            boolean z2 = false;
            while (i < length) {
                Item auq = objArr[i];
                Iterator it = TItemPanel.this.evM.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((C1447VJ) it.next()).mo5990XH() == auq) {
                            it.remove();
                            z = true;
                            break;
                        }
                    } else {
                        z = z2;
                        break;
                    }
                }
                i++;
                z2 = z;
            }
            if (z2) {
                TItemPanel.this.refresh();
            }
        }
    }

    /* renamed from: taikodom.addon.components.TItemPanel$b */
    /* compiled from: a */
    class C4233b implements C3385qw {
        C4233b() {
        }

        /* renamed from: Xg */
        public void mo21537Xg() {
            TItemPanel.this.refresh();
        }
    }
}
