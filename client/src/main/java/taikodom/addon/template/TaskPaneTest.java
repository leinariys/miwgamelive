package taikodom.addon.template;

import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.ui.item.TaskPane;
import p001a.aGC;
import p001a.aSZ;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;

/* compiled from: a */
public class TaskPaneTest implements C2495fo {

    /* renamed from: kj */
    private IAddonProperties f10279kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        int i;
        this.f10279kj = addonPropertie;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < 14) {
                C5014a aVar = new C5014a("", "");
                int random = (int) (Math.random() * 999.0d);
                TaskPane bpVar = (TaskPane) this.f10279kj.bHv().mo16794bQ("item.xml");
                bpVar.setTitle("[" + random + "]");
                C6245ajJ aVU = this.f10279kj.aVU();
                if (i3 % 2 == 0) {
                    i = 1;
                } else {
                    i = 2;
                }
                aVU.publish(new aSZ(bpVar, aVar, random, i));
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void stop() {
    }

    /* renamed from: taikodom.addon.template.TaskPaneTest$a */
    class C5014a extends aGC {


        C5014a(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
        }
    }
}
