package taikodom.addon.template.helper;

import game.network.message.externalizable.C3689to;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.C1274Sq;
import logic.aaa.C3949xF;
import logic.ui.item.InternalFrame;
import logic.ui.item.TaskPane;
import p001a.aGC;
import p001a.aSZ;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.template.helper")
/* compiled from: a */
public class TestHelper implements C2495fo {

    /* renamed from: nM */
    private InternalFrame f10282nM;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10282nM = addonPropertie.bHv().mo16792bN("helper.xml");
        this.f10282nM.mo4913cb("button1").addActionListener(new C5036c(addonPropertie));
        this.f10282nM.mo4913cb("button2").addActionListener(new C5035b(addonPropertie));
        this.f10282nM.mo4915cd("button3").addActionListener(new C5034a(addonPropertie));
        this.f10282nM.mo4913cb("button4").addActionListener(new C5040f(addonPropertie));
        this.f10282nM.mo4915cd("button5").addActionListener(new C5039e(addonPropertie));
        this.f10282nM.mo4913cb("button6").addActionListener(new C5037d(addonPropertie));
        this.f10282nM.pack();
        this.f10282nM.center();
        this.f10282nM.setVisible(true);
    }

    public void stop() {
    }

    /* renamed from: taikodom.addon.template.helper.TestHelper$c */
    /* compiled from: a */
    class C5036c implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;

        C5036c(IAddonProperties vWVar) {
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.bJA.aVU().mo13975h(new C1274Sq(C1274Sq.C1275a.DOCK));
        }
    }

    /* renamed from: taikodom.addon.template.helper.TestHelper$b */
    /* compiled from: a */
    class C5035b implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;

        C5035b(IAddonProperties vWVar) {
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.bJA.aVU().mo13975h(new C1274Sq(C1274Sq.C1275a.UNDOCK));
        }
    }

    /* renamed from: taikodom.addon.template.helper.TestHelper$a */
    class C5034a implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;

        C5034a(IAddonProperties vWVar) {
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1274Sq.C1275a aVar;
            C6245ajJ aVU = this.bJA.aVU();
            if (((JToggleButton) actionEvent.getSource()).isSelected()) {
                aVar = C1274Sq.C1275a.ENTER_GATE;
            } else {
                aVar = C1274Sq.C1275a.EXIT_GATE;
            }
            aVU.mo13975h(new C1274Sq(aVar));
        }
    }

    /* renamed from: taikodom.addon.template.helper.TestHelper$f */
    /* compiled from: a */
    class C5040f implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;

        C5040f(IAddonProperties vWVar) {
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.bJA.aVU().mo13975h(new C3689to());
        }
    }

    /* renamed from: taikodom.addon.template.helper.TestHelper$e */
    /* compiled from: a */
    class C5039e implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;

        C5039e(IAddonProperties vWVar) {
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.bJA.aVU().mo13975h(new C3949xF(((JToggleButton) actionEvent.getSource()).isSelected()));
        }
    }

    /* renamed from: taikodom.addon.template.helper.TestHelper$d */
    /* compiled from: a */
    class C5037d implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;

        /* renamed from: i */
        int f10283i = 0;

        C5037d(IAddonProperties vWVar) {
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int i;
            C5038a aVar = new C5038a("", "");
            int random = (int) (Math.random() * 999.0d);
            TaskPane bpVar = (TaskPane) this.bJA.bHv().mo16794bQ("../item.xml");
            bpVar.setTitle("[" + random + "]");
            C6245ajJ aVU = this.bJA.aVU();
            if (this.f10283i % 2 == 0) {
                i = 1;
            } else {
                i = 2;
            }
            aVU.publish(new aSZ(bpVar, aVar, random, i));
            this.f10283i++;
        }

        /* renamed from: taikodom.addon.template.helper.TestHelper$d$a */
        class C5038a extends aGC {


            C5038a(String str, String str2) {
                super(str, str2);
            }

            public void setVisible(boolean z) {
            }
        }
    }
}
