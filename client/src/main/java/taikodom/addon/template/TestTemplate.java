package taikodom.addon.template;

import logic.IAddonSettings;
import logic.aPE;
import logic.swing.C0454GJ;
import logic.swing.C3940wz;
import logic.swing.aDX;
import logic.ui.Panel;
import logic.ui.item.*;
import p001a.C0037AU;
import p001a.C2122cE;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;

@TaikodomAddon("taikodom.game.addon.test")
/* compiled from: a */
public class TestTemplate implements C2495fo {
    /* access modifiers changed from: private */
    public Progress bzf;
    /* access modifiers changed from: private */
    public Progress bzg;
    /* access modifiers changed from: private */
    public Progress bzh;
    /* access modifiers changed from: private */
    public Progress bzi;
    /* access modifiers changed from: private */
    public boolean finished = false;
    /* renamed from: kj */
    public IAddonProperties f10280kj;
    /* access modifiers changed from: private */
    private InternalFrame bwQ;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10280kj = addonPropertie;
        this.bwQ = this.f10280kj.bHv().mo16792bN("test.xml");
        this.bwQ.center();
        m45553a(this.bwQ);
        this.bwQ.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m45553a(InternalFrame nxVar) {
        ButtonGroup buttonGroup = new ButtonGroup();
        for (int i = 1; i <= 4; i++) {
            buttonGroup.add(nxVar.mo4915cd("radio" + i));
        }
        ButtonGroup buttonGroup2 = new ButtonGroup();
        for (int i2 = 1; i2 <= 3; i2++) {
            buttonGroup2.add(nxVar.mo4915cd("toggle-button" + i2));
        }
        nxVar.mo4913cb("normalButton2").addActionListener(new C5029o(nxVar));
        nxVar.mo4913cb("normalButton3").addActionListener(new C5030p());
        nxVar.mo4913cb("normalButton4").addActionListener(new C5025k());
        nxVar.mo4915cd("toggle-button4").addActionListener(new C5026l(nxVar));
        JComboBox cc = nxVar.mo4914cc("combobox");
        for (int i3 = 1; i3 <= 50; i3++) {
            cc.addItem("item " + i3);
        }
        this.bzi = nxVar.mo4915cd("normal_progress");
        this.bzf = nxVar.mo4915cd("shield_progress");
        this.bzg = nxVar.mo4915cd("hull_progress");
        this.bzh = nxVar.mo4915cd("cargo_progress");
        C5027m mVar = new C5027m();
        C5028n nVar = new C5028n();
        C5031q qVar = new C5031q();
        C5032r rVar = new C5032r();
        this.f10280kj.mo11833a(80, (aPE) mVar);
        this.f10280kj.mo11833a(100, (aPE) nVar);
        this.f10280kj.mo11833a(120, (aPE) qVar);
        this.f10280kj.mo11833a(140, (aPE) rVar);
        nxVar.mo4915cd("html").setText("<html><p class='title'>Title</p><p>bla bla bla</p></html>");
        JTable cd = nxVar.mo4915cd("table1");
        cd.setModel(new DefaultTableModel(new String[][]{new String[]{"2623", "Dang", "32"}, new String[]{"2342", "Deng", "22"}, new String[]{"2341", "Ding", "21"}, new String[]{"2134", "Dong", "62"}, new String[]{"2142", "Dung", "24"}}, new String[]{"ID", "Name", "Age"}));
        cd.getColumnModel().getColumn(0).setCellRenderer(new C5033s());
        cd.getColumnModel().getColumn(1).setCellRenderer(new C5023i());
        cd.getColumnModel().getColumn(2).setCellRenderer(new C5022h());
        new JPopupMenu().add(new C5020f("action"));
        DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode("The Java Series");
        m45554a(defaultMutableTreeNode);
        C5923acz cd2 = nxVar.mo4915cd("tree1");
        cd2.setModel(new DefaultTreeModel(defaultMutableTreeNode));
        cd2.setCellRenderer(new C5021g());
        C3940wz.m40776a((JComponent) nxVar.mo4917cf("labelTooltip"), (C3940wz) new C5018d());
        C3940wz.m40775a("someCoolTooltipProvider", (C3940wz) new C5019e((Panel) this.f10280kj.bHv().mo16794bQ("myTooltip.xml")));
        new JPopupMenu().add(new C5016b("Action!"));
        new JPopupMenu().add(new C5017c("Leaf Action!"));
        new JPopupMenu().add(new C5015a("Tree Action!"));
    }

    public void stop() {
        this.finished = true;
    }

    /* renamed from: a */
    private void m45554a(DefaultMutableTreeNode defaultMutableTreeNode) {
        DefaultMutableTreeNode defaultMutableTreeNode2 = new DefaultMutableTreeNode("Books for Java Programmers");
        defaultMutableTreeNode.add(defaultMutableTreeNode2);
        defaultMutableTreeNode2.add(new DefaultMutableTreeNode(new C5024j("The Java Tutorial: A Short Course on the Basics", "tutorial.html")));
        defaultMutableTreeNode2.add(new DefaultMutableTreeNode(new C5024j("The Java Tutorial Continued: The Rest of the JDK", "tutorialcont.html")));
        defaultMutableTreeNode2.add(new DefaultMutableTreeNode(new C5024j("The JFC Swing Tutorial: A Guide to Constructing GUIs", "swingtutorial.html")));
        defaultMutableTreeNode2.add(new DefaultMutableTreeNode(new C5024j("Effective Java Programming Language Guide", "bloch.html")));
        defaultMutableTreeNode2.add(new DefaultMutableTreeNode(new C5024j("The Java Programming Language", "arnold.html")));
        defaultMutableTreeNode2.add(new DefaultMutableTreeNode(new C5024j("The Java Developers Almanac", "chan.html")));
        DefaultMutableTreeNode defaultMutableTreeNode3 = new DefaultMutableTreeNode("Books for Java Implementers");
        defaultMutableTreeNode.add(defaultMutableTreeNode3);
        defaultMutableTreeNode3.add(new DefaultMutableTreeNode(new C5024j("The Java Virtual Machine Specification", "vm.html")));
        defaultMutableTreeNode3.add(new DefaultMutableTreeNode(new C5024j("The Java Language Specification", "jls.html")));
    }

    /* renamed from: taikodom.addon.template.TestTemplate$o */
    /* compiled from: a */
    class C5029o implements ActionListener {
        private final /* synthetic */ InternalFrame gRx;

        C5029o(InternalFrame nxVar) {
            this.gRx = nxVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            JLabel cf = this.gRx.mo4917cf("label");
            new aDX(cf, "[0.." + cf.getText().length() + "] dur " + (cf.getText().length() * 100), C2830kk.asT, (C0454GJ) null);
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$p */
    /* compiled from: a */
    class C5030p implements ActionListener {
        C5030p() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InternalFrame c = TestTemplate.this.f10280kj.bHv().mo16795c(TestTemplate.class, "test.xml");
            TestTemplate.this.m45553a(c);
            c.setVisible(true);
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$k */
    /* compiled from: a */
    class C5025k implements ActionListener {
        C5025k() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InternalFrame c = TestTemplate.this.f10280kj.bHv().mo16795c(TestTemplate.class, "test.xml");
            c.setModal(true);
            TestTemplate.this.m45553a(c);
            c.setVisible(true);
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$l */
    /* compiled from: a */
    class C5026l implements ActionListener {
        private final /* synthetic */ InternalFrame gRx;

        C5026l(InternalFrame nxVar) {
            this.gRx = nxVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.gRx.setModal(!this.gRx.isModal());
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$m */
    /* compiled from: a */
    class C5027m implements aPE {

        /* renamed from: i */
        int f10281i = 1;

        C5027m() {
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            int value = TestTemplate.this.bzi.getValue();
            if (value >= 100) {
                this.f10281i = -1;
            } else if (value <= 0) {
                this.f10281i = 1;
            }
            TestTemplate.this.bzi.setValue(value + this.f10281i);
            if (TestTemplate.this.finished) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$n */
    /* compiled from: a */
    class C5028n implements aPE {
        C5028n() {
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            int value = TestTemplate.this.bzf.getValue();
            if (value >= 100) {
                TestTemplate.this.bzf.setValue(0);
            } else {
                TestTemplate.this.bzf.setValue(value + 1);
            }
            if (TestTemplate.this.finished) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$q */
    /* compiled from: a */
    class C5031q implements aPE {
        C5031q() {
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            int value = TestTemplate.this.bzg.getValue();
            if (value >= 100) {
                TestTemplate.this.bzg.setValue(0);
            } else {
                TestTemplate.this.bzg.setValue(value + 1);
            }
            if (TestTemplate.this.finished) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$r */
    /* compiled from: a */
    class C5032r implements aPE {
        C5032r() {
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            int value = TestTemplate.this.bzh.getValue();
            if (value >= 100) {
                TestTemplate.this.bzh.setValue(0);
            } else {
                TestTemplate.this.bzh.setValue(value + 1);
            }
            if (TestTemplate.this.finished) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$s */
    /* compiled from: a */
    class C5033s extends C2122cE {
        C5033s() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("IDView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$i */
    /* compiled from: a */
    class C5023i extends C2122cE {
        C5023i() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            Panel nR = aur.mo11631nR("NameView");
            nR.mo4917cf("nameLabel").setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$h */
    /* compiled from: a */
    class C5022h extends C2122cE {
        C5022h() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("AgeView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$f */
    /* compiled from: a */
    class C5020f extends AbstractAction {


        C5020f(String str) {
            super(str);
        }

        public void actionPerformed(ActionEvent actionEvent) {
            System.out.println("action!");
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$g */
    /* compiled from: a */
    class C5021g extends C0037AU {
        C5021g() {
        }

        /* renamed from: a */
        public Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
            JLabel nR;
            if (z3) {
                nR = (JLabel) aur.mo11631nR("Leaf");
            } else if (z2) {
                nR = (JLabel) aur.mo11631nR("ParentExpanded");
            } else {
                nR = aur.mo11631nR("Parent");
            }
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$d */
    /* compiled from: a */
    class C5018d extends C3940wz {
        C5018d() {
        }

        /* renamed from: b */
        public Component mo2675b(Component component) {
            return new JButton("Bla bla");
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$e */
    /* compiled from: a */
    class C5019e extends C3940wz {
        private final /* synthetic */ Panel dde;

        C5019e(Panel aco) {
            this.dde = aco;
        }

        /* renamed from: b */
        public Component mo2675b(Component component) {
            this.dde.mo4917cf("label").setText("Original tooltip text: " + ((JComponent) component).getToolTipText());
            return this.dde;
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$b */
    /* compiled from: a */
    class C5016b extends AbstractAction {


        C5016b(String str) {
            super(str);
        }

        public void actionPerformed(ActionEvent actionEvent) {
            System.out.println("action!!");
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$c */
    /* compiled from: a */
    class C5017c extends AbstractAction {


        C5017c(String str) {
            super(str);
        }

        public void actionPerformed(ActionEvent actionEvent) {
            System.out.println("leaf action!!");
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$a */
    class C5015a extends AbstractAction {


        C5015a(String str) {
            super(str);
        }

        public void actionPerformed(ActionEvent actionEvent) {
            System.out.println("tree action!!");
        }
    }

    /* renamed from: taikodom.addon.template.TestTemplate$j */
    /* compiled from: a */
    private class C5024j {
        public String fpN;
        public URL fpO;

        public C5024j(String str, String str2) {
            this.fpN = str;
        }

        public String toString() {
            return this.fpN;
        }
    }
}
