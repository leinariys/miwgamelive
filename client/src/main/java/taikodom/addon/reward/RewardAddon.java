package taikodom.addon.reward;

import logic.IAddonSettings;
import logic.aaa.C1549Wj;
import logic.aaa.C3164oa;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import p001a.C3428rT;
import p001a.C5956adg;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

@TaikodomAddon("taikodom.addon.reward")
/* compiled from: a */
public class RewardAddon implements C2495fo {

    /* renamed from: Ks */
    private static /* synthetic */ int[] f10239Ks;
    /* renamed from: Kr */
    public ArrayList<Panel> f10241Kr = new ArrayList<>();
    /* access modifiers changed from: private */
    /* renamed from: kj */
    public IAddonProperties f10242kj;
    /* access modifiers changed from: private */
    /* renamed from: Kq */
    private Panel f10240Kq;

    /* renamed from: qr */
    static /* synthetic */ int[] m45371qr() {
        int[] iArr = f10239Ks;
        if (iArr == null) {
            iArr = new int[C4963b.values().length];
            try {
                iArr[C4963b.MERITOS.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C4963b.PDA.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C4963b.TAIES.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            f10239Ks = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10242kj = addonPropertie;
        init();
    }

    private void init() {
        Panel aco = (Panel) this.f10242kj.bHv().mo16794bQ("reward-messages.xml");
        aco.setVisible(true);
        aco.setLayout((LayoutManager) null);
        aco.pack();
        aco.setLocation((this.f10242kj.ale().aes() / 2) - (aco.getSize().width / 2), 50);
        this.f10240Kq = aco.mo4915cd("rewardPanel");
        m45370qq();
        this.f10242kj.aVU().mo13965a(C3164oa.class, new C4964c());
        this.f10242kj.aVU().mo13965a(C1549Wj.class, new C4965d());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m45366a(String str, C4963b bVar) {
        Panel aco = new Panel();
        IComponentManager e = ComponentManager.getCssHolder(aco);
        switch (m45371qr()[bVar.ordinal()]) {
            case 1:
                e.setAttribute("class", "taeis");
                break;
            case 2:
                e.setAttribute("class", "meritos");
                break;
            case 3:
                e.setAttribute("class", "pda");
                break;
        }
        e.mo13045Vk();
        aco.add(new JLabel(str));
        while (this.f10240Kq.getComponents().length >= 3) {
            this.f10241Kr.remove(0);
        }
        this.f10241Kr.add(aco);
        m45370qq();
        new aDX(aco, "[0..255] dur 250", C2830kk.asS, new C4961a(aco));
    }

    /* access modifiers changed from: private */
    /* renamed from: qq */
    public void m45370qq() {
        this.f10240Kq.removeAll();
        Iterator<Panel> it = this.f10241Kr.iterator();
        while (it.hasNext()) {
            this.f10240Kq.add(it.next());
        }
        this.f10240Kq.setSize(this.f10240Kq.getParent().getWidth(), this.f10240Kq.getPreferredSize().height);
        this.f10240Kq.validate();
    }

    public void stop() {
    }

    /* renamed from: taikodom.addon.reward.RewardAddon$b */
    /* compiled from: a */
    enum C4963b {
        TAIES,
        MERITOS,
        PDA
    }

    /* renamed from: taikodom.addon.reward.RewardAddon$c */
    /* compiled from: a */
    class C4964c extends C3428rT<C3164oa> {
        C4964c() {
        }

        /* renamed from: b */
        public void mo321b(C3164oa oaVar) {
            RewardAddon.this.m45366a(C5956adg.format("+ T$ {0}", Long.valueOf(oaVar.mo21005Sh())), C4963b.TAIES);
        }
    }

    /* renamed from: taikodom.addon.reward.RewardAddon$d */
    /* compiled from: a */
    class C4965d extends C3428rT<C1549Wj> {
        C4965d() {
        }

        /* renamed from: b */
        public void mo321b(C1549Wj wj) {
            RewardAddon.this.m45366a(RewardAddon.this.f10242kj.translate("New PDA Info"), C4963b.PDA);
        }
    }

    /* renamed from: taikodom.addon.reward.RewardAddon$a */
    class C4961a implements C0454GJ {

        /* renamed from: xY */
        private final /* synthetic */ Panel f10243xY;

        C4961a(Panel aco) {
            this.f10243xY = aco;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            new aDX(this.f10243xY, "[255..0] dur 15000", C2830kk.asS, new C4962a(this.f10243xY));
        }

        /* renamed from: taikodom.addon.reward.RewardAddon$a$a */
        class C4962a implements C0454GJ {

            /* renamed from: xY */
            private final /* synthetic */ Panel f10245xY;

            C4962a(Panel aco) {
                this.f10245xY = aco;
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                RewardAddon.this.f10241Kr.remove(this.f10245xY);
                RewardAddon.this.m45370qq();
            }
        }
    }
}
