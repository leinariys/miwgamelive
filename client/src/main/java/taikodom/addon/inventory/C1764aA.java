package taikodom.addon.inventory;

import game.script.hangar.Bay;
import game.script.item.ItemLocation;
import game.script.ship.CargoHold;
import game.script.storage.Storage;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.inventory")
/* renamed from: a.aA */
/* compiled from: a */
public class C1764aA {

    /* renamed from: je */
    public static String f2279je = "res://data/gui/imageset/imageset_iconography/symbol_container";

    /* renamed from: jf */
    public static String f2280jf = "res://data/gui/imageset/imageset_iconography/symbol_hangar";

    /* renamed from: jg */
    public static String f2281jg = "res://data/gui/imageset/imageset_iconography/symbol_ship";

    /* renamed from: jh */
    public static String f2282jh = "res://data/gui/imageset/imageset_iconography/symbo_storage";

    /* renamed from: a */
    public static String m12412a(ItemLocation aag) {
        if (aag instanceof Bay) {
            return f2280jf;
        }
        if (aag instanceof CargoHold) {
            return f2279je;
        }
        if (aag instanceof Storage) {
            return f2282jh;
        }
        return f2279je;
    }

    /* renamed from: a */
    public static String m12413a(IAddonProperties vWVar, ItemLocation aag) {
        if (aag instanceof Bay) {
            return vWVar.mo11834a((Class<?>) C1764aA.class, "Hangar", new Object[0]);
        }
        if (aag instanceof Storage) {
            return vWVar.mo11834a((Class<?>) C1764aA.class, "Storage", new Object[0]);
        }
        if (aag instanceof CargoHold) {
            return vWVar.mo11834a((Class<?>) C1764aA.class, "Container", new Object[0]);
        }
        return "";
    }
}
