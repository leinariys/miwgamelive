package taikodom.addon.inventory;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C5820abA;
import game.network.message.externalizable.C6018aeq;
import game.script.Actor;
import game.script.item.*;
import game.script.player.Player;
import game.script.ship.Station;
import game.script.space.Node;
import logic.C6307akT;
import logic.IAddonSettings;
import logic.aaa.C2055be;
import logic.aaa.C5685aSv;
import logic.baa.C4033yO;
import logic.baa.C4068yr;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.C5923acz;
import logic.ui.item.LabeledIcon;
import logic.ui.item.Picture;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.popup.DefaultMenuItems;
import taikodom.addon.pda.PdaAddon;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.*;

@TaikodomAddon("taikodom.addon.inventory")
/* compiled from: a */
public class InventoryAddon implements C1215Rv, C2495fo {

    /* renamed from: qw */
    private static final String f9987qw = "inventory.xml";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9988P;
    /* access modifiers changed from: private */
    public List<C5820abA> aoA;
    /* access modifiers changed from: private */
    public JComboBox aoC;
    /* access modifiers changed from: private */
    public JComboBox aoD;
    /* access modifiers changed from: private */
    public C1032Oz aoI;
    /* access modifiers changed from: private */
    public PdaAddon aoJ;
    /* access modifiers changed from: private */
    public C5685aSv aoK;
    /* access modifiers changed from: private */
    public C2698il aoL;
    /* renamed from: kj */
    public IAddonProperties f9989kj;
    /* access modifiers changed from: private */
    public JTextField searchField;
    public C5393aHp<Node> aoG;
    public C5393aHp<ItemTypeCategory> aoH;
    private C5923acz akV;
    private Map<Actor, List<C5820abA>> aoB;
    /* access modifiers changed from: private */
    private Set<Node> aoE;
    private Set<ItemTypeCategory> aoF;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9989kj = addonPropertie;
        this.aoJ = (PdaAddon) this.f9989kj.mo11830U(PdaAddon.class);
        this.aoK = new C5685aSv("pda", this.f9989kj, this);
        this.f9989kj.aVU().mo13965a(C2055be.class, new C4586d());
        this.f9989kj.aVU().publish(this.aoK);
    }

    public void open() {
        m44199Im();
        this.aoL.mo4911Kk();
        ((Player) C3582se.m38985a(this.f9988P, (C6144ahM<?>) new C4581a())).dxW();
    }

    /* access modifiers changed from: private */
    /* renamed from: Il */
    public void m44198Il() {
        this.aoB.clear();
        this.aoL.mo4920ci("search").setText("");
        this.aoL.mo4917cf("nodeLabel").setText(this.f9989kj.translate("Node:"));
        this.aoE = new HashSet();
        this.aoG = new C5393aHp<>(null, this.f9989kj.translate("All"));
        this.aoC = m44201a("nodesCombobox", this.aoG);
        this.aoC.addActionListener(new C4585c());
        this.aoL.mo4917cf("categoryLabel").setText(this.f9989kj.translate("Category:"));
        this.aoF = new HashSet();
        this.aoH = new C5393aHp<>(null, this.f9989kj.translate("All"));
        this.aoD = m44201a("itensCombobox", this.aoH);
        this.aoD.addActionListener(new C4584b());
        for (C5820abA next : this.aoA) {
            Station eT = next.mo12361eT();
            List list = this.aoB.get(next.bNF());
            if (eT == null) {
                this.f9988P.logError("Player has a item inside a null station.");
            } else {
                if (list == null) {
                    list = new ArrayList();
                    this.aoB.put(next.bNF(), list);
                }
                Node azW = eT.azW();
                if (!this.aoE.contains(azW)) {
                    this.aoE.add(azW);
                    this.aoC.addItem(new C5393aHp(azW, azW.mo21665ke().get()));
                }
                ItemTypeCategory bJS = next.mo12359XH().bAP().mo19866HC().bJS();
                if (!this.aoF.contains(bJS)) {
                    this.aoF.add(bJS);
                    this.aoD.addItem(new C5393aHp(bJS, bJS.toString()));
                }
                list.add(next);
            }
        }
        this.aoC.setSelectedIndex(0);
        this.aoG = (C5393aHp) this.aoC.getSelectedItem();
        this.aoD.setSelectedIndex(0);
        this.aoH = (C5393aHp) this.aoD.getSelectedItem();
        refresh();
        this.f9989kj.aVU().publish(new C6018aeq("inventory", ""));
        this.f9989kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, getClass()));
    }

    /* renamed from: Im */
    private void m44199Im() {
        m44200In();
        this.searchField = this.aoL.mo4920ci("search");
        this.searchField.addKeyListener(new C4590h());
        this.f9988P = this.f9989kj.ala().getPlayer();
        this.f9989kj.mo11836a("items-reload", (C6307akT) new C4589g());
        this.aoB = new HashMap();
    }

    /* renamed from: In */
    private void m44200In() {
        this.akV = this.aoL.mo4915cd("itemsTree");
        this.akV.addMouseListener(new C4588f(this.akV));
        this.akV.setCellRenderer(new C4587e());
    }

    /* renamed from: a */
    private <T> JComboBox m44201a(String str, C5393aHp<T> ahp) {
        JComboBox cc = this.aoL.mo4914cc(str);
        cc.removeAllItems();
        cc.addItem(ahp);
        cc.setSelectedIndex(0);
        return cc;
    }

    /* renamed from: a */
    private void m44203a(DefaultMutableTreeNode defaultMutableTreeNode, List<C5820abA> list) {
        if (list != null && list.size() != 0) {
            for (C5820abA next : list) {
                if (this.aoH.mo4586D(next.mo12359XH().bAP().mo19866HC().bJS()) && this.aoI.mo4586D(next.mo12359XH().bAP().mo19891ke().get())) {
                    DefaultMutableTreeNode defaultMutableTreeNode2 = new DefaultMutableTreeNode(next.mo12359XH());
                    defaultMutableTreeNode.add(defaultMutableTreeNode2);
                    if (next.mo12359XH() instanceof ShipItem) {
                        m44203a(defaultMutableTreeNode2, this.aoB.get(((ShipItem) next.mo12359XH()).mo11127al()));
                    }
                } else if ((next.mo12359XH() instanceof ShipItem) && m44205a((Object) next.mo12359XH(), this.aoB.get(((ShipItem) next.mo12359XH()).mo11127al()))) {
                    DefaultMutableTreeNode defaultMutableTreeNode3 = new DefaultMutableTreeNode(next.mo12359XH());
                    defaultMutableTreeNode.add(defaultMutableTreeNode3);
                    m44203a(defaultMutableTreeNode3, this.aoB.get(((ShipItem) next.mo12359XH()).mo11127al()));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        this.akV.bOW();
        this.aoI = new C1032Oz(this.aoL.mo4920ci("search").getText(), "name");
        for (Map.Entry next : this.aoB.entrySet()) {
            if ((next.getKey() instanceof Station) && this.aoG.mo4586D(((Station) next.getKey()).azW()) && m44205a(next.getKey(), (List<C5820abA>) (List) next.getValue())) {
                DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode(next.getKey());
                this.akV.mo12740a(defaultMutableTreeNode);
                m44203a(defaultMutableTreeNode, (List<C5820abA>) (List) next.getValue());
            }
        }
        this.akV.expandAll();
    }

    /* renamed from: a */
    private boolean m44205a(Object obj, List<C5820abA> list) {
        if (list == null || list.size() == 0) {
            return false;
        }
        for (C5820abA next : list) {
            if (this.aoH.mo4586D(next.mo12359XH().bAP().mo19866HC().bJS()) && this.aoI.mo4586D(next.mo12359XH().bAP().mo19891ke().get())) {
                return true;
            }
            if ((next.mo12359XH() instanceof ShipItem) && m44205a((Object) next.mo12359XH(), this.aoB.get(((ShipItem) next.mo12359XH()).mo11127al()))) {
                return true;
            }
        }
        return false;
    }

    @C2602hR(mo19255zf = "INVENTORY_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24160al(boolean z) {
        if (z) {
            open();
        }
    }

    public String getTitle() {
        return this.f9989kj.translate("Inventory");
    }

    /* renamed from: Io */
    public String mo5290Io() {
        return f9987qw;
    }

    public void hide() {
    }

    /* renamed from: a */
    public void mo5291a(C2698il ilVar) {
        this.aoL = ilVar;
    }

    public void show() {
    }

    public void update() {
        open();
    }

    public void stop() {
        this.f9989kj.aVU().mo13974g(this.aoK);
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$d */
    /* compiled from: a */
    class C4586d extends C3428rT<C2055be> {
        C4586d() {
        }

        /* renamed from: b */
        public void mo321b(C2055be beVar) {
            if (!InventoryAddon.this.aoJ.isVisible()) {
                InventoryAddon.this.aoJ.bWg();
            } else {
                InventoryAddon.this.update();
            }
            InventoryAddon.this.aoJ.dvC().mo23272c(InventoryAddon.this.aoK);
        }
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$a */
    class C4581a implements C6144ahM<List<C5820abA>> {
        C4581a() {
        }

        /* renamed from: b */
        public void mo1931n(List<C5820abA> list) {
            InventoryAddon.this.aoA = list;
            SwingUtilities.invokeLater(new C4582a());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(new C4583b());
        }

        /* renamed from: taikodom.addon.inventory.InventoryAddon$a$a */
        class C4582a implements Runnable {
            C4582a() {
            }

            public void run() {
                InventoryAddon.this.m44198Il();
                InventoryAddon.this.aoL.mo4912Kl();
            }
        }

        /* renamed from: taikodom.addon.inventory.InventoryAddon$a$b */
        /* compiled from: a */
        class C4583b implements Runnable {
            C4583b() {
            }

            public void run() {
                InventoryAddon.this.aoL.mo4912Kl();
            }
        }
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$c */
    /* compiled from: a */
    class C4585c implements ActionListener {
        C4585c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InventoryAddon.this.aoG = (C5393aHp) InventoryAddon.this.aoC.getSelectedItem();
            InventoryAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$b */
    /* compiled from: a */
    class C4584b implements ActionListener {
        C4584b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InventoryAddon.this.aoH = (C5393aHp) InventoryAddon.this.aoD.getSelectedItem();
            InventoryAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$h */
    /* compiled from: a */
    class C4590h extends KeyAdapter {
        C4590h() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (InventoryAddon.this.aoI != null && InventoryAddon.this.searchField != null && !InventoryAddon.this.aoI.equals(InventoryAddon.this.searchField.getText())) {
                InventoryAddon.this.refresh();
            }
        }
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$g */
    /* compiled from: a */
    class C4589g implements C6307akT {
        C4589g() {
        }

        /* renamed from: j */
        public void mo14337j(String... strArr) {
            InventoryAddon.this.aoA = InventoryAddon.this.f9988P.dxW();
            InventoryAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$f */
    /* compiled from: a */
    class C4588f extends C0960Ny {
        C4588f(JTree jTree) {
            super(jTree);
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public JPopupMenu mo4315c(TreePath treePath) {
            JPopupMenu jPopupMenu;
            Object userObject = ((DefaultMutableTreeNode) treePath.getLastPathComponent()).getUserObject();
            if (userObject instanceof Station) {
                return null;
            }
            if (userObject instanceof Item) {
                ItemType bAP = ((Item) userObject).bAP();
                if (bAP instanceof C4068yr) {
                    JPopupMenu jPopupMenu2 = new JPopupMenu();
                    JMenuItem a = DefaultMenuItems.m43024a((C4033yO) bAP);
                    if (a != null) {
                        jPopupMenu2.add(a);
                        jPopupMenu = jPopupMenu2;
                    } else {
                        jPopupMenu = null;
                    }
                    return jPopupMenu;
                }
            }
            jPopupMenu = null;
            return jPopupMenu;
        }
    }

    /* renamed from: taikodom.addon.inventory.InventoryAddon$e */
    /* compiled from: a */
    class C4587e extends C0037AU {
        C4587e() {
        }

        /* renamed from: a */
        public java.awt.Component mo307a(aUR aur, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
            Object userObject = ((DefaultMutableTreeNode) obj).getUserObject();
            if (userObject instanceof Station) {
                Station bf = (Station) userObject;
                Panel nR = aur.mo11631nR("stationItem");
                nR.mo4917cf("stationName").setText(bf.getName());
                nR.mo4917cf("nodeName").setText(bf.azW().mo21665ke().get());
                return nR;
            } else if (!(userObject instanceof Item)) {
                return (Panel) aur.mo11631nR("item");
            } else {
                Item auq = (Item) userObject;
                Panel nR2 = aur.mo11631nR("item");
                LabeledIcon cd = nR2.mo4915cd("itemIcon");
                nR2.mo4917cf("itemName").setText(auq.bAP().mo19891ke().get());
                cd.mo2605a((Icon) new C2539gY(auq));
                cd.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, String.valueOf(auq.mo302Iq()));
                Picture a = cd.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
                if (auq instanceof Component) {
                    ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) auq.bAP()).cuJ());
                    a.setVisible(true);
                } else {
                    a.setVisible(false);
                }
                nR2.mo4917cf("itemHangar").setIcon(new ImageIcon(InventoryAddon.this.f9989kj.bHv().adz().getImage(C1764aA.m12412a(auq.bNh()))));
                return nR2;
            }
        }
    }
}
