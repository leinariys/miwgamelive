package taikodom.addon.login;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.engine.C5452aJw;
import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.aUU;
import logic.aaa.C5477aKv;
import logic.aaa.C6105agZ;
import logic.res.ConfigIniKeyValue;
import logic.res.ConfigManager;
import logic.res.ConfigManagerSection;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.C1453VN;
import p001a.RuntimeExec;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.infra.script.I18NString;
import taikodom.render.graphics2d.C0559Hm;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@TaikodomAddon("taikodom.addon.login")
/* renamed from: a.akH  reason: case insensitive filesystem */
/* compiled from: a */
public class C6295akH {
    private final Log logger = LogPrinter.setClass(LoginAddon.class);
    private static final String defaultValue = "http://www.taikodom.com.br/";
    private static String siteDomain = "";
    private final String UrlSignup;
    private final String UrlForgotten;
    private final String UrlSupport;
    public String UrlGameCreateUser;
    private String currentLocation;
    private String UrlGameResendActivationEmail;
    private String UrlGameGetEmailFromUser;
    private String fUl;
    /* renamed from: kj */
    private IAddonProperties f4755kj;

    public C6295akH(IAddonProperties vWVar) {
        this.f4755kj = vWVar;
        this.currentLocation = I18NString.getCurrentLocation();
        if (!this.currentLocation.equalsIgnoreCase("pt") && !this.currentLocation.equalsIgnoreCase(I18NString.DEFAULT_LOCATION)) {
            this.currentLocation = I18NString.DEFAULT_LOCATION;
        }
        String domain = getSiteDomain();
        this.UrlSignup = String.valueOf(domain) + "/signup";
        this.UrlForgotten = String.valueOf(domain) + "/auth/forgotten";//забыл пароль, открытие ссылки в браузере
        this.UrlSupport = String.valueOf(domain) + "/support/";//поддержка, открытие ссылки в браузере
        this.UrlGameResendActivationEmail = String.valueOf(domain) + "/users/game_resend_activation_email";
        this.UrlGameGetEmailFromUser = String.valueOf(domain) + "/users/game_get_email_from_user";
        this.UrlGameCreateUser = String.valueOf(domain) + "/users/game_create_user";
    }

    /* renamed from: jf */
    public void mo14255jf(String str) {
        ConfigManager bhb = this.f4755kj.getEngineGame().getConfigManager();
        ConfigManagerSection kW = bhb.getSection(ConfigIniKeyValue.USER_NAME.getValue());
        if (kW == null) {
            kW = new ConfigManagerSection();
            bhb.sectionOptions.put(ConfigIniKeyValue.USER_NAME.getValue(), kW);
        }
        kW.mo7203b(ConfigIniKeyValue.USER_NAME, str);
        ConfigManagerSection kW2 = bhb.getSection(ConfigIniKeyValue.LAST_RUN_VERSION.getValue());
        if (kW2 == null) {
            kW2 = new ConfigManagerSection();
            bhb.sectionOptions.put(ConfigIniKeyValue.LAST_RUN_VERSION.getValue(), kW2);
        }
        kW2.mo7203b(ConfigIniKeyValue.LAST_RUN_VERSION, TaikodomVersion.VERSION);
        try {
            bhb.loadConfigIni("config/user.ini");
        } catch (IOException e) {
            this.logger.error("User config options could not be saved", e);
        }
    }

    public String cgz() {
        ConfigManagerSection kW = this.f4755kj.getEngineGame().getConfigManager().getSection(ConfigIniKeyValue.USER_NAME.getValue());
        if (kW == null) {
            return "";
        }
        return kW.getValuePropertyString(ConfigIniKeyValue.USER_NAME, "");
    }

    public String cgA() {
        ConfigManagerSection kW = this.f4755kj.getEngineGame().getConfigManager().getSection(ConfigIniKeyValue.LAST_RUN_VERSION.getValue());
        if (kW == null) {
            return "";
        }
        return kW.getValuePropertyString(ConfigIniKeyValue.LAST_RUN_VERSION, "");
    }

    public String getSiteDomain() {
        if (siteDomain.length() == 0) {
            try {
                siteDomain = this.f4755kj.getEngineGame().getConfigManager().getSection(ConfigIniKeyValue.WEB_SITE.getValue()).getValuePropertyString(ConfigIniKeyValue.WEB_SITE, defaultValue);
            } catch (C5452aJw e) {
                siteDomain = defaultValue;
            }
        }
        return siteDomain;
    }

    public void exitGame() {
        this.f4755kj.getEngineGame().exit();
    }

    public void cgC() {
        try {
            openToBrowser(this.UrlSignup);
        } catch (Exception e) {
            this.logger.error("Problems opening Taikodom site (Register)", e);
        }
    }

    public void openUrlForgotten() {
        try {
            openToBrowser(this.UrlForgotten);
        } catch (Exception e) {
            this.logger.error("Problems opening Taikodom site (Forgot Password)", e);
        }
    }

    public void openUrlSupport() {
        try {
            openToBrowser(this.UrlSupport);
        } catch (Exception e) {
            this.logger.error("Problems opening Taikodom site (Support)", e);
        }
    }

    public void openUrlWebSait() {
        try {
            openToBrowser(getSiteDomain());
        } catch (Exception e) {
            this.logger.error("Problems opening Taikodom site (Home)", e);
        }
    }

    public void cgG() {
        this.f4755kj.aVU().mo13975h(new C6105agZ());
    }

    public void cgH() {
        this.f4755kj.aVU().mo13975h(new C5477aKv());
    }

    public String requestServerStatus() {
        return downloadToHttp(String.valueOf(getSiteDomain()) + "/server_status_" + this.currentLocation + ".txt");
    }

    public String requestServerAlert() {
        return downloadToHttp(String.valueOf(getSiteDomain()) + "/server_alert_" + this.currentLocation + ".txt");
    }

    /* renamed from: jg */
    public String downloadToHttp(String str) {
        String defResult;
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            if (httpURLConnection.getResponseCode() != 200) {
                throw new IOException();
            }
            String resultHttp = C0559Hm.m5245a(httpURLConnection.getInputStream(), C0559Hm.igv);
            if (resultHttp == null) {
                return "";
            }
            try {
                return resultHttp.trim();
            } catch (MalformedURLException e) {
                e = e;
            } catch (IOException e2) {
                this.logger.error("Could not download the server status or alert update. URL tried: " + str);
                return r0;
            }
        } catch (MalformedURLException e3) {
            e = e3;
            defResult = "";
        } catch (IOException e4) {
            String str3 = "";
            this.logger.error("Could not download the server status or alert update. URL tried: " + str);
            return str3;
        }
        this.logger.error("MalformedURLException", e);
        return defResult;
    }

    public String cgK() {
        String str;
        try {
            StringBuilder sb = new StringBuilder("res/client/notActivated");
            if (this.currentLocation.equalsIgnoreCase("pt")) {
                str = "";
            } else {
                str = "_" + this.currentLocation;
            }
            return m23139jh(sb.append(str).append(".html").toString());
        } catch (Exception e) {
            this.logger.error("Could not open not activated notification file.", e);
            return "";
        }
    }

    public String cgL() {
        String str;
        try {
            StringBuilder sb = new StringBuilder("res/client/license");
            if (this.currentLocation.equalsIgnoreCase("pt")) {
                str = "";
            } else {
                str = "_" + this.currentLocation;
            }
            return m23139jh(sb.append(str).append(".txt").toString());
        } catch (Exception e) {
            this.logger.error("Could not open license file.", e);
            return "";
        }
    }

    public String requestReleaseNotes() {
        return downloadToHttp(String.valueOf(getSiteDomain()) + "release_notes_" + this.currentLocation + ".txt");
    }

    /* renamed from: jh */
    private String m23139jh(String str) {
        return C0559Hm.m5245a(this.f4755kj.bHu().getRootPath().concat(str).openInputStream(), C0559Hm.igv);
    }

    /* renamed from: gg */
    private void openToBrowser(String url) {
        RuntimeExec.openURL(url);
    }

    /* renamed from: b */
    private void m23137b(C0939Nn.C0940a aVar, String str) {
        ((MessageDialogAddon) this.f4755kj.mo11830U(MessageDialogAddon.class)).mo24306a(aVar, str, this.f4755kj.translate("OK"));
    }

    public boolean cgN() {
        cgQ();
        int length = cgP().length();
        if (length <= 0 || length >= 500) {
            return false;
        }
        return true;
    }

    public void cgO() {
        if (cgN()) {
            this.f4755kj.aVU().mo13975h(new aUU(1));
            m23137b(C0939Nn.C0940a.ERROR, cgP());
        }
    }

    public String cgP() {
        return this.fUl;
    }

    private void cgQ() {
        mo14257ji(requestServerAlert());
    }

    /* renamed from: ji */
    public void mo14257ji(String str) {
        this.fUl = str;
    }

    public String cgR() {
        return this.currentLocation;
    }

    /* renamed from: m */
    public boolean mo14258m(String str, String str2, String str3) {
        String g = C1453VN.requestPostArgument(this.UrlGameResendActivationEmail, new Object[]{"locale", this.currentLocation, "external_agent", "true", "name", str, "password", str2, "email", str3});
        if (g.length() == 0) {
            m23137b(C0939Nn.C0940a.ERROR, this.f4755kj.translate("Server is offline. Please try again later."));
            return false;
        } else if (g.equalsIgnoreCase("OK")) {
            m23137b(C0939Nn.C0940a.INFO, this.f4755kj.translate("Email sent. Check your inbox."));
            return true;
        } else {
            m23137b(C0939Nn.C0940a.WARNING, g);
            return false;
        }
    }

    /* renamed from: ar */
    public String mo14235ar(String str, String str2) {
        return C1453VN.m10592h(this.UrlGameGetEmailFromUser, new Object[]{"locale", this.currentLocation, "external_agent", "true", "name", str, "password", str2}).trim();
    }
}
