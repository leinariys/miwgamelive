package taikodom.addon.login;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.aUU;
import game.script.player.User;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C1169RJ;
import logic.aaa.C2667iJ;
import logic.aaa.C2682iV;
import logic.aaa.C5211aAp;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.item.EditorPane;
import logic.ui.item.InternalFrame;
import logic.ui.item.Picture;
import logic.ui.item.TextField;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.infra.script.I18NString;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

@TaikodomAddon("taikodom.addon.login")
/* compiled from: a */
public class LoginAddon implements C2495fo {

    /* renamed from: qu */
    private static final String RELEASE_NOTES_XML = "release-notes.xml";

    /* renamed from: qv */
    private static final String EULA_XML = "eula.xml";

    /* renamed from: qz */
    private static final String NOT_ACTIVATED_XML = "notActivated.xml";
    /* access modifiers changed from: private */
    /* renamed from: qw */
    private final String LOGIN_XML = "login.xml";
    /* access modifiers changed from: private */
    /* renamed from: kj */
    public IAddonProperties f10026kj;
    /* access modifiers changed from: private */
    /* renamed from: qA */
    public InternalFrame f10027qA;
    /* renamed from: qB */
    public C6295akH f10028qB;
    /* renamed from: qG */
    public C2698il f10033qG;
    /* renamed from: qH */
    public InternalFrame f10034qH;
    /* renamed from: qP */
    public InternalFrame f10042qP;
    /* access modifiers changed from: private */
    /* renamed from: qQ */
    public C6144ahM<Boolean> f10043qQ;
    /* access modifiers changed from: private */
    /* renamed from: qR */
    public C6144ahM<Boolean> f10044qR;
    /* renamed from: qx */
    public C2698il f10047qx;
    /* renamed from: qy */
    public InternalFrame f10048qy;
    /* renamed from: qC */
    private JTextField loginField;
    /* renamed from: qD */
    private JPasswordField passwordField;
    /* renamed from: qE */
    private JCheckBox f10031qE;
    /* renamed from: qF */
    private String f10032qF;
    /* renamed from: qI */
    private C3428rT<aUU> f10035qI;
    /* access modifiers changed from: private */
    /* renamed from: qJ */
    private C3428rT<C1169RJ> f10036qJ;
    /* access modifiers changed from: private */
    /* renamed from: qK */
    private C3428rT<C5211aAp> f10037qK;
    /* access modifiers changed from: private */
    /* renamed from: qL */
    private PopUpWindowCreateUser windowCreateUser;
    /* renamed from: qM */
    private TextField f10039qM;
    /* renamed from: qN */
    private JButton f10040qN;
    /* access modifiers changed from: private */
    /* renamed from: qO */
    private C3428rT<C2682iV> f10041qO;
    /* access modifiers changed from: private */
    /* renamed from: qS */
    private C3428rT<C2667iJ> f10045qS;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        if (addonPropertie.getPlayer() == null) {
            this.f10026kj = addonPropertie;
            this.f10028qB = new C6295akH(this.f10026kj);
            this.f10035qI = new C4645e();
            this.f10037qK = new C4644d();
            this.f10036qJ = new C4641b();
            this.f10041qO = new C4649i();
            this.f10045qS = new C4651k();
            this.f10026kj.aVU().mo13965a(C2682iV.class, this.f10041qO);
            this.f10026kj.aVU().mo13965a(C2667iJ.class, this.f10045qS);
            this.f10026kj.aVU().mo13965a(aUU.class, this.f10035qI);
            this.f10026kj.aVU().mo13965a(C1169RJ.class, this.f10036qJ);
            this.f10026kj.aVU().mo13965a(C5211aAp.class, this.f10037qK);
            init();
        }
    }

    /* access modifiers changed from: private */
    public void show() {
        if (this.f10047qx == null) {
            init();
        }
        this.f10047qx.setVisible(true);
        if (this.f10032qF.equals("")) {
            this.loginField.requestFocus();
        } else {
            this.passwordField.requestFocus();
        }
    }

    private void init() {
        String U;
        this.f10047qx = (C2698il) this.f10026kj.bHv().mo16794bQ(LOGIN_XML);
        this.f10032qF = this.f10028qB.cgz();
        initButtonWindowBottom();
        initButtonCloseAndEnter();
        initFieldsEntry();
        initNewsPanel();
        initWindowReleaseNotes();
        this.f10047qx.setLocation(0, 0);
        Dimension screenSize = this.f10026kj.bHv().getScreenSize();
        this.f10047qx.setSize(screenSize);
        String U2 = this.f10026kj.mo11831U("imageset", "login");
        if (U2 != null && !U2.isEmpty() && (U = this.f10026kj.mo11831U("imagelist", "login")) != null && !U.isEmpty()) {
            new C4650j("Login BG Reescaler", U, U2, screenSize).run();
        }
        this.f10047qx.validate();
        m44405hs();
    }

    /* renamed from: hs */
    private void m44405hs() {
        if (this.f10028qB != null && !this.f10028qB.cgR().equalsIgnoreCase("pt")) {
            ((MessageDialogAddon) this.f10026kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.INFO, this.f10026kj.translate("english-version-not-ready"), this.f10026kj.translate("OK"));
        }
        if (this.f10028qB.cgA().length() == 0) {
            m44409hy();
        }
    }

    private void hide() {
        this.f10047qx.setVisible(false);
    }

    /* access modifiers changed from: package-private */
    public void destroy() {
        if (this.f10047qx != null) {
            this.f10047qx.setVisible(false);
            this.f10047qx = null;
        }
    }

    /* access modifiers changed from: private */
    public void close() {
        String str;
        User bhe = this.f10026kj.getEngineGame().getUser();
        C6295akH akh = this.f10028qB;
        if (this.f10031qE.isSelected()) {
            str = bhe.getName();
        } else {
            str = "";
        }
        akh.mo14255jf(str);
        hide();
    }

    public void stop() {
        if (this.f10047qx != null) {
            this.f10026kj.aVU().mo13964a(this.f10035qI);
        }
        destroy();
    }

    /* renamed from: ht */
    private void initButtonWindowBottom() {
        this.f10047qx.mo4917cf("versionLabel").setText(TaikodomVersion.VERSION);
        this.f10047qx.mo4913cb("terms").addActionListener(new ClickTerms());
        this.f10047qx.mo4913cb("eula").addActionListener(new ClickEula());
        this.f10047qx.mo4913cb("notes").addActionListener(new ClickNotes());
        this.f10047qx.mo4913cb("register").addActionListener(new ClickRegister());
        this.f10047qx.mo4913cb("forgot").addActionListener(new ClickForgot());
        this.f10047qx.mo4913cb("support").addActionListener(new ClickSupport());
        this.f10047qx.mo4913cb("website").addActionListener(new ClickWebsite());
        this.f10047qx.mo4913cb("settings").addActionListener(new ClickSettings());
        this.f10047qx.mo4913cb("credits").addActionListener(new ClickCredits());
    }

    /* renamed from: hu */
    private void initButtonCloseAndEnter() {
        this.f10047qx.mo4913cb("closeButton").addActionListener(new ClickCloseButton());
        this.f10040qN = this.f10047qx.mo4913cb("enterButton");
        this.f10040qN.addActionListener(new ClickEnterButton());
    }

    /* renamed from: hv */
    private void initFieldsEntry() {
        this.loginField = this.f10047qx.mo4920ci("login");
        this.loginField.setText(this.f10032qF);
        this.passwordField = this.f10047qx.mo4915cd("password");
        this.loginField.addKeyListener(new ClickLoginField());
        this.passwordField.addKeyListener(new ClickPassField());
        this.f10031qE = this.f10047qx.mo4915cd("saveUserCheckBox");
        this.f10031qE.setSelected(!this.f10032qF.equals(""));
    }

    /* access modifiers changed from: protected */
    /* renamed from: hw */
    public void mo24212hw() {
        String str;
        String str2;
        if (this.f10027qA != null) {
            this.f10027qA.setVisible(true);
            return;
        }
        this.f10027qA = this.f10026kj.bHv().mo16792bN(NOT_ACTIVATED_XML);
        this.f10027qA.pack();
        try {
            str = this.f10028qB.cgK();
        } catch (Exception e) {
            str = "";
        }
        this.f10027qA.mo4915cd("content").setText(str);
        this.f10027qA.mo4920ci("username").setText(this.loginField.getText());
        this.f10039qM = this.f10027qA.mo4920ci("email");
        try {
            str2 = this.f10028qB.mo14235ar(this.loginField.getText(), String.valueOf(this.passwordField.getPassword()));
        } catch (Exception e2) {
            str2 = "";
        }
        this.f10039qM.setText(str2);
        this.f10027qA.mo4913cb("sendEmailButton").addActionListener(new C4660t());
        this.f10027qA.mo4913cb("closeButton").addActionListener(new C4659s());
        this.f10027qA.center();
        this.f10027qA.setVisible(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: hx */
    public void mo24213hx() {
        if (this.f10028qB.mo14258m(this.loginField.getText(), String.valueOf(this.passwordField.getPassword()), this.f10039qM.getText())) {
            this.f10027qA.setVisible(false);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: hy */
    public void m44409hy() {
        if (this.windowCreateUser == null) {
            this.windowCreateUser = new PopUpWindowCreateUser(this.f10026kj, this.f10028qB.cgR(), this.f10028qB.UrlGameCreateUser);
        }
        this.windowCreateUser.showPopUpWindowCreateUser();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44389a(C6144ahM<Boolean> ahm, boolean z) {
        if (this.f10048qy == null) {
            this.f10048qy = this.f10026kj.bHv().mo16792bN(EULA_XML);
            JButton cb = this.f10048qy.mo4913cb("acceptButton");
            this.f10048qy.setTitle(this.f10026kj.translate("End User License Agreement"));
            this.f10048qy.pack();
            this.f10048qy.mo4913cb("refuseButton").addActionListener(new C4658r());
            cb.addActionListener(new C4657q());
        }
        this.f10043qQ = ahm;
        m44390a(this.f10048qy, "end_user_license_in_game", z);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44393b(C6144ahM<Boolean> ahm, boolean z) {
        if (this.f10042qP == null) {
            this.f10042qP = this.f10026kj.bHv().mo16792bN(EULA_XML);
            this.f10042qP.setTitle(this.f10026kj.translate("Terms Of Use"));
            this.f10042qP.pack();
            this.f10042qP.mo4913cb("refuseButton").addActionListener(new C4656p());
            this.f10042qP.mo4913cb("acceptButton").addActionListener(new C4655o());
        }
        this.f10044qR = ahm;
        m44390a(this.f10042qP, "terms_of_use_in_game", z);
    }

    /* renamed from: a */
    private void m44390a(InternalFrame nxVar, String str, boolean z) {
        JButton cb = nxVar.mo4913cb("refuseButton");
        JButton cb2 = nxVar.mo4913cb("acceptButton");
        if (z) {
            cb.setVisible(false);
            nxVar.setClosable(true);
            cb2.setText(this.f10026kj.translate("OK"));
            cb2.setEnabled(true);
        } else {
            nxVar.setClosable(false);
            cb.setVisible(true);
            cb2.setEnabled(false);
            cb2.setText(this.f10026kj.translate("Accept"));
        }
        C4640a aVar = new C4640a(nxVar.mo4917cf("loading"));
        Container ce = nxVar.mo4916ce("switcher");
        CardLayout layout = ce.getLayout();
        aVar.start();
        layout.show(ce, "loading");
        C4654n nVar = new C4654n("Some Contract page", str, nxVar, aVar, layout, ce, cb2);
        nVar.setDaemon(true);
        nVar.start();
        nxVar.center();
        nxVar.setVisible(true);
    }

    /* renamed from: hz */
    private void initNewsPanel() {
        this.f10033qG = this.f10047qx.mo4915cd("newsPanel");
        this.f10033qG.setVisible(false);
        C4652l lVar = new C4652l("Init news panel");
        lVar.setDaemon(true);
        lVar.start();
    }

    /* renamed from: hA */
    private void initWindowReleaseNotes() {
        this.f10034qH = this.f10026kj.bHv().mo16792bN(RELEASE_NOTES_XML);
        this.f10034qH.setTitle(this.f10026kj.translate("Release Notes"));
        this.f10034qH.pack();
        this.f10034qH.center();
        this.f10034qH.mo4913cb("acceptButton").addActionListener(new C4653m());
        this.f10034qH.mo4915cd("content").setText(this.f10028qB.requestReleaseNotes());
    }

    /* access modifiers changed from: private */
    /* renamed from: hB */
    public void releaseNotesVisible() {
        this.f10034qH.setVisible(true);
    }

    public void writeLoginPassToFile() {
        this.f10026kj.aVU().mo13975h(new StorageLoginPass(this.loginField.getText(), String.valueOf(this.passwordField.getPassword()), false));
        this.f10031qE.requestFocus();
    }

    /* renamed from: taikodom.addon.login.LoginAddon$e */
    /* compiled from: a */
    class C4645e extends C3428rT<aUU> {
        C4645e() {
        }

        /* renamed from: a */
        public void mo321b(aUU auu) {
            switch (auu.dAD()) {
                case 1:
                    LoginAddon.this.show();
                    return;
                case 2:
                    LoginAddon.this.close();
                    return;
                case 5:
                    LoginAddon.this.destroy();
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$d */
    /* compiled from: a */
    class C4644d extends C3428rT<C5211aAp> {
        C4644d() {
        }

        /* renamed from: a */
        public void mo321b(C5211aAp aap) {
            LoginAddon.this.show();
            LoginAddon.this.mo24212hw();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$b */
    /* compiled from: a */
    class C4641b extends C3428rT<C1169RJ> {
        C4641b() {
        }

        /* renamed from: a */
        public void mo321b(C1169RJ rj) {
            ((MessageDialogAddon) LoginAddon.this.f10026kj.mo11830U(MessageDialogAddon.class)).mo24307a(LoginAddon.this.f10026kj.translate("You were disconnected from the server"), (aEP) new C4642a(), LoginAddon.this.f10026kj.translate("Ok"));
        }

        /* renamed from: taikodom.addon.login.LoginAddon$b$a */
        class C4642a extends aEP {
            C4642a() {
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                System.exit(-1);
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                return C0939Nn.C0940a.INFO;
            }
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$i */
    /* compiled from: a */
    class C4649i extends C3428rT<C2682iV> {
        C4649i() {
        }

        /* renamed from: a */
        public void mo321b(C2682iV iVVar) {
            LoginAddon.this.m44389a(iVVar.mo19622BJ(), iVVar.mo19622BJ() == null);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$k */
    /* compiled from: a */
    class C4651k extends C3428rT<C2667iJ> {
        C4651k() {
        }

        /* renamed from: a */
        public void mo321b(C2667iJ iJVar) {
            LoginAddon.this.m44393b(iJVar.mo19473BJ(), iJVar.mo19473BJ() == null);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$j */
    /* compiled from: a */
    class C4650j extends Thread {
        private final /* synthetic */ String fUV;
        private final /* synthetic */ String fUW;
        private final /* synthetic */ Dimension fUX;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4650j(String str, String str2, String str3, Dimension dimension) {
            super(str);
            this.fUV = str2;
            this.fUW = str3;
            this.fUX = dimension;
        }

        public void run() {
            String[] split = this.fUV.split(" ");
            String str = String.valueOf(this.fUW) + split[(int) Math.floor(Math.random() * ((double) split.length))];
            Picture cd = LoginAddon.this.f10047qx.mo4915cd("backgroundContainer");
            cd.mo16825aw(str);
            Picture.m27173a(cd, this.fUX);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$g */
    /* compiled from: a */
    class ClickTerms implements ActionListener {
        ClickTerms() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.m44393b((C6144ahM<Boolean>) null, true);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$f */
    /* compiled from: a */
    class ClickEula implements ActionListener {
        ClickEula() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.m44389a((C6144ahM<Boolean>) null, true);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$h */
    /* compiled from: a */
    class ClickNotes implements ActionListener {
        ClickNotes() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.releaseNotesVisible();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$y */
    /* compiled from: a */
    class ClickRegister implements ActionListener {
        ClickRegister() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.m44409hy();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$z */
    /* compiled from: a */
    class ClickForgot implements ActionListener {
        ClickForgot() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10028qB.openUrlForgotten();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$A */
    /* compiled from: a */
    class ClickSupport implements ActionListener {
        ClickSupport() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10028qB.openUrlSupport();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$B */
    /* compiled from: a */
    class ClickWebsite implements ActionListener {
        ClickWebsite() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10028qB.openUrlWebSait();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$C */
    /* compiled from: a */
    class ClickSettings implements ActionListener {
        ClickSettings() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10028qB.cgG();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$D */
    /* compiled from: a */
    class ClickCredits implements ActionListener {
        ClickCredits() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10028qB.cgH();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$v */
    /* compiled from: a */
    class ClickCloseButton implements ActionListener {
        ClickCloseButton() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10028qB.exitGame();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$u */
    /* compiled from: a */
    class ClickEnterButton implements ActionListener {
        ClickEnterButton() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.writeLoginPassToFile();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$x */
    /* compiled from: a */
    class ClickLoginField extends KeyAdapter {
        ClickLoginField() {
        }

        public void keyTyped(KeyEvent keyEvent) {
            if (keyEvent.getKeyChar() == 10) {
                LoginAddon.this.writeLoginPassToFile();
            }
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$w */
    /* compiled from: a */
    class ClickPassField extends KeyAdapter {
        ClickPassField() {
        }

        public void keyTyped(KeyEvent keyEvent) {
            if (keyEvent.getKeyChar() == 10) {
                LoginAddon.this.writeLoginPassToFile();
            }
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$t */
    /* compiled from: a */
    class C4660t implements ActionListener {
        C4660t() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.mo24213hx();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$s */
    /* compiled from: a */
    class C4659s implements ActionListener {
        C4659s() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10027qA.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$r */
    /* compiled from: a */
    class C4658r implements ActionListener {
        C4658r() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (LoginAddon.this.f10043qQ != null) {
                LoginAddon.this.f10043qQ.mo1931n(false);
                LoginAddon.this.f10043qQ = null;
            }
            LoginAddon.this.f10028qB.exitGame();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$q */
    /* compiled from: a */
    class C4657q implements ActionListener {
        C4657q() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10048qy.setVisible(false);
            LoginAddon.this.f10048qy.mo4915cd("content").setText("");
            if (LoginAddon.this.f10043qQ != null) {
                LoginAddon.this.f10043qQ.mo1931n(true);
                LoginAddon.this.f10043qQ = null;
            }
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$p */
    /* compiled from: a */
    class C4656p implements ActionListener {
        C4656p() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (LoginAddon.this.f10044qR != null) {
                LoginAddon.this.f10044qR.mo1931n(false);
                LoginAddon.this.f10044qR = null;
            }
            LoginAddon.this.f10028qB.exitGame();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$o */
    /* compiled from: a */
    class C4655o implements ActionListener {
        C4655o() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10042qP.setVisible(false);
            LoginAddon.this.f10042qP.mo4915cd("content").setText("");
            if (LoginAddon.this.f10044qR != null) {
                LoginAddon.this.f10044qR.mo1931n(true);
                LoginAddon.this.f10044qR = null;
            }
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$n */
    /* compiled from: a */
    class C4654n extends Thread {
        private final /* synthetic */ String hYe;
        private final /* synthetic */ InternalFrame hYf;
        private final /* synthetic */ C4640a hYg;
        private final /* synthetic */ CardLayout hYh;
        private final /* synthetic */ C2698il hYi;
        private final /* synthetic */ JButton hYj;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4654n(String str, String str2, InternalFrame nxVar, C4640a aVar, CardLayout cardLayout, C2698il ilVar, JButton jButton) {
            super(str);
            this.hYe = str2;
            this.hYf = nxVar;
            this.hYg = aVar;
            this.hYh = cardLayout;
            this.hYi = ilVar;
            this.hYj = jButton;
        }

        public void run() {
            String a = C5673aSj.m18227a("http://www.taikodom.com.br/" + this.hYe, "locale=" + I18NString.getCurrentLocation(), new File("data/web-cache/"));
            EditorPane cd = this.hYf.mo4915cd("content");
            if (a != null) {
                cd.setText(a);
            } else {
                cd.setText(LoginAddon.this.f10026kj.translate("Error retrieving Terms of Use"));
            }
            this.hYg.cancel();
            this.hYh.show(this.hYi, "contents");
            this.hYj.setEnabled(true);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$l */
    /* compiled from: a */
    class C4652l extends Thread {
        C4652l(String str) {
            super(str);
        }

        public void run() {
            String str;
            try {
                str = LoginAddon.this.f10028qB.requestServerStatus();
            } catch (RuntimeException e) {
                e.printStackTrace();
                str = "Could not connect to server.";
            }
            if (LoginAddon.this.f10033qG != null) {
                LoginAddon.this.f10033qG.mo4915cd("content").setText(str);
                LoginAddon.this.f10033qG.setVisible(true);
            }
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$m */
    /* compiled from: a */
    class C4653m implements ActionListener {
        C4653m() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LoginAddon.this.f10034qH.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$a */
    private class C4640a extends WrapRunnable {

        /* renamed from: qk */
        final JLabel f10053qk;

        /* renamed from: ql */
        final C4643c f10054ql;

        public C4640a(JLabel jLabel) {
            this.f10053qk = jLabel;
            this.f10054ql = new C4643c(LoginAddon.this.f10026kj.bHv().adz().getImage(String.valueOf(C5378aHa.LOGIN.getPath()) + "loading_icon"));
            jLabel.setIcon(this.f10054ql);
        }

        public void start() {
            this.f10054ql.start();
            LoginAddon.this.f10026kj.alf().addTask("UpdateTargetPositiontask", this, 10);
        }

        public void run() {
            this.f10053qk.repaint();
        }
    }

    /* renamed from: taikodom.addon.login.LoginAddon$c */
    /* compiled from: a */
    private class C4643c extends ImageIcon {
        private static final int BORDER = 6;


        /* renamed from: cW */
        private boolean f10057cW;

        public C4643c(Image image) {
            super(image);
        }

        public int getIconHeight() {
            return LoginAddon.super.getIconHeight() + 6;
        }

        public int getIconWidth() {
            return LoginAddon.super.getIconWidth() + 6;
        }

        public void start() {
            this.f10057cW = true;
        }

        public void stop() {
            this.f10057cW = false;
        }

        public synchronized void paintIcon(Component component, Graphics graphics, int i, int i2) {
            Graphics2D create = graphics.create();
            create.translate(3, 3);
            if (this.f10057cW) {
                create.rotate((((double) ((System.currentTimeMillis() / 10) % 360)) * 3.141592653589793d) / 180.0d, (double) ((getImage().getWidth(component) / 2) + i), (double) ((getImage().getHeight(component) / 2) + i2));
            } else {
                create.rotate(ScriptRuntime.NaN);
            }
            create.drawImage(getImage(), i, i2, component);
        }
    }
}
