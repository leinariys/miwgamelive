package taikodom.addon.config;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C2922lp;
import game.network.message.externalizable.C3689to;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.aaa.C6105agZ;
import logic.render.IEngineGraphics;
import logic.res.ConfigManager;
import logic.res.ConfigManagerSection;
import logic.ui.item.InternalFrame;
import logic.ui.item.Table;
import logic.ui.item.aOA;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.input.CommandTranslatorAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.addon.neo.preferences.GUIPrefAddon;
import taikodom.game.input.C6983axw;
import taikodom.infra.script.I18NString;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

@TaikodomAddon("taikodom.addon.config")
/* compiled from: a */
public class ConfigAddon implements C2495fo {
    private static final String aDD = "config.xml";
    private static final String fdn = "assign-key.xml";
    /* access modifiers changed from: private */
    public static IEngineGraphics fcy = null;
    /* access modifiers changed from: private */
    public float fcA;
    /* access modifiers changed from: private */
    public float fcB;
    /* access modifiers changed from: private */
    public float fcC;
    /* access modifiers changed from: private */
    public float fcD;
    /* access modifiers changed from: private */
    public float fcE;
    /* access modifiers changed from: private */
    public JComboBox fcX;
    /* access modifiers changed from: private */
    public C2581hD fcz;
    /* access modifiers changed from: private */
    public Table fdo;
    /* access modifiers changed from: private */
    public Map<String, String> fdp;
    /* access modifiers changed from: private */
    public Map<String, String> fdq;
    /* access modifiers changed from: private */
    public Object fdt;
    /* access modifiers changed from: private */
    public int fdu;
    /* access modifiers changed from: private */
    public String fdw;
    /* renamed from: kj */
    public IAddonProperties f9824kj;
    /* renamed from: nM */
    public InternalFrame f9825nM;
    InternalFrame fdr;
    private C3428rT<C5783aaP> aoN;
    private JButton aoP;
    private C6622aqW dcE;
    private JLabel fcF;
    private JLabel fcG;
    private JLabel fcH;
    private JLabel fcI;
    private JLabel fcJ;
    private JScrollBar fcK;
    private JScrollBar fcL;
    private JScrollBar fcM;
    private JScrollBar fcN;
    private JScrollBar fcO;
    private JButton fcP;
    private JComboBox fcQ;
    private JComboBox fcR;
    private JComboBox fcS;
    private JComboBox fcT;
    private JComboBox fcU;
    private JComboBox fcV;
    private JComboBox fcW;
    private JComboBox fcY;
    private JComboBox fcZ;
    private JComboBox fda;
    private JComboBox fdb;
    private C3428rT<C6105agZ> fdc;
    private ActionListener fdd;
    private ActionListener fde;
    private ActionListener fdf;
    private ActionListener fdg;
    private AdjustmentListener fdh;
    private AdjustmentListener fdi;
    private AdjustmentListener fdj;
    private AdjustmentListener fdk;
    private AdjustmentListener fdl;
    private boolean fdm = false;
    private RowFilter<TableRowSorter<TableModel>, String> fds;
    /* access modifiers changed from: private */
    private String fdv;
    /* access modifiers changed from: private */
    private C3428rT<C0963OA> fdx;
    /* renamed from: wz */
    private C3428rT<C3689to> f9826wz;

    public static String[] split(String str, String str2) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, str2);
        String[] strArr = new String[stringTokenizer.countTokens()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = stringTokenizer.nextToken();
        }
        return strArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        fcy = addonPropertie.getEngineGame().getEngineGraphics();
        this.f9824kj = addonPropertie;
        this.dcE = new C4284x("config", this.f9824kj.translate("Config"), "config");
        this.f9824kj.mo2322a("config", (Action) this.dcE);
        this.fdc = new C4283w();
        this.f9824kj.aVU().mo13965a(C6105agZ.class, this.fdc);
    }

    /* renamed from: iM */
    private void m43092iM() {
        this.fcU = this.f9825nM.mo4915cd("language");
        this.fcX = this.f9825nM.mo4915cd("pre-configuration");
        this.fcY = this.f9825nM.mo4915cd("resolution");
        this.fcT = this.f9825nM.mo4915cd("full-screen");
        this.fdb = this.f9825nM.mo4915cd("texture-quality");
        this.fda = this.f9825nM.mo4915cd("texture-filtering");
        this.fcR = this.f9825nM.mo4915cd("anti-aliasing");
        this.fcW = this.f9825nM.mo4915cd("post-processing-fx");
        this.fcZ = this.f9825nM.mo4915cd("shader-quality");
        this.fcQ = this.f9825nM.mo4915cd("anisotropic-filtering");
        this.fcV = this.f9825nM.mo4915cd("object-quality");
        this.fcS = this.f9825nM.mo4915cd("ambient-fx");
        this.fcJ = this.f9825nM.mo4917cf("music");
        this.fcF = this.f9825nM.mo4917cf("ambience");
        this.fcI = this.f9825nM.mo4917cf("interface");
        this.fcG = this.f9825nM.mo4917cf("effects");
        this.fcH = this.f9825nM.mo4917cf("engine");
        this.fcP = this.f9825nM.mo4913cb("save");
        this.aoP = this.f9825nM.mo4913cb("cancel");
        this.fcK = this.f9825nM.mo4915cd("music-slider");
        this.fcL = this.f9825nM.mo4915cd("ambience-slider");
        this.fcM = this.f9825nM.mo4915cd("interface-slider");
        this.fcN = this.f9825nM.mo4915cd("effects-slider");
        this.fcO = this.f9825nM.mo4915cd("engine-slider");
    }

    /* renamed from: Fa */
    private void m43063Fa() {
        this.fdg = new C4258B();
        this.fdd = new C4257A();
        this.fdf = new C4286z();
        this.fde = new C4285y();
        this.fdh = new C4280t();
        this.fdi = new C4281u();
        this.fdj = new C4282v();
        this.fdk = new C4260a();
        this.fdl = new C4261b();
        this.f9826wz = new C4264e();
        this.aoN = new C4265f();
        this.fcX.addActionListener(this.fdg);
        this.fcP.addActionListener(this.fdf);
        this.aoP.addActionListener(this.fde);
        this.fcY.addActionListener(this.fdd);
        this.fcT.addActionListener(this.fdd);
        this.fdb.addActionListener(this.fdd);
        this.fda.addActionListener(this.fdd);
        this.fcR.addActionListener(this.fdd);
        this.fcW.addActionListener(this.fdd);
        this.fcZ.addActionListener(this.fdd);
        this.fcQ.addActionListener(this.fdd);
        this.fcV.addActionListener(this.fdd);
        this.fcS.addActionListener(this.fdd);
        this.fcK.addAdjustmentListener(this.fdh);
        this.fcL.addAdjustmentListener(this.fdi);
        this.fcM.addAdjustmentListener(this.fdj);
        this.fcN.addAdjustmentListener(this.fdk);
        this.fcO.addAdjustmentListener(this.fdl);
        this.f9824kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f9824kj.aVU().mo13965a(C3689to.class, this.f9826wz);
    }

    /* access modifiers changed from: private */
    public void bPd() {
        if (!this.fdm) {
            bPf();
            if (this.fcz.mo19192c(C2581hD.ixx)) {
                this.fcX.setSelectedIndex(0);
            } else if (this.fcz.mo19192c(C2581hD.ixy)) {
                this.fcX.setSelectedIndex(1);
            } else if (this.fcz.mo19192c(C2581hD.ixz)) {
                this.fcX.setSelectedIndex(2);
            } else {
                this.fcX.setSelectedIndex(3);
            }
        }
    }

    private void bPe() {
        this.fcU.addItem(new C4259C(I18NString.DEFAULT_LOCATION, this.f9824kj.translate("English")));
        this.fcU.addItem(new C4259C("pt", this.f9824kj.translate("Portuguese")));
        this.fcX.addItem(new C4259C(C2581hD.ixx, this.f9824kj.translate("Low quality configuration")));
        this.fcX.addItem(new C4259C(C2581hD.ixy, this.f9824kj.translate("Medium configuration")));
        this.fcX.addItem(new C4259C(C2581hD.ixz, this.f9824kj.translate("High quality configuration")));
        this.fcX.addItem(new C4259C(this.fcz, this.f9824kj.translate("Customized configuration")));
        for (int i = 0; i < fcy.ael(); i++) {
            Vec3f eH = fcy.mo3064eH(i);
            this.fcY.addItem(new C4259C(eH, new String(String.valueOf((int) eH.getX()) + GUIPrefAddon.C4817c.X + ((int) eH.getY()) + " (" + ((int) eH.getZ()) + " Hz)")));
        }
        this.fcT.addItem(new C4259C(Boolean.TRUE, this.f9824kj.translate("Full-screen On")));
        this.fcT.addItem(new C4259C(Boolean.FALSE, this.f9824kj.translate("Full-screen Off")));
        this.fdb.addItem(new C4259C(C2581hD.C2585d.VERY_LOW, this.f9824kj.translate("Very Low texture quality")));
        this.fdb.addItem(new C4259C(C2581hD.C2585d.LOW, this.f9824kj.translate("Low texture quality")));
        this.fdb.addItem(new C4259C(C2581hD.C2585d.HIGH, this.f9824kj.translate("High texture quality")));
        this.fdb.addItem(new C4259C(C2581hD.C2585d.VERY_HIGH, this.f9824kj.translate("Very High texture quality")));
        this.fda.addItem(new C4259C(C2581hD.C2582a.LOW, this.f9824kj.translate("Low quality texture filter")));
        this.fda.addItem(new C4259C(C2581hD.C2582a.MEDIUM, this.f9824kj.translate("Medium quality texture filter")));
        this.fda.addItem(new C4259C(C2581hD.C2582a.HIGH, this.f9824kj.translate("High quality texture filter")));
        this.fcR.addItem(new C4259C(C2581hD.C2588g.OFF, this.f9824kj.translate("Antialiasing filter Off")));
        this.fcR.addItem(new C4259C(C2581hD.C2588g.LOW, this.f9824kj.translate("Antialiasing filter Low")));
        this.fcR.addItem(new C4259C(C2581hD.C2588g.MEDIUM, this.f9824kj.translate("Antialiasing filter Medium")));
        this.fcR.addItem(new C4259C(C2581hD.C2588g.HIGH, this.f9824kj.translate("Antialiasing filter High")));
        this.fcR.addItem(new C4259C(C2581hD.C2588g.VERY_HIGH, this.f9824kj.translate("Antialiasing filter Very High")));
        this.fcW.addItem(new C4259C(Boolean.TRUE, this.f9824kj.translate("Post-processing FX On")));
        this.fcW.addItem(new C4259C(Boolean.FALSE, this.f9824kj.translate("Post-processing FX Off")));
        this.fcZ.addItem(new C4259C(C2581hD.C2586e.LOW, this.f9824kj.translate("Shader programs quality Low")));
        this.fcZ.addItem(new C4259C(C2581hD.C2586e.MEDIUM, this.f9824kj.translate("Shader programs quality Medium")));
        this.fcZ.addItem(new C4259C(C2581hD.C2586e.HIGH, this.f9824kj.translate("Shader programs quality High")));
        this.fcQ.addItem(new C4259C(C2581hD.C2587f.OFF, this.f9824kj.translate("Anisotropic filtering Off")));
        this.fcQ.addItem(new C4259C(C2581hD.C2587f.LOW, this.f9824kj.translate("Anisotropic filtering Low")));
        this.fcQ.addItem(new C4259C(C2581hD.C2587f.MEDIUM, this.f9824kj.translate("Anisotropic filtering Medium")));
        this.fcQ.addItem(new C4259C(C2581hD.C2587f.HIGH, this.f9824kj.translate("Anisotropic filtering High")));
        this.fcQ.addItem(new C4259C(C2581hD.C2587f.VERY_HIGH, this.f9824kj.translate("Anisotropic filtering Very High")));
        this.fcV.addItem(new C4259C(C2581hD.C2584c.LOW, this.f9824kj.translate("Object quality Low")));
        this.fcV.addItem(new C4259C(C2581hD.C2584c.MEDIUM, this.f9824kj.translate("Object quality Medium")));
        this.fcV.addItem(new C4259C(C2581hD.C2584c.HIGH, this.f9824kj.translate("Object quality High")));
        this.fcS.addItem(new C4259C(C2581hD.C2583b.LOW, this.f9824kj.translate("Environmental FX Low")));
        this.fcS.addItem(new C4259C(C2581hD.C2583b.MEDIUM, this.f9824kj.translate("Environmental FX Medium")));
        this.fcS.addItem(new C4259C(C2581hD.C2583b.HIGH, this.f9824kj.translate("Environmental FX High")));
    }

    /* access modifiers changed from: private */
    public void bPf() {
        Vec3f vec3f;
        C4259C c = (C4259C) this.fcY.getSelectedItem();
        if (c == null) {
            vec3f = C2581hD.ixy.dmL();
        } else {
            vec3f = (Vec3f) c.getObject();
        }
        this.fcz = new C2581hD((int) vec3f.getX(), (int) vec3f.getY(), (int) vec3f.getZ(), ((Boolean) ((C4259C) this.fcT.getSelectedItem()).getObject()).booleanValue(), (C2581hD.C2585d) ((C4259C) this.fdb.getSelectedItem()).getObject(), (C2581hD.C2582a) ((C4259C) this.fda.getSelectedItem()).getObject(), (C2581hD.C2588g) ((C4259C) this.fcR.getSelectedItem()).getObject(), ((Boolean) ((C4259C) this.fcW.getSelectedItem()).getObject()).booleanValue(), (C2581hD.C2586e) ((C4259C) this.fcZ.getSelectedItem()).getObject(), (C2581hD.C2587f) ((C4259C) this.fcQ.getSelectedItem()).getObject(), (C2581hD.C2584c) ((C4259C) this.fcV.getSelectedItem()).getObject(), (C2581hD.C2583b) ((C4259C) this.fcS.getSelectedItem()).getObject());
        this.fcz.setLanguage((String) ((C4259C) this.fcU.getSelectedItem()).getObject());
        this.fcz.mo19205dq(this.fcE);
        this.fcz.mo19193dm(this.fcA);
        this.fcz.mo19204dp(this.fcD);
        this.fcz.mo19203do(this.fcB);
        this.fcz.mo19202dn(this.fcC);
    }

    private void removeListeners() {
        this.f9824kj.aVU().mo13968b(C5783aaP.class, this.aoN);
        this.f9824kj.aVU().mo13968b(C3689to.class, this.f9826wz);
    }

    public void stop() {
        this.f9824kj.aVU().mo13968b(C6105agZ.class, this.fdc);
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f9825nM != null) {
            close();
            return;
        }
        this.f9825nM = this.f9824kj.bHv().mo16792bN(aDD);
        m43092iM();
        this.fcz = C0598IS.m5381a(this.f9824kj.getEngineGame().getConfigManager());
        bPe();
        this.fcU.setSelectedIndex(m43093ir(this.fcz.getLanguage()));
        m43066a(this.fcz);
        m43078b(this.fcz);
        bPd();
        m43063Fa();
        bPn();
        this.f9825nM.setVisible(true);
        this.f9825nM.center();
        this.f9825nM.moveToFront();
    }

    /* renamed from: ir */
    private int m43093ir(String str) {
        return str.equals(I18NString.DEFAULT_LOCATION) ? 0 : 1;
    }

    /* access modifiers changed from: private */
    /* renamed from: Ia */
    public void m43064Ia() {
        if (this.f9825nM != null) {
            this.f9825nM.destroy();
            this.f9825nM = null;
            removeListeners();
        }
    }

    /* access modifiers changed from: protected */
    public void close() {
        m43064Ia();
    }

    /* access modifiers changed from: private */
    public void bPg() {
        ((MessageDialogAddon) this.f9824kj.mo11830U(MessageDialogAddon.class)).mo24307a(this.f9824kj.translate("You may need to restart your game for some changes to take effect."), (aEP) new C4262c(), this.f9824kj.translate("OK"));
    }

    /* renamed from: av */
    private int m43076av(Vec3f vec3f) {
        for (int i = 0; i < fcy.ael(); i++) {
            if (vec3f.equals(fcy.mo3064eH(i))) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public void bPh() {
        fcy.setShaderQuality(this.fcz.dmP().getValue());
        fcy.setLodQuality(this.fcz.dmR().getValue());
        fcy.mo3066eJ(this.fcz.dmS().getValue());
        fcy.setTextureQuality(this.fcz.dmM().getValue());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43066a(C2581hD hDVar) {
        int i = 1;
        this.fdm = true;
        this.fcY.setSelectedIndex(m43076av(hDVar.dmL()));
        this.fcT.setSelectedIndex(hDVar.isFullscreen() ? 0 : 1);
        this.fdb.setSelectedIndex(hDVar.dmM().ordinal());
        this.fda.setSelectedIndex(hDVar.dmN().ordinal());
        this.fcR.setSelectedIndex(hDVar.dmO().ordinal());
        JComboBox jComboBox = this.fcW;
        if (hDVar.isPostProcessingFX()) {
            i = 0;
        }
        jComboBox.setSelectedIndex(i);
        this.fcZ.setSelectedIndex(hDVar.dmP().ordinal());
        this.fcQ.setSelectedIndex(hDVar.dmQ().ordinal());
        this.fcV.setSelectedIndex(hDVar.dmR().ordinal());
        this.fcS.setSelectedIndex(hDVar.dmS().ordinal());
        this.fdm = false;
    }

    /* access modifiers changed from: private */
    public void bPi() {
        this.fcF.setText(String.valueOf((int) (this.fcA * 100.0f)) + "%");
    }

    /* access modifiers changed from: private */
    public void bPj() {
        this.fcG.setText(String.valueOf((int) (this.fcB * 100.0f)) + "%");
    }

    /* access modifiers changed from: private */
    public void bPk() {
        this.fcH.setText(String.valueOf((int) (this.fcC * 100.0f)) + "%");
    }

    /* access modifiers changed from: private */
    public void bPl() {
        this.fcI.setText(String.valueOf((int) (this.fcD * 100.0f)) + "%");
    }

    /* access modifiers changed from: private */
    public void bPm() {
        this.fcJ.setText(String.valueOf((int) (this.fcE * 100.0f)) + "%");
    }

    /* renamed from: b */
    private void m43078b(C2581hD hDVar) {
        this.fcE = hDVar.aek();
        this.fcA = hDVar.adX();
        this.fcD = hDVar.aeg();
        this.fcB = hDVar.aed();
        this.fcC = hDVar.aeb();
        this.fcK.setValue(Math.round(this.fcE * 100.0f));
        this.fcL.setValue(Math.round(this.fcA * 100.0f));
        this.fcM.setValue(Math.round(this.fcD * 100.0f));
        this.fcN.setValue(Math.round(this.fcB * 100.0f));
        this.fcO.setValue(Math.round(this.fcC * 100.0f));
        bPm();
        bPi();
        bPl();
        bPj();
        bPk();
    }

    private void bPn() {
        this.fdq = new HashMap();
        this.fdp = new HashMap();
        bPq();
        this.fdo = this.f9825nM.mo4915cd("keysTable");
        this.fdo.setAutoCreateRowSorter(true);
        this.fdo.setModel(new C4263d());
        this.fdo.getRowSorter().setComparator(0, new C4268i());
        bPo();
        this.fdo.getColumnModel().getColumn(0).setCellRenderer(new C4269j());
        this.fdo.getColumnModel().getColumn(1).setCellRenderer(new C4266g());
        this.fdo.addMouseListener(new C4267h());
        this.fds = new C4270k();
        this.fdo.addKeyListener(new C4271l());
        this.f9825nM.mo4913cb("assignKeyButton").setEnabled(false);
        this.fdo.getSelectionModel().addListSelectionListener(new C4273m());
        bPp();
        this.f9825nM.mo4913cb("restoreDefaultButton").addActionListener(new C4274n());
        this.f9825nM.mo4913cb("assignKeyButton").addActionListener(new C4275o());
        C5616aQe.m17577a(this.fdo, 1);
    }

    private void bPo() {
        this.fdo.getRowSorter().toggleSortOrder(0);
    }

    /* access modifiers changed from: private */
    public void bPp() {
        this.fdo.getRowSorter().setRowFilter(this.fds);
    }

    private void bPq() {
        bPu();
    }

    /* access modifiers changed from: private */
    /* renamed from: ai */
    public void m43074ai(Object obj) {
        this.fdt = obj;
        this.fdu = -1;
        this.fdv = null;
        this.fdw = null;
        this.fdr = this.f9824kj.bHv().mo16792bN(fdn);
        this.fdr.mo4917cf("actionInfoLabel").setText(this.f9824kj.mo11834a((Class<?>) C6983axw.class, (String) obj, new Object[0]));
        this.fdx = new C4276p();
        this.fdr.addMouseListener(new C4277q());
        this.fdr.addMouseWheelListener(new C4278r());
        this.fdr.addKeyListener(new C4279s());
        this.f9824kj.aVU().mo13965a(C0963OA.class, this.fdx);
        this.fdr.setVisible(true);
        this.fdr.center();
        this.fdr.moveToFront();
    }

    /* access modifiers changed from: private */
    public void bPr() {
        this.f9824kj.aVU().mo13964a(this.fdx);
        this.fdr.destroy();
        this.fdr = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: an */
    public boolean m43075an(String str, String str2) {
        String str3;
        if (this.fdu != -1) {
            str2 = String.valueOf(C6724asU.m25696ut(this.fdu)) + "_" + str2;
        }
        boolean z = false;
        for (Map.Entry next : this.fdp.entrySet()) {
            String[] split = split((String) next.getValue(), ",");
            boolean z2 = z;
            for (String equals : split) {
                if (str2.equals(equals)) {
                    String[] split2 = split((String) next.getValue(), ",");
                    for (int i = 0; i < split2.length; i++) {
                        if (str2.equals(split2[i])) {
                            split2[i] = null;
                        }
                    }
                    String str4 = "";
                    for (int i2 = 0; i2 < split2.length; i2++) {
                        if (split2[i2] != null) {
                            str4 = String.valueOf(str4) + split2[i2] + ",";
                        }
                    }
                    if (str4.endsWith(",")) {
                        str3 = str4.substring(0, str4.length() - 1);
                    } else {
                        str3 = str4;
                    }
                    this.fdp.remove(next.getKey());
                    this.fdp.put((String) next.getKey(), str3);
                    this.fdv = (String) next.getKey();
                    z2 = true;
                }
            }
            if (z2) {
                break;
            }
            z = z2;
        }
        String str5 = (String) this.fdt;
        m43079b(this.fdp, str2, str5);
        this.fdq.put(str2, str5);
        this.fdu = -1;
        return true;
    }

    /* access modifiers changed from: private */
    public void bPs() {
        if (this.fdv != null) {
            ((MessageDialogAddon) this.f9824kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, C5956adg.format(this.f9824kj.translate("The key [{0}] is not assigned anymore."), this.f9824kj.mo11834a((Class<?>) C6983axw.class, this.fdv, new Object[0])), this.f9824kj.translate("Ok"));
        }
    }

    /* access modifiers changed from: private */
    public void bPt() {
        if (this.fdw != null) {
            ((MessageDialogAddon) this.f9824kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, C5956adg.format(this.f9824kj.translate("The key [{0}] is a modifier one and should conflict with ather funcionalities."), this.f9824kj.mo11834a((Class<?>) C6983axw.class, this.fdw, new Object[0])), this.f9824kj.translate("Ok"));
        }
    }

    /* access modifiers changed from: private */
    public String getKeyName(int i) {
        return KeyEvent.getKeyText(i);
    }

    private void bPu() {
        ConfigManager aao = new ConfigManager();
        ConfigManager aao2 = new ConfigManager();
        try {
            aao.loadConfig("config/defaults.ini");
            aao2.loadConfig("config/defaults.ini");
            ConfigManagerSection kW = aao2.getSection("input");
            aao.loadConfig("config/user.ini");
            ConfigManagerSection kW2 = aao.getSection("input");
            this.fdp.clear();
            this.fdq.clear();
            for (Map.Entry next : kW2.getOptions().entrySet()) {
                if (((ayQ) next.getValue()).getType() == String.class) {
                    String str = (String) ((ayQ) next.getValue()).getValue();
                    String str2 = (String) next.getKey();
                    this.fdq.put(str2, str);
                    String[] split = split(str, ",");
                    String str3 = str;
                    for (int i = 1; i < split.length; i++) {
                        m43079b(this.fdp, str2, split[i - 1]);
                        str3 = split[i];
                    }
                    if (!this.fdp.containsKey(str3)) {
                        this.fdp.put(str3, str2);
                    } else {
                        this.fdp.put(str3, String.valueOf(this.fdp.get(str3)) + "," + str2);
                    }
                }
            }
            for (Map.Entry next2 : kW.getOptions().entrySet()) {
                if (((ayQ) next2.getValue()).getType() == String.class) {
                    String str4 = (String) ((ayQ) next2.getValue()).getValue();
                    if (!this.fdp.containsKey(str4)) {
                        this.fdp.put(str4, "");
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void bPv() {
        this.fdo.clear();
        ConfigManager aao = new ConfigManager();
        try {
            aao.loadConfig("config/defaults.ini");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        ConfigManagerSection kW = aao.getSection("input");
        this.fdp.clear();
        this.fdq.clear();
        for (Map.Entry next : kW.getOptions().entrySet()) {
            if (((ayQ) next.getValue()).getType() == String.class) {
                String str = (String) ((ayQ) next.getValue()).getValue();
                String str2 = (String) next.getKey();
                this.fdq.put(str2, str);
                String[] split = split(str, ",");
                String str3 = str;
                for (int i = 1; i < split.length; i++) {
                    m43079b(this.fdp, str2, split[i - 1]);
                    str3 = split[i];
                }
                if (!this.fdp.containsKey(str3)) {
                    this.fdp.put(str3, str2);
                } else {
                    this.fdp.put(str3, String.valueOf(this.fdp.get(str3)) + "," + str2);
                }
            }
        }
        bPo();
        bPo();
        bPp();
    }

    /* renamed from: w */
    private void m43108w(Map<String, String> map) {
        ConfigManagerSection kW = this.f9824kj.getEngineGame().getConfigManager().getSection("input");
        map.remove((Object) null);
        map.remove("");
        for (Map.Entry next : map.entrySet()) {
            kW.mo7190X((String) next.getKey(), (String) next.getValue());
        }
        try {
            this.f9824kj.getEngineGame().getConfigManager().loadConfigIni("config/user.ini");
        } catch (IOException e) {
            e.printStackTrace();
        }
        ((CommandTranslatorAddon) this.f9824kj.mo11830U(CommandTranslatorAddon.class)).mo24074pk();
    }

    /* access modifiers changed from: private */
    public void bPw() {
        m43108w(this.fdq);
    }

    /* renamed from: a */
    private void m43067a(Map<String, String> map, String str, String str2) {
        String[] split = split(str, ",");
        for (int i = 1; i < split.length; i++) {
            m43079b(map, str2, split[i - 1]);
            str = split[i];
        }
        map.put(str, str2);
    }

    /* renamed from: b */
    private void m43079b(Map<String, String> map, String str, String str2) {
        if (str != null && !"".equals(str)) {
            if (!map.containsKey(str2)) {
                map.put(str2, str);
                return;
            }
            String[] split = split(map.get(str2), ",");
            int i = 0;
            while (i < split.length) {
                if (!split[i].equals(str)) {
                    i++;
                } else {
                    return;
                }
            }
            if (!"".equals(map.get(str2))) {
                map.put(str2, String.valueOf(map.get(str2)) + "," + str);
            } else {
                map.put(str2, str);
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$C */
    /* compiled from: a */
    private class C4259C<T> {
        private String name;
        private T object;

        public C4259C(T t, String str) {
            this.name = str;
            this.object = t;
        }

        public T getObject() {
            return this.object;
        }

        public String toString() {
            return this.name;
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$x */
    /* compiled from: a */
    class C4284x extends C6622aqW {


        C4284x(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            ConfigAddon.this.open();
        }

        public boolean isActive() {
            return ConfigAddon.this.f9825nM != null && ConfigAddon.this.f9825nM.isVisible();
        }

        public boolean isEnabled() {
            return true;
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$w */
    /* compiled from: a */
    class C4283w extends C3428rT<C6105agZ> {
        C4283w() {
        }

        /* renamed from: a */
        public void mo321b(C6105agZ agz) {
            ConfigAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$B */
    /* compiled from: a */
    class C4258B implements ActionListener {
        C4258B() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (ConfigAddon.this.fcX.getSelectedIndex() != 3) {
                ConfigAddon.this.m43066a((C2581hD) ((C4259C) ConfigAddon.this.fcX.getSelectedItem()).getObject());
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$A */
    /* compiled from: a */
    class C4257A implements ActionListener {
        C4257A() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ConfigAddon.this.bPd();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$z */
    /* compiled from: a */
    class C4286z implements ActionListener {
        C4286z() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ConfigAddon.this.bPf();
            boolean a = C0598IS.m5387a(ConfigAddon.this.f9824kj.getEngineGame().getConfigManager(), ConfigAddon.this.fcz);
            ConfigAddon.this.bPh();
            ConfigAddon.this.bPw();
            ConfigAddon.this.m43064Ia();
            if (a) {
                ConfigAddon.this.bPg();
            }
            ConfigAddon.this.f9824kj.aVU().mo13975h(new C2922lp());
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$y */
    /* compiled from: a */
    class C4285y implements ActionListener {
        C4285y() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ConfigAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$t */
    /* compiled from: a */
    class C4280t implements AdjustmentListener {
        C4280t() {
        }

        public void adjustmentValueChanged(AdjustmentEvent adjustmentEvent) {
            ConfigAddon.this.fcE = ((float) ((JScrollBar) adjustmentEvent.getSource()).getValue()) / 100.0f;
            ConfigAddon.fcy.mo3061dq(ConfigAddon.this.fcE);
            ConfigAddon.this.bPm();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$u */
    /* compiled from: a */
    class C4281u implements AdjustmentListener {
        C4281u() {
        }

        public void adjustmentValueChanged(AdjustmentEvent adjustmentEvent) {
            ConfigAddon.this.fcA = ((float) ((JScrollBar) adjustmentEvent.getSource()).getValue()) / 100.0f;
            ConfigAddon.fcy.mo3057dm(ConfigAddon.this.fcA);
            ConfigAddon.this.bPi();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$v */
    /* compiled from: a */
    class C4282v implements AdjustmentListener {
        C4282v() {
        }

        public void adjustmentValueChanged(AdjustmentEvent adjustmentEvent) {
            ConfigAddon.this.fcD = ((float) ((JScrollBar) adjustmentEvent.getSource()).getValue()) / 100.0f;
            ConfigAddon.fcy.mo3060dp(ConfigAddon.this.fcD);
            ConfigAddon.this.bPl();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$a */
    class C4260a implements AdjustmentListener {
        C4260a() {
        }

        public void adjustmentValueChanged(AdjustmentEvent adjustmentEvent) {
            ConfigAddon.this.fcB = ((float) ((JScrollBar) adjustmentEvent.getSource()).getValue()) / 100.0f;
            ConfigAddon.fcy.mo3059do(ConfigAddon.this.fcB);
            ConfigAddon.this.bPj();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$b */
    /* compiled from: a */
    class C4261b implements AdjustmentListener {
        C4261b() {
        }

        public void adjustmentValueChanged(AdjustmentEvent adjustmentEvent) {
            ConfigAddon.this.fcC = ((float) ((JScrollBar) adjustmentEvent.getSource()).getValue()) / 100.0f;
            ConfigAddon.fcy.mo3058dn(ConfigAddon.this.fcC);
            ConfigAddon.this.bPk();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$e */
    /* compiled from: a */
    class C4264e extends C3428rT<C3689to> {
        C4264e() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (ConfigAddon.this.f9825nM == null) {
                return;
            }
            if (ConfigAddon.this.fdr != null) {
                ConfigAddon.this.bPr();
                return;
            }
            ConfigAddon.this.close();
            toVar.adC();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$f */
    /* compiled from: a */
    class C4265f extends C3428rT<C5783aaP> {
        C4265f() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            ConfigAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$c */
    /* compiled from: a */
    class C4262c extends aEP {
        C4262c() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            ConfigAddon.this.close();
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.INFO;
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$d */
    /* compiled from: a */
    class C4263d extends AbstractTableModel {


        C4263d() {
        }

        public int getColumnCount() {
            return 2;
        }

        public int getRowCount() {
            return ConfigAddon.this.fdp.size();
        }

        public Object getValueAt(int i, int i2) {
            if (i >= ConfigAddon.this.fdp.keySet().toArray().length) {
                return " - ";
            }
            return ConfigAddon.this.fdp.keySet().toArray()[i];
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return ConfigAddon.this.f9824kj.translate("Action");
                default:
                    return ConfigAddon.this.f9824kj.translate("Key");
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$i */
    /* compiled from: a */
    class C4268i implements Comparator<String> {
        C4268i() {
        }

        public int compare(String str, String str2) {
            return ConfigAddon.this.f9824kj.mo11834a((Class<?>) C6983axw.class, str, new Object[0]).compareTo(ConfigAddon.this.f9824kj.mo11834a((Class<?>) C6983axw.class, str2, new Object[0]));
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$j */
    /* compiled from: a */
    class C4269j extends C2122cE {
        C4269j() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            aOA nR = aur.mo11631nR("actionView");
            String str = (String) obj;
            String str2 = "";
            int indexOf = str.indexOf(44);
            int i3 = 0;
            while (indexOf != -1) {
                String str3 = String.valueOf(str2) + ConfigAddon.this.f9824kj.mo11834a((Class<?>) C6983axw.class, str.substring(i3, indexOf), new Object[0]);
                i3 = indexOf + 1;
                if (i3 != 0) {
                    str3 = String.valueOf(str3) + ",";
                }
                indexOf = str.indexOf(i3, 44);
                if (indexOf == -1) {
                    str2 = String.valueOf(str3) + ConfigAddon.this.f9824kj.mo11834a((Class<?>) C6983axw.class, str.substring(i3), new Object[0]);
                } else {
                    str2 = String.valueOf(str3) + ConfigAddon.this.f9824kj.mo11834a((Class<?>) C6983axw.class, str.substring(i3, indexOf), new Object[0]);
                }
            }
            if (str2.equals("")) {
                str2 = ConfigAddon.this.f9824kj.mo11834a((Class<?>) C6983axw.class, str, new Object[0]);
            }
            nR.setText(str2);
            return nR;
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$g */
    /* compiled from: a */
    class C4266g extends C2122cE {
        C4266g() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            aOA nR = aur.mo11631nR("keyView");
            nR.setText((String) ConfigAddon.this.fdp.get(obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$h */
    /* compiled from: a */
    class C4267h extends MouseAdapter {
        C4267h() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 1 && mouseEvent.getClickCount() == 2) {
                ConfigAddon.this.m43074ai(ConfigAddon.this.fdo.getModel().getValueAt(ConfigAddon.this.fdo.convertRowIndexToModel(ConfigAddon.this.fdo.getSelectedRow()), 0));
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$k */
    /* compiled from: a */
    class C4270k extends RowFilter<TableRowSorter<TableModel>, String> {
        C4270k() {
        }

        public boolean include(RowFilter.Entry entry) {
            String str = (String) entry.getValue(0);
            return str != null && !str.equals("");
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$l */
    /* compiled from: a */
    class C4271l extends KeyAdapter {
        C4271l() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            Object valueAt;
            if ((keyEvent.getKeyCode() == 127 || keyEvent.getKeyCode() == 8) && (valueAt = ConfigAddon.this.fdo.getModel().getValueAt(ConfigAddon.this.fdo.convertRowIndexToModel(ConfigAddon.this.fdo.getSelectedRow()), -1)) != null) {
                String str = (String) valueAt;
                if (ConfigAddon.this.fdp.get(str) != null && !"".equals(ConfigAddon.this.fdp.get(str))) {
                    ((MessageDialogAddon) ConfigAddon.this.f9824kj.mo11830U(MessageDialogAddon.class)).mo24307a(ConfigAddon.this.f9824kj.translate("Do You want to delete the assigned keys?"), (aEP) new C4272a(str), ConfigAddon.this.f9824kj.translate("No"), ConfigAddon.this.f9824kj.translate("Yes"));
                }
            }
        }

        /* renamed from: taikodom.addon.config.ConfigAddon$l$a */
        class C4272a extends aEP {
            private final /* synthetic */ String aMT;

            C4272a(String str) {
                this.aMT = str;
            }

            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                if (i == 1) {
                    String[] split = ConfigAddon.split((String) ConfigAddon.this.fdp.get(this.aMT), ",");
                    if (split.length > 0) {
                        for (String put : split) {
                            ConfigAddon.this.fdq.put(put, "");
                        }
                        ConfigAddon.this.fdp.put(this.aMT, "");
                    }
                }
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                return C0939Nn.C0940a.CONFIRM;
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$m */
    /* compiled from: a */
    class C4273m implements ListSelectionListener {
        C4273m() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting()) {
                ConfigAddon.this.f9825nM.mo4913cb("assignKeyButton").setEnabled(true);
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$n */
    /* compiled from: a */
    class C4274n implements ActionListener {
        C4274n() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ConfigAddon.this.bPv();
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$o */
    /* compiled from: a */
    class C4275o implements ActionListener {
        C4275o() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int selectedRow = ConfigAddon.this.fdo.getSelectedRow();
            if (selectedRow != -1) {
                ConfigAddon.this.m43074ai(ConfigAddon.this.fdo.getModel().getValueAt(ConfigAddon.this.fdo.convertRowIndexToModel(selectedRow), 0));
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$p */
    /* compiled from: a */
    class C4276p extends C3428rT<C0963OA> {
        C4276p() {
        }

        /* renamed from: a */
        public void mo321b(C0963OA oa) {
            if ("UNDEFINED".equals(C3942xA.m40808bf(oa.getButton()))) {
                ConfigAddon.this.bPr();
                return;
            }
            String bf = C3942xA.m40808bf(oa.getButton());
            if (ConfigAddon.this.m43075an((String) ConfigAddon.this.fdp.get(ConfigAddon.this.fdt), bf)) {
                ConfigAddon.this.bPp();
                ConfigAddon.this.bPr();
                ConfigAddon.this.bPs();
                ConfigAddon.this.bPt();
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$q */
    /* compiled from: a */
    class C4277q extends MouseAdapter {
        C4277q() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            if (C6724asU.m25698uv(mouseEvent.getButton()) == null) {
                ConfigAddon.this.bPr();
                return;
            }
            String uv = C6724asU.m25698uv(mouseEvent.getButton());
            if (ConfigAddon.this.m43075an((String) ConfigAddon.this.fdp.get(ConfigAddon.this.fdt), uv)) {
                ConfigAddon.this.bPp();
                ConfigAddon.this.bPr();
                ConfigAddon.this.bPs();
                ConfigAddon.this.bPt();
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$r */
    /* compiled from: a */
    class C4278r implements MouseWheelListener {
        C4278r() {
        }

        public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
            String uw;
            if (mouseWheelEvent.getWheelRotation() < 0) {
                uw = C6724asU.m25699uw(0);
            } else {
                uw = C6724asU.m25699uw(1);
            }
            if (uw == null) {
                ((MessageDialogAddon) ConfigAddon.this.f9824kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, ConfigAddon.this.f9824kj.translate("Invalid key."), ConfigAddon.this.f9824kj.translate("Ok"));
                return;
            }
            if (ConfigAddon.this.m43075an((String) ConfigAddon.this.fdp.get(ConfigAddon.this.fdt), uw)) {
                ConfigAddon.this.bPp();
                ConfigAddon.this.bPr();
                ConfigAddon.this.bPs();
                ConfigAddon.this.bPt();
            }
        }
    }

    /* renamed from: taikodom.addon.config.ConfigAddon$s */
    /* compiled from: a */
    class C4279s extends KeyAdapter {
        C4279s() {
        }

        public void keyPressed(KeyEvent keyEvent) {
            if (C6724asU.m25697uu(keyEvent.getKeyCode())) {
                if (ConfigAddon.this.fdu != keyEvent.getKeyCode()) {
                    ConfigAddon.this.fdr.mo4917cf("modifierInfoLabel").setText(C5956adg.format(ConfigAddon.this.f9824kj.translate("{0} + PRESS A KEY..."), ConfigAddon.this.getKeyName(keyEvent.getKeyCode())));
                }
                ConfigAddon.this.fdu = keyEvent.getKeyCode();
            }
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (C6724asU.m25697uu(keyEvent.getKeyCode())) {
                ConfigAddon.this.fdu = -1;
                ConfigAddon.this.fdr.mo4917cf("modifierInfoLabel").setText("");
                ConfigAddon.this.fdw = C6724asU.m25696ut(keyEvent.getKeyCode());
            }
            if (C6724asU.m25696ut(keyEvent.getKeyCode()) == null) {
                ((MessageDialogAddon) ConfigAddon.this.f9824kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, ConfigAddon.this.f9824kj.translate("Invalid key."), ConfigAddon.this.f9824kj.translate("Ok"));
                return;
            }
            String ut = C6724asU.m25696ut(keyEvent.getKeyCode());
            if (ConfigAddon.this.m43075an((String) ConfigAddon.this.fdp.get(ConfigAddon.this.fdt), ut)) {
                ConfigAddon.this.bPp();
                ConfigAddon.this.bPr();
                ConfigAddon.this.bPs();
                ConfigAddon.this.bPt();
            }
        }
    }
}
