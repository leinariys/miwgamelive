package taikodom.addon.wardrobe;

import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.script.avatar.AvatarManager;
import game.script.avatar.cloth.Cloth;
import game.script.avatar.cloth.ClothCategory;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.baa.C0211Cc;
import logic.baa.aDJ;
import logic.data.link.C1643YC;
import logic.res.code.C5663aRz;
import logic.res.sound.C0907NJ;
import logic.ui.Panel;
import logic.ui.item.*;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.camera.OrbitalCamera;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RGroup;
import taikodom.render.scene.RModel;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.wardrobe")
/* compiled from: a */
public class WardrobeAddon implements C2495fo {
    private static final double gnI = 3.0d;
    private static final double gnJ = 0.5d;
    private static final int gnK = 1;
    private static final double gnL = 0.01d;
    private static final double gnM = 0.85d;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10294P;
    /* access modifiers changed from: private */
    public Component3d aDL;
    /* access modifiers changed from: private */
    public OrbitalCamera bmO;
    /* access modifiers changed from: private */
    public ButtonGroup gnP;
    /* access modifiers changed from: private */
    public C5085k gnQ = C5085k.NO_TASK;
    /* access modifiers changed from: private */
    public JLabel gnT;
    /* access modifiers changed from: private */
    public JLabel gnU;
    /* renamed from: kj */
    public IAddonProperties f10295kj;
    /* renamed from: nM */
    public InternalFrame f10296nM;
    private C3428rT<C5783aaP> aoN;
    private C6622aqW dcE;
    private C0211Cc gnN;
    private Repeater.C3671a<ClothCategory> gnO;
    private C0211Cc gnR;
    private Timer gnS;
    private JLabel gnV;
    private JButton gnW;
    private JButton gnX;
    private JButton gnY;
    /* access modifiers changed from: private */
    private JButton gnZ;
    /* access modifiers changed from: private */
    private Repeater<ClothCategory> goa;
    /* renamed from: wz */
    private C3428rT<C3689to> f10297wz;

    /* renamed from: iM */
    private void m45701iM() {
        this.goa = this.f10296nM.mo4919ch("categoryRepeater");
        this.gnV = this.f10296nM.mo4917cf("characterName");
        this.gnU = this.f10296nM.mo4917cf("balance");
        this.gnT = this.f10296nM.mo4917cf("hoplonCredits");
        this.aDL = this.f10296nM.mo4915cd("avatarViewer");
        this.gnY = this.f10296nM.mo4913cb("zoomIn");
        this.gnZ = this.f10296nM.mo4913cb("zoomOut");
        this.gnW = this.f10296nM.mo4913cb("rotateCCW");
        this.gnX = this.f10296nM.mo4913cb("rotateCW");
    }

    /* renamed from: ND */
    private void m45685ND() {
        this.f10295kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/nod_spa_moon.pro", "nod_spa_moon", (Scene) null, new C5076b(), getClass().getSimpleName());
    }

    /* renamed from: iG */
    private void m45700iG() {
        this.f10297wz = new C5077c();
        this.aoN = new C5078d();
        this.f10295kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f10295kj.aVU().mo13965a(C3689to.class, this.f10297wz);
        this.gnN = new C5079e(this.f10294P.mo5156Qw(), C1643YC.eMu);
        this.gnN.mo1144a(new C5080f());
        this.gnR = new C5081g(this.f10294P.bOx().mo5156Qw(), C1643YC.eMu);
        this.gnR.mo1144a(new C5084j());
        this.gnY.addMouseListener(new C5083i());
        this.gnZ.addMouseListener(new C5082h());
        this.gnW.addMouseListener(new C5094p());
        this.gnX.addMouseListener(new C5095q());
        this.f10296nM.addComponentListener(new C5091m());
    }

    private void cqq() {
        this.gnO = new C5086l();
    }

    /* renamed from: Im */
    private void m45684Im() {
        this.f10296nM = this.f10295kj.bHv().mo16792bN("wardrobe.xml");
        this.gnP = new ButtonGroup();
        m45701iM();
        cqq();
        this.goa.mo22250a(this.gnO);
        this.gnV.setText(this.f10294P.getName());
        m45700iG();
        cqr();
        m45685ND();
    }

    private void cqr() {
        AvatarManager aLa = this.f10295kj.ala().aLa();
        if (aLa != null) {
            for (ClothCategory G : aLa.aFC()) {
                this.goa.mo22248G(G);
            }
            for (Cloth next : aLa.aFA()) {
                this.goa.get(next.bEb()).get(0).mo4915cd("equipmentsTable").getModel().getItems().add(next);
            }
        }
    }

    public void open() {
        if (this.f10296nM == null) {
            m45684Im();
        } else if (this.f10296nM.isVisible()) {
            this.f10296nM.setVisible(false);
            this.f10295kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            return;
        }
        this.f10295kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, getClass()));
        this.f10296nM.setVisible(true);
    }

    @C2602hR(mo19255zf = "WARDROBE_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24662al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10295kj = addonPropertie;
        this.f10294P = this.f10295kj.getPlayer();
        this.dcE = new C5092n("wardrobe", this.f10295kj.translate("Wardrobe"), "wardrobe");
        this.gnS = new Timer(20, new C5093o());
        this.dcE.mo15602jH(this.f10295kj.translate("Here you can change your clothes and buy new ones using your Hoplon Credits."));
        this.dcE.mo15603jI("WARDROBE_WINDOW");
        this.f10295kj.mo2322a("wardrobe", (Action) this.dcE);
        this.f10295kj.aVU().publish(new C5344aFs(this.dcE, 808, false));
        this.f10295kj.mo2317P(this);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m45688a(C5085k kVar) {
        this.gnQ = kVar;
        this.gnS.setInitialDelay(1);
        this.gnS.start();
    }

    public void stop() {
        if (this.f10296nM != null) {
            this.f10296nM.destroy();
            this.f10296nM = null;
        }
        if (this.aDL != null) {
            this.aDL.destroy();
        }
    }

    /* access modifiers changed from: private */
    public void cqs() {
        this.gnQ = C5085k.NO_TASK;
        this.gnS.stop();
    }

    /* access modifiers changed from: private */
    /* renamed from: U */
    public void m45686U(double d) {
        double d2 = this.bmO.getDistance().z;
        double zoomFactor = d2 - ((this.bmO.getZoomFactor() + (0.3d * d2)) * d);
        if (zoomFactor >= gnJ && zoomFactor <= gnI) {
            this.bmO.setDistance(new Vec3d(this.bmO.getDistance().x, (1.0d - (zoomFactor / gnI)) * gnM, zoomFactor));
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$k */
    /* compiled from: a */
    private enum C5085k {
        NO_TASK,
        ROTATE_CCW,
        ROTATE_CW,
        ZOOM_IN,
        ZOOM_OUT
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$a */
    private class C5075a extends AbstractTableModel {
        private static final long serialVersionUID = 4217488683416201880L;

        /* renamed from: hP */
        private final List<Cloth> f10298hP;

        public C5075a() {
            this.f10298hP = new ArrayList();
        }

        public C5075a(List<Cloth> list) {
            this.f10298hP = list;
        }

        public int getColumnCount() {
            return 2;
        }

        public String getColumnName(int i) {
            if (i == 0) {
                return "Item";
            }
            return "Price";
        }

        public List<Cloth> getItems() {
            return this.f10298hP;
        }

        public int getRowCount() {
            return this.f10298hP.size();
        }

        public Object getValueAt(int i, int i2) {
            Cloth wj = this.f10298hP.get(i);
            switch (i2) {
                case 0:
                    return new C1540Wd(wj.mo6530sK(), wj.mo6528ke().get());
                case 1:
                    return wj;
                default:
                    return "";
            }
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$b */
    /* compiled from: a */
    class C5076b implements C0907NJ {
        C5076b() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (WardrobeAddon.this.aDL != null) {
                WardrobeAddon.this.aDL.getScene().removeAllChildren();
                WardrobeAddon.this.aDL.getScene().addChild((SceneObject) renderAsset);
                RGroup rGroup = new RGroup();
                RModel rModel = null;
                if (WardrobeAddon.this.f10294P.bQI() != null) {
                    rModel = WardrobeAddon.this.f10294P.bQI().aOo();
                }
                if (rModel != null) {
                    TransformWrap bcVar = new TransformWrap();
                    bcVar.setTranslation(ScriptRuntime.NaN, -0.9d, ScriptRuntime.NaN);
                    rModel.setTransform(bcVar);
                    rGroup.addChild(rModel);
                    WardrobeAddon.this.f10294P.bQI().mo2026ak(WardrobeAddon.this.f10294P.bQI().mo2030cJ());
                }
                WardrobeAddon.this.aDL.getScene().addChild(rGroup);
                WardrobeAddon.this.aDL.mo16901b((SceneObject) rGroup, 0.0f);
                WardrobeAddon.this.aDL.setVisible(true);
                WardrobeAddon.this.bmO = WardrobeAddon.this.aDL.csq();
                WardrobeAddon.this.bmO.setYaw(180.0f);
                WardrobeAddon.this.bmO.setDistance(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, WardrobeAddon.gnI));
                WardrobeAddon.this.bmO.setFarPlane(100.0f);
                WardrobeAddon.this.bmO.setNearPlane(1.0E-4f);
            }
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$c */
    /* compiled from: a */
    class C5077c extends C3428rT<C3689to> {
        C5077c() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (WardrobeAddon.this.f10296nM != null && WardrobeAddon.this.f10296nM.isVisible()) {
                toVar.adC();
                WardrobeAddon.this.f10296nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$d */
    /* compiled from: a */
    class C5078d extends C3428rT<C5783aaP> {
        C5078d() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (aap.bMO() == C5783aaP.C1841a.ONI && WardrobeAddon.this.f10296nM != null && WardrobeAddon.this.f10296nM.isVisible()) {
                WardrobeAddon.this.f10296nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$e */
    /* compiled from: a */
    class C5079e extends C0211Cc {
        C5079e(aDJ adj, C5663aRz arz) {
            super(adj, arz);
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public Object mo1146d(Object obj) {
            if (!(obj instanceof Long)) {
                return super.mo1146d(obj);
            }
            return String.format("%,d", new Object[]{obj});
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$f */
    /* compiled from: a */
    class C5080f implements C3663tS {
        C5080f() {
        }

        /* renamed from: e */
        public void mo22231e(Object obj) {
            if (WardrobeAddon.this.gnU != null) {
                WardrobeAddon.this.gnU.setText(obj == null ? "" : obj.toString());
            }
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$g */
    /* compiled from: a */
    class C5081g extends C0211Cc {
        C5081g(aDJ adj, C5663aRz arz) {
            super(adj, arz);
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public Object mo1146d(Object obj) {
            if (!(obj instanceof Long)) {
                return super.mo1146d(obj);
            }
            return String.format("%,d", new Object[]{obj});
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$j */
    /* compiled from: a */
    class C5084j implements C3663tS {
        C5084j() {
        }

        /* renamed from: e */
        public void mo22231e(Object obj) {
            String obj2;
            if (WardrobeAddon.this.gnT != null) {
                JLabel f = WardrobeAddon.this.gnT;
                if (obj == null) {
                    obj2 = "";
                } else {
                    obj2 = obj.toString();
                }
                f.setText(obj2);
            }
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$i */
    /* compiled from: a */
    class C5083i extends MouseAdapter {
        C5083i() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            WardrobeAddon.this.m45688a(C5085k.ZOOM_IN);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            WardrobeAddon.this.cqs();
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$h */
    /* compiled from: a */
    class C5082h extends MouseAdapter {
        C5082h() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            WardrobeAddon.this.m45688a(C5085k.ZOOM_OUT);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            WardrobeAddon.this.cqs();
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$p */
    /* compiled from: a */
    class C5094p extends MouseAdapter {
        C5094p() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            WardrobeAddon.this.m45688a(C5085k.ROTATE_CCW);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            WardrobeAddon.this.cqs();
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$q */
    /* compiled from: a */
    class C5095q extends MouseAdapter {
        C5095q() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            WardrobeAddon.this.m45688a(C5085k.ROTATE_CW);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            WardrobeAddon.this.cqs();
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$m */
    /* compiled from: a */
    class C5091m extends ComponentAdapter {
        C5091m() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            WardrobeAddon.this.f10295kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$l */
    /* compiled from: a */
    class C5086l implements Repeater.C3671a<ClothCategory> {
        C5086l() {
        }

        /* renamed from: a */
        public void mo843a(ClothCategory dHVar, Component component) {
            Panel aco = (Panel) component;
            Table cd = aco.mo4915cd("equipmentsTable");
            aOA cf = aco.mo4917cf("categoryTitle");
            JToggleButton cd2 = aco.mo4915cd("showCategory");
            WardrobeAddon.this.gnP.add(cd2);
            cd2.addChangeListener(new C5088b(aco.mo4915cd("itemsView"), aco.mo4915cd("visibilityIconDown"), aco.mo4915cd("visibilityIconUp"), cd));
            cf.addMouseListener(new C5087a(cd2));
            cf.setText(dHVar.mo17733ke().get());
            cd.setModel(new C5075a());
            cd.getColumnModel().getColumn(0).setCellRenderer(new C5090d());
            cd.getColumnModel().getColumn(1).setCellRenderer(new C5089c());
        }

        /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$l$b */
        /* compiled from: a */
        class C5088b implements ChangeListener {
            private final /* synthetic */ Panel dRT;
            private final /* synthetic */ Picture dRU;
            private final /* synthetic */ Picture dRV;
            private final /* synthetic */ Table dRW;

            C5088b(Panel aco, Picture axm, Picture axm2, Table pzVar) {
                this.dRT = aco;
                this.dRU = axm;
                this.dRV = axm2;
                this.dRW = pzVar;
            }

            public void stateChanged(ChangeEvent changeEvent) {
                this.dRT.setVisible(!this.dRT.isVisible());
                if (this.dRT.isVisible()) {
                    this.dRU.setVisible(false);
                    this.dRV.setVisible(true);
                    return;
                }
                this.dRV.setVisible(false);
                this.dRU.setVisible(true);
                this.dRW.clearSelection();
            }
        }

        /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$l$a */
        class C5087a extends MouseAdapter {
            private final /* synthetic */ JToggleButton dRS;

            C5087a(JToggleButton jToggleButton) {
                this.dRS = jToggleButton;
            }

            public void mousePressed(MouseEvent mouseEvent) {
                if (this.dRS.isSelected()) {
                    WardrobeAddon.this.gnP.clearSelection();
                } else {
                    this.dRS.setSelected(true);
                }
            }
        }

        /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$l$d */
        /* compiled from: a */
        class C5090d extends C2122cE {
            C5090d() {
            }

            /* renamed from: a */
            public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
                C1540Wd wd = (C1540Wd) obj;
                Panel nR = aur.mo11631nR("item");
                Picture cd = nR.mo4915cd("itemIcon");
                if (wd.dpG != null) {
                    cd.mo16825aw(((Asset) wd.dpG).getFile());
                }
                nR.mo4917cf("itemName").setText((String) wd.dpH);
                return nR;
            }
        }

        /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$l$c */
        /* compiled from: a */
        class C5089c extends C2122cE {
            C5089c() {
            }

            /* renamed from: a */
            public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
                Cloth wj = (Cloth) obj;
                Panel nR = aur.mo11631nR("price");
                aOA cf = nR.mo4917cf("itemWore");
                Picture cd = nR.mo4915cd("hoplonIcon");
                aOA cf2 = nR.mo4917cf("itemPrice");
                if (WardrobeAddon.this.f10294P.bQI().aOu().contains(wj)) {
                    cf.setVisible(true);
                    cd.setVisible(false);
                    cf2.setVisible(false);
                } else {
                    cf2.setText(String.valueOf(wj.ajU()));
                    cd.setVisible(true);
                    cf2.setVisible(true);
                    cf.setVisible(false);
                }
                return nR;
            }
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$n */
    /* compiled from: a */
    class C5092n extends C6622aqW {
        private static final long serialVersionUID = 6901449631126991296L;

        C5092n(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            WardrobeAddon.this.open();
        }

        public boolean isActive() {
            return WardrobeAddon.this.f10296nM != null && WardrobeAddon.this.f10296nM.isVisible();
        }

        public boolean isEnabled() {
            return true;
        }
    }

    /* renamed from: taikodom.addon.wardrobe.WardrobeAddon$o */
    /* compiled from: a */
    class C5093o implements ActionListener {
        private static /* synthetic */ int[] ekF;

        C5093o() {
        }

        static /* synthetic */ int[] bwd() {
            int[] iArr = ekF;
            if (iArr == null) {
                iArr = new int[C5085k.values().length];
                try {
                    iArr[C5085k.NO_TASK.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5085k.ROTATE_CCW.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5085k.ROTATE_CW.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5085k.ZOOM_IN.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C5085k.ZOOM_OUT.ordinal()] = 5;
                } catch (NoSuchFieldError e5) {
                }
                ekF = iArr;
            }
            return iArr;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            switch (bwd()[WardrobeAddon.this.gnQ.ordinal()]) {
                case 2:
                    WardrobeAddon.this.bmO.setYaw(WardrobeAddon.this.bmO.getYaw() - 1.0f);
                    return;
                case 3:
                    WardrobeAddon.this.bmO.setYaw(WardrobeAddon.this.bmO.getYaw() + 1.0f);
                    return;
                case 4:
                    WardrobeAddon.this.m45686U(WardrobeAddon.gnL);
                    return;
                case 5:
                    WardrobeAddon.this.m45686U(-0.01d);
                    return;
                default:
                    WardrobeAddon.this.cqs();
                    return;
            }
        }
    }
}
