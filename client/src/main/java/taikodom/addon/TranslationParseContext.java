package taikodom.addon;

import game.engine.C6174ahq;
import logic.EngineGame;
import logic.IAddonManager;
import logic.res.XmlNode;
import logic.ui.*;
import logic.ui.item.IBaseItem;

import java.awt.*;
import java.net.URL;

/* renamed from: a.dW */
/* compiled from: a */
public class TranslationParseContext extends C3684tj {
    private static final String[] attributes = {"title", "text", "tooltip"};
    private final IAddonProperties addonProperties;
    /* access modifiers changed from: private */
    public Class<?> eJh;

    public TranslationParseContext(IAddonManager axz, IAddonProperties addonProperties) {
        super(axz, addonProperties);
        this.addonProperties = addonProperties;
    }

    public String toString() {
        return "CTX: " + this.addonProperties + ", SCOPE: " + this.eJh;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends C1276Sr> T mo17857a(Object obj, String str, C2698il ilVar) {
        Object obj2;
        if (obj == null || (obj instanceof URL)) {
            obj2 = this.addonProperties.bHy();
        } else {
            obj2 = obj;
        }
        if (obj2 == null) {
            obj2 = null;
        } else if (!(obj2 instanceof Class)) {
            obj2 = obj2.getClass();
        }
        this.eJh = (Class) obj2;
        return super.mo17857a(obj, str, ilVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends C1276Sr> T mo17858a(URL url, C2698il ilVar) {
        ((EngineGame) this.addonProperties.getEngineGame()).cno().mo20939a(url, new C2276b());
        return super.mo22275a((C6342alC) new C2274a(new MapKeyValue((C6342alC) ((BaseUiTegXml) this.addonManager.getBaseUItagXML()).cnl(), url)), url, ilVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public void mo17859e(XmlNode agy) {
        if (!agy.setFlagBool()) {
            String attribute = agy.getAttribute("ihint");
            for (String str : attributes) {
                String attribute2 = agy.getAttribute(str);
                if (attribute2 != null) {
                    if (attribute != null) {
                        agy.addAttribute(str, this.addonProperties.mo2320a(this.eJh, attribute, attribute2, new Object[0]));
                    } else {
                        agy.addAttribute(str, this.addonProperties.mo11834a(this.eJh, attribute2, new Object[0]));
                    }
                }
            }
            for (XmlNode e : agy.getListChildrenTag()) {
                mo17859e(e);
            }
            agy.getFlagBool();
        }
    }

    /* renamed from: a.dW$a */
    class C2274a extends MapKeyValue {
        public C2274a(C6342alC alc) {
            super(alc, alc.getPathFile());
        }

        /* renamed from: gC */
        public IBaseItem getClassBaseUi(String tegName) {
            IBaseItem gC = super.getClassBaseUi(tegName);
            if (gC == null) {
                return null;
            }
            return new C2275a(gC);
        }

        public String toString() {
            return "TranslationParseContext: " + TranslationParseContext.this.eJh;
        }

        /* renamed from: a.dW$a$a */
        class C2275a implements IBaseItem {

            /* renamed from: Bb */
            private final /* synthetic */ IBaseItem f6537Bb;

            C2275a(IBaseItem jc) {
                this.f6537Bb = jc;
            }

            /* renamed from: d */
            public Component creatUi(C6342alC alc, Container container, XmlNode agy) {
                TranslationParseContext.this.mo17859e(agy);
                return this.f6537Bb.creatUi(alc, container, agy);
            }
        }
    }

    /* renamed from: a.dW$b */
    /* compiled from: a */
    class C2276b implements C6174ahq {
        C2276b() {
        }

        /* renamed from: a */
        public void mo13624a(URL url) {
            TranslationParseContext.this.adA().restart();
        }
    }
}
