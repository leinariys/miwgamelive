package taikodom.addon.mission;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5719aUd;
import game.script.Actor;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.npcchat.NPCChat;
import game.script.npcchat.NPCSpeech;
import game.script.npcchat.PlayerSpeech;
import game.script.npcchat.SpeechAction;
import game.script.npcchat.actions.NurseryTutorialSpeechAction;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.C6245ajJ;
import logic.aaa.C0537HY;
import logic.aaa.C5783aaP;
import logic.swing.C5378aHa;
import logic.ui.Panel;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import logic.ui.item.TextArea;
import p001a.*;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@TaikodomAddon("taikodom.addon.mission")
/* renamed from: a.lC */
/* compiled from: a */
public class C2851lC {

    /* access modifiers changed from: private */
    public C3428rT<C5783aaP> aoN;
    /* access modifiers changed from: private */
    public AgentsAddon bhS;
    /* access modifiers changed from: private */
    public NPC cqw;
    /* access modifiers changed from: private */
    public NPCSpeech hUB;
    /* access modifiers changed from: private */
    public C3428rT<C5719aUd> hUw;
    /* access modifiers changed from: private */
    public C3320qJ hUy;
    /* access modifiers changed from: private */
    public boolean hUz;
    /* renamed from: kj */
    public IAddonProperties f8494kj;
    /* renamed from: wz */
    public C3428rT<C3689to> f8496wz;
    /* renamed from: nM */
    public InternalFrame f8495nM;
    /* access modifiers changed from: private */
    /* renamed from: P */
    private Player f8493P = this.f8494kj.getPlayer();
    private ComponentAdapter hUA;
    /* access modifiers changed from: private */
    private Repeater<PlayerSpeech> hUx;

    public C2851lC(AgentsAddon agentsAddon, IAddonProperties vWVar, NPC auf) {
        this.bhS = agentsAddon;
        this.f8494kj = vWVar;
        this.cqw = auf;
        this.f8493P.bQG().mo20294z(auf);
        m34690iG();
        C6245ajJ aVU = this.f8494kj.aVU();
        aVU.mo13965a(C5719aUd.class, this.hUw);
        aVU.mo13965a(C5783aaP.class, this.aoN);
        aVU.mo13965a(C3689to.class, this.f8496wz);
        NPCType Fs = auf.mo11654Fs();
        if (this.f8493P.bQG().cXc().get(Fs) == null || this.f8493P.bQG().cXc().get(Fs).size() <= 0) {
            m34673C(auf);
        } else {
            this.hUy = new C2863i(agentsAddon, vWVar, Fs, auf);
        }
    }

    /* renamed from: iG */
    private void m34690iG() {
        this.hUw = new C2860f();
        this.aoN = new C2859e();
        this.f8496wz = new C2862h();
        this.hUA = new C2861g();
    }

    /* access modifiers changed from: private */
    /* renamed from: C */
    public void m34673C(NPC auf) {
        m34674D(auf);
        this.f8494kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, this));
        this.f8495nM.center();
        this.f8495nM.setVisible(true);
    }

    /* renamed from: D */
    private void m34674D(NPC auf) {
        this.f8495nM = this.f8494kj.bHv().mo16795c(MissionAddon.class, "npc-chat.xml");
        this.f8495nM.mo4915cd("agent-image").mo2605a((Icon) new ImageIcon(this.f8494kj.bHv().adz().getImage(String.valueOf(C5378aHa.NPC_AVATAR.getPath()) + auf.bTI())));
        this.f8495nM.mo4917cf("agent-name").setText(auf.getName());
        Actor bhE = auf.bhE();
        JLabel cf = this.f8495nM.mo4917cf("location-name");
        if (bhE != null) {
            cf.setVisible(true);
            cf.setText(bhE.getName());
        } else {
            cf.setVisible(false);
        }
        this.hUx = this.f8495nM.mo4919ch("pc-option-list");
        this.hUx.mo22250a(new C2852a());
        m34685d(auf.mo11654Fs().bTK());
        this.f8495nM.addComponentListener(this.hUA);
    }

    public boolean isActive() {
        return this.f8495nM != null && this.f8495nM.isVisible() && !this.hUz;
    }

    public void close() {
        Asset tCVar;
        if (this.hUB != null) {
            tCVar = this.hUB.abx();
        } else {
            tCVar = null;
        }
        if (tCVar != null) {
            this.f8494kj.aVU().mo13975h(new C0208CZ(tCVar, true));
        }
        if (this.hUy != null) {
            this.hUy.close();
        }
        if (this.f8495nM != null) {
            this.f8495nM.destroy();
            this.f8495nM = null;
        }
    }

    /* renamed from: d */
    private void m34685d(NPCChat aaq) {
        this.f8495nM.mo4911Kk();
        ((NPCChat) C3582se.m38985a(aaq, (C6144ahM<?>) new C2854b())).avT();
    }

    /* access modifiers changed from: private */
    /* renamed from: q */
    public void m34691q(PlayerSpeech av) {
        this.f8495nM.mo4911Kk();
        ((PlayerSpeech) C3582se.m38985a(av, (C6144ahM<?>) new C2857d())).avT();
    }

    /* access modifiers changed from: private */
    /* renamed from: I */
    public void m34675I(Map<NPCSpeech, List<PlayerSpeech>> map) {
        List<PlayerSpeech> list;
        if (this.f8495nM != null) {
            NPCSpeech agl = null;
            List arrayList = new ArrayList();
            Iterator<Map.Entry<NPCSpeech, List<PlayerSpeech>>> it = map.entrySet().iterator();
            while (true) {
                list = arrayList;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                agl = (NPCSpeech) next.getKey();
                arrayList = (List) next.getValue();
            }
            this.hUB = agl;
            if (this.hUB.abx() != null) {
                this.f8494kj.aVU().mo13975h(new C0208CZ(this.hUB.abx(), false));
            }
            this.f8495nM.mo4915cd("npc-text").setText(C5956adg.format(this.hUB.ddT().get(), this.f8493P.getName()));
            this.hUx.clear();
            for (PlayerSpeech G : list) {
                this.hUx.mo22248G(G);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: r */
    public void m34692r(PlayerSpeech av) {
        this.hUz = false;
        if (C2319dz.m29373b(av)) {
            this.hUz = true;
            this.f8494kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, this.bhS));
        }
        for (SpeechAction zkVar : av.avP()) {
            if (zkVar instanceof NurseryTutorialSpeechAction) {
                close();
            }
        }
        try {
            if (this.f8495nM != null) {
                this.f8495nM.mo4911Kk();
            }
            ((PlayerSpeech) C3582se.m38985a(av, (C6144ahM<?>) new C2856c(av))).mo533ae(this.f8493P);
        } catch (C3512ro e) {
        }
    }

    /* renamed from: a.lC$i */
    /* compiled from: a */
    class C2863i extends C3320qJ {
        private final /* synthetic */ NPC aTH;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C2863i(AgentsAddon agentsAddon, IAddonProperties vWVar, NPCType aed, NPC auf) {
            super(agentsAddon, vWVar, aed);
            this.aTH = auf;
        }

        public void close() {
            super.close();
            C2851lC.this.hUy = null;
            C2851lC.this.m34673C(this.aTH);
        }
    }

    /* renamed from: a.lC$f */
    /* compiled from: a */
    class C2860f extends C3428rT<C5719aUd> {
        C2860f() {
        }

        /* renamed from: a */
        public void mo321b(C5719aUd aud) {
            if (C2851lC.this.cqw.mo11654Fs().getHandle().equals(aud.baM.dBi().getHandle())) {
                C2851lC.this.bhS.mo24323a(C2851lC.this.cqw, new C2129cL(C2851lC.this.f8494kj, aud.baM, C2851lC.this.cqw));
            }
        }
    }

    /* renamed from: a.lC$e */
    /* compiled from: a */
    class C2859e extends C3428rT<C5783aaP> {
        C2859e() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            C2851lC.this.close();
        }
    }

    /* renamed from: a.lC$h */
    /* compiled from: a */
    class C2862h extends C3428rT<C3689to> {
        C2862h() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C2851lC.this.f8495nM.isClosable()) {
                C2851lC.this.close();
            }
        }
    }

    /* renamed from: a.lC$g */
    /* compiled from: a */
    class C2861g extends ComponentAdapter {
        C2861g() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C2851lC.this.f8494kj.aVU().mo13964a(C2851lC.this.aoN);
            C2851lC.this.f8494kj.aVU().mo13964a(C2851lC.this.f8496wz);
            C2851lC.this.f8494kj.aVU().mo13964a(C2851lC.this.hUw);
            C2851lC.this.f8494kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, C2851lC.this.bhS));
            C2851lC.this.close();
        }
    }

    /* renamed from: a.lC$a */
    class C2852a implements Repeater.C3671a<PlayerSpeech> {
        C2852a() {
        }

        /* renamed from: a */
        public void mo843a(PlayerSpeech av, Component component) {
            TextArea cd = ((Panel) component).mo4915cd("pc-option");
            cd.setText(av.ddT().get());
            cd.addMouseListener(new C2853a(av));
        }

        /* renamed from: a.lC$a$a */
        class C2853a extends MouseAdapter {
            private final /* synthetic */ PlayerSpeech azJ;

            C2853a(PlayerSpeech av) {
                this.azJ = av;
            }

            public void mouseReleased(MouseEvent mouseEvent) {
                if (mouseEvent.getComponent().contains(mouseEvent.getPoint())) {
                    C2851lC.this.f8494kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, C2851lC.this.bhS));
                    C2851lC.this.m34692r(this.azJ);
                }
            }
        }
    }

    /* renamed from: a.lC$b */
    /* compiled from: a */
    class C2854b implements C6144ahM<Map<NPCSpeech, List<PlayerSpeech>>> {
        C2854b() {
        }

        /* renamed from: b */
        public void mo1931n(Map<NPCSpeech, List<PlayerSpeech>> map) {
            SwingUtilities.invokeLater(new C2855a(map));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            C2851lC.this.f8495nM.mo4912Kl();
        }

        /* renamed from: a.lC$b$a */
        class C2855a implements Runnable {
            private final /* synthetic */ Map bjB;

            C2855a(Map map) {
                this.bjB = map;
            }

            public void run() {
                C2851lC.this.f8495nM.mo4912Kl();
                if (this.bjB != null) {
                    C2851lC.this.m34675I(this.bjB);
                    if (C2851lC.this.hUB.abx() != null) {
                        C2851lC.this.f8494kj.aVU().mo13975h(new C0208CZ(C2851lC.this.hUB.abx(), false));
                    }
                    if (C2851lC.this.hUB.bXp()) {
                        C2851lC.this.f8495nM.setClosable(false);
                    } else {
                        C2851lC.this.f8495nM.setClosable(true);
                    }
                }
            }
        }
    }

    /* renamed from: a.lC$d */
    /* compiled from: a */
    class C2857d implements C6144ahM<Map<NPCSpeech, List<PlayerSpeech>>> {
        C2857d() {
        }

        /* renamed from: b */
        public void mo1931n(Map<NPCSpeech, List<PlayerSpeech>> map) {
            SwingUtilities.invokeLater(new C2858a(map));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            C2851lC.this.f8495nM.mo4912Kl();
        }

        /* renamed from: a.lC$d$a */
        class C2858a implements Runnable {
            private final /* synthetic */ Map bjB;

            C2858a(Map map) {
                this.bjB = map;
            }

            public void run() {
                C2851lC.this.f8495nM.mo4912Kl();
                if (this.bjB != null) {
                    C2851lC.this.m34675I(this.bjB);
                }
            }
        }
    }

    /* renamed from: a.lC$c */
    /* compiled from: a */
    class C2856c implements C6144ahM {
        private static /* synthetic */ int[] aTG;
        private final /* synthetic */ PlayerSpeech aTF;

        C2856c(PlayerSpeech av) {
            this.aTF = av;
        }

        /* renamed from: Wq */
        static /* synthetic */ int[] m34700Wq() {
            int[] iArr = aTG;
            if (iArr == null) {
                iArr = new int[C3512ro.C3513a.values().length];
                try {
                    iArr[C3512ro.C3513a.CRAFT_NO_INGREDIENT.ordinal()] = 3;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C3512ro.C3513a.CRAFT_NO_REQUIRED_BLUEPRINT.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C3512ro.C3513a.CRAFT_NO_RESULT.ordinal()] = 4;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C3512ro.C3513a.CRAFT_NO_ROOM.ordinal()] = 5;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C3512ro.C3513a.GIVE_NO_ROOM.ordinal()] = 7;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[C3512ro.C3513a.INSUFICIENT_ITENS.ordinal()] = 6;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[C3512ro.C3513a.MONEY_NO_BALANCE.ordinal()] = 1;
                } catch (NoSuchFieldError e7) {
                }
                aTG = iArr;
            }
            return iArr;
        }

        /* renamed from: n */
        public void mo1931n(Object obj) {
            if (C2851lC.this.f8495nM != null) {
                C2851lC.this.f8495nM.mo4912Kl();
            }
            if (C2319dz.m29374c(this.aTF)) {
                C2851lC.this.close();
                if (!C2851lC.this.hUz) {
                    C2851lC.this.f8494kj.aVU().mo13975h(new C0537HY());
                    return;
                }
                return;
            }
            Asset abx = C2851lC.this.hUB.abx();
            if (abx != null) {
                C2851lC.this.f8494kj.aVU().mo13975h(new C0208CZ(abx, true));
            }
            C2851lC.this.m34691q(this.aTF);
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            if (C2851lC.this.f8495nM != null) {
                C2851lC.this.f8495nM.mo4912Kl();
            }
            if (th instanceof C3512ro) {
                switch (m34700Wq()[((C3512ro) th).mo21846Yr().ordinal()]) {
                    case 1:
                        C2851lC.this.f8494kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2851lC.this.f8494kj.mo11834a((Class<?>) MissionAddon.class, "Money No Balance Message", new Object[0]), new Object[0]));
                        break;
                    case 2:
                        C2851lC.this.f8494kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2851lC.this.f8494kj.mo11834a((Class<?>) MissionAddon.class, "Required Blueprint Not Found", new Object[0]), new Object[0]));
                        break;
                    case 3:
                        C2851lC.this.f8494kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2851lC.this.f8494kj.mo11834a((Class<?>) MissionAddon.class, "Required Ingredient Not Found", new Object[0]), new Object[0]));
                        break;
                    case 5:
                        C2851lC.this.f8494kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2851lC.this.f8494kj.mo11834a((Class<?>) MissionAddon.class, "No Room For Result", new Object[0]), new Object[0]));
                        break;
                    case 6:
                        C2851lC.this.f8494kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2851lC.this.f8494kj.mo11834a((Class<?>) MissionAddon.class, "Insuficient Itens", new Object[0]), new Object[0]));
                        break;
                    case 7:
                        C2851lC.this.f8494kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2851lC.this.f8494kj.mo11834a((Class<?>) MissionAddon.class, "No Room", new Object[0]), new Object[0]));
                        break;
                }
                C2851lC.this.close();
            }
        }
    }
}
