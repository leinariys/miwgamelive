package taikodom.addon.mission;

import game.script.Actor;
import game.script.mission.MissionTemplate;
import game.script.npc.NPC;
import game.script.npcchat.actions.OpenMissionBriefingSpeechAction;
import game.script.player.Player;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.IAddonSettings;
import logic.aaa.C0085Az;
import logic.aaa.C2055be;
import logic.aaa.C5685aSv;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.Table;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.pda.PdaAddon;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;
import java.util.*;

@TaikodomAddon("taikodom.addon.mission")
/* compiled from: a */
public class AvailableMissions implements C1215Rv, C2495fo {

    /* renamed from: qw */
    private static final String f10132qw = "available.xml";
    /* access modifiers changed from: private */
    public PdaAddon aoJ;
    /* access modifiers changed from: private */
    public C5685aSv aoK;
    /* access modifiers changed from: private */
    public Table ehG;
    /* access modifiers changed from: private */
    public Object ehK;
    /* renamed from: kj */
    public IAddonProperties f10134kj;
    /* renamed from: P */
    private Player f10133P;
    private C2698il aoL;
    private DefaultTableModel ehH;
    private C6124ags<C0085Az> ehI;
    private List<C4760k> ehJ;
    private JList ehL;
    /* access modifiers changed from: private */
    private int ehM;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10134kj = addonPropertie;
        this.f10133P = this.f10134kj.getPlayer();
        if (this.f10133P != null) {
            this.aoJ = (PdaAddon) addonPropertie.mo11830U(PdaAddon.class);
            this.aoK = new C5685aSv("pda", this.f10134kj, this);
            this.f10134kj.aVU().mo13965a(C2055be.class, new C4751b());
            this.ehI = new C4752c();
            this.f10134kj.aVU().mo13965a(C0085Az.class, this.ehI);
            this.f10134kj.aVU().publish(this.aoK);
        }
    }

    /* renamed from: Im */
    private void m44684Im() {
        bvc();
    }

    /* access modifiers changed from: private */
    /* renamed from: oS */
    public void m44699oS() {
        this.ehL = this.aoL.mo4915cd("spaces");
        JPanel cd = this.aoL.mo4915cd("mainPanel");
        synchronized (this.ehK) {
            if (!this.ehJ.isEmpty()) {
                cd.getLayout().show(cd, "spaces");
                this.ehL.setCellRenderer(new C4761l(this, (C4761l) null));
                this.ehL.setModel(new C2302dn(this.ehJ));
            } else {
                cd.getLayout().show(cd, "noMission");
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v0, resolved type: java.lang.Object[]} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m44690b(java.util.HashMap<NPC, MissionTemplate> r12) {
        /*
            r11 = this;
            r10 = 3
            r9 = 2
            r8 = 1
            r7 = 0
            taikodom.addon.mission.AvailableMissions$g r0 = new taikodom.addon.mission.AvailableMissions$g
            r0.<init>(r7, r10)
            r11.ehH = r0
            a.pz r0 = r11.ehG
            javax.swing.table.DefaultTableModel r1 = r11.ehH
            r0.setModel(r1)
            a.pz r0 = r11.ehG
            javax.swing.table.TableColumnModel r0 = r0.getColumnModel()
            javax.swing.table.TableColumn r0 = r0.getColumn(r7)
            r1 = 150(0x96, float:2.1E-43)
            r0.setMinWidth(r1)
            a.pz r0 = r11.ehG
            taikodom.addon.mission.AvailableMissions$f r1 = new taikodom.addon.mission.AvailableMissions$f
            r1.<init>()
            r0.setSelectionModel(r1)
            java.util.Set r0 = r12.keySet()
            java.util.Iterator r2 = r0.iterator()
        L_0x0033:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x003a
            return
        L_0x003a:
            java.lang.Object r0 = r2.next()
            a.aUf r0 = (p001a.C5721aUf) r0
            java.lang.Object r1 = r12.get(r0)
            a.aVh r1 = (p001a.C5749aVh) r1
            taikodom.infra.game.script.I18NString r3 = r1.mo711rP()
            java.lang.String r3 = r3.get()
            int r1 = r1.mo710lt()
            a.CR r4 = r0.bhE()
            javax.swing.table.DefaultTableModel r5 = r11.ehH
            java.lang.Object[] r6 = new java.lang.Object[r10]
            r6[r7] = r0
            java.lang.Object[] r0 = new java.lang.Object[r9]
            r0[r7] = r3
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)
            r0[r8] = r1
            r6[r8] = r0
            java.lang.String r0 = r4.getName()
            r6[r9] = r0
            r5.addRow(r6)
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.mission.AvailableMissions.m44690b(java.util.HashMap):void");
    }

    /* access modifiers changed from: private */
    public void buY() {
        this.ehG.getColumnModel().getColumn(0).setCellRenderer(new C4758i());
        this.ehG.getColumnModel().getColumn(1).setCellRenderer(new C4757h());
        this.ehG.getColumnModel().getColumn(2).setCellRenderer(new C4753d());
    }

    private void buZ() {
        this.aoL.mo4911Kk();
    }

    /* access modifiers changed from: private */
    public void bva() {
        this.aoL.mo4912Kl();
    }

    /* access modifiers changed from: private */
    public void bvb() {
        bvc();
    }

    private void bvc() {
        buZ();
        if (this.ehJ == null) {
            this.ehJ = new LinkedList();
            this.ehK = new Object();
        } else {
            this.ehJ.clear();
        }
        ((LDScriptingController) C3582se.m38985a(this.f10133P.bQG(), (C6144ahM<?>) new C4749a())).cXg();
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m44698i(Set<NPC> set) {
        C4760k kVar;
        if (set.isEmpty()) {
            SwingUtilities.invokeLater(new C4754e());
        }
        this.ehM = set.size();
        for (NPC next : set) {
            Actor bhE = next.bhE();
            StellarSystem Nc = bhE.mo960Nc();
            Node azW = bhE.azW();
            C4760k kVar2 = null;
            synchronized (this.ehK) {
                for (C4760k next2 : this.ehJ) {
                    if (next2.dqG.equals(Nc)) {
                        kVar2 = next2;
                    }
                }
                if (kVar2 == null) {
                    kVar = new C4760k(Nc);
                    this.ehJ.add(kVar);
                } else {
                    kVar = kVar2;
                }
            }
            ((LDScriptingController) C3582se.m38985a(this.f10133P.bQG(), (C6144ahM<?>) new C4764o(kVar, azW, next))).mo20249B(next);
        }
    }

    /* access modifiers changed from: private */
    public void bvd() {
        int i = this.ehM - 1;
        this.ehM = i;
        if (i <= 0) {
            SwingUtilities.invokeLater(new C4763n());
        }
    }

    public void bve() {
        if (!this.aoJ.isVisible() || !this.aoJ.dvC().mo23273d(this.aoK)) {
            this.aoJ.dvC().mo23272c(this.aoK);
            this.aoJ.makeVisible();
            update();
            return;
        }
        this.aoJ.bWg();
    }

    public void stop() {
        this.f10134kj.aVU().mo13964a(this.ehI);
        this.f10134kj.aVU().mo13974g(this.aoK);
    }

    public String getTitle() {
        return this.f10134kj.translate("Available Missions");
    }

    /* renamed from: Io */
    public String mo5290Io() {
        return f10132qw;
    }

    /* renamed from: a */
    public void mo5291a(C2698il ilVar) {
        this.aoL = ilVar;
    }

    public void hide() {
    }

    public void show() {
    }

    public void update() {
        m44684Im();
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$b */
    /* compiled from: a */
    class C4751b extends C6124ags<C2055be> {
        C4751b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2055be beVar) {
            if (!AvailableMissions.this.aoJ.isVisible()) {
                AvailableMissions.this.aoJ.bWg();
            } else {
                AvailableMissions.this.update();
            }
            AvailableMissions.this.aoJ.dvC().mo23272c(AvailableMissions.this.aoK);
            return true;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$c */
    /* compiled from: a */
    class C4752c extends C6124ags<C0085Az> {
        C4752c() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0085Az az) {
            AvailableMissions.this.bvb();
            return false;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$m */
    /* compiled from: a */
    private class C4762m extends AbstractListModel {

        private final HashMap<Node, HashMap<NPC, MissionTemplate>> dqH;
        private List<Node> nodes = new ArrayList();

        public C4762m(HashMap<Node, HashMap<NPC, MissionTemplate>> hashMap) {
            this.dqH = hashMap;
            for (Node add : hashMap.keySet()) {
                this.nodes.add(add);
            }
        }

        public Object getElementAt(int i) {
            Node rPVar = this.nodes.get(i);
            return new Object[]{rPVar, this.dqH.get(rPVar)};
        }

        public int getSize() {
            return this.dqH.size();
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$l */
    /* compiled from: a */
    private class C4761l extends C5517aMj {
        private C4761l() {
        }

        /* synthetic */ C4761l(AvailableMissions availableMissions, C4761l lVar) {
            this();
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            Panel dAx = aur.dAx();
            C4760k kVar = (C4760k) obj;
            dAx.mo4917cf("spaceName").setText(kVar.dqG.mo3250ke().get());
            JList cd = dAx.mo4915cd("nodes");
            cd.setCellRenderer(new C4759j(AvailableMissions.this, (C4759j) null));
            cd.setModel(new C4762m(kVar.dqH));
            return dAx;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$j */
    /* compiled from: a */
    private class C4759j extends C5517aMj {
        private C4759j() {
        }

        /* synthetic */ C4759j(AvailableMissions availableMissions, C4759j jVar) {
            this();
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            Panel dAx = aur.dAx();
            Object[] objArr = (Object[]) obj;
            dAx.mo4917cf("nodeName").setText(((Node) objArr[0]).mo21665ke().get());
            AvailableMissions.this.ehG = dAx.mo4915cd("table");
            AvailableMissions.this.m44690b((HashMap<NPC, MissionTemplate>) (HashMap) objArr[1]);
            AvailableMissions.this.buY();
            return dAx;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$g */
    /* compiled from: a */
    class C4756g extends DefaultTableModel {


        C4756g(int i, int i2) {
            super(i, i2);
        }

        public boolean isCellEditable(int i, int i2) {
            return false;
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return AvailableMissions.this.f10134kj.translate("Agent");
                case 1:
                    return AvailableMissions.this.f10134kj.translate("Mission");
                default:
                    return AvailableMissions.this.f10134kj.translate("Station");
            }
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$f */
    /* compiled from: a */
    class C4755f extends DefaultListSelectionModel {


        C4755f() {
        }

        public void addSelectionInterval(int i, int i2) {
        }

        public void setLeadSelectionIndex(int i) {
        }

        public void setSelectionInterval(int i, int i2) {
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$i */
    /* compiled from: a */
    class C4758i extends C2122cE {
        C4758i() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            Panel nR = aur.mo11631nR("AgentsView");
            NPC auf = (NPC) obj;
            nR.mo4915cd("agent-picture").mo2605a((Icon) new ImageIcon(AvailableMissions.this.f10134kj.bHv().adz().getImage("res://imageset_avatar/material/" + auf.bTI())));
            nR.mo4917cf("agent-name").setText(auf.getName());
            return nR;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$h */
    /* compiled from: a */
    class C4757h extends C2122cE {
        C4757h() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            Panel nR = aur.mo11631nR("MissionView");
            Object[] objArr = (Object[]) obj;
            nR.mo4917cf("mission-details").setText(String.valueOf((String) objArr[0]) + "   [" + AvailableMissions.this.f10134kj.translate("Rank:") + " " + ((Integer) objArr[1]).intValue() + "]");
            return nR;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$d */
    /* compiled from: a */
    class C4753d extends C2122cE {
        C4753d() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("StationView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$k */
    /* compiled from: a */
    class C4760k {
        public StellarSystem dqG;
        public HashMap<Node, HashMap<NPC, MissionTemplate>> dqH = new HashMap<>();

        public C4760k(StellarSystem jj) {
            this.dqG = jj;
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$a */
    class C4749a implements C6144ahM<Set<NPC>> {
        C4749a() {
        }

        /* renamed from: d */
        public void mo1931n(Set<NPC> set) {
            AvailableMissions.this.m44698i(set);
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(new C4750a());
        }

        /* renamed from: taikodom.addon.mission.AvailableMissions$a$a */
        class C4750a implements Runnable {
            C4750a() {
            }

            public void run() {
                AvailableMissions.this.bva();
            }
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$e */
    /* compiled from: a */
    class C4754e implements Runnable {
        C4754e() {
        }

        public void run() {
            AvailableMissions.this.bva();
            AvailableMissions.this.m44699oS();
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$o */
    /* compiled from: a */
    class C4764o implements C6144ahM<OpenMissionBriefingSpeechAction> {
        private final /* synthetic */ NPC aTH;
        private final /* synthetic */ C4760k iZx;
        private final /* synthetic */ Node iZy;

        C4764o(C4760k kVar, Node rPVar, NPC auf) {
            this.iZx = kVar;
            this.iZy = rPVar;
            this.aTH = auf;
        }

        /* renamed from: a */
        public void mo1931n(OpenMissionBriefingSpeechAction zu) {
            if (zu == null) {
                AvailableMissions.this.bvd();
                return;
            }
            MissionTemplate bfA = zu.bfA();
            if (bfA == null) {
                AvailableMissions.this.f10134kj.getLog().error("Briefing speech " + zu.getHandle() + " with null mission template.");
                return;
            }
            synchronized (AvailableMissions.this.ehK) {
                if (!this.iZx.dqH.containsKey(this.iZy)) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(this.aTH, bfA);
                    this.iZx.dqH.put(this.iZy, hashMap);
                } else if (!this.iZx.dqH.values().contains(this.aTH)) {
                    this.iZx.dqH.get(this.iZy).put(this.aTH, bfA);
                }
            }
            AvailableMissions.this.bvd();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            AvailableMissions.this.bvd();
        }
    }

    /* renamed from: taikodom.addon.mission.AvailableMissions$n */
    /* compiled from: a */
    class C4763n implements Runnable {
        C4763n() {
        }

        public void run() {
            AvailableMissions.this.bva();
            AvailableMissions.this.m44699oS();
        }
    }
}
