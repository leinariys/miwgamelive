package taikodom.addon.mission;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.script.npc.NPCType;
import logic.aaa.C5783aaP;
import logic.swing.C5378aHa;
import logic.ui.Panel;
import logic.ui.item.InternalFrame;
import logic.ui.item.aUR;
import p001a.C3428rT;
import p001a.C4045yZ;
import p001a.C5517aMj;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.List;

@TaikodomAddon("taikodom.addon.mission")
/* renamed from: a.qJ */
/* compiled from: a */
public class C3320qJ {
    /* access modifiers changed from: private */
    public C3428rT<C5783aaP> aoN;
    /* access modifiers changed from: private */
    public AgentsAddon bhS;
    /* renamed from: kj */
    public IAddonProperties f8892kj;
    /* renamed from: wz */
    public C3428rT<C3689to> f8896wz;
    /* renamed from: nM */
    public InternalFrame f8893nM;
    private JList bhQ;
    /* access modifiers changed from: private */
    private JList bhR;
    private NPCType bhT;
    private C4045yZ bhU = null;
    /* renamed from: wM */
    private Panel f8894wM;
    /* access modifiers changed from: private */
    /* renamed from: wN */
    private Panel f8895wN;

    public C3320qJ(AgentsAddon agentsAddon, IAddonProperties vWVar, NPCType aed) {
        this.f8892kj = vWVar;
        this.bhS = agentsAddon;
        this.bhT = aed;
        this.aoN = new C3327g();
        this.f8896wz = new C3326f();
        this.f8892kj.aVU().mo13965a(C3689to.class, this.f8896wz);
        this.f8892kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        m37479Im();
        m37486iJ();
        this.f8892kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, this));
    }

    /* renamed from: Im */
    private void m37479Im() {
        this.f8893nM = this.f8892kj.bHv().mo16795c(MissionAddon.class, "npc-reward.xml");
        this.f8893nM.addComponentListener(new C3324d());
        this.f8893nM.mo4913cb("ok").addActionListener(new C3325e());
        this.f8893nM.mo4913cb("cancel").addActionListener(new C3322b());
        this.f8895wN = (Panel) this.f8893nM.mo4916ce("choices-panel");
        this.bhQ = this.f8893nM.mo4915cd("choices-list");
        this.bhQ.setSelectionMode(0);
        this.bhQ.setModel(new DefaultListModel());
        this.bhQ.setCellRenderer(new C3323c());
        this.f8894wM = (Panel) this.f8893nM.mo4916ce("rewards-panel");
        this.bhR = this.f8893nM.mo4915cd("rewards-list");
        this.bhR.setCellRenderer(new C3321a());
    }

    public void close() {
        if (this.f8893nM != null) {
            this.f8893nM.destroy();
            this.f8893nM = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: NI */
    public void m37480NI() {
        if (this.bhQ.getSelectedValue() == null || !(this.bhQ.getSelectedValue() instanceof ItemTypeTableItem) || this.bhU == null) {
            this.f8892kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f8892kj.translate("Dialog Message: Trying to confirm reward selection without selecting a reward"), new Object[0]));
            return;
        }
        this.f8892kj.getPlayer().bQG().mo20260b(this.bhT, this.bhU, (ItemTypeTableItem) this.bhQ.getSelectedValue());
        m37486iJ();
    }

    /* renamed from: iJ */
    private void m37486iJ() {
        List list = this.f8892kj.getPlayer().bQG().cXc().get(this.bhT);
        if (list == null || list.size() <= 0) {
            close();
            return;
        }
        this.bhU = (C4045yZ) list.get(0);
        this.f8893nM.mo4917cf("mission-title").setText(this.bhU.mo711rP().get());
        List<ItemTypeTableItem> aqN = this.bhU.aqN();
        List<ItemTypeTableItem> aqM = this.bhU.aqM();
        if (aqN.size() > 0 || aqM.size() > 0) {
            DefaultListModel model = this.bhQ.getModel();
            DefaultListModel model2 = this.bhR.getModel();
            model.clear();
            model2.clear();
            for (ItemTypeTableItem addElement : aqN) {
                model.addElement(addElement);
            }
            for (ItemTypeTableItem addElement2 : aqM) {
                model2.addElement(addElement2);
            }
            if (aqM.size() <= 0) {
                this.f8894wM.setVisible(false);
            }
            if (aqN.size() <= 0) {
                this.f8895wN.setVisible(false);
                return;
            }
            return;
        }
        System.err.println("Reward window error");
        close();
    }

    public boolean isActive() {
        return this.f8893nM != null && this.f8893nM.isVisible();
    }

    /* renamed from: a.qJ$g */
    /* compiled from: a */
    class C3327g extends C3428rT<C5783aaP> {
        C3327g() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (C3320qJ.this.f8893nM != null) {
                C3320qJ.this.close();
            }
        }
    }

    /* renamed from: a.qJ$f */
    /* compiled from: a */
    class C3326f extends C3428rT<C3689to> {
        C3326f() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C3320qJ.this.f8893nM != null) {
                toVar.adC();
                C3320qJ.this.close();
            }
        }
    }

    /* renamed from: a.qJ$d */
    /* compiled from: a */
    class C3324d extends ComponentAdapter {
        C3324d() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C3320qJ.this.f8892kj.aVU().mo13964a(C3320qJ.this.f8896wz);
            C3320qJ.this.f8892kj.aVU().mo13964a(C3320qJ.this.aoN);
            C3320qJ.this.f8892kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, C3320qJ.this.bhS));
            C3320qJ.this.close();
        }
    }

    /* renamed from: a.qJ$e */
    /* compiled from: a */
    class C3325e implements ActionListener {
        C3325e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3320qJ.this.m37480NI();
        }
    }

    /* renamed from: a.qJ$b */
    /* compiled from: a */
    class C3322b implements ActionListener {
        C3322b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3320qJ.this.close();
        }
    }

    /* renamed from: a.qJ$c */
    /* compiled from: a */
    class C3323c extends C5517aMj {
        C3323c() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            if (!(obj instanceof ItemTypeTableItem)) {
                return null;
            }
            ItemTypeTableItem zlVar = (ItemTypeTableItem) obj;
            Panel dAx = aur.dAx();
            JLabel cf = dAx.mo4917cf("choice-item");
            JLabel cf2 = dAx.mo4917cf("choice-item-amount");
            cf.setIcon(new ImageIcon(C3320qJ.this.f8892kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + zlVar.mo23385az().mo185Sa())));
            cf2.setText(new StringBuilder().append(zlVar.getAmount()).toString());
            return dAx;
        }
    }

    /* renamed from: a.qJ$a */
    class C3321a extends C5517aMj {
        C3321a() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            if (!(obj instanceof ItemTypeTableItem)) {
                return null;
            }
            ItemTypeTableItem zlVar = (ItemTypeTableItem) obj;
            Panel dAx = aur.dAx();
            JLabel cf = dAx.mo4917cf("reward-item");
            JLabel cf2 = dAx.mo4917cf("reward-item-amount");
            cf.setIcon(new ImageIcon(C3320qJ.this.f8892kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + zlVar.mo23385az().mo185Sa())));
            cf2.setText(new StringBuilder().append(zlVar.getAmount()).toString());
            return dAx;
        }
    }
}
