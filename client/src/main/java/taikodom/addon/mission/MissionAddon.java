package taikodom.addon.mission;

import game.script.mission.Mission;
import game.script.mission.MissionObjective;
import game.script.mission.scripting.MissionScript;
import game.script.mission.scripting.ScriptableMission;
import game.script.npc.NPC;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.baa.C5473aKr;
import logic.baa.aDJ;
import logic.data.link.C2043bU;
import logic.data.link.C3817vO;
import logic.data.link.C3995xs;
import logic.res.code.C5663aRz;
import logic.swing.C5378aHa;
import logic.ui.Panel;
import logic.ui.item.Picture;
import logic.ui.item.Repeater;
import logic.ui.item.Scrollable;
import logic.ui.item.TaskPane;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@TaikodomAddon("taikodom.addon.mission")
/* compiled from: a */
public class MissionAddon implements C2043bU.C2044a, C2495fo, C3817vO.C3818a, C3995xs.C3996a {

    /* renamed from: qw */
    private static final String f10138qw = "mission.xml";

    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10139P;
    /* access modifiers changed from: private */
    public TaskPane cQI;
    /* access modifiers changed from: private */
    public Scrollable efw;
    /* access modifiers changed from: private */
    public List<Mission<?>> haA;
    /* access modifiers changed from: private */
    public HashMap<NPC, C2129cL> haC = new HashMap<>();
    /* access modifiers changed from: private */
    public Mission<?> haD;
    /* access modifiers changed from: private */
    public Panel haF;
    /* access modifiers changed from: private */
    public Panel haK;
    /* access modifiers changed from: private */
    public Runnable haL = new aAU(this);
    /* renamed from: kj */
    public IAddonProperties f10140kj;
    /* renamed from: wu */
    public Panel f10141wu;
    private aSZ aCF;
    private Repeater<C2780jt> haB;
    private Panel haE;
    private C5473aKr haG;
    private ActionListener haH;
    private ActionListener haI;
    private ActionListener haJ;
    private aGC hax;
    /* access modifiers changed from: private */
    private JComboBox hay;
    /* access modifiers changed from: private */
    private JLabel haz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10140kj = addonPropertie;
        this.f10139P = addonPropertie.getPlayer();
        if (this.f10139P != null) {
            m44728Im();
            m44727Fa();
            this.hax = new C4774h(this.f10140kj.translate("mission"), "mission");
            this.hax.mo15602jH(this.f10140kj.translate("Here you can check your current, completed and available missions."));
            this.hax.mo15603jI("MISSION_WINDOW");
            this.aCF = new aSZ(this.cQI, this.hax, 200, 1);
            this.f10140kj.aVU().publish(this.aCF);
            open();
        }
    }

    /* renamed from: Im */
    private void m44728Im() {
        this.cQI = (TaskPane) this.f10140kj.bHv().mo16794bQ(f10138qw);
        this.haE = this.cQI.mo4915cd("mission-card");
        this.haF = this.cQI.mo4915cd("mission-labels");
        this.efw = this.cQI.mo4915cd("missions-scroll");
        this.hay = this.haF.mo4914cc("missions");
        this.haz = this.haF.mo4917cf("mission-selected");
        this.haK = this.cQI.mo4915cd("npanel");
        this.f10141wu = this.cQI.mo4915cd("buttons");
    }

    /* access modifiers changed from: private */
    public void open() {
        this.haA = new ArrayList();
        for (Mission next : this.f10139P.bQG().cWH()) {
            if (!this.haA.contains(next)) {
                this.haA.add(next);
            }
        }
        cGm();
        this.haF.getLayout().show(this.haF, "missions");
    }

    /* renamed from: Fa */
    private void m44727Fa() {
        this.haH = new C4772f();
        this.hay.addActionListener(this.haH);
        this.haG = new C4773g();
        this.f10139P.bQG().mo8319a(C3817vO.eAG, (C5473aKr<?, ?>) this.haG);
        JButton cb = this.cQI.mo4913cb("available-missions");
        this.haI = new C4770d();
        cb.addActionListener(this.haI);
        JButton cb2 = this.cQI.mo4913cb("mission-details");
        this.haJ = new C4771e();
        cb2.addActionListener(this.haJ);
    }

    /* renamed from: v */
    public void mo24357v(NPC auf) {
        this.haC.remove(auf);
    }

    /* access modifiers changed from: private */
    public void cGm() {
        int i;
        int i2;
        this.hay.removeAllItems();
        Mission cWL = this.f10139P.bQG().cWL();
        for (int i3 = 0; i3 < this.haA.size(); i3++) {
            Mission af = this.haA.get(i3);
            if (af != null) {
                this.hay.addItem(new C4767a(af));
            }
        }
        if (cWL != null) {
            int i4 = 0;
            i = 0;
            while (i4 < this.haA.size()) {
                Mission af2 = this.haA.get(i4);
                if (af2 == null) {
                    i2 = i;
                } else if (cWL.cdp() == af2.cdp()) {
                    i2 = i4;
                } else {
                    i2 = i;
                }
                i4++;
                i = i2;
            }
        } else {
            i = 0;
        }
        if (this.hay.getItemCount() == 0) {
            i = -1;
        }
        this.hay.setSelectedIndex(i);
        cGn();
    }

    /* access modifiers changed from: private */
    public void cGn() {
        C4767a aVar = (C4767a) this.hay.getSelectedItem();
        if (aVar == null) {
            this.haz.setText("");
            this.haD = null;
        } else if (this.haD != aVar.btC()) {
            this.haz.setText(aVar.toString());
            this.haD = aVar.btC();
            LDScriptingController bQG = this.f10139P.bQG();
            this.cQI.mo4911Kk();
            ((LDScriptingController) C3582se.m38985a(bQG, (C6144ahM<?>) new C4768b())).mo20283i(this.haD);
        }
        if (this.haD != null) {
            this.haE.getLayout().show(this.haE, "mission-briefing");
            this.haD.mo8355h(C3995xs.C3996a.class, this);
            this.haD.mo8355h(C2043bU.C2044a.class, this);
            this.haD.mo8348d(C3995xs.C3996a.class, this);
            this.haD.mo8348d(C2043bU.C2044a.class, this);
            this.f10141wu.mo4913cb("mission-details").setEnabled(true);
        } else {
            this.f10141wu.mo4913cb("mission-details").setEnabled(false);
            this.haE.getLayout().show(this.haE, "no-mission");
        }
        cGo();
    }

    private void cGo() {
        this.haB = this.cQI.mo4919ch("objectives-list");
        List<MissionObjective> list = null;
        if (this.haD instanceof MissionScript) {
            list = ((MissionScript) this.haD).azk();
        } else if (this.haD instanceof ScriptableMission) {
            list = ((ScriptableMission) this.haD).azk();
        }
        if (list != null) {
            m44730a((List<? extends C2780jt>) list);
            return;
        }
        this.haB.clear();
        this.cQI.doLayout();
    }

    /* renamed from: a */
    private void m44730a(List<? extends C2780jt> list) {
        this.haB.clear();
        this.haB.mo22250a(new C4769c());
        for (C2780jt G : list) {
            this.haB.mo22248G(G);
        }
        this.cQI.doLayout();
    }

    public void stop() {
    }

    /* renamed from: a */
    public void mo22578a(LDScriptingController lYVar) {
        cGn();
    }

    /* renamed from: a */
    public void mo23003a(MissionScript bc) {
        cGo();
    }

    /* renamed from: c */
    public void mo17299c(ScriptableMission gEVar) {
        cGo();
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$h */
    /* compiled from: a */
    class C4774h extends aGC {


        C4774h(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            return true;
        }

        public void setVisible(boolean z) {
            if (z && !MissionAddon.this.cQI.isVisible()) {
                MissionAddon.this.open();
            }
        }

        /* renamed from: m */
        public void mo8924m(boolean z) {
            String str;
            if (z) {
                MissionAddon.this.f10141wu.setVisible(false);
                MissionAddon.this.haK.setVisible(false);
                MissionAddon.this.efw.setHorizontalScrollBarPolicy(31);
                MissionAddon.this.efw.setVerticalScrollBarPolicy(21);
                str = "mission-selected";
            } else {
                MissionAddon.this.f10141wu.setVisible(true);
                MissionAddon.this.haK.setVisible(true);
                MissionAddon.this.efw.setHorizontalScrollBarPolicy(30);
                MissionAddon.this.efw.setVerticalScrollBarPolicy(20);
                str = "missions";
            }
            MissionAddon.this.haF.getLayout().show(MissionAddon.this.haF, str);
        }
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$f */
    /* compiled from: a */
    class C4772f implements ActionListener {
        C4772f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MissionAddon.this.cGn();
        }
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$g */
    /* compiled from: a */
    class C4773g implements C5473aKr {
        C4773g() {
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            for (Mission af : objArr) {
                if (!MissionAddon.this.haA.contains(af)) {
                    MissionAddon.this.haA.add(af);
                }
            }
            MissionAddon.this.cGm();
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            for (Object remove : objArr) {
                MissionAddon.this.haA.remove(remove);
            }
            MissionAddon.this.cGm();
        }
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$d */
    /* compiled from: a */
    class C4770d implements ActionListener {
        C4770d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((AvailableMissions) MissionAddon.this.f10140kj.mo11830U(AvailableMissions.class)).bve();
        }
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$e */
    /* compiled from: a */
    class C4771e implements ActionListener {
        C4771e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (MissionAddon.this.haD != null) {
                NPC cII = cII();
                if (((C2129cL) MissionAddon.this.haC.get(cII)) == null) {
                    MissionAddon.this.haC.put(cII, new C2129cL(MissionAddon.this.f10140kj, MissionAddon.this.haD.cdp(), cII, MissionAddon.this.haD));
                }
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:26:0x00a1 A[LOOP:2: B:13:0x005c->B:26:0x00a1, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0082 A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private NPC cII() {
            /*
                r9 = this;
                r1 = 0
                taikodom.addon.mission.MissionAddon r0 = taikodom.addon.mission.MissionAddon.this
                a.AF r0 = r0.haD
                a.yZ r0 = r0.cdp()
                boolean r0 = r0 instanceof p001a.C5749aVh
                if (r0 == 0) goto L_0x00a3
                taikodom.addon.mission.MissionAddon r0 = taikodom.addon.mission.MissionAddon.this
                a.AF r0 = r0.haD
                a.yZ r0 = r0.cdp()
                a.aVh r0 = (p001a.C5749aVh) r0
                r2 = r0
            L_0x001c:
                if (r2 == 0) goto L_0x0036
                taikodom.addon.mission.MissionAddon r0 = taikodom.addon.mission.MissionAddon.this
                a.vW r0 = r0.f10140kj
                a.DN r0 = r0.ala()
                java.util.List r0 = r0.aKa()
                java.util.Iterator r3 = r0.iterator()
            L_0x0030:
                boolean r0 = r3.hasNext()
                if (r0 != 0) goto L_0x0037
            L_0x0036:
                return r1
            L_0x0037:
                java.lang.Object r0 = r3.next()
                a.Jj r0 = (p001a.C0683Jj) r0
                java.util.List r0 = r0.getNodes()
                java.util.Iterator r4 = r0.iterator()
            L_0x0045:
                boolean r0 = r4.hasNext()
                if (r0 != 0) goto L_0x004e
            L_0x004b:
                if (r1 == 0) goto L_0x0030
                goto L_0x0036
            L_0x004e:
                java.lang.Object r0 = r4.next()
                a.rP r0 = (p001a.C3422rP) r0
                java.util.Set r0 = r0.mo21624Zw()
                java.util.Iterator r5 = r0.iterator()
            L_0x005c:
                boolean r0 = r5.hasNext()
                if (r0 != 0) goto L_0x0065
            L_0x0062:
                if (r1 == 0) goto L_0x0045
                goto L_0x004b
            L_0x0065:
                java.lang.Object r0 = r5.next()
                a.CR r0 = (p001a.C0192CR) r0
                boolean r6 = r0 instanceof p001a.C0096BF
                if (r6 == 0) goto L_0x007f
                a.BF r0 = (p001a.C0096BF) r0
                java.util.List r0 = r0.azz()
                java.util.Iterator r6 = r0.iterator()
            L_0x0079:
                boolean r0 = r6.hasNext()
                if (r0 != 0) goto L_0x0084
            L_0x007f:
                r0 = r1
            L_0x0080:
                if (r0 == 0) goto L_0x00a1
                r1 = r0
                goto L_0x0062
            L_0x0084:
                java.lang.Object r0 = r6.next()
                a.aUf r0 = (p001a.C5721aUf) r0
                a.go r7 = r2.dBi()
                a.aed r8 = r0.mo11654Fs()
                java.lang.String r7 = r7.getHandle()
                java.lang.String r8 = r8.getHandle()
                boolean r7 = r7.equalsIgnoreCase(r8)
                if (r7 == 0) goto L_0x0079
                goto L_0x0080
            L_0x00a1:
                r1 = r0
                goto L_0x005c
            L_0x00a3:
                r2 = r1
                goto L_0x001c
            */
            throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.mission.MissionAddon.C4771e.cII():a.aUf");
        }
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$a */
    class C4767a {
        private String eei;

        /* renamed from: wx */
        private Mission<?> f10142wx;

        /* JADX WARNING: type inference failed for: r4v0, types: [a.AF<?>, a.AF] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public C4767a(Mission<?> r4) {
            /*
                r2 = this;
                taikodom.addon.mission.MissionAddon.this = r3
                r2.<init>()
                r2.f10142wx = r4
                a.yZ r0 = r4.cdp()
                a.akU r1 = r3.f10139P
                java.lang.String r0 = p001a.C2560gn.m32373a(r0, r1)
                r2.eei = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.mission.MissionAddon.C4767a.<init>(taikodom.addon.mission.MissionAddon, a.AF):void");
        }

        public Mission<?> btC() {
            return this.f10142wx;
        }

        public String toString() {
            return "<html><font color=#" + this.eei + ">[</font><font color=#ffffff>" + this.f10142wx.cdp().mo711rP().get() + "</font><font color=#" + this.eei + ">]</font></html>";
        }
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$b */
    /* compiled from: a */
    class C4768b implements C6144ahM<Object> {
        C4768b() {
        }

        /* renamed from: n */
        public void mo1931n(Object obj) {
            SwingUtilities.invokeLater(MissionAddon.this.haL);
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(MissionAddon.this.haL);
        }
    }

    /* renamed from: taikodom.addon.mission.MissionAddon$c */
    /* compiled from: a */
    class C4769c implements Repeater.C3671a<C2780jt> {
        C4769c() {
        }

        /* renamed from: a */
        public void mo843a(C2780jt jtVar, Component component) {
            JLabel cd = ((Panel) component).mo4915cd("obj-desc");
            Picture cd2 = ((Panel) component).mo4915cd("obj-icon");
            String info = jtVar.getInfo();
            String str = jtVar.mo4247vW().get();
            String[] split = info.split(C0147Bi.SEPARATOR);
            cd.setText("<html>" + str + ": <font color=#AAFFAA>" + split[0] + "</font> - " + split[1] + "</html>");
            if (jtVar instanceof MissionObjective) {
                MissionObjective sGVar = (MissionObjective) jtVar;
                if (sGVar.abN() == Mission.C0015a.ACTIVE) {
                    cd2.mo16824a(C5378aHa.CORE, "status_mission_ongoing");
                } else if (sGVar.abN() == Mission.C0015a.ACCOMPLISHED) {
                    cd2.mo16824a(C5378aHa.CORE, "status_mission_completed");
                } else if (sGVar.abN() == Mission.C0015a.FAILED) {
                    cd2.mo16824a(C5378aHa.CORE, "status_mission_failed");
                }
            }
            if (!jtVar.isHidden()) {
                component.setVisible(true);
                cd.setVisible(true);
                cd2.setVisible(true);
            }
        }
    }
}
