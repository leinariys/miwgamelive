package taikodom.addon.mission;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C3689to;
import game.script.Actor;
import game.script.contract.types.ContractTemplate;
import game.script.mission.Mission;
import game.script.mission.MissionObjective;
import game.script.mission.MissionTemplate;
import game.script.mission.scripting.MissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.mission.scripting.ScriptableMissionTemplate;
import game.script.npc.NPC;
import logic.aaa.C0822Lr;
import logic.aaa.C3949xF;
import logic.swing.C0454GJ;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.TextArea;
import logic.ui.item.*;
import p001a.*;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.mission")
/* renamed from: a.cL */
/* compiled from: a */
public class C2129cL {

    /* renamed from: wv */
    public C4045yZ f6084wv;
    /* renamed from: wx */
    public Mission<?> f6086wx;
    /* renamed from: kj */
    public IAddonProperties f6053kj;
    /* renamed from: wr */
    public InternalFrame f6080wr;
    /* renamed from: wA */
    private C3428rT<C3949xF> f6054wA;
    /* renamed from: wB */
    private Component f6055wB;
    /* renamed from: wC */
    private ComponentAdapter f6056wC;
    /* renamed from: wD */
    private JButton f6057wD;
    /* renamed from: wE */
    private JButton f6058wE;
    /* renamed from: wF */
    private JButton f6059wF;
    /* renamed from: wG */
    private JButton f6060wG;
    /* renamed from: wH */
    private JButton f6061wH;
    /* renamed from: wI */
    private logic.ui.item.TextArea f6062wI;
    /* renamed from: wJ */
    private TextArea f6063wJ;
    /* renamed from: wK */
    private aOA f6064wK;
    /* renamed from: wL */
    private Panel f6065wL;
    /* renamed from: wM */
    private Panel f6066wM;
    /* renamed from: wN */
    private Panel f6067wN;
    /* renamed from: wO */
    private JLabel f6068wO;
    /* renamed from: wP */
    private JButton f6069wP;
    /* renamed from: wQ */
    private JButton f6070wQ;
    /* renamed from: wR */
    private JLabel f6071wR;
    /* renamed from: wS */
    private JLabel f6072wS;
    /* renamed from: wT */
    private JLabel f6073wT;
    /* renamed from: wU */
    private ActionListener f6074wU;
    /* renamed from: wV */
    private ActionListener f6075wV;
    /* renamed from: wW */
    private ActionListener f6076wW;
    /* renamed from: wX */
    private ActionListener f6077wX;
    /* renamed from: wY */
    private ActionListener f6078wY;
    /* renamed from: wZ */
    private JButton f6079wZ;
    /* renamed from: ws */
    private JList f6081ws;
    /* access modifiers changed from: private */
    /* renamed from: wt */
    private JList f6082wt;
    /* renamed from: wu */
    private Panel f6083wu;
    /* access modifiers changed from: private */
    /* renamed from: ww */
    private NPC f6085ww;
    /* renamed from: wy */
    private Repeater<C2780jt> f6087wy;

    /* renamed from: wz */
    private C3428rT<C3689to> f6088wz;

    public C2129cL(IAddonProperties vWVar, C4045yZ yZVar, NPC auf) {
        this(vWVar, yZVar, auf, (Mission<?>) null);
    }

    public C2129cL(IAddonProperties vWVar, C4045yZ yZVar, NPC auf, Mission<?> af) {
        this.f6053kj = vWVar;
        this.f6084wv = yZVar;
        this.f6085ww = auf;
        this.f6086wx = af;
        this.f6080wr = this.f6053kj.bHv().mo16795c(MissionAddon.class, "mission-detail.xml");
        m28292iM();
        m28286iG();
        this.f6080wr.addComponentListener(this.f6056wC);
        this.f6053kj.aVU().mo13965a(C3689to.class, this.f6088wz);
        this.f6053kj.aVU().mo13965a(C3949xF.class, this.f6054wA);
        if (this.f6084wv instanceof MissionTemplate) {
            this.f6055wB.mo2605a((Icon) new ImageIcon(this.f6053kj.bHv().adz().getImage(String.valueOf(C5378aHa.NPC_AVATAR.getPath()) + this.f6084wv.aqP())));
        } else if (this.f6084wv instanceof ContractTemplate) {
            this.f6055wB.mo16824a(C5378aHa.ICONOGRAPHY, "button_reassign_selected");
        }
        if (auf != null) {
            this.f6071wR.setText(auf.getName());
            Actor bhE = auf.bhE();
            this.f6072wS.setText(String.valueOf(bhE.getName()) + " - " + bhE.mo960Nc().mo3250ke().get());
        }
        String str = this.f6084wv.mo711rP().get();
        ComponentManager.getCssHolder(this.f6073wT).mo13049Vr().setColorMultiplier(C2560gn.m32374b(this.f6084wv, this.f6053kj.getPlayer()));
        this.f6073wT.setText(str);
        this.f6081ws.setCellRenderer(new C2150n());
        this.f6082wt.setCellRenderer(new C2151o());
        if (af != null) {
            m28287iH();
            this.f6083wu.getLayout().show(this.f6083wu, "details");
        } else {
            m28288iI();
            this.f6083wu.getLayout().show(this.f6083wu, "briefing");
            if (this.f6084wv.aqS()) {
                this.f6080wr.setClosable(false);
            } else {
                this.f6080wr.setClosable(true);
            }
        }
        show();
    }

    /* renamed from: iG */
    private void m28286iG() {
        this.f6088wz = new C2148l();
        this.f6054wA = new C2149m();
        this.f6056wC = new C2147k();
        this.f6074wU = new C2136g();
        this.f6075wV = new C2146j();
        this.f6076wW = new C2145i();
        this.f6077wX = new C2141h();
        this.f6078wY = new C2130a();
    }

    /* renamed from: iH */
    private void m28287iH() {
        if (this.f6084wv.aqS()) {
            this.f6057wD.setVisible(false);
        } else {
            this.f6057wD.addActionListener(this.f6074wU);
        }
        this.f6058wE.setVisible(false);
        this.f6059wF.setVisible(false);
        this.f6079wZ.addActionListener(this.f6078wY);
    }

    /* renamed from: iI */
    private void m28288iI() {
        this.f6060wG.addActionListener(this.f6077wX);
        this.f6079wZ.addActionListener(this.f6078wY);
        if (this.f6084wv.aqS()) {
            this.f6061wH.setVisible(false);
        } else {
            this.f6061wH.addActionListener(this.f6078wY);
        }
    }

    private void show() {
        C2131b bVar;
        m28289iJ();
        this.f6080wr.validate();
        this.f6080wr.center();
        if (!(this.f6084wv instanceof MissionTemplate) || ((MissionTemplate) this.f6084wv).dBk() == null) {
            bVar = null;
        } else {
            bVar = new C2131b();
        }
        this.f6080wr.mo20868b(true, (C0454GJ) bVar);
    }

    /* renamed from: iJ */
    private void m28289iJ() {
        boolean z;
        boolean z2 = true;
        if (this.f6084wv != null) {
            this.f6062wI.setText(C5956adg.format(this.f6084wv.mo713vW().get(), this.f6053kj.ala().getPlayer().getName()));
            this.f6063wJ.setText(this.f6084wv.aqH().get());
            List<ItemTypeTableItem> aqN = this.f6084wv.aqN();
            List<ItemTypeTableItem> aqM = this.f6084wv.aqM();
            DefaultListModel model = this.f6081ws.getModel();
            DefaultListModel model2 = this.f6082wt.getModel();
            model.clear();
            model2.clear();
            for (ItemTypeTableItem addElement : aqN) {
                model.addElement(addElement);
            }
            for (ItemTypeTableItem addElement2 : aqM) {
                model2.addElement(addElement2);
            }
            Panel aco = this.f6066wM;
            if (aqM.size() > 0) {
                z = true;
            } else {
                z = false;
            }
            aco.setVisible(z);
            Panel aco2 = this.f6067wN;
            if (aqN.size() <= 0) {
                z2 = false;
            }
            aco2.setVisible(z2);
            if (this.f6084wv.aqJ()) {
                this.f6064wK.setText(this.f6084wv.aqI().get());
            } else {
                this.f6064wK.setText("T$ " + this.f6084wv.aqL());
            }
            this.f6065wL.doLayout();
        }
        List<MissionObjective> list = null;
        if (this.f6084wv instanceof MissionScriptTemplate) {
            list = new ArrayList<>(((MissionScriptTemplate) this.f6084wv).cKb());
        } else if (this.f6084wv instanceof ScriptableMissionTemplate) {
            list = new ArrayList<>(((ScriptableMissionTemplate) this.f6084wv).azk());
        } else if (this.f6084wv instanceof MissionScript) {
            list = ((MissionScript) this.f6084wv).azk();
        }
        if (list != null) {
            m28282a((List<? extends C2780jt>) list);
            return;
        }
        this.f6087wy.clear();
        this.f6068wO.setVisible(false);
        this.f6080wr.doLayout();
    }

    /* renamed from: a */
    private void m28282a(List<? extends C2780jt> list) {
        this.f6087wy.clear();
        this.f6087wy.mo22250a(new C2132c());
        for (C2780jt G : list) {
            this.f6087wy.mo22248G(G);
        }
        this.f6068wO.setVisible(list.size() > 0);
        this.f6080wr.doLayout();
    }

    public void hide() {
        if ((this.f6084wv instanceof MissionTemplate) && ((MissionTemplate) this.f6084wv).dBk() != null) {
            this.f6053kj.aVU().mo13975h(new C0208CZ(((MissionTemplate) this.f6084wv).dBk(), true));
        }
        ((AgentsAddon) this.f6053kj.mo11830U(AgentsAddon.class)).mo24325v(this.f6085ww);
        ((MissionAddon) this.f6053kj.mo11830U(MissionAddon.class)).mo24357v(this.f6085ww);
        if (this.f6080wr != null) {
            this.f6080wr.setVisible(false);
        }
    }

    public void destroy() {
        if (this.f6080wr != null) {
            hide();
            this.f6080wr.destroy();
            this.f6080wr = null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo17548a(String str, C6622aqW aqw) {
        this.f6069wP.setVisible(false);
        this.f6070wQ.setText(str);
        this.f6070wQ.addActionListener(new C2133d(aqw));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo17549a(String str, String str2, C5530aMw amw, C6622aqW aqw) {
        this.f6069wP.setEnabled(!this.f6053kj.ala().aLU().bMX());
        this.f6069wP.setText(str);
        this.f6069wP.addActionListener(new C2134e(amw));
        this.f6070wQ.setText(str2);
        this.f6070wQ.addActionListener(new C2135f(aqw));
    }

    public boolean isVisible() {
        return this.f6080wr != null && this.f6080wr.isVisible();
    }

    /* access modifiers changed from: private */
    /* renamed from: iK */
    public boolean m28290iK() {
        return !m28291iL() && this.f6053kj.getPlayer().cXm() != aMU.FINISHED_NURSERY;
    }

    /* renamed from: iL */
    private boolean m28291iL() {
        return this.f6053kj != null && this.f6053kj.ala().aLS().mo22273iL();
    }

    /* renamed from: iM */
    private void m28292iM() {
        this.f6055wB = this.f6080wr.mo4915cd("agent-image");
        this.f6062wI = this.f6080wr.mo4915cd("description");
        this.f6063wJ = this.f6080wr.mo4915cd("summary");
        this.f6064wK = this.f6080wr.mo4917cf("reward");
        this.f6065wL = this.f6080wr.mo4915cd("rewards-container");
        this.f6066wM = this.f6080wr.mo4915cd("rewards-panel");
        this.f6067wN = this.f6080wr.mo4915cd("choices-panel");
        this.f6068wO = this.f6080wr.mo4917cf("obj-label");
        this.f6071wR = this.f6080wr.mo4917cf("agent-name");
        this.f6072wS = this.f6080wr.mo4917cf("location-name");
        this.f6073wT = this.f6080wr.mo4915cd("mission-name");
        this.f6087wy = this.f6080wr.mo4919ch("objectives-list");
        this.f6081ws = this.f6080wr.mo4915cd("choices-list");
        this.f6082wt = this.f6080wr.mo4915cd("rewards-list");
        this.f6060wG = this.f6080wr.mo4913cb("accept");
        this.f6061wH = this.f6080wr.mo4913cb("refuse");
        this.f6069wP = this.f6080wr.mo4913cb("mission-action");
        this.f6070wQ = this.f6080wr.mo4913cb("mission-back");
        this.f6083wu = this.f6080wr.mo4915cd("buttons");
        this.f6057wD = this.f6083wu.mo4913cb("mission-abandon");
        this.f6058wE = this.f6083wu.mo4913cb("mission-accomplish");
        this.f6059wF = this.f6083wu.mo4913cb("mission-fail");
        this.f6079wZ = this.f6083wu.mo4913cb("mission-close");
    }

    /* renamed from: a.cL$n */
    /* compiled from: a */
    class C2150n extends C5517aMj {
        C2150n() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            if (!(obj instanceof ItemTypeTableItem)) {
                return null;
            }
            ItemTypeTableItem zlVar = (ItemTypeTableItem) obj;
            Panel dAx = aur.dAx();
            JLabel cf = dAx.mo4917cf("choice-item");
            JLabel cf2 = dAx.mo4917cf("choice-item-amount");
            cf.setIcon(new ImageIcon(C2129cL.this.f6053kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + zlVar.mo23385az().mo12100sK().getHandle())));
            cf2.setText(new StringBuilder().append(zlVar.getAmount()).toString());
            return dAx;
        }
    }

    /* renamed from: a.cL$o */
    /* compiled from: a */
    class C2151o extends C5517aMj {
        C2151o() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            if (!(obj instanceof ItemTypeTableItem)) {
                return null;
            }
            ItemTypeTableItem zlVar = (ItemTypeTableItem) obj;
            Panel dAx = aur.dAx();
            JLabel cf = dAx.mo4917cf("reward-item");
            JLabel cf2 = dAx.mo4917cf("reward-item-amount");
            cf.setIcon(new ImageIcon(C2129cL.this.f6053kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + zlVar.mo23385az().mo12100sK().getHandle())));
            cf2.setText(new StringBuilder().append(zlVar.getAmount()).toString());
            return dAx;
        }
    }

    /* renamed from: a.cL$l */
    /* compiled from: a */
    class C2148l extends C3428rT<C3689to> {
        C2148l() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (!C2129cL.this.m28290iK()) {
                C2129cL.this.destroy();
            }
        }
    }

    /* renamed from: a.cL$m */
    /* compiled from: a */
    class C2149m extends C3428rT<C3949xF> {
        C2149m() {
        }

        /* renamed from: a */
        public void mo321b(C3949xF xFVar) {
            if (xFVar.anX()) {
                C2129cL.this.destroy();
            }
        }
    }

    /* renamed from: a.cL$k */
    /* compiled from: a */
    class C2147k extends ComponentAdapter {
        C2147k() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C2129cL.this.destroy();
        }
    }

    /* renamed from: a.cL$g */
    /* compiled from: a */
    class C2136g implements ActionListener {
        C2136g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2137a aVar = new C2137a();
            ((MessageDialogAddon) C2129cL.this.f6053kj.mo11830U(MessageDialogAddon.class)).mo24307a(C2129cL.this.f6053kj.translate("Abandon mission?"), (aEP) aVar, C2129cL.this.f6053kj.translate("No"), C2129cL.this.f6053kj.translate("Yes"));
        }

        /* renamed from: a.cL$g$a */
        class C2137a extends aEP {
            C2137a() {
            }

            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                if (i == 1) {
                    LDScriptingController bQG = C2129cL.this.f6053kj.getPlayer().bQG();
                    C2129cL.this.f6080wr.mo4911Kk();
                    ((LDScriptingController) C3582se.m38985a(bQG, (C6144ahM<?>) new C2138a())).mo20280g(C2129cL.this.f6086wx);
                }
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                return C0939Nn.C0940a.CONFIRM;
            }

            /* renamed from: a.cL$g$a$a */
            class C2138a implements C6144ahM<Object> {
                C2138a() {
                }

                /* renamed from: n */
                public void mo1931n(Object obj) {
                    SwingUtilities.invokeLater(new C2140b());
                }

                /* renamed from: a */
                public void mo1930a(Throwable th) {
                    SwingUtilities.invokeLater(new C2139a());
                }

                /* renamed from: a.cL$g$a$a$b */
                /* compiled from: a */
                class C2140b implements Runnable {
                    C2140b() {
                    }

                    public void run() {
                        C2129cL.this.f6080wr.mo4912Kl();
                        C2129cL.this.destroy();
                    }
                }

                /* renamed from: a.cL$g$a$a$a */
                class C2139a implements Runnable {
                    C2139a() {
                    }

                    public void run() {
                        C2129cL.this.f6080wr.mo4912Kl();
                    }
                }
            }
        }
    }

    /* renamed from: a.cL$j */
    /* compiled from: a */
    class C2146j implements ActionListener {
        C2146j() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2129cL.this.f6053kj.getPlayer().bQG().mo20286o(C2129cL.this.f6086wx);
            C2129cL.this.destroy();
        }
    }

    /* renamed from: a.cL$i */
    /* compiled from: a */
    class C2145i implements ActionListener {
        C2145i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2129cL.this.f6053kj.getPlayer().bQG().mo20284m(C2129cL.this.f6086wx);
            C2129cL.this.destroy();
        }
    }

    /* renamed from: a.cL$h */
    /* compiled from: a */
    class C2141h implements ActionListener {
        C2141h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            try {
                if (C2129cL.this.f6084wv instanceof MissionScriptTemplate) {
                    long cKh = ((MissionScriptTemplate) C2129cL.this.f6084wv).cKh();
                    if (cKh > 0 && cKh > C2129cL.this.f6053kj.getPlayer().mo5156Qw().bSs()) {
                        C2129cL.this.f6053kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C2129cL.this.f6053kj.translate("You don't have enough money to start this mission"), new Object[0]));
                        return;
                    }
                }
                LDScriptingController bQG = C2129cL.this.f6053kj.getPlayer().bQG();
                C2129cL.this.f6080wr.mo4911Kk();
                ((LDScriptingController) C3582se.m38985a(bQG, (C6144ahM<?>) new C2142a())).mo20278e(C2129cL.this.f6084wv);
            } catch (C3243pZ e) {
                e.printStackTrace();
            }
        }

        /* renamed from: a.cL$h$a */
        class C2142a implements C6144ahM<Object> {
            C2142a() {
            }

            /* renamed from: n */
            public void mo1931n(Object obj) {
                SwingUtilities.invokeLater(new C2144b());
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                SwingUtilities.invokeLater(new C2143a());
            }

            /* renamed from: a.cL$h$a$b */
            /* compiled from: a */
            class C2144b implements Runnable {
                C2144b() {
                }

                public void run() {
                    C2129cL.this.f6080wr.mo4912Kl();
                    C2129cL.this.f6053kj.aVU().mo13975h(new C0822Lr((MissionTemplate) C2129cL.this.f6084wv));
                    C2129cL.this.destroy();
                }
            }

            /* renamed from: a.cL$h$a$a */
            class C2143a implements Runnable {
                C2143a() {
                }

                public void run() {
                    C2129cL.this.f6080wr.mo4912Kl();
                }
            }
        }
    }

    /* renamed from: a.cL$a */
    class C2130a implements ActionListener {
        C2130a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2129cL.this.destroy();
        }
    }

    /* renamed from: a.cL$b */
    /* compiled from: a */
    class C2131b implements C0454GJ {
        C2131b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            C2129cL.this.f6053kj.aVU().mo13975h(new C0208CZ(((MissionTemplate) C2129cL.this.f6084wv).dBk(), false));
        }
    }

    /* renamed from: a.cL$c */
    /* compiled from: a */
    class C2132c implements Repeater.C3671a<C2780jt> {
        C2132c() {
        }

        /* renamed from: a */
        public void mo843a(C2780jt jtVar, Component component) {
            String info = jtVar.getInfo();
            if (component.getName() != null && component.getName().equals("obj-desc")) {
                ((JLabel) component).setText(jtVar.mo4247vW().get());
            } else if (component.getName() != null && component.getName().equals("obj-qnt")) {
                ((JLabel) component).setText(info);
            } else if (jtVar instanceof MissionObjective) {
                MissionObjective sGVar = (MissionObjective) jtVar;
                Picture axm = (Picture) component;
                if (sGVar.abN() == Mission.C0015a.ACTIVE) {
                    axm.mo16824a(C5378aHa.CORE, "status_mission_ongoing");
                } else if (sGVar.abN() == Mission.C0015a.ACCOMPLISHED) {
                    axm.mo16824a(C5378aHa.CORE, "status_mission_completed");
                } else if (sGVar.abN() == Mission.C0015a.FAILED) {
                    axm.mo16824a(C5378aHa.CORE, "status_mission_failed");
                }
            }
            if (!jtVar.isHidden()) {
                component.setVisible(true);
            }
        }
    }

    /* renamed from: a.cL$d */
    /* compiled from: a */
    class C2133d implements ActionListener {
        private final /* synthetic */ C6622aqW apl;

        C2133d(C6622aqW aqw) {
            this.apl = aqw;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2129cL.this.destroy();
            if (this.apl != null) {
                this.apl.mo15605u(true);
            }
        }
    }

    /* renamed from: a.cL$e */
    /* compiled from: a */
    class C2134e implements ActionListener {
        private final /* synthetic */ C5530aMw apn;

        C2134e(C5530aMw amw) {
            this.apn = amw;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.apn.djs();
        }
    }

    /* renamed from: a.cL$f */
    /* compiled from: a */
    class C2135f implements ActionListener {
        private final /* synthetic */ C6622aqW apl;

        C2135f(C6622aqW aqw) {
            this.apl = aqw;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2129cL.this.destroy();
            if (this.apl != null) {
                this.apl.mo15605u(true);
            }
        }
    }
}
