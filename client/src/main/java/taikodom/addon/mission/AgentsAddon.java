package taikodom.addon.mission;

import game.network.message.externalizable.C1475Vi;
import game.network.message.externalizable.C5719aUd;
import game.script.Actor;
import game.script.agent.Agent;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.player.Player;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.aaa.C0085Az;
import logic.aaa.C5783aaP;
import logic.baa.C4068yr;
import logic.baa.aDJ;
import logic.ui.Panel;
import logic.ui.item.LabeledIcon;
import logic.ui.item.Repeater;
import logic.ui.item.TaskPane;
import logic.ui.item.TaskPaneContainer;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.Timer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.*;

@TaikodomAddon("taikodom.addon.mission")
/* compiled from: a */
public class AgentsAddon implements C2495fo {
    private static /* synthetic */ int[] iJM;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10130P;
    /* access modifiers changed from: private */
    public aSZ aCF;
    /* access modifiers changed from: private */
    public aGC hax;
    /* access modifiers changed from: private */
    public HashMap<NPC, C2851lC> iJD = new HashMap<>();
    /* access modifiers changed from: private */
    public Set<NPC> iJE;
    /* access modifiers changed from: private */
    public TreeMap<Integer, Agent> iJG;
    /* access modifiers changed from: private */
    public HashMap<NPC, LabeledIcon> iJJ = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<NPC, LabeledIcon.C0531b> iJK = new HashMap<>();
    /* renamed from: kj */
    public IAddonProperties f10131kj;
    public TaskPane cQI;
    private C6124ags<C5783aaP> amy;
    private C3428rT<C5719aUd> hUw;
    private HashMap<NPC, C2129cL> haC = new HashMap<>();
    private Panel iJC;
    private Repeater<NPC> iJF;
    private C3428rT<C0085Az> iJH;
    private C6124ags<C1475Vi> iJI;
    /* access modifiers changed from: private */
    private ComponentAdapter iJL;

    static /* synthetic */ int[] dtn() {
        int[] iArr = iJM;
        if (iArr == null) {
            iArr = new int[C6902avq.values().length];
            try {
                iArr[C6902avq.CRAFTER.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C6902avq.FIXER.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C6902avq.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C6902avq.RESEARCHER.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            iJM = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10131kj = addonPropertie;
        this.f10130P = this.f10131kj.getPlayer();
        if (this.f10130P != null) {
            this.hax = new C4741g(this.f10131kj.translate("Agents"), "agents");
            this.hax.mo15602jH(this.f10131kj.translate("AgentsExtTooltip"));
            this.hax.mo15603jI("AGENTS_WINDOW");
            m44633Im();
            m44632Fa();
            this.aCF = new aSZ(this.cQI, this.hax, 0, 2);
            this.f10131kj.aVU().publish(this.aCF);
            this.hUw = new C4742h();
            this.f10131kj.aVU().mo13965a(C5719aUd.class, this.hUw);
            this.iJI = new C4743i();
            this.f10131kj.aVU().mo13965a(C1475Vi.class, this.iJI);
            open();
        }
    }

    @C2602hR(mo19255zf = "AGENTS_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24324al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: a */
    public void mo24323a(NPC auf, C2129cL cLVar) {
        this.haC.put(auf, cLVar);
    }

    /* renamed from: v */
    public void mo24325v(NPC auf) {
        this.haC.remove(auf);
    }

    public void stop() {
        if (this.cQI != null) {
            this.cQI.destroy();
            this.cQI = null;
        }
        removeListeners();
    }

    /* renamed from: Im */
    private void m44633Im() {
        this.cQI = (TaskPane) this.f10131kj.bHv().mo16794bQ("agents.xml");
        this.iJC = this.cQI.mo4915cd("agent-list");
        C4744j jVar = new C4744j();
        this.iJF = this.iJC.mo4919ch("repeater");
        this.iJF.mo22250a(jVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: E */
    public void m44631E(NPC auf) {
        if (this.iJD.get(auf) == null && this.haC.get(auf) == null) {
            NPCType Fs = auf.mo11654Fs();
            this.f10130P.dyl().mo11981e((C4068yr) Fs);
            Actor bhE = this.f10131kj.getPlayer().bhE();
            if (bhE instanceof Station) {
                this.f10130P.dyl().mo11964b((C4068yr) Fs, (aDJ) bhE);
            }
            this.iJD.put(auf, new C4737c(this, this.f10131kj, auf, auf));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public String m44642d(C6902avq avq) {
        switch (dtn()[avq.ordinal()]) {
            case 2:
                return this.f10131kj.translate("Crafter");
            case 3:
                return this.f10131kj.translate("Researcher");
            case 4:
                return this.f10131kj.translate("Fixer");
            default:
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void dtk() {
        LDScriptingController bQG = this.f10130P.bQG();
        this.cQI.mo4911Kk();
        ((LDScriptingController) C3582se.m38985a(bQG, (C6144ahM<?>) new C4738d())).cXe();
    }

    /* access modifiers changed from: private */
    public void dtl() {
        if (!this.iJJ.isEmpty() && !this.iJK.isEmpty()) {
            for (NPC next : this.iJJ.keySet()) {
                this.iJJ.get(next).mo2604a(this.iJK.get(next));
            }
        }
    }

    /* access modifiers changed from: private */
    public void open() {
        if (!this.haC.isEmpty()) {
            for (NPC auf : this.haC.keySet()) {
                this.haC.get(auf).destroy();
            }
            this.haC.clear();
        }
        Actor bhE = this.f10131kj.getPlayer().bhE();
        if (bhE instanceof Station) {
            this.iJG = new TreeMap<>();
            Map<Agent, Integer> azB = ((Station) bhE).azB();
            for (Agent next : azB.keySet()) {
                Integer num = azB.get(next);
                if (num != null && num.intValue() >= 0) {
                    while (this.iJG.get(num) != null) {
                        num = Integer.valueOf(num.intValue() + 1);
                    }
                    this.iJG.put(num, next);
                }
            }
            this.iJC.removeAll();
            this.iJF.clear();
            NPC auf2 = null;
            int i = 0;
            for (Integer num2 : this.iJG.keySet()) {
                NPC auf3 = this.iJG.get(num2);
                if (auf3 == null) {
                    auf2 = auf3;
                } else {
                    this.iJF.mo22248G(auf3);
                    i++;
                    auf2 = auf3;
                }
            }
            if (auf2 != null) {
                m44636a(auf2, i);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44636a(NPC auf, int i) {
        List<Component> list = this.iJF.get(auf);
        if (list != null) {
            Panel aco = list.get(0);
            Insets insets = aco.getInsets();
            int floor = (int) Math.floor((double) (((float) this.cQI.getWidth()) / 66.5f));
            if (aco != null) {
                int ceil = ((int) Math.ceil((double) (((float) i) / ((float) floor)))) * 73;
                Dimension dimension = new Dimension(this.iJC.getWidth(), ceil);
                this.iJC.setPreferredSize(dimension);
                Dimension dimension2 = new Dimension(dimension);
                dimension2.height = ceil + (this.cQI.djo().getHeight() * 2) + insets.top;
                this.cQI.setSize(dimension2);
                TaskPaneContainer djq = this.cQI.djq();
                if (djq != null) {
                    djq.validate();
                }
            }
        }
    }

    /* renamed from: Fa */
    private void m44632Fa() {
        this.iJH = new C4739e();
        this.iJL = new C4740f();
        this.cQI.addComponentListener(this.iJL);
        this.amy = new C4735b();
        this.f10131kj.mo2317P(this);
        this.f10131kj.aVU().mo13965a(C0085Az.class, this.iJH);
        this.f10131kj.aVU().mo13965a(C5783aaP.class, this.amy);
    }

    /* access modifiers changed from: private */
    public void dtm() {
        if (this.f10130P.bhE() instanceof Station) {
            ((Station) C3582se.m38985a((Station) this.f10130P.bhE(), (C6144ahM<?>) new C4733a())).azY();
        }
    }

    private void removeListeners() {
        this.f10131kj.aVU().mo13964a(this.iJI);
        this.f10131kj.aVU().mo13964a(this.hUw);
        this.f10131kj.aVU().mo13964a(this.iJH);
        this.f10131kj.aVU().mo13964a(this.amy);
        if (this.cQI != null) {
            this.cQI.removeComponentListener(this.iJL);
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$k */
    /* compiled from: a */
    private final class C4748k extends LabeledIcon.C0531b {
        private static /* synthetic */ int[] eLR;
        private final NPC cqw;

        /* synthetic */ C4748k(AgentsAddon agentsAddon, NPC auf, C4748k kVar) {
            this(auf);
        }

        private C4748k(NPC auf) {
            this.cqw = auf;
        }

        static /* synthetic */ int[] bHs() {
            int[] iArr = eLR;
            if (iArr == null) {
                iArr = new int[LabeledIcon.C0530a.values().length];
                try {
                    iArr[LabeledIcon.C0530a.NORTH_EAST.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[LabeledIcon.C0530a.NORTH_WEST.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[LabeledIcon.C0530a.SOUTH_EAST.ordinal()] = 4;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[LabeledIcon.C0530a.SOUTH_WEST.ordinal()] = 3;
                } catch (NoSuchFieldError e4) {
                }
                eLR = iArr;
            }
            return iArr;
        }

        /* renamed from: b */
        public String mo2612b(LabeledIcon.C0530a aVar) {
            switch (bHs()[aVar.ordinal()]) {
                case 1:
                    return AgentsAddon.this.m44642d(this.cqw.bTM());
                case 4:
                    if (AgentsAddon.this.iJE.contains(this.cqw)) {
                        return AgentsAddon.this.f10131kj.translate("Mission");
                    }
                    return "";
                default:
                    return null;
            }
        }

        /* renamed from: c */
        public Icon mo2613c(LabeledIcon.C0530a aVar) {
            return null;
        }

        public Icon deh() {
            return new ImageIcon(AgentsAddon.this.f10131kj.bHv().adz().getImage("res://imageset_avatar/material/" + this.cqw.bTI()).getScaledInstance(60, 60, 4));
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$g */
    /* compiled from: a */
    class C4741g extends aGC {
        private static final long serialVersionUID = 327058653855727714L;

        C4741g(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            return AgentsAddon.this.f10130P.bQB() && (AgentsAddon.this.f10130P.bhE() instanceof Station);
        }

        public void setVisible(boolean z) {
            if (z) {
                AgentsAddon.this.open();
            }
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$h */
    /* compiled from: a */
    class C4742h extends C3428rT<C5719aUd> {
        C4742h() {
        }

        /* renamed from: a */
        public void mo321b(C5719aUd aud) {
            if (aud.cqw != null) {
                AgentsAddon.this.mo24323a(aud.cqw, new C2129cL(AgentsAddon.this.f10131kj, aud.baM, aud.cqw));
            }
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$i */
    /* compiled from: a */
    class C4743i extends C6124ags<C1475Vi> {
        C4743i() {
        }

        /* renamed from: a */
        public boolean updateInfo(C1475Vi vi) {
            AgentsAddon.this.m44631E(vi.bAj());
            return false;
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$j */
    /* compiled from: a */
    class C4744j implements Repeater.C3671a<NPC> {
        C4744j() {
        }

        /* renamed from: a */
        public void mo843a(NPC auf, Component component) {
            Panel aco = (Panel) component;
            if (aco.getClientProperty("npc") == null) {
                m44675a(auf, aco);
            }
        }

        /* renamed from: a */
        private void m44675a(NPC auf, Panel aco) {
            aco.putClientProperty("npc", auf);
            aco.addMouseListener(new C4747b(auf));
            LabeledIcon cd = aco.mo4915cd("npc-image");
            C4748k kVar = new C4748k(AgentsAddon.this, auf, (C4748k) null);
            AgentsAddon.this.iJJ.put(auf, cd);
            AgentsAddon.this.iJK.put(auf, kVar);
            if (AgentsAddon.this.iJE == null) {
                LDScriptingController bQG = AgentsAddon.this.f10130P.bQG();
                AgentsAddon.this.cQI.mo4911Kk();
                ((LDScriptingController) C3582se.m38985a(bQG, (C6144ahM<?>) new C4745a(cd, kVar))).cXe();
                return;
            }
            cd.mo2604a((LabeledIcon.C0531b) kVar);
        }

        /* renamed from: taikodom.addon.mission.AgentsAddon$j$b */
        /* compiled from: a */
        class C4747b extends MouseAdapter {
            private final /* synthetic */ NPC aTH;

            C4747b(NPC auf) {
                this.aTH = auf;
            }

            public void mouseReleased(MouseEvent mouseEvent) {
                AgentsAddon.this.m44631E(this.aTH);
            }

            public void mousePressed(MouseEvent mouseEvent) {
                AgentsAddon.this.f10131kj.aVU().mo13975h(new C6811auD(C3667tV.MOUSE_CLICK_SFX));
            }
        }

        /* renamed from: taikodom.addon.mission.AgentsAddon$j$a */
        class C4745a implements C6144ahM<Set<NPC>> {
            private final /* synthetic */ LabeledIcon iid;
            private final /* synthetic */ C4748k iie;

            C4745a(LabeledIcon hs, C4748k kVar) {
                this.iid = hs;
                this.iie = kVar;
            }

            /* renamed from: d */
            public void mo1931n(Set<NPC> set) {
                SwingUtilities.invokeLater(new C4746a(set, this.iid, this.iie));
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }

            /* renamed from: taikodom.addon.mission.AgentsAddon$j$a$a */
            class C4746a implements Runnable {
                private final /* synthetic */ Set iic;
                private final /* synthetic */ LabeledIcon iid;
                private final /* synthetic */ C4748k iie;

                C4746a(Set set, LabeledIcon hs, C4748k kVar) {
                    this.iic = set;
                    this.iid = hs;
                    this.iie = kVar;
                }

                public void run() {
                    AgentsAddon.this.cQI.mo4912Kl();
                    AgentsAddon.this.iJE = this.iic;
                    this.iid.mo2604a((LabeledIcon.C0531b) this.iie);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$c */
    /* compiled from: a */
    class C4737c extends C2851lC {
        private final /* synthetic */ NPC aTH;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4737c(AgentsAddon agentsAddon, IAddonProperties vWVar, NPC auf, NPC auf2) {
            super(agentsAddon, vWVar, auf);
            this.aTH = auf2;
        }

        public void close() {
            super.close();
            AgentsAddon.this.iJD.remove(this.aTH);
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$d */
    /* compiled from: a */
    class C4738d extends C0947Nt<Set<NPC>> {
        C4738d() {
        }

        /* renamed from: k */
        public void mo4278E(Set<NPC> set) {
            AgentsAddon.this.cQI.mo4912Kl();
            AgentsAddon.this.iJE = set;
            AgentsAddon.this.dtl();
        }

        /* renamed from: c */
        public void mo4279c(Throwable th) {
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$e */
    /* compiled from: a */
    class C4739e extends C3428rT<C0085Az> {
        C4739e() {
        }

        /* renamed from: b */
        public void mo321b(C0085Az az) {
            AgentsAddon.this.dtk();
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$f */
    /* compiled from: a */
    class C4740f extends ComponentAdapter {
        int gEk = -1;

        C4740f() {
        }

        public void componentResized(ComponentEvent componentEvent) {
            Component component = componentEvent.getComponent();
            if (!(this.gEk == component.getWidth() || AgentsAddon.this.iJG == null)) {
                NPC auf = null;
                int i = 0;
                for (Integer num : AgentsAddon.this.iJG.keySet()) {
                    NPC auf2 = (NPC) AgentsAddon.this.iJG.get(num);
                    if (auf2 == null) {
                        auf = auf2;
                    } else {
                        i++;
                        auf = auf2;
                    }
                }
                AgentsAddon.this.m44636a(auf, i);
            }
            this.gEk = component.getWidth();
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$b */
    /* compiled from: a */
    class C4735b extends C6124ags<C5783aaP> {
        C4735b() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            boolean z = aap.bMO() == C5783aaP.C1841a.DOCKED;
            AgentsAddon.this.f10131kj.aVU().mo13975h(new C0269DS(AgentsAddon.this.aCF, z));
            if (z && AgentsAddon.this.hax.isEnabled()) {
                C5905ach.m20567b((IAddonSettings) AgentsAddon.this.f10131kj).mo12679a((ActionListener) new C4736a());
            }
            return false;
        }

        /* renamed from: taikodom.addon.mission.AgentsAddon$b$a */
        class C4736a implements ActionListener {
            C4736a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                AgentsAddon.this.dtm();
            }
        }
    }

    /* renamed from: taikodom.addon.mission.AgentsAddon$a */
    class C4733a implements C6144ahM<Agent> {
        C4733a() {
        }

        /* renamed from: a */
        public void mo1931n(Agent abk) {
            if (abk != null) {
                Timer timer = new Timer(2000, new C4734a(abk));
                timer.setRepeats(false);
                timer.start();
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: taikodom.addon.mission.AgentsAddon$a$a */
        class C4734a implements ActionListener {
            private final /* synthetic */ Agent dbX;

            C4734a(Agent abk) {
                this.dbX = abk;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                AgentsAddon.this.m44631E(this.dbX);
            }
        }
    }
}
