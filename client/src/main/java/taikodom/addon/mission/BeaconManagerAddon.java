package taikodom.addon.mission;

import game.script.Actor;
import logic.IAddonSettings;
import logic.aaa.C5274aDa;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.mission")
/* compiled from: a */
public class BeaconManagerAddon implements C2495fo {
    /* access modifiers changed from: private */
    public C4765a ecA;
    /* renamed from: kj */
    public IAddonProperties f10136kj;
    /* access modifiers changed from: private */
    private C6124ags<C5274aDa> ecz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10136kj = addonPropertie;
        this.ecA = new C4765a();
        this.ecz = new C4766b();
        this.f10136kj.aVU().mo13965a(C5274aDa.class, this.ecz);
    }

    public void stop() {
        this.f10136kj.aVU().mo13974g(this.ecA);
        this.f10136kj.aVU().mo13968b(C5274aDa.class, this.ecz);
    }

    /* renamed from: taikodom.addon.mission.BeaconManagerAddon$a */
    public class C4765a {

        /* renamed from: pI */
        Actor f10137pI;

        public C4765a() {
        }

        /* renamed from: ha */
        public Actor mo24355ha() {
            return this.f10137pI;
        }
    }

    /* renamed from: taikodom.addon.mission.BeaconManagerAddon$b */
    /* compiled from: a */
    class C4766b extends C6124ags<C5274aDa> {
        C4766b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5274aDa ada) {
            if (ada.mo8424ha() == null) {
                BeaconManagerAddon.this.f10136kj.aVU().mo13974g(BeaconManagerAddon.this.ecA);
                return false;
            }
            BeaconManagerAddon.this.ecA.f10137pI = ada.mo8424ha();
            BeaconManagerAddon.this.f10136kj.aVU().publish(BeaconManagerAddon.this.ecA);
            return false;
        }
    }
}
