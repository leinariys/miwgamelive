package taikodom.addon.hoplons;

import game.network.message.externalizable.C0939Nn;
import game.script.item.ComponentType;
import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import game.script.itembilling.ItemBillingOrder;
import game.script.player.Player;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.*;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

@TaikodomAddon("taikodom.addon.hoplons")
/* renamed from: a.q */
/* compiled from: a */
public class C3291q {

    /* access modifiers changed from: private */
    public C5923acz akV;
    /* access modifiers changed from: private */
    public JTextField akY;
    /* access modifiers changed from: private */
    public C2495fo apa;
    /* access modifiers changed from: private */
    public JButton dcI;
    /* access modifiers changed from: private */
    public String ejz;
    /* access modifiers changed from: private */
    public C2939lu gNV;
    /* access modifiers changed from: private */
    public JTextField gNW;
    /* access modifiers changed from: private */
    public JLabel gNX;
    /* access modifiers changed from: private */
    public Table gNZ;
    /* renamed from: kj */
    public IAddonProperties f8860kj;
    /* renamed from: P */
    private Player f8859P;
    private JButton dcH;
    private C5519aMl ejA = new C3390r(this);
    private JButton gNY;
    private RowFilter gOa;
    /* access modifiers changed from: private */
    private C6964axd gOb;
    /* renamed from: nM */
    private InternalFrame f8861nM;

    public C3291q(IAddonProperties vWVar, C2495fo foVar, C2939lu luVar, InternalFrame nxVar) {
        this.f8860kj = vWVar;
        this.apa = foVar;
        this.f8859P = vWVar.getPlayer();
        this.gNV = luVar;
        this.f8861nM = nxVar;
        cBm();
        aRX();
    }

    private void cBm() {
        this.gNW = this.f8861nM.mo4915cd("conversionOfHSTextField");
        this.gNX = this.f8861nM.mo4915cd("conversionToTSTextField");
        this.dcI = this.f8861nM.mo4913cb("convertionButton");
        this.gNY = this.f8861nM.mo4913cb("linkHoplonsSiteButton");
        this.akY = this.f8861nM.mo4920ci("searchTextField");
        this.dcH = this.f8861nM.mo4913cb("buyItemsButton");
        this.gNX.setText(!cBv() ? "0" : this.gNW.getText());
    }

    public void cBn() {
        ((HoplonsAddon) this.apa).cUJ();
        if (this.akV == null) {
            this.akV = this.f8861nM.mo4915cd("itemsTree");
            this.gNZ = this.f8861nM.mo4915cd("itemsTable");
            cBq();
            cBo();
            cBr();
            cBs();
        }
        bWw();
    }

    private void cBo() {
        this.gNZ.setAutoCreateRowSorter(true);
        this.gNZ.setModel(new C3292a());
        this.gNZ.getRowSorter().setComparator(0, new C3294c());
        this.gNZ.getRowSorter().setComparator(1, new C3293b());
        this.gNZ.getRowSorter().setComparator(2, new C3296e());
        this.gNZ.getColumnModel().getColumn(0).setCellRenderer(new C3295d());
        this.gNZ.getColumnModel().getColumn(1).setCellRenderer(new C3298g());
        this.gNZ.getColumnModel().getColumn(2).setCellRenderer(new C3297f());
        this.gOa = new C3299h();
        cBp();
    }

    /* access modifiers changed from: private */
    public void cBp() {
        this.gNZ.getRowSorter().setRowFilter(this.gOa);
    }

    public void bWw() {
        if (this.f8861nM != null) {
            this.gNZ.getModel().fireTableDataChanged();
            C5616aQe.m17578a(this.gNZ, (Insets) null, true, false);
        }
    }

    private void cBq() {
        this.akV = this.f8861nM.mo4915cd("itemsTree");
        this.gOb = new C6964axd("hidden root", new C3306o());
        TreePath treePath = new TreePath(this.gOb);
        this.akV.setModel(new C5518aMk(this.gOb, this.ejA));
        this.akV.setCellRenderer(new C3305n());
        m37397a(this.gOb);
        this.akV.expandPath(treePath);
        this.akV.setSelectionPath(treePath);
        this.akV.setVisible(true);
    }

    /* renamed from: a */
    private void m37397a(C6964axd axd) {
        ItemTypeCategory HC;
        HashMap hashMap = new HashMap();
        for (ItemBillingOrder next : this.gNV.bms()) {
            if (!(next.mo22367az() == null || (HC = next.mo22367az().mo19866HC()) == null || next.ajU() == 0)) {
                ItemTypeCategory bJS = HC.bJS();
                if (!hashMap.containsKey(bJS)) {
                    hashMap.put(bJS, new HashSet());
                }
                ((Set) hashMap.get(bJS)).add(HC);
            }
        }
        ArrayList<ItemTypeCategory> arrayList = new ArrayList<>(hashMap.keySet());
        C3304m mVar = new C3304m();
        Collections.sort(arrayList, mVar);
        for (ItemTypeCategory aai : arrayList) {
            C6964axd axd2 = new C6964axd(aai, this.ejA);
            axd.add(axd2);
            ArrayList<ItemTypeCategory> arrayList2 = new ArrayList<>((Collection) hashMap.get(aai));
            Collections.sort(arrayList2, mVar);
            for (ItemTypeCategory axd3 : arrayList2) {
                axd2.add(new C6964axd(axd3, this.ejA));
            }
        }
    }

    private void aRX() {
        this.dcI.addActionListener(new C3303l());
        this.gNY.addActionListener(new C3310s());
        this.gNW.addKeyListener(new C3309r());
        this.dcH.addActionListener(new C3308q());
        this.akY.getDocument().addDocumentListener(new C3307p());
    }

    private void cBr() {
        this.akV.addTreeSelectionListener(new C3302k());
    }

    private void cBs() {
        this.gNZ.addMouseListener(new C3301j());
    }

    public void cBt() {
        if (this.f8861nM != null) {
            this.gNW.setText("");
            this.gNX.setText("0");
            this.akY.setText("");
        }
    }

    /* access modifiers changed from: private */
    public void cBu() {
        int selectedRow = this.gNZ.getSelectedRow();
        if (selectedRow == -1) {
            mo21271eR(this.f8860kj.translate("Select an item."));
            return;
        }
        this.gNV.mo20439m(this.f8859P, (ItemBillingOrder) this.gNZ.getModel().getValueAt(this.gNZ.convertRowIndexToModel(selectedRow), -1));
    }

    /* access modifiers changed from: private */
    public boolean cBv() {
        try {
            return this.gNV.mo20437fh(Long.parseLong(this.gNW.getText()));
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void cBw() {
        ((MessageDialogAddon) this.f8860kj.mo11830U(MessageDialogAddon.class)).mo24307a(this.f8860kj.translate("Confirm the transfer, please."), (aEP) new C3300i(), this.f8860kj.translate("No"), this.f8860kj.translate("Yes"));
    }

    /* renamed from: eR */
    public void mo21271eR(String str) {
        ((MessageDialogAddon) this.f8860kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, str, this.f8860kj.translate("Ok"));
    }

    /* renamed from: ky */
    public void mo21273ky(String str) {
        ((MessageDialogAddon) this.f8860kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.ERROR, str, this.f8860kj.translate("Ok"));
    }

    /* renamed from: kz */
    public void mo21274kz(String str) {
        ((MessageDialogAddon) this.f8860kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.INFO, str, this.f8860kj.translate("Ok"));
    }

    /* renamed from: hb */
    public void mo21272hb(boolean z) {
        this.gNY.setEnabled(z);
    }

    /* renamed from: Kk */
    public void mo21266Kk() {
        this.f8861nM.mo4911Kk();
    }

    /* renamed from: Kl */
    public void mo21267Kl() {
        this.f8861nM.mo4912Kl();
    }

    /* renamed from: a.q$a */
    class C3292a extends AbstractTableModel {


        C3292a() {
        }

        public int getColumnCount() {
            return 3;
        }

        public int getRowCount() {
            return C3291q.this.gNV.bms().size();
        }

        public Object getValueAt(int i, int i2) {
            return C3291q.this.gNV.bms().get(i);
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return C3291q.this.f8860kj.translate("Item Name");
                case 1:
                    return C3291q.this.f8860kj.translate("Cost");
                case 2:
                    return C3291q.this.f8860kj.translate("Durability (days)");
                default:
                    return "$POST_A_TICKET_PLEASE";
            }
        }
    }

    /* renamed from: a.q$c */
    /* compiled from: a */
    class C3294c implements Comparator<ItemBillingOrder> {
        C3294c() {
        }

        /* renamed from: a */
        public int compare(ItemBillingOrder uNVar, ItemBillingOrder uNVar2) {
            return uNVar.mo22367az().mo19891ke().get().compareTo(uNVar2.mo22367az().mo19891ke().get());
        }
    }

    /* renamed from: a.q$b */
    /* compiled from: a */
    class C3293b implements Comparator<ItemBillingOrder> {
        C3293b() {
        }

        /* renamed from: a */
        public int compare(ItemBillingOrder uNVar, ItemBillingOrder uNVar2) {
            return (int) (Long.valueOf(uNVar.ajU()).longValue() - Long.valueOf(uNVar2.ajU()).longValue());
        }
    }

    /* renamed from: a.q$e */
    /* compiled from: a */
    class C3296e implements Comparator<ItemBillingOrder> {
        C3296e() {
        }

        /* renamed from: a */
        public int compare(ItemBillingOrder uNVar, ItemBillingOrder uNVar2) {
            return (int) (Long.valueOf(uNVar.ajW()).longValue() - Long.valueOf(uNVar2.ajW()).longValue());
        }
    }

    /* renamed from: a.q$d */
    /* compiled from: a */
    class C3295d extends C2122cE {
        C3295d() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            ItemType az = ((ItemBillingOrder) obj).mo22367az();
            Panel nR = aur.mo11631nR("nameView");
            LabeledIcon cd = nR.mo4915cd("itemItemIcon");
            Picture a = cd.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
            cd.mo2605a((Icon) new C2539gY(az));
            if (az instanceof ComponentType) {
                ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) az).cuJ());
                a.setVisible(true);
            } else {
                a.setVisible(false);
            }
            cd.setVisible(true);
            nR.mo4917cf("nameItem").setText(az.mo19891ke().get());
            return nR;
        }
    }

    /* renamed from: a.q$g */
    /* compiled from: a */
    class C3298g extends C2122cE {
        C3298g() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            Panel nR = aur.mo11631nR("costView");
            nR.mo4915cd("costLabel").setText(Long.toString(((ItemBillingOrder) obj).ajU()));
            return nR;
        }
    }

    /* renamed from: a.q$f */
    /* compiled from: a */
    class C3297f extends C2122cE {
        C3297f() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("durabilityView");
            nR.setText(Long.toString(Long.valueOf(((ItemBillingOrder) obj).ajW()).longValue()));
            return nR;
        }
    }

    /* renamed from: a.q$h */
    /* compiled from: a */
    class C3299h extends RowFilter {
        C3299h() {
        }

        public boolean include(RowFilter.Entry entry) {
            if (C3291q.this.akY.getText().equals("")) {
                if (!m37426a((RowFilter.Entry<DefaultMutableTreeNode, ItemType>) entry, C3291q.this.akY) || !m37424a(entry) || !m37425a(entry, C3291q.this.akV)) {
                    return false;
                }
                return true;
            } else if ((C3291q.this.akV.getLastSelectedPathComponent() != null || !m37426a((RowFilter.Entry<DefaultMutableTreeNode, ItemType>) entry, C3291q.this.akY) || !m37424a(entry)) && !m37425a(entry, C3291q.this.akV)) {
                return false;
            } else {
                return true;
            }
        }

        /* renamed from: a */
        private boolean m37426a(RowFilter.Entry<DefaultMutableTreeNode, ItemType> entry, JTextField jTextField) {
            return ((ItemBillingOrder) entry.getValue(0)).mo22367az().mo19891ke().get().toLowerCase().contains(jTextField.getText().toLowerCase());
        }

        /* renamed from: a */
        private boolean m37424a(RowFilter.Entry entry) {
            if (((ItemBillingOrder) entry.getValue(0)).ajU() == 0) {
                return false;
            }
            return true;
        }

        /* renamed from: a */
        private boolean m37425a(RowFilter.Entry entry, C5923acz acz) {
            ItemType az;
            boolean z = true;
            if (!(entry.getValue(0) instanceof ItemBillingOrder) || (az = ((ItemBillingOrder) entry.getValue(0)).mo22367az()) == null || az.mo19866HC() == null) {
                return false;
            }
            DefaultMutableTreeNode defaultMutableTreeNode = (DefaultMutableTreeNode) acz.getLastSelectedPathComponent();
            if (defaultMutableTreeNode != null) {
                if (!defaultMutableTreeNode.isRoot()) {
                    if (defaultMutableTreeNode.getUserObject() != null) {
                        if (!defaultMutableTreeNode.isLeaf()) {
                            z = az.mo19866HC().bJS() == defaultMutableTreeNode.getUserObject();
                        } else if (az.mo19866HC() != defaultMutableTreeNode.getUserObject()) {
                            z = false;
                        }
                    }
                }
                return z;
            }
            z = false;
            return z;
        }
    }

    /* renamed from: a.q$o */
    /* compiled from: a */
    class C3306o extends C5519aMl {
        C3306o() {
        }

        /* renamed from: a */
        public boolean mo10165a(Object obj) {
            return false;
        }

        /* renamed from: z */
        public boolean mo10166z() {
            return false;
        }
    }

    /* renamed from: a.q$n */
    /* compiled from: a */
    class C3305n extends C0037AU {
        C3305n() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x0090  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0099  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.awt.Component mo307a(aUR r9, java.lang.Object r10, boolean r11, boolean r12, boolean r13, int r14, boolean r15) {
            /*
                r8 = this;
                r3 = 0
                java.lang.String r4 = ""
                if (r13 == 0) goto L_0x0020
                java.lang.String r0 = "itemsTreeLeaf"
                java.awt.Component r0 = r9.mo11631nR(r0)
                a.aCo r0 = (logic.ui.C5262aCo) r0
                java.lang.String r1 = "itemsTreeNameLeaf"
                javax.swing.JLabel r1 = r0.mo4917cf(r1)
                r2 = r0
            L_0x0014:
                javax.swing.tree.DefaultMutableTreeNode r10 = (javax.swing.tree.DefaultMutableTreeNode) r10
                boolean r0 = r10.isRoot()
                if (r0 == 0) goto L_0x0042
                r1.setIcon(r3)
            L_0x001f:
                return r2
            L_0x0020:
                if (r12 == 0) goto L_0x0032
                java.lang.String r0 = "itemsTreeParentExpanded"
                java.awt.Component r0 = r9.mo11631nR(r0)
                a.aCo r0 = (logic.ui.C5262aCo) r0
                java.lang.String r1 = "itemsTreeNameParentExpanded"
                javax.swing.JLabel r1 = r0.mo4917cf(r1)
                r2 = r0
                goto L_0x0014
            L_0x0032:
                java.lang.String r0 = "itemsTreeParent"
                java.awt.Component r0 = r9.mo11631nR(r0)
                a.aCo r0 = (logic.ui.C5262aCo) r0
                java.lang.String r1 = "itemsTreeNameParent"
                javax.swing.JLabel r1 = r0.mo4917cf(r1)
                r2 = r0
                goto L_0x0014
            L_0x0042:
                java.lang.Object r0 = r10.getUserObject()
                boolean r5 = r0 instanceof p001a.C5802aai
                if (r5 == 0) goto L_0x009d
                a.aai r0 = (p001a.C5802aai) r0
                taikodom.infra.game.script.I18NString r4 = r0.mo12246ke()
                java.lang.String r4 = r4.get()
                a.tC r5 = r0.mo12100sK()
                if (r5 == 0) goto L_0x009d
                a.q r5 = taikodom.addon.hoplons.C3291q.this
                a.vW r5 = r5.f8860kj
                a.axa r5 = r5.bHv()
                a.azB r5 = r5.adz()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                a.aHa r7 = logic.swing.C5378aHa.ICONOGRAPHY
                java.lang.String r7 = r7.getPath()
                java.lang.String r7 = java.lang.String.valueOf(r7)
                r6.<init>(r7)
                a.tC r0 = r0.mo12100sK()
                java.lang.String r0 = r0.getHandle()
                java.lang.StringBuilder r0 = r6.append(r0)
                java.lang.String r0 = r0.toString()
                java.awt.Image r0 = r5.getImage(r0)
            L_0x008b:
                r1.setText(r4)
                if (r0 == 0) goto L_0x0099
                javax.swing.ImageIcon r3 = new javax.swing.ImageIcon
                r3.<init>(r0)
                r1.setIcon(r3)
                goto L_0x001f
            L_0x0099:
                r1.setIcon(r3)
                goto L_0x001f
            L_0x009d:
                r0 = r3
                goto L_0x008b
            */
            throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.hoplons.C3291q.C3305n.mo307a(a.aUR, java.lang.Object, boolean, boolean, boolean, int, boolean):java.awt.Component");
        }
    }

    /* renamed from: a.q$m */
    /* compiled from: a */
    class C3304m implements Comparator<ItemTypeCategory> {
        C3304m() {
        }

        /* renamed from: a */
        public int compare(ItemTypeCategory aai, ItemTypeCategory aai2) {
            return aai.mo12246ke().get().compareTo(aai2.mo12246ke().get());
        }
    }

    /* renamed from: a.q$l */
    /* compiled from: a */
    class C3303l implements ActionListener {
        C3303l() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3291q.this.cBw();
        }
    }

    /* renamed from: a.q$s */
    /* compiled from: a */
    class C3310s implements ActionListener {
        C3310s() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3291q.this.gNV.bmm();
            C3291q.this.mo21274kz(C3291q.this.f8860kj.translate("You`ll be redirected to Hoplons site. Please wait."));
        }
    }

    /* renamed from: a.q$r */
    /* compiled from: a */
    class C3309r extends KeyAdapter {
        C3309r() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 10) {
                C3291q.this.cBw();
                return;
            }
            String str = "0";
            boolean z = true;
            if (C3291q.this.cBv()) {
                str = Long.toString(C3291q.this.gNV.mo20438fi(Long.parseLong(C3291q.this.gNW.getText())));
            } else {
                z = false;
            }
            C3291q.this.gNX.setText(str);
            C3291q.this.dcI.setEnabled(z);
        }
    }

    /* renamed from: a.q$q */
    /* compiled from: a */
    class C3308q implements ActionListener {
        C3308q() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3291q.this.cBu();
        }
    }

    /* renamed from: a.q$p */
    /* compiled from: a */
    class C3307p implements DocumentListener {
        C3307p() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
            C3291q.this.cBp();
        }

        public void insertUpdate(DocumentEvent documentEvent) {
            m37433Fu();
        }

        public void removeUpdate(DocumentEvent documentEvent) {
            m37433Fu();
        }

        /* renamed from: Fu */
        private void m37433Fu() {
            String str;
            C3291q.this.cBp();
            if (C3291q.this.gNZ.getRowCount() == 0) {
                C3291q.this.akV.expandPath(C3291q.this.akV.bOY());
                C3291q.this.akV.setSelectionRow(0);
            }
            C3291q.this.ejz = C3291q.this.akY.getText();
            if (C3291q.this.ejz.equals("")) {
                C3291q.this.ejz = null;
            }
            C3291q.this.akV.getModel().reload();
            if (C3291q.this.akV.getRowCount() == 0 && C3291q.this.gNZ.getRowCount() != 0) {
                String str2 = "";
                int i = 0;
                String str3 = "";
                int i2 = 0;
                int i3 = 0;
                ItemTypeCategory aai = null;
                ItemTypeCategory aai2 = null;
                while (i < C3291q.this.gNZ.getRowCount()) {
                    ItemTypeCategory HC = ((ItemBillingOrder) C3291q.this.gNZ.getValueAt(i, 0)).mo22367az().mo19866HC();
                    if (aai != HC.bJS()) {
                        aai = HC.bJS();
                        i2++;
                        str3 = ((ItemBillingOrder) C3291q.this.gNZ.getValueAt(i, 0)).mo22367az().mo19866HC().bJS().mo12246ke().get();
                    }
                    if (aai2 != HC) {
                        str = ((ItemBillingOrder) C3291q.this.gNZ.getValueAt(i, 0)).mo22367az().mo19866HC().mo12246ke().get();
                        i3++;
                    } else {
                        str = str2;
                        HC = aai2;
                    }
                    i++;
                    str2 = str;
                    aai2 = HC;
                }
                if (i2 == 1) {
                    if (i3 <= 1) {
                        C3291q.this.ejz = str2;
                    } else {
                        C3291q.this.ejz = str3;
                    }
                    C3291q.this.akV.getModel().reload();
                    C3291q.this.akV.expandAll();
                    return;
                }
                C3291q.this.ejz = null;
                C3291q.this.akV.getModel().reload();
            } else if (C3291q.this.akV.getRowCount() == 1) {
                C3291q.this.akV.expandRow(0);
            } else if (C3291q.this.ejz == null) {
                C3291q.this.akV.expandPath(C3291q.this.akV.bOY());
                C3291q.this.akV.setSelectionPath(C3291q.this.akV.bOY());
            }
        }
    }

    /* renamed from: a.q$k */
    /* compiled from: a */
    class C3302k implements TreeSelectionListener {
        C3302k() {
        }

        public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
            C3291q.this.cBp();
        }
    }

    /* renamed from: a.q$j */
    /* compiled from: a */
    class C3301j extends MouseAdapter {
        C3301j() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 1 && mouseEvent.getClickCount() == 2) {
                C3291q.this.cBu();
            }
        }
    }

    /* renamed from: a.q$i */
    /* compiled from: a */
    class C3300i extends aEP {
        C3300i() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                try {
                    long parseLong = Long.parseLong(C3291q.this.gNW.getText());
                    if (!C3291q.this.gNV.mo20437fh(parseLong)) {
                        C3291q.this.mo21271eR(C3291q.this.f8860kj.translate("Insuficient Hoplons credits."));
                        return;
                    }
                    if (!C3291q.this.f8860kj.getPlayer().mo14432me(parseLong)) {
                        C3291q.this.f8860kj.getLog().error("Invalid conversion operation.");
                        C3291q.this.mo21273ky(C3291q.this.f8860kj.translate("Transaction error. Try again."));
                    }
                    ((HoplonsAddon) C3291q.this.apa).cUJ();
                    C3291q.this.mo21274kz(C5956adg.format(C3291q.this.f8860kj.translate("transferHoplonsCompleteMessage - H${0} to T${1}"), Long.valueOf(parseLong), Long.valueOf(C3291q.this.gNV.mo20438fi(parseLong))));
                } catch (Exception e) {
                    C3291q.this.mo21271eR(C3291q.this.f8860kj.translate("Incorrect value."));
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }
}
