package taikodom.addon.hoplons;

import game.network.message.externalizable.C0352Eg;
import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1749Zq;
import game.network.message.serializable.C3438ra;
import game.script.citizenship.*;
import game.script.nls.NLSManager;
import game.script.nls.NLSWindowAlert;
import game.script.npc.NPC;
import logic.aPE;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.TextArea;
import logic.ui.item.TextField;
import logic.ui.item.*;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

@TaikodomAddon("taikodom.addon.hoplons")
/* renamed from: a.rJ */
/* compiled from: a */
public class C3405rJ {
    private static final long ONE_HOUR = 3600000;
    private static final long iWf = 900000;
    /* access modifiers changed from: private */
    public InternalFrame iWj;
    /* access modifiers changed from: private */
    public CitizenshipPack iWk;
    /* access modifiers changed from: private */
    public JList iWm;
    /* renamed from: kj */
    public IAddonProperties f8977kj;
    /* renamed from: nM */
    public InternalFrame f8978nM;
    private C2495fo apa;
    private TextArea iWg;
    private logic.ui.item.TextField iWh;
    private TextField iWi;
    private List<CitizenshipPack> iWl;
    /* access modifiers changed from: private */
    private Repeater<CitizenImprovementType> iWn;
    /* access modifiers changed from: private */
    private aPE iWo;

    public C3405rJ(IAddonProperties vWVar, C2495fo foVar, C2973mS mSVar, InternalFrame nxVar) {
        this.apa = foVar;
        this.f8977kj = vWVar;
        this.f8978nM = nxVar;
        m38069iM();
        dzq();
        dzp();
    }

    /* access modifiers changed from: private */
    public void dzp() {
        for (Citizenship vm : this.f8977kj.getPlayer().cXk().bqx()) {
            m38064d(vm);
            if (vm.getExpiration() < ONE_HOUR) {
                m38067e(vm);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m38064d(Citizenship vm) {
        this.f8977kj.aVU().mo13975h(new C0352Eg(this.f8977kj.getPlayer(), ((NLSWindowAlert) this.f8977kj.ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT)).dcs(), vm.bJB().mo12809rP().get(), m38065dn(vm.getExpiration())));
    }

    /* renamed from: e */
    private void m38067e(Citizenship vm) {
        this.iWo = new C3410d(vm);
        this.f8977kj.mo11833a((long) iWf, this.iWo);
    }

    private void dzq() {
        this.f8978nM.mo4917cf("title").setText(this.f8977kj.translate("Affiliations global title"));
        this.f8978nM.mo4915cd("information").setText(this.f8977kj.translate("Afilliations global description"));
        this.f8978nM.mo4913cb("moreinfo").setVisible(false);
        this.f8978nM.mo4913cb("acquire").setEnabled(false);
    }

    /* renamed from: iM */
    private void m38069iM() {
        this.iWl = this.f8977kj.ala().aMw().aLw();
        dzs();
        this.f8978nM.mo4913cb("acquire").addActionListener(new C3411e());
        this.iWm.addListSelectionListener(new C3408b());
        this.f8978nM.mo4913cb("moreinfo").addActionListener(new C3409c());
        this.f8978nM.mo4913cb("previous").addActionListener(new C3413g());
        this.iWg = this.f8978nM.mo4915cd("information");
        this.iWh = this.f8978nM.mo4920ci("value");
        this.iWi = this.f8978nM.mo4920ci("expiration");
    }

    /* access modifiers changed from: private */
    public void dzr() {
        if (!this.iWm.isSelectionEmpty()) {
            CitizenshipPack acu = (CitizenshipPack) this.iWm.getSelectedValue();
            this.f8978nM.mo4917cf("title").setText(acu.bOV().mo12809rP().get());
            this.f8978nM.mo4917cf("title").setHorizontalAlignment(0);
            if (acu.bOV().bRJ() != null) {
                ComponentManager.getCssHolder(this.f8978nM.mo4915cd("citizenshipMainPanel")).mo13049Vr().mo369a(this.f8977kj.bHv().adz().getImage(String.valueOf(C5378aHa.CITIZENSHIP.getPath()) + acu.bOV().bRJ().getHandle()));
            }
            this.f8978nM.mo4915cd("information").setText(acu.bOV().mo12815vW().get());
            this.f8978nM.mo4913cb("moreinfo").setVisible(true);
            if (this.f8977kj.getPlayer().bQB()) {
                this.f8978nM.mo4913cb("acquire").setEnabled(true);
            }
            m38062cG(acu.bOV().bRL());
            m38061cF(acu.bOV().bSf());
        }
    }

    /* renamed from: cF */
    private void m38061cF(C3438ra<CitizenshipReward> raVar) {
    }

    /* renamed from: cG */
    private void m38062cG(C3438ra<CitizenImprovementType> raVar) {
        this.iWn.clear();
        for (CitizenImprovementType G : raVar) {
            this.iWn.mo22248G(G);
        }
    }

    private void dzs() {
        this.iWm = this.f8978nM.mo4915cd("typesTable");
        this.iWm.setModel(new C2302dn(this.iWl));
        this.iWm.setCellRenderer(new C3414h());
        this.iWn = this.f8978nM.mo4915cd("improvements");
        this.iWn.mo22250a(new C3415i());
    }

    /* renamed from: dn */
    private String m38065dn(long j) {
        C3987xk xkVar = C3987xk.bFK;
        long j2 = j / ONE_HOUR;
        long j3 = (j - (ONE_HOUR * j2)) / 60000;
        long j4 = j2 % 24;
        long j5 = (j2 - j4) / 24;
        if (j5 <= 0) {
            return String.format("%02d:%02d %s", new Object[]{Long.valueOf(j2), Long.valueOf(j3), this.f8977kj.translate("hour(s)")});
        }
        if (j4 > 12) {
            j5++;
        }
        return String.valueOf(xkVar.format(j5)) + " " + this.f8977kj.translate("days");
    }

    /* access modifiers changed from: protected */
    public void dzt() {
        if (this.iWj == null) {
            this.iWj = this.f8977kj.bHv().mo16792bN("confirm-acquisition.xml");
            this.iWj.mo4913cb("confirm").addActionListener(new C3416j());
            this.iWj.mo4913cb("cancel").addActionListener(new C3412f());
        }
        C3987xk xkVar = C3987xk.bFK;
        this.iWj.mo4917cf("title").setText(this.iWk.bOV().mo12809rP().get());
        this.iWj.mo4917cf("price").setText(String.valueOf(this.iWk.mo12738rZ()));
        this.iWj.mo4917cf("lifeTime").setText(String.valueOf(String.valueOf(xkVar.mo22980dY(this.iWk.bOT()))) + " " + this.f8977kj.translate("days"));
        if (this.iWk.bOV().mo12811sK() != null) {
            Picture cd = this.iWj.mo4915cd("icon");
            cd.putClientProperty(C0564Ho.f691DU, this.iWk);
            ComponentManager.getCssHolder(cd).mo13049Vr().mo369a(this.f8977kj.bHv().adz().getImage(String.valueOf(C5378aHa.CITIZENSHIP.getPath()) + this.iWk.bOV().mo12811sK().getHandle()));
        }
        long k = this.f8977kj.getPlayer().mo14427k(this.iWk.bOV());
        JLabel cf = this.iWj.mo4917cf("lifeTimeLabel");
        if (k > 0) {
            cf.setText(m38065dn(k));
            cf.setVisible(true);
        } else {
            cf.setVisible(false);
        }
        this.iWj.pack();
        this.iWj.center();
        this.iWj.toFront();
        this.iWj.setVisible(true);
    }

    /* access modifiers changed from: protected */
    public void dzu() {
        this.iWj.mo4911Kk();
        ((CitizenshipOffice) C3582se.m38985a(this.f8977kj.ala().aMw(), (C6144ahM<?>) new C3406a())).mo5462i(this.iWk);
    }

    /* access modifiers changed from: protected */
    /* renamed from: F */
    public void mo21582F(NPC auf) {
        this.iWg.setText(auf.getName());
        this.iWh.setText(auf.getName());
        this.iWi.setText(auf.getName());
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m38060c(Boolean bool) {
        if (bool.booleanValue()) {
            this.f8978nM.setVisible(false);
            ((MessageDialogAddon) this.f8977kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.INFO, C5956adg.format(this.f8977kj.translate("Welcome to {0}!"), this.iWk.bOV().mo12809rP().get()), this.f8977kj.translate("Ok"));
            ((HoplonsAddon) this.apa).cUJ();
            return;
        }
        ((MessageDialogAddon) this.f8977kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f8977kj.translate("Sorry. You don't have enough Hoplons to buy {0}."), this.iWk.bOV().mo12809rP().get()), (aEP) null, this.f8977kj.translate("Ok"));
    }

    public void cBt() {
    }

    /* renamed from: a.rJ$d */
    /* compiled from: a */
    class C3410d implements aPE {
        private final /* synthetic */ Citizenship gML;

        C3410d(Citizenship vm) {
            this.gML = vm;
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            C3405rJ.this.m38064d(this.gML);
            if (this.gML.getExpiration() > C3405rJ.iWf) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: a.rJ$e */
    /* compiled from: a */
    class C3411e implements ActionListener {
        C3411e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C3405rJ.this.iWm.getSelectedValue() == null) {
                ((MessageDialogAddon) C3405rJ.this.f8977kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, C3405rJ.this.f8977kj.translate("Select an item first"), C3405rJ.this.f8977kj.translate("Ok"));
                return;
            }
            C3405rJ.this.iWk = (CitizenshipPack) C3405rJ.this.iWm.getSelectedValue();
            C3405rJ.this.dzt();
        }
    }

    /* renamed from: a.rJ$b */
    /* compiled from: a */
    class C3408b implements ListSelectionListener {
        C3408b() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            C3405rJ.this.dzr();
        }
    }

    /* renamed from: a.rJ$c */
    /* compiled from: a */
    class C3409c implements ActionListener {
        C3409c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3405rJ.this.f8978nM.mo4915cd("mainPanel").getLayout().show(C3405rJ.this.f8978nM.mo4915cd("mainPanel"), "DETAIL");
        }
    }

    /* renamed from: a.rJ$g */
    /* compiled from: a */
    class C3413g implements ActionListener {
        C3413g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3405rJ.this.f8978nM.mo4915cd("mainPanel").getLayout().show(C3405rJ.this.f8978nM.mo4915cd("mainPanel"), "MAIN");
        }
    }

    /* renamed from: a.rJ$h */
    /* compiled from: a */
    class C3414h extends C5517aMj {
        C3414h() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            CitizenshipPack acu = (CitizenshipPack) obj;
            Panel dAx = aur.dAx();
            dAx.mo4917cf("price").setText(String.valueOf(acu.mo12738rZ()));
            Panel aco = (Panel) dAx.mo4916ce("lifeTime");
            aco.getLayout().show(aco, "LIFE_TIME_INFO");
            aco.mo4917cf("lifeTimeInfo").setText(String.valueOf(String.valueOf((int) acu.bOT())) + " " + C3405rJ.this.f8977kj.translate("days"));
            if (acu.bOV().mo12811sK() != null) {
                Panel cd = dAx.mo4915cd("iconPanel");
                cd.putClientProperty(C0564Ho.f691DU, acu);
                ComponentManager.getCssHolder(cd).mo13049Vr().mo369a(C3405rJ.this.f8977kj.bHv().adz().getImage(String.valueOf(C5378aHa.CITIZENSHIP.getPath()) + acu.bOV().mo12811sK().getHandle()));
            }
            return dAx;
        }
    }

    /* renamed from: a.rJ$i */
    /* compiled from: a */
    class C3415i implements Repeater.C3671a<CitizenImprovementType> {
        C3415i() {
        }

        /* renamed from: a */
        public void mo843a(CitizenImprovementType kt, Component component) {
            Panel aco = (Panel) component;
            if (kt.mo3548sK() != null) {
                aco.mo4917cf("improvementIcon").setIcon(new ImageIcon(C3405rJ.this.f8977kj.bHv().adz().getImage(String.valueOf(C5378aHa.ICONOGRAPHY.getPath()) + kt.mo3548sK().getHandle())));
            }
            aco.mo4915cd("information").setText(kt.mo3550vW().get());
        }
    }

    /* renamed from: a.rJ$j */
    /* compiled from: a */
    class C3416j implements ActionListener {
        C3416j() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3405rJ.this.dzu();
        }
    }

    /* renamed from: a.rJ$f */
    /* compiled from: a */
    class C3412f implements ActionListener {
        C3412f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C3405rJ.this.iWj.destroy();
            C3405rJ.this.iWj = null;
        }
    }

    /* renamed from: a.rJ$a */
    class C3406a implements C6144ahM<Boolean> {
        C3406a() {
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            SwingUtilities.invokeLater(new C3407a(bool));
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: a.rJ$a$a */
        class C3407a implements Runnable {
            private final /* synthetic */ Boolean hHv;

            C3407a(Boolean bool) {
                this.hHv = bool;
            }

            public void run() {
                C3405rJ.this.m38060c(this.hHv);
                C3405rJ.this.dzp();
                C3405rJ.this.iWj.mo4912Kl();
                C3405rJ.this.iWj.destroy();
                C3405rJ.this.iWj = null;
                C3405rJ.this.f8977kj.aVU().mo13975h(new C1749Zq());
            }
        }
    }
}
