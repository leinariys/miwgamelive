package taikodom.addon.hoplons;

import game.script.player.Player;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.hoplons")
/* renamed from: a.afD  reason: case insensitive filesystem */
/* compiled from: a */
public class C6031afD {

    /* renamed from: P */
    private Player f4450P;
    private C2495fo apa;
    private C2659iG fuV;

    /* renamed from: kj */
    private IAddonProperties f4451kj;

    public C6031afD(IAddonProperties vWVar, C2495fo foVar) {
        this.f4451kj = vWVar;
        this.f4450P = vWVar.getPlayer();
        this.apa = foVar;
        this.fuV = new C2659iG(vWVar, foVar, this);
    }

    public void dispose() {
    }

    public C2659iG bVC() {
        return this.fuV;
    }

    /* renamed from: Py */
    public void mo13230Py() {
        ((HoplonsAddon) this.apa).cUK().mo20430Py();
        ((HoplonsAddon) this.apa).cUL().mo20554Py();
    }
}
