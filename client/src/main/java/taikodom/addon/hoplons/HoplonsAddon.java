package taikodom.addon.hoplons;

import logic.IAddonSettings;
import logic.ui.item.InternalFrame;
import p001a.C0962O;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.hoplons")
/* compiled from: a */
public class HoplonsAddon implements C2495fo {
    private C6031afD dcF;
    private C2939lu gNV;
    private C2973mS hAZ;

    /* renamed from: kj */
    private IAddonProperties f9902kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9902kj = addonPropertie;
        this.dcF = new C6031afD(this.f9902kj, this);
        InternalFrame aRY = this.dcF.bVC().aRY();
        this.gNV = new C2939lu(this.f9902kj, this, aRY);
        this.hAZ = new C2973mS(this.f9902kj, this, aRY);
        aRY.addComponentListener(new C0962O("hoplons"));
    }

    public void cUJ() {
        this.dcF.bVC().aRZ();
    }

    public void stop() {
        this.dcF.dispose();
        this.gNV.dispose();
        this.hAZ.dispose();
    }

    public C2939lu cUK() {
        return this.gNV;
    }

    public C2973mS cUL() {
        return this.hAZ;
    }
}
