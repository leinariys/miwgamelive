package taikodom.addon.hoplons;

import game.network.message.externalizable.*;
import game.script.player.Player;
import logic.aaa.C5783aaP;
import logic.ui.item.InternalFrame;
import p001a.C2602hR;
import p001a.C3428rT;
import p001a.C6622aqW;
import p001a.aMU;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.hoplons")
/* renamed from: a.iG */
/* compiled from: a */
public class C2659iG {

    /* renamed from: kj */
    public IAddonProperties f8126kj;
    /* renamed from: nM */
    public InternalFrame f8127nM;
    /* renamed from: P */
    private Player f8125P;
    private C3428rT<C5783aaP> aoN;
    private C2495fo apa;
    private C6622aqW dcE;
    private C6031afD dcF;
    private JLabel dcG;
    private JButton dcH;
    /* access modifiers changed from: private */
    private JButton dcI;
    /* access modifiers changed from: private */
    private JButton dcJ;
    /* renamed from: wz */
    private C3428rT<C3689to> f8128wz;

    public C2659iG(IAddonProperties vWVar, C2495fo foVar, C6031afD afd) {
        this.f8126kj = vWVar;
        this.apa = foVar;
        this.f8125P = vWVar.getPlayer();
        this.dcF = afd;
        aRU();
        m33079Im();
        aRW();
        aRX();
    }

    private void aRU() {
        this.dcE = new C2661b("hoplons", this.f8126kj.mo11832V("tooltip", "Hoplons"), "hoplons");
        this.dcE.mo15602jH(this.f8126kj.translate("In this window you can exchange Hoplon credits for goods and Taels."));
        this.dcE.mo15603jI("HOPLONS_WINDOW");
        this.f8128wz = new C2662c();
        this.aoN = new C2663d();
        this.f8126kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f8126kj.aVU().mo13965a(C3689to.class, this.f8128wz);
        this.f8126kj.aVU().publish(new C5344aFs(this.dcE, 200, true));
        this.f8126kj.aVU().publish(new aTX(this.dcE, 100, "hoplons-commands.css", this));
        this.f8126kj.mo2317P(this);
    }

    public void stop() {
        if (this.f8127nM != null) {
            this.f8127nM.destroy();
            this.f8127nM = null;
        }
    }

    public void aRV() {
        if (this.f8127nM == null) {
            m33079Im();
            aRW();
            aRX();
        } else if (this.f8127nM.isVisible()) {
            m33078Ia();
            return;
        }
        m33086xK();
    }

    /* renamed from: Ia */
    private void m33078Ia() {
        this.f8127nM.setVisible(false);
        this.f8126kj.aVU().mo13974g(new C6018aeq("Hoplons", ""));
        this.f8126kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
    }

    /* renamed from: Im */
    private void m33079Im() {
        this.f8127nM = this.f8126kj.bHv().mo16792bN("hoplons.xml");
        this.f8127nM.addComponentListener(new C2660a());
        this.dcH = this.f8127nM.mo4913cb("buyItemsButton");
        this.dcI = this.f8127nM.mo4913cb("convertionButton");
        this.dcJ = this.f8127nM.mo4913cb("acquire");
    }

    private void aRW() {
        this.dcG = this.f8127nM.mo4917cf("hoplonsCreditsLabel");
        aRZ();
    }

    private void aRX() {
    }

    /* renamed from: xK */
    private void m33086xK() {
        this.f8126kj.aVU().publish(new C6018aeq("Hoplons", ""));
        boolean bQB = this.f8126kj.getPlayer().bQB();
        m33081a(bQB, this.dcH, true);
        m33081a(bQB, this.dcI, false);
        m33081a(bQB, this.dcJ, true);
        this.f8127nM.setVisible(true);
        this.f8127nM.center();
        this.f8126kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, getClass()));
        this.dcF.mo13230Py();
    }

    /* renamed from: a */
    private void m33081a(boolean z, JButton jButton, boolean z2) {
        String str;
        jButton.setEnabled(z && z2);
        if (!z) {
            str = this.f8126kj.translate("This button is active only inside station.");
        } else {
            str = null;
        }
        jButton.setToolTipText(str);
    }

    public InternalFrame aRY() {
        return this.f8127nM;
    }

    public void aRZ() {
        this.dcG.setText(" " + this.f8125P.bOx().mo5156Qw().bSs());
    }

    @C2602hR(mo19255zf = "HOPLONS_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo19465al(boolean z) {
        if (z && !m33084iK()) {
            aRV();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: iK */
    public boolean m33084iK() {
        if (m33085iL() || aMU.FINISHED_NURSERY.equals(this.f8125P.cXm())) {
            return false;
        }
        return true;
    }

    /* renamed from: iL */
    private boolean m33085iL() {
        return this.f8126kj != null && this.f8126kj.ala().aLS().mo22273iL();
    }

    /* renamed from: a.iG$b */
    /* compiled from: a */
    class C2661b extends C6622aqW {


        C2661b(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isEnabled() {
            if (C2659iG.this.f8126kj.ala() != null && !C2659iG.this.m33084iK()) {
                return true;
            }
            return false;
        }

        public boolean isActive() {
            return C2659iG.this.f8127nM != null && (C2659iG.this.f8127nM.isVisible() || C2659iG.this.f8127nM.isAnimating());
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            C2659iG.this.aRV();
        }
    }

    /* renamed from: a.iG$c */
    /* compiled from: a */
    class C2662c extends C3428rT<C3689to> {
        C2662c() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C2659iG.this.f8127nM != null && C2659iG.this.f8127nM.isVisible()) {
                toVar.adC();
                C2659iG.this.f8127nM.setVisible(false);
            }
        }
    }

    /* renamed from: a.iG$d */
    /* compiled from: a */
    class C2663d extends C3428rT<C5783aaP> {

        /* renamed from: Zn */
        private static /* synthetic */ int[] f8132Zn;

        C2663d() {
        }

        /* renamed from: BK */
        static /* synthetic */ int[] m33091BK() {
            int[] iArr = f8132Zn;
            if (iArr == null) {
                iArr = new int[C5783aaP.C1841a.values().length];
                try {
                    iArr[C5783aaP.C1841a.DOCKED.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5783aaP.C1841a.FLYING.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5783aaP.C1841a.JUMPING.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5783aaP.C1841a.ONI.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                f8132Zn = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            switch (m33091BK()[aap.bMO().ordinal()]) {
                case 4:
                    if (C2659iG.this.f8127nM != null && C2659iG.this.f8127nM.isVisible()) {
                        C2659iG.this.f8127nM.setVisible(false);
                        return;
                    }
                    return;
                default:
                    if (C2659iG.this.f8127nM != null && C2659iG.this.f8127nM.isVisible()) {
                        C2659iG.this.f8127nM.setVisible(false);
                        return;
                    }
                    return;
            }
        }
    }

    /* renamed from: a.iG$a */
    class C2660a extends ComponentAdapter {
        C2660a() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C2659iG.this.f8126kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
        }
    }
}
