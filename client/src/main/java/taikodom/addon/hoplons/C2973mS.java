package taikodom.addon.hoplons;

import game.script.player.Player;
import logic.ui.item.InternalFrame;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.hoplons")
/* renamed from: a.mS */
/* compiled from: a */
public class C2973mS {

    /* renamed from: P */
    private Player f8652P;
    private C3405rJ aGg;
    private InternalFrame aGh;
    private C2495fo apa;

    /* renamed from: kj */
    private IAddonProperties f8653kj;

    public C2973mS(IAddonProperties vWVar, HoplonsAddon hoplonsAddon, InternalFrame nxVar) {
        this.f8653kj = vWVar;
        this.f8652P = vWVar.getPlayer();
        this.apa = hoplonsAddon;
        this.aGh = nxVar;
        this.aGg = new C3405rJ(vWVar, hoplonsAddon, this, this.aGh);
    }

    public void dispose() {
    }

    /* renamed from: Py */
    public void mo20554Py() {
        this.aGg.cBt();
    }
}
