package taikodom.addon.hoplons;

import game.network.message.externalizable.C0939Nn;
import game.script.billing.ItemBillingDefaults;
import game.script.item.ItemType;
import game.script.itembilling.ItemBilling;
import game.script.itembilling.ItemBillingOrder;
import game.script.player.Player;
import game.script.player.PlayerAssistant;
import logic.baa.C4068yr;
import logic.ui.item.InternalFrame;
import p001a.C2370eZ;
import p001a.C3582se;
import p001a.RuntimeExec;
import p001a.aEP;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.addon.pda.PdaAddon;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.hoplons")
/* renamed from: a.lu */
/* compiled from: a */
public class C2939lu {
    private static /* synthetic */ int[] dNF;
    /* access modifiers changed from: private */
    public C2495fo apa;
    /* access modifiers changed from: private */
    public ItemBilling cLq;
    /* access modifiers changed from: private */
    public C3291q dND;
    /* access modifiers changed from: private */
    public List<ItemBillingOrder> dNE = new ArrayList();
    /* renamed from: kj */
    public IAddonProperties f8593kj;
    /* renamed from: P */
    private Player f8592P;
    private InternalFrame aGh;
    /* access modifiers changed from: private */
    private String dNC;

    public C2939lu(IAddonProperties vWVar, C2495fo foVar, InternalFrame nxVar) {
        this.f8593kj = vWVar;
        this.f8592P = vWVar.getPlayer();
        this.cLq = vWVar.ala().aJi();
        this.apa = foVar;
        this.aGh = nxVar;
        bml();
        this.dNC = bmn();
    }

    static /* synthetic */ int[] bmt() {
        int[] iArr = dNF;
        if (iArr == null) {
            iArr = new int[C2370eZ.C2371a.values().length];
            try {
                iArr[C2370eZ.C2371a.INVALID_ITEM.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C2370eZ.C2371a.INVALID_LOCATION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C2370eZ.C2371a.INVALID_PARAMETERS.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C2370eZ.C2371a.NO_FOUNDS.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C2370eZ.C2371a.UNABLE_TO_TRANSFER.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            dNF = iArr;
        }
        return iArr;
    }

    public void dispose() {
    }

    private void bml() {
        this.dND = new C3291q(this.f8593kj, this.apa, this, this.aGh);
    }

    public void bmm() {
        this.dNC = bmn();
        try {
            openToBrowser(this.dNC);
        } catch (Exception e) {
            this.f8593kj.getLog().error("Problems opening Taikodom site (Hoplons)", e);
        }
    }

    /* renamed from: gg */
    private void openToBrowser(String str) {
        RuntimeExec.openURL(str);
    }

    public String bmn() {
        String cX = this.cLq.mo12553cX();
        if (cX == null || "".equals(cX)) {
            this.dND.mo21272hb(false);
            return ItemBilling.f4187gx;
        }
        this.dND.mo21272hb(true);
        return cX;
    }

    public void bmo() {
        if (this.dND == null) {
            bml();
        }
        this.dND.mo21266Kk();
        ((ItemBilling) C3582se.m38985a(this.cLq, (C6144ahM<?>) new C2940a())).mo12556db();
    }

    /* renamed from: v */
    private C4068yr m35349v(ItemType jCVar) {
        return ((PdaAddon) this.f8593kj.mo11830U(PdaAddon.class)).mo24484aa(jCVar);
    }

    /* access modifiers changed from: private */
    public void bmp() {
        ArrayList arrayList = new ArrayList();
        for (ItemBillingOrder az : this.dNE) {
            C4068yr v = m35349v(az.mo22367az());
            if (v != null && !this.f8592P.dyl().dBQ().containsKey(v)) {
                arrayList.add(v);
            }
        }
        ((PlayerAssistant) C3582se.m38985a(this.f8592P.dyl(), (C6144ahM<?>) new C2943c())).mo11957V(arrayList);
    }

    /* renamed from: m */
    public void mo20439m(Player aku, ItemBillingOrder uNVar) {
        if (this.f8592P != aku) {
            this.dND.mo21271eR(this.f8593kj.translate("Invalid data. Please check the connection and try again."));
            this.f8593kj.getLog().error("Buy operation: Different players.");
        } else if (!this.cLq.mo12554cZ().contains(uNVar)) {
            this.dND.mo21271eR(this.f8593kj.translate("Item specified is not an item billing."));
            this.f8593kj.getLog().error("Buy operation: Item is not an item billing one.");
        } else {
            ((MessageDialogAddon) this.f8593kj.mo11830U(MessageDialogAddon.class)).mo24307a(this.f8593kj.translate("Confirm the transfer, please."), (aEP) new C2941b(aku, uNVar), this.f8593kj.translate("No"), this.f8593kj.translate("Yes"));
        }
    }

    private long bmq() {
        return this.f8593kj.getPlayer().bOx().mo5156Qw().bSs();
    }

    public long bmr() {
        ItemBillingDefaults xn = this.f8593kj.ala().aJe().mo19015xn();
        if (xn != null) {
            return xn.bmr();
        }
        this.f8593kj.getLog().error("Hoplons Conversion not defined.");
        return 0;
    }

    /* renamed from: fh */
    public boolean mo20437fh(long j) {
        return j > 0 && bmq() >= j;
    }

    /* renamed from: fi */
    public long mo20438fi(long j) {
        if (j < 0) {
            return 0;
        }
        return bmr() * j;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m35343b(C2370eZ.C2371a aVar) {
        switch (bmt()[aVar.ordinal()]) {
            case 1:
                this.dND.mo21271eR(this.f8593kj.translate("Insuficient Hoplons."));
                return;
            case 2:
                this.dND.mo21271eR(this.f8593kj.translate("Invalid location."));
                return;
            case 3:
                this.dND.mo21271eR(this.f8593kj.translate("Invalid item."));
                return;
            case 4:
                this.dND.mo21271eR(this.f8593kj.translate("Invalid data. Please check the connection and try again."));
                return;
            case 5:
                this.dND.mo21271eR(this.f8593kj.translate("Unable to transfer."));
                return;
            default:
                this.dND.mo21271eR(this.f8593kj.translate("Unknown error. Please contact assistance."));
                this.f8593kj.getLog().error("Error reported by server is unknown. - " + aVar.toString());
                return;
        }
    }

    public List<ItemBillingOrder> bms() {
        return this.dNE;
    }

    /* renamed from: Py */
    public void mo20430Py() {
        this.dND.cBt();
        bmo();
        bmn();
    }

    /* renamed from: a.lu$a */
    class C2940a implements C6144ahM<Boolean> {
        C2940a() {
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                C2939lu.this.dNE.clear();
                for (ItemBillingOrder next : C2939lu.this.cLq.mo12554cZ()) {
                    if (next.mo22367az() != null) {
                        C2939lu.this.dNE.add(next);
                    }
                }
            }
            C2939lu.this.bmp();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: a.lu$c */
    /* compiled from: a */
    class C2943c implements C6144ahM<Void> {
        C2943c() {
        }

        /* renamed from: a */
        public void mo1931n(Void voidR) {
            SwingUtilities.invokeLater(new C2944a());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: a.lu$c$a */
        class C2944a implements Runnable {
            C2944a() {
            }

            public void run() {
                C2939lu.this.dND.cBn();
                C2939lu.this.dND.mo21267Kl();
            }
        }
    }

    /* renamed from: a.lu$b */
    /* compiled from: a */
    class C2941b extends aEP {
        private final /* synthetic */ Player agw;
        private final /* synthetic */ ItemBillingOrder axP;

        C2941b(Player aku, ItemBillingOrder uNVar) {
            this.agw = aku;
            this.axP = uNVar;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                try {
                    ((ItemBilling) C3582se.m38985a(C2939lu.this.cLq, (C6144ahM<?>) new C2942a())).mo12550b(this.agw, this.axP);
                } catch (C2370eZ e) {
                    e.printStackTrace();
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }

        /* renamed from: a.lu$b$a */
        class C2942a implements C6144ahM<Boolean> {
            C2942a() {
            }

            /* renamed from: a */
            public void mo1931n(Boolean bool) {
                ((HoplonsAddon) C2939lu.this.apa).cUJ();
                C2939lu.this.dND.mo21274kz(C2939lu.this.f8593kj.translate("Item successful transfered."));
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                if (th instanceof C2370eZ) {
                    C2939lu.this.m35343b(((C2370eZ) th).mo18132pa());
                } else {
                    C2939lu.this.dND.mo21271eR(C2939lu.this.f8593kj.translate("Invalid data. Please check the connection and try again."));
                }
            }
        }
    }
}
