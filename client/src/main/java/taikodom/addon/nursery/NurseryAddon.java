package taikodom.addon.nursery;

import com.hoplon.geometry.Color;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.network.message.externalizable.C1475Vi;
import game.network.message.externalizable.C3131oI;
import game.network.message.externalizable.C5543aNj;
import game.network.message.externalizable.C6559apL;
import game.script.ai.npc.AIController;
import game.script.mission.actions.RecurrentMessageRequestMissionAction;
import game.script.nursery.NurseryDefaults;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.IAddonSettings;
import logic.aaa.*;
import logic.render.IEngineGraphics;
import logic.res.KeyCode;
import logic.res.sound.C0907NJ;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import logic.ui.item.InternalFrame;
import logic.ui.item.Progress;
import logic.ui.item.Repeater;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.infra.script.I18NString;
import taikodom.render.SceneView;
import taikodom.render.camera.Camera;
import taikodom.render.gui.GBillboard;
import taikodom.render.loader.RenderAsset;
import taikodom.render.textures.Material;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

@TaikodomAddon("taikodom.addon.nursery")
/* compiled from: a */
public class NurseryAddon implements C2495fo {

    private static final int aWO = 35;
    private static final int iwJ = 4;
    private static final int iwK = 20;
    private static final String iwL = "open market";
    private static final String iwM = "select mission item";
    private static final String iwN = "destiny container";
    private static final String iwO = "buy item";
    /* renamed from: JA */
    private static /* synthetic */ int[] f10180JA = null;
    /* access modifiers changed from: private */
    /* renamed from: P */
    public Player f10181P;
    /* access modifiers changed from: private */

    /* renamed from: Sf */
    public NurseryDefaults f10182Sf;
    /* access modifiers changed from: private */
    public int iwP;
    /* access modifiers changed from: private */
    public int iwQ;
    /* access modifiers changed from: private */
    public float iwR = 0.0f;
    /* access modifiers changed from: private */
    public InternalFrame iwS;
    /* access modifiers changed from: private */
    public JLabel iwV;
    /* access modifiers changed from: private */
    public Progress iwW;
    /* access modifiers changed from: private */
    public Repeater<C4860n> iwX;
    /* access modifiers changed from: private */
    public C3131oI iwZ;
    /* access modifiers changed from: private */
    public List<C4860n> ixa;
    /* access modifiers changed from: private */
    public List<C4860n> ixb;
    /* access modifiers changed from: private */
    public Timer ixd;
    /* access modifiers changed from: private */
    public ActionListener ixe;
    /* access modifiers changed from: private */
    public ActionListener ixg;
    /* renamed from: kj */
    public IAddonProperties f10183kj;
    /* access modifiers changed from: private */
    public SceneView sceneView;
    private float aSB;
    private GBillboard aja;
    private C5905ach giY;
    private Timer ilH;
    private GBillboard ilI;
    private Timer ilJ;
    private JLabel iwT;
    private JLabel iwU;
    private Repeater.C3671a<C4860n> iwY;
    private List<C4860n> ixc;
    private ActionListener ixf;
    private C6124ags<C0822Lr> ixh;
    private C6124ags<C6885avZ> ixi;
    private C6124ags<C3131oI> ixj;
    /* access modifiers changed from: private */
    private C6124ags<C0752Kk> ixk;
    private ActionListener ixl;

    /* renamed from: pf */
    static /* synthetic */ int[] m45028pf() {
        int[] iArr = f10180JA;
        if (iArr == null) {
            iArr = new int[aMU.values().length];
            try {
                iArr[aMU.BOUGHT_MISSION_ITEM.ordinal()] = 8;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[aMU.CAREER_SHIP_CHOOSER.ordinal()] = 11;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[aMU.CREATED_AVATAR.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[aMU.CREATED_PLAYER.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[aMU.FINISHED_NURSERY.ordinal()] = 12;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[aMU.FIRST_LAUNCH.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[aMU.MISSION_1_ACCOMPLISHED.ordinal()] = 6;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[aMU.MISSION_1_TAKEN.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[aMU.MISSION_2_ACCOMPLISHED.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[aMU.MISSION_2_TAKEN.ordinal()] = 7;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[aMU.MISSION_3_TAKEN.ordinal()] = 10;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[aMU.SEEN_ONI_INITIALIZATION.ordinal()] = 2;
            } catch (NoSuchFieldError e12) {
            }
            f10180JA = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: private */
    public void dkm() {
        this.f10183kj.aVU().mo13975h(new C1475Vi(this.f10182Sf.bZK()));
    }

    /* access modifiers changed from: private */
    public void dkn() {
        if (this.f10181P.cXm() == null) {
            this.f10181P.mo14420f(aMU.CREATED_PLAYER);
        }
        switch (m45028pf()[this.f10181P.cXm().ordinal()]) {
            case 1:
                this.giY.mo12679a(this.ixl);
                return;
            case 2:
                this.f10183kj.aVU().mo13975h(new C2733jJ(C5956adg.format(this.f10183kj.translate("Station {0} - {1}"), this.f10181P.bhE().getName(), dku()), false, new Object[0]));
                m44993a(6000, (ActionListener) new C4845c());
                return;
            case 3:
                this.f10183kj.aVU().mo13975h(new C6492anw());
                dkp();
                return;
            case 4:
                this.f10183kj.aVU().mo13975h(new C2395eo());
                return;
            case 7:
                this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.HIGHLIGHT_MARKET));
                m45009c(iwL, this.f10182Sf.bZq());
                return;
            case 11:
                this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.CAREER_CHOOSER));
                return;
            case 12:
                this.f10183kj.aVU().mo13975h(new C2733jJ(dku(), false, new Object[0]));
                this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.FINISHED_NURSERY));
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void dko() {
        m44994a(this.f10183kj.translate("Ship systems check-up"), this.ixc, 500, new C4841b());
    }

    private void dkp() {
        dhJ();
        if (this.sceneView != null) {
            dks();
        }
        dhH();
    }

    private void dkq() {
        dkm();
    }

    private void dhH() {
        this.aSB = 1.0f;
        this.ilJ = new Timer(10, new C4848e());
        this.ilJ.setInitialDelay(0);
        this.ilJ.start();
    }

    /* access modifiers changed from: protected */
    public void dhI() {
        Color primitiveColor = this.ilI.getPrimitiveColor();
        primitiveColor.w = this.aSB;
        this.ilI.setPrimitiveColor(primitiveColor);
        this.aSB -= 0.005f;
        if (this.aSB < 0.0f) {
            this.ilJ.stop();
            this.f10183kj.getEngineGame().getEngineGraphics().aeh().removeChild(this.ilI);
            this.ilI.setRender(false);
            this.ilI.dispose();
            this.ilI = null;
            dkq();
        }
    }

    private void dhJ() {
        IEngineGraphics ale = this.f10183kj.getEngineGame().getEngineGraphics();
        if (ale != null) {
            int aet = ale.aet();
            int aes = ale.aes();
            this.ilI = new GBillboard();
            this.ilI.setPrimitiveColor(new Color(0.0f, 0.0f, 0.0f, 1.0f));
            this.ilI.setSize(new Vector2fWrap((float) (aes * 2), (float) (aet * 2)));
            this.ilI.setPosition(new Vector2fWrap((float) (aes / 2), (float) (aet / 2)));
            this.ilI.setName("fadeInBillBoard");
            this.ilI.setRender(true);
            IEngineGraphics ale2 = this.f10183kj.getEngineGame().getEngineGraphics();
            if (ale2 != null) {
                Material material = new Material();
                material.setShader(ale2.aej().getDefaultVertexColorShader());
                this.ilI.setMaterial(material);
                ale2.aeh().addChild(this.ilI);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44993a(int i, ActionListener actionListener) {
        Timer timer = new Timer(i, actionListener);
        timer.setRepeats(false);
        timer.start();
    }

    /* access modifiers changed from: private */
    public void dkr() {
        this.f10183kj.aVU().mo13975h(new C2327eB(false));
        this.f10183kj.aVU().mo13975h(new C0696Jv(1, false));
        this.f10183kj.aVU().mo13975h(new C0696Jv(2, false));
    }

    private void dks() {
        if (this.sceneView != null) {
            this.sceneView.setEnabled(false);
            this.f10183kj.ale().mo3046b(this.sceneView);
            this.sceneView = null;
        }
        this.f10183kj.ale().aea().setEnabled(true);
    }

    private void dkt() {
        if (this.iwS == null) {
            this.iwS = this.f10183kj.bHv().mo16792bN("checklist.xml");
            this.iwX = this.iwS.mo4919ch("elementRepeater");
            this.iwW = this.iwS.mo4918cg("installationProgress");
            this.iwT = this.iwS.mo4915cd("checklistTitle1");
            this.iwU = this.iwS.mo4915cd("checklistTitle2");
            cqq();
            this.iwX.mo22250a(this.iwY);
        }
        this.iwS.center();
        this.iwS.setAlwaysOnTop(true);
        this.iwS.setVisible(true);
    }

    /* renamed from: iG */
    private void m45016iG() {
        this.ixf = new C4847d();
        this.ixj = new C4850g();
        this.ixh = new C4849f();
        this.ixk = new C4853i();
        this.ixi = new C4852h();
        this.ixg = new C4839a();
        this.ixl = new C4854j();
    }

    private void cqq() {
        this.iwY = new C4857k();
    }

    private String dku() {
        Date date = new Date(this.f10183kj.getEngineGame().getTaikodom().cVr());
        int year = date.getYear() + KeyCode.csX;
        Date date2 = new Date(year, date.getMonth(), date.getDate());
        return C5956adg.format(this.f10183kj.translate("{0,date,medium}  ({1} ER)"), date2, Integer.valueOf((year - 2073) + 1900));
    }

    private void dkv() {
        if (this.ixb == null) {
            this.ixb = new ArrayList();
            this.ixb.add(new C4860n(this.f10183kj.translate("Personal menu"), new C3131oI(C3131oI.C3132a.ONI_CHECKLIST_PERSONAL_MENU)));
            this.ixb.add(new C4860n(this.f10183kj.translate("Comunications Menu"), new C3131oI(C3131oI.C3132a.ONI_CHECKLIST_LEFT_SIDE_MENU)));
            this.ixb.add(new C4860n(this.f10183kj.translate("Station Menu"), new C3131oI(C3131oI.C3132a.ONI_CHECKLIST_RIGHT_SIDE_MENU)));
            this.ixb.add(new C4860n(this.f10183kj.translate("Accessories"), new C3131oI(C3131oI.C3132a.ONI_CHECKLIST_OTHER_ELEMENTS)));
        }
        if (this.ixc == null) {
            this.ixc = new ArrayList();
            this.ixc.add(new C4860n(this.f10183kj.translate("Armor"), (C3131oI) null));
            this.ixc.add(new C4860n(this.f10183kj.translate("Reactor"), (C3131oI) null));
            this.ixc.add(new C4860n(this.f10183kj.translate("Thruster"), (C3131oI) null));
            this.ixc.add(new C4860n(this.f10183kj.translate("Container"), (C3131oI) null));
            this.ixc.add(new C4860n(this.f10183kj.translate("COC"), (C3131oI) null));
            this.ixc.add(new C4860n(this.f10183kj.translate("Weapons"), (C3131oI) null));
            this.ixc.add(new C4860n(this.f10183kj.translate("Systems"), (C3131oI) null));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public void m45019j(RecurrentMessageRequestMissionAction aul) {
        this.f10183kj.aVU().mo13975h(new C6559apL(aul));
    }

    /* access modifiers changed from: private */
    /* renamed from: nh */
    public void m45025nh(String str) {
        this.f10183kj.aVU().mo13975h(new C6559apL(str));
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m45021k(RecurrentMessageRequestMissionAction aul) {
        this.f10183kj.aVU().mo13975h(new C5543aNj(aul));
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m45009c(String str, I18NString i18NString) {
        this.f10183kj.aVU().mo13975h(new C5543aNj(str, i18NString, (Asset) null));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44994a(String str, List<C4860n> list, int i, ActionListener actionListener) {
        this.ixa = list;
        this.iwP = i;
        this.ixe = actionListener;
        dkt();
        StringTokenizer stringTokenizer = new StringTokenizer(str);
        int countTokens = stringTokenizer.countTokens();
        if (countTokens < 4) {
            this.iwT.setText(str);
            this.iwU.setVisible(false);
        } else {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i2 = 0; i2 < countTokens / 2; i2++) {
                if (stringTokenizer.hasMoreTokens()) {
                    if (i2 != 0) {
                        stringBuffer.append(" ");
                    }
                    stringBuffer.append(stringTokenizer.nextToken());
                }
            }
            this.iwT.setText(stringBuffer.toString());
            this.iwU.setVisible(true);
            StringBuffer stringBuffer2 = new StringBuffer();
            while (stringTokenizer.hasMoreTokens()) {
                stringBuffer2.append(" ");
                stringBuffer2.append(stringTokenizer.nextToken());
            }
            this.iwU.setText(stringBuffer2.toString());
            this.iwS.setSize(this.iwS.getWidth(), this.iwS.getHeight() + 20);
            this.iwS.center();
        }
        this.iwW.setValue(0);
        this.iwX.clear();
        this.iwQ = 0;
        this.iwR = 0.0f;
        this.iwX.mo22248G(list.get(0));
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10183kj = addonPropertie;
        if (m45017iL()) {
            this.f10183kj.getPlayer().mo14420f(aMU.FINISHED_NURSERY);
            return;
        }
        this.f10181P = this.f10183kj.getPlayer();
        if (this.f10181P != null) {
            this.f10182Sf = this.f10183kj.ala().aJe().mo19006xF();
            m45016iG();
            if (!m45017iL() && (aMU.CREATED_PLAYER.equals(this.f10183kj.getPlayer().cXm()) || aMU.SEEN_ONI_INITIALIZATION.equals(this.f10183kj.getPlayer().cXm()))) {
                dhQ();
                art();
            }
            dkv();
            this.f10183kj.aVU().mo13965a(C3131oI.class, this.ixj);
            this.f10183kj.aVU().mo13965a(C0822Lr.class, this.ixh);
            this.f10183kj.aVU().mo13965a(C6885avZ.class, this.ixi);
            this.f10183kj.aVU().mo13965a(C0752Kk.class, this.ixk);
            this.giY = C5905ach.m20567b((IAddonSettings) this.f10183kj);
            this.giY.mo12679a(this.ixf);
        }
    }

    private void art() {
        Camera camera = new Camera();
        camera.setPosition(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, 3.5d));
        camera.setAspect((float) (this.f10183kj.bHv().getScreenWidth() / this.f10183kj.bHv().getScreenHeight()));
        camera.setNearPlane(0.01f);
        camera.setFovY(40.0f);
        this.sceneView.setCamera(camera);
    }

    private void dhQ() {
        this.f10183kj.ale().aea().setEnabled(false);
        this.sceneView = new SceneView();
        this.sceneView.setViewport(0, 0, this.f10183kj.bHv().getScreenWidth(), this.f10183kj.bHv().getScreenHeight());
        this.sceneView.setHandle("avatarSceneView");
        this.sceneView.setClearColorBuffer(true);
        this.sceneView.setClearDepthBuffer(true);
        this.sceneView.setClearStencilBuffer(true);
        this.f10183kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/nod_avt_scene.pro", "nod_avt_scene", this.sceneView.getScene(), new C4858l(), getClass().getSimpleName());
    }

    /* renamed from: iL */
    private boolean m45017iL() {
        return this.f10183kj != null && this.f10183kj.ala().aLS().mo22273iL();
    }

    public void stop() {
        this.giY.cCb();
        this.f10183kj.aVU().mo13964a(this.ixj);
        this.f10183kj.aVU().mo13964a(this.ixh);
        this.f10183kj.aVU().mo13964a(this.ixk);
    }

    /* access modifiers changed from: private */
    public void dhK() {
        IEngineGraphics ale = this.f10183kj.getEngineGame().getEngineGraphics();
        if (ale != null) {
            int aet = ale.aet();
            int aes = ale.aes();
            this.aja = new GBillboard();
            this.aja.setPrimitiveColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
            this.aja.setSize(new Vector2fWrap((float) (aes * 2), (float) (aet * 2)));
            this.aja.setPosition(new Vector2fWrap((float) (aes / 2), (float) (aet / 2)));
            this.aja.setName("fadeOutBillBoard");
            this.aja.setRender(true);
            IEngineGraphics ale2 = this.f10183kj.getEngineGame().getEngineGraphics();
            if (ale2 != null) {
                Material material = new Material();
                material.setShader(ale2.aej().getDefaultVertexColorShader());
                this.aja.setMaterial(material);
                ale2.aeh().addChild(this.aja);
                this.aSB = 0.0f;
                this.ilH = new Timer(10, new C4859m());
                this.ilH.setInitialDelay(0);
                this.ilH.start();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void dhL() {
        Color primitiveColor = this.aja.getPrimitiveColor();
        primitiveColor.w = this.aSB;
        this.aja.setPrimitiveColor(primitiveColor);
        this.aSB = (float) (((double) this.aSB) + 0.01d);
        if (this.aSB > 1.0f) {
            this.ilH.stop();
            this.f10183kj.getEngineGame().getEngineGraphics().aeh().removeChild(this.aja);
            this.aja.setRender(false);
            this.aja.dispose();
            this.aja = null;
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$n */
    /* compiled from: a */
    private class C4860n {
        private final C3131oI dbt;
        private final String elementName;

        public C4860n(String str, C3131oI oIVar) {
            this.elementName = str;
            this.dbt = oIVar;
        }

        public String getElementName() {
            return this.elementName;
        }

        public C3131oI aRx() {
            return this.dbt;
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$c */
    /* compiled from: a */
    class C4845c implements ActionListener {
        C4845c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NurseryAddon.this.dkr();
            NurseryAddon.this.m44993a(700, (ActionListener) new C4846a());
        }

        /* renamed from: taikodom.addon.nursery.NurseryAddon$c$a */
        class C4846a implements ActionListener {
            C4846a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                NurseryAddon.this.dkm();
            }
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$b */
    /* compiled from: a */
    class C4841b implements ActionListener {
        C4841b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NurseryAddon.this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.SHIP_INSTALLED));
            NurseryAddon.this.f10183kj.aVU().mo13975h(new C2733jJ(NurseryAddon.this.f10183kj.translate("Ship active and ready to undock."), false, new Object[0]));
            NurseryAddon.this.m44993a((int) C1298TD.hFl, (ActionListener) new C4843b());
            NurseryAddon.this.m44993a(6500, (ActionListener) new C4844c());
            NurseryAddon.this.m44993a(9500, (ActionListener) new C4842a());
        }

        /* renamed from: taikodom.addon.nursery.NurseryAddon$b$b */
        /* compiled from: a */
        class C4843b implements ActionListener {
            C4843b() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                NurseryAddon.this.f10183kj.aVU().mo13975h(new C2327eB(true));
            }
        }

        /* renamed from: taikodom.addon.nursery.NurseryAddon$b$c */
        /* compiled from: a */
        class C4844c implements ActionListener {
            C4844c() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                NurseryAddon.this.f10183kj.aVU().mo13975h(new C6811auD(C3667tV.WELCOME_ABOARD));
                NurseryAddon.this.f10183kj.aVU().mo13975h(new C2733jJ(NurseryAddon.this.f10183kj.translate("Welcome aboard, pilot"), false, new Object[0]));
            }
        }

        /* renamed from: taikodom.addon.nursery.NurseryAddon$b$a */
        class C4842a implements ActionListener {
            C4842a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                NurseryAddon.this.m45021k(NurseryAddon.this.f10182Sf.bZo());
                NurseryAddon.this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.UNLOCK_HANGAR));
            }
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$e */
    /* compiled from: a */
    class C4848e implements ActionListener {
        C4848e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NurseryAddon.this.dhI();
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$d */
    /* compiled from: a */
    class C4847d implements ActionListener {
        C4847d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NurseryAddon.this.dkn();
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$g */
    /* compiled from: a */
    class C4850g extends C6124ags<C3131oI> {

        /* renamed from: JB */
        private static /* synthetic */ int[] f10191JB;

        C4850g() {
        }

        /* renamed from: pg */
        static /* synthetic */ int[] m45044pg() {
            int[] iArr = f10191JB;
            if (iArr == null) {
                iArr = new int[C3131oI.C3132a.values().length];
                try {
                    iArr[C3131oI.C3132a.BOUGHT_MISSION_ITEM.ordinal()] = 22;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C3131oI.C3132a.CAREER_CHOOSER.ordinal()] = 28;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C3131oI.C3132a.CAREER_CHOSEN.ordinal()] = 29;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C3131oI.C3132a.CREATED_AVATAR.ordinal()] = 11;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C3131oI.C3132a.FINISHED_NURSERY.ordinal()] = 30;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[C3131oI.C3132a.FIRST_LAUNCH.ordinal()] = 15;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_DESTINATION_CONTAINER.ordinal()] = 20;
                } catch (NoSuchFieldError e7) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_DOCK_BUTTON.ordinal()] = 24;
                } catch (NoSuchFieldError e8) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_HANGAR.ordinal()] = 14;
                } catch (NoSuchFieldError e9) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_MARKET.ordinal()] = 16;
                } catch (NoSuchFieldError e10) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_MISSION_ITEM.ordinal()] = 18;
                } catch (NoSuchFieldError e11) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_OUTPOST.ordinal()] = 23;
                } catch (NoSuchFieldError e12) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_SHIP_CONTAINER.ordinal()] = 27;
                } catch (NoSuchFieldError e13) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_SHIP_WINDOW.ordinal()] = 25;
                } catch (NoSuchFieldError e14) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_CHAT.ordinal()] = 6;
                } catch (NoSuchFieldError e15) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_EQUIPMENT_SELECT.ordinal()] = 8;
                } catch (NoSuchFieldError e16) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_LEFT_SIDE_MENU.ordinal()] = 2;
                } catch (NoSuchFieldError e17) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_OTHER_ELEMENTS.ordinal()] = 9;
                } catch (NoSuchFieldError e18) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_PDA.ordinal()] = 5;
                } catch (NoSuchFieldError e19) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_PERSONAL_MENU.ordinal()] = 1;
                } catch (NoSuchFieldError e20) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_RIGHT_SIDE_MENU.ordinal()] = 3;
                } catch (NoSuchFieldError e21) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_STATION_OPTIONS.ordinal()] = 4;
                } catch (NoSuchFieldError e22) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_WEAPON_SELECT.ordinal()] = 7;
                } catch (NoSuchFieldError e23) {
                }
                try {
                    iArr[C3131oI.C3132a.OPENED_MARKET.ordinal()] = 17;
                } catch (NoSuchFieldError e24) {
                }
                try {
                    iArr[C3131oI.C3132a.OPENED_SHIP_WINDOW.ordinal()] = 26;
                } catch (NoSuchFieldError e25) {
                }
                try {
                    iArr[C3131oI.C3132a.OPEN_AVATAR_CREATION.ordinal()] = 10;
                } catch (NoSuchFieldError e26) {
                }
                try {
                    iArr[C3131oI.C3132a.SELECTED_DESTINATION_CONTAINER.ordinal()] = 21;
                } catch (NoSuchFieldError e27) {
                }
                try {
                    iArr[C3131oI.C3132a.SELECTED_MISSION_ITEM.ordinal()] = 19;
                } catch (NoSuchFieldError e28) {
                }
                try {
                    iArr[C3131oI.C3132a.SHIP_INSTALLED.ordinal()] = 12;
                } catch (NoSuchFieldError e29) {
                }
                try {
                    iArr[C3131oI.C3132a.UNLOCK_HANGAR.ordinal()] = 13;
                } catch (NoSuchFieldError e30) {
                }
                f10191JB = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public boolean updateInfo(C3131oI oIVar) {
            switch (m45044pg()[oIVar.mo20956Us().ordinal()]) {
                case 10:
                    NurseryAddon.this.dhK();
                    return false;
                case 11:
                    if (!aMU.SEEN_ONI_INITIALIZATION.equals(NurseryAddon.this.f10181P.cXm())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.CREATED_AVATAR);
                    NurseryAddon.this.dkn();
                    return false;
                case 15:
                    if (!aMU.MISSION_1_TAKEN.equals(NurseryAddon.this.f10181P.cXm())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.FIRST_LAUNCH);
                    NurseryAddon.this.dkn();
                    return false;
                case 17:
                    if (!aMU.MISSION_2_TAKEN.equals(NurseryAddon.this.f10181P.cXm())) {
                        return false;
                    }
                    NurseryAddon.this.m45025nh(NurseryAddon.iwL);
                    NurseryAddon.this.m45025nh(NurseryAddon.iwN);
                    NurseryAddon.this.m45025nh(NurseryAddon.iwO);
                    NurseryAddon.this.m45009c(NurseryAddon.iwM, NurseryAddon.this.f10182Sf.bZs());
                    NurseryAddon.this.m44993a(2000, (ActionListener) new C4851a());
                    return false;
                case 19:
                    if (!aMU.MISSION_2_TAKEN.equals(NurseryAddon.this.f10181P.cXm())) {
                        return false;
                    }
                    NurseryAddon.this.m45025nh(NurseryAddon.iwM);
                    NurseryAddon.this.m45025nh(NurseryAddon.iwO);
                    NurseryAddon.this.m45009c(NurseryAddon.iwN, NurseryAddon.this.f10182Sf.bZu());
                    NurseryAddon.this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.HIGHLIGHT_DESTINATION_CONTAINER));
                    return false;
                case 21:
                    if (!aMU.MISSION_2_TAKEN.equals(NurseryAddon.this.f10181P.cXm())) {
                        return false;
                    }
                    NurseryAddon.this.m45025nh(NurseryAddon.iwM);
                    NurseryAddon.this.m45025nh(NurseryAddon.iwN);
                    NurseryAddon.this.m45009c(NurseryAddon.iwO, NurseryAddon.this.f10182Sf.bZw());
                    return false;
                case 22:
                    if (!aMU.MISSION_2_TAKEN.equals(NurseryAddon.this.f10181P.cXm())) {
                        return false;
                    }
                    NurseryAddon.this.m45025nh(NurseryAddon.iwM);
                    NurseryAddon.this.m45025nh(NurseryAddon.iwN);
                    NurseryAddon.this.m45025nh(NurseryAddon.iwO);
                    NurseryAddon.this.f10181P.mo14420f(aMU.BOUGHT_MISSION_ITEM);
                    NurseryAddon.this.dkn();
                    return false;
                case 26:
                    if (!aMU.BOUGHT_MISSION_ITEM.equals(NurseryAddon.this.f10181P.cXm())) {
                        return false;
                    }
                    NurseryAddon.this.m45019j(NurseryAddon.this.f10182Sf.bZy());
                    NurseryAddon.this.m45021k(NurseryAddon.this.f10182Sf.bZA());
                    NurseryAddon.this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.HIGHLIGHT_SHIP_CONTAINER));
                    return false;
                case 29:
                    NurseryAddon.this.f10181P.mo14420f(aMU.FINISHED_NURSERY);
                    NurseryAddon.this.dkn();
                    return false;
                default:
                    return false;
            }
        }

        /* renamed from: taikodom.addon.nursery.NurseryAddon$g$a */
        class C4851a implements ActionListener {
            C4851a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                NurseryAddon.this.f10183kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.HIGHLIGHT_MISSION_ITEM));
            }
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$f */
    /* compiled from: a */
    class C4849f extends C6124ags<C0822Lr> {

        /* renamed from: JA */
        private static /* synthetic */ int[] f10189JA;

        C4849f() {
        }

        /* renamed from: pf */
        static /* synthetic */ int[] m45040pf() {
            int[] iArr = f10189JA;
            if (iArr == null) {
                iArr = new int[aMU.values().length];
                try {
                    iArr[aMU.BOUGHT_MISSION_ITEM.ordinal()] = 8;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[aMU.CAREER_SHIP_CHOOSER.ordinal()] = 11;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[aMU.CREATED_AVATAR.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[aMU.CREATED_PLAYER.ordinal()] = 1;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[aMU.FINISHED_NURSERY.ordinal()] = 12;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[aMU.FIRST_LAUNCH.ordinal()] = 5;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[aMU.MISSION_1_ACCOMPLISHED.ordinal()] = 6;
                } catch (NoSuchFieldError e7) {
                }
                try {
                    iArr[aMU.MISSION_1_TAKEN.ordinal()] = 4;
                } catch (NoSuchFieldError e8) {
                }
                try {
                    iArr[aMU.MISSION_2_ACCOMPLISHED.ordinal()] = 9;
                } catch (NoSuchFieldError e9) {
                }
                try {
                    iArr[aMU.MISSION_2_TAKEN.ordinal()] = 7;
                } catch (NoSuchFieldError e10) {
                }
                try {
                    iArr[aMU.MISSION_3_TAKEN.ordinal()] = 10;
                } catch (NoSuchFieldError e11) {
                }
                try {
                    iArr[aMU.SEEN_ONI_INITIALIZATION.ordinal()] = 2;
                } catch (NoSuchFieldError e12) {
                }
                f10189JA = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public boolean updateInfo(C0822Lr lr) {
            switch (m45040pf()[NurseryAddon.this.f10181P.cXm().ordinal()]) {
                case 3:
                    if (!NurseryAddon.this.f10182Sf.bZe().equals(lr.bfA())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.MISSION_1_TAKEN);
                    NurseryAddon.this.dkn();
                    return false;
                case 6:
                    if (!NurseryAddon.this.f10182Sf.bZg().equals(lr.bfA())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.MISSION_2_TAKEN);
                    NurseryAddon.this.dkn();
                    return false;
                case 9:
                    if (!NurseryAddon.this.f10182Sf.bZk().equals(lr.bfA()) && !NurseryAddon.this.f10182Sf.bZm().equals(lr.bfA())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.MISSION_3_TAKEN);
                    return false;
                default:
                    return false;
            }
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$i */
    /* compiled from: a */
    class C4853i extends C6124ags<C0752Kk> {
        C4853i() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0752Kk kk) {
            NurseryAddon.this.dko();
            return false;
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$h */
    /* compiled from: a */
    class C4852h extends C6124ags<C6885avZ> {

        /* renamed from: JA */
        private static /* synthetic */ int[] f10193JA;

        C4852h() {
        }

        /* renamed from: pf */
        static /* synthetic */ int[] m45047pf() {
            int[] iArr = f10193JA;
            if (iArr == null) {
                iArr = new int[aMU.values().length];
                try {
                    iArr[aMU.BOUGHT_MISSION_ITEM.ordinal()] = 8;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[aMU.CAREER_SHIP_CHOOSER.ordinal()] = 11;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[aMU.CREATED_AVATAR.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[aMU.CREATED_PLAYER.ordinal()] = 1;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[aMU.FINISHED_NURSERY.ordinal()] = 12;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[aMU.FIRST_LAUNCH.ordinal()] = 5;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[aMU.MISSION_1_ACCOMPLISHED.ordinal()] = 6;
                } catch (NoSuchFieldError e7) {
                }
                try {
                    iArr[aMU.MISSION_1_TAKEN.ordinal()] = 4;
                } catch (NoSuchFieldError e8) {
                }
                try {
                    iArr[aMU.MISSION_2_ACCOMPLISHED.ordinal()] = 9;
                } catch (NoSuchFieldError e9) {
                }
                try {
                    iArr[aMU.MISSION_2_TAKEN.ordinal()] = 7;
                } catch (NoSuchFieldError e10) {
                }
                try {
                    iArr[aMU.MISSION_3_TAKEN.ordinal()] = 10;
                } catch (NoSuchFieldError e11) {
                }
                try {
                    iArr[aMU.SEEN_ONI_INITIALIZATION.ordinal()] = 2;
                } catch (NoSuchFieldError e12) {
                }
                f10193JA = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public boolean updateInfo(C6885avZ avz) {
            switch (m45047pf()[NurseryAddon.this.f10181P.cXm().ordinal()]) {
                case 5:
                    if (!NurseryAddon.this.f10182Sf.bZe().equals(avz.bfA())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.MISSION_1_ACCOMPLISHED);
                    return false;
                case 8:
                    if (!NurseryAddon.this.f10182Sf.bZg().equals(avz.bfA())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.MISSION_2_ACCOMPLISHED);
                    NurseryAddon.this.dkn();
                    return false;
                case 9:
                    NurseryAddon.this.f10181P.mo14420f(aMU.MISSION_3_TAKEN);
                    NurseryAddon.this.dkn();
                    return false;
                case 10:
                    if (!NurseryAddon.this.f10182Sf.bZk().equals(avz.bfA()) && !NurseryAddon.this.f10182Sf.bZm().equals(avz.bfA())) {
                        return false;
                    }
                    NurseryAddon.this.f10181P.mo14420f(aMU.CAREER_SHIP_CHOOSER);
                    NurseryAddon.this.f10181P.dyy();
                    NurseryAddon.this.dkn();
                    return false;
                default:
                    return false;
            }
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$a */
    class C4839a implements ActionListener {
        C4839a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (((float) NurseryAddon.this.iwW.getValue()) >= NurseryAddon.this.iwR) {
                NurseryAddon.this.ixd.stop();
                NurseryAddon.this.iwV.setText(NurseryAddon.this.f10183kj.translate("ONLINE"));
                ComponentManager.getCssHolder(NurseryAddon.this.iwV).setAttribute("class", "title online");
                NurseryAddon.this.f10183kj.aVU().mo13975h(new C6811auD(C3667tV.OPEN_WINDOW_SFX));
                if (NurseryAddon.this.iwZ != null) {
                    NurseryAddon.this.f10183kj.aVU().mo13975h(NurseryAddon.this.iwZ);
                }
                if (NurseryAddon.this.iwQ < NurseryAddon.this.ixa.size() - 1) {
                    NurseryAddon nurseryAddon = NurseryAddon.this;
                    nurseryAddon.iwQ = nurseryAddon.iwQ + 1;
                    NurseryAddon.this.iwW.setValue(NurseryAddon.this.iwR);
                    NurseryAddon.this.iwX.mo22248G((C4860n) NurseryAddon.this.ixa.get(NurseryAddon.this.iwQ));
                    return;
                }
                NurseryAddon.this.iwW.setValue(100);
                NurseryAddon.this.m44993a(1500, (ActionListener) new C4840a());
                return;
            }
            NurseryAddon.this.iwW.setValue(((float) NurseryAddon.this.iwW.getValue()) + ((100.0f / ((float) NurseryAddon.this.ixa.size())) / 20.0f));
        }

        /* renamed from: taikodom.addon.nursery.NurseryAddon$a$a */
        class C4840a implements ActionListener {
            C4840a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                NurseryAddon.this.iwS.setVisible(false);
                if (NurseryAddon.this.ixe != null) {
                    NurseryAddon.this.ixe.actionPerformed((ActionEvent) null);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$j */
    /* compiled from: a */
    class C4854j implements ActionListener {
        C4854j() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NurseryAddon.this.m44994a(NurseryAddon.this.f10183kj.translate("Installing Intracranian Neural Organizer (ONI)"), NurseryAddon.this.ixb, 3000, new C4855a());
        }

        /* renamed from: taikodom.addon.nursery.NurseryAddon$j$a */
        class C4855a implements ActionListener {
            C4855a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                NurseryAddon.this.f10183kj.aVU().mo13975h(new C2733jJ(NurseryAddon.this.f10183kj.translate("ONI successfully activated!"), false, new Object[0]));
                NurseryAddon.this.m44993a((int) AIController.AI_RADAR_RADIUS, (ActionListener) new C4856a());
            }

            /* renamed from: taikodom.addon.nursery.NurseryAddon$j$a$a */
            class C4856a implements ActionListener {
                C4856a() {
                }

                public void actionPerformed(ActionEvent actionEvent) {
                    NurseryAddon.this.f10181P.mo14420f(aMU.SEEN_ONI_INITIALIZATION);
                    NurseryAddon.this.dkn();
                }
            }
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$k */
    /* compiled from: a */
    class C4857k implements Repeater.C3671a<C4860n> {
        C4857k() {
        }

        /* renamed from: a */
        public void mo843a(C4860n nVar, Component component) {
            Panel aco = (Panel) component;
            JLabel cf = aco.mo4917cf("elementName");
            cf.setText(nVar.getElementName());
            NurseryAddon.this.iwV = aco.mo4917cf("elementProgress");
            NurseryAddon.this.iwZ = nVar.aRx();
            int length = cf.getText().length();
            new aDX(cf, "[0.." + length + "] dur " + (length * 35), C2830kk.asT, (C0454GJ) null);
            cf.setVisible(true);
            if (NurseryAddon.this.ixd != null) {
                NurseryAddon.this.ixd.stop();
            }
            NurseryAddon.this.ixd = new Timer(((length * 35) + NurseryAddon.this.iwP) / 20, NurseryAddon.this.ixg);
            NurseryAddon nurseryAddon = NurseryAddon.this;
            nurseryAddon.iwR = nurseryAddon.iwR + (100.0f / ((float) NurseryAddon.this.ixa.size()));
            NurseryAddon.this.ixd.start();
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$l */
    /* compiled from: a */
    class C4858l implements C0907NJ {
        C4858l() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            NurseryAddon.this.f10183kj.ale().mo3005a(NurseryAddon.this.sceneView);
            NurseryAddon.this.sceneView.setEnabled(true);
        }
    }

    /* renamed from: taikodom.addon.nursery.NurseryAddon$m */
    /* compiled from: a */
    class C4859m implements ActionListener {
        C4859m() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            NurseryAddon.this.dhL();
        }
    }
}
