package taikodom.addon;

import com.steadystate.css.parser.CSSOMParser;
import logic.C6961axa;
import logic.IAddonManager;
import logic.IAddonSettings;
import logic.aWa;
import logic.res.ILoaderImageInterface;
import logic.res.css.C0325EQ;
import logic.res.css.C6868avI;
import logic.res.css.CssNode;
import logic.ui.*;
import logic.ui.item.InternalFrame;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Загрузчик ресурсов аддона
 */
/* renamed from: a.tj */
/* compiled from: a */
public class C3684tj implements C6961axa {

    private static Map<String, Class<? extends C1276Sr>> bkU = new ConcurrentHashMap();
    /**
     * ссылка на Addon manager
     */
    public final IAddonManager addonManager;
    /**
     * ссылка на Обёртка класса аддона логирование и локализация
     */
    private final IAddonSettings addonSettings;
    /**
     * Список добавленых панелий в рут панель
     * пенели аддонов
     */
    private List<JComponent> bkS = new ArrayList();

    /**
     * Конструктор класса
     *
     * @param axz ссылка на Addon manager
     * @param avf ссылка на Обёртка класса аддона логирование и локализация
     */
    public C3684tj(IAddonManager axz, IAddonSettings avf) {
        this.addonManager = axz;
        this.addonSettings = avf;
    }

    /**
     * Формирование адреса для загрузки  .xml ресурса аддона
     *
     * @param obj ссылка на Загруженный класс аддона Пример taikodom.addon.login.LoginAddon
     * @param str Пример login.xml
     * @param ilVar
     * @return
     */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends C1276Sr> T mo17857a(Object obj, String str, C2698il ilVar) {
        URL url;
        if (obj instanceof Class) {
            url = ((Class) obj).getResource(String.valueOf(((Class) obj).getSimpleName()) + ".class");
        } else if (obj instanceof URL) {
            url = (URL) obj;
        } else if (obj != null) {
            Class<?> cls = obj.getClass();
            while (cls.getEnclosingClass() != null) {
                cls = cls.getEnclosingClass();
            }
            url = obj.getClass().getResource(String.valueOf(cls.getSimpleName()) + ".class");
        } else {
            url = null;
        }
        try {
            return mo17858a(new URL(url, str), ilVar);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param alc Пример TranslationParseContext: class taikodom.addon.debugtools.taikodomversion.TaikodomVersionAddon
     * @param url Пример file:/C:/.../taikodom/addon/debugtools/taikodomversion/taikodomversion.xml
     * @param ilVar
     * @return Возвращает Базовые элементы интерфейса Пример, для class LoginAddon это class aCo extends JPanel
     */
    /* renamed from: a */
    public <T extends C1276Sr> T mo22275a(C6342alC alc, URL url, C2698il ilVar) {
        T t = (T) this.addonManager.getBaseUItagXML().createJComponent(alc, url);//получаем графический компонент на котором будет рисаваться аддон
        if (ilVar != null) {
            ((Container) ilVar).add((JComponent) t);
        } else {
            this.bkS.add((JComponent) t);
        }
        return t;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public <T extends C1276Sr> T mo17858a(URL url, C2698il ilVar) {
        return mo22275a((C6342alC) new MapKeyValue((C6342alC) ((BaseUiTegXml) this.addonManager.getBaseUItagXML()).cnl(), url), url, ilVar);
    }

    /* renamed from: bN */
    public InternalFrame mo16792bN(String str) {
        return (InternalFrame) m39727c((Object) this.addonSettings.bHy(), str);
    }

    public void dispose() {
        for (JComponent next : this.bkS) {
            try {
                Container parent = next.getParent();
                if (parent != null) {
                    parent.remove(next);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.bkS.clear();
    }
    /**
     * Размер окна java.awt.Dimension[width=1024,height=780]
     *
     * @return
     */
    public Dimension getScreenSize() {
        return new Dimension(getScreenWidth(), getScreenHeight());
    }

    public int getScreenHeight() {
        return this.addonManager.getWrapEngGameAddonManager().getScreenHeight();
    }

    public int getScreenWidth() {
        return this.addonManager.getWrapEngGameAddonManager().getScreenWidth();
    }

    public Point getMousePosition() {
        return new Point(0, 0);
    }

    /* renamed from: aY */
    public CSSStyleDeclaration mo16785aY(String str) {
        CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader("{" + str + "}")));
        if (d != null) {
            C0325EQ.m2829a(d);
        }
        return d;
    }

    /* renamed from: c */
    public <T extends C1276Sr> void mo16797c(String str, Class<T> cls) {
        if (bkU.containsKey(str)) {//Уже существует компонентный регистр для тега
            throw new IllegalArgumentException("There already is a component register for tag: " + str);
        }
        bkU.put(str, cls);
    }

    public ILoaderImageInterface adz() {
        return this.addonSettings.bHu().getBaseUItagXML().adz();
    }

    /* renamed from: bO */
    public <T extends C1276Sr> void mo16793bO(String str) {
        bkU.remove(str);
    }

    /* renamed from: a */
    public C6868avI mo16782a(Class<? extends Object> cls, String str) {
        if (str.startsWith("res://")) {
            C6868avI avi = (C6868avI) this.addonManager.getRes(str);
            if (avi != null) {
                return avi;
            }
            C6868avI bP = m39726bP(str);
            this.addonManager.putRes(str, bP);
            return bP;
        }
        C6868avI avi2 = (C6868avI) this.addonManager.getRes(cls + str);
        if (avi2 != null) {
            return avi2;
        }
        C6868avI a = this.addonSettings.bHu().getBaseUItagXML().loadCss((Object) cls, str);
        this.addonManager.putRes(cls + str, a);
        return a;
    }

    /* renamed from: a */
    public C6868avI mo16781a(InputStream inputStream) throws UnsupportedEncodingException {
        CssNode akf = new CssNode();
        new C0325EQ(akf);
        akf.loadFileCSS(inputStream);
        return akf;
    }

    /* renamed from: a */
    public C6868avI mo16783a(Object obj, String str) {
        if (str.startsWith("res://")) {
            return mo16782a((Class<? extends Object>) null, str);
        }
        if (obj instanceof Class) {
            return this.addonSettings.bHu().getBaseUItagXML().loadCss(obj, str);
        }
        return mo16782a((Class<? extends Object>) obj.getClass(), str);
    }

    /* renamed from: bP */
    private C6868avI m39726bP(String str) {
        if (!str.startsWith("res://")) {
            return null;
        }
        try {
            return mo16781a(this.addonSettings.mo11847hE(str));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    public void mo16790b(Object obj, String str) {
    }

    /* renamed from: bQ */
    public <T extends C1276Sr> T mo16794bQ(String str) {
        return mo17857a((Object) this.addonSettings.bHy(), str, (C2698il) null);
    }

    /* renamed from: a */
    public <T extends C1276Sr> T mo16780a(String str, C2698il ilVar) {
        return mo17857a((Object) this.addonSettings.bHy(), str, ilVar);
    }

    /* renamed from: c */
    private <T extends C1276Sr> T m39727c(Object obj, String str) {
        return mo17857a(obj, str, (C2698il) null);
    }

    public IAddonSettings adA() {
        return this.addonSettings;
    }

    /**
     * Добавить в рут панель содержимого новый компонент
     *
     * @param jComponent
     */
    /* renamed from: b */
    public void addInRootPane(JComponent jComponent) {
        this.bkS.add(jComponent);
        JRootPane rootPane = this.addonManager.getBaseUItagXML().getRootPane();
        if (jComponent instanceof JInternalFrame) {
            rootPane.getLayeredPane().add(jComponent);
        } else {
            rootPane.getContentPane().add(jComponent); //Добавить в панель содержимого
        }
    }

    /* renamed from: s */
    public void mo16803s(int i, int i2) {
    }

    public boolean adB() {
        return false;
    }

    /* renamed from: a */
    public void mo16784a(JComponent jComponent, C6868avI avi, String str) {
        IComponentManager e = ComponentManager.getCssHolder(jComponent);
        e.mo13054a(avi);
        e.setAttribute("class", str);
        e.mo13045Vk();
    }

    /* renamed from: c */
    public Rectangle mo16796c(JComponent jComponent) {
        return SwingUtilities.convertRectangle(jComponent.getParent(), jComponent.getBounds(), this.addonManager.getBaseUItagXML().getRootPane());
    }

    /* JADX WARNING: type inference failed for: r2v0, types: [java.lang.Class, java.lang.Class<C>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T extends C1276Sr, C extends aWa> T mo16789b(Class<C> paramClass, String paramString) {
        return mo16779a(paramClass, paramString, null);
    }

    /* renamed from: a */
    public <T extends C1276Sr, C extends aWa> T mo16779a(Class<C> cls, String str, C2698il ilVar) {
        try {
            return mo17858a(new URL(cls.getResource(String.valueOf(cls.getSimpleName()) + ".class"), str), ilVar);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: c */
    public <C extends aWa> InternalFrame mo16795c(Class<C> cls, String str) {
        return (InternalFrame) mo16789b(cls, str);
    }
}
