package taikodom.addon.conversioncampaign;

import logic.IAddonSettings;
import logic.metric.GoogleAdservicesNetWeb;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.C0592IN;
import p001a.C6124ags;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.io.IOException;

@TaikodomAddon("taikodom.addon.conversioncampaign")
/* compiled from: a */
public class ConversionCampaignAddon implements C2495fo {

    /* renamed from: yo */
    public static final String f9840yo = "V2OoCM-iYRDnmYH2Aw";

    /* renamed from: yp */
    public static final String f9841yp = "0TBrCIOkYRDnmYH2Aw";

    /* renamed from: yq */
    public static final String f9842yq = "7mGNCLelYRDnmYH2Aw";

    /* renamed from: yr */
    public static final String f9843yr = "8W0fCJ_7YBDnmYH2Aw";

    /* renamed from: ys */
    private static final String f9844ys = "1052789991";
    /* access modifiers changed from: private */
    public final Log log = LogPrinter.setClass(ConversionCampaignAddon.class);
    /* renamed from: yt */
    public GoogleAdservicesNetWeb f9846yt;
    /* access modifiers changed from: private */
    /* renamed from: kj */
    private IAddonProperties f9845kj;
    /* renamed from: yu */
    private C6124ags<C0592IN> f9847yu;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9845kj = addonPropertie;
        this.f9846yt = new GoogleAdservicesNetWeb(f9844ys);
        this.f9846yt.setAction(f9840yo);
        this.f9847yu = new C4315a();
        this.f9845kj.aVU().mo13965a(C0592IN.class, this.f9847yu);
    }

    public void stop() {
        this.f9845kj.aVU().mo13968b(C0592IN.class, this.f9847yu);
    }

    /* access modifiers changed from: private */
    /* renamed from: P */
    public void m43237P(String str) {
        new Thread(new C4316b(str)).start();
    }

    /* renamed from: taikodom.addon.conversioncampaign.ConversionCampaignAddon$a */
    class C4315a extends C6124ags<C0592IN> {
        C4315a() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0592IN in) {
            ConversionCampaignAddon.this.m43237P(in.getAction());
            return false;
        }
    }

    /* renamed from: taikodom.addon.conversioncampaign.ConversionCampaignAddon$b */
    /* compiled from: a */
    class C4316b implements Runnable {
        private final /* synthetic */ String cRo;

        C4316b(String str) {
            this.cRo = str;
        }

        public void run() {
            try {
                ConversionCampaignAddon.this.f9846yt.stertRequest(this.cRo);
            } catch (IOException e) {
                ConversionCampaignAddon.this.log.warn("Error sending conversion campaign request (HTTP): " + e.getMessage());
            }
        }
    }
}
