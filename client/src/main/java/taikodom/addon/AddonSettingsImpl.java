package taikodom.addon;

import game.engine.IEngineGame;
import game.script.PlayerController;
import game.script.Taikodom;
import game.script.nls.NLSManager;
import game.script.player.Player;
import logic.*;
import logic.baa.C5473aKr;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.render.IEngineGraphics;
import logic.res.ILoaderTrail;
import logic.res.code.C5663aRz;
import logic.res.code.SoftTimer;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import p001a.C2918lo;
import p001a.C3582se;
import p001a.C5388aHk;
import p001a.C6124ags;
import taikodom.infra.script.I18NString;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Properties;

/**
 * Обёртка класса аддона логирование и локализация
 */
/* renamed from: a.GK */
/* compiled from: a */
@Slf4j
public class AddonSettingsImpl extends C3268pp implements IAddonProperties {
    /* renamed from: vS */
    private final EngineGame engineGame;

    /**
     * Таблица локализации ключ - значение
     */
    private Properties cZz;

    public AddonSettingsImpl(EngineGame engineGame, IAddonManager axz, IWrapFileXmlOrJar avw, C2495fo foVar) {
        this.addonManager = axz;
        this.bCI = foVar;
        this.eMb = avw;
        this.eLY = new TranslationParseContext(axz, this);
        this.dej = this.addonManager.getSoftTimer();
        this.eMa = new C0613Ih(axz.getEventManager());
        this.engineGame = engineGame;
        this.cZz = m3387g(foVar.getClass(), I18NString.getCurrentLocation());
    }

    /* renamed from: a */
    public <T extends aDJ> T mo2319a(T t, C6144ahM<?> ahm) {
        return C3582se.m38985a(t, (C6144ahM<?>) mo21221b(ahm));
    }

    public Taikodom aRt() {
        return (Taikodom) this.engineGame.getRoot();
    }

    public Taikodom ala() {
        return (Taikodom) this.engineGame.getRoot();
    }

    public void dispose() {
        try {
            bHy().stop();
        } finally {
            super.dispose();
        }
    }

    /* renamed from: a */
    public void mo2321a(aDJ adj, C5663aRz arz, Action action) {
        if (arz.isCollection()) {
            adj.mo8319a(arz, (C5473aKr<?, ?>) new C0458c(action));
        } else {
            adj.mo8320a(arz, (C6200aiQ<?>) new C0457b(action));
        }
    }

    /* renamed from: a */
    public String mo2320a(Class<?> cls, String str, String str2, Object... objArr) {
        for (I18NString i18NString : NLSManager.gNs) {
            if (i18NString.get(I18NString.DEFAULT_LOCATION).equalsIgnoreCase(str2)) {
                return i18NString.get();
            }
        }
        if (cls == null) {
            cls = bHy().getClass();
        } else if (cls.getEnclosingClass() != null) {
            cls = bHy().getClass();
        }
        if (ala() != null) {
            if (ala().aIY() != null) {
                return ala().aIY().mo6307a(cls, str, str2, objArr);
            }
            if (objArr == null || objArr.length <= 0) {
                return str2;
            }
            return String.format(str2, objArr);
        } else if (this.cZz != null && this.cZz.containsKey(str2)) {
            return this.cZz.getProperty(str2);
        } else {
            Properties g = m3387g(cls, I18NString.getCurrentLocation());
            if (g == null || !g.containsKey(str2)) {
                return "$" + str2;
            }
            return g.getProperty(str2);
        }
    }

    /* renamed from: dL */
    public Player getPlayer() {
        if (ala() == null) {
            return null;
        }
        return ala().getPlayer();
    }

    public PlayerController alb() {
        return this.engineGame.alb();
    }

    public Logger getLog() {
        return this.log;
    }

    public IEngineGraphics ale() {
        return this.engineGame.getEngineGraphics();
    }

    public SoftTimer alf() {
        return this.engineGame.alf();
    }

    public IEngineGame getEngineGame() {
        return this.engineGame;
    }

    public ILoaderTrail alg() {
        return this.engineGame.getLoaderTrail();
    }

    /* renamed from: a */
    public void mo2322a(String str, Action action) {
        this.eMa.mo7499a(str, C5388aHk.class, new C0456a(action));
    }

    /* renamed from: af */
    public String mo2324af(String str) {
        Collection<String> a = aVU().mo13963a(String.valueOf(str) + "_KEY", String.class);
        if (a == null || a.size() <= 0) {
            return null;
        }
        return a.iterator().next();
    }

    /* renamed from: P */
    public void mo2317P(Object obj) {
        C2918lo.m35303a((C6245ajJ) this.eMa, obj);
    }

    /* renamed from: Q */
    public void mo2318Q(Object obj) {
        C2918lo.m35302a(this.eMa, obj);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x008b A[SYNTHETIC, Splitter:B:23:0x008b] */
    /* renamed from: g */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Properties m3387g(java.lang.Class<?> var1, java.lang.String var2) {
        TaikodomAddon var3 = (TaikodomAddon) var1.getAnnotation(TaikodomAddon.class);
        if (var3 == null) {//Класс не имеет перевода
            log.error("Calling class " + var1.getName() + " has test_GLCanvas @TranslationScope");
            return null;
        } else {
            InputStream var4 = null;
            try {
                var4 = this.mo11846f(var1, "nls." + var3.value() + "-" + var2 + ".properties"); //nls.taikodom.addon.debugtools.taikodomversion-en.properties
                if (var4 != null)//Файл открыт на чтение
                {
                    Properties var5 = new Properties();
                    var5.load(var4);
                    Properties var7 = var5;
                    return var7;
                }
            } catch (IOException var15) {
                log.error("Could not load local NLS properties file for language '" + var2 + "'", var15);
            } finally {
                if (var4 != null) {
                    try {
                        var4.close();
                    } catch (IOException var14) {
                        var14.printStackTrace();
                    }
                }
            }
            return null;
        }
    }

    /* renamed from: a.GK$c */
    /* compiled from: a */
    class C0458c implements C5473aKr<aDJ, Object> {
        private final /* synthetic */ Action dCF;

        C0458c(Action action) {
            this.dCF = action;
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            this.dCF.actionPerformed((ActionEvent) null);
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            this.dCF.actionPerformed((ActionEvent) null);
        }
    }

    /* renamed from: a.GK$b */
    /* compiled from: a */
    class C0457b implements C6200aiQ<aDJ> {
        private final /* synthetic */ Action dCF;

        C0457b(Action action) {
            this.dCF = action;
        }

        /* renamed from: a */
        public void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
            this.dCF.actionPerformed((ActionEvent) null);
        }
    }

    /* renamed from: a.GK$a */
    class C0456a extends C6124ags<C5388aHk> {
        private final /* synthetic */ Action gcT;

        C0456a(Action action) {
            this.gcT = action;
        }

        /* renamed from: a */
        public boolean updateInfo(C5388aHk ahk) {
            this.gcT.actionPerformed(ahk);
            return true;
        }
    }
}
