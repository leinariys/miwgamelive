package taikodom.addon.progression;

import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.network.message.externalizable.aTX;
import game.script.avatar.Avatar;
import game.script.citizenship.Citizenship;
import game.script.citizenship.CitizenshipType;
import game.script.player.Player;
import game.script.progression.CharacterEvolution;
import game.script.progression.CharacterProgression;
import game.script.progression.ProgressionCell;
import game.script.progression.ProgressionLine;
import logic.IAddonSettings;
import logic.aaa.C3949xF;
import logic.aaa.C5783aaP;
import logic.res.sound.C0907NJ;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.*;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.tooltip.C2353eO;
import taikodom.render.camera.OrbitalCamera;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RGroup;
import taikodom.render.scene.RModel;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.progression")
/* compiled from: a */
public class ProgressionAddon implements C2495fo {
    private static final String aDD = "progression.xml";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10224P;
    /* access modifiers changed from: private */
    public Component3d aDL;
    /* renamed from: kj */
    public IAddonProperties f10226kj;
    /* renamed from: nM */
    public InternalFrame f10227nM;
    /* renamed from: wz */
    public C3428rT<C3689to> f10229wz;
    /* renamed from: Vd */
    private Progress f10225Vd;
    private CharacterEvolution aDE;
    private CharacterProgression aDF;
    private JLabel aDG;
    private JLabel aDH;
    private JLabel aDI;
    private JLabel aDJ;
    private JLabel aDK;
    private Repeater<ProgressionLine> aDM;
    private int aDN;
    private int aDO;
    private int aDP;
    private Picture amN;
    /* access modifiers changed from: private */
    private Repeater<CitizenshipType> amP;
    /* access modifiers changed from: private */
    private C3428rT<C5783aaP> aoN;
    private C6622aqW atn;
    /* access modifiers changed from: private */
    /* renamed from: wA */
    private C3428rT<C3949xF> f10228wA;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10226kj = addonPropertie;
        this.atn = new C4931f("progression", this.f10226kj.translate("Career"), "progression");
        this.atn.mo15602jH(this.f10226kj.translate("ProgressionExtTooltip"));
        this.atn.mo15603jI("PROGRESSION_WINDOW");
        this.aoN = new C4930e();
        this.f10229wz = new C4929d();
        this.f10228wA = new C4928c();
        this.f10226kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f10226kj.aVU().mo13965a(C3949xF.class, this.f10228wA);
        this.f10226kj.mo2317P(this);
        this.f10226kj.aVU().publish(new C5344aFs(this.atn, 200, true));
        this.f10226kj.aVU().publish(new aTX(this.atn, 400, "progression-commands.css", this));
        this.f10226kj.mo2317P(this);
    }

    /* renamed from: Im */
    private void m45258Im() {
        this.f10227nM = this.f10226kj.bHv().mo16792bN(aDD);
        this.f10227nM.mo4915cd("scroll").getVerticalScrollBar().setUnitIncrement(16);
        this.f10227nM.addComponentListener(new C4927b());
    }

    private void destroy() {
        if (this.f10227nM != null) {
            this.f10227nM.destroy();
            this.f10227nM = null;
        }
    }

    public void stop() {
        destroy();
        if (this.aDL != null) {
            this.aDL.destroy();
        }
    }

    /* renamed from: ND */
    private void m45259ND() {
        this.f10226kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/nod_prg_scene.pro", "nod_prg_scene", (Scene) null, new C4924a(), getClass().getSimpleName());
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f10226kj.getPlayer().mo12659ly() != null && this.f10226kj.getPlayer().mo12637Rn() != null) {
            if (this.f10227nM == null) {
                m45258Im();
                this.f10227nM.setSize(this.f10226kj.bHv().getScreenSize());
                m45278iM();
            }
            m45260NE();
            this.f10227nM.center();
            setVisible(true);
            m45259ND();
            this.f10226kj.aVU().mo13965a(C3689to.class, this.f10229wz);
        }
    }

    /* renamed from: iM */
    private void m45278iM() {
        this.f10224P = this.f10226kj.getPlayer();
        this.aDF = this.f10224P.mo12659ly();
        this.aDE = this.aDF.mo20722Rp();
        this.aDM = this.f10227nM.mo4915cd("typesTable");
        this.aDK = this.f10227nM.mo4917cf("player");
        this.aDG = this.f10227nM.mo4917cf("careerCR");
        this.aDJ = this.f10227nM.mo4917cf("freePoints");
        this.aDH = this.f10227nM.mo4917cf("meritPoints");
        this.aDI = this.f10227nM.mo4917cf("nextMeritPoints");
        this.f10225Vd = this.f10227nM.mo4915cd("mpProgress");
        this.amP = this.f10227nM.mo4915cd("affiliationRepeater");
        this.amN = this.f10227nM.mo4915cd("corporationIcon");
        this.f10227nM.mo4913cb("undo").addActionListener(new C4934i());
        this.f10227nM.mo4913cb("cancel").addActionListener(new C4933h());
        this.f10227nM.mo4913cb("confirm").addActionListener(new C4932g());
        this.aDL = this.f10227nM.mo4915cd("avatarViewer");
    }

    /* renamed from: NE */
    private void m45260NE() {
        this.aDK.setText(this.f10224P.getName());
        int lt = this.aDF.mo20740lt();
        this.aDG.setText("[" + this.aDF.mo20721Rn().mo21959rP().get() + " - " + this.f10226kj.translate("Rank") + " " + lt + "]");
        m45262NG();
        m45261NF();
        long rS = this.f10226kj.ala().aJk().cqL().mo14477rS(lt - 1);
        long rS2 = this.f10226kj.ala().aJk().cqL().mo14477rS(lt) - rS;
        long en = this.aDE.mo18219en() - rS;
        this.aDH.setText(String.valueOf(en));
        this.aDI.setText(String.valueOf(this.f10226kj.translate("Next Rank:")) + rS2);
        this.f10225Vd.setMinimum(0);
        this.f10225Vd.mo17398E((float) rS2);
        this.f10225Vd.setValue((float) en);
        this.f10225Vd.setToolTipText(String.valueOf(en) + " / " + rS2);
        IComponentManager e = ComponentManager.getCssHolder(this.f10227nM.mo4915cd("carrer-logo"));
        e.setAttribute("class", this.aDF.mo20721Rn().aaK());
        e.mo13063q(2, 2);
        m45263NH();
    }

    /* renamed from: NF */
    private void m45261NF() {
        String aOQ;
        if (this.f10224P.bYd() != null && (aOQ = this.f10224P.bYd().mo10706Qu().aOQ()) != null && aOQ.length() > 0) {
            this.amN.mo16824a(C5378aHa.CORP_IMAGES, aOQ);
        }
    }

    /* renamed from: NG */
    private void m45262NG() {
        this.amP.mo22250a(new C4941m());
        this.amP.clear();
        if (this.f10224P.cXk() != null) {
            for (Citizenship vm : this.f10224P.cXk().bqx()) {
                if (vm.bJB().bRH() != null) {
                    this.amP.mo22248G(vm.bJB());
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: NH */
    public void m45263NH() {
        this.aDJ.setText(String.valueOf(this.aDE.mo18222ll()));
        this.aDM.mo22250a(new C4942n());
        this.aDM.clear();
        for (ProgressionLine G : this.aDF.mo20720Rl()) {
            this.aDM.mo22248G(G);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24521a(ProgressionLine afy, Panel aco) {
        Repeater cd = aco.mo4915cd("cellsTable");
        cd.mo22250a(new C4937k());
        cd.clear();
        for (C0335EX G : afy.cZM()) {
            cd.mo22248G(G);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m45269b(ProgressionCell jyVar) {
        if (this.aDF.mo20722Rp().mo18222ll() >= 1) {
            ((CharacterProgression) C3582se.m38985a(this.aDF, (C6144ahM<?>) new C4943o())).mo20735f(jyVar);
        }
    }

    /* access modifiers changed from: private */
    public void undo() {
        ((CharacterProgression) C3582se.m38985a(this.aDF, (C6144ahM<?>) new C4935j())).mo20723Rr();
    }

    /* access modifiers changed from: private */
    public void cancel() {
        ((CharacterProgression) C3582se.m38985a(this.aDF, (C6144ahM<?>) new C4939l())).mo20723Rr();
    }

    /* access modifiers changed from: private */
    /* renamed from: NI */
    public void m45264NI() {
        this.aDF.mo20724Rt();
        setVisible(false);
    }

    /* access modifiers changed from: private */
    public void setVisible(boolean z) {
        if (z) {
            this.aDN = ToolTipManager.sharedInstance().getInitialDelay();
            this.aDP = ToolTipManager.sharedInstance().getReshowDelay();
            ToolTipManager.sharedInstance().setInitialDelay(1);
            ToolTipManager.sharedInstance().setReshowDelay(Integer.MAX_VALUE);
        } else {
            ToolTipManager.sharedInstance().setInitialDelay(this.aDN);
            ToolTipManager.sharedInstance().setReshowDelay(this.aDP);
        }
        this.f10227nM.setVisible(z);
    }

    @C2602hR(mo19255zf = "PROGRESSION_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24522al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$f */
    /* compiled from: a */
    class C4931f extends C6622aqW {
        private static final long serialVersionUID = -1515289216240656702L;

        C4931f(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return ProgressionAddon.this.f10227nM != null && (ProgressionAddon.this.f10227nM.isVisible() || ProgressionAddon.this.f10227nM.isAnimating());
        }

        public boolean isEnabled() {
            return true;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            ProgressionAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$e */
    /* compiled from: a */
    class C4930e extends C3428rT<C5783aaP> {
        C4930e() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (ProgressionAddon.this.f10227nM != null && ProgressionAddon.this.f10227nM.isVisible()) {
                ProgressionAddon.this.f10227nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$d */
    /* compiled from: a */
    class C4929d extends C3428rT<C3689to> {
        C4929d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (ProgressionAddon.this.f10227nM != null && ProgressionAddon.this.f10227nM.isVisible()) {
                ProgressionAddon.this.f10227nM.setVisible(false);
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$c */
    /* compiled from: a */
    class C4928c extends C3428rT<C3949xF> {
        C4928c() {
        }

        /* renamed from: a */
        public void mo321b(C3949xF xFVar) {
            if (xFVar.anX() && ProgressionAddon.this.f10227nM != null && ProgressionAddon.this.f10227nM.isVisible()) {
                ProgressionAddon.this.f10227nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$b */
    /* compiled from: a */
    class C4927b extends ComponentAdapter {
        C4927b() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            ProgressionAddon.this.f10226kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            ProgressionAddon.this.f10226kj.aVU().mo13964a(ProgressionAddon.this.f10229wz);
            ProgressionAddon.this.f10227nM = null;
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$a */
    class C4924a implements C0907NJ {
        /* access modifiers changed from: private */
        public OrbitalCamera bmO;

        C4924a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (ProgressionAddon.this.aDL != null) {
                ProgressionAddon.this.aDL.getScene().removeAllChildren();
                ProgressionAddon.this.aDL.getScene().addChild((SceneObject) renderAsset);
                ((Player) C3582se.m38985a(ProgressionAddon.this.f10224P, (C6144ahM<?>) new C4925a())).bQI();
            }
        }

        /* renamed from: taikodom.addon.progression.ProgressionAddon$a$a */
        class C4925a implements C6144ahM<aVE> {
            C4925a() {
            }

            /* renamed from: a */
            public void mo1931n(aVE ave) {
                if (ave instanceof Avatar) {
                    ave.mo2027b((C0907NJ) new C4926a(ave));
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }

            /* renamed from: taikodom.addon.progression.ProgressionAddon$a$a$a */
            class C4926a implements C0907NJ {
                private final /* synthetic */ aVE eJP;

                C4926a(aVE ave) {
                    this.eJP = ave;
                }

                /* renamed from: a */
                public void mo968a(RenderAsset renderAsset) {
                    RGroup rGroup = new RGroup();
                    RModel aOo = this.eJP.aOo();
                    this.eJP.mo2026ak(this.eJP.mo2030cJ());
                    if (aOo != null) {
                        TransformWrap bcVar = new TransformWrap();
                        bcVar.mo17392z(180.0f);
                        bcVar.setTranslation(ScriptRuntime.NaN, -0.9d, ScriptRuntime.NaN);
                        aOo.setTransform(bcVar);
                        rGroup.addChild(aOo);
                    }
                    ProgressionAddon.this.aDL.getScene().addChild(rGroup);
                    ProgressionAddon.this.aDL.mo16901b((SceneObject) rGroup, 0.0f);
                    ProgressionAddon.this.aDL.setVisible(true);
                    C4924a.this.bmO = ProgressionAddon.this.aDL.csq();
                    C4924a.this.bmO.setDistance(new Vec3d(ScriptRuntime.NaN, (double) (this.eJP.getHeight() * 0.05f), 7.0d));
                    C4924a.this.bmO.setFarPlane(5000.0f);
                    C4924a.this.bmO.setNearPlane(0.01f);
                    C4924a.this.bmO.setFovY(20.0f);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$i */
    /* compiled from: a */
    class C4934i implements ActionListener {
        C4934i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ProgressionAddon.this.undo();
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$h */
    /* compiled from: a */
    class C4933h implements ActionListener {
        C4933h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ProgressionAddon.this.cancel();
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$g */
    /* compiled from: a */
    class C4932g implements ActionListener {
        C4932g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ProgressionAddon.this.m45264NI();
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$m */
    /* compiled from: a */
    class C4941m implements Repeater.C3671a<CitizenshipType> {
        C4941m() {
        }

        /* renamed from: a */
        public void mo843a(CitizenshipType adl, Component component) {
            Picture axm = (Picture) component;
            axm.setImage(ProgressionAddon.this.f10226kj.bHv().adz().getImage(String.valueOf(C5378aHa.CITIZENSHIP.getPath()) + adl.bRH().getHandle()));
            axm.setToolTipText(adl.mo12809rP().get());
            axm.setSize(10, 10);
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$n */
    /* compiled from: a */
    class C4942n implements Repeater.C3671a<ProgressionLine> {
        C4942n() {
        }

        /* renamed from: a */
        public void mo843a(ProgressionLine afy, Component component) {
            Panel aco = (Panel) component;
            JLabel cf = aco.mo4917cf("title");
            cf.setText(afy.cZK().mo19151rP().get());
            cf.putClientProperty(C2353eO.f6741DU, afy);
            ProgressionCell cZG = afy.cZG();
            if (!cZG.mo20008FY() && !cZG.mo20003FN() && !cZG.mo20005FR()) {
                ComponentManager.getCssHolder(aco).setAttribute("class", "line-disabled");
            }
            if (afy.cZK().mo19152sK() != null) {
                aco.mo4915cd("line-icon").mo16824a(C5378aHa.PROGRESSION, afy.cZK().mo19152sK().getHandle());
            }
            aco.mo4915cd("line-icon-panel").putClientProperty(C2353eO.f6741DU, afy);
            ProgressionAddon.this.mo24521a(afy, aco);
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$k */
    /* compiled from: a */
    class C4937k implements Repeater.C3671a<C0335EX> {
        int counter = 0;

        C4937k() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00a8  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void mo843a(p001a.C0335EX r7, java.awt.Component r8) {
            /*
                r6 = this;
                r0 = 0
                a.jy r3 = r7.aPf()
                a.kg r4 = r7.mo1881FP()
                javax.swing.JButton r8 = (javax.swing.JButton) r8
                int r1 = r6.counter
                int r1 = r1 + 1
                r6.counter = r1
                java.lang.Object r1 = taikodom.addon.tooltip.C2353eO.f6741DU
                a.kg r2 = r7.mo1881FP()
                r8.putClientProperty(r1, r2)
                java.lang.String r2 = "disabled"
                r1 = 1
                if (r3 == 0) goto L_0x00b2
                boolean r5 = r3.mo20003FN()
                if (r5 != 0) goto L_0x002b
                boolean r5 = r3.mo20005FR()
                if (r5 == 0) goto L_0x0096
            L_0x002b:
                java.lang.String r1 = "installed"
                r2 = r1
            L_0x002e:
                a.tC r1 = r4.mo20115sK()
                if (r1 == 0) goto L_0x0038
                if (r0 == 0) goto L_0x0038
                java.lang.String r2 = "disabled-icon"
            L_0x0038:
                a.aeK r0 = p001a.C3280pv.m37354e(r8)
                java.lang.String r1 = "class"
                r0.setAttribute(r1, r2)
                a.tC r0 = r4.mo20115sK()
                if (r0 == 0) goto L_0x00a8
                taikodom.addon.progression.ProgressionAddon r0 = taikodom.addon.progression.ProgressionAddon.this
                a.vW r0 = r0.f10226kj
                a.axa r0 = r0.bHv()
                a.azB r0 = r0.adz()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                a.aHa r2 = logic.swing.C5378aHa.PROGRESSION
                java.lang.String r2 = r2.getPath()
                java.lang.String r2 = java.lang.String.valueOf(r2)
                r1.<init>(r2)
                a.tC r2 = r4.mo20115sK()
                java.lang.String r2 = r2.getHandle()
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                java.awt.Image r0 = r0.getImage(r1)
                java.awt.Dimension r1 = r8.getPreferredSize()
                int r1 = r1.width
                java.awt.Dimension r2 = r8.getPreferredSize()
                int r2 = r2.height
                if (r1 <= 0) goto L_0x0095
                if (r2 <= 0) goto L_0x0095
                javax.swing.ImageIcon r3 = new javax.swing.ImageIcon
                r4 = 4
                java.awt.Image r0 = r0.getScaledInstance(r1, r2, r4)
                r3.<init>(r0)
                r8.setIcon(r3)
            L_0x0095:
                return
            L_0x0096:
                boolean r5 = r3.mo20008FY()
                if (r5 == 0) goto L_0x00b2
                java.lang.String r1 = "installable"
                taikodom.addon.progression.ProgressionAddon$k$a r2 = new taikodom.addon.progression.ProgressionAddon$k$a
                r2.<init>(r3)
                r8.addActionListener(r2)
                r2 = r1
                goto L_0x002e
            L_0x00a8:
                int r0 = r6.counter
                java.lang.String r0 = java.lang.String.valueOf(r0)
                r8.setText(r0)
                goto L_0x0095
            L_0x00b2:
                r0 = r1
                goto L_0x002e
            */
            throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.progression.ProgressionAddon.C4937k.mo843a(a.EX, java.awt.Component):void");
        }

        /* renamed from: taikodom.addon.progression.ProgressionAddon$k$a */
        class C4938a implements ActionListener {
            private final /* synthetic */ ProgressionCell dLw;

            C4938a(ProgressionCell jyVar) {
                this.dLw = jyVar;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                ProgressionAddon.this.m45269b(this.dLw);
            }
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$o */
    /* compiled from: a */
    class C4943o implements C6144ahM<Boolean> {
        C4943o() {
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            ProgressionAddon.this.m45263NH();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$j */
    /* compiled from: a */
    class C4935j implements C6144ahM {
        C4935j() {
        }

        /* renamed from: n */
        public void mo1931n(Object obj) {
            SwingUtilities.invokeLater(new C4936a());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: taikodom.addon.progression.ProgressionAddon$j$a */
        class C4936a implements Runnable {
            C4936a() {
            }

            public void run() {
                ProgressionAddon.this.m45263NH();
            }
        }
    }

    /* renamed from: taikodom.addon.progression.ProgressionAddon$l */
    /* compiled from: a */
    class C4939l implements C6144ahM {
        C4939l() {
        }

        /* renamed from: n */
        public void mo1931n(Object obj) {
            SwingUtilities.invokeLater(new C4940a());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
        }

        /* renamed from: taikodom.addon.progression.ProgressionAddon$l$a */
        class C4940a implements Runnable {
            C4940a() {
            }

            public void run() {
                ProgressionAddon.this.setVisible(false);
            }
        }
    }
}
