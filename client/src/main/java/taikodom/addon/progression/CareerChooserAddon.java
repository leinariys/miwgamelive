package taikodom.addon.progression;

import game.script.progression.ProgressionCareer;
import game.script.ship.ShipType;
import logic.IAddonSettings;
import logic.aaa.aPY;
import logic.ui.Panel;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.progression")
/* compiled from: a */
public class CareerChooserAddon implements C2495fo {
    private static final String hCU = "career-chooser.xml";
    /* renamed from: kj */
    public IAddonProperties f10219kj;
    /* access modifiers changed from: private */
    /* renamed from: nM */
    public InternalFrame f10220nM;
    /* access modifiers changed from: private */
    private C3428rT<aPY> fdc;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10219kj = addonPropertie;
        this.fdc = new C4913a();
        this.f10219kj.aVU().mo13965a(aPY.class, this.fdc);
    }

    /* renamed from: Im */
    private void m45226Im() {
        this.f10220nM = this.f10219kj.bHv().mo16792bN(hCU);
    }

    /* renamed from: iJ */
    private void m45231iJ() {
        Repeater cd = this.f10220nM.mo4915cd("typesTable");
        cd.mo22250a(new C4914b());
        cd.clear();
        for (ProgressionCareer G : this.f10219kj.ala().aJk().mo15612aD()) {
            cd.mo22248G(G);
        }
        this.f10220nM.setSize(560, 520);
        this.f10220nM.setLocation((this.f10219kj.bHv().getScreenSize().width - this.f10220nM.getSize().width) / 2, (this.f10219kj.bHv().getScreenSize().height - this.f10220nM.getSize().height) / 2);
    }

    /* access modifiers changed from: private */
    public void destroy() {
        if (this.f10220nM != null) {
            this.f10220nM.destroy();
            this.f10220nM = null;
        }
    }

    public void stop() {
        destroy();
        this.f10219kj.aVU().mo13964a(this.fdc);
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f10220nM == null) {
            m45226Im();
        }
        m45231iJ();
        this.f10220nM.setVisible(true);
    }

    /* renamed from: taikodom.addon.progression.CareerChooserAddon$a */
    class C4913a extends C3428rT<aPY> {
        C4913a() {
        }

        /* renamed from: b */
        public void mo321b(aPY apy) {
            if (apy.mo10808Rn() == null) {
                CareerChooserAddon.this.open();
            } else {
                CareerChooserAddon.this.destroy();
            }
        }
    }

    /* renamed from: taikodom.addon.progression.CareerChooserAddon$b */
    /* compiled from: a */
    class C4914b implements Repeater.C3671a<ProgressionCareer> {
        C4914b() {
        }

        /* renamed from: a */
        public void mo843a(ProgressionCareer sbVar, Component component) {
            Panel aco = (Panel) component;
            aco.mo4917cf("title").setText(sbVar.mo21959rP().get());
            aco.mo4915cd("description").setText(sbVar.mo21961vW().get());
            if (sbVar.mo21962xh() != null) {
                aco.mo4917cf("station").setText(sbVar.mo21962xh().getName());
            }
            if (sbVar.aaA().size() > 0) {
                aco.mo4917cf("ship").setText(((ShipType) sbVar.aaA().get(0)).mo19891ke().get());
            }
            aco.mo4913cb("choose").addActionListener(new C4915a(sbVar));
        }

        /* renamed from: taikodom.addon.progression.CareerChooserAddon$b$a */
        class C4915a implements ActionListener {
            private final /* synthetic */ ProgressionCareer gJA;

            C4915a(ProgressionCareer sbVar) {
                this.gJA = sbVar;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                CareerChooserAddon.this.f10220nM.setVisible(false);
                CareerChooserAddon.this.f10219kj.aVU().mo13975h(new aPY(this.gJA));
            }
        }
    }
}
