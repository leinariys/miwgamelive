package taikodom.addon.progression;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C3131oI;
import game.script.item.WeaponType;
import game.script.progression.ProgressionCareer;
import game.script.resource.Asset;
import game.script.ship.ShipStructureType;
import game.script.ship.ShipType;
import game.script.ship.categories.BomberType;
import game.script.ship.categories.FighterType;
import logic.IAddonSettings;
import logic.res.sound.C0907NJ;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.Component3d;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import p001a.C2293dh;
import p001a.C3428rT;
import p001a.C3667tV;
import p001a.C6811auD;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.addon.tooltip.C6120ago;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@TaikodomAddon("taikodom.addon.progression")
/* compiled from: a */
public class CareerShipChooserAddon implements C2495fo {
    private static final String aDD = "ship-chooser.xml";
    private static final String iSo = "regular";
    private static final String iSp = "good";
    private static final String iSq = "bad";
    /* access modifiers changed from: private */
    public ShipType iSm;
    /* renamed from: kj */
    public IAddonProperties f10222kj;
    /* renamed from: BU */
    private ProgressionCareer f10221BU;
    private C3428rT<C3131oI> bCD;
    private JButton gbK;
    private Repeater<ShipType> iSl;
    /* access modifiers changed from: private */
    private ArrayList<ShipType> iSn;
    /* renamed from: nM */
    private InternalFrame f10223nM;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10222kj = addonPropertie;
        m45240Im();
        m45247iM();
        bli();
        this.f10223nM.setModal(true);
        setVisible(false);
    }

    /* renamed from: iM */
    private void m45247iM() {
        this.iSl = this.f10223nM.mo4915cd("ships");
        this.gbK = this.f10223nM.mo4913cb("confirm");
        this.gbK.addActionListener(new C4920d());
        dwj();
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.f10221BU == null) {
            this.f10221BU = this.f10222kj.getPlayer().mo12637Rn();
        }
        dwh();
        dwi();
        this.f10223nM.center();
        setVisible(true);
    }

    private void dwh() {
        if (this.iSn == null) {
            this.iSn = new ArrayList<>();
        }
        if (this.f10221BU != null) {
            for (ShipType ng : this.f10221BU.aaA()) {
                this.iSn.add(ng);
                float VH = ng.mo4153VH();
                float ra = ng.mo4217ra();
                float wE = ng.mo4156Wb().mo11172wE();
                float hh = ((ShipStructureType) ng.agt().get(0)).mo9731VX().mo15373hh();
                float hh2 = ng.mo4155VT().mo22727hh();
                if (VH > C4922f.few) {
                    if (C4923g.few == 0.0f) {
                        C4923g.few = C4922f.few;
                    }
                    C4922f.few = VH;
                } else if (VH < C4923g.few || C4923g.few == 0.0f) {
                    C4923g.few = VH;
                }
                if (ra > C4922f.fex) {
                    if (C4923g.fex == 0.0f) {
                        C4923g.fex = C4922f.fex;
                    }
                    C4922f.fex = ra;
                } else if (ra < C4923g.fex || C4923g.fex == 0.0f) {
                    C4923g.fex = ra;
                }
                if (wE > C4922f.fey) {
                    if (C4923g.fey == 0.0f) {
                        C4923g.fey = C4922f.fey;
                    }
                    C4922f.fey = wE;
                } else if (wE < C4923g.fey || C4923g.fey == 0.0f) {
                    C4923g.fey = wE;
                }
                if (hh2 > C4922f.feA) {
                    if (C4923g.feA == 0.0f) {
                        C4923g.feA = C4922f.feA;
                    }
                    C4922f.feA = hh2;
                } else if (hh2 < C4923g.feA || C4923g.feA == 0.0f) {
                    C4923g.feA = hh2;
                }
                if (hh > C4922f.fez) {
                    if (C4923g.fez == 0.0f) {
                        C4923g.fez = C4922f.fez;
                    }
                    C4922f.fez = hh;
                } else if (hh < C4923g.fez || C4923g.fez == 0.0f) {
                    C4923g.fez = hh;
                }
            }
        }
    }

    private void dwi() {
        this.iSl.clear();
        Iterator<ShipType> it = this.iSn.iterator();
        while (it.hasNext()) {
            this.iSl.mo22248G(it.next());
        }
    }

    private void dwj() {
        this.iSl.mo22250a(new C4918c());
    }

    /* access modifiers changed from: private */
    /* renamed from: A */
    public void m45239A(ShipType ng) {
        if (this.iSm == null || !this.iSm.equals(ng)) {
            this.iSm = ng;
            dwi();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24512a(ShipType ng, Component3d ayVar) {
        Asset Nu = ng.mo4151Nu();
        if (Nu != null) {
            C4917b bVar = new C4917b(ayVar);
            this.f10222kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/default_scene.pro", "default_scene", (Scene) null, new C4916a(ayVar), "Career Ship");
            this.f10222kj.getEngineGame().getLoaderTrail().mo4995a(Nu.getFile(), Nu.getHandle(), (Scene) null, bVar, "Career Ship");
        }
    }

    private void setVisible(boolean z) {
        this.f10223nM.setVisible(z);
    }

    private void bli() {
        this.bCD = new C4921e();
        this.f10222kj.aVU().mo13965a(C3131oI.class, this.bCD);
    }

    /* renamed from: Im */
    private void m45240Im() {
        this.f10223nM = this.f10222kj.bHv().mo16792bN(aDD);
    }

    public void stop() {
        this.f10222kj.aVU().mo13964a(this.bCD);
        if (this.f10223nM != null) {
            this.f10223nM.destroy();
            this.f10223nM = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: NI */
    public void m45241NI() {
        if (this.iSm == null) {
            ((MessageDialogAddon) this.f10222kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.INFO, this.f10222kj.translate("You must select a ship"), this.f10222kj.translate("Ok"));
            return;
        }
        try {
            this.f10222kj.ala().mo1437b(this.f10222kj.getPlayer(), this.iSm, (List<WeaponType>) this.f10221BU.aaE());
            setVisible(false);
            this.f10222kj.aVU().mo13975h(new C3131oI(C3131oI.C3132a.CAREER_CHOSEN));
            this.f10222kj.aVU().mo13964a(this.bCD);
            this.bCD = null;
        } catch (C2293dh e) {
            throw new IllegalStateException("Could not create a ship for player " + this.f10222kj.getPlayer());
        }
    }

    /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$f */
    /* compiled from: a */
    static class C4922f {
        static float feA = 0.0f;
        static float few = 0.0f;
        static float fex = 0.0f;
        static float fey = 0.0f;
        static float fez = 0.0f;

        C4922f() {
        }
    }

    /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$g */
    /* compiled from: a */
    static class C4923g {
        static float feA = 0.0f;
        static float few = 0.0f;
        static float fex = 0.0f;
        static float fey = 0.0f;
        static float fez = 0.0f;

        C4923g() {
        }
    }

    /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$d */
    /* compiled from: a */
    class C4920d implements ActionListener {
        C4920d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            CareerShipChooserAddon.this.m45241NI();
        }
    }

    /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$c */
    /* compiled from: a */
    class C4918c implements Repeater.C3671a<ShipType> {
        C4918c() {
        }

        /* renamed from: a */
        public void mo843a(ShipType ng, Component component) {
            Panel aco = (Panel) component;
            aco.putClientProperty(C6120ago.f4532DU, ng);
            aco.addMouseListener(new C4919a(ng));
            aco.mo4917cf("name").setText(ng.mo19891ke().get());
            Component3d cd = aco.mo4915cd("iconRender");
            if (CareerShipChooserAddon.this.iSm != null && CareerShipChooserAddon.this.iSm.equals(ng)) {
                ComponentManager.getCssHolder(cd).setAttribute("class", "horizontalbox ship-selected");
            }
            CareerShipChooserAddon.this.mo24512a(ng, cd);
            float VH = ng.mo4153VH();
            float ra = ng.mo4217ra();
            float wE = ng.mo4156Wb().mo11172wE();
            float hh = ((ShipStructureType) ng.agt().get(0)).mo9731VX().mo15373hh();
            float hh2 = ng.mo4155VT().mo22727hh();
            String str = CareerShipChooserAddon.iSo;
            if (ra == C4922f.fex) {
                str = CareerShipChooserAddon.iSp;
            } else if (ra == C4923g.fex) {
                str = CareerShipChooserAddon.iSq;
            }
            aco.mo4917cf("firstName").setText(CareerShipChooserAddon.this.f10222kj.translate("Velocity"));
            ComponentManager.getCssHolder(aco.mo4915cd("firstValue")).setAttribute("class", str);
            if ((ng instanceof BomberType) || (ng instanceof FighterType)) {
                String str2 = CareerShipChooserAddon.iSo;
                if (VH == C4922f.few) {
                    str2 = CareerShipChooserAddon.iSp;
                } else if (VH == C4923g.few) {
                    str2 = CareerShipChooserAddon.iSq;
                }
                aco.mo4917cf("secondName").setText(CareerShipChooserAddon.this.f10222kj.translate("Maneuver"));
                ComponentManager.getCssHolder(aco.mo4915cd("secondValue")).setAttribute("class", str2);
            } else {
                String str3 = CareerShipChooserAddon.iSo;
                if (wE == C4922f.fey) {
                    str3 = CareerShipChooserAddon.iSp;
                } else if (wE == C4923g.fey) {
                    str3 = CareerShipChooserAddon.iSq;
                }
                aco.mo4917cf("secondName").setText(CareerShipChooserAddon.this.f10222kj.translate("Capacity"));
                ComponentManager.getCssHolder(aco.mo4915cd("secondValue")).setAttribute("class", str3);
            }
            String str4 = CareerShipChooserAddon.iSo;
            if (hh == C4922f.fez) {
                str4 = CareerShipChooserAddon.iSp;
            } else if (hh == C4923g.fez) {
                str4 = CareerShipChooserAddon.iSq;
            }
            aco.mo4917cf("thirdName").setText(CareerShipChooserAddon.this.f10222kj.translate("Shield"));
            ComponentManager.getCssHolder(aco.mo4915cd("thirdValue")).setAttribute("class", str4);
            String str5 = CareerShipChooserAddon.iSo;
            if (hh2 == C4922f.feA) {
                str5 = CareerShipChooserAddon.iSp;
            } else if (hh2 == C4923g.feA) {
                str5 = CareerShipChooserAddon.iSq;
            }
            aco.mo4917cf("fourthName").setText(CareerShipChooserAddon.this.f10222kj.translate("Hull"));
            ComponentManager.getCssHolder(aco.mo4915cd("fourthValue")).setAttribute("class", str5);
        }

        /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$c$a */
        class C4919a implements MouseListener {
            private final /* synthetic */ ShipType aSp;

            C4919a(ShipType ng) {
                this.aSp = ng;
            }

            public void mouseClicked(MouseEvent mouseEvent) {
                CareerShipChooserAddon.this.m45239A(this.aSp);
                CareerShipChooserAddon.this.f10222kj.aVU().mo13975h(new C6811auD(C3667tV.MOUSE_CLICK_SFX));
            }

            public void mouseEntered(MouseEvent mouseEvent) {
            }

            public void mouseExited(MouseEvent mouseEvent) {
            }

            public void mousePressed(MouseEvent mouseEvent) {
            }

            public void mouseReleased(MouseEvent mouseEvent) {
            }
        }
    }

    /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$b */
    /* compiled from: a */
    class C4917b implements C0907NJ {
        private final /* synthetic */ Component3d eyq;

        C4917b(Component3d ayVar) {
            this.eyq = ayVar;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            this.eyq.getSceneView().getSceneViewQuality().setShaderQuality(CareerShipChooserAddon.this.f10222kj.ale().getShaderQuality());
            SceneObject sceneObject = (SceneObject) renderAsset;
            this.eyq.getScene().addChild(sceneObject);
            this.eyq.mo16902c(sceneObject, 20.0f);
            this.eyq.setVisible(true);
        }
    }

    /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$a */
    class C4916a implements C0907NJ {
        private final /* synthetic */ Component3d eyq;

        C4916a(Component3d ayVar) {
            this.eyq = ayVar;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            this.eyq.getScene().removeAllChildren();
            this.eyq.getScene().addChild((SceneObject) renderAsset);
        }
    }

    /* renamed from: taikodom.addon.progression.CareerShipChooserAddon$e */
    /* compiled from: a */
    class C4921e extends C3428rT<C3131oI> {
        C4921e() {
        }

        /* renamed from: b */
        public void mo321b(C3131oI oIVar) {
            if (oIVar.mo20956Us().equals(C3131oI.C3132a.CAREER_CHOOSER) && CareerShipChooserAddon.this.f10222kj.getPlayer().bQB() && CareerShipChooserAddon.this.f10222kj.getPlayer().bhE().equals(CareerShipChooserAddon.this.f10222kj.getPlayer().dxZ().mo5652xh())) {
                CareerShipChooserAddon.this.open();
            }
        }
    }
}
