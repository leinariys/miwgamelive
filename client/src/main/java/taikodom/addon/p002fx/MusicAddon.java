package taikodom.addon.p002fx;

import game.network.message.externalizable.C3131oI;
import game.network.message.externalizable.aUU;
import game.script.space.Node;
import logic.IAddonSettings;
import logic.aaa.C1274Sq;
import logic.aaa.C5783aaP;
import logic.render.IEngineGraphics;
import p001a.C5990aeO;
import p001a.C6087agH;
import p001a.C6124ags;
import p001a.aMU;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.fx")
/* renamed from: taikodom.addon.fx.MusicAddon */
/* compiled from: a */
public class MusicAddon implements C2495fo {
    private static final long cUc = 2000;
    /* renamed from: kj */
    public IAddonProperties f9889kj;
    /* renamed from: Rh */
    private C6124ags<C5783aaP> f9888Rh;
    private C6124ags<C3131oI> bwZ;
    private C6124ags<C1274Sq> cUd;
    /* access modifiers changed from: private */
    private C6124ags<aUU> cUe;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9889kj = addonPropertie;
        this.cUd = new C4421b();
        this.f9888Rh = new C4419a();
        this.cUe = new C4422c();
        this.bwZ = new C4423d();
        this.f9889kj.aVU().mo13965a(C1274Sq.class, this.cUd);
        this.f9889kj.aVU().mo13965a(C5783aaP.class, this.f9888Rh);
        this.f9889kj.aVU().mo13965a(aUU.class, this.cUe);
        this.f9889kj.aVU().mo13965a(C3131oI.class, this.bwZ);
    }

    public void stop() {
        this.f9889kj.aVU().mo13964a(this.cUd);
        this.f9889kj.aVU().mo13964a(this.f9888Rh);
        this.f9889kj.aVU().mo13964a(this.cUe);
        this.f9889kj.aVU().mo13964a(this.bwZ);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43547a(C5990aeO aeo) {
        m43548a(aeo, 0.0f);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43548a(C5990aeO aeo, float f) {
        IEngineGraphics ale = this.f9889kj.ale();
        if (ale != null) {
            ale.mo3049bV(aeo.getFile());
            ale.mo3054c(aeo.getHandle(), f);
        }
    }

    /* renamed from: b */
    private void m43552b(C5990aeO aeo) {
        m43553b(aeo, 0.0f);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m43553b(C5990aeO aeo, float f) {
        IEngineGraphics ale = this.f9889kj.ale();
        if (ale != null) {
            ale.mo3049bV(aeo.getFile());
            ale.mo3045b(aeo.getHandle(), f);
        }
    }

    /* access modifiers changed from: private */
    public void aPa() {
        this.f9889kj.ale().mo3062dr(2000.0f);
    }

    /* access modifiers changed from: private */
    public void aPb() {
        this.f9889kj.ale().mo3063ds(2000.0f);
    }

    /* renamed from: taikodom.addon.fx.MusicAddon$b */
    /* compiled from: a */
    class C4421b extends C6124ags<C1274Sq> {
        private static /* synthetic */ int[] hxI;

        C4421b() {
        }

        static /* synthetic */ int[] cRQ() {
            int[] iArr = hxI;
            if (iArr == null) {
                iArr = new int[C1274Sq.C1275a.values().length];
                try {
                    iArr[C1274Sq.C1275a.DOCK.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C1274Sq.C1275a.ENTER_GATE.ordinal()] = 3;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C1274Sq.C1275a.EXIT_GATE.ordinal()] = 4;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C1274Sq.C1275a.UNDOCK.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                hxI = iArr;
            }
            return iArr;
        }

        /* renamed from: b */
        public boolean updateInfo(C1274Sq sq) {
            switch (cRQ()[sq.fMR.ordinal()]) {
                case 1:
                    MusicAddon.this.aPa();
                    MusicAddon.this.aPb();
                    MusicAddon.this.m43553b(C6087agH.STATION_AMBIANCE, 3000.0f);
                    if (MusicAddon.this.f9889kj.getPlayer().cXm() != aMU.FINISHED_NURSERY && MusicAddon.this.f9889kj.getPlayer().cXm() != aMU.CAREER_SHIP_CHOOSER) {
                        return false;
                    }
                    MusicAddon.this.m43547a((C5990aeO) C6087agH.INSIDE_STATION);
                    return false;
                default:
                    return false;
            }
        }
    }

    /* renamed from: taikodom.addon.fx.MusicAddon$a */
    class C4419a extends C6124ags<C5783aaP> {

        /* renamed from: Zn */
        private static /* synthetic */ int[] f9890Zn;

        C4419a() {
        }

        /* renamed from: BK */
        static /* synthetic */ int[] m43559BK() {
            int[] iArr = f9890Zn;
            if (iArr == null) {
                iArr = new int[C5783aaP.C1841a.values().length];
                try {
                    iArr[C5783aaP.C1841a.DOCKED.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5783aaP.C1841a.FLYING.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5783aaP.C1841a.JUMPING.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5783aaP.C1841a.ONI.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                f9890Zn = iArr;
            }
            return iArr;
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            switch (m43559BK()[aap.bMO().ordinal()]) {
                case 2:
                    MusicAddon.this.aPa();
                    MusicAddon.this.aPb();
                    Node azW = MusicAddon.this.f9889kj.getPlayer().bQx().azW();
                    if (azW == null) {
                        return false;
                    }
                    C4420a aVar = new C4420a(azW);
                    if (aVar.getHandle().contains("birth")) {
                        MusicAddon.this.m43548a((C5990aeO) aVar, 4000.0f);
                        return false;
                    }
                    MusicAddon.this.m43553b(aVar, 2000.0f);
                    return false;
                default:
                    return false;
            }
        }

        /* renamed from: taikodom.addon.fx.MusicAddon$a$a */
        class C4420a implements C5990aeO {
            private final /* synthetic */ Node cAn;

            C4420a(Node rPVar) {
                this.cAn = rPVar;
            }

            public String getHandle() {
                return String.valueOf(this.cAn.mo21662ip()) + "_ambience";
            }

            public String getFile() {
                return C6087agH.fyR;
            }
        }
    }

    /* renamed from: taikodom.addon.fx.MusicAddon$c */
    /* compiled from: a */
    class C4422c extends C6124ags<aUU> {
        C4422c() {
        }

        /* renamed from: b */
        public boolean updateInfo(aUU auu) {
            switch (auu.dAD()) {
                case 1:
                    MusicAddon.this.m43547a((C5990aeO) C6087agH.LOGIN_SCREEN);
                    return false;
                case 2:
                    MusicAddon.this.aPb();
                    return false;
                default:
                    return false;
            }
        }
    }

    /* renamed from: taikodom.addon.fx.MusicAddon$d */
    /* compiled from: a */
    class C4423d extends C6124ags<C3131oI> {

        /* renamed from: JB */
        private static /* synthetic */ int[] f9891JB;

        C4423d() {
        }

        /* renamed from: pg */
        static /* synthetic */ int[] m43566pg() {
            int[] iArr = f9891JB;
            if (iArr == null) {
                iArr = new int[C3131oI.C3132a.values().length];
                try {
                    iArr[C3131oI.C3132a.BOUGHT_MISSION_ITEM.ordinal()] = 22;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C3131oI.C3132a.CAREER_CHOOSER.ordinal()] = 28;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C3131oI.C3132a.CAREER_CHOSEN.ordinal()] = 29;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C3131oI.C3132a.CREATED_AVATAR.ordinal()] = 11;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C3131oI.C3132a.FINISHED_NURSERY.ordinal()] = 30;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[C3131oI.C3132a.FIRST_LAUNCH.ordinal()] = 15;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_DESTINATION_CONTAINER.ordinal()] = 20;
                } catch (NoSuchFieldError e7) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_DOCK_BUTTON.ordinal()] = 24;
                } catch (NoSuchFieldError e8) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_HANGAR.ordinal()] = 14;
                } catch (NoSuchFieldError e9) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_MARKET.ordinal()] = 16;
                } catch (NoSuchFieldError e10) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_MISSION_ITEM.ordinal()] = 18;
                } catch (NoSuchFieldError e11) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_OUTPOST.ordinal()] = 23;
                } catch (NoSuchFieldError e12) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_SHIP_CONTAINER.ordinal()] = 27;
                } catch (NoSuchFieldError e13) {
                }
                try {
                    iArr[C3131oI.C3132a.HIGHLIGHT_SHIP_WINDOW.ordinal()] = 25;
                } catch (NoSuchFieldError e14) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_CHAT.ordinal()] = 6;
                } catch (NoSuchFieldError e15) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_EQUIPMENT_SELECT.ordinal()] = 8;
                } catch (NoSuchFieldError e16) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_LEFT_SIDE_MENU.ordinal()] = 2;
                } catch (NoSuchFieldError e17) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_OTHER_ELEMENTS.ordinal()] = 9;
                } catch (NoSuchFieldError e18) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_PDA.ordinal()] = 5;
                } catch (NoSuchFieldError e19) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_PERSONAL_MENU.ordinal()] = 1;
                } catch (NoSuchFieldError e20) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_RIGHT_SIDE_MENU.ordinal()] = 3;
                } catch (NoSuchFieldError e21) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_STATION_OPTIONS.ordinal()] = 4;
                } catch (NoSuchFieldError e22) {
                }
                try {
                    iArr[C3131oI.C3132a.ONI_CHECKLIST_WEAPON_SELECT.ordinal()] = 7;
                } catch (NoSuchFieldError e23) {
                }
                try {
                    iArr[C3131oI.C3132a.OPENED_MARKET.ordinal()] = 17;
                } catch (NoSuchFieldError e24) {
                }
                try {
                    iArr[C3131oI.C3132a.OPENED_SHIP_WINDOW.ordinal()] = 26;
                } catch (NoSuchFieldError e25) {
                }
                try {
                    iArr[C3131oI.C3132a.OPEN_AVATAR_CREATION.ordinal()] = 10;
                } catch (NoSuchFieldError e26) {
                }
                try {
                    iArr[C3131oI.C3132a.SELECTED_DESTINATION_CONTAINER.ordinal()] = 21;
                } catch (NoSuchFieldError e27) {
                }
                try {
                    iArr[C3131oI.C3132a.SELECTED_MISSION_ITEM.ordinal()] = 19;
                } catch (NoSuchFieldError e28) {
                }
                try {
                    iArr[C3131oI.C3132a.SHIP_INSTALLED.ordinal()] = 12;
                } catch (NoSuchFieldError e29) {
                }
                try {
                    iArr[C3131oI.C3132a.UNLOCK_HANGAR.ordinal()] = 13;
                } catch (NoSuchFieldError e30) {
                }
                f9891JB = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public boolean updateInfo(C3131oI oIVar) {
            switch (m43566pg()[oIVar.mo20956Us().ordinal()]) {
                case 10:
                    MusicAddon.this.aPa();
                    MusicAddon.this.aPb();
                    MusicAddon.this.m43547a((C5990aeO) C6087agH.AVATAR_CREATION_MUSIC);
                    return false;
                case 11:
                    MusicAddon.this.aPa();
                    MusicAddon.this.aPb();
                    MusicAddon.this.m43553b(C6087agH.STATION_AMBIANCE, 3000.0f);
                    return false;
                default:
                    return false;
            }
        }
    }
}
