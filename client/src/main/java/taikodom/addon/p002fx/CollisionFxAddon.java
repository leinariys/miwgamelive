package taikodom.addon.p002fx;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.C1042PH;
import game.script.Actor;
import game.script.item.Shot;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.space.Asteroid;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C5783aaP;
import logic.baa.C6909avx;
import p001a.C3257pi;
import p001a.C6124ags;
import p001a.C6467anX;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.util.HashMap;

@TaikodomAddon("taikodom.addon.fx")
/* renamed from: taikodom.addon.fx.CollisionFxAddon */
/* compiled from: a */
public class CollisionFxAddon implements C2495fo {

    /* renamed from: Ri */
    private static final long f9877Ri = 120000;
    /* access modifiers changed from: private */

    /* renamed from: Rl */
    public static long f9878Rl = 100;
    /* access modifiers changed from: private */

    /* renamed from: Rm */
    public static long f9879Rm = 250;
    /* renamed from: Rj */
    public HashMap<C4405e, C4404d> f9882Rj = new HashMap<>();
    /* renamed from: Rk */
    public HashMap<C4401a, Long> f9883Rk = new HashMap<>();
    /* access modifiers changed from: private */
    /* renamed from: Rn */
    public WrapRunnable f9884Rn = new C6467anX(this);
    /* access modifiers changed from: private */
    /* renamed from: kj */
    public IAddonProperties f9885kj;
    /* access modifiers changed from: private */
    /* renamed from: Rg */
    private C6124ags<C1042PH> f9880Rg;
    /* access modifiers changed from: private */
    /* renamed from: Rh */
    private C6124ags<C5783aaP> f9881Rh;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9885kj = addonPropertie;
        this.f9880Rg = new C4403c();
        this.f9881Rh = new C4402b();
        this.f9885kj.aVU().mo13965a(C1042PH.class, this.f9880Rg);
        this.f9885kj.aVU().mo13965a(C5783aaP.class, this.f9881Rh);
    }

    public void stop() {
        this.f9884Rn.cancel();
        this.f9885kj.aVU().mo13964a(this.f9880Rg);
        this.f9885kj.aVU().mo13964a(this.f9881Rh);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23843a(C1042PH ph) {
        boolean z;
        C6909avx avx;
        Actor bnT = ph.bnT();
        Actor bnU = ph.bnU();
        if (bnT.mo1000b(bnT.azW(), bnT.mo960Nc())) {
            if (bnT instanceof Ship) {
                z = ((Ship) bnT).agZ();
            } else {
                z = false;
            }
            if (bnU instanceof Ship) {
                Ship fAVar = (Ship) bnU;
                z = fAVar.agZ() || fAVar.ahd();
            }
            C6909avx a = m43488a(bnT, bnU, z);
            if (!z || a != null) {
                avx = a;
            } else {
                avx = m43488a(bnT, bnU, false);
            }
            if (avx != null) {
                Vec3d cgp = ph.bnX().cgp();
                if (bnU instanceof Shot) {
                    Pawn cLw = ((Shot) bnU).cLw();
                    Vec3f dfO = bnT.getPosition().dfV().mo23503i(ph.bnV()).mo23506j(bnU.getPosition().dfV()).dfO();
                    if (!(bnT instanceof Ship) || ((Ship) bnT).mo8288zv() == null || ((Ship) bnT).mo8288zv().mo19072TD()) {
                        m43489a(avx.czp(), cgp, dfO);
                        m43493a(avx.czr(), cgp, dfO, avx.czx(), bnT, cLw);
                        return;
                    }
                    m43493a(avx.czt() == null ? avx.czr() : avx.czt(), cgp, dfO, avx.czt() == null ? avx.czx() : avx.czz(), bnT, cLw);
                    if (bnT instanceof Ship) {
                        Ship fAVar2 = (Ship) bnT;
                        fAVar2.aho().mo17863a(fAVar2, ph.bnV(), dfO, ph.mo4650vO());
                        Vec3f kS = fAVar2.aho().mo17866kS();
                        if (kS != null) {
                            m43489a(avx.czp(), cgp, bnT.getPosition().dfV().mo23503i(kS).mo23506j(bnU.getPosition().dfV()).dfO());
                        }
                    }
                } else if (!m43494a(bnT, bnU, avx)) {
                    m43489a(avx.czp(), cgp, (Vec3f) null);
                    m43493a(avx.czr(), cgp, (Vec3f) null, avx.czx(), bnT, bnU);
                }
            }
        }
    }

    /* renamed from: a */
    private C6909avx m43488a(Actor cr, Actor cr2, boolean z) {
        String a = m43491a(cr2, z);
        if (cr instanceof Asteroid) {
            Asteroid aeg = (Asteroid) cr;
            if (aeg.ddg().bkb() != null) {
                return aeg.ddg().bkb().mo23146cE(a);
            }
            return null;
        } else if (!(cr instanceof Ship)) {
            return null;
        } else {
            Ship fAVar = (Ship) cr;
            if (fAVar.agH().bkb() != null) {
                return fAVar.agH().bkb().mo23146cE(a);
            }
            return null;
        }
    }

    /* renamed from: a */
    private String m43491a(Actor cr, boolean z) {
        if (cr instanceof Shot) {
            return ((Shot) cr).mo17516il().cPF();
        }
        String iu = cr.mo651iu();
        if (iu == null || iu.length() <= 0) {
            iu = this.f9885kj.ala().aJe().mo19008xJ().mo21878Q(cr);
        }
        if (iu == null) {
            return null;
        }
        if (!z) {
            return iu;
        }
        String acs = this.f9885kj.ala().aJe().mo19008xJ().acs();
        if ((String.valueOf(iu) + acs) == null) {
            return "";
        }
        return acs;
    }

    /* renamed from: a */
    private void m43493a(Asset tCVar, Vec3d ajr, Vec3f vec3f, boolean z, Actor cr, Actor cr2) {
        if (tCVar != null && cr2 != null && cr != null) {
            if (z) {
                C4405e eVar = new C4405e(this, (C4405e) null);
                eVar.gEd = tCVar;
                eVar.gEc = cr2;
                eVar.bte = cr;
                if (this.f9882Rj.containsKey(eVar)) {
                    C4404d dVar = this.f9882Rj.get(eVar);
                    if (dVar.guA.asx() != null) {
                        dVar.guA.asx().setPosition(cr.mo989an(ajr));
                    }
                    dVar.cua();
                    return;
                }
                C4404d dVar2 = new C4404d(this, (C4404d) null);
                dVar2.guB = eVar;
                dVar2.guA = m43490a(tCVar, cr.mo989an(ajr).dfR(), vec3f, cr);
                this.f9882Rj.put(eVar, dVar2);
                dVar2.cua();
                return;
            }
            m43489a(tCVar, ajr, vec3f);
        }
    }

    /* renamed from: a */
    private C3257pi m43490a(Asset tCVar, Vec3d ajr, Vec3f vec3f, Actor cr) {
        if (tCVar == null) {
            return null;
        }
        C3257pi piVar = new C3257pi();
        if (vec3f == null) {
            piVar.mo21213a(tCVar, ajr, cr.cHg(), C3257pi.C3261d.NEAR_ENOUGH);
            return piVar;
        }
        piVar.mo21212a(tCVar, ajr, vec3f, cr.cHg(), C3257pi.C3261d.NEAR_ENOUGH);
        return piVar;
    }

    /* renamed from: a */
    private C3257pi m43489a(Asset tCVar, Vec3d ajr, Vec3f vec3f) {
        if (tCVar == null) {
            return null;
        }
        C3257pi piVar = new C3257pi();
        if (vec3f == null) {
            piVar.mo21209a(tCVar, ajr, C3257pi.C3261d.NEAR_ENOUGH);
            return piVar;
        }
        piVar.mo21211a(tCVar, ajr, vec3f, C3257pi.C3261d.NEAR_ENOUGH);
        return piVar;
    }

    /* renamed from: a */
    private boolean m43494a(Actor cr, Actor cr2, C6909avx avx) {
        long currentTimeMillis = System.currentTimeMillis();
        C4401a aVar = new C4401a(this, (C4401a) null);
        aVar.btg = avx;
        aVar.bte = cr;
        aVar.btf = cr2;
        if (this.f9883Rk.containsKey(aVar) && this.f9883Rk.get(aVar).longValue() + f9878Rl > currentTimeMillis) {
            return true;
        }
        this.f9883Rk.put(aVar, Long.valueOf(currentTimeMillis));
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: wl */
    public void m43499wl() {
        for (C4405e remove : this.f9882Rj.keySet()) {
            this.f9882Rj.remove(remove).guA.asy();
        }
    }

    /* renamed from: taikodom.addon.fx.CollisionFxAddon$a */
    private class C4401a {
        Actor bte;
        Actor btf;
        C6909avx btg;

        private C4401a() {
        }

        /* synthetic */ C4401a(CollisionFxAddon collisionFxAddon, C4401a aVar) {
            this();
        }

        public boolean equals(Object obj) {
            boolean z;
            boolean z2;
            if (!(obj instanceof C4401a)) {
                return super.equals(obj);
            }
            C4401a aVar = (C4401a) obj;
            if (this.bte == aVar.bte && this.btf == aVar.btf) {
                z = true;
            } else {
                z = false;
            }
            if (this.bte == aVar.btf && this.btf == aVar.bte) {
                z2 = true;
            } else {
                z2 = false;
            }
            if ((z || z2) && this.btg == aVar.btg) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (int) (this.bte.mo8317Ej() + this.btf.mo8317Ej() + this.btg.mo8317Ej());
        }
    }

    /* renamed from: taikodom.addon.fx.CollisionFxAddon$e */
    /* compiled from: a */
    private class C4405e {
        Actor bte;
        Actor gEc;
        Asset gEd;

        private C4405e() {
        }

        /* synthetic */ C4405e(CollisionFxAddon collisionFxAddon, C4405e eVar) {
            this();
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C4405e)) {
                return super.equals(obj);
            }
            C4405e eVar = (C4405e) obj;
            return this.bte == eVar.bte && this.gEc == eVar.gEc && this.gEd == eVar.gEd;
        }

        public int hashCode() {
            return (int) (this.bte.mo8317Ej() + this.gEc.mo8317Ej() + this.gEd.mo8317Ej());
        }
    }

    /* renamed from: taikodom.addon.fx.CollisionFxAddon$d */
    /* compiled from: a */
    private class C4404d extends WrapRunnable {
        C3257pi guA;
        C4405e guB;

        private C4404d() {
        }

        /* synthetic */ C4404d(CollisionFxAddon collisionFxAddon, C4404d dVar) {
            this();
        }

        public void run() {
            this.guA.asy();
            CollisionFxAddon.this.f9882Rj.remove(this.guB);
            cancel();
        }

        public void cua() {
            CollisionFxAddon.this.f9885kj.alf().mo7963a(this);
            CollisionFxAddon.this.f9885kj.alf().addTask("loopTask-" + this.guA, this, CollisionFxAddon.f9879Rm);
        }

        public String toString() {
            return this.guA + " [ " + this.guB.bte + " / " + this.guB.gEc + " ] - " + this.guB.gEd;
        }
    }

    /* renamed from: taikodom.addon.fx.CollisionFxAddon$c */
    /* compiled from: a */
    class C4403c extends C6124ags<C1042PH> {
        C4403c() {
        }

        /* renamed from: b */
        public boolean updateInfo(C1042PH ph) {
            CollisionFxAddon.this.mo23843a(ph);
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.CollisionFxAddon$b */
    /* compiled from: a */
    class C4402b extends C6124ags<C5783aaP> {

        /* renamed from: Zn */
        private static /* synthetic */ int[] f9886Zn;

        C4402b() {
        }

        /* renamed from: BK */
        static /* synthetic */ int[] m43505BK() {
            int[] iArr = f9886Zn;
            if (iArr == null) {
                iArr = new int[C5783aaP.C1841a.values().length];
                try {
                    iArr[C5783aaP.C1841a.DOCKED.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5783aaP.C1841a.FLYING.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5783aaP.C1841a.JUMPING.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5783aaP.C1841a.ONI.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                f9886Zn = iArr;
            }
            return iArr;
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            switch (m43505BK()[aap.bMO().ordinal()]) {
                case 1:
                case 3:
                    CollisionFxAddon.this.f9884Rn.cancel();
                    break;
                case 2:
                    CollisionFxAddon.this.f9885kj.alf().addTask("CollisionFXUpdater", CollisionFxAddon.this.f9884Rn, CollisionFxAddon.f9877Ri);
                    break;
            }
            CollisionFxAddon.this.m43499wl();
            return false;
        }
    }
}
