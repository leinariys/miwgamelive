package taikodom.addon.p002fx;

import game.network.message.externalizable.C3002mq;
import game.network.message.externalizable.C5966adq;
import game.script.item.EnergyWeapon;
import logic.IAddonSettings;
import logic.aaa.*;
import logic.res.sound.ISoundPlayer;
import logic.res.sound.SoundPlayer;
import logic.thred.LogPrinter;
import logic.ui.IBaseUiTegXml;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.scene.SoundObject;

import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@TaikodomAddon("taikodom.addon.fx")
/* renamed from: taikodom.addon.fx.InterfaceSFXAddon */
/* compiled from: a */
public class InterfaceSFXAddon implements C2495fo {
    private static final int iYf = 2000;
    private static final LogPrinter log = LogPrinter.setClass(InterfaceSFXAddon.class);
    /* access modifiers changed from: private */
    public final Map<SoundObject, Long> iYh = new HashMap();
    /* access modifiers changed from: private */
    public final Timer iYq = new Timer(100, new C7021azi(this));
    /* access modifiers changed from: private */
    public IBaseUiTegXml aPy;
    /* access modifiers changed from: private */
    public boolean enabled = true;
    /* access modifiers changed from: private */
    public Map<C5990aeO, C4407b> iYg = new HashMap();
    /* renamed from: kj */
    public IAddonProperties f9887kj;
    private C6124ags<C1274Sq> fgV;
    private C6124ags<C3002mq> fwc;
    private C6124ags<C3949xF> glE;
    private C6124ags<C2240dG> iYi;
    private C6124ags<C6811auD> iYj;
    private C6124ags<C0208CZ> iYk;
    private C6124ags<C0797LY> iYl;
    private C6124ags<C6831auX> iYm;
    private C6124ags<C6904avs> iYn;
    private C6124ags<C1549Wj> iYo;
    /* access modifiers changed from: private */
    private C6124ags<C5966adq> iYp;

    private void dAM() {
        this.f9887kj.aVU().mo13965a(C3949xF.class, this.glE);
        this.f9887kj.aVU().mo13965a(C0797LY.class, this.iYl);
        this.f9887kj.aVU().mo13965a(C6831auX.class, this.iYm);
        this.f9887kj.aVU().mo13965a(C6904avs.class, this.iYn);
        this.f9887kj.aVU().mo13965a(C5966adq.class, this.iYp);
        this.f9887kj.aVU().mo13965a(C1549Wj.class, this.iYo);
        this.f9887kj.aVU().mo13965a(C1274Sq.class, this.fgV);
    }

    /* renamed from: iG */
    private void m43522iG() {
        this.iYi = new C4414i();
        this.iYj = new C4413h();
        this.iYk = new C4412g();
        this.fwc = new C4411f();
        this.glE = new C4410e();
        this.iYl = new C4409d();
        this.iYm = new C4408c();
        this.iYn = new C4415j();
        this.iYp = new C4417l();
        this.iYo = new C4416k();
        this.fgV = new C4418m();
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m43518c(C5990aeO aeo) {
        m43511a(aeo, false, false);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43511a(C5990aeO aeo, boolean z, boolean z2) {
        C4406a aVar;
        if (this.enabled) {
            if (!z) {
                C4407b bVar = this.iYg.get(aeo);
                if (bVar == null || bVar.bIo() != C4406a.LOOP || !bVar.bIp().isPlaying()) {
                    SoundObject C = this.aPy.any().mo2913C(aeo.getHandle(), aeo.getFile());
                    if (C != null) {
                        if (bVar != null) {
                            bVar.bIp().stop();
                            this.iYg.remove(aeo);
                        }
                        if (z2) {
                            aVar = C4406a.CONTROLLABLE;
                        } else if (C.getLoopCount() == 0 || C.getLoopCount() > 1) {
                            aVar = C4406a.LOOP;
                        } else {
                            return;
                        }
                        this.iYg.put(aeo, new C4407b(aeo, C, aVar));
                    } else if (this.f9887kj.ale().aeg() > 0.0f) {
                        log.error("SoundPlayer failed to create: " + aeo.getFile() + " - " + aeo.getHandle());
                    }
                }
            } else {
                m43520d(aeo);
            }
        }
    }

    private void dAN() {
        this.f9887kj.aVU().mo13964a(this.glE);
        this.f9887kj.aVU().mo13964a(this.iYl);
        this.f9887kj.aVU().mo13964a(this.iYm);
        this.f9887kj.aVU().mo13964a(this.iYn);
        this.f9887kj.aVU().mo13964a(this.iYp);
        this.f9887kj.aVU().mo13964a(this.iYo);
        this.f9887kj.aVU().mo13964a(this.fgV);
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9887kj = addonPropertie;
        this.aPy = IBaseUiTegXml.initBaseUItegXML();
        this.aPy.mo13701a((ISoundPlayer) SoundPlayer.aWp());
        m43522iG();
        this.f9887kj.aVU().mo13965a(C2240dG.class, this.iYi);
        this.f9887kj.aVU().mo13965a(C6811auD.class, this.iYj);
        this.f9887kj.aVU().mo13965a(C0208CZ.class, this.iYk);
        this.f9887kj.aVU().mo13965a(C3002mq.class, this.fwc);
        dAM();
    }

    public void stop() {
        this.f9887kj.aVU().mo13964a(this.iYi);
        this.f9887kj.aVU().mo13964a(this.iYj);
        this.f9887kj.aVU().mo13964a(this.iYk);
        this.f9887kj.aVU().mo13964a(this.fwc);
        dAN();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m43520d(C5990aeO aeo) {
        C4407b bVar = this.iYg.get(aeo);
        if (bVar != null) {
            this.iYg.remove(bVar);
            SoundObject bIp = bVar.bIp();
            if (bIp.isPlaying() || bIp.isDisposed()) {
                bIp.stop();
            } else if (bIp.getSource() == null) {
                this.iYh.put(bIp, Long.valueOf(System.currentTimeMillis()));
                this.iYq.restart();
            }
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$a */
    private enum C4406a {
        CONTROLLABLE,
        LOOP,
        VOICE
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$b */
    /* compiled from: a */
    private class C4407b {
        private final C5990aeO cDc;
        private final C4406a eNt;
        private final SoundObject eNu;

        public C4407b(C5990aeO aeo, SoundObject soundObject, C4406a aVar) {
            this.cDc = aeo;
            this.eNu = soundObject;
            this.eNt = aVar;
        }

        public C5990aeO bIn() {
            return this.cDc;
        }

        public C4406a bIo() {
            return this.eNt;
        }

        public SoundObject bIp() {
            return this.eNu;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$i */
    /* compiled from: a */
    class C4414i extends C6124ags<C2240dG> {
        C4414i() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2240dG dGVar) {
            InterfaceSFXAddon.this.aPy.any().mo2911A(dGVar.getFile(), dGVar.getHandle());
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$h */
    /* compiled from: a */
    class C4413h extends C6124ags<C6811auD> {
        C4413h() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6811auD aud) {
            if (aud.aDJ()) {
                InterfaceSFXAddon.this.m43520d(aud.aDI());
            } else {
                InterfaceSFXAddon.this.m43511a(aud.aDI(), false, aud.cxL());
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$g */
    /* compiled from: a */
    class C4412g extends C6124ags<C0208CZ> {
        C4412g() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0208CZ cz) {
            if (cz.aDJ()) {
                InterfaceSFXAddon.this.m43520d(cz.aDI());
            } else {
                ArrayList<C4407b> arrayList = new ArrayList<>();
                for (C4407b bVar : InterfaceSFXAddon.this.iYg.values()) {
                    if (bVar.bIo() == C4406a.VOICE) {
                        arrayList.add(bVar);
                    }
                }
                for (C4407b bIn : arrayList) {
                    InterfaceSFXAddon.this.m43520d(bIn.bIn());
                }
                InterfaceSFXAddon.this.m43511a(cz.aDI(), false, true);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$f */
    /* compiled from: a */
    class C4411f extends C6124ags<C3002mq> {
        C4411f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3002mq mqVar) {
            switch (mqVar.aiH()) {
                case 0:
                    InterfaceSFXAddon.this.enabled = false;
                    break;
                case 1:
                    InterfaceSFXAddon.this.enabled = true;
                    break;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$e */
    /* compiled from: a */
    class C4410e extends C6124ags<C3949xF> {
        C4410e() {
        }

        /* renamed from: b */
        public boolean updateInfo(C3949xF xFVar) {
            InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.FLY_MODE_CHANGE);
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$d */
    /* compiled from: a */
    class C4409d extends C6124ags<C0797LY> {
        C4409d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0797LY ly) {
            if (ly.afR() instanceof EnergyWeapon) {
                InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.WEAPON_EMPTY_ENERGY);
                return false;
            }
            InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.WEAPON_EMPTY_PROJECTILE_LAUNCHER);
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$c */
    /* compiled from: a */
    class C4408c extends C6124ags<C6831auX> {
        C4408c() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6831auX aux) {
            InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.WEAPON_EMPTY_PROJECTILE_LAUNCHER);
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$j */
    /* compiled from: a */
    class C4415j extends C6124ags<C6904avs> {
        C4415j() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6904avs avs) {
            InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.CHAR_LEVEL_UP);
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$l */
    /* compiled from: a */
    class C4417l extends C6124ags<C5966adq> {
        C4417l() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5966adq adq) {
            InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.EQUIP_REAMMO);
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$k */
    /* compiled from: a */
    class C4416k extends C6124ags<C1549Wj> {
        C4416k() {
        }

        /* renamed from: c */
        public boolean updateInfo(C1549Wj wj) {
            if (InterfaceSFXAddon.this.f9887kj.getPlayer() == null || aMU.MISSION_1_TAKEN.compareTo(InterfaceSFXAddon.this.f9887kj.getPlayer().cXm()) >= 0) {
                return false;
            }
            InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.CHAR_REWARD_GAINED);
            return false;
        }
    }

    /* renamed from: taikodom.addon.fx.InterfaceSFXAddon$m */
    /* compiled from: a */
    class C4418m extends C6124ags<C1274Sq> {
        C4418m() {
        }

        /* renamed from: b */
        public boolean updateInfo(C1274Sq sq) {
            if (sq.fMR != C1274Sq.C1275a.ENTER_GATE) {
                return false;
            }
            InterfaceSFXAddon.this.m43518c((C5990aeO) C3667tV.GATE_ENTER);
            return false;
        }
    }
}
