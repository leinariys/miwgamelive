package taikodom.addon;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.awK  reason: case insensitive filesystem */
/* compiled from: a */
public @interface TaikodomAddon {
    String value();
}
