package taikodom.addon.contract;

import game.network.message.externalizable.C0939Nn;
import game.script.contract.types.ContractTemplate;
import logic.ui.C2698il;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.contract")
/* renamed from: a.PJ */
/* compiled from: a */
public class C1044PJ extends C3704uA {
    /* renamed from: kj */
    public IAddonProperties f1361kj;
    /* access modifiers changed from: private */
    private JButton dSd;

    public C1044PJ(IAddonProperties vWVar, C2698il ilVar, ContractAddon contractAddon) {
        super(vWVar, ilVar, contractAddon);
        this.f1361kj = vWVar;
        this.dSd = ilVar.mo4913cb("take-button");
        this.dSd.addActionListener(new C1045a());
        this.dSd.setEnabled(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String mo4652a(ContractTemplate bj) {
        return String.valueOf(this.f1361kj.translate("Issued by")) + ": " + bj.aAw().getName();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo4653b(ContractTemplate bj) {
        return !bj.aAw().equals(this.f1361kj.getPlayer()) && bj.aAs() == ContractTemplate.C0104a.AVAILABLE;
    }

    /* renamed from: c */
    public void mo4654c(ContractTemplate bj) {
        if (bj != null) {
            this.dSd.setEnabled(true);
        } else {
            this.dSd.setEnabled(false);
        }
    }

    /* renamed from: a.PJ$a */
    class C1045a implements ActionListener {
        C1045a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C1044PJ.this.dav() != null) {
                if (!C1044PJ.this.f1361kj.ala().aLO().mo19177g(C1044PJ.this.dav())) {
                    C1044PJ.this.mo22292c(C0939Nn.C0940a.ERROR, C1044PJ.this.f1361kj.translate("Contract cannot be taken."));
                }
                C1044PJ.this.refresh();
            }
        }
    }
}
