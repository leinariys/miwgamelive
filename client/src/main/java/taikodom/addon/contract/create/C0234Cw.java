package taikodom.addon.contract.create;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C5820abA;
import game.network.message.serializable.C2774jo;
import game.network.message.serializable.C5553aNt;
import game.script.Actor;
import game.script.contract.types.ContractBaseInfo;
import game.script.item.BagItem;
import game.script.item.BagItemType;
import game.script.item.Item;
import game.script.ship.Outpost;
import game.script.ship.Station;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.aaa.C0945Ns;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.item.VBox;
import p001a.C2916lm;
import p001a.C3105ns;
import p001a.C3428rT;
import p001a.C3987xk;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

@TaikodomAddon("taikodom.addon.contract.create")
/* renamed from: a.Cw */
/* compiled from: a */
public class C0234Cw implements C2916lm {
    private static final String gFJ = "transportCreate.xml";
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f376IU = C3987xk.bFK;
    /* access modifiers changed from: private */
    public C2774jo ipC = new C2774jo();
    /* access modifiers changed from: private */
    public JComboBox ipD;
    /* access modifiers changed from: private */
    public JComboBox ipE;
    /* access modifiers changed from: private */
    public JLabel ipF;
    /* access modifiers changed from: private */
    public C0950Nu ipH;
    /* renamed from: kj */
    public IAddonProperties f377kj;
    private C0135Bg gFL;
    private C2698il gFM;
    private JButton ipG;
    /* access modifiers changed from: private */
    private C3428rT<C0945Ns> ipI;

    public C0234Cw(IAddonProperties vWVar, VBox vAVar, C0135Bg bg) {
        this.f377kj = vWVar;
        this.gFL = bg;
        this.gFM = (C2698il) this.f377kj.bHv().mo16779a(getClass(), gFJ, (C2698il) vAVar);
        this.ipF = this.gFM.mo4917cf("container-volume");
        this.ipF.setText(String.valueOf(this.f377kj.mo11834a((Class<?>) C0234Cw.class, "Volume", new Object[0])) + ": " + this.f376IU.mo22980dY(0.0f));
        BagItemType uM = this.f377kj.ala().aLO().cHY().mo19037uM();
        this.gFM.mo4917cf("container-name").setText(String.valueOf(this.f377kj.mo11834a((Class<?>) C0234Cw.class, "Item", new Object[0])) + ": " + uM.mo19891ke().get());
        this.gFM.mo4915cd("transport-container").mo16824a(C5378aHa.ITEMS, uM.anN().getHandle());
        this.ipD = this.gFM.mo4914cc("from-comboBox");
        this.ipD.setRenderer(new C3105ns());
        HashSet<Station> hashSet = new HashSet<>();
        for (C5820abA next : this.f377kj.getPlayer().dxW()) {
            if (!(next.mo12359XH() instanceof BagItem)) {
                hashSet.add(next.mo12361eT());
            }
        }
        this.ipD.setModel(new DefaultComboBoxModel());
        this.ipD.getModel().addElement(this.f377kj.mo11834a((Class<?>) C0234Cw.class, "select a station", new Object[0]));
        for (Station addElement : hashSet) {
            this.ipD.getModel().addElement(addElement);
        }
        this.ipD.addActionListener(new C0241e());
        this.ipD.setSelectedIndex(0);
        this.ipE = this.gFM.mo4914cc("to-comboBox");
        this.ipE.setRenderer(new C3105ns());
        this.ipE.setModel(new DefaultComboBoxModel());
        this.ipE.getModel().addElement(this.f377kj.mo11834a((Class<?>) C0234Cw.class, "select a station", new Object[0]));
        for (Station addElement2 : djw()) {
            this.ipE.getModel().addElement(addElement2);
        }
        ArrayList<Station> arrayList = new ArrayList<>();
        for (Station add : djw()) {
            arrayList.add(add);
        }
        Collections.sort(arrayList, new C0239c());
        for (Station addElement3 : arrayList) {
            this.ipE.getModel().addElement(addElement3);
        }
        this.ipE.addActionListener(new C0240d());
        this.ipE.setSelectedIndex(0);
        this.ipG = this.gFM.mo4913cb("container-insert");
        this.ipG.addActionListener(new C0235a());
        this.ipI = new C0238b();
        this.f377kj.aVU().mo13965a(C0945Ns.class, this.ipI);
        this.gFM.pack();
    }

    /* renamed from: LH */
    public C5553aNt mo1186LH() {
        return this.ipC;
    }

    /* renamed from: LI */
    public ContractBaseInfo mo1187LI() {
        return this.f377kj.ala().aLO().cHY();
    }

    private Set<Station> djw() {
        HashSet hashSet = new HashSet();
        for (StellarSystem nodes : this.f377kj.ala().aKa()) {
            for (Node Zw : nodes.getNodes()) {
                for (Actor next : Zw.mo21624Zw()) {
                    if ((next instanceof Station) && !(next instanceof Outpost) && !next.getName().contains("Core")) {
                        hashSet.add((Station) next);
                    }
                }
            }
        }
        return hashSet;
    }

    /* renamed from: LJ */
    public boolean mo1188LJ() {
        if (this.ipC.mo3130Fl() == null) {
            this.gFL.mo769c(C0939Nn.C0940a.ERROR, this.f377kj.mo11834a((Class<?>) C0234Cw.class, "Select station where transported items come from.", new Object[0]));
            return false;
        } else if (this.ipC.mo3131Fm() == null) {
            this.gFL.mo769c(C0939Nn.C0940a.ERROR, this.f377kj.mo11834a((Class<?>) C0234Cw.class, "Select station where items must be transported to.", new Object[0]));
            return false;
        } else if (this.ipC.mo3132Fo().aRz().size() <= 0) {
            this.gFL.mo769c(C0939Nn.C0940a.ERROR, this.f377kj.mo11834a((Class<?>) C0234Cw.class, "Select at least one item to be carried.", new Object[0]));
            return false;
        } else {
            for (Item contains : this.ipC.mo3132Fo().aRz()) {
                if (this.ipC.aAT().aRz().contains(contains)) {
                    this.gFL.mo769c(C0939Nn.C0940a.ERROR, this.f377kj.mo11834a((Class<?>) C0234Cw.class, "Transported item cannot be part of the reward.", new Object[0]));
                    return false;
                }
            }
            if (this.ipC.mo3130Fl() != this.ipC.mo3131Fm()) {
                return true;
            }
            this.gFL.mo769c(C0939Nn.C0940a.ERROR, this.f377kj.mo11834a((Class<?>) C0234Cw.class, "To and from stations must be different.", new Object[0]));
            return false;
        }
    }

    public void destroy() {
        if (this.ipH != null) {
            this.ipH.destroy();
            this.ipH = null;
        }
        if (this.gFM != null) {
            this.gFM.destroy();
            this.gFM = null;
        }
    }

    /* renamed from: a.Cw$e */
    /* compiled from: a */
    class C0241e implements ActionListener {
        C0241e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C0234Cw.this.ipD.getSelectedItem() instanceof Station) {
                Station bf = (Station) C0234Cw.this.ipD.getSelectedItem();
                if (C0234Cw.this.ipC.mo3130Fl() != bf) {
                    C0234Cw.this.ipC.mo3137e(bf);
                    C0234Cw.this.ipC.mo3132Fo().removeAll();
                    return;
                }
                return;
            }
            C0234Cw.this.ipC.mo3137e((Station) null);
            C0234Cw.this.ipC.mo3132Fo().removeAll();
        }
    }

    /* renamed from: a.Cw$c */
    /* compiled from: a */
    class C0239c implements Comparator<Station> {
        C0239c() {
        }

        /* renamed from: b */
        public int compare(Station bf, Station bf2) {
            return C3105ns.m36510a(bf, bf2);
        }
    }

    /* renamed from: a.Cw$d */
    /* compiled from: a */
    class C0240d implements ActionListener {
        C0240d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C0234Cw.this.ipE.getSelectedItem() instanceof Station) {
                C0234Cw.this.ipC.mo3138f((Station) C0234Cw.this.ipE.getSelectedItem());
            }
        }
    }

    /* renamed from: a.Cw$a */
    class C0235a implements ActionListener {
        C0235a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Station bf = null;
            boolean z = false;
            if (C0234Cw.this.ipH != null) {
                z = C0234Cw.this.ipH.isOpen();
                C0234Cw.this.ipH.destroy();
                C0234Cw.this.ipH = null;
            }
            if (!z) {
                if (C0234Cw.this.ipD.getSelectedItem() instanceof Station) {
                    bf = (Station) C0234Cw.this.ipD.getSelectedItem();
                }
                if (C0234Cw.this.ipC == null || C0234Cw.this.ipC.mo3132Fo().aRz().size() == 0) {
                    C0234Cw.this.ipH = new C0236a(C0234Cw.this.f377kj, bf);
                } else {
                    C0234Cw.this.ipH = new C0237b(C0234Cw.this.f377kj, bf, C0234Cw.this.ipC.mo3132Fo().aRz());
                }
            }
        }

        /* renamed from: a.Cw$a$a */
        class C0236a extends C0950Nu {
            C0236a(IAddonProperties vWVar, Station bf) {
                super(vWVar, bf);
            }

            public void destroy() {
                super.destroy();
                C0234Cw.this.ipH = null;
            }
        }

        /* renamed from: a.Cw$a$b */
        /* compiled from: a */
        class C0237b extends C0950Nu {
            C0237b(IAddonProperties vWVar, Station bf, Set set) {
                super(vWVar, bf, set);
            }

            public void destroy() {
                super.destroy();
                C0234Cw.this.ipH = null;
            }
        }
    }

    /* renamed from: a.Cw$b */
    /* compiled from: a */
    class C0238b extends C3428rT<C0945Ns> {
        C0238b() {
        }

        /* renamed from: a */
        public void mo321b(C0945Ns ns) {
            if (ns.dzx() == C0945Ns.C0946a.TRANSPORT) {
                C0234Cw.this.ipC.mo3137e(ns.mo4275Fl());
                C0234Cw.this.ipD.setSelectedItem(ns.mo4275Fl());
                C0234Cw.this.ipC.mo3132Fo().removeAll();
                float f = 0.0f;
                Iterator<Item> it = ns.dzw().iterator();
                while (true) {
                    float f2 = f;
                    if (!it.hasNext()) {
                        C0234Cw.this.ipF.setText(String.valueOf(C0234Cw.this.f377kj.mo11834a((Class<?>) C0234Cw.class, "Volume", new Object[0])) + ": " + C0234Cw.this.f376IU.mo22980dY(f2));
                        return;
                    }
                    Item next = it.next();
                    C0234Cw.this.ipC.mo3132Fo().mo2693s(next);
                    f = next.getVolume() + f2;
                }
            }
        }
    }
}
