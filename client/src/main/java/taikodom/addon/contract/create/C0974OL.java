package taikodom.addon.contract.create;

import game.script.item.Item;
import game.script.ship.Station;
import logic.aaa.C0945Ns;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.util.ArrayList;
import java.util.Set;

@TaikodomAddon("taikodom.addon.contract.create")
/* renamed from: a.OL */
/* compiled from: a */
public class C0974OL extends C6190aiG {

    /* renamed from: iH */
    private Station f1293iH;

    public C0974OL(IAddonProperties vWVar, Station bf) {
        super(vWVar, bf);
    }

    public C0974OL(IAddonProperties vWVar, Station bf, Set<Item> set) {
        super(vWVar, bf, set);
    }

    /* access modifiers changed from: protected */
    /* renamed from: O */
    public void mo4282O(Station bf) {
        this.f1293iH = bf;
        this.gbJ.setVisible(false);
        this.gbJ.setEnabled(false);
        this.gbJ.getParent().remove(this.gbJ);
        this.gbI.setText(String.valueOf(bf.azW().mo21665ke().get()) + " - " + bf.getName());
    }

    /* access modifiers changed from: protected */
    /* renamed from: eT */
    public Station mo4284eT() {
        return this.f1293iH;
    }

    /* access modifiers changed from: protected */
    public void bmD() {
        ArrayList arrayList = new ArrayList();
        for (Item add : cjX()) {
            arrayList.add(add);
        }
        this.f4599kj.aVU().mo13975h(new C0945Ns(C0945Ns.C0946a.REWARD, arrayList, this.f1293iH));
        destroy();
    }
}
