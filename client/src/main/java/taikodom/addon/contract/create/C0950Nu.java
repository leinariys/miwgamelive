package taikodom.addon.contract.create;

import game.network.message.externalizable.C5820abA;
import game.script.item.BagItem;
import game.script.item.Item;
import game.script.ship.Station;
import logic.aaa.C0945Ns;
import p001a.C3105ns;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;

@TaikodomAddon("taikodom.addon.contract.create")
/* renamed from: a.Nu */
/* compiled from: a */
public class C0950Nu extends C6190aiG {
    /* access modifiers changed from: private */

    /* renamed from: iH */
    public Station f1258iH;

    public C0950Nu(IAddonProperties vWVar, Station bf) {
        super(vWVar, bf);
    }

    public C0950Nu(IAddonProperties vWVar, Station bf, Set<Item> set) {
        super(vWVar, bf, set);
    }

    /* access modifiers changed from: protected */
    /* renamed from: O */
    public void mo4282O(Station bf) {
        this.gbI.setVisible(false);
        this.gbI.setEnabled(false);
        this.gbI.getParent().remove(this.gbI);
        this.gbJ.setRenderer(new C3105ns());
        ArrayList<Station> arrayList = new ArrayList<>();
        for (C5820abA next : this.f4599kj.getPlayer().dxW()) {
            if (!(next.mo12359XH() instanceof BagItem) && !arrayList.contains(next.mo12361eT())) {
                arrayList.add(next.mo12361eT());
            }
        }
        Collections.sort(arrayList, new C0951a());
        this.gbJ.setModel(new DefaultComboBoxModel());
        this.gbJ.getModel().addElement(this.f4599kj.mo11834a((Class<?>) C0234Cw.class, "select a station", new Object[0]));
        for (Station addElement : arrayList) {
            this.gbJ.getModel().addElement(addElement);
        }
        this.gbJ.addActionListener(new C0952b());
        this.f1258iH = bf;
        if (bf != null) {
            this.gbJ.setSelectedItem(bf);
        } else {
            this.gbJ.setSelectedIndex(0);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: eT */
    public Station mo4284eT() {
        return this.f1258iH;
    }

    /* access modifiers changed from: protected */
    public void bmD() {
        ArrayList arrayList = new ArrayList();
        for (Item add : cjX()) {
            arrayList.add(add);
        }
        this.f4599kj.aVU().mo13975h(new C0945Ns(C0945Ns.C0946a.TRANSPORT, arrayList, this.f1258iH));
        destroy();
    }

    /* renamed from: a.Nu$a */
    class C0951a implements Comparator<Station> {
        C0951a() {
        }

        /* renamed from: b */
        public int compare(Station bf, Station bf2) {
            return C3105ns.m36510a(bf, bf2);
        }
    }

    /* renamed from: a.Nu$b */
    /* compiled from: a */
    class C0952b implements ActionListener {
        C0952b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C0950Nu.this.gbJ.getSelectedItem() instanceof Station) {
                C0950Nu.this.f1258iH = (Station) C0950Nu.this.gbJ.getSelectedItem();
            } else {
                C0950Nu.this.f1258iH = null;
            }
            C0950Nu.this.cjW();
        }
    }
}
