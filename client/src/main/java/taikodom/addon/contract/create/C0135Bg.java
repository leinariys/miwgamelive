package taikodom.addon.contract.create;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.serializable.C6158aha;
import game.script.contract.ContractBoard;
import game.script.item.Item;
import game.script.ship.Station;
import logic.aaa.C0945Ns;
import logic.ui.item.InternalFrame;
import p001a.C2916lm;
import p001a.C3428rT;
import p001a.C3987xk;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.Vector;

@TaikodomAddon("taikodom.addon.contract.create")
/* renamed from: a.Bg */
/* compiled from: a */
public class C0135Bg {
    private static final String iLW = "contractCreate.xml";
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f245IU;
    /* access modifiers changed from: private */
    public C2916lm iMa;
    /* access modifiers changed from: private */
    public C6158aha iMb;
    /* access modifiers changed from: private */
    public JButton iMc;
    /* access modifiers changed from: private */
    public JButton iMd;
    /* access modifiers changed from: private */
    public JLabel iMh;
    /* access modifiers changed from: private */
    public C0974OL iMi;
    /* renamed from: kj */
    IAddonProperties f246kj;
    /* renamed from: nM */
    public InternalFrame f247nM = this.f246kj.bHv().mo16792bN(iLW);
    private JButton aoP;
    private C3428rT<C3689to> dHH = new C0144i();
    private JButton eWJ;
    private JComboBox iLX;
    private JLabel iLY;
    private JLabel iLZ;
    private JButton iMe;
    private JSpinner iMf;
    private JSpinner iMg;
    /* access modifiers changed from: private */
    private C3428rT<C0945Ns> iMj;

    public C0135Bg(IAddonProperties vWVar) {
        this.f246kj = vWVar;
        this.f246kj.aVU().mo13965a(C3689to.class, this.dHH);
        this.f245IU = C3987xk.bFK;
        this.iLY = this.f247nM.mo4917cf("contract-type-name");
        this.iLZ = this.f247nM.mo4917cf("contract-type-cost");
        this.iMh = this.f247nM.mo4917cf("reward-volume");
        this.iMh.setText(this.f245IU.format(0));
        this.iMc = this.f247nM.mo4913cb("contract-type-transport");
        this.iMc.addActionListener(new C0143h());
        this.iMc.setToolTipText(this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Transport", new Object[0]));
        this.iMd = this.f247nM.mo4913cb("contract-type-bounty");
        this.iMd.addActionListener(new C0142g());
        this.iMd.setToolTipText(this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Bounty Hunt", new Object[0]));
        this.iLX = this.f247nM.mo4914cc("contract-duration");
        this.iLX.setRenderer(new C0136a());
        Vector vector = new Vector();
        vector.ensureCapacity(C0145j.values().length);
        for (C0145j jVar : C0145j.values()) {
            if (jVar != C0145j.DEBUG || this.f246kj.ala().aLS().adm()) {
                vector.add(jVar);
            }
        }
        this.iLX.setModel(new DefaultComboBoxModel(vector));
        this.iLX.getModel().setSelectedItem(this.iLX.getModel().getElementAt(0));
        this.iMf = this.f247nM.mo4915cd("reward-edit");
        this.iMg = this.f247nM.mo4915cd("safeguard-edit");
        this.iMe = this.f247nM.mo4913cb("reward-add-button");
        this.iMe.addActionListener(new C0140e());
        this.aoP = this.f247nM.mo4913cb("cancel-button");
        this.aoP.addActionListener(new C0141f());
        this.eWJ = this.f247nM.mo4913cb("create-button");
        this.eWJ.addActionListener(new C0138c());
        this.iMj = new C0139d();
        this.f246kj.aVU().mo13965a(C0945Ns.class, this.iMj);
        this.f247nM.addInternalFrameListener(new C0137b());
        this.f246kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, this));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m1276b(JButton jButton) {
        if (jButton == this.iMc) {
            this.iMc.setSelected(true);
        } else {
            this.iMc.setSelected(false);
        }
        if (jButton == this.iMd) {
            this.iMd.setSelected(true);
        } else {
            this.iMd.setSelected(false);
        }
    }

    /* access modifiers changed from: private */
    public void refresh() {
        if (this.iMa != null) {
            this.iLY.setText(this.iMa.mo1187LI().mo22205rP().get());
            this.iLZ.setText(this.f245IU.format(this.iMa.mo1187LI().aeH()));
        }
        this.f247nM.doLayout();
    }

    /* renamed from: c */
    public void mo769c(C0939Nn.C0940a aVar, String str) {
        ((MessageDialogAddon) this.f246kj.mo11830U(MessageDialogAddon.class)).mo24306a(aVar, str, this.f246kj.translate("Ok"));
    }

    /* renamed from: LJ */
    public boolean mo768LJ() {
        if (this.iMa == null) {
            mo769c(C0939Nn.C0940a.ERROR, this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Select a contract type first.", new Object[0]));
            return false;
        }
        if (this.iMb == null) {
            this.iMb = new C6158aha();
        }
        this.iMb.mo4437fq(((Number) this.iMf.getValue()).longValue());
        this.iMa.mo1186LH().mo701b(this.iMb);
        this.iMa.mo1186LH().setTimeToLive(((C0145j) this.iLX.getSelectedItem()).getDuration());
        this.iMa.mo1186LH().mo704eg(((Number) this.iMg.getValue()).longValue());
        return this.iMa.mo1188LJ();
    }

    public void destroy() {
        if (this.iMa != null) {
            this.iMa.destroy();
        }
        if (this.iMi != null) {
            this.iMi.destroy();
        }
        this.f246kj.aVU().mo13964a(this.dHH);
        this.f247nM.dispose();
    }

    /* renamed from: a.Bg$j */
    /* compiled from: a */
    private enum C0145j {
        DEBUG(5),
        THREE_HOURS(180),
        SIX_HOURS(360),
        TWELVE_HOURS(720),
        ONE_DAY(1440),
        TWO_DAYS(2880),
        TEN_DAYS(14400);

        private long duration;

        private C0145j(long j) {
            this.duration = 60 * j * 1000;
        }

        public long getDuration() {
            return this.duration;
        }
    }

    /* renamed from: a.Bg$a */
    public class C0136a extends JLabel implements ListCellRenderer {

        private static /* synthetic */ int[] ckT = null;

        public C0136a() {
            setOpaque(true);
            setHorizontalAlignment(2);
            setVerticalAlignment(0);
        }

        static /* synthetic */ int[] ayn() {
            int[] iArr = ckT;
            if (iArr == null) {
                iArr = new int[C0145j.values().length];
                try {
                    iArr[C0145j.DEBUG.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C0145j.ONE_DAY.ordinal()] = 5;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C0145j.SIX_HOURS.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C0145j.TEN_DAYS.ordinal()] = 7;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C0145j.THREE_HOURS.ordinal()] = 2;
                } catch (NoSuchFieldError e5) {
                }
                try {
                    iArr[C0145j.TWELVE_HOURS.ordinal()] = 4;
                } catch (NoSuchFieldError e6) {
                }
                try {
                    iArr[C0145j.TWO_DAYS.ordinal()] = 6;
                } catch (NoSuchFieldError e7) {
                }
                ckT = iArr;
            }
            return iArr;
        }

        public Component getListCellRendererComponent(JList jList, Object obj, int i, boolean z, boolean z2) {
            if (obj instanceof C0145j) {
                setText(m1286a((C0145j) obj));
            }
            return this;
        }

        /* renamed from: a */
        private String m1286a(C0145j jVar) {
            switch (ayn()[jVar.ordinal()]) {
                case 1:
                    return "5 " + C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "minutes", new Object[0]);
                case 2:
                    return "3 " + C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "hours", new Object[0]);
                case 3:
                    return "6 " + C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "hours", new Object[0]);
                case 4:
                    return "12 " + C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "hours", new Object[0]);
                case 5:
                    return "1 " + C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "day", new Object[0]);
                case 6:
                    return "2 " + C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "days", new Object[0]);
                case 7:
                    return "10 " + C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "days", new Object[0]);
                default:
                    return null;
            }
        }
    }

    /* renamed from: a.Bg$i */
    /* compiled from: a */
    class C0144i extends C3428rT<C3689to> {
        C0144i() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C0135Bg.this.f247nM != null && C0135Bg.this.f247nM.isVisible()) {
                C0135Bg.this.destroy();
                toVar.adC();
            }
        }
    }

    /* renamed from: a.Bg$h */
    /* compiled from: a */
    class C0143h implements ActionListener {
        C0143h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!(C0135Bg.this.iMa instanceof C0234Cw)) {
                if (C0135Bg.this.iMa != null) {
                    C0135Bg.this.iMa.destroy();
                }
                C0135Bg.this.iMa = new C0234Cw(C0135Bg.this.f246kj, C0135Bg.this.f247nM.mo4915cd("specific-box"), C0135Bg.this);
                C0135Bg.this.m1276b(C0135Bg.this.iMc);
                C0135Bg.this.refresh();
            }
        }
    }

    /* renamed from: a.Bg$g */
    /* compiled from: a */
    class C0142g implements ActionListener {
        C0142g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!(C0135Bg.this.iMa instanceof C1821aR)) {
                if (C0135Bg.this.iMa != null) {
                    C0135Bg.this.iMa.destroy();
                }
                C0135Bg.this.iMa = new C1821aR(C0135Bg.this.f246kj, C0135Bg.this.f247nM.mo4915cd("specific-box"), C0135Bg.this);
                C0135Bg.this.m1276b(C0135Bg.this.iMd);
                C0135Bg.this.refresh();
            }
        }
    }

    /* renamed from: a.Bg$e */
    /* compiled from: a */
    class C0140e implements ActionListener {
        C0140e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            boolean z = false;
            if (C0135Bg.this.iMi != null) {
                z = C0135Bg.this.iMi.isOpen();
                C0135Bg.this.iMi.destroy();
                C0135Bg.this.iMi = null;
            }
            if (z || !(C0135Bg.this.f246kj.getPlayer().bhE() instanceof Station)) {
                return;
            }
            if (C0135Bg.this.iMb == null || C0135Bg.this.iMb.aRz().size() == 0) {
                C0135Bg.this.iMi = new C0974OL(C0135Bg.this.f246kj, (Station) C0135Bg.this.f246kj.getPlayer().bhE());
            } else {
                C0135Bg.this.iMi = new C0974OL(C0135Bg.this.f246kj, (Station) C0135Bg.this.f246kj.getPlayer().bhE(), C0135Bg.this.iMb.aRz());
            }
        }
    }

    /* renamed from: a.Bg$f */
    /* compiled from: a */
    class C0141f implements ActionListener {
        C0141f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C0135Bg.this.destroy();
        }
    }

    /* renamed from: a.Bg$c */
    /* compiled from: a */
    class C0138c implements ActionListener {
        private static /* synthetic */ int[] cWX;

        C0138c() {
        }

        static /* synthetic */ int[] aQr() {
            int[] iArr = cWX;
            if (iArr == null) {
                iArr = new int[ContractBoard.C2580d.values().length];
                try {
                    iArr[ContractBoard.C2580d.BAD_BOUNTY_PREY.ordinal()] = 4;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[ContractBoard.C2580d.INCONSISTENT_DATA.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[ContractBoard.C2580d.INSUFFICIENT_MONEY.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[ContractBoard.C2580d.TRANSPORT_INCONSISTENT_BAG.ordinal()] = 1;
                } catch (NoSuchFieldError e4) {
                }
                cWX = iArr;
            }
            return iArr;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C0135Bg.this.mo768LJ()) {
                try {
                    C0135Bg.this.f246kj.ala().aLO().mo19170b(C0135Bg.this.iMa.mo1186LH());
                    C0135Bg.this.mo769c(C0939Nn.C0940a.INFO, C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Contract created.", new Object[0]));
                    C0135Bg.this.destroy();
                } catch (ContractBoard.C2579c e) {
                    switch (aQr()[e.dfd().ordinal()]) {
                        case 1:
                            C0135Bg.this.mo769c(C0939Nn.C0940a.ERROR, C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Inconsistent data in bag", new Object[0]));
                            return;
                        case 2:
                            C0135Bg.this.mo769c(C0939Nn.C0940a.ERROR, C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Inconsistent data", new Object[0]));
                            return;
                        case 3:
                            C0135Bg.this.mo769c(C0939Nn.C0940a.ERROR, C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Insufficient money", new Object[0]));
                            return;
                        case 4:
                            C0135Bg.this.mo769c(C0939Nn.C0940a.ERROR, C0135Bg.this.f246kj.mo11834a((Class<?>) C0135Bg.class, "Invalid prey for bounty", new Object[0]));
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }

    /* renamed from: a.Bg$d */
    /* compiled from: a */
    class C0139d extends C3428rT<C0945Ns> {
        C0139d() {
        }

        /* renamed from: a */
        public void mo321b(C0945Ns ns) {
            if (ns.dzx() == C0945Ns.C0946a.REWARD) {
                if (C0135Bg.this.iMb == null) {
                    C0135Bg.this.iMb = new C6158aha();
                }
                C0135Bg.this.iMb.removeAll();
                float f = 0.0f;
                Iterator<Item> it = ns.dzw().iterator();
                while (true) {
                    float f2 = f;
                    if (!it.hasNext()) {
                        C0135Bg.this.iMh.setText(C0135Bg.this.f245IU.mo22980dY(f2));
                        return;
                    }
                    Item next = it.next();
                    C0135Bg.this.iMb.mo2693s(next);
                    f = next.getVolume() + f2;
                }
            }
        }
    }

    /* renamed from: a.Bg$b */
    /* compiled from: a */
    class C0137b extends InternalFrameAdapter {
        C0137b() {
        }

        public void internalFrameClosing(InternalFrameEvent internalFrameEvent) {
            C0135Bg.this.destroy();
        }
    }
}
