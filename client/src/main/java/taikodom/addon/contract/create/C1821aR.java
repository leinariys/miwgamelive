package taikodom.addon.contract.create;

import game.network.message.externalizable.C0939Nn;
import game.network.message.serializable.C5553aNt;
import game.network.message.serializable.aVU;
import game.script.contract.types.ContractBaseInfo;
import game.script.player.Player;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.item.Picture;
import logic.ui.item.VBox;
import p001a.C2916lm;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.contract.create")
/* renamed from: a.aR */
/* compiled from: a */
public class C1821aR implements C2916lm {
    private static final String gFJ = "bountyCreate.xml";
    /* access modifiers changed from: private */
    public aVU gFK = new aVU();
    /* access modifiers changed from: private */
    public C0135Bg gFL;
    /* access modifiers changed from: private */
    public C2698il gFM;
    /* access modifiers changed from: private */
    public JTextField gFN;
    /* access modifiers changed from: private */
    public JLabel gFO;
    /* access modifiers changed from: private */
    public JLabel gFP;
    /* access modifiers changed from: private */
    public Picture gFQ;
    /* renamed from: kj */
    public IAddonProperties f3679kj;
    /* access modifiers changed from: private */
    private JButton gFR;

    public C1821aR(IAddonProperties vWVar, VBox vAVar, C0135Bg bg) {
        this.f3679kj = vWVar;
        this.gFL = bg;
        this.gFM = (C2698il) this.f3679kj.bHv().mo16779a(getClass(), gFJ, (C2698il) vAVar);
        this.gFO = this.gFM.mo4917cf("prey-name");
        this.gFP = this.gFM.mo4917cf("corp-name");
        this.gFQ = this.gFM.mo4915cd("corp-icon");
        this.gFN = this.gFM.mo4920ci("prey-name-edit");
        this.gFR = this.gFM.mo4913cb("check");
        this.gFR.addActionListener(new C1822a());
        this.gFM.pack();
    }

    /* renamed from: LH */
    public C5553aNt mo1186LH() {
        return this.gFK;
    }

    /* renamed from: LI */
    public ContractBaseInfo mo1187LI() {
        return this.f3679kj.ala().aLO().cIa();
    }

    /* renamed from: LJ */
    public boolean mo1188LJ() {
        if (this.gFK.mo9371KG() == null) {
            this.gFL.mo769c(C0939Nn.C0940a.ERROR, this.f3679kj.mo11834a((Class<?>) C1821aR.class, "Select the player you want to be hunted.", new Object[0]));
            return false;
        } else if (this.gFK.mo9371KG() != this.f3679kj.getPlayer()) {
            return true;
        } else {
            this.gFL.mo769c(C0939Nn.C0940a.ERROR, this.f3679kj.mo11834a((Class<?>) C1821aR.class, "Cannot put a bounty on yourself.", new Object[0]));
            return false;
        }
    }

    public void destroy() {
        this.gFM.destroy();
    }

    /* renamed from: a.aR$a */
    class C1822a implements ActionListener {
        C1822a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Player aku;
            if (C1821aR.this.gFN.getText() == null || C1821aR.this.gFN.getText().length() <= 0) {
                aku = null;
            } else {
                aku = C1821aR.this.f3679kj.ala().aJm().mo9908nd(C1821aR.this.gFN.getText());
            }
            if (aku == C1821aR.this.f3679kj.getPlayer()) {
                C1821aR.this.gFL.mo769c(C0939Nn.C0940a.ERROR, C1821aR.this.f3679kj.mo11834a((Class<?>) C1821aR.class, "Cannot put a bounty on yourself.", new Object[0]));
            } else if (aku != null) {
                C1821aR.this.gFO.setText(aku.getName());
                if (aku.dxE()) {
                    C1821aR.this.gFP.setText(aku.bYd().getName());
                    C1821aR.this.gFQ.mo16824a(C5378aHa.CORP_IMAGES, aku.bYd().mo10706Qu().aOO());
                    C1821aR.this.gFQ.setVisible(true);
                    ((VBox) C1821aR.this.gFM).doLayout();
                } else {
                    C1821aR.this.gFP.setText("");
                    C1821aR.this.gFQ.setVisible(false);
                    ((VBox) C1821aR.this.gFM).doLayout();
                }
                C1821aR.this.gFK.mo11796cy(aku);
            } else {
                C1821aR.this.gFL.mo769c(C0939Nn.C0940a.ERROR, C1821aR.this.f3679kj.mo11834a((Class<?>) C1821aR.class, "Player not found.", new Object[0]));
            }
        }
    }
}
