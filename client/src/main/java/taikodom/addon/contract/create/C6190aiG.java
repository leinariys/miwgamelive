package taikodom.addon.contract.create;

import game.network.message.externalizable.C5820abA;
import game.script.item.Item;
import game.script.ship.Station;
import logic.aWa;
import logic.ui.C2698il;
import logic.ui.item.InternalFrame;
import logic.ui.item.aUR;
import p001a.C1447VJ;
import p001a.C3987xk;
import p001a.C5517aMj;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.*;

@TaikodomAddon("taikodom.addon.contract.create")
/* renamed from: a.aiG  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6190aiG implements aWa {
    private static final String gbG = "itemView.xml";
    public List<C5820abA> aoA;
    public JLabel gbI;
    public JComboBox gbJ;
    /* renamed from: kj */
    public IAddonProperties f4599kj;
    /* renamed from: IU */
    private C3987xk f4598IU = C3987xk.bFK;
    private JList eyk;
    private JLabel gbH;
    private JButton gbK;
    /* renamed from: nM */
    private InternalFrame f4600nM;
    private JTextField searchField;

    public C6190aiG(IAddonProperties vWVar, Station bf) {
        this.f4599kj = vWVar;
        m22449ai(bf);
    }

    public C6190aiG(IAddonProperties vWVar, Station bf, Set<Item> set) {
        this.f4599kj = vWVar;
        m22449ai(bf);
        mo13653j(set);
    }

    /* access modifiers changed from: protected */
    /* renamed from: O */
    public abstract void mo4282O(Station bf);

    /* access modifiers changed from: protected */
    public abstract void bmD();

    /* access modifiers changed from: protected */
    /* renamed from: eT */
    public abstract Station mo4284eT();

    /* renamed from: ai */
    private void m22449ai(Station bf) {
        if (this.f4600nM == null) {
            this.f4600nM = this.f4599kj.bHv().mo16795c(C6190aiG.class, gbG);
            this.gbH = this.f4600nM.mo4917cf("volume-label");
            this.gbJ = this.f4600nM.mo4914cc("stations-comboBox");
            this.searchField = this.f4600nM.mo4920ci("search-field");
            this.gbK = this.f4600nM.mo4913cb("confirm-button");
            this.gbI = this.f4600nM.mo4917cf("station-name-label");
            this.searchField.getDocument().addDocumentListener(new C1900c());
            this.eyk = this.f4600nM.mo4915cd("item-list");
            this.eyk.setCellRenderer(new C1901d());
            this.eyk.setModel(new DefaultListModel());
            this.eyk.setSelectionMode(2);
            this.eyk.setLayoutOrientation(2);
            this.eyk.setVisibleRowCount(-1);
            this.eyk.addListSelectionListener(new C1898a());
            this.aoA = this.f4599kj.getPlayer().dxW();
            this.gbH.setText(this.f4598IU.mo22980dY(0.0f));
            this.gbK.addActionListener(new C1899b());
            mo4282O(bf);
            cjW();
            this.f4600nM.moveToFront();
        } else if (this.f4600nM.isVisible()) {
            this.f4600nM.setVisible(false);
        }
    }

    public boolean isOpen() {
        if (this.f4600nM != null && !this.f4600nM.isClosed()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void cjW() {
        this.aoA = this.f4599kj.getPlayer().dxW();
        ArrayList<C5820abA> arrayList = new ArrayList<>();
        for (C5820abA next : this.aoA) {
            if (next.mo12361eT() == mo4284eT() && !next.mo12359XH().cxh()) {
                arrayList.add(next);
            }
        }
        this.eyk.getModel().clear();
        for (C5820abA XH : arrayList) {
            this.eyk.getModel().addElement(new C1447VJ(XH.mo12359XH()));
        }
        refresh();
    }

    /* access modifiers changed from: protected */
    public void refresh() {
        float f = 0.0f;
        Iterator<Item> it = cjX().iterator();
        while (true) {
            float f2 = f;
            if (!it.hasNext()) {
                this.gbH.setText(this.f4598IU.mo22980dY(f2));
                return;
            }
            f = it.next().getVolume() + f2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public void mo13653j(Set<Item> set) {
        this.eyk.clearSelection();
        DefaultListModel model = this.eyk.getModel();
        Enumeration elements = model.elements();
        while (elements.hasMoreElements()) {
            C1447VJ vj = (C1447VJ) elements.nextElement();
            int indexOf = model.indexOf(vj);
            if (set.contains(vj.mo5990XH())) {
                this.eyk.addSelectionInterval(indexOf, indexOf);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Set<Item> cjX() {
        Object[] selectedValues = this.eyk.getSelectedValues();
        HashSet hashSet = new HashSet();
        for (Object obj : selectedValues) {
            hashSet.add(((C1447VJ) obj).mo5990XH());
        }
        return hashSet;
    }

    public void destroy() {
        if (this.f4600nM != null) {
            this.f4600nM.destroy();
            this.f4600nM = null;
        }
    }

    /* renamed from: a.aiG$c */
    /* compiled from: a */
    class C1900c implements DocumentListener {
        C1900c() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
        }

        public void insertUpdate(DocumentEvent documentEvent) {
        }

        public void removeUpdate(DocumentEvent documentEvent) {
        }
    }

    /* renamed from: a.aiG$d */
    /* compiled from: a */
    class C1901d extends C5517aMj {
        C1901d() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            C2698il ilVar = (JPanel) aur.mo11631nR("item");
            JLabel cf = ilVar.mo4917cf("icon");
            JLabel cf2 = ilVar.mo4917cf("name");
            cf.setIcon(((C1447VJ) obj).getIcon());
            cf2.setText(((C1447VJ) obj).mo5990XH().bAP().mo19891ke().get());
            return ilVar;
        }
    }

    /* renamed from: a.aiG$a */
    class C1898a implements ListSelectionListener {
        C1898a() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            C6190aiG.this.refresh();
        }
    }

    /* renamed from: a.aiG$b */
    /* compiled from: a */
    class C1899b implements ActionListener {
        C1899b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C6190aiG.this.bmD();
        }
    }
}
