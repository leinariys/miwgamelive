package taikodom.addon.contract;

import game.network.message.externalizable.C0939Nn;
import game.script.contract.types.*;
import game.script.item.Item;
import logic.WrapRunnable;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.item.Repeater;
import p001a.C3987xk;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;

@TaikodomAddon("taikodom.addon.contract")
/* renamed from: a.uA */
/* compiled from: a */
public abstract class C3704uA {
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f9293IU = C3987xk.bFK;
    /* access modifiers changed from: private */
    public Comparator<ContractTemplate> hQA;
    /* access modifiers changed from: private */
    public Comparator<ContractTemplate> hQB;
    /* access modifiers changed from: private */
    public ContractBaseInfo hQD;
    /* access modifiers changed from: private */
    public JComboBox hQE;
    /* access modifiers changed from: private */
    public Comparator<ContractTemplate> hQF;
    /* access modifiers changed from: private */
    public ContractAddon hQw;
    /* access modifiers changed from: private */
    public Comparator<ContractTemplate> hQx;
    /* access modifiers changed from: private */
    public Comparator<ContractTemplate> hQy;
    /* access modifiers changed from: private */
    public Comparator<ContractTemplate> hQz;
    /* renamed from: kj */
    public IAddonProperties f9294kj;
    private JTextField hQG;
    private ContractTemplate hQH;
    private Repeater<ContractTemplate> hQI;
    private C2698il hQv;
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public JComboBox hQC = this.hQv.mo4914cc("services-comboBox");

    public C3704uA(IAddonProperties vWVar, C2698il ilVar, ContractAddon contractAddon) {
        this.f9294kj = vWVar;
        this.hQv = ilVar;
        this.hQw = contractAddon;
        this.hQC.setRenderer(new C3714j());
        this.hQC.setModel(new DefaultComboBoxModel(new Object[]{this.f9294kj.translate("All Services"), this.hQw.brF().cHY(), this.hQw.brF().cIa()}));
        this.hQC.addActionListener(new C3705a());
        this.hQD = null;
        this.hQC.getModel().setSelectedItem(this.hQC.getModel().getElementAt(0));
        this.hQC.doLayout();
        this.hQx = new C3707c();
        this.hQy = new C3706b();
        this.hQz = new C3711g();
        this.hQB = new C3710f();
        this.hQA = new C3713i();
        this.hQE = this.hQv.mo4914cc("order-comboBox");
        this.hQE.setRenderer(new C3721l());
        this.hQE.setModel(new DefaultComboBoxModel(new Object[]{this.hQx, this.hQy, this.hQz, this.hQB, this.hQA}));
        this.hQE.addActionListener(new C3712h());
        this.hQF = this.hQx;
        this.hQE.getModel().setSelectedItem(this.hQx);
        this.hQE.doLayout();
        this.hQG = this.hQv.mo4920ci("search-field");
        this.hQG.getDocument().addDocumentListener(new C3709e());
        this.hQI = this.hQv.mo4915cd("contract-list");
        this.hQI.mo22250a(new C3715k());
        this.hQv.mo4915cd("contract-list-panel").addMouseListener(new C3708d());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract String mo4652a(ContractTemplate bj);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract boolean mo4653b(ContractTemplate bj);

    /* renamed from: c */
    public abstract void mo4654c(ContractTemplate bj);

    public ContractTemplate dav() {
        return null;
    }

    /* access modifiers changed from: private */
    /* renamed from: kQ */
    public String m39804kQ(long j) {
        String str = "";
        float f = (((float) j) / 60.0f) / 1000.0f;
        if ((f / 60.0f) / 24.0f > 1.0f) {
            str = String.format("%d %s, ", new Object[]{Integer.valueOf((int) ((f / 60.0f) / 24.0f)), this.f9294kj.translate("day(s)")});
            f %= 1440.0f;
        }
        return String.valueOf(str) + String.format("%02d:%02d", new Object[]{Integer.valueOf((int) (f / 60.0f)), Integer.valueOf((int) (f % 60.0f))}) + " " + this.f9294kj.translate("hour(s)");
    }

    public void daw() {
        if (this.hQG != null) {
            this.hQG.setText("");
        }
    }

    public void refresh() {
        this.hQI.clear();
        ArrayList<ContractTemplate> arrayList = new ArrayList<>();
        for (ContractTemplate next : this.hQw.brF().cHU()) {
            if (m39805s(next)) {
                arrayList.add(next);
            }
        }
        Collections.sort(arrayList, this.hQF);
        for (ContractTemplate G : arrayList) {
            this.hQI.mo22248G(G);
        }
    }

    /* renamed from: s */
    private boolean m39805s(ContractTemplate bj) {
        if (!mo4653b(bj)) {
            return false;
        }
        boolean z = true;
        if (this.hQD != null && !this.hQD.equals(bj.aAj())) {
            return false;
        }
        for (String str : this.hQG.getText().trim().split(" ")) {
            if (str.length() > 0 && !((z = z & m39794a(str, bj)))) {
                return false;
            }
        }
        return z;
    }

    /* renamed from: a */
    private boolean m39794a(String str, ContractTemplate bj) {
        String str2;
        HashSet<String> hashSet = new HashSet<>();
        hashSet.add(bj.mo711rP().get());
        hashSet.add(bj.mo713vW().get());
        hashSet.add(bj.aAw().getName());
        hashSet.add(bj.aAq().getName());
        if (bj.aAC() != null) {
            str2 = bj.aAC().getName();
        } else {
            str2 = "";
        }
        hashSet.add(str2);
        hashSet.add(this.f9293IU.format(bj.aAj().aeH()));
        hashSet.add(this.f9293IU.format(bj.aAA()));
        hashSet.add(this.f9293IU.format(bj.aAy().aqL()));
        for (Item next : bj.aAy().aRz()) {
            hashSet.add(next.bAP().mo19891ke().get());
            hashSet.add(next.bAP().mo19866HC().mo12246ke().get());
        }
        if (bj instanceof TransportTemplate) {
            TransportTemplate jw = (TransportTemplate) bj;
            hashSet.add(jw.mo3130Fl().getName());
            hashSet.add(jw.mo3131Fm().getName());
        }
        for (String lowerCase : hashSet) {
            if (lowerCase.toLowerCase().indexOf(str.toLowerCase()) >= 0) {
                return true;
            }
        }
        return false;
    }

    public void destroy() {
        this.hQI.clear();
        this.hQv.destroy();
        this.hQv = null;
    }

    /* renamed from: c */
    public void mo22292c(C0939Nn.C0940a aVar, String str) {
        ((MessageDialogAddon) this.f9294kj.mo11830U(MessageDialogAddon.class)).mo24306a(aVar, str, this.f9294kj.translate("Ok"));
    }

    /* renamed from: a.uA$j */
    /* compiled from: a */
    public class C3714j extends JLabel implements ListCellRenderer {


        public C3714j() {
            setOpaque(true);
            setHorizontalAlignment(2);
            setVerticalAlignment(0);
        }

        public Component getListCellRendererComponent(JList jList, Object obj, int i, boolean z, boolean z2) {
            if (obj instanceof String) {
                setText((String) obj);
            }
            if (obj instanceof TransportBaseInfo) {
                setText(C3704uA.this.f9294kj.translate("Transport Contracts"));
            }
            if (obj instanceof BountyBaseInfo) {
                setText(C3704uA.this.f9294kj.translate("Bounty Contracts"));
            }
            return this;
        }
    }

    /* renamed from: a.uA$l */
    /* compiled from: a */
    public class C3721l extends JLabel implements ListCellRenderer {


        public C3721l() {
            setOpaque(true);
            setHorizontalAlignment(2);
            setVerticalAlignment(0);
        }

        public Component getListCellRendererComponent(JList jList, Object obj, int i, boolean z, boolean z2) {
            if (obj == C3704uA.this.hQx) {
                setText(C3704uA.this.f9294kj.translate("Service Type"));
            }
            if (obj == C3704uA.this.hQy) {
                setText(C3704uA.this.f9294kj.translate("Reward Value"));
            }
            if (obj == C3704uA.this.hQz) {
                setText(C3704uA.this.f9294kj.translate("Reward Items"));
            }
            if (obj == C3704uA.this.hQB) {
                setText(C3704uA.this.f9294kj.translate("Remaining Time"));
            }
            if (obj == C3704uA.this.hQA) {
                setText(C3704uA.this.f9294kj.translate("Delivery Volume"));
            }
            return this;
        }
    }

    /* renamed from: a.uA$a */
    class C3705a implements ActionListener {
        C3705a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Object selectedItem = C3704uA.this.hQC.getModel().getSelectedItem();
            if (selectedItem instanceof String) {
                C3704uA.this.hQD = null;
            } else if (selectedItem instanceof ContractBaseInfo) {
                C3704uA.this.hQD = (ContractBaseInfo) selectedItem;
            } else {
                C3704uA.this.hQD = null;
                System.err.println("Unexpected choice in comboBox - " + selectedItem);
            }
            C3704uA.this.refresh();
        }
    }

    /* renamed from: a.uA$c */
    /* compiled from: a */
    class C3707c implements Comparator<ContractTemplate> {
        C3707c() {
        }

        /* renamed from: a */
        public int compare(ContractTemplate bj, ContractTemplate bj2) {
            return bj.aAj().mo22205rP().get().compareTo(bj2.aAj().mo22205rP().get());
        }
    }

    /* renamed from: a.uA$b */
    /* compiled from: a */
    class C3706b implements Comparator<ContractTemplate> {
        C3706b() {
        }

        /* renamed from: a */
        public int compare(ContractTemplate bj, ContractTemplate bj2) {
            if (bj.aAy().aqL() < bj2.aAy().aqL()) {
                return -1;
            }
            if (bj.aAy().aqL() > bj2.aAy().aqL()) {
                return 1;
            }
            return 0;
        }
    }

    /* renamed from: a.uA$g */
    /* compiled from: a */
    class C3711g implements Comparator<ContractTemplate> {
        C3711g() {
        }

        /* renamed from: a */
        public int compare(ContractTemplate bj, ContractTemplate bj2) {
            if (bj.aAy().size() < bj2.aAy().size()) {
                return -1;
            }
            if (bj.aAy().size() > bj2.aAy().size()) {
                return 1;
            }
            return 0;
        }
    }

    /* renamed from: a.uA$f */
    /* compiled from: a */
    class C3710f implements Comparator<ContractTemplate> {
        C3710f() {
        }

        /* renamed from: a */
        public int compare(ContractTemplate bj, ContractTemplate bj2) {
            if (bj.aAE() < bj2.aAE()) {
                return -1;
            }
            if (bj.aAE() > bj2.aAE()) {
                return 1;
            }
            return 0;
        }
    }

    /* renamed from: a.uA$i */
    /* compiled from: a */
    class C3713i implements Comparator<ContractTemplate> {
        C3713i() {
        }

        /* renamed from: a */
        public int compare(ContractTemplate bj, ContractTemplate bj2) {
            if (!(bj instanceof TransportTemplate)) {
                return 1;
            }
            if (bj2 instanceof TransportTemplate) {
                TransportTemplate jw = (TransportTemplate) bj;
                TransportTemplate jw2 = (TransportTemplate) bj;
                if (jw.baH().getVolume() < jw2.baH().getVolume()) {
                    return -1;
                }
                if (jw.baH().getVolume() > jw2.baH().getVolume()) {
                    return 1;
                }
            }
            return -1;
        }
    }

    /* renamed from: a.uA$h */
    /* compiled from: a */
    class C3712h implements ActionListener {
        C3712h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Object selectedItem = C3704uA.this.hQE.getModel().getSelectedItem();
            if (selectedItem instanceof Comparator) {
                C3704uA.this.hQF = (Comparator) selectedItem;
            } else {
                C3704uA.this.hQF = C3704uA.this.hQx;
                System.err.println("Unexpected choice in comboBox - " + selectedItem);
            }
            C3704uA.this.refresh();
        }
    }

    /* renamed from: a.uA$e */
    /* compiled from: a */
    class C3709e implements DocumentListener {
        C3709e() {
        }

        public void changedUpdate(DocumentEvent documentEvent) {
            C3704uA.this.refresh();
        }

        public void insertUpdate(DocumentEvent documentEvent) {
            C3704uA.this.refresh();
        }

        public void removeUpdate(DocumentEvent documentEvent) {
            C3704uA.this.refresh();
        }
    }

    /* renamed from: a.uA$d */
    /* compiled from: a */
    class C3708d extends MouseAdapter {
        C3708d() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            C3704uA.this.mo4654c((ContractTemplate) null);
        }
    }

    /* renamed from: a.uA$k */
    /* compiled from: a */
    public class C3715k implements Repeater.C3671a<ContractTemplate> {
        public C3715k() {
        }

        /* renamed from: a */
        public void mo843a(ContractTemplate bj, Component component) {
            JPanel jPanel = (JPanel) component;
            C2698il bQ = C3704uA.this.f9294kj.bHv().mo16794bQ("infoContract.xml");
            jPanel.add(bQ);
            C2698il ilVar = bQ;
            ilVar.mo4915cd("contract-image").mo16824a(C5378aHa.ICONOGRAPHY, m39819r(bj));
            JLabel cf = ilVar.mo4917cf("contract-name");
            JLabel cf2 = ilVar.mo4917cf("contract-issuer");
            JLabel cf3 = ilVar.mo4917cf("contract-timer");
            cf.setText(bj.mo711rP().get());
            cf2.setText(C3704uA.this.mo4652a(bj));
            cf3.setText(C3704uA.this.m39804kQ(C3704uA.this.hQw.brF().mo19179k(bj)));
            C3718c cVar = new C3718c(cf3, bj);
            C3704uA.this.f9294kj.alf().addTask("ContractTimer-" + bj.cWm(), cVar, 30000);
            JPanel ce = ilVar.mo4916ce("specific-panel");
            if (bj instanceof TransportTemplate) {
                C2698il bQ2 = C3704uA.this.f9294kj.bHv().mo16794bQ("infoTransport.xml");
                ce.add(bQ2);
                m39816a(bj, bQ2);
            } else if (bj instanceof BountyTemplate) {
                C2698il bQ3 = C3704uA.this.f9294kj.bHv().mo16794bQ("infoBounty.xml");
                ce.add(bQ3);
                m39817b(bj, bQ3);
            }
            C2698il bQ4 = C3704uA.this.f9294kj.bHv().mo16794bQ("infoReward.xml");
            m39818c(bj, bQ4);
            ilVar.mo4916ce("reward-panel").add(bQ4);
            C2698il ce2 = ilVar.mo4916ce("children-panel");
            ilVar.mo4913cb("contract-expand").addActionListener(new C3720e(ce2, bj));
            ce2.setVisible(false);
            jPanel.validate();
            jPanel.addMouseListener(new C3719d(bj));
            jPanel.addHierarchyListener(new C3716a(jPanel, cVar));
        }

        /* renamed from: r */
        private String m39819r(ContractTemplate bj) {
            if (bj instanceof TransportTemplate) {
                return "button_transport_selected";
            }
            if (bj instanceof BountyTemplate) {
                return "button_bounty_selected";
            }
            return "button_contracts_selected";
        }

        /* renamed from: a */
        private void m39816a(ContractTemplate bj, C2698il ilVar) {
            TransportTemplate jw = (TransportTemplate) bj;
            ilVar.mo4915cd("transport-container").mo16824a(C5378aHa.ITEMS, jw.baH().bAP().anN().getHandle());
            ilVar.mo4915cd("transport-container").setToolTipText(String.valueOf(C3704uA.this.f9294kj.translate("Volume")) + ": " + C3704uA.this.f9293IU.mo22980dY(jw.baH().getVolume()));
            ilVar.mo4917cf("transport-from").setText(String.valueOf(C3704uA.this.f9294kj.translate("From")) + ": " + jw.mo3130Fl().getName());
            ilVar.mo4917cf("transport-to").setText(String.valueOf(C3704uA.this.f9294kj.translate("To")) + ": " + jw.mo3131Fm().getName());
        }

        /* renamed from: b */
        private void m39817b(ContractTemplate bj, C2698il ilVar) {
            BountyTemplate aif = (BountyTemplate) bj;
            ilVar.mo4917cf("prey-name").setText(aif.mo9371KG().getName());
            ilVar.mo4917cf("corp-name").setText("");
            ilVar.mo4917cf("prey-name").setText(aif.mo9371KG().getName());
            if (aif.mo9371KG().dxE()) {
                ilVar.mo4917cf("corp-name").setText(aif.mo9371KG().bYd().getName());
                ilVar.mo4915cd("corp-icon").mo16824a(C5378aHa.CORP_IMAGES, aif.mo9371KG().bYd().mo10706Qu().aOO());
                ilVar.mo4915cd("corp-icon").setVisible(true);
            } else {
                ilVar.mo4917cf("corp-name").setText("");
                ilVar.mo4915cd("corp-icon").setVisible(false);
            }
            ((JPanel) ilVar).doLayout();
        }

        /* renamed from: c */
        private void m39818c(ContractTemplate bj, C2698il ilVar) {
            ilVar.mo4917cf("contract-reward").setText(C3704uA.this.f9293IU.format(bj.aAy().aqL()));
            if (bj.aAy().aRz().size() > 0) {
                Repeater cd = ilVar.mo4915cd("reward-item-repeater");
                cd.mo22250a(new C3717b(bj.aAq().getName()));
                for (Item G : bj.aAy().aRz()) {
                    cd.mo22248G(G);
                }
            }
            ilVar.mo4917cf("contract-safeguard").setText(C3704uA.this.f9293IU.format(bj.aAA()));
        }

        /* renamed from: a.uA$k$c */
        /* compiled from: a */
        class C3718c extends WrapRunnable {
            private final /* synthetic */ JLabel huN;
            private final /* synthetic */ ContractTemplate huO;

            C3718c(JLabel jLabel, ContractTemplate bj) {
                this.huN = jLabel;
                this.huO = bj;
            }

            public void run() {
                this.huN.setText(C3704uA.this.m39804kQ(C3704uA.this.hQw.brF().mo19179k(this.huO)));
            }
        }

        /* renamed from: a.uA$k$e */
        /* compiled from: a */
        class C3720e implements ActionListener {
            private final /* synthetic */ ContractTemplate huO;
            private final /* synthetic */ C2698il huP;

            C3720e(C2698il ilVar, ContractTemplate bj) {
                this.huP = ilVar;
                this.huO = bj;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                this.huP.setVisible(!this.huP.isVisible());
                C3704uA.this.mo4654c(this.huO);
            }
        }

        /* renamed from: a.uA$k$d */
        /* compiled from: a */
        class C3719d extends MouseAdapter {
            private final /* synthetic */ ContractTemplate huO;

            C3719d(ContractTemplate bj) {
                this.huO = bj;
            }

            public void mouseClicked(MouseEvent mouseEvent) {
                C3704uA.this.mo4654c(this.huO);
            }
        }

        /* renamed from: a.uA$k$a */
        class C3716a implements HierarchyListener {
            private final /* synthetic */ JPanel bHr;
            private final /* synthetic */ WrapRunnable gCu;

            C3716a(JPanel jPanel, WrapRunnable aen) {
                this.bHr = jPanel;
                this.gCu = aen;
            }

            public void hierarchyChanged(HierarchyEvent hierarchyEvent) {
                if (this.bHr.getParent() == null) {
                    this.gCu.cancel();
                }
            }
        }

        /* renamed from: a.uA$k$b */
        /* compiled from: a */
        class C3717b implements Repeater.C3671a<Item> {
            private final /* synthetic */ String gCv;

            C3717b(String str) {
                this.gCv = str;
            }

            /* renamed from: a */
            public void mo843a(Item auq, Component component) {
                C2698il ilVar = (C2698il) component;
                ilVar.mo4915cd("reward-item").mo16824a(C5378aHa.ITEMS, auq.bAP().anN().getHandle());
                ilVar.mo4917cf("reward-item-name").setText(auq.bAP().mo19891ke().get());
                ilVar.mo4917cf("reward-item-location").setText(this.gCv);
            }
        }
    }
}
