package taikodom.addon.contract;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.script.Actor;
import game.script.contract.ContractBoard;
import game.script.player.Player;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.aaa.C1274Sq;
import logic.aaa.C5783aaP;
import logic.res.css.C6868avI;
import logic.ui.item.InternalFrame;
import p001a.C2602hR;
import p001a.C3428rT;
import p001a.C6124ags;
import p001a.C6622aqW;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.contract")
/* compiled from: a */
public class ContractAddon implements C2495fo {
    public static final String eaM = "contract.xml";
    public static final String eaN = "contract.css";
    public static final String eaO = "contract-commands.css";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9836P;
    /* access modifiers changed from: private */
    public C1044PJ eaQ;
    /* access modifiers changed from: private */
    public C4049yc eaR;
    /* renamed from: kj */
    public IAddonProperties f9837kj;
    /* renamed from: nM */
    public InternalFrame f9838nM;
    private C6868avI aSA;
    private C3428rT<C5783aaP> aoN;
    private C3428rT<C3689to> dHH;
    /* access modifiers changed from: private */
    private C6622aqW dcE;
    private ContractBoard eaP;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9837kj = addonPropertie;
        this.f9837kj.bHv().mo16790b((Object) this, eaN);
        this.aSA = this.f9837kj.bHv().mo16783a((Object) this, eaO);
        this.f9836P = this.f9837kj.ala().getPlayer();
        this.eaP = this.f9837kj.ala().aLO();
        this.aoN = new C4313c();
        this.f9837kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.dHH = new C4312b();
        this.f9837kj.aVU().mo13965a(C3689to.class, this.dHH);
        this.f9837kj.aVU().mo13965a(C1274Sq.class, new C4314d());
        this.dcE = new C4310a("contract", this.f9837kj.translate("Contract"), "contract");
        this.f9837kj.aVU().publish(new C5344aFs(this.dcE, 505, false));
        this.f9837kj.mo2317P(this);
    }

    public void stop() {
        removeListeners();
        if (this.eaQ != null) {
            this.eaQ.destroy();
        }
        if (this.eaR != null) {
            this.eaR.destroy();
        }
    }

    private void removeListeners() {
        if (this.f9838nM != null) {
            this.f9837kj.aVU().mo13964a(this.dHH);
            this.f9837kj.aVU().mo13964a(this.aoN);
        }
    }

    public ContractBoard brF() {
        return this.eaP;
    }

    /* renamed from: gq */
    public void mo23755gq() {
        if (this.f9838nM != null && this.f9838nM.isVisible()) {
            this.f9838nM.setVisible(false);
        }
    }

    @C2602hR(mo19255zf = "CONTRACT_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo23753al(boolean z) {
        if (z && this.f9837kj.getPlayer().bQB()) {
            this.dcE.mo315a(new Object[0]);
        }
    }

    /* renamed from: taikodom.addon.contract.ContractAddon$c */
    /* compiled from: a */
    class C4313c extends C3428rT<C5783aaP> {
        C4313c() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (ContractAddon.this.f9838nM != null && ContractAddon.this.f9838nM.isVisible()) {
                ContractAddon.this.f9838nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.contract.ContractAddon$b */
    /* compiled from: a */
    class C4312b extends C3428rT<C3689to> {
        C4312b() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (ContractAddon.this.f9838nM != null && ContractAddon.this.f9838nM.isVisible()) {
                ContractAddon.this.f9838nM.setVisible(false);
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.contract.ContractAddon$d */
    /* compiled from: a */
    class C4314d extends C6124ags<C1274Sq> {
        C4314d() {
        }

        /* renamed from: b */
        public boolean updateInfo(C1274Sq sq) {
            if (sq.fMR != C1274Sq.C1275a.DOCK) {
                C1274Sq.C1275a aVar = sq.fMR;
                C1274Sq.C1275a aVar2 = C1274Sq.C1275a.UNDOCK;
            } else if (!(ContractAddon.this.f9836P.bhE() instanceof Station)) {
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.contract.ContractAddon$a */
    class C4310a extends C6622aqW {
        C4310a(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public String chp() {
            return "CONTRACT_WINDOW";
        }

        public boolean isActive() {
            return ContractAddon.this.f9838nM != null && ContractAddon.this.f9838nM.isVisible();
        }

        public boolean isEnabled() {
            if (ContractAddon.this.f9837kj.ala() != null) {
                ContractAddon.this.f9836P = ContractAddon.this.f9837kj.ala().getPlayer();
                Actor bhE = ContractAddon.this.f9836P.bhE();
                if (bhE instanceof Station) {
                    Station bf = (Station) bhE;
                }
            }
            return false;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (ContractAddon.this.f9838nM == null) {
                ContractAddon.this.f9838nM = ContractAddon.this.f9837kj.bHv().mo16792bN(ContractAddon.eaM);
                ContractAddon.this.f9838nM.addComponentListener(new C4311a());
                ContractAddon.this.eaQ = new C1044PJ(ContractAddon.this.f9837kj, ContractAddon.this.f9838nM.mo4915cd("boardBox"), ContractAddon.this);
                ContractAddon.this.eaR = new C4049yc(ContractAddon.this.f9837kj, ContractAddon.this.f9838nM.mo4915cd("myBox"), ContractAddon.this);
            } else if (ContractAddon.this.f9838nM.isVisible()) {
                ContractAddon.this.f9838nM.setVisible(false);
                return;
            }
            ContractAddon.this.eaQ.daw();
            ContractAddon.this.eaR.daw();
            ContractAddon.this.eaQ.refresh();
            ContractAddon.this.eaR.refresh();
        }

        /* renamed from: taikodom.addon.contract.ContractAddon$a$a */
        class C4311a extends ComponentAdapter {
            C4311a() {
            }

            public void componentHidden(ComponentEvent componentEvent) {
                ContractAddon.this.f9837kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, ContractAddon.this));
                ContractAddon.this.f9838nM = null;
            }

            public void componentShown(ComponentEvent componentEvent) {
                ContractAddon.this.eaQ.refresh();
                ContractAddon.this.eaR.refresh();
            }
        }
    }
}
