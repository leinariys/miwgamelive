package taikodom.addon.contract;

import game.network.message.externalizable.C0939Nn;
import game.script.contract.types.ContractTemplate;
import logic.ui.C2698il;
import p001a.aEP;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.contract.create.C0135Bg;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.contract")
/* renamed from: a.yc */
/* compiled from: a */
public class C4049yc extends C3704uA {
    private static /* synthetic */ int[] dng;
    /* renamed from: kj */
    public IAddonProperties f9590kj;
    private JButton aoP;
    /* access modifiers changed from: private */
    private JButton eWJ;

    public C4049yc(IAddonProperties vWVar, C2698il ilVar, ContractAddon contractAddon) {
        super(vWVar, ilVar, contractAddon);
        this.f9590kj = vWVar;
        this.eWJ = ilVar.mo4913cb("create-button");
        this.eWJ.addActionListener(new C4050a(contractAddon, vWVar));
        this.aoP = ilVar.mo4913cb("cancel-button");
        this.aoP.addActionListener(new C4051b());
        this.aoP.setEnabled(false);
    }

    static /* synthetic */ int[] baP() {
        int[] iArr = dng;
        if (iArr == null) {
            iArr = new int[ContractTemplate.C0104a.values().length];
            try {
                iArr[ContractTemplate.C0104a.AVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ContractTemplate.C0104a.DEAD.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ContractTemplate.C0104a.TAKEN.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            dng = iArr;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String mo4652a(ContractTemplate bj) {
        switch (baP()[bj.aAs().ordinal()]) {
            case 1:
                return this.f9590kj.translate("Available");
            case 2:
                return String.valueOf(this.f9590kj.translate("Taken by")) + ": " + bj.aAC().getName();
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo4653b(ContractTemplate bj) {
        return bj.aAw().equals(this.f9590kj.getPlayer());
    }

    /* renamed from: c */
    public void mo4654c(ContractTemplate bj) {
        if (bj != null) {
            this.aoP.setEnabled(true);
        } else {
            this.aoP.setEnabled(false);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m41355d(ContractTemplate bj) {
        if (!this.f9590kj.ala().aLO().mo19178i(bj)) {
            mo22292c(C0939Nn.C0940a.ERROR, this.f9590kj.translate("Contract cannot be canceled."));
            return;
        }
        refresh();
        this.aoP.setEnabled(false);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public boolean m41356e(ContractTemplate bj) {
        System.out.println("TODO: Should popUp and confirm cancel action");
        m41355d(bj);
        ((MessageDialogAddon) this.f9590kj.mo11830U(MessageDialogAddon.class)).mo24307a(this.f9590kj.translate("Contract will be canceled. Are you sure?"), (aEP) new C4052c(bj), this.f9590kj.translate("Cancel"), this.f9590kj.translate("OK"));
        return false;
    }

    /* renamed from: a.yc$a */
    class C4050a implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;
        private final /* synthetic */ ContractAddon bJz;

        C4050a(ContractAddon contractAddon, IAddonProperties vWVar) {
            this.bJz = contractAddon;
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.bJz.mo23755gq();
            new C0135Bg(this.bJA);
        }
    }

    /* renamed from: a.yc$b */
    /* compiled from: a */
    class C4051b implements ActionListener {
        C4051b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C4049yc.this.dav() == null) {
                return;
            }
            if (C4049yc.this.dav().aAs() == ContractTemplate.C0104a.AVAILABLE) {
                boolean unused = C4049yc.this.m41356e(C4049yc.this.dav());
            } else {
                C4049yc.this.mo22292c(C0939Nn.C0940a.ERROR, C4049yc.this.f9590kj.translate("Contract cannot be canceled."));
            }
        }
    }

    /* renamed from: a.yc$c */
    /* compiled from: a */
    class C4052c extends aEP {
        private final /* synthetic */ ContractTemplate bKy;

        C4052c(ContractTemplate bj) {
            this.bKy = bj;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                C4049yc.this.m41355d(this.bKy);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.WARNING;
        }
    }
}
