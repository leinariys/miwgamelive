package taikodom.addon.market;

import logic.IAddonSettings;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.newmarket.C2331eF;
import taikodom.addon.newmarket.C3442re;

@TaikodomAddon("taikodom.addon.market")
/* compiled from: a */
public class MarketAddon implements C2495fo {
    private C2331eF aoM;

    /* renamed from: kj */
    private IAddonProperties f10106kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10106kj = addonPropertie;
        this.aoM = new C2331eF(this.f10106kj, this);
    }

    public void stop() {
        this.aoM.dispose();
    }

    public C3442re bio() {
        return this.aoM.bio();
    }
}
