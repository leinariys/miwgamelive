package taikodom.addon.market;

import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.baa.C5473aKr;
import logic.baa.aDJ;
import logic.data.link.aWg;
import logic.res.code.C5663aRz;
import logic.ui.item.C5923acz;
import logic.ui.item.TaskPane;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.*;
import java.util.Arrays;
import java.util.HashMap;

@TaikodomAddon("taikodom.addon.market")
/* compiled from: a */
public class CoreMarketAddon implements C2495fo {
    /* access modifiers changed from: private */
    public TaskPane aCG;
    /* access modifiers changed from: private */
    public C4713g hCX;
    /* renamed from: kj */
    public IAddonProperties f10105kj;
    private aSZ aCF;
    private C5923acz akV;
    private KeyListener hCY;
    private C5473aKr<aDJ, Object> hCZ;
    /* access modifiers changed from: private */
    private aGC hDa;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10105kj = addonPropertie;
        if (this.f10105kj.ala() != null) {
            m44547Im();
            m44554iG();
            this.hDa = new C4710e("CoreMarket", "CoreMarket");
            this.aCF = new aSZ(this.aCG, this.hDa, 900, 2);
            this.f10105kj.aVU().publish(this.aCF);
            if (this.hDa.isEnabled()) {
                open();
            }
        }
    }

    /* renamed from: iG */
    private void m44554iG() {
        this.hCX = new C4713g((C4713g) null);
        this.hCX.mo2166a(new C4706a());
        this.hCY = new C4707b();
        this.hCZ = new C4708c();
        this.f10105kj.ala().mo8319a(aWg.jfj, (C5473aKr<?, ?>) this.hCZ);
    }

    public void stop() {
        if (this.f10105kj.ala() != null) {
            this.f10105kj.ala().mo8325b(aWg.jfj, (C5473aKr<?, ?>) this.hCZ);
        }
        this.f10105kj.aVU().mo13974g(this.aCF);
    }

    /* access modifiers changed from: private */
    public void open() {
        refresh();
    }

    /* renamed from: Im */
    private void m44547Im() {
        this.aCG = (TaskPane) this.f10105kj.bHv().mo16794bQ("coreMarket.xml");
        this.aCG.addComponentListener(new C4709d());
        this.aCG.mo4920ci("searchtextfield").addKeyListener(this.hCY);
        this.akV = this.aCG.mo4915cd("types");
        this.akV.setCellRenderer(new C5735aUt(this.f10105kj.bHv().adz()));
        this.akV.setDragEnabled(true);
        this.akV.setTransferHandler(new C4711f((C4711f) null));
    }

    /* access modifiers changed from: private */
    public void refresh() {
        ItemTypeCategory bJS;
        if (this.akV != null) {
            this.akV.bOW();
            HashMap hashMap = new HashMap();
            for (ItemType next : this.f10105kj.ala().aKQ()) {
                ItemTypeCategory HC = next.mo19866HC();
                if (!(HC == null || (bJS = HC.bJS()) == null || !this.hCX.evaluate(next))) {
                    if (!hashMap.containsKey(bJS)) {
                        DefaultMutableTreeNode defaultMutableTreeNode = new DefaultMutableTreeNode(bJS);
                        hashMap.put(bJS, defaultMutableTreeNode);
                        this.akV.mo12740a(defaultMutableTreeNode);
                    }
                    if (!hashMap.containsKey(HC)) {
                        DefaultMutableTreeNode defaultMutableTreeNode2 = new DefaultMutableTreeNode(HC);
                        hashMap.put(HC, defaultMutableTreeNode2);
                        ((DefaultMutableTreeNode) hashMap.get(bJS)).add(defaultMutableTreeNode2);
                    }
                    ((DefaultMutableTreeNode) hashMap.get(HC)).add(new DefaultMutableTreeNode(next));
                }
            }
            this.akV.bOX();
        }
    }

    /* renamed from: taikodom.addon.market.CoreMarketAddon$g */
    /* compiled from: a */
    private static class C4713g extends C0400FZ<ItemType> {
        private String ewp;

        private C4713g() {
            this.ewp = "";
        }

        /* synthetic */ C4713g(C4713g gVar) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: N */
        public boolean mo2164N(Object obj) {
            String lowerCase = obj.toString().trim().toLowerCase();
            if (lowerCase.equals(this.ewp)) {
                return false;
            }
            this.ewp = lowerCase;
            return true;
        }

        /* renamed from: M */
        public boolean evaluate(ItemType jCVar) {
            if (this.ewp == null || this.ewp.length() == 0 || jCVar.mo19891ke().get().toLowerCase().indexOf(this.ewp.toString()) >= 0) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.market.CoreMarketAddon$f */
    /* compiled from: a */
    private static class C4711f extends TransferHandler {
        private static final long serialVersionUID = -4710679820887330304L;

        private C4711f() {
        }

        /* synthetic */ C4711f(C4711f fVar) {
            this();
        }

        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            return false;
        }

        /* access modifiers changed from: protected */
        public Transferable createTransferable(JComponent jComponent) {
            try {
                Object userObject = ((DefaultMutableTreeNode) ((C5923acz) jComponent).getSelectionModel().getSelectionPath().getLastPathComponent()).getUserObject();
                if (!(userObject instanceof ItemType)) {
                    return null;
                }
                return new C4712a((ItemType) userObject);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public int getSourceActions(JComponent jComponent) {
            return 1;
        }

        /* renamed from: taikodom.addon.market.CoreMarketAddon$f$a */
        class C4712a implements Transferable {
            private final /* synthetic */ ItemType elU;

            C4712a(ItemType jCVar) {
                this.elU = jCVar;
            }

            public Object getTransferData(DataFlavor dataFlavor) {
                return Arrays.asList(new ItemType[]{this.elU});
            }

            public DataFlavor[] getTransferDataFlavors() {
                return new DataFlavor[]{C2125cH.f6026vP, DataFlavor.stringFlavor};
            }

            public boolean isDataFlavorSupported(DataFlavor dataFlavor) {
                return dataFlavor == C2125cH.f6026vP;
            }
        }
    }

    /* renamed from: taikodom.addon.market.CoreMarketAddon$e */
    /* compiled from: a */
    class C4710e extends aGC {


        C4710e(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            if (CoreMarketAddon.this.f10105kj.ala() == null) {
                return false;
            }
            Player aPC = CoreMarketAddon.this.f10105kj.ala().getPlayer();
            if ((CoreMarketAddon.this.f10105kj.ala().aLS().ads()) && aPC.bQB()) {
                return true;
            }
            return false;
        }

        public void setVisible(boolean z) {
            if (CoreMarketAddon.this.f10105kj.ala() != null && z) {
                CoreMarketAddon.this.open();
            }
        }
    }

    /* renamed from: taikodom.addon.market.CoreMarketAddon$a */
    class C4706a implements C3385qw {
        C4706a() {
        }

        /* renamed from: Xg */
        public void mo21537Xg() {
            CoreMarketAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.market.CoreMarketAddon$b */
    /* compiled from: a */
    class C4707b extends KeyAdapter {
        C4707b() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (CoreMarketAddon.this.aCG != null) {
                CoreMarketAddon.this.hCX.mo2165O(CoreMarketAddon.this.aCG.mo4920ci("searchtextfield").getText().trim());
            }
        }
    }

    /* renamed from: taikodom.addon.market.CoreMarketAddon$c */
    /* compiled from: a */
    class C4708c implements C5473aKr<aDJ, Object> {
        C4708c() {
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            CoreMarketAddon.this.refresh();
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            CoreMarketAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.market.CoreMarketAddon$d */
    /* compiled from: a */
    class C4709d extends ComponentAdapter {
        C4709d() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            CoreMarketAddon.this.aCG = null;
            CoreMarketAddon.this.hCX.mo2165O("");
        }
    }
}
