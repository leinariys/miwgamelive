package taikodom.addon.market;

import game.network.message.externalizable.C3689to;
import game.script.item.*;
import game.script.newmarket.BuyOrderInfo;
import logic.aaa.C5783aaP;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import logic.ui.item.LabeledIcon;
import logic.ui.item.Picture;
import p001a.C2539gY;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.newmarket.C2331eF;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;

@TaikodomAddon("taikodom.addon.market")
/* renamed from: a.jK */
/* compiled from: a */
public class C2734jK {
    /* access modifiers changed from: private */
    public C2331eF aoM;
    /* access modifiers changed from: private */
    public BuyOrderInfo aoQ;
    /* access modifiers changed from: private */
    public Item aoR;
    /* access modifiers changed from: private */
    public ItemLocation apb;
    private C3428rT<C5783aaP> aoN = new C2737c();
    private JButton aoO;
    private JButton aoP;
    private JSpinner aoS;
    private JTextField aoT;
    private JTextField aoU;
    private LabeledIcon aoV;
    private JLabel aoW;
    private JLabel aoX;
    private long aoY;
    private int aoZ;
    private C2495fo apa;
    /* renamed from: kj */
    private IAddonProperties f8300kj;

    /* renamed from: nM */
    private InternalFrame f8301nM;

    /* renamed from: nP */
    private C3428rT<C3689to> f8302nP = new C2738d();

    public C2734jK(IAddonProperties vWVar, C2331eF eFVar, BuyOrderInfo sfVar, Item auq, int i, C2495fo foVar, ItemLocation aag) {
        this.f8300kj = vWVar;
        this.aoM = eFVar;
        this.aoQ = sfVar;
        this.aoR = auq;
        this.aoZ = i;
        this.apa = foVar;
        this.apb = aag;
        m33860Im();
        this.aoV.mo2605a((Icon) new C2539gY(this.f8300kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + this.aoQ.mo8620eP().mo12100sK().getHandle())));
        this.aoV.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, String.valueOf(this.aoQ.mo8627ff()));
        Picture a = this.aoV.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
        if (this.aoR instanceof Component) {
            ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) this.aoR.bAP()).cuJ());
            a.setVisible(true);
        } else {
            a.setVisible(false);
        }
        m33875xL();
        m33859Fa();
        this.f8301nM.toFront();
        this.f8301nM.setVisible(true);
    }

    private void clear() {
        hide();
    }

    /* renamed from: xL */
    private void m33875xL() {
    }

    /* access modifiers changed from: private */
    public void hide() {
        this.f8301nM.setVisible(false);
        removeListeners();
        this.f8301nM.destroy();
    }

    private void removeListeners() {
        this.f8300kj.aVU().mo13964a(this.f8302nP);
        this.f8300kj.aVU().mo13964a(this.aoN);
    }

    /* access modifiers changed from: private */
    public boolean isVisible() {
        return this.f8301nM != null && this.f8301nM.isVisible();
    }

    /* renamed from: Fa */
    private void m33859Fa() {
        this.f8300kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f8300kj.aVU().mo13965a(C3689to.class, this.f8302nP);
        this.aoO.addActionListener(new C2735a());
        this.aoP.addActionListener(new C2736b());
        this.aoS.addChangeListener(new C2739e());
    }

    /* access modifiers changed from: private */
    /* renamed from: Ip */
    public void m33861Ip() {
        this.aoU.setText(Long.toString(m33864Is()));
    }

    /* renamed from: Im */
    private void m33860Im() {
        int It;
        this.f8301nM = this.f8300kj.bHv().mo16795c(this.apa.getClass(), "supplyBuyOrder.xml");
        this.f8301nM.setLocation((this.f8300kj.bHv().getScreenWidth() - this.f8301nM.getSize().width) / 2, (this.f8300kj.bHv().getScreenHeight() - this.f8301nM.getSize().height) / 2);
        this.aoO = this.f8301nM.mo4913cb("okButton");
        this.aoP = this.f8301nM.mo4913cb("cancelButton");
        this.aoS = this.f8301nM.mo4915cd("amount");
        this.aoT = this.f8301nM.mo4915cd("unity");
        this.aoU = this.f8301nM.mo4915cd("total");
        this.aoV = this.f8301nM.mo4915cd("image");
        this.aoW = this.f8301nM.mo4917cf("itemName");
        this.aoX = this.f8301nM.mo4917cf("itemLocation");
        this.aoW.setText(this.aoQ.mo8620eP().mo19891ke().get());
        Number number = this.aoS.getModel().getNumber();
        Comparable minimum = this.aoS.getModel().getMinimum();
        if (m33865It() > this.aoQ.mo8627ff()) {
            It = this.aoQ.mo8627ff();
        } else {
            It = m33865It();
        }
        Integer valueOf = Integer.valueOf(It);
        this.aoS.setModel(new SpinnerNumberModel(number, minimum, valueOf, this.aoS.getModel().getStepSize()));
        this.aoS.getModel().setValue(valueOf);
        this.aoY = this.aoQ.mo8621eR();
        this.aoT.setText(Long.toString(this.aoY));
        this.aoS.getEditor().requestFocus();
        m33861Ip();
        this.f8301nM.pack();
    }

    /* access modifiers changed from: private */
    /* renamed from: Iq */
    public int m33862Iq() {
        return ((Integer) this.aoS.getValue()).intValue();
    }

    /* renamed from: Ir */
    private long m33863Ir() {
        return this.aoY;
    }

    /* renamed from: Is */
    private long m33864Is() {
        return ((long) m33862Iq()) * m33863Ir();
    }

    /* renamed from: It */
    private int m33865It() {
        return this.aoZ;
    }

    /* renamed from: a.jK$c */
    /* compiled from: a */
    class C2737c extends C3428rT<C5783aaP> {
        C2737c() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (C2734jK.this.isVisible()) {
                C2734jK.this.hide();
            }
        }
    }

    /* renamed from: a.jK$d */
    /* compiled from: a */
    class C2738d extends C3428rT<C3689to> {
        C2738d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C2734jK.this.isVisible()) {
                C2734jK.this.hide();
                toVar.adC();
            }
        }
    }

    /* renamed from: a.jK$a */
    class C2735a implements ActionListener {
        C2735a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            int c = C2734jK.this.m33862Iq();
            if (C2734jK.this.aoR instanceof BulkItem) {
                Iterator<Item> iterator = C2734jK.this.apb.getIterator();
                while (iterator.hasNext() && C2734jK.this.aoR.mo302Iq() < c) {
                    Item next = iterator.next();
                    if (C2734jK.this.aoQ.mo8620eP() == next.bAP()) {
                        ((BulkItem) C2734jK.this.aoR).mo11562c((BulkItem) next);
                    }
                }
                if (C2734jK.this.aoR.mo302Iq() > c) {
                    C2734jK.this.aoR = ((BulkItem) C2734jK.this.aoR).mo11560AQ(c);
                }
            }
            C2734jK.this.aoM.mo17965a(C2734jK.this.aoQ, C2734jK.this.aoR);
            C2734jK.this.hide();
        }
    }

    /* renamed from: a.jK$b */
    /* compiled from: a */
    class C2736b implements ActionListener {
        C2736b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C2734jK.this.hide();
        }
    }

    /* renamed from: a.jK$e */
    /* compiled from: a */
    class C2739e implements ChangeListener {
        C2739e() {
        }

        public void stateChanged(ChangeEvent changeEvent) {
            C2734jK.this.m33861Ip();
        }
    }
}
