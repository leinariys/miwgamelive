package taikodom.addon.market;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C3689to;
import game.script.item.ComponentType;
import game.script.newmarket.SellOrderInfo;
import logic.aaa.C5783aaP;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import logic.ui.item.LabeledIcon;
import logic.ui.item.Picture;
import p001a.C0314EF;
import p001a.C2539gY;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;
import taikodom.addon.newmarket.C2331eF;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.market")
/* renamed from: a.Su */
/* compiled from: a */
public class C1279Su {
    /* access modifiers changed from: private */
    public C2331eF aoM;
    /* access modifiers changed from: private */
    public SellOrderInfo gaT;
    /* access modifiers changed from: private */
    public C0314EF gaU;
    /* renamed from: kj */
    public IAddonProperties f1575kj;
    private C3428rT<C5783aaP> aoN = new C1282c();
    private JButton aoO;
    private JButton aoP;
    private JSpinner aoS;
    private JTextField aoT;
    private JTextField aoU;
    private LabeledIcon aoV;
    private JLabel aoW;
    private JLabel aoX;
    private long aoY;
    /* access modifiers changed from: private */
    private C2495fo apa;
    /* renamed from: nM */
    private InternalFrame f1576nM;

    /* renamed from: nP */
    private C3428rT<C3689to> f1577nP = new C1283d();

    public C1279Su(IAddonProperties vWVar, C2331eF eFVar, SellOrderInfo xTVar, C0314EF ef, C2495fo foVar) {
        this.f1575kj = vWVar;
        this.aoM = eFVar;
        this.gaT = xTVar;
        this.gaU = ef;
        this.f1575kj.bHv().mo16790b((Object) foVar, "supplySellOrder.css");
        this.apa = foVar;
        m9610Im();
        this.aoV.mo2605a((Icon) new C2539gY(this.f1575kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + this.gaT.mo8620eP().mo12100sK().getHandle())));
        this.aoV.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, String.valueOf(this.gaT.mo8627ff()));
        Picture a = this.aoV.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
        if (this.gaT.mo8620eP() instanceof ComponentType) {
            ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) this.gaT.mo8620eP()).cuJ());
            a.setVisible(true);
        } else {
            a.setVisible(false);
        }
        m9609Fa();
        this.f1576nM.setVisible(true);
    }

    /* renamed from: Fa */
    private void m9609Fa() {
        this.f1575kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f1575kj.aVU().mo13965a(C3689to.class, this.f1577nP);
        this.aoO.addActionListener(new C1280a());
        this.aoP.addActionListener(new C1281b());
        this.aoS.addChangeListener(new C1284e());
    }

    /* access modifiers changed from: private */
    /* renamed from: Ip */
    public void m9611Ip() {
        this.aoU.setText(Long.toString(m9614Is()));
    }

    /* renamed from: Im */
    private void m9610Im() {
        this.f1576nM = this.f1575kj.bHv().mo16795c(this.apa.getClass(), "supplySellOrder.xml");
        this.f1576nM.setLocation((this.f1575kj.bHv().getScreenWidth() - this.f1576nM.getSize().width) / 2, (this.f1575kj.bHv().getScreenHeight() - this.f1576nM.getSize().height) / 2);
        this.aoP = this.f1576nM.mo4913cb("cancelButton");
        this.aoO = this.f1576nM.mo4913cb("okButton");
        this.aoS = this.f1576nM.mo4915cd("amount");
        this.aoT = this.f1576nM.mo4915cd("unity");
        this.aoU = this.f1576nM.mo4915cd("total");
        this.aoV = this.f1576nM.mo4915cd("image");
        this.aoW = this.f1576nM.mo4917cf("itemName");
        this.aoX = this.f1576nM.mo4917cf("itemLocation");
        this.aoW.setText(this.gaT.mo8620eP().mo19891ke().get());
        this.aoX.setText(this.f1575kj.translate(this.gaU.getName()));
        this.aoY = this.gaT.mo8621eR();
        this.aoT.setText(Long.toString(this.aoY));
        this.aoS.setModel(new SpinnerNumberModel(this.aoS.getModel().getNumber(), this.aoS.getModel().getMinimum(), Integer.valueOf(this.gaT.mo8627ff()), this.aoS.getModel().getStepSize()));
        this.aoS.getEditor().requestFocus();
        m9611Ip();
        this.f1576nM.pack();
    }

    /* access modifiers changed from: private */
    public void hide() {
        this.f1576nM.setVisible(false);
        removeListeners();
        this.f1576nM.destroy();
    }

    private void removeListeners() {
        this.f1575kj.aVU().mo13964a(this.f1577nP);
        this.f1575kj.aVU().mo13964a(this.aoN);
    }

    /* access modifiers changed from: private */
    public boolean isVisible() {
        return this.f1576nM != null && this.f1576nM.isVisible();
    }

    /* access modifiers changed from: private */
    public boolean cjH() {
        return m9614Is() == 0 || this.f1575kj.getPlayer().mo5156Qw().bSs() >= m9614Is();
    }

    /* access modifiers changed from: private */
    /* renamed from: Iq */
    public int m9612Iq() {
        return ((Integer) this.aoS.getValue()).intValue();
    }

    /* renamed from: Ir */
    private long m9613Ir() {
        return this.aoY;
    }

    /* renamed from: Is */
    private long m9614Is() {
        return ((long) m9612Iq()) * m9613Ir();
    }

    /* renamed from: a.Su$c */
    /* compiled from: a */
    class C1282c extends C3428rT<C5783aaP> {
        C1282c() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (C1279Su.this.isVisible()) {
                C1279Su.this.hide();
            }
        }
    }

    /* renamed from: a.Su$d */
    /* compiled from: a */
    class C1283d extends C3428rT<C3689to> {
        C1283d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C1279Su.this.isVisible()) {
                C1279Su.this.hide();
                toVar.adC();
            }
        }
    }

    /* renamed from: a.Su$a */
    class C1280a implements ActionListener {
        C1280a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (C1279Su.this.cjH()) {
                C1279Su.this.aoM.mo17966a(C1279Su.this.gaT, C1279Su.this.m9612Iq(), C1279Su.this.gaU);
            } else {
                ((MessageDialogAddon) C1279Su.this.f1575kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, C1279Su.this.f1575kj.translate("Saldo insuficiente."), C1279Su.this.f1575kj.translate("Ok"));
            }
            C1279Su.this.hide();
        }
    }

    /* renamed from: a.Su$b */
    /* compiled from: a */
    class C1281b implements ActionListener {
        C1281b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            C1279Su.this.hide();
        }
    }

    /* renamed from: a.Su$e */
    /* compiled from: a */
    class C1284e implements ChangeListener {
        C1284e() {
        }

        public void stateChanged(ChangeEvent changeEvent) {
            C1279Su.this.m9611Ip();
        }
    }
}
