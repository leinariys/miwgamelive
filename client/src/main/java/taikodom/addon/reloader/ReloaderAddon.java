package taikodom.addon.reloader;

import logic.AddonManager;
import logic.IAddonExecutor;
import logic.IAddonSettings;
import logic.IWrapFileXmlOrJar;
import logic.ui.item.InternalFrame;
import p001a.C2602hR;
import taikodom.addon.C2495fo;
import taikodom.addon.C3268pp;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@TaikodomAddon("taikodom.addon.reloader")
/* compiled from: a */
public class ReloaderAddon implements C2495fo {

    /* renamed from: kk */
    public InternalFrame f10235kk;
    /* access modifiers changed from: private */
    /* renamed from: kl */
    public C4956a f10236kl = new C4956a(this, (C4956a) null);
    /* access modifiers changed from: private */
    /* renamed from: km */
    public JButton f10237km;
    /* access modifiers changed from: private */
    /* renamed from: kj */
    private IAddonProperties f10234kj;
    /* renamed from: kn */
    private JButton f10238kn;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10234kj = addonPropertie;
        this.f10234kj.mo2317P(this);
        this.f10235kk = this.f10234kj.bHv().mo16792bN("reloader.xml");
        this.f10235kk.pack();
        this.f10235kk.setLocation(400, 50);
        this.f10238kn = this.f10235kk.mo4913cb("reload");
        this.f10238kn.addActionListener(new C4959d());
        this.f10238kn.setText("Reload Addons");
        this.f10237km = this.f10235kk.mo4913cb("lastLoaded");
        this.f10237km.addActionListener(this.f10236kl);
        this.f10237km.setText("...");
        C4960e eVar = new C4960e();
        this.f10237km.addMouseListener(eVar);
        this.f10238kn.addMouseListener(eVar);
        this.f10235kk.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: eu */
    public void m45359eu() {
        String str;
        JPopupMenu jPopupMenu = new JPopupMenu();
        for (IWrapFileXmlOrJar next : this.f10234kj.bHu().getCloneLoadAddonFile()) {
            ArrayList arrayList = new ArrayList(next.dCN());
            Collections.sort(arrayList, new C4957b());
            int i = 0;
            while (i < arrayList.size()) {
                JMenu jMenu = new JMenu();
                String str2 = "";
                String str3 = "";
                int i2 = 20;
                int i3 = i;
                while (true) {
                    int i4 = i2 - 1;
                    if (i4 < 0 || i3 >= arrayList.size()) {
                        jMenu.setText(String.valueOf(str2.substring(0, 3)) + "   ~   " + str3.substring(0, 3));
                        jPopupMenu.add(jMenu);
                        i = i3;
                    } else {
                        IAddonExecutor ams = (IAddonExecutor) arrayList.get(i3);
                        String trim = ams.getClass().getSimpleName().replaceAll("Addon$", "").replaceAll("([a-zA-Z])([0-9A-Z])", "$1 $2").trim();
                        JMenuItem jMenuItem = new JMenuItem(trim);
                        jMenu.add(jMenuItem);
                        jMenuItem.addActionListener(new C4958c(next, ams, trim));
                        if (i4 == 19) {
                            str = str3;
                            str2 = trim;
                        } else if (i4 == 0 || i3 == arrayList.size() - 1) {
                            str = trim;
                        } else {
                            str = str3;
                        }
                        i3++;
                        i2 = i4;
                        str3 = str;
                    }
                }
                jMenu.setText(String.valueOf(str2.substring(0, 3)) + "   ~   " + str3.substring(0, 3));
                jPopupMenu.add(jMenu);
                i = i3;
            }
        }
        jPopupMenu.show(this.f10235kk, this.f10235kk.getWidth(), this.f10238kn.getHeight());
    }

    @C2602hR(mo19255zf = "RELOAD_ADDONS")
    /* renamed from: o */
    public void mo24547o(boolean z) {
        if (!z) {
            ((AddonManager) ((C3268pp) this.f10234kj).bHu()).dispose();
        }
    }

    public void stop() {
        this.f10234kj.mo2318Q(this);
    }

    /* renamed from: taikodom.addon.reloader.ReloaderAddon$d */
    /* compiled from: a */
    class C4959d implements ActionListener {
        C4959d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ReloaderAddon.this.m45359eu();
        }
    }

    /* renamed from: taikodom.addon.reloader.ReloaderAddon$e */
    /* compiled from: a */
    class C4960e extends MouseAdapter {
        C4960e() {
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            ReloaderAddon.this.f10235kk.moveToFront();
        }
    }

    /* renamed from: taikodom.addon.reloader.ReloaderAddon$b */
    /* compiled from: a */
    class C4957b implements Comparator<IAddonExecutor<IAddonProperties>> {
        C4957b() {
        }

        /* renamed from: a */
        public int compare(IAddonExecutor<IAddonProperties> ams, IAddonExecutor<IAddonProperties> ams2) {
            return ams.getClass().getSimpleName().compareTo(ams2.getClass().getSimpleName());
        }
    }

    /* renamed from: taikodom.addon.reloader.ReloaderAddon$c */
    /* compiled from: a */
    class C4958c implements ActionListener {
        private final /* synthetic */ IWrapFileXmlOrJar gZG;
        private final /* synthetic */ IAddonExecutor gZH;
        private final /* synthetic */ String gZI;

        C4958c(IWrapFileXmlOrJar avw, IAddonExecutor ams, String str) {
            this.gZG = avw;
            this.gZH = ams;
            this.gZI = str;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.gZG.mo11803a(this.gZH);
            ReloaderAddon.this.f10237km.setText(this.gZI);
            ReloaderAddon.this.f10237km.setEnabled(true);
            ReloaderAddon.this.f10236kl.mo24548a(this.gZG, this.gZH);
        }
    }

    /* renamed from: taikodom.addon.reloader.ReloaderAddon$a */
    private class C4956a implements ActionListener {
        private IWrapFileXmlOrJar eMb;
        private IAddonExecutor<IAddonProperties> fnA;

        private C4956a() {
        }

        /* synthetic */ C4956a(ReloaderAddon reloaderAddon, C4956a aVar) {
            this();
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (this.eMb != null && this.fnA != null) {
                this.eMb.mo11803a(this.fnA);
            }
        }

        /* renamed from: a */
        public void mo24548a(IWrapFileXmlOrJar avw, IAddonExecutor<IAddonProperties> ams) {
            this.eMb = avw;
            this.fnA = ams;
        }
    }
}
