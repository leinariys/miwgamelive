package taikodom.addon.welcome;

import game.script.cloning.CloningDefaults;
import game.script.npc.NPC;
import game.script.player.Player;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.aaa.C6958awx;
import logic.swing.C5378aHa;
import logic.thred.LogPrinter;
import logic.ui.item.InternalFrame;
import org.apache.commons.logging.Log;
import p001a.C3428rT;
import p001a.C5956adg;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.welcome")
/* compiled from: a */
public class WelcomeAddon implements C2495fo {

    /* renamed from: qw */
    private static final String f10315qw = "welcome.xml";
    private final Log log = LogPrinter.setClass(WelcomeAddon.class);
    /* renamed from: nM */
    public InternalFrame f10317nM;
    /* access modifiers changed from: private */
    /* renamed from: kj */
    private IAddonProperties f10316kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        NPC bTa;
        this.f10316kj = addonPropertie;
        Player dL = addonPropertie.getPlayer();
        if (!m45723iL() || (dL != null && !dL.dwW())) {
            dL.mo14348am("login", "");
            stop();
            return;
        }
        this.f10317nM = addonPropertie.bHv().mo16792bN(f10315qw);
        this.f10317nM.pack();
        if (dL != null) {
            Station xh = dL.dxZ().mo5652xh();
            CloningDefaults xd = addonPropertie.ala().aJe().mo19010xd();
            if (xh != null && xh.equals(xd.bSK())) {
                bTa = xd.bSY();
            } else if (xh == null || !xh.equals(xd.bSM())) {
                m45722b(addonPropertie, "User starts at a non default station!");
                return;
            } else {
                bTa = xd.bTa();
            }
            if (bTa == null) {
                m45722b(addonPropertie, "There is no default agent!");
                return;
            }
            this.f10317nM.mo4915cd("agent-picture").mo16824a(C5378aHa.NPC_AVATAR, bTa.bTI());
            this.f10317nM.mo4917cf("agent-name").setText(bTa.getName());
            this.f10317nM.mo4915cd("content-label3").setText(C5956adg.format(addonPropertie.translate("WELCOME_PARAGRAPH3"), bTa.getName()));
            this.f10317nM.mo4915cd("content-label1").setText(C5956adg.format(addonPropertie.translate("WELCOME_PARAGRAPH1"), xh.getName()));
            this.f10317nM.mo4915cd("content-label2").setText(addonPropertie.translate("WELCOME_PARAGRAPH2"));
        } else {
            this.f10317nM.mo4917cf("agent-picture").setIcon(new ImageIcon(addonPropertie.bHv().adz().getImage("res://data/gui/imageset/imageset_avatar/npc_car_isis")));
            this.f10317nM.mo4917cf("agent-name").setText("isis");
            this.f10317nM.mo4915cd("content-label3").setText("The official release date for the movie here is tommorrow and i’ll try my best to get a ticket although it’s highly impossible as i heard they were sold out till next week!!.");
            this.f10317nM.mo4915cd("content-label1").setText("I am back with another cool trick and some tips to remember as well. ");
            this.f10317nM.mo4915cd("content-label2").setText("Epaminondas redesenhou o mapa político da Grécia, fragmentou antigas alianças, criou novas e supervisionou a construção de cidades inteiras.");
        }
        this.f10317nM.mo4913cb("ok-button").addActionListener(new C5097b(addonPropertie));
        this.f10317nM.pack();
        this.f10317nM.center();
        addonPropertie.aVU().mo13965a(C6958awx.class, new C5096a());
    }

    /* renamed from: b */
    private void m45722b(IAddonProperties vWVar, String str) {
        this.log.error(str);
        vWVar.getPlayer().dwY();
        stop();
    }

    public void stop() {
        try {
            if (this.f10317nM != null) {
                this.f10317nM.destroy();
                this.f10317nM = null;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: iL */
    private boolean m45723iL() {
        return this.f10316kj != null && this.f10316kj.ala().aLS().mo22273iL();
    }

    /* renamed from: taikodom.addon.welcome.WelcomeAddon$b */
    /* compiled from: a */
    class C5097b implements ActionListener {
        private final /* synthetic */ IAddonProperties bJA;

        C5097b(IAddonProperties vWVar) {
            this.bJA = vWVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (this.bJA.getPlayer() != null) {
                this.bJA.getPlayer().dwY();
            }
            WelcomeAddon.this.stop();
        }
    }

    /* renamed from: taikodom.addon.welcome.WelcomeAddon$a */
    class C5096a extends C3428rT<C6958awx> {
        C5096a() {
        }

        /* renamed from: a */
        public void mo321b(C6958awx awx) {
            if (WelcomeAddon.this.f10317nM != null) {
                WelcomeAddon.this.f10317nM.setVisible(true);
            }
        }
    }
}
