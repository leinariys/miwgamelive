package taikodom.addon.party;

import game.script.Actor;
import game.script.player.Player;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.baa.C6200aiQ;
import logic.data.link.C0422Ft;
import logic.data.link.C1092Px;
import logic.res.code.C5663aRz;
import logic.ui.ComponentManager;
import logic.ui.item.Table;
import logic.ui.item.TaskPane;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.popup.DefaultMenuItems;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

@TaikodomAddon("taikodom.addon.party")
/* compiled from: a */
public class PartyAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10205P;
    /* access modifiers changed from: private */
    public Table ehG;
    /* access modifiers changed from: private */
    public aSZ ith;
    /* renamed from: kj */
    public IAddonProperties f10206kj;
    private TaskPane cQI;
    private DefaultTableModel ehH;
    private aGC itg;
    private C6200aiQ<Player> iti;
    private C0422Ft.C0425c itj;
    /* access modifiers changed from: private */
    private C0422Ft.C0424b itk;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10206kj = addonPropertie;
        this.f10205P = this.f10206kj.getPlayer();
        if (this.f10205P != null) {
            m45081Im();
            m45093iG();
            dkf();
            dkh();
            this.f10206kj.aVU().publish(this.ith);
            this.f10205P.mo8320a(C1092Px.dQD, (C6200aiQ<?>) this.iti);
        }
    }

    public void stop() {
    }

    /* renamed from: Im */
    private void m45081Im() {
        this.cQI = (TaskPane) this.f10206kj.bHv().mo16794bQ("party.xml");
        dkg();
    }

    /* renamed from: iG */
    private void m45093iG() {
        this.iti = new C4874f();
        this.itj = new C4875g();
        this.itk = new C4876h();
    }

    private void dkf() {
        /*
        Текстовые команды:
        Пригласить в отряд: / пригласить <имя>
        Удалить из отряда: / kick <имя>
        */
        this.f10206kj.mo2322a("leave", (Action) new C4877i("leave", this.f10206kj.translate("Leave Party"), "leave"));
        this.f10206kj.mo2322a("invite", (Action) new C4870b("invite", this.f10206kj.translate("Invite to party"), "invite"));
        this.f10206kj.mo2322a("kick", (Action) new C4869a("kick", this.f10206kj.translate("Kick from party"), "kick"));
        this.f10206kj.mo2322a("lead", (Action) new C4872d("lead", this.f10206kj.translate("Transfer party leadership"), "lead"));
    }

    private void dkg() {
        this.ehG = this.cQI.mo4915cd("players");
        this.ehG.addMouseListener(new C4871c(this.ehG));
        this.ehH = new C4873e(0, 3);
        this.ehG.setModel(this.ehH);
        this.ehG.getColumnModel().getColumn(0).setCellRenderer(new C4878j());
        this.ehG.getColumnModel().getColumn(1).setCellRenderer(new C4879k());
        this.ehG.getColumnModel().getColumn(2).setCellRenderer(new C4881l());
    }

    private void dkh() {
        this.itg = new C4883m(this.f10206kj.translate("Party"), "party");
        this.itg.mo15602jH(this.f10206kj.translate("Party window, where you can see all player that is in your party."));
        this.ith = new aSZ(this.cQI, this.itg, 800, 2);
    }

    /* renamed from: b */
    private void m45085b(Party gt) {
        if (gt != null) {
            this.ehH.getDataVector().clear();
            for (Player next : gt.aQS()) {
                this.ehH.addRow(new Object[]{next, next, next});
            }
        }
    }

    /* access modifiers changed from: private */
    public void open() {
        Party dxi = this.f10205P.dxi();
        if (dxi != null) {
            dxi.mo8348d(C0422Ft.C0425c.class, this.itj);
            dxi.mo8348d(C0422Ft.C0424b.class, this.itk);
            m45085b(dxi);
        }
    }

    /* access modifiers changed from: private */
    public void close() {
        Party dxi = this.f10205P.dxi();
        if (dxi != null) {
            dxi.mo8355h(C0422Ft.C0425c.class, this.itj);
            dxi.mo8355h(C0422Ft.C0424b.class, this.itk);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m45088c(Party gt, Player aku) {
        if (gt != null && aku != null) {
            m45085b(gt);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m45090d(Party gt, Player aku) {
        if (gt != null && aku != null) {
            m45085b(gt);
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$f */
    /* compiled from: a */
    class C4874f implements C6200aiQ<Player> {
        C4874f() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            PartyAddon.this.f10206kj.aVU().mo13975h(new C0269DS(PartyAddon.this.ith, PartyAddon.this.f10205P.dxk()));
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$g */
    /* compiled from: a */
    class C4875g implements C0422Ft.C0425c {
        private static final long serialVersionUID = 990896662799383770L;

        C4875g() {
        }

        /* renamed from: a */
        public void mo2228a(Party gt, Player aku) {
            PartyAddon.this.m45088c(gt, aku);
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$h */
    /* compiled from: a */
    class C4876h implements C0422Ft.C0424b {
        private static final long serialVersionUID = 990896662799383770L;

        C4876h() {
        }

        /* renamed from: b */
        public void mo2227b(Party gt, Player aku) {
            PartyAddon.this.m45090d(gt, aku);
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$i */
    /* compiled from: a */
    class C4877i extends C6622aqW {
        private static final long serialVersionUID = 8757450812181919295L;

        C4877i(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            PartyAddon.this.f10205P.mo14428ko(false);
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$b */
    /* compiled from: a */
    class C4870b extends C6622aqW {
        private static final long serialVersionUID = 6111065386035509742L;

        C4870b(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1 && !"".equals(objArr[0])) {
                PartyAddon.this.f10205P.dwU().mo21447mN(objArr[0].toString());
            }
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$a */
    class C4869a extends C6622aqW {
        private static final long serialVersionUID = -4208667132236986480L;

        C4869a(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1 && !"".equals(objArr[0])) {
                PartyAddon.this.f10205P.dwU().mo21448mP(objArr[0].toString());
            }
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$d */
    /* compiled from: a */
    class C4872d extends C6622aqW {
        private static final long serialVersionUID = 2216440986834396009L;

        C4872d(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1 && !"".equals(objArr[0])) {
                PartyAddon.this.f10205P.dwU().mo21449mR(objArr[0].toString());
            }
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$c */
    /* compiled from: a */
    class C4871c extends C5492aLk {
        C4871c(JTable jTable) {
            super(jTable);
        }

        /* access modifiers changed from: protected */
        /* renamed from: j */
        public JPopupMenu mo9938j(int i, int i2) {
            if (i < 0 || i2 < 0) {
                return null;
            }
            return DefaultMenuItems.m43036bi((Player) PartyAddon.this.ehG.getModel().getValueAt(i, i2));
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$e */
    /* compiled from: a */
    class C4873e extends DefaultTableModel {
        private static final long serialVersionUID = -6274607578229718593L;

        C4873e(int i, int i2) {
            super(i, i2);
        }

        public boolean isCellEditable(int i, int i2) {
            return false;
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$j */
    /* compiled from: a */
    class C4878j extends C2122cE {
        C4878j() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("playerName");
            if (obj != null) {
                Player aku = (Player) obj;
                nR.setText(aku.getName());
                if (aku.equals(aku.dxi().aQU())) {
                    ComponentManager.getCssHolder(nR).setAttribute("class", "leader");
                } else {
                    ComponentManager.getCssHolder(nR).setAttribute("class", "wing");
                    nR.setIcon((Icon) null);
                }
            }
            return nR;
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$k */
    /* compiled from: a */
    class C4879k extends C2122cE {
        C4879k() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            String str;
            JLabel nR = aur.mo11631nR("place");
            if (obj != null) {
                Player aku = (Player) obj;
                Actor bhE = aku.bhE();
                if (!C1298TD.m9830t(bhE).bFT()) {
                    if (bhE instanceof Station) {
                        str = bhE.getName();
                    } else {
                        str = bhE.azW().mo21665ke().get();
                    }
                    nR.setText(str);
                } else {
                    ((Player) C3582se.m38985a(aku, (C6144ahM<?>) new C4880a())).dyJ();
                }
            }
            return nR;
        }

        /* renamed from: taikodom.addon.party.PartyAddon$k$a */
        class C4880a implements C6144ahM {
            C4880a() {
            }

            /* renamed from: n */
            public void mo1931n(Object obj) {
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$l */
    /* compiled from: a */
    class C4881l extends C2122cE {
        C4881l() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:3:0x000d, code lost:
            r9 = (p001a.C6308akU) r9;
         */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.awt.Component mo17543a(aUR r7, javax.swing.JTable r8, java.lang.Object r9, boolean r10, boolean r11, int r12, int r13) {
            /*
                r6 = this;
                r5 = 1120403456(0x42c80000, float:100.0)
                java.lang.String r0 = "life"
                java.awt.Component r0 = r7.mo11631nR(r0)
                a.aCo r0 = (logic.ui.C5262aCo) r0
                if (r9 != 0) goto L_0x000d
            L_0x000c:
                return r0
            L_0x000d:
                a.akU r9 = (p001a.C6308akU) r9
                a.fA r2 = r9.bQx()
                if (r2 == 0) goto L_0x000c
                a.TD r1 = p001a.C1298TD.m9830t(r2)
                boolean r1 = r1.bFT()
                if (r1 != 0) goto L_0x0054
                a.jR r3 = r2.mo8287zt()
                java.lang.String r1 = "hull"
                java.awt.Component r1 = r0.mo4915cd(r1)
                a.bn r1 = (p001a.C2064bn) r1
                float r4 = r3.mo19923Tx()
                float r4 = r4 * r5
                float r3 = r3.mo19934hh()
                float r3 = r4 / r3
                r1.setValue((float) r3)
                a.gj r2 = r2.mo8288zv()
                java.lang.String r1 = "shield"
                java.awt.Component r1 = r0.mo4915cd(r1)
                a.bn r1 = (p001a.C2064bn) r1
                float r3 = r2.mo19079Tx()
                float r3 = r3 * r5
                float r2 = r2.mo19089hh()
                float r2 = r3 / r2
                r1.setValue((float) r2)
                goto L_0x000c
            L_0x0054:
                taikodom.addon.party.PartyAddon$l$a r1 = new taikodom.addon.party.PartyAddon$l$a
                r1.<init>()
                a.aDJ r1 = p001a.C3582se.m38985a(r9, (taikodom.addon.C6144ahM<?>) r1)
                a.akU r1 = (p001a.C6308akU) r1
                r1.dyJ()
                goto L_0x000c
            */
            throw new UnsupportedOperationException("Method not decompiled: taikodom.addon.party.PartyAddon.C4881l.mo17543a(a.aUR, javax.swing.JTable, java.lang.Object, boolean, boolean, int, int):java.awt.Component");
        }

        /* renamed from: taikodom.addon.party.PartyAddon$l$a */
        class C4882a implements C6144ahM {
            C4882a() {
            }

            /* renamed from: n */
            public void mo1931n(Object obj) {
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }
        }
    }

    /* renamed from: taikodom.addon.party.PartyAddon$m */
    /* compiled from: a */
    class C4883m extends aGC {
        private static final long serialVersionUID = -636902033806027775L;

        C4883m(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
            if (z) {
                PartyAddon.this.open();
            } else {
                PartyAddon.this.close();
            }
        }

        public boolean isEnabled() {
            return (PartyAddon.this.f10205P == null || PartyAddon.this.f10205P.dxi() == null) ? false : true;
        }
    }
}
