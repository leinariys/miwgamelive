package taikodom.addon.eventpanel;

import game.network.message.externalizable.C3689to;
import logic.IAddonSettings;
import logic.aaa.C2327eB;
import logic.aaa.C5476aKu;
import logic.aaa.C5783aaP;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.C2698il;
import logic.ui.item.C2830kk;
import logic.ui.item.Repeater;
import logic.ui.item.StackJ;
import p001a.C3428rT;
import p001a.C6457anN;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@TaikodomAddon("taikodom.addon.eventpanel")
/* compiled from: a */
public class EventPanelAddon implements C2495fo {
    public static final String dJt = "eventPanelButton.submenu.removeItem";
    static final String dJw = "event_panel_blink";
    private static final int dJu = 155;
    private static final int dJv = 120;
    /* renamed from: qw */
    private static final String f9867qw = "eventpanel.xml";
    /* access modifiers changed from: private */

    /* renamed from: SZ */
    public aDX f9868SZ;
    /* access modifiers changed from: private */
    public C2698il aoL;
    /* access modifiers changed from: private */
    public Map<String, C6457anN> dJx;
    private Repeater.C3671a<C6457anN> buV = new C4355g();
    private C3428rT<C5783aaP> bvz;
    private Repeater<C6457anN> clJ;
    private C3428rT<C3689to> dHH;
    private C3428rT<C2327eB> dJA;
    private C3428rT<HudVisibilityAddon.C4534d> dJB;
    private C3428rT<C5476aKu> dJy;
    private int dJz;

    /* renamed from: kj */
    private IAddonProperties f9869kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        int i;
        this.f9869kj = addonPropertie;
        this.dJx = new HashMap();
        this.aoL = (C2698il) addonPropertie.bHv().mo16794bQ(f9867qw);
        m43352a(false, false);
        this.clJ = this.aoL.mo4919ch("repButton");
        this.clJ.mo22250a(this.buV);
        if (addonPropertie.getPlayer().bQB()) {
            i = 155;
        } else {
            i = 120;
        }
        m43358me(i);
        bli();
    }

    private void bli() {
        this.dJy = new C4356h();
        this.dHH = new C4353e();
        this.bvz = new C4354f();
        this.dJA = new C4351c();
        this.dJB = new C4352d();
        this.f9869kj.aVU().mo13965a(C5476aKu.class, this.dJy);
        this.f9869kj.aVU().mo13965a(C3689to.class, this.dHH);
        this.f9869kj.aVU().mo13965a(C5783aaP.class, this.bvz);
        this.f9869kj.aVU().mo13965a(C2327eB.class, this.dJA);
        this.f9869kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.dJB);
    }

    public void stop() {
        this.f9869kj.aVU().mo13964a(this.dJy);
        this.f9869kj.aVU().mo13964a(this.dHH);
        this.f9869kj.aVU().mo13964a(this.bvz);
        this.f9869kj.aVU().mo13964a(this.dJA);
        this.f9869kj.aVU().mo13964a(this.dJB);
    }

    /* renamed from: b */
    public void mo23797b(C5476aKu aku) {
        boolean d;
        C6457anN ann = this.dJx.get(aku.getIdentifier());
        if (ann == null) {
            C6457anN ann2 = new C6457anN(this.f9869kj, aku);
            this.dJx.put(aku.getIdentifier(), ann2);
            this.clJ.mo22248G(ann2);
            ann2.mo14957d(aku);
            blj();
            d = true;
        } else {
            d = ann.mo14957d(aku);
        }
        if (d) {
            m43352a(true, false);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m43355c(C5476aKu aku) {
        if (this.dJx.size() > 0) {
            this.dJx.remove(aku.getIdentifier()).dispose();
            blj();
            if (this.dJx.size() == 0) {
                m43352a(false, false);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: gd */
    public void m43356gd(String str) {
        Iterator<C6457anN> it = this.dJx.values().iterator();
        while (true) {
            if (it.hasNext()) {
                C6457anN next = it.next();
                if (next.getIdentifier().equals(str)) {
                    this.dJx.remove(str);
                    next.dispose();
                    break;
                }
            } else {
                break;
            }
        }
        blj();
        if (this.dJx.size() == 0) {
            m43352a(false, false);
        }
    }

    /* access modifiers changed from: private */
    public void blj() {
        this.aoL.pack();
        this.aoL.setLocation((this.f9869kj.bHv().getScreenWidth() - this.aoL.getWidth()) / 2, this.dJz);
    }

    /* access modifiers changed from: private */
    /* renamed from: me */
    public void m43358me(int i) {
        this.dJz = this.f9869kj.bHv().getScreenHeight() - i;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43352a(boolean z, boolean z2) {
        if (!z2) {
            this.aoL.setVisible(z);
        } else if (z) {
            this.aoL.setVisible(true);
            if (this.f9868SZ != null) {
                this.f9868SZ.kill();
            }
            this.f9868SZ = new aDX(this.aoL, "[0..255] dur 500 strong_out", C2830kk.asS, new C4349a());
        } else {
            if (this.f9868SZ != null) {
                this.f9868SZ.kill();
            }
            this.f9868SZ = new aDX(this.aoL, "[255..0] dur 500 strong_in;", C2830kk.asS, new C4350b());
        }
    }

    /* renamed from: iL */
    private boolean m43357iL() {
        return this.f9869kj != null && this.f9869kj.ala().aLS().mo22273iL();
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$g */
    /* compiled from: a */
    class C4355g implements Repeater.C3671a<C6457anN> {
        C4355g() {
        }

        /* renamed from: a */
        public void mo843a(C6457anN ann, Component component) {
            if (component instanceof C2698il) {
                ann.mo14955a((StackJ) component);
            }
        }
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$h */
    /* compiled from: a */
    class C4356h extends C3428rT<C5476aKu> {
        C4356h() {
        }

        /* renamed from: e */
        public void mo321b(C5476aKu aku) {
            if (aku.djc().equals(C5476aKu.C1809a.ADD)) {
                EventPanelAddon.this.mo23797b(aku);
            } else if (aku.djc().equals(C5476aKu.C1809a.REMOVE)) {
                EventPanelAddon.this.m43355c(aku);
            } else if (aku.djc().equals(C5476aKu.C1809a.CLEAR)) {
                EventPanelAddon.this.m43356gd(aku.getIdentifier());
            }
        }
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$e */
    /* compiled from: a */
    class C4353e extends C3428rT<C3689to> {
        C4353e() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            for (C6457anN dcO : EventPanelAddon.this.dJx.values()) {
                dcO.dcO();
            }
        }
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$f */
    /* compiled from: a */
    class C4354f extends C3428rT<C5783aaP> {
        C4354f() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            C5783aaP.C1841a bMO = aap.bMO();
            if (bMO.equals(C5783aaP.C1841a.DOCKED)) {
                EventPanelAddon.this.m43358me(155);
            } else if (bMO.equals(C5783aaP.C1841a.FLYING) || bMO.equals(C5783aaP.C1841a.ONI)) {
                EventPanelAddon.this.m43358me(120);
            }
            EventPanelAddon.this.blj();
            EventPanelAddon.this.m43352a(true, true);
        }
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$c */
    /* compiled from: a */
    class C4351c extends C3428rT<C2327eB> {
        C4351c() {
        }

        /* renamed from: a */
        public void mo321b(C2327eB eBVar) {
            if (!eBVar.isVisible() && EventPanelAddon.this.aoL != null) {
                EventPanelAddon.this.m43352a(false, true);
            }
        }
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$d */
    /* compiled from: a */
    class C4352d extends C3428rT<HudVisibilityAddon.C4534d> {
        C4352d() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            if (EventPanelAddon.this.aoL != null) {
                EventPanelAddon.this.m43352a(dVar.isVisible(), true);
            }
        }
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$a */
    class C4349a implements C0454GJ {
        C4349a() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            EventPanelAddon.this.f9868SZ = null;
        }
    }

    /* renamed from: taikodom.addon.eventpanel.EventPanelAddon$b */
    /* compiled from: a */
    class C4350b implements C0454GJ {
        C4350b() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            EventPanelAddon.this.f9868SZ = null;
            EventPanelAddon.this.aoL.setVisible(false);
        }
    }
}
