package taikodom.addon.highlight;

import game.network.message.externalizable.C3689to;
import logic.IAddonSettings;
import logic.aaa.C2123cF;
import logic.swing.C0454GJ;
import logic.ui.item.InternalFrame;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;

@TaikodomAddon("taikodom.addon.highlight")
/* compiled from: a */
public class HighlightAddon implements C2495fo {
    private static final int fYX = 20;
    private static final int fYY = 10;
    private static final int fYZ = 1000;
    private static final int fZa = 1500;
    private static final int fZb = 10;
    /* access modifiers changed from: private */
    public JComponent component;
    /* access modifiers changed from: private */
    public Rectangle2D.Float fZc;
    /* access modifiers changed from: private */
    public Rectangle fZd;
    /* access modifiers changed from: private */
    public Timer fZe;
    /* access modifiers changed from: private */
    public Rectangle2D.Float fZf;
    /* access modifiers changed from: private */
    public Timer fZg;
    /* access modifiers changed from: private */
    public Rectangle fZh;
    /* access modifiers changed from: private */
    public C4449a fZj = C4449a.IDLE;
    /* renamed from: kj */
    public IAddonProperties f9899kj;
    /* renamed from: nM */
    public InternalFrame f9900nM;
    /* access modifiers changed from: private */
    private C3428rT<C2123cF> fZi;
    /* access modifiers changed from: private */
    private Rectangle fZk;
    /* renamed from: nP */
    private C3428rT<C3689to> f9901nP;

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m43637a(MouseEvent mouseEvent) {
        if (this.component != null) {
            int x = (int) (((double) mouseEvent.getX()) + this.fZf.getX());
            int y = (int) (((double) mouseEvent.getY()) + this.fZf.getY());
            if (this.fZd.contains(x, y)) {
                this.component.dispatchEvent(new MouseEvent(this.component, mouseEvent.getID(), mouseEvent.getWhen(), mouseEvent.getModifiers(), x - this.fZd.x, y - this.fZd.y, x, y, mouseEvent.getClickCount(), mouseEvent.isPopupTrigger(), mouseEvent.getButton()));
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void ciq() {
        this.fZe.stop();
        this.fZg.stop();
        this.component = null;
        if (this.f9900nM != null) {
            this.f9900nM.mo20868b(false, (C0454GJ) new C4454e());
        }
    }

    /* renamed from: iG */
    private void m43647iG() {
        this.f9901nP = new C4453d();
        this.fZi = new C4451c();
        this.f9900nM.addMouseListener(new C4450b());
    }

    private void bVR() {
        this.fZe = new Timer(Math.round(100.0f), new C4456g());
        this.fZg = new Timer(fZa, new C4455f());
        this.fZg.setRepeats(false);
    }

    /* access modifiers changed from: private */
    public void cir() {
        this.fZd = this.f9899kj.bHv().mo16796c(this.component);
        int i = (int) (((float) this.fZd.width) + 40.0f);
        int i2 = (int) (((float) this.fZd.height) + 40.0f);
        int i3 = (int) (((float) this.fZd.width) + 20.0f);
        int i4 = (int) (((float) this.fZd.height) + 20.0f);
        this.fZk = new Rectangle((int) (((float) this.fZd.x) - (((float) (i - this.fZd.width)) / 2.0f)), (int) (((float) this.fZd.y) - (((float) (i2 - this.fZd.height)) / 2.0f)), i, i2);
        this.fZh = new Rectangle((int) (((float) this.fZd.x) - (((float) (i3 - this.fZd.width)) / 2.0f)), (int) (((float) this.fZd.y) - (((float) (i4 - this.fZd.height)) / 2.0f)), i3, i4);
        this.fZf = new Rectangle2D.Float((float) this.fZk.x, (float) this.fZk.y, (float) this.fZk.width, (float) this.fZk.height);
        this.fZc = new Rectangle2D.Float(((float) (this.fZh.getX() - this.fZk.getX())) / 10.0f, ((float) (this.fZh.getY() - this.fZk.getY())) / 10.0f, ((float) (this.fZh.getWidth() - this.fZk.getWidth())) / 10.0f, ((float) (this.fZh.getHeight() - this.fZk.getHeight())) / 10.0f);
        this.f9900nM.setBounds(this.fZk);
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9899kj = addonPropertie;
        this.f9900nM = this.f9899kj.bHv().mo16792bN("highlight.xml");
        this.f9900nM.setAlwaysOnTop(true);
        m43647iG();
        bVR();
        this.f9899kj.aVU().mo13965a(C3689to.class, this.f9901nP);
        this.f9899kj.aVU().mo13965a(C2123cF.class, this.fZi);
        for (C2123cF k : this.f9899kj.aVU().mo13967b(C2123cF.class)) {
            this.fZi.updateInfo(k);
        }
    }

    public void stop() {
        this.fZe.stop();
        this.fZg.stop();
        this.f9899kj.aVU().mo13964a(this.f9901nP);
        this.f9899kj.aVU().mo13964a(this.fZi);
    }

    /* renamed from: taikodom.addon.highlight.HighlightAddon$a */
    private enum C4449a {
        IDLE,
        RESIZING,
        WAITING_FADE
    }

    /* renamed from: taikodom.addon.highlight.HighlightAddon$e */
    /* compiled from: a */
    class C4454e implements C0454GJ {
        C4454e() {
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            HighlightAddon.this.fZj = C4449a.IDLE;
        }
    }

    /* renamed from: taikodom.addon.highlight.HighlightAddon$d */
    /* compiled from: a */
    class C4453d extends C3428rT<C3689to> {
        C4453d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (HighlightAddon.this.f9900nM != null && HighlightAddon.this.f9900nM.isVisible()) {
                HighlightAddon.this.ciq();
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.highlight.HighlightAddon$c */
    /* compiled from: a */
    class C4451c extends C3428rT<C2123cF> {
        C4451c() {
        }

        /* renamed from: a */
        public void mo321b(C2123cF cFVar) {
            JComponent component = cFVar.getComponent();
            if (component instanceof JComponent) {
                HighlightAddon.this.ciq();
                HighlightAddon.this.component = component;
                HighlightAddon.this.cir();
                HighlightAddon.this.f9900nM.mo20868b(true, (C0454GJ) new C4452a());
            }
        }

        /* renamed from: taikodom.addon.highlight.HighlightAddon$c$a */
        class C4452a implements C0454GJ {
            C4452a() {
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                HighlightAddon.this.fZj = C4449a.RESIZING;
                HighlightAddon.this.fZe.restart();
            }
        }
    }

    /* renamed from: taikodom.addon.highlight.HighlightAddon$b */
    /* compiled from: a */
    class C4450b extends MouseAdapter {
        C4450b() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (!HighlightAddon.this.m43637a(mouseEvent)) {
                HighlightAddon.this.ciq();
            }
        }

        public void mousePressed(MouseEvent mouseEvent) {
            if (!HighlightAddon.this.m43637a(mouseEvent)) {
                HighlightAddon.this.ciq();
            }
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            boolean unused = HighlightAddon.this.m43637a(mouseEvent);
        }
    }

    /* renamed from: taikodom.addon.highlight.HighlightAddon$g */
    /* compiled from: a */
    class C4456g implements ActionListener {
        C4456g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (HighlightAddon.this.component == null || !HighlightAddon.this.component.isShowing() || C4449a.IDLE.equals(HighlightAddon.this.fZj)) {
                HighlightAddon.this.ciq();
            } else if (!HighlightAddon.this.fZd.equals(HighlightAddon.this.f9899kj.bHv().mo16796c(HighlightAddon.this.component))) {
                HighlightAddon.this.fZg.stop();
                HighlightAddon.this.fZj = C4449a.RESIZING;
                HighlightAddon.this.cir();
            } else {
                if (C4449a.RESIZING.equals(HighlightAddon.this.fZj)) {
                    HighlightAddon.this.fZf.setRect(HighlightAddon.this.fZf.x + HighlightAddon.this.fZc.x, HighlightAddon.this.fZf.y + HighlightAddon.this.fZc.y, HighlightAddon.this.fZf.width + HighlightAddon.this.fZc.width, HighlightAddon.this.fZf.height + HighlightAddon.this.fZc.height);
                    if (HighlightAddon.this.fZf.width <= ((float) HighlightAddon.this.fZh.width) || HighlightAddon.this.fZf.height <= ((float) HighlightAddon.this.fZh.height)) {
                        HighlightAddon.this.fZj = C4449a.WAITING_FADE;
                        HighlightAddon.this.fZg.restart();
                        return;
                    }
                    HighlightAddon.this.f9900nM.setBounds(Math.round(HighlightAddon.this.fZf.x), Math.round(HighlightAddon.this.fZf.y), Math.round(HighlightAddon.this.fZf.width), Math.round(HighlightAddon.this.fZf.height));
                }
                HighlightAddon.this.f9900nM.moveToFront();
            }
        }
    }

    /* renamed from: taikodom.addon.highlight.HighlightAddon$f */
    /* compiled from: a */
    class C4455f implements ActionListener {
        C4455f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            HighlightAddon.this.ciq();
        }
    }
}
