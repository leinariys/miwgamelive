package taikodom.addon.stationmarket;

import logic.IAddonSettings;
import p001a.C0716KM;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

@TaikodomAddon("taikodom.addon.stationmarket")
/* compiled from: a */
public class StationMarketAddon implements C2495fo {
    private C0716KM cYm;

    /* renamed from: kj */
    private IAddonProperties f10277kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10277kj = addonPropertie;
        this.cYm = new C0716KM(this.f10277kj, this);
    }

    public void stop() {
        this.cYm.dispose();
    }
}
