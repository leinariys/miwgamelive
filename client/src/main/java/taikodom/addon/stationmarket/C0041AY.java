package taikodom.addon.stationmarket;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.network.message.externalizable.C6018aeq;
import game.script.newmarket.CommercialOrderInfo;
import game.script.player.Player;
import game.script.ship.Station;
import logic.aWa;
import logic.aaa.C5783aaP;
import logic.render.GUIModule;
import logic.ui.item.C5923acz;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

@TaikodomAddon("taikodom.addon.stationmarket")
/* renamed from: a.AY */
/* compiled from: a */
public class C0041AY implements aWa {

    /* renamed from: kj */
    public IAddonProperties f66kj;
    /* renamed from: nM */
    public InternalFrame f67nM;
    /* renamed from: P */
    private Player f65P = this.f66kj.getPlayer();
    private C3428rT<C5783aaP> aoN;
    private C2495fo apa;
    private C6622aqW dcE;
    private C0716KM hmA;
    private C3740uR hmB;
    private C5923acz hmC;
    private C5923acz hmD;
    /* access modifiers changed from: private */
    private JButton hmE;
    /* access modifiers changed from: private */
    private JButton hmF;
    /* renamed from: wz */
    private C3428rT<C3689to> f68wz;

    public C0041AY(IAddonProperties vWVar, C0716KM km, C2495fo foVar) {
        this.f66kj = vWVar;
        this.apa = foVar;
        this.hmA = km;
        this.hmB = new C3740uR(this.f66kj);
        this.dcE = new C0042a("stationmarket", this.f66kj.mo11832V("tooltip", "Station Market"), "stationmarket");
        this.dcE.putValue("ShortDescription", this.f66kj.mo11832V("tooltip", "Station Market"));
        this.f68wz = new C0045d();
        this.aoN = new C0046e();
        this.f66kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f66kj.aVU().mo13965a(C3689to.class, this.f68wz);
        this.f66kj.aVU().publish(new C5344aFs(this.dcE, 900, true));
        this.f66kj.mo2317P(this);
    }

    public void stop() {
        if (this.f67nM != null) {
            this.f67nM.destroy();
            this.f67nM = null;
        }
    }

    public void open() {
        if (this.f67nM == null) {
            m413Im();
        } else if (this.f67nM.isVisible()) {
            this.f67nM.setVisible(false);
            this.f66kj.aVU().mo13974g(new C6018aeq("Station Market", ""));
            this.f66kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            return;
        }
        this.f66kj.aVU().publish(new C6018aeq("Station Market", ""));
        this.f66kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, getClass()));
        GUIModule bhd = C5916acs.getSingolton().getGuiModule();
        this.f67nM.setLocation((bhd.getScreenWidth() / 2) - (this.f67nM.getWidth() / 2), ((bhd.getScreenHeight() / 2) - (this.f67nM.getHeight() / 2)) - 50);
        this.f67nM.setVisible(true);
    }

    /* renamed from: Im */
    private void m413Im() {
        this.f67nM = this.f66kj.bHv().mo16795c(C0041AY.class, "stationmarket.xml");
        this.f67nM.addComponentListener(new C0043b());
        this.hmC = this.f67nM.mo4915cd("buyItemsTree");
        this.hmD = this.f67nM.mo4915cd("sellItemsTree");
        this.hmA.bdd();
        this.hmB.mo22381a(this.hmC);
        this.hmB.mo22384b(this.hmD);
        this.hmE = this.f67nM.mo4913cb("buyButton");
        this.hmF = this.f67nM.mo4913cb("sellButton");
        this.hmE.addMouseListener(new C0044c());
    }

    /* access modifiers changed from: private */
    public void cMM() {
        cMN();
        cMO();
    }

    private void cMN() {
        this.hmB.mo22382a(this.hmC, (List<? extends CommercialOrderInfo>) this.hmA.bdf());
    }

    private void cMO() {
        this.hmB.mo22382a(this.hmD, (List<? extends CommercialOrderInfo>) this.hmA.bde());
    }

    public void bjh() {
        cMN();
    }

    public void bji() {
        cMO();
    }

    /* renamed from: a.AY$a */
    class C0042a extends C6622aqW {


        C0042a(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isEnabled() {
            if (C0041AY.this.f66kj.ala() != null && (C0041AY.this.f66kj.ala().getPlayer().bhE() instanceof Station)) {
                return true;
            }
            return false;
        }

        public boolean isActive() {
            return C0041AY.this.f67nM != null && C0041AY.this.f67nM.isVisible();
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            C0041AY.this.open();
        }
    }

    /* renamed from: a.AY$d */
    /* compiled from: a */
    class C0045d extends C3428rT<C3689to> {
        C0045d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (C0041AY.this.f67nM != null && C0041AY.this.f67nM.isVisible()) {
                toVar.adC();
                C0041AY.this.f67nM.setVisible(false);
            }
        }
    }

    /* renamed from: a.AY$e */
    /* compiled from: a */
    class C0046e extends C3428rT<C5783aaP> {

        /* renamed from: Zn */
        private static /* synthetic */ int[] f69Zn;

        C0046e() {
        }

        /* renamed from: BK */
        static /* synthetic */ int[] m421BK() {
            int[] iArr = f69Zn;
            if (iArr == null) {
                iArr = new int[C5783aaP.C1841a.values().length];
                try {
                    iArr[C5783aaP.C1841a.DOCKED.ordinal()] = 1;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C5783aaP.C1841a.FLYING.ordinal()] = 2;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C5783aaP.C1841a.JUMPING.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C5783aaP.C1841a.ONI.ordinal()] = 4;
                } catch (NoSuchFieldError e4) {
                }
                f69Zn = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            switch (m421BK()[aap.bMO().ordinal()]) {
                case 4:
                    if (C0041AY.this.f67nM != null && C0041AY.this.f67nM.isVisible()) {
                        C0041AY.this.f67nM.setVisible(false);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: a.AY$b */
    /* compiled from: a */
    class C0043b extends ComponentAdapter {
        C0043b() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            C0041AY.this.f66kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            C0041AY.this.f67nM = null;
        }
    }

    /* renamed from: a.AY$c */
    /* compiled from: a */
    class C0044c extends MouseAdapter {
        C0044c() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            C0041AY.this.cMM();
        }
    }
}
