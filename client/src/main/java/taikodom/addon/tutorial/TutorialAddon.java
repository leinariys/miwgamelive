package taikodom.addon.tutorial;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C6018aeq;
import logic.IAddonSettings;
import logic.aaa.C0554Hi;
import logic.aaa.C5476aKu;
import logic.aaa.C5685aSv;
import logic.sql.C3580sc;
import logic.sql.C5649aRl;
import logic.sql.C5878acG;
import logic.sql.aFJ;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import logic.ui.item.Scrollable;
import logic.ui.item.TextArea;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

@TaikodomAddon("taikodom.addon.tutorial")
@Deprecated
/* compiled from: a */
public class TutorialAddon implements C1215Rv, C2495fo {
    public static final String hdo = "TutorialAddon";
    private static final String hdn = "tutorial";
    private static final String hdp = "tutorials.done";
    private static final String hdq = "tutorials.onEventPanel";
    private static final String hdr = "status";
    private static final String hds = "close_confirmation";
    private static final String hdt = "tutorial_rule_complete";
    /* renamed from: qw */
    private static final String f10291qw = "tutorial.xml";
    private static /* synthetic */ int[] hdJ = null;
    /* access modifiers changed from: private */
    public final List<C6124ags<C6018aeq>> hdu = new ArrayList();
    /* access modifiers changed from: private */
    public Tutorial hdz = null;
    /* renamed from: kj */
    public IAddonProperties f10293kj;
    /* renamed from: Af */
    private String f10292Af;
    private C5685aSv aoK;
    private C2698il aoL;
    private JButton aoP;
    private C6622aqW dcE;
    private JButton fvi;
    private JButton hdA;
    private JButton hdB;
    private TextArea hdC;
    private TextArea hdD;
    private String hdE = hdt;
    private C6124ags<C6018aeq> hdF;
    private JLabel hdG;
    private C6124ags<C0554Hi> hdH;
    private Scrollable hdI;
    private List<Tutorial> hdv;
    private ArrayList<Integer> hdw;
    private String hdx;
    /* access modifiers changed from: private */
    private int hdy = -1;

    static /* synthetic */ int[] cHL() {
        int[] iArr = hdJ;
        if (iArr == null) {
            iArr = new int[Tutorial.C1922a.values().length];
            try {
                iArr[Tutorial.C1922a.END_OF_TUTORIAL.ordinal()] = 6;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Tutorial.C1922a.FIRST_PAGE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Tutorial.C1922a.LAST_PAGE.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[Tutorial.C1922a.LAST_PAGE_WITH_CONFIRM.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[Tutorial.C1922a.NORMAL_PAGE.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[Tutorial.C1922a.ONE_PAGE.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            hdJ = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10293kj = addonPropertie;
        this.f10292Af = String.valueOf(addonPropertie.getPlayer().cWm());
        this.dcE = new C5069e("", "", hdn);
        this.dcE = new C5068d(hdn, "Tutorial", hdn);
        this.hdF = new C5071g();
        this.hdH = new C5070f();
        addonPropertie.aVU().mo13965a(C6018aeq.class, this.hdF);
        addonPropertie.aVU().mo13965a(C0554Hi.class, this.hdH);
        cHD();
        if (addonPropertie.getPlayer().dwW()) {
            cHG();
        } else {
            cHB();
        }
        this.aoK = new C5685aSv("monitor", addonPropertie, this);
    }

    public void stop() {
        if (this.aoL != null) {
            this.aoL.destroy();
        }
        cHI();
        this.f10293kj.aVU().mo13964a(this.hdF);
        this.f10293kj.aVU().mo13964a(this.hdH);
    }

    private void cHx() {
        boolean z;
        boolean z2;
        boolean z3 = false;
        TutorialPage cjj = this.hdz.cjj();
        if (cjj != null) {
            cHI();
            for (Rule iVar : cjj.cdV()) {
                C5073i iVar2 = new C5073i(iVar, cjj);
                this.f10293kj.aVU().mo13965a(C6018aeq.class, iVar2);
                this.hdu.add(iVar2);
            }
            m45661h(cjj);
            this.hdD.setText(cjj.cdT().toString());
            this.hdI.getVerticalScrollBar().setValue(0);
            if (Tutorial.C1922a.FIRST_PAGE.equals(cjj.ceg()) || Tutorial.C1922a.ONE_PAGE.equals(cjj.ceg())) {
                z = true;
            } else {
                z = false;
            }
            m45662hD(z);
            if (Tutorial.C1922a.LAST_PAGE.equals(cjj.ceg()) || Tutorial.C1922a.ONE_PAGE.equals(cjj.ceg())) {
                z2 = true;
            } else {
                z2 = false;
            }
            m45663hE(z2);
            JButton jButton = this.fvi;
            if (!cjj.cee()) {
                z3 = true;
            }
            jButton.setEnabled(z3);
            this.hdG.setText(String.valueOf(this.hdz.cjt()) + C0147Bi.SEPARATOR + this.hdz.cjv());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: wN */
    public void m45668wN(int i) {
        if (this.hdv == null || i < 0 || i >= this.hdv.size()) {
            this.hdy = -1;
        } else if (this.hdy >= 0) {
            cHC();
            m45668wN(i);
        } else {
            Tutorial ajv = this.hdv.get(i);
            if (Tutorial.C1923b.DONE.equals(ajv.cjn())) {
                this.hdy = -1;
                return;
            }
            this.hdy = i;
            this.hdz = ajv;
            this.hdz.aMn();
            this.hdz.mo14102b(Tutorial.C1923b.VISIBLE);
            this.f10293kj.aVU().publish(this.aoK);
            cHx();
            this.f10293kj.alb().mo2066a((C5878acG) new C5649aRl(this.f10293kj.getPlayer(), this.hdz.getHandle()));
        }
    }

    /* renamed from: Im */
    private void m45643Im() {
        this.hdE = ComponentManager.getCssHolder(this.aoL).getAttribute("sound");
        if (this.hdE == null) {
            this.hdE = hdt;
        }
        this.fvi = this.aoL.mo4913cb("next-button");
        this.fvi.setText(">");
        this.hdA = this.aoL.mo4913cb("prev-button");
        this.hdA.setText("<");
        this.aoP = this.aoL.mo4913cb("cancel-button");
        this.hdB = this.aoL.mo4913cb("minimize-button");
        this.hdC = this.aoL.mo4915cd("content-title");
        this.hdG = this.aoL.mo4917cf("pages");
        this.hdD = this.aoL.mo4915cd("content");
        this.hdI = this.aoL.mo4915cd("content-scroll");
        this.fvi.addActionListener(new C5072h());
        this.hdA.addActionListener(new C5065b());
        this.aoP.addActionListener(new C5066c());
        this.hdB.addActionListener(new C5064a());
    }

    /* access modifiers changed from: private */
    public void cHy() {
        this.hdw.remove(Integer.valueOf(this.hdy));
        this.hdz.mo14102b(Tutorial.C1923b.DONE);
        m45651aH(hdp, cHJ());
        this.f10293kj.aVU().mo13974g(this.aoK);
        this.hdy = -1;
        this.hdz = null;
        cHI();
    }

    /* access modifiers changed from: private */
    public void cHz() {
        switch (cHL()[this.hdz.cjf().ordinal()]) {
            case 6:
                this.f10293kj.alb().mo2066a((C5878acG) new C3580sc(this.f10293kj.getPlayer(), this.hdz.getHandle()));
                cHy();
                return;
            default:
                cHx();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void cHA() {
        this.hdz.cjh();
        cHx();
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m45658g(TutorialPage ajq) {
        if (!ajq.cec()) {
            cHz();
            return;
        }
        if (Tutorial.C1922a.LAST_PAGE_WITH_CONFIRM.equals(ajq)) {
            m45663hE(true);
        }
        this.hdD.setText(ajq.cdX().toString());
        this.fvi.setEnabled(true);
    }

    private void cHB() {
        if (this.hdw != null && this.hdw.size() > 0) {
            Iterator<Integer> it = this.hdw.iterator();
            while (it.hasNext()) {
                m45666l(it.next().intValue(), true);
            }
        }
    }

    public void cHC() {
        if (this.aoL == null) {
            this.hdy = -1;
            return;
        }
        this.f10293kj.aVU().mo13974g(this.aoK);
        this.hdv.get(this.hdy).mo14102b(Tutorial.C1923b.MINIMIZED);
        m45651aH(hdp, cHJ());
        m45666l(this.hdy, false);
        this.hdy = -1;
        cHI();
    }

    /* access modifiers changed from: private */
    /* renamed from: aH */
    public void m45651aH(String str, String str2) {
        this.f10293kj.mo11845e(String.valueOf(this.f10292Af) + "." + str, str2, hdo);
    }

    /* access modifiers changed from: private */
    /* renamed from: r */
    public String m45667r(String str) {
        return this.f10293kj.mo11831U(String.valueOf(this.f10292Af) + "." + str, hdo);
    }

    private boolean cHD() {
        String r = m45667r(hdr);
        if (r != null && (C5074j.CANCELED.toString().equals(r) || C5074j.DONE.toString().equals(r))) {
            return false;
        }
        List<Tutorial> aKO = this.f10293kj.ala().aKO();
        if (aKO.size() == 0) {
            return false;
        }
        this.hdv = new ArrayList();
        this.hdx = m45667r(hdp);
        if (this.hdx == null) {
            this.hdx = "";
        }
        List<String> kY = m45665kY(this.hdx);
        this.hdw = new ArrayList<>();
        List<String> kY2 = m45665kY(m45667r(hdq));
        if (kY.size() == aKO.size()) {
            m45651aH(hdr, C5074j.DONE.toString());
            return false;
        }
        int i = 0;
        for (Tutorial next : aKO) {
            if (!kY.contains(next.getHandle())) {
                if (kY2.contains(next.getHandle())) {
                    this.hdw.add(Integer.valueOf(i));
                }
                i++;
                this.hdv.add(next);
            }
        }
        return true;
    }

    private void cHE() {
        cHI();
        m45651aH(hdp, "");
        m45651aH(hdq, "");
        if (this.hdw != null) {
            this.hdw.clear();
        }
        if (this.hdv != null) {
            this.hdv.clear();
        }
        this.hdy = -1;
    }

    /* access modifiers changed from: private */
    public void cHF() {
        m45651aH(hdr, C5074j.CANCELED.toString());
        cHE();
        this.f10293kj.aVU().mo13975h(new C5476aKu(C5476aKu.C1809a.CLEAR, hdn));
        if (this.hdz != null) {
            cHy();
        }
    }

    private void cHG() {
        m45651aH(hdr, "");
        cHE();
        cHD();
    }

    /* access modifiers changed from: private */
    public void cHH() {
        cHG();
        this.f10293kj.aVU().mo13975h(new C5476aKu(C5476aKu.C1809a.CLEAR, hdn));
        List<Tutorial> aKO = this.f10293kj.ala().aKO();
        if (aKO.size() != 0) {
            for (Tutorial reset : aKO) {
                reset.reset();
            }
            if (this.hdz != null) {
                cHy();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public int m45652b(C6018aeq aeq) {
        if (this.hdv == null) {
            return -1;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.hdv.size()) {
                return -1;
            }
            if (this.hdv.get(i2).mo14100at(aeq.getEvent(), aeq.bUu())) {
                Tutorial.C1923b cjn = this.hdv.get(i2).cjn();
                if (cjn == null || Tutorial.C1923b.INVISIBLE.equals(cjn)) {
                    return i2;
                }
                return -1;
            }
            i = i2 + 1;
        }
    }

    private void cHI() {
        for (C6124ags<C6018aeq> a : this.hdu) {
            this.f10293kj.aVU().mo13964a(a);
        }
        this.hdu.clear();
    }

    /* renamed from: kY */
    private List<String> m45665kY(String str) {
        if (str == null || "".equals(str)) {
            return new ArrayList();
        }
        return Arrays.asList(str.split(";"));
    }

    private String cHJ() {
        String str = "";
        for (Tutorial next : this.hdv) {
            if (Tutorial.C1923b.DONE.equals(next.cjn())) {
                str = String.valueOf(str) + next.getHandle() + ";";
            }
        }
        return String.valueOf(this.hdx) + str;
    }

    private String cHK() {
        String str = "";
        Iterator<Integer> it = this.hdw.iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            str = String.valueOf(str2) + this.hdv.get(it.next().intValue()).getHandle() + ";";
        }
    }

    /* renamed from: h */
    private void m45661h(TutorialPage ajq) {
        int i = 18;
        String i18NString = ajq.mo14215rP().toString();
        if ("".equals(i18NString)) {
            i18NString = this.hdv.get(this.hdy).mo14124rP().toString();
        }
        if (i18NString.length() > 36 && i18NString.length() > 18) {
            int lastIndexOf = i18NString.lastIndexOf(32, 18) + 1;
            if (lastIndexOf != 0) {
                i = lastIndexOf;
            }
            i18NString = String.valueOf(i18NString.substring(0, i)) + 10 + i18NString.substring(i);
        }
        if (this.hdC != null) {
            this.hdC.setText(i18NString);
        }
    }

    /* renamed from: hD */
    private void m45662hD(boolean z) {
        this.hdA.setEnabled(!z);
    }

    /* renamed from: hE */
    private void m45663hE(boolean z) {
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m45664k(int i, boolean z) {
        Tutorial ajv = this.hdv.get(i);
        if (this.hdv != null && i >= 0 && i < this.hdv.size() && !Tutorial.C1923b.DONE.equals(ajv.cjn())) {
            if (!this.hdw.contains(Integer.valueOf(i))) {
                this.hdw.add(Integer.valueOf(i));
                m45651aH(hdq, cHK());
            }
            if (!ajv.bXn()) {
                m45666l(i, z);
            } else if (!Tutorial.C1923b.MINIMIZED.equals(ajv.cjn())) {
                m45668wN(i);
            }
        }
    }

    /* renamed from: l */
    private void m45666l(int i, boolean z) {
        Tutorial ajv = this.hdv.get(i);
        if (ajv != null) {
            this.f10293kj.aVU().mo13975h(new C5476aKu(hdn, this.dcE, new C6274ajm(Integer.valueOf(i), ajv.mo14124rP().toString(), (Object) null), z));
            ajv.mo14102b(Tutorial.C1923b.MINIMIZED);
        }
    }

    public String getTitle() {
        return this.f10293kj.translate("Tutorial");
    }

    /* renamed from: Io */
    public String mo5290Io() {
        return f10291qw;
    }

    /* renamed from: a */
    public void mo5291a(C2698il ilVar) {
        this.aoL = ilVar;
        m45643Im();
    }

    public void hide() {
    }

    public void show() {
    }

    public void update() {
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$j */
    /* compiled from: a */
    private enum C5074j {
        NORMAL,
        DONE,
        CANCELED
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$e */
    /* compiled from: a */
    class C5069e extends C6622aqW {


        C5069e(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length > 0) {
                TutorialAddon.this.m45668wN(Integer.valueOf(objArr[0].toString()).intValue());
            }
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$d */
    /* compiled from: a */
    class C5068d extends C6622aqW {


        C5068d(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length > 0) {
                TutorialAddon.this.m45668wN(Integer.valueOf(objArr[0].toString()).intValue());
            }
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$g */
    /* compiled from: a */
    class C5071g extends C6124ags<C6018aeq> {
        C5071g() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6018aeq aeq) {
            int a = TutorialAddon.this.m45652b(aeq);
            if (a >= 0) {
                TutorialAddon.this.m45664k(a, true);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$f */
    /* compiled from: a */
    class C5070f extends C6124ags<C0554Hi> {
        C5070f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0554Hi hi) {
            if (C0554Hi.C0555a.RESET.equals(hi.aRw())) {
                TutorialAddon.this.cHH();
                return false;
            } else if (!C0554Hi.C0555a.CANCEL.equals(hi.aRw())) {
                return false;
            } else {
                TutorialAddon.this.cHF();
                return false;
            }
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$i */
    /* compiled from: a */
    class C5073i extends C6124ags<C6018aeq> {
        private final /* synthetic */ Rule cqk;
        private final /* synthetic */ TutorialPage cql;

        C5073i(Rule agr, TutorialPage ajq) {
            this.cqk = agr;
            this.cql = ajq;
        }

        /* renamed from: a */
        public boolean updateInfo(C6018aeq aeq) {
            if (this.cqk.mo13465aq(aeq.getEvent(), aeq.bUu())) {
                TutorialAddon.this.f10293kj.aVU().mo13964a(this);
                TutorialAddon.this.hdu.remove(this);
                this.cqk.mo13467ek(false);
                if (this.cql.isComplete()) {
                    TutorialAddon.this.m45658g(this.cql);
                }
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$h */
    /* compiled from: a */
    class C5072h implements ActionListener {
        C5072h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            TutorialAddon.this.cHz();
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$b */
    /* compiled from: a */
    class C5065b implements ActionListener {
        C5065b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            TutorialAddon.this.cHA();
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$c */
    /* compiled from: a */
    class C5066c implements ActionListener {
        C5066c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if ("true".equals(TutorialAddon.this.m45667r(TutorialAddon.hds))) {
                TutorialAddon.this.cHy();
                return;
            }
            ((MessageDialogAddon) TutorialAddon.this.f10293kj.mo11830U(MessageDialogAddon.class)).mo24307a(TutorialAddon.this.f10293kj.translate("Are you sure you want to cancel this tutorial?"), (aEP) new C5067a(), TutorialAddon.this.f10293kj.translate("No"), TutorialAddon.this.f10293kj.translate("Yes"));
        }

        /* renamed from: taikodom.addon.tutorial.TutorialAddon$c$a */
        class C5067a extends aEP {
            C5067a() {
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                if (i == 1) {
                    TutorialAddon.this.f10293kj.alb().mo2066a((C5878acG) new aFJ(TutorialAddon.this.f10293kj.getPlayer(), TutorialAddon.this.hdz.getHandle()));
                    TutorialAddon.this.cHy();
                    if (z) {
                        TutorialAddon.this.m45651aH(TutorialAddon.hds, "true");
                    }
                }
            }

            /* access modifiers changed from: protected */
            public String cyE() {
                return TutorialAddon.this.f10293kj.translate("Never show this message again");
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                return C0939Nn.C0940a.CONFIRM;
            }
        }
    }

    /* renamed from: taikodom.addon.tutorial.TutorialAddon$a */
    class C5064a implements ActionListener {
        C5064a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            TutorialAddon.this.cHC();
        }
    }
}
