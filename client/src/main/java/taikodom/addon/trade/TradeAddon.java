package taikodom.addon.trade;

import game.network.message.externalizable.C0350Ef;
import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1191Rb;
import game.network.message.externalizable.C3689to;
import game.script.Actor;
import game.script.item.Item;
import game.script.item.ShipItem;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.space.Node;
import game.script.trade.Trade;
import game.script.trade.TradePlayerStatus;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C1506WA;
import logic.aaa.C2733jJ;
import logic.aaa.C5783aaP;
import logic.baa.C5473aKr;
import logic.baa.C6200aiQ;
import logic.baa.aDJ;
import logic.res.code.C5663aRz;
import logic.ui.ComponentManager;
import logic.ui.item.InternalFrame;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Collection;

@TaikodomAddon("taikodom.addon.trade")
/* compiled from: a */
public class TradeAddon implements C2495fo {
    private static final String gMR = "trade.xml";
    private static final String gMS = "trade.css";
    private static /* synthetic */ int[] gNk;
    /* access modifiers changed from: private */
    public JLabel dtS;
    /* access modifiers changed from: private */
    public Player elO;
    /* access modifiers changed from: private */
    public Trade fky;
    /* access modifiers changed from: private */
    public JCheckBox gNb;
    /* access modifiers changed from: private */
    public JList gNc;
    /* access modifiers changed from: private */
    public JList gNd;
    /* access modifiers changed from: private */
    public JLabel gNe;
    /* access modifiers changed from: private */
    public JLabel gNf;
    /* access modifiers changed from: private */
    public JSpinner gNg;
    /* access modifiers changed from: private */
    public JLabel gNj;
    /* renamed from: nM */
    public InternalFrame f10289nM;
    /* renamed from: kj */
    public IAddonProperties f10288kj;
    private C3428rT<C5783aaP> aoN;
    private C6622aqW dcE;
    private ActionListener gMT;
    private C3428rT<C0350Ef> gMU;
    private C3428rT<C1191Rb> gMV;
    private C5473aKr<aDJ, Object> gMW;
    private C5473aKr<aDJ, Object> gMX;
    private C6200aiQ<TradePlayerStatus> gMY;
    private C6200aiQ<TradePlayerStatus> gMZ;
    private C6200aiQ<TradePlayerStatus> gNa;
    private ComponentListener gNh;
    /* access modifiers changed from: private */
    private C5045b gNi;
    /* renamed from: wz */
    private C3428rT<C3689to> f10290wz;

    static /* synthetic */ int[] cAY() {
        int[] iArr = gNk;
        if (iArr == null) {
            iArr = new int[C1191Rb.C1192a.values().length];
            try {
                iArr[C1191Rb.C1192a.NOT_IN_SAME_NODE.ordinal()] = 12;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C1191Rb.C1192a.NOT_IN_SAME_STATION.ordinal()] = 11;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C1191Rb.C1192a.OTHER_BUSY.ordinal()] = 10;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C1191Rb.C1192a.OTHER_REFUSED.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_BLOCKED.ordinal()] = 13;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_CANCELED.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_CANNOT_RECEIVE_ALL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_HAS_NOT_ENOUGH_MONEY.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_HAS_NO_STORAGE.ordinal()] = 5;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_OFFLINE.ordinal()] = 2;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_TOO_FAR.ordinal()] = 15;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[C1191Rb.C1192a.PLAYER_UNKNOWN.ordinal()] = 3;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[C1191Rb.C1192a.SAME_PLAYER.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[C1191Rb.C1192a.SUCCESSFUL.ordinal()] = 8;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[C1191Rb.C1192a.TESTER_INTERACTION_DENIED.ordinal()] = 14;
            } catch (NoSuchFieldError e15) {
            }
            gNk = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10288kj = addonPropertie;
        this.f10288kj.bHv().mo16790b((Object) this, gMS);
        this.dcE = new C5053g("trade", this.f10288kj.translate("Trade"), "trade");
        this.f10288kj.mo2322a("trade", (Action) this.dcE);
        m45606iG();
        m45589Fa();
    }

    public void stop() {
        m45603gZ(true);
        removeListeners();
    }

    /* renamed from: iG */
    private void m45606iG() {
        this.f10290wz = new C5054h();
        this.aoN = new C5055i();
        this.gMU = new C5056j();
        this.gMV = new C5049c();
        this.gMT = new C5050d();
        this.gMW = new C5051e();
        this.gMX = new C5052f();
        this.gMY = new C5058k();
        this.gMZ = new C5062o();
        this.gNa = new C5061n();
        this.gNh = new C5060m();
    }

    /* renamed from: Fa */
    private void m45589Fa() {
        this.f10288kj.aVU().mo13965a(C3689to.class, this.f10290wz);
        this.f10288kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f10288kj.aVU().mo13965a(C0350Ef.class, this.gMU);
        this.f10288kj.aVU().mo13965a(C1191Rb.class, this.gMV);
    }

    private void removeListeners() {
        this.f10288kj.aVU().mo13968b(C3689to.class, this.f10290wz);
        this.f10288kj.aVU().mo13968b(C5783aaP.class, this.aoN);
        this.f10288kj.aVU().mo13968b(C0350Ef.class, this.gMU);
        this.f10288kj.aVU().mo13968b(C1191Rb.class, this.gMV);
    }

    /* access modifiers changed from: private */
    /* renamed from: Im */
    public void m45590Im() {
        if (this.f10289nM != null) {
            m45603gZ(true);
            return;
        }
        this.f10289nM = this.f10288kj.bHv().mo16792bN(gMR);
        m45607iM();
        this.dtS.setText(this.f10288kj.getPlayer().getName());
        this.gNj.setText(this.elO.getName());
        this.gNc.setCellRenderer(new C5059l());
        this.gNc.setModel(new DefaultListModel());
        this.gNc.setSelectionMode(2);
        this.gNc.setLayoutOrientation(2);
        this.gNc.setVisibleRowCount(-1);
        this.gNc.setTransferHandler(new C5044a(this, (C5044a) null));
        this.gNc.setDropMode(DropMode.ON_OR_INSERT);
        this.gNc.setDragEnabled(true);
        this.gNd.setCellRenderer(new C5063p());
        this.gNd.setModel(new DefaultListModel());
        this.gNd.setSelectionMode(2);
        this.gNd.setLayoutOrientation(2);
        this.gNd.setVisibleRowCount(-1);
        this.gNe.setText("0");
        this.gNi = new C5045b(this.gNg);
        cAW();
        this.f10289nM.center();
        this.f10289nM.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: gZ */
    public void m45603gZ(boolean z) {
        if (this.f10289nM != null) {
            cAX();
            if (z) {
                this.f10289nM.close();
            }
            this.f10289nM = null;
        }
        if (this.fky != null) {
            this.f10288kj.getPlayer().dxo();
        }
        if (this.gNi != null) {
            this.gNi.terminate();
        }
        this.elO = null;
        this.fky = null;
    }

    /* renamed from: iM */
    private void m45607iM() {
        this.gNb = this.f10289nM.mo4915cd("confirm-trade");
        this.gNc = this.f10289nM.mo4915cd("given-items-list");
        this.gNg = this.f10289nM.mo4915cd("given-money-amount");
        this.gNf = this.f10289nM.mo4917cf("other-player-status");
        this.gNd = this.f10289nM.mo4915cd("received-items-list");
        this.gNe = this.f10289nM.mo4917cf("received-money-amount");
        this.dtS = this.f10289nM.mo4917cf("player-name");
        this.gNj = this.f10289nM.mo4917cf("other-player-name");
    }

    private void cAW() {
        this.gNb.addActionListener(this.gMT);
        this.f10289nM.addComponentListener(this.gNh);
        this.fky.mo5699b(this.gMW, this.gMY, this.gMX, this.gNa, this.gMZ);
    }

    private void cAX() {
        if (this.f10289nM != null) {
            this.gNb.removeActionListener(this.gMT);
            if (this.fky != null) {
                this.fky.mo5705d(this.gMW, this.gMY, this.gMX, this.gNa, this.gMZ);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m45592a(C1191Rb.C1192a aVar) {
        String aVar2 = aVar.toString();
        switch (cAY()[aVar.ordinal()]) {
            case 1:
                return this.f10288kj.translate("{0} refused your trade invitation.");
            case 2:
                return this.f10288kj.translate("{0} not found or currently disconnected.");
            case 3:
                return this.f10288kj.translate("Player not found or currently disconnected.");
            case 4:
                return this.f10288kj.translate("{0} cancelled trade.");
            case 5:
                return this.f10288kj.translate("{0} has no storage for items.");
            case 6:
                return this.f10288kj.translate("{0} offered an invalid amount of money.");
            case 7:
                return this.f10288kj.translate("{0} cannot receive all items.");
            case 8:
                return this.f10288kj.translate("Trade successful.");
            case 9:
                return this.f10288kj.translate("You cannnot trade with yourself.");
            case 10:
                return this.f10288kj.translate("{0} is already trading.");
            case 11:
                return this.f10288kj.translate("Target must be in the same station for the trade to be successful.");
            case 12:
                return this.f10288kj.translate("Target must be in the same region for the trade to be successful.");
            case 13:
                return this.f10288kj.translate("User blocked.");
            case 15:
                return this.f10288kj.translate("{0} is too far from you, beyond trade range limit");
            default:
                return aVar2;
        }
    }

    @C2602hR(mo19255zf = "TRADE_INVITE", mo19256zg = "UNDOCKED")
    /* renamed from: ha */
    public boolean mo24639ha(boolean z) {
        Actor agB;
        if (!z) {
            return false;
        }
        Ship bQx = this.f10288kj.getPlayer().bQx();
        if (bQx == null || (agB = bQx.agB()) == null || !(agB instanceof Ship) || !(((Ship) agB).agj() instanceof Player)) {
            return false;
        }
        this.f10288kj.getPlayer().dwU().mo21450mT(((Ship) agB).agj().getName());
        return true;
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$a */
    private class C5044a extends TransferHandler {
        private static final long serialVersionUID = 9021574569187737196L;

        private C5044a() {
        }

        /* synthetic */ C5044a(TradeAddon tradeAddon, C5044a aVar) {
            this();
        }

        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            return true;
        }

        public boolean importData(TransferHandler.TransferSupport transferSupport) {
            String str;
            if (!transferSupport.isDrop()) {
                return false;
            }
            if (!transferSupport.isDataFlavorSupported(C2125cH.f6026vP)) {
                return false;
            }
            try {
                Node azW = TradeAddon.this.f10288kj.getPlayer().bhE().azW();
                if (azW == null) {
                    throw new IllegalStateException("player " + TradeAddon.this.f10288kj.getPlayer().getName() + " is at no node");
                } else if (azW.mo21621Zk()) {
                    TradeAddon.this.f10288kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, TradeAddon.this.f10288kj.translate("You cannot trade items at this node"), new Object[0]));
                    return false;
                } else {
                    for (aDJ adj : (Collection) transferSupport.getTransferable().getTransferData(C2125cH.f6026vP)) {
                        if (adj instanceof Item) {
                            if (((Item) adj).isBound()) {
                                str = TradeAddon.this.f10288kj.translate("Bounded items cannot be traded");
                            } else {
                                if (adj instanceof ShipItem) {
                                    Ship al = ((ShipItem) adj).mo11127al();
                                    if (al == TradeAddon.this.f10288kj.getPlayer().bQx()) {
                                        str = TradeAddon.this.f10288kj.translate("You cannot trade your active ship");
                                    } else if (!al.isEmpty()) {
                                        str = TradeAddon.this.f10288kj.translate("You cannot trade equipped ship");
                                    }
                                }
                                str = null;
                            }
                            if (str != null) {
                                TradeAddon.this.f10288kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, str, new Object[0]));
                                return false;
                            }
                            TradeAddon.this.fky.mo5697A((Item) adj);
                        }
                    }
                    return true;
                }
            } catch (Exception e) {
                TradeAddon.this.f10288kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, TradeAddon.this.f10288kj.translate("Unkwow error on trade"), new Object[0]));
                e.printStackTrace();
                return false;
            }
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$b */
    /* compiled from: a */
    private class C5045b {
        private static final int gQX = 2000;
        /* access modifiers changed from: private */
        public final JSpinner gQY;
        /* access modifiers changed from: private */
        public final WrapRunnable gQZ = cDk();
        private ChangeListener changeListener;
        private KeyListener gRa;

        public C5045b(JSpinner jSpinner) {
            this.gQY = jSpinner;
            m45614Fa();
        }

        /* renamed from: Fa */
        private void m45614Fa() {
            this.gRa = new C5046a();
            this.changeListener = new C5047b();
            this.gQY.getEditor().getTextField().addKeyListener(this.gRa);
            this.gQY.addChangeListener(this.changeListener);
        }

        private WrapRunnable cDk() {
            return new C5048c();
        }

        /* access modifiers changed from: private */
        public void startTimer() {
            this.gQZ.cancel();
            TradeAddon.this.f10288kj.alf().mo7960a("MoneyInputUpdater", this.gQZ, 2000);
        }

        /* access modifiers changed from: private */
        public void terminate() {
            if (this.gQY != null) {
                this.gQY.getEditor().getTextField().removeKeyListener(this.gRa);
                this.gQY.removeChangeListener(this.changeListener);
                if (this.gQZ != null) {
                    this.gQZ.cancel();
                    return;
                }
                return;
            }
            throw new IllegalStateException();
        }

        /* renamed from: taikodom.addon.trade.TradeAddon$b$a */
        class C5046a extends KeyAdapter {
            C5046a() {
            }

            public void keyTyped(KeyEvent keyEvent) {
                C5045b.this.startTimer();
            }
        }

        /* renamed from: taikodom.addon.trade.TradeAddon$b$b */
        /* compiled from: a */
        class C5047b implements ChangeListener {
            C5047b() {
            }

            public void stateChanged(ChangeEvent changeEvent) {
                C5045b.this.startTimer();
            }
        }

        /* renamed from: taikodom.addon.trade.TradeAddon$b$c */
        /* compiled from: a */
        class C5048c extends WrapRunnable {
            C5048c() {
            }

            public void run() {
                TradeAddon.this.gNg.requestFocus();
                C5045b.this.gQZ.cancel();
                TradeAddon.this.fky.mo5707fH(((Integer) C5045b.this.gQY.getValue()).longValue());
            }
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$g */
    /* compiled from: a */
    class C5053g extends C6622aqW {


        C5053g(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1 && objArr[0] != null && !"".equals(objArr[0])) {
                TradeAddon.this.f10288kj.getPlayer().dwU().mo21450mT(objArr[0].toString());
            } else if (!TradeAddon.this.mo24639ha(true)) {
                TradeAddon.this.f10288kj.getPlayer().mo14419f((C1506WA) new C2733jJ(String.valueOf(TradeAddon.this.f10288kj.translate("Wrong command usage")) + ": " + TradeAddon.this.f10288kj.translate("trade <somebody>"), false, new Object[0]));
            }
        }

        public boolean isActive() {
            return TradeAddon.this.f10289nM != null;
        }

        public boolean isEnabled() {
            return true;
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$h */
    /* compiled from: a */
    class C5054h extends C3428rT<C3689to> {
        C5054h() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (TradeAddon.this.f10289nM != null && TradeAddon.this.f10289nM.isVisible()) {
                toVar.adC();
            }
            TradeAddon.this.m45603gZ(true);
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$i */
    /* compiled from: a */
    class C5055i extends C3428rT<C5783aaP> {
        C5055i() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            TradeAddon.this.m45603gZ(true);
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$j */
    /* compiled from: a */
    class C5056j extends C3428rT<C0350Ef> {
        C5056j() {
        }

        /* renamed from: a */
        public void mo321b(C0350Ef ef) {
            if (ef.bVH() == C0350Ef.C0351a.INVITED) {
                TradeAddon.this.elO = ef.buL();
                ((MessageDialogAddon) TradeAddon.this.f10288kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(TradeAddon.this.f10288kj.translate("You received a trade invitation from {0}. Accept it?"), TradeAddon.this.elO.getName()), (aEP) new C5057a(), TradeAddon.this.f10288kj.translate("no"), TradeAddon.this.f10288kj.translate("yes"));
            } else if (ef.bVH() == C0350Ef.C0351a.ACCEPTED) {
                TradeAddon.this.fky = ef.bVI();
                TradeAddon.this.elO = TradeAddon.this.fky.buL();
                TradeAddon.this.m45590Im();
            }
        }

        /* renamed from: taikodom.addon.trade.TradeAddon$j$a */
        class C5057a extends aEP {
            C5057a() {
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                if (i == 1) {
                    TradeAddon.this.elO.mo14433n(TradeAddon.this.f10288kj.getPlayer(), true);
                } else if (TradeAddon.this.elO == null) {
                    throw new IllegalStateException();
                } else {
                    TradeAddon.this.elO.mo14433n(TradeAddon.this.f10288kj.getPlayer(), false);
                }
            }

            /* access modifiers changed from: protected */
            public String getHeader() {
                return TradeAddon.this.f10288kj.translate("Private trade");
            }
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$c */
    /* compiled from: a */
    class C5049c extends C3428rT<C1191Rb> {
        C5049c() {
        }

        /* renamed from: a */
        public void mo321b(C1191Rb rb) {
            TradeAddon.this.f10288kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C5956adg.format(TradeAddon.this.m45592a(rb.bwG()), rb.buL().getName()), new Object[0]));
            TradeAddon.this.m45603gZ(true);
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$d */
    /* compiled from: a */
    class C5050d implements ActionListener {
        C5050d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (TradeAddon.this.elO == null) {
                throw new IllegalStateException();
            }
            try {
                long longValue = ((Integer) TradeAddon.this.gNg.getValue()).longValue();
                long bSs = TradeAddon.this.f10288kj.getPlayer().mo5156Qw().bSs();
                if (longValue > bSs) {
                    TradeAddon.this.gNg.getModel().setValue(Long.valueOf(bSs));
                } else {
                    bSs = longValue;
                }
                TradeAddon.this.fky.mo5707fH(bSs);
                TradeAddon.this.f10288kj.getPlayer().dxq();
                TradeAddon.this.gNg.setEnabled(!TradeAddon.this.gNb.isSelected());
            } catch (C6847aun e) {
                TradeAddon.this.fky.buV();
                TradeAddon.this.f10288kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, TradeAddon.this.f10288kj.translate("Some problem prevented this trade from being finished"), new Object[0]));
            }
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$e */
    /* compiled from: a */
    class C5051e implements C5473aKr<aDJ, Object> {
        C5051e() {
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            if (TradeAddon.this.f10289nM != null) {
                for (Item auq : objArr) {
                    if (auq.cxc()) {
                        TradeAddon.this.gNc.getModel().removeElement(auq);
                    }
                }
            }
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            if (TradeAddon.this.f10289nM != null) {
                for (Item auq : objArr) {
                    if ((auq instanceof Item) && auq.cxc()) {
                        TradeAddon.this.gNc.getModel().addElement(auq);
                    }
                }
            }
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$f */
    /* compiled from: a */
    class C5052f implements C5473aKr<aDJ, Object> {
        C5052f() {
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            if (TradeAddon.this.f10289nM != null) {
                for (Item auq : objArr) {
                    if (auq.cxc()) {
                        TradeAddon.this.gNd.getModel().removeElement(auq);
                    }
                }
            }
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            if (TradeAddon.this.f10289nM != null) {
                for (Item auq : objArr) {
                    if ((auq instanceof Item) && auq.cxc()) {
                        TradeAddon.this.gNd.getModel().addElement(auq);
                    }
                }
            }
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$k */
    /* compiled from: a */
    class C5058k implements C6200aiQ<TradePlayerStatus> {
        C5058k() {
        }

        /* renamed from: a */
        public void mo1143a(TradePlayerStatus aiVar, C5663aRz arz, Object obj) {
            if (((Boolean) obj).booleanValue()) {
                ComponentManager.getCssHolder(TradeAddon.this.dtS).setAttribute("class", "horizontalbox title confirmed");
            } else {
                ComponentManager.getCssHolder(TradeAddon.this.dtS).setAttribute("class", "horizontalbox title waiting");
                TradeAddon.this.gNb.setSelected(false);
                TradeAddon.this.gNg.setEnabled(true);
            }
            TradeAddon.this.dtS.repaint();
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$o */
    /* compiled from: a */
    class C5062o implements C6200aiQ<TradePlayerStatus> {
        C5062o() {
        }

        /* renamed from: a */
        public void mo1143a(TradePlayerStatus aiVar, C5663aRz arz, Object obj) {
            if (((Boolean) obj).booleanValue()) {
                ComponentManager.getCssHolder(TradeAddon.this.gNj).setAttribute("class", "horizontalbox title confirmed");
                TradeAddon.this.gNf.setText(TradeAddon.this.f10288kj.translate("Confirmed"));
            } else {
                ComponentManager.getCssHolder(TradeAddon.this.gNj).setAttribute("class", "horizontalbox title waiting");
                TradeAddon.this.gNf.setText(TradeAddon.this.f10288kj.translate("Waiting"));
                TradeAddon.this.gNb.setSelected(false);
                TradeAddon.this.gNg.setEnabled(true);
            }
            TradeAddon.this.gNj.repaint();
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$n */
    /* compiled from: a */
    class C5061n implements C6200aiQ<TradePlayerStatus> {
        C5061n() {
        }

        /* renamed from: a */
        public void mo1143a(TradePlayerStatus aiVar, C5663aRz arz, Object obj) {
            TradeAddon.this.gNe.setText(((Long) obj).toString());
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$m */
    /* compiled from: a */
    class C5060m extends ComponentAdapter {
        C5060m() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            TradeAddon.this.m45603gZ(false);
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$l */
    /* compiled from: a */
    class C5059l extends C5517aMj {
        C5059l() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            JLabel nR = aur.mo11631nR("given-item-icon");
            nR.setIcon(new C2539gY((Item) obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.trade.TradeAddon$p */
    /* compiled from: a */
    class C5063p extends C5517aMj {
        C5063p() {
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            JLabel nR = aur.mo11631nR("received-item-icon");
            nR.setIcon(new C2539gY((Item) obj));
            return nR;
        }
    }
}
