package taikodom.addon.itemlocation;

import game.network.message.externalizable.C0939Nn;
import game.script.item.Item;
import game.script.ship.RestrictedCargoHold;
import p001a.C1493Vz;
import p001a.C2293dh;
import p001a.C3780ux;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import java.util.Comparator;

@TaikodomAddon("taikodom.addon.itemlocation")
/* renamed from: a.aBF */
/* compiled from: a */
public abstract class aBF {
    public static final Comparator<Item> hhz = new C3780ux();

    /* renamed from: a */
    public static boolean m12741a(Item auq, C1493Vz vz, IAddonProperties vWVar) {
        if ((vz instanceof RestrictedCargoHold) && !((RestrictedCargoHold) vz).mo9151b(auq.bAP().mo19866HC())) {
            m12742c(vWVar);
            return false;
        } else if (vz.mo2696wE() - auq.getVolume() < vz.aRB()) {
            m12743d(vWVar);
            return false;
        } else if (vz.mo2694t(auq)) {
            return false;
        } else {
            if (!auq.aNN()) {
                return false;
            }
            try {
                vz.mo2693s(auq);
                return true;
            } catch (C2293dh e) {
                System.out.println(" Could not add item to " + vz + " - Exception:" + e.getMessage());
                return false;
            }
        }
    }

    /* renamed from: c */
    private static void m12742c(IAddonProperties vWVar) {
        m12740a(vWVar, vWVar.mo11834a((Class<?>) aBF.class, "Your vault can't accept this type of item.", new Object[0]));
    }

    /* renamed from: d */
    private static void m12743d(IAddonProperties vWVar) {
        m12740a(vWVar, vWVar.mo11834a((Class<?>) aBF.class, "There is not enough space on destination storage.", new Object[0]));
    }

    /* renamed from: a */
    private static void m12740a(IAddonProperties vWVar, String str) {
        ((MessageDialogAddon) vWVar.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, str, vWVar.translate("Ok"));
    }
}
