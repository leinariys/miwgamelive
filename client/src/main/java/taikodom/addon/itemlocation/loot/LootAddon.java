package taikodom.addon.itemlocation.loot;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.script.Actor;
import game.script.item.ItemLocation;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.space.Loot;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.aaa.C1190Ra;
import logic.aaa.C5783aaP;
import logic.aaa.C5901acd;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.TItemPanel;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.itemlocation.loot")
/* compiled from: a */
public class LootAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9999P;
    /* access modifiers changed from: private */
    public Loot aZM;
    /* renamed from: kj */
    public IAddonProperties f10000kj;
    /* renamed from: nM */
    public InternalFrame f10001nM;
    private C6622aqW aZJ;
    private TItemPanel aZK;
    private C6124ags<C3689to> aZL;
    private C6622aqW aZN;
    private C3428rT<C5901acd> aZO;
    private C6124ags<C1190Ra> aZP;
    private ComponentAdapter aZQ;
    /* access modifiers changed from: private */
    private boolean aZR = false;
    /* access modifiers changed from: private */
    private C6124ags<C5783aaP> amy;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10000kj = addonPropertie;
        this.f9999P = this.f10000kj.getPlayer();
        C6245ajJ aVU = this.f10000kj.aVU();
        this.aZQ = new C4608b();
        this.amy = new C4610d();
        this.aZL = new C4609c();
        this.aZO = new C4612f();
        this.aZP = new C4611e();
        this.aZJ = new C4614h("loot", this.f10000kj.translate("Loot"), "loot");
        this.f10000kj.mo2322a("loot", (Action) this.aZJ);
        this.f10000kj.mo2322a("loot-all", (Action) this.aZN);
        this.f10000kj.mo2317P(this);
        aVU.mo13965a(C5901acd.class, this.aZO);
        aVU.mo13965a(C1190Ra.class, this.aZP);
    }

    @C2602hR(mo19255zf = "GRAB_ALL_LOOT")
    /* renamed from: aQ */
    public void mo24180aQ(boolean z) {
        if (z) {
            this.f10000kj.alb().cPj();
        }
    }

    public void stop() {
        close();
        this.f10000kj.aVU().mo13964a(this.aZO);
        this.f10000kj.aVU().mo13964a(this.aZP);
        this.f10000kj.mo2318Q(this);
    }

    /* renamed from: a */
    public void mo24179a(Loot ael) {
        this.aZM = ael;
        this.aZM.mo8588ki(true);
        if (this.f10001nM == null) {
            this.f10001nM = this.f10000kj.bHv().mo16792bN("loot.xml");
            if (this.aZM.mo961QW()) {
                this.f10001nM.mo4911Kk();
                ((Loot) C3582se.m38985a(this.aZM, (C6144ahM<?>) new C4613g())).mo8569QU();
            } else {
                m44285Im();
            }
        } else if (this.f10001nM.isVisible()) {
            close();
            return;
        }
        this.f10000kj.aVU().mo13965a(C5783aaP.class, this.amy);
        this.f10000kj.aVU().mo13965a(C3689to.class, this.aZL);
        this.f10000kj.aVU().mo13975h(new C1532WW((C1532WW.C1533a) null, this));
        m44288aR(true);
        this.f10001nM.center();
        this.f10001nM.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: Im */
    public void m44285Im() {
        this.aZK = new TItemPanel();
        this.aZK.mo23647b((ItemLocation) this.aZM.dtA());
        this.f10001nM.mo4915cd("itensContainer").add(this.aZK);
        JButton cb = this.f10001nM.mo4913cb("take-all");
        cb.requestFocusInWindow();
        cb.addActionListener(new C4606a());
        this.f10001nM.mo4913cb("close").addActionListener(new C4615i());
        this.f10001nM.addComponentListener(this.aZQ);
    }

    /* renamed from: Ym */
    private void m44286Ym() {
        if (this.aZM != null) {
            this.aZM.mo8588ki(false);
            this.aZM.dtG();
            this.aZM = null;
        }
    }

    /* renamed from: Yn */
    public void mo24178Yn() {
        ((MessageDialogAddon) this.f10000kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, this.f10000kj.translate("There is not enough room."), this.f10000kj.translate("Ok"));
    }

    /* access modifiers changed from: private */
    public void close() {
        m44286Ym();
        this.f10000kj.aVU().mo13964a(this.amy);
        this.f10000kj.aVU().mo13964a(this.aZL);
        if (this.aZM != null) {
            this.aZM.mo8588ki(false);
        }
        this.f10000kj.aVU().mo13975h(new C1532WW((C1532WW.C1533a) null, this));
        m44288aR(false);
        if (this.f10001nM != null) {
            this.f10001nM.close();
            this.f10001nM = null;
        }
    }

    /* renamed from: aR */
    private void m44288aR(boolean z) {
        if (!z || this.f10000kj.getPlayer().bQB() || !this.f10000kj.alb().anX()) {
            if (!z && this.aZR && !this.f10000kj.alb().anX() && !this.f10000kj.getPlayer().bQB()) {
                this.f10000kj.alb().mo22059aR(false);
            }
            this.aZR = false;
            return;
        }
        this.aZR = true;
        this.f10000kj.alb().mo22059aR(false);
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$b */
    /* compiled from: a */
    class C4608b extends ComponentAdapter {
        C4608b() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            LootAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$d */
    /* compiled from: a */
    class C4610d extends C6124ags<C5783aaP> {
        C4610d() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            if (LootAddon.this.f10001nM == null) {
                return false;
            }
            LootAddon.this.close();
            return false;
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$c */
    /* compiled from: a */
    class C4609c extends C6124ags<C3689to> {
        C4609c() {
        }

        /* renamed from: b */
        public boolean updateInfo(C3689to toVar) {
            if (LootAddon.this.f10001nM == null || !LootAddon.this.f10001nM.isVisible()) {
                return false;
            }
            toVar.adC();
            LootAddon.this.close();
            return false;
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$f */
    /* compiled from: a */
    class C4612f extends C3428rT<C5901acd> {
        C4612f() {
        }

        /* renamed from: a */
        public void mo321b(C5901acd acd) {
            LootAddon.this.mo24179a(acd.bOl());
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$e */
    /* compiled from: a */
    class C4611e extends C6124ags<C1190Ra> {
        C4611e() {
        }

        /* renamed from: a */
        public boolean updateInfo(C1190Ra ra) {
            if (LootAddon.this.f10001nM == null) {
                return false;
            }
            LootAddon.this.close();
            return false;
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$h */
    /* compiled from: a */
    class C4614h extends C6622aqW {


        C4614h(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return LootAddon.this.f10001nM != null && LootAddon.this.f10001nM.isVisible();
        }

        public boolean isEnabled() {
            Ship bQx = LootAddon.this.f9999P.bQx();
            if (bQx != null) {
                return bQx.agB() instanceof Loot;
            }
            return false;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            Ship bQx = LootAddon.this.f9999P.bQx();
            if (bQx != null) {
                Actor agB = bQx.agB();
                if (agB instanceof Loot) {
                    PlayerController dxc = LootAddon.this.f9999P.dxc();
                    try {
                        dxc.cON();
                        dxc.mo22138e((C0286Dh) (Loot) agB);
                    } catch (C5475aKt e) {
                        e.printStackTrace();
                        dxc.cOS();
                    }
                }
            }
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$g */
    /* compiled from: a */
    class C4613g extends C0947Nt<Void> {
        C4613g() {
        }

        /* renamed from: b */
        public void mo4278E(Void voidR) {
            LootAddon.this.f10001nM.mo4912Kl();
            LootAddon.this.m44285Im();
        }

        /* renamed from: c */
        public void mo4279c(Throwable th) {
            LootAddon.this.f10001nM.mo4912Kl();
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$a */
    class C4606a implements ActionListener {
        C4606a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (LootAddon.this.aZM != null) {
                ((Loot) C3582se.m38985a(LootAddon.this.aZM, (C6144ahM<?>) new C4607a())).mo8571aj(LootAddon.this.f9999P);
                LootAddon.this.close();
            }
        }

        /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$a$a */
        class C4607a extends C0947Nt<Boolean> {
            C4607a() {
            }

            /* renamed from: b */
            public void mo4278E(Boolean bool) {
                if (!bool.booleanValue()) {
                    ((MessageDialogAddon) LootAddon.this.f10000kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.WARNING, LootAddon.this.f10000kj.translate("There is not enough space to all items on destination storage."), LootAddon.this.f10000kj.translate("Ok"));
                }
                LootAddon.this.f10000kj.getPlayer().mo14348am("lootAll", "");
            }

            /* renamed from: c */
            public void mo4279c(Throwable th) {
            }
        }
    }

    /* renamed from: taikodom.addon.itemlocation.loot.LootAddon$i */
    /* compiled from: a */
    class C4615i implements ActionListener {
        C4615i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            LootAddon.this.close();
        }
    }
}
