package taikodom.addon.itemlocation.outpost;

import game.script.Actor;
import game.script.item.ItemLocation;
import game.script.player.Player;
import game.script.ship.Outpost;
import logic.IAddonSettings;
import logic.aaa.C6388alw;
import logic.baa.C5473aKr;
import logic.baa.C6200aiQ;
import logic.data.link.C1092Px;
import logic.data.link.C2743jO;
import logic.res.code.C5663aRz;
import logic.ui.item.TaskPane;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;

@TaikodomAddon("taikodom.addon.itemlocation.outpost")
/* compiled from: a */
public class OutpostStorageAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10002P;
    /* access modifiers changed from: private */
    public aSZ aCF;
    /* access modifiers changed from: private */
    public TaskPane aCG;
    /* renamed from: kj */
    public IAddonProperties f10003kj;
    private C3428rT<C6388alw> aCD;
    private C6200aiQ<Player> aCE;
    private ItemLocation bLj;
    private aGC coU;
    private C6116agk eib;
    private C0072At eic;
    /* access modifiers changed from: private */
    private C5473aKr<ItemLocation, Object> eid;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10003kj = addonPropertie;
        this.f10002P = this.f10003kj.getPlayer();
        if (this.f10002P != null) {
            m44324iG();
            m44315Im();
            this.coU = new C4619d(this.f10003kj.translate("Outpost Storage"), "outpost-storage");
            this.coU.mo15602jH(this.f10003kj.translate("This storage area is reserved to you and your corporation comrades."));
            this.coU.mo15603jI("OUTPOST_STORAGE_WINDOW");
            this.f10003kj.mo2322a("outpost-storage", (Action) this.coU);
            this.aCF = new aSZ(this.aCG, this.coU, 201, 2);
            this.f10003kj.aVU().publish(this.aCF);
            if (this.coU.isEnabled()) {
                open();
            }
        }
    }

    /* renamed from: iG */
    private void m44324iG() {
        this.aCE = new C4618c();
        this.f10002P.mo8320a(C1092Px.dQN, (C6200aiQ<?>) this.aCE);
        this.eid = new C4617b();
        this.aCD = new C4616a();
        this.f10003kj.aVU().mo13965a(C6388alw.class, this.aCD);
        this.f10003kj.mo2317P(this);
    }

    private C6116agk bvk() {
        if (this.eib == null) {
            this.eib = new C6116agk(this.f10003kj, this.aCG.mo4916ce("items"));
            this.eib.mo525Fa();
        }
        return this.eib;
    }

    private C0072At bvl() {
        if (this.eic == null) {
            this.eic = new C0072At(this.f10003kj, this.aCG.mo4916ce("records"));
            this.eic.mo525Fa();
        }
        return this.eic;
    }

    /* renamed from: Im */
    private void m44315Im() {
        if (this.aCG == null) {
            this.aCG = (TaskPane) this.f10003kj.bHv().mo16794bQ("outpost-storage.xml");
        }
    }

    @C2602hR(mo19255zf = "OUTPOST_STORAGE_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24190al(boolean z) {
        if (z) {
            open();
        }
    }

    public void stop() {
    }

    /* access modifiers changed from: private */
    public void open() {
        this.bLj = m44316Ns().mo21390by(this.f10002P);
        if (this.bLj != null) {
            this.bLj.mo8319a(C2743jO.apm, (C5473aKr<?, ?>) this.eid);
            bvk().mo13457b(this.bLj);
            refresh();
        }
    }

    /* access modifiers changed from: private */
    public void close() {
        if (this.bLj != null) {
            this.bLj.mo8325b(C2743jO.apm, (C5473aKr<?, ?>) this.eid);
        }
    }

    /* renamed from: Ns */
    private Outpost m44316Ns() {
        Actor bhE = this.f10002P.bhE();
        if (!(bhE instanceof Outpost)) {
            return null;
        }
        return (Outpost) bhE;
    }

    /* access modifiers changed from: private */
    public void refresh() {
        bvl().refresh();
        bvk().refresh();
    }

    /* renamed from: taikodom.addon.itemlocation.outpost.OutpostStorageAddon$d */
    /* compiled from: a */
    class C4619d extends aGC {
        private static final long serialVersionUID = 7009555292956522135L;

        C4619d(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
            if (z) {
                OutpostStorageAddon.this.open();
            } else {
                OutpostStorageAddon.this.close();
            }
        }

        public boolean isEnabled() {
            Actor bhE = OutpostStorageAddon.this.f10002P.bhE();
            if (!(bhE instanceof Outpost)) {
                return false;
            }
            return ((Outpost) bhE).mo628b(C0437GE.CORP_STORAGE, OutpostStorageAddon.this.f10002P);
        }
    }

    /* renamed from: taikodom.addon.itemlocation.outpost.OutpostStorageAddon$c */
    /* compiled from: a */
    class C4618c implements C6200aiQ<Player> {
        C4618c() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            if (OutpostStorageAddon.this.f10002P.bQB()) {
                OutpostStorageAddon.this.f10003kj.aVU().mo13975h(new C0269DS(OutpostStorageAddon.this.aCF, arz != null));
            }
        }
    }

    /* renamed from: taikodom.addon.itemlocation.outpost.OutpostStorageAddon$b */
    /* compiled from: a */
    class C4617b implements C5473aKr<ItemLocation, Object> {
        C4617b() {
        }

        /* renamed from: a */
        public void mo1102b(ItemLocation aag, C5663aRz arz, Object[] objArr) {
            OutpostStorageAddon.this.refresh();
        }

        /* renamed from: b */
        public void mo1101a(ItemLocation aag, C5663aRz arz, Object[] objArr) {
            OutpostStorageAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.itemlocation.outpost.OutpostStorageAddon$a */
    class C4616a extends C3428rT<C6388alw> {
        C4616a() {
        }

        /* renamed from: a */
        public void mo321b(C6388alw alw) {
            if (OutpostStorageAddon.this.aCG != null && !alw.cic()) {
                OutpostStorageAddon.this.refresh();
            }
        }
    }
}
