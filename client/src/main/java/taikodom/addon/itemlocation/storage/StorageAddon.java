package taikodom.addon.itemlocation.storage;

import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.player.Player;
import game.script.ship.Outpost;
import game.script.ship.Station;
import game.script.storage.Storage;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.ui.item.Progress;
import logic.ui.item.TaskPane;
import logic.ui.item.TextField;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.TItemPanel;
import taikodom.addon.components.popup.DefaultMenuItems;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@TaikodomAddon("taikodom.addon.itemlocation.storage")
/* compiled from: a */
public class StorageAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f10004IU;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10005P;
    /* access modifiers changed from: private */
    public TItemPanel aZK;
    /* access modifiers changed from: private */
    public ItemLocation bLj;
    private aSZ aCF;
    private C6124ags<C5783aaP> amy;
    private TaskPane bgR;
    private TextField cpa;
    private aGC itg;

    /* renamed from: kj */
    private IAddonProperties f10006kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10006kj = addonPropertie;
        this.f10005P = this.f10006kj.getPlayer();
        if (this.f10005P != null) {
            this.f10004IU = C3987xk.bFK;
            this.itg = new C4620a(this.f10006kj.translate("Storage"), "storage");
            this.amy = new C4622c();
            this.f10006kj.aVU().mo13965a(C5783aaP.class, this.amy);
            this.itg.mo15602jH(this.f10006kj.translate("Storage window, where you can check the items you left in this station."));
            this.itg.mo15603jI("STORAGE_WINDOW");
            this.f10006kj.mo2317P(this);
            m44336Im();
            if (this.f10005P.bQB()) {
                dkj();
            }
            this.aCF = new aSZ(this.bgR, this.itg, 100, 2);
            this.f10006kj.aVU().publish(this.aCF);
        }
    }

    public void stop() {
        if (this.bgR != null) {
            this.bgR.destroy();
            this.bgR = null;
        }
        this.f10006kj.aVU().mo13964a(this.amy);
    }

    /* renamed from: Im */
    private void m44336Im() {
        this.bgR = (TaskPane) this.f10006kj.bHv().mo16794bQ("storage.xml");
        this.cpa = this.bgR.mo4920ci("search-field");
        this.aZK = new C4621b();
        this.aZK.mo23647b(this.bLj);
        this.bgR.mo4915cd("items-container").add(this.aZK);
        this.cpa.addKeyListener(new C4624e());
        this.bgR.mo4918cg("capacity").mo17403a(new C4623d());
    }

    /* access modifiers changed from: private */
    public void open() {
        this.cpa.setText("");
    }

    /* access modifiers changed from: private */
    public void dkj() {
        if (this.f10005P != null) {
            ((Player) C3582se.m38985a(this.f10005P, (C6144ahM<?>) new C4625f())).dxU();
        }
    }

    @C2602hR(mo19255zf = "STORAGE_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24195al(boolean z) {
        if (z && this.f10006kj.getPlayer().bQB()) {
            open();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: iL */
    public boolean m44345iL() {
        return this.f10006kj != null && this.f10006kj.ala().aLS().mo22273iL();
    }

    /* renamed from: taikodom.addon.itemlocation.storage.StorageAddon$a */
    class C4620a extends aGC {


        C4620a(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            if (!StorageAddon.this.m44345iL()) {
                if (!StorageAddon.this.f10005P.bQB() || !(StorageAddon.this.f10005P.bhE() instanceof Station)) {
                    return false;
                }
                return true;
            } else if (!StorageAddon.this.f10005P.bQB() || !(StorageAddon.this.f10005P.bhE() instanceof Station) || (StorageAddon.this.f10005P.bhE() instanceof Outpost)) {
                return false;
            } else {
                return true;
            }
        }

        public void setVisible(boolean z) {
            if (z) {
                StorageAddon.this.open();
            }
        }
    }

    /* renamed from: taikodom.addon.itemlocation.storage.StorageAddon$c */
    /* compiled from: a */
    class C4622c extends C6124ags<C5783aaP> {
        C4622c() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            StorageAddon.this.dkj();
            return false;
        }
    }

    /* renamed from: taikodom.addon.itemlocation.storage.StorageAddon$b */
    /* compiled from: a */
    class C4621b extends TItemPanel {


        C4621b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: r */
        public JPopupMenu mo12148r(Item auq) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            JMenu a = DefaultMenuItems.m43020a(19, new Item[]{auq});
            if (a != null) {
                jPopupMenu.add(a);
            }
            JMenuItem w = DefaultMenuItems.m43038w(auq);
            if (w != null) {
                jPopupMenu.add(w);
            }
            return jPopupMenu;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public JPopupMenu mo12147a(Item[] auqArr) {
            JPopupMenu jPopupMenu = new JPopupMenu();
            JMenu a = DefaultMenuItems.m43020a(51, auqArr);
            if (a != null) {
                jPopupMenu.add(a);
            }
            return jPopupMenu;
        }
    }

    /* renamed from: taikodom.addon.itemlocation.storage.StorageAddon$e */
    /* compiled from: a */
    class C4624e extends KeyAdapter {
        C4624e() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            Component component = (JTextField) keyEvent.getComponent();
            if (keyEvent.getKeyCode() == 27) {
                if (component == KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner()) {
                    component.transferFocusUpCycle();
                }
            } else if (StorageAddon.this.aZK != null) {
                StorageAddon.this.aZK.bCU().mo2165O(component.getText());
            }
        }
    }

    /* renamed from: taikodom.addon.itemlocation.storage.StorageAddon$d */
    /* compiled from: a */
    class C4623d implements Progress.C2065a {
        C4623d() {
        }

        public String getText() {
            if (StorageAddon.this.bLj == null) {
                return null;
            }
            return String.valueOf(StorageAddon.this.f10004IU.mo22980dY(StorageAddon.this.bLj.aRB())) + C0147Bi.SEPARATOR + StorageAddon.this.f10004IU.mo22980dY(StorageAddon.this.bLj.mo2696wE());
        }

        public float getValue() {
            if (StorageAddon.this.bLj == null) {
                return 0.0f;
            }
            return StorageAddon.this.bLj.aRB();
        }

        public float getMaximum() {
            if (StorageAddon.this.bLj == null) {
                return 0.0f;
            }
            return StorageAddon.this.bLj.mo2696wE();
        }

        public float getMinimum() {
            return 0.0f;
        }
    }

    /* renamed from: taikodom.addon.itemlocation.storage.StorageAddon$f */
    /* compiled from: a */
    class C4625f extends C0947Nt<Storage> {
        C4625f() {
        }

        /* renamed from: f */
        public void mo4278E(Storage qzVar) {
            StorageAddon.this.bLj = qzVar;
            if (StorageAddon.this.aZK != null) {
                StorageAddon.this.aZK.mo23647b(StorageAddon.this.bLj);
            }
        }

        /* renamed from: c */
        public void mo4279c(Throwable th) {
        }
    }
}
