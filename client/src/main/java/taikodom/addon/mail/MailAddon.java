package taikodom.addon.mail;

import game.network.message.externalizable.C5344aFs;
import game.script.mail.MailBox;
import game.script.mail.MailMessage;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.baa.C5473aKr;
import logic.data.link.C5596aPk;
import logic.res.KeyCode;
import logic.res.code.C5663aRz;
import logic.ui.item.Scrollable;
import logic.ui.item.*;
import p001a.C0277Da;
import p001a.C2602hR;
import p001a.C6622aqW;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.mail")
/* compiled from: a */
public class MailAddon implements C2495fo {
    /* access modifiers changed from: private */
    public InternalFrame atd;
    /* access modifiers changed from: private */
    public Table atg;
    /* access modifiers changed from: private */
    public MailBox ath;
    /* renamed from: kj */
    public IAddonProperties f10082kj;
    private JList ate;
    private Scrollable atf;
    private C4673g ati;
    private MailBox atj;
    private C4673g atk;
    private GBox atl;
    private EditorPane atm;
    private C6622aqW atn;
    /* access modifiers changed from: private */
    private MailMessage ato;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10082kj = addonPropertie;
        this.atn = new C4671e("mail", addonPropertie.translate("Mail"), "mail");
        this.atn.mo15602jH(this.f10082kj.translate("Send email to your friends and fans!"));
        this.atn.mo15603jI("MAIL_WINDOW");
        addonPropertie.aVU().publish(new C5344aFs(this.atn, 809, false));
        this.f10082kj.mo2317P(this);
    }

    /* access modifiers changed from: private */
    public void open() {
        if (this.atd == null) {
            init();
        } else if (this.atd.isVisible()) {
            this.atd.setVisible(false);
            this.atd.destroy();
            this.atd = null;
        } else {
            this.atd.setVisible(true);
        }
    }

    private void init() {
        this.atd = this.f10082kj.bHv().mo16792bN("mail.xml");
        this.atd.addComponentListener(new C4672f());
        this.atd.pack();
        this.atd.center();
        this.atd.setVisible(true);
        this.atl = this.atd.mo4915cd("msgHeader");
        this.atm = this.atd.mo4915cd("msgBody");
        this.atf = this.atd.mo4915cd("msgsListContainer");
        this.atg = this.atd.mo4915cd("msgsList");
        m44433JM();
        m44457hu();
        m44434JN();
    }

    /* renamed from: hu */
    private void m44457hu() {
        JButton cd = this.atd.mo4915cd("newMessageButton");
        cd.addActionListener(new C4669c());
        cd.setToolTipText("New Message");
        JButton cd2 = this.atd.mo4915cd("replyMessageButton");
        cd2.addActionListener(new C4670d());
        cd2.setToolTipText("Reply Message");
        JButton cd3 = this.atd.mo4915cd("replyAllMessageButton");
        cd3.addActionListener(new C4667a());
        cd3.setToolTipText("Reply Message to all");
        JButton cd4 = this.atd.mo4915cd("forwardMessageButton");
        cd4.addActionListener(new C4668b());
        cd4.setToolTipText("Forward Message");
        JButton cd5 = this.atd.mo4915cd("delMessageButton");
        cd5.addActionListener(new C4675i());
        cd5.setToolTipText("Delete Message");
    }

    /* renamed from: JM */
    private void m44433JM() {
        this.atg.setSelectionMode(0);
        this.atg.getSelectionModel().addListSelectionListener(new C4676j());
    }

    /* renamed from: JN */
    private void m44434JN() {
        this.ath = this.f10082kj.getPlayer().dwU().dem();
        this.atj = this.f10082kj.getPlayer().dwU().deo();
        this.ati = new C4677k(this.ath);
        this.atk = new C4679m(this.atj);
        this.ate = this.atd.mo4915cd("boxList");
        DefaultListModel defaultListModel = new DefaultListModel();
        this.ate.setModel(defaultListModel);
        defaultListModel.addElement(this.ath);
        defaultListModel.addElement(this.atj);
        this.ate.addListSelectionListener(new C4674h());
        mo24251JP();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44444a(MailBox afg) {
        C4673g gVar;
        if (afg == this.ath) {
            gVar = this.ati;
        } else if (afg == this.atj) {
            gVar = this.atk;
        } else {
            return;
        }
        this.atg.setModel(gVar);
        this.atg.getColumnModel().getColumn(0).setPreferredWidth(KeyCode.ctY);
        afg.mo8319a(C5596aPk.iAV, (C5473aKr<?, ?>) new C4678l(gVar));
        this.atf.validate();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m44442a(MailMessage asf) {
        StringBuilder sb = new StringBuilder();
        int size = asf.ctO().size();
        for (int i = 0; i < size; i++) {
            sb.append(asf.ctO().get(i).getName());
            if (i < size - 1) {
                sb.append(", ");
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44449b(MailMessage asf) {
        String subject = asf == null ? " " : asf.getSubject();
        String name = asf == null ? " " : asf.mo15962KJ().getName();
        String a = asf == null ? " " : m44442a(asf);
        String ctR = asf == null ? " " : asf.ctR();
        String ctL = asf == null ? "" : asf.ctL();
        this.atl.mo4917cf("subject").setText("Subject:");
        this.atl.mo4917cf("subjectText").setText(subject);
        this.atl.mo4917cf("from").setText("From:");
        this.atl.mo4917cf("fromText").setText(name);
        this.atl.mo4917cf("to").setText("To:");
        this.atl.mo4917cf("toText").setText(a);
        this.atl.mo4917cf("date").setText("Date:");
        this.atl.mo4917cf("dateText").setText(ctR);
        this.atm.setText(ctL);
        this.ato = asf;
    }

    /* access modifiers changed from: protected */
    /* renamed from: JO */
    public void mo24250JO() {
        m44448b(this.atj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: JP */
    public void mo24251JP() {
        m44448b(this.ath);
    }

    /* renamed from: b */
    private void m44448b(MailBox afg) {
        this.ate.setSelectedValue(afg, true);
        this.ate.validate();
        m44449b((MailMessage) null);
    }

    /* renamed from: JQ */
    private InternalFrame m44435JQ() {
        InternalFrame bN = this.f10082kj.bHv().mo16792bN("message.xml");
        bN.moveToFront();
        return bN;
    }

    /* access modifiers changed from: private */
    /* renamed from: JR */
    public void m44436JR() {
        new C0277Da(this, m44435JQ());
    }

    /* access modifiers changed from: private */
    /* renamed from: JS */
    public void m44437JS() {
        if (this.ato != null) {
            new C0277Da(this, m44435JQ(), C0277Da.C0279b.REPLY, this.ato);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: JT */
    public void m44438JT() {
        if (this.ato != null) {
            new C0277Da(this, m44435JQ(), C0277Da.C0279b.REPLYALL, this.ato);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: JU */
    public void m44439JU() {
        if (this.ato != null) {
            new C0277Da(this, m44435JQ(), C0277Da.C0279b.FORWARD, this.ato);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: JV */
    public void m44440JV() {
        MailMessage asf = (MailMessage) this.atg.getValueAt(this.atg.getSelectedRow(), 0);
        if (asf != null) {
            if (this.ate.getSelectedValue() == this.ath) {
                this.ath.mo8773h(asf);
            } else if (this.ate.getSelectedValue() == this.atj) {
                this.atj.mo8773h(asf);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo24254c(MailMessage asf) {
        this.ath.mo8771f(asf);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo24255d(MailMessage asf) {
        this.atj.mo8771f(asf);
    }

    public void stop() {
        if (this.atd != null) {
            this.atd.destroy();
            this.atd = null;
        }
    }

    /* renamed from: JW */
    public IAddonProperties mo24252JW() {
        return this.f10082kj;
    }

    @C2602hR(mo19255zf = "MAIL_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24253al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$g */
    /* compiled from: a */
    private abstract class C4673g extends AbstractTableModel {


        /* renamed from: UI */
        private final MailBox f10090UI;

        /* synthetic */ C4673g(MailAddon mailAddon, MailBox afg, C4673g gVar) {
            this(afg);
        }

        private C4673g(MailBox afg) {
            this.f10090UI = afg;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abstract Object mo24261a(MailMessage asf, int i);

        public int getRowCount() {
            return this.f10090UI.cZh().size();
        }

        public Object getValueAt(int i, int i2) {
            if (i < 0 || i >= this.f10090UI.cZh().size()) {
                return null;
            }
            return mo24261a(this.f10090UI.cZh().get(i), i2);
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$k */
    /* compiled from: a */
    private final class C4677k extends C4673g {


        public C4677k(MailBox afg) {
            super(MailAddon.this, afg, (C4673g) null);
        }

        public int getColumnCount() {
            return 4;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Object mo24261a(MailMessage asf, int i) {
            switch (i) {
                case 0:
                    return asf;
                case 1:
                    return Boolean.valueOf(asf.isRead());
                case 2:
                    return asf.mo15962KJ().getName();
                case 3:
                    return asf.ctR();
                default:
                    return "";
            }
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return "Subject";
                case 1:
                    return "Read";
                case 2:
                    return "From";
                case 3:
                    return "Date";
                default:
                    return "";
            }
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$m */
    /* compiled from: a */
    private final class C4679m extends C4673g {


        public C4679m(MailBox afg) {
            super(MailAddon.this, afg, (C4673g) null);
        }

        public int getColumnCount() {
            return 3;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Object mo24261a(MailMessage asf, int i) {
            switch (i) {
                case 0:
                    return asf;
                case 1:
                    return MailAddon.this.m44442a(asf);
                case 2:
                    return asf.ctR();
                default:
                    return "";
            }
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return "Subject";
                case 1:
                    return "To";
                case 2:
                    return "Date";
                default:
                    return "";
            }
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$e */
    /* compiled from: a */
    class C4671e extends C6622aqW {


        C4671e(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return MailAddon.this.atd != null && MailAddon.this.atd.isVisible();
        }

        public boolean isEnabled() {
            return MailAddon.this.f10082kj.getPlayer().bQB() && MailAddon.this.f10082kj.getPlayer().bhE().getClass() == Station.class;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (isEnabled()) {
                MailAddon.this.open();
            }
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$f */
    /* compiled from: a */
    class C4672f extends ComponentAdapter {
        C4672f() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            MailAddon.this.atd = null;
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$c */
    /* compiled from: a */
    class C4669c implements ActionListener {
        C4669c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MailAddon.this.m44436JR();
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$d */
    /* compiled from: a */
    class C4670d implements ActionListener {
        C4670d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MailAddon.this.m44437JS();
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$a */
    class C4667a implements ActionListener {
        C4667a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MailAddon.this.m44438JT();
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$b */
    /* compiled from: a */
    class C4668b implements ActionListener {
        C4668b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MailAddon.this.m44439JU();
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$i */
    /* compiled from: a */
    class C4675i implements ActionListener {
        C4675i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            MailAddon.this.m44440JV();
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$j */
    /* compiled from: a */
    class C4676j implements ListSelectionListener {
        C4676j() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (!listSelectionEvent.getValueIsAdjusting()) {
                MailMessage asf = (MailMessage) MailAddon.this.atg.getValueAt(MailAddon.this.atg.getSelectedRow(), 0);
                MailAddon.this.m44449b(asf);
                if (asf != null && MailAddon.this.ath.cZh().contains(asf)) {
                    asf.mo15970gv(true);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$h */
    /* compiled from: a */
    class C4674h implements ListSelectionListener {
        C4674h() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            MailBox afg;
            if (!listSelectionEvent.getValueIsAdjusting() && (afg = (MailBox) ((JList) listSelectionEvent.getSource()).getSelectedValue()) != null) {
                MailAddon.this.m44444a(afg);
            }
        }
    }

    /* renamed from: taikodom.addon.mail.MailAddon$l */
    /* compiled from: a */
    class C4678l implements C5473aKr<MailBox, Object> {
        private final /* synthetic */ C4673g iKk;

        C4678l(C4673g gVar) {
            this.iKk = gVar;
        }

        /* renamed from: a */
        public void mo1102b(MailBox afg, C5663aRz arz, Object[] objArr) {
            this.iKk.fireTableDataChanged();
            MailAddon.this.atg.getParent().invalidate();
        }

        /* renamed from: b */
        public void mo1101a(MailBox afg, C5663aRz arz, Object[] objArr) {
            this.iKk.fireTableDataChanged();
            MailAddon.this.atg.getParent().invalidate();
        }
    }
}
