package taikodom.addon.jump;

import com.hoplon.geometry.Vec3f;
import game.script.space.Gate;
import logic.IAddonSettings;
import logic.aaa.C3097nl;
import p001a.C1950ap;
import p001a.C5916acs;
import p001a.C6124ags;
import p001a.aQA;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.scene.SceneObject;

@TaikodomAddon("taikodom.addon.jump")
/* compiled from: a */
public class JumpAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: BN */
    public Gate f10007BN;
    private C1950ap gKX;
    private SceneObject gKY;
    private C6124ags<C3097nl> gKZ;
    private C6124ags<aQA> gLa;

    /* renamed from: kj */
    private IAddonProperties f10008kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10008kj = addonPropertie;
        this.gKZ = new C4626a();
        this.gLa = new C4627b();
        this.f10008kj.aVU().mo13965a(C3097nl.class, this.gKZ);
        this.f10008kj.aVU().mo13965a(aQA.class, this.gLa);
    }

    public void stop() {
        if (this.gKX != null) {
            this.gKX.stop();
            this.gKX = null;
        }
    }

    /* access modifiers changed from: private */
    public void cAC() {
        this.gKY = (SceneObject) C5916acs.getSingolton().getEngineGraphics().mo3047bT(this.f10008kj.getPlayer().bQx().mo648ip());
        this.gKY.setVelocity(new Vec3f(0.0f, 0.0f, -600.0f));
        this.gKX = new C1950ap(this.f10008kj, this.f10007BN, this.gKY);
        this.gKX.start();
    }

    /* access modifiers changed from: private */
    public void cAD() {
        if (this.gKX != null) {
            this.gKX.cuO();
            this.gKX = null;
        }
    }

    /* renamed from: taikodom.addon.jump.JumpAddon$a */
    class C4626a extends C6124ags<C3097nl> {
        C4626a() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3097nl nlVar) {
            JumpAddon.this.f10007BN = nlVar.f8747BN;
            JumpAddon.this.cAC();
            return false;
        }
    }

    /* renamed from: taikodom.addon.jump.JumpAddon$b */
    /* compiled from: a */
    class C4627b extends C6124ags<aQA> {
        C4627b() {
        }

        /* renamed from: a */
        public boolean updateInfo(aQA aqa) {
            JumpAddon.this.cAD();
            return false;
        }
    }
}
