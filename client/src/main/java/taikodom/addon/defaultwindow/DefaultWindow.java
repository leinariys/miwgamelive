package taikodom.addon.defaultwindow;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C6623aqX;
import game.script.item.Item;
import game.script.item.ShipItem;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import game.script.space.LootItems;
import game.script.space.Node;
import logic.IAddonSettings;
import logic.baa.aDJ;
import logic.ui.IBaseUiTegXml;
import p001a.C2125cH;
import p001a.C5916acs;
import p001a.C5956adg;
import p001a.aEP;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@TaikodomAddon("taikodom.addon.defaultwindow")
/* compiled from: a */
public class DefaultWindow implements C2495fo {

    /* renamed from: kj */
    private IAddonProperties f9866kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9866kj = addonPropertie;
        IBaseUiTegXml.initBaseUItegXML().getRootPane().getContentPane().setTransferHandler(new C4348b(this, (C4348b) null));
    }

    public void stop() {
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public void m43341i(Collection<aDJ> collection) {
        String str;
        NLSItem sh = (NLSItem) C5916acs.getSingolton().getTaikodom().aIY().mo6310c(NLSManager.C1472a.ITEM);
        Node azW = this.f9866kj.getPlayer().bhE().azW();
        if (azW == null) {
            throw new IllegalStateException("Player is at no node!");
        } else if (azW.mo21621Zk()) {
            this.f9866kj.aVU().mo13975h(new C6623aqX(C0939Nn.C0940a.ERROR, sh.btq(), new Object[0]));
        } else {
            ArrayList arrayList = new ArrayList();
            for (aDJ next : collection) {
                if (next instanceof Item) {
                    Item auq = (Item) next;
                    if (auq.cxh()) {
                        this.f9866kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f9866kj.translate("You cannot destroy a quest item"), new Object[0]));
                        return;
                    } else if (auq.cxa() && !(auq.bNh() instanceof LootItems)) {
                        arrayList.add(auq);
                    } else if (auq instanceof ShipItem) {
                        this.f9866kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f9866kj.translate("You cannot destroy a loaded ship"), new Object[0]));
                        return;
                    } else {
                        this.f9866kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, C5956adg.format(this.f9866kj.translate("You cannot destroy the item {0}"), auq.bAP().mo19891ke().get()), new Object[0]));
                        return;
                    }
                }
            }
            if (!arrayList.isEmpty()) {
                if (arrayList.size() == 1) {
                    str = String.valueOf(sh.bsW().get()) + ((Item) arrayList.get(0)).bAP().mo19891ke();
                } else {
                    str = sh.btm().get();
                }
                ((MessageDialogAddon) this.f9866kj.mo11830U(MessageDialogAddon.class)).mo24307a(String.valueOf(str) + "?", (aEP) new C4347a(arrayList), this.f9866kj.translate("No"), this.f9866kj.translate("Yes"));
            }
        }
    }

    /* renamed from: taikodom.addon.defaultwindow.DefaultWindow$b */
    /* compiled from: a */
    private class C4348b extends TransferHandler {
        private static final long serialVersionUID = 2731794056355706935L;

        private C4348b() {
        }

        /* synthetic */ C4348b(DefaultWindow defaultWindow, C4348b bVar) {
            this();
        }

        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            return transferSupport.isDataFlavorSupported(C2125cH.f6026vP);
        }

        public boolean importData(TransferHandler.TransferSupport transferSupport) {
            try {
                DefaultWindow.this.m43341i((Collection) transferSupport.getTransferable().getTransferData(C2125cH.f6026vP));
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } catch (UnsupportedFlavorException e2) {
                e2.printStackTrace();
                return false;
            }
        }
    }

    /* renamed from: taikodom.addon.defaultwindow.DefaultWindow$a */
    class C4347a extends aEP {
        private final /* synthetic */ List aMV;

        C4347a(List list) {
            this.aMV = list;
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                for (Item cxj : this.aMV) {
                    cxj.cxj();
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }
}
