package taikodom.addon.tabattacher;

import logic.IAddonSettings;
import logic.aVH;
import logic.aaa.C4086zH;
import logic.aaa.C5685aSv;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.util.HashMap;

@TaikodomAddon("taikodom.addon.tabattacher")
/* compiled from: a */
public class TabAttacherAddon implements C2495fo {
    private HashMap<String, C4086zH> fFu = new HashMap<>();
    private aVH<C5685aSv> fFv;
    private aVH<C4086zH> fFw;

    /* renamed from: kj */
    private IAddonProperties f10278kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10278kj = addonPropertie;
        this.fFv = new C5013b();
        this.fFw = new C5012a();
        this.f10278kj.aVU().mo13965a(C5685aSv.class, this.fFv);
        this.f10278kj.aVU().mo13965a(C4086zH.class, this.fFw);
        for (C4086zH a : this.f10278kj.aVU().mo13967b(C4086zH.class)) {
            m45533a(a);
        }
        for (C5685aSv a2 : this.f10278kj.aVU().mo13967b(C5685aSv.class)) {
            m45532a(a2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m45532a(C5685aSv asv) {
        this.fFu.get(asv.dur()).mo23269a(asv);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m45536b(C5685aSv asv) {
        this.fFu.get(asv.dur()).mo23271b(asv);
    }

    public void stop() {
        for (String str : this.fFu.keySet()) {
            this.fFu.get(str).dui();
        }
        this.fFu.clear();
        this.f10278kj.aVU().mo13964a(this.fFv);
        this.f10278kj.aVU().mo13964a(this.fFw);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m45533a(C4086zH zHVar) {
        this.fFu.put(zHVar.getKey(), zHVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m45537b(C4086zH zHVar) {
        this.fFu.remove(zHVar.getKey());
    }

    /* renamed from: taikodom.addon.tabattacher.TabAttacherAddon$b */
    /* compiled from: a */
    class C5013b implements aVH<C5685aSv> {
        C5013b() {
        }

        /* renamed from: a */
        public void mo2004k(String str, C5685aSv asv) {
            TabAttacherAddon.this.m45532a(asv);
        }

        /* renamed from: b */
        public void mo2003f(String str, C5685aSv asv) {
            TabAttacherAddon.this.m45536b(asv);
        }
    }

    /* renamed from: taikodom.addon.tabattacher.TabAttacherAddon$a */
    class C5012a implements aVH<C4086zH> {
        C5012a() {
        }

        /* renamed from: a */
        public void mo2004k(String str, C4086zH zHVar) {
            TabAttacherAddon.this.m45533a(zHVar);
        }

        /* renamed from: b */
        public void mo2003f(String str, C4086zH zHVar) {
            TabAttacherAddon.this.m45537b(zHVar);
        }
    }
}
