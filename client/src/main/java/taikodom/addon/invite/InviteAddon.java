package taikodom.addon.invite;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C6205aiV;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.aaa.C1506WA;
import logic.aaa.C5476aKu;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import java.util.HashMap;

@TaikodomAddon("taikodom.addon.invite")
/* compiled from: a */
public class InviteAddon implements C2495fo {
    private static final String gcZ = "invite";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9992P;
    /* access modifiers changed from: private */
    public C6622aqW dcE;
    /* access modifiers changed from: private */
    public HashMap<C6205aiV.C1909a, String> gdb;
    /* renamed from: kj */
    public IAddonProperties f9993kj;
    /* access modifiers changed from: private */
    private C6124ags<C6205aiV> gda;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9993kj = addonPropertie;
        this.f9992P = this.f9993kj.getPlayer();
        this.gdb = new HashMap<>();
        this.gdb.put(C6205aiV.C1909a.PARTY, this.f9993kj.translate("Party Invitation"));
        this.gdb.put(C6205aiV.C1909a.TRADE, this.f9993kj.translate("Trade Invitation"));
        this.gdb.put(C6205aiV.C1909a.FRIEND, this.f9993kj.translate("Friendship Invitation"));
        this.gdb.put(C6205aiV.C1909a.CORP_HIRE, this.f9993kj.translate("Corporation Hire"));
        this.gdb.put(C6205aiV.C1909a.CORP_CREATION, this.f9993kj.translate("Corporation Invitation"));
        this.dcE = new C4593c("", "", gcZ);
        this.gda = new C4592b();
        this.f9993kj.aVU().mo13965a(C6205aiV.class, this.gda);
    }

    public void stop() {
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m44239d(C6205aiV aiv) {
        if (aiv != null) {
            C6205aiV.C1909a ckf = aiv.ckf();
            if (C6205aiV.C1909a.PARTY.equals(ckf)) {
                m44234bH(aiv.cke());
            } else if (C6205aiV.C1909a.TRADE.equals(ckf)) {
                m44235bI(aiv.cke());
            } else if (C6205aiV.C1909a.FRIEND.equals(ckf)) {
                m44236bJ(aiv.cke());
            } else if (C6205aiV.C1909a.CORP_HIRE.equals(ckf)) {
                m44233bG(aiv.cke());
            } else if (C6205aiV.C1909a.CORP_CREATION.equals(ckf)) {
                m44232bF(aiv.cke());
            }
        }
    }

    /* renamed from: bF */
    private void m44232bF(Player aku) {
        if (aku != null) {
            ((MessageDialogAddon) this.f9993kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f9993kj.translate("You were invited to join a corporation by {0}"), aku.getName()), (aEP) new C4595e(aku), this.f9993kj.translate("Answer later"), this.f9993kj.translate("Decline"), this.f9993kj.translate("Accept"));
        }
    }

    /* renamed from: bG */
    private void m44233bG(Player aku) {
        if (aku != null) {
            ((MessageDialogAddon) this.f9993kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f9993kj.translate("You were invited to join a corporation by {0}"), aku.getName()), (aEP) new C4594d(aku), this.f9993kj.translate("Answer later"), this.f9993kj.translate("Decline"), this.f9993kj.translate("Accept"));
        }
    }

    /* renamed from: bH */
    private void m44234bH(Player aku) {
        if (aku != null) {
            ((MessageDialogAddon) this.f9993kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f9993kj.translate("You were invited to join a squadron by {0}"), aku.getName()), (aEP) new C4597g(aku), this.f9993kj.translate("Answer later"), this.f9993kj.translate("Decline"), this.f9993kj.translate("Accept"));
        }
    }

    /* renamed from: bI */
    private void m44235bI(Player aku) {
        if (aku != null) {
            ((MessageDialogAddon) this.f9993kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f9993kj.translate("You received a trade invitation from {0}. Accept it?"), aku.getName()), (aEP) new C4596f(aku), this.f9993kj.translate("Answer later"), this.f9993kj.translate("Decline"), this.f9993kj.translate("Accept"));
        }
    }

    /* renamed from: bJ */
    private void m44236bJ(Player aku) {
        if (aku != null) {
            ((MessageDialogAddon) this.f9993kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f9993kj.translate("You received a friendship invitation from {0}. Accept it?"), aku.getName()), (aEP) new C4591a(aku), this.f9993kj.translate("Answer later"), this.f9993kj.translate("Decline"), this.f9993kj.translate("Accept"));
        }
    }

    /* renamed from: taikodom.addon.invite.InviteAddon$c */
    /* compiled from: a */
    class C4593c extends C6622aqW {


        C4593c(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length > 1 && (objArr[1] instanceof C6205aiV)) {
                InviteAddon.this.m44239d(objArr[1]);
            }
        }
    }

    /* renamed from: taikodom.addon.invite.InviteAddon$b */
    /* compiled from: a */
    class C4592b extends C6124ags<C6205aiV> {
        C4592b() {
        }

        /* renamed from: b */
        public boolean updateInfo(C6205aiV aiv) {
            Player cke = aiv.cke();
            if (InviteAddon.this.f9992P.cWm() == cke.cWm()) {
                ((MessageDialogAddon) InviteAddon.this.f9993kj.mo11830U(MessageDialogAddon.class)).mo24306a(C0939Nn.C0940a.ERROR, InviteAddon.this.f9993kj.translate("You cannot invite yourself."), InviteAddon.this.f9993kj.translate("OK"));
            } else if (!InviteAddon.this.f9993kj.getPlayer().dxu().mo8514ct(cke)) {
                InviteAddon.this.f9993kj.aVU().mo13975h(new C5476aKu(InviteAddon.gcZ, InviteAddon.this.dcE, new C6274ajm(String.valueOf(aiv.cke().cWm()) + "." + aiv.ckf().toString(), (String) InviteAddon.this.gdb.get(aiv.ckf()), aiv), false));
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.invite.InviteAddon$e */
    /* compiled from: a */
    class C4595e extends aEP {
        private final /* synthetic */ Player aCs;

        C4595e(Player aku) {
            this.aCs = aku;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            switch (i) {
                case 0:
                    InviteAddon.this.f9992P.mo14419f((C1506WA) new C6205aiV(C6205aiV.C1909a.CORP_CREATION, this.aCs));
                    return;
                case 1:
                    this.aCs.mo14439r(InviteAddon.this.f9992P, false);
                    return;
                case 2:
                    this.aCs.mo14439r(InviteAddon.this.f9992P, true);
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public String getHeader() {
            return (String) InviteAddon.this.gdb.get(C6205aiV.C1909a.CORP_HIRE);
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }

    /* renamed from: taikodom.addon.invite.InviteAddon$d */
    /* compiled from: a */
    class C4594d extends aEP {
        private final /* synthetic */ Player aCs;

        C4594d(Player aku) {
            this.aCs = aku;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            switch (i) {
                case 0:
                    InviteAddon.this.f9992P.mo14419f((C1506WA) new C6205aiV(C6205aiV.C1909a.CORP_HIRE, this.aCs));
                    return;
                case 1:
                    this.aCs.mo14442t(InviteAddon.this.f9992P, false);
                    return;
                case 2:
                    this.aCs.mo14442t(InviteAddon.this.f9992P, true);
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public String getHeader() {
            return (String) InviteAddon.this.gdb.get(C6205aiV.C1909a.CORP_HIRE);
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }

    /* renamed from: taikodom.addon.invite.InviteAddon$g */
    /* compiled from: a */
    class C4597g extends aEP {
        private final /* synthetic */ Player aCs;

        C4597g(Player aku) {
            this.aCs = aku;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            switch (i) {
                case 0:
                    InviteAddon.this.f9992P.mo14419f((C1506WA) new C6205aiV(C6205aiV.C1909a.PARTY, this.aCs));
                    return;
                case 1:
                    this.aCs.mo14422h(InviteAddon.this.f9992P, false);
                    return;
                case 2:
                    this.aCs.mo14422h(InviteAddon.this.f9992P, true);
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public String getHeader() {
            return (String) InviteAddon.this.gdb.get(C6205aiV.C1909a.PARTY);
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }

    /* renamed from: taikodom.addon.invite.InviteAddon$f */
    /* compiled from: a */
    class C4596f extends aEP {
        private final /* synthetic */ Player aCs;

        C4596f(Player aku) {
            this.aCs = aku;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            switch (i) {
                case 0:
                    InviteAddon.this.f9992P.mo14419f((C1506WA) new C6205aiV(C6205aiV.C1909a.PARTY, this.aCs));
                    return;
                case 1:
                    this.aCs.mo14433n(InviteAddon.this.f9993kj.getPlayer(), false);
                    return;
                case 2:
                    this.aCs.mo14433n(InviteAddon.this.f9993kj.getPlayer(), true);
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        public String getHeader() {
            return (String) InviteAddon.this.gdb.get(C6205aiV.C1909a.TRADE);
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }

    /* renamed from: taikodom.addon.invite.InviteAddon$a */
    class C4591a extends aEP {
        private final /* synthetic */ Player aCs;

        C4591a(Player aku) {
            this.aCs = aku;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            switch (i) {
                case 0:
                    InviteAddon.this.f9992P.mo14419f((C1506WA) new C6205aiV(C6205aiV.C1909a.FRIEND, this.aCs));
                    return;
                case 1:
                    this.aCs.mo14438p(InviteAddon.this.f9993kj.getPlayer(), false);
                    return;
                case 2:
                    this.aCs.mo14438p(InviteAddon.this.f9993kj.getPlayer(), true);
                    return;
                default:
                    return;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }

        /* access modifiers changed from: protected */
        public String getHeader() {
            return (String) InviteAddon.this.gdb.get(C6205aiV.C1909a.FRIEND);
        }
    }
}
