package taikodom.addon.invite;

import game.network.message.externalizable.C6128agw;
import game.network.message.externalizable.C6205aiV;
import logic.IAddonSettings;
import logic.aaa.C0575Hy;
import logic.aaa.C1506WA;
import logic.aaa.C1549Wj;
import logic.aaa.C2055be;
import logic.baa.C4033yO;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import p001a.C1166RG;
import p001a.C1459VT;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.invite")
/* compiled from: a */
public class InviteNewAddon implements C2495fo {
    private static final int aAy = 30000;
    private static /* synthetic */ int[] aAz;
    /* renamed from: kj */
    public IAddonProperties f9995kj;
    /* renamed from: Kq */
    private Panel f9994Kq;
    private C3428rT<C6205aiV> aAw;
    /* access modifiers changed from: private */
    private C3428rT<C1549Wj> aAx;

    /* renamed from: MH */
    static /* synthetic */ int[] m44257MH() {
        int[] iArr = aAz;
        if (iArr == null) {
            iArr = new int[C6205aiV.C1909a.values().length];
            try {
                iArr[C6205aiV.C1909a.CORP_CREATION.ordinal()] = 5;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C6205aiV.C1909a.CORP_HIRE.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C6205aiV.C1909a.FRIEND.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C6205aiV.C1909a.PARTY.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C6205aiV.C1909a.TRADE.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            aAz = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9995kj = addonPropertie;
        init();
        this.aAw = new C4600c();
        this.aAx = new C4599b();
        this.f9995kj.aVU().mo13965a(C6205aiV.class, this.aAw);
        this.f9995kj.aVU().mo13965a(C1549Wj.class, this.aAx);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44262a(C1549Wj wj) {
        Panel MF = m44255MF();
        MF.add(m44259a((C1506WA) wj));
        JLabel b = m44270b((C1506WA) wj);
        MF.add(b);
        MF.add(m44260a(wj, b, (JPanel) MF));
        m44264a((JPanel) MF);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44263a(C6205aiV aiv) {
        Panel MF = m44255MF();
        MF.add(m44259a((C1506WA) aiv));
        JLabel b = m44270b((C1506WA) aiv);
        MF.add(b);
        C4605h hVar = new C4605h(MF);
        JPanel a = m44261a(hVar, aiv, b, MF);
        JLabel MG = m44256MG();
        C0575Hy hy = new C0575Hy(hVar, this.f9995kj.translate("{0} sec."), 0, 1000, C6128agw.C1890a.WHITE, Integer.valueOf(aAy));
        hy.mo2708a((C1166RG) new C4604g(MG, a, MF));
        hy.start();
        MF.add(MG);
        MF.add(a);
        m44264a((JPanel) MF);
    }

    /* renamed from: a */
    private void m44264a(JPanel jPanel) {
        while (this.f9994Kq.getComponents().length >= 3) {
            this.f9994Kq.remove(this.f9994Kq.getComponents().length - 1);
        }
        this.f9994Kq.add((Component) jPanel, 0);
        m44272qq();
    }

    /* renamed from: MF */
    private Panel m44255MF() {
        Panel aco = new Panel();
        aco.setLayout(new BoxLayout(aco, 0));
        IComponentManager e = ComponentManager.getCssHolder(aco);
        e.setAttribute("class", "event");
        e.mo13045Vk();
        return aco;
    }

    /* renamed from: a */
    private JLabel m44259a(C1506WA wa) {
        JLabel jLabel = new JLabel();
        IComponentManager e = ComponentManager.getCssHolder(jLabel);
        String str = "";
        if (wa instanceof C6205aiV) {
            switch (m44257MH()[((C6205aiV) wa).ckf().ordinal()]) {
                case 1:
                    str = "tradeIcon";
                    break;
                case 2:
                    str = "friendIcon";
                    break;
                case 3:
                    str = "partyIcon";
                    break;
                case 4:
                    str = "corpIcon";
                    break;
            }
        } else {
            str = "pdaIcon";
        }
        e.setAttribute("class", str);
        e.mo13045Vk();
        return jLabel;
    }

    /* renamed from: b */
    private JLabel m44270b(C1506WA wa) {
        JLabel jLabel;
        if (wa instanceof C6205aiV) {
            jLabel = new JLabel(((C6205aiV) wa).cke().getName());
        } else {
            jLabel = new JLabel(this.f9995kj.translate("New PDA Info"));
        }
        IComponentManager e = ComponentManager.getCssHolder(jLabel);
        e.setAttribute("class", "inviter");
        e.mo13045Vk();
        return jLabel;
    }

    /* renamed from: MG */
    private JLabel m44256MG() {
        JLabel jLabel = new JLabel("");
        IComponentManager e = ComponentManager.getCssHolder(jLabel);
        e.setAttribute("class", "status");
        e.mo13045Vk();
        return jLabel;
    }

    /* renamed from: a */
    private JPanel m44260a(C1549Wj wj, JLabel jLabel, JPanel jPanel) {
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new BoxLayout(jPanel2, 0));
        JButton jButton = new JButton();
        IComponentManager e = ComponentManager.getCssHolder(jButton);
        e.setAttribute("class", "buttonSee");
        e.mo13045Vk();
        jButton.addActionListener(new C4598a(wj.mo6622go()));
        jPanel2.add(jButton);
        JButton jButton2 = new JButton();
        IComponentManager e2 = ComponentManager.getCssHolder(jButton2);
        e2.setAttribute("class", "buttonClose");
        e2.mo13045Vk();
        jButton2.addActionListener(new C4602e(jPanel));
        jPanel2.add(jButton2);
        return jPanel2;
    }

    /* renamed from: a */
    private JPanel m44261a(C4605h hVar, C6205aiV aiv, JLabel jLabel, JPanel jPanel) {
        JPanel jPanel2 = new JPanel();
        jPanel2.setLayout(new BoxLayout(jPanel2, 0));
        ComponentManager.getCssHolder(jPanel2).mo13045Vk();
        JButton jButton = new JButton();
        IComponentManager e = ComponentManager.getCssHolder(jButton);
        e.setAttribute("class", "buttonYes");
        e.mo13045Vk();
        jButton.addActionListener(new C4603f(aiv, true, jPanel2, jPanel, hVar));
        jPanel2.add(jButton);
        JButton jButton2 = new JButton();
        IComponentManager e2 = ComponentManager.getCssHolder(jButton2);
        e2.setAttribute("class", "buttonNo");
        e2.mo13045Vk();
        jButton2.addActionListener(new C4603f(aiv, false, jPanel2, jPanel, hVar));
        jPanel2.add(jButton2);
        return jPanel2;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44265a(JPanel jPanel, JPanel jPanel2) {
        jPanel.removeAll();
        JButton jButton = new JButton();
        IComponentManager e = ComponentManager.getCssHolder(jButton);
        e.setAttribute("class", "buttonClose");
        e.mo13045Vk();
        jButton.addActionListener(new C4601d(jPanel2));
        jPanel.add(jButton);
        this.f9994Kq.validate();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44271b(JPanel jPanel) {
        jPanel.setVisible(false);
        this.f9994Kq.remove(jPanel);
        m44272qq();
    }

    private void init() {
        Panel aco = (Panel) this.f9995kj.bHv().mo16794bQ("invitenew.xml");
        aco.setVisible(true);
        aco.setLayout((LayoutManager) null);
        aco.pack();
        Dimension size = aco.getSize();
        aco.setLocation(this.f9995kj.ale().aes() - size.width, this.f9995kj.ale().aet() - size.height);
        this.f9994Kq = aco.mo4915cd("eventPanel");
        m44272qq();
    }

    /* renamed from: qq */
    private void m44272qq() {
        this.f9994Kq.setSize(this.f9994Kq.getParent().getWidth(), this.f9994Kq.getPreferredSize().height);
        this.f9994Kq.setLocation(0, Math.max(0, this.f9994Kq.getParent().getHeight() - this.f9994Kq.getHeight()));
    }

    public void stop() {
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$c */
    /* compiled from: a */
    class C4600c extends C3428rT<C6205aiV> {
        C4600c() {
        }

        /* renamed from: c */
        public void mo321b(C6205aiV aiv) {
            InviteNewAddon.this.m44263a(aiv);
        }
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$b */
    /* compiled from: a */
    class C4599b extends C3428rT<C1549Wj> {
        C4599b() {
        }

        /* renamed from: b */
        public void mo321b(C1549Wj wj) {
            InviteNewAddon.this.m44262a(wj);
        }
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$g */
    /* compiled from: a */
    class C4604g implements C1166RG {

        /* renamed from: BL */
        private JPanel f9998BL;
        private JPanel bMw;
        private JLabel enb;

        public C4604g(JLabel jLabel, JPanel jPanel, JPanel jPanel2) {
            this.enb = jLabel;
            this.f9998BL = jPanel;
            this.bMw = jPanel2;
        }

        /* renamed from: b */
        public void mo5161b(String str, Object... objArr) {
            if (objArr.length > 0 && objArr[0].intValue() < 1) {
                this.enb.setText(InviteNewAddon.this.f9995kj.translate("Canceled"));
                new aDX(this.bMw, "[255..150] dur 100", C2830kk.asS, (C0454GJ) null);
            }
            InviteNewAddon.this.m44265a(this.f9998BL, this.bMw);
        }

        /* renamed from: a */
        public void mo5160a(String str, Object... objArr) {
            this.enb.setText(str);
        }
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$a */
    class C4598a implements ActionListener {

        /* renamed from: nf */
        private C4033yO f9996nf;

        public C4598a(C4033yO yOVar) {
            this.f9996nf = yOVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InviteNewAddon.this.f9995kj.aVU().mo13975h(new C2055be(this.f9996nf));
        }
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$f */
    /* compiled from: a */
    class C4603f implements ActionListener {
        private static /* synthetic */ int[] aAz;
        C4605h bMx;
        C6205aiV bMy;
        boolean result;
        /* renamed from: BL */
        private JPanel f9997BL;
        private JPanel bMw;

        public C4603f(C6205aiV aiv, boolean z, JPanel jPanel, JPanel jPanel2, C4605h hVar) {
            this.bMy = aiv;
            this.result = z;
            this.f9997BL = jPanel;
            this.bMw = jPanel2;
            this.bMx = hVar;
        }

        /* renamed from: MH */
        static /* synthetic */ int[] m44279MH() {
            int[] iArr = aAz;
            if (iArr == null) {
                iArr = new int[C6205aiV.C1909a.values().length];
                try {
                    iArr[C6205aiV.C1909a.CORP_CREATION.ordinal()] = 5;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C6205aiV.C1909a.CORP_HIRE.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C6205aiV.C1909a.FRIEND.ordinal()] = 2;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C6205aiV.C1909a.PARTY.ordinal()] = 3;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[C6205aiV.C1909a.TRADE.ordinal()] = 1;
                } catch (NoSuchFieldError e5) {
                }
                aAz = iArr;
            }
            return iArr;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            switch (m44279MH()[this.bMy.ckf().ordinal()]) {
                case 1:
                    this.bMy.cke().mo14433n(InviteNewAddon.this.f9995kj.getPlayer(), this.result);
                    break;
                case 2:
                    this.bMy.cke().mo14438p(InviteNewAddon.this.f9995kj.getPlayer(), this.result);
                    break;
                case 3:
                    this.bMy.cke().mo14422h(InviteNewAddon.this.f9995kj.getPlayer(), this.result);
                    break;
                case 4:
                    this.bMy.cke().mo14442t(InviteNewAddon.this.f9995kj.getPlayer(), this.result);
                    break;
            }
            new aDX(this.bMw, "[255..150] dur 100", C2830kk.asS, (C0454GJ) null);
            InviteNewAddon.this.m44265a(this.f9997BL, this.bMw);
            this.bMx.mo24177gb(true);
        }
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$h */
    /* compiled from: a */
    class C4605h implements C1459VT {
        private JPanel bMw;
        private boolean gpp = false;

        public C4605h(JPanel jPanel) {
            this.bMw = jPanel;
        }

        /* renamed from: a */
        public void mo6145a(int i, Object... objArr) {
            Integer num = objArr[0];
            if (num.intValue() >= 1000) {
                num = Integer.valueOf(num.intValue() / 1000);
            }
            objArr[0] = Integer.valueOf(num.intValue() - (i / 1000));
        }

        /* renamed from: b */
        public boolean mo6146b(Object... objArr) {
            return this.gpp || objArr[0].intValue() < 1;
        }

        /* renamed from: gb */
        public void mo24177gb(boolean z) {
            this.gpp = z;
        }
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$e */
    /* compiled from: a */
    class C4602e implements ActionListener {
        private final /* synthetic */ JPanel bDk;

        C4602e(JPanel jPanel) {
            this.bDk = jPanel;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InviteNewAddon.this.m44271b(this.bDk);
        }
    }

    /* renamed from: taikodom.addon.invite.InviteNewAddon$d */
    /* compiled from: a */
    class C4601d implements ActionListener {
        private final /* synthetic */ JPanel bDk;

        C4601d(JPanel jPanel) {
            this.bDk = jPanel;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InviteNewAddon.this.m44271b(this.bDk);
        }
    }
}
