package taikodom.addon.map3d;

import game.network.message.externalizable.C5344aFs;
import game.network.message.externalizable.aTX;
import game.script.ship.Station;
import logic.IAddonSettings;
import logic.ui.item.Component3d;
import logic.ui.item.InternalFrame;
import p001a.C0730KV;
import p001a.C1180RT;
import p001a.C6622aqW;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.camera.Camera;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

@TaikodomAddon("taikodom.addon.map3d")
/* compiled from: a */
public class Map3DAddon implements C2495fo {
    /* access modifiers changed from: private */
    public C1180RT fop;
    /* access modifiers changed from: private */

    /* renamed from: kj */
    public IAddonProperties f10103kj;
    /* access modifiers changed from: private */

    /* renamed from: nM */
    public InternalFrame f10104nM;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10103kj = addonPropertie;
        C4705b bVar = new C4705b("map3d", addonPropertie.translate("Map 3D"), "map3d");
        addonPropertie.aVU().publish(new C5344aFs(bVar, 810, false));
        addonPropertie.aVU().publish(new aTX(bVar, 810, "map3d-commands.css", this));
    }

    /* renamed from: JW */
    public IAddonProperties mo24290JW() {
        return this.f10103kj;
    }

    public void open() {
        if (this.f10104nM == null) {
            init();
        } else if (this.f10104nM.isVisible()) {
            this.f10104nM.setVisible(false);
            this.f10104nM.destroy();
            this.f10104nM = null;
        } else {
            this.f10104nM.setVisible(true);
        }
    }

    public IAddonProperties bUA() {
        return this.f10103kj;
    }

    public InternalFrame aRY() {
        return this.f10104nM;
    }

    public C1180RT bUB() {
        return this.fop;
    }

    private void init() {
        this.f10104nM = (InternalFrame) this.f10103kj.bHv().mo16794bQ("map3d.xml");
        Component3d cd = this.f10104nM.mo4915cd("mapViewer");
        new C0730KV(this);
        Camera camera = new Camera();
        cd.getSceneView().setCamera(camera);
        this.fop = new C1180RT(camera, this);
        cd.addMouseWheelListener(new C4704a());
        cd.setVisible(true);
    }

    public void stop() {
    }

    /* renamed from: taikodom.addon.map3d.Map3DAddon$b */
    /* compiled from: a */
    class C4705b extends C6622aqW {


        C4705b(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return Map3DAddon.this.f10104nM != null && Map3DAddon.this.f10104nM.isVisible();
        }

        public boolean isEnabled() {
            return Map3DAddon.this.f10103kj.getPlayer().bQB() && Map3DAddon.this.f10103kj.getPlayer().bhE().getClass() == Station.class;
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (isEnabled()) {
                Map3DAddon.this.open();
            }
        }
    }

    /* renamed from: taikodom.addon.map3d.Map3DAddon$a */
    class C4704a implements MouseWheelListener {
        C4704a() {
        }

        public void mouseWheelMoved(MouseWheelEvent mouseWheelEvent) {
            if (mouseWheelEvent.getWheelRotation() < 0) {
                Map3DAddon.this.fop.bsk();
            } else if (mouseWheelEvent.getWheelRotation() > 0) {
                Map3DAddon.this.fop.bsl();
            }
        }
    }
}
