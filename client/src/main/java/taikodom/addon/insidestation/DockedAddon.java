package taikodom.addon.insidestation;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.geometry.Vector2fWrap;
import game.network.message.externalizable.C2651i;
import game.script.corporation.CorporationLogo;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.categories.Bomber;
import game.script.ship.categories.Explorer;
import game.script.ship.categories.Fighter;
import game.script.ship.categories.Freighter;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.*;
import logic.render.IEngineGraphics;
import logic.res.sound.C0907NJ;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.gui.GBillboard;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.Material;

@TaikodomAddon("taikodom.addon.insidestation")
/* compiled from: a */
public class DockedAddon implements C2495fo {
    private static final String aiW = "ship_mark";
    private static final String aiX = "data/models/doc_spa_hangar.pro";
    private static final String aiY = "cam_mark";
    private static final String aiZ = "doc_spa_hangar";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9968P;
    /* access modifiers changed from: private */
    public GBillboard aja;
    /* access modifiers changed from: private */
    public SceneObject ajb;
    /* access modifiers changed from: private */
    public SceneObject ajc;
    /* access modifiers changed from: private */
    public SceneObject ajd;
    /* access modifiers changed from: private */
    public C3668tW aje;
    /* access modifiers changed from: private */
    public Vec3d ajf;
    /* access modifiers changed from: private */
    public C1255SZ ajg;
    /* access modifiers changed from: private */
    public float ajj;
    /* access modifiers changed from: private */
    public SceneObject ajk;
    /* access modifiers changed from: private */
    public Vec3d ajm;
    /* renamed from: bz */
    public SceneObject f9969bz;
    /* renamed from: kj */
    public IAddonProperties f9970kj;
    private C6124ags<C3285px> ajh;
    private C6124ags<C6882avW> aji;
    private C6124ags<C1274Sq> ajl;
    private C4556d ajn;
    private boolean ajo;
    private C6124ags<C5839abT> ajp;
    private C6124ags<C2651i> ajq;
    private C6124ags<C2640ht> ajr;
    private C6124ags<C6531aoj> ajs;
    private C6124ags<C6492anw> ajt;
    /* access modifiers changed from: private */
    private C6124ags<C2395eo> aju;
    /* access modifiers changed from: private */
    private C0354Ei ajv;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9970kj = addonPropertie;
        this.f9968P = addonPropertie.getPlayer();
        m44105Fb();
        m44104Fa();
    }

    /* renamed from: Fa */
    private void m44104Fa() {
        this.f9970kj.aVU().mo13965a(C6492anw.class, this.ajt);
        this.f9970kj.aVU().mo13965a(C2395eo.class, this.aju);
        this.f9970kj.aVU().mo13965a(C1274Sq.class, this.ajl);
        this.f9970kj.aVU().mo13965a(C5839abT.class, this.ajp);
        this.f9970kj.aVU().mo13965a(C2651i.class, this.ajq);
        this.f9970kj.aVU().mo13965a(C2640ht.class, this.ajr);
        this.f9970kj.aVU().mo13965a(C6531aoj.class, this.ajs);
    }

    /* renamed from: Fb */
    private void m44105Fb() {
        this.ajt = new C4564l();
        this.aju = new C4565m();
        this.ajl = new C4562j();
        this.ajp = new C4563k();
        this.ajq = new C4560h();
        this.ajr = new C4561i();
        this.ajs = new C4558f();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Fc */
    public void mo24119Fc() {
        this.f9970kj.ale().mo3004a((C6296akI.C1925a) this.ajv);
    }

    /* access modifiers changed from: protected */
    /* renamed from: Fd */
    public void mo24120Fd() {
        this.ajv = new C0354Ei(this.f9970kj, this.ajd, this.ajf);
    }

    /* access modifiers changed from: private */
    /* renamed from: Fe */
    public void m44106Fe() {
        if (this.ajd != null) {
            this.ajd.dispose();
            this.ajd = null;
        }
        if (this.ajn != null) {
            this.ajn.cancel();
            this.ajn = null;
        }
    }

    /* renamed from: Ff */
    public void mo24121Ff() {
        if (!this.ajo) {
            this.ajo = true;
            if (this.f9970kj.ale() != null) {
                m44107Fh();
                mo24123Fi();
            }
        }
    }

    /* renamed from: Fg */
    public void mo24122Fg() {
        if (this.ajo) {
            this.ajo = false;
            if (this.ajb != null) {
                this.ajb.dispose();
                this.ajb = null;
            }
            if (this.ajc != null) {
                this.ajc.dispose();
                this.ajc = null;
            }
            if (this.f9969bz != null) {
                this.f9969bz.dispose();
                this.f9969bz = null;
            }
            if (this.ajk != null) {
                this.ajk.dispose();
                this.ajk = null;
            }
        }
    }

    /* renamed from: Fh */
    private void m44107Fh() {
        this.f9970kj.alg().mo4995a(aiX, aiZ, this.f9970kj.ale().adZ(), new C4559g(this.f9970kj.ale()), "PlayerControllerInfra:updateDockedObject ");
    }

    /* renamed from: Fi */
    public void mo24123Fi() {
        IEngineGraphics ale = this.f9970kj.ale();
        Ship al = this.f9968P.dxc().mo22061al();
        this.ajh = new C4557e(al);
        this.f9970kj.aVU().mo13965a(C3285px.class, this.ajh);
        this.aji = new C4554b();
        this.f9970kj.aVU().mo13965a(C6882avW.class, this.aji);
        if (al == null) {
            if (this.f9969bz != null) {
                this.f9969bz.dispose();
                this.f9969bz = null;
            }
            if (this.ajd != null) {
                this.ajd.dispose();
                this.ajd = null;
            }
            if (this.ajg != null) {
                this.ajg.destroy();
                this.ajg = null;
            }
            this.ajm = new Vec3d(ScriptRuntime.NaN, 250.0d, 600.0d);
        } else if (ale != null) {
            if (this.f9969bz != null) {
                this.f9969bz.hide();
                this.f9969bz.dispose();
                this.f9969bz = null;
            }
            if (this.ajg != null) {
                this.ajg.destroy();
                this.ajg = null;
            }
            this.f9970kj.alg().mo4995a(al.mo649ir(), al.mo648ip(), (Scene) null, new C4555c(al, ale), "PlayerControllerInfra:UpdateActiveShip for shiptype " + al.agH().mo19891ke());
        }
    }

    /* access modifiers changed from: protected */
    @ClientOnly
    /* renamed from: a */
    public void mo24124a(SceneObject sceneObject, Ship fAVar) {
        for (Turret jq : fAVar.agF()) {
            this.f9970kj.alg().mo4995a(jq.mo649ir(), jq.mo648ip(), (Scene) null, new C4553a(jq, sceneObject), "PlayerControllerInfra:UpdateTurret for shiptype " + fAVar.agH().mo19891ke());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: Fj */
    public void m44108Fj() {
        if (this.aje != null) {
            this.aje.cancel();
        }
        this.aja = new GBillboard();
        this.aja.setPrimitiveColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        this.aja.setSize(new Vector2fWrap((float) (this.f9970kj.bHv().getScreenWidth() * 2), (float) (this.f9970kj.bHv().getScreenHeight() * 2)));
        this.aja.setPosition(new Vector2fWrap((float) (this.f9970kj.bHv().getScreenWidth() / 2), (float) (this.f9970kj.bHv().getScreenHeight() / 2)));
        this.aja.setName("fadeOutBillBoard");
        this.aja.setRender(true);
        IEngineGraphics ale = this.f9970kj.ale();
        if (ale != null) {
            Material material = new Material();
            material.setShader(ale.aej().getDefaultVertexColorShader());
            this.aja.setMaterial(material);
            ale.aeh().addChild(this.aja);
            if (this.ajn == null) {
                this.ajn = new C4556d(this, (C4556d) null);
            }
            this.ajn.caM();
            this.f9970kj.alf().addTask("Undock Animation", this.ajn, 30);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: Fk */
    public void m44109Fk() {
        if (this.ajh != null) {
            this.f9970kj.aVU().mo13964a(this.ajh);
            this.ajh = null;
        }
        if (this.aji != null) {
            this.f9970kj.aVU().mo13964a(this.aji);
            this.aji = null;
        }
        this.f9968P.dxc().cOp();
    }

    public void stop() {
        removeListeners();
    }

    private void removeListeners() {
        this.f9970kj.aVU().mo13968b(C1274Sq.class, this.ajl);
        this.f9970kj.aVU().mo13968b(C5839abT.class, this.ajp);
        this.f9970kj.aVU().mo13968b(C2651i.class, this.ajq);
        this.f9970kj.aVU().mo13968b(C2640ht.class, this.ajr);
        this.f9970kj.aVU().mo13968b(C6531aoj.class, this.ajs);
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$l */
    /* compiled from: a */
    class C4564l extends C6124ags<C6492anw> {
        C4564l() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6492anw anw) {
            DockedAddon.this.mo24120Fd();
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$m */
    /* compiled from: a */
    class C4565m extends C6124ags<C2395eo> {
        C4565m() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2395eo eoVar) {
            DockedAddon.this.mo24119Fc();
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$j */
    /* compiled from: a */
    class C4562j extends C6124ags<C1274Sq> {
        C4562j() {
        }

        /* renamed from: b */
        public boolean updateInfo(C1274Sq sq) {
            if (sq.fMR.equals(C1274Sq.C1275a.DOCK)) {
                DockedAddon.this.mo24121Ff();
                return false;
            } else if (!sq.fMR.equals(C1274Sq.C1275a.UNDOCK)) {
                return false;
            } else {
                DockedAddon.this.mo24122Fg();
                return false;
            }
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$k */
    /* compiled from: a */
    class C4563k extends C6124ags<C5839abT> {
        C4563k() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5839abT abt) {
            DockedAddon.this.m44108Fj();
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$h */
    /* compiled from: a */
    class C4560h extends C6124ags<C2651i> {
        C4560h() {
        }

        /* renamed from: b */
        public boolean updateInfo(C2651i iVar) {
            DockedAddon.this.mo24123Fi();
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$i */
    /* compiled from: a */
    class C4561i extends C6124ags<C2640ht> {
        C4561i() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2640ht htVar) {
            DockedAddon.this.m44106Fe();
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$f */
    /* compiled from: a */
    class C4558f extends C6124ags<C6531aoj> {
        C4558f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6531aoj aoj) {
            float f = 100.0f;
            if (DockedAddon.this.f9969bz != null) {
                f = (float) DockedAddon.this.f9969bz.getAabbLSLenght();
            }
            DockedAddon.this.f9970kj.aVU().mo13975h(new C6060afg(DockedAddon.this.ajd, (Vec3d) null, 0, true, f));
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$g */
    /* compiled from: a */
    class C4559g implements C0907NJ {
        private final /* synthetic */ IEngineGraphics eQX;

        C4559g(IEngineGraphics abd) {
            this.eQX = abd;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset instanceof SceneObject) {
                DockedAddon.this.ajb = (SceneObject) renderAsset;
                DockedAddon.this.ajc = (SceneObject) this.eQX.mo3047bT(DockedAddon.aiY);
                if (DockedAddon.this.ajb != null) {
                    DockedAddon.this.ajb.setPosition(new Vec3f(0.0f, 0.0f, 0.0f));
                } else {
                    DockedAddon.this.f9970kj.getLog().error("updateDockObject : invalid object");
                }
            }
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$e */
    /* compiled from: a */
    class C4557e extends C6124ags<C3285px> {
        private final /* synthetic */ Ship eQW;

        C4557e(Ship fAVar) {
            this.eQW = fAVar;
        }

        /* renamed from: a */
        public boolean updateInfo(C3285px pxVar) {
            CorporationLogo bFK = DockedAddon.this.f9968P.dxy().bFK();
            if (bFK == null) {
                return false;
            }
            C2403eu.m30125a(bFK.getHandle(), this.eQW.agH().mo4151Nu().getHandle(), DockedAddon.this.f9969bz);
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$b */
    /* compiled from: a */
    class C4554b extends C6124ags<C6882avW> {
        C4554b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6882avW avw) {
            C2403eu.m30126e(DockedAddon.this.f9969bz);
            return false;
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$c */
    /* compiled from: a */
    class C4555c implements C0907NJ {
        private final /* synthetic */ Ship eQW;
        private final /* synthetic */ IEngineGraphics eQX;

        C4555c(Ship fAVar, IEngineGraphics abd) {
            this.eQW = fAVar;
            this.eQX = abd;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            float f;
            float f2;
            float f3;
            float f4;
            if (renderAsset != null) {
                if (DockedAddon.this.f9969bz != null) {
                    DockedAddon.this.f9969bz.hide();
                    DockedAddon.this.f9969bz.dispose();
                    DockedAddon.this.f9969bz = null;
                }
                if (DockedAddon.this.ajd != null) {
                    DockedAddon.this.ajd.dispose();
                    DockedAddon.this.ajd = null;
                }
                if (this.eQW == null) {
                    DockedAddon.this.f9969bz = (SceneObject) renderAsset;
                    DockedAddon.this.f9969bz.hide();
                    DockedAddon.this.f9969bz.dispose();
                    DockedAddon.this.f9969bz = null;
                } else {
                    DockedAddon.this.f9969bz = (SceneObject) renderAsset;
                }
                DockedAddon.this.ajk = (SceneObject) this.eQX.mo3047bT(DockedAddon.aiW);
                if (DockedAddon.this.ajk == null) {
                    DockedAddon.this.f9970kj.getLog().warn("Ship mark could not be resolved");
                    return;
                }
                DockedAddon.this.ajf = DockedAddon.this.ajk.getPosition();
                if (DockedAddon.this.f9969bz != null) {
                    double d = DockedAddon.this.f9969bz.computeLocalSpaceAABB().dim().y;
                    Vec3d m = DockedAddon.this.ajf;
                    m.y = (-d) + 245.0d + m.y;
                    DockedAddon.this.f9969bz.setPosition(DockedAddon.this.ajf);
                    this.eQX.adZ().addChild(DockedAddon.this.f9969bz);
                    DockedAddon.this.f9969bz.setAllowSelection(true);
                    float diq = DockedAddon.this.f9969bz.computeLocalSpaceAABB().diq() * 2.0f;
                    if (this.eQW.agF().size() > 0) {
                        DockedAddon.this.mo24124a(DockedAddon.this.f9969bz, this.eQW);
                    }
                    DockedAddon.this.ajg = new C1255SZ();
                    DockedAddon.this.ajg.mo5422kc(true);
                    DockedAddon.this.ajg.mo5419a(this.eQW, DockedAddon.this.f9969bz);
                    CorporationLogo bFK = DockedAddon.this.f9968P.dxy().bFK();
                    if (bFK != null) {
                        C2403eu.m30125a(bFK.getHandle(), this.eQW.agH().mo4151Nu().getHandle(), DockedAddon.this.f9969bz);
                    }
                    if (DockedAddon.this.aje != null) {
                        DockedAddon.this.aje.aiR();
                        DockedAddon.this.aje = null;
                    }
                    DockedAddon.this.aje = new C3668tW(DockedAddon.this.f9968P, DockedAddon.this.f9969bz);
                    C5916acs.getSingolton().alf().addTask("ship bounce", DockedAddon.this.aje, 60);
                    this.eQW.mo18321i(DockedAddon.this.f9969bz);
                    f = (float) DockedAddon.this.f9969bz.getAabbLSLenght();
                    f2 = diq;
                } else {
                    f = 100.0f;
                    f2 = 50.0f;
                }
                this.eQX.mo3049bV("data/dummy_data.pro");
                DockedAddon.this.ajd = (SceneObject) this.eQX.mo3047bT("dummy_group");
                DockedAddon.this.ajd.setPosition(DockedAddon.this.ajf);
                float bkz = this.eQW.agH().bkz();
                Ship bQx = DockedAddon.this.f9968P.bQx();
                if (bQx != null) {
                    if (bQx instanceof Fighter) {
                        f3 = 0.0f;
                        f4 = 1.4f;
                    } else if (bQx instanceof Bomber) {
                        f3 = 20.0f;
                        f4 = 0.8f;
                    } else if (bQx instanceof Explorer) {
                        f3 = 0.0f;
                        f4 = 1.2f;
                    } else if (bQx instanceof Freighter) {
                        f3 = 0.0f;
                        f4 = 2.0f;
                    }
                    DockedAddon.this.ajm = new Vec3d(ScriptRuntime.NaN, (double) (f3 + bkz), (double) (f2 * f4));
                    DockedAddon.this.f9970kj.aVU().mo13975h(new C6060afg(DockedAddon.this.ajd, DockedAddon.this.ajm, -1, true, f));
                    return;
                }
                f3 = 0.0f;
                f4 = 1.0f;
                DockedAddon.this.ajm = new Vec3d(ScriptRuntime.NaN, (double) (f3 + bkz), (double) (f2 * f4));
                DockedAddon.this.f9970kj.aVU().mo13975h(new C6060afg(DockedAddon.this.ajd, DockedAddon.this.ajm, -1, true, f));
                return;
            }
            DockedAddon.this.f9970kj.getLog().error("updateActiveShip : invalid object");
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$a */
    class C4553a implements C0907NJ {
        private final /* synthetic */ Turret eQU;
        private final /* synthetic */ SceneObject eQV;

        C4553a(Turret jq, SceneObject sceneObject) {
            this.eQU = jq;
            this.eQV = sceneObject;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null && (renderAsset instanceof SceneObject)) {
                SceneObject sceneObject = (SceneObject) renderAsset;
                sceneObject.setPosition(this.eQU.aZU().mo16402gL());
                sceneObject.setOrientation(this.eQU.aZU().getOrientation());
                this.eQV.addChild(sceneObject);
            }
        }
    }

    /* renamed from: taikodom.addon.insidestation.DockedAddon$d */
    /* compiled from: a */
    private class C4556d extends WrapRunnable {
        private static final int fLp = 5000;
        private static final int fLq = 4500;
        private boolean fLr;

        private C4556d() {
            this.fLr = false;
        }

        /* synthetic */ C4556d(DockedAddon dockedAddon, C4556d dVar) {
            this();
        }

        public void run() {
            if (DockedAddon.this.f9969bz != null && !this.fLr) {
                TransformWrap transform = DockedAddon.this.f9969bz.getTransform();
                Vec3f vectorZ = transform.getVectorZ();
                DockedAddon.this.f9969bz.setPosition(transform.position.mo9531q(vectorZ.mo23510mS(-30.0f)));
                if (DockedAddon.this.ajd != null) {
                    DockedAddon.this.ajd.setPosition(DockedAddon.this.ajd.getPosition().mo9531q(vectorZ.mo23510mS(-30.0f)));
                }
                Color primitiveColor = DockedAddon.this.aja.getPrimitiveColor();
                if (DockedAddon.this.ajj > 4500.0f) {
                    primitiveColor.w = (DockedAddon.this.ajj - 4500.0f) / 500.0f;
                }
                DockedAddon.this.aja.setPrimitiveColor(primitiveColor);
                DockedAddon dockedAddon = DockedAddon.this;
                dockedAddon.ajj = dockedAddon.ajj + 30.0f;
                if (DockedAddon.this.ajg != null) {
                    DockedAddon.this.ajg.mo5423nT(0.6f);
                }
                if (DockedAddon.this.ajj > 5000.0f) {
                    DockedAddon.this.m44109Fk();
                    this.fLr = true;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void caM() {
            this.fLr = false;
            DockedAddon.this.ajj = 0.0f;
        }
    }
}
