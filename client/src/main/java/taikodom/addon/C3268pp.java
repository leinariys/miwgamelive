package taikodom.addon;

import game.network.exception.CreatListenerChannelException;
import gnu.trove.THashSet;
import logic.*;
import logic.res.code.SoftTimer;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Чтение настроек аддонов
 */
/* renamed from: a.pp */
/* compiled from: a */
public class C3268pp implements IAddonSettings {
    private static final String eLU = "./config/addons";
    public IAddonExecutor<IAddonSettings> bCI;
    public IAddonManager addonManager;
    public SoftTimer dej;
    public List<C3270b> dkr = new ArrayList();
    public List<Runnable> eLV = new ArrayList();
    public Properties eLW = new Properties();
    public Map<String, List<C6307akT>> eLX = new ConcurrentHashMap();
    public C6961axa eLY;
    public THashSet<WeakReference<C3271c>> eLZ = new THashSet<>();
    public C0613Ih eMa;
    public IWrapFileXmlOrJar eMb;
    public PrintStream out = System.out;

    public C3268pp() {
    }

    public C3268pp(IAddonManager addonManager, IWrapFileXmlOrJar wrapFileXmlOrJar, IAddonExecutor<IAddonSettings> addonExecutor) {
        this.addonManager = addonManager;
        this.eMb = wrapFileXmlOrJar;
        this.bCI = addonExecutor;
        this.eLY = new C3684tj(addonManager, this);
        this.dej = this.addonManager.getSoftTimer();
        this.eMa = new C0613Ih(addonManager.getEventManager());
    }

    /* renamed from: U */
    public <T> T mo11830U(Class<T> cls) {
        return this.addonManager.getInstanceWrapFileXmlOrJar(cls);
    }

    public void dispose() {
        this.eLY.dispose();
        this.eMa.dispose();
        Iterator it = this.eLZ.iterator();
        while (it.hasNext()) {
            C3271c cVar = (C3271c) ((WeakReference) it.next()).get();
            if (cVar != null) {
                cVar.mo21224a((C6144ahM<Object>) null);
            }
        }
        this.eLZ.clear();
        for (C3270b cancel : this.dkr) {
            cancel.cancel();
        }
        for (Runnable run : this.eLV) {
            run.run();
        }
    }

    /* renamed from: hE */
    public InputStream mo11847hE(String str) throws FileNotFoundException {
        return this.addonManager.getRootPathRender().concat(str.replaceAll("^res://", "")).openInputStream();
    }

    public IAddonManager bHu() {
        return this.addonManager;
    }

    public C6961axa bHv() {
        return this.eLY;
    }

    /* renamed from: c */
    public void mo11844c(String str, String... strArr) {
        List<C6307akT> list = this.eLX.get(str);
        if (list != null) {
            for (C6307akT j : list) {
                j.mo14337j(strArr);
            }
        }
    }

    /* renamed from: U */
    public String mo11831U(String str, String str2) {
        FilePath aC = this.addonManager.getRootPath().concat("./config/addons/" + str2 + ".ini");
        if (!aC.exists()) {
            return null;
        }
        try {
            InputStream openInputStream = aC.openInputStream();
            this.eLW.load(openInputStream);
            String str3 = (String) this.eLW.get(str);
            openInputStream.close();
            return str3;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    /* renamed from: a */
    public void mo11836a(String str, C6307akT akt) {
        List list = this.eLX.get(str);
        if (list == null) {
            list = new CopyOnWriteArrayList();
            this.eLX.put(str, list);
        }
        list.add(akt);
    }

    public PrintStream bHw() {
        return this.out;
    }

    /* renamed from: a */
    public aPE mo11833a(long j, aPE ape) {
        C3270b bVar = new C3270b(ape);
        this.dkr.add(bVar);
        this.dej.addTask("Addon timer - " + j, bVar, j);
        return bVar.dsq;
    }

    /* renamed from: P */
    public void mo2317P(Object obj) {
    }

    /* renamed from: Q */
    public void mo2318Q(Object obj) {
    }

    public Object bHx() {
        throw new CreatListenerChannelException();
    }

    /* renamed from: e */
    public void mo11845e(String str, String str2, String str3) {
        FilePath aC = this.addonManager.getRootPath().concat(eLU);
        if (!aC.exists()) {
            aC.mo2251BD();
        }
        try {
            OutputStream openOutputStream = aC.concat(String.valueOf(str3) + ".ini").openOutputStream();
            this.eLW.put(str, str2);
            this.eLW.store(openOutputStream, "");
            openOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    /* renamed from: b */
    public aPE mo21220b(long j, aPE ape) {
        C3270b bVar = new C3270b(ape);
        this.dej.mo7960a("Addon timer - " + j, bVar, j);
        return bVar.dsq;
    }

    /* renamed from: b */
    public C6144ahM<Object> mo21221b(C6144ahM<?> ahm) {
        C3271c cVar = new C3271c(this, ahm, (C3271c) null);
        this.eLZ.add(new C3269a(cVar));
        return cVar;
    }

    public <T extends IAddonSettings> IAddonExecutor<T> bHy() {
        return (IAddonExecutor<T>) this.bCI;
    }

    /**
     * Открытие файла на чтение, Пример nls.taikodom.addon.debugtools.taikodomversion-en.properties
     *
     * @param obj Класс аддона
     * @param str имя файла Пример nls.taikodom.addon.debugtools.taikodomversion-en.properties
     * @return
     * @throws FileNotFoundException
     */
    /* renamed from: f */
    public InputStream mo11846f(Object obj, String str) throws FileNotFoundException {
        Class<?> cls;
        if (str.startsWith("res://")) {
            return mo11847hE(str);
        }
        if (obj == null) {
            cls = bHy().getClass();
        } else if (obj instanceof Class) {
            cls = (Class) obj;
        } else {
            cls = obj.getClass();
        }
        return cls.getResourceAsStream(str);
    }

    public final String translate(String str) {
        return mo2320a((Class<?>) null, (String) null, str, (Object[]) null);
    }

    public final String translate(String str, Object... objArr) {
        return mo2320a((Class<?>) null, (String) null, str, objArr);
    }

    /* renamed from: V */
    public final String mo11832V(String str, String str2) {
        return mo2320a((Class<?>) null, str, str2, (Object[]) null);
    }

    /* renamed from: a */
    public final String mo11835a(String str, String str2, Object... objArr) {
        return mo2320a((Class<?>) null, str, str2, objArr);
    }

    /* renamed from: a */
    public final String mo11834a(Class<?> cls, String str, Object... objArr) {
        return mo2320a(cls, (String) null, str, objArr);
    }

    /* renamed from: a */
    public String mo2320a(Class<?> cls, String str, String str2, Object... objArr) {
        return String.format(str2, objArr);
    }

    public C6245ajJ aVU() {
        return this.eMa;
    }

    public IWrapFileXmlOrJar bHz() {
        return this.eMb;
    }

    public void restart() {
        this.eMb.mo11803a(this.bCI);
    }

    /* renamed from: a.pp$c */
    /* compiled from: a */
    private final class C3271c implements C6144ahM<Object> {

        /* renamed from: Zl */
        private C6144ahM<Object> f8851Zl;

        /* synthetic */ C3271c(C3268pp ppVar, C6144ahM ahm, C3271c cVar) {
            this(ahm);
        }

        private C3271c(C6144ahM<Object> ahm) {
            this.f8851Zl = ahm;
        }

        /* renamed from: n */
        public void mo1931n(Object obj) {
            if (this.f8851Zl != null) {
                this.f8851Zl.mo1931n(obj);
            }
        }

        /* renamed from: a */
        public void mo21224a(C6144ahM<Object> ahm) {
            this.f8851Zl = ahm;
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            if (this.f8851Zl != null) {
                this.f8851Zl.mo1930a(th);
            }
        }
    }

    /* renamed from: a.pp$b */
    /* compiled from: a */
    class C3270b extends WrapRunnable {
        /* access modifiers changed from: private */
        public final aPE dsq;
        private long last = System.currentTimeMillis();

        C3270b(aPE ape) {
            this.dsq = ape;
        }

        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            if (!this.dsq.mo6618fg(currentTimeMillis - this.last)) {
                cancel();
            }
            this.last = currentTimeMillis;
        }
    }

    /* renamed from: a.pp$a */
    class C3269a extends WeakReference<C3271c> {
        C3269a(C3271c cVar) {
            super(cVar);
        }

        public boolean enqueue() {
            C3268pp.this.eLZ.remove(this);
            return super.enqueue();
        }
    }
}
