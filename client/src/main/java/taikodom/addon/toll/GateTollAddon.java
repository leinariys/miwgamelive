package taikodom.addon.toll;

import game.network.message.externalizable.C0939Nn;
import game.script.Actor;
import game.script.ship.Ship;
import game.script.space.Gate;
import logic.IAddonSettings;
import logic.aPE;
import logic.aaa.C2393em;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

@TaikodomAddon("taikodom.addon.toll")
/* compiled from: a */
public class GateTollAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: BN */
    public Gate f10284BN;
    /* renamed from: kj */
    public IAddonProperties f10285kj;
    /* renamed from: nM */
    public InternalFrame f10286nM;
    /* access modifiers changed from: private */
    private C6124ags<C2393em> eQf;
    /* access modifiers changed from: private */
    private boolean eQg = false;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10285kj = addonPropertie;
        this.eQf = new C5041a();
        this.f10285kj.aVU().mo13965a(C2393em.class, this.eQf);
    }

    public void stop() {
        close();
    }

    /* access modifiers changed from: private */
    public void close() {
        if (this.f10286nM != null) {
            this.f10286nM.destroy();
            this.f10286nM = null;
        }
        if (this.eQg && !this.f10285kj.alb().anX() && !this.f10285kj.getPlayer().bQB()) {
            this.eQg = false;
            this.f10285kj.alb().cOW();
        }
    }

    /* renamed from: o */
    public void mo24637o(Gate fFVar) {
        this.f10284BN = fFVar;
        if (this.f10286nM == null) {
            this.f10286nM = ((MessageDialogAddon) this.f10285kj.mo11830U(MessageDialogAddon.class)).mo24307a(C5956adg.format(this.f10285kj.translate("The toll is {0}. Do you want to pay?"), Long.valueOf(fFVar.mo18393rw())), (aEP) new C5042b(), this.f10285kj.translate("no"), this.f10285kj.translate("yes"));
        }
        if (this.f10285kj.getPlayer().bQB() || !this.f10285kj.alb().anX()) {
            this.eQg = false;
        } else {
            this.f10285kj.alb().cOW();
            this.eQg = true;
        }
        this.f10285kj.mo11833a(1000, (aPE) new C5043c());
    }

    /* renamed from: taikodom.addon.toll.GateTollAddon$a */
    class C5041a extends C6124ags<C2393em> {
        C5041a() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2393em emVar) {
            GateTollAddon.this.mo24637o(emVar.mo18189lb());
            return false;
        }
    }

    /* renamed from: taikodom.addon.toll.GateTollAddon$b */
    /* compiled from: a */
    class C5042b extends aEP {
        C5042b() {
        }

        /* renamed from: a */
        public void mo8596a(int i, String str, boolean z) {
            if (i == 1) {
                GateTollAddon.this.f10286nM.setVisible(false);
                try {
                    GateTollAddon.this.f10285kj.getPlayer().dxc().cON();
                    GateTollAddon.this.f10285kj.getPlayer().dxc().mo22138e((C0286Dh) GateTollAddon.this.f10284BN);
                } catch (C5475aKt e) {
                    e.printStackTrace();
                    GateTollAddon.this.f10285kj.getPlayer().dxc().cOS();
                }
            }
            GateTollAddon.this.close();
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return C0939Nn.C0940a.CONFIRM;
        }
    }

    /* renamed from: taikodom.addon.toll.GateTollAddon$c */
    /* compiled from: a */
    class C5043c implements aPE {
        C5043c() {
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            if (GateTollAddon.this.f10286nM == null || !GateTollAddon.this.f10286nM.isVisible()) {
                return false;
            }
            Ship bQx = GateTollAddon.this.f10285kj.getPlayer().bQx();
            if (bQx != null && bQx.mo1001bB((Actor) GateTollAddon.this.f10284BN) <= PlayerController.m39266c((C0286Dh) GateTollAddon.this.f10284BN)) {
                return true;
            }
            GateTollAddon.this.close();
            return false;
        }
    }
}
