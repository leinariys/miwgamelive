package taikodom.addon.chat;

import game.network.message.externalizable.*;
import game.script.Character;
import game.script.player.Player;
import game.script.ship.Station;
import game.script.space.Node;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C1506WA;
import logic.aaa.C5783aaP;
import logic.baa.C6200aiQ;
import logic.data.link.C1092Px;
import logic.res.code.C5663aRz;
import logic.thred.LogPrinter;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.EditorPane;
import logic.ui.item.TaskPane;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.console.ConsoleAddon;
import taikodom.addon.selection.HudObjectListAddon;
import taikodom.infra.script.I18NString;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@TaikodomAddon("taikodom.addon.chat")
/* compiled from: a */
public class ChatAddon implements C2495fo {
    private static final int PORTRAIT = 2457;
    private static final String gAS = "oldClass";
    private static final long gAT = 30000;
    private static final int gAU = 5;
    private static final long gAV = 3000;
    private static final int gAW = 10;
    private static final int gAX = 11;
    private static final int gAY = 14;
    private static final int gAZ = 1638;
    private static int gBr = 5;
    private static int gBt = 200;
    /* access modifiers changed from: private */

    /* renamed from: DV */
    public Panel f9779DV;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9780P;
    /* access modifiers changed from: private */
    public aSZ aCF;
    /* access modifiers changed from: private */
    public boolean aZR = false;
    /* access modifiers changed from: private */
    public TaskPane bgR;
    /* access modifiers changed from: private */
    public boolean ccv;
    /* access modifiers changed from: private */
    public boolean fxW;
    /* access modifiers changed from: private */
    public EditorPane gBc;
    public C5866abu gBe;
    /* access modifiers changed from: private */
    public HashMap<String, JToggleButton> gBf = new HashMap<>();
    /* access modifiers changed from: private */
    public JTextField gBg;
    /* access modifiers changed from: private */
    public Panel gBi;
    /* access modifiers changed from: private */
    public Panel gBj;
    /* access modifiers changed from: private */
    public JToggleButton gBk;
    /* access modifiers changed from: private */
    public JToggleButton gBl;
    /* access modifiers changed from: private */
    public JToggleButton gBm;
    /* access modifiers changed from: private */
    public JToggleButton gBn;
    /* access modifiers changed from: private */
    public JToggleButton gBo;
    /* access modifiers changed from: private */
    public JToggleButton gBp;
    /* access modifiers changed from: private */
    public ArrayList<String> gBs = new ArrayList<>();
    /* access modifiers changed from: private */
    public aFU gBv = null;
    /* access modifiers changed from: private */
    public C2848l gBw;
    /* access modifiers changed from: private */
    public DefaultComboBoxModel gBx = new DefaultComboBoxModel();
    /* access modifiers changed from: private */
    public String gBy = null;
    /* renamed from: kj */
    public IAddonProperties f9781kj;
    private C3428rT<C5783aaP> bvz;
    private C4193j gBa;
    private C4216x gBb;
    private HTMLDocument gBd;
    private JComboBox gBh;
    private ArrayList<String> gBq = new ArrayList<>();
    private Map<aFU, I18NString> gBu;
    /* access modifiers changed from: private */
    private C4194k gBz;
    private HashMap<Object, Object> listeners = new HashMap<>();

    /* access modifiers changed from: private */
    /* renamed from: jZ */
    public boolean m42912jZ(String str) {
        if (str.startsWith("/l ") || str.startsWith("/local ")) {
            this.gBx.setSelectedItem(m42871a(aFU.LOCAL));
            this.gBg.setText("");
            this.gBv = aFU.LOCAL;
            return true;
        }
        if (str.startsWith("/s ") || str.startsWith("/squadron ") || str.startsWith("/esquadrilha ")) {
            if (this.gBx.getIndexOf(m42871a(aFU.SQUAD)) >= 0) {
                this.gBx.setSelectedItem(m42871a(aFU.SQUAD));
                this.gBg.setText("");
                this.gBv = aFU.SQUAD;
                return true;
            }
        } else if (str.startsWith("/c ") || str.startsWith("/corporation ") || str.startsWith("/corporacao ")) {
            if (this.gBx.getIndexOf(m42871a(aFU.CORPORATION)) >= 0) {
                this.gBx.setSelectedItem(m42871a(aFU.CORPORATION));
                this.gBg.setText("");
                this.gBv = aFU.CORPORATION;
                return true;
            }
        } else if (str.startsWith("/t ") || str.startsWith("/station ") || str.startsWith("/estacao ")) {
            this.gBx.setSelectedItem(m42871a(aFU.STATION));
            this.gBg.setText("");
            this.gBv = aFU.STATION;
            return true;
        } else if (str.startsWith("/p ")) {
            Matcher matcher = Pattern.compile("/p ([\\w|\\s]+),").matcher(str);
            if (matcher.find() && matcher.groupCount() > 0) {
                this.gBw.setName(matcher.group(1));
                if (this.gBx.getIndexOf(this.gBw) < 0) {
                    this.gBx.addElement(this.gBw);
                }
                this.gBx.setSelectedItem(this.gBw);
                this.gBg.setText("");
                this.gBv = aFU.TO;
                return true;
            }
        }
        return false;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9781kj = addonPropertie;
        this.f9780P = this.f9781kj.getPlayer();
        if (this.f9780P != null) {
            C3649tJ.m39576d(this.f9781kj.ale());
            ConsoleAddon consoleAddon = (ConsoleAddon) this.f9781kj.bHu().getInstanceWrapFileXmlOrJar(ConsoleAddon.class);
            if (consoleAddon != null) {
                consoleAddon.mo23744v("chat", this);
            }
            this.gBu = this.f9781kj.ala().aJe().mo19004xB().mo20117JG();
            this.gBa = new C4193j();
            this.gBb = new C4216x(this, (C4216x) null);
            this.bgR = (TaskPane) this.f9781kj.bHv().mo16794bQ("chat.xml");
            this.f9779DV = (Panel) this.f9781kj.bHv().mo16794bQ("chatpanel.xml");
            this.f9779DV.addMouseListener(new C4191h());
            this.gBg = this.f9779DV.mo4920ci("textfield");
            this.gBg.addMouseListener(new C4192i());
            this.gBg.addActionListener(new C4189f());
            this.gBg.addFocusListener(new C4190g());
            this.gBg.addKeyListener(new C4187d());
            this.gBc = this.f9779DV.mo4915cd("chatArea");
            this.gBc.mo8862e(ChatAddon.class.getResource("chatDefs.css"));
            this.gBe = new C5866abu(this.gBc);
            this.gBd = this.gBc.getDocument();
            this.gBc.addComponentListener(new C4188e());
            this.gBc.addHyperlinkListener(new C4185b());
            this.gBc.addMouseListener(new C4184a());
            this.gBi = (Panel) this.f9779DV.mo4916ce("npanel");
            this.gBj = (Panel) this.f9779DV.mo4916ce("spanel");
            this.gBk = this.gBi.mo4915cd("tbLocal");
            this.gBk.addActionListener(new C4204l(this.gBk));
            this.gBf.put("local", this.gBk);
            this.gBl = this.gBi.mo4915cd("tbGlobal");
            this.gBl.addActionListener(new C4204l(this.gBl));
            this.gBf.put("global", this.gBl);
            this.gBl.setSelected(true);
            this.gBm = this.gBi.mo4915cd("tbParty");
            this.gBm.addActionListener(new C4204l(this.gBm));
            if (!this.f9780P.dxk()) {
                this.gBm.setVisible(false);
            }
            this.gBf.put("party", this.gBm);
            this.gBn = this.gBi.mo4915cd("tbCorporation");
            this.gBn.addActionListener(new C4204l(this.gBn));
            if (!this.f9780P.dxE()) {
                this.gBn.setVisible(false);
            }
            this.gBf.put("corp", this.gBn);
            this.gBo = this.gBi.mo4915cd("tbStation");
            this.gBo.addActionListener(new C4204l(this.gBo));
            this.gBf.put("station", this.gBo);
            this.gBp = this.gBi.mo4915cd("tbActions");
            this.gBp.addActionListener(new C4204l(this.gBp));
            this.gBf.put("actions", this.gBp);
            this.gBh = this.f9779DV.mo4914cc("prefix");
            this.gBh.setModel(this.gBx);
            this.gBh.setPreferredSize(new Dimension(100, this.gBh.getHeight()));
            this.gBh.addItemListener(new C4186c());
            this.gBx.addElement(m42871a(aFU.LOCAL));
            this.gBx.addElement(m42871a(aFU.STATION));
            if (this.f9780P.dxE()) {
                this.gBx.addElement(m42871a(aFU.CORPORATION));
            }
            this.gBw = new C2848l(m42871a(aFU.TO), "");
            this.gBh.addMouseListener(new C4205m());
            this.f9781kj.mo2317P(this);
            this.bgR.mo17446q(this.f9779DV);
            C4206n nVar = new C4206n(this.f9781kj.translate("chat"), "chat");
            nVar.mo15602jH(this.f9781kj.translate("This it the chat window, where you can communicate with other ressucets and keep track of messages from your ONI."));
            this.aCF = new aSZ(this.bgR, nVar, 999, 1);
            m42909iG();
            cwu();
            this.f9781kj.aVU().publish(this.aCF);
        }
    }

    /* renamed from: iG */
    private void m42909iG() {
        boolean z = true;
        C4209q qVar = new C4209q();
        this.listeners.put(C1368Tv.class, qVar);
        this.f9781kj.aVU().mo13965a(C1368Tv.class, qVar);
        C4210r rVar = new C4210r();
        this.listeners.put(C2797kG.class, rVar);
        this.f9781kj.aVU().mo13965a(C2797kG.class, rVar);
        C4207o oVar = new C4207o();
        this.listeners.put(C0352Eg.class, oVar);
        this.f9781kj.aVU().mo13965a(C0352Eg.class, oVar);
        C4208p pVar = new C4208p();
        this.listeners.put(C1092Px.dQD, pVar);
        this.f9780P.mo8320a(C1092Px.dQD, (C6200aiQ<?>) pVar);
        C4213u uVar = new C4213u();
        this.listeners.put(C1092Px.dQN, uVar);
        this.f9780P.mo8320a(C1092Px.dQN, (C6200aiQ<?>) uVar);
        this.bvz = new C4214v();
        this.listeners.put(C5783aaP.class, this.bvz);
        this.f9781kj.aVU().mo13965a(C5783aaP.class, this.bvz);
        this.f9781kj.aVU().mo13965a(C5950ada.class, new C4211s());
        if (!m42910iL()) {
            if (aMU.CREATED_PLAYER.equals(this.f9781kj.getPlayer().cXm())) {
                z = false;
            }
            this.ccv = z;
            if (!this.ccv) {
                C4212t tVar = new C4212t();
                this.listeners.put(C3131oI.class, tVar);
                this.f9781kj.aVU().mo13965a(C3131oI.class, tVar);
                return;
            }
            return;
        }
        this.ccv = true;
    }

    /* renamed from: iL */
    private boolean m42910iL() {
        return this.f9781kj != null && this.f9781kj.ala().aLS().mo22273iL();
    }

    public void stop() {
        this.bgR.destroy();
    }

    /* access modifiers changed from: private */
    /* renamed from: ka */
    public void m42914ka(String str) {
        if (m42912jZ(str)) {
            str = str.substring(str.indexOf(32) + 1);
            m42917kd(m42887au(m42871a(this.gBv)));
        }
        m42899cP(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: cP */
    public void m42899cP(String str) {
        if (!"actions".equals(cww())) {
            String cwt = cwt();
            if (this.gBs.size() >= gBt) {
                this.gBs.remove(0);
            }
            this.gBs.add(0, str);
            if ("pvt".equals(cwt)) {
                Matcher matcher = Pattern.compile(" (\\w|\\s)+").matcher(this.gBw.toString());
                matcher.find();
                mo23574ax(matcher.group(0).trim(), str);
            } else {
                mo23573aw(cwt, str);
            }
            this.f9781kj.aVU().mo13975h(new C6811auD(C3667tV.CHAT_SEND));
            IBaseUiTegXml.initBaseUItegXML().getRootPane().requestFocus();
        }
    }

    /* renamed from: aw */
    public void mo23573aw(String str, String str2) {
        String str3;
        String str4;
        String trim = str2.trim();
        if (!trim.isEmpty()) {
            boolean z = trim.charAt(0) == '/' && trim.length() > 3;
            if (!this.fxW || z) {
                if (z) {
                    String substring = trim.substring(1);
                    int indexOf = substring.indexOf(" ");
                    if (indexOf != -1) {
                        str4 = substring.substring(0, indexOf);
                        str3 = substring.substring(indexOf + 1);
                    } else {
                        str3 = null;
                        str4 = substring;
                    }
                    this.f9781kj.aVU().mo13972d(str4, new C5388aHk(this, 1, str3));
                } else if (str.equals("party")) {
                    if (this.f9780P.dxk()) {
                        this.f9780P.dxi().mo2429h(this.f9780P, str2);
                    } else {
                        this.f9780P.mo14419f((C1506WA) new C1368Tv((Player) null, this.f9780P, C1368Tv.C1369a.SYSTEM, this.f9781kj.translate("You are not in a party")));
                    }
                } else if (str.equals("corp")) {
                    if (this.f9780P.dxE()) {
                        this.f9780P.bYd().mo10716b(str2, this.f9780P);
                    } else {
                        this.f9780P.mo14419f((C1506WA) new C1368Tv((Player) null, this.f9780P, C1368Tv.C1369a.SYSTEM, this.f9781kj.translate("You are not in a corporation")));
                    }
                } else if (str.equals("station")) {
                    if (this.f9780P.bQB()) {
                        this.f9780P.mo14353b(C1368Tv.C1369a.STATION, str2);
                    }
                } else if (this.f9780P.bQB()) {
                    ((Station) this.f9780P.bhE()).azW().mo21647d(this.f9780P, str2);
                } else {
                    Node azW = this.f9780P.bQx().azW();
                    if (azW != null) {
                        azW.mo21647d(this.f9780P, str2);
                    } else {
                        LogPrinter.setClass(ChatAddon.class).error("The player is not docked but his ship is not in a Node.");
                    }
                }
                this.gBb.mo23625LR();
                return;
            }
            this.f9781kj.aVU().mo13975h(new C1368Tv((Player) null, this.f9781kj.getPlayer(), C1368Tv.C1369a.SYSTEM, this.f9781kj.translate("You message has been blocked by flood control")));
            this.f9781kj.getEngineGame().alf().mo7960a("unblock task", this.gBa, gAT);
        }
    }

    /* renamed from: ax */
    public void mo23574ax(String str, String str2) {
        this.f9780P.mo14349ax(str, str2);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m42877a(C0352Eg eg) {
        m42878a(new C1368Tv((Player) null, this.f9780P, C1368Tv.C1369a.SYSTEM, eg.getMessage()));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m42879a(C2797kG kGVar) {
        m42878a(new C1368Tv(kGVar.mo20033KJ(), this.f9780P, kGVar.mo20031KH(), C5956adg.format(kGVar.mo20032KI().get(), kGVar.mo20035KL())));
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m42878a(C1368Tv tv) {
        if (tv == null || tv.mo5759KH() == null) {
            System.out.println("Chat: received unknown event: " + tv);
            return;
        }
        Player KJ = tv.mo5760KJ();
        if (!this.f9780P.dxu().mo8514ct(KJ)) {
            if (tv.mo5759KH() == C1368Tv.C1369a.PRIVATE && !KJ.getName().equals(this.f9780P.getName())) {
                if (this.gBq.size() >= gBr) {
                    this.gBq.remove(this.gBq.size() - 1);
                }
                this.gBq.add(0, KJ.getName());
            }
            mo23575b(tv);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo23575b(C1368Tv tv) {
        String str = tv.mo5759KH().name;
        String c = m42895c(tv);
        this.gBe.mo12530ai(str, c);
        if (!cwx()) {
            c = c.replaceFirst("<img width='48' height='48' src='res://.*' border='0'/>", "");
        }
        if (m42916kc(str) || ((m42916kc("global") && !str.equals("actions")) || str.equals("sys") || str.equals("pvt"))) {
            try {
                synchronized (this.gBc) {
                    this.gBc.setIgnoreRepaint(true);
                    this.gBd.insertBeforeEnd(this.gBd.getElement("body"), c);
                    cwv();
                    this.gBc.setIgnoreRepaint(false);
                }
            } catch (BadLocationException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    private String cwt() {
        return m42887au(this.gBx.getSelectedItem());
    }

    /* renamed from: au */
    private String m42887au(Object obj) {
        if (obj instanceof C2848l) {
            return "pvt";
        }
        if (obj.equals(m42871a(aFU.LOCAL))) {
            return "local";
        }
        if (obj.equals(m42871a(aFU.STATION))) {
            return "station";
        }
        if (obj.equals(m42871a(aFU.CORPORATION))) {
            return "corp";
        }
        if (obj.equals(m42871a(aFU.SQUAD))) {
            return "party";
        }
        return "local";
    }

    /* renamed from: a */
    private String m42872a(String str, C1368Tv tv) {
        Player KJ = tv.mo5760KJ();
        if ("local".equals(str)) {
            return "[L] [" + KJ.getName() + "]:";
        }
        if ("station".equals(str)) {
            return "[T] [" + KJ.getName() + "]:";
        }
        if ("party".equals(str)) {
            return "[S] [" + KJ.getName() + "]:";
        }
        if ("corp".equals(str)) {
            return "[C] [" + KJ.getName() + "]:";
        }
        if (!"pvt".equals(str)) {
            return "";
        }
        if (KJ.equals(this.f9780P)) {
            return String.valueOf(this.f9781kj.translate("To")) + " [" + tv.mo5761KK().getName() + "]:";
        }
        return String.valueOf(this.f9781kj.translate("From")) + " [" + KJ.getName() + "]:";
    }

    /* renamed from: c */
    private String m42895c(C1368Tv tv) {
        Player KJ = tv.mo5760KJ();
        String str = tv.mo5759KH().name;
        Player KK = tv.mo5761KK();
        String replaceAll = tv.getMessage().replaceAll(">", "&gt;").replaceAll("<", "&lt;");
        if (KJ == null || tv.mo5759KH().equals(C1368Tv.C1369a.SYSTEM)) {
            return "<table width='100%'><tr><td class='" + str + "'>" + replaceAll + "</td></tr></table>";
        }
        C3649tJ.m39575cd(this.f9780P);
        String a = C3649tJ.m39572a((Character) KJ, (C3649tJ.C3650a) new C4215w());
        String a2 = m42872a(str, tv);
        String str2 = "<img width='48' height='48' src='res://" + a + "' border='0'/>";
        String str3 = "";
        String str4 = "";
        if (KK == null || !KK.equals(KJ)) {
            str3 = "<a href='" + KJ.getName() + "'>";
            str4 = "</a>";
        }
        return "<table width='100%'><tr class='" + str + "'><td width='0%' valign='top'>" + str3 + str2 + str4 + "</td><td width='100%'><b>" + str3 + a2 + str4 + "</b> " + replaceAll + "</td></tr></table>";
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m42873a(JToggleButton jToggleButton) {
        if (jToggleButton == this.gBl) {
            return "";
        }
        if (jToggleButton == this.gBk) {
            return m42871a(aFU.LOCAL);
        }
        if (jToggleButton == this.gBo) {
            return m42871a(aFU.STATION);
        }
        if (jToggleButton == this.gBm) {
            return m42871a(aFU.SQUAD);
        }
        if (jToggleButton == this.gBn) {
            return m42871a(aFU.CORPORATION);
        }
        return "";
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m42890b(JToggleButton jToggleButton) {
        if (jToggleButton == this.gBl) {
            this.gBx.setSelectedItem(m42871a(aFU.LOCAL));
        } else if (jToggleButton == this.gBk) {
            this.gBx.setSelectedItem(m42871a(aFU.LOCAL));
        } else if (jToggleButton == this.gBo) {
            this.gBx.setSelectedItem(m42871a(aFU.STATION));
        } else if (jToggleButton == this.gBm) {
            if (this.gBx.getIndexOf(m42871a(aFU.SQUAD)) < 0) {
                this.gBx.setSelectedItem(m42871a(aFU.LOCAL));
            } else {
                this.gBx.setSelectedItem(m42871a(aFU.SQUAD));
            }
        } else if (jToggleButton != this.gBn) {
        } else {
            if (this.gBx.getIndexOf(m42871a(aFU.CORPORATION)) < 0) {
                this.gBx.setSelectedItem(m42871a(aFU.LOCAL));
            } else {
                this.gBx.setSelectedItem(m42871a(aFU.CORPORATION));
            }
        }
    }

    public void cwu() {
        m42915kb(this.gBe.mo12538id(cww()));
    }

    /* access modifiers changed from: private */
    /* renamed from: kb */
    public void m42915kb(String str) {
        try {
            this.gBd.remove(0, this.gBd.getLength());
            synchronized (this.gBc) {
                this.gBc.setIgnoreRepaint(true);
                this.gBc.getEditorKit().read(new StringReader(str), this.gBd, 0);
                this.gBc.setIgnoreRepaint(false);
            }
            String text = this.gBd.getText(0, this.gBd.getLength());
            int lastIndexOf = text.lastIndexOf(" ");
            if (!text.substring(lastIndexOf + 1).equals("")) {
                this.gBc.setCaretPosition(lastIndexOf + 1);
            }
        } catch (BadLocationException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void cwv() {
        if (this.gBe.bNk() > 1300) {
            this.gBe.mo12540pF(1000);
        }
    }

    /* renamed from: kc */
    private boolean m42916kc(String str) {
        JToggleButton jToggleButton = this.gBf.get(str);
        if (jToggleButton == null || !jToggleButton.isSelected()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public String cww() {
        for (String next : this.gBf.keySet()) {
            if (this.gBf.get(next).isSelected()) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: kd */
    private void m42917kd(String str) {
        if (!this.f9780P.bQB() && this.bgR.isPinned() && str != null && !"".equals(str)) {
            for (JToggleButton jToggleButton : this.gBi.getComponents()) {
                if (jToggleButton instanceof JToggleButton) {
                    JToggleButton jToggleButton2 = jToggleButton;
                    if (jToggleButton2.isSelected()) {
                        jToggleButton2.setSelected(false);
                    }
                    IComponentManager e = ComponentManager.getCssHolder(jToggleButton2);
                    jToggleButton2.putClientProperty(gAS, "tabpane-button");
                    e.setAttribute("class", String.valueOf((String) jToggleButton2.getClientProperty(gAS)) + " simpleMode");
                }
            }
            JToggleButton jToggleButton3 = this.gBf.get(str);
            jToggleButton3.setSelected(true);
            ComponentManager.getCssHolder(jToggleButton3).setAttribute("class", "tabpane-button");
        }
    }

    @C2602hR(mo19255zf = "REQUEST_CHAT")
    /* renamed from: gC */
    public void mo23578gC(boolean z) {
        if (z && !this.f9780P.dxc().anX()) {
            this.gBg.requestFocus();
        }
    }

    @C2602hR(mo19255zf = "CYCLE_PVT")
    /* renamed from: gD */
    public void mo23579gD(boolean z) {
        if (z && this.gBq.size() > 0) {
            mo23581ke(this.gBq.remove(0));
        }
    }

    /* renamed from: ke */
    public void mo23581ke(String str) {
        this.gBw.setName(str);
        if (this.gBx.getIndexOf(this.gBw) < 0) {
            this.gBx.addElement(this.gBw);
        }
        this.gBx.setSelectedItem(this.gBw);
        this.gBg.setText("");
        this.gBg.requestFocus();
        this.gBq.add(str);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m42889b(MouseEvent mouseEvent) {
        if (this.gBz == null) {
            this.gBz = new C4194k();
        }
        if (mouseEvent.isPopupTrigger()) {
            this.gBz.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: aR */
    public void m42886aR(boolean z) {
        if (!z || this.f9781kj.getPlayer().bQB() || !this.f9781kj.alb().anX()) {
            if (!z && this.aZR && !this.f9781kj.getPlayer().bQB() && !this.f9781kj.alb().anX()) {
                this.f9781kj.alb().cOW();
            }
            this.aZR = false;
            return;
        }
        this.aZR = true;
        this.f9781kj.alb().cOW();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m42871a(aFU afu) {
        if (afu == null) {
            return "";
        }
        return this.gBu.get(afu).get();
    }

    /* renamed from: t */
    public void mo23583t(String str, int i) {
        if (str.equals("local")) {
            this.gBe.mo12531aj("local.font-color", Integer.toHexString(i));
        } else if (str.equals("party")) {
            this.gBe.mo12531aj("party.font-color", Integer.toHexString(i));
        } else if (str.equals("corp")) {
            this.gBe.mo12531aj("corp.font-color", Integer.toHexString(i));
        } else if (str.equals("pvt")) {
            this.gBe.mo12531aj("private.font-color", Integer.toHexString(i));
        } else if (str.equals("station")) {
            this.gBe.mo12531aj("station.font-color", Integer.toHexString(i));
        }
        this.gBe.mo12541t(str, i);
    }

    /* renamed from: pE */
    public void mo23582pE(int i) {
        this.gBe.mo12531aj("font-size", String.valueOf(i));
        this.gBe.mo12539pE(i);
    }

    /* renamed from: gE */
    public void mo23580gE(boolean z) {
        this.gBe.mo12535dV(z);
        cwu();
    }

    public boolean cwx() {
        return this.gBe.bNm();
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$l */
    /* compiled from: a */
    private class C4204l implements ActionListener {
        private JToggleButton giK;

        public C4204l(JToggleButton jToggleButton) {
            this.giK = jToggleButton;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            String text = ChatAddon.this.gBg.getText();
            if (ChatAddon.this.m42873a(this.giK).isEmpty()) {
                ChatAddon.this.gBx.setSelectedItem(ChatAddon.this.m42871a(aFU.LOCAL));
            } else {
                ChatAddon.this.m42890b(this.giK);
            }
            ChatAddon.this.gBg.setText(text);
            for (String str : ChatAddon.this.gBf.keySet()) {
                JToggleButton jToggleButton = (JToggleButton) ChatAddon.this.gBf.get(str);
                jToggleButton.setSelected(this.giK == jToggleButton);
                if (this.giK == jToggleButton) {
                    ChatAddon.this.m42915kb(ChatAddon.this.gBe.mo12538id(str));
                }
            }
            if (this.giK == ChatAddon.this.gBl) {
                ChatAddon.this.gBv = null;
            } else if (this.giK == ChatAddon.this.gBk) {
                ChatAddon.this.gBv = aFU.LOCAL;
            } else if (this.giK == ChatAddon.this.gBn) {
                ChatAddon.this.gBv = aFU.CORPORATION;
            } else if (this.giK == ChatAddon.this.gBp) {
                ChatAddon.this.gBv = aFU.LOG;
            } else if (this.giK == ChatAddon.this.gBm) {
                ChatAddon.this.gBv = aFU.SQUAD;
            } else if (this.giK == ChatAddon.this.gBo) {
                ChatAddon.this.gBv = aFU.STATION;
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$h */
    /* compiled from: a */
    class C4191h extends MouseAdapter {
        C4191h() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$i */
    /* compiled from: a */
    class C4192i extends MouseAdapter {
        C4192i() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$f */
    /* compiled from: a */
    class C4189f implements ActionListener {
        C4189f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ChatAddon.this.m42899cP(ChatAddon.this.gBg.getText());
            ChatAddon.this.gBg.setText("");
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$g */
    /* compiled from: a */
    class C4190g extends FocusAdapter {
        C4190g() {
        }

        public void focusLost(FocusEvent focusEvent) {
            ChatAddon.this.aZR = false;
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$d */
    /* compiled from: a */
    class C4187d extends KeyAdapter {
        C4187d() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == 27) {
                if (ChatAddon.this.gBg == KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner()) {
                    ChatAddon.this.gBg.transferFocusUpCycle();
                }
            } else if (keyEvent.getKeyCode() == 34) {
                ChatAddon.this.mo23579gD(true);
                keyEvent.consume();
            }
            boolean unused = ChatAddon.this.m42912jZ(ChatAddon.this.gBg.getText());
        }

        public void keyPressed(KeyEvent keyEvent) {
            int indexOf;
            char keyChar = keyEvent.getKeyChar();
            if (keyChar == 10) {
                ChatAddon.this.m42886aR(false);
            } else if (keyChar == 27) {
                if (ChatAddon.this.gBx.getIndexOf(ChatAddon.this.gBw) > 0) {
                    ChatAddon.this.gBw.setName("");
                    ChatAddon.this.gBx.setSelectedItem(ChatAddon.this.m42871a(aFU.LOCAL));
                }
                ChatAddon.this.m42886aR(false);
            } else if (keyChar == '&' && ChatAddon.this.gBs.size() > 0) {
                int indexOf2 = ChatAddon.this.gBs.indexOf(ChatAddon.this.gBg.getText());
                if (indexOf2 < ChatAddon.this.gBs.size() - 1) {
                    ChatAddon.this.gBg.setText((String) ChatAddon.this.gBs.get(indexOf2 + 1));
                }
            } else if (keyChar == '(' && ChatAddon.this.gBs.size() > 0 && (indexOf = ChatAddon.this.gBs.indexOf(ChatAddon.this.gBg.getText())) > 0) {
                ChatAddon.this.gBg.setText((String) ChatAddon.this.gBs.get(indexOf - 1));
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$e */
    /* compiled from: a */
    class C4188e extends ComponentAdapter {
        C4188e() {
        }

        public void componentResized(ComponentEvent componentEvent) {
            JScrollPane cd = ChatAddon.this.bgR.mo4915cd("scroll");
            if (cd != null) {
                JViewport viewport = cd.getViewport();
                viewport.setViewPosition(new Point(0, viewport.getView().getHeight() - viewport.getHeight()));
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$b */
    /* compiled from: a */
    class C4185b implements HyperlinkListener {
        C4185b() {
        }

        public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
            if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                ChatAddon.this.gBw.setName(hyperlinkEvent.getDescription());
                if (ChatAddon.this.gBx.getIndexOf(ChatAddon.this.gBw) < 0) {
                    ChatAddon.this.gBx.addElement(ChatAddon.this.gBw);
                }
                ChatAddon.this.gBx.setSelectedItem(ChatAddon.this.gBw);
                ChatAddon.this.gBg.setText("");
                ChatAddon.this.gBg.requestFocus();
            } else if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.ENTERED) {
                ChatAddon.this.gBy = hyperlinkEvent.getDescription();
                ComponentManager.getCssHolder(ChatAddon.this.gBc).setAttribute("class", "hyperlink");
            } else if (hyperlinkEvent.getEventType() == HyperlinkEvent.EventType.EXITED) {
                ChatAddon.this.gBy = null;
                ComponentManager.getCssHolder(ChatAddon.this.gBc).setAttribute("class", "");
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$a */
    class C4184a extends MouseAdapter {
        C4184a() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$c */
    /* compiled from: a */
    class C4186c implements ItemListener {
        C4186c() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            if (itemEvent.getStateChange() == 1 && !"global".equals(ChatAddon.this.cww())) {
                Object item = itemEvent.getItem();
                if (item.equals(ChatAddon.this.m42871a(aFU.LOCAL)) && ChatAddon.this.gBk.isVisible() && !ChatAddon.this.gBk.isSelected()) {
                    ChatAddon.this.gBk.doClick();
                }
                if (item.equals(ChatAddon.this.m42871a(aFU.STATION)) && ChatAddon.this.gBo.isVisible() && !ChatAddon.this.gBo.isSelected()) {
                    ChatAddon.this.gBo.doClick();
                }
                if (item.equals(ChatAddon.this.m42871a(aFU.SQUAD)) && ChatAddon.this.gBm.isVisible() && !ChatAddon.this.gBm.isSelected()) {
                    ChatAddon.this.gBm.doClick();
                }
                if (item.equals(ChatAddon.this.m42871a(aFU.CORPORATION)) && ChatAddon.this.gBn.isVisible() && !ChatAddon.this.gBn.isSelected()) {
                    ChatAddon.this.gBn.doClick();
                }
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$m */
    /* compiled from: a */
    class C4205m extends MouseAdapter {
        C4205m() {
        }

        public void mousePressed(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            ChatAddon.this.m42889b(mouseEvent);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$n */
    /* compiled from: a */
    class C4206n extends aGC {
        private static final long serialVersionUID = 6373274262077004912L;

        C4206n(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
            ChatAddon.this.f9779DV.setVisible(z && ChatAddon.this.ccv);
        }

        public boolean isEnabled() {
            return ChatAddon.this.ccv;
        }

        /* renamed from: m */
        public void mo8924m(boolean z) {
            if (z) {
                for (JToggleButton jToggleButton : ChatAddon.this.gBi.getComponents()) {
                    if (jToggleButton instanceof JToggleButton) {
                        JToggleButton jToggleButton2 = jToggleButton;
                        IComponentManager e = ComponentManager.getCssHolder(jToggleButton2);
                        jToggleButton2.putClientProperty(ChatAddon.gAS, e.getAttribute("class"));
                        if (!jToggleButton2.isSelected()) {
                            e.setAttribute("class", String.valueOf((String) jToggleButton2.getClientProperty(ChatAddon.gAS)) + " simpleMode");
                        }
                    }
                }
                ComponentManager.getCssHolder(ChatAddon.this.gBi).setAttribute("class", "simpleMode");
                ChatAddon.this.bgR.mo4915cd("scroll").setVerticalScrollBarPolicy(21);
            } else {
                for (JToggleButton jToggleButton3 : ChatAddon.this.gBi.getComponents()) {
                    if (jToggleButton3 instanceof JToggleButton) {
                        JToggleButton jToggleButton4 = jToggleButton3;
                        ComponentManager.getCssHolder(jToggleButton4).setAttribute("class", (String) jToggleButton4.getClientProperty(ChatAddon.gAS));
                    }
                }
                ChatAddon.this.bgR.mo4915cd("scroll").setVerticalScrollBarPolicy(20);
                ChatAddon.this.cwu();
            }
            ComponentManager.getCssHolder(ChatAddon.this.gBi).setAttribute("class", "");
            ChatAddon.this.gBj.setVisible(!z);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$q */
    /* compiled from: a */
    class C4209q extends C3428rT<C1368Tv> {
        C4209q() {
        }

        /* renamed from: d */
        public void mo321b(C1368Tv tv) {
            ChatAddon.this.m42878a(tv);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$r */
    /* compiled from: a */
    class C4210r extends C3428rT<C2797kG> {
        C4210r() {
        }

        /* renamed from: b */
        public void mo321b(C2797kG kGVar) {
            ChatAddon.this.m42879a(kGVar);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$o */
    /* compiled from: a */
    class C4207o extends C3428rT<C0352Eg> {
        C4207o() {
        }

        /* renamed from: b */
        public void mo321b(C0352Eg eg) {
            ChatAddon.this.m42877a(eg);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$p */
    /* compiled from: a */
    class C4208p implements C6200aiQ<Player> {
        C4208p() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            int indexOf = ChatAddon.this.gBx.getIndexOf(ChatAddon.this.gBw);
            if (obj != null) {
                ChatAddon.this.gBm.setVisible(true);
                if (indexOf < 0) {
                    ChatAddon.this.gBx.addElement(ChatAddon.this.m42871a(aFU.SQUAD));
                } else {
                    ChatAddon.this.gBx.insertElementAt(ChatAddon.this.m42871a(aFU.SQUAD), indexOf - 1);
                }
            } else {
                ChatAddon.this.gBm.setVisible(false);
                if (indexOf >= 0) {
                    ChatAddon.this.gBx.removeElementAt(ChatAddon.this.gBx.getIndexOf(ChatAddon.this.m42871a(aFU.SQUAD)));
                }
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$u */
    /* compiled from: a */
    class C4213u implements C6200aiQ<Player> {
        C4213u() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            int indexOf = ChatAddon.this.gBx.getIndexOf(ChatAddon.this.gBw);
            if (obj != null) {
                if (indexOf < 0) {
                    ChatAddon.this.gBx.addElement(ChatAddon.this.m42871a(aFU.CORPORATION));
                } else {
                    ChatAddon.this.gBx.insertElementAt(ChatAddon.this.m42871a(aFU.CORPORATION), indexOf - 1);
                }
            } else if (indexOf >= 0) {
                ChatAddon.this.gBx.removeElementAt(ChatAddon.this.gBx.getIndexOf(ChatAddon.this.m42871a(aFU.CORPORATION)));
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$v */
    /* compiled from: a */
    class C4214v extends C3428rT<C5783aaP> {
        C4214v() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            boolean z = true;
            C5783aaP.C1841a bMO = aap.bMO();
            boolean equals = C5783aaP.C1841a.DOCKED.equals(bMO);
            if (ChatAddon.this.gBo.isSelected()) {
                ChatAddon.this.m42890b(ChatAddon.this.gBl);
            }
            ChatAddon.this.gBo.setEnabled(equals);
            ChatAddon.this.gBo.setVisible(equals);
            if (equals) {
                ChatAddon.this.gBx.insertElementAt(ChatAddon.this.m42871a(aFU.STATION), 1);
            } else {
                ChatAddon.this.gBx.removeElement(ChatAddon.this.m42871a(aFU.STATION));
            }
            TaskPane o = ChatAddon.this.bgR;
            if (!equals && !C5783aaP.C1841a.FLYING.equals(bMO)) {
                z = false;
            }
            o.setVisible(z);
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$s */
    /* compiled from: a */
    class C4211s extends C6124ags<C5950ada> {
        C4211s() {
        }

        /* renamed from: a */
        public boolean updateInfo(C5950ada ada) {
            ChatAddon.this.m42914ka(ada.getMessage());
            return false;
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$t */
    /* compiled from: a */
    class C4212t extends C6124ags<C3131oI> {
        C4212t() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3131oI oIVar) {
            if (!C3131oI.C3132a.CREATED_AVATAR.equals(oIVar.mo20956Us())) {
                return false;
            }
            ChatAddon.this.ccv = true;
            ChatAddon.this.f9781kj.aVU().mo13975h(new C0269DS(ChatAddon.this.aCF, true));
            ChatAddon.this.f9781kj.aVU().mo13964a(this);
            return false;
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$w */
    /* compiled from: a */
    class C4215w implements C3649tJ.C3650a {
        C4215w() {
        }

        /* renamed from: d */
        public void mo2039d(String str, boolean z) {
            if (!z) {
                ChatAddon.this.cwu();
            }
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$j */
    /* compiled from: a */
    class C4193j extends WrapRunnable {
        C4193j() {
        }

        public void run() {
            ChatAddon.this.f9781kj.aVU().mo13975h(new C1368Tv((Player) null, ChatAddon.this.f9781kj.getPlayer(), C1368Tv.C1369a.SYSTEM, ChatAddon.this.f9781kj.translate("Flood block has been removed")));
            ChatAddon.this.fxW = false;
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$x */
    /* compiled from: a */
    private class C4216x {
        private long[] iNr;

        /* synthetic */ C4216x(ChatAddon chatAddon, C4216x xVar) {
            this();
        }

        private C4216x() {
            this.iNr = new long[5];
            reset();
        }

        private void reset() {
            for (int i = 0; i < this.iNr.length; i++) {
                this.iNr[i] = -1;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: LR */
        public void mo23625LR() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = this.iNr[0];
            int i = 0;
            while (i < this.iNr.length) {
                long j2 = this.iNr[i];
                if (j2 == -1) {
                    if (i == 0) {
                        this.iNr[i] = currentTimeMillis;
                        return;
                    } else if (currentTimeMillis - this.iNr[i - 1] > ChatAddon.gAV || currentTimeMillis - j > ChatAddon.gAV) {
                        reset();
                        this.iNr[0] = currentTimeMillis;
                        return;
                    } else {
                        this.iNr[i] = currentTimeMillis;
                        return;
                    }
                } else if (i != this.iNr.length - 1 || (currentTimeMillis - j2 <= ChatAddon.gAV && currentTimeMillis - j <= ChatAddon.gAV)) {
                    i++;
                } else {
                    reset();
                    this.iNr[0] = currentTimeMillis;
                    return;
                }
            }
            ChatAddon.this.fxW = true;
            reset();
        }
    }

    /* renamed from: taikodom.addon.chat.ChatAddon$k */
    /* compiled from: a */
    class C4194k extends JPopupMenu {
        /* access modifiers changed from: private */
        public String eSn;
        JMenu ioB;
        JMenuItem ioC = new JMenuItem(new C4203i(0));
        JMenuItem ioD = new JMenuItem(new C4203i(ChatAddon.PORTRAIT));
        JMenuItem ioE = new JMenuItem(new C4203i(ChatAddon.gAZ));
        JMenu ioF;
        JMenuItem ioG;
        JMenuItem ioH;
        JMenuItem ioI;
        JMenuItem ioJ;
        JMenuItem ioK;

        public C4194k() {
            this.ioB = new JMenu(ChatAddon.this.f9781kj.translate("change font size"));
            JMenuItem jMenuItem = new JMenuItem(new C4203i(10));
            JMenuItem jMenuItem2 = new JMenuItem(new C4203i(11));
            JMenuItem jMenuItem3 = new JMenuItem(new C4203i(14));
            this.ioB.add(jMenuItem);
            this.ioB.add(jMenuItem2);
            this.ioB.add(jMenuItem3);
            this.ioF = new JMenu(ChatAddon.this.f9781kj.translate("enable/disable tabs"));
            JMenuItem jMenuItem4 = new JMenuItem(ChatAddon.this.f9781kj.translate("Local"));
            jMenuItem4.addActionListener(new C4201g());
            JMenuItem jMenuItem5 = new JMenuItem(ChatAddon.this.f9781kj.translate("Station"));
            jMenuItem5.addActionListener(new C4202h());
            this.ioG = new JMenuItem(ChatAddon.this.f9781kj.translate("Party"));
            this.ioG.addActionListener(new C4199e());
            this.ioH = new JMenuItem(ChatAddon.this.f9781kj.translate("Corporation"));
            this.ioH.addActionListener(new C4200f());
            JMenuItem jMenuItem6 = new JMenuItem(ChatAddon.this.f9781kj.translate("Action log"));
            jMenuItem6.addActionListener(new C4197c());
            this.ioF.add(jMenuItem4);
            this.ioF.add(jMenuItem5);
            this.ioF.add(this.ioG);
            this.ioF.add(this.ioH);
            this.ioF.add(jMenuItem6);
            this.ioI = new JMenuItem(ChatAddon.this.f9781kj.translate("select player"));
            this.ioI.addActionListener(new C4198d());
            this.ioJ = new JMenuItem(ChatAddon.this.f9781kj.translate("squadron invite"));
            this.ioJ.addActionListener(new C4195a());
            this.ioK = new JMenuItem(ChatAddon.this.f9781kj.translate("send private message"));
            this.ioK.addActionListener(new C4196b());
            add(this.ioB);
            add(this.ioC);
            add(this.ioD);
            add(this.ioE);
            add(this.ioF);
            add(this.ioI);
            add(this.ioJ);
            add(this.ioK);
        }

        private void diZ() {
            if (ChatAddon.this.gBy != null) {
                this.eSn = ChatAddon.this.gBy;
                this.ioB.setVisible(false);
                this.ioC.setVisible(false);
                this.ioF.setVisible(false);
                this.ioI.setVisible(true);
                this.ioJ.setVisible(true);
                this.ioK.setVisible(true);
            } else {
                this.ioB.setVisible(true);
                this.ioC.setVisible(true);
                this.ioF.setVisible(true);
                this.ioI.setVisible(false);
                this.ioJ.setVisible(false);
                this.ioK.setVisible(false);
            }
            if (ChatAddon.this.cwx()) {
                this.ioE.setVisible(true);
                this.ioD.setVisible(false);
                return;
            }
            this.ioE.setVisible(false);
            this.ioD.setVisible(true);
        }

        public void show(Component component, int i, int i2) {
            if (ChatAddon.this.f9780P.dxk()) {
                this.ioG.setEnabled(true);
            } else {
                this.ioG.setEnabled(false);
            }
            if (ChatAddon.this.f9780P.dxE()) {
                this.ioH.setEnabled(true);
            } else {
                this.ioH.setEnabled(false);
            }
            diZ();
            ChatAddon.super.show(component, i, i2);
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$g */
        /* compiled from: a */
        class C4201g implements ActionListener {
            C4201g() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (ChatAddon.this.gBk.isVisible()) {
                    ChatAddon.this.gBk.setVisible(false);
                    if (ChatAddon.this.gBk.isSelected()) {
                        ChatAddon.this.gBl.doClick();
                        return;
                    }
                    return;
                }
                ChatAddon.this.gBk.setVisible(true);
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$h */
        /* compiled from: a */
        class C4202h implements ActionListener {
            C4202h() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (ChatAddon.this.gBo.isVisible()) {
                    ChatAddon.this.gBo.setVisible(false);
                    if (ChatAddon.this.gBo.isSelected()) {
                        ChatAddon.this.gBl.doClick();
                        return;
                    }
                    return;
                }
                ChatAddon.this.gBo.setVisible(true);
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$e */
        /* compiled from: a */
        class C4199e implements ActionListener {
            C4199e() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (ChatAddon.this.gBm.isVisible()) {
                    ChatAddon.this.gBm.setVisible(false);
                    if (ChatAddon.this.gBm.isSelected()) {
                        ChatAddon.this.gBl.doClick();
                        return;
                    }
                    return;
                }
                ChatAddon.this.gBm.setVisible(true);
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$f */
        /* compiled from: a */
        class C4200f implements ActionListener {
            C4200f() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (ChatAddon.this.gBn.isVisible()) {
                    ChatAddon.this.gBn.setVisible(false);
                    if (ChatAddon.this.gBn.isSelected()) {
                        ChatAddon.this.gBl.doClick();
                        return;
                    }
                    return;
                }
                ChatAddon.this.gBn.setVisible(true);
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$c */
        /* compiled from: a */
        class C4197c implements ActionListener {
            C4197c() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (ChatAddon.this.gBp.isVisible()) {
                    ChatAddon.this.gBp.setVisible(false);
                    if (ChatAddon.this.gBp.isSelected()) {
                        ChatAddon.this.gBl.doClick();
                        return;
                    }
                    return;
                }
                ChatAddon.this.gBp.setVisible(true);
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$d */
        /* compiled from: a */
        class C4198d implements ActionListener {
            C4198d() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                C6415amX cpR = ((HudObjectListAddon) ChatAddon.this.f9781kj.mo11830U(HudObjectListAddon.class)).cpR();
                int rowCount = cpR.getRowCount();
                for (int i = 0; i < rowCount; i++) {
                    if (((String) cpR.getValueAt(i, 1)).equals(C4194k.this.eSn)) {
                        cpR.mo14841sE(i);
                        return;
                    }
                }
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$a */
        class C4195a implements ActionListener {
            C4195a() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                ChatAddon.this.mo23573aw((String) null, "/invite " + C4194k.this.eSn);
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$b */
        /* compiled from: a */
        class C4196b implements ActionListener {
            C4196b() {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                if (C4194k.this.eSn != null) {
                    ChatAddon.this.gBw.setName(C4194k.this.eSn);
                    if (ChatAddon.this.gBx.getIndexOf(ChatAddon.this.gBw) < 0) {
                        ChatAddon.this.gBx.addElement(ChatAddon.this.gBw);
                    }
                    ChatAddon.this.gBx.setSelectedItem(ChatAddon.this.gBw);
                    ChatAddon.this.gBg.setText("");
                    ChatAddon.this.gBg.requestFocus();
                }
            }
        }

        /* renamed from: taikodom.addon.chat.ChatAddon$k$i */
        /* compiled from: a */
        class C4203i implements Action {
            private int eqa = 11;
            private C6718asO eqb;

            public C4203i(int i) {
                this.eqa = i;
            }

            public void addPropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            }

            public Object getValue(String str) {
                if (str == "Name") {
                    switch (this.eqa) {
                        case 0:
                            return ChatAddon.this.f9781kj.translate("change font color");
                        case 10:
                            return ChatAddon.this.f9781kj.translate("small");
                        case 11:
                            return ChatAddon.this.f9781kj.translate("normal");
                        case 14:
                            return ChatAddon.this.f9781kj.translate("big");
                        case ChatAddon.gAZ /*1638*/:
                            return ChatAddon.this.f9781kj.translate("hide portraits");
                        case ChatAddon.PORTRAIT /*2457*/:
                            return ChatAddon.this.f9781kj.translate("show portraits");
                    }
                }
                return null;
            }

            public boolean isEnabled() {
                return true;
            }

            public void setEnabled(boolean z) {
            }

            public void putValue(String str, Object obj) {
            }

            public void removePropertyChangeListener(PropertyChangeListener propertyChangeListener) {
            }

            public void actionPerformed(ActionEvent actionEvent) {
                switch (this.eqa) {
                    case 0:
                        if (this.eqb == null) {
                            this.eqb = new C6718asO(ChatAddon.this.f9781kj);
                        }
                        this.eqb.show();
                        return;
                    case ChatAddon.gAZ /*1638*/:
                        ChatAddon.this.mo23580gE(false);
                        return;
                    case ChatAddon.PORTRAIT /*2457*/:
                        ChatAddon.this.mo23580gE(true);
                        return;
                    default:
                        ChatAddon.this.mo23582pE(this.eqa);
                        return;
                }
            }
        }
    }
}
