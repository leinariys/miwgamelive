package taikodom.addon.messagedialog;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C6623aqX;
import logic.C6245ajJ;
import logic.IAddonSettings;
import logic.ui.ComponentManager;
import logic.ui.item.FormattedTextField;
import logic.ui.item.InternalFrame;
import logic.ui.item.Repeater;
import logic.ui.item.TextArea;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.messagedialog")
/* compiled from: a */
public class MessageDialogAddon implements C2495fo {

    /* renamed from: nR */
    private static /* synthetic */ int[] f10107nR;
    /* access modifiers changed from: private */

    /* renamed from: kj */
    public IAddonProperties f10108kj;
    /* access modifiers changed from: private */

    /* renamed from: nM */
    public InternalFrame f10109nM;
    /* renamed from: nQ */
    public JCheckBox f10113nQ;
    /* renamed from: nN */
    private C6124ags<C6623aqX> f10110nN;
    /* renamed from: nO */
    private C6124ags<C0939Nn> f10111nO;
    /* access modifiers changed from: private */
    /* renamed from: nP */
    private C3428rT<C3689to> f10112nP;

    /* renamed from: gr */
    static /* synthetic */ int[] m44571gr() {
        int[] iArr = f10107nR;
        if (iArr == null) {
            iArr = new int[C0939Nn.C0940a.values().length];
            try {
                iArr[C0939Nn.C0940a.CONFIRM.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C0939Nn.C0940a.ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C0939Nn.C0940a.INFO.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C0939Nn.C0940a.WARNING.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            f10107nR = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10108kj = addonPropertie;
        this.f10110nN = new C4721f();
        this.f10111nO = new C4714a();
        this.f10112nP = new C4718d();
        C6245ajJ aVU = this.f10108kj.aVU();
        aVU.mo13965a(C6623aqX.class, this.f10110nN);
        aVU.mo13965a(C0939Nn.class, this.f10111nO);
        aVU.mo13965a(C3689to.class, this.f10112nP);
    }

    public void stop() {
    }

    /* access modifiers changed from: private */
    /* renamed from: gq */
    public void m44570gq() {
        synchronized (this) {
            if (this.f10109nM != null) {
                this.f10109nM.setVisible(false);
                this.f10109nM.destroy();
                this.f10109nM = null;
                notify();
            }
        }
    }

    /* renamed from: a */
    public InternalFrame mo24307a(String str, aEP aep, String... strArr) {
        if (aep == null) {
            aep = new aEP();
        }
        boolean VC = aep.mo8595VC();
        this.f10109nM = this.f10108kj.bHv().mo16792bN("messageDialog.xml");
        C0939Nn.C0940a yD = aep.mo8601yD();
        ComponentManager.getCssHolder(this.f10109nM).setAttribute("class", yD.title);
        ComponentManager.getCssHolder(this.f10109nM).mo13045Vk();
        Repeater<?> ch = this.f10109nM.mo4919ch("buttons");
        FormattedTextField cd = this.f10109nM.mo4915cd("input");
        this.f10113nQ = this.f10109nM.mo4915cd("checkbox");
        String cyE = aep.cyE();
        if (cyE != null) {
            this.f10113nQ.setText(cyE);
            this.f10113nQ.setVisible(true);
        } else {
            this.f10113nQ.setVisible(false);
        }
        ch.mo22250a((Repeater.C3671a<?>) new C4719e(aep, VC, cd));
        for (int i = 0; i < strArr.length; i++) {
            ch.mo22248G(new C4724h(strArr[i], i));
        }
        TextArea cd2 = this.f10109nM.mo4915cd("message");
        cd2.setText(str);
        cd2.setSize(cd2.getPreferredSize());
        String header = aep.getHeader();
        if (header == null) {
            header = m44564a(yD);
        }
        if (header == null) {
            this.f10109nM.mo4915cd("header").setVisible(false);
        } else {
            this.f10109nM.mo4917cf("header").setText(header);
        }
        cd.setVisible(VC);
        cd.setEnabled(VC);
        if (VC) {
            cd.requestFocus();
            aep.mo8597a(cd);
        }
        this.f10109nM.pack();
        this.f10109nM.center();
        this.f10109nM.setVisible(true);
        this.f10109nM.setAlwaysOnTop(true);
        return this.f10109nM;
    }

    /* renamed from: a */
    public InternalFrame mo24306a(C0939Nn.C0940a aVar, String str, String... strArr) {
        return mo24307a(str, (aEP) new C4716b(aVar), strArr);
    }

    /* renamed from: a */
    public InternalFrame mo24308a(String str, aFH afh) {
        if (afh == null) {
            afh = new aFH();
        }
        this.f10109nM = this.f10108kj.bHv().mo16792bN("quantityDialog.xml");
        JSpinner cd = this.f10109nM.mo4915cd("amount");
        cd.setModel(new SpinnerNumberModel(afh.getValue(), afh.getMinimum(), afh.getMaximum(), afh.cZi()));
        this.f10109nM.mo4913cb("cancel").addActionListener(new C4717c(afh, cd));
        this.f10109nM.mo4913cb("confirm").addActionListener(new C4723g(afh, cd));
        this.f10109nM.pack();
        this.f10109nM.center();
        this.f10109nM.setVisible(true);
        this.f10109nM.setAlwaysOnTop(true);
        return this.f10109nM;
    }

    /* renamed from: a */
    private String m44564a(C0939Nn.C0940a aVar) {
        switch (m44571gr()[aVar.ordinal()]) {
            case 2:
                return this.f10108kj.translate("Corfirmation");
            case 3:
                return this.f10108kj.translate("Warning");
            case 4:
                return this.f10108kj.translate("Error");
            default:
                return this.f10108kj.translate("Info");
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$h */
    /* compiled from: a */
    private class C4724h {
        final String gjy;
        final int index;

        public C4724h(String str, int i) {
            this.gjy = str;
            this.index = i;
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$f */
    /* compiled from: a */
    class C4721f extends C6124ags<C6623aqX> {
        C4721f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6623aqX aqx) {
            String format = C5956adg.format(aqx.mo15606KI().get(), aqx.getParams());
            C4722a aVar = new C4722a(aqx);
            synchronized (MessageDialogAddon.this) {
                while (MessageDialogAddon.this.f10109nM != null) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                    }
                }
                MessageDialogAddon.this.f10109nM = MessageDialogAddon.this.mo24307a(format, (aEP) aVar, MessageDialogAddon.this.f10108kj.translate("OK"));
            }
            return false;
        }

        /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$f$a */
        class C4722a extends aEP {
            private final /* synthetic */ C6623aqX eoA;

            C4722a(C6623aqX aqx) {
                this.eoA = aqx;
            }

            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                MessageDialogAddon.this.m44570gq();
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                if (this.eoA.crM() == null) {
                    return super.mo8601yD();
                }
                return this.eoA.crM();
            }
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$a */
    class C4714a extends C6124ags<C0939Nn> {
        C4714a() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0939Nn nn) {
            String format = C5956adg.format(nn.getMessage(), nn.getParams());
            C4715a aVar = new C4715a(nn);
            synchronized (MessageDialogAddon.this) {
                while (MessageDialogAddon.this.f10109nM != null) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                    }
                }
                MessageDialogAddon.this.f10109nM = MessageDialogAddon.this.mo24307a(format, (aEP) aVar, MessageDialogAddon.this.f10108kj.translate("OK"));
            }
            return false;
        }

        /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$a$a */
        class C4715a extends aEP {
            private final /* synthetic */ C0939Nn cYh;

            C4715a(C0939Nn nn) {
                this.cYh = nn;
            }

            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                MessageDialogAddon.this.m44570gq();
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                if (this.cYh.crM() == null) {
                    return super.mo8601yD();
                }
                return this.cYh.crM();
            }
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$d */
    /* compiled from: a */
    class C4718d extends C3428rT<C3689to> {
        C4718d() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (MessageDialogAddon.this.f10109nM != null) {
                MessageDialogAddon.this.m44570gq();
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$e */
    /* compiled from: a */
    class C4719e implements Repeater.C3671a<C4724h> {
        private final /* synthetic */ aEP cYn;
        private final /* synthetic */ boolean cYo;
        private final /* synthetic */ FormattedTextField cYp;

        C4719e(aEP aep, boolean z, FormattedTextField rAVar) {
            this.cYn = aep;
            this.cYo = z;
            this.cYp = rAVar;
        }

        /* renamed from: a */
        public void mo843a(C4724h hVar, Component component) {
            JButton jButton = (JButton) component;
            jButton.setText(hVar.gjy);
            jButton.addActionListener(new C4720a(this.cYn, hVar, this.cYo, this.cYp));
        }

        /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$e$a */
        class C4720a implements ActionListener {
            private final /* synthetic */ aEP cYn;
            private final /* synthetic */ boolean cYo;
            private final /* synthetic */ FormattedTextField cYp;
            private final /* synthetic */ C4724h ger;

            C4720a(aEP aep, C4724h hVar, boolean z, FormattedTextField rAVar) {
                this.cYn = aep;
                this.ger = hVar;
                this.cYo = z;
                this.cYp = rAVar;
            }

            public void actionPerformed(ActionEvent actionEvent) {
                MessageDialogAddon.this.m44570gq();
                this.cYn.mo8596a(this.ger.index, this.cYo ? this.cYp.getText() : null, MessageDialogAddon.this.f10113nQ.isSelected());
            }
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$b */
    /* compiled from: a */
    class C4716b extends aEP {
        private final /* synthetic */ C0939Nn.C0940a cYk;

        C4716b(C0939Nn.C0940a aVar) {
            this.cYk = aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: yD */
        public C0939Nn.C0940a mo8601yD() {
            return this.cYk;
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$c */
    /* compiled from: a */
    class C4717c implements ActionListener {
        private final /* synthetic */ JSpinner aDs;
        private final /* synthetic */ aFH cYl;

        C4717c(aFH afh, JSpinner jSpinner) {
            this.cYl = afh;
            this.aDs = jSpinner;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.cYl.mo8775a(false, ((Integer) this.aDs.getModel().getValue()).intValue());
            MessageDialogAddon.this.m44570gq();
        }
    }

    /* renamed from: taikodom.addon.messagedialog.MessageDialogAddon$g */
    /* compiled from: a */
    class C4723g implements ActionListener {
        private final /* synthetic */ JSpinner aDs;
        private final /* synthetic */ aFH cYl;

        C4723g(aFH afh, JSpinner jSpinner) {
            this.cYl = afh;
            this.aDs = jSpinner;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.cYl.mo8775a(true, ((Integer) this.aDs.getModel().getValue()).intValue());
            MessageDialogAddon.this.m44570gq();
        }
    }
}
