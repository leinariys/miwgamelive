package taikodom.addon.insurance;

import game.network.message.externalizable.C0186CL;
import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1368Tv;
import game.script.insurance.Insurer;
import game.script.item.Component;
import game.script.item.ComponentType;
import game.script.item.Item;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.IAddonSettings;
import logic.aaa.C2733jJ;
import logic.aaa.C6958awx;
import logic.swing.C5378aHa;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.TextArea;
import logic.ui.item.*;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.loading.LoadingAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

@TaikodomAddon("taikodom.addon.insurance")
/* compiled from: a */
public class InsuranceAddon implements C2495fo {

    /* renamed from: IW */
    private static final String f9971IW = "confirm-rescue.xml";
    /* renamed from: IJ */
    public aQF f9973IJ;
    /* access modifiers changed from: private */
    /* renamed from: IT */
    public InternalFrame f9982IT;
    /* renamed from: IU */
    public C3987xk f9983IU;
    /* renamed from: kj */
    public IAddonProperties f9985kj;
    /* renamed from: nM */
    public InternalFrame f9986nM;
    /* renamed from: II */
    private C6124ags<C0186CL> f9972II;
    /* renamed from: IK */
    private Table f9974IK;
    /* renamed from: IL */
    private Table f9975IL;
    /* renamed from: IM */
    private JLabel f9976IM;
    /* renamed from: IO */
    private JLabel f9977IO;
    /* access modifiers changed from: private */
    /* renamed from: IP */
    private JLabel f9978IP;
    /* access modifiers changed from: private */
    /* renamed from: IQ */
    private JLabel f9979IQ;
    /* renamed from: IR */
    private TextArea f9980IR;
    /* access modifiers changed from: private */
    /* renamed from: IS */
    private TextArea f9981IS;
    /* access modifiers changed from: private */
    /* renamed from: IV */
    private Table f9984IV;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9985kj = addonPropertie;
        this.f9972II = new C4566a();
        this.f9985kj.aVU().mo13965a(C0186CL.class, this.f9972II);
    }

    public void stop() {
        if (this.f9986nM != null) {
            this.f9986nM.destroy();
            this.f9986nM = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m44166a(Player aku, Ship fAVar) {
        if (this.f9986nM == null) {
            this.f9986nM = this.f9985kj.bHv().mo16792bN("insurance.xml");
        }
        this.f9973IJ = new aQF(this.f9985kj.ala().aJe().mo19019xv(), aku, fAVar);
        this.f9973IJ.dqa();
        m44176oS();
        this.f9986nM.center();
        this.f9986nM.setVisible(true);
    }

    /* renamed from: oS */
    private void m44176oS() {
        String str;
        String str2;
        this.f9983IU = C3987xk.bFK;
        this.f9974IK = this.f9986nM.mo4915cd("notInsuredItemsTable");
        this.f9975IL = this.f9986nM.mo4915cd("insuredItemsTable");
        this.f9976IM = this.f9986nM.mo4917cf("notInsuredItemsPrice");
        this.f9977IO = this.f9986nM.mo4917cf("insuredItemsPrice");
        this.f9978IP = this.f9986nM.mo4917cf("surplus");
        this.f9979IQ = this.f9986nM.mo4917cf("deductible");
        this.f9980IR = this.f9986nM.mo4915cd("descriptionTop");
        TextArea ae = this.f9980IR;
        if (this.f9973IJ.dpZ() != null) {
            str = this.f9973IJ.dpZ();
        } else {
            str = "";
        }
        ae.setText(str);
        this.f9981IS = this.f9986nM.mo4915cd("descriptionDefault");
        TextArea ae2 = this.f9981IS;
        if (this.f9973IJ.mo10892xv().awx() != null) {
            str2 = this.f9973IJ.mo10892xv().awx().get();
        } else {
            str2 = "";
        }
        ae2.setText(str2);
        mo24139oV();
        this.f9986nM.mo4913cb("add").addActionListener(new C4568b());
        this.f9986nM.mo4913cb("remove").addActionListener(new C4569c());
        this.f9986nM.mo4913cb("addAll").addActionListener(new C4570d());
        this.f9986nM.mo4913cb("removeAll").addActionListener(new C4571e());
        this.f9986nM.mo4913cb("rescueItems").addActionListener(new C4572f());
    }

    /* access modifiers changed from: protected */
    /* renamed from: oT */
    public void mo24137oT() {
        if (this.f9982IT == null) {
            this.f9982IT = this.f9985kj.bHv().mo16792bN(f9971IW);
            this.f9982IT.mo4913cb("yesBtn").addActionListener(new C4573g());
            this.f9982IT.mo4913cb("noBtn").addActionListener(new C4574h());
            this.f9982IT.center();
            this.f9982IT.setVisible(true);
        } else if (this.f9982IT.isVisible()) {
            this.f9982IT.center();
            this.f9982IT.toFront();
        } else {
            this.f9982IT.setVisible(true);
        }
        if (this.f9984IV == null) {
            this.f9984IV = this.f9982IT.mo4915cd("confirmItemsTable");
        }
        m44167a(this.f9984IV, this.f9973IJ.dpY());
    }

    /* access modifiers changed from: protected */
    /* renamed from: oU */
    public void mo24138oU() {
        String translate = this.f9985kj.translate("You don't have enough Taels to pay the insurance for the selected items.");
        if (this.f9973IJ.dpU()) {
            try {
                ((Insurer) C3582se.m38985a(this.f9985kj.ala().aJA(), (C6144ahM<?>) new C4575i(translate))).mo11895b(this.f9985kj.getPlayer(), this.f9973IJ.dpY());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            mo24136a(C0939Nn.C0940a.ERROR, translate);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m44172c(List<Item> list) {
        if (list.size() > 0) {
            this.f9985kj.aVU().mo13975h(new C2733jJ(this.f9985kj.translate("Insurance paid. The selected items are now redeemed."), false, new Object[0]));
            String translate = this.f9985kj.translate("Items redeemed by insurance: ");
            boolean z = true;
            for (Item next : list) {
                if (z) {
                    z = false;
                } else {
                    translate = String.valueOf(translate) + ", ";
                }
                translate = String.valueOf(translate) + next.bAP().mo19891ke().get();
            }
            this.f9985kj.aVU().mo13975h(new C1368Tv((Player) null, this.f9985kj.getPlayer(), C1368Tv.C1369a.ACTIONS, translate));
        }
        this.f9986nM.setVisible(false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: oV */
    public void mo24139oV() {
        m44167a(this.f9974IK, this.f9973IJ.dpX());
        m44167a(this.f9975IL, this.f9973IJ.dpY());
        this.f9976IM.setText(String.valueOf(this.f9983IU.format(this.f9973IJ.mo10868N(this.f9973IJ.dpX()))));
        long N = this.f9973IJ.mo10868N(this.f9973IJ.dpY());
        this.f9977IO.setText(String.valueOf(this.f9983IU.format(N)));
        this.f9978IP.setText(String.valueOf(this.f9983IU.format(this.f9973IJ.mo10888lM(N))));
        this.f9979IQ.setText(String.valueOf(this.f9983IU.mo22980dY(this.f9973IJ.mo10889lN(N))));
    }

    /* access modifiers changed from: protected */
    /* renamed from: oW */
    public void mo24140oW() {
        this.f9973IJ.mo10890oW();
        mo24139oV();
    }

    /* access modifiers changed from: protected */
    /* renamed from: oX */
    public void mo24141oX() {
        this.f9973IJ.mo10891oX();
        mo24139oV();
    }

    /* access modifiers changed from: protected */
    /* renamed from: oY */
    public void mo24142oY() {
        if (this.f9975IL.getSelectedRowCount() > 0) {
            ArrayList arrayList = new ArrayList();
            for (int convertRowIndexToModel : this.f9975IL.getSelectedRows()) {
                arrayList.add((Item) this.f9975IL.getModel().getValueAt(this.f9975IL.convertRowIndexToModel(convertRowIndexToModel), -1));
            }
            this.f9973IJ.mo10870P(arrayList);
            mo24139oV();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: oZ */
    public void mo24143oZ() {
        if (this.f9974IK.getSelectedRowCount() > 0) {
            ArrayList arrayList = new ArrayList();
            for (int convertRowIndexToModel : this.f9974IK.getSelectedRows()) {
                arrayList.add((Item) this.f9974IK.getModel().getValueAt(this.f9974IK.convertRowIndexToModel(convertRowIndexToModel), -1));
            }
            this.f9973IJ.mo10869O(arrayList);
            mo24139oV();
        }
    }

    /* renamed from: a */
    private void m44167a(Table pzVar, List<Item> list) {
        pzVar.setModel(new C4579k(list));
        pzVar.getColumnModel().getColumn(0).setCellRenderer(new C4578j());
        pzVar.getColumnModel().getColumn(1).setCellRenderer(new C4580l());
        C5616aQe.m17578a(pzVar, (Insets) null, true, false);
    }

    /* renamed from: a */
    public void mo24136a(C0939Nn.C0940a aVar, String str) {
        ((MessageDialogAddon) this.f9985kj.mo11830U(MessageDialogAddon.class)).mo24306a(aVar, str, this.f9985kj.translate("Ok"));
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public long m44175j(Item auq) {
        return this.f9973IJ.mo10886g(auq, true);
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$a */
    class C4566a extends C3428rT<C0186CL> {
        C4566a() {
        }

        /* renamed from: a */
        public void mo321b(C0186CL cl) {
            if (((LoadingAddon) InsuranceAddon.this.f9985kj.mo11830U(LoadingAddon.class)).isVisible()) {
                InsuranceAddon.this.f9985kj.aVU().mo13965a(C6958awx.class, new C4567a(cl));
            } else {
                InsuranceAddon.this.m44166a(cl.mo954dL(), cl.mo953al());
            }
        }

        /* renamed from: taikodom.addon.insurance.InsuranceAddon$a$a */
        class C4567a extends C3428rT<C6958awx> {
            private final /* synthetic */ C0186CL bnw;

            C4567a(C0186CL cl) {
                this.bnw = cl;
            }

            /* renamed from: a */
            public void mo321b(C6958awx awx) {
                InsuranceAddon.this.m44166a(this.bnw.mo954dL(), this.bnw.mo953al());
                InsuranceAddon.this.f9985kj.aVU().mo13964a(this);
            }
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$b */
    /* compiled from: a */
    class C4568b implements ActionListener {
        C4568b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InsuranceAddon.this.mo24143oZ();
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$c */
    /* compiled from: a */
    class C4569c implements ActionListener {
        C4569c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InsuranceAddon.this.mo24142oY();
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$d */
    /* compiled from: a */
    class C4570d implements ActionListener {
        C4570d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InsuranceAddon.this.mo24140oW();
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$e */
    /* compiled from: a */
    class C4571e implements ActionListener {
        C4571e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InsuranceAddon.this.mo24141oX();
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$f */
    /* compiled from: a */
    class C4572f implements ActionListener {
        C4572f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InsuranceAddon.this.mo24137oT();
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$g */
    /* compiled from: a */
    class C4573g implements ActionListener {
        C4573g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InsuranceAddon.this.f9982IT.setVisible(false);
            InsuranceAddon.this.mo24138oU();
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$h */
    /* compiled from: a */
    class C4574h implements ActionListener {
        C4574h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            InsuranceAddon.this.f9982IT.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$i */
    /* compiled from: a */
    class C4575i implements C6144ahM<List<Item>> {
        private final /* synthetic */ String bBJ;

        C4575i(String str) {
            this.bBJ = str;
        }

        /* renamed from: b */
        public void mo1931n(List<Item> list) {
            SwingUtilities.invokeLater(new C4577b());
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            SwingUtilities.invokeLater(new C4576a(th, this.bBJ));
        }

        /* renamed from: taikodom.addon.insurance.InsuranceAddon$i$b */
        /* compiled from: a */
        class C4577b implements Runnable {
            C4577b() {
            }

            public void run() {
                InsuranceAddon.this.m44172c(InsuranceAddon.this.f9973IJ.dpY());
            }
        }

        /* renamed from: taikodom.addon.insurance.InsuranceAddon$i$a */
        class C4576a implements Runnable {
            private final /* synthetic */ String bBJ;
            private final /* synthetic */ Throwable dUm;

            C4576a(Throwable th, String str) {
                this.dUm = th;
                this.bBJ = str;
            }

            public void run() {
                if (this.dUm instanceof C1061PX) {
                    InsuranceAddon.this.mo24136a(C0939Nn.C0940a.ERROR, this.bBJ);
                }
                if (this.dUm instanceof C3099nn) {
                    this.dUm.printStackTrace();
                    InsuranceAddon.this.f9986nM.setVisible(false);
                }
                if (this.dUm instanceof C2293dh) {
                    this.dUm.printStackTrace();
                    InsuranceAddon.this.f9986nM.setVisible(false);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$k */
    /* compiled from: a */
    class C4579k extends AbstractTableModel {

        private final /* synthetic */ List dNe;

        C4579k(List list) {
            this.dNe = list;
        }

        public int getColumnCount() {
            return 2;
        }

        public int getRowCount() {
            return this.dNe.size();
        }

        public Object getValueAt(int i, int i2) {
            Item auq = (Item) this.dNe.get(i);
            switch (i2) {
                case 1:
                    return InsuranceAddon.this.f9983IU.format(InsuranceAddon.this.m44175j(auq));
                default:
                    return auq;
            }
        }

        public String getColumnName(int i) {
            switch (i) {
                case 0:
                    return "Item";
                case 1:
                    return "Price (T$)";
                default:
                    return "";
            }
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$j */
    /* compiled from: a */
    class C4578j extends C2122cE {
        C4578j() {
        }

        /* renamed from: a */
        public java.awt.Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            Item auq = (Item) obj;
            Panel nR = aur.mo11631nR("itemDetailView");
            LabeledIcon cd = nR.mo4915cd("icon");
            cd.mo2605a((Icon) new C2539gY(InsuranceAddon.this.f9985kj.bHv().adz().getImage(String.valueOf(C5378aHa.ITEMS.getPath()) + auq.bAP().mo12100sK().getHandle())));
            cd.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, String.valueOf(auq.mo302Iq()));
            Picture a = cd.mo2601a(LabeledIcon.C0530a.NORTH_WEST);
            if (auq instanceof Component) {
                ComponentManager.getCssHolder(a).setAttribute("class", "size" + ((ComponentType) auq.bAP()).cuJ());
                a.setVisible(true);
            } else {
                a.setVisible(false);
            }
            nR.mo4917cf("name").setText(auq.bAP().mo19891ke().get());
            nR.mo4917cf("billing").setVisible(auq.cxn());
            return nR;
        }
    }

    /* renamed from: taikodom.addon.insurance.InsuranceAddon$l */
    /* compiled from: a */
    class C4580l extends C2122cE {
        C4580l() {
        }

        /* renamed from: a */
        public java.awt.Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel nR = aur.mo11631nR("itemPriceView");
            nR.setText(String.valueOf(obj));
            return nR;
        }
    }
}
