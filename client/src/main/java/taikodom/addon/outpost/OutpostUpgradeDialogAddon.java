package taikodom.addon.outpost;

import game.network.message.externalizable.C3689to;
import logic.IAddonSettings;
import logic.ui.item.InternalFrame;
import p001a.C3428rT;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.outpost")
/* compiled from: a */
public class OutpostUpgradeDialogAddon implements C2495fo {
    /* renamed from: nM */
    public InternalFrame f10204nM;
    private C3428rT<C3689to> dHH;
    /* access modifiers changed from: private */
    /* renamed from: kj */
    private IAddonProperties f10203kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10203kj = addonPropertie;
        this.dHH = new C4866b();
        this.f10203kj.aVU().mo13965a(C3689to.class, this.dHH);
    }

    public void stop() {
        if (this.f10204nM != null) {
            this.f10204nM.destroy();
            this.f10204nM = null;
        }
    }

    /* renamed from: a */
    public void mo24458a(C4868d dVar, long j) {
        if (this.f10204nM == null) {
            this.f10204nM = this.f10203kj.bHv().mo16792bN("outpostUpgradeDialog.xml");
            this.f10204nM.mo4913cb("confirm").addActionListener(new C4865a(dVar));
            this.f10204nM.mo4913cb("cancel").addActionListener(new C4867c());
            this.f10204nM.mo4917cf("upgradeCost").setText(String.valueOf(j));
            this.f10204nM.setSize(250, 130);
            this.f10204nM.pack();
            this.f10204nM.setLocation((this.f10203kj.bHv().getScreenWidth() / 2) - (this.f10204nM.getSize().width / 2), (this.f10203kj.bHv().getScreenHeight() / 2) - (this.f10204nM.getSize().height / 2));
            this.f10204nM.setVisible(true);
        }
    }

    public boolean isVisible() {
        if (this.f10204nM == null) {
            return false;
        }
        return this.f10204nM.isVisible();
    }

    /* renamed from: taikodom.addon.outpost.OutpostUpgradeDialogAddon$d */
    /* compiled from: a */
    interface C4868d {
        void bmD();
    }

    /* renamed from: taikodom.addon.outpost.OutpostUpgradeDialogAddon$b */
    /* compiled from: a */
    class C4866b extends C3428rT<C3689to> {
        C4866b() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (OutpostUpgradeDialogAddon.this.f10204nM != null && OutpostUpgradeDialogAddon.this.f10204nM.isVisible()) {
                OutpostUpgradeDialogAddon.this.f10204nM.setVisible(false);
                OutpostUpgradeDialogAddon.this.f10204nM.destroy();
                OutpostUpgradeDialogAddon.this.f10204nM = null;
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.outpost.OutpostUpgradeDialogAddon$a */
    class C4865a implements ActionListener {
        private final /* synthetic */ C4868d dpz;

        C4865a(C4868d dVar) {
            this.dpz = dVar;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            this.dpz.bmD();
            OutpostUpgradeDialogAddon.this.f10204nM.destroy();
            OutpostUpgradeDialogAddon.this.f10204nM = null;
        }
    }

    /* renamed from: taikodom.addon.outpost.OutpostUpgradeDialogAddon$c */
    /* compiled from: a */
    class C4867c implements ActionListener {
        C4867c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            OutpostUpgradeDialogAddon.this.f10204nM.destroy();
            OutpostUpgradeDialogAddon.this.f10204nM = null;
        }
    }
}
