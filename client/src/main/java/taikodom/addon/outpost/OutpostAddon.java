package taikodom.addon.outpost;

import game.script.Actor;
import game.script.corporation.Corporation;
import game.script.player.Player;
import game.script.ship.Outpost;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.aaa.C6388alw;
import logic.baa.C6200aiQ;
import logic.data.link.C1092Px;
import logic.res.code.C5663aRz;
import logic.ui.item.TaskPane;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;

@TaikodomAddon("taikodom.addon.outpost")
/* compiled from: a */
public class OutpostAddon implements C2495fo {

    /* access modifiers changed from: private */
    public aSZ aCF;
    /* renamed from: kj */
    public IAddonProperties f10202kj;
    /* renamed from: P */
    private Player f10201P;
    private aGC aCC;
    private C3428rT<C6388alw> aCD;
    private C6200aiQ<Player> aCE;
    private TaskPane aCG;
    private C5884acM aCH;
    private C5884acM aCI;
    /* access modifiers changed from: private */
    private C6124ags<C5783aaP> amy;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10202kj = addonPropertie;
        this.f10201P = this.f10202kj.getPlayer();
        if (this.f10201P != null) {
            m45064iG();
            m45057Im();
            this.aCC = new C4861a(this.f10202kj.translate("Outpost"), "outpost");
            this.aCC.mo15602jH(this.f10202kj.translate("Here you can check the status of your corporation outpost and pay its upkeep."));
            this.f10202kj.mo2322a("outpost", (Action) this.aCC);
            this.aCF = new aSZ(this.aCG, this.aCC, 200, 2);
            this.f10202kj.aVU().publish(this.aCF);
            if (this.aCC.isEnabled()) {
                open();
            }
        }
    }

    /* renamed from: iG */
    private void m45064iG() {
        this.amy = new C4862b();
        this.f10202kj.aVU().mo13965a(C5783aaP.class, this.amy);
        this.aCD = new C4863c();
        this.f10202kj.aVU().mo13965a(C6388alw.class, this.aCD);
        this.aCE = new C4864d();
        this.f10201P.mo8320a(C1092Px.dQN, (C6200aiQ<?>) this.aCE);
    }

    @C2602hR(mo19255zf = "OUTPOST_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24454al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: Im */
    private void m45057Im() {
        this.aCG = (TaskPane) this.f10202kj.bHv().mo16794bQ("outpostview.xml");
        this.aCH = new C6745asp(this.f10202kj, this.aCG.mo4916ce(C0891Mw.DATA));
        this.aCI = new C6252ajQ(this.f10202kj, this.aCG.mo4916ce("upkeep"));
    }

    public void stop() {
        if (this.f10201P != null) {
            this.f10201P.mo8326b(C1092Px.dQN, (C6200aiQ<?>) this.aCE);
        }
        destroy();
    }

    /* access modifiers changed from: private */
    public void refresh(boolean z) {
        Outpost Ns = m45058Ns();
        if (Ns != null) {
            Corporation bYd = this.f10201P.bYd();
            if (bYd == null || !bYd.mo10707Qy().contains(Ns)) {
                this.f10202kj.aVU().mo13975h(new C0269DS(this.aCF, false));
                return;
            }
            this.aCH.mo12600a(Ns, z);
            this.aCI.mo12600a(Ns, z);
        }
    }

    private void destroy() {
        this.aCH.destroy();
        this.aCI.destroy();
        this.aCG.destroy();
        this.aCG = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: Ns */
    public Outpost m45058Ns() {
        Actor bhE = this.f10201P.bhE();
        if (!(bhE instanceof Outpost)) {
            return null;
        }
        return (Outpost) bhE;
    }

    /* access modifiers changed from: private */
    public void open() {
        refresh(false);
        this.f10202kj.aVU().mo13975h(new C0269DS(this.aCF, true));
    }

    /* renamed from: taikodom.addon.outpost.OutpostAddon$a */
    class C4861a extends aGC {
        private static final long serialVersionUID = 4728384141844315652L;

        C4861a(String str, String str2) {
            super(str, str2);
        }

        public boolean isEnabled() {
            Outpost b;
            Corporation bYd = OutpostAddon.this.f10202kj.ala().getPlayer().bYd();
            if (bYd != null && (b = OutpostAddon.this.m45058Ns()) != null && bYd.mo10707Qy().contains(b) && b.bXV() == Outpost.C3349b.SOLD) {
                return true;
            }
            return false;
        }

        public void setVisible(boolean z) {
            if (z) {
                OutpostAddon.this.open();
            }
        }
    }

    /* renamed from: taikodom.addon.outpost.OutpostAddon$b */
    /* compiled from: a */
    class C4862b extends C6124ags<C5783aaP> {
        C4862b() {
        }

        /* renamed from: b */
        public boolean updateInfo(C5783aaP aap) {
            if (aap.bMO() != C5783aaP.C1841a.DOCKED) {
                return false;
            }
            OutpostAddon.this.f10202kj.aVU().mo13975h(new C0269DS(OutpostAddon.this.aCF, true));
            return false;
        }
    }

    /* renamed from: taikodom.addon.outpost.OutpostAddon$c */
    /* compiled from: a */
    class C4863c extends C3428rT<C6388alw> {
        C4863c() {
        }

        /* renamed from: a */
        public void mo321b(C6388alw alw) {
            OutpostAddon.this.refresh(alw.cic());
        }
    }

    /* renamed from: taikodom.addon.outpost.OutpostAddon$d */
    /* compiled from: a */
    class C4864d implements C6200aiQ<Player> {
        C4864d() {
        }

        /* renamed from: a */
        public void mo1143a(Player aku, C5663aRz arz, Object obj) {
            OutpostAddon.this.f10202kj.aVU().mo13975h(new C0269DS(OutpostAddon.this.aCF, arz != null));
        }
    }
}
