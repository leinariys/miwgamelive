package taikodom.addon.pda;

import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import game.network.message.externalizable.aTX;
import game.script.item.ClipType;
import game.script.item.ItemType;
import game.script.item.WeaponType;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.aaa.*;
import logic.baa.C4068yr;
import logic.res.KeyCode;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

@TaikodomAddon("taikodom.addon.pda")
/* compiled from: a */
public class PdaAddon implements C2495fo {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10210P;
    /* access modifiers changed from: private */
    public C4086zH ihS;
    /* renamed from: kj */
    public IAddonProperties f10211kj;
    /* renamed from: nM */
    public InternalFrame f10212nM;
    private C3428rT<C5783aaP> bvz;
    private C6622aqW dcE;
    /* access modifiers changed from: private */
    private C6124ags<C0633Iy> iOr;
    /* access modifiers changed from: private */
    private C3428rT<C1549Wj> iOs;
    /* renamed from: wz */
    private C3428rT<C3689to> f10213wz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10211kj = addonPropertie;
        this.f10210P = this.f10211kj.getEngineGame().mo4089dL();
        m45152Im();
        m45158iG();
        this.dcE = new C4897a("", this.f10211kj.translate("PDA"), "PDA");
        this.dcE.mo15602jH(this.f10211kj.translate("This opens your PDA, where data gathered from everywhere you have been is available."));
        this.dcE.mo15603jI("PDA_WINDOW");
        this.dcE.putValue("ShortDescription", this.f10211kj.mo11832V("tooltip", "PDA"));
        this.f10211kj.aVU().mo13965a(C0633Iy.class, this.iOr);
        this.f10211kj.aVU().mo13965a(C5783aaP.class, this.bvz);
        this.f10211kj.aVU().mo13965a(C3689to.class, this.f10213wz);
        this.f10211kj.aVU().mo13965a(C1549Wj.class, this.iOs);
        this.f10211kj.aVU().publish(new C5344aFs(this.dcE, 999, true));
        this.f10211kj.aVU().publish(new aTX(this.dcE, KeyCode.cua, "pda-commands.css", this));
    }

    @C2602hR(mo19255zf = "PDA_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo24485al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: iG */
    private void m45158iG() {
        this.bvz = new C4898b();
        this.f10213wz = new C4899c();
        this.iOr = new C4900d();
        this.iOs = new C4901e();
    }

    /* renamed from: aa */
    public C4068yr mo24484aa(ItemType jCVar) {
        if ((jCVar instanceof WeaponType) || (jCVar instanceof ClipType)) {
            return jCVar.mo19866HC();
        }
        if (jCVar instanceof C4068yr) {
            return (C4068yr) jCVar;
        }
        return null;
    }

    /* renamed from: JW */
    public IAddonProperties mo24483JW() {
        return this.f10211kj;
    }

    /* access modifiers changed from: private */
    public void open() {
        if (!isVisible()) {
            for (C5685aSv dirty : this.ihS.duf()) {
                dirty.setDirty(true);
            }
            this.ihS.duh().duw();
        }
        if (this.f10212nM == null) {
            m45152Im();
        } else {
            bWg();
        }
    }

    public void bWg() {
        if (isVisible()) {
            this.f10212nM.setVisible(false);
        } else {
            makeVisible();
        }
    }

    public void makeVisible() {
        m45159xK();
        this.f10212nM.center();
    }

    /* renamed from: xK */
    private void m45159xK() {
        this.f10212nM.setVisible(true);
    }

    /* renamed from: Im */
    private void m45152Im() {
        this.f10212nM = this.f10211kj.bHv().mo16792bN("pda.xml");
        this.f10212nM.addComponentListener(new C0962O("pda"));
        this.f10212nM.center();
        this.ihS = new C4086zH("pda", this.f10212nM);
        this.f10211kj.aVU().publish(this.ihS);
        dvB();
    }

    private void dvB() {
        this.f10212nM.addComponentListener(new C4902f());
    }

    public boolean isVisible() {
        return this.f10212nM != null && this.f10212nM.isVisible();
    }

    public void stop() {
        if (this.f10212nM != null) {
            this.f10212nM.destroy();
        }
        this.f10211kj.aVU().mo13974g(this.ihS);
        this.f10211kj.aVU().mo13964a(this.iOr);
        this.f10211kj.aVU().mo13964a(this.bvz);
        this.f10211kj.aVU().mo13964a(this.f10213wz);
        this.f10211kj.aVU().mo13964a(this.iOs);
    }

    public C4086zH dvC() {
        return this.ihS;
    }

    /* renamed from: taikodom.addon.pda.PdaAddon$a */
    class C4897a extends C6622aqW {


        C4897a(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        public boolean isActive() {
            return PdaAddon.this.f10212nM != null && (PdaAddon.this.f10212nM.isVisible() || PdaAddon.this.f10212nM.isAnimating());
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            PdaAddon.this.open();
        }
    }

    /* renamed from: taikodom.addon.pda.PdaAddon$b */
    /* compiled from: a */
    class C4898b extends C3428rT<C5783aaP> {
        C4898b() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (PdaAddon.this.isVisible()) {
                PdaAddon.this.f10212nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.pda.PdaAddon$c */
    /* compiled from: a */
    class C4899c extends C3428rT<C3689to> {
        C4899c() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (PdaAddon.this.isVisible()) {
                PdaAddon.this.f10212nM.setVisible(false);
                toVar.adC();
            }
        }
    }

    /* renamed from: taikodom.addon.pda.PdaAddon$d */
    /* compiled from: a */
    class C4900d extends C6124ags<C0633Iy> {
        C4900d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C0633Iy iy) {
            C4068yr aa = PdaAddon.this.mo24484aa(iy.mo2918az());
            if (aa == null || PdaAddon.this.f10210P.dyl().dBQ().containsKey(aa)) {
                return false;
            }
            PdaAddon.this.f10210P.dyl().mo11981e(aa);
            return false;
        }
    }

    /* renamed from: taikodom.addon.pda.PdaAddon$e */
    /* compiled from: a */
    class C4901e extends C3428rT<C1549Wj> {
        C4901e() {
        }

        /* renamed from: b */
        public void mo321b(C1549Wj wj) {
            C5685aSv duh;
            if (PdaAddon.this.isVisible() && (duh = PdaAddon.this.ihS.duh()) != null) {
                duh.dut().update();
            }
        }
    }

    /* renamed from: taikodom.addon.pda.PdaAddon$f */
    /* compiled from: a */
    class C4902f extends ComponentAdapter {
        C4902f() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            PdaAddon.this.f10211kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, PdaAddon.this));
        }
    }
}
