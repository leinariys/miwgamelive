package taikodom.addon.pda;

import game.script.item.ItemType;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.pda.DatabaseCategory;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.ship.ShipSectorType;
import logic.IAddonSettings;
import logic.aaa.C2055be;
import logic.aaa.C5685aSv;
import logic.baa.C4033yO;
import logic.baa.C4068yr;
import logic.res.sound.C0907NJ;
import logic.swing.C5378aHa;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.TextField;
import logic.ui.item.*;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.lang.reflect.Method;
import java.util.*;

@TaikodomAddon("taikodom.addon.pda")
/* compiled from: a */
public class DataBase implements C1215Rv, C2495fo {
    private static final String git = "database://";
    private static final int giu = 100;
    private static final int giv = 100;

    /* renamed from: qw */
    private static final String f10207qw = "database.xml";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10208P;
    /* access modifiers changed from: private */
    public PdaAddon aoJ;
    /* access modifiers changed from: private */
    public C5685aSv aoK;
    /* access modifiers changed from: private */
    public Component3d giA;
    /* access modifiers changed from: private */
    public Stack<C4033yO> giC;
    /* access modifiers changed from: private */
    public C4033yO giD;
    /* access modifiers changed from: private */
    public C5923acz giE;
    /* access modifiers changed from: private */
    public JCheckBox giw;
    /* access modifiers changed from: private */
    public JButton giy;
    /* renamed from: kj */
    public IAddonProperties f10209kj;
    /* access modifiers changed from: private */
    public JTextField searchField;
    C6964axd giH = new C6964axd("hidden root", new C6167ahj(this));
    private C2698il aoL;
    private DatabaseCategory avy;
    private DefaultTableModel ehH;
    private C5519aMl ejA = new C6164ahg(this);
    private HashMap<String, C4033yO> giB;
    private HashMap<DatabaseCategory, C6964axd> giF;
    private C0037AU giG = new C6165ahh(this);
    private EditorPane gix;
    /* access modifiers changed from: private */
    private Panel giz;
    private JLabel iconLabel;
    private JTable table;

    /* renamed from: Im */
    private void m45113Im() {
        this.giE = this.aoL.mo4915cd("databaseTree");
        this.giE.setRootVisible(false);
        this.searchField = this.aoL.mo4915cd("searchField");
        this.giw = this.aoL.mo4915cd("newCheckBox");
        this.gix = this.aoL.mo4915cd("description");
        this.iconLabel = this.aoL.mo4915cd("iconLabel");
        this.giz = (Panel) this.aoL.mo4916ce("iconPanel");
        this.giA = this.aoL.mo4915cd("iconRender");
        this.giy = this.aoL.mo4915cd("backButton");
        this.giy.setEnabled(false);
        this.table = this.aoL.mo4915cd("table");
        addHyperlinkListener(new C4896m());
        cnI();
        buY();
        cnG();
        cnH();
        m45112Fa();
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10209kj = addonPropertie;
        this.aoJ = (PdaAddon) addonPropertie.mo11830U(PdaAddon.class);
        this.aoK = new C5685aSv("pda", this.f10209kj, this);
        this.f10209kj.aVU().mo13965a(C2055be.class, new C4894k());
        this.f10208P = this.f10209kj.getPlayer();
        this.giB = new HashMap<>();
        this.giC = new Stack<>();
        if (this.f10208P != null) {
            this.avy = this.f10209kj.ala().aJe().mo19020xx().cJv();
            this.giF = new HashMap<>();
            this.f10209kj.aVU().publish(this.aoK);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: jB */
    public void m45135jB(String str) {
        if (str.startsWith(git)) {
            this.aoJ.dvC().mo23272c(this.aoK);
            mo24470jC(str.substring(git.length()));
        }
    }

    public void stop() {
        this.f10209kj.aVU().mo13974g(this.aoK);
    }

    public void addHyperlinkListener(HyperlinkListener hyperlinkListener) {
        this.gix.addHyperlinkListener(hyperlinkListener);
    }

    public void cnF() {
        this.giC.clear();
        this.giy.setEnabled(false);
    }

    private void cnG() {
        this.giE.setModel(new C5518aMk(this.giH, this.ejA));
        this.giE.setCellRenderer(this.giG);
    }

    private void cnH() {
        TreePath treePath = new TreePath(this.giH);
        Iterator<C4033yO> it = cnJ().iterator();
        while (it.hasNext()) {
            C4033yO next = it.next();
            if (next instanceof DatabaseCategory) {
                m45115a(this.giH, (DatabaseCategory) next);
            }
        }
        this.giE.expandPath(treePath);
    }

    private void cnI() {
        this.ehH = new C4895l(0, 2);
        this.table.setModel(this.ehH);
        this.table.setSelectionModel(new C4892i());
    }

    private void buY() {
        this.table.getColumnModel().getColumn(0).setCellRenderer(new C4893j());
        this.table.getColumnModel().getColumn(1).setCellRenderer(new C4891h());
    }

    /* renamed from: a */
    private void m45115a(C6964axd axd, DatabaseCategory aik) {
        if (!aik.ccB() && !this.giF.containsKey(aik)) {
            C6964axd axd2 = new C6964axd(aik, this.ejA);
            this.giF.put(aik, axd2);
            axd.add(axd2);
        }
    }

    private ArrayList<C4033yO> cnJ() {
        ArrayList<C4033yO> arrayList = new ArrayList<>();
        for (C4033yO add : this.f10208P.dyl().dBQ().keySet()) {
            arrayList.add(add);
        }
        Collections.sort(arrayList, new C4890g());
        return arrayList;
    }

    public void cnK() {
        this.giB.clear();
        ArrayList<C4033yO> cnJ = cnJ();
        for (C6964axd removeAllChildren : this.giF.values()) {
            removeAllChildren.removeAllChildren();
        }
        Iterator<C4033yO> it = cnJ.iterator();
        while (it.hasNext()) {
            C4033yO next = it.next();
            if ((next instanceof C4068yr) && !(next instanceof DatabaseCategory)) {
                DatabaseCategory RY = ((C4068yr) next).mo184RY();
                if (RY != null) {
                    C6964axd axd = this.giF.get(RY);
                    C6964axd axd2 = new C6964axd(next, this.ejA);
                    if (axd != null) {
                        axd.add(axd2);
                    }
                    this.giB.put(next.mo195rP().get(), next);
                }
            } else if (!(next instanceof DatabaseCategory)) {
                this.f10209kj.getLog().error("Invalid PdaHolder in known objects.");
            }
        }
        this.giE.getModel().reload();
        this.giE.expandAll();
    }

    /* renamed from: Fa */
    private void m45112Fa() {
        this.giE.addTreeSelectionListener(new C4889f());
        this.searchField.addKeyListener(new C4886c());
        this.giw.addItemListener(new C4885b());
        this.giy.addActionListener(new C4888e());
    }

    /* renamed from: b */
    public void mo24466b(C4033yO yOVar) {
        mo24470jC(yOVar.mo195rP().get());
    }

    /* renamed from: jC */
    public void mo24470jC(String str) {
        C4033yO yOVar = this.giB.get(str);
        if (yOVar != null) {
            if (this.giD != null) {
                this.giC.push(this.giD);
                this.giy.setEnabled(true);
            }
            m45122c(yOVar);
            return;
        }
        this.f10209kj.getLog().error("Cannot show an entry not added in the Database.");
    }

    public void cnL() {
        if (this.giD != null) {
            C4033yO yOVar = this.giD;
            this.giD = null;
            m45122c(yOVar);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m45122c(C4033yO yOVar) {
        if (this.giD != yOVar) {
            this.f10208P.dyl().mo11963b(yOVar, false);
            this.giD = yOVar;
            if (yOVar instanceof C4068yr) {
                this.gix.setText(yOVar.mo182RU().get());
                m45123c((C4068yr) yOVar);
            }
            this.gix.getAccessibleContext().getAccessibleStateSet();
            m45127e(yOVar);
            DatabaseCategory RY = ((C4068yr) yOVar).mo184RY();
            if (RY == null) {
                RY = this.avy;
            }
            this.giE.getModel().reload(this.giF.get(RY));
            m45125d(yOVar);
        }
    }

    /* renamed from: d */
    private void m45125d(C4033yO yOVar) {
        if (yOVar instanceof DatabaseCategory) {
            this.giE.setSelectionPath(new TreePath(this.giF.get(yOVar).getPath()));
            return;
        }
        DatabaseCategory RY = ((C4068yr) yOVar).mo184RY();
        if (RY == null) {
            RY = this.avy;
        }
        C6964axd axd = this.giF.get(RY);
        Enumeration children = axd.children();
        C6964axd axd2 = null;
        while (axd2 == null && children.hasMoreElements()) {
            C6964axd axd3 = (C6964axd) children.nextElement();
            if (axd3.getUserObject() == yOVar) {
                axd2 = axd3;
            }
        }
        this.giE.setSelectionPath(new TreePath(axd.getPath()).pathByAddingChild(axd2));
    }

    /* renamed from: c */
    private void m45123c(C4068yr yrVar) {
        Object invoke;
        try {
            this.ehH.setRowCount(0);
            for (Method method : yrVar.getClass().getMethods()) {
                aFW afw = (aFW) method.getAnnotation(aFW.class);
                if (afw != null) {
                    if (afw.cZD()) {
                        invoke = method.invoke(yrVar, new Object[]{this.f10209kj.getPlayer()});
                    } else {
                        invoke = method.invoke(yrVar, new Object[0]);
                    }
                    if (invoke != null) {
                        this.ehH.addRow(new String[]{this.f10209kj.translate(afw.value()), m45118ap(invoke)});
                    }
                }
            }
            C5616aQe.m17578a(this.table, (Insets) null, true, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: ap */
    private String m45118ap(Object obj) {
        StringBuilder sb = new StringBuilder();
        if (obj instanceof Iterable) {
            boolean z = true;
            for (Object next : (Iterable) obj) {
                if (next != null) {
                    if (z) {
                        sb.append(m45118ap(next));
                        z = false;
                    } else {
                        sb.append(", " + m45118ap(next));
                    }
                }
            }
        } else if (obj instanceof C4068yr) {
            sb.append(((C4068yr) obj).mo195rP().get());
        } else if (obj instanceof NPC) {
            sb.append(((NPC) obj).getName());
        } else if (obj instanceof Ship) {
            sb.append(((Ship) obj).getName());
        } else if (obj instanceof ShipSectorType) {
            ShipSectorType aux = (ShipSectorType) obj;
            sb.append(aux.bmW().mo18438ke().get()).append(": ").append(aux.dAF()).append(" ");
        } else {
            sb.append(obj.toString());
        }
        return sb.toString();
    }

    private void cnM() {
        this.giz.getLayout().show(this.giz, "iconLabel");
    }

    private void cnN() {
        this.giz.getLayout().show(this.giz, "iconRender");
    }

    /* renamed from: e */
    private void m45127e(C4033yO yOVar) {
        if (!m45129f(yOVar)) {
            Image g = m45131g(yOVar);
            if (g != null) {
                if (!(g.getWidth((ImageObserver) null) == 100 && g.getHeight((ImageObserver) null) == 100)) {
                    g = m45120c(g);
                }
                this.iconLabel.setIcon(new ImageIcon(g));
            } else {
                this.iconLabel.setIcon((Icon) null);
            }
            cnM();
        }
    }

    /* renamed from: c */
    private Image m45120c(Image image) {
        BufferedImage bufferedImage = new BufferedImage(100, 100, 2);
        bufferedImage.getGraphics().drawImage(image, 0, 0, 100, 100, (ImageObserver) null);
        return bufferedImage;
    }

    /* renamed from: f */
    private boolean m45129f(C4033yO yOVar) {
        Asset Se = ((C4068yr) yOVar).mo187Se();
        if (Se == null) {
            return false;
        }
        C4887d dVar = new C4887d();
        this.f10209kj.getEngineGame().getLoaderTrail().mo4995a("data/scene/default_scene.pro", "default_scene", (Scene) null, new C4884a(), "PDA Icon");
        this.f10209kj.getEngineGame().getLoaderTrail().mo4995a(Se.getFile(), Se.getHandle(), (Scene) null, dVar, "PDA Icon");
        cnN();
        return true;
    }

    /* renamed from: g */
    private Image m45131g(C4033yO yOVar) {
        String str;
        String Sa = ((C4068yr) yOVar).mo185Sa();
        if (Sa == null) {
            return null;
        }
        try {
            if (yOVar instanceof NPCType) {
                str = C5378aHa.NPC_AVATAR.getPath();
            } else if (yOVar instanceof ItemType) {
                str = C5378aHa.ITEMS.getPath();
            } else {
                str = C5378aHa.ICONOGRAPHY.getPath();
            }
            try {
                return this.f10209kj.bHv().adz().getImage(String.valueOf(str) + Sa);
            } catch (IllegalArgumentException e) {
            }
        } catch (IllegalArgumentException e2) {
            str = null;
            this.f10209kj.getLog().warn("Could not load image " + str + Sa);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void cnO() {
        if (!this.giC.empty()) {
            m45122c(this.giC.pop());
            if (this.giC.empty()) {
                this.giy.setEnabled(false);
                return;
            }
            return;
        }
        this.f10209kj.getLog().error("There is no entry to go back.");
    }

    /* access modifiers changed from: private */
    public void cnP() {
        TreePath selectionPath = this.giE.getSelectionPath();
        this.giE.clearSelection();
        this.giE.getModel().reload();
        this.giE.expandAll();
        this.giE.setSelectionPath(selectionPath);
    }

    private void cnQ() {
        this.f10209kj.translate("Extraction itens");
        this.f10209kj.translate("Foundation Date");
        this.f10209kj.translate("Number of Affiliates");
        this.f10209kj.translate("Ranking");
        this.f10209kj.translate("Faction");
        this.f10209kj.translate("Occupation");
        this.f10209kj.translate("Stations");
        this.f10209kj.translate("Ship");
        this.f10209kj.translate("Armor");
        this.f10209kj.translate("Basic slots");
        this.f10209kj.translate("Cargo Hold capacity");
        this.f10209kj.translate("Hull");
        this.f10209kj.translate("Regeneration");
        this.f10209kj.translate("Shield");
        this.f10209kj.translate("Turrets");
        this.f10209kj.translate("UnitVolume");
        this.f10209kj.translate("Agents");
        this.f10209kj.translate("Faction");
        this.f10209kj.translate("Node");
        this.f10209kj.translate("Stellar System");
        this.f10209kj.translate("Nodes");
    }

    public String getTitle() {
        return this.f10209kj.translate("Data Base");
    }

    /* renamed from: Io */
    public String mo5290Io() {
        return f10207qw;
    }

    /* renamed from: a */
    public void mo5291a(C2698il ilVar) {
        this.aoL = ilVar;
        m45113Im();
    }

    public void hide() {
    }

    public void show() {
    }

    public void update() {
        cnK();
        cnL();
    }

    /* renamed from: taikodom.addon.pda.DataBase$m */
    /* compiled from: a */
    class C4896m implements HyperlinkListener {
        C4896m() {
        }

        public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
            if (HyperlinkEvent.EventType.ACTIVATED.equals(hyperlinkEvent.getEventType())) {
                DataBase.this.m45135jB(hyperlinkEvent.getDescription());
            }
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$k */
    /* compiled from: a */
    class C4894k extends C3428rT<C2055be> {
        C4894k() {
        }

        /* renamed from: b */
        public void mo321b(C2055be beVar) {
            C4033yO go = beVar.mo17395go();
            if (go instanceof C4068yr) {
                if (!DataBase.this.aoJ.isVisible()) {
                    DataBase.this.aoJ.bWg();
                }
                DataBase.this.update();
                DataBase.this.mo24466b(go);
                DataBase.this.aoJ.dvC().mo23272c(DataBase.this.aoK);
            }
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$l */
    /* compiled from: a */
    class C4895l extends DefaultTableModel {
        private static final long serialVersionUID = 4149970951641119335L;

        C4895l(int i, int i2) {
            super(i, i2);
        }

        public boolean isCellEditable(int i, int i2) {
            return false;
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$i */
    /* compiled from: a */
    class C4892i extends DefaultListSelectionModel {
        private static final long serialVersionUID = -1448814252640343354L;

        C4892i() {
        }

        public void addSelectionInterval(int i, int i2) {
        }

        public void setLeadSelectionIndex(int i) {
        }

        public void setSelectionInterval(int i, int i2) {
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$j */
    /* compiled from: a */
    class C4893j extends C2122cE {
        C4893j() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            Panel nR = aur.mo11631nR("itemKey");
            nR.mo4917cf("key").setText(String.valueOf(obj));
            return nR;
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$h */
    /* compiled from: a */
    class C4891h extends C2122cE {
        C4891h() {
        }

        /* renamed from: a */
        public Component mo17543a(aUR aur, JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            C3987xk xkVar = C3987xk.bFK;
            TextField nR = aur.mo11631nR("itemValue");
            try {
                nR.setText(xkVar.mo22980dY(Float.parseFloat(obj.toString())));
            } catch (NumberFormatException e) {
                nR.setText(String.valueOf(obj));
            }
            return nR;
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$g */
    /* compiled from: a */
    class C4890g implements Comparator<C4033yO> {
        C4890g() {
        }

        /* renamed from: a */
        public int compare(C4033yO yOVar, C4033yO yOVar2) {
            return yOVar.mo195rP().get().compareTo(yOVar2.mo195rP().get());
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$f */
    /* compiled from: a */
    class C4889f implements TreeSelectionListener {
        C4889f() {
        }

        public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
            if (DataBase.this.giE.getLastSelectedPathComponent() instanceof C6964axd) {
                C6964axd axd = (C6964axd) DataBase.this.giE.getLastSelectedPathComponent();
                if (axd.getUserObject() instanceof C4033yO) {
                    C4033yO yOVar = (C4033yO) axd.getUserObject();
                    if (!(DataBase.this.giD == null || yOVar == DataBase.this.giD)) {
                        DataBase.this.giC.push(DataBase.this.giD);
                        DataBase.this.giy.setEnabled(true);
                    }
                    DataBase.this.m45122c(yOVar);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$c */
    /* compiled from: a */
    class C4886c extends KeyAdapter {
        C4886c() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            DataBase.this.cnP();
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$b */
    /* compiled from: a */
    class C4885b implements ItemListener {
        C4885b() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            DataBase.this.cnP();
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$e */
    /* compiled from: a */
    class C4888e implements ActionListener {
        C4888e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            DataBase.this.giE.clearSelection();
            DataBase.this.cnO();
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$d */
    /* compiled from: a */
    class C4887d implements C0907NJ {
        C4887d() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            DataBase.this.giA.getSceneView().getSceneViewQuality().setShaderQuality(DataBase.this.f10209kj.ale().getShaderQuality());
            SceneObject sceneObject = (SceneObject) renderAsset;
            DataBase.this.giA.getScene().addChild(sceneObject);
            DataBase.this.giA.mo16902c(sceneObject, 30.0f);
        }
    }

    /* renamed from: taikodom.addon.pda.DataBase$a */
    class C4884a implements C0907NJ {
        C4884a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            DataBase.this.giA.getScene().removeAllChildren();
            DataBase.this.giA.getScene().addChild((SceneObject) renderAsset);
        }
    }
}
