package taikodom.addon.pda;

import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import logic.IAddonSettings;
import logic.aaa.C2055be;
import logic.aaa.C5685aSv;
import logic.baa.C4033yO;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.EditorPane;
import logic.ui.item.aUR;
import p001a.C1215Rv;
import p001a.C2456fb;
import p001a.C3428rT;
import p001a.C5517aMj;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.util.Stack;
import java.util.TreeMap;

@TaikodomAddon("taikodom.addon.pda")
/* compiled from: a */
public class Taikopedia implements C1215Rv, C2495fo {
    private static final String hPv = "taikopedia://";

    /* renamed from: qw */
    private static final String f10214qw = "taikopedia.xml";
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f10215P;
    /* access modifiers changed from: private */
    public PdaAddon aoJ;
    /* access modifiers changed from: private */
    public C5685aSv aoK;
    /* access modifiers changed from: private */
    public C4033yO giD;
    /* access modifiers changed from: private */
    public JCheckBox giw;
    /* access modifiers changed from: private */
    public TreeMap<String, C4033yO> hPA;
    /* access modifiers changed from: private */
    public JList hPw;
    /* access modifiers changed from: private */
    public C2456fb hPy;
    /* renamed from: kj */
    public IAddonProperties f10216kj;
    /* access modifiers changed from: private */
    public JTextField searchField;
    private C2698il aoL;
    private JButton giy;
    private Stack<String> hPB;
    /* access modifiers changed from: private */
    private DefaultListModel hPx;
    private EditorPane hPz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10216kj = addonPropertie;
        this.f10215P = this.f10216kj.getPlayer();
        this.aoJ = (PdaAddon) addonPropertie.mo11830U(PdaAddon.class);
        this.aoK = new C5685aSv("pda", this.f10216kj, this);
        this.f10216kj.aVU().mo13965a(C2055be.class, new C4905c());
        this.f10216kj.aVU().publish(this.aoK);
    }

    /* access modifiers changed from: private */
    /* renamed from: jB */
    public void m45188jB(String str) {
        if (str.startsWith(hPv)) {
            this.aoJ.dvC().mo23272c(this.aoK);
            mo24500jC(str.substring(hPv.length()));
        }
    }

    /* renamed from: Im */
    private void m45174Im() {
        this.hPA = new TreeMap<>();
        this.hPB = new Stack<>();
        this.giD = null;
        this.hPw = this.aoL.mo4915cd("titles");
        this.hPw.setCellRenderer(new C4910h(this, (C4910h) null));
        this.hPz = this.aoL.mo4915cd("descriptionArea");
        this.searchField = this.aoL.mo4915cd("searchField");
        this.giw = this.aoL.mo4915cd("newCheckBox");
        this.giy = this.aoL.mo4915cd("backButton");
        this.giy.setEnabled(false);
        dag();
        dah();
        addHyperlinkListener(new C4904b());
    }

    public void addHyperlinkListener(HyperlinkListener hyperlinkListener) {
        this.hPz.addHyperlinkListener(hyperlinkListener);
    }

    public void cnF() {
        this.hPB.clear();
        this.giy.setEnabled(false);
    }

    private void dag() {
        this.hPx = new DefaultListModel();
        this.hPy = new C4907e(this.hPx);
        this.hPw.setModel(this.hPy);
    }

    private void dah() {
        this.searchField.addKeyListener(new C4906d());
        this.giw.addItemListener(new C4909g());
        this.giy.addActionListener(new C4908f());
        this.hPw.addListSelectionListener(new C4903a());
    }

    /* access modifiers changed from: private */
    /* renamed from: mw */
    public void m45190mw(String str) {
        if (str == null) {
            return;
        }
        if (this.giD == null || !str.equals(this.giD.mo195rP().get())) {
            if (this.giw.isSelected()) {
                this.hPy.invalidate();
            }
            if (this.giD != null) {
                this.hPB.push(this.giD.mo195rP().get());
                this.giy.setEnabled(true);
            }
            m45180c(this.hPA.get(str));
        }
    }

    /* renamed from: b */
    public void mo24496b(C4033yO yOVar) {
        mo24500jC(yOVar.mo195rP().get());
    }

    /* renamed from: jC */
    public void mo24500jC(String str) {
        C4033yO yOVar = this.hPA.get(str);
        if (yOVar != null) {
            if (this.giD != null) {
                this.hPB.push(this.giD.mo195rP().get());
                this.giy.setEnabled(true);
            }
            m45180c(yOVar);
            return;
        }
        this.f10216kj.getLog().error("Cannot show an entry not added in the Taikopedia.");
    }

    public void cnL() {
        if (this.giD != null) {
            m45180c(this.giD);
        }
    }

    /* renamed from: c */
    private void m45180c(C4033yO yOVar) {
        if (yOVar != null) {
            this.f10215P.dyl().mo11968d(yOVar, false);
            this.hPz.setText(yOVar.mo182RU().get());
            this.giD = yOVar;
            this.hPw.clearSelection();
            return;
        }
        this.f10216kj.getLog().error("Invalid taikopedia entry.");
    }

    /* access modifiers changed from: private */
    public void cnO() {
        if (!this.hPB.empty()) {
            m45180c(this.hPA.get(this.hPB.pop()));
            if (this.hPB.empty()) {
                this.giy.setEnabled(false);
                return;
            }
            return;
        }
        this.f10216kj.getLog().error("There is no entry to go back.");
    }

    public void dai() {
        this.hPA.clear();
        for (C4033yO next : this.f10215P.dyl().dBS().keySet()) {
            if (next instanceof TaikopediaEntry) {
                this.hPA.put(next.mo195rP().get(), next);
            } else {
                this.f10216kj.getLog().error("Invalid PdaHolder in known taikopedia.");
            }
        }
        this.hPx.clear();
        for (String addElement : this.hPA.keySet()) {
            this.hPx.addElement(addElement);
        }
    }

    public void stop() {
        this.f10216kj.aVU().mo13974g(this.aoK);
    }

    public String getTitle() {
        return this.f10216kj.translate("Taikopedia");
    }

    /* renamed from: Io */
    public String mo5290Io() {
        return f10214qw;
    }

    /* renamed from: a */
    public void mo5291a(C2698il ilVar) {
        this.aoL = ilVar;
        m45174Im();
    }

    public void hide() {
    }

    public void show() {
    }

    public void update() {
        dai();
        cnL();
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$c */
    /* compiled from: a */
    class C4905c extends C3428rT<C2055be> {
        C4905c() {
        }

        /* renamed from: b */
        public void mo321b(C2055be beVar) {
            C4033yO go = beVar.mo17395go();
            if (go instanceof TaikopediaEntry) {
                if (!Taikopedia.this.aoJ.isVisible()) {
                    Taikopedia.this.aoJ.bWg();
                }
                Taikopedia.this.update();
                Taikopedia.this.mo24496b(go);
                Taikopedia.this.aoJ.dvC().mo23272c(Taikopedia.this.aoK);
            }
        }
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$b */
    /* compiled from: a */
    class C4904b implements HyperlinkListener {
        C4904b() {
        }

        public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
            if (HyperlinkEvent.EventType.ACTIVATED.equals(hyperlinkEvent.getEventType())) {
                Taikopedia.this.m45188jB(hyperlinkEvent.getDescription());
            }
        }
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$e */
    /* compiled from: a */
    class C4907e extends C2456fb {
        C4907e(ListModel listModel) {
            super(listModel);
        }

        /* access modifiers changed from: protected */
        /* renamed from: D */
        public boolean mo18734D(Object obj) {
            C4033yO yOVar = (C4033yO) Taikopedia.this.hPA.get(String.valueOf(obj));
            if (!Taikopedia.this.giw.isSelected() || Taikopedia.this.f10215P.dyl().dBS().get(yOVar).booleanValue()) {
                return yOVar.mo195rP().get().indexOf(Taikopedia.this.searchField.getText()) != -1;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$d */
    /* compiled from: a */
    class C4906d extends KeyAdapter {
        C4906d() {
        }

        public void keyReleased(KeyEvent keyEvent) {
            Taikopedia.this.hPy.invalidate();
            Taikopedia.this.hPw.repaint();
        }
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$g */
    /* compiled from: a */
    class C4909g implements ItemListener {
        C4909g() {
        }

        public void itemStateChanged(ItemEvent itemEvent) {
            Taikopedia.this.hPy.invalidate();
            Taikopedia.this.hPw.repaint();
        }
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$f */
    /* compiled from: a */
    class C4908f implements ActionListener {
        C4908f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            Taikopedia.this.cnO();
        }
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$a */
    class C4903a implements ListSelectionListener {
        C4903a() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            Taikopedia.this.m45190mw((String) Taikopedia.this.hPw.getSelectedValue());
        }
    }

    /* renamed from: taikodom.addon.pda.Taikopedia$h */
    /* compiled from: a */
    private class C4910h extends C5517aMj {
        private C4910h() {
        }

        /* synthetic */ C4910h(Taikopedia taikopedia, C4910h hVar) {
            this();
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            Panel dAx = aur.dAx();
            JLabel cf = dAx.mo4917cf("titleLabel");
            cf.setText(String.valueOf(obj));
            C4033yO yOVar = (C4033yO) Taikopedia.this.hPA.get(String.valueOf(obj));
            if (yOVar == null) {
                Taikopedia.this.f10216kj.getLog().error("Null TaikopediaEntry.");
            } else if (Taikopedia.this.f10215P.dyl().dBS().get(yOVar).booleanValue()) {
                cf.setText("(" + Taikopedia.this.f10216kj.translate("New") + ") " + String.valueOf(obj));
            }
            if (yOVar == Taikopedia.this.giD && !z) {
                Taikopedia.this.hPw.setSelectedValue(obj, false);
            }
            return dAx;
        }
    }
}
