package taikodom.addon.flashplayer;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.network.message.externalizable.C5344aFs;
import logic.IAddonSettings;
import logic.aaa.C5783aaP;
import logic.res.sound.C0907NJ;
import logic.ui.ComponentManager;
import logic.ui.Panel;
import logic.ui.item.InternalFrame;
import p001a.C2602hR;
import p001a.C3428rT;
import p001a.C6622aqW;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.render.TexBackedImage;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.C2781ju;
import taikodom.render.scene.Scene;
import taikodom.render.textures.FlashTexture;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.IOException;

@TaikodomAddon("taikodom.addon.flashplayer")
/* compiled from: a */
public class FlashPlayerAddon implements C2495fo {
    /* access modifiers changed from: private */
    public static final String[] fuY = {"cena08_final.swf", "cena05 _final.swf", "cena10_final.swf", "super_banner.swf", "allyourbase.swf"};
    private static final float fuW = 2.0f;
    private static final String fuX = "data/movies/";
    /* access modifiers changed from: private */
    public Panel dub;
    /* access modifiers changed from: private */
    public FlashTexture fuZ;
    /* access modifiers changed from: private */
    public JLabel fve;
    /* access modifiers changed from: private */
    public Timer fvg;
    /* access modifiers changed from: private */
    public int fvh = 0;
    /* renamed from: kj */
    public IAddonProperties f9872kj;
    /* renamed from: nM */
    public InternalFrame f9873nM;
    private C3428rT<C5783aaP> aoN;
    private C0907NJ duc;
    private Action fva;
    private JButton fvb;
    private JButton fvc;
    private JButton fvd;
    /* access modifiers changed from: private */
    private JButton fvf;
    /* access modifiers changed from: private */
    private JButton fvi;
    /* renamed from: wz */
    private C3428rT<C3689to> f9874wz;

    /* renamed from: iM */
    private void m43412iM() {
        this.dub = (Panel) this.f9873nM.mo4916ce("movie");
        this.fve = this.f9873nM.mo4917cf("status");
        this.fvc = this.f9873nM.mo4913cb("play");
        this.fvb = this.f9873nM.mo4913cb("pause");
        this.fvf = this.f9873nM.mo4913cb("stop");
        this.fvd = this.f9873nM.mo4913cb("rewind");
        this.fvi = this.f9873nM.mo4913cb("next");
    }

    /* renamed from: iG */
    private void m43409iG() {
        this.duc = new C4365a();
        this.fvc.addActionListener(new C4366b());
        this.fvb.addActionListener(new C4367c());
        this.fvf.addActionListener(new C4368d());
        this.fvd.addActionListener(new C4369e());
        this.fvi.addActionListener(new C4370f());
    }

    /* renamed from: Im */
    private void m43398Im() {
        this.f9873nM = this.f9872kj.bHv().mo16792bN("flash-player.xml");
        this.f9873nM.addComponentListener(new C4371g());
        m43412iM();
        m43409iG();
        try {
            m43410iG(fuY[this.fvh]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: iG */
    public void m43410iG(String str) {
        this.f9872kj.alg().mo4995a(fuX + str, fuX + str, (Scene) null, this.duc, getClass().getSimpleName());
    }

    public void open() {
        if (m43411iL()) {
            this.f9872kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, "You must set TaikodomDebug.SKIP_NURSERY_TUTORIAL to false if you want to use FlashPlayerAddon.", new Object[0]));
            return;
        }
        if (this.f9873nM == null) {
            m43398Im();
        } else if (this.f9873nM.isVisible()) {
            this.fvg.stop();
            this.f9873nM.setVisible(false);
            this.f9872kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
            return;
        }
        this.f9872kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.OPENED, getClass()));
        this.f9873nM.setVisible(true);
    }

    /* renamed from: iL */
    private boolean m43411iL() {
        return this.f9872kj != null && this.f9872kj.ala().aLS().mo22273iL();
    }

    @C2602hR(mo19255zf = "FLASH_PLAYER_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo23811al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: qz */
    public void mo23813qz(int i) {
        if (this.fvg != null) {
            this.fvg.stop();
        }
        this.fvg = new Timer(i, new C4372h());
        this.fvg.start();
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9872kj = addonPropertie;
        this.fva = new C4373i("flash-player", this.f9872kj.translate("Flash Player"), "flashplayer");
        this.f9874wz = new C4375k();
        this.aoN = new C4374j();
        this.f9872kj.mo2322a("flashplayer", this.fva);
        this.f9872kj.aVU().publish(new C5344aFs(this.fva, 808, false));
        this.f9872kj.aVU().mo13965a(C5783aaP.class, this.aoN);
        this.f9872kj.aVU().mo13965a(C3689to.class, this.f9874wz);
    }

    public void stop() {
        if (this.fvg != null) {
            this.fvg.stop();
            this.fvg = null;
        }
        if (this.f9873nM != null) {
            this.f9873nM.destroy();
            this.f9873nM = null;
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$a */
    class C4365a implements C0907NJ {
        C4365a() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset != null) {
                FlashPlayerAddon.this.fuZ = (FlashTexture) renderAsset;
                C2781ju SC = FlashPlayerAddon.this.fuZ.getPlayer().mo21035SC();
                FlashPlayerAddon.this.fve.setText(FlashPlayerAddon.this.fuZ.getName());
                ComponentManager.getCssHolder(FlashPlayerAddon.this.dub).mo13049Vr().mo369a((Image) new TexBackedImage(FlashPlayerAddon.this.dub.getHeight(), FlashPlayerAddon.this.dub.getWidth(), FlashPlayerAddon.this.fuZ.getScaleX(), FlashPlayerAddon.this.fuZ.getScaleY(), FlashPlayerAddon.this.fuZ));
                FlashPlayerAddon.this.mo23813qz(Math.max((int) ((1000.0f / SC.mo19990Fr()) / 2.0f), 10));
                FlashPlayerAddon.this.fvg.start();
            }
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$b */
    /* compiled from: a */
    class C4366b implements ActionListener {
        C4366b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FlashPlayerAddon.this.fuZ.getPlayer().play();
            FlashPlayerAddon.this.fvg.restart();
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$c */
    /* compiled from: a */
    class C4367c implements ActionListener {
        C4367c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FlashPlayerAddon.this.fuZ.getPlayer().stop();
            FlashPlayerAddon.this.fvg.stop();
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$d */
    /* compiled from: a */
    class C4368d implements ActionListener {
        C4368d() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FlashPlayerAddon.this.fuZ.getPlayer().stop();
            FlashPlayerAddon.this.fuZ.getPlayer().mo21048dv(0);
            FlashPlayerAddon.this.fvg.stop();
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$e */
    /* compiled from: a */
    class C4369e implements ActionListener {
        C4369e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FlashPlayerAddon.this.fuZ.getPlayer().mo21048dv(0);
            FlashPlayerAddon.this.fvg.restart();
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$f */
    /* compiled from: a */
    class C4370f implements ActionListener {
        C4370f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!(FlashPlayerAddon.this.fuZ == null || FlashPlayerAddon.this.fuZ.getPlayer() == null)) {
                FlashPlayerAddon.this.fuZ.getPlayer().stop();
            }
            FlashPlayerAddon.this.fvh = (FlashPlayerAddon.this.fvh + 1) % FlashPlayerAddon.fuY.length;
            try {
                FlashPlayerAddon.this.m43410iG(FlashPlayerAddon.fuY[FlashPlayerAddon.this.fvh]);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FlashPlayerAddon.this.fvg.restart();
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$g */
    /* compiled from: a */
    class C4371g extends ComponentAdapter {
        C4371g() {
        }

        public void componentHidden(ComponentEvent componentEvent) {
            FlashPlayerAddon.this.f9872kj.aVU().mo13975h(new C1532WW(C1532WW.C1533a.CLOSED, getClass()));
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$h */
    /* compiled from: a */
    class C4372h implements ActionListener {
        C4372h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FlashPlayerAddon.this.dub.repaint();
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$i */
    /* compiled from: a */
    class C4373i extends C6622aqW {
        private static final long serialVersionUID = 3380475231526774845L;

        C4373i(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            FlashPlayerAddon.this.open();
        }

        public boolean isActive() {
            return FlashPlayerAddon.this.f9873nM != null && FlashPlayerAddon.this.f9873nM.isVisible();
        }

        public boolean isEnabled() {
            return true;
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$k */
    /* compiled from: a */
    class C4375k extends C3428rT<C3689to> {
        C4375k() {
        }

        /* renamed from: a */
        public void mo321b(C3689to toVar) {
            if (FlashPlayerAddon.this.f9873nM != null && FlashPlayerAddon.this.f9873nM.isVisible()) {
                toVar.adC();
                FlashPlayerAddon.this.f9873nM.setVisible(false);
            }
        }
    }

    /* renamed from: taikodom.addon.flashplayer.FlashPlayerAddon$j */
    /* compiled from: a */
    class C4374j extends C3428rT<C5783aaP> {
        C4374j() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            if (aap.bMO() == C5783aaP.C1841a.ONI && FlashPlayerAddon.this.f9873nM != null && FlashPlayerAddon.this.f9873nM.isVisible()) {
                FlashPlayerAddon.this.f9873nM.setVisible(false);
            }
        }
    }
}
