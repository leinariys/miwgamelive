package taikodom.addon.connect;

import game.network.exception.*;
import game.network.message.externalizable.aUU;
import game.script.player.User;
import logic.EngineGame;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aaa.C1169RJ;
import logic.aaa.C2667iJ;
import logic.aaa.C2682iV;
import logic.aaa.C5211aAp;
import logic.swing.C5378aHa;
import logic.thred.LogPrinter;
import logic.ui.item.InternalFrame;
import logic.ui.item.Progress;
import logic.ui.item.TextArea;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.game.main.ClientMain;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

@TaikodomAddon("taikodom.addon.connect")
/* compiled from: a */
public class ConnectAddon implements C2495fo {
    /* access modifiers changed from: private */
    public static final Log log = LogPrinter.setClass(ConnectAddon.class);
    /* access modifiers changed from: private */

    /* renamed from: IU */
    public C3987xk f9827IU;
    /* access modifiers changed from: private */
    public Progress progressBar;
    /* access modifiers changed from: private */
    public String userName;
    /* access modifiers changed from: private */
    public String password;
    /* access modifiers changed from: private */
    public JButton eWP;
    /* access modifiers changed from: private */
    public LabelLoadingRunnable loadingRunnable = new LabelLoadingRunnable(this, (LabelLoadingRunnable) null);
    /* access modifiers changed from: private */
    public JLabel labelLoading;
    /* renamed from: kj */
    public IAddonProperties properties;
    /* renamed from: nM */
    public InternalFrame f9829nM = null;
    /* renamed from: qk */
    public JLabel f9830qk;
    /**
     * Происходит ли сейчас подключение к серверу
     */
    boolean isRunnableConnectToServer = false;
    private WrapRunnable aov = new C4289c(this, (C4289c) null);
    /* access modifiers changed from: private */
    private TextArea eWN;
    /* access modifiers changed from: private */
    private JButton eWO;
    /* access modifiers changed from: private */
    private C4300m eWQ;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.properties = (IAddonProperties) addonPropertie;
        this.f9827IU = C3987xk.bFK;
        addonPropertie.aVU().mo13965a(StorageLoginPass.class, new C4299l());
        addonPropertie.aVU().mo13965a(C3932wv.class, new C4298k(addonPropertie));
        addonPropertie.aVU().mo13965a(aUU.class, new C4297j());
    }

    /* access modifiers changed from: private */
    public void bMt() {
        m43131Ik();
        bMG();
        User bhe = this.properties.getEngineGame().getUser();
        String cTt = User.cTt();
        String cTu = User.cTu();
        m43130Ia();
        if (!cTt.equals(bhe.cTw())) {
            this.properties.aVU().mo13975h(new C2682iV(new C4295i(bhe, cTt, cTu)));
        } else if (!cTu.equals(bhe.cTy())) {
            this.properties.aVU().mo13975h(new C2667iJ(new C4294h(bhe, cTu)));
        } else {
            this.properties.aVU().mo13975h(new aUU(2));
        }
    }

    public void stop() {
        if (this.f9829nM != null) {
            this.f9829nM.destroy();
        }
        m43131Ik();
        bMG();
    }

    /* renamed from: Im */
    private void m43132Im() {
        if (this.f9829nM == null) {
            this.f9829nM = this.properties.bHv().mo16792bN("connect.xml");
            this.f9829nM.pack();
            this.f9829nM.center();
            this.eWQ = new C4300m(this.properties.bHv().adz().getImage(String.valueOf(C5378aHa.LOGIN.getPath()) + "loading_icon"));
            this.f9830qk = this.f9829nM.mo4917cf("image");
            this.f9830qk.setIcon(this.eWQ);
            this.eWN = this.f9829nM.mo4915cd("message");
            this.labelLoading = this.f9829nM.mo4915cd("netinfo");
            this.eWO = this.f9829nM.mo4913cb("buttonL");
            this.eWO.setVisible(false);
            this.eWP = this.f9829nM.mo4913cb("buttonR");
            this.eWP.setVisible(false);
            this.progressBar = this.f9829nM.mo4915cd("progress");
            this.progressBar.mo17403a(new C4293g());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: xK */
    public void m43162xK() {
        if (this.f9829nM == null) {
            m43132Im();
        }
        this.f9829nM.setVisible(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: Ia */
    public void m43130Ia() {
        if (this.f9829nM != null) {
            this.f9829nM.setVisible(false);
            m43134a(this.eWO);
            m43134a(this.eWP);
        }
    }

    /* renamed from: a */
    private void m43135a(JButton jButton, String str) {
        jButton.setText(str);
        jButton.setVisible(true);
        jButton.setEnabled(true);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43134a(JButton jButton) {
        jButton.setVisible(false);
        jButton.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public void setMessage(String str) {
        this.eWN.setText(str);
        this.eWN.repaint(50);
    }

    /* renamed from: ia */
    private void showMessageError(String str) {
        setMessage(str);
        m43131Ik();
        bMG();
        JButton jButton = this.eWP;
        m43134a(this.eWO);
        jButton.addActionListener(new C4292f(jButton));
        m43135a(jButton, "OK");
        jButton.requestFocus();
        this.properties.aVU().mo13975h(new C6811auD(C3667tV.MESS_ERROR));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m43139b(String userName, String password, boolean isForcingNewUser) {
        if (this.isRunnableConnectToServer) {
            messageConnectionAlready();
            return;
        }
        bME();
        bMF();
        this.userName = userName;
        this.password = password;
        setMessage(this.properties.translate("Connection started"));
        this.properties.aVU().mo13975h(new C3932wv(10));
        new Thread(new ConnectRun(userName, password, isForcingNewUser)).start();
    }

    /* access modifiers changed from: protected */
    /* renamed from: pA */
    public void mo23706pA(int i) {
        this.isRunnableConnectToServer = false;
        stop();
        this.properties.aVU().mo13975h(new aUU(1));
        this.properties.aVU().mo13975h(new aAS(i));
    }

    /* access modifiers changed from: private */
    public void messageProblemsConnection() {
        this.isRunnableConnectToServer = false;
        showMessageError(this.properties.translate("Problems with connection."));
    }

    /* access modifiers changed from: private */
    public void messageIncorrectLoginPass() {
        this.isRunnableConnectToServer = false;
        showMessageError(this.properties.translate("Username and/or password incorrect."));
    }

    /* access modifiers changed from: private */
    public void bMw() {
        this.isRunnableConnectToServer = false;
        m43131Ik();
        bMG();
        setMessage(this.properties.translate("There is already an active connection. Do you want to close it?"));
        JButton jButton = this.eWO;
        JButton jButton2 = this.eWP;
        m43135a(jButton, this.properties.translate("yes"));
        m43135a(jButton2, this.properties.translate("no"));
        jButton2.requestFocus();
        jButton.addActionListener(new C4290d(jButton, jButton2));
        jButton2.addActionListener(new C4287a(jButton, jButton2));
    }

    /* access modifiers changed from: private */
    public void bMx() {
        this.isRunnableConnectToServer = false;
        this.properties.aVU().mo13975h(new aUU(1));
        this.properties.aVU().mo13975h(new C5211aAp());
        m43130Ia();
    }

    /* access modifiers changed from: private */
    public void messageAccountIsCanceled() {
        this.isRunnableConnectToServer = false;
        showMessageError(this.properties.translate("Account is canceled. Go to our support if you don't know the reason."));
    }

    /* access modifiers changed from: private */
    public void messageSuspendedAccount() {
        this.isRunnableConnectToServer = false;
        showMessageError(this.properties.translate("Suspended account. Go to our support if you don't know the reason."));
    }

    /* access modifiers changed from: private */
    public void messageCreditExpired() {
        this.isRunnableConnectToServer = false;
        showMessageError(this.properties.translate("Your Taikodom credit has expired. Please visit our billing section to buy more."));
    }

    /* access modifiers changed from: private */
    public void messageIncompatibleVersion() {
        this.isRunnableConnectToServer = false;
        showMessageError(this.properties.translate("Incompatible version. Update your game."));
    }

    private void messageConnectionAlready() {
        showMessageError(this.properties.translate("Connection already started."));
    }

    /* access modifiers changed from: private */
    public void connectSuccessLog() {
        this.isRunnableConnectToServer = false;
        log.info("connect finished - user " + this.properties.getEngineGame().getUser().getName());
    }

    private void bME() {
        this.eWQ.start();
        this.properties.alf().addTask("UpdateTargetPositiontask", this.aov, 10);
    }

    /* renamed from: Ik */
    private void m43131Ik() {
        this.properties.alf().mo7963a(this.aov);
        if (this.eWQ != null) {
            this.eWQ.stop();
        }
        if (this.f9830qk != null) {
            this.f9830qk.repaint();
        }
    }

    private void bMF() {
        this.progressBar.setVisible(true);
        this.properties.alf().addTask("UpdateProgressTask", this.loadingRunnable, 50);
    }

    private void bMG() {
        this.properties.alf().mo7963a(this.loadingRunnable);
        if (this.progressBar != null) {
            this.progressBar.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$l */
    /* compiled from: a */
    class C4299l extends C3428rT<StorageLoginPass> {
        C4299l() {
        }

        /* renamed from: a */
        public void mo321b(StorageLoginPass loginPass) {
            ConnectAddon.this.m43162xK();
            ConnectAddon.this.m43139b(loginPass.getUserName(), loginPass.getPassword(), loginPass.getForcingNewUser());
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$k */
    /* compiled from: a */
    class C4298k extends C3428rT<C3932wv> {
        private final /* synthetic */ IAddonProperties addonProperties;

        C4298k(IAddonProperties vWVar) {
            this.addonProperties = vWVar;
        }

        /* renamed from: a */
        public void mo321b(C3932wv wvVar) {
            switch (wvVar.codeStepLoad()) {
                case 11:
                    ConnectAddon.this.setMessage(this.addonProperties.translate("Connected to server..."));
                    this.addonProperties.aVU().mo13975h(new C6811auD(C3667tV.LOGIN_START));
                    ConnectAddon.this.loadingRunnable.setValue(5.0f);
                    return;
                case 12:
                    ConnectAddon.this.setMessage(this.addonProperties.translate("Starting game..."));
                    ConnectAddon.this.loadingRunnable.setValue(10.0f);
                    return;
                case 13:
                    ConnectAddon.this.setMessage(this.addonProperties.translate("Game loaded"));
                    ConnectAddon.this.loadingRunnable.addOn(10.0f);
                    return;
                case 14:
                    ConnectAddon.this.setMessage(this.addonProperties.translate("Loading game..."));
                    ConnectAddon.this.loadingRunnable.addOn(10.0f);
                    return;
                case 20:
                    ConnectAddon.this.setMessage(this.addonProperties.translate("Data received"));
                    ConnectAddon.this.loadingRunnable.done();
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$j */
    /* compiled from: a */
    class C4297j extends C3428rT<aUU> {
        C4297j() {
        }

        /* renamed from: a */
        public void mo321b(aUU auu) {
            switch (auu.dAD()) {
                case 5:
                    if (ConnectAddon.this.f9829nM != null && !ConnectAddon.this.f9829nM.isVisible()) {
                        ConnectAddon.this.f9829nM.destroy();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$i */
    /* compiled from: a */
    class C4295i implements C6144ahM<Boolean> {
        private final /* synthetic */ String gEA;
        private final /* synthetic */ User gEy;
        private final /* synthetic */ String gEz;

        C4295i(User adk, String str, String str2) {
            this.gEy = adk;
            this.gEA = str;
            this.gEz = str2;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                this.gEy.mo8481lR(this.gEA);
                if (!this.gEz.equals(this.gEy.cTy())) {
                    ConnectAddon.this.properties.aVU().mo13975h(new C2667iJ(new C4296a(this.gEy, this.gEz)));
                }
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            ConnectAddon.log.error("EULA error", th);
        }

        /* renamed from: taikodom.addon.connect.ConnectAddon$i$a */
        class C4296a implements C6144ahM<Boolean> {
            private final /* synthetic */ User gEy;
            private final /* synthetic */ String gEz;

            C4296a(User adk, String str) {
                this.gEy = adk;
                this.gEz = str;
            }

            /* renamed from: a */
            public void mo1931n(Boolean bool) {
                if (bool.booleanValue()) {
                    this.gEy.mo8482lT(this.gEz);
                    ConnectAddon.this.properties.aVU().mo13975h(new aUU(2));
                }
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
                ConnectAddon.log.error("ToU error", th);
            }
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$h */
    /* compiled from: a */
    class C4294h implements C6144ahM<Boolean> {
        private final /* synthetic */ User gEy;
        private final /* synthetic */ String gEz;

        C4294h(User adk, String str) {
            this.gEy = adk;
            this.gEz = str;
        }

        /* renamed from: a */
        public void mo1931n(Boolean bool) {
            if (bool.booleanValue()) {
                this.gEy.mo8482lT(this.gEz);
                ConnectAddon.this.properties.aVU().mo13975h(new aUU(2));
            }
            ConnectAddon.this.m43130Ia();
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            ConnectAddon.log.error("ToU error", th);
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$g */
    /* compiled from: a */
    class C4293g implements Progress.C2065a {
        C4293g() {
        }

        public float getMaximum() {
            return 100.0f;
        }

        public float getMinimum() {
            return 0.0f;
        }

        public String getText() {
            return String.valueOf(String.valueOf(ConnectAddon.this.f9827IU.mo22980dY(ConnectAddon.this.loadingRunnable.getValue()))) + " %";
        }

        public float getValue() {
            return ConnectAddon.this.loadingRunnable.getValue();
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$f */
    /* compiled from: a */
    class C4292f implements ActionListener {
        private final /* synthetic */ JButton gEm;

        C4292f(JButton jButton) {
            this.gEm = jButton;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ConnectAddon.this.m43130Ia();
            ConnectAddon.this.m43134a(this.gEm);
            ConnectAddon.this.properties.aVU().mo13975h(new aUU(1));
            ConnectAddon.this.eWP.removeActionListener(this);
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$e */
    /* compiled from: a */
    class ConnectRun implements Runnable {
        private final /* synthetic */ String userName;
        private final /* synthetic */ String password;
        private final /* synthetic */ boolean isForcingNewUser;

        ConnectRun(String userName, String password, boolean isForcingNewUser) {
            this.userName = userName;
            this.password = password;
            this.isForcingNewUser = isForcingNewUser;
        }

        public void run() {
            try {
                ConnectAddon.this.isRunnableConnectToServer = true;
                ClientMain.getClientMain().connectToServer(this.userName, this.password, this.isForcingNewUser);
                ConnectAddon.this.connectSuccessLog();
            } catch (RuntimeException e) {
                ConnectAddon.log.error("Login exception happened", e);
                ConnectAddon.this.properties.aVU().mo13975h(new C3932wv(1));
                ConnectAddon.this.messageProblemsConnection();
            } catch (InvalidUsernamePasswordException e2) {
                ConnectAddon.this.properties.aVU().mo13975h(new C3932wv(2));
                ConnectAddon.this.messageIncorrectLoginPass();
            } catch (ClientAlreadyConnectedException e3) {
                if (!this.isForcingNewUser) {
                    ConnectAddon.this.bMw();
                } else {
                    ConnectAddon.log.error("Login returned 'already logged in' even after forcing a new user session");
                }
                ConnectAddon.this.properties.aVU().mo13975h(new C3932wv(3));
            } catch (ServerLoginException e4) {
                ConnectAddon.this.properties.aVU().mo13975h(new C3932wv(1));
                ConnectAddon.this.messageProblemsConnection();
            } catch (IOException e5) {
                ConnectAddon.this.properties.aVU().mo13975h(new C3932wv(1));
                ConnectAddon.this.messageProblemsConnection();
            } catch (UserAccountNotActivatedException e6) {
                ConnectAddon.this.bMx();
            } catch (UserBannedException e7) {
                ConnectAddon.this.messageSuspendedAccount();
            } catch (InvalidHandshakeException e8) {
                ConnectAddon.this.messageIncompatibleVersion();
            } catch (InterruptedException e9) {
                e9.printStackTrace();
            } catch (HasNotCreditsException e10) {
                ConnectAddon.this.messageCreditExpired();
            } catch (UserDeletedException e11) {
                ConnectAddon.this.messageAccountIsCanceled();
            } catch (ServerFullException e12) {
                ConnectAddon.this.mo23706pA(e12.cop());
            } finally {
                ConnectAddon.this.isRunnableConnectToServer = false;
            }
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$d */
    /* compiled from: a */
    class C4290d implements ActionListener {
        private final /* synthetic */ JButton dgR;
        private final /* synthetic */ JButton dgS;

        C4290d(JButton jButton, JButton jButton2) {
            this.dgR = jButton;
            this.dgS = jButton2;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ConnectAddon.this.m43134a(this.dgR);
            ConnectAddon.this.m43134a(this.dgS);
            ConnectAddon.this.properties.getEngineGame().disconnect();
            ConnectAddon.this.m43139b(ConnectAddon.this.userName, ConnectAddon.this.password, true);
            this.dgR.removeActionListener(this);
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$a */
    class C4287a implements ActionListener {
        private final /* synthetic */ JButton dgR;
        private final /* synthetic */ JButton dgS;

        C4287a(JButton jButton, JButton jButton2) {
            this.dgR = jButton;
            this.dgS = jButton2;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ConnectAddon.this.m43134a(this.dgR);
            ConnectAddon.this.m43134a(this.dgS);
            ConnectAddon.this.m43130Ia();
            ConnectAddon.this.properties.aVU().mo13975h(new aUU(1));
            this.dgS.removeActionListener(this);
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$c */
    /* compiled from: a */
    private class C4289c extends WrapRunnable {
        private C4289c() {
        }

        /* synthetic */ C4289c(ConnectAddon connectAddon, C4289c cVar) {
            this();
        }

        public void run() {
            ConnectAddon.this.f9830qk.repaint();
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$m */
    /* compiled from: a */
    public class C4300m extends ImageIcon {
        private static final int BORDER = 6;


        /* renamed from: cW */
        private boolean f9831cW;

        public C4300m(Image image) {
            super(image);
        }

        public int getIconHeight() {
            return ConnectAddon.super.getIconHeight() + 6;
        }

        public int getIconWidth() {
            return ConnectAddon.super.getIconWidth() + 6;
        }

        public void start() {
            this.f9831cW = true;
        }

        public void stop() {
            this.f9831cW = false;
        }

        public synchronized void paintIcon(Component component, Graphics graphics, int i, int i2) {
            Graphics2D create = graphics.create();
            create.translate(3, 3);
            if (this.f9831cW) {
                create.rotate((((double) ((System.currentTimeMillis() / 10) % 360)) * 3.141592653589793d) / 180.0d, (double) ((getImage().getWidth(component) / 2) + i), (double) ((getImage().getHeight(component) / 2) + i2));
            } else {
                create.rotate(ScriptRuntime.NaN);
            }
            create.drawImage(getImage(), i, i2, component);
        }
    }

    /* renamed from: taikodom.addon.connect.ConnectAddon$b */
    /* compiled from: a */
    private class LabelLoadingRunnable extends WrapRunnable {
        private float estimatedValue;
        private float value;
        private transient int dWd;
        private boolean dWe;

        public LabelLoadingRunnable(ConnectAddon connectAddon, LabelLoadingRunnable labelLoadingRunnable) {
            this.estimatedValue = 0.0f;
            this.value = 0.0f;
            this.dWd = 0;
            this.dWe = false;
        }


        public void run() {
            if (this.value + 1.0f <= this.estimatedValue) {
                this.value += 1.0f;
            }
            if (this.value == this.estimatedValue && this.estimatedValue == 100.0f) {
                this.dWd++;
                if (this.dWd == 8) {
                    ConnectAddon.this.bMt();
                }
            }
            EngineGame engineGame = (EngineGame) ConnectAddon.this.properties.getEngineGame();
            if (engineGame.getNetworkChannel() == null) {
                ConnectAddon.this.labelLoading.setText("");
            } else if (engineGame.getNetworkChannel().isUp()) {
                ConnectAddon.this.labelLoading.setText(String.format("%,d bytes", engineGame.getNetworkChannel().metricReadByte()));
            } else if (!this.dWe) {
                ConnectAddon.this.properties.aVU().mo13975h(new C1169RJ());
                this.dWe = true;
            }
            ConnectAddon.this.progressBar.repaint(50);
        }

        /* access modifiers changed from: package-private */
        public float getValue() {
            return this.value;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: hp */
        public void setValue(float value) {
            if (value > this.estimatedValue && value <= 100.0f) {
                this.estimatedValue = value;
            }
        }

        /* access modifiers changed from: package-private */
        public void done() {
            setValue(100.0f);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: hq */
        public void addOn(float value) {
            setValue(this.estimatedValue + value);
        }
    }
}
