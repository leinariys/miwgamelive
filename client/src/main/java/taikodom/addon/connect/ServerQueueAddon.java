package taikodom.addon.connect;

import logic.IAddonSettings;
import logic.ui.item.InternalFrame;
import p001a.C3428rT;
import p001a.aAS;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.login.LoginAddon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.connect")
/* compiled from: a */
public class ServerQueueAddon implements C2495fo {
    private static final String XML = "server-queue.xml";
    private static final int gRg = 10;
    /* access modifiers changed from: private */
    public static int gRh = 0;
    /* access modifiers changed from: private */
    public JLabel gRm;
    private C3428rT<aAS> gRi;
    private LoginAddon loginAddon;
    private Timer gRk;
    private JLabel gRl;
    /* renamed from: kj */
    private IAddonProperties addonProperties;

    /* renamed from: nM */
    private InternalFrame f9833nM;

    /* renamed from: iM */
    private void m43188iM() {
        this.loginAddon = (LoginAddon) this.addonProperties.mo11830U(LoginAddon.class);
        this.gRl = this.f9833nM.mo4917cf("queue-position");
        this.gRm = this.f9833nM.mo4917cf("reconnecting-in");
        this.f9833nM.mo4913cb("close").addActionListener(new C4303c());
    }

    /* access modifiers changed from: protected */
    public void reconnect() {
        this.loginAddon.writeLoginPassToFile();
    }

    /* renamed from: iG */
    private void m43187iG() {
        this.gRi = new C4302b();
        this.addonProperties.aVU().mo13965a(aAS.class, this.gRi);
    }

    private void cDl() {
        this.gRk = new Timer(1000, new C4301a());
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m43185a(aAS aas) {
        if (this.f9833nM == null) {
            this.f9833nM = this.addonProperties.bHv().mo16792bN(XML);
            this.f9833nM.setVisible(false);
            m43188iM();
            cDl();
        }
        this.gRl.setText(String.valueOf(aas.cop()));
        this.gRm.setText(String.valueOf(10 - gRh));
        this.gRk.start();
        this.f9833nM.center();
        this.f9833nM.setModal(true);
        this.f9833nM.setVisible(true);
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.addonProperties = addonPropertie;
        m43187iG();
    }

    public void stop() {
        this.gRk.stop();
        this.f9833nM.destroy();
    }

    /* renamed from: taikodom.addon.connect.ServerQueueAddon$c */
    /* compiled from: a */
    class C4303c implements ActionListener {
        C4303c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ServerQueueAddon.this.stop();
        }
    }

    /* renamed from: taikodom.addon.connect.ServerQueueAddon$b */
    /* compiled from: a */
    class C4302b extends C3428rT<aAS> {
        C4302b() {
        }

        /* renamed from: b */
        public void mo321b(aAS aas) {
            ServerQueueAddon.this.m43185a(aas);
        }
    }

    /* renamed from: taikodom.addon.connect.ServerQueueAddon$a */
    class C4301a implements ActionListener {
        C4301a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (ServerQueueAddon.gRh >= 10) {
                ServerQueueAddon.gRh = 0;
                ServerQueueAddon.this.reconnect();
                ServerQueueAddon.this.stop();
                return;
            }
            ServerQueueAddon.this.gRm.setText(String.valueOf(10 - ServerQueueAddon.gRh));
            ServerQueueAddon.gRh = ServerQueueAddon.gRh + 1;
        }
    }
}
