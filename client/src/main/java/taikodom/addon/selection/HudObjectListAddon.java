package taikodom.addon.selection;

import game.network.message.externalizable.aNH;
import game.script.Actor;
import game.script.Character;
import game.script.item.Shot;
import game.script.mission.MissionTrigger;
import game.script.npc.NPC;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Gate;
import game.script.space.Loot;
import logic.IAddonSettings;
import logic.WrapRunnable;
import logic.aVH;
import logic.aaa.*;
import logic.ui.C2698il;
import logic.ui.Panel;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.hud.visibility.HudVisibilityAddon;
import taikodom.addon.hudmarkmanager.HudMarkManagerAddon;
import taikodom.addon.mission.BeaconManagerAddon;
import taikodom.addon.neo.preferences.GUIPrefAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashSet;

@TaikodomAddon("taikodom.addon.selection")
/* compiled from: a */
public class HudObjectListAddon implements C2495fo {

    /* renamed from: qw */
    private static final String f10246qw = "selection.xml";
    /* access modifiers changed from: private */
    public C2698il gmc;
    /* access modifiers changed from: private */
    public C0306Dz gmd;
    /* access modifiers changed from: private */
    public C0306Dz gme;
    /* access modifiers changed from: private */
    public C0306Dz gmf;
    /* access modifiers changed from: private */
    public C0306Dz gmg;
    /* access modifiers changed from: private */
    public C0306Dz gmh;
    /* access modifiers changed from: private */
    public C0306Dz gmi;
    /* access modifiers changed from: private */
    public boolean gmo;
    /* renamed from: kj */
    public IAddonProperties f10247kj;
    private C3428rT<C1274Sq> aYo;
    private C3428rT<HudVisibilityAddon.C4534d> bLC;
    private C3428rT<C2996mm> bvv;
    private C3428rT<C5783aaP> bvz;
    private InternalFrame bwQ;
    private C6622aqW dcE;
    private boolean eQg = false;
    private C6124ags<C6898avm> gmj;
    private C6124ags<C2041bS> gmk;
    private aVH<BeaconManagerAddon.C4765a> gml;
    private Actor gmm;
    private WrapRunnable gmn;
    private Panel gmp;
    private Panel gmq;
    private Panel gmr;
    private Panel gms;
    private C0306Dz gmt;
    private JToggleButton gmu;
    private JToggleButton gmv;
    private JToggleButton gmw;
    private JToggleButton gmx;
    private JToggleButton gmy;
    /* access modifiers changed from: private */
    private JToggleButton gmz;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10247kj = addonPropertie;
        if (this.f10247kj.getPlayer() != null) {
            this.gmn = new C4969d();
            cpG();
            m45381Fb();
            this.f10247kj.alf().addTask("Selection Table Update", this.gmn, 500);
            Collection<BeaconManagerAddon.C4765a> b = this.f10247kj.aVU().mo13967b(BeaconManagerAddon.C4765a.class);
            if (b.size() > 0) {
                m45385aV(((BeaconManagerAddon.C4765a) b.toArray()[0]).mo24355ha());
            }
            open(false);
            cpP();
        }
    }

    /* renamed from: JW */
    public IAddonProperties mo24557JW() {
        return this.f10247kj;
    }

    private void cpG() {
        if (this.bwQ == null) {
            this.bwQ = (InternalFrame) this.f10247kj.bHv().mo16794bQ(f10246qw);
            m45396iM();
            cpH();
            this.gmc = this.bwQ.mo4916ce("selectionPanel");
        }
    }

    private void cpH() {
        this.gmd = new C0306Dz(this, this.bwQ.mo4915cd("npcs-table"));
        this.gme = new C0306Dz(this, this.bwQ.mo4915cd("pcs-table"));
        this.gmf = new C0306Dz(this, this.bwQ.mo4915cd("waypoint-table"));
        this.gmg = new C0306Dz(this, this.bwQ.mo4915cd("portals-table"));
        this.gmh = new C0306Dz(this, this.bwQ.mo4915cd("stations-table"));
        this.gmi = new C0306Dz(this, this.bwQ.mo4915cd("loots-table"));
    }

    /* renamed from: iM */
    private void m45396iM() {
        this.gms = this.bwQ.mo4915cd("portals");
        this.gmr = this.bwQ.mo4915cd("stations");
        this.gmq = this.bwQ.mo4915cd("loots");
        this.gmp = this.bwQ.mo4915cd("waypoint");
        this.gmu = this.bwQ.mo4915cd("visibilityWaypoint");
        this.gmv = this.bwQ.mo4915cd("visibilityStations");
        this.gmw = this.bwQ.mo4915cd("visibilityPortals");
        this.gmx = this.bwQ.mo4915cd("visibilityLoots");
        this.gmy = this.bwQ.mo4915cd("visibilityNpcs");
        this.gmz = this.bwQ.mo4915cd("visibilityPcs");
        this.gmu.setSelected(m45389c(C0308E.WAYPOINT));
        this.gmv.setSelected(m45389c(C0308E.STATION));
        this.gmw.setSelected(m45389c(C0308E.PORTAL));
        this.gmx.setSelected(m45389c(C0308E.LOOT));
        this.gmy.setSelected(m45389c(C0308E.NPC));
        this.gmz.setSelected(m45389c(C0308E.PLAYER));
        cpI();
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m45387b(C0308E e) {
        GUIPrefAddon.m44897e(String.valueOf(HudObjectListAddon.class.getName().toLowerCase()) + "." + e.name().toLowerCase(), GUIPrefAddon.C4817c.P, new StringBuilder(String.valueOf(((HudMarkManagerAddon) this.f10247kj.mo11830U(HudMarkManagerAddon.class)).mo24069e(e))).toString());
    }

    /* renamed from: c */
    private boolean m45389c(C0308E e) {
        return Boolean.parseBoolean(GUIPrefAddon.m44898f(String.valueOf(getClass().getName().toLowerCase()) + "." + e.name().toLowerCase(), GUIPrefAddon.C4817c.P, "true"));
    }

    private void cpI() {
        this.gmu.addActionListener(new C4970e());
        this.gmv.addActionListener(new C4967b());
        this.gmw.addActionListener(new C4968c());
        this.gmx.addActionListener(new C4973h());
        this.gmy.addActionListener(new C4974i());
        this.gmz.addActionListener(new C4971f());
    }

    /* renamed from: a */
    public void mo24558a(C0306Dz dz) {
        this.gmt = dz;
    }

    public C0306Dz cpJ() {
        return this.gmt;
    }

    public Actor cpK() {
        return this.gmm;
    }

    public void stop() {
        removeListeners();
        this.gmn.cancel();
        if (this.bwQ != null) {
            this.bwQ.destroy();
            this.bwQ = null;
        }
    }

    public void updateSize() {
        int i = this.gmp.getPreferredSize().height + this.gms.getPreferredSize().height + this.gmr.getPreferredSize().height + this.gmq.getPreferredSize().height;
        Insets insets = ((Panel) this.gmc).getInsets();
        int i2 = i + insets.top + insets.bottom;
        ((Panel) this.gmc).setPreferredSize(new Dimension(this.f10247kj.bHv().getScreenWidth() - 100, i2));
    }

    /* renamed from: Fb */
    private void m45381Fb() {
        this.bvv = new C4972g();
        this.bLC = new C4966a();
        this.bvz = new C4978m();
        this.aYo = new C4977l();
        this.gmj = new C4976k();
        this.gmk = new C4975j();
        this.gml = new C4979n();
        this.f10247kj.mo2322a("selection", (Action) this.dcE);
        this.f10247kj.mo2317P(this);
        this.f10247kj.aVU().mo13965a(C2996mm.class, this.bvv);
        this.f10247kj.aVU().mo13965a(C5783aaP.class, this.bvz);
        this.f10247kj.aVU().mo13965a(C1274Sq.class, this.aYo);
        this.f10247kj.aVU().mo13965a(C6898avm.class, this.gmj);
        this.f10247kj.aVU().mo13965a(C2041bS.class, this.gmk);
        this.f10247kj.aVU().mo13965a(BeaconManagerAddon.C4765a.class, this.gml);
        this.f10247kj.aVU().mo13965a(HudVisibilityAddon.C4534d.class, this.bLC);
    }

    private void removeListeners() {
        this.f10247kj.mo2318Q(this);
        this.f10247kj.aVU().mo13968b(C2996mm.class, this.bvv);
        this.f10247kj.aVU().mo13968b(C5783aaP.class, this.bvz);
        this.f10247kj.aVU().mo13968b(C1274Sq.class, this.aYo);
        this.f10247kj.aVU().mo13968b(C6898avm.class, this.gmj);
        this.f10247kj.aVU().mo13968b(C2041bS.class, this.gmk);
        this.f10247kj.aVU().mo13968b(BeaconManagerAddon.C4765a.class, this.gml);
        this.f10247kj.aVU().mo13968b(HudVisibilityAddon.C4534d.class, this.bLC);
    }

    /* access modifiers changed from: private */
    /* renamed from: aV */
    public void m45385aV(Actor cr) {
        this.gmm = cr;
        mo24565f(cr);
    }

    public void cpL() {
        open(true);
        if (this.f10247kj.getPlayer().bQB() || !this.f10247kj.alb().anX()) {
            this.eQg = false;
            return;
        }
        this.f10247kj.alb().mo22059aR(false);
        this.eQg = true;
    }

    public void cpM() {
        if (this.bwQ != null) {
            close();
            if (this.eQg && !this.f10247kj.alb().anX() && !this.f10247kj.getPlayer().bQB()) {
                this.eQg = false;
                this.f10247kj.alb().cOW();
            }
        }
    }

    private void open(boolean z) {
        if (this.bwQ == null) {
            cpG();
        }
        this.bwQ.add(this.gmc);
        this.gmc.setVisible(z);
        this.gmo = true;
        cpO();
        this.bwQ.setVisible(z);
        cpN();
        this.f10247kj.aVU().mo13975h(new aNH(z));
    }

    /* access modifiers changed from: private */
    public void cpN() {
        this.bwQ.pack();
        Dimension size = this.bwQ.getSize();
        this.bwQ.setLocation((this.f10247kj.ale().aes() / 2) - (size.width / 2), (this.f10247kj.ale().aet() / 2) - (size.height / 2));
    }

    /* access modifiers changed from: private */
    public void close() {
        this.gmo = false;
        if (this.gmt != null) {
            this.gmt.ddv();
        }
        this.gmt = null;
        if (this.bwQ != null) {
            this.bwQ.setVisible(false);
        }
        this.f10247kj.aVU().mo13975h(new aNH(false));
    }

    private void cpO() {
        if (this.gmo && this.f10247kj.getPlayer() != null && !this.f10247kj.getPlayer().bQB() && this.f10247kj.getPlayer().bQx() != null && this.gmc != null && this.f10247kj.ala().aLW().dvO() != null) {
            cpQ();
            HashSet<Actor> hashSet = new HashSet<>();
            hashSet.addAll(this.f10247kj.ala().aLW().dvO().cKv());
            for (Actor f : hashSet) {
                mo24565f(f);
            }
        }
    }

    private void cpP() {
        this.gmd.ddq();
        this.gme.ddq();
        this.gmf.ddq();
        this.gmg.ddq();
        this.gmh.ddq();
        this.gmi.ddq();
    }

    private void cpQ() {
        this.gmd.cpQ();
        this.gme.cpQ();
        this.gmf.cpQ();
        this.gmg.cpQ();
        this.gmh.cpQ();
        this.gmi.cpQ();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo24564e(Actor cr) {
        if ((cr instanceof Ship) || (cr instanceof MissionTrigger) || (cr instanceof C0286Dh)) {
            if (cr instanceof Ship) {
                if (cr != this.f10247kj.getPlayer().bQx()) {
                    Character agj = ((Ship) cr).agj();
                    if (agj instanceof NPC) {
                        NPC auf = (NPC) agj;
                        if (auf.dzD() == null || auf.dzD() == this.f10247kj.getPlayer()) {
                            this.gmd.mo1797e(cr);
                        } else {
                            return;
                        }
                    } else if (agj instanceof Player) {
                        this.gme.mo1797e(cr);
                    }
                } else {
                    return;
                }
            } else if (cr instanceof Gate) {
                this.gmg.mo1797e(cr);
            } else if (cr instanceof Station) {
                this.gmh.mo1797e(cr);
            } else if (cr instanceof Loot) {
                this.gmi.mo1797e(cr);
            } else if (cr instanceof MissionTrigger) {
                this.gmf.mo1797e(cr);
            }
            updateSize();
            cpN();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo24565f(Actor cr) {
        if ((cr instanceof Ship) || (cr instanceof MissionTrigger) || (cr instanceof C0286Dh)) {
            if (cr instanceof Ship) {
                if (cr != this.f10247kj.getPlayer().bQx()) {
                    Character agj = ((Ship) cr).agj();
                    if (agj instanceof NPC) {
                        NPC auf = (NPC) agj;
                        if (auf.dzD() == null || auf.dzD() == this.f10247kj.getPlayer()) {
                            this.gmd.mo1798f(cr);
                        } else {
                            return;
                        }
                    } else if (agj instanceof Player) {
                        this.gme.mo1798f(cr);
                    }
                } else {
                    return;
                }
            } else if (cr instanceof Gate) {
                this.gmg.mo1798f(cr);
            } else if (cr instanceof Station) {
                this.gmh.mo1798f(cr);
            } else if (cr instanceof Loot) {
                this.gmi.mo1798f(cr);
            } else if (cr instanceof MissionTrigger) {
                this.gmf.mo1798f(cr);
            }
            updateSize();
            cpN();
        }
    }

    @C2602hR(mo19255zf = "SHOW_OBJECTS")
    /* renamed from: fT */
    public void mo24566fT(boolean z) {
        if (!this.f10247kj.getPlayer().bQB()) {
            if (z) {
                cpL();
            } else {
                cpM();
            }
        }
    }

    public C6415amX cpR() {
        return this.gmd.ddu();
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$d */
    /* compiled from: a */
    class C4969d extends WrapRunnable {
        C4969d() {
        }

        public void run() {
            if (HudObjectListAddon.this.gmo && HudObjectListAddon.this.gmc != null && HudObjectListAddon.this.gmc.isVisible()) {
                HudObjectListAddon.this.gmd.ddq();
                HudObjectListAddon.this.gme.ddq();
                HudObjectListAddon.this.gmf.ddq();
                HudObjectListAddon.this.gmg.ddq();
                HudObjectListAddon.this.gmh.ddq();
                HudObjectListAddon.this.gmi.ddq();
                HudObjectListAddon.this.updateSize();
                HudObjectListAddon.this.cpN();
            }
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$e */
    /* compiled from: a */
    class C4970e implements ActionListener {
        C4970e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((HudMarkManagerAddon) HudObjectListAddon.this.f10247kj.mo11830U(HudMarkManagerAddon.class)).mo24068d(C0308E.WAYPOINT);
            HudObjectListAddon.this.m45387b(C0308E.WAYPOINT);
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$b */
    /* compiled from: a */
    class C4967b implements ActionListener {
        C4967b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((HudMarkManagerAddon) HudObjectListAddon.this.f10247kj.mo11830U(HudMarkManagerAddon.class)).mo24068d(C0308E.STATION);
            HudObjectListAddon.this.m45387b(C0308E.STATION);
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$c */
    /* compiled from: a */
    class C4968c implements ActionListener {
        C4968c() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((HudMarkManagerAddon) HudObjectListAddon.this.f10247kj.mo11830U(HudMarkManagerAddon.class)).mo24068d(C0308E.PORTAL);
            HudObjectListAddon.this.m45387b(C0308E.PORTAL);
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$h */
    /* compiled from: a */
    class C4973h implements ActionListener {
        C4973h() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((HudMarkManagerAddon) HudObjectListAddon.this.f10247kj.mo11830U(HudMarkManagerAddon.class)).mo24068d(C0308E.LOOT);
            HudObjectListAddon.this.m45387b(C0308E.LOOT);
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$i */
    /* compiled from: a */
    class C4974i implements ActionListener {
        C4974i() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((HudMarkManagerAddon) HudObjectListAddon.this.f10247kj.mo11830U(HudMarkManagerAddon.class)).mo24068d(C0308E.NPC);
            HudObjectListAddon.this.m45387b(C0308E.NPC);
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$f */
    /* compiled from: a */
    class C4971f implements ActionListener {
        C4971f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((HudMarkManagerAddon) HudObjectListAddon.this.f10247kj.mo11830U(HudMarkManagerAddon.class)).mo24068d(C0308E.PLAYER);
            HudObjectListAddon.this.m45387b(C0308E.PLAYER);
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$g */
    /* compiled from: a */
    class C4972g extends C3428rT<C2996mm> {
        C4972g() {
        }

        /* renamed from: a */
        public void mo321b(C2996mm mmVar) {
            HudObjectListAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$a */
    class C4966a extends C3428rT<HudVisibilityAddon.C4534d> {
        C4966a() {
        }

        /* renamed from: a */
        public void mo321b(HudVisibilityAddon.C4534d dVar) {
            if (!dVar.isVisible()) {
                HudObjectListAddon.this.close();
            }
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$m */
    /* compiled from: a */
    class C4978m extends C3428rT<C5783aaP> {
        C4978m() {
        }

        /* renamed from: a */
        public void mo321b(C5783aaP aap) {
            HudObjectListAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$l */
    /* compiled from: a */
    class C4977l extends C3428rT<C1274Sq> {
        C4977l() {
        }

        /* renamed from: a */
        public void mo321b(C1274Sq sq) {
            HudObjectListAddon.this.close();
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$k */
    /* compiled from: a */
    class C4976k extends C6124ags<C6898avm> {
        C4976k() {
        }

        /* renamed from: a */
        public boolean updateInfo(C6898avm avm) {
            Actor ha = avm.mo16628ha();
            if (!(ha instanceof Shot)) {
                HudObjectListAddon.this.mo24565f(ha);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$j */
    /* compiled from: a */
    class C4975j extends C6124ags<C2041bS> {
        C4975j() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2041bS bSVar) {
            Actor ha = bSVar.mo17298ha();
            if (!(ha instanceof Shot)) {
                HudObjectListAddon.this.mo24564e(ha);
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.selection.HudObjectListAddon$n */
    /* compiled from: a */
    class C4979n implements aVH<BeaconManagerAddon.C4765a> {
        C4979n() {
        }

        /* renamed from: a */
        public void mo2004k(String str, BeaconManagerAddon.C4765a aVar) {
            HudObjectListAddon.this.m45385aV(aVar.mo24355ha());
        }

        /* renamed from: b */
        public void mo2003f(String str, BeaconManagerAddon.C4765a aVar) {
            HudObjectListAddon.this.m45385aV((Actor) null);
        }
    }
}
