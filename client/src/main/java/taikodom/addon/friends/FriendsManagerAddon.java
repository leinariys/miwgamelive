package taikodom.addon.friends;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.aQH;
import game.script.Actor;
import game.script.associates.Associate;
import game.script.associates.Associates;
import game.script.corporation.CorporationLogo;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Node;
import logic.IAddonSettings;
import logic.aaa.C3225pT;
import logic.baa.C5473aKr;
import logic.baa.C6200aiQ;
import logic.data.link.C6488ans;
import logic.data.link.C7038azz;
import logic.res.code.C5663aRz;
import logic.ui.Panel;
import logic.ui.item.TaskPane;
import logic.ui.item.TextField;
import logic.ui.item.aUR;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.C6144ahM;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.components.popup.DefaultMenuItems;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

@TaikodomAddon("taikodom.addon.friends")
/* compiled from: a */
public class FriendsManagerAddon implements C2495fo {
    private static /* synthetic */ int[] bhj;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public Player f9875P;
    /* access modifiers changed from: private */
    public TaskPane bgR;
    /* access modifiers changed from: private */
    public JList bgT;
    /* access modifiers changed from: private */
    public DefaultListModel bgU;
    /* access modifiers changed from: private */
    public JList bgV;
    /* access modifiers changed from: private */
    public DefaultListModel bgW;
    /* access modifiers changed from: private */
    public C6124ags<aQH> bgX;
    /* access modifiers changed from: private */
    public C6200aiQ<Associate> bgZ;
    /* access modifiers changed from: private */
    public C3428rT<C3225pT> bhb;
    /* access modifiers changed from: private */
    public TextField bhc;
    /* access modifiers changed from: private */
    public JButton bhd;
    /* access modifiers changed from: private */
    public JCheckBox bhe;
    /* access modifiers changed from: private */
    public Panel bhf;
    /* access modifiers changed from: private */
    public Panel bhg;
    /* access modifiers changed from: private */
    public Panel bhh;
    /* access modifiers changed from: private */
    public Panel bhi;
    /* renamed from: kj */
    public IAddonProperties f9876kj;
    private aSZ aCF;
    private aGC bgS;
    private C5473aKr<Associates, Object> bgY;
    /* access modifiers changed from: private */
    private C5473aKr<Associates, Object> bha;

    static /* synthetic */ int[] abH() {
        int[] iArr = bhj;
        if (iArr == null) {
            iArr = new int[PlayerSocialController.C3362a.values().length];
            try {
                iArr[PlayerSocialController.C3362a.PLAYER_IS_ALREADY_FRIEND.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[PlayerSocialController.C3362a.PLAYER_IS_YOURSELF.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[PlayerSocialController.C3362a.PLAYER_NOT_FOUND.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[PlayerSocialController.C3362a.PLAYER_NOT_FOUND_OR_DISCONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[PlayerSocialController.C3362a.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e5) {
            }
            bhj = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9876kj = addonPropertie;
        this.f9875P = this.f9876kj.getPlayer();
        if (this.f9875P != null) {
            this.bgU = new DefaultListModel();
            this.bgW = new DefaultListModel();
            installListeners();
            abE();
            abF();
            this.bgS = new C4376a(this.f9876kj.translate("Associates"), "associates");
            this.bgS.mo15602jH(this.f9876kj.translate("Here you have a list of your friends, can contact the ones that are online and manage your block list."));
            this.bgS.mo15603jI("ASSOCIATES_WINDOW");
            this.aCF = new aSZ(this.bgR, this.bgS, 100, 1);
            this.f9876kj.aVU().publish(this.aCF);
            this.f9876kj.mo2317P(this);
            if (this.bgS.isEnabled()) {
                open();
            }
        }
    }

    private void installListeners() {
        this.bgX = new C4379d();
        this.bgY = new C4380e();
        this.bha = new C4377b();
        this.bgZ = new C4378c();
        this.bhb = new C4383h();
    }

    private void abE() { //Текстовые команды
        /*to
        Добавить в список друзей: / friend <name>
        Удалить из списка друзей: / unfriend <имя>
        Блокировать связь: / block <имя>
        Разблокировать связь: / unblock <имя>

        */
        this.f9876kj.mo2322a("friend", (Action) new C4384i("associates", this.f9876kj.translate("Associates"), "friend"));
        this.f9876kj.mo2322a("unfriend", (Action) new C4381f("associates", this.f9876kj.translate("Associates"), "unfriend"));
        this.f9876kj.mo2322a("block", (Action) new C4382g("associates", this.f9876kj.translate("Associates"), "block"));
        this.f9876kj.mo2322a("unblock", (Action) new C4394r("associates", this.f9876kj.translate("Associates"), "unblock"));
    }

    public void stop() {
        abG();
        if (this.bgR != null) {
            this.bgR.destroy();
            this.bgR = null;
        }
    }

    private void abF() {
        if (this.bgR == null) {
            this.bgR = (TaskPane) this.f9876kj.bHv().mo16794bQ("friends.xml");
            this.bhc = this.bgR.mo4920ci("add-player-name");
            this.bhd = this.bgR.mo4913cb("remove");
            this.bhe = this.bgR.mo4915cd("off-lines");
            this.bhf = this.bgR.mo4915cd("fspanel");
            this.bhg = this.bgR.mo4915cd("bspanel");
            this.bhh = this.bgR.mo4915cd("tfspanel");
            this.bhi = this.bgR.mo4915cd("tbspanel");
            this.bhc.addKeyListener(new C4393q());
            this.bgR.mo4913cb("add").addActionListener(new C4392p());
            this.bhd.setEnabled(false);
            this.bhd.addActionListener(new C4391o());
            this.bgT = this.bgR.mo4915cd("friends-list");
            this.bgT.setModel(this.bgU);
            this.bgT.setCellRenderer(new C4385j(this, (C4385j) null));
            this.bgT.setDragEnabled(true);
            this.bgT.setTransferHandler(new C3980xh());
            this.bgT.addListSelectionListener(new C4390n());
            this.bgT.addMouseListener(new C4389m(this.bgT));
            this.bhe.addActionListener(new C4388l());
            this.bgR.mo4913cb("block").addActionListener(new C4397u());
            JButton cb = this.bgR.mo4913cb("unblock");
            cb.setEnabled(false);
            cb.addActionListener(new C4396t(cb));
            this.bgV = this.bgR.mo4915cd("blocked-list");
            this.bgV.setModel(this.bgW);
            this.bgV.setCellRenderer(new C4387k(this, (C4387k) null));
            this.bgV.addListSelectionListener(new C4395s(cb));
            Associates dxu = this.f9875P.dxu();
            dxu.mo8319a(C7038azz.gZK, (C5473aKr<?, ?>) this.bgY);
            dxu.mo8319a(C7038azz.gZL, (C5473aKr<?, ?>) this.bha);
        }
    }

    /* access modifiers changed from: private */
    public void open() {
        SwingUtilities.invokeLater(new C4399w());
    }

    /* access modifiers changed from: private */
    /* renamed from: bp */
    public void m43434bp(String str) {
        if (str != null && !"".equals(str)) {
            ((PlayerSocialController) C3582se.m38985a(this.f9875P.dwU(), (C6144ahM<?>) new C4398v(str))).mo21451mV(str.trim());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: bq */
    public void m43435bq(String str) {
        if (str != null && !"".equals(str)) {
            switch (abH()[this.f9875P.dwU().mo21452mX(str.trim()).ordinal()]) {
                case 2:
                    this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f9876kj.translate("<Player> not found or disconected"), str));
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m43437c(String str, boolean z) {
        if (str != null && str.trim().length() != 0) {
            String trim = str.trim();
            if (this.f9875P.getName().equals(trim)) {
                this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f9876kj.translate("<Player> is yourself"), str));
            } else if (!z || !this.f9875P.mo14436nL(trim)) {
                ((PlayerSocialController) C3582se.m38985a(this.f9875P.dwU(), (C6144ahM<?>) new C4400x(str))).mo21456u(trim, z);
            } else {
                this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, this.f9876kj.translate("<Player> already blocked"), str));
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: X */
    public void m43423X(Player aku) {
        this.f9875P.mo14342X(aku);
    }

    /* access modifiers changed from: private */
    /* renamed from: Y */
    public void m43424Y(Player aku) {
        this.f9875P.mo14370cY(aku);
        refresh();
    }

    /* access modifiers changed from: private */
    public void refresh() {
        Associates dxu = this.f9875P.dxu();
        m43426a(dxu);
        m43430b(dxu);
    }

    /* renamed from: a */
    private void m43426a(Associates adq) {
        this.bgU.clear();
        boolean isSelected = this.bhe.isSelected();
        for (Associate next : adq.cUP()) {
            if (isSelected || next.aPI()) {
                this.bgU.addElement(next);
            }
        }
        if (this.bgT != null) {
            this.bgT.repaint(50);
        }
    }

    /* renamed from: b */
    private void m43430b(Associates adq) {
        this.bgW.clear();
        for (Player addElement : adq.cUR()) {
            this.bgW.addElement(addElement);
        }
        if (this.bgV != null) {
            this.bgV.repaint(50);
        }
    }

    private void abG() {
        Associates dxu = this.f9875P.dxu();
        dxu.mo8325b(C7038azz.gZK, (C5473aKr<?, ?>) this.bgY);
        dxu.mo8325b(C7038azz.gZL, (C5473aKr<?, ?>) this.bha);
        for (Associate b : dxu.cUP()) {
            b.mo8326b(C6488ans.gdt, (C6200aiQ<?>) this.bgZ);
        }
        this.bgT = null;
        this.bgV = null;
        this.bgU.clear();
        this.bgW.clear();
        this.f9876kj.aVU().mo13964a(this.bgX);
        this.f9876kj.aVU().mo13964a(this.bhb);
    }

    @C2602hR(mo19255zf = "ASSOCIATES_WINDOW", mo19256zg = "WINDOW")
    /* renamed from: al */
    public void mo23823al(boolean z) {
        if (z) {
            open();
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$a */
    class C4376a extends aGC {


        C4376a(String str, String str2) {
            super(str, str2);
        }

        public void setVisible(boolean z) {
            if (z) {
                FriendsManagerAddon.this.open();
            }
        }

        public boolean isEnabled() {
            return true;
        }

        /* renamed from: m */
        public void mo8924m(boolean z) {
            boolean z2;
            boolean z3;
            boolean z4;
            boolean z5 = false;
            Panel c = FriendsManagerAddon.this.bhf;
            if (z) {
                z2 = false;
            } else {
                z2 = true;
            }
            c.setVisible(z2);
            Panel d = FriendsManagerAddon.this.bhg;
            if (z) {
                z3 = false;
            } else {
                z3 = true;
            }
            d.setVisible(z3);
            Panel e = FriendsManagerAddon.this.bhh;
            if (z) {
                z4 = false;
            } else {
                z4 = true;
            }
            e.setVisible(z4);
            Panel f = FriendsManagerAddon.this.bhi;
            if (!z) {
                z5 = true;
            }
            f.setVisible(z5);
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$d */
    /* compiled from: a */
    class C4379d extends C6124ags<aQH> {
        C4379d() {
        }

        /* renamed from: a */
        public boolean updateInfo(aQH aqh) {
            switch (aqh.aiH()) {
                case 0:
                    FriendsManagerAddon.this.m43434bp(aqh.dqc());
                    break;
                case 1:
                    FriendsManagerAddon.this.m43435bq(aqh.dqc());
                    break;
                case 2:
                    FriendsManagerAddon.this.m43437c(aqh.dqc(), true);
                    break;
                case 3:
                    FriendsManagerAddon.this.m43437c(aqh.dqc(), false);
                    break;
            }
            return false;
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$e */
    /* compiled from: a */
    class C4380e implements C5473aKr<Associates, Object> {
        C4380e() {
        }

        /* renamed from: a */
        public void mo1102b(Associates adq, C5663aRz arz, Object[] objArr) {
            for (Associate ff : objArr) {
                if (!FriendsManagerAddon.this.bgU.contains(ff)) {
                    ff.mo8320a(C6488ans.gdt, (C6200aiQ<?>) FriendsManagerAddon.this.bgZ);
                    FriendsManagerAddon.this.bgU.addElement(ff);
                }
            }
            if (FriendsManagerAddon.this.bgT != null) {
                FriendsManagerAddon.this.bgT.getSelectionModel().clearSelection();
                FriendsManagerAddon.this.bgT.repaint(50);
            }
        }

        /* renamed from: b */
        public void mo1101a(Associates adq, C5663aRz arz, Object[] objArr) {
            for (Associate ff : objArr) {
                if (FriendsManagerAddon.this.bgU.removeElement(ff)) {
                    ff.mo8326b(C6488ans.gdt, (C6200aiQ<?>) FriendsManagerAddon.this.bgZ);
                }
            }
            if (FriendsManagerAddon.this.bgT != null) {
                FriendsManagerAddon.this.bgT.getSelectionModel().clearSelection();
                FriendsManagerAddon.this.bgT.repaint(50);
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$b */
    /* compiled from: a */
    class C4377b implements C5473aKr<Associates, Object> {
        C4377b() {
        }

        /* renamed from: a */
        public void mo1102b(Associates adq, C5663aRz arz, Object[] objArr) {
            for (Object obj : objArr) {
                if (!FriendsManagerAddon.this.bgW.contains(obj)) {
                    FriendsManagerAddon.this.bgW.addElement(obj);
                }
            }
            if (FriendsManagerAddon.this.bgV != null) {
                FriendsManagerAddon.this.bgV.getSelectionModel().clearSelection();
                FriendsManagerAddon.this.bgV.repaint(50);
            }
        }

        /* renamed from: b */
        public void mo1101a(Associates adq, C5663aRz arz, Object[] objArr) {
            for (Object removeElement : objArr) {
                FriendsManagerAddon.this.bgW.removeElement(removeElement);
            }
            if (FriendsManagerAddon.this.bgV != null) {
                FriendsManagerAddon.this.bgV.getSelectionModel().clearSelection();
                FriendsManagerAddon.this.bgV.repaint(50);
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$c */
    /* compiled from: a */
    class C4378c implements C6200aiQ<Associate> {
        C4378c() {
        }

        /* renamed from: a */
        public void mo1143a(Associate ff, C5663aRz arz, Object obj) {
            if (FriendsManagerAddon.this.bgR != null) {
                boolean isSelected = FriendsManagerAddon.this.bhe.isSelected();
                if (!FriendsManagerAddon.this.f9875P.dxu().mo8514ct(ff.mo2077dL()) || (!isSelected && !((Boolean) obj).booleanValue())) {
                    FriendsManagerAddon.this.bgU.removeElement(ff);
                } else {
                    FriendsManagerAddon.this.bgU.addElement(ff);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$h */
    /* compiled from: a */
    class C4383h extends C3428rT<C3225pT> {
        C4383h() {
        }

        /* renamed from: a */
        public void mo321b(C3225pT pTVar) {
            FriendsManagerAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$i */
    /* compiled from: a */
    class C4384i extends C6622aqW {


        C4384i(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1) {
                FriendsManagerAddon.this.m43434bp(objArr[0].toString());
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$f */
    /* compiled from: a */
    class C4381f extends C6622aqW {


        C4381f(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1) {
                FriendsManagerAddon.this.m43435bq(objArr[0].toString());
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$g */
    /* compiled from: a */
    class C4382g extends C6622aqW {


        C4382g(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1) {
                FriendsManagerAddon.this.m43437c(objArr[0].toString(), true);
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$r */
    /* compiled from: a */
    class C4394r extends C6622aqW {


        C4394r(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (objArr.length == 1) {
                FriendsManagerAddon.this.m43437c(objArr[0].toString(), false);
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$q */
    /* compiled from: a */
    class C4393q extends KeyAdapter {
        C4393q() {
        }

        public void keyTyped(KeyEvent keyEvent) {
            if (keyEvent.getKeyChar() == 10) {
                FriendsManagerAddon.this.m43434bp(FriendsManagerAddon.this.bhc.getText());
                FriendsManagerAddon.this.bhc.setText("");
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$p */
    /* compiled from: a */
    class C4392p implements ActionListener {
        C4392p() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FriendsManagerAddon.this.m43434bp(FriendsManagerAddon.this.bhc.getText());
            FriendsManagerAddon.this.bhc.setText("");
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$o */
    /* compiled from: a */
    class C4391o implements ActionListener {
        C4391o() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (FriendsManagerAddon.this.bgT != null && !FriendsManagerAddon.this.bgT.isSelectionEmpty()) {
                for (int i : FriendsManagerAddon.this.bgT.getSelectedIndices()) {
                    FriendsManagerAddon.this.m43423X(((Associate) FriendsManagerAddon.this.bgU.get(i)).mo2077dL());
                }
                FriendsManagerAddon.this.bhd.setEnabled(false);
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$n */
    /* compiled from: a */
    class C4390n implements ListSelectionListener {
        C4390n() {
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (FriendsManagerAddon.this.bgR != null && FriendsManagerAddon.this.bgT != null) {
                FriendsManagerAddon.this.bhd.setEnabled(!FriendsManagerAddon.this.bgT.isSelectionEmpty());
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$m */
    /* compiled from: a */
    class C4389m extends C5375aGx {
        C4389m(JList jList) {
            super(jList);
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public JPopupMenu mo9137d(int i, Object[] objArr) {
            if (objArr == null || objArr.length != 1 || !(objArr[0] instanceof Associate)) {
                return null;
            }
            return DefaultMenuItems.m43035bh(objArr[0].mo2077dL());
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$l */
    /* compiled from: a */
    class C4388l implements ActionListener {
        C4388l() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FriendsManagerAddon.this.refresh();
            if (FriendsManagerAddon.this.bgT != null) {
                FriendsManagerAddon.this.bgT.getSelectionModel().clearSelection();
                FriendsManagerAddon.this.bgT.validate();
            }
            if (FriendsManagerAddon.this.bgV != null) {
                FriendsManagerAddon.this.bgV.getSelectionModel().clearSelection();
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$u */
    /* compiled from: a */
    class C4397u implements ActionListener {
        C4397u() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            FriendsManagerAddon.this.m43437c(FriendsManagerAddon.this.bgR.mo4920ci("block-player-name").getText(), true);
            FriendsManagerAddon.this.bgR.mo4920ci("block-player-name").setText("");
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$t */
    /* compiled from: a */
    class C4396t implements ActionListener {
        private final /* synthetic */ JButton ijI;

        C4396t(JButton jButton) {
            this.ijI = jButton;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!FriendsManagerAddon.this.bgV.isSelectionEmpty()) {
                for (int i : FriendsManagerAddon.this.bgV.getSelectedIndices()) {
                    FriendsManagerAddon.this.m43424Y((Player) FriendsManagerAddon.this.bgW.get(i));
                }
                this.ijI.setEnabled(false);
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$s */
    /* compiled from: a */
    class C4395s implements ListSelectionListener {
        private final /* synthetic */ JButton ijI;

        C4395s(JButton jButton) {
            this.ijI = jButton;
        }

        public void valueChanged(ListSelectionEvent listSelectionEvent) {
            if (FriendsManagerAddon.this.bgV != null) {
                this.ijI.setEnabled(!FriendsManagerAddon.this.bgV.isSelectionEmpty());
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$w */
    /* compiled from: a */
    class C4399w implements Runnable {
        C4399w() {
        }

        public void run() {
            FriendsManagerAddon.this.bhc.setText("");
            FriendsManagerAddon.this.bgR.mo4920ci("block-player-name").setText("");
            FriendsManagerAddon.this.f9876kj.aVU().mo13965a(aQH.class, FriendsManagerAddon.this.bgX);
            FriendsManagerAddon.this.f9876kj.aVU().mo13965a(C3225pT.class, FriendsManagerAddon.this.bhb);
            FriendsManagerAddon.this.refresh();
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$v */
    /* compiled from: a */
    class C4398v implements C6144ahM<PlayerSocialController.C3362a> {
        private static /* synthetic */ int[] bhj;
        private final /* synthetic */ String ijT;

        C4398v(String str) {
            this.ijT = str;
        }

        static /* synthetic */ int[] abH() {
            int[] iArr = bhj;
            if (iArr == null) {
                iArr = new int[PlayerSocialController.C3362a.values().length];
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_IS_ALREADY_FRIEND.ordinal()] = 4;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_IS_YOURSELF.ordinal()] = 5;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_NOT_FOUND.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_NOT_FOUND_OR_DISCONNECTED.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.SUCCESS.ordinal()] = 1;
                } catch (NoSuchFieldError e5) {
                }
                bhj = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        public void mo1931n(PlayerSocialController.C3362a aVar) {
            switch (abH()[aVar.ordinal()]) {
                case 2:
                    FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("<Player> not found or disconected"), this.ijT));
                    return;
                case 4:
                    FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("<Player> already your friend"), this.ijT));
                    return;
                case 5:
                    FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("<Player> is yourself"), this.ijT));
                    return;
                default:
                    return;
            }
        }

        /* renamed from: a */
        public void mo1930a(Throwable th) {
            FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("Unknow error"), new Object[0]));
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$x */
    /* compiled from: a */
    class C4400x extends C0947Nt<PlayerSocialController.C3362a> {
        private static /* synthetic */ int[] bhj;
        private final /* synthetic */ String ijT;

        C4400x(String str) {
            this.ijT = str;
        }

        static /* synthetic */ int[] abH() {
            int[] iArr = bhj;
            if (iArr == null) {
                iArr = new int[PlayerSocialController.C3362a.values().length];
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_IS_ALREADY_FRIEND.ordinal()] = 4;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_IS_YOURSELF.ordinal()] = 5;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_NOT_FOUND.ordinal()] = 3;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.PLAYER_NOT_FOUND_OR_DISCONNECTED.ordinal()] = 2;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[PlayerSocialController.C3362a.SUCCESS.ordinal()] = 1;
                } catch (NoSuchFieldError e5) {
                }
                bhj = iArr;
            }
            return iArr;
        }

        /* renamed from: b */
        public void mo4278E(PlayerSocialController.C3362a aVar) {
            switch (abH()[aVar.ordinal()]) {
                case 1:
                    FriendsManagerAddon.this.refresh();
                    return;
                case 2:
                    FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("<Player> not found or disconected"), this.ijT));
                    return;
                case 3:
                    FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("<Player> not found"), this.ijT));
                    return;
                case 5:
                    FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("<Player> is yourself"), this.ijT));
                    return;
                default:
                    return;
            }
        }

        /* renamed from: c */
        public void mo4279c(Throwable th) {
            FriendsManagerAddon.this.f9876kj.aVU().mo13975h(new C0939Nn(C0939Nn.C0940a.ERROR, FriendsManagerAddon.this.f9876kj.translate("Unknow error"), new Object[0]));
            th.printStackTrace();
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$j */
    /* compiled from: a */
    private class C4385j extends C5517aMj {


        private C4385j() {
        }

        /* synthetic */ C4385j(FriendsManagerAddon friendsManagerAddon, C4385j jVar) {
            this();
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            String str;
            Node cLb;
            if (obj == null) {
                return null;
            }
            Panel dAx = aur.dAx();
            Player dL = ((Associate) obj).mo2077dL();
            dAx.mo4917cf("contact-name").setText(dL.getName());
            CorporationLogo bFK = dL.dxy().bFK();
            if (bFK != null) {
                dAx.mo4915cd("contact-corp").mo16825aw("res://imageset_corpimages/material/" + bFK.aOQ());
            } else {
                dAx.mo4915cd("contact-corp").cCy();
            }
            JLabel cf = dAx.mo4917cf("contact-location");
            Actor bhE = dL.bhE();
            String translate = FriendsManagerAddon.this.f9876kj.translate("Space");
            if (bhE != null) {
                if (!C1298TD.m9830t(bhE).bFT()) {
                    if (bhE instanceof Station) {
                        str = String.valueOf(((Station) bhE).getName()) + " - " + ((Station) bhE).cLb().mo21665ke().get() + " - " + ((Station) bhE).mo960Nc().mo3250ke().get();
                    } else if ((bhE instanceof Ship) && (cLb = ((Ship) bhE).cLb()) != null) {
                        str = cLb.mo21665ke().get();
                    }
                    cf.setText(str);
                    return dAx;
                }
                ((Actor) C3582se.m38985a(bhE, (C6144ahM<?>) new C4386a())).cML();
            }
            str = translate;
            cf.setText(str);
            return dAx;
        }

        /* renamed from: taikodom.addon.friends.FriendsManagerAddon$j$a */
        class C4386a implements C6144ahM<Void> {
            C4386a() {
            }

            /* renamed from: a */
            public void mo1931n(Void voidR) {
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }
        }
    }

    /* renamed from: taikodom.addon.friends.FriendsManagerAddon$k */
    /* compiled from: a */
    private class C4387k extends C5517aMj {


        private C4387k() {
        }

        /* synthetic */ C4387k(FriendsManagerAddon friendsManagerAddon, C4387k kVar) {
            this();
        }

        /* renamed from: a */
        public Component mo1307a(aUR aur, JList jList, Object obj, int i, boolean z, boolean z2) {
            if (obj == null) {
                return null;
            }
            Player aku = (Player) obj;
            Panel dAx = aur.dAx();
            dAx.mo4917cf("contact-name").setText(aku.getName());
            CorporationLogo bFK = aku.dxy().bFK();
            if (bFK == null) {
                return dAx;
            }
            dAx.mo4915cd("contact-corp").mo16825aw("res://imageset_corpimages/material/" + bFK.aOQ());
            return dAx;
        }
    }
}
