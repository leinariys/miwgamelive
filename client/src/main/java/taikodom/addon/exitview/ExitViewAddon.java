package taikodom.addon.exitview;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C1532WW;
import game.network.message.externalizable.C3689to;
import game.script.PlayerController;
import logic.IAddonSettings;
import logic.aaa.C1399UW;
import logic.aaa.C6105agZ;
import logic.ui.item.InternalFrame;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;
import taikodom.addon.config.ConfigAddon;
import taikodom.addon.help.TmpHelpAddon;
import taikodom.addon.messagedialog.MessageDialogAddon;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@TaikodomAddon("taikodom.addon.exitview")
/* compiled from: a */
public class ExitViewAddon implements C2495fo {
    private static final String aDD = "exitmenu.xml";
    private static final String hsU = "exitmenu.css";
    /* access modifiers changed from: private */
    public C0375FA hsV = null;
    /* renamed from: kj */
    public IAddonProperties f9870kj;
    /* renamed from: nM */
    public InternalFrame f9871nM;
    private boolean aZR = false;
    private C6622aqW dcE;
    private C6124ags<C1399UW> hsW;
    private C6124ags<C1532WW> hsX;
    private ActionListener hsY;
    private ActionListener hsZ;
    /* access modifiers changed from: private */
    private ActionListener hta;
    /* access modifiers changed from: private */
    private ActionListener htb;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f9870kj = addonPropertie;
        this.f9870kj.bHv().mo16790b((Object) this, hsU);
        this.dcE = new C4360c("exitmenu", this.f9870kj.translate("Exit menu"), "exit");
        m43386iG();
        this.f9870kj.aVU().mo13965a(C1399UW.class, this.hsW);
        this.f9870kj.aVU().mo13965a(C1532WW.class, this.hsX);
        this.f9870kj.mo2317P(this);
    }

    public void stop() {
        m43376Ia();
        removeListeners();
    }

    @C2602hR(mo19255zf = "EXIT", mo19256zg = "general")
    /* renamed from: iA */
    public void mo23804iA(boolean z) {
        if (!z) {
            return;
        }
        if (this.hsV != null) {
            this.hsV.mo2065kT();
            this.hsV = null;
            return;
        }
        C3689to toVar = new C3689to();
        this.f9870kj.aVU().mo13975h(toVar);
        if (!toVar.adD() && this.f9870kj.getEngineGame().getTaikodom() != null) {
            this.dcE.mo315a(new Object[0]);
        }
    }

    /* renamed from: iG */
    private void m43386iG() {
        this.hsW = new C4361d();
        this.hsY = new C4362e();
        this.hsZ = new C4363f();
        this.hta = new C4364g();
        this.htb = new C4357a();
        this.hsX = new C4359b();
    }

    private void cAW() {
        this.f9871nM.mo4913cb("return").addActionListener(this.hsY);
        this.f9871nM.mo4913cb("config").addActionListener(this.hsZ);
        this.f9871nM.mo4913cb("help").addActionListener(this.hta);
        this.f9871nM.mo4913cb("exit").addActionListener(this.htb);
    }

    private void removeListeners() {
        this.f9870kj.aVU().mo13968b(C1399UW.class, this.hsW);
        this.f9870kj.aVU().mo13968b(C1532WW.class, this.hsX);
        this.f9870kj.mo2318Q(this);
    }

    /* access modifiers changed from: private */
    /* renamed from: Im */
    public void m43377Im() {
        this.f9871nM = this.f9870kj.bHv().mo16792bN(aDD);
        this.f9871nM.pack();
        this.f9871nM.center();
        this.f9871nM.setVisible(true);
        cAW();
        PlayerController alb = this.f9870kj.alb();
        if (alb == null) {
            return;
        }
        if (this.f9870kj.getPlayer() == null || this.f9870kj.getPlayer().bQB() || !alb.anX()) {
            this.aZR = false;
            return;
        }
        alb.mo22059aR(false);
        this.aZR = true;
    }

    /* access modifiers changed from: private */
    /* renamed from: Ia */
    public void m43376Ia() {
        if (this.f9871nM != null) {
            this.f9871nM.close();
            this.f9871nM = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: gq */
    public void m43385gq() {
        m43376Ia();
        cPM();
    }

    /* access modifiers changed from: private */
    public void cPM() {
        if (this.f9870kj.getPlayer() != null && !this.f9870kj.getPlayer().bQB() && this.aZR && !this.f9870kj.alb().anX() && !this.f9870kj.getPlayer().bQB()) {
            this.aZR = false;
            this.f9870kj.alb().mo22059aR(false);
        }
    }

    /* renamed from: taikodom.addon.exitview.ExitViewAddon$c */
    /* compiled from: a */
    class C4360c extends C6622aqW {


        C4360c(String str, String str2, String str3) {
            super(str, str2, str3);
        }

        /* renamed from: a */
        public void mo315a(Object... objArr) {
            if (ExitViewAddon.this.f9871nM == null) {
                ExitViewAddon.this.m43377Im();
            } else {
                ExitViewAddon.this.m43385gq();
            }
        }

        public boolean isActive() {
            return ExitViewAddon.this.f9871nM != null && ExitViewAddon.this.f9871nM.isVisible();
        }

        public boolean isEnabled() {
            return true;
        }
    }

    /* renamed from: taikodom.addon.exitview.ExitViewAddon$d */
    /* compiled from: a */
    class C4361d extends C6124ags<C1399UW> {
        C4361d() {
        }

        /* renamed from: a */
        public boolean updateInfo(C1399UW uw) {
            ExitViewAddon.this.hsV = uw.erh;
            return false;
        }
    }

    /* renamed from: taikodom.addon.exitview.ExitViewAddon$e */
    /* compiled from: a */
    class C4362e implements ActionListener {
        C4362e() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ExitViewAddon.this.m43385gq();
        }
    }

    /* renamed from: taikodom.addon.exitview.ExitViewAddon$f */
    /* compiled from: a */
    class C4363f implements ActionListener {
        C4363f() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ExitViewAddon.this.f9870kj.aVU().mo13975h(new C6105agZ());
            ExitViewAddon.this.m43376Ia();
        }
    }

    /* renamed from: taikodom.addon.exitview.ExitViewAddon$g */
    /* compiled from: a */
    class C4364g implements ActionListener {
        C4364g() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((TmpHelpAddon) ExitViewAddon.this.f9870kj.mo11830U(TmpHelpAddon.class)).open();
            ExitViewAddon.this.m43385gq();
        }
    }

    /* renamed from: taikodom.addon.exitview.ExitViewAddon$a */
    class C4357a implements ActionListener {
        C4357a() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            ((MessageDialogAddon) ExitViewAddon.this.f9870kj.mo11830U(MessageDialogAddon.class)).mo24307a(ExitViewAddon.this.f9870kj.translate("Are you sure you want to exit the game?"), (aEP) new C4358a(), ExitViewAddon.this.f9870kj.translate("cancel"), ExitViewAddon.this.f9870kj.translate("Ok"));
        }

        /* renamed from: taikodom.addon.exitview.ExitViewAddon$a$a */
        class C4358a extends aEP {
            C4358a() {
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public void mo8596a(int i, String str, boolean z) {
                if (i == 1) {
                    ExitViewAddon.this.f9870kj.getEngineGame().cleanUp();
                    ExitViewAddon.this.f9870kj.getEngineGame().disconnect();
                    ExitViewAddon.this.m43385gq();
                    ExitViewAddon.this.f9870kj.getEngineGame().exit();
                }
            }

            /* access modifiers changed from: protected */
            /* renamed from: yD */
            public C0939Nn.C0940a mo8601yD() {
                return C0939Nn.C0940a.CONFIRM;
            }
        }
    }

    /* renamed from: taikodom.addon.exitview.ExitViewAddon$b */
    /* compiled from: a */
    class C4359b extends C6124ags<C1532WW> {
        C4359b() {
        }

        /* renamed from: a */
        public boolean updateInfo(C1532WW ww) {
            if (!ww.bEZ().equals(ConfigAddon.class) || !ww.bEY().equals(C1532WW.C1533a.CLOSED)) {
                return false;
            }
            ExitViewAddon.this.cPM();
            return false;
        }
    }
}
