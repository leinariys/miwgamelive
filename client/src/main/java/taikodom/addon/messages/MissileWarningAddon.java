package taikodom.addon.messages;

import game.script.nls.NLSConsoleAlert;
import game.script.nls.NLSManager;
import logic.IAddonSettings;
import logic.aPE;
import logic.aaa.C1274Sq;
import logic.aaa.C2783jw;
import logic.ui.C2698il;
import logic.ui.ComponentManager;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;

@TaikodomAddon("taikodom.addon.messages")
@Deprecated
/* compiled from: a */
public class MissileWarningAddon implements C2495fo {
    /* access modifiers changed from: private */
    public int aYk;
    /* access modifiers changed from: private */
    public C2698il aYl;
    private JLabel aYm;
    private C3428rT<C2783jw> aYn = new C6264ajc(this);
    private C3428rT<C1274Sq> aYo = new C6262aja(this);

    /* renamed from: kj */
    private IAddonProperties f10114kj;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10114kj = addonPropertie;
        this.aYk = 0;
        m44593XY();
        this.f10114kj.aVU().mo13965a(C2783jw.class, this.aYn);
        this.f10114kj.aVU().mo13965a(C1274Sq.class, this.aYo);
    }

    public void stop() {
        m44595Ya();
        this.aYl = null;
        this.aYm = null;
    }

    /* renamed from: XY */
    private void m44593XY() {
        this.aYl = (C2698il) this.f10114kj.bHv().mo16794bQ("warning.xml");
        this.aYm = this.aYl.mo4917cf("warningMessage");
        this.aYm.setText(((NLSConsoleAlert) this.f10114kj.ala().aIY().mo6310c(NLSManager.C1472a.CONSOLEALERT)).bcs().get());
        ComponentManager.getCssHolder(this.aYm).setAttribute("class", "missile");
        int max = Math.max(this.aYm.getPreferredSize().height, 64);
        this.aYl.setSize(Math.max(this.aYm.getPreferredSize().width, 500), max);
        if (this.aYl instanceof JPanel) {
            this.aYl.doLayout();
        }
        Dimension screenSize = this.f10114kj.bHv().getScreenSize();
        this.aYl.setLocation((int) (((float) (screenSize.width - this.aYl.getSize().width)) * 0.5f), (int) (((float) screenSize.height) * 0.23f));
        this.aYl.setVisible(false);
    }

    /* access modifiers changed from: private */
    /* renamed from: XZ */
    public void m44594XZ() {
        this.aYl.setVisible(true);
        this.f10114kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_PURSUIT_LOOP));
        this.f10114kj.mo11833a(500, (aPE) new C4725a());
    }

    /* access modifiers changed from: private */
    /* renamed from: Ya */
    public void m44595Ya() {
        this.aYl.setVisible(false);
        this.f10114kj.aVU().mo13975h(new C6811auD(C3667tV.MISSILE_PURSUIT_LOOP, true));
    }

    /* renamed from: taikodom.addon.messages.MissileWarningAddon$a */
    class C4725a implements aPE {
        C4725a() {
        }

        /* renamed from: fg */
        public boolean mo6618fg(long j) {
            if (MissileWarningAddon.this.aYl.isVisible()) {
                return true;
            }
            return false;
        }
    }
}
