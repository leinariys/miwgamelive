package taikodom.addon.messages;

import game.network.message.externalizable.C6128agw;
import logic.IAddonSettings;
import logic.aaa.*;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.item.C2830kk;
import p001a.*;
import taikodom.addon.C2495fo;
import taikodom.addon.IAddonProperties;
import taikodom.addon.TaikodomAddon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

@TaikodomAddon("taikodom.addon.messages")
/* compiled from: a */
public class SystemMessageAddon implements C2495fo {

    /* renamed from: Xf */
    private static final int f10115Xf = 3;
    /* renamed from: Kr */
    public ArrayList<JLabel> f10117Kr = new ArrayList<>();
    /* access modifiers changed from: private */
    /* renamed from: Xk */
    public Timer f10123Xk = null;
    /* renamed from: Xl */
    public C4727b f10124Xl = null;
    /* renamed from: DV */
    private Panel f10116DV;
    /* renamed from: SZ */
    private aDX f10118SZ;
    /* renamed from: Xg */
    private HashMap<JLabel, Color> f10119Xg = new HashMap<>();
    /* renamed from: Xh */
    private Panel f10120Xh;
    /* access modifiers changed from: private */
    /* renamed from: Xi */
    private int f10121Xi;
    /* access modifiers changed from: private */
    /* renamed from: Xj */
    private C0929Nd f10122Xj;
    /* renamed from: Xm */
    private C6124ags<C0284Df> f10125Xm = new C5725aUj(this);

    /* renamed from: Xn */
    private C6124ags<C2733jJ> f10126Xn = new C5724aUi(this);

    /* renamed from: Xo */
    private C6124ags<C0575Hy> f10127Xo = new C5722aUg(this);

    /* renamed from: Xp */
    private C6124ags<C5850abe> f10128Xp = new C5720aUe(this);

    /* renamed from: kj */
    private IAddonProperties f10129kj;
    private JLayeredPane layeredPane;

    /* renamed from: a */
    public void initAddon(IAddonSettings addonPropertie) {
        this.f10129kj = addonPropertie;
        init();
    }

    private void init() {
        this.f10129kj.aVU().mo13965a(C0284Df.class, this.f10125Xm);
        this.f10129kj.aVU().mo13965a(C2733jJ.class, this.f10126Xn);
        this.f10129kj.aVU().mo13965a(C0575Hy.class, this.f10127Xo);
        this.f10129kj.aVU().mo13965a(C5850abe.class, this.f10128Xp);
        this.f10120Xh = (Panel) this.f10129kj.bHv().mo16794bQ("system-messages.xml");
        this.f10116DV = this.f10120Xh.mo4915cd("content");
        this.f10116DV.setLayout((LayoutManager) null);
        Container parent = this.f10120Xh.getParent();
        while (parent != null && !(parent instanceof JLayeredPane)) {
            parent = parent.getParent();
        }
        if (parent != null) {
            this.layeredPane = (JLayeredPane) parent;
        }
        this.f10120Xh.pack();
        this.f10122Xj = new C0929Nd(this, this.f10120Xh, 0);
        this.f10129kj.aVU().publish(this.f10122Xj);
        this.f10121Xi = this.f10120Xh.getHeight() / 3;
    }

    /* renamed from: a */
    private void m44607a(C6128agw agw) {
        if (this.f10124Xl == null) {
            this.f10124Xl = new C4727b(this, (C4727b) null);
            this.f10123Xk = new Timer(1000, this.f10124Xl);
            this.f10123Xk.start();
        }
        this.f10124Xl.mo24320c(agw);
    }

    /* renamed from: a */
    public boolean mo24316a(C5850abe abe) {
        return mo24317a(abe.bNc(), (C4732f) new C4731e(abe));
    }

    /* renamed from: b */
    public boolean mo24318b(C6128agw agw) {
        return mo24317a(agw, (C4732f) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo24317a(C6128agw agw, C4732f fVar) {
        if (IBaseUiTegXml.initBaseUItegXML().anx()) {
            m44607a(agw);
            return false;
        }
        JLabel jLabel = new JLabel();
        jLabel.setName(agw.bWP());
        boolean z = this.f10117Kr.size() >= 2;
        while (this.f10117Kr.size() > 2) {
            JLabel remove = this.f10117Kr.remove(0);
            this.f10119Xg.remove(remove);
            remove.setVisible(false);
        }
        int size = this.f10117Kr.size() - 1;
        while (size >= 0) {
            JLabel jLabel2 = this.f10117Kr.get(size);
            if (agw.fyq && jLabel2.getName().equals(jLabel.getName())) {
                return true;
            }
            IComponentManager e = ComponentManager.getCssHolder(jLabel2);
            e.setAttribute("class", String.valueOf((!z || size != 0) ? "small " : "mini ") + this.f10119Xg.get(jLabel2).toString());
            e.mo13045Vk();
            size--;
        }
        this.f10117Kr.add(jLabel);
        IComponentManager e2 = ComponentManager.getCssHolder(jLabel);
        e2.setAttribute("class", "big " + agw.getColor().toString());
        e2.mo13045Vk();
        this.f10119Xg.put(jLabel, agw.getColor());
        if (agw.fyp) {
            m44608a(jLabel, agw, fVar);
        } else {
            m44614b(jLabel, agw, fVar);
        }
        if (agw instanceof C0575Hy) {
            ((C0575Hy) agw).mo2708a((C1166RG) new C4726a(jLabel));
        }
        m44604Bg();
        return true;
    }

    /* renamed from: bQ */
    private int m44615bQ(int i) {
        return m44618i(i, this.f10117Kr.size());
    }

    /* renamed from: i */
    private int m44618i(int i, int i2) {
        return (this.f10120Xh.getHeight() - (this.f10121Xi * (i2 - i))) - this.f10116DV.getInsets().bottom;
    }

    /* renamed from: bR */
    private void m44616bR(int i) {
        if (i >= 0 && i <= this.f10117Kr.size()) {
            JLabel jLabel = this.f10117Kr.get(i);
            jLabel.setBounds(0, m44615bQ(i), this.f10116DV.getWidth(), this.f10121Xi);
            this.f10116DV.add(jLabel);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: Bg */
    public void m44604Bg() {
        if (this.f10116DV != null) {
            for (Component component : this.f10116DV.getComponents()) {
                if (!(component.isVisible() || this.f10120Xh == null || this.f10120Xh.getParent() == null)) {
                    this.f10120Xh.getParent().remove(component);
                }
            }
            int size = this.f10117Kr.size();
            for (int i = size - 1; i >= 0; i--) {
                m44616bR(i);
            }
            if (this.layeredPane != null) {
                this.layeredPane.moveToFront(this.f10120Xh);
            }
            if (size > 0) {
                m44605X(true);
            }
            this.f10120Xh.validate();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: X */
    public void m44605X(boolean z) {
        if (z != this.f10120Xh.isVisible()) {
            this.f10120Xh.setVisible(z);
        }
    }

    /* renamed from: a */
    private void m44608a(JLabel jLabel, C6128agw agw, C4732f fVar) {
        jLabel.setText(agw.bWP());
        if (fVar != null) {
            fVar.dvy();
        }
        if (!(agw instanceof C0575Hy)) {
            m44613b(jLabel);
        }
    }

    /* renamed from: b */
    private void m44614b(JLabel jLabel, C6128agw agw, C4732f fVar) {
        jLabel.setText(agw.bWP());
        new aDX(jLabel, "[0.." + jLabel.getText().length() + "] dur " + (jLabel.getText().length() * 80), C2830kk.asT, new C4730d(fVar, jLabel));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m44613b(JLabel jLabel) {
        Timer timer = new Timer(C1298TD.hFl, new C4728c(jLabel));
        timer.setRepeats(false);
        timer.start();
    }

    public void stop() {
        if (this.f10120Xh != null) {
            this.f10120Xh.destroy();
        }
        this.f10129kj.aVU().mo13974g(this.f10122Xj);
    }

    /* renamed from: taikodom.addon.messages.SystemMessageAddon$f */
    /* compiled from: a */
    interface C4732f {
        void dvy();
    }

    /* renamed from: taikodom.addon.messages.SystemMessageAddon$b */
    /* compiled from: a */
    private class C4727b implements ActionListener {
        C5850abe euZ;

        private C4727b() {
            this.euZ = new C5850abe();
        }

        /* synthetic */ C4727b(SystemMessageAddon systemMessageAddon, C4727b bVar) {
            this();
        }

        /* renamed from: c */
        public void mo24320c(C6128agw agw) {
            this.euZ.mo12495d(agw);
        }

        public void actionPerformed(ActionEvent actionEvent) {
            if (!IBaseUiTegXml.initBaseUItegXML().anx()) {
                SystemMessageAddon.this.mo24316a(this.euZ);
                if (SystemMessageAddon.this.f10123Xk != null) {
                    SystemMessageAddon.this.f10123Xk.stop();
                }
                SystemMessageAddon.this.f10124Xl = null;
            }
        }
    }

    /* renamed from: taikodom.addon.messages.SystemMessageAddon$e */
    /* compiled from: a */
    class C4731e implements C4732f {
        private final /* synthetic */ C5850abe iOn;

        C4731e(C5850abe abe) {
            this.iOn = abe;
        }

        public void dvy() {
            if (!this.iOn.isEmpty()) {
                SystemMessageAddon.this.mo24317a(this.iOn.bNc(), (C4732f) this);
            }
        }
    }

    /* renamed from: taikodom.addon.messages.SystemMessageAddon$a */
    class C4726a implements C1166RG {
        private JLabel label;

        public C4726a(JLabel jLabel) {
            this.label = jLabel;
        }

        /* renamed from: a */
        public void mo5160a(String str, Object... objArr) {
            this.label.setText(str);
        }

        /* renamed from: b */
        public void mo5161b(String str, Object... objArr) {
            this.label.setVisible(false);
        }
    }

    /* renamed from: taikodom.addon.messages.SystemMessageAddon$c */
    /* compiled from: a */
    private class C4728c implements ActionListener {
        /* access modifiers changed from: private */
        public JLabel iLc;

        public C4728c(JLabel jLabel) {
            this.iLc = jLabel;
        }

        public void actionPerformed(ActionEvent actionEvent) {
            new aDX(this.iLc, "[255..0] dur 1000", C2830kk.asS, new C4729a());
            SystemMessageAddon.this.m44604Bg();
        }

        /* renamed from: taikodom.addon.messages.SystemMessageAddon$c$a */
        class C4729a implements C0454GJ {
            C4729a() {
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                jComponent.setVisible(false);
                SystemMessageAddon.this.f10117Kr.remove(C4728c.this.iLc);
                if (SystemMessageAddon.this.f10117Kr.size() == 0) {
                    SystemMessageAddon.this.m44605X(false);
                }
            }
        }
    }

    /* renamed from: taikodom.addon.messages.SystemMessageAddon$d */
    /* compiled from: a */
    class C4730d implements C0454GJ {
        private final /* synthetic */ C4732f iOl;
        private final /* synthetic */ JLabel iOm;

        C4730d(C4732f fVar, JLabel jLabel) {
            this.iOl = fVar;
            this.iOm = jLabel;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            if (this.iOl != null) {
                this.iOl.dvy();
            }
            SystemMessageAddon.this.m44613b(this.iOm);
        }
    }
}
