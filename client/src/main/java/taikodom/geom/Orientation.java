package taikodom.geom;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;

import javax.vecmath.Quat4f;

@Deprecated
/* compiled from: a */
public final class Orientation extends Quat4fWrap {
    public static final float PRECISION = 1.0E-5f;
    private static final long serialVersionUID = -3680864844918856070L;

    public Orientation() {
    }

    public Orientation(Vec3f vec3f, double d) {
        super(vec3f, d);
    }

    public Orientation(double d, double d2, double d3) {
        super(d, d2, d3);
    }

    public Orientation(Quat4fWrap aoy) {
        super(aoy);
    }

    public Orientation(Matrix4fWrap ajk) {
        super(ajk);
    }

    public Orientation(float f, float f2, float f3, float f4) {
        super(f2, f3, f4, f);
    }

    /* renamed from: a */
    public static Orientation m45808a(Vec3f vec3f, double d) {
        return new Orientation(vec3f, d);
    }

    /* renamed from: b */
    public static Orientation m45810b(Vec3f vec3f, double d) {
        return new Orientation(vec3f, (3.141592653589793d * d) / 180.0d);
    }

    /* renamed from: a */
    public static Orientation m45807a(double d, double d2, double d3) {
        return new Orientation(d, d2, d3);
    }

    /* renamed from: b */
    public static Orientation m45809b(double d, double d2, double d3) {
        return new Orientation((d * 3.141592653589793d) / 180.0d, (d2 * 3.141592653589793d) / 180.0d, (3.141592653589793d * d3) / 180.0d);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Quat4fWrap)) {
            return false;
        }
        return mo24726b((Quat4fWrap) obj);
    }

    /* renamed from: b */
    public boolean mo24726b(Quat4fWrap aoy) {
        return m45815d(this.x, aoy.x) && m45815d(this.y, aoy.y) && m45815d(this.z, aoy.z) && m45815d(this.w, aoy.w);
    }

    /* renamed from: d */
    private boolean m45815d(float f, float f2) {
        return Math.abs(f - f2) < 1.0E-5f;
    }

    /* renamed from: d */
    public Orientation mo24727d(Orientation orientation) {
        return new Orientation(super.mo15232d((Quat4fWrap) orientation));
    }

    /* renamed from: yY */
    public Orientation mo15256zd() {
        return new Orientation(super.mo15256zd());
    }

    /* renamed from: yZ */
    public Orientation mo15255zc() {
        return new Orientation(super.mo15255zc());
    }

    /* renamed from: za */
    public Orientation mo15254zb() {
        return new Orientation(super.mo15254zb());
    }

    /* renamed from: a */
    public Orientation mo24724a(Orientation orientation, float f) {
        return new Orientation(Quat4fWrap.m24422a(this, orientation, f, new Quat4fWrap()));
    }

    /* renamed from: a */
    public Orientation mo24725a(Orientation orientation, Orientation orientation2, float f) {
        return new Orientation(Quat4fWrap.m24422a(orientation, orientation2, f, new Quat4fWrap()));
    }

    /* renamed from: e */
    public Orientation mo24728e(Orientation orientation) {
        return new Orientation(super.mo15233d((Quat4f) orientation));
    }
}
