package taikodom.geom;

import com.hoplon.geometry.Vec3f;
import com.hoplon.geometry.Vec3l;
import game.geometry.Vec3d;
import game.geometry.Vec3dWrap;
import p001a.C2886lU;

@Deprecated
/* compiled from: a */
public class Position extends Vec3d implements C2886lU {
    private Vec3l center = new Vec3l();
    private Vec3f relativePosition = new Vec3f();

    public Position() {
    }

    public Position(double d, double d2, double d3) {
        super(d, d2, d3);
    }

    public Position(Vec3dWrap iv) {
        super(iv);
    }

    public Position(Vec3d ajr) {
        super(ajr);
    }

    public Position(Vec3f vec3f) {
        super(vec3f);
    }

    /* renamed from: aH */
    public void mo20230aH() {
        this.x = ((double) this.center.x) + ((double) this.relativePosition.x);
        this.y = ((double) this.center.y) + ((double) this.relativePosition.y);
        this.z = ((double) this.center.z) + ((double) this.relativePosition.z);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Position) {
            return this.center == ((Position) obj).center && this.relativePosition == ((Position) obj).relativePosition && super.equals(obj);
        }
        return super.equals(obj);
    }
}
