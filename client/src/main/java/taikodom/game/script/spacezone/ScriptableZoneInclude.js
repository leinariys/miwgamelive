/*
 * Include loaded by the ScriptableZone controller
 * @author: Helder
 */

// Mission states array object
var state = new Object();

// current mission state
var current = null;
var currentStateName = null;

// functions to invoke triggers in the current state
function invokeTrigger0(trigger) {
    if (current == null) {
        return null;
    }

    if (current[trigger] == null) {
        return null;
    }

    if (current[trigger] instanceof Function) {
        return current[trigger]();
    }
}

function invokeTrigger1(trigger, param) {
    if (current == null) {
        return null;
    }

    if (current[trigger] == null) {
        return null;
    }

    if (current[trigger] instanceof Function) {
        return current[trigger](param);
    }
}

function invokeTrigger2(trigger, param1, param2) {
    if (current == null) {
        return null;
    }

    if (current[trigger] == null) {
        return null;
    }

    if (current[trigger] instanceof Function) {
        return current[trigger](param1, param2);
    }
}

function invokeTrigger3(trigger, param1, param2, param3) {
    if (current == null) {
        return null;
    }

    if (current[trigger] == null) {
        return null;
    }

    if (current[trigger] instanceof Function) {
        return current[trigger](param1, param2, param3);
    }
}

// function to change state
function gotoState(goalState) {
    current = state[goalState];

    if (current.onEnterState != null) {
        current.onEnterState();
    }
}

// initial evaluation of the game.script
eval(script);
gotoState(initialState);
