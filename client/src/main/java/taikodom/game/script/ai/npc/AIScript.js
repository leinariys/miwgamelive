/*
 * AIScript loaded by the ScriptableAIController.
 * Controls every ai game.script.
 * @author: Islon
 */

// AIState array object
state = new Object();

// behaviour package
ai = Packages.taikodom.infra.script.ai.behaviours;

// i18n function
// TODO: internationalization
function i18n(str) {

}

// continuations
_contis = new Object()
_stimes = new Object()

function continuate() {
    var conti = _contis[controller.currentState]
    // already get the continuation, delete the entry from the map
    _contis[controller.currentState] = null
    if (conti)
        conti()
}

function getEnd() {
    return _stimes[controller.currentState]
}

// action functions
function wait() {
    if (arguments.length > 0)
        _stimes[controller.currentState] = arguments[0]
    else
        _stimes[controller.currentState] = 0
    _contis[controller.currentState] = new Continuation()
    throw "waiting"
}

function sleep(secs) {
    // do nothing for 'secs' seconds
    var behaves = controller.getBehaviours(controller.currentState)
    controller.cleanBehaviors(controller.currentState)
    var waitbeh = new ai.StopBehavior()
    controller.addBehaviour(controller.currentState, waitbeh)
    wait(secs)
    controller.setBehaviors(controller.currentState, behaves)
}

function moveto(x, y, z) {
    // keep the old behaviors
    var behaves = controller.getBehaviours(controller.currentState)
    controller.cleanBehaviors(controller.currentState)
    var point = new Packages.com.hoplon.geometry.Vec3d(x, y, z)
    var seek = new ai.SeekBehaviour(point, false)
    // make the ship move to the point
    controller.addBehaviour(controller.currentState, seek)
    // wait it arrive
    out.println('start waiting move')
    wait(seek)
    out.println('done waiting move')
    // set the old behaviors
    controller.setBehaviors(controller.currentState, behaves)
}

// setup behaviors
function behaviors() {
    controller.cleanBehaviors(controller.currentState);
    for (var i = 0; i < arguments.length; i++) {
        controller.addBehaviour(controller.currentState, arguments[i]);
    }
}

function gotoState(stat) {
    controller.currentState = controller.getState(stat);
    state[stat].begin();
}

function invokeTrigger(trigger) {
    var stat = state[controller.currentState.name];
    if (stat[trigger] instanceof Function)
        stat[trigger]()
}

// behaviors list
function fight() {
    controller.cleanBehaviors(controller.currentState);
    var pursuit = new ai.BetterPursuitBehaviour(controller.getTrackedShip());
    pursuit.setDistanceToShip(200);
    pursuit.setPredictMoviment(true);
    var shoot = new ai.ShootEnemyBehaviour(controller.getTrackedShip());
    shoot.setAngleToShoot(12);
    shoot.setPredictMoviment(true);
    var dodge = new ai.DodgeBulletsBehaviour();
    dodge.setAngleToDodge(12);
    dodge.setDistanceToDodge(2000);
    dodge.setReactionTime(0.3);
    dodge.setDisabled(false);
    controller.addBehaviour(controller.currentState, new ai.GroupHelperBehaviour(pursuit, shoot));
    controller.addBehaviour(controller.currentState, dodge);
}

function wander() {
    controller.cleanBehaviors(controller.currentState);
    var avoid = new ai.AvoidColisionBehaviour();
    var avoidO = new ai.AvoidObstaclesBehaviour();
    var wand = new ai.WanderBehaviour();
    var dshoot = new ai.DontShootBehaviour();
    var group = new ai.GroupHelperBehaviour(avoidO, wand, dshoot);
    controller.addBehaviour(controller.currentState, avoid);
    controller.addBehaviour(controller.currentState, group);
}

function returnSphere() {
    controller.cleanBehaviors(controller.currentState);
    var avoid = new ai.AvoidColisionBehaviour();
    var avoidO = new ai.AvoidObstaclesBehaviour();
    var seek = new ai.SeekBehaviour(controller.center);
    var dshoot = new ai.DontShootBehaviour();
    var group = new ai.GroupHelperBehaviour(avoidO, seek, dshoot);
    controller.addBehaviour(controller.currentState, avoid);
    controller.addBehaviour(controller.currentState, group);
}

/**
 * process the user/content editor game.script.
 */
function processScript(userScript) {
    // evaluate the game.script
    eval(userScript);

    // here the magic happens!

    for (stateName in state) {
        var st = state[stateName];
        var stat = new Packages.taikodom.game.script.ai.AIState(stateName);

        // check if is an auto state
        if (st.auto) {
            stat.auto = true;
            controller.setCurrentState(stat);
        }

        controller.addState(stat);

        // checking triggers
        if (st.validTarget)
            controller.addTrigger(stat, "validTarget");
        if (st.invalidTarget)
            controller.addTrigger(stat, "invalidTarget");
        if (st.outsideSphere)
            controller.addTrigger(stat, "outsideSphere");
        if (st.insideSphere)
            controller.addTrigger(stat, "insideSphere");
    }
}

// call the processScript
processScript(script);

// init the state
state[controller.currentState.name].begin();