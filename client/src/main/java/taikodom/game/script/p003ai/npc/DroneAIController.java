package taikodom.game.script.p003ai.npc;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.ai.npc.AIControllerType;
import game.script.ai.npc.DroneAIControllerType;
import game.script.ai.npc.TrackShipAIController;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.abb.*;
import logic.baa.*;
import logic.data.mbean.C6097agR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;

import javax.vecmath.Tuple3d;
import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: taikodom.game.script.ai.npc.DroneAIController */
/* compiled from: a */
public class DroneAIController extends TrackShipAIController implements C1616Xf, aJO {
    public static final C5663aRz _f_center = null;
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_configureBehaviours_0020_0028_0029V = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C5663aRz _f_dodgeAngle = null;
    public static final C5663aRz _f_dodgeDisabled = null;
    public static final C5663aRz _f_dodgeDistance = null;
    public static final C5663aRz _f_dodgeReactionTime = null;
    /* renamed from: _f_getCenter_0020_0028_0029Lcom_002fhoplon_002fgeometry_002fVec3d_003b */
    public static final C2491fm f10322xdc7de0cc = null;
    /* renamed from: _f_getType_0020_0028_0029Ltaikodom_002fgame_002fscript_002fai_002fnpc_002fAIControllerType_003b */
    public static final C2491fm f10323xed6e425 = null;
    /* renamed from: _f_getType_0020_0028_0029Ltaikodom_002fgame_002fscript_002fai_002fnpc_002fDroneAIControllerType_003b */
    public static final C2491fm f10324x9c160ae3 = null;
    public static final C5663aRz _f_initialReactionTime = null;
    public static final C2491fm _f_isValidTrackedShip_0020_0028_0029Z = null;
    public static final C2491fm _f_offloadInit_0020_0028_0029V = null;
    public static final C5663aRz _f_pursuitDistance = null;
    public static final C5663aRz _f_pursuitPredictMovimet = null;
    public static final C5663aRz _f_radius2 = null;
    /* renamed from: _f_setCenter_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003b_0029V */
    public static final C2491fm f10325x6107980 = null;
    public static final C2491fm _f_setRadius2_0020_0028F_0029V = null;
    public static final C2491fm _f_setRadius_0020_0028F_0029V = null;
    /* renamed from: _f_setTrackedPilot_0020_0028Ltaikodom_002fgame_002fscript_002fCharacter_003b_0029V */
    public static final C2491fm f10326xf8357b61 = null;
    /* renamed from: _f_setTrackedShip_0020_0028Ltaikodom_002fgame_002fscript_002fship_002fShip_003b_0029V */
    public static final C2491fm f10327x30fdc3fb = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final C5663aRz _f_shootAngle = null;
    public static final C5663aRz _f_shootPredictMovimet = null;
    public static final C5663aRz _f_startTime = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_updateBehaviors_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static final int FIGHTING = 1;
    public static final int RETURNING_TO_SPHERE = 2;
    public static final int WAITING = 0;
    public static final int WANDERING = 3;
    private static final int RADIUS_ALLOWED_OUTSIDE_SPHERE_SQUARED = 100000;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1c9bf9fc59619784448714e8f5b3bc72", aum = 9)
    private static Vec3d center;
    @C0064Am(aul = "bd86b27f4e83a67a4376cdc24ba963cf", aum = 4)
    @C5454aJy("Dodge Bullets - Max angle beetween enemy direction and AI ship (º)")
    private static float dodgeAngle = 12.0f;
    @C0064Am(aul = "50c4261e3302f8298174ec3e55bfea00", aum = 5)
    @C5454aJy("Dodge Bullets - Disabled")
    private static boolean dodgeDisabled = false;
    @C0064Am(aul = "a5884c99335c0a8407ade29fb5870d8c", aum = 3)
    @C5454aJy("Dodge Bullets - Distance to start dodgin")
    private static float dodgeDistance = 2000.0f;
    @C0064Am(aul = "76b86616644e9e43adea347ef4e45377", aum = 2)
    @C5454aJy("Dodge Bullets - Reaction time (s)")
    private static float dodgeReactionTime = 0.5f;
    @C0064Am(aul = "ba9e2042e11c56cdd27f4eea3b16f2b8", aum = 8)
    @C5454aJy("Initial reaction time before going to the fight")
    private static float initialReactionTime = 2.0f;
    @C0064Am(aul = "80d9cfbd99b2604a5e557fe6f5d2d550", aum = 0)
    @C5454aJy("Pursuit - Desired distance to target")
    private static float pursuitDistance = 300.0f;
    @C0064Am(aul = "0c05e37437d78dd34803de18c9c04663", aum = 1)
    @C5454aJy("Pursuit - Predict moviment")
    private static boolean pursuitPredictMovimet = true;
    @C0064Am(aul = "94c1c6262c401c7ccc61cc034f0b4c6d", aum = 10)
    private static float radius2;
    @C0064Am(aul = "b46ec6bb2d1b87776665fe57511c5304", aum = 6)
    @C5454aJy("Shoot - Angle to start shooting (º)")
    private static float shootAngle = 15.0f;
    @C0064Am(aul = "dd38a465d05ec5edbc912cb1b63b67f1", aum = 7)
    @C5454aJy("Shoot - Predict target moviment")
    private static boolean shootPredictMovimet = true;
    @C0064Am(aul = "81f8d696a198ba8f66b2be341d1702b5", aum = 11)
    private static long startTime;

    static {
        m45762V();
    }

    public transient C6822auO avoidObstaclesBehaviour;
    public transient C2653iB dodgeBulletsBehaviour;
    public transient aER pursuitBehaviour;
    public transient C6305akR shootEnemyBehaviour;

    public DroneAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public DroneAIController(DroneAIControllerType aco) {
        super((C5540aNg) null);
        super._m_script_init(aco);
    }

    public DroneAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m45762V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TrackShipAIController._m_fieldCount + 12;
        _m_methodCount = TrackShipAIController._m_methodCount + 17;
        int i = TrackShipAIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(DroneAIController.class, "80d9cfbd99b2604a5e557fe6f5d2d550", i);
        _f_pursuitDistance = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(DroneAIController.class, "0c05e37437d78dd34803de18c9c04663", i2);
        _f_pursuitPredictMovimet = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(DroneAIController.class, "76b86616644e9e43adea347ef4e45377", i3);
        _f_dodgeReactionTime = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(DroneAIController.class, "a5884c99335c0a8407ade29fb5870d8c", i4);
        _f_dodgeDistance = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(DroneAIController.class, "bd86b27f4e83a67a4376cdc24ba963cf", i5);
        _f_dodgeAngle = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(DroneAIController.class, "50c4261e3302f8298174ec3e55bfea00", i6);
        _f_dodgeDisabled = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(DroneAIController.class, "b46ec6bb2d1b87776665fe57511c5304", i7);
        _f_shootAngle = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(DroneAIController.class, "dd38a465d05ec5edbc912cb1b63b67f1", i8);
        _f_shootPredictMovimet = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(DroneAIController.class, "ba9e2042e11c56cdd27f4eea3b16f2b8", i9);
        _f_initialReactionTime = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(DroneAIController.class, "1c9bf9fc59619784448714e8f5b3bc72", i10);
        _f_center = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(DroneAIController.class, "94c1c6262c401c7ccc61cc034f0b4c6d", i11);
        _f_radius2 = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(DroneAIController.class, "81f8d696a198ba8f66b2be341d1702b5", i12);
        _f_startTime = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_fields, (Object[]) _m_fields);
        int i14 = TrackShipAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 17)];
        C2491fm a = C4105zY.m41624a(DroneAIController.class, "e5bddda09a699fabcbf5fd27abe8da56", i14);
        _f_step_0020_0028F_0029V = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(DroneAIController.class, "c91cab9991aef83736b2be450d9df543", i15);
        _f_setRadius_0020_0028F_0029V = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(DroneAIController.class, "38894a6ae6c565b64d473cb91f2a254b", i16);
        _f_setRadius2_0020_0028F_0029V = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(DroneAIController.class, "e840c452a3a501262c0a42241668e863", i17);
        f10327x30fdc3fb = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(DroneAIController.class, "cf25f5b0581484fc43b2370de4d05087", i18);
        f10326xf8357b61 = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(DroneAIController.class, "cc13fb7560a486f719b48f3c0c212d73", i19);
        f10322xdc7de0cc = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(DroneAIController.class, "55dfe9d1a3cee6b81c64d4a3d0764839", i20);
        f10325x6107980 = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(DroneAIController.class, "6aa39e33462e3df7790b3e1aa9dff2db", i21);
        _f_start_0020_0028_0029V = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(DroneAIController.class, "4da03537924a875da8a729b840417f07", i22);
        _f_setupBehaviours_0020_0028_0029V = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(DroneAIController.class, "d723da95a298aed0d4ceb93c96dd949a", i23);
        _f_configureBehaviours_0020_0028_0029V = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(DroneAIController.class, "9a674fff187019fd1de56daa07edacd1", i24);
        _f_changeCurrentAction_0020_0028_0029I = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(DroneAIController.class, "fb9bf3fa21d100a4fd5fa38ab31e1171", i25);
        _f_updateBehaviors_0020_0028_0029V = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(DroneAIController.class, "47b9725fabac49d98a0f5011961e4da3", i26);
        _f_isValidTrackedShip_0020_0028_0029Z = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(DroneAIController.class, "d032375d701197bf43e01465325abc38", i27);
        f10324x9c160ae3 = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(DroneAIController.class, "a2a7ab1ec8ed55b6dbc032c89a2cad79", i28);
        _f_offloadInit_0020_0028_0029V = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(DroneAIController.class, "988624b064e44eda95dd1c105599b442", i29);
        _f_dispose_0020_0028_0029V = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(DroneAIController.class, "acd6cf8f4f9c7769e5ade79caa7b2673", i30);
        f10323xed6e425 = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(DroneAIController.class, C6097agR.class, _m_fields, _m_methods);
    }

    /* renamed from: Bk */
    private long m45759Bk() {
        return bFf().mo5608dq().mo3213o(_f_startTime);
    }

    /* renamed from: aI */
    private float m45765aI() {
        return ((DroneAIControllerType) getType()).mo8010aM();
    }

    /* renamed from: aJ */
    private boolean m45766aJ() {
        return ((DroneAIControllerType) getType()).mo8011aO();
    }

    /* renamed from: az */
    private void m45768az(float f) {
        bFf().mo5608dq().mo3150a(_f_radius2, f);
    }

    /* renamed from: b */
    private void m45769b(boolean z) {
        throw new C6039afL();
    }

    private float bwe() {
        return ((DroneAIControllerType) getType()).bwj();
    }

    private float bwf() {
        return ((DroneAIControllerType) getType()).bwl();
    }

    private float bwg() {
        return ((DroneAIControllerType) getType()).bwn();
    }

    private float bwh() {
        return ((DroneAIControllerType) getType()).bwp();
    }

    /* renamed from: bz */
    private void m45770bz(long j) {
        bFf().mo5608dq().mo3184b(_f_startTime, j);
    }

    private boolean cbK() {
        return ((DroneAIControllerType) getType()).cQs();
    }

    private boolean cbL() {
        return ((DroneAIControllerType) getType()).cQu();
    }

    private float cbM() {
        return ((DroneAIControllerType) getType()).cQw();
    }

    private void cbO() {
        switch (bFf().mo6893i(_f_updateBehaviors_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_updateBehaviors_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_updateBehaviors_0020_0028_0029V, new Object[0]));
                break;
        }
        cbN();
    }

    /* renamed from: eU */
    private void m45771eU(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: eV */
    private void m45772eV(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m45773f(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(_f_center, ajr);
    }

    /* renamed from: ht */
    private void m45776ht(float f) {
        throw new C6039afL();
    }

    /* renamed from: hu */
    private void m45777hu(float f) {
        throw new C6039afL();
    }

    /* renamed from: hv */
    private void m45778hv(float f) {
        throw new C6039afL();
    }

    /* renamed from: hw */
    private void m45779hw(float f) {
        throw new C6039afL();
    }

    /* renamed from: i */
    private void m45780i(float f) {
        throw new C6039afL();
    }

    /* renamed from: jy */
    private void m45781jy(float f) {
        throw new C6039afL();
    }

    /* renamed from: zL */
    private Vec3d m45783zL() {
        return (Vec3d) bFf().mo5608dq().mo3214p(_f_center);
    }

    /* renamed from: zM */
    private float m45784zM() {
        return bFf().mo5608dq().mo3211m(_f_radius2);
    }

    @C0064Am(aul = "acd6cf8f4f9c7769e5ade79caa7b2673", aum = 0)
    /* renamed from: zX */
    private AIControllerType m45789zX() {
        return cbQ();
    }

    /* renamed from: J */
    public void mo4588J(Ship fAVar) {
        switch (bFf().mo6893i(f10327x30fdc3fb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f10327x30fdc3fb, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f10327x30fdc3fb, new Object[]{fAVar}));
                break;
        }
        m45760I(fAVar);
    }

    /* renamed from: L */
    public void mo4589L(Character acx) {
        switch (bFf().mo6893i(f10326xf8357b61)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f10326xf8357b61, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f10326xf8357b61, new Object[]{acx}));
                break;
        }
        m45761K(acx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6097agR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TrackShipAIController._m_methodCount) {
            case 0:
                m45767ay(((Float) args[0]).floatValue());
                return null;
            case 1:
                m45763aA(((Float) args[0]).floatValue());
                return null;
            case 2:
                m45764aB(((Float) args[0]).floatValue());
                return null;
            case 3:
                m45760I((Ship) args[0]);
                return null;
            case 4:
                m45761K((Character) args[0]);
                return null;
            case 5:
                return m45785zN();
            case 6:
                m45775g((Vec3d) args[0]);
                return null;
            case 7:
                m45782qN();
                return null;
            case 8:
                m45786zP();
                return null;
            case 9:
                m45787zR();
                return null;
            case 10:
                return new Integer(m45788zT());
            case 11:
                cbN();
                return null;
            case 12:
                return new Boolean(bnI());
            case 13:
                return cbP();
            case 14:
                aYF();
                return null;
            case 15:
                m45774fg();
                return null;
            case 16:
                return m45789zX();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aC */
    public void mo24721aC(float f) {
        switch (bFf().mo6893i(_f_setRadius2_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius2_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius2_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m45764aB(f);
    }

    @C1253SX
    public void aYG() {
        switch (bFf().mo6893i(_f_offloadInit_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                break;
        }
        aYF();
    }

    /* access modifiers changed from: protected */
    public boolean bnJ() {
        switch (bFf().mo6893i(_f_isValidTrackedShip_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]));
                break;
        }
        return bnI();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public DroneAIControllerType cbQ() {
        switch (bFf().mo6893i(f10324x9c160ae3)) {
            case 0:
                return null;
            case 2:
                return (DroneAIControllerType) bFf().mo5606d(new aCE(this, f10324x9c160ae3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f10324x9c160ae3, new Object[0]));
                break;
        }
        return cbP();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m45774fg();
    }

    /* renamed from: h */
    public void mo4610h(Vec3d ajr) {
        switch (bFf().mo6893i(f10325x6107980)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f10325x6107980, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f10325x6107980, new Object[]{ajr}));
                break;
        }
        m45775g(ajr);
    }

    public void setRadius(float f) {
        switch (bFf().mo6893i(_f_setRadius_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m45763aA(f);
    }

    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m45782qN();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m45767ay(f);
    }

    /* renamed from: zO */
    public Vec3d mo24723zO() {
        switch (bFf().mo6893i(f10322xdc7de0cc)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, f10322xdc7de0cc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f10322xdc7de0cc, new Object[0]));
                break;
        }
        return m45785zN();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m45786zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zS */
    public void mo3333zS() {
        switch (bFf().mo6893i(_f_configureBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_configureBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_configureBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m45787zR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m45788zT();
    }

    /* renamed from: zY */
    public /* bridge */ /* synthetic */ AIControllerType mo3334zY() {
        switch (bFf().mo6893i(f10323xed6e425)) {
            case 0:
                return null;
            case 2:
                return (AIControllerType) bFf().mo5606d(new aCE(this, f10323xed6e425, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f10323xed6e425, new Object[0]));
                break;
        }
        return m45789zX();
    }

    @C0064Am(aul = "e5bddda09a699fabcbf5fd27abe8da56", aum = 0)
    /* renamed from: ay */
    private void m45767ay(float f) {
        if (cbQ() != null) {
            super.step(f);
        }
    }

    /* renamed from: a */
    public void mo4238a(DroneAIControllerType aco) {
        super.mo967a((C2961mJ) aco);
        m45768az(-1.0f);
        if (aco == null) {
            mo8358lY("DroneAIController created without a type: " + cWm());
        }
    }

    @C0064Am(aul = "c91cab9991aef83736b2be450d9df543", aum = 0)
    /* renamed from: aA */
    private void m45763aA(float f) {
        m45768az(f * f);
    }

    @C0064Am(aul = "38894a6ae6c565b64d473cb91f2a254b", aum = 0)
    /* renamed from: aB */
    private void m45764aB(float f) {
        m45768az(f);
    }

    @C0064Am(aul = "e840c452a3a501262c0a42241668e863", aum = 0)
    /* renamed from: I */
    private void m45760I(Ship fAVar) {
        if (!(bUT() == null || !(aYa() instanceof Ship) || ((Ship) aYa()).agj() == null)) {
            ((Ship) aYa()).agj().mo12633P(bUT().agj());
        }
        super.mo4588J(fAVar);
        if (bUT() != null && (aYa() instanceof Ship) && ((Ship) aYa()).agj() != null) {
            ((Ship) aYa()).agj().mo12632N(bUT().agj());
        }
    }

    @C0064Am(aul = "cf25f5b0581484fc43b2370de4d05087", aum = 0)
    /* renamed from: K */
    private void m45761K(Character acx) {
        if (bUT() != null && (aYa() instanceof Ship)) {
            ((Ship) aYa()).agj().mo12633P(bUT().agj());
        }
        super.mo4589L(acx);
        if (bUT() != null && (aYa() instanceof Ship)) {
            ((Ship) aYa()).agj().mo12632N(bUT().agj());
        }
    }

    @C0064Am(aul = "cc13fb7560a486f719b48f3c0c212d73", aum = 0)
    /* renamed from: zN */
    private Vec3d m45785zN() {
        return m45783zL();
    }

    @C0064Am(aul = "55dfe9d1a3cee6b81c64d4a3d0764839", aum = 0)
    /* renamed from: g */
    private void m45775g(Vec3d ajr) {
        m45773f(ajr);
    }

    @C0064Am(aul = "6aa39e33462e3df7790b3e1aa9dff2db", aum = 0)
    /* renamed from: qN */
    private void m45782qN() {
        m45770bz(currentTimeMillis());
        super.start();
    }

    @C0064Am(aul = "4da03537924a875da8a729b840417f07", aum = 0)
    /* renamed from: zP */
    private void m45786zP() {
        boolean z;
        mo3318b(m45783zL() != null, "You must set a center for DroneAIController");
        if (m45784zM() > 0.0f) {
            z = true;
        } else {
            z = false;
        }
        mo3318b(z, "You must set the radius for DroneAIController");
        if (this.avoidObstaclesBehaviour == null) {
            this.avoidObstaclesBehaviour = new C6822auO(bUT());
        } else {
            this.avoidObstaclesBehaviour.mo16334a(bUT());
        }
        if (this.pursuitBehaviour == null) {
            this.pursuitBehaviour = new aER(bUT());
        } else {
            this.pursuitBehaviour.mo8609J(bUT());
        }
        if (this.shootEnemyBehaviour == null) {
            this.shootEnemyBehaviour = new C6305akR(bUT());
        } else {
            this.shootEnemyBehaviour.mo14322J(bUT());
        }
        if (this.dodgeBulletsBehaviour == null) {
            this.dodgeBulletsBehaviour = new C2653iB();
        }
        mo3328ld(4);
        mo3327lb(0);
        mo3317b(0, new C1829aV());
        mo3317b(1, mo3316b(this.pursuitBehaviour, this.shootEnemyBehaviour), this.dodgeBulletsBehaviour);
        mo3317b(2, new C1829aV(), mo3316b(new C6822auO(), new C5706aTq(m45783zL(), false), new C6434amq()));
        mo3317b(3, new C1829aV(), mo3316b(new C6822auO(), new C5280aDg(), new C6434amq()));
        if (bUT() != null && (aYa() instanceof Ship)) {
            ((Ship) aYa()).agj().mo12632N(bUT().agj());
        }
    }

    @C0064Am(aul = "d723da95a298aed0d4ceb93c96dd949a", aum = 0)
    /* renamed from: zR */
    private void m45787zR() {
        this.pursuitBehaviour.mo11520oC(bwh());
        this.pursuitBehaviour.mo8612fe(cbK());
        this.shootEnemyBehaviour.mo14327jN(m45765aI());
        this.shootEnemyBehaviour.mo14326fe(m45766aJ());
        this.dodgeBulletsBehaviour.mo19450aL(bwg());
        this.dodgeBulletsBehaviour.mo19448aJ(bwf());
        this.dodgeBulletsBehaviour.mo19449aK(bwe());
        this.dodgeBulletsBehaviour.setDisabled(cbL());
    }

    @C0064Am(aul = "9a674fff187019fd1de56daa07edacd1", aum = 0)
    /* renamed from: zT */
    private int m45788zT() {
        double d;
        if (mo3314al() == null || mo3314al().getPosition() == null || m45783zL() == null) {
            mo8358lY("Issue #3356 - " + mo3314al() + " - " + m45783zL() + " - " + (mo3314al() != null ? mo3314al().getPosition() : null));
            mo8358lY(" ** " + isRunning() + " ** " + aYE() + " ** " + isEnabled());
            d = ScriptRuntime.NaN;
        } else {
            d = Vec3d.m15654o(m45783zL(), mo3314al().getPosition());
        }
        switch (aYs()) {
            case 0:
                if (((float) (currentTimeMillis() - m45759Bk())) / 1000.0f > cbM()) {
                    return 1;
                }
                break;
            case 1:
                if (bnJ()) {
                    cbO();
                    break;
                } else {
                    return 3;
                }
            case 2:
                if (d < ((double) m45784zM()) || bnJ()) {
                    return 1;
                }
            case 3:
                if (bnJ()) {
                    return 1;
                }
                if (d > ((double) (m45784zM() + 100000.0f))) {
                    return 2;
                }
                break;
        }
        return -1;
    }

    @C0064Am(aul = "fb9bf3fa21d100a4fd5fa38ab31e1171", aum = 0)
    private void cbN() {
        Ship bUT = bUT();
        Character agj = bUT.agj();
        if (agj == null || !(agj instanceof Player)) {
            mo3317b(1, this.dodgeBulletsBehaviour, mo3316b(this.pursuitBehaviour, this.shootEnemyBehaviour));
        } else {
            mo3317b(1, mo3316b(this.pursuitBehaviour, this.shootEnemyBehaviour), this.dodgeBulletsBehaviour);
        }
        this.avoidObstaclesBehaviour.mo16334a(bUT);
        this.pursuitBehaviour.mo8609J(bUT);
        this.shootEnemyBehaviour.mo14322J(bUT);
    }

    @C0064Am(aul = "47b9725fabac49d98a0f5011961e4da3", aum = 0)
    private boolean bnI() {
        return super.bnJ() && aYa().getPosition().mo9517f((Tuple3d) bUT().getPosition()).lengthSquared() < ((double) m45784zM());
    }

    @C0064Am(aul = "d032375d701197bf43e01465325abc38", aum = 0)
    private DroneAIControllerType cbP() {
        return (DroneAIControllerType) super.mo3334zY();
    }

    @C0064Am(aul = "a2a7ab1ec8ed55b6dbc032c89a2cad79", aum = 0)
    @C1253SX
    private void aYF() {
        super.aYG();
    }

    @C0064Am(aul = "988624b064e44eda95dd1c105599b442", aum = 0)
    /* renamed from: fg */
    private void m45774fg() {
        if (isDisposed()) {
            mo8358lY(String.valueOf(bFY()) + " is being disposed again");
            return;
        }
        if (bUT() != null && (aYa() instanceof Ship)) {
            if (((Ship) aYa()).agj() == null) {
                mo8358lY(String.valueOf(bFY()) + " is being disposed but pilot is already null. We are not calling revokeEnmity for whoever wants to know.");
            } else {
                ((Ship) aYa()).agj().mo12633P(bUT().agj());
            }
        }
        super.dispose();
    }
}
