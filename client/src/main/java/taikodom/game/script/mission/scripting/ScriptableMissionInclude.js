/*
 * Include loaded by the ScriptableMission controller
 * @author: Helder
 */

// ================================ UTILITY FUNCTIONS: BEGIN ================================

var util = new Object();

util.spawnHostileNPCs = function (amount, spawnTable, coordinates) {
    var npcs = mission.spawnNPCs(amount, spawnTable, coordinates, true);
    var jsnpcs = new Array();

    for (var i = 0; i < npcs.length; i++) {
        jsnpcs[i] = npcs[i] + "";
    }

    return jsnpcs;
};

util.spawnNeutralNPCs = function (amount, spawnTable, coordinates) {
    var npcs = mission.spawnNPCs(amount, spawnTable, coordinates, false);
    var jsnpcs = new Array();

    for (var i = 0; i < npcs.length; i++) {
        jsnpcs[i] = npcs[i] + "";
    }

    return jsnpcs;
};

util.removeObjectives = function (handleArray) {
    for (var i = 0; i < handleArray.length; i++) {
        mission.removeObjective(handleArray[i]);
    }
};

util.aliveCount = function (handleArray) {
    count = 0;

    for (i = 0; i < handleArray.length; i++) {
        if (mission.isNPCAlive(handleArray[i])) {
            count++;
        }
    }

    return count;
};

util.arrayContains = function (array, value) {

    if (array == undefined) {
        return false;
    }

    for (i = 0; i < array.length; i++) {
        if (array[i] == value) {
            return true;
        }
    }

    return false;
};

function spawnHostileNPCs(amount, spawnTable, coordinates) {
    return util.spawnHostileNPCs(amount, spawnTable, coordinates);
}

function spawnNeutralNPCs(amount, spawnTable, coordinates) {
    return util.spawnNeutralNPCs(amount, spawnTable, coordinates);
}

function removeObjectives(handleArray) {
    return util.removeObjectives(handleArray);
}

function aliveCount(handleArray) {
    return util.aliveCount(handleArray);
}

function arrayContains(array, value) {
    return util.arrayContains(array, value);
}

function accomplish() {
    mission.accomplish();
}

function fail() {
    mission.fail();
}

function putBeaconOnActor(handle) {
    mission.putBeaconOnActor(handle);
}

function takeItem(handle, amount) {
    return mission.takeItem(handle, amount);
}

function giveItem(handle, amount) {
    return mission.giveItem(handle, amount);
}

function setValue(key, value) {
    mission.setValue(key, value);
}

function getValue(key) {
    return mission.getValue(key);
}

function setParameter(parameter, value) {
    mission.setParameter(parameter, value);
}

function getParameter(parameter) {
    return mission.getParameter(parameter);
}

function getPlayerProgressionCareer() {
    return mission.getPlayerProgressionCareer();
}

function declareSpawnTable(spawnTableName, table) {
    mission.declareSpawnTable(spawnTableName, table);
}

function declareArea(name, node, x, y, z, radius) {
    mission.declareArea(name, node, x, y, z, radius);
}

function createObjective(handle, description, maxValue) {
    mission.createObjective(handle, description, maxValue);
}

function createObjective(handle) {
    mission.createObjective(handle);
}

function removeObjective(handle) {
    mission.removeObjective(handle);
}

function accomplishObjective(handle) {
    mission.accomplishObjective(handle);
}

function failObjective(handle) {
    mission.failObjective(handle);
}

function setObjectiveValue(handle, value) {
    mission.setObjectiveValue(handle, value);
}

function countItem(handle) {
    return mission.countItem(handle);
}

function countItemStorage(item, station) {
    return mission.countItemStorage(item, station);
}

function showNPCMessage(npc, message, timeout) {
    mission.showNPCMessage(npc, message, timeout);
}

function recurrentMessageRequest(handle, header, footer, asset) {
    mission.recurrentMessageRequest(handle, header, footer, asset);
}

function recurrentMessageRemove(handle) {
    mission.recurrentMessageRemove(handle);
}

function createWaypoint(area) {
    return mission.createWaypoint(area);
}

function createRandomWaypoint(area, radius) {
    return mission.createRandomWaypoint(area, radius);
}

function destroyWaypoint(area) {
    mission.removeWaypoint(area);
}

function listenNPCDamage(handle) {
    mission.listenNPCDamage(handle);
}

function getNPCHull(handle) {
    return mission.getNpcHull(handle);
}

function getNPCShield(handle) {
    return mission.getNpcShield(handle);
}

function getPlayerHull() {
    return mission.getPlayerHull();
}

function getPlayerShield() {
    return mission.getPlayerShield();
}

function isNPCAlive(handle) {
    return mission.isNPCAlive(handle);
}

function declareCounter(handle) {
    mission.setValue(handle, new java.lang.Integer(0));
}

function increaseCounter(handle, amount) {
    var value = (mission.getValue(handle) - 0) + (amount - 0);
    mission.setValue(handle, value);
}

function decreaseCounter(handle, amount) {
    var value = (mission.getValue(handle) - 0) - (amount - 0);
    mission.setValue(handle, value);
}

function setFlag(handle) {
    mission.setValue(handle, true);
}

function resetFlag(handle) {
    mission.setValue(handle, false);
}

function getFlag(handle) {
    return (mission.getValue(handle) == true);
}

function addToTeam(teamName, teamMembers) {
    mission.addToTeam(teamName, teamMembers);
}

function teamAttack(attackers, attacked) {
    mission.teamAttack(attackers, attacked);
}

function teamAttackPlayer(attackers) {
    mission.teamAttackPlayer(attackers);
}

function setTimer(handle, timeout) {
    mission.setTimer(handle, timeout, 1);
}

function unsetTimer(handle) {
    mission.cancelTimer(handle);
}

function postNurseryEvent(transition) {
    mission.postNurseryEvent(transition);
}

// ================================ UTILITY FUNCTIONS: END ================================


// Mission states array object
var state = new Object();
var data = new Object();

// current mission state
var current = null;
var currentStateName = mission.getValue("currentStateName");

// functions to invoke triggers in the current state
function invokeTrigger0(trigger) {
    if (current[trigger] instanceof Function) {
        return current[trigger]();
    }
}

function invokeTrigger1(trigger, param) {
    if (current[trigger] instanceof Function) {
        return current[trigger](param);
    }
}

function invokeTrigger2(trigger, param1, param2) {
    if (current[trigger] instanceof Function) {
        return current[trigger](param1, param2);
    }
}

function invokeTrigger3(trigger, param1, param2, param3) {
    if (current[trigger] instanceof Function) {
        return current[trigger](param1, param2, param3);
    }
}

// function to change state
function gotoState(goalState) {
    current = state[goalState];
    mission.setValue("currentStateName", goalState);

    if (current.onEnterState != null) {
        current.onEnterState();
    }
}

function getData() {
    return data;
}

function setData(newData) {
    data = newData;
}

// initial evaluation of the game.script
eval(script);

if (currentStateName == null) {
    gotoState(initialState);
} else {
    current = state[currentStateName];
}
