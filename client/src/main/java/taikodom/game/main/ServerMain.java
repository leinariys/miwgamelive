package taikodom.game.main;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.engine.C3658tN;
import game.network.WhoAmI;
import game.network.build.BuilderSocketStatic;
import game.network.build.IServerConnect;
import game.network.channel.Connect;
import game.network.channel.NetworkChannel;
import game.script.Taikodom;
import logic.res.FileControl;
import logic.res.code.DataGameEventImpl;
import logic.res.code.ResourceDeobfuscation;
import logic.res.html.C7025azm;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import logic.thred.WatchDog;
import org.apache.commons.logging.Log;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.render.loader.provider.FilePath;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: a */
public class ServerMain {
    private static final boolean dDi = false;
    private static final Log logger = LogPrinter.setClass(ServerMain.class);
    public static C7025azm<Object> dDj = MessageContainer.m16006yw(1);
    private static FilePath componentsDirectory;
    private static FilePath rootDirectory;
    /* renamed from: ng */
    private static FilePath renderRootpath;
    private static WatchDog watchDog;
    private C0493Gq dDl;
    private long dDm;

    public static void main(String[] strArr) {
        m45755a(
            strArr,
            new FileControl("."),
            new FileControl(System.getProperty("gametoolkit.client.render.rootpath", "."))
        );
    }

    /* renamed from: a */
    public static void m45755a(String[] strArr, FilePath rootDirect, FilePath renderRootpathDirectory) throws FileNotFoundException, UnsupportedEncodingException {
        rootDirectory = rootDirect;
        renderRootpath = renderRootpathDirectory;
        logger.info("Starting taikodom server 1.0.4933.55");
        watchDog = new WatchDog();
        watchDog.setPrintWriter(LogPrinter.createLogFile("server-watchdog", "log"));
        watchDog.start();

        bir();
        String comandKeyTestCache = null;
        int i = 0;
        boolean isRunServerOnlySnapshot = false;
        boolean isPreloadClasses = false;
        while (i < strArr.length) {
            String argument = strArr[i];
            if (argument.equals("-preloadClasses")) {
                isPreloadClasses = true;
            } else if ("-onlySnapshot".equals(argument)) {
                isRunServerOnlySnapshot = true;
            } else if ("-testCache".equals(argument) && i < strArr.length - 1) {
                i++;
                comandKeyTestCache = strArr[i];
            }
            i++;
        }
        loadProperties();
        if (isRunServerOnlySnapshot) {
            //Запуск сервера только для моментального снимка
            logger.info("Running server for snapshot only");
            ServerSocketSnapshot socketSnapshot = new ServerSocketSnapshot();
            ResourceDeobfuscation resDeob = new ResourceDeobfuscation();
            resDeob.setServerSocket(socketSnapshot);
            resDeob.setWhoAmI(WhoAmI.SERVER);
            resDeob.setClassObject(Taikodom.class);
            resDeob.mo13372ev(true);
            resDeob.mo13373ew(true);
            resDeob.setResursData(rootDirect.concat(C0891Mw.DATA));
            resDeob.mo13370et(true);
            resDeob.mo13371eu(true);
            new DataGameEventImpl(resDeob).mo3335HY();
            PoolThread.sleep(5000);
            System.exit(0);
        } else if (comandKeyTestCache == null) {
            new ServerMain().m45757dd(isPreloadClasses);
        }
    }

    public static void loadProperties() {
        Properties properties = new Properties();
        try {
            properties.load(m45758eQ(C0645JJ.TAIKODOM_SERVER_DEFAULTS_PROPERTIES).openInputStream());
        } catch (IOException e) {
            logger.error("Error reading taikodom.server.defaults.properties", e);
        }
        try {
            FilePath eQ = m45758eQ(C0645JJ.TAIKODOM_SERVER_CUSTOM_PROPERTIES);
            if (eQ.exists()) {
                properties.load(eQ.openInputStream());
            }
        } catch (IOException e2) {
            logger.error("Error reading taikodom.server.custom.properties", e2);
        }
        System.getProperties().putAll(properties);
        componentsDirectory = rootDirectory.concat("components");
    }

    /* renamed from: eQ */
    private static FilePath m45758eQ(String str) {
        FilePath ain = null;
        if (File.pathSeparator.equals(C0147Bi.SEPARATOR)) {
            FilePath aC = rootDirectory.concat(C0645JJ.ETC_TAIKODOM).concat(str);
            if (aC.exists()) {
                ain = aC;
            }
        }
        if (ain == null) {
            return rootDirectory.concat(C0645JJ.RES_SERVER_CONFIG).concat(str);
        }
        return ain;
    }

    private static void bir() {
        ArrayList<Field> arrayList = new ArrayList<>();
        for (Field field : C5877acF.class.getFields()) {
            if (field.getType() == Boolean.TYPE) {
                try {
                    if (field.getBoolean((Object) null)) {
                        arrayList.add(field);
                    }
                } catch (Exception e) {
                    logger.error("Error reading TaikodomDebug", e);
                }
            }
        }
        if (!arrayList.isEmpty()) {
            logger.warn("******* ENABLED TAIKODOM DEBUG VARIABLES FOR THIS SERVER SESSION ********");
            for (Field name : arrayList) {
                logger.warn("\t\t" + name.getName());
            }
            logger.warn("*************************************************************************");
        }
    }

    /* renamed from: dd */
    private void m45757dd(boolean isPreloadClasses) {
        String str;
        EngineGameServer engineGameServer = new EngineGameServer(rootDirectory, renderRootpath);
        C5916acs.setRootAndRenderPath(engineGameServer);
        engineGameServer.mo9091a((C3658tN) new C0891Mw("appLog"));
        int max = Math.max(5, Runtime.getRuntime().availableProcessors());
        int listenerPort = Integer.parseInt(System.getProperty(C0645JJ.TAIKODOM_SERVER_PORT, Integer.toString(IServerConnect.PORT_LOGIN)));
        NetworkChannel serverSocket = BuilderSocketStatic.buildServerSocket(listenerPort, TaikodomVersion.VERSION);
        //Сервер слушает tcp порт
        logger.info("Server listening to tcp port " + listenerPort);
        ResourceDeobfuscation sh = new ResourceDeobfuscation();
        sh.setServerSocket(serverSocket);
        sh.setWhoAmI(WhoAmI.SERVER);
        sh.setClassObject(Taikodom.class);
        sh.mo13372ev(true);
        sh.mo13373ew(true);
        sh.setResursData(rootDirectory.concat(C0891Mw.DATA));
        sh.setResursComponent(rootDirectory.concat("components"));
        sh.setWraperDbFile((WraperDbFile) new WraperDbFileImpl());
        sh.setEngineGame(engineGameServer);
        new TimeServer(15230).start();
        DataGameEventImpl managerGameEventImpl = new DataGameEventImpl(sh);

        ThreadPoolExecutor poolExecutor = PoolThread.newFixedThreadPool(max, max, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), "Tick Thread", false);
        managerGameEventImpl.bGS().setExecutor(poolExecutor);
        Taikodom dn = (Taikodom) managerGameEventImpl.bGz().mo6901yn();
        serverSocket.creatListenerChannel((Connect) new WraperManagerGameEvent(managerGameEventImpl, dn));
        serverSocket.buildListenerChannel(IServerConnect.PORT, (int[]) null, (int[]) null, false);
        dDj.push(new Object());

        //Запуск цепочки тиков тасклета
        logger.info("Starting tasklet tick thread");
        Thread thread = new Thread(new C3041nM(poolExecutor, managerGameEventImpl.getSynchronizerTime(), dn.aJE()), "Tasklet Ticker");
        thread.setDaemon(true);
        thread.start();
        managerGameEventImpl.mo3370b(thread);

        logger.info("TaikodomServer version [1.0.4933.55] is entering main loop");
        this.dDl = new C0493Gq(componentsDirectory, managerGameEventImpl.getTaikodom());

        while (true) {
            try {
                PoolThread.sleep(10);
                managerGameEventImpl.bGS().mo2194gZ();
                m45756c(dn);
            } catch (Exception e) {
                logger.error("Ignoring error at main loop:", e);
            } catch (Throwable th) {
                try {
                    logger.error("Fatal error at main loop:", th);
                    try {
                        return;
                    } catch (Exception e2) {
                        logger.error(e2);
                        return;
                    }
                } finally {
                    str = "Exiting main loop (finally)";
                    logger.info(str);
                    try {
                        this.dDl.dispose();
                    } catch (Exception e3) {
                        logger.error(e3);
                    }
                }
            }
        }
    }

    /* renamed from: c */
    private void m45756c(Taikodom dn) {
        long currentTimeMillis = SingletonCurrentTimeMilliImpl.currentTimeMillis();
        if (currentTimeMillis - this.dDm >= 1000) {
            dn.aLY().dps();
            this.dDm = currentTimeMillis;
        }
    }
}
