package taikodom.game.main;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.engine.*;
import game.network.WhoAmI;
import game.network.build.BuilderSocketStatic;
import game.network.build.IServerConnect;
import game.network.channel.NetworkChannel;
import game.network.exception.IOExceptionServer;
import game.network.exception.ServerLoginException;
import game.network.message.externalizable.aUU;
import game.script.Taikodom;
import game.script.player.User;
import logic.*;
import logic.aaa.C1169RJ;
import logic.render.EngineGraphics;
import logic.render.GUIModule;
import logic.render.IEngineGraphics;
import logic.render.RepaintManagerWraper;
import logic.res.*;
import logic.res.code.DataGameEventImpl;
import logic.res.code.ResourceDeobfuscation;
import logic.res.code.SoftTimer;
import logic.res.css.C6868avI;
import logic.res.css.CssNode;
import logic.res.css.LoaderCss;
import logic.swing.C5378aHa;
import logic.thred.LockInstanceApp;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import logic.thred.WatchDog;
import logic.ui.BaseUiTegXml;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.IComponentManager;
import org.apache.commons.logging.Log;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.infra.script.I18NString;
import taikodom.render.SceneEvent;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.loader.provider.ImageLoader;

import javax.imageio.ImageIO;
import javax.media.opengl.Threading;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: a */
public class ClientMain {
    /* access modifiers changed from: private */
    public static final Log logger = LogPrinter.setClass(ClientMain.class);
    private static final boolean dDi = false;
    private static final String clientDomain = "com.hoplon.Taikodom";
    /* access modifiers changed from: private */
    public static ClientMain clientMain;
    /**
     * Корневой путь. Каталог с кодом отрисовки графики
     */
    /* renamed from: ng */
    public static FilePath rootPathRender;
    /**
     * Логирование часов потока
     */
    private static C5282aDi dTn;
    private static Object dTp;
    /**
     * Это единственно запущенный клиент? true - незапущен  false - запущен
     */
    private static boolean isRunClient;
    /**
     * Каталог главный, rootpath
     */
    /* access modifiers changed from: private */
    private static FilePath rootPath;
    /**
     * Диагностика работы потоков
     */
    private static WatchDog watchDog;
    public ThreadPoolExecutor dTr = new ThreadPoolExecutor(1, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new PoolThread.ThreadFactoryWrapper("Worker", true));

    /**
     * Блокировжик ресурса
     */
    public C1175RP dTs = new C1175RP();
    /**
     * Список задач для очереди потоков
     */
    /* access modifiers changed from: private */
    public C6187aiD dTt = new C6187aiD(this);
    /* access modifiers changed from: private */
    public boolean dTu;
    /**
     * Менеджер аддонов
     */
    /* access modifiers changed from: private */
    public AddonManager addonManager;
    /* access modifiers changed from: private */
    public long dTx;
    /* renamed from: lV */
    public IEngineGraphics engineGraphics;
    /* renamed from: vS */
    public EngineGame engineGame;
    private int dTw;
    /* access modifiers changed from: private */
    private long startTime;

    public ClientMain() {
        this.dTr.execute(new C5109i());
    }

    public static void main(String[] strArr) {
        PrintWriter M;
        PrintWriter M2;
        logger.info("Starting ClientMain");
        rootPath = new FileControl(".");
        String property = System.getProperty("gametoolkit.client.render.rootpath", ".");
        System.out.println("Client MAIN: Loading path '" + property + "'");
        rootPathRender = new FileControl(property);

        C2606hU.setRootPathResource(new FileControl(System.getProperty("physics.resources.rootpath", ".")));
        URL.setURLStreamHandlerFactory(C2600hP.m32602ze());//Задает URL-адрес приложения для Java Virtual Machine.

        logger.debug("step1");
        C2600hP.m32602ze().mo19253a("res", new StreamRootPathRender(rootPathRender));

        logger.debug("step2");
        isRunClient = !LockInstanceApp.isCopyApp(clientDomain, System.getProperty("user.dir"));

        logger.debug("step3");
        dTp = LockInstanceApp.setMarkOriginalTime(clientDomain, System.getProperty("user.dir"));

        logger.debug("step4");
        if (!isRunClient) {
            logger.warn("Taikodom already running");
        }
        clientMain = new ClientMain();

        logger.debug("step5");
        watchDog = new WatchDog();

        logger.debug("step6");
        logger.debug("step6.1");
        if (isRunClient) {
            logger.debug("step6.2");
            M = m45737gq("client-watchdog");
        } else {
            logger.debug("step6.3");
            M = LogPrinter.createLogFile("client-watchdog", "log");
        }
        M.println("Starting taikodom client 1.0.4933.55");
        watchDog.setPrintWriter(M);

        logger.debug("step7");
        if (C5877acF.feh) {
            watchDog.start();
        }

        logger.debug("step8");
        dTn = new C5099b("Glitch Watch Thread");

        logger.debug("step9");
        if (isRunClient) {
            M2 = m45737gq("client-glitch");
        } else {
            M2 = LogPrinter.createLogFile("client-glitch", "log");
        }
        M2.println("Starting taikodom client 1.0.4933.55");
        dTn.setPrintWriter(M2);

        logger.debug("step10");
        if (C5877acF.feg) {
            dTn.start();
        }

        logger.debug("step11");
        C5378aHa.cNQ();
        DataGameEventImpl.bxC();
        C5098a aVar = new C5098a("Class loading");
        aVar.setDaemon(true);
        aVar.start();
        logger.info("Starting taikodom client 1.0.4933.55");

        clientMain.m45738gr("binkw32");
        clientMain.m45738gr("SDL");
        clientMain.m45738gr("granny2");
        clientMain.m45738gr("grn2gr2");
        clientMain.m45738gr("mss32");
        clientMain.m45738gr("utaikodom.render");
        clientMain.m45738gr("libgameswf");
        clientMain.m45738gr("utaikodom.render.swf");
        Threading.disableSingleThreading();
        clientMain.run();
    }

    /* renamed from: gq */
    private static PrintWriter m45737gq(String str) {
        logger.debug("createWriter: " + str);
        String property = System.getProperty("log-dir", "log");
        logger.debug("logDir: " + str);
        File file = new File(String.valueOf(property) + C0147Bi.SEPARATOR + str + ".log");
        file.getParentFile().mkdirs();
        logger.debug("logFile: " + file.getCanonicalPath());
        if (file.exists()) {
            logger.debug("file already exists");
            file.delete();
            logger.debug("deleted");
        }
        logger.debug("file already exists");
        PrintWriter printWriter = new PrintWriter(new FileWriter(file), true);
        logger.debug("done createWriter");
        return printWriter;
    }

    public static ClientMain getClientMain() {
        return clientMain;
    }

    /* renamed from: gr */
    private void m45738gr(String str) {
        FilePath aC = rootPath.concat("./libs/" + str + ".dll");//natives
        if (aC.exists()) {//Нашло библиотеку
            System.load(aC.getFile().getAbsolutePath());
        } else {//Не нашло библиотеку
            System.loadLibrary(str);
        }
    }

    /**
     * Установка языка
     * СОздание движка отрисовки
     * Установка настройки  отрисовки
     */
    private void boL() {
        this.engineGame = new EngineGame(rootPath, rootPathRender);//Передача ссылок на каталок
        this.engineGame.mo15325fn(true);
        this.engineGame.mo15308a((Executor) this.dTt);//Передача списка задач
        ConfigManager configManager = ConfigManager.initConfigManager();//Создали/получили обёртку настроек
        ConfigManagerLoad(configManager);//Загружаем настройки из файлов
        String str = I18NString.DEFAULT_LOCATION;
        Locale locale = Locale.getDefault();
        if ("BR".equals(locale.getCountry()) || locale.getLanguage().equals("pt")) {
            str = "pt";
        }

        try {
            I18NString.setCurrentLocation(configManager.getSection(ConfigIniKeyValue.LANGUAGE.getValue()).getValuePropertyString(ConfigIniKeyValue.LANGUAGE, str));
        } catch (C5452aJw e) {
            logger.error("Could not retrieve 'language' entry from configuration file!");
        } catch (C6130agy e2) {
            logger.error("Entry 'language' in configuration file has wrong type (should be string)!");
        }

        this.engineGame.setConfigManager(configManager);//передали менеджер настроек
        this.engineGame.getVerbose();//Достали настройку [console] verbose
        this.engineGame.mo15306a((EventManager) new C5101d(Thread.currentThread()));
        //Создаём движок отрисовки
        this.engineGraphics = new EngineGraphics(this.engineGame.getEventManager());
        //Передача иконок
        this.engineGraphics.mo3075j(Arrays.asList(new Image[]{ImageIO.read(getClass().getResource("taikodom_icon_16.png")), ImageIO.read(getClass().getResource("taikodom_icon_32.png")), ImageIO.read(getClass().getResource("taikodom_icon_64.png"))}));
        try {
            //Передаём настройки раздела  [render]
            //Настройки графики
            this.engineGraphics.mo3003a(configManager.getSection("render"), rootPathRender);
        } catch (C5452aJw e3) {
            logger.error("Couldn't initialize the render module.");
            e3.printStackTrace();
        } catch (Exception e4) {
            logger.error("Couldn't initialize the render module.");
            e4.printStackTrace();
        }
        this.engineGame.setEngineGraphics(this.engineGraphics);
        LoaderTrail glVar = new LoaderTrail(this.engineGraphics);
        glVar.setExecutor(this.dTt);
        glVar.mo19096a(this.dTs.cYc());
        this.engineGame.mo15304a(this.dTs);
        this.engineGame.mo4065a((ILoaderTrail) glVar);
        this.engineGame.getLoaderTrail().start();
        SoftTimer abg = new SoftTimer(true);
        this.engineGame.mo4066a(abg);
        this.engineGraphics.mo3049bV("data/hud.pro");
        GUIModule hl = new GUIModule();
        hl.setProperties(rootPathRender, "/data/gui/imageset/", "/data/fonts/", this.engineGraphics, abg);
        this.engineGame.setGUIModule(hl);
        this.startTime = SingletonCurrentTimeMilliImpl.currentTimeMillis();
        boM();
    }

    private void boM() {
        try {
            BaseUiTegXml baseUItegXML = new BaseUiTegXml();
            C3126oE oEVar = new C3126oE();
            this.engineGame.mo15307a(oEVar);
            this.engineGame.alf().addTask("FileChangeCheckerTimer", new C5100c(), 1000);
            baseUItegXML.setLoaderCss((LoaderCss) new C5105f(oEVar));
            baseUItegXML.mo13719c(baseUItegXML.loadCss((Object) null, "res://data/styles/core.css"));
            baseUItegXML.setRootPane(this.engineGraphics.aee().getRootPane());
            this.engineGraphics.aee().getContentPane().setLayout((LayoutManager) null);
            baseUItegXML.setLoaderImage((ILoaderImageInterface) new C5104e());
            baseUItegXML.mo13703a((ILoadFonts) new C5108h());
            C5107g gVar = new C5107g();
            gVar.setDefaultFocusTraversalPolicy(new LayoutFocusTraversalPolicy());
            KeyboardFocusManager.setCurrentKeyboardFocusManager(gVar);
            RepaintManager.setCurrentManager(new RepaintManagerWraper());
            this.addonManager = new AddonManager(new WrapEngGameAddonManager(this.engineGame));
            //Базовые элементы интерфейса
            this.addonManager.setBaseUItagXML(baseUItegXML);
            this.engineGame.mo15305a((IAddonManager) this.addonManager);
            if (new File("addons").exists()) {
                this.addonManager.setFolderAddons(new File("addons"));//addons\taikodom.xml
            } else {
                this.addonManager.setFolderAddons(new File("../utaikodom.game.addons/addons"));
            }
        } catch (Exception e) {
            logger.warn("Error loading addon manager", e);
        }
    }

    /**
     * Запуск окна
     */
    public void run() {
        try {
            boL();//создание движка отрисовки
            this.dTs.mo5173h(Thread.currentThread()); //Настраиваем Блакировщик потока доступ к графическому движку например из аддона class InterfaceSFXAddon
            C5916acs.m20630eb(false);
            C5916acs.setRootAndRenderPath(this.engineGame);
            if (this.addonManager != null) {
                this.addonManager.startAddonManager();
            }
            if (C5877acF.fdL != null) {
                while (true) {
                    try {
                        runConnectToServer(C5877acF.fdL, C5877acF.fdM);
                        break;
                    } catch (Exception e) {
                        logger.info("could not connect trying again");//не удалось подключиться снова
                        PoolThread.sleep(500);
                    }
                }
            }
            this.dTw = 0;
            logger.info("TaikodomClient version [1.0.4933.55] is entering main loop");
            this.engineGame.getEventManager().publish(new aUU((short) 0));
            this.engineGraphics = this.engineGame.getEngineGraphics();
            try {
                this.dTx = SingletonCurrentTimeMilliImpl.currentTimeMillis() - 10;
                watchDog.mo14456f((Thread) null);
                while (this.engineGame.isExit()) {
                    if (!boN()) {
                        break;
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            C5110j jVar = new C5110j();
            jVar.setDaemon(true);
            jVar.start();
            if (this.engineGame.getLoaderTrail() != null) {
                logger.info("releasing resources...");
                this.engineGame.getLoaderTrail().dispose();
            }
            if (this.engineGraphics != null) {
                this.engineGraphics.close();
            }
            logger.info(" *** terminated *** ");
        } catch (Exception e3) {
            logger.error("Error initializing modules. Client will shutdown.", e3);
            e3.printStackTrace();
        }
    }

    /**
     * В бесонечном цикле
     *
     * @return
     */
    /* JADX INFO: finally extract failed */
    private boolean boN() {
        long nanoTime = System.nanoTime();
        if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == null) {
            synchronized (this.engineGraphics.aee().getTreeLock()) {
                if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == null) {
                    this.engineGraphics.aee().requestFocus();
                }
            }
        }
        if (this.engineGraphics != null && this.engineGraphics.aeu()) {
            this.engineGame.disconnect();
            this.engineGame.exit();
            return false;
        } else if (this.engineGraphics.aeB()) {
            this.engineGraphics.aeC();//Начать проигрывание видео заставки
            try {
                this.engineGraphics.swapBuffers();
            } catch (RuntimeException e) {
            }
            return true;
        } else {
            this.dTs.cXZ();
            try {
                long currentTimeMillis = SingletonCurrentTimeMilliImpl.currentTimeMillis();
                //Ставим задачу для потока постоянно проверять не требуется ли разорвать соединение
                if (this.engineGame.aPT() != null && this.engineGame.bhc() != null && !this.engineGame.aPT().isUp() && !this.dTu) {
                    this.dTu = true;
                    this.dTt.execute(new C5112l());
                }
                float second = ((float) (currentTimeMillis - this.dTx)) * 0.001f;
                this.dTx = currentTimeMillis;
                if (second <= 1.0E-8f) {
                    second = 1.0E-8f;
                }
                Threading.disableSingleThreading();
                m45740hm(second);
                this.engineGraphics.aew();
                if (this.addonManager != null) {
                    this.dTt.execute(new C5111k(second));
                }
                ArrayList<SceneEvent> arrayList = new ArrayList<>();
                arrayList.addAll(this.engineGraphics.aeo());
                for (SceneEvent nVar : arrayList) {
                    this.dTt.execute(new C5114n(nVar));
                }
                for (SceneEvent mVar : this.engineGame.getGuiModule().aeo()) {
                    this.dTt.execute(new C5113m(mVar));
                }
                this.engineGame.getGuiModule().aeo().clear();
                this.engineGame.getLoaderTrail().mo5008vu();
                m45730a(this.dTt, second);
                try {
                    this.engineGraphics.swapBuffers();
                } catch (RuntimeException e2) {
                }
                m45739hl(second);
                this.dTs.cXZ();
                try {
                    if (this.engineGame.getTaikodom() != null) {
                        this.engineGame.getTaikodom().aLW().dvJ();
                        this.engineGame.getTaikodom().aLW().mo11542gZ();
                    }
                    m45729a(this.engineGraphics, ((float) (currentTimeMillis - this.startTime)) * 0.001f, second);
                    IBaseUiTegXml.initBaseUItegXML().mo13708a(this.engineGraphics.getRenderView());
                    ImageLoader.processDestroyList(this.engineGraphics.getRenderView().getDrawContext());
                    ImageLoader.processForcedUpdate();
                    this.engineGraphics.aeE();
                    this.dTw++;
                    this.dTs.cYb();
                    boO();
                    PoolThread.sleep(Math.max(2, Math.min(7, (long) (7.0d - (((double) (System.nanoTime() - nanoTime)) * 1.0E-6d)))));
                    return true;
                } catch (Throwable th) {
                    this.dTs.cYb();
                    throw th;
                }
            } finally {
                this.dTs.cYb();
            }
        }
    }

    /* renamed from: a */
    private void m45729a(IEngineGraphics abd, float f, float f2) {
        abd.step(f2);
        abd.mo3074i(f, f2);
        abd.adW();
    }

    /* renamed from: hl */
    private void m45739hl(float f) {
        try {
            this.engineGame.getGuiModule().step(f);
        } catch (Exception e) {
            System.err.println("Ignoring exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void boO() {
        try {
            this.engineGame.getGuiModule().render();
        } catch (Exception e) {
            System.err.println("Ignoring exception: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    private void m45730a(C6187aiD aid, float f) {
        if (this.engineGame.bhc() != null) {
            this.engineGame.bhc().mo3366b(f, (Executor) aid);
            aid.cCM();
        }
    }

    /* renamed from: hm */
    private void m45740hm(float f) {
        if (this.engineGame.bhc() != null) {
            this.dTt.execute(new C5116p());
        }
        this.engineGame.alf().step((long) (1000.0f * f), this.dTt);
        this.dTt.cCM();
    }

    /**
     * Отключение от сервера
     */
    /* access modifiers changed from: private */
    public void boP() {
        this.engineGame.cleanUp();
        this.engineGame.disconnect();
        logger.info("*** disconnected from server *** ");
        this.engineGame.getEventManager().mo13975h(new C1169RJ());
    }

    /**
     * Подключению к серверу логинизации
     *
     * @param str  username
     * @param str2 password
     */
    /* renamed from: K */
    public void runConnectToServer(String str, String str2) {
        connectToServer(str, str2, false);
    }

    /**
     * Подключению к серверу логинизации
     *
     * @param userName         username
     * @param password         password
     * @param isForcingNewUser
     */
    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public void connectToServer(String userName, String password, boolean isForcingNewUser) {
        String cTp;
        NetworkChannel ahg;
        this.engineGame.setNetworkChannel((NetworkChannel) null);
        String connectHost = IServerConnect.DEFAULT_HOST; //Адрес подключения по умолчанию
        int connectPort = IServerConnect.PORT_LOGIN;//Порт подключения по умолчанию
        ConfigManager configManager = this.engineGame.getConfigManager();//Получили настройки со всеми разделами

        try {//пробуем достать настройки
            ConfigManagerSection kW = configManager.getSection(ConfigIniKeyValue.SERVER.getValue());//Получили Параметры раздела настроек
            connectHost = kW.getValuePropertyString(ConfigIniKeyValue.SERVER, IServerConnect.DEFAULT_HOST);//Например
            connectPort = kW.mo7195a(ConfigIniKeyValue.SERVER_PORT, Integer.valueOf(IServerConnect.PORT_LOGIN)).intValue();
        } catch (RuntimeException e) {
            logger.info(e);
        }

        MetricCheckpoint metricConn = new MetricCheckpoint("ClientMain.doLogin");
        String[] splitHost = connectHost.split(",");
        int i2 = 0;
        NetworkChannel networkChannel = null;
        boolean z2 = false;

        while (!z2) {
            boolean brL = this.dTs.cYc().brL();//блокировщик потока
            try {
                this.engineGame.getEventManager().mo13975h(new C3932wv(10));
                logger.info("Trying to connect to " + splitHost[i2] + ":" + connectPort);
                //TODO Здесь надо цикл сделать , чтобы каждый адрес в var8 проверило на подключение
                ahg = BuilderSocketStatic.m21641a(splitHost[i2], connectPort, userName, password, isForcingNewUser, TaikodomVersion.VERSION);
                try {
                    this.engineGame.setNetworkChannel(ahg);
                    if (brL) {
                        this.dTs.cYc().brK();
                    }
                    networkChannel = ahg;
                    z2 = true;
                } catch (IOExceptionServer e2) {
                    try {
                        this.engineGame.getEventManager().mo13975h(new C3932wv(1));
                        i2++;
                        if (i2 >= splitHost.length) {
                            throw new ServerLoginException();
                        } else if (brL) {
                            this.dTs.cYc().brK();
                            networkChannel = ahg;
                        } else {
                            networkChannel = ahg;
                        }
                    } catch (Throwable th) {
                        if (brL) {
                            this.dTs.cYc().brK();//блокировщик потока
                        }
                        throw th;
                    }
                }
            } catch (IOExceptionServer e3) {
                ahg = networkChannel;
            }
        }
        this.engineGame.getEventManager().mo13975h(new C3932wv(11));
        if (z2) {
            String str4 = splitHost[i2];
            int i3 = i2;
            for (int i4 = 1; i4 < splitHost.length; i4++) {
                i3 = (i3 + 1) % splitHost.length;
                str4 = String.valueOf(str4) + "," + splitHost[i3];
            }
            configManager.getSection(ConfigIniKeyValue.SERVER.getValue()).mo7203b(ConfigIniKeyValue.SERVER, str4);
            try {
                configManager.loadConfigIni("config/user.ini");
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        boolean brL2 = this.dTs.cYc().brL();
        try {
            ResourceDeobfuscation sh = new ResourceDeobfuscation();
            sh.setServerSocket(networkChannel);
            sh.setWhoAmI(WhoAmI.CLIENT);
            sh.setClassObject(Taikodom.class);
            sh.mo13372ev(false);
            sh.mo13373ew(false);
            sh.setEngineGame(this.engineGame);
            logger.info("Getting environment");
            this.engineGame.getEventManager().mo13975h(new C3932wv(12));
            DataGameEventImpl arVar = new DataGameEventImpl(sh, new C5115o());
            arVar.init();//Initing Environment.
            this.engineGame.getEventManager().mo13975h(new C3932wv(13));
            logger.info("Environment received");
            if (brL2) {
                this.dTs.cYc().brK();
            }
            this.engineGame.setNetworkChannel(networkChannel);
            arVar.mo3367b(this.dTs.cYc());
            metricConn.logPrintTime();
            if (this.engineGame.getTaikodom() != null) {
                throw new IllegalStateException("Environment not yet disposed");
            }
            this.engineGame.mo15322d((DataGameEvent) arVar);
            User bhe = this.engineGame.getUser();
            if (bhe.cST()) {
                throw new RuntimeException("########################################\n Editor não pode logar no cliente!!!!\n#######################################\n ");
            }
            metricConn.logPrintTime();
            this.engineGame.getEventManager().mo13975h(new C3932wv(20));
            metricConn.logPrintTime();
            try {
                cTp = configManager.mo7761kX("client.id");
                bhe.mo8480lP(cTp);
            } catch (IOException e5) {
                cTp = bhe.cTp();
            }
            Log log = logger;
            StringBuilder append = new StringBuilder("Log in with: ").append(bhe.getFullName()).append(" - GUID: ");
            if (cTp == null) {
                cTp = "not available";
            }
            log.info(append.append(cTp).toString());
            metricConn.logPrintTime();
            metricConn.logPrintTime();
            this.engineGame.mo15313c(networkChannel);
        } catch (Throwable th2) {
            if (brL2) {
                this.dTs.cYc().brK();
            }
            this.engineGame.setNetworkChannel(networkChannel);
            throw th2;
        }
    }

    /**
     * Загрузка настроек
     *
     * @param configManager Менеджер настроек
     */
    /* renamed from: b */
    private void ConfigManagerLoad(ConfigManager configManager) {
        try {//Загрузка настроек по умолчанию
            configManager.loadConfig("config/defaults.ini");
            logger.trace("  Default configuration file loaded");
        } catch (IOException e) {
            logger.error("  Unable to load configuration file", e);
        }
        configManager.setMarkAsDefaultConfig();
        try {//Загрузка настроек пользователя
            configManager.loadConfig("config/user.ini");
            logger.trace("  User configuration file loaded");
        } catch (IOException e2) {
            logger.warn("  User configuration file not loaded (does not exist?)");
        }
        try {//Загрузка настроек разработчика
            configManager.loadConfig("config/devel.ini");
            logger.warn("  Development configuration file loaded");
        } catch (IOException e3) {
            logger.warn("  Development configuration file not loaded (does not exist?)");
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$i */
    /* compiled from: a */
    class C5109i implements Runnable {
        C5109i() {
        }

        public void run() {
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$b */
    /* compiled from: a */
    static class C5099b extends C5282aDi {
        C5099b(String str) {
            super(str);
        }

        /* access modifiers changed from: protected */
        /* renamed from: cR */
        public long mo8429cR(long var3) {
            return var3 - ClientMain.clientMain.dTx;
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$a */
    static class C5098a extends Thread {
        C5098a(String str) {
            super(str);
        }

        public void run() {
            long j;
            C6280ajs ate = new ResourceDeobfuscation().ate();
            long nanoTime = System.nanoTime();
            for (String cls : ate.aXV()) {
                try {
                    j = nanoTime;
                    for (Class<?> cls2 = Class.forName(cls); cls2 != null; cls2 = cls2.getSuperclass()) {
                        cls2.getAnnotations();
                        for (Field type : cls2.getDeclaredFields()) {
                            type.getType().getAnnotations();
                            if (((double) (System.nanoTime() - j)) > 1.0E7d) {
                                PoolThread.sleep(3 * ((long) (((double) (System.nanoTime() - j)) * 1.0E-6d)));
                                j = System.nanoTime();
                            }
                        }
                    }
                    try {
                        if (((double) (System.nanoTime() - j)) > 1.0E7d) {
                            PoolThread.sleep(3 * ((long) (((double) (System.nanoTime() - j)) * 1.0E-6d)));
                            nanoTime = System.nanoTime();
                        }
                    } catch (Exception e) {
                    }
                } catch (Exception e2) {
                    j = nanoTime;
                }
                nanoTime = j;
            }
            ClientMain.logger.info("Done loading classes");
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$d */
    /* compiled from: a */
    class C5101d extends EventManager {
        private final /* synthetic */ Thread bhW;

        C5101d(Thread thread) {
            this.bhW = thread;
        }

        /* renamed from: d */
        public void mo13972d(String str, Object obj) {
            if (Thread.currentThread() == this.bhW) {
                ClientMain.this.dTt.execute(new C5102a(str, obj));
            } else {
                super.mo13972d(str, obj);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: j */
        public void mo24691j(String str, Object obj) {
            super.mo13972d(str, obj);
        }

        /* renamed from: c */
        public void mo13971c(String str, Object obj) {
            ClientMain.this.dTt.execute(new C5103b(str, obj));
        }

        /* renamed from: taikodom.game.main.ClientMain$d$a */
        class C5102a implements Runnable {
            private final /* synthetic */ String axQ;
            private final /* synthetic */ Object beo;

            C5102a(String str, Object obj) {
                this.axQ = str;
                this.beo = obj;
            }

            public void run() {
                C5101d.this.mo24691j(this.axQ, this.beo);
            }
        }

        /* renamed from: taikodom.game.main.ClientMain$d$b */
        /* compiled from: a */
        class C5103b implements Runnable {
            private final /* synthetic */ String axQ;
            private final /* synthetic */ Object beo;

            C5103b(String str, Object obj) {
                this.axQ = str;
                this.beo = obj;
            }

            public void run() {
                C5101d.this.mo24691j(this.axQ, this.beo);
            }
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$c */
    /* compiled from: a */
    class C5100c extends WrapRunnable {
        C5100c() {
        }

        public void run() {
            ClientMain.this.engineGame.cno().run();
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$f */
    /* compiled from: a */
    class C5105f extends LoaderCss {
        private final /* synthetic */ C3126oE bhY;

        C5105f(C3126oE oEVar) {
            this.bhY = oEVar;
        }

        /* renamed from: b */
        public C6868avI loadCSS(URL url) {
            if ("res".equals(url.getProtocol())) {
                url = ClientMain.rootPathRender.concat(url.toString().replaceAll("^res://", "")).getFile().toURI().toURL();
            }
            CssNode akf = new CssNode();
            akf.loadFileCSS(url.openStream());
            if ("file".equals(url.getProtocol())) {
                this.bhY.mo20939a(url, new C5106a(akf));
            }
            return akf;
        }

        /* renamed from: taikodom.game.main.ClientMain$f$a */
        class C5106a implements C6174ahq {
            private final /* synthetic */ CssNode cQv;

            C5106a(CssNode akf) {
                this.cQv = akf;
            }

            /* renamed from: a */
            public void mo13624a(URL url) {
                try {
                    ClientMain.this.dTs.cYc().brL();
                    this.cQv.loadFileCSS(url.openStream());
                    for (IComponentManager next : ComponentManager.m37352Vn()) {
                        next.mo13045Vk();
                        next.getCurrentComponent().invalidate();
                        next.getCurrentComponent().repaint();
                    }
                    ClientMain.this.dTs.cYc().brK();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                } catch (Throwable th) {
                    ClientMain.this.dTs.cYc().brK();
                    throw th;
                }
            }
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$e */
    /* compiled from: a */
    class C5104e implements ILoaderImageInterface {
        C5104e() {
        }

        public Image getImage(String str) {
            if (str != null && !str.equals("none")) {
                return ClientMain.this.engineGame.getGuiModule().adz().getImage(str);
            }
            return null;
        }

        /* renamed from: a */
        public Cursor getNull(String str, Point point) {
            return ClientMain.this.engineGame.getGuiModule().adz().getNull(str, point);
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$h */
    /* compiled from: a */
    class C5108h implements ILoadFonts {
        C5108h() {
        }

        /* renamed from: b */
        public Font mo13453b(String str, int i, int i2, boolean z) {
            if (str != null && !str.equals("none")) {
                return ClientMain.this.engineGame.getGuiModule().ddG().mo13453b(str, i2, i, z);
            }
            return null;
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$g */
    /* compiled from: a */
    class C5107g extends DefaultKeyboardFocusManager {
        C5107g() {
        }

        public boolean dispatchEvent(AWTEvent aWTEvent) {
            boolean dispatchEvent;
            if (ClientMain.this.engineGraphics == null || ClientMain.this.engineGraphics.aee() == null) {
                return ClientMain.super.dispatchEvent(aWTEvent);
            }
            synchronized (ClientMain.this.engineGraphics.aee().getTreeLock()) {
                try {
                    ClientMain.this.dTs.cYc().brK();
                    dispatchEvent = ClientMain.super.dispatchEvent(aWTEvent);
                    ClientMain.this.dTs.cYc().brL();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return false;
                } catch (Throwable th) {
                    ClientMain.this.dTs.cYc().brL();
                    throw th;
                }
            }
            return dispatchEvent;
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$j */
    /* compiled from: a */
    class C5110j extends Thread {
        C5110j() {
        }

        public void run() {
            try {
                Thread.sleep(8000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.exit(0);
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$l */
    /* compiled from: a */
    class C5112l implements Runnable {
        C5112l() {
        }

        public void run() {
            try {
                ClientMain.this.boP();
            } finally {
                ClientMain.this.dTu = false;
            }
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$k */
    /* compiled from: a */
    class C5111k implements Runnable {
        private final /* synthetic */ float second;

        C5111k(float second) {
            this.second = second;
        }

        public void run() {
            ClientMain.this.addonManager.step(this.second);
        }//выполнение аддонов
    }

    /* renamed from: taikodom.game.main.ClientMain$n */
    /* compiled from: a */
    class C5114n implements Runnable {
        private final /* synthetic */ SceneEvent bBX;

        C5114n(SceneEvent sceneEvent) {
            this.bBX = sceneEvent;
        }

        public void run() {
            ClientMain.this.engineGame.getEventManager().mo13972d(this.bBX.getMessage(), this.bBX.getObject());
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$m */
    /* compiled from: a */
    class C5113m implements Runnable {
        private final /* synthetic */ SceneEvent bBX;

        C5113m(SceneEvent sceneEvent) {
            this.bBX = sceneEvent;
        }

        public void run() {
            ClientMain.this.engineGame.getEventManager().mo13972d(this.bBX.getMessage(), this.bBX.getObject());
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$p */
    /* compiled from: a */
    class C5116p implements Runnable {
        C5116p() {
        }

        public void run() {
            ClientMain.this.engineGame.bhc().bGS().mo2194gZ();
        }
    }

    /* renamed from: taikodom.game.main.ClientMain$o */
    /* compiled from: a */
    class C5115o implements DataGameEventImpl.C1966a {
        C5115o() {
        }

        /* renamed from: s */
        public void mo15694s(String str) {
            ClientMain.this.engineGame.getEventManager().mo13975h(new C3932wv(14, str));
        }

        /* renamed from: a */
        public void mo15693a(String str, float f, float f2) {
            ClientMain.this.engineGame.getEventManager().mo13975h(new C3932wv(14, str, f, f2));
        }
    }
}
