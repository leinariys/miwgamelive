package taikodom.game.main.bot;

import game.script.main.bot.BotScript;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.C1285Sv;
import p001a.C6605aqF;

/* compiled from: a */
public class BotScript$BotScriptConsole implements BotScript$BotScriptConsoleMBean {
    private static /* synthetic */ int[] inu;
    /* renamed from: int  reason: not valid java name */
    final /* synthetic */ BotScript f10385int;
    private long aNu = 0;
    private double inh = ScriptRuntime.NaN;
    private long ini = 0;
    private long inj;
    private long ink;
    private long inl;
    private long inm;
    private long inn;
    private long ino;
    private long inp;
    private long inq;
    private long inr;
    private int ins;

    public BotScript$BotScriptConsole(BotScript ati) {
        this.f10385int = ati;
    }

    static /* synthetic */ int[] diy() {
        int[] iArr = inu;
        if (iArr == null) {
            iArr = new int[C1285Sv.C1286a.values().length];
            try {
                iArr[C1285Sv.C1286a.Docked.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C1285Sv.C1286a.LongFlight.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C1285Sv.C1286a.Minning.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C1285Sv.C1286a.NPCFightMoving.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C1285Sv.C1286a.NPCFightStopped.ordinal()] = 7;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[C1285Sv.C1286a.PVPFight.ordinal()] = 5;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[C1285Sv.C1286a.ShortFlight.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[C1285Sv.C1286a.Waiting.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            inu = iArr;
        }
        return iArr;
    }

    public float getRawPingTime_inst_ms() {
        float cvt;
        synchronized (BotScript.gyx) {
            if (BotScript.gyr.ifZ <= 0) {
                cvt = BotScript.gys;
            } else {
                BotScript.gys = BotScript.gyr.ifY;
                BotScript.gyr.reset();
                cvt = BotScript.gys;
            }
        }
        return cvt;
    }

    public float getRawPingTime_avg_ms() {
        float f;
        synchronized (BotScript.gyx) {
            f = BotScript.gyq.ifY;
        }
        return f;
    }

    public long getResponseTime_max_ms() {
        return BotScript.gyv;
    }

    public long getResponseTime_min_ms() {
        return BotScript.gyw;
    }

    public double getResponseTime_stdDev_ms() {
        return Math.sqrt((((double) BotScript.gyB) - (((double) (BotScript.gyA * BotScript.gyA)) / ((double) BotScript.gyz))) / ((double) BotScript.gyz));
    }

    private void diw() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis >= this.aNu + 1000) {
            this.aNu = currentTimeMillis;
            long[] cvp = BotScript.cvp();
            long j = cvp[0];
            long j2 = cvp[1];
            if (j != 0) {
                this.ini = j;
                this.inh = ((double) j2) / ((double) j);
            }
        }
    }

    public double getResponseTime_inst_ms() {
        diw();
        return this.inh;
    }

    public long getResponseTime_inst_points() {
        diw();
        return this.ini;
    }

    public double getResponseTime_avg_ms() {
        return ((double) BotScript.gyA) / ((double) BotScript.gyz);
    }

    public long getNumBots() {
        dix();
        return BotScript.gyC - ((long) this.ins);
    }

    public long getBotWeight() {
        dix();
        return this.inr;
    }

    public long getDockedCount() {
        dix();
        return this.inq;
    }

    public long getLongFlightCount() {
        dix();
        return this.inp;
    }

    public long getMinningCount() {
        dix();
        return this.ino;
    }

    public long getNPCFightMovingCount() {
        dix();
        return this.inn;
    }

    public long getNPCFightStoppedCount() {
        dix();
        return this.inm;
    }

    public long getPVPFightCount() {
        dix();
        return this.inl;
    }

    public long getShortFlightCount() {
        dix();
        return this.ink;
    }

    public long getWaitingCount() {
        dix();
        return this.inj;
    }

    public long getDeadBotsCount() {
        dix();
        return (long) this.ins;
    }

    private void dix() {
        if (BotScript.gyl) {
            synchronized (BotScript.gym) {
                if (BotScript.gyl) {
                    this.inj = 0;
                    this.ink = 0;
                    this.inl = 0;
                    this.inm = 0;
                    this.inn = 0;
                    this.ino = 0;
                    this.inp = 0;
                    this.inq = 0;
                    this.inr = 0;
                    this.ins = 0;
                    for (C6605aqF next : BotScript.gym.values()) {
                        if (next.timeStamp + 60000 < System.currentTimeMillis()) {
                            this.ins++;
                        } else {
                            this.inr += (long) next.modifier;
                            if (next.gqQ != null) {
                                this.inr += (long) next.gqQ.ffE;
                                switch (diy()[next.gqQ.ordinal()]) {
                                    case 1:
                                        this.inp++;
                                        break;
                                    case 2:
                                        this.ink++;
                                        break;
                                    case 3:
                                        this.inq++;
                                        break;
                                    case 4:
                                        this.ino++;
                                        break;
                                    case 5:
                                        this.inl++;
                                        break;
                                    case 6:
                                        this.inn++;
                                        break;
                                    case 7:
                                        this.inm++;
                                        break;
                                    case 8:
                                        this.inj++;
                                        break;
                                }
                            }
                        }
                    }
                    BotScript.gyl = false;
                }
            }
        }
    }
}
