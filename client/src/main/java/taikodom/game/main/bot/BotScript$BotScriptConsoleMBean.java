package taikodom.game.main.bot;

/* compiled from: a */
public interface BotScript$BotScriptConsoleMBean {
    long getBotWeight();

    long getDeadBotsCount();

    long getDockedCount();

    long getLongFlightCount();

    long getMinningCount();

    long getNPCFightMovingCount();

    long getNPCFightStoppedCount();

    long getNumBots();

    long getPVPFightCount();

    float getRawPingTime_avg_ms();

    float getRawPingTime_inst_ms();

    double getResponseTime_avg_ms();

    double getResponseTime_inst_ms();

    long getResponseTime_inst_points();

    long getResponseTime_max_ms();

    long getResponseTime_min_ms();

    double getResponseTime_stdDev_ms();

    long getShortFlightCount();

    long getWaitingCount();
}
