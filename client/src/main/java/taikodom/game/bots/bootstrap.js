function print(m) {
    java.lang.System.out.println(m)
}

var _x_sleep = 0
var _x_waitForSpawn = false
var _x_cont = null

function sleep(ms) {
    _x_sleep = ms
    _x_cont = new Continuation()
    throw "_x_cont"
}

function waitPlayerSpawn() {
    _x_waitForSpawn = true
    _x_cont = new Continuation()
    throw "_x_cont"
}

state = function (r) {
    return JavaAdapter(Packages.java.lang.Runnable, {
        run: function () {
            r()
        }
    })
}
