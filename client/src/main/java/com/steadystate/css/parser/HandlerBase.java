package com.steadystate.css.parser;

import com.steadystate.css.sac.DocumentHandler;
import logic.res.css.CSSParseException;
import logic.res.css.SACMediaList;
import org.w3c.css.sac.ErrorHandler;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.SelectorList;

/* renamed from: a.YE */
/* compiled from: a */
public class HandlerBase implements ErrorHandler, DocumentHandler {
    /* renamed from: a */
    public void startDocument(InputSource fh) {
    }

    /* renamed from: b */
    public void endDocument(InputSource fh) {
    }

    public void comment(String str) {
    }

    /* renamed from: Q */
    public void ignorableAtRule(String str) {
    }

    /* renamed from: d */
    public void mo6346d(String str, String str2) {
    }

    /* renamed from: a */
    public void ignorableAtRule(String str, SACMediaList atf, String str2) {
    }

    /* renamed from: a */
    public void startMedia(SACMediaList atf) {
    }

    /* renamed from: b */
    public void endMedia(SACMediaList atf) {
    }

    /* renamed from: e */
    public void mo6347e(String str, String str2) {
    }

    /* renamed from: f */
    public void endPage(String str, String str2) {
    }

    /* renamed from: jv */
    public void mo6350jv() {
    }

    /* renamed from: jw */
    public void endFontFace() {
    }

    /* renamed from: a */
    public void mo6338a(SelectorList ahq) {
    }

    /* renamed from: b */
    public void endSelector(SelectorList ahq) {
    }

    /* renamed from: a */
    public void mo6341a(String str, LexicalUnit ald, boolean z) {
    }

    /* renamed from: a */
    public void mo6919a(CSSParseException bw) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(bw.getURI()).append(" [").append(bw.getLineNumber()).append(":").append(bw.getColumnNumber()).append("] ").append(bw.getMessage());
        System.err.println(stringBuffer.toString());
    }

    /* renamed from: b */
    public void mo6920b(CSSParseException bw) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(bw.getURI()).append(" [").append(bw.getLineNumber()).append(":").append(bw.getColumnNumber()).append("] ").append(bw.getMessage());
        System.err.println(stringBuffer.toString());
    }

    /* renamed from: c */
    public void mo6921c(CSSParseException bw) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(bw.getURI()).append(" [").append(bw.getLineNumber()).append(":").append(bw.getColumnNumber()).append("] ").append(bw.getMessage());
        System.err.println(stringBuffer.toString());
    }
}
