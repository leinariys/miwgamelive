package com.steadystate.css.parser;

import com.steadystate.css.dom.CSSStyleSheetImpl;
import com.steadystate.css.dom.CSSValueImpl;
import com.steadystate.css.sac.DocumentHandler;
import logic.res.css.*;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.Parser;
import org.w3c.css.sac.SelectorList;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.css.CSSValue;

import java.lang.ref.Reference;
import java.lang.ref.SoftReference;
import java.util.Properties;
import java.util.Stack;

/* renamed from: a.Vl */
/* compiled from: a */
public class CSSOMParser {
    /* renamed from: yv */
    private static final String DEFAULT_PARSER = ParseSpecificationCSS2.class.getName();
    private static ThreadLocal<Reference<CSSOMParser>> LOCK = new C5859abn();
    /* renamed from: qq */
    public CSSStyleSheetImpl parentStyleSheet_ = null;
    /* access modifiers changed from: private */
    private Parser parser_ = null;
    /* renamed from: qr */
    private CSSRule f1910qr = null;

    public CSSOMParser() {
        try {
            setProperty("org.w3c.css.sac.parser", DEFAULT_PARSER);
            this.parser_ = new ParserFactory().makeParser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static CSSOMParser getInstance() {
        CSSOMParser vl = (CSSOMParser) LOCK.get().get();
        if (vl != null) {
            return vl;
        }
        CSSOMParser vl2 = new CSSOMParser();
        LOCK.set(new SoftReference(vl2));
        return vl2;
    }

    public static void setProperty(String str, String str2) {
        Properties properties = System.getProperties();
        properties.put(str, str2);
        System.setProperties(properties);
    }

    /* renamed from: c */
    public CSSStyleSheet parseStyleSheet(InputSource fh) {
        if (this.parser_ == null) {
            System.err.println("Null parser!");
        }
        CSSOMHandler handler = new CSSOMHandler();
        this.parser_.setDocumentHandler((DocumentHandler) handler);
        this.parser_.parseStyleSheet(fh);
        return (CSSStyleSheet) handler.getRoot();
    }

    /* renamed from: d */
    public CSSStyleDeclaration parseStyleDeclaration(InputSource fh) {
        CSSStyleDeclarationImpl ahx = new CSSStyleDeclarationImpl((CSSRule) null);
        parseStyleDeclaration((CSSStyleDeclaration) ahx, fh);
        return ahx;
    }

    /* renamed from: a */
    public void parseStyleDeclaration(CSSStyleDeclaration cSSStyleDeclaration, InputSource fh) {
        Stack stack = new Stack();
        stack.push(cSSStyleDeclaration);
        this.parser_.setDocumentHandler((DocumentHandler) new CSSOMHandler(stack));
        this.parser_.parseStyleDeclaration(fh);
    }

    /* renamed from: e */
    public CSSValue parsePropertyValue(InputSource fh) {
        this.parser_.setDocumentHandler((DocumentHandler) new CSSOMHandler());
        return new CSSValueImpl(this.parser_.parsePropertyValue(fh));
    }

    /* renamed from: f */
    public CSSRule parseRule(InputSource fh) {
        CSSOMHandler aVar = new CSSOMHandler();
        this.parser_.setDocumentHandler((DocumentHandler) aVar);
        this.parser_.parseRule(fh);
        return (CSSRule) aVar.getRoot();
    }

    /* renamed from: g */
    public SelectorList parseSelectors(InputSource fh) {
        this.parser_.setDocumentHandler((DocumentHandler) new HandlerBase());
        return this.parser_.parseSelectors(fh);
    }

    /* renamed from: a */
    public void setParentStyleSheet(CSSStyleSheetImpl parentStyleSheet) {
        this.parentStyleSheet_ = parentStyleSheet;
    }

    /* renamed from: b */
    public void getParentStyleSheet(CSSRule cSSRule) {
        this.f1910qr = cSSRule;
    }

    /* renamed from: a.Vl$a */
    class CSSOMHandler implements DocumentHandler {
        private Stack nodeStack_;
        private Object root_;

        public CSSOMHandler(Stack stack) {
            this.root_ = null;
            this.nodeStack_ = stack;
        }

        public CSSOMHandler() {
            this.root_ = null;
            this.nodeStack_ = new Stack();
        }

        public Object getRoot() {
            return this.root_;
        }

        /* renamed from: a */
        public void startDocument(InputSource fh) {
            if (this.nodeStack_.empty()) {
                CSSStyleSheetImpl ss = new CSSStyleSheetImpl();
                CSSOMParser.this.parentStyleSheet_ = ss;
                CSSRuleListImpl rk = new CSSRuleListImpl();
                ss.mo5395a(rk);
                this.nodeStack_.push(ss);
                this.nodeStack_.push(rk);
            }
        }

        /* renamed from: b */
        public void endDocument(InputSource fh) {
            this.nodeStack_.pop();
            this.root_ = this.nodeStack_.pop();
        }

        public void comment(String str) {
        }

        private CSSRule getParentRule() {
            if (!nodeStack_.empty() && nodeStack_.size() > 1) {
                final Object node = nodeStack_.get(nodeStack_.size() - 2);
                if (node instanceof CSSRule) {
                    return (CSSRule) node;
                }
            }
            return null;
        }

        /* renamed from: Q */
        public void ignorableAtRule(String str) {
            CSSUnknownRuleImpl atf = new CSSUnknownRuleImpl(
                CSSOMParser.this.parentStyleSheet_,
                (CSSRule) null, //todo getParentRule()
                str);
            if (!this.nodeStack_.empty()) {
                ((CSSRuleListImpl) this.nodeStack_.peek()).add(atf);
            } else {
                this.root_ = atf;
            }
        }

        /* renamed from: d */
        public void mo6346d(String str, String str2) {
        }

        /* renamed from: a */
        public void ignorableAtRule(String str, SACMediaList atf, String str2) {
            CssRuleImport cfVar = new CssRuleImport(CSSOMParser.this.parentStyleSheet_, (CSSRule) null, str, new C4132zw(atf));
            if (!this.nodeStack_.empty()) {
                ((CSSRuleListImpl) this.nodeStack_.peek()).add(cfVar);
            } else {
                this.root_ = cfVar;
            }
        }

        /* renamed from: a */
        public void startMedia(SACMediaList atf) {
            CssRuleMedia ani = new CssRuleMedia(CSSOMParser.this.parentStyleSheet_, (CSSRule) null, new C4132zw(atf));
            if (!this.nodeStack_.empty()) {
                ((CSSRuleListImpl) this.nodeStack_.peek()).add(ani);
            }
            CSSRuleListImpl rk = new CSSRuleListImpl();
            ani.mo10212a(rk);
            this.nodeStack_.push(ani);
            this.nodeStack_.push(rk);
        }

        /* renamed from: b */
        public void endMedia(SACMediaList atf) {
            this.nodeStack_.pop();
            this.root_ = this.nodeStack_.pop();
        }

        /* renamed from: e */
        public void mo6347e(String str, String str2) {
            CssRulePage aie = new CssRulePage(CSSOMParser.this.parentStyleSheet_, (CSSRule) null, str, str2);
            if (!this.nodeStack_.empty()) {
                ((CSSRuleListImpl) this.nodeStack_.peek()).add(aie);
            }
            CSSStyleDeclarationImpl ahx = new CSSStyleDeclarationImpl(aie);
            aie.mo13765a(ahx);
            this.nodeStack_.push(aie);
            this.nodeStack_.push(ahx);
        }

        /* renamed from: f */
        public void endPage(String str, String str2) {
            this.nodeStack_.pop();
            this.root_ = this.nodeStack_.pop();
        }

        /* renamed from: jv */
        public void mo6350jv() {
            CssRuleFontFace fw = new CssRuleFontFace(CSSOMParser.this.parentStyleSheet_, (CSSRule) null);
            if (!this.nodeStack_.empty()) {
                ((CSSRuleListImpl) this.nodeStack_.peek()).add(fw);
            }
            CSSStyleDeclarationImpl ahx = new CSSStyleDeclarationImpl(fw);
            fw.mo2156a(ahx);
            this.nodeStack_.push(fw);
            this.nodeStack_.push(ahx);
        }

        /* renamed from: jw */
        public void endFontFace() {
            this.nodeStack_.pop();
            this.root_ = this.nodeStack_.pop();
        }

        /* renamed from: a */
        public void mo6338a(SelectorList ahq) {
            aQJ aqj = new aQJ(CSSOMParser.this.parentStyleSheet_, (CSSRule) null, ahq);
            if (!this.nodeStack_.empty()) {
                ((CSSRuleListImpl) this.nodeStack_.peek()).add(aqj);
            }
            CSSStyleDeclarationImpl ahx = new CSSStyleDeclarationImpl(aqj);
            aqj.mo10899a(ahx);
            this.nodeStack_.push(aqj);
            this.nodeStack_.push(ahx);
        }

        /* renamed from: b */
        public void endSelector(SelectorList ahq) {
            this.nodeStack_.pop();
            this.root_ = this.nodeStack_.pop();
        }

        /* renamed from: a */
        public void mo6341a(String str, LexicalUnit ald, boolean z) {
            ((CSSStyleDeclarationImpl) this.nodeStack_.peek()).mo9285a(new C5576aOq(str, new CSSValueImpl(ald), z));
        }
    }
}
