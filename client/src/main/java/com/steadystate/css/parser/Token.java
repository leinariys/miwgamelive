package com.steadystate.css.parser;

/* renamed from: a.zV */
/* compiled from: a */
public class Token {
    public int beginColumn;
    public int beginLine;
    public Token next;
    public Token cct;
    public int endColumn;
    public int endLine;
    public String image;
    public int kind;

    /* renamed from: fQ */
    public static final Token newToken(int i) {
        return new Token();
    }

    public final String toString() {
        return this.image;
    }
}
