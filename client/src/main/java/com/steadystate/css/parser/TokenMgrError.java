package com.steadystate.css.parser;

/* renamed from: a.cu */
/* compiled from: a */
public class TokenMgrError extends Error {

    /* renamed from: su */
    static final int LEXICAL_ERROR = 0;

    /* renamed from: sv */
    static final int STATIC_LEXER_ERROR = 1;

    /* renamed from: sw */
    static final int INVALID_LEXICAL_STATE = 2;

    /* renamed from: sx */
    static final int LOOP_DETECTED = 3;
    int errorCode;

    public TokenMgrError() {
    }

    public TokenMgrError(String str, int i) {
        super(str);
        this.errorCode = i;
    }

    public TokenMgrError(boolean z, int i, int i2, int i3, String str, char c, int i4) {
        this(LexicalError(z, i, i2, i3, str, c), i4);
    }

    public static final String addEscapes(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case 0:
                    break;
                case 8:
                    stringBuffer.append("\\b");
                    break;
                case 9:
                    stringBuffer.append("\\t");
                    break;
                case 10:
                    stringBuffer.append("\\n");
                    break;
                case 12:
                    stringBuffer.append("\\f");
                    break;
                case 13:
                    stringBuffer.append("\\r");
                    break;
                case '\"':
                    stringBuffer.append("\\\"");
                    break;
                case '\'':
                    stringBuffer.append("\\'");
                    break;
                case '\\':
                    stringBuffer.append("\\\\");
                    break;
                default:
                    char charAt = str.charAt(i);
                    if (charAt >= ' ' && charAt <= '~') {
                        stringBuffer.append(charAt);
                        break;
                    } else {
                        String str2 = "0000" + Integer.toString(charAt, 16);
                        stringBuffer.append("\\u" + str2.substring(str2.length() - 4, str2.length()));
                        break;
                    }
            }
        }
        return stringBuffer.toString();
    }

    private static final String LexicalError(boolean z, int i, int i2, int i3, String str, char c) {
        return "Lexical error at line " + i2 + ", column " + i3 + ".  Encountered: " + (z ? "<EOF> " : "\"" + addEscapes(String.valueOf(c)) + "\"" + " (" + c + "), ") + "after : \"" + addEscapes(str) + "\"";
    }

    public String getMessage() {
        return super.getMessage();
    }
}
