package com.steadystate.css.parser;

import java.io.IOException;

/* renamed from: a.ho */
/* compiled from: a */
public interface CharStream {
    char BeginToken() throws IOException;

    void Done();

    String GetImage();

    char[] GetSuffix(int i);

    void backup(int i);

    int getBeginColumn();

    int getBeginLine();

    int getColumn();

    int getEndColumn();

    int getEndLine();

    int getLine();

    char readChar() throws IOException;
}
