package com.steadystate.css.parser;


import com.steadystate.css.sac.DocumentHandler;
import logic.res.css.*;
import org.w3c.css.sac.*;

import java.util.Enumeration;
import java.util.Locale;
import java.util.Vector;

/* renamed from: a.VK */
/* compiled from: a */
public class ParseSpecificationCSS2 implements SACParserCSS2Constants, Parser {
    public SACParserCSS2TokenManager token_source;
    private int[] evq;
    private int[] evr;
    private int[] evs;
    public Token token;
    public Token jj_nt;
    private int[] jj_la1;
    private JJCalls[] jj_2_rtns;
    public boolean lookingAhead;
    private Locale locale_;
    private int evA;
    private InputSource source_;
    private DocumentHandler documentHandler_;
    private ErrorHandler errorHandler_;
    private C1704ZE evd;
    private C0864MW eve;
    private boolean evf;
    private int jj_ntk;
    private Token jj_lastpos;
    private Token jj_scanpos;
    private int jj_la;
    private boolean evn;
    private int jj_gen;
    private boolean jj_rescan;
    private int evv;
    private Vector evw;
    private int[] evx;
    private int evy;
    private int[] evz;

    public ParseSpecificationCSS2() {
        this((CharStream) null);
    }

    public ParseSpecificationCSS2(CharStream hoVar) {
        this.source_ = null;
        this.locale_ = null;
        this.documentHandler_ = null;
        this.errorHandler_ = null;
        this.evd = new C3548sT();
        this.eve = new aJW();
        this.evf = true;
        this.lookingAhead = false;
        this.jj_la1 = new int[95];
        int[] iArr = new int[95];
        iArr[1] = 50331650;
        iArr[2] = 50331650;
        iArr[3] = 268435456;
        iArr[4] = 50331650;
        iArr[5] = 50331650;
        iArr[6] = -536212224;
        iArr[7] = -536212224;
        iArr[8] = 50331650;
        iArr[9] = 50331650;
        iArr[10] = -267776768;
        iArr[11] = 2;
        iArr[12] = 2;
        iArr[13] = 2;
        iArr[14] = 9437184;
        iArr[15] = 2;
        iArr[17] = 2;
        iArr[18] = 2;
        iArr[19] = 537529600;
        iArr[20] = 128;
        iArr[21] = 2;
        iArr[22] = 537529600;
        iArr[23] = 2;
        iArr[24] = 537529600;
        iArr[25] = 537529600;
        iArr[26] = 2;
        iArr[27] = 2;
        iArr[28] = 2;
        iArr[29] = 2;
        iArr[30] = 2;
        iArr[31] = 1024;
        iArr[32] = 1024;
        iArr[33] = 2;
        iArr[35] = 512;
        iArr[36] = 2;
        iArr[38] = 2;
        iArr[39] = 2;
        iArr[41] = 512;
        iArr[42] = 2;
        iArr[44] = 2;
        iArr[45] = 2;
        iArr[46] = 4224;
        iArr[47] = 2;
        iArr[48] = 2;
        iArr[49] = 73728;
        iArr[50] = 2;
        iArr[51] = 73728;
        iArr[52] = 73730;
        iArr[53] = 24576;
        iArr[54] = 2;
        iArr[55] = 2;
        iArr[57] = 512;
        iArr[58] = 2;
        iArr[60] = 128;
        iArr[61] = 2;
        iArr[62] = 2;
        iArr[63] = 656640;
        iArr[64] = 656640;
        iArr[65] = 656640;
        iArr[66] = 656640;
        iArr[67] = 658688;
        iArr[68] = 2048;
        iArr[69] = 2;
        iArr[70] = 2;
        iArr[71] = 201359360;
        iArr[72] = 2;
        iArr[73] = 1048576;
        iArr[74] = 2;
        iArr[75] = 201359360;
        iArr[76] = 2;
        iArr[77] = 2;
        iArr[79] = 2;
        iArr[81] = 512;
        iArr[82] = 2;
        iArr[84] = 2;
        iArr[86] = 2;
        iArr[87] = 9990272;
        iArr[88] = 4224;
        iArr[89] = 24576;
        iArr[91] = 9961472;
        iArr[92] = 2;
        iArr[93] = 2;
        iArr[94] = 2;
        this.evq = iArr;
        int[] iArr2 = new int[95];
        iArr2[0] = 1;
        iArr2[6] = 16777218;
        iArr2[7] = 16777218;
        iArr2[10] = 16777219;
        iArr2[16] = 16777216;
        iArr2[19] = 16777218;
        iArr2[22] = 16777218;
        iArr2[24] = 16777218;
        iArr2[25] = 16777218;
        iArr2[31] = 16777216;
        iArr2[32] = 16777216;
        iArr2[34] = 16777216;
        iArr2[37] = 16777216;
        iArr2[40] = 16777216;
        iArr2[43] = 16777216;
        iArr2[56] = 16777216;
        iArr2[59] = 16777216;
        iArr2[67] = 16777216;
        iArr2[68] = 16777216;
        iArr2[73] = 16777216;
        iArr2[78] = 25165824;
        iArr2[80] = 16777216;
        iArr2[83] = 16777216;
        iArr2[85] = 4;
        iArr2[87] = 167772152;
        iArr2[90] = 12058608;
        iArr2[91] = 167772152;
        this.evr = iArr2;
        this.evs = new int[95];
        this.jj_2_rtns = new JJCalls[2];
        this.jj_rescan = false;
        this.evv = 0;
        this.evw = new Vector();
        this.evy = -1;
        this.evz = new int[100];
        this.token_source = new SACParserCSS2TokenManager(hoVar);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 95; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public ParseSpecificationCSS2(SACParserCSS2TokenManager abv) {
        this.source_ = null;
        this.locale_ = null;
        this.documentHandler_ = null;
        this.errorHandler_ = null;
        this.evd = new C3548sT();
        this.eve = new aJW();
        this.evf = true;
        this.lookingAhead = false;
        this.jj_la1 = new int[95];
        int[] iArr = new int[95];
        iArr[1] = 50331650;
        iArr[2] = 50331650;
        iArr[3] = 268435456;
        iArr[4] = 50331650;
        iArr[5] = 50331650;
        iArr[6] = -536212224;
        iArr[7] = -536212224;
        iArr[8] = 50331650;
        iArr[9] = 50331650;
        iArr[10] = -267776768;
        iArr[11] = 2;
        iArr[12] = 2;
        iArr[13] = 2;
        iArr[14] = 9437184;
        iArr[15] = 2;
        iArr[17] = 2;
        iArr[18] = 2;
        iArr[19] = 537529600;
        iArr[20] = 128;
        iArr[21] = 2;
        iArr[22] = 537529600;
        iArr[23] = 2;
        iArr[24] = 537529600;
        iArr[25] = 537529600;
        iArr[26] = 2;
        iArr[27] = 2;
        iArr[28] = 2;
        iArr[29] = 2;
        iArr[30] = 2;
        iArr[31] = 1024;
        iArr[32] = 1024;
        iArr[33] = 2;
        iArr[35] = 512;
        iArr[36] = 2;
        iArr[38] = 2;
        iArr[39] = 2;
        iArr[41] = 512;
        iArr[42] = 2;
        iArr[44] = 2;
        iArr[45] = 2;
        iArr[46] = 4224;
        iArr[47] = 2;
        iArr[48] = 2;
        iArr[49] = 73728;
        iArr[50] = 2;
        iArr[51] = 73728;
        iArr[52] = 73730;
        iArr[53] = 24576;
        iArr[54] = 2;
        iArr[55] = 2;
        iArr[57] = 512;
        iArr[58] = 2;
        iArr[60] = 128;
        iArr[61] = 2;
        iArr[62] = 2;
        iArr[63] = 656640;
        iArr[64] = 656640;
        iArr[65] = 656640;
        iArr[66] = 656640;
        iArr[67] = 658688;
        iArr[68] = 2048;
        iArr[69] = 2;
        iArr[70] = 2;
        iArr[71] = 201359360;
        iArr[72] = 2;
        iArr[73] = 1048576;
        iArr[74] = 2;
        iArr[75] = 201359360;
        iArr[76] = 2;
        iArr[77] = 2;
        iArr[79] = 2;
        iArr[81] = 512;
        iArr[82] = 2;
        iArr[84] = 2;
        iArr[86] = 2;
        iArr[87] = 9990272;
        iArr[88] = 4224;
        iArr[89] = 24576;
        iArr[91] = 9961472;
        iArr[92] = 2;
        iArr[93] = 2;
        iArr[94] = 2;
        this.evq = iArr;
        int[] iArr2 = new int[95];
        iArr2[0] = 1;
        iArr2[6] = 16777218;
        iArr2[7] = 16777218;
        iArr2[10] = 16777219;
        iArr2[16] = 16777216;
        iArr2[19] = 16777218;
        iArr2[22] = 16777218;
        iArr2[24] = 16777218;
        iArr2[25] = 16777218;
        iArr2[31] = 16777216;
        iArr2[32] = 16777216;
        iArr2[34] = 16777216;
        iArr2[37] = 16777216;
        iArr2[40] = 16777216;
        iArr2[43] = 16777216;
        iArr2[56] = 16777216;
        iArr2[59] = 16777216;
        iArr2[67] = 16777216;
        iArr2[68] = 16777216;
        iArr2[73] = 16777216;
        iArr2[78] = 25165824;
        iArr2[80] = 16777216;
        iArr2[83] = 16777216;
        iArr2[85] = 4;
        iArr2[87] = 167772152;
        iArr2[90] = 12058608;
        iArr2[91] = 167772152;
        this.evr = iArr2;
        this.evs = new int[95];
        this.jj_2_rtns = new JJCalls[2];
        this.jj_rescan = false;
        this.evv = 0;
        this.evw = new Vector();
        this.evy = -1;
        this.evz = new int[100];
        this.token_source = abv;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 95; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public void setLocale(Locale locale) {
        this.locale_ = locale;
        throw new CSSException(CSSException.SAC_NOT_SUPPORTED_ERR);
    }

    /* renamed from: a */
    public void setDocumentHandler(DocumentHandler tvVar) {
        this.documentHandler_ = tvVar;
    }

    /* renamed from: a */
    public void mo5997a(C1704ZE ze) {
        this.evd = ze;
    }

    /* renamed from: a */
    public void mo5996a(C0864MW mw) {
        this.eve = mw;
    }

    /* renamed from: a */
    public void mo6001a(ErrorHandler mLVar) {
        this.errorHandler_ = mLVar;
    }

    /* renamed from: h */
    public void parseStyleSheet(InputSource source) {
        this.source_ = source;
        ReInit(getCharStream(source));
        try {
            styleSheet();
        } catch (ParseException e) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, e.getMessage(), e);
        }
    }

    /* renamed from: hl */
    public void parseStyleSheet(String str) {
        parseStyleSheet(new InputSource(str));
    }

    /* renamed from: i */
    public void parseStyleDeclaration(InputSource fh) {
        this.source_ = fh;
        ReInit(getCharStream(fh));
        try {
            bBV();
        } catch (ParseException e) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, e.getMessage(), e);
        }
    }

    /* renamed from: j */
    public void parseRule(InputSource fh) {
        this.source_ = fh;
        ReInit(getCharStream(fh));
        try {
            styleSheetRuleSingle();
        } catch (ParseException e) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, e.getMessage(), e);
        }
    }

    public String getParserVersion() {
        return "http://www.w3.org/TR/REC-CSS2";
    }

    /* renamed from: g */
    public SelectorList parseSelectors(InputSource fh) {
        this.source_ = fh;
        ReInit(getCharStream(fh));
        try {
            return bBS();
        } catch (ParseException e) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, e.getMessage(), e);
        }
    }

    /* renamed from: k */
    public LexicalUnit parsePropertyValue(InputSource fh) {
        this.source_ = fh;
        ReInit(getCharStream(fh));
        try {
            return expr();
        } catch (ParseException e) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, e.getMessage(), e);
        }
    }

    /* renamed from: l */
    public boolean parsePriority(InputSource fh) {
        this.source_ = fh;
        ReInit(getCharStream(fh));
        try {
            return bBX();
        } catch (ParseException e) {
            throw new CSSException(CSSException.SAC_SYNTAX_ERR, e.getMessage(), e);
        }
    }

    /* renamed from: m */
    private CharStream getCharStream(InputSource fh) {
        if (fh.getCharacterStream() != null) {
            return new C0986OW(fh.getCharacterStream(), 1, 1);
        }
        return null;
    }

    private aJE bBA() {
        return null;
    }

    public final void styleSheet() throws ParseException {
        try {
            this.documentHandler_.startDocument(this.source_);
            styleSheetRuleList();
            jj_consume_token(0);
        } finally {
            this.documentHandler_.endDocument(this.source_);
        }
    }


    public final void styleSheetRuleList() throws ParseException {
        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
            case 32:
                charsetRule();
                break;
            default:
                this.jj_la1[0] = this.jj_gen;
        }
        for (; ; ) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                case 24:
                case 25:
                    break;
                default:
                    this.jj_la1[1] = this.jj_gen;
                    break;
            }
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(1);
                    break;
                case 24:
                    jj_consume_token(24);
                    break;
                case 25:
                    jj_consume_token(25);
            }
        }

        /*
        this.evp[2] = this.evo;
        m10491oO(-1);
        throw new aNQ();
        switch (this.evj == -1 ? bCF() : this.evj) {
            case 28:
                break;
            default:
                this.evp[3] = this.evo;
                break;
        }

        bBG();
        for (; ; ) {
            switch (this.evj == -1 ? bCF() : this.evj) {
                case 1:
                case 24:
                case 25:
                    break;
                default:
                    this.evp[4] = this.evo;
                    break;
            }
            switch (this.evj == -1 ? bCF() : this.evj) {
                case 1:
                    m10491oO(1);
                    break;
                case 24:
                    m10491oO(24);
                    break;
                case 25:
                    m10491oO(25);
            }
        }
        this.evp[5] = this.evo;
        m10491oO(-1);
        throw new aNQ();
        switch (this.evj == -1 ? bCF() : this.evj) {
            case 8:
            case 10:
            case 11:
            case 17:
            case 19:
            case 29:
            case 30:
            case 31:
            case 33:
            case 56:
                break;
            default:
                this.evp[6] = this.evo;
                break;
        }
        switch (this.evj == -1 ? bCF() : this.evj) {
            case 8:
            case 10:
            case 11:
            case 17:
            case 19:
            case 56:
                bBR();
                break;
            case 30:
                bBH();
                break;
            case 29:
                bBL();
                break;
            case 31:
                bBN();
                break;
            case 33:
                bBF();
                break;
            default:
                this.evp[7] = this.evo;
                m10491oO(-1);
                throw new aNQ();
        }
        for (; ; ) {
            switch (this.evj == -1 ? bCF() : this.evj) {
                case 1:
                case 24:
                case 25:
                    break;
                default:
                    this.evp[8] = this.evo;
                    break;
            }
            switch (this.evj == -1 ? bCF() : this.evj) {
                case 1:
                    m10491oO(1);
                    break;
                case 24:
                    m10491oO(24);
                    break;
                case 25:
                    m10491oO(25);
            }
        }
        this.evp[9] = this.evo;
        m10491oO(-1);
        throw new aNQ();
        */
    }


    public final void styleSheetRuleSingle() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = jj_ntk_f();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case DOT:
            case COLON:
            case ASTERISK:
            case LSQUARE:
            case HASH:
            case IDENT:
                styleRule();
                return;
            case 28:
                importRule();
                return;
            case 29:
                pageRule();
                return;
            case 30:
                mediaRule();
                return;
            case 31:
                fontFaceRule();
                return;
            case 32:
                charsetRule();
                return;
            case 33:
                unknownAtRule();
                return;
            default:
                this.jj_la1[10] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final void charsetRule() throws ParseException {
        jj_consume_token(CHARSET_SYM);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(1);
                default:
                    this.jj_la1[11] = this.jj_gen;
                    jj_consume_token(STRING);
                    while (true) {
                        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                            case 1:
                                jj_consume_token(1);
                            default:
                                this.jj_la1[12] = this.jj_gen;
                                jj_consume_token(SEMICOLON);
                                return;
                        }
                    }
            }
        }
    }

    public final void unknownAtRule() throws ParseException {
        jj_consume_token(ATKEYWORD);
        this.documentHandler_.ignorableAtRule(skip());
    }

    public final void importRule() throws ParseException {
        Token zVVar;
        SACMediaListImpl asq = new SACMediaListImpl();
        jj_consume_token(IMPORT_SYM);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(1);
                default:
                    this.jj_la1[13] = this.jj_gen;
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 20:
                            zVVar = jj_consume_token(STRING);
                            break;
                        case 23:
                            zVVar = jj_consume_token(URI);
                            break;
                        default:
                            this.jj_la1[14] = this.jj_gen;
                            jj_consume_token(-1);
                            throw new ParseException();
                    }
                    while (true) {
                        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                            case 1:
                                jj_consume_token(1);
                            default:
                                this.jj_la1[15] = this.jj_gen;
                                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                                    case 56:
                                        mediaList(asq);
                                        break;
                                    default:
                                        this.jj_la1[16] = this.jj_gen;
                                        break;
                                }
                                jj_consume_token(SEMICOLON);
                                this.documentHandler_.ignorableAtRule(unescape(zVVar.image), (SACMediaList) asq, (String) null);
                                return;
                        }
                    }
            }
        }
    }

    public final void mediaRule() {
        Throwable th = new Throwable();
        int i;
        boolean start = false;
        SACMediaListImpl asq = new SACMediaListImpl();
        try {
            jj_consume_token(MEDIA_SYM);
            while (true) {
                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                    case 1:
                        jj_consume_token(S);
                    default:
                        this.jj_la1[17] = this.jj_gen;
                        mediaList(asq);
                        try {
                            this.documentHandler_.startMedia((SACMediaList) asq);
                            jj_consume_token(LBRACE);
                            while (true) {
                                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                                    case 1:
                                        jj_consume_token(1);
                                    default:
                                        this.jj_la1[18] = this.jj_gen;
                                        if (this.jj_ntk == -1) {
                                            i = jj_ntk_f();
                                        } else {
                                            i = this.jj_ntk;
                                        }
                                        switch (i) {
                                            case DOT:
                                            case COLON:
                                            case ASTERISK:
                                            case LSQUARE:
                                            case HASH:
                                            case PAGE_SYM:
                                            case ATKEYWORD:
                                            case IDENT:
                                                mediaRuleList();
                                                break;
                                            default:
                                                this.jj_la1[19] = this.jj_gen;
                                                break;
                                        }
                                        jj_consume_token(6);
                                        this.documentHandler_.endMedia((SACMediaList) asq);
                                        return;
                                }
                            }
                        } catch (Throwable th1) {
                            th = th1;
                            start = true;
                            break;
                        }
                }
                if (start) {
                    this.documentHandler_.endMedia((SACMediaList) asq);
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
        }
    }


    /* renamed from: a */
    public final void mediaList(SACMediaListImpl paramasq) throws ParseException {
        String str = medium();
        for (; ; ) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 7:
                    break;
                default:
                    this.jj_la1[20] = this.jj_gen;
                    break;
            }
            jj_consume_token(7);
            while (true) {
                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                    case 1:
                        break;
                    default:
                        this.jj_la1[21] = this.jj_gen;
                        break;
                }
                jj_consume_token(1);
            }
            //todo
            //paramasq.add(str);
            //str = bBK();
        }
        // paramasq.add(str);
    }


    public final void mediaRuleList() throws ParseException {
        for (; ; ) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 8:
                case 10:
                case 11:
                case 17:
                case 19:
                case 56:
                    styleRule();
                    break;
                case 29:
                    pageRule();
                    break;
                case 33:
                    unknownAtRule();
                    break;
                default:
                    this.jj_la1[22] = this.jj_gen;
                    jj_consume_token(-1);
                    throw new ParseException();
            }
            for (; ; ) {
                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                    case 1:
                        break;
                    default:
                        this.jj_la1[23] = this.jj_gen;
                        break;
                }
                jj_consume_token(1);
            }
            //todo
            /*
            switch (this.evj == -1 ? bCF() : this.evj)
            {
            }*/
        }
        //  this.evp[24] = this.evo;
    }

    public final void bBJ() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = jj_ntk_f();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 8:
            case 10:
            case 11:
            case 17:
            case 19:
            case 56:
                styleRule();
                return;
            case 29:
                pageRule();
                return;
            case 33:
                unknownAtRule();
                return;
            default:
                this.jj_la1[25] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final String medium() throws ParseException {
        Token oO = jj_consume_token(IDENT);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(1);
                default:
                    this.jj_la1[26] = this.jj_gen;
                    return oO.image;
            }
        }
    }


    public final void pageRule() throws ParseException {
        /*
            r9 = this;
            r0 = 0
            r3 = 1
            r8 = -1
            r2 = 0
            r1 = 29
            r9.m10491oO(r1)     // Catch:{ all -> 0x0096 }
        L_0x0009:
            int r1 = r9.evj     // Catch:{ all -> 0x0096 }
            if (r1 != r8) goto L_0x008d
            int r1 = r9.bCF()     // Catch:{ all -> 0x0096 }
        L_0x0011:
            switch(r1) {
                case 1: goto L_0x0090;
                default: goto L_0x0014;
            }     // Catch:{ all -> 0x0096 }
        L_0x0014:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0096 }
            r4 = 27
            int r5 = r9.evo     // Catch:{ all -> 0x0096 }
            r1[r4] = r5     // Catch:{ all -> 0x0096 }
            int r1 = r9.evj     // Catch:{ all -> 0x0096 }
            if (r1 != r8) goto L_0x00a9
            int r1 = r9.bCF()     // Catch:{ all -> 0x0096 }
        L_0x0024:
            switch(r1) {
                case 10: goto L_0x00ad;
                case 56: goto L_0x00ad;
                default: goto L_0x0027;
            }     // Catch:{ all -> 0x0096 }
        L_0x0027:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0096 }
            r4 = 32
            int r5 = r9.evo     // Catch:{ all -> 0x0096 }
            r1[r4] = r5     // Catch:{ all -> 0x0096 }
            r4 = r0
            r5 = r0
        L_0x0031:
            r1 = 5
            r9.m10491oO(r1)     // Catch:{ all -> 0x011a }
        L_0x0035:
            int r1 = r9.evj     // Catch:{ all -> 0x011a }
            if (r1 != r8) goto L_0x014b
            int r1 = r9.bCF()     // Catch:{ all -> 0x011a }
        L_0x003d:
            switch(r1) {
                case 1: goto L_0x014f;
                default: goto L_0x0040;
            }     // Catch:{ all -> 0x011a }
        L_0x0040:
            int[] r1 = r9.evp     // Catch:{ all -> 0x011a }
            r6 = 33
            int r7 = r9.evo     // Catch:{ all -> 0x011a }
            r1[r6] = r7     // Catch:{ all -> 0x011a }
            a.tv r2 = r9.evb     // Catch:{ all -> 0x0161 }
            if (r5 == 0) goto L_0x0155
            java.lang.String r1 = r5.image     // Catch:{ all -> 0x0161 }
            java.lang.String r1 = r9.unescape(r1)     // Catch:{ all -> 0x0161 }
        L_0x0052:
            r2.mo6347e(r1, r4)     // Catch:{ all -> 0x0161 }
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            if (r1 != r8) goto L_0x0158
            int r1 = r9.bCF()     // Catch:{ all -> 0x0161 }
        L_0x005d:
            switch(r1) {
                case 56: goto L_0x015c;
                default: goto L_0x0060;
            }     // Catch:{ all -> 0x0161 }
        L_0x0060:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0161 }
            r2 = 34
            int r6 = r9.evo     // Catch:{ all -> 0x0161 }
            r1[r2] = r6     // Catch:{ all -> 0x0161 }
        L_0x0068:
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            if (r1 != r8) goto L_0x0165
            int r1 = r9.bCF()     // Catch:{ all -> 0x0161 }
        L_0x0070:
            switch(r1) {
                case 9: goto L_0x0169;
                default: goto L_0x0073;
            }     // Catch:{ all -> 0x0161 }
        L_0x0073:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0161 }
            r2 = 35
            int r6 = r9.evo     // Catch:{ all -> 0x0161 }
            r1[r2] = r6     // Catch:{ all -> 0x0161 }
            r1 = 6
            r9.m10491oO(r1)     // Catch:{ all -> 0x0161 }
            a.tv r1 = r9.evb
            if (r5 == 0) goto L_0x0089
            java.lang.String r0 = r5.image
            java.lang.String r0 = r9.unescape(r0)
        L_0x0089:
            r1.mo6348f(r0, r4)
            return
        L_0x008d:
            int r1 = r9.evj     // Catch:{ all -> 0x0096 }
            goto L_0x0011
        L_0x0090:
            r1 = 1
            r9.m10491oO(r1)     // Catch:{ all -> 0x0096 }
            goto L_0x0009
        L_0x0096:
            r1 = move-exception
            r4 = r0
            r5 = r0
        L_0x0099:
            if (r2 == 0) goto L_0x00a8
            a.tv r2 = r9.evb
            if (r5 == 0) goto L_0x00a5
            java.lang.String r0 = r5.image
            java.lang.String r0 = r9.unescape(r0)
        L_0x00a5:
            r2.mo6348f(r0, r4)
        L_0x00a8:
            throw r1
        L_0x00a9:
            int r1 = r9.evj     // Catch:{ all -> 0x0096 }
            goto L_0x0024
        L_0x00ad:
            r1 = 2
            boolean r1 = r9.m10489oM(r1)     // Catch:{ all -> 0x0096 }
            if (r1 == 0) goto L_0x00db
            r1 = 56
            a.zV r5 = r9.m10491oO(r1)     // Catch:{ all -> 0x0096 }
        L_0x00ba:
            int r1 = r9.evj     // Catch:{ all -> 0x00d8 }
            if (r1 != r8) goto L_0x00d0
            int r1 = r9.bCF()     // Catch:{ all -> 0x00d8 }
        L_0x00c2:
            switch(r1) {
                case 1: goto L_0x00d3;
                default: goto L_0x00c5;
            }     // Catch:{ all -> 0x00d8 }
        L_0x00c5:
            int[] r1 = r9.evp     // Catch:{ all -> 0x00d8 }
            r4 = 28
            int r6 = r9.evo     // Catch:{ all -> 0x00d8 }
            r1[r4] = r6     // Catch:{ all -> 0x00d8 }
            r4 = r0
            goto L_0x0031
        L_0x00d0:
            int r1 = r9.evj     // Catch:{ all -> 0x00d8 }
            goto L_0x00c2
        L_0x00d3:
            r1 = 1
            r9.m10491oO(r1)     // Catch:{ all -> 0x00d8 }
            goto L_0x00ba
        L_0x00d8:
            r1 = move-exception
            r4 = r0
            goto L_0x0099
        L_0x00db:
            int r1 = r9.evj     // Catch:{ all -> 0x0096 }
            if (r1 != r8) goto L_0x00f8
            int r1 = r9.bCF()     // Catch:{ all -> 0x0096 }
        L_0x00e3:
            switch(r1) {
                case 10: goto L_0x0125;
                case 56: goto L_0x00fb;
                default: goto L_0x00e6;
            }     // Catch:{ all -> 0x0096 }
        L_0x00e6:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0096 }
            r3 = 31
            int r4 = r9.evo     // Catch:{ all -> 0x0096 }
            r1[r3] = r4     // Catch:{ all -> 0x0096 }
            r1 = -1
            r9.m10491oO(r1)     // Catch:{ all -> 0x0096 }
            a.aNQ r1 = new a.aNQ     // Catch:{ all -> 0x0096 }
            r1.<init>()     // Catch:{ all -> 0x0096 }
            throw r1     // Catch:{ all -> 0x0096 }
        L_0x00f8:
            int r1 = r9.evj     // Catch:{ all -> 0x0096 }
            goto L_0x00e3
        L_0x00fb:
            r1 = 56
            a.zV r5 = r9.m10491oO(r1)     // Catch:{ all -> 0x0096 }
            java.lang.String r4 = r9.bBM()     // Catch:{ all -> 0x00d8 }
        L_0x0105:
            int r1 = r9.evj     // Catch:{ all -> 0x011a }
            if (r1 != r8) goto L_0x011d
            int r1 = r9.bCF()     // Catch:{ all -> 0x011a }
        L_0x010d:
            switch(r1) {
                case 1: goto L_0x0120;
                default: goto L_0x0110;
            }     // Catch:{ all -> 0x011a }
        L_0x0110:
            int[] r1 = r9.evp     // Catch:{ all -> 0x011a }
            r6 = 29
            int r7 = r9.evo     // Catch:{ all -> 0x011a }
            r1[r6] = r7     // Catch:{ all -> 0x011a }
            goto L_0x0031
        L_0x011a:
            r1 = move-exception
            goto L_0x0099
        L_0x011d:
            int r1 = r9.evj     // Catch:{ all -> 0x011a }
            goto L_0x010d
        L_0x0120:
            r1 = 1
            r9.m10491oO(r1)     // Catch:{ all -> 0x011a }
            goto L_0x0105
        L_0x0125:
            java.lang.String r4 = r9.bBM()     // Catch:{ all -> 0x0096 }
        L_0x0129:
            int r1 = r9.evj     // Catch:{ all -> 0x0147 }
            if (r1 != r8) goto L_0x013f
            int r1 = r9.bCF()     // Catch:{ all -> 0x0147 }
        L_0x0131:
            switch(r1) {
                case 1: goto L_0x0142;
                default: goto L_0x0134;
            }     // Catch:{ all -> 0x0147 }
        L_0x0134:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0147 }
            r5 = 30
            int r6 = r9.evo     // Catch:{ all -> 0x0147 }
            r1[r5] = r6     // Catch:{ all -> 0x0147 }
            r5 = r0
            goto L_0x0031
        L_0x013f:
            int r1 = r9.evj     // Catch:{ all -> 0x0147 }
            goto L_0x0131
        L_0x0142:
            r1 = 1
            r9.m10491oO(r1)     // Catch:{ all -> 0x0147 }
            goto L_0x0129
        L_0x0147:
            r1 = move-exception
            r5 = r0
            goto L_0x0099
        L_0x014b:
            int r1 = r9.evj     // Catch:{ all -> 0x011a }
            goto L_0x003d
        L_0x014f:
            r1 = 1
            r9.m10491oO(r1)     // Catch:{ all -> 0x011a }
            goto L_0x0035
        L_0x0155:
            r1 = r0
            goto L_0x0052
        L_0x0158:
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            goto L_0x005d
        L_0x015c:
            r9.bBW()     // Catch:{ all -> 0x0161 }
            goto L_0x0068
        L_0x0161:
            r1 = move-exception
            r2 = r3
            goto L_0x0099
        L_0x0165:
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            goto L_0x0070
        L_0x0169:
            r1 = 9
            r9.m10491oO(r1)     // Catch:{ all -> 0x0161 }
        L_0x016e:
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            if (r1 != r8) goto L_0x0196
            int r1 = r9.bCF()     // Catch:{ all -> 0x0161 }
        L_0x0176:
            switch(r1) {
                case 1: goto L_0x0199;
                default: goto L_0x0179;
            }     // Catch:{ all -> 0x0161 }
        L_0x0179:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0161 }
            r2 = 36
            int r6 = r9.evo     // Catch:{ all -> 0x0161 }
            r1[r2] = r6     // Catch:{ all -> 0x0161 }
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            if (r1 != r8) goto L_0x019e
            int r1 = r9.bCF()     // Catch:{ all -> 0x0161 }
        L_0x0189:
            switch(r1) {
                case 56: goto L_0x01a1;
                default: goto L_0x018c;
            }     // Catch:{ all -> 0x0161 }
        L_0x018c:
            int[] r1 = r9.evp     // Catch:{ all -> 0x0161 }
            r2 = 37
            int r6 = r9.evo     // Catch:{ all -> 0x0161 }
            r1[r2] = r6     // Catch:{ all -> 0x0161 }
            goto L_0x0068
        L_0x0196:
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            goto L_0x0176
        L_0x0199:
            r1 = 1
            r9.m10491oO(r1)     // Catch:{ all -> 0x0161 }
            goto L_0x016e
        L_0x019e:
            int r1 = r9.evj     // Catch:{ all -> 0x0161 }
            goto L_0x0189
        L_0x01a1:
            r9.bBW()     // Catch:{ all -> 0x0161 }
            goto L_0x0068
        */
        // throw new UnsupportedOperationException("Method not decompiled: logic.res.css.C1448VK.bBL():void");
        throw new ParseException();
    }

    public final String bBM() throws ParseException {
        jj_consume_token(COLON);
        return jj_consume_token(IDENT).image;
    }

    public final void fontFaceRule() throws ParseException {
        /*
            r6 = this;
            r2 = 1
            r5 = -1
            r1 = 0
            r0 = 31
            r6.m10491oO(r0)     // Catch:{ all -> 0x006f }
        L_0x0008:
            int r0 = r6.evj     // Catch:{ all -> 0x006f }
            if (r0 != r5) goto L_0x0067
            int r0 = r6.bCF()     // Catch:{ all -> 0x006f }
        L_0x0010:
            switch(r0) {
                case 1: goto L_0x006a;
                default: goto L_0x0013;
            }     // Catch:{ all -> 0x006f }
        L_0x0013:
            int[] r0 = r6.evp     // Catch:{ all -> 0x006f }
            r3 = 38
            int r4 = r6.evo     // Catch:{ all -> 0x006f }
            r0[r3] = r4     // Catch:{ all -> 0x006f }
            r0 = 5
            r6.m10491oO(r0)     // Catch:{ all -> 0x006f }
        L_0x001f:
            int r0 = r6.evj     // Catch:{ all -> 0x006f }
            if (r0 != r5) goto L_0x0078
            int r0 = r6.bCF()     // Catch:{ all -> 0x006f }
        L_0x0027:
            switch(r0) {
                case 1: goto L_0x007b;
                default: goto L_0x002a;
            }     // Catch:{ all -> 0x006f }
        L_0x002a:
            int[] r0 = r6.evp     // Catch:{ all -> 0x006f }
            r3 = 39
            int r4 = r6.evo     // Catch:{ all -> 0x006f }
            r0[r3] = r4     // Catch:{ all -> 0x006f }
            a.tv r0 = r6.evb     // Catch:{ all -> 0x0087 }
            r0.mo6350jv()     // Catch:{ all -> 0x0087 }
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            if (r0 != r5) goto L_0x0080
            int r0 = r6.bCF()     // Catch:{ all -> 0x0087 }
        L_0x003f:
            switch(r0) {
                case 56: goto L_0x0083;
                default: goto L_0x0042;
            }     // Catch:{ all -> 0x0087 }
        L_0x0042:
            int[] r0 = r6.evp     // Catch:{ all -> 0x0087 }
            r1 = 40
            int r3 = r6.evo     // Catch:{ all -> 0x0087 }
            r0[r1] = r3     // Catch:{ all -> 0x0087 }
        L_0x004a:
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            if (r0 != r5) goto L_0x008a
            int r0 = r6.bCF()     // Catch:{ all -> 0x0087 }
        L_0x0052:
            switch(r0) {
                case 9: goto L_0x008d;
                default: goto L_0x0055;
            }     // Catch:{ all -> 0x0087 }
        L_0x0055:
            int[] r0 = r6.evp     // Catch:{ all -> 0x0087 }
            r1 = 41
            int r3 = r6.evo     // Catch:{ all -> 0x0087 }
            r0[r1] = r3     // Catch:{ all -> 0x0087 }
            r0 = 6
            r6.m10491oO(r0)     // Catch:{ all -> 0x0087 }
            a.tv r0 = r6.evb
            r0.mo6351jw()
            return
        L_0x0067:
            int r0 = r6.evj     // Catch:{ all -> 0x006f }
            goto L_0x0010
        L_0x006a:
            r0 = 1
            r6.m10491oO(r0)     // Catch:{ all -> 0x006f }
            goto L_0x0008
        L_0x006f:
            r0 = move-exception
        L_0x0070:
            if (r1 == 0) goto L_0x0077
            a.tv r1 = r6.evb
            r1.mo6351jw()
        L_0x0077:
            throw r0
        L_0x0078:
            int r0 = r6.evj     // Catch:{ all -> 0x006f }
            goto L_0x0027
        L_0x007b:
            r0 = 1
            r6.m10491oO(r0)     // Catch:{ all -> 0x006f }
            goto L_0x001f
        L_0x0080:
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            goto L_0x003f
        L_0x0083:
            r6.bBW()     // Catch:{ all -> 0x0087 }
            goto L_0x004a
        L_0x0087:
            r0 = move-exception
            r1 = r2
            goto L_0x0070
        L_0x008a:
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            goto L_0x0052
        L_0x008d:
            r0 = 9
            r6.m10491oO(r0)     // Catch:{ all -> 0x0087 }
        L_0x0092:
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            if (r0 != r5) goto L_0x00b9
            int r0 = r6.bCF()     // Catch:{ all -> 0x0087 }
        L_0x009a:
            switch(r0) {
                case 1: goto L_0x00bc;
                default: goto L_0x009d;
            }     // Catch:{ all -> 0x0087 }
        L_0x009d:
            int[] r0 = r6.evp     // Catch:{ all -> 0x0087 }
            r1 = 42
            int r3 = r6.evo     // Catch:{ all -> 0x0087 }
            r0[r1] = r3     // Catch:{ all -> 0x0087 }
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            if (r0 != r5) goto L_0x00c1
            int r0 = r6.bCF()     // Catch:{ all -> 0x0087 }
        L_0x00ad:
            switch(r0) {
                case 56: goto L_0x00c4;
                default: goto L_0x00b0;
            }     // Catch:{ all -> 0x0087 }
        L_0x00b0:
            int[] r0 = r6.evp     // Catch:{ all -> 0x0087 }
            r1 = 43
            int r3 = r6.evo     // Catch:{ all -> 0x0087 }
            r0[r1] = r3     // Catch:{ all -> 0x0087 }
            goto L_0x004a
        L_0x00b9:
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            goto L_0x009a
        L_0x00bc:
            r0 = 1
            r6.m10491oO(r0)     // Catch:{ all -> 0x0087 }
            goto L_0x0092
        L_0x00c1:
            int r0 = r6.evj     // Catch:{ all -> 0x0087 }
            goto L_0x00ad
        L_0x00c4:
            r6.bBW()     // Catch:{ all -> 0x0087 }
            goto L_0x004a
        */
        // throw new UnsupportedOperationException("Method not decompiled: logic.res.css.C1448VK.bBN():void");
        throw new ParseException();
    }

    /* renamed from: b */
    public final LexicalUnit mo6004b(LexicalUnit ald) throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = jj_ntk_f();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 7:
                jj_consume_token(COMMA);
                while (true) {
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 1:
                            jj_consume_token(1);
                        default:
                            this.jj_la1[45] = this.jj_gen;
                            return new LexicalUnitImpl(ald, 0);
                    }
                }
            case 12:
                jj_consume_token(SLASH);
                while (true) {
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 1:
                            jj_consume_token(1);
                        default:
                            this.jj_la1[44] = this.jj_gen;
                            return new LexicalUnitImpl(ald, 4);
                    }
                }
            default:
                this.jj_la1[46] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final char bBO() throws ParseException {
        int i;
        int i2;
        char c = '+';
        if (this.jj_ntk == -1) {
            i = jj_ntk_f();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 1:
                jj_consume_token(1);
                if (this.jj_ntk == -1) {
                    i2 = jj_ntk_f();
                } else {
                    i2 = this.jj_ntk;
                }
                switch (i2) {
                    case 13:
                    case 16:
                        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                            case 13:
                                jj_consume_token(PLUS);
                                break;
                            case 16:
                                jj_consume_token(GT);
                                c = '>';
                                break;
                            default:
                                this.jj_la1[49] = this.jj_gen;
                                jj_consume_token(-1);
                                throw new ParseException();
                        }
                        while (true) {
                            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                                case 1:
                                    jj_consume_token(1);
                                default:
                                    this.jj_la1[50] = this.jj_gen;
                                    return c;
                            }
                        }
                    default:
                        this.jj_la1[51] = this.jj_gen;
                        return ' ';
                }
            case 13:
                jj_consume_token(PLUS);
                while (true) {
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 1:
                            jj_consume_token(1);
                        default:
                            this.jj_la1[47] = this.jj_gen;
                            return '+';
                    }
                }
            case 16:
                jj_consume_token(GT);
                while (true) {
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 1:
                            jj_consume_token(1);
                        default:
                            this.jj_la1[48] = this.jj_gen;
                            return '>';
                    }
                }
            default:
                this.jj_la1[52] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final char bBP() throws ParseException {
        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
            case 13:
                jj_consume_token(PLUS);
                return '+';
            case 14:
                jj_consume_token(MINUS);
                return '-';
            default:
                this.jj_la1[53] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    public final String bBQ() throws ParseException {
        Token oO = jj_consume_token(IDENT);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(1);
                default:
                    this.jj_la1[54] = this.jj_gen;
                    return unescape(oO.image);
            }
        }
    }


    public final void styleRule() throws ParseException {
        /*
            r7 = this;
            r1 = 1
            r6 = -1
            r2 = 0
            r0 = 0
            a.aHQ r2 = r7.bBS()     // Catch:{ aNQ -> 0x005c }
            r3 = 5
            r7.m10491oO(r3)     // Catch:{ aNQ -> 0x005c }
        L_0x000c:
            int r3 = r7.evj     // Catch:{ aNQ -> 0x005c }
            if (r3 != r6) goto L_0x0054
            int r3 = r7.bCF()     // Catch:{ aNQ -> 0x005c }
        L_0x0014:
            switch(r3) {
                case 1: goto L_0x0057;
                default: goto L_0x0017;
            }     // Catch:{ aNQ -> 0x005c }
        L_0x0017:
            int[] r3 = r7.evp     // Catch:{ aNQ -> 0x005c }
            r4 = 55
            int r5 = r7.evo     // Catch:{ aNQ -> 0x005c }
            r3[r4] = r5     // Catch:{ aNQ -> 0x005c }
            a.tv r0 = r7.evb     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r0.mo6338a((p001a.aHQ) r2)     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            if (r0 != r6) goto L_0x0068
            int r0 = r7.bCF()     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x002c:
            switch(r0) {
                case 56: goto L_0x006b;
                default: goto L_0x002f;
            }     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x002f:
            int[] r0 = r7.evp     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r3 = 56
            int r4 = r7.evo     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r0[r3] = r4     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x0037:
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            if (r0 != r6) goto L_0x0072
            int r0 = r7.bCF()     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x003f:
            switch(r0) {
                case 9: goto L_0x0075;
                default: goto L_0x0042;
            }     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x0042:
            int[] r0 = r7.evp     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r3 = 57
            int r4 = r7.evo     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r0[r3] = r4     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r0 = 6
            r7.m10491oO(r0)     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            a.tv r0 = r7.evb
            r0.mo6343b((p001a.aHQ) r2)
        L_0x0053:
            return
        L_0x0054:
            int r3 = r7.evj     // Catch:{ aNQ -> 0x005c }
            goto L_0x0014
        L_0x0057:
            r3 = 1
            r7.m10491oO(r3)     // Catch:{ aNQ -> 0x005c }
            goto L_0x000c
        L_0x005c:
            r1 = move-exception
        L_0x005d:
            r7.bCb()     // Catch:{ all -> 0x00bc }
            if (r0 == 0) goto L_0x0053
            a.tv r0 = r7.evb
            r0.mo6343b((p001a.aHQ) r2)
            goto L_0x0053
        L_0x0068:
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x002c
        L_0x006b:
            r7.bBW()     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x0037
        L_0x006f:
            r0 = move-exception
            r0 = r1
            goto L_0x005d
        L_0x0072:
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x003f
        L_0x0075:
            r0 = 9
            r7.m10491oO(r0)     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x007a:
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            if (r0 != r6) goto L_0x00ac
            int r0 = r7.bCF()     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x0082:
            switch(r0) {
                case 1: goto L_0x00af;
                default: goto L_0x0085;
            }     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x0085:
            int[] r0 = r7.evp     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r3 = 58
            int r4 = r7.evo     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r0[r3] = r4     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            if (r0 != r6) goto L_0x00b4
            int r0 = r7.bCF()     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x0095:
            switch(r0) {
                case 56: goto L_0x00b7;
                default: goto L_0x0098;
            }     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
        L_0x0098:
            int[] r0 = r7.evp     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r3 = 59
            int r4 = r7.evo     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            r0[r3] = r4     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x0037
        L_0x00a1:
            r0 = move-exception
            r3 = r0
            r4 = r1
        L_0x00a4:
            if (r4 == 0) goto L_0x00ab
            a.tv r0 = r7.evb
            r0.mo6343b((p001a.aHQ) r2)
        L_0x00ab:
            throw r3
        L_0x00ac:
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x0082
        L_0x00af:
            r0 = 1
            r7.m10491oO(r0)     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x007a
        L_0x00b4:
            int r0 = r7.evj     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x0095
        L_0x00b7:
            r7.bBW()     // Catch:{ aNQ -> 0x006f, all -> 0x00a1 }
            goto L_0x0037
        L_0x00bc:
            r1 = move-exception
            r3 = r1
            r4 = r0
            goto L_0x00a4
        */
        // throw new UnsupportedOperationException("Method not decompiled: logic.res.css.C1448VK.bBR():void");
        throw new ParseException();
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002d A[Catch:{ aNQ -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0045 A[Catch:{ aNQ -> 0x004d }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0048 A[Catch:{ aNQ -> 0x004d }, LOOP:1: B:11:0x0029->B:17:0x0048, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0034 A[SYNTHETIC] */
    public final SelectorList bBS() throws ParseException {
        /*
            r6 = this;
            r5 = -1
            a.ayE r2 = new a.ayE
            r2.<init>()
            a.awz r0 = r6.bBT()     // Catch:{ aNQ -> 0x004d }
            r1 = r0
        L_0x000b:
            int r0 = r6.evj     // Catch:{ aNQ -> 0x004d }
            if (r0 != r5) goto L_0x0022
            int r0 = r6.bCF()     // Catch:{ aNQ -> 0x004d }
        L_0x0013:
            switch(r0) {
                case 7: goto L_0x0025;
                default: goto L_0x0016;
            }     // Catch:{ aNQ -> 0x004d }
        L_0x0016:
            int[] r0 = r6.evp     // Catch:{ aNQ -> 0x004d }
            r3 = 60
            int r4 = r6.evo     // Catch:{ aNQ -> 0x004d }
            r0[r3] = r4     // Catch:{ aNQ -> 0x004d }
            r2.add(r1)     // Catch:{ aNQ -> 0x004d }
            return r2
        L_0x0022:
            int r0 = r6.evj     // Catch:{ aNQ -> 0x004d }
            goto L_0x0013
        L_0x0025:
            r0 = 7
            r6.m10491oO(r0)     // Catch:{ aNQ -> 0x004d }
        L_0x0029:
            int r0 = r6.evj     // Catch:{ aNQ -> 0x004d }
            if (r0 != r5) goto L_0x0045
            int r0 = r6.bCF()     // Catch:{ aNQ -> 0x004d }
        L_0x0031:
            switch(r0) {
                case 1: goto L_0x0048;
                default: goto L_0x0034;
            }     // Catch:{ aNQ -> 0x004d }
        L_0x0034:
            int[] r0 = r6.evp     // Catch:{ aNQ -> 0x004d }
            r3 = 61
            int r4 = r6.evo     // Catch:{ aNQ -> 0x004d }
            r0[r3] = r4     // Catch:{ aNQ -> 0x004d }
            r2.add(r1)     // Catch:{ aNQ -> 0x004d }
            a.awz r0 = r6.bBT()     // Catch:{ aNQ -> 0x004d }
            r1 = r0
            goto L_0x000b
        L_0x0045:
            int r0 = r6.evj     // Catch:{ aNQ -> 0x004d }
            goto L_0x0031
        L_0x0048:
            r0 = 1
            r6.m10491oO(r0)     // Catch:{ aNQ -> 0x004d }
            goto L_0x0029
        L_0x004d:
            r0 = move-exception
            java.io.PrintStream r0 = java.lang.System.err
            java.lang.String r1 = "Exception in selectorList()"
            r0.println(r1)
            java.lang.Error r0 = new java.lang.Error
            java.lang.String r1 = "Missing return statement in function"
            r0.<init>(r1)
            throw r0
        */
        // throw new UnsupportedOperationException("Method not decompiled: logic.res.css.C1448VK.bBS():a.aHQ");
        throw new ParseException();
    }

    public final Selector bBT() {
        try {
            Selector a = mo5995a((Selector) null, ' ');
            while (m10490oN(2)) {
                a = mo5995a(a, bBO());
            }
            while (true) {
                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                    case 1:
                        jj_consume_token(1);
                    default:
                        this.jj_la1[62] = this.jj_gen;
                        return a;
                }
            }
        } catch (ParseException e) {
            bBZ();
            throw new Error("Missing return statement in function");
        }
    }


    public final Selector mo5995a(Selector paramawz, char paramChar) throws ParseException {
        Object localObject = null;
        Condition localaiB = null;
        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
            case 11:
            case 56:
                localObject = bBU();
                for (; ; ) {
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 8:
                        case 10:
                        case 17:
                        case 19:
                            break;
                        default:
                            this.jj_la1[63] = this.jj_gen;
                            break;
                    }
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 19:
                            localaiB = mo6041e(localaiB);
                            break;
                        case 8:
                            localaiB = mo6003b(localaiB);
                            break;
                        case 17:
                            localaiB = mo6036c(localaiB);
                            break;
                        case 10:
                            localaiB = mo6038d(localaiB);
                    }
                }
                //todo
                // this.evp[64] = this.evo;
                // m10491oO(-1);
                // throw new aNQ();
            case 8:
            case 10:
            case 17:
            case 19:
                localObject = this.evd.mo7334m(null, null);
                for (; ; ) {
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 19:
                            localaiB = mo6041e(localaiB);
                            break;
                        case 8:
                            localaiB = mo6003b(localaiB);
                            break;
                        case 17:
                            localaiB = mo6036c(localaiB);
                            break;
                        case 10:
                            localaiB = mo6038d(localaiB);
                            break;
                        default:
                            this.jj_la1[65] = this.jj_gen;
                            jj_consume_token(-1);
                            throw new ParseException();
                    }
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                    }
                }
                //  this.evp[66] = this.evo;

                //  break;
            default:
                this.jj_la1[67] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        /*
        if (localaiB != null) {
            localObject = this.evd.mo7325a((C0737KW)localObject, localaiB);
        }
        if (paramawz != null) {
            switch (paramChar)
            {
                case ' ':
                    paramawz = this.evd.mo7324a(paramawz, (C0737KW)localObject);
                    break;
                case '+':
                    paramawz = this.evd.mo7326a(paramawz.getSelectorType(), paramawz, (C0737KW)localObject);
                    break;
                case '>':
                    paramawz = this.evd.mo7330b(paramawz, (C0737KW)localObject);
            }
        } else {
            paramawz = (ISelector)localObject;
        }
        */
        //  return paramawz;
    }


    /* renamed from: b */
    public final Condition mo6003b(Condition aib) throws ParseException {
        jj_consume_token(DOT);
        AttributeCondition D = this.eve.mo3959D((String) null, jj_consume_token(56).image);
        return aib == null ? D : this.eve.mo3964a(aib, D);
    }

    public final SimpleSelector bBU() throws ParseException {
        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
            case 11:
                jj_consume_token(ASTERISK);
                return this.evd.mo7334m((String) null, (String) null);
            case 56:
                return this.evd.mo7334m((String) null, unescape(jj_consume_token(IDENT).image));
            default:
                this.jj_la1[68] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    /* renamed from: c */
    public final Condition mo6036c(Condition aib) throws ParseException {
        int i;
        int i2;
        char c;
        String str;
        AttributeCondition c2;
        jj_consume_token(LSQUARE);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(1);
                default:
                    this.jj_la1[69] = this.jj_gen;
                    String unescape = unescape(jj_consume_token(IDENT).image);
                    while (true) {
                        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                            case 1:
                                jj_consume_token(1);
                            default:
                                this.jj_la1[70] = this.jj_gen;
                                if (this.jj_ntk == -1) {
                                    i = jj_ntk_f();
                                } else {
                                    i = this.jj_ntk;
                                }
                                switch (i) {
                                    case 15:
                                    case 26:
                                    case 27:
                                        if (this.jj_ntk == -1) {
                                            i2 = jj_ntk_f();
                                        } else {
                                            i2 = this.jj_ntk;
                                        }
                                        switch (i2) {
                                            case 15:
                                                jj_consume_token(EQUALS);
                                                c = 1;
                                                break;
                                            case 26:
                                                jj_consume_token(INCLUDES);
                                                c = 2;
                                                break;
                                            case 27:
                                                jj_consume_token(DASHMATCH);
                                                c = 3;
                                                break;
                                            default:
                                                this.jj_la1[71] = this.jj_gen;
                                                jj_consume_token(-1);
                                                throw new ParseException();
                                        }
                                        while (true) {
                                            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                                                case 1:
                                                    jj_consume_token(1);
                                                default:
                                                    this.jj_la1[72] = this.jj_gen;
                                                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                                                        case 20:
                                                            str = unescape(jj_consume_token(STRING).image);
                                                            break;
                                                        case 56:
                                                            str = jj_consume_token(IDENT).image;
                                                            break;
                                                        default:
                                                            this.jj_la1[73] = this.jj_gen;
                                                            jj_consume_token(-1);
                                                            throw new ParseException();
                                                    }
                                                    while (true) {
                                                        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                                                            case 1:
                                                                jj_consume_token(1);
                                                            default:
                                                                this.jj_la1[74] = this.jj_gen;
                                                                break;
                                                        }
                                                    }
                                            }
                                        }
                                    default:
                                        this.jj_la1[75] = this.jj_gen;
                                        c = 0;
                                        str = null;
                                        break;
                                }
                                jj_consume_token(RSQUARE);
                                switch (c) {
                                    case 0:
                                        c2 = this.eve.mo3962a(unescape, (String) null, false, (String) null);
                                        break;
                                    case 1:
                                        c2 = this.eve.mo3962a(unescape, (String) null, false, str);
                                        break;
                                    case 2:
                                        c2 = this.eve.mo3965b(unescape, (String) null, false, str);
                                        break;
                                    case 3:
                                        c2 = this.eve.mo3969c(unescape, (String) null, false, str);
                                        break;
                                    default:
                                        c2 = null;
                                        break;
                                }
                                return aib == null ? c2 : this.eve.mo3964a(aib, c2);
                        }
                    }
            }
        }
    }

    /* renamed from: d */
    public final Condition mo6038d(Condition aib) throws ParseException {
        int i;
        jj_consume_token(COLON);
        if (this.jj_ntk == -1) {
            i = jj_ntk_f();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 55:
                String unescape = unescape(jj_consume_token(FUNCTION).image);
                while (true) {
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 1:
                            jj_consume_token(1);
                        default:
                            this.jj_la1[76] = this.jj_gen;
                            String unescape2 = unescape(jj_consume_token(IDENT).image);
                            while (true) {
                                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                                    case 1:
                                        jj_consume_token(1);
                                    default:
                                        this.jj_la1[77] = this.jj_gen;
                                        jj_consume_token(RROUND);
                                        if (unescape.equalsIgnoreCase("lang(")) {
                                            LangCondition eM = this.eve.mo3971eM(unescape(unescape2));
                                            if (aib != null) {
                                                return this.eve.mo3964a(aib, eM);
                                            }
                                            return eM;
                                        }
                                        throw new CSSParseException("Invalid pseudo function name " + unescape, bBA());
                                }
                            }
                    }
                }
            case 56:
                AttributeCondition E = this.eve.mo3960E((String) null, unescape(jj_consume_token(IDENT).image));
                if (aib == null) {
                    return E;
                }
                return this.eve.mo3964a(aib, E);
            default:
                this.jj_la1[78] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    /* renamed from: e */
    public final Condition mo6041e(Condition aib) throws ParseException {
        AttributeCondition eL = this.eve.mo3970eL(jj_consume_token(HASH).image.substring(1));
        return aib == null ? eL : this.eve.mo3964a(aib, eL);
    }

    public final void bBV() throws ParseException {
        jj_consume_token(LBRACE);
        for (; ; ) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    break;
                default:
                    this.jj_la1[79] = this.jj_gen;
                    break;
            }
            jj_consume_token(1);
        }
        /*
        switch (this.evj == -1 ? bCF() : this.evj)
        {
            case 56:
                bBW();
                break;
            default:
                this.evp[80] = this.evo;
        }
        for (;;)
        {
            switch (this.evj == -1 ? bCF() : this.evj)
            {
                case 9:
                    break;
                default:
                    this.evp[81] = this.evo;
                    break;
            }
            m10491oO(9);
            for (;;)
            {
                switch (this.evj == -1 ? bCF() : this.evj)
                {
                    case 1:
                        break;
                    default:
                        this.evp[82] = this.evo;
                        break;
                }
                m10491oO(1);
            }
            switch (this.evj == -1 ? bCF() : this.evj)
            {
                case 56:
                    bBW();
                    break;
                default:
                    this.evp[83] = this.evo;
            }
        }
        m10491oO(6);
        */
    }

    public final void bBW() {
        boolean z = false;
        try {
            String bBQ = bBQ();
            jj_consume_token(COLON);
            while (true) {
                switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                    case 1:
                        jj_consume_token(1);
                    default:
                        this.jj_la1[84] = this.jj_gen;
                        LexicalUnit bBY = expr();
                        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                            case 34:
                                z = bBX();
                                break;
                            default:
                                this.jj_la1[85] = this.jj_gen;
                                break;
                        }
                        this.documentHandler_.mo6341a(bBQ, bBY, z);
                        return;
                }
                // bCc();
            }
        } catch (ParseException e) {
            error_skipdecl();
        }
    }

    public final boolean bBX() throws ParseException {
        jj_consume_token(IMPORTANT_SYM);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(1);
                default:
                    this.jj_la1[86] = this.jj_gen;
                    return true;
            }
        }
    }

    public final LexicalUnit expr() throws ParseException {
        int i;
        LexicalUnit c = term((LexicalUnit) null);
        LexicalUnit ald = c;
        while (true) {
            if (this.jj_ntk == -1) {
                i = jj_ntk_f();
            } else {
                i = this.jj_ntk;
            }
            switch (i) {
                case 7:
                case 12:
                case 13:
                case 14:
                case 19:
                case 20:
                case 23:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 59:
                    switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                        case 7:
                        case 12:
                            ald = mo6004b(ald);
                            break;
                        default:
                            this.jj_la1[88] = this.jj_gen;
                            break;
                    }
                    ald = term(ald);
                default:
                    this.jj_la1[87] = this.jj_gen;
                    return c;
            }
        }
    }

    /* renamed from: c */
    public final LexicalUnit term(LexicalUnit ald) throws ParseException {
        int i;
        LexicalUnit ald2;
        int i2;
        char c = ' ';
        switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
            case PLUS:
            case MINUS:
                c = bBP();
                break;
            default:
                this.jj_la1[89] = this.jj_gen;
                break;
        }
        if (this.jj_ntk == -1) {
            i = jj_ntk_f();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 19:
                ald2 = mo6044f(ald);
                break;
            case 20:
                ald2 = new LexicalUnitImpl(ald, (short) 36, jj_consume_token(20).image);
                break;
            case 23:
                ald2 = new LexicalUnitImpl(ald, (short) 24, jj_consume_token(23).image);
                break;
            case 35:
                ald2 = new LexicalUnitImpl(ald, (short) 12, jj_consume_token(35).image);
                break;
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 52:
            case 53:
            case FUNCTION:
                if (this.jj_ntk == -1) {
                    i2 = jj_ntk_f();
                } else {
                    i2 = this.jj_ntk;
                }
                switch (i2) {
                    case 36:
                        ald2 = LexicalUnitImpl.createEm(ald, mo5994a(c, jj_consume_token(36).image));
                        break;
                    case 37:
                        ald2 = LexicalUnitImpl.createEx(ald, mo5994a(c, jj_consume_token(37).image));
                        break;
                    case 38:
                        ald2 = LexicalUnitImpl.createPixel(ald, mo5994a(c, jj_consume_token(38).image));
                        break;
                    case 39:
                        ald2 = LexicalUnitImpl.createCentimeter(ald, mo5994a(c, jj_consume_token(39).image));
                        break;
                    case 40:
                        ald2 = LexicalUnitImpl.createMillimeter(ald, mo5994a(c, jj_consume_token(40).image));
                        break;
                    case 41:
                        ald2 = LexicalUnitImpl.createInch(ald, mo5994a(c, jj_consume_token(41).image));
                        break;
                    case 42:
                        ald2 = LexicalUnitImpl.createPoint(ald, mo5994a(c, jj_consume_token(42).image));
                        break;
                    case 43:
                        ald2 = LexicalUnitImpl.createPica(ald, mo5994a(c, jj_consume_token(43).image));
                        break;
                    case 44:
                        ald2 = LexicalUnitImpl.createDegree(ald, mo5994a(c, jj_consume_token(44).image));
                        break;
                    case 45:
                        ald2 = LexicalUnitImpl.createRadian(ald, mo5994a(c, jj_consume_token(45).image));
                        break;
                    case 46:
                        ald2 = LexicalUnitImpl.createGradian(ald, mo5994a(c, jj_consume_token(46).image));
                        break;
                    case 47:
                        ald2 = LexicalUnitImpl.createMillisecond(ald, mo5994a(c, jj_consume_token(47).image));
                        break;
                    case 48:
                        ald2 = LexicalUnitImpl.createSecond(ald, mo5994a(c, jj_consume_token(48).image));
                        break;
                    case 49:
                        ald2 = LexicalUnitImpl.createHertz(ald, mo5994a(c, jj_consume_token(49).image));
                        break;
                    case 50:
                        ald2 = LexicalUnitImpl.createKiloHertz(ald, mo5994a(c, jj_consume_token(50).image));
                        break;
                    case 52:
                        ald2 = LexicalUnitImpl.createPercentage(ald, mo5994a(c, jj_consume_token(52).image));
                        break;
                    case 53:
                        ald2 = LexicalUnitImpl.perseValueColor(ald, mo5994a(c, jj_consume_token(53).image));
                        break;
                    case 55:
                        ald2 = function(ald);
                        break;
                    default:
                        this.jj_la1[90] = this.jj_gen;
                        jj_consume_token(-1);
                        throw new ParseException();
                }
            case DIMEN:
                Token oO = jj_consume_token(DIMEN);
                int hm = mo6048hm(oO.image);
                ald2 = LexicalUnitImpl.createDimension(ald, mo5994a(c, oO.image.substring(0, hm + 1)), oO.image.substring(hm + 1));
                break;
            case 54:
                ald2 = rgb(ald);
                break;
            case 56:
                ald2 = new LexicalUnitImpl(ald, (short) 35, jj_consume_token(56).image);
                break;
            case 59:
                ald2 = new LexicalUnitImpl(ald, (short) 39, jj_consume_token(59).image);
                break;
            default:
                this.jj_la1[91] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(S);
                default:
                    this.jj_la1[92] = this.jj_gen;
                    return ald2;
            }
        }
    }

    /* renamed from: d */
    public final LexicalUnit function(LexicalUnit ald) throws ParseException {
        Token oO = jj_consume_token(FUNCTION);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(S);
                default:
                    this.jj_la1[93] = this.jj_gen;
                    LexicalUnit bBY = expr();
                    jj_consume_token(21);
                    if (oO.image.equalsIgnoreCase("counter(")) {
                        return LexicalUnitImpl.createCounter(ald, bBY);
                    }
                    if (oO.image.equalsIgnoreCase("counters(")) {
                        return LexicalUnitImpl.createCounters(ald, bBY);
                    }
                    if (oO.image.equalsIgnoreCase("attr(")) {
                        return LexicalUnitImpl.createAttr(ald, bBY);
                    }
                    if (oO.image.equalsIgnoreCase("rect(")) {
                        return LexicalUnitImpl.createRect(ald, bBY);
                    }
                    return LexicalUnitImpl.createFunction(ald, oO.image.substring(0, oO.image.length() - 1), bBY);
            }
        }
    }

    /* renamed from: e */
    public final LexicalUnit rgb(LexicalUnit ald) throws ParseException {
        jj_consume_token(RGB);
        while (true) {
            switch (this.jj_ntk == -1 ? jj_ntk_f() : this.jj_ntk) {
                case 1:
                    jj_consume_token(S);
                default:
                    this.jj_la1[94] = this.jj_gen;
                    LexicalUnit params = expr();
                    jj_consume_token(RROUND);
                    return LexicalUnitImpl.createRgbColor(ald, params);
            }
        }
    }

    /* renamed from: f */
    public final LexicalUnit mo6044f(LexicalUnit ald) throws ParseException {
        int i;
        int i2;
        int i3;
        Token oO = jj_consume_token(HASH);
        int length = oO.image.length() - 1;
        if (length == 3) {
            int parseInt = Integer.parseInt(oO.image.substring(1, 2), 16);
            int parseInt2 = Integer.parseInt(oO.image.substring(2, 3), 16);
            int parseInt3 = Integer.parseInt(oO.image.substring(3, 4), 16);
            i3 = (parseInt << 4) | parseInt;
            i = parseInt3 | (parseInt3 << 4);
            i2 = (parseInt2 << 4) | parseInt2;
        } else if (length == 6) {
            i3 = Integer.parseInt(oO.image.substring(1, 3), 16);
            int parseInt4 = Integer.parseInt(oO.image.substring(3, 5), 16);
            i = Integer.parseInt(oO.image.substring(5, 7), 16);
            i2 = parseInt4;
        } else {
            i = 0;
            i2 = 0;
            i3 = 0;
        }
        LexicalUnit a = LexicalUnitImpl.perseValueColor((LexicalUnit) null, (float) i3);
        LexicalUnitImpl.perseValueColor(LexicalUnitImpl.createComma(LexicalUnitImpl.perseValueColor(LexicalUnitImpl.createComma(a), (float) i2)), (float) i);
        return LexicalUnitImpl.createRgbColor(ald, a);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public float mo5994a(char c, String str) {
        return ((float) (c == '-' ? -1 : 1)) * Float.parseFloat(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: hm */
    public int mo6048hm(String str) {
        int i = 0;
        while (i < str.length() && !Character.isLetter(str.charAt(i))) {
            i++;
        }
        return i - 1;
    }

    /* access modifiers changed from: package-private */
    public String unescape(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length);
        int i = 0;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt == '\\') {
                i++;
                if (i < length) {
                    char charAt2 = str.charAt(i);
                    switch (charAt2) {
                        case 10:
                        case 12:
                            break;
                        case 13:
                            if (i + 1 < length && str.charAt(i + 1) == 10) {
                                i++;
                                break;
                            }
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                        case 'A':
                        case 'B':
                        case 'C':
                        case 'D':
                        case 'E':
                        case 'F':
                        case 'a':
                        case 'b':
                        case 'c':
                        case 'd':
                        case 'e':
                        case 'f':
                            int digit = Character.digit(charAt2, 16);
                            int i2 = 16;
                            while (true) {
                                if (i + 1 < length) {
                                    char charAt3 = str.charAt(i + 1);
                                    if (Character.digit(charAt3, 16) != -1) {
                                        digit = (digit * 16) + Character.digit(charAt3, 16);
                                        i2 *= 16;
                                        i++;
                                    } else if (charAt3 == ' ') {
                                        i++;
                                    }
                                }
                            }
                            //  stringBuffer.append((char) digit);
                            //  break;
                        default:
                            stringBuffer.append(charAt2);
                            break;
                    }
                } else {
                    throw new CSSParseException("invalid string " + str, bBA());
                }
            } else {
                stringBuffer.append(charAt);
            }
            i++;
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: package-private */
    public void bBZ() {
        Token oQ = getToken(1);
        while (oQ.kind != 7 && oQ.kind != 9 && oQ.kind != 5 && oQ.kind != 0) {
            getNextToken();
            oQ = getToken(1);
        }
    }

    /* access modifiers changed from: package-private */
    public String skip() {
        int nesting = 0;
        StringBuffer sb = new StringBuffer();
        Token tok = getToken(0);
        if (tok.image != null) {
            sb.append(tok.image);
        }
        while (true) {
            Token bCE = getNextToken();
            if (bCE.kind != 0) {
                sb.append(bCE.image);
                if (bCE.kind != LBRACE) {
                    if (bCE.kind != RBRACE) {
                        if (bCE.kind == SEMICOLON && nesting <= 0) {
                            break;
                        }
                    } else {
                        nesting--;
                    }
                } else {
                    nesting++;
                }
                if (bCE.kind == RBRACE && nesting <= 0) {
                    break;
                }
            } else {
                break;
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public void error_skipblock() {
        if (!this.evf) {
            System.err.println("** error_skipblock **\n" + generateParseException().toString());
        }
        int i = 0;
        while (true) {
            Token tok = getNextToken();
            if (tok.kind == LBRACE) {
                i++;
            } else if (tok.kind == RBRACE) {
                i--;
            } else if (tok.kind == 0) {
                return;
            }
            if (tok.kind == RBRACE && i <= 0) {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void error_skipdecl() {
        if (!this.evf) {
            System.err.println("** error_skipdecl **\n" + generateParseException().toString());
        }
        Token oQ = getToken(1);
        while (oQ.kind != SEMICOLON && oQ.kind != RBRACE && oQ.kind != 0) {
            getNextToken();
            oQ = getToken(1);
        }
    }

    /* renamed from: oM */
    private final boolean m10489oM(int i) {
        this.jj_la = i;
        Token zVVar = this.token;
        this.jj_lastpos = zVVar;
        this.jj_scanpos = zVVar;
        boolean z = !bCD();
        jj_save(0, i);
        return z;
    }

    /* renamed from: oN */
    private final boolean m10490oN(int i) {
        boolean z;
        this.jj_la = i;
        Token zVVar = this.token;
        this.jj_lastpos = zVVar;
        this.jj_scanpos = zVVar;
        if (bCd()) {
            z = false;
        } else {
            z = true;
        }
        jj_save(1, i);
        return z;
    }

    private final boolean bCd() {
        if (bCs()) {
            return true;
        }
        if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
            return false;
        }
        if (!bCy()) {
            return (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) ? false : false;
        }
        return true;
    }

    private final boolean bCe() {
        if (jj_scan_token(13)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCf() {
        Token zVVar = this.jj_lastpos;
        if (bCe()) {
            this.jj_lastpos = zVVar;
            if (bCw()) {
                return true;
            }
            if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
            }
            return false;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCg() {
        if (jj_scan_token(11)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCh() {
        if (bCx()) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCi() {
        if (jj_scan_token(1)) {
            return true;
        }
        if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
            return false;
        }
        Token zVVar = this.jj_lastpos;
        if (bCf()) {
            this.jj_lastpos = zVVar;
        } else if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
            return false;
        }
        return false;
    }

    private final boolean bCj() {
        if (bCz()) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCk() {
        if (jj_scan_token(16)) {
            return true;
        }
        if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
            return false;
        }
        while (true) {
            Token zVVar = this.jj_lastpos;
            if (bCB()) {
                this.jj_lastpos = zVVar;
                return false;
            } else if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
                return false;
            }
        }
    }

    private final boolean bCl() {
        if (bCu()) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCm() {
        if (jj_scan_token(56)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCn() {
        Token zVVar = this.jj_lastpos;
        if (bCm()) {
            this.jj_lastpos = zVVar;
            if (bCg()) {
                return true;
            }
            if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
            }
            return false;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCo() {
        if (jj_scan_token(13)) {
            return true;
        }
        if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
            return false;
        }
        while (true) {
            Token zVVar = this.jj_lastpos;
            if (bCA()) {
                this.jj_lastpos = zVVar;
                return false;
            } else if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
                return false;
            }
        }
    }

    private final boolean bCp() {
        if (bCC()) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCq() {
        Token zVVar = this.jj_lastpos;
        if (bCp()) {
            this.jj_lastpos = zVVar;
            if (bCl()) {
                this.jj_lastpos = zVVar;
                if (bCj()) {
                    this.jj_lastpos = zVVar;
                    if (bCh()) {
                        return true;
                    }
                    if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
                    }
                    return false;
                }
                if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
                }
                return false;
            }
            if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
            }
            return false;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCr() {
        if (jj_scan_token(1)) {
            return true;
        }
        return (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) ? false : false;
    }

    private final boolean bCs() {
        Token zVVar = this.jj_lastpos;
        if (bCo()) {
            this.jj_lastpos = zVVar;
            if (bCk()) {
                this.jj_lastpos = zVVar;
                if (bCi()) {
                    return true;
                }
                if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
                }
                return false;
            }
            if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
            }
            return false;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCt() {
        if (bCq()) {
            return true;
        }
        if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
            return false;
        }
        while (true) {
            Token zVVar = this.jj_lastpos;
            if (bCq()) {
                this.jj_lastpos = zVVar;
                return false;
            } else if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
                return false;
            }
        }
    }

    private final boolean bCu() {
        if (jj_scan_token(8)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCv() {
        if (bCn()) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCw() {
        if (jj_scan_token(16)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCx() {
        if (jj_scan_token(10)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCy() {
        Token zVVar = this.jj_lastpos;
        if (bCv()) {
            this.jj_lastpos = zVVar;
            if (bCt()) {
                return true;
            }
            if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
            }
            return false;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCz() {
        if (jj_scan_token(17)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCA() {
        if (jj_scan_token(1)) {
            return true;
        }
        return (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) ? false : false;
    }

    private final boolean bCB() {
        if (jj_scan_token(1)) {
            return true;
        }
        return (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) ? false : false;
    }

    private final boolean bCC() {
        if (jj_scan_token(19)) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_lastpos == this.jj_scanpos) {
        }
        return false;
    }

    private final boolean bCD() {
        if (jj_scan_token(56)) {
            return true;
        }
        if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
            return false;
        }
        while (true) {
            Token zVVar = this.jj_lastpos;
            if (bCr()) {
                this.jj_lastpos = zVVar;
                return false;
            } else if (this.jj_la == 0 && this.jj_lastpos == this.jj_scanpos) {
                return false;
            }
        }
    }

    /* renamed from: a */
    public void ReInit(CharStream hoVar) {
        this.token_source.ReInit(hoVar);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 95; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    /* renamed from: a */
    public void SACParserCSS2(SACParserCSS2TokenManager abv) {
        this.token_source = abv;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 95; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    /* renamed from: oO */
    private final Token jj_consume_token(int i) throws ParseException {
        Token zVVar = this.token;
        if (zVVar.next != null) {
            this.token = this.token.next;
        } else {
            Token zVVar2 = this.token;
            Token bCE = this.token_source.getNextToken();
            zVVar2.next = bCE;
            this.token = bCE;
        }
        this.jj_ntk = -1;
        if (this.token.kind == i) {
            this.jj_gen++;
            int i2 = this.evv + 1;
            this.evv = i2;
            if (i2 > 100) {
                this.evv = 0;
                for (JJCalls aVar : this.jj_2_rtns) {
                    while (aVar != null) {
                        if (aVar.gen < this.jj_gen) {
                            aVar.first = null;
                        }
                        aVar = aVar.next;
                    }
                }
            }
            return this.token;
        }
        this.token = zVVar;
        this.evy = i;
        throw generateParseException();
    }

    /* renamed from: oP */
    private final boolean jj_scan_token(int i) {
        if (this.jj_lastpos == this.jj_scanpos) {
            this.jj_la--;
            if (this.jj_lastpos.next == null) {
                Token zVVar = this.jj_lastpos;
                Token bCE = this.token_source.getNextToken();
                zVVar.next = bCE;
                this.jj_lastpos = bCE;
                this.jj_scanpos = bCE;
            } else {
                Token zVVar2 = this.jj_lastpos.next;
                this.jj_lastpos = zVVar2;
                this.jj_scanpos = zVVar2;
            }
        } else {
            this.jj_lastpos = this.jj_lastpos.next;
        }
        if (this.jj_rescan) {
            Token zVVar3 = this.token;
            int i2 = 0;
            while (zVVar3 != null && zVVar3 != this.jj_lastpos) {
                i2++;
                zVVar3 = zVVar3.next;
            }
            if (zVVar3 != null) {
                jj_add_error_token(i, i2);
            }
        }
        if (this.jj_lastpos.kind != i) {
            return true;
        }
        return false;
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token zVVar = this.token;
            Token bCE = this.token_source.getNextToken();
            zVVar.next = bCE;
            this.token = bCE;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    /* renamed from: oQ */
    public final Token getToken(int i) {
        Token bCE;
        int i2 = 0;
        Token zVVar = this.lookingAhead ? this.jj_lastpos : this.token;
        while (i2 < i) {
            if (zVVar.next != null) {
                bCE = zVVar.next;
            } else {
                bCE = this.token_source.getNextToken();
                zVVar.next = bCE;
            }
            i2++;
            zVVar = bCE;
        }
        return zVVar;
    }

    private final int jj_ntk_f() {
        Token zVVar = this.token.next;
        this.jj_nt = zVVar;
        if (zVVar == null) {
            Token zVVar2 = this.token;
            Token bCE = this.token_source.getNextToken();
            zVVar2.next = bCE;
            int i = bCE.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    /* renamed from: U */
    private void jj_add_error_token(int i, int i2) {
        boolean z;
        if (i2 < 100) {
            if (i2 == this.evA + 1) {
                int[] iArr = this.evz;
                int i3 = this.evA;
                this.evA = i3 + 1;
                iArr[i3] = i;
            } else if (this.evA != 0) {
                this.evx = new int[this.evA];
                for (int i4 = 0; i4 < this.evA; i4++) {
                    this.evx[i4] = this.evz[i4];
                }
                Enumeration elements = this.evw.elements();
                boolean z2 = false;
                while (true) {
                    if (!elements.hasMoreElements()) {
                        z = z2;
                        break;
                    }
                    int[] iArr2 = (int[]) elements.nextElement();
                    if (iArr2.length == this.evx.length) {
                        int i5 = 0;
                        while (true) {
                            if (i5 >= this.evx.length) {
                                z = true;
                                break;
                            } else if (iArr2[i5] != this.evx[i5]) {
                                z = false;
                                break;
                            } else {
                                i5++;
                            }
                        }
                        if (z) {
                            break;
                        }
                        z2 = z;
                    }
                }
                if (!z) {
                    this.evw.addElement(this.evx);
                }
                if (i2 != 0) {
                    int[] iArr3 = this.evz;
                    this.evA = i2;
                    iArr3[i2 - 1] = i;
                }
            }
        }
    }

    public final ParseException generateParseException() {
        int i = 0;
        this.evw.removeAllElements();
        boolean[] zArr = new boolean[78];
        for (int i2 = 0; i2 < 78; i2++) {
            zArr[i2] = false;
        }
        if (this.evy >= 0) {
            zArr[this.evy] = true;
            this.evy = -1;
        }
        for (int i3 = 0; i3 < 95; i3++) {
            if (this.jj_la1[i3] == this.jj_gen) {
                for (int i4 = 0; i4 < 32; i4++) {
                    if ((this.evq[i3] & (1 << i4)) != 0) {
                        zArr[i4] = true;
                    }
                    if ((this.evr[i3] & (1 << i4)) != 0) {
                        zArr[i4 + 32] = true;
                    }
                    if ((this.evs[i3] & (1 << i4)) != 0) {
                        zArr[i4 + 64] = true;
                    }
                }
            }
        }
        for (int i5 = 0; i5 < 78; i5++) {
            if (zArr[i5]) {
                this.evx = new int[1];
                this.evx[0] = i5;
                this.evw.addElement(this.evx);
            }
        }
        this.evA = 0;
        jj_rescan_token();
        jj_add_error_token(0, 0);
        int[][] iArr = new int[this.evw.size()][];
        while (true) {
            int i6 = i;
            if (i6 >= this.evw.size()) {
                return new ParseException(this.token, iArr, tokenImage);
            }
            iArr[i6] = (int[]) this.evw.elementAt(i6);
            i = i6 + 1;
        }
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }

    private final void jj_rescan_token() {
        this.jj_rescan = true;
        for (int i = 0; i < 2; i++) {
            JJCalls aVar = this.jj_2_rtns[i];
            do {
                if (aVar.gen > this.jj_gen) {
                    this.jj_la = aVar.arg;
                    Token zVVar = aVar.first;
                    this.jj_lastpos = zVVar;
                    this.jj_scanpos = zVVar;
                    switch (i) {
                        case 0:
                            bCD();
                            break;
                        case 1:
                            bCd();
                            break;
                    }
                }
                aVar = aVar.next;
            } while (aVar != null);
        }
        this.jj_rescan = false;
    }

    /* renamed from: V */
    private final void jj_save(int index, int xla) {
        JJCalls aVar = this.jj_2_rtns[index];
        while (true) {
            if (aVar.gen <= this.jj_gen) {
                break;
            } else if (aVar.next == null) {
                JJCalls aVar2 = new JJCalls();
                aVar.next = aVar2;
                aVar = aVar2;
                break;
            } else {
                aVar = aVar.next;
            }
        }
        aVar.gen = (this.jj_gen + xla) - this.jj_la;
        aVar.first = this.token;
        aVar.arg = xla;
    }

    /* renamed from: a.VK$a */
    static final class JJCalls {
        int gen;
        Token first;
        int arg;
        JJCalls next;

        JJCalls() {
        }
    }
}
