package com.steadystate.css.parser;

import org.w3c.css.sac.LexicalUnit;

import java.io.Serializable;

/* renamed from: a.et */
/* compiled from: a */
public class LexicalUnitImpl implements LexicalUnit, Serializable {

    /* renamed from: CD */
    private short lexicalUnitType_;

    /* renamed from: CE */
    private LexicalUnit nextLexicalUnit_;

    /* renamed from: CF */
    private LexicalUnit previousLexicalUnit_;

    /* renamed from: CG */
    private float floatValue_;

    /* renamed from: CH */
    private String dimension_;

    /* renamed from: CI */
    private String functionName_;

    /* renamed from: CJ */
    private LexicalUnit subValues;

    /* renamed from: CK */
    private String stringValue_;

    public LexicalUnitImpl(LexicalUnit ald, short s) {
        this.lexicalUnitType_ = s;
        this.previousLexicalUnit_ = ald;
        if (this.previousLexicalUnit_ != null) {
            ((LexicalUnitImpl) this.previousLexicalUnit_).nextLexicalUnit_ = this;
        }
    }

    public LexicalUnitImpl(LexicalUnit ald, int i) {
       // this(ald, 13);
        this.floatValue_ = (float) i;
    }

    public LexicalUnitImpl(LexicalUnit ald, short s, float f) {
        this(ald, s);
        this.floatValue_ = f;
    }

    public LexicalUnitImpl(LexicalUnit ald, short s, String str, float f) {
        this(ald, s);
        this.dimension_ = str != null ? str.intern() : null;
        this.floatValue_ = f;
    }

    public LexicalUnitImpl(LexicalUnit ald, short s, String str) {
        this(ald, s);
        this.stringValue_ = str != null ? str.intern() : null;
    }

    public LexicalUnitImpl(LexicalUnit ald, short s, String str, LexicalUnit ald2) {
        this(ald, s);
        this.functionName_ = str != null ? str.intern() : null;
        this.subValues = ald2;
    }

    /* renamed from: a */
    private static float m30088a(char c, String str) {
        return ((float) (c == '-' ? -1 : 1)) * Float.valueOf(str).floatValue();
    }

    /* renamed from: a */
    public static LexicalUnit perseValueColor(LexicalUnit ald, float f) {
        if (f > ((float) ((int) f))) {
            return new LexicalUnitImpl(ald, (short) 14, f);
        }
        return new LexicalUnitImpl(ald, (int) f);
    }

    /* renamed from: b */
    public static LexicalUnit createPercentage(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 23, f);
    }

    /* renamed from: c */
    public static LexicalUnit createPixel(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 17, f);
    }

    /* renamed from: d */
    public static LexicalUnit createCentimeter(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 19, f);
    }

    /* renamed from: e */
    public static LexicalUnit createMillimeter(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 20, f);
    }

    /* renamed from: f */
    public static LexicalUnit createInch(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 18, f);
    }

    /* renamed from: g */
    public static LexicalUnit createPoint(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 21, f);
    }

    /* renamed from: h */
    public static LexicalUnit createPica(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 22, f);
    }

    /* renamed from: i */
    public static LexicalUnit createEm(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 15, f);
    }

    /* renamed from: j */
    public static LexicalUnit createEx(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 16, f);
    }

    /* renamed from: k */
    public static LexicalUnit createDegree(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 28, f);
    }

    /* renamed from: l */
    public static LexicalUnit createRadian(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 30, f);
    }

    /* renamed from: m */
    public static LexicalUnit createGradian(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 29, f);
    }

    /* renamed from: n */
    public static LexicalUnit createMillisecond(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 31, f);
    }

    /* renamed from: o */
    public static LexicalUnit createSecond(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 32, f);
    }

    /* renamed from: p */
    public static LexicalUnit createHertz(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 33, f);
    }

    /* renamed from: a */
    public static LexicalUnit createDimension(LexicalUnit ald, float f, String str) {
        return new LexicalUnitImpl(ald, (short) 42, str, f);
    }

    /* renamed from: q */
    public static LexicalUnit createKiloHertz(LexicalUnit ald, float f) {
        return new LexicalUnitImpl(ald, (short) 34, f);
    }

    /* renamed from: a */
    public static LexicalUnit createCounter(LexicalUnit ald, LexicalUnit ald2) {
        return new LexicalUnitImpl(ald, (short) 25, "counter", ald2);
    }

    /* renamed from: b */
    public static LexicalUnit createCounters(LexicalUnit ald, LexicalUnit ald2) {
        return new LexicalUnitImpl(ald, (short) 26, "counters", ald2);
    }

    /* renamed from: c */
    public static LexicalUnit createAttr(LexicalUnit ald, LexicalUnit ald2) {
        return new LexicalUnitImpl(ald, (short) 37, "attr", ald2);
    }

    /* renamed from: d */
    public static LexicalUnit createRect(LexicalUnit ald, LexicalUnit ald2) {
        return new LexicalUnitImpl(ald, (short) 38, "rect", ald2);
    }

    /* renamed from: e */
    public static LexicalUnit createRgbColor(LexicalUnit ald, LexicalUnit ald2) {
        return new LexicalUnitImpl(ald, (short) 27, "rgb", ald2);
    }

    /* renamed from: a */
    public static LexicalUnit createFunction(LexicalUnit ald, String str, LexicalUnit ald2) {
        return new LexicalUnitImpl(ald, (short) 41, str, ald2);
    }

    /* renamed from: a */
    public static LexicalUnit m30093a(LexicalUnit ald, String str) {
        return new LexicalUnitImpl(ald, (short) 36, str);
    }

    /* renamed from: b */
    public static LexicalUnit m30098b(LexicalUnit ald, String str) {
        return new LexicalUnitImpl(ald, (short) 35, str);
    }

    /* renamed from: c */
    public static LexicalUnit m30101c(LexicalUnit ald, String str) {
        return new LexicalUnitImpl(ald, (short) 24, str);
    }

    /* renamed from: a */
    public static LexicalUnit createComma(LexicalUnit ald) {
        return new LexicalUnitImpl(ald, 0);
    }

    /* renamed from: lF */
    public short getLexicalUnitType() {
        return this.lexicalUnitType_;
    }

    /* renamed from: lG */
    public LexicalUnit getNextLexicalUnit() {
        return this.nextLexicalUnit_;
    }

    /* renamed from: lH */
    public LexicalUnit getPreviousLexicalUnit() {
        return this.previousLexicalUnit_;
    }

    /* renamed from: lI */
    public int getIntegerValue() {
        return (int) this.floatValue_;
    }

    public float getFloatValue() {
        return this.floatValue_;
    }

    /* renamed from: lJ */
    public String getDimensionUnitText() {
        switch (this.lexicalUnitType_) {
            case 15:
                return "em";
            case 16:
                return "ex";
            case 17:
                return "px";
            case 18:
                return "in";
            case 19:
                return "cm";
            case 20:
                return "mm";
            case 21:
                return "pt";
            case 22:
                return "pc";
            case 23:
                return "%";
            case 28:
                return "deg";
            case 29:
                return "grad";
            case 30:
                return "rad";
            case 31:
                return "ms";
            case 32:
                return "s";
            case 33:
                return "Hz";
            case 34:
                return "kHz";
            case 42:
                return this.dimension_;
            default:
                return "";
        }
    }

    public String getFunctionName() {
        return this.functionName_;
    }

    /* renamed from: lK */
    public LexicalUnit getParameters() {
        return this.subValues;
    }

    public String getStringValue() {
        return this.stringValue_;
    }

    /* renamed from: lL */
    public LexicalUnit getSubValues() {
        return this.subValues;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        switch (this.lexicalUnitType_) {
            case 0:
                stringBuffer.append(",");
                break;
            case 1:
                stringBuffer.append("+");
                break;
            case 2:
                stringBuffer.append("-");
                break;
            case 3:
                stringBuffer.append("*");
                break;
            case 4:
                stringBuffer.append("/");
                break;
            case 5:
                stringBuffer.append("%");
                break;
            case 6:
                stringBuffer.append("^");
                break;
            case 7:
                stringBuffer.append("<");
                break;
            case 8:
                stringBuffer.append(">");
                break;
            case 9:
                stringBuffer.append("<=");
                break;
            case 10:
                stringBuffer.append(">=");
                break;
            case 11:
                stringBuffer.append("~");
                break;
            case 12:
                stringBuffer.append("inherit");
                break;
            case 13:
                stringBuffer.append(String.valueOf(getIntegerValue()));
                break;
            case 14:
                stringBuffer.append(m30087O(getFloatValue()));
                break;
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 42:
                stringBuffer.append(m30087O(getFloatValue())).append(getDimensionUnitText());
                break;
            case 24:
                stringBuffer.append("url(").append(getStringValue()).append(")");
                break;
            case 25:
                stringBuffer.append("counter(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append(")");
                break;
            case 26:
                stringBuffer.append("counters(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append(")");
                break;
            case 27:
                stringBuffer.append("rgb(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append(")");
                break;
            case 35:
                stringBuffer.append(getStringValue());
                break;
            case 36:
                stringBuffer.append("\"").append(getStringValue()).append("\"");
                break;
            case 37:
                stringBuffer.append("attr(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append(")");
                break;
            case 38:
                stringBuffer.append("rect(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append(")");
                break;
            case 39:
                stringBuffer.append(getStringValue());
                break;
            case 40:
                stringBuffer.append(getStringValue());
                break;
            case 41:
                stringBuffer.append(getFunctionName());
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append(")");
                break;
        }
        return stringBuffer.toString();
    }

    public String toDebugString() {
        StringBuffer stringBuffer = new StringBuffer();
        switch (this.lexicalUnitType_) {
            case 0:
                stringBuffer.append("SAC_OPERATOR_COMMA");
                break;
            case 1:
                stringBuffer.append("SAC_OPERATOR_PLUS");
                break;
            case 2:
                stringBuffer.append("SAC_OPERATOR_MINUS");
                break;
            case 3:
                stringBuffer.append("SAC_OPERATOR_MULTIPLY");
                break;
            case 4:
                stringBuffer.append("SAC_OPERATOR_SLASH");
                break;
            case 5:
                stringBuffer.append("SAC_OPERATOR_MOD");
                break;
            case 6:
                stringBuffer.append("SAC_OPERATOR_EXP");
                break;
            case 7:
                stringBuffer.append("SAC_OPERATOR_LT");
                break;
            case 8:
                stringBuffer.append("SAC_OPERATOR_GT");
                break;
            case 9:
                stringBuffer.append("SAC_OPERATOR_LE");
                break;
            case 10:
                stringBuffer.append("SAC_OPERATOR_GE");
                break;
            case 11:
                stringBuffer.append("SAC_OPERATOR_TILDE");
                break;
            case 12:
                stringBuffer.append("SAC_INHERIT");
                break;
            case 13:
                stringBuffer.append("SAC_INTEGER(").append(String.valueOf(getIntegerValue())).append(")");
                break;
            case 14:
                stringBuffer.append("SAC_REAL(").append(m30087O(getFloatValue())).append(")");
                break;
            case 15:
                stringBuffer.append("SAC_EM(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 16:
                stringBuffer.append("SAC_EX(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 17:
                stringBuffer.append("SAC_PIXEL(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 18:
                stringBuffer.append("SAC_INCH(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 19:
                stringBuffer.append("SAC_CENTIMETER(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 20:
                stringBuffer.append("SAC_MILLIMETER(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 21:
                stringBuffer.append("SAC_POINT(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 22:
                stringBuffer.append("SAC_PICA(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 23:
                stringBuffer.append("SAC_PERCENTAGE(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 24:
                stringBuffer.append("SAC_URI(url(").append(getStringValue()).append("))");
                break;
            case 25:
                stringBuffer.append("SAC_COUNTER_FUNCTION(counter(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append("))");
                break;
            case 26:
                stringBuffer.append("SAC_COUNTERS_FUNCTION(counters(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append("))");
                break;
            case 27:
                stringBuffer.append("SAC_RGBCOLOR(rgb(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append("))");
                break;
            case 28:
                stringBuffer.append("SAC_DEGREE(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 29:
                stringBuffer.append("SAC_GRADIAN(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 30:
                stringBuffer.append("SAC_RADIAN(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 31:
                stringBuffer.append("SAC_MILLISECOND(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 32:
                stringBuffer.append("SAC_SECOND(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 33:
                stringBuffer.append("SAC_HERTZ(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 34:
                stringBuffer.append("SAC_KILOHERTZ(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
            case 35:
                stringBuffer.append("SAC_IDENT(").append(getStringValue()).append(")");
                break;
            case 36:
                stringBuffer.append("SAC_STRING_VALUE(\"").append(getStringValue()).append("\")");
                break;
            case 37:
                stringBuffer.append("SAC_ATTR(attr(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append("))");
                break;
            case 38:
                stringBuffer.append("SAC_RECT_FUNCTION(rect(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append("))");
                break;
            case 39:
                stringBuffer.append("SAC_UNICODERANGE(").append(getStringValue()).append(")");
                break;
            case 40:
                stringBuffer.append("SAC_SUB_EXPRESSION(").append(getStringValue()).append(")");
                break;
            case 41:
                stringBuffer.append("SAC_FUNCTION(").append(getFunctionName()).append("(");
                m30095a(stringBuffer, this.subValues);
                stringBuffer.append("))");
                break;
            case 42:
                stringBuffer.append("SAC_DIMENSION(").append(m30087O(getFloatValue())).append(getDimensionUnitText()).append(")");
                break;
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    private void m30095a(StringBuffer stringBuffer, LexicalUnit ald) {
        while (ald != null) {
            stringBuffer.append(ald.toString());
            ald = ald.getNextLexicalUnit();
        }
    }

    /* renamed from: O */
    private String m30087O(float f) {
        String valueOf = String.valueOf(getFloatValue());
        return f - ((float) ((int) f)) != 0.0f ? valueOf : valueOf.substring(0, valueOf.length() - 2);
    }
}
