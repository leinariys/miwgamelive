package com.steadystate.css.sac;

import logic.res.css.SACMediaList;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.css.sac.SelectorList;

/* renamed from: a.tv */
/* compiled from: a */
public interface DocumentHandler {
    /* renamed from: Q */
    void ignorableAtRule(String str);

    /* renamed from: a */
    void startDocument(InputSource fh);

    /* renamed from: a */
    void mo6338a(SelectorList ahq);

    /* renamed from: a */
    void startMedia(SACMediaList atf);

    /* renamed from: a */
    void ignorableAtRule(String str, SACMediaList atf, String str2);

    /* renamed from: a */
    void mo6341a(String str, LexicalUnit ald, boolean z);

    /* renamed from: b */
    void endDocument(InputSource fh);

    /* renamed from: b */
    void endSelector(SelectorList ahq);

    /* renamed from: b */
    void endMedia(SACMediaList atf);

    void comment(String str);

    /* renamed from: d */
    void mo6346d(String str, String str2);

    /* renamed from: e */
    void mo6347e(String str, String str2);

    /* renamed from: f */
    void endPage(String str, String str2);

    /* renamed from: jv */
    void mo6350jv();

    /* renamed from: jw */
    void endFontFace();
}
