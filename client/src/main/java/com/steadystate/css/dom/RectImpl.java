package com.steadystate.css.dom;

import org.w3c.css.sac.LexicalUnit;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.Rect;

import java.io.Serializable;

/* renamed from: a.aTP */
/* compiled from: a */
public class RectImpl implements Serializable, Rect {
    private CSSPrimitiveValue top_;
    private CSSPrimitiveValue right_;
    private CSSPrimitiveValue bottom_;
    private CSSPrimitiveValue left_;

    public RectImpl(LexicalUnit ald) {
        this.top_ = new CSSValueImpl(ald, true);
        LexicalUnit lG = ald.getNextLexicalUnit().getNextLexicalUnit();
        this.right_ = new CSSValueImpl(lG, true);
        LexicalUnit lG2 = lG.getNextLexicalUnit().getNextLexicalUnit();
        this.bottom_ = new CSSValueImpl(lG2, true);
        this.left_ = new CSSValueImpl(lG2.getNextLexicalUnit().getNextLexicalUnit(), true);
    }

    public CSSPrimitiveValue getTop() {
        return this.right_;
    }

    public CSSPrimitiveValue getRight() {
        return this.bottom_;
    }

    public CSSPrimitiveValue getBottom() {
        return this.left_;
    }

    public CSSPrimitiveValue getLeft() {
        return this.top_;
    }

    public String toString() {
        return new StringBuffer().append("rect(")
            .append(this.top_.toString()).append(", ")
            .append(this.right_.toString()).append(", ")
            .append(this.bottom_.toString()).append(", ")
            .append(this.left_.toString()).append(")")
            .toString();
    }
}
