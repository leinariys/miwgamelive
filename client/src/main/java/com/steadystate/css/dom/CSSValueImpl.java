package com.steadystate.css.dom;

import com.steadystate.css.parser.CSSOMParser;
import com.steadystate.css.parser.LexicalUnitImpl;
import logic.res.css.C3331qN;
import logic.res.css.C6336akw;
import logic.res.css.ColorCss;
import org.w3c.css.sac.InputSource;
import org.w3c.css.sac.LexicalUnit;
import org.w3c.dom.css.*;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Vector;

/* renamed from: a.Bj */
/* compiled from: a */
public class CSSValueImpl implements Serializable, CSSPrimitiveValue, CSSValueList {
    private Object _value;

    public CSSValueImpl(RGBColor rGBColor) {
        this._value = null;
        this._value = rGBColor;
    }

    public CSSValueImpl(LexicalUnit ald, boolean z) {
        this._value = null;
        if (ald.getParameters() != null) {
            if (ald.getLexicalUnitType() == 38) {
                this._value = new RectImpl(ald.getParameters());
            } else if (ald.getLexicalUnitType() == 27) {
                this._value = new ColorCss(ald.getParameters());
            } else if (ald.getLexicalUnitType() == 25) {
                this._value = new C6336akw(false, ald.getParameters());
            } else if (ald.getLexicalUnitType() == 26) {
                this._value = new C6336akw(true, ald.getParameters());
            } else {
                this._value = ald;
            }
        } else if (z || ald.getNextLexicalUnit() == null) {
            this._value = ald;
        } else {
            Vector vector = new Vector();
            while (ald != null) {
                if (!(ald.getLexicalUnitType() == 0 || ald.getLexicalUnitType() == 4)) {
                    vector.addElement(new CSSValueImpl(ald, true));
                }
                ald = ald.getNextLexicalUnit();
            }
            this._value = vector;
        }
    }

    public CSSValueImpl(LexicalUnit ald) {
        this(ald, false);
    }

    public String getCssText() {
        if (getCssValueType() != 2) {
            return this._value.toString();
        }
        StringBuffer stringBuffer = new StringBuffer();
        LexicalUnit ald = (LexicalUnit) ((CSSValueImpl) ((Vector) this._value).elementAt(0))._value;
        while (ald != null) {
            stringBuffer.append(ald.toString());
            LexicalUnit lG = ald.getNextLexicalUnit();
            if (!(lG == null || lG.getLexicalUnitType() == 0 || lG.getLexicalUnitType() == 4 || ald.getLexicalUnitType() == 4)) {
                stringBuffer.append(" ");
            }
            ald = lG;
        }
        return stringBuffer.toString();
    }

    public void setCssText(String str) {
        try {
            this._value = ((CSSValueImpl) new CSSOMParser().parsePropertyValue(new InputSource((Reader) new StringReader(str))))._value;
        } catch (Exception e) {
            throw new C3331qN((short) 12, 0, e.getMessage());
        }
    }

    public short getCssValueType() {
        return this._value instanceof Vector ? (short) 2 : 1;
    }

    public short getPrimitiveType() {
        if (this._value instanceof LexicalUnit) {
            switch (((LexicalUnit) this._value).getLexicalUnitType()) {
                case 12:
                    return 21;
                case 13:
                case 14:
                    return 1;
                case 15:
                    return 3;
                case 16:
                    return 4;
                case 17:
                    return 5;
                case 18:
                    return 8;
                case 19:
                    return 6;
                case 20:
                    return 7;
                case 21:
                    return 9;
                case 22:
                    return 10;
                case 23:
                    return 2;
                case 24:
                    return 20;
                case 28:
                    return 11;
                case 29:
                    return 13;
                case 30:
                    return 12;
                case 31:
                    return 14;
                case 32:
                    return 15;
                case 33:
                    return 17;
                case 34:
                    return 16;
                case 35:
                    return 21;
                case 36:
                    return 19;
                case 37:
                    return 22;
                case 39:
                case 40:
                case 41:
                    return 19;
                case 42:
                    return 18;
            }
        } else if (this._value instanceof RectImpl) {
            return 24;
        } else {
            if (this._value instanceof ColorCss) {
                return 25;
            }
            if (this._value instanceof C6336akw) {
                return 23;
            }
        }
        return 0;
    }

    public void setFloatValue(short s, float f) {
        this._value = LexicalUnitImpl.perseValueColor((LexicalUnit) null, f);
    }

    public float getFloatValue(short s) {
        if (this._value instanceof LexicalUnit) {
            return ((LexicalUnit) this._value).getFloatValue();
        }
        throw new C3331qN(15, 10);
    }

    public void setStringValue(short s, String str) {
        switch (s) {
            case 19:
                this._value = LexicalUnitImpl.m30093a((LexicalUnit) null, str);
                return;
            case 20:
                this._value = LexicalUnitImpl.m30101c((LexicalUnit) null, str);
                return;
            case 21:
                this._value = LexicalUnitImpl.m30098b((LexicalUnit) null, str);
                return;
            case 22:
                throw new C3331qN(9, 19);
            default:
                throw new C3331qN(15, 11);
        }
    }

    public String getStringValue() {
        if (this._value instanceof LexicalUnit) {
            LexicalUnit ald = (LexicalUnit) this._value;
            if (ald.getLexicalUnitType() == 35 || ald.getLexicalUnitType() == 36 || ald.getLexicalUnitType() == 24 || ald.getLexicalUnitType() == 37) {
                return ald.getStringValue();
            }
        } else if (this._value instanceof Vector) {
            return null;
        }
        throw new C3331qN(15, 11);
    }

    public Counter getCounterValue() {
        if (this._value instanceof Counter) {
            return (Counter) this._value;
        }
        throw new C3331qN(15, 12);
    }

    public Rect getRectValue() {
        if (this._value instanceof Rect) {
            return (Rect) this._value;
        }
        throw new C3331qN(15, 13);
    }

    public RGBColor getRGBColorValue() {
        if (this._value instanceof RGBColor) {
            return (RGBColor) this._value;
        }
        throw new C3331qN(15, 14);
    }

    public int getLength() {
        if (this._value instanceof Vector) {
            return ((Vector) this._value).size();
        }
        return 0;
    }

    public CSSValue item(int i) {
        if (this._value instanceof Vector) {
            return (CSSValue) ((Vector) this._value).elementAt(i);
        }
        return null;
    }

    public String toString() {
        return getCssText();
    }
}
