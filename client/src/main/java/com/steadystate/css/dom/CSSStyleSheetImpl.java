package com.steadystate.css.dom;

import com.steadystate.css.parser.CSSOMParser;
import logic.res.css.C3331qN;
import logic.res.css.CSSException;
import logic.res.css.CSSRuleListImpl;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.Node;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSRuleList;
import org.w3c.dom.css.CSSStyleSheet;
import org.w3c.dom.stylesheets.MediaList;
import org.w3c.dom.stylesheets.StyleSheet;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/* renamed from: a.SS */
/* compiled from: a */
public class CSSStyleSheetImpl implements Serializable, CSSStyleSheet {
    private boolean _readOnly = false;
    private String _title = null;
    private StyleSheet egA = null;
    private CSSRule egB = null;
    private CSSRuleListImpl egC = null;
    private boolean egy = false;
    private Node egz = null;

    /* renamed from: qs */
    private String f1542qs = null;

    /* renamed from: qt */
    private MediaList f1543qt = null;

    public String getType() {
        return "text/css";
    }

    public boolean getDisabled() {
        return this.egy;
    }

    public void setDisabled(boolean z) {
        this.egy = z;
    }

    public Node getOwnerNode() {
        return this.egz;
    }

    public StyleSheet getParentStyleSheet() {
        return this.egA;
    }

    public String getHref() {
        return this.f1542qs;
    }

    public void setHref(String str) {
        this.f1542qs = str;
    }

    public String getTitle() {
        return this._title;
    }

    public void setTitle(String str) {
        this._title = str;
    }

    public MediaList getMedia() {
        return this.f1543qt;
    }

    public void setMedia(String str) {
    }

    public CSSRule getOwnerRule() {
        return this.egB;
    }

    public CSSRuleList getCssRules() {
        return this.egC;
    }

    public int insertRule(String str, int i) {
        int i2;
        if (this._readOnly) {
            throw new C3331qN(7, 2);
        }
        try {
            InputSource fh = new InputSource((Reader) new StringReader(str));
            CSSOMParser bAk = CSSOMParser.getInstance();
            bAk.setParentStyleSheet(this);
            CSSRule f = bAk.parseRule(fh);
            if (getCssRules().getLength() > 0) {
                if (f.getType() != 2) {
                    if (f.getType() == 3 && i <= getCssRules().getLength()) {
                        int i3 = 0;
                        while (true) {
                            if (i3 >= i) {
                                break;
                            }
                            short type = getCssRules().item(i3).getType();
                            if (type == 2 && type == 3) {
                                i3++;
                            } else {
                                i2 = 17;
                            }
                        }
                        i2 = 17;
                    }
                    i2 = -1;
                } else if (i != 0) {
                    i2 = 15;
                } else {
                    if (getCssRules().item(0).getType() == 2) {
                        i2 = 16;
                    }
                    i2 = -1;
                }
                if (i2 > -1) {
                    throw new C3331qN(3, i2);
                }
            }
            ((CSSRuleListImpl) getCssRules()).mo5162a(f, i);
            return i;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new C3331qN((short) 1, 1, e.getMessage());
        } catch (CSSException e2) {
            throw new C3331qN((short) 12, 0, e2.getMessage());
        }
    }

    public void deleteRule(int i) {
        if (this._readOnly) {
            throw new C3331qN(7, 2);
        }
        try {
            ((CSSRuleListImpl) getCssRules()).delete(i);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new C3331qN((short) 1, 1, e.getMessage());
        }
    }

    public boolean isReadOnly() {
        return this._readOnly;
    }

    public void setReadOnly(boolean z) {
        this._readOnly = z;
    }

    /* renamed from: a */
    public void mo5396a(Node node) {
        this.egz = node;
    }

    /* renamed from: a */
    public void mo5398a(StyleSheet styleSheet) {
        this.egA = styleSheet;
    }

    /* renamed from: a */
    public void mo5397a(CSSRule cSSRule) {
        this.egB = cSSRule;
    }

    /* renamed from: a */
    public void mo5395a(CSSRuleListImpl rk) {
        this.egC = rk;
    }

    public String toString() {
        return this.egC.toString();
    }
}
