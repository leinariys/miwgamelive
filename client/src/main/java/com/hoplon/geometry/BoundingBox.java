package com.hoplon.geometry;

import game.geometry.*;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;

import javax.vecmath.Tuple3f;
import java.io.Serializable;

/* compiled from: a */
public class BoundingBox implements IExternalIO, Serializable {

    private Vec3f maxes;
    private Vec3f mins;

    public BoundingBox() {
        this.maxes = new Vec3f(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
        this.mins = new Vec3f(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
    }

    public BoundingBox(Vec3f vec3f, Vec3f vec3f2) {
        this.maxes = vec3f2;
        this.mins = vec3f;
    }

    public BoundingBox(float f, float f2, float f3, float f4, float f5, float f6) {
        this(new Vec3f(f, f2, f3), new Vec3f(f4, f5, f6));
    }

    public BoundingBox(float[] fArr) {
        this.mins = new Vec3f(fArr[0], fArr[1], fArr[2]);
        this.maxes = new Vec3f(fArr[3], fArr[4], fArr[5]);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        BoundingBox boundingBox = (BoundingBox) obj;
        if (!this.maxes.equals(boundingBox.maxes) || !this.mins.equals(boundingBox.mins)) {
            return false;
        }
        return true;
    }

    /* renamed from: c */
    public void mo23432c(BoundingBox boundingBox) {
        this.mins.set(boundingBox.dqP());
        this.maxes.set(boundingBox.dqQ());
    }

    /* renamed from: g */
    public void mo23448g(aLH alh) {
        this.mins.set(alh.dim());
        this.maxes.set(alh.din());
    }

    /* renamed from: bP */
    public void mo23427bP(Vec3f vec3f) {
        this.mins = vec3f;
    }

    public Vec3f dqP() {
        return this.mins;
    }

    /* renamed from: bQ */
    public void mo23428bQ(Vec3f vec3f) {
        this.maxes = vec3f;
    }

    public Vec3f dqQ() {
        return this.maxes;
    }

    public float dio() {
        float f = this.maxes.x - this.mins.x;
        if (((double) f) > 1.0E26d) {
            return 0.0f;
        }
        return Math.max(0.0f, f);
    }

    public float dip() {
        float f = this.maxes.y - this.mins.y;
        if (((double) f) > 1.0E26d) {
            return 0.0f;
        }
        return Math.max(0.0f, f);
    }

    public float diq() {
        float f = this.maxes.z - this.mins.z;
        if (((double) f) > 1.0E26d) {
            return 0.0f;
        }
        return Math.max(0.0f, f);
    }

    public float dir() {
        float dio = dio();
        float dip = dip();
        return (float) Math.sqrt((double) ((dio * dio) + (dip * dip)));
    }

    public float dis() {
        float dip = dip();
        float diq = diq();
        return (float) Math.sqrt((double) ((dip * dip) + (diq * diq)));
    }

    public float length() {
        return (float) Math.sqrt((double) lengthSquared());
    }

    public float lengthSquared() {
        float dio = dio();
        float dip = dip();
        float diq = diq();
        return (dio * dio) + (dip * dip) + (diq * diq);
    }

    public float dit() {
        return Vec3f.distance(this.mins, this.maxes) * 0.5f;
    }

    public float diu() {
        return Math.max(this.mins.lengthSquared(), this.maxes.lengthSquared());
    }

    /* renamed from: a */
    public boolean mo23422a(C1085Pt pt) {
        float f = 0.0f;
        if (pt.bny().x < this.mins.x) {
            float f2 = pt.bny().x - this.mins.x;
            f = f2 * f2;
        }
        if (pt.bny().y < this.mins.y) {
            float f3 = pt.bny().y - this.mins.y;
            f += f3 * f3;
        }
        if (pt.bny().z < this.mins.z) {
            float f4 = pt.bny().z - this.mins.z;
            f += f4 * f4;
        }
        if (pt.bny().x > this.maxes.x) {
            float f5 = pt.bny().x - this.maxes.x;
            f += f5 * f5;
        }
        if (pt.bny().y > this.maxes.y) {
            float f6 = pt.bny().y - this.maxes.y;
            f += f6 * f6;
        }
        if (pt.bny().z > this.maxes.z) {
            float f7 = pt.bny().z - this.maxes.z;
            f += f7 * f7;
        }
        return f <= pt.getRadius() * pt.getRadius();
    }

    /* renamed from: d */
    public boolean mo23434d(BoundingBox boundingBox) {
        return mo23423a(boundingBox);
    }

    /* renamed from: a */
    public boolean mo23423a(BoundingBox boundingBox) {
        Vec3f dqQ = boundingBox.dqQ();
        Vec3f vec3f = this.mins;
        if (vec3f.x > dqQ.x) {
            return false;
        }
        Vec3f dqP = boundingBox.dqP();
        Vec3f vec3f2 = this.maxes;
        if (vec3f2.x < dqP.x || vec3f.y > dqQ.y || vec3f2.y < dqP.y || vec3f.z > dqQ.z || vec3f2.z < dqP.z) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public C2567gu mo23417a(Vec3f vec3f, Vec3f vec3f2, boolean z) {
        float f;
        float f2;
        boolean z2;
        float f3;
        boolean z3;
        int c;
        C2567gu guVar = new C2567gu(vec3f, vec3f2);
        boolean z4 = true;
        float f4 = 0.0f;
        if (vec3f.x < this.mins.x) {
            float f5 = this.mins.x - vec3f.x;
            if (f5 > guVar.mo19147vR().x) {
                return guVar;
            }
            f = f5 / guVar.mo19147vR().x;
            z4 = false;
            f4 = -1.0f;
        } else if (vec3f.x > this.maxes.x) {
            float f6 = this.maxes.x - vec3f.x;
            if (f6 < guVar.mo19147vR().x) {
                return guVar;
            }
            f = f6 / guVar.mo19147vR().x;
            z4 = false;
            f4 = 1.0f;
        } else {
            f = -1.0f;
        }
        float f7 = 0.0f;
        if (vec3f.y < this.mins.y) {
            float f8 = this.mins.y - vec3f.y;
            if (f8 > guVar.mo19147vR().y) {
                return guVar;
            }
            f2 = f8 / guVar.mo19147vR().y;
            z2 = false;
            f7 = -1.0f;
        } else if (vec3f.y > this.maxes.y) {
            float f9 = this.maxes.y - vec3f.y;
            if (f9 < guVar.mo19147vR().y) {
                return guVar;
            }
            f2 = f9 / guVar.mo19147vR().y;
            z2 = false;
            f7 = 1.0f;
        } else {
            f2 = -1.0f;
            z2 = z4;
        }
        float f10 = 0.0f;
        if (vec3f.z < this.mins.z) {
            float f11 = this.mins.z - vec3f.z;
            if (f11 > guVar.mo19147vR().z) {
                return guVar;
            }
            f3 = f11 / guVar.mo19147vR().z;
            z3 = false;
            f10 = -1.0f;
        } else if (vec3f.z > this.maxes.z) {
            float f12 = this.maxes.z - vec3f.z;
            if (f12 < guVar.mo19147vR().z) {
                return guVar;
            }
            f3 = f12 / guVar.mo19147vR().z;
            z3 = false;
            f10 = 1.0f;
        } else {
            f3 = -1.0f;
            z3 = z2;
        }
        if (!z3) {
            int z5 = 0;
            if (f2 > f) {
                z5 = 1;
                f = f2;
            }
            if (f3 > f) {
                c = 2;
                f = f3;
            } else {
                c = z5;
            }
            switch (c) {
                case 0:
                    float f13 = vec3f.y + (guVar.mo19147vR().y * f);
                    if (f13 >= this.mins.y && f13 <= this.maxes.y) {
                        float f14 = vec3f.z + (guVar.mo19147vR().z * f);
                        if (f14 >= this.mins.z && f14 <= this.maxes.z) {
                            guVar.mo19135b(f4, 0.0f, 0.0f);
                            break;
                        } else {
                            return guVar;
                        }
                    } else {
                        return guVar;
                    }
                case 1:
                    float f15 = vec3f.x + (guVar.mo19147vR().x * f);
                    if (f15 >= this.mins.x && f15 <= this.maxes.x) {
                        float f16 = vec3f.z + (guVar.mo19147vR().z * f);
                        if (f16 >= this.mins.z && f16 <= this.maxes.z) {
                            guVar.mo19135b(0.0f, f7, 0.0f);
                            break;
                        } else {
                            return guVar;
                        }
                    } else {
                        return guVar;
                    }
                case 2:
                    float f17 = vec3f.x + (guVar.mo19147vR().x * f);
                    if (f17 >= this.mins.x && f17 <= this.maxes.x) {
                        float f18 = vec3f.y + (guVar.mo19147vR().y * f);
                        if (f18 >= this.mins.y && f18 <= this.maxes.y) {
                            guVar.mo19135b(0.0f, 0.0f, f10);
                            break;
                        } else {
                            return guVar;
                        }
                    } else {
                        return guVar;
                    }
            }
            guVar.mo19134ac(f);
            guVar.setIntersected(true);
            return guVar;
        } else if (!z) {
            return guVar;
        } else {
            guVar.mo19138l(guVar.mo19147vR().dfS().dfP());
            guVar.mo19134ac(0.0f);
            guVar.setIntersected(true);
            return guVar;
        }
    }

    public void reset() {
        Vec3f vec3f = this.mins;
        Vec3f vec3f2 = this.mins;
        this.mins.z = Float.POSITIVE_INFINITY;
        vec3f2.y = Float.POSITIVE_INFINITY;
        vec3f.x = Float.POSITIVE_INFINITY;
        Vec3f vec3f3 = this.maxes;
        Vec3f vec3f4 = this.maxes;
        this.maxes.z = Float.NEGATIVE_INFINITY;
        vec3f4.y = Float.NEGATIVE_INFINITY;
        vec3f3.x = Float.NEGATIVE_INFINITY;
    }

    /* renamed from: bR */
    public void mo23429bR(Vec3f vec3f) {
        Vec3f vec3f2 = this.maxes;
        Vec3f vec3f3 = this.mins;
        float f = vec3f.x;
        vec3f3.x = f;
        vec3f2.x = f;
        Vec3f vec3f4 = this.maxes;
        Vec3f vec3f5 = this.mins;
        float f2 = vec3f.y;
        vec3f5.y = f2;
        vec3f4.y = f2;
        Vec3f vec3f6 = this.maxes;
        Vec3f vec3f7 = this.mins;
        float f3 = vec3f.z;
        vec3f7.z = f3;
        vec3f6.z = f3;
    }

    public void addPoint(Vec3f vec3f) {
        if (vec3f.x < this.mins.x) {
            this.mins.x = vec3f.x;
        }
        if (vec3f.y < this.mins.y) {
            this.mins.y = vec3f.y;
        }
        if (vec3f.z < this.mins.z) {
            this.mins.z = vec3f.z;
        }
        if (vec3f.x > this.maxes.x) {
            this.maxes.x = vec3f.x;
        }
        if (vec3f.y > this.maxes.y) {
            this.maxes.y = vec3f.y;
        }
        if (vec3f.z > this.maxes.z) {
            this.maxes.z = vec3f.z;
        }
    }

    public void addPoint(float f, float f2, float f3) {
        if (f < this.mins.x) {
            this.mins.x = f;
        }
        if (f2 < this.mins.y) {
            this.mins.y = f2;
        }
        if (f3 < this.mins.z) {
            this.mins.z = f3;
        }
        if (f > this.maxes.x) {
            this.maxes.x = f;
        }
        if (f2 > this.maxes.y) {
            this.maxes.y = f2;
        }
        if (f3 > this.maxes.z) {
            this.maxes.z = f3;
        }
    }

    /* renamed from: a */
    public void mo23418a(Matrix4fWrap ajk, BoundingBox boundingBox) {
        Vec3f vec3f = new Vec3f(this.maxes.x, this.maxes.y, this.maxes.z);
        Vec3f vec3f2 = new Vec3f(this.maxes.x, this.maxes.y, this.mins.z);
        Vec3f vec3f3 = new Vec3f(this.maxes.x, this.mins.y, this.mins.z);
        Vec3f vec3f4 = new Vec3f(this.maxes.x, this.mins.y, this.maxes.z);
        Vec3f vec3f5 = new Vec3f(this.mins.x, this.maxes.y, this.maxes.z);
        Vec3f vec3f6 = new Vec3f(this.mins.x, this.maxes.y, this.mins.z);
        Vec3f vec3f7 = new Vec3f(this.mins.x, this.mins.y, this.mins.z);
        Vec3f vec3f8 = new Vec3f(this.mins.x, this.mins.y, this.maxes.z);
        boundingBox.reset();
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f2));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f3));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f4));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f5));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f6));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f7));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f8));
    }

    /* renamed from: a */
    public void mo23421a(TransformWrap bcVar, aLH alh) {
        Vec3d ajr = new Vec3d((double) this.maxes.x, (double) this.maxes.y, (double) this.maxes.z);
        Vec3d ajr2 = new Vec3d((double) this.maxes.x, (double) this.maxes.y, (double) this.mins.z);
        Vec3d ajr3 = new Vec3d((double) this.maxes.x, (double) this.mins.y, (double) this.mins.z);
        Vec3d ajr4 = new Vec3d((double) this.maxes.x, (double) this.mins.y, (double) this.maxes.z);
        Vec3d ajr5 = new Vec3d((double) this.mins.x, (double) this.maxes.y, (double) this.maxes.z);
        Vec3d ajr6 = new Vec3d((double) this.mins.x, (double) this.maxes.y, (double) this.mins.z);
        Vec3d ajr7 = new Vec3d((double) this.mins.x, (double) this.mins.y, (double) this.mins.z);
        Vec3d ajr8 = new Vec3d((double) this.mins.x, (double) this.mins.y, (double) this.maxes.z);
        alh.reset();
        bcVar.mo17331a(ajr);
        alh.mo9842aG(ajr);
        bcVar.mo17331a(ajr2);
        alh.mo9842aG(ajr2);
        bcVar.mo17331a(ajr3);
        alh.mo9842aG(ajr3);
        bcVar.mo17331a(ajr4);
        alh.mo9842aG(ajr4);
        bcVar.mo17331a(ajr5);
        alh.mo9842aG(ajr5);
        bcVar.mo17331a(ajr6);
        alh.mo9842aG(ajr6);
        bcVar.mo17331a(ajr7);
        alh.mo9842aG(ajr7);
        bcVar.mo17331a(ajr8);
        alh.mo9842aG(ajr8);
    }

    /* renamed from: b */
    public void mo23426b(Matrix4fWrap ajk, BoundingBox boundingBox) {
        Vec3f vec3f = new Vec3f(this.maxes.x, this.maxes.y, this.maxes.z);
        Vec3f vec3f2 = new Vec3f(this.maxes.x, this.maxes.y, this.mins.z);
        Vec3f vec3f3 = new Vec3f(this.maxes.x, this.mins.y, this.mins.z);
        Vec3f vec3f4 = new Vec3f(this.maxes.x, this.mins.y, this.maxes.z);
        Vec3f vec3f5 = new Vec3f(this.mins.x, this.maxes.y, this.maxes.z);
        Vec3f vec3f6 = new Vec3f(this.mins.x, this.maxes.y, this.mins.z);
        Vec3f vec3f7 = new Vec3f(this.mins.x, this.mins.y, this.mins.z);
        Vec3f vec3f8 = new Vec3f(this.mins.x, this.mins.y, this.maxes.z);
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f2));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f3));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f4));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f5));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f6));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f7));
        boundingBox.addPoint(ajk.mo14004c((Tuple3f) vec3f8));
    }

    /* renamed from: c */
    public void mo23431c(Matrix4fWrap ajk, BoundingBox boundingBox) {
        float abs = Math.abs(ajk.m00);
        float abs2 = Math.abs(ajk.m01);
        float abs3 = Math.abs(ajk.m02);
        float abs4 = Math.abs(ajk.m10);
        float abs5 = Math.abs(ajk.m11);
        float abs6 = Math.abs(ajk.m12);
        float abs7 = Math.abs(ajk.m20);
        float abs8 = Math.abs(ajk.m21);
        float abs9 = Math.abs(ajk.m22);
        float dio = dio();
        float dip = dip();
        float diq = diq();
        float f = (((abs * dio) + (abs2 * dip)) + (diq * abs3)) / 2.0f;
        float f2 = (((dio * abs4) + (dip * abs5)) + (diq * abs6)) / 2.0f;
        float f3 = (((dio * abs7) + (dip * abs8)) + (diq * abs9)) / 2.0f;
        float f4 = ajk.m03;
        float f5 = ajk.m13;
        float f6 = ajk.m23;
        boundingBox.maxes.x = f4 + f;
        boundingBox.maxes.y = f5 + f2;
        boundingBox.maxes.z = f6 + f3;
        boundingBox.mins.x = f4 - f;
        boundingBox.mins.y = f5 - f2;
        boundingBox.mins.z = f6 - f3;
    }

    /* renamed from: a */
    public void mo23420a(Quat4fWrap aoy, Vec3f vec3f, BoundingBox boundingBox) {
        boundingBox.reset();
        float f = aoy.x;
        float f2 = aoy.y;
        float f3 = aoy.z;
        float f4 = aoy.w;
        float f5 = (f4 * f4) + (f * f) + (f2 * f2) + (f3 * f3);
        if (!Quat4fWrap.isZero(1.0f - f5)) {
            float sqrt = (float) Math.sqrt((double) f5);
            f /= sqrt;
            f2 /= sqrt;
            f3 /= sqrt;
            f4 /= sqrt;
        }
        float f6 = f * f;
        float f7 = f * f2;
        float f8 = f * f3;
        float f9 = f * f4;
        float f10 = f2 * f2;
        float f11 = f2 * f3;
        float f12 = f2 * f4;
        float f13 = f3 * f3;
        float f14 = f4 * f3;
        float abs = Math.abs(1.0f - (2.0f * (f10 + f13)));
        float abs2 = Math.abs(2.0f * (f7 - f14));
        float abs3 = Math.abs(2.0f * (f8 + f12));
        float abs4 = Math.abs((f14 + f7) * 2.0f);
        float abs5 = Math.abs(1.0f - ((f13 + f6) * 2.0f));
        float abs6 = Math.abs(2.0f * (f11 - f9));
        float abs7 = Math.abs((f8 - f12) * 2.0f);
        float abs8 = Math.abs((f9 + f11) * 2.0f);
        float abs9 = Math.abs(1.0f - ((f6 + f10) * 2.0f));
        float dio = dio();
        float dip = dip();
        float diq = diq();
        float f15 = (((abs * dio) + (abs2 * dip)) + (diq * abs3)) / 2.0f;
        float f16 = (((abs4 * dio) + (abs5 * dip)) + (diq * abs6)) / 2.0f;
        float f17 = (((abs7 * dio) + (abs8 * dip)) + (diq * abs9)) / 2.0f;
        float f18 = vec3f.x;
        float f19 = vec3f.y;
        float f20 = vec3f.z;
        boundingBox.maxes.x = f18 + f15;
        boundingBox.maxes.y = f19 + f16;
        boundingBox.maxes.z = f20 + f17;
        boundingBox.mins.x = f18 - f15;
        boundingBox.mins.y = f19 - f16;
        boundingBox.mins.z = f20 - f17;
    }

    /* renamed from: a */
    public void mo23419a(Quat4fWrap aoy, Vec3d ajr, aLH alh) {
        alh.reset();
        float f = aoy.x;
        float f2 = aoy.y;
        float f3 = aoy.z;
        float f4 = aoy.w;
        float f5 = (f4 * f4) + (f * f) + (f2 * f2) + (f3 * f3);
        if (!Quat4fWrap.isZero(1.0f - f5)) {
            float sqrt = (float) Math.sqrt((double) f5);
            f /= sqrt;
            f2 /= sqrt;
            f3 /= sqrt;
            f4 /= sqrt;
        }
        float f6 = f * f;
        float f7 = f * f2;
        float f8 = f * f3;
        float f9 = f * f4;
        float f10 = f2 * f2;
        float f11 = f2 * f3;
        float f12 = f2 * f4;
        float f13 = f3 * f3;
        float f14 = f4 * f3;
        float abs = Math.abs(1.0f - (2.0f * (f10 + f13)));
        float abs2 = Math.abs(2.0f * (f7 - f14));
        float abs3 = Math.abs(2.0f * (f8 + f12));
        float abs4 = Math.abs((f14 + f7) * 2.0f);
        float abs5 = Math.abs(1.0f - ((f13 + f6) * 2.0f));
        float abs6 = Math.abs(2.0f * (f11 - f9));
        float abs7 = Math.abs((f8 - f12) * 2.0f);
        float abs8 = Math.abs((f9 + f11) * 2.0f);
        float abs9 = Math.abs(1.0f - ((f6 + f10) * 2.0f));
        float dio = dio();
        float dip = dip();
        float diq = diq();
        float f15 = (((abs * dio) + (abs2 * dip)) + (diq * abs3)) / 2.0f;
        float f16 = (((abs4 * dio) + (dip * abs5)) + (diq * abs6)) / 2.0f;
        float f17 = (((dio * abs7) + (dip * abs8)) + (diq * abs9)) / 2.0f;
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        alh.mo9824A(((double) f15) + d, ((double) f16) + d2, ((double) f17) + d3);
        alh.mo9825B(d - ((double) f15), d2 - ((double) f16), d3 - ((double) f17));
    }

    public String toString() {
        return "[(" + this.mins.x + ", " + this.mins.y + ", " + this.mins.z + ") -> (" + this.maxes.x + ", " + this.maxes.y + ", " + this.maxes.z + ")]";
    }

    public int hashCode() {
        return this.mins.hashCode() * this.maxes.hashCode();
    }

    /* renamed from: dqR */
    public BoundingBox clone() {
        return new BoundingBox(this.mins, this.maxes);
    }

    public Vec3f bny() {
        new Vec3f();
        return this.mins.mo23503i(this.maxes.mo23506j(this.mins).mo23510mS(0.5f));
    }

    /* renamed from: G */
    public void mo23415G(float f, float f2, float f3) {
        this.maxes.x = f;
        this.maxes.y = f2;
        this.maxes.z = f3;
    }

    /* renamed from: H */
    public void mo23416H(float f, float f2, float f3) {
        this.mins.x = f;
        this.mins.y = f2;
        this.mins.z = f3;
    }

    /* renamed from: e */
    public void mo23445e(BoundingBox boundingBox) {
        Vec3f dqQ = boundingBox.dqQ();
        Vec3f dqP = boundingBox.dqP();
        mo23415G(dqQ.x, dqQ.y, dqQ.z);
        mo23416H(dqP.x, dqP.y, dqP.z);
    }

    /* renamed from: f */
    public void mo23447f(BoundingBox boundingBox) {
        addPoint(boundingBox.maxes);
        addPoint(boundingBox.mins);
    }

    public void readExternal(IReadExternal vm) {
        this.mins = (Vec3f) vm.mo6367he("mins");
        this.maxes = (Vec3f) vm.mo6367he("maxes");
    }

    public void writeExternal(IWriteExternal att) {
        att.mo16261a("mins", (IExternalIO) this.mins);
        att.mo16261a("maxes", (IExternalIO) this.maxes);
    }
}
