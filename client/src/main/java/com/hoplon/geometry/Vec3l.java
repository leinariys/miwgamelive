package com.hoplon.geometry;

import game.geometry.Vec3d;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import taikodom.addon.neo.preferences.GUIPrefAddon;

import java.io.Serializable;

/* compiled from: a */
public class Vec3l implements IExternalIO, Serializable {


    /* renamed from: x */
    public long x;

    /* renamed from: y */
    public long y;

    /* renamed from: z */
    public long z;

    public Vec3l(Vec3l vec3l) {
        this.x = vec3l.x;
        this.y = vec3l.y;
        this.z = vec3l.z;
    }

    public Vec3l(long j, long j2, long j3) {
        this.x = j;
        this.y = j2;
        this.z = j3;
    }

    public Vec3l() {
    }

    /* renamed from: a */
    public static long m41954a(Vec3l vec3l, Vec3l vec3l2) {
        return ((vec3l.x - vec3l2.x) * (vec3l.x - vec3l2.x)) + ((vec3l.y - vec3l2.y) * (vec3l.y - vec3l2.y)) + ((vec3l.z - vec3l2.z) * (vec3l.z - vec3l2.z));
    }

    /* renamed from: a */
    public void mo23526a(Vec3l vec3l) {
        this.x = vec3l.x;
        this.y = vec3l.y;
        this.z = vec3l.z;
    }

    /* renamed from: b */
    public Vec3l mo23527b(Vec3l vec3l) {
        return new Vec3l(this.x + vec3l.x, this.y + vec3l.y, this.z + vec3l.z);
    }

    /* renamed from: c */
    public Vec3l mo23528c(Vec3l vec3l) {
        return new Vec3l(this.x - vec3l.x, this.y - vec3l.y, this.z - vec3l.z);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof Vec3l)) {
            return false;
        }
        Vec3l vec3l = (Vec3l) obj;
        if (this.x == vec3l.x && this.y == vec3l.y && this.z == vec3l.z) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (int) ((this.x + this.y) ^ ((this.y + this.z) >>> 32));
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public long dfU() {
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
    }

    public Vec3f dfV() {
        return new Vec3f((float) this.x, (float) this.y, (float) this.z);
    }

    public Vec3d dfR() {
        return new Vec3d((double) this.x, (double) this.y, (double) this.z);
    }

    public void readExternal(IReadExternal vm) {
        this.x = vm.readLong(GUIPrefAddon.C4817c.X);
        this.y = vm.readLong(GUIPrefAddon.C4817c.Y);
        this.z = vm.readLong("z");
    }

    public void writeExternal(IWriteExternal att) {
        att.writeLong(GUIPrefAddon.C4817c.X, this.x);
        att.writeLong(GUIPrefAddon.C4817c.Y, this.y);
        att.writeLong("z", this.z);
    }
}
