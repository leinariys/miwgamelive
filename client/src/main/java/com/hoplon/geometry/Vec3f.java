package com.hoplon.geometry;

import game.geometry.*;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import org.mozilla1.javascript.ScriptRuntime;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.io.IOException;

/* compiled from: a */
public final class Vec3f extends Vector3f implements IExternalIO, IGeometryF {

    public Vec3f() {
    }

    public Vec3f(float[] fArr) {
        super(fArr);
    }

    public Vec3f(Vector3f vector3f) {
        super(vector3f);
    }

    public Vec3f(Vector3d vector3d) {
        super(vector3d);
    }

    public Vec3f(Tuple3f tuple3f) {
        super(tuple3f);
    }

    public Vec3f(Tuple3d tuple3d) {
        super(tuple3d);
    }

    public Vec3f(float x, float y, float z) {
        super(x, y, z);
    }

    public Vec3f(Vec3d ajr) {
        this.x = (float) ajr.x;
        this.y = (float) ajr.y;
        this.z = (float) ajr.z;
    }

    public Vec3f(Vec3dWrap iv) {
        this.x = (float) iv.x;
        this.y = (float) iv.y;
        this.z = (float) iv.z;
    }

    public Vec3f(Vec3fWrap iw) {
        this.x = iw.x;
        this.y = iw.y;
        this.z = iw.z;
    }

    /* renamed from: L */
    public static float distance(Vec3f vec3f, Vec3f vec3f2) {
        return (float) Math.sqrt((double) (((vec3f.x - vec3f2.x) * (vec3f.x - vec3f2.x)) + ((vec3f.y - vec3f2.y) * (vec3f.y - vec3f2.y)) + ((vec3f.z - vec3f2.z) * (vec3f.z - vec3f2.z))));
    }

    /* renamed from: M */
    public static float direction(Vec3f vec3f, Vec3f vec3f2) {
        return ((vec3f.x - vec3f2.x) * (vec3f.x - vec3f2.x)) + ((vec3f.y - vec3f2.y) * (vec3f.y - vec3f2.y)) + ((vec3f.z - vec3f2.z) * (vec3f.z - vec3f2.z));
    }

    /* renamed from: r */
    public static Vec3f m41904r(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        Vec3f j = vec3f2.mo23506j(vec3f);
        float length = j.length();
        Vec3f mS = j.mo23510mS(1.0f / length);
        float dot = mS.dot(vec3f3.mo23506j(vec3f));
        if (dot <= 0.0f) {
            return new Vec3f((Vector3f) vec3f);
        }
        if (dot >= 1.0f) {
            return new Vec3f((Vector3f) vec3f2);
        }
        return vec3f.mo23503i(mS.mo23510mS(length * dot));
    }

    /* renamed from: s */
    public static float distance(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        return distance(m41904r(vec3f, vec3f2, vec3f3), vec3f3);
    }

    /* renamed from: b */
    public static Vec3f m41901b(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        return new Vec3f((ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3), (ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3), (f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3));
    }

    /* renamed from: c */
    public static Vec3f m41902c(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        return new Vec3f((ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3) + ajk.m03, (ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3) + ajk.m13, (f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3) + ajk.m23);
    }

    /* renamed from: b */
    public static Vec3f m41900b(float f, Tuple3f tuple3f, Tuple3f tuple3f2) {
        return new Vec3f().mo23471a(f, tuple3f, tuple3f2);
    }

    /* renamed from: d */
    public static Vec3f m41903d(Tuple3d tuple3d, Tuple3d tuple3d2) {
        return new Vec3f().mo23480c(tuple3d, tuple3d2);
    }

    /* renamed from: mS */
    public Vec3f mo23510mS(float f) {
        return new Vec3f(this.x * f, this.y * f, this.z * f);
    }

    /* renamed from: f */
    public Vec3f mo23499f(Tuple3f tuple3f) {
        return new Vec3f(this.x * tuple3f.x, this.y * tuple3f.y, this.z * tuple3f.z);
    }

    /* renamed from: g */
    public Vec3f mo23500g(Tuple3f tuple3f) {
        this.x *= tuple3f.x;
        this.y *= tuple3f.y;
        this.z *= tuple3f.z;
        return this;
    }

    /* renamed from: h */
    public void mo23501h(Tuple3f tuple3f) {
        this.x *= tuple3f.x;
        this.y *= tuple3f.y;
        this.z *= tuple3f.z;
    }

    /* renamed from: i */
    public Vec3f mo23503i(Tuple3f tuple3f) {
        return new Vec3f(this.x + tuple3f.x, this.y + tuple3f.y, this.z + tuple3f.z);
    }

    /* renamed from: j */
    public Vec3f mo23506j(Tuple3f tuple3f) {
        return new Vec3f(this.x - tuple3f.x, this.y - tuple3f.y, this.z - tuple3f.z);
    }

    public void sub(Tuple3d tuple3d, Tuple3d tuple3d2) {
        this.x = (float) (tuple3d.x - tuple3d2.x);
        this.y = (float) (tuple3d.y - tuple3d2.y);
        this.z = (float) (tuple3d.z - tuple3d2.z);
    }

    public void cox() {
        float length = length();
        if (length > 0.0f) {
            this.x /= length;
            this.y /= length;
            this.z /= length;
        }
    }

    /* renamed from: b */
    public Vec3f mo23476b(Vector3f vector3f) {
        return new Vec3f((this.y * vector3f.z) - (this.z * vector3f.y), (this.z * vector3f.x) - (this.x * vector3f.z), (this.x * vector3f.y) - (this.y * vector3f.x));
    }

    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public Vec3f dfO() {
        double sqrt = Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
        if (sqrt <= ScriptRuntime.NaN) {
            return new Vec3f((Vector3f) this);
        }
        float f = (float) (1.0d / sqrt);
        return new Vec3f(this.x * f, this.y * f, this.z * f);
    }

    public Vec3f dfP() {
        double sqrt = Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
        if (sqrt > ScriptRuntime.NaN) {
            float f = (float) (1.0d / sqrt);
            this.x *= f;
            this.y *= f;
            this.z = f * this.z;
        }
        return this;
    }

    public Vec3f dfQ() {
        return new Vec3f((Vector3f) this);
    }

    public float get(int i) {
        switch (i) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void set(int i, float f) {
        switch (i) {
            case 0:
                this.x = f;
                return;
            case 1:
                this.y = f;
                return;
            case 2:
                this.z = f;
                return;
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public int hashCode() {
        return (((Float.floatToIntBits(this.x) * 31) + Float.floatToIntBits(this.y)) * 31) + Float.floatToIntBits(this.z);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        Vec3f vec3f = (Vec3f) obj;
        if (vec3f.x == this.x && vec3f.y == this.y && vec3f.z == this.z) {
            return true;
        }
        return false;
    }

    /**
     * Сколярнок
     *
     * @param iw
     * @return
     */
    /* renamed from: a */
    public float dot(Vec3fWrap iw) {
        return (this.x * iw.x) + (this.y * iw.y) + (this.z * iw.z);
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public float getZ() {
        return this.z;
    }

    public Vec3d dfR() {
        return new Vec3d((double) this.x, (double) this.y, (double) this.z);
    }

    public boolean anA() {
        float f = this.x + this.y + this.z;
        return (Float.isNaN(f) || f == Float.NEGATIVE_INFINITY || f == Float.POSITIVE_INFINITY) ? false : true;
    }

    public boolean isZero() {
        if (this.x == 0.0f && this.y == 0.0f && this.z == 0.0f) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public Vec3f mo23470a(float f, Tuple3f tuple3f) {
        this.x = tuple3f.x * f;
        this.y = tuple3f.y * f;
        this.z = tuple3f.z * f;
        return this;
    }

    /* renamed from: b */
    public Vec3f mo23475b(float f, Tuple3f tuple3f) {
        return new Vec3f((Vector3f) this).mo23470a(f, tuple3f);
    }

    /* renamed from: mT */
    public Vec3f mo23511mT(float f) {
        this.x *= f;
        this.y *= f;
        this.z *= f;
        return this;
    }

    /* renamed from: d */
    public Vec3d mo23485d(Tuple3d tuple3d) {
        return new Vec3d(((double) this.x) + tuple3d.x, ((double) this.y) + tuple3d.y, ((double) this.z) + tuple3d.z);
    }

    /* renamed from: v */
    public Vec3f mo23521v(float f, float f2, float f3) {
        float f4 = (this.y * f3) - (this.z * f2);
        float f5 = (this.z * f) - (this.x * f3);
        this.x = f4;
        this.y = f5;
        this.z = (this.x * f2) - (this.y * f);
        return this;
    }

    /* renamed from: w */
    public Vec3f mo23522w(float f, float f2, float f3) {
        return new Vec3f((this.y * f3) - (this.z * f2), (this.z * f) - (this.x * f3), (this.x * f2) - (this.y * f));
    }

    /* renamed from: x */
    public void mo23523x(float f, float f2, float f3) {
        float f4 = (this.y * f3) - (this.z * f2);
        float f5 = (this.z * f) - (this.x * f3);
        this.x = f4;
        this.y = f5;
        this.z = (this.x * f2) - (this.y * f);
    }

    /* renamed from: c */
    public void mo23483c(Vector3f vector3f) {
        mo23523x(vector3f.x, vector3f.y, vector3f.z);
    }

    /* renamed from: d */
    public Vec3f mo23489d(Vector3f vector3f) {
        return mo23521v(vector3f.x, vector3f.y, vector3f.z);
    }

    /* renamed from: y */
    public float mo23524y(float f, float f2, float f3) {
        return (this.x * f) + (this.y * f2) + (this.z * f3);
    }

    /* renamed from: k */
    public Vec3f mo23507k(Tuple3f tuple3f) {
        return new Vec3f((float) Math.pow((double) this.x, (double) tuple3f.x), (float) Math.pow((double) this.y, (double) tuple3f.y), (float) Math.pow((double) this.z, (double) tuple3f.z));
    }

    /* renamed from: l */
    public Vec3f mo23508l(Tuple3f tuple3f) {
        this.x = (float) Math.pow((double) this.x, (double) tuple3f.x);
        this.y = (float) Math.pow((double) this.y, (double) tuple3f.y);
        this.z = (float) Math.pow((double) this.z, (double) tuple3f.z);
        return this;
    }

    /* renamed from: m */
    public void mo23509m(Tuple3f tuple3f) {
        this.x = (float) Math.pow((double) this.x, (double) tuple3f.x);
        this.y = (float) Math.pow((double) this.y, (double) tuple3f.y);
        this.z = (float) Math.pow((double) this.z, (double) tuple3f.z);
    }

    public Vec3f dfS() {
        return new Vec3f(-this.x, -this.y, -this.z);
    }

    public Vec3f dfT() {
        negate();
        return this;
    }

    /* renamed from: n */
    public Vec3f mo23512n(Tuple3f tuple3f) {
        this.x = tuple3f.x;
        this.y = tuple3f.y;
        this.z = tuple3f.z;
        return this;
    }

    /* renamed from: z */
    public Vec3f mo23525z(float f, float f2, float f3) {
        this.x = f;
        this.y = f2;
        this.z = f3;
        return this;
    }

    /* renamed from: o */
    public Vec3f mo23513o(Tuple3f tuple3f) {
        this.x -= tuple3f.x;
        this.y -= tuple3f.y;
        this.z -= tuple3f.z;
        return this;
    }

    /* renamed from: A */
    public void mo23464A(float f, float f2, float f3) {
        this.x -= f;
        this.y -= f2;
        this.z -= f3;
    }

    /* renamed from: B */
    public Vec3f mo23465B(float f, float f2, float f3) {
        this.x -= f;
        this.y -= f2;
        this.z -= f3;
        return this;
    }

    /* renamed from: C */
    public Vec3f mo23466C(float f, float f2, float f3) {
        float f4 = this.x - f;
        this.x = f4;
        float f5 = this.z - f3;
        this.z = f5;
        return new Vec3f(f4, this.y - f2, f5);
    }

    /* renamed from: p */
    public Vec3f mo23515p(Tuple3f tuple3f) {
        this.x += tuple3f.x;
        this.y += tuple3f.y;
        this.z += tuple3f.z;
        return this;
    }

    /* renamed from: D */
    public Vec3f mo23467D(float f, float f2, float f3) {
        this.x += f;
        this.y += f2;
        this.z += f3;
        return this;
    }

    /* renamed from: E */
    public void mo23468E(float f, float f2, float f3) {
        this.x += f;
        this.y += f2;
        this.z += f3;
    }

    /* renamed from: F */
    public Vec3f mo23469F(float f, float f2, float f3) {
        return new Vec3f(this.x + f, this.y + f2, this.z + f3);
    }

    /* renamed from: c */
    public Vec3f mo23481c(Tuple3f tuple3f, Tuple3f tuple3f2) {
        this.x = tuple3f.x - tuple3f2.x;
        this.y = tuple3f.y - tuple3f2.y;
        this.z = tuple3f.z - tuple3f2.z;
        return this;
    }

    /* renamed from: d */
    public Vec3f mo23488d(Tuple3f tuple3f, Tuple3f tuple3f2) {
        return new Vec3f(tuple3f.x - tuple3f2.x, tuple3f.y - tuple3f2.y, tuple3f.z - tuple3f2.z);
    }

    /* renamed from: b */
    public Vec3f mo23477b(Vector3f vector3f, Vector3f vector3f2) {
        float f = (vector3f.y * vector3f2.z) - (vector3f.z * vector3f2.y);
        float f2 = (vector3f.z * vector3f2.x) - (vector3f.x * vector3f2.z);
        this.x = f;
        this.y = f2;
        this.z = (vector3f.x * vector3f2.y) - (vector3f.y * vector3f2.x);
        return this;
    }

    /* renamed from: c */
    public Vec3f mo23482c(Vector3f vector3f, Vector3f vector3f2) {
        return new Vec3f((vector3f.y * vector3f2.z) - (vector3f.z * vector3f2.y), (vector3f.z * vector3f2.x) - (vector3f.x * vector3f2.z), (vector3f.x * vector3f2.y) - (vector3f.y * vector3f2.x));
    }

    /* renamed from: bF */
    public float mo23478bF(Vec3f vec3f) {
        return (((this.y * vec3f.z) - (this.z * vec3f.y)) * vec3f.x) +
                (((this.z * vec3f.x) - (this.x * vec3f.z)) * vec3f.y) +
                (vec3f.z * ((this.x * vec3f.y) - (this.y * vec3f.x)));
    }

    /* renamed from: a */
    public void mo23473a(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        this.x = (ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3);
        this.y = (ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3);
        this.z = (f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3);
    }

    /* renamed from: a */
    public void mo23472a(Matrix3fWrap ajd, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        this.x = (ajd.m00 * f) + (ajd.m01 * f2) + (ajd.m02 * f3);
        this.y = (ajd.m10 * f) + (ajd.m11 * f2) + (ajd.m12 * f3);
        this.z = (f * ajd.m20) + (f2 * ajd.m21) + (ajd.m22 * f3);
    }

    /* renamed from: p */
    public Vec3f mo23514p(Matrix4fWrap ajk) {
        return new Vec3f((this.x * ajk.m00) + (this.y * ajk.m01) + (this.z * ajk.m02), (this.x * ajk.m10) + (this.y * ajk.m11) + (this.z * ajk.m12), (this.x * ajk.m20) + (this.y * ajk.m21) + (this.z * ajk.m22));
    }

    /* renamed from: d */
    public Vec3f mo23487d(Matrix4fWrap ajk, Tuple3f tuple3f) {
        mo23473a(ajk, tuple3f);
        return this;
    }

    /* renamed from: q */
    public Vec3f mo23516q(Matrix4fWrap ajk) {
        mo23517r(ajk);
        return this;
    }

    /* renamed from: r */
    public void mo23517r(Matrix4fWrap ajk) {
        mo23473a(ajk, (Tuple3f) this);
    }

    /* renamed from: e */
    public void mo23496e(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        this.x = (ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3) + ajk.m03;
        this.y = (ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3) + ajk.m13;
        this.z = (f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3) + ajk.m23;
    }

    /* renamed from: f */
    public Vec3f mo23498f(Matrix4fWrap ajk, Tuple3f tuple3f) {
        mo23496e(ajk, tuple3f);
        return this;
    }

    /* renamed from: a */
    public Vec3f mo23471a(float f, Tuple3f tuple3f, Tuple3f tuple3f2) {
        scaleAdd(f, tuple3f, tuple3f2);
        return this;
    }

    /* renamed from: c */
    public Vec3f mo23479c(float f, Tuple3f tuple3f) {
        scaleAdd(f, tuple3f);
        return this;
    }

    /* renamed from: d */
    public Vec3f mo23486d(float f, Tuple3f tuple3f) {
        return new Vec3f().mo23471a(f, this, tuple3f);
    }

    public boolean isValid() {
        return !Float.isNaN(this.x) && !Float.isInfinite(this.x) && !Float.isNaN(this.y) && !Float.isInfinite(this.y) && !Float.isNaN(this.z) && !Float.isInfinite(this.z);
    }

    /* renamed from: c */
    public Vec3f mo23480c(Tuple3d tuple3d, Tuple3d tuple3d2) {
        this.x = (float) (tuple3d.x - tuple3d2.x);
        this.y = (float) (tuple3d.y - tuple3d2.y);
        this.z = (float) (tuple3d.z - tuple3d2.z);
        return this;
    }

    public void readExternal(IReadExternal vm) throws IOException {
        this.x = vm.readFloat("x");
        this.y = vm.readFloat("y");
        this.z = vm.readFloat("z");
    }

    public void writeExternal(IWriteExternal att) throws IOException {
        att.writeFloat("x", this.x);
        att.writeFloat("y", this.y);
        att.writeFloat("z", this.z);
    }
}
