package com.hoplon.taikodom.common;

import logic.thred.LogPrinter;

/* compiled from: a */
public class TaikodomVersion {
    public static final String VERSION = "1.0.4933.55";
    private static final Log log = LogPrinter.setClass(TaikodomVersion.class);

    public static void main(String[] strArr) {
        log.debug(VERSION);
    }

    public String getVersion() {
        return VERSION;
    }
}
