package com.hoplon.commons.thread;

import p001a.C4108zb;

/* compiled from: a */
public class ProfilingThreadPoolExecutor$ProfilingThreadPoolExecutorConsole implements C4141x861a4a60 {
    final /* synthetic */ C4108zb dva;
    long duT = 0;
    int duU;
    int duV;
    int duW;
    int duX;
    int duY;
    int duZ;

    public ProfilingThreadPoolExecutor$ProfilingThreadPoolExecutorConsole(C4108zb zbVar) {
        this.dva = zbVar;
    }

    private void bfI() {
        if (this.duT == 0) {
            this.dva.bNe.set(0);
            this.dva.bNb.set(0);
            this.dva.bNd.set(0);
            this.duT = System.currentTimeMillis();
            return;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.duT + 1000 <= currentTimeMillis) {
            this.duT = currentTimeMillis;
            this.duU = this.dva.bNe.getAndSet(0);
            this.duW = Math.round(((float) this.dva.bNc.getAndSet(0)) / 1000.0f);
            long andSet = this.dva.bNb.getAndSet(0);
            this.duX = Math.round(((float) andSet) / 1000.0f);
            this.duV = Math.round((((float) andSet) / ((float) this.duU)) / 1000.0f);
            this.duZ = (int) this.dva.bNd.getAndSet(0);
            this.duY = Math.round(((float) this.duZ) / ((float) this.duU));
        }
    }

    public int getNumExecs() {
        bfI();
        return this.duU;
    }

    public int getQueueSize() {
        return this.dva.getQueue().size();
    }

    public int getNumThreads() {
        return this.dva.bNf.gvI.get();
    }

    public int getExecTimeAveragePerThread_ms() {
        bfI();
        return this.duV;
    }

    public int getMaxExecTime_ms() {
        bfI();
        return this.duW;
    }

    public int getTotalExecTime_ms() {
        bfI();
        return this.duX;
    }

    public int getTotalWaitedCount() {
        bfI();
        return this.duZ;
    }

    public int getWaitedCountAveragePerThread_ms() {
        bfI();
        return this.duY;
    }
}
