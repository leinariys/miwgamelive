package com.hoplon.commons.thread;

/* renamed from: com.hoplon.commons.thread.ProfilingThreadPoolExecutor$ProfilingThreadPoolExecutorConsoleMBean */
/* compiled from: a */
public interface C4141x861a4a60 {
    int getExecTimeAveragePerThread_ms();

    int getMaxExecTime_ms();

    int getNumExecs();

    int getNumThreads();

    int getQueueSize();

    int getTotalExecTime_ms();

    int getTotalWaitedCount();

    int getWaitedCountAveragePerThread_ms();
}
