package com.hoplon.commons;

import logic.thred.LogPrinter;

/* compiled from: a */
public class TaikodomLogger$TaikodomLoggerConsole implements TaikodomLogger$TaikodomLoggerConsoleMBean {
    boolean debugEnabled = false;

    public boolean getDebugEnabled() {
        return this.debugEnabled;
    }

    public void setDebugEnabled(boolean z) {
        byte b = z ? (byte) 124 : 120;
        this.debugEnabled = z;
        LogPrinter.eqS = b;
        for (LogPrinter b2 : LogPrinter.eqW.values()) {
            b2.mo5853b(b);
        }
    }
}
