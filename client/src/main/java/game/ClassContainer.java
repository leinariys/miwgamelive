package game;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5977aeB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aBH */
/* compiled from: a */
public class ClassContainer extends aDJ implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz hhA = null;
    public static final C2491fm hhB = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9524fc0ecda69554b8f6a10f5571fb29", aum = 0)
    private static Class<?> clazz;

    static {
        m12747V();
    }

    public ClassContainer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ClassContainer(C5540aNg ang) {
        super(ang);
    }

    public ClassContainer(Class<?> cls) {
        super((C5540aNg) null);
        super._m_script_init(cls);
    }

    /* renamed from: V */
    static void m12747V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 2;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ClassContainer.class, "9524fc0ecda69554b8f6a10f5571fb29", i);
        hhA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(ClassContainer.class, "8ba4e95fb6ec05497d86ce0d034a0ec5", i3);
        hhB = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ClassContainer.class, "5713100dbc6216e3e010b951e842a07a", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ClassContainer.class, C5977aeB.class, _m_fields, _m_methods);
    }

    /* renamed from: az */
    private void m12749az(Class cls) {
        bFf().mo5608dq().mo3197f(hhA, cls);
    }

    private Class cJq() {
        return (Class) bFf().mo5608dq().mo3214p(hhA);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5977aeB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return cJr();
            case 1:
                return m12748au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Class<?> getClazz() {
        switch (bFf().mo6893i(hhB)) {
            case 0:
                return null;
            case 2:
                return (Class) bFf().mo5606d(new aCE(this, hhB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hhB, new Object[0]));
                break;
        }
        return cJr();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m12748au();
    }

    /* renamed from: aA */
    public void mo7833aA(Class<?> cls) {
        super.mo10S();
        m12749az(cls);
    }

    @C0064Am(aul = "8ba4e95fb6ec05497d86ce0d034a0ec5", aum = 0)
    private Class<?> cJr() {
        return cJq();
    }

    @C0064Am(aul = "5713100dbc6216e3e010b951e842a07a", aum = 0)
    /* renamed from: au */
    private String m12748au() {
        return cJq().getName().replaceAll("^.*[.]", "");
    }
}
