package game.engine;

import game.network.message.WriterBitsData;
import logic.res.KeyCode;
import util.Syst;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UTFDataFormatException;

/* renamed from: a.ala  reason: case insensitive filesystem */
/* compiled from: a */
public final class SocketMessage extends OutputStream implements WriterBitsData, Serializable {
    public static final int[] MASK_INT = new int[32];
    static final FloatBit FLOAT_DEF = new FloatDef();
    static final FloatBit FLOAT_TINY = new FloatCastom("Tiny Float", true, 4, 3, true, 7);
    static final FloatBit FLOAT_24BIT = new FloatCastom("24 bit FloatEncoding", true, 8, 15, true, 127);

    static {
        int i = 0;
        int i2 = 0;
        while (i < 32) {
            MASK_INT[i] = i2;
            i++;
            i2 = (i2 << 1) | 1;
        }
    }

    private byte[] message;
    private int count;
    private int mark;
    private int posit;

    public SocketMessage() {
        this(16);
    }

    public SocketMessage(int sizeMessage) {
        this.mark = -1;
        this.posit = 0;
        if (sizeMessage < 0) {
            throw new IllegalArgumentException("Invalid size: " + sizeMessage);
        }
        this.message = new byte[sizeMessage];
    }

    /* renamed from: d */
    public void writeBitLong(int i, long j) {
        if (i > 32) {
            writeBits(32, (int) (j >> (i - 32)));
            writeBits(i - 32, (int) j);
            return;
        }
        writeBits(i, (int) j);
    }

    public void writeBits(int i, int i2) {
        int i3 = this.posit;
        while (i > 0) {
            if (i < i3) {
                byte[] bArr = this.message;
                int i4 = this.mark;
                bArr[i4] = (byte) (bArr[i4] | ((MASK_INT[i] & i2) << (i3 - i)));
                this.posit = i3 - i;
                return;
            } else if (i == i3) {
                byte[] bArr2 = this.message;
                int i5 = this.mark;
                bArr2[i5] = (byte) (bArr2[i5] | (MASK_INT[i] & i2));
                this.posit = 0;
                return;
            } else if (i3 == 0) {
                ensure(this.count + 1);
                this.mark = this.count;
                this.message[this.count] = 0;
                this.count++;
                i3 = 8;
            } else {
                byte[] bArr3 = this.message;
                int i6 = this.mark;
                bArr3[i6] = (byte) (bArr3[i6] | ((i2 >> (i - i3)) & MASK_INT[i3]));
                i -= i3;
                i3 = 0;
            }
        }
    }

    public final void write(int i) {
        int i2 = this.count + 1;
        ensure(i2);
        this.message[this.count] = (byte) i;
        this.count = i2;
    }

    public final void write(byte[] bArr, int i, int i2) {
        if (i2 != 0) {
            int i3 = this.count + i2;
            ensure(i3);
            System.arraycopy(bArr, i, this.message, this.count, i2);
            this.count = i3;
        }
    }

    private final void ensure(int i) {
        if (i > this.message.length) {
            byte[] bArr = new byte[Math.max(this.message.length << 1, i)];
            System.arraycopy(this.message, 0, bArr, 0, this.count);
            this.message = bArr;
        }
    }

    public final void reset() {
        this.count = 0;
        this.mark = -1;
        this.posit = 0;
    }

    public final void seek(int i) {
        this.count = i;
    }

    public final byte[] toByteArray() {
        if (this.message.length == this.count) {
            return this.message;
        }
        byte[] bArr = new byte[this.count];
        System.arraycopy(this.message, 0, bArr, 0, this.count);
        return bArr;
    }

    public final int size() {
        return this.count;
    }

    public final void writeBoolean(boolean z) {
        writeBits(1, z ? 1 : 0);
    }

    public final void writeByte(int i) {
        write(i);
    }

    /* renamed from: sn */
    private final void writeByteInt(int i) {
        write(i);
    }

    /* renamed from: so */
    private final void writeByteInt2(int i) {
        int i2 = this.count + 2;
        ensure(i2);
        this.message[this.count] = (byte) ((i >>> 8) & 255);
        this.message[this.count + 1] = (byte) ((i >>> 0) & 255);
        this.count = i2;
    }

    /* renamed from: sp */
    private void writeByteInt3(int i) {
        int i2 = this.count + 3;
        ensure(i2);
        this.message[this.count] = (byte) ((i >>> 16) & 255);
        this.message[this.count + 1] = (byte) ((i >>> 8) & 255);
        this.message[this.count + 2] = (byte) ((i >>> 0) & 255);
        this.count = i2;
    }

    /* renamed from: sq */
    public final void writeByteInt4(int i) {
        int i2 = this.count + 4;
        ensure(i2);
        this.message[this.count] = (byte) ((i >>> 24) & 255);
        this.message[this.count + 1] = (byte) ((i >>> 16) & 255);
        this.message[this.count + 2] = (byte) ((i >>> 8) & 255);
        this.message[this.count + 3] = (byte) ((i >>> 0) & 255);
        this.count = i2;
    }

    /* renamed from: hP */
    private void writeByteLong5(long j) {
        int i = this.count + 5;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 32));
        this.message[this.count + 1] = (byte) ((int) (j >>> 24));
        this.message[this.count + 2] = (byte) ((int) (j >>> 16));
        this.message[this.count + 3] = (byte) ((int) (j >>> 8));
        this.message[this.count + 4] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    /* renamed from: hQ */
    private void writeByteLong6(long j) {
        int i = this.count + 6;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 40));
        this.message[this.count + 1] = (byte) ((int) (j >>> 32));
        this.message[this.count + 2] = (byte) ((int) (j >>> 24));
        this.message[this.count + 3] = (byte) ((int) (j >>> 16));
        this.message[this.count + 4] = (byte) ((int) (j >>> 8));
        this.message[this.count + 5] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    /* renamed from: hR */
    private void writeByteLong7(long j) {
        int i = this.count + 7;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 48));
        this.message[this.count + 1] = (byte) ((int) (j >>> 40));
        this.message[this.count + 2] = (byte) ((int) (j >>> 32));
        this.message[this.count + 3] = (byte) ((int) (j >>> 24));
        this.message[this.count + 4] = (byte) ((int) (j >>> 16));
        this.message[this.count + 5] = (byte) ((int) (j >>> 8));
        this.message[this.count + 6] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    /* renamed from: hS */
    private final void writeByteLong8(long j) {
        int i = this.count + 8;
        ensure(i);
        this.message[this.count] = (byte) ((int) (j >>> 56));
        this.message[this.count + 1] = (byte) ((int) (j >>> 48));
        this.message[this.count + 2] = (byte) ((int) (j >>> 40));
        this.message[this.count + 3] = (byte) ((int) (j >>> 32));
        this.message[this.count + 4] = (byte) ((int) (j >>> 24));
        this.message[this.count + 5] = (byte) ((int) (j >>> 16));
        this.message[this.count + 6] = (byte) ((int) (j >>> 8));
        this.message[this.count + 7] = (byte) ((int) (j >>> 0));
        this.count = i;
    }

    public final void writeShort(int i) {
        boolean z;
        short s;
        int i2 = 0;
        short s2 = (short) i;
        if (s2 < 0) {
            s = (short) (-s2);
            z = true;
        } else {
            z = false;
            s = s2;
        }
        if (s > 255 || s < 0) {
            writeBits(1, 1);
            writeByteInt2(s2);
            return;
        }
        writeBits(1, 0);
        if (z) {
            i2 = 1;
        }
        writeBits(1, i2);
        writeByteInt(s);
    }

    public final void writeChar(int i) {
        int i2 = 65535 & i;
        if (i2 <= 255) {
            writeBits(1, 0);
            writeByteInt(i2);
            return;
        }
        writeBits(1, 1);
        writeByteInt2(i2);
    }


    public final void writeInt(int i) {
        int i2;
        boolean z;
        int i3 = 0;
        if (i < 0) {
            i2 = -i;
            z = true;
        } else {
            i2 = i;
            z = false;
        }
        if (i2 > 16777215 || i2 < 0) {
            writeBits(2, 3);
            writeByteInt4(i);
        } else if (i2 <= 255) {
            writeBits(2, 0);
            if (z) {
                i3 = 1;
            }
            writeBits(1, i3);
            writeByte(i2);
        } else if (i2 <= 65535) {
            writeBits(2, 1);
            if (z) {
                i3 = 1;
            }
            writeBits(1, i3);
            writeByteInt2(i2);
        } else {
            writeBits(2, 2);
            if (z) {
                i3 = 1;
            }
            writeBits(1, i3);
            writeByteInt3(i2);
        }
    }

    /* renamed from: sr */
    public final void writeByteAuto(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(String.valueOf(i) + "<0");
        } else if (i <= 255) {
            writeBits(2, 0);
            writeByteInt(i);
        } else if (i <= 65535) {
            writeBits(2, 1);
            writeByteInt2(i);
        } else if (i <= 16777215) {
            writeBits(2, 2);
            writeByteInt3(i);
        } else {
            writeBits(2, 3);
            writeByteInt4(i);
        }
    }


    public final void writeLong(long j) {
        long j2;
        boolean z;
        int i = 0;
        if (j < 0) {
            j2 = -j;
            z = true;
        } else {
            j2 = j;
            z = false;
        }
        if (j2 < 0) {
            writeBits(3, 7);
            writeByteLong8(j);
        } else if (j2 <= 255) {
            writeBits(3, 0);
            if (z) {
                i = 1;
            }
            writeBits(1, i);
            writeByteInt((byte) ((int) j2));
        } else if (j2 <= 65535) {
            writeBits(3, 1);
            if (z) {
                i = 1;
            }
            writeBits(1, i);
            writeByteInt2((short) ((int) j2));
        } else if (j2 <= 16777215) {
            writeBits(3, 2);
            if (z) {
                i = 1;
            }
            writeBits(1, i);
            writeByteInt3((int) j2);
        } else if (j2 <= -1) {
            writeBits(3, 3);
            if (z) {
                i = 1;
            }
            writeBits(1, i);
            writeByteInt4((int) j2);
        } else if (j2 <= 1099511627775L) {
            writeBits(3, 4);
            if (z) {
                i = 1;
            }
            writeBits(1, i);
            writeByteLong5(j2);
        } else if (j2 <= 281474976710655L) {
            writeBits(3, 5);
            if (z) {
                i = 1;
            }
            writeBits(1, i);
            writeByteLong6(j2);
        } else if (j2 <= 72057594037927935L) {
            writeBits(3, 6);
            if (z) {
                i = 1;
            }
            writeBits(1, i);
            writeByteLong7(j2);
        } else {
            writeBits(3, 7);
            writeByteLong8(j);
        }
    }

    public final void writeFloat(float f) {
        int i = (int) f;
        if (((float) i) != f || f == 0.0f) {
            int floatToRawIntBits = Float.floatToRawIntBits(f);
            int aT = FLOAT_TINY.mo8282aT(f);
            if (Float.floatToRawIntBits(FLOAT_TINY.mo8283cF(aT)) == floatToRawIntBits) {
                writeBits(3, 5);
                writeByteInt(aT);
                return;
            }
            int aT2 = FLOAT_DEF.mo8282aT(f);
            if (Float.floatToRawIntBits(FLOAT_DEF.mo8283cF(aT2)) == floatToRawIntBits) {
                writeBits(3, 6);
                writeByteInt2(aT2);
                return;
            }
            int aT3 = FLOAT_24BIT.mo8282aT(f);
            if (Float.floatToRawIntBits(FLOAT_24BIT.mo8283cF(aT3)) == floatToRawIntBits) {
                writeBits(3, 7);
                writeByteInt3(aT3);
                return;
            }
            writeBits(1, 0);
            writeByteInt4(floatToRawIntBits);
            return;
        }
        writeBits(3, 4);
        writeInt(i);
    }

    public final void writeDouble(double d) {
        float f = (float) d;
        if (((double) f) == d) {
            writeBits(1, 1);
            writeFloat(f);
            return;
        }
        writeBits(1, 0);
        writeByteLong8(Double.doubleToLongBits(d));
    }

    public final void writeBytes(String str) {
        int length = str.length();
        ensure(this.count + length);
        int i = this.count;
        int i2 = 0;
        while (i2 < length) {
            this.message[i] = (byte) str.charAt(i2);
            i2++;
            i++;
        }
        this.count = i;
    }

    public final void writeChars(String str) {
        int length = str.length();
        ensure(this.count + (length << 1));
        for (int i = 0; i < length; i++) {
            writeChar(str.charAt(i));
        }
    }

    public final void writeUTF(String str) throws UTFDataFormatException {
        int i;
        int i2;
        int length = str.length();
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            char charAt = str.charAt(i4);
            if (charAt <= 127) {
                i3++;
            } else if (charAt > 2047) {
                i3 += 3;
            } else {
                i3 += 2;
            }
        }
        if (i3 > 65535) {
            throw new UTFDataFormatException("encoded string too long: " + i3 + " bytes");
        }
        writeShort(i3);
        ensure(i3 + this.count);
        int i5 = this.count;
        int i6 = 0;
        while (true) {
            if (i6 >= length) {
                i = i6;
                break;
            }
            char charAt2 = str.charAt(i6);
            if (charAt2 > 127) {
                i = i6;
                break;
            }
            this.message[i5] = (byte) charAt2;
            i6++;
            i5++;
        }
        while (i < length) {
            char charAt3 = str.charAt(i);
            if (charAt3 <= 127) {
                i2 = i5 + 1;
                this.message[i5] = (byte) charAt3;
            } else if (charAt3 > 2047) {
                int i7 = i5 + 1;
                this.message[i5] = (byte) (((charAt3 >> 12) & 15) | KeyCode.ctb);
                int i8 = i7 + 1;
                this.message[i7] = (byte) (((charAt3 >> 6) & 63) | 128);
                i2 = i8 + 1;
                this.message[i8] = (byte) ((charAt3 & '?') | 128);
            } else {
                int i9 = i5 + 1;
                this.message[i5] = (byte) (((charAt3 >> 6) & 31) | 192);
                i2 = i9 + 1;
                this.message[i9] = (byte) ((charAt3 & '?') | 128);
            }
            i++;
            i5 = i2;
        }
        this.count = i5;
    }

    public final int getCount() {
        return this.count;
    }

    public final void writeTo(OutputStream outputStream) throws IOException {
        outputStream.write(this.message, 0, this.count);
    }

    public final byte[] getMessage() {
        return this.message;
    }

    public String toString() {
        return Syst.byteToStringLog(this.message, 0, this.message.length, " ", 16);
    }
}
