package game.engine;

import logic.sql.C5878acG;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.ks */
/* compiled from: a */
public abstract class FloatBit {
    /* renamed from: Ka */
    public abstract int mo8271Ka();

    /* renamed from: Kb */
    public abstract int mo8272Kb();

    /* renamed from: Kc */
    public abstract int mo8273Kc();

    /* renamed from: Kd */
    public abstract int mo8274Kd();

    /* renamed from: Ke */
    public abstract int mo8275Ke();

    /* renamed from: Kf */
    public abstract float mo8276Kf();

    /* renamed from: Kg */
    public abstract float mo8277Kg();

    /* renamed from: Kh */
    public abstract float mo8278Kh();

    /* renamed from: Ki */
    public abstract boolean mo8279Ki();

    /* renamed from: Kj */
    public abstract boolean mo8280Kj();

    /* renamed from: a */
    public abstract void mo8281a(ObjectOutput objectOutput, float f) throws IOException;

    /* renamed from: aT */
    public abstract int mo8282aT(float f);

    /* renamed from: cF */
    public abstract float mo8283cF(int i);

    /* renamed from: d */
    public abstract float mo8284d(ObjectInput objectInput) throws IOException;

    public String getName() {
        return getClass().getSimpleName();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getName()).append(": ");
        if (mo8279Ki()) {
            sb.append("1+");
        }
        sb.append(mo8275Ke()).append('+');
        sb.append(mo8274Kd());
        sb.append('=').append(mo8273Kc());
        sb.append(" Exp:[").append(mo8272Kb()).append(C5878acG.dBB).append(mo8271Ka()).append("] ");
        int Kc = mo8273Kc();
        if (mo8279Ki()) {
            sb.append('S');
            Kc--;
            if (Kc % 4 == 0) {
                sb.append(' ');
            }
        }
        int i = Kc;
        for (int i2 = 0; i2 < mo8275Ke(); i2++) {
            sb.append('E');
            i--;
            if (i % 4 == 0) {
                sb.append(' ');
            }
        }
        for (int i3 = 0; i3 < mo8274Kd(); i3++) {
            sb.append('M');
            i--;
            if (i % 4 == 0 && i > 0) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
}
