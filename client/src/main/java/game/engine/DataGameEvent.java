package game.engine;

import game.DataNetworkInformation;
import game.network.GameScriptClasses;
import game.network.ProxyTyp;
import game.network.Transport;
import game.network.WhoAmI;
import game.network.build.ManagerMessageTransportImpl;
import game.network.channel.NetworkChannel;
import game.network.channel.client.ClientConnect;
import game.network.exception.C1728ZX;
import game.network.exception.C2571gy;
import game.network.manager.C1431Uy;
import game.network.manager.C1810aL;
import game.network.message.Blocking;
import game.network.message.TransportCommand;
import game.network.message.externalizable.BlockingImpl;
import game.network.message.externalizable.C0275DY;
import game.network.message.externalizable.C6302akO;
import game.network.message.externalizable.ObjectIdImpl;
import game.network.message.serializable.C1695Yw;
import game.network.message.serializable.ReceivedTime;
import game.network.metric.MetricVector;
import logic.baa.C1616Xf;
import logic.baa.C6222aim;
import logic.baa.aDJ;
import logic.bbb.aDR;
import logic.data.mbean.C5439aJj;
import logic.obfuscation.read.ObfuscationReader;
import logic.res.FileControl;
import logic.res.code.C1625Xn;
import logic.res.html.C6663arL;
import logic.res.html.GameScriptClassesImpl;
import logic.res.html.axI;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import logic.ui.C3783v;
import org.apache.commons.logging.Log;
import p001a.*;
import taikodom.infra.enviroment.jmx.EnvironmentConsole;
import taikodom.render.loader.provider.FilePath;

import javax.management.InstanceAlreadyExistsException;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: a.Jz */
/* compiled from: a */
public class DataGameEvent implements C1412Uh, CurrentTimeMilli {
    private static final Log log = LogPrinter.setClass(DataGameEvent.class);
    public static boolean eKb;

    static {
        boolean z = false;
        eKb = false;
        String property = System.getProperty("Environment.DISABLE_PERSISTENCE");
        if (property != null && property.equalsIgnoreCase("true")) {
            z = true;
        }
        eKb = z;
    }

    public final long eKo;
    public final C0390FP eKw;
    private final AtomicLong nextId;
    private final WhoAmI whoAmI;
    public boolean disposed;
    public C6656arE eKt;
    public Collection<aDR> eKz;
    public ThreadLocal<DirectionTransportCommand> eKD;
    public Object[] eKI;
    public int eKc;
    public C1489Vv eKe;
    public boolean eKl;
    public boolean eKm;
    public ThreadPoolExecutor threadPoolExecutor;
    public boolean isWhoAmIClient;
    private C2913lj threadServerInfraCacheCleanup;
    private C6663arL eJV;
    private boolean eJW;
    private boolean eJX;
    private C1431Uy eJY;
    private C3783v<Class<C1616Xf>, C3582se> eJZ;
    private FilePath eKA;
    private C3828vU eKB;
    private Thread eKC;
    private C6280ajs eKE;
    private CurrentTimeMilli synchronizerClientTime;
    private SynchronizerServerTime synchronizerServerTime;
    private LinkedBlockingQueue<Runnable> eKH;
    private boolean eKJ;
    private boolean eKK;
    private Object eKa;
    private EnvironmentConsole eKd;
    private ArrayList<C2034bM<?>> eKf;
    private MetricVector eKg;
    private C0413Fm eKh;
    private boolean eKi;
    private ThreadLocal<List<Runnable>> eKj;
    private ReadWriteLock eKk;
    private azC eKn;
    private C2844kw eKp;
    private C6663arL eKs;
    private WraperDbFile eKu;
    private Class<? extends C1616Xf> eKv;
    private ManagerMessageTransportImpl eKx;
    private C1625Xn eKy;
    private boolean initialized;
    /* renamed from: qU */
    private ClientConnect f918qU;

    public DataGameEvent(C0390FP fp) {
        this(fp, true);
    }

    public DataGameEvent(C0390FP fp, boolean z) {
        this.nextId = new AtomicLong(1);
        this.eJW = true;
        this.eJX = false;
        this.eJY = new C1431Uy(this);
        this.eJZ = new C3783v<>();
        this.eKc = 0;
        this.eKe = new C1489Vv();
        this.eKf = null;
        this.eKh = new C0413Fm(this);
        this.eKi = true;
        this.eKj = new ThreadLocal<>();
        this.eKk = new ReentrantReadWriteLock(true);
        this.eKl = true;
        this.eKn = new azC();
        this.disposed = false;
        this.eKy = new C5506aLy(this);
        this.initialized = false;
        this.eKz = new CopyOnWriteArrayList();
        this.eKD = new ThreadLocal<>();
        this.synchronizerClientTime = SingletonCurrentTimeMilliImpl.CURRENT_TIME_MILLI;
        this.synchronizerServerTime = new SynchronizerServerTime();
        this.eKI = new Object[1];
        this.eKJ = true;
        this.eKK = true;
        this.eKw = fp;
        this.whoAmI = fp.getWhoAmI();
        this.eKo = System.currentTimeMillis();
        this.eKA = fp.aQe();
        if (this.eKA == null) {
            this.eKA = new FileControl(C0891Mw.DATA);
        }
        if (z) {
            init();
        }
    }

    /* renamed from: aa */
    public static void m6166aa(Object obj) {
    }

    public FilePath aQe() {
        return this.eKA;
    }

    public void init() {
        boolean z;
        boolean isWhoAmiClient;
        boolean z3 = true;
        if (this.initialized) {
            throw new IllegalStateException("Environment already initialized");
        }
        this.initialized = true;
        log.info("Initing Environment.");
        NetworkChannel networkChannel = this.eKw.aPT();
        Class<?> aPV = this.eKw.aPV();
        boolean aPY = this.eKw.aPY();
        Object aPW = this.eKw.aPW();
        boolean aPX = this.eKw.aPX();
        this.eKi = this.eKw.aPZ();
        this.eKa = aPW;
        this.eKv = aPV;
        finalizingEnvironmentSetupAndRebuildingClassCache();
        boolean z4 = getWhoAmI() == WhoAmI.SERVER;
        if (getClass().getName().startsWith("a")) {
            z = false;
        } else {
            z = true;
        }
        this.eKm = z4 | z;
        if (this.whoAmI == WhoAmI.SERVER) {
            this.threadServerInfraCacheCleanup = new BuildThreadServerInfraCacheCleanup(this);
            registerTransactions();
            if (this.eKw.atf()) {
                log.info("Ensure loaded");
                bGH().aiL();
                log.info("Ensure done");
            }
        } else {
            this.threadServerInfraCacheCleanup = new BuildThreadServerInfraCacheCleanup(this);
            if (this.eKw.atf()) {
                log.info("Ensure loaded");
                bGH().aiL();
                log.info("Ensure done");
            }
            this.nextId.set(9223372036853727232L);
        }
        try {
            File file = new File(System.getProperty("taikodom.obmap", "ob-client.map"));
            if (file.exists()) {
                log.info("Using obfuscation map: " + file.getAbsolutePath());
                this.eKt.mo12005B(new ObfuscationReader(new FileInputStream(file)).getMap());
            } else if (this.whoAmI == WhoAmI.SERVER) {
                //Карта обфускации не найдена, используйте -Dtaikodom.obmap = ob_map_file
                log.warn("Obfuscation map not found, use -Dtaikodom.obmap=ob_map_file");
            }
        } catch (FileNotFoundException e) {
            log.warn("Error reading obfuscation map", e);
        } catch (IOException e2) {
            log.warn("Error reading obfuscation map", e2);
        }
        for (String next : bGH().aXV()) {
            this.eKt.mo12016jM(next);
            this.eKt.mo12016jM("[L" + next + ";");
            this.eKt.mo12016jM("[[L" + next + ";");
        }
        if (getWhoAmI() == WhoAmI.CLIENT) {
            isWhoAmiClient = true;
        } else {
            isWhoAmiClient = false;
        }
        this.isWhoAmIClient = isWhoAmiClient;
        int max = Math.max(5, Runtime.getRuntime().availableProcessors() * 2);
        this.eKH = new LinkedBlockingQueue<>(max * 20000);
        int i = max * 10;
        TimeUnit timeUnit = TimeUnit.SECONDS;
        LinkedBlockingQueue<Runnable> linkedBlockingQueue = this.eKH;
        if (this.isWhoAmIClient) {
            z3 = false;
        }
        this.threadPoolExecutor = PoolThread.newFixedThreadPool(max, i, 60, timeUnit, linkedBlockingQueue, "Command Executer", z3);
        log.info("Setup transport manager.");
        if (this.eKw.aQd()) {
            setupNetworkTransportLayer(networkChannel);//ea$c: waiting...
        }


        if (this.whoAmI != WhoAmI.SERVER || !networkChannel.isListenerPort()) {
            log.info("Resolving rt.");
            this.eJV = resolvingTaikodomRootObject().bFf();
            log.info("Pull.");
            if (this.whoAmI == WhoAmI.SERVER) {
                ((C1298TD) this.eJV).mo5609ds();
            }
            log.info("Rt resolved.");
        } else {
            if (this.eKw.aPZ() || this.eKw.aPY()) {
                WraperDbFile aQh = this.eKw.aQh();
                this.eKu = aQh;
                if (aQh == null) {
                    this.eKu = new C1185RX(this, "Transaction log flusher");
                }
                this.eKu.mo5234f(this.eKA);
                this.eKu.mo5219a(this);
                try {
                    this.eKu.init();//Opening repository: .\data\environment
                } catch (IOException e3) {
                    throw new RuntimeException(e3);
                }
            }
            try {
                m6165a(aPV, aPY);
                if (aPX) {
                    try {
                        bGp();
                        return;
                    } catch (IOException e4) {
                        throw new RuntimeException(e4);
                    }
                } else {
                    this.eKp = new C6393amB(this);
                    this.eKp.start();
                }
            } catch (Exception e5) {
                throw new RuntimeException(e5);
            }
        }


        if (this.whoAmI == WhoAmI.CLIENT) {
            initializingClient();
        } else {
            if (this.eKw.aQd()) {
                bGm();
            }
            this.eJW = false;
            if (this.eJV != null) {
                aMr();
            }
        }
        this.eJW = false;
        if (this.whoAmI == WhoAmI.SERVER && this.eKw.aPZ()) {
            this.eKu.start();
        }
    }

    /* access modifiers changed from: protected */
    public void aMr() {
    }

    private void bGm() {
        bGU().mo15544a((C1810aL) new C0702b());
    }

    /* access modifiers changed from: protected */
    public void initializingClient() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void setupNetworkTransportLayer(NetworkChannel ahg) {
        this.eKx = new ManagerMessageTransportImpl(this, ahg);
    }

    /* access modifiers changed from: protected */
    public void finalizingEnvironmentSetupAndRebuildingClassCache() {
        this.eKt = new C2696ij();
        this.eKt.mo12009at(this.eKv);
    }

    public boolean bGn() {
        return this.isWhoAmIClient;
    }

    /* renamed from: dI */
    public void mo3427dI(boolean z) {
        this.isWhoAmIClient = z;
    }

    public final boolean bGo() {
        return this.eJW;
    }

    /* renamed from: a */
    private void m6165a(Class<?> cls, boolean z) {
        if (getWhoAmI() != WhoAmI.SERVER) {
            //Только сервер может воскресить данные
            throw new RuntimeException("Only the server can resurrect data");
        }
        if (z) {
            try {
                m6164O(cls);
            } catch (Exception e) {
                //Проблемы с чтением сохраненных данных
                throw new RuntimeException("Problems reading persisted data", e);
            }
        }
        if (this.eJV == null && this.eKw.aQc()) {
            try {
                this.eJV = mo3342a(mo3336P(cls), 1);
                long nanoTime = System.nanoTime();
                ayN();
                try {
                    this.eJV.mo6901yn().bFg();
                    mo3442ge(System.nanoTime() - nanoTime);
                    if (aQh() != null) {
                        aQh().mo5238i((C3582se) this.eJV);
                        aQh().bOc();
                    }
                } catch (Exception e2) {
                    try {
                        mo3441gd(System.nanoTime() - nanoTime);
                    } catch (Exception e3) {
                        log.error(e3);
                        log.error(e2);
                    }
                    throw new CountFailedReadOrWriteException("Error creating root", e2);
                }
            } catch (Exception e4) {
                throw new RuntimeException(e4);
            }
        }
    }

    /* renamed from: O */
    private void m6164O(Class<?> cls) {
        this.eJV = mo3425d(cls, 1);
        if (!this.eKu.bOd()) {
            this.eJV.mo6901yn().bFg();
            if (this.eKu != null) {
                this.eKu.mo5238i((C3582se) this.eJV);
                return;
            }
            return;
        }
        if (this.eKw.aQa()) {
            try {
                this.eKu.bOe();
            } catch (Throwable th) {
                throw new RuntimeException("Error reading transaction log.", th);
            }
        }
        System.gc();
        System.gc();
        //Проверка работоспособности репозитория ...
        log.info("Checking repository sanity... ");
        for (C3582se next : this.threadServerInfraCacheCleanup) {
            if (next.bFT() && System.getProperty("fill-hollow-hack") != null) {
                next.cVj().mo3146A(false);
                //Заполнение полого объекта
                log.error("Filling up hollow object " + next.bFY());
            }
        }
    }

    public void ayN() {
        aWq dDA = aWq.dDA();
        if (dDA != null) {
            dDA.dDR();
            return;
        }
        try {
            bGs();
            aWq.m19285b(currentTimeMillis(), (aDR) null, (C0495Gr) null);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: gd */
    public void mo3441gd(long j) {
        aWq dDA = aWq.dDA();
        if (dDA != null) {
            if (dDA.dDS() < 0) {
                dDA.dispose();
            } else {
                return;
            }
        }
        mo3445gf(j);
    }

    /* renamed from: ge */
    public void mo3442ge(long j) {
        aWq dDA = aWq.dDA();
        if (dDA == null || dDA.dDS() < 0) {
            try {
                if (dDA.mo12081g(this) == -1) {
                    new RuntimeException();
                }
                if (dDA.dDF() != null && dDA.dDF().size() > 0) {
                    bGU().mo15541C(dDA.dDF());
                }
                dDA.dispose();
                mo3445gf(j);
            } catch (C6510aoO e) {
                dDA.dDO();
                throw e;
            } catch (InterruptedException e2) {
                dDA.dDO();
                throw new RuntimeException(e2);
            } catch (Throwable th) {
                dDA.dispose();
                mo3445gf(j);
                throw th;
            }
        }
    }

    public void bGp() {
        //Сохранение начинается
        log.info("Persist is beginning");
        if (getWhoAmI() != WhoAmI.SERVER) {
            //Только сервер может сохранять данные
            throw new RuntimeException("Only the server can persist data");
        }
        try {
            bGt();
            bGq();
        } catch (InterruptedException e) {
            //Прервано при ожидании семафора для моментального снимка
            log.warn("Interrupted while waiting the semaphore for snapshot");
        } finally {
            bGu();
        }
    }

    private boolean bGq() {
        return this.eKu.bOc();
    }

    public azC bGr() {
        return this.eKn;
    }

    public void bGs() {
        if (this.eKw.aPZ()) {
            azC azc = this.eKn;
            azc.han.incrementAndGet();
            azc.haq.incrementAndGet();
            this.eKk.readLock().lock();
        }
    }

    /* renamed from: gf */
    public void mo3445gf(long j) {
        if (this.eKw.aPZ()) {
            this.eKk.readLock().unlock();
            azC azc = this.eKn;
            if (bHd()) {
                azc.ham.addAndGet(j);
                azc.har.addAndGet(j);
                azc.has.addAndGet(j * j);
                if (j < azc.hau.get()) {
                    azc.hau.set(j);
                }
                if (j > azc.hav.get()) {
                    azc.hav.set(j);
                }
            }
        }
    }

    public void bGt() {
        this.eKk.writeLock().lock();
    }

    public void bGu() {
        this.eKk.writeLock().unlock();
    }

    public final WhoAmI getWhoAmI() {
        return this.whoAmI;
    }

    public boolean bGw() {
        return getWhoAmI() == WhoAmI.CLIENT && this.eKj.get() != null;
    }

    /* renamed from: f */
    public void mo3438f(Runnable runnable) {
        this.eKj.get().add(runnable);
    }

    public void bGx() {
        this.eKj.set(new ArrayList());
    }

    public void bGy() {
        List<Runnable> list = this.eKj.get();
        if (list != null) {
            try {
                for (Runnable run : list) {
                    try {
                        run.run();
                    } catch (Exception e) {
                        //Исключение при выполнении вызова из очереди приложения
                        log.warn("Exception caught while executing an app queued call", e);
                    }
                }
            } catch (Exception e2) {
                //Исключение при выполнении вызовов из очереди приложения
                log.warn("Exception caught while executing an app queued calls", e2);
            }
            this.eKj.set((Object) null);
            return;
        }
        //не следует вызывать endQueueApplicationCalls без предварительного вызова beginQueueApplicationCalls
        log.warn("should not call endQueueApplicationCalls without first calling beginQueueApplicationCalls");
    }

    /* access modifiers changed from: package-private */
    public void exit(int i) {
        mo3335HY();
        System.exit(i);
    }

    /* renamed from: a */
    public C3582se mo3343a(ObjectId apo) {
        C3582se a = this.threadServerInfraCacheCleanup.mo15459a(apo);
        if (a != null) {
            return a;
        }
        throw new RuntimeException("Unknown object id: " + apo.getId());
    }

    /* renamed from: c */
    public C3582se mo3423c(ObjectId apo) {
        return this.threadServerInfraCacheCleanup.mo15459a(apo);
    }

    /* renamed from: c */
    public C1616Xf mo3421c(int i, long j) {
        C3582se a;
        ProxyTyp bh;
        synchronized (this.threadServerInfraCacheCleanup) {
            ObjectId apo = new ObjectId(j);
            a = this.threadServerInfraCacheCleanup.mo15459a(apo);
            if (a == null) {
                Class<?> tS = this.eKt.mo12017tS(i);
                if (tS == null) {
                    //Недействительный магический номер класса
                    throw new IllegalArgumentException("Invalid class magic number " + i);
                }
                if (getWhoAmI() == WhoAmI.SERVER) {
                    bh = ProxyTyp.AUTHORITY;
                } else {
                    bh = ProxyTyp.CLIENT_PROXY;
                }
                a = mo3344a((Class<? extends C1616Xf>) tS, apo, bh);
            }
        }
        return a.mo6901yn();
    }

    /* renamed from: c */
    public C1616Xf mo3422c(Class cls, long j) {
        C3582se a;
        ProxyTyp proxyTyp;
        synchronized (this.threadServerInfraCacheCleanup) {
            ObjectId apo = new ObjectId(j);
            a = this.threadServerInfraCacheCleanup.mo15459a(apo);
            if (a == null) {
                if (getWhoAmI() == WhoAmI.SERVER) {
                    proxyTyp = ProxyTyp.AUTHORITY;
                } else {
                    proxyTyp = ProxyTyp.CLIENT_PROXY;
                }
                a = mo3344a((Class<? extends C1616Xf>) cls, apo, proxyTyp);
                a.mo22000iY(true);
            }
        }
        return a.mo6901yn();
    }

    /* renamed from: j */
    public C3582se mo3451j(C1616Xf xf) {
        return (C3582se) xf.bFf();
    }

    /* renamed from: a */
    public C3582se mo3344a(Class<? extends C1616Xf> cls, ObjectId apo, ProxyTyp bh) {
        C3582se td;
        if (bGZ()) {
            td = new C1875af();
        } else {
            td = new C1298TD();
        }
        try {
            td.mo21981c(this);
            td.mo21971a(cls, mo3336P(cls));
            td.mo21995d(apo);
            td.mo21967a(bh);
            this.threadServerInfraCacheCleanup.mo15460a(td);
            td.mo13226dt();
            return td;
        } catch (Exception e) {
            throw new RuntimeException("Error creating " + cls + ":" + apo.getId(), e);
        }
    }

    /* renamed from: e */
    public void mo3434e(C3582se seVar) {
        this.threadServerInfraCacheCleanup.mo15461b(seVar);
    }

    /* renamed from: a */
    public void mo3356a(Class<? extends C1616Xf> cls, C3582se seVar) {
        this.threadServerInfraCacheCleanup.mo15460a(seVar);
    }

    /* access modifiers changed from: protected */
    public <T extends C1616Xf> T resolvingTaikodomRootObject() {
        try {
            BlockingImpl zw = new BlockingImpl();
            zw.setBlocking(true);
            TransportCommand transportCommand = new TransportCommand();
            this.eKx.addSerializeDataReceivedTime((Transport) transportCommand, (ReceivedTime) zw);
            return mo3344a(
                this.eKv,
                ((ObjectIdImpl) ManagerMessageTransportImpl.messageTransformation(this, (Transport) bGU().cxz().sendTransportMessageAndWaitForResponse((ClientConnect) null, transportCommand)))
                    .getObjectId(),
                getWhoAmI() == WhoAmI.SERVER ? ProxyTyp.SERVER_PROXY : ProxyTyp.CLIENT_PROXY)
                .mo6901yn();
        } catch (C1728ZX e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public Object mo3345a(GameScriptClasses aec) {
        if (aec instanceof GameScriptClassesImpl) {
            GameScriptClassesImpl fv = (GameScriptClassesImpl) aec;
            try {
                return mo3421c(fv.aPv(), fv.mo2229Ej());
            } catch (Exception e) {
                throw new C2571gy(e, "Error deserializing game.script: " + fv.mo2229Ej());
            }
        } else {
            throw new RuntimeException("Dont know how to handle " + aec);
        }
    }

    /* renamed from: a */
    public Object mo3346a(DataNetworkInformation amr) {
        if (amr instanceof C1616Xf) {
            C6663arL bFf = ((C1616Xf) amr).bFf();
            if (this.whoAmI == WhoAmI.CLIENT && bFf.bFS()) {
                //Вы пытаетесь отправить экземпляр @@ id = 23 на стороне клиента на сервер.
                throw new IllegalStateException("You are trying to send a " + bFf.bFU().getName() + " id=" + bFf.getObjectId().getId() + " instanced on client side, to the server.");
            } else if (bFf != null) {
                return bFf.mo6888dv();
            } else {
                log.error("SERIOUS ERROR!!! null infra!");
                log.error("null infra for " + amr.getClass());
                return amr;
            }
        } else {
            throw new RuntimeException("Dont know how to handle " + amr);
        }
    }

    /* renamed from: k */
    public C3582se mo3453k(C1616Xf xf) {
        return mo3342a(xf, this.nextId.incrementAndGet());
    }

    /* renamed from: d */
    public C3582se mo3425d(Class<?> cls, long j) {
        return mo3342a(mo3336P(cls), j);
    }

    /* renamed from: P */
    public C1616Xf mo3336P(Class<?> cls) {
        try {
            Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{C5540aNg.class});
            declaredConstructor.setAccessible(true);
            return (C1616Xf) declaredConstructor.newInstance(this.eKI);
        } catch (Exception e) {
            throw new RuntimeException("Class to be instantiated " + cls, e);
        }
    }

    /* renamed from: Q */
    public C3582se mo3337Q(Class<?> cls) {
        return mo3453k(mo3336P(cls));
    }

    /* renamed from: a */
    public C3582se mo3342a(C1616Xf xf, long j) {
        ObjectId apo = new ObjectId(j);
        C3582se afVar = bGZ() ? new C1875af() : new C1298TD();
        afVar.mo21981c(this);
        afVar.mo21971a((Class<? extends C1616Xf>) xf.getClass(), xf);
        afVar.mo21995d(apo);
        afVar.mo22000iY(false);
        if (!this.threadServerInfraCacheCleanup.mo15463c(afVar)) {
            throw new RuntimeException("Given OID is already in use: " + j);
        }
        m6167gg(j);
        afVar.mo13226dt();
        afVar.mo21967a(ProxyTyp.AUTHORITY);
        return afVar;
    }

    /* renamed from: gg */
    private void m6167gg(long j) {
        long j2;
        if (this.whoAmI != WhoAmI.CLIENT) {
            do {
                j2 = this.nextId.get();
            } while (!this.nextId.compareAndSet(j2, Math.max(1 + j, j2)));
        }
    }

    /* renamed from: R */
    public C3582se mo3338R(Class<? extends C1616Xf> cls) {
        return mo3433e(cls, this.nextId.incrementAndGet());
    }

    /* renamed from: S */
    public aDJ mo3339S(Class<? extends aDJ> cls) {
        return (aDJ) mo3433e(cls, this.nextId.incrementAndGet()).mo6901yn();
    }

    /* renamed from: T */
    public C1616Xf mo3340T(Class<? extends aDJ> cls) {
        if (getWhoAmI() == WhoAmI.CLIENT && cls.getAnnotation(C5566aOg.class) != null && cls.getAnnotation(ClientOnly.class) == null && cls.getAnnotation(C0909NL.class) == null) {
            throw new IllegalAccessError("Clients can only create game.script objects annotated with @ClientOnly");
        } else if (getWhoAmI() != WhoAmI.SERVER || cls.getAnnotation(ClientOnly.class) == null) {
            try {
                return mo3425d(cls, this.nextId.incrementAndGet()).mo6901yn();
            } catch (SecurityException e) {
                throw new RuntimeException(e);
            } catch (IllegalArgumentException e2) {
                throw new RuntimeException(e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException(e3);
            } catch (IllegalAccessException e4) {
                throw new RuntimeException(e4);
            }
        } else {
            throw new IllegalAccessError("Server can not create game.script objects annotated with @ClientOnly");
        }
    }

    /* renamed from: e */
    public C3582se mo3433e(Class<? extends C1616Xf> cls, long j) {
        if (getWhoAmI() == WhoAmI.CLIENT && cls.getAnnotation(ClientOnly.class) == null) {
            throw new IllegalAccessError("Clients can only create game.script objects annotated with @ClientOnly");
        } else if (getWhoAmI() != WhoAmI.SERVER || cls.getAnnotation(ClientOnly.class) == null) {
            try {
                C3582se d = mo3425d(cls, j);
                d.mo6901yn().bFg();
                return d;
            } catch (SecurityException e) {
                throw new RuntimeException(e);
            } catch (IllegalArgumentException e2) {
                throw new RuntimeException(e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException(e3);
            } catch (IllegalAccessException e4) {
                throw new RuntimeException(e4);
            }
        } else {
            throw new IllegalAccessError("Server can not create game.script objects annotated with @ClientOnly");
        }
    }

    public C6663arL bGz() {
        return this.eJV;
    }

    public C2913lj bGA() {
        return this.threadServerInfraCacheCleanup;
    }

    public void step(float f) {
        this.eKx.mo15561gZ();
    }

    /* renamed from: b */
    public void mo3366b(float f, Executor executor) {
        this.eKx.mo15550b(executor);
    }

    /* renamed from: a */
    public aDR mo3341a(aDR adr, C1616Xf xf, boolean z) {
        return this.eKe.mo6438a(adr, mo3451j(xf), z);
    }

    /* renamed from: i */
    public void mo3449i(ClientConnect bVar) {
        try {
            aDR g = this.eKe.mo6443g(bVar);
            if (g != null && g.cWs()) {
                mo3448i(g);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public SynchronizerServerTime getSynchronizerServerTime() {
        return this.synchronizerServerTime;
    }

    /* renamed from: a */
    public void setSynchronizerServerTime(SynchronizerServerTime hr) {
        this.synchronizerServerTime = hr;
    }

    /* access modifiers changed from: package-private */
    public DirectionTransportCommand bGC() {
        if (this.eKD.get() == null) {
            this.eKD.set(new DirectionTransportCommand());
        }
        return this.eKD.get();
    }

    public ClientConnect bGD() {
        return bGC().hmQ;
    }

    public C6663arL bGE() {
        aDR f = this.eKe.mo6442f(bGD());
        if (f == null) {
            return null;
        }
        return f.hDb;
    }

    public aDR bGF() {
        return this.eKe.mo6442f(bGD());
    }

    public long bGG() {
        if (bGC() == null || bGC().gqU == null) {
            return 0;
        }
        return bGC().gqU.getTimeReadMesage();
    }

    public C6280ajs bGH() {
        if (this.eKE == null) {
            this.eKE = this.eKw.ate();
        }
        return this.eKE;
    }

    /* renamed from: HY */
    public void mo3335HY() {
        this.disposed = true;
        this.threadPoolExecutor.shutdown();
        if (this.eKx != null) {
            this.eKx.mo15543HY();
        }
        if (this.eKp != null) {
            this.eKp.mo14757HY();
        }
        this.eJY.dispose();
        bGA().dispose();
        if (getWhoAmI() == WhoAmI.SERVER && this.eKu != null) {
            try {
                this.eKu.mo5218HY();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        if (this.eKC != null) {
            this.eKC.interrupt();
        }
        this.eKC = null;
    }

    /* renamed from: ab */
    public C3582se mo3361ab(Object obj) {
        return mo3451j((C1616Xf) obj);
    }

    /* renamed from: a */
    public final void mo3348a(C1695Yw yw) {
        if (getWhoAmI() != WhoAmI.SERVER) {
            throw new RuntimeException("Only the server can add transaction log");
        } else if (this.disposed) {
            log.error("Environment is disposed. Can't push transactions");
        } else {
            this.eKn.haw.addAndGet((long) (yw.bHF() + yw.bHE()));
            if (!eKb && this.eKi) {
                try {
                    this.eKu.mo5226c(yw);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public C6663arL bGI() {
        if (getWhoAmI() == WhoAmI.CLIENT) {
            return this.eKs;
        }
        throw new IllegalAccessError("Client only method!");
    }

    public C1489Vv bGJ() {
        return this.eKe;
    }

    public C1625Xn bGK() {
        return this.eKy;
    }

    /* renamed from: b */
    public void mo3367b(C1625Xn xn) {
        this.eKy = xn;
    }

    /* renamed from: a */
    public void mo3352a(C2034bM<?> bMVar) {
        if (this.eKf == null) {
            this.eKf = new ArrayList<>();
        }
        synchronized (this.eKf) {
            this.eKf.add(bMVar);
        }
    }

    /* renamed from: b */
    public void mo3369b(C2034bM<?> bMVar) {
        if (this.eKf != null) {
            synchronized (this.eKf) {
                this.eKf.remove(bMVar);
            }
        }
    }

    /* renamed from: a */
    public void mo3354a(C3582se seVar, C5439aJj ajj) {
        List<C2034bM> list;
        if (this.eKf != null) {
            synchronized (this.eKf) {
                list = (List) this.eKf.clone();
            }
            for (C2034bM a : list) {
                a.mo17264a(seVar.mo6901yn(), ajj);
            }
        }
    }

    public boolean bGL() {
        return this.eKf != null && this.eKf.size() > 0;
    }

    public final boolean bGM() {
        return this.eJX;
    }

    /* renamed from: dJ */
    public void mo3428dJ(boolean z) {
        this.eJX = z;
    }

    public MetricVector bGN() {
        return this.eKg;
    }

    /* renamed from: a */
    public void mo3355a(MetricVector szVar) {
        this.eKg = szVar;
    }

    /* renamed from: f */
    public void mo3437f(C3582se seVar) {
        mo3434e(seVar);
    }

    public void bGO() {
        for (C3582se next : this.threadServerInfraCacheCleanup) {
            long nanoTime = System.nanoTime();
            ayN();
            try {
                m6168n((aDJ) next.mo6901yn());
                mo3442ge(System.nanoTime() - nanoTime);
            } catch (RuntimeException e) {
                e.printStackTrace();
                mo3441gd(nanoTime);
                throw e;
            }
        }
    }

    /* renamed from: n */
    private void m6168n(aDJ adj) {
        adj.__ressurectCalled = false;
        try {
            adj.mo70aH();
            if (!adj.__ressurectCalled) {
                log.warn("onRessurect call did not arrive to ScriptObject, please check the class hierarquy of " + adj.getClass().getName() + ", the onRessurect method must call super.onRessurect");
            }
        } catch (Exception e) {
            log.warn("Problems running onRessurect", e);
        }
    }

    public C6656arE bxy() {
        return this.eKt;
    }

    public final long currentTimeMillis() {
        return this.synchronizerServerTime.currentTimeMillis();
    }

    private void registerTransactions() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            this.eKd = new EnvironmentConsole(this);
            try {
                platformMBeanServer.registerMBean(this.eKd, new ObjectName("Bitverse:name=Transactions"));
            } catch (InstanceAlreadyExistsException e) {
                System.err.println(e.getMessage());
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public Object aPW() {
        return this.eKa;
    }

    /* renamed from: ac */
    public void mo3362ac(Object obj) {
        this.eKa = obj;
    }

    public int getScriptObjectCount() {
        return this.threadServerInfraCacheCleanup.size();
    }

    public EnvironmentConsole bGP() {
        return this.eKd;
    }

    public void bGQ() {
        this.eKu.bOf();
    }

    public C1431Uy bGR() {
        return this.eJY;
    }

    /* renamed from: a */
    public void mo3353a(BuildThreadServerInfraCacheCleanup.C2200a aVar) {
        if (getWhoAmI() == WhoAmI.CLIENT && aVar.mo17666hD().getAnnotation(C0909NL.class) == null && !aVar.mo17668hF()) {
            this.eKx.mo15549b(this.eJV.mo6892hE(), (Blocking) new C0275DY(aVar.mo17665hC().getId(), aVar.mo17666hD()));
        }
    }

    public C0413Fm bGS() {
        return this.eKh;
    }

    /* renamed from: d */
    public void mo3426d(C0413Fm fm) {
        this.eKh = fm;
    }

    /* renamed from: dK */
    public void mo3429dK(boolean z) {
        this.eKi = z;
    }

    public C6280ajs ate() {
        return this.eKw.ate();
    }

    public long getNextId() {
        return this.nextId.get();
    }

    /* renamed from: a */
    public void mo3351a(WraperDbFile aqc) {
        this.eKu = aqc;
    }

    public WraperDbFile bGT() {
        return this.eKu;
    }

    public ManagerMessageTransportImpl bGU() {
        return this.eKx;
    }

    /* renamed from: b */
    public void mo3370b(Thread thread) {
        this.eKC = thread;
    }

    public void bGV() {
        try {
            bGU().mo15551c((ClientConnect) null, new C0701a());
        } catch (axI e) {
            throw new RuntimeException(e);
        } catch (C1728ZX e2) {
            throw new RuntimeException(e2);
        }
    }

    /* renamed from: l */
    public Collection<aDR> mo3454l(C1616Xf xf) {
        if (xf instanceof C6222aim) {
            return ((C6222aim) xf).aae();
        }
        return this.eKz;
    }

    public CurrentTimeMilli getSynchronizerTime() {
        if (this.whoAmI != WhoAmI.SERVER) {
            return this.synchronizerClientTime;
        }
        return this.synchronizerServerTime;
    }

    /* renamed from: b */
    public void setSynchronizerClientTime(CurrentTimeMilli ahw) {
        this.synchronizerClientTime = ahw;
    }

    @Deprecated
    /* renamed from: eP */
    public void mo3435eP(long j) {
        this.synchronizerServerTime.mo2678eP(j);
    }

    @Deprecated
    /* renamed from: dL */
    public void mo3430dL(boolean z) {
        this.synchronizerServerTime.mo2676cN(z);
    }

    public boolean bGX() {
        return getWhoAmI() == WhoAmI.CLIENT && !this.eKw.aQb();
    }

    public boolean bGY() {
        return getWhoAmI() == WhoAmI.CLIENT && this.eKw.aQb();
    }

    public boolean bGZ() {
        return getWhoAmI() == WhoAmI.SERVER;
    }

    public boolean bHa() {
        return bGZ();
    }

    public boolean bHb() {
        return getWhoAmI() == WhoAmI.CLIENT;
    }

    /* renamed from: hE */
    public ClientConnect mo3447hE() {
        return this.f918qU;
    }

    /* renamed from: j */
    public void mo3452j(ClientConnect bVar) {
        this.f918qU = bVar;
    }

    /* renamed from: c */
    public void mo3424c(C6663arL arl) {
        this.eKs = arl;
    }

    /* renamed from: a */
    public void mo3350a(ManagerMessageTransportImpl aqi) {
        this.eKx = aqi;
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo3448i(aDR adr) {
        this.eKz.remove(adr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo3446h(aDR adr) {
        adr.mo8391a(aDR.C1780a.OffloadNode);
        this.eKz.clear();
        this.eKz.add(adr);
    }

    public C3828vU bHc() {
        return this.eKB;
    }

    public boolean bHd() {
        return this.eKK;
    }

    public boolean bHe() {
        return this.eKJ;
    }

    /* renamed from: dM */
    public void mo3431dM(boolean z) {
        this.eKK = z;
    }

    /* renamed from: dN */
    public void mo3432dN(boolean z) {
        this.eKJ = z;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object mo3365b(java.lang.Class<?> r5, long r6) {
        /*
            r4 = this;
            a.lj r2 = r4.eJU
            monitor-enter(r2)
            a.apO r1 = new a.apO     // Catch:{ all -> 0x004a }
            r1.<init>(r6)     // Catch:{ all -> 0x004a }
            a.lj r0 = r4.eJU     // Catch:{ all -> 0x004a }
            a.se r0 = r0.mo15459a((p001a.C6562apO) r1)     // Catch:{ all -> 0x004a }
            if (r0 != 0) goto L_0x005e
            boolean r0 = r4.bGZ()     // Catch:{ all -> 0x004a }
            if (r0 == 0) goto L_0x0044
            a.af r0 = new a.af     // Catch:{ all -> 0x004a }
            r0.<init>()     // Catch:{ all -> 0x004a }
        L_0x001b:
            r0.mo21981c((game.engine.C0700Jz) r4)     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            a.Xf r3 = r4.mo3336P(r5)     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            r0.mo21971a((java.lang.Class<? extends logic.baa.C1616Xf>) r5, (logic.baa.C1616Xf) r3)     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            r0.mo21995d((p001a.C6562apO) r1)     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            a.Lt r1 = r4.bGv()     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            a.Lt r3 = p001a.C0824Lt.SERVER     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            if (r1 != r3) goto L_0x004d
            a.BH r1 = p001a.C0101BH.AUTHORITY     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
        L_0x0032:
            r0.mo21967a((p001a.C0101BH) r1)     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            a.lj r1 = r4.eJU     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            r1.mo15460a((p001a.C3582se) r0)     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            r1 = 1
            r0.mo22000iY(r1)     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            a.Xf r0 = r0.mo6901yn()     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            monitor-exit(r2)     // Catch:{ all -> 0x004a }
        L_0x0043:
            return r0
        L_0x0044:
            a.TD r0 = new a.TD     // Catch:{ all -> 0x004a }
            r0.<init>()     // Catch:{ all -> 0x004a }
            goto L_0x001b
        L_0x004a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x004a }
            throw r0
        L_0x004d:
            a.BH r1 = p001a.C0101BH.CLIENT_PROXY     // Catch:{ InstantiationException -> 0x0050, IllegalAccessException -> 0x0057 }
            goto L_0x0032
        L_0x0050:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x004a }
            r1.<init>(r0)     // Catch:{ all -> 0x004a }
            throw r1     // Catch:{ all -> 0x004a }
        L_0x0057:
            r0 = move-exception
            java.lang.RuntimeException r1 = new java.lang.RuntimeException     // Catch:{ all -> 0x004a }
            r1.<init>(r0)     // Catch:{ all -> 0x004a }
            throw r1     // Catch:{ all -> 0x004a }
        L_0x005e:
            monitor-exit(r2)     // Catch:{ all -> 0x004a }
            a.Xf r0 = r0.mo6901yn()
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: game.engine.C0700Jz.mo3365b(java.lang.Class, long):java.lang.Object");
    }

    public C1616Xf getTaikodom() {
        return bGz().mo6901yn();
    }

    /* renamed from: fL */
    public void setNextId(long newValue) {
        this.nextId.set(newValue);
    }

    public WraperDbFile aQh() {
        return this.eKu;
    }

    /* renamed from: g */
    public C3582se mo3440g(C3582se seVar) {
        Class<?> cls = seVar.mo6901yn().getClass();
        C3582se seVar2 = this.eJZ.get(cls);
        if (seVar2 != null) {
            return seVar2;
        }
        C1875af afVar = (C1875af) ((C1616Xf) mo3365b(cls, this.nextId.incrementAndGet())).bFf();
        afVar.cVs();
        afVar.hCm.mo3146A(false);
        afVar.hCm.mo3191cS(true);
        C3582se putIfAbsent = this.eJZ.putIfAbsent(cls, afVar);
        if (putIfAbsent != null) {
            return putIfAbsent;
        }
        return afVar;
    }

    public C2844kw bHf() {
        return this.eKp;
    }

    public int bHg() {
        return this.eKH.size();
    }

    /* renamed from: a.Jz$a */
    public static class C0701a extends Blocking implements Externalizable {

    }

    /* renamed from: a.Jz$b */
    /* compiled from: a */
    class C0702b implements C1810aL {
        C0702b() {
        }

        public Class<? extends ReceivedTime> getMessageClass() {
            return C0701a.class;
        }

        /* renamed from: a */
        public <M extends ReceivedTime> C6302akO mo3456a(ClientConnect bVar, M m) {
            DataGameEvent.this.mo3446h(DataGameEvent.this.eKe.mo6442f(bVar));
            return new C5544aNk();
        }
    }
}
