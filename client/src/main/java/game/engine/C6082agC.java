package game.engine;


import logic.render.GUIModule;

import java.awt.*;
import java.awt.image.BufferedImage;

/* renamed from: a.agC  reason: case insensitive filesystem */
/* compiled from: a */
public class C6082agC implements C0507HC {
    private static C0507HC fyE;
    private final GUIModule fyI;
    private Cursor fyF;
    private Cursor fyG;
    private Cursor fyH;

    public C6082agC(GUIModule hl) {
        this.fyI = hl;
    }

    public static C0507HC bWV() {
        return fyE;
    }

    /* renamed from: a */
    public static void m21936a(C0507HC hc) {
        fyE = hc;
    }

    public Cursor aRO() {
        if (this.fyF == null) {
            this.fyF = Toolkit.getDefaultToolkit().createCustomCursor(this.fyI.adz().getImage("res://data/gui/imageset/imageset_core/mouse_enabled"), new Point(0, 0), "SelectionCursor");
        }
        return this.fyF;
    }

    public Cursor aRP() {
        if (this.fyG == null) {
            this.fyG = Toolkit.getDefaultToolkit().createCustomCursor(this.fyI.adz().getImage("res://data/gui/imageset/imageset_core/mouse_combat"), new Point(0, 0), "SelectionCursor");
        }
        return this.fyG;
    }

    public Cursor aRQ() {
        if (this.fyH == null) {
            this.fyH = Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(1, 1, 2), new Point(0, 0), "HiddenCursor");
        }
        return this.fyH;
    }

    public void aRR() {
        this.fyI.ddH().setCursor(aRO());
    }

    public void aRS() {
        this.fyI.ddH().setCursor(aRQ());
    }

    public void aRT() {
        this.fyI.ddH().setCursor(aRP());
    }
}
