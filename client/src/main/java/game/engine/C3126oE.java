package game.engine;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* renamed from: a.oE */
/* compiled from: a */
public class C3126oE {
    public Set<C3127a> aPQ = new HashSet();
    private ArrayList<C3127a> aPR = new ArrayList<>();

    /* renamed from: a */
    public synchronized void mo20939a(URL url, C6174ahq ahq) {
        this.aPQ.add(new C3127a(url, ahq));
    }

    /* renamed from: b */
    public synchronized void mo20940b(URL url, C6174ahq ahq) {
        this.aPQ.remove(new C3127a(url, ahq));
    }

    public void run() {
        this.aPR.clear();
        synchronized (this) {
            for (C3127a add : this.aPQ) {
                this.aPR.add(add);
            }
        }
        Iterator<C3127a> it = this.aPR.iterator();
        while (it.hasNext()) {
            C3127a next = it.next();
            if (next.file != null) {
                long lastModified = next.file.lastModified();
                if (next.f8753Ne != lastModified) {
                    next.f8753Ne = lastModified;
                    try {
                        next.eqy.mo13624a(next.eqx);
                    } catch (RuntimeException e) {
                        mo20940b(next.eqx, next.eqy);
                        throw e;
                    }
                }
            }
        }
        this.aPR.clear();
    }

    /* renamed from: a.oE$a */
    public static class C3127a {

        /* renamed from: Ne */
        long f8753Ne;
        URL eqx;
        C6174ahq eqy;
        File file;

        public C3127a(URL url, C6174ahq ahq) {
            this.eqx = url;
            this.eqy = ahq;
            if ("file".equals(url.getProtocol())) {
                try {
                    this.file = new File(url.toURI());
                    this.f8753Ne = this.file.lastModified();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
