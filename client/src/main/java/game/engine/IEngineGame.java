package game.engine;

import game.script.PlayerController;
import game.script.Taikodom;
import game.script.player.Player;
import game.script.player.User;
import logic.C6245ajJ;
import logic.render.GUIModule;
import logic.render.IEngineGraphics;
import logic.res.ConfigManager;
import logic.res.ILoaderTrail;
import logic.res.code.SoftTimer;
import taikodom.render.loader.provider.FilePath;

/* renamed from: a.adE  reason: case insensitive filesystem */
/* compiled from: a */
public interface IEngineGame {
    /* renamed from: a */
    void mo4065a(ILoaderTrail qu);

    /* renamed from: a */
    void mo4066a(SoftTimer abg);

    C6245ajJ getEventManager();

    void initAllAddonWaitPlayer();

    FilePath getRootPathRender();

    FilePath getRootPath();

    Taikodom getTaikodom();

    PlayerController alb();

    IEngineGraphics getEngineGraphics();

    SoftTimer alf();

    ILoaderTrail getLoaderTrail();

    C3658tN bgX();

    void bgY();

    void bha();

    ConfigManager getConfigManager();

    DataGameEvent bhc();

    GUIModule getGuiModule();

    User getUser();

    boolean bhf();

    boolean bhg();

    C0507HC bhh();

    void cleanUp();

    /* renamed from: dL */
    Player mo4089dL();

    void disconnect();

    /* renamed from: e */
    void mo4091e(Runnable runnable);

    void exit();

    boolean isVerbose();
}
