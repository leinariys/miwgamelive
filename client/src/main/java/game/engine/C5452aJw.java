package game.engine;

/* renamed from: a.aJw  reason: case insensitive filesystem */
/* compiled from: a */
public class C5452aJw extends RuntimeException {
    private static final long serialVersionUID = -2661184512718272595L;
    private String entryName;

    public C5452aJw(String str) {
        this.entryName = str;
    }

    public final String getEntryName() {
        return this.entryName;
    }
}
