package game.engine;

import taikodom.render.textures.DDSLoader;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.amS  reason: case insensitive filesystem */
/* compiled from: a */
public class FloatDef extends FloatBit {
    public static final int gbL = -126;
    public static final FloatDef gbM = new FloatDef();
    public static final int gbN = 6;
    public static final int gbO = 9;
    public static final int gbP = 511;
    public static final int gbQ = 63;
    public static final int gbR = -96;
    private static final int MAX_EXPONENT = 31;
    private static final float MAX_VALUE = Float.intBitsToFloat(1333772288);
    private static final int MIN_EXPONENT = -30;
    private static final float MIN_VALUE = Float.intBitsToFloat(813694976);
    private static final float TOLERANCE = (Float.intBitsToFloat(1065369599) - 1.0f);
    private static final int gbS = 16;
    private static final int gbT = 8192;

    /* renamed from: rb */
    public static int m23889rb(int i) {
        int i2 = 0;
        int i3 = i;
        for (int i4 = 16; i4 > 0; i4 >>= 1) {
            if ((i3 >>> i4) != 0) {
                i3 >>>= i4;
                i2 |= i4;
            }
        }
        return i2 + i3;
    }

    /* renamed from: Kc */
    public int mo8273Kc() {
        return gbS;
    }

    /* renamed from: Ka */
    public int mo8271Ka() {
        return MAX_EXPONENT;
    }

    /* renamed from: Kb */
    public int mo8272Kb() {
        return MIN_EXPONENT;
    }

    /* renamed from: Ke */
    public int mo8275Ke() {
        return gbN;
    }

    /* renamed from: Kd */
    public int mo8274Kd() {
        return gbO;
    }

    /* renamed from: Kf */
    public float mo8276Kf() {
        return MAX_VALUE;
    }

    /* renamed from: Kg */
    public float mo8277Kg() {
        return MIN_VALUE;
    }

    /* renamed from: aT */
    public int mo8282aT(float f) {
        int i;
        int i2;
        int i3 = 8388607;
        int floatToIntBits = Float.floatToIntBits(f);
        int i4 = floatToIntBits & 8388607;
        int i5 = (floatToIntBits >> 23) & 255;
        if (i5 != 255) {
            if ((i4 & gbT) != 0) {
                floatToIntBits += gbT;
                i5 = (floatToIntBits >> 23) & 255;
                i4 = floatToIntBits & 8388607;
            }
            if (i5 != 0) {
                i5 -= 96;
                if (i5 <= 0 && i5 > gbL) {
                    if (i5 > -9) {
                        i2 = (8388608 | i4) >>> (1 - i5);
                    } else {
                        i2 = 0;
                    }
                    i = 0;
                    i3 = i2;
                } else if (i5 >= gbQ) {
                    i = 62;
                }
            } else {
                i = i5;
                i3 = 0;
            }
            return ((floatToIntBits >> gbS) & DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEZ) | ((i & 63) << 9) | (i3 >> 14);
        }
        i = i5;
        i3 = i4;
        return ((floatToIntBits >> gbS) & DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEZ) | ((i & 63) << 9) | (i3 >> 14);
    }

    /* renamed from: cF */
    public float mo8283cF(int i) {
        int i2 = (i >> 9) & 63;
        int i3 = i & gbP;
        if (i2 == 63) {
            i2 = 255;
        } else if (i2 != 0) {
            i2 += 96;
        } else if (i3 != 0) {
            int rb = (9 - m23889rb(i3)) + 1;
            i2 = (-30 - rb) + 127;
            i3 = (i3 << rb) & gbP;
        }
        return Float.intBitsToFloat((i3 << 14) | (i2 << 23) | ((i << 16) & Integer.MIN_VALUE));
    }

    /* renamed from: a */
    public void mo8281a(ObjectOutput objectOutput, float f) throws IOException {
        objectOutput.writeShort(mo8282aT(f));
    }

    /* renamed from: d */
    public float mo8284d(ObjectInput objectInput) throws IOException {
        return mo8283cF(objectInput.readUnsignedShort());
    }

    /* renamed from: Kh */
    public float mo8278Kh() {
        return TOLERANCE;
    }

    /* renamed from: Ki */
    public boolean mo8279Ki() {
        return true;
    }

    /* renamed from: Kj */
    public boolean mo8280Kj() {
        return true;
    }
}
