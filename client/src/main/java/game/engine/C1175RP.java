package game.engine;

import logic.res.code.C1625Xn;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/* renamed from: a.RP */
/* compiled from: a */
public class C1175RP {
    /* access modifiers changed from: private */
    public ThreadLocal<Integer> hIa = new C7033azu(this);
    /* access modifiers changed from: private */
    public ReentrantReadWriteLock hIb = new ReentrantReadWriteLock(false);
    private C1176a hIc = new C1176a(this, (C1176a) null);

    public void cXZ() {
        this.hIb.writeLock().lock();
    }

    public boolean cYa() {
        return this.hIb.writeLock().tryLock();
    }

    public void cYb() {
        this.hIb.writeLock().unlock();
    }

    public C1625Xn cYc() {
        return this.hIc;
    }

    /* renamed from: h */
    public void mo5173h(Thread thread) {
    }

    /* renamed from: a.RP$a */
    private class C1176a implements C1625Xn {
        private C1176a() {
        }

        /* synthetic */ C1176a(C1175RP rp, C1176a aVar) {
            this();
        }

        public void brK() {
            C1175RP.this.hIa.set(Integer.valueOf(((Integer) C1175RP.this.hIa.get()).intValue() + 1));
            C1175RP.this.hIb.readLock().lock();
        }

        public boolean brL() {
            int intValue = ((Integer) C1175RP.this.hIa.get()).intValue();
            if (intValue <= 0) {
                return false;
            }
            C1175RP.this.hIa.set(Integer.valueOf(intValue - 1));
            C1175RP.this.hIb.readLock().unlock();
            return true;
        }
    }
}
