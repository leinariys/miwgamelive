package game;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.*;
import logic.data.mbean.C6947awm;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.ya */
/* compiled from: a */
public class CollisionFXSet extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bJp = null;
    public static final C5663aRz bJr = null;
    public static final C2491fm bJs = null;
    public static final C2491fm bJt = null;
    public static final C2491fm bJu = null;
    public static final C2491fm bJv = null;
    public static final C2491fm bJw = null;
    public static final C2491fm bJx = null;
    /* renamed from: bL */
    public static final C5663aRz f9584bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9585bM = null;
    /* renamed from: bN */
    public static final C2491fm f9586bN = null;
    /* renamed from: bO */
    public static final C2491fm f9587bO = null;
    /* renamed from: bP */
    public static final C2491fm f9588bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9589bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "be5d4ae961df69e526ec240da7bcb917", aum = 0)
    private static String bJo;
    @C0064Am(aul = "099f388b6479f9a81d982654d923f53a", aum = 1)
    private static C3438ra<C6909avx> bJq;
    @C0064Am(aul = "850f76d56bd3df673300f868c5a6d08a", aum = 2)

    /* renamed from: bK */
    private static UUID f9583bK;
    @C0064Am(aul = "82e98a57a8b0c9c597da6a96fc36096a", aum = 3)
    private static String handle;

    static {
        m41320V();
    }

    public CollisionFXSet() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CollisionFXSet(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41320V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CollisionFXSet.class, "be5d4ae961df69e526ec240da7bcb917", i);
        bJp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CollisionFXSet.class, "099f388b6479f9a81d982654d923f53a", i2);
        bJr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CollisionFXSet.class, "850f76d56bd3df673300f868c5a6d08a", i3);
        f9584bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CollisionFXSet.class, "82e98a57a8b0c9c597da6a96fc36096a", i4);
        f9585bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 11)];
        C2491fm a = C4105zY.m41624a(CollisionFXSet.class, "1a5cc85d2090b4570e1d81d919e82473", i6);
        f9586bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CollisionFXSet.class, "c459ea323618808920b598ec5c0655d0", i7);
        f9587bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CollisionFXSet.class, "46de5f78e3b95a634660bd3d1ba85c43", i8);
        f9588bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CollisionFXSet.class, "e3edb658c8bb347020aae54b662cf971", i9);
        f9589bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CollisionFXSet.class, "9e52c3bf0a53f783da802102c2c16be2", i10);
        bJs = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CollisionFXSet.class, "2135afbb180dc50b27a665bf5e446249", i11);
        bJt = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CollisionFXSet.class, "1a2f40b2ef4dd8f4dd8856e71ea23455", i12);
        bJu = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CollisionFXSet.class, "d94d681060ba5ff4ea8a55f6ebe66198", i13);
        bJv = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CollisionFXSet.class, "75dad864894542659b080c9f2fe07f42", i14);
        bJw = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(CollisionFXSet.class, "122c7c311573fb0517ebc0643de51be0", i15);
        bJx = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(CollisionFXSet.class, "e67592f9012edf9737ad394b21b64124", i16);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CollisionFXSet.class, C6947awm.class, _m_fields, _m_methods);
    }

    /* renamed from: Z */
    private void m41321Z(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bJr, raVar);
    }

    /* renamed from: a */
    private void m41323a(String str) {
        bFf().mo5608dq().mo3197f(f9585bM, str);
    }

    /* renamed from: a */
    private void m41324a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9584bL, uuid);
    }

    /* renamed from: an */
    private UUID m41325an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9584bL);
    }

    /* renamed from: ao */
    private String m41326ao() {
        return (String) bFf().mo5608dq().mo3214p(f9585bM);
    }

    private String apr() {
        return (String) bFf().mo5608dq().mo3214p(bJp);
    }

    private C3438ra aps() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bJr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "e3edb658c8bb347020aae54b662cf971", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m41330b(String str) {
        throw new aWi(new aCE(this, f9589bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m41333c(UUID uuid) {
        switch (bFf().mo6893i(f9587bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9587bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9587bO, new Object[]{uuid}));
                break;
        }
        m41331b(uuid);
    }

    /* renamed from: cC */
    private void m41334cC(String str) {
        bFf().mo5608dq().mo3197f(bJp, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "FXSet name")
    @C0064Am(aul = "122c7c311573fb0517ebc0643de51be0", aum = 0)
    @C5566aOg
    /* renamed from: cF */
    private void m41336cF(String str) {
        throw new aWi(new aCE(this, bJx, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6947awm(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m41327ap();
            case 1:
                m41331b((UUID) args[0]);
                return null;
            case 2:
                return m41328ar();
            case 3:
                m41330b((String) args[0]);
                return null;
            case 4:
                return m41335cD((String) args[0]);
            case 5:
                m41322a((C6909avx) args[0]);
                return null;
            case 6:
                m41332c((C6909avx) args[0]);
                return null;
            case 7:
                return apt();
            case 8:
                return apv();
            case 9:
                m41336cF((String) args[0]);
                return null;
            case 10:
                return m41329au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "FX List")
    public List<C6909avx> apu() {
        switch (bFf().mo6893i(bJv)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bJv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJv, new Object[0]));
                break;
        }
        return apt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "FXSet name")
    public String apw() {
        switch (bFf().mo6893i(bJw)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bJw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJw, new Object[0]));
                break;
        }
        return apv();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9586bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9586bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9586bN, new Object[0]));
                break;
        }
        return m41327ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "FX List")
    /* renamed from: b */
    public void mo23145b(C6909avx avx) {
        switch (bFf().mo6893i(bJt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJt, new Object[]{avx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJt, new Object[]{avx}));
                break;
        }
        m41322a(avx);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cE */
    public C6909avx mo23146cE(String str) {
        switch (bFf().mo6893i(bJs)) {
            case 0:
                return null;
            case 2:
                return (C6909avx) bFf().mo5606d(new aCE(this, bJs, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, bJs, new Object[]{str}));
                break;
        }
        return m41335cD(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "FXSet name")
    @C5566aOg
    /* renamed from: cG */
    public void mo23147cG(String str) {
        switch (bFf().mo6893i(bJx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJx, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJx, new Object[]{str}));
                break;
        }
        m41336cF(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "FX List")
    /* renamed from: d */
    public void mo23148d(C6909avx avx) {
        switch (bFf().mo6893i(bJu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJu, new Object[]{avx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJu, new Object[]{avx}));
                break;
        }
        m41332c(avx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9588bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9588bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9588bP, new Object[0]));
                break;
        }
        return m41328ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9589bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9589bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9589bQ, new Object[]{str}));
                break;
        }
        m41330b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m41329au();
    }

    @C0064Am(aul = "1a5cc85d2090b4570e1d81d919e82473", aum = 0)
    /* renamed from: ap */
    private UUID m41327ap() {
        return m41325an();
    }

    @C0064Am(aul = "c459ea323618808920b598ec5c0655d0", aum = 0)
    /* renamed from: b */
    private void m41331b(UUID uuid) {
        m41324a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "46de5f78e3b95a634660bd3d1ba85c43", aum = 0)
    /* renamed from: ar */
    private String m41328ar() {
        return m41326ao();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m41324a(UUID.randomUUID());
    }

    @C0064Am(aul = "9e52c3bf0a53f783da802102c2c16be2", aum = 0)
    /* renamed from: cD */
    private C6909avx m41335cD(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        for (C6909avx avx : aps()) {
            if (avx.czv().equals(str)) {
                return avx;
            }
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "FX List")
    @C0064Am(aul = "2135afbb180dc50b27a665bf5e446249", aum = 0)
    /* renamed from: a */
    private void m41322a(C6909avx avx) {
        aps().add(avx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "FX List")
    @C0064Am(aul = "1a2f40b2ef4dd8f4dd8856e71ea23455", aum = 0)
    /* renamed from: c */
    private void m41332c(C6909avx avx) {
        aps().remove(avx);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "FX List")
    @C0064Am(aul = "d94d681060ba5ff4ea8a55f6ebe66198", aum = 0)
    private List<C6909avx> apt() {
        return Collections.unmodifiableList(aps());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "FXSet name")
    @C0064Am(aul = "75dad864894542659b080c9f2fe07f42", aum = 0)
    private String apv() {
        return apr();
    }

    @C0064Am(aul = "e67592f9012edf9737ad394b21b64124", aum = 0)
    /* renamed from: au */
    private String m41329au() {
        return "[" + apr() + "]";
    }
}
