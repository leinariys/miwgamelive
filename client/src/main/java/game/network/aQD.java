package game.network;

import org.apache.commons.jxpath.ClassFunctions;
import org.apache.commons.jxpath.JXPathContext;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.aQD */
/* compiled from: a */
public class aQD implements C2695ii {
    public boolean iEL = false;
    public ClassFunctions iEM;

    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (String str : list) {
            sb.append(" | ").append(map.get(str));
        }
        sb.append("\n");
        while (it.hasNext()) {
            JXPathContext newContext = JXPathContext.newContext(it.next());
            newContext.setFunctions(this.iEM);
            for (String selectNodes : list) {
                sb.append(" | ");
                String str2 = "";
                for (Object append : newContext.selectNodes(selectNodes)) {
                    sb.append(str2).append(append);
                    str2 = ", ";
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /* renamed from: a */
    public String mo10863a(C0165Bs bs) {
        StringBuilder sb = new StringBuilder();
        C2881lP lPVar = (C2881lP) getClass().getAnnotation(C2881lP.class);
        if (lPVar == null) {
            sb.append(bs.ayQ()).append("\n\n");
        } else {
            sb.append(bs.ayQ()).append(":\n").append(lPVar.value()).append("\n\n");
        }
        sb.append("Usage:\n");
        sb.append(mo10866b(bs));
        sb.append("\n");
        m17432c(bs);
        mo10864a(bs.ayV(), sb, "<", ">", "\nRequired Parameters:\n");
        mo10864a(bs.ayW(), sb, "[", "]", "\nOptional Parameters:\n");
        return sb.toString();
    }

    /* renamed from: c */
    private void m17432c(C0165Bs bs) {
        try {
            bs.ayW().add(C0165Bs.class.getDeclaredField("clR"));
            bs.ayW().add(C0165Bs.class.getDeclaredField("clS"));
            bs.ayW().add(C0165Bs.class.getDeclaredField("labels"));
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo10864a(List<Field> list, StringBuilder sb, String str, String str2, String str3) {
        if (list.size() > 0) {
            sb.append(str3);
            for (Field next : list) {
                sb.append(str).append(next.getName()).append(str2).append(" - Type: ").append(next.getType().getSimpleName());
                C2884lS lSVar = (C2884lS) next.getAnnotation(C2884lS.class);
                if (lSVar != null) {
                    sb.append(", Mask: ").append(lSVar.value());
                }
                C2881lP lPVar = (C2881lP) getClass().getAnnotation(C2881lP.class);
                if (lPVar != null) {
                    sb.append(", ").append(lPVar.value());
                }
                sb.append("\n");
            }
        }
    }

    /* renamed from: b */
    public String mo10866b(C0165Bs bs) {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        sb.append("exec ").append(bs.ayQ());
        for (Field field : getClass().getDeclaredFields()) {
            if (field.getAnnotation(aUV.class) == null && !Modifier.isStatic(field.getModifiers())) {
                if (field.getAnnotation(C3221pP.class) == null) {
                    bs.ayV().add(field);
                    str = " <";
                    str2 = ">";
                } else {
                    bs.ayW().add(field);
                    str = " [";
                    str2 = "]";
                }
                sb.append(str).append(field.getName()).append("=");
                if (field.getType() == List.class) {
                    sb.append("List(;E1;E2;..En)");
                } else {
                    sb.append(field.getType().getSimpleName());
                }
                C2884lS lSVar = (C2884lS) field.getAnnotation(C2884lS.class);
                if (lSVar != null) {
                    sb.append("(").append(lSVar.value()).append(")");
                }
                sb.append(str2);
            }
        }
        return sb.toString();
    }

    /* renamed from: Y */
    public void mo10862Y(boolean z) {
        this.iEL = z;
    }

    /* renamed from: Bh */
    public ClassFunctions mo10861Bh() {
        return this.iEM;
    }

    /* renamed from: a */
    public void mo10865a(ClassFunctions classFunctions) {
        this.iEM = classFunctions;
    }
}
