package game.network;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.lP */
/* compiled from: a */
public @interface C2881lP {
    /* renamed from: MI */
    String mo20222MI() default "en";

    String value();
}
