package game.network.build;

import game.network.channel.NetworkChannel;

/* renamed from: a.afq  reason: case insensitive filesystem */
/* compiled from: a */
public class BuilderSocketStatic {
    public static final IServerConnect BUILDER_SOCKET = new BuilderNioSocket();

    /* renamed from: a */
    public static NetworkChannel m21641a(String host, int port, String userName, String password, boolean isForcingNewUser, String ckientVersion) throws Exception {
        return BUILDER_SOCKET.buildSocketChannel(host, port, userName, password, isForcingNewUser, ckientVersion);
    }

    /* renamed from: d */
    public static NetworkChannel m21644d(String str, String str2, boolean z, String str3) {
        return BUILDER_SOCKET.buildSocketChannel(str, str2, z, str3);
    }

    /* renamed from: a */
    public static TransportThread buildTransportThread(ManagerMessageTransport zCVar, NetworkChannel ahg) {
        return BUILDER_SOCKET.buildTransportThread(zCVar, ahg);
    }

    /* renamed from: d */
    public static NetworkChannel m21643d(String str, int port, String version) {
        return BUILDER_SOCKET.buildSocketChannel(str, port, version);
    }

    /* renamed from: b */
    public static NetworkChannel buildServerSocket(int listenerPort, String version) {
        return BUILDER_SOCKET.buildServerSocket(listenerPort, version);
    }

    /* renamed from: iD */
    public static NetworkChannel m21645iD(String str) {
        return BUILDER_SOCKET.buildServerSocket(IServerConnect.PORT_LOGIN, str);
    }
}
