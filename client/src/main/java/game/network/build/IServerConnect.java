package game.network.build;

import game.network.channel.NetworkChannel;


/* renamed from: a.Te */
/* compiled from: a */
public interface IServerConnect {
    public static final String DEFAULT_HOST = "localhost";
    /* renamed from: ehA */
    public static final int PORT_LOGIN = 15225;
    public static final int PORT = 15226;

    /* renamed from: a */
    TransportThread buildTransportThread(ManagerMessageTransport zCVar, NetworkChannel ahg);

    /* renamed from: a */
    NetworkChannel buildSocketChannel(String host, int port, String userName, String password, boolean isForcingNewUser, String ckientVersion) throws Exception;

    /* renamed from: b */
    NetworkChannel buildServerSocket(int listenerPort, String version);

    /* renamed from: d */
    NetworkChannel buildSocketChannel(String str, int port, String version);

    /* renamed from: d */
    NetworkChannel buildSocketChannel(String userName, String password, boolean isForcingNewUser, String ckientVersion);
}
