package game.network.build;

import game.engine.DataGameEvent;
import game.network.Transport;
import game.network.WhoAmI;
import game.network.channel.NetworkChannel;
import game.network.channel.client.ClientConnect;
import game.network.exception.*;
import game.network.manager.*;
import game.network.message.*;
import game.network.message.externalizable.C0500Gw;
import game.network.message.externalizable.C6302akO;
import game.network.message.externalizable.C7037azy;
import game.network.message.serializable.ReceivedTime;
import gnu.trove.THashMap;
import logic.bbb.aDR;
import logic.res.html.MessageContainer;
import logic.res.html.axI;
import logic.thred.LogPrinter;
import taikodom.render.textures.DDSLoader;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutput;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;

/* renamed from: a.aqI  reason: case insensitive filesystem */
/* compiled from: a */
public final class ManagerMessageTransportImpl implements ManagerMessageTransport {
    /* access modifiers changed from: private */
    public static final LogPrinter logger = LogPrinter.m10275K(ManagerMessageTransportImpl.class);
    /* access modifiers changed from: private */
    public final DataGameEvent dataGameEvent;
    /* access modifiers changed from: private */
    public TransportThread transportThread;
    private NetworkChannel networkChannel;
    private boolean gDJ = false;
    private List<C6054afa> gDK = new CopyOnWriteArrayList();
    private ArrayBlockingQueue<Runnable> poolExecuterCommand = new ArrayBlockingQueue<>(DDSLoader.DDSD_MIPMAPCOUNT);
    private THashMap<Class<?>, C1810aL> gDM = new THashMap<>();
    private C1810aL gDN = new C0488Gl(this);
    private C1810aL gDO = new C0490Gn(this);
    private C1810aL gDP = new C0485Gi(this);
    private C1810aL gDQ = new C0487Gk(this);
    private C1810aL gDR = new C0484Gh(this);

    public ManagerMessageTransportImpl(DataGameEvent jz, NetworkChannel networkChannel) {
        mo15544a(this.gDQ);
        mo15544a(this.gDO);
        mo15544a(this.gDP);
        mo15544a(this.gDR);
        this.dataGameEvent = jz;
        this.networkChannel = networkChannel;
        this.transportThread = BuilderSocketStatic.buildTransportThread(this, networkChannel);
        this.transportThread.setIsServer(jz.getWhoAmI() == WhoAmI.SERVER ? true : false);
    }

    /* renamed from: a */
    public static ReceivedTime messageTransformation(DataGameEvent jz, Transport transportCommand) {
        ByteMessageReader inputStream = transportCommand.getByteMessageReader();
        try {
            return (ReceivedTime) DefaultMagicDataClassSerializer
                .dataClassSerializer
                .inputReadObject(new ContainerBinaryData(jz, inputStream, jz.eKm, true, 0).readObject());
        } catch (Error e) {
            C0288Di.errorDeserializing((InputStream) inputStream, (Throwable) e);
            throw e;
        } catch (RuntimeException e2) {
            C0288Di.errorDeserializing((InputStream) inputStream, (Throwable) e2);
            throw e2;
        } catch (Exception e3) {
            C0288Di.errorDeserializing((InputStream) inputStream, (Throwable) e3);
            throw new RuntimeException(e3);
        }
    }

    public TransportThread cxz() {
        return this.transportThread;
    }

    public NetworkChannel aPT() {
        return this.networkChannel;
    }

    /* renamed from: HY */
    public void mo15543HY() {
        this.transportThread.pendingClose();
        if (this.dataGameEvent.getWhoAmI() == WhoAmI.CLIENT) {
            try {
                this.networkChannel.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void dispose() {
        this.transportThread.dispose();
        if (this.dataGameEvent.getWhoAmI() == WhoAmI.CLIENT) {
            try {
                this.networkChannel.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: b */
    public void mo15549b(ClientConnect bVar, Blocking kUVar) {
        TransportCommand transportCommand = new TransportCommand();
        addSerializeDataReceivedTime((Transport) transportCommand, (ReceivedTime) kUVar);
        this.transportThread.sendTransportMessageInSeparateThread(bVar, transportCommand);
        if (cxA()) {
            m25077b(bVar, kUVar, transportCommand);
        }
    }

    /* renamed from: c */
    public C6302akO mo15551c(ClientConnect bVar, Blocking kUVar) {
        logger.isDebugEnabled();
        boolean brL = this.dataGameEvent.bGK().brL();
        kUVar.setBlocking(true);
        TransportCommand transportCommand = new TransportCommand();
        addSerializeDataReceivedTime((Transport) transportCommand, (ReceivedTime) kUVar);
        TransportResponse b = this.transportThread.sendTransportMessageAndWaitForResponse(bVar, transportCommand);
        if (brL) {
            try {
                this.dataGameEvent.bGK().brK();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        C6302akO ako = (C6302akO) messageTransformation(this.dataGameEvent, (Transport) b);
        if (logger.isDebugEnabled()) {
            logger.debug("response... " + ako);
        }
        if (!(ako instanceof C0500Gw)) {
            return ako;
        }
        C0500Gw gw = (C0500Gw) ako;
        throw new C1728ZX(gw.getCause(), gw.aQW());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void addSerializeDataReceivedTime(Transport transport, ReceivedTime receivedTime) {
        try {
            C0387FM fm = new C0387FM(this.dataGameEvent, transport.getOutputStream(), this.dataGameEvent.eKm, true, 0, false);
            DefaultMagicDataClassSerializer.dataClassSerializer.serializeData((ObjectOutput) fm, (Object) receivedTime);
            fm.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (RuntimeException e2) {
            throw e2;
        }
    }

    /* renamed from: d */
    public TransportResponse preparingResponseMessage(ClientConnect connect, TransportCommand transportCommand) {
        this.dataGameEvent.bGC().setDirectionTransportCommand(connect, transportCommand);
        try {
            Blocking block = (Blocking) messageTransformation(this.dataGameEvent, (Transport) transportCommand);
            block.setReceivedTimeMilis(transportCommand.getTimeReadMesage());
            C6302akO d = mo15556d(connect, block);
            if (d != null) {
                TransportResponse createResponse = transportCommand.createResponse();
                addSerializeDataReceivedTime((Transport) createResponse, (ReceivedTime) d);
                return createResponse;
            }
        } catch (C2571gy e) {
            if (transportCommand.isBlocking()) {
                TransportResponse createResponse2 = transportCommand.createResponse();
                addSerializeDataReceivedTime((Transport) createResponse2, (ReceivedTime) new C0500Gw(C0500Gw.cYS));
                return createResponse2;
            }
            logger.info("Ignoring referring disposed object: " + e.getMessage());
        } finally {
            this.dataGameEvent.bGC().setNullClientConnect();
        }
        this.dataGameEvent.bGC().setNullClientConnect();
        return null;
    }

    /* renamed from: e */
    public void addToPoolExecuterCommand(ClientConnect clientConnect, TransportCommand transportCommand) {
        if (this.dataGameEvent.isWhoAmIClient) {
            this.poolExecuterCommand.add(new ExecuterCommandRunnable(clientConnect, transportCommand));
        } else if (this.dataGameEvent.threadPoolExecutor == null) {
            throw new IllegalStateException("commandExecuterPool should not be null!");
        } else if (!this.dataGameEvent.threadPoolExecutor.isShutdown()) {
            ExecuterCommandRunnable runnable = new ExecuterCommandRunnable(clientConnect, transportCommand);
            if (BuilderSocketStatic.BUILDER_SOCKET.getClass() == BuilderMinaSocket.class) {
                runnable.run();
            } else {
                this.dataGameEvent.threadPoolExecutor.execute(runnable);
            }
        } else {
            logger.warn("Command " + transportCommand + " asked to be sent to " + clientConnect + " was ignored coz the executer pool has been shutdown");
        }
    }

    /* renamed from: a */
    public void mo15544a(C1810aL aLVar) {
        this.gDM.put(aLVar.getMessageClass(), aLVar);
    }

    /* renamed from: d */
    public C6302akO mo15556d(ClientConnect bVar, Blocking kUVar) {
        if (this.gDJ) {
            m25079e(bVar, kUVar);
        }
        C1810aL aLVar = (C1810aL) this.gDM.get(kUVar.getClass());
        if (aLVar != null) {
            return aLVar.mo3456a(bVar, kUVar);
        }
        if (kUVar instanceof C5581aOv) {
            return m25076b(bVar, (C5581aOv) kUVar);
        }
        try {
            return kUVar.mo876a(bVar, this.dataGameEvent);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    private C6302akO m25076b(ClientConnect bVar, C5581aOv aov) {
        try {
            return aov.mo876a(bVar, this.dataGameEvent);
        } catch (TransportIsDisposedException e) {
            logger.fatal("System going down coz the application thrown an FatalRuntimeException", e);
            this.dataGameEvent.exit(-1);
            return null;
        } catch (Throwable th) {
            Throwable th2 = th;
            boolean z = th2.getClass().getAnnotation(C6546aoy.class) != null;
            if (!(th2 instanceof aOQ) && !(th2 instanceof C3578sa) && !z) {
                logger.warn("######################################################################################");
                logger.warn("An exception happened in a call requested by " + bVar + ":", th2);
                logger.warn("Message: " + aov);
                logger.warn("######################################################################################");
            }
            if (aov.isBlocking()) {
                return new C0500Gw(th2);
            }
            return null;
        }
    }

    /* renamed from: C */
    public void mo15541C(Map<aDR, List<Blocking>> map) {
        for (Map.Entry next : map.entrySet()) {
            aDR adr = (aDR) next.getKey();
            List list = (List) next.getValue();
            if (list.size() > 1) {
                C1431Uy.m10411u(list);
                Blocking[] kUVarArr = new Blocking[list.size()];
                list.toArray(kUVarArr);
                mo15549b(adr.bgt(), (Blocking) new C7037azy(kUVarArr));
            } else {
                mo15549b(adr.bgt(), (Blocking) list.get(0));
            }
        }
    }

    /* renamed from: HX */
    public int mo15542HX() {
        return this.transportThread.getThreadInteger();
    }

    /* renamed from: c */
    public void authorizationChannelClose(ClientConnect bVar) {
        this.networkChannel.authorizationChannelClose(bVar);
    }

    public boolean cxA() {
        return this.gDJ;
    }

    /* renamed from: gP */
    public void mo15560gP(boolean z) {
        this.gDJ = z;
    }

    /* renamed from: e */
    private void m25079e(ClientConnect bVar, Blocking kUVar) {
        for (C6054afa a : this.gDK) {
            a.mo13263a(bVar, kUVar);
        }
    }

    /* renamed from: b */
    private void m25077b(ClientConnect bVar, Blocking kUVar, TransportCommand transportCommand) {
        for (C6054afa a : this.gDK) {
            a.mo13264a(bVar, kUVar, transportCommand);
        }
    }

    /* renamed from: a */
    public void mo15545a(C6054afa afa) {
        this.gDK.add(afa);
    }

    /* renamed from: b */
    public void mo15548b(C6054afa afa) {
        this.gDK.remove(afa);
    }

    public void runNextRunnableExecuterCommand() {
        this.poolExecuterCommand.take().run();
    }

    /* renamed from: gZ */
    public void mo15561gZ() {
        List<Runnable> dgK = MessageContainer.init();
        while (!this.poolExecuterCommand.isEmpty()) {
            this.poolExecuterCommand.drainTo(dgK);
            for (Runnable run : dgK) {
                run.run();
            }
            dgK.clear();
        }
    }

    /* renamed from: b */
    public void mo15550b(Executor executor) {
        List<Runnable> dgK = MessageContainer.init();
        while (!this.poolExecuterCommand.isEmpty()) {
            this.poolExecuterCommand.drainTo(dgK);
            for (Runnable execute : dgK) {
                executor.execute(execute);
            }
            dgK.clear();
        }
    }

    /* renamed from: a.aqI$a */
    private class ExecuterCommandRunnable implements Runnable {
        private final ClientConnect clientConnect;
        private TransportCommand command;

        public ExecuterCommandRunnable(ClientConnect bVar, TransportCommand transportCommand) {
            this.clientConnect = bVar;
            this.command = transportCommand;
        }

        public void run() {
            try {
                TransportResponse response = ManagerMessageTransportImpl.this.preparingResponseMessage(this.clientConnect, this.command);
                if (response != null) {
                    ManagerMessageTransportImpl.this.transportThread.sendTransportResponse(this.clientConnect, response);
                }
            } catch (axI e) {
                System.err.println("queue disposed");
            } catch (Throwable th) {
                System.err.println("!!!Uncaught exception:");
                th.printStackTrace();
            }
        }
    }
}
