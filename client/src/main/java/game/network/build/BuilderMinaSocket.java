package game.network.build;

import game.network.channel.NetworkChannel;
import p001a.OpenSocketMina;
import p001a.OpenSocketMinaClient;
import p001a.OpenSocketMinaServer;

/* renamed from: a.aNu  reason: case insensitive filesystem */
/* compiled from: a */
public class BuilderMinaSocket implements IServerConnect {
    /* renamed from: d */
    public NetworkChannel buildSocketChannel(String userName, String password, boolean isForcingNewUser, String ckientVersion) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public NetworkChannel buildSocketChannel(String host, int port, String userName, String password, boolean isForcingNewUser, String ckientVersion) throws Exception {
        return new OpenSocketMinaClient(host, port, userName, password, isForcingNewUser, ckientVersion);
    }

    /* renamed from: d */
    public NetworkChannel buildSocketChannel(String str, int port, String version) {
        try {
            return new OpenSocketMinaClient(str, port, (String) null, (String) null, false, version);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /* renamed from: b */
    public NetworkChannel buildServerSocket(int listenerPort, String version) {
        return new OpenSocketMinaServer(listenerPort, version);
    }

    /* renamed from: a */
    public TransportThread buildTransportThread(ManagerMessageTransport zCVar, NetworkChannel ahg) {
        return ((OpenSocketMina) ahg).mo19616a(zCVar);
    }
}
