package game.network.build;

import game.network.channel.NetworkChannel;
import game.network.channel.client.OpenSocketNioClient;
import game.network.channel.server.OpenSocketNioServer;
import game.network.exception.IOExceptionServer;
import game.network.thread.TransportThreadImpl;

/* renamed from: a.aBm  reason: case insensitive filesystem */
/* compiled from: a */
public class BuilderNioSocket implements IServerConnect {
    /* renamed from: d */
    public NetworkChannel buildSocketChannel(String userName, String password, boolean isForcingNewUser, String ckientVersion) {
        try {
            return new OpenSocketNioClient(IServerConnect.DEFAULT_HOST, IServerConnect.PORT_LOGIN, userName, password, isForcingNewUser, ckientVersion);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    /* renamed from: a */
    public NetworkChannel buildSocketChannel(String host, int port, String userName, String password, boolean isForcingNewUser, String ckientVersion) throws IOExceptionServer {
        return new OpenSocketNioClient(host, port, userName, password, isForcingNewUser, ckientVersion);
    }

    /* renamed from: d */
    public NetworkChannel buildSocketChannel(String str, int port, String version) {
        try {
            return new OpenSocketNioClient(str, port, (String) null, (String) null, false, version, false);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    /* renamed from: b */
    public NetworkChannel buildServerSocket(int listenerPort, String version) {
        return new OpenSocketNioServer(listenerPort, version);
    }

    /* renamed from: a */
    public TransportThread buildTransportThread(ManagerMessageTransport zCVar, NetworkChannel networkChannel) {
        return new TransportThreadImpl(zCVar, networkChannel.getClientConnect(), networkChannel.getMessageProcessing(), networkChannel.getProcessSendMessage());
    }

    /* renamed from: iD */
    public NetworkChannel buildOpenServerSocketChannel(String version) {
        return buildServerSocket(IServerConnect.PORT_LOGIN, version);
    }
}
