package game.network.build;

import game.network.channel.client.ClientConnect;
import game.network.message.TransportCommand;
import game.network.message.TransportResponse;

/* renamed from: a.zC */
/* compiled from: a */
public interface ManagerMessageTransport {
    /* renamed from: d */
    TransportResponse preparingResponseMessage(ClientConnect bVar, TransportCommand transportCommand);

    /* renamed from: e */
    void addToPoolExecuterCommand(ClientConnect bVar, TransportCommand transportCommand);
}
