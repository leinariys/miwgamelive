package game.network;

import util.C0831Lz;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.hA */
/* compiled from: a */
public class C2574hA {

    /* renamed from: UM */
    List<Integer> f7839UM = new ArrayList();

    /* renamed from: UN */
    List<Boolean> f7840UN = new ArrayList();

    /* renamed from: UO */
    List<C3865vx> f7841UO = new C0831Lz();

    /* renamed from: UQ */
    C3865vx f7842UQ;

    C2574hA() {
    }

    /* renamed from: a */
    public void mo19168a(List<?>[] listArr) {
        this.f7841UO.add(new C3865vx(listArr));
        for (int i = 0; i < listArr.length; i++) {
            for (int i2 = 0; i2 < listArr[i].size(); i2++) {
                Object obj = listArr[i].get(i2);
                String obj2 = obj == null ? "" : obj.toString();
                int length = obj2.length();
                if (length > this.f7839UM.get(i).intValue()) {
                    this.f7839UM.set(i, Integer.valueOf(length));
                }
                if (this.f7840UN.get(i).booleanValue() && obj2.length() > 0) {
                    try {
                        Double.parseDouble(obj2);
                    } catch (Throwable th) {
                        this.f7840UN.set(i, false);
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public void mo19169b(String[] strArr) {
        List[] listArr = new List[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            listArr[i] = new ArrayList();
            listArr[i].add(strArr[i]);
        }
        this.f7842UQ = new C3865vx(listArr);
        m32467c(strArr);
    }

    /* renamed from: c */
    private void m32467c(String[] strArr) {
        if (this.f7839UM.size() < strArr.length) {
            for (int i = 0; i < strArr.length; i++) {
                if (strArr[i] == null) {
                    strArr[i] = "";
                }
                this.f7839UM.add(Integer.valueOf(strArr[i].length()));
                this.f7840UN.add(true);
            }
        }
    }

    /* renamed from: a */
    public String mo19167a(StringBuilder sb) {
        C0831Lz<C3865vx> lz = new C0831Lz<>();
        for (C3865vx add : this.f7841UO) {
            lz.add(add);
        }
        int i = 0;
        for (Integer intValue : this.f7839UM) {
            i = intValue.intValue() + i + 3;
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append("\n");
        for (int i2 = 0; i2 < i; i2++) {
            sb2.append('-');
        }
        String sb3 = sb2.toString();
        sb.append(this.f7842UQ.mo22680a(this.f7840UN, this.f7839UM)).append(sb3);
        for (C3865vx vxVar : lz) {
            vxVar.f9440UN = this.f7840UN;
            vxVar.f9439UM = this.f7839UM;
            vxVar.mo22682b(sb);
            sb.append(sb3);
        }
        sb.append("\n");
        return sb.toString();
    }
}
