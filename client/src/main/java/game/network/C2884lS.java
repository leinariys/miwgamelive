package game.network;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.lS */
/* compiled from: a */
public @interface C2884lS {
    String value();
}
