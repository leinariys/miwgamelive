package game.network.message;

import game.engine.SocketMessage;
import game.network.channel.client.ClientConnect;
import util.Syst;

/* renamed from: a.aBB */
/* compiled from: a */
public class ByteMessage {
    private final byte[] message;
    private final int length;
    private final int offset;
    private transient ClientConnect fromClientConnect;
    private transient ClientConnect inClientConnect;
    private int messageId;

    public ByteMessage(ClientConnect bVar, byte[] message) {
        this.fromClientConnect = bVar;
        this.message = message;
        this.offset = 0;
        this.length = message.length;
    }

    public ByteMessage(byte[] message) {
        this.message = message;
        this.offset = 0;
        this.length = message.length;
    }

    public ByteMessage(SocketMessage ala) {
        this(ala.getMessage(), 0, ala.getCount());
    }

    public ByteMessage(byte[] bArr, int i, int i2) {
        this.message = bArr;
        this.offset = i;
        this.length = i2;
    }

    public ByteMessage(ClientConnect bVar, byte[] bArr, int i, int i2) {
        this.fromClientConnect = bVar;
        this.message = bArr;
        this.offset = i;
        this.length = i2;
    }

    public String toString() {
        return Syst.byteToStringLog(this.message, this.offset, this.length, " ", 16);
    }

    public final int getMessageId() {
        return this.messageId;
    }

    public final void setMessageId(int i) {
        this.messageId = i;
    }

    public final ClientConnect getFromClientConnect() {
        return this.fromClientConnect;
    }

    /* renamed from: k */
    public final void setFromClientConnect(ClientConnect bVar) {
        this.fromClientConnect = bVar;
    }

    public final ClientConnect getInClientConnect() {
        return this.inClientConnect;
    }

    /* renamed from: l */
    public final void setInClientConnect(ClientConnect bVar) {
        this.inClientConnect = bVar;
    }

    public final byte[] getMessage() {
        return this.message;
    }

    public final int getOffset() {
        return this.offset;
    }

    public final int getLength() {
        return this.length;
    }
}
