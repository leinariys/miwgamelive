package game.network.message;

import game.network.message.externalizable.C0152Bl;
import game.network.message.externalizable.C6302akO;
import logic.bbb.aDR;
import p001a.C3582se;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.aif  reason: case insensitive filesystem */
/* compiled from: a */
class C6215aif implements C3767um {
    public static final byte idA = 11;
    public static final byte idB = 12;
    public static final byte idC = 13;
    public static final byte idD = 14;
    public static final byte idE = 21;
    public static final byte idF = 22;
    public static final byte idG = 23;
    public static final byte idH = 24;
    /* access modifiers changed from: private */
    public final C3582se cVz;
    private final C3767um idL;
    ReentrantLock idJ = new ReentrantLock(false);
    private Map<aDR, C1408Ue> idI = new HashMap();
    private C6972axl idK;

    C6215aif(C3582se seVar, C3767um umVar) {
        this.cVz = seVar;
        this.idL = umVar;
    }

    /* renamed from: a */
    public C1408Ue mo279a(C6972axl axl) {
        this.idK = axl;
        return new C1913b();
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public C0152Bl m22622e(byte b) {
        return new C0152Bl(this.cVz.mo6901yn(), b);
    }

    /* renamed from: a */
    public C6302akO mo13776a(aDR adr, C0152Bl bl) {
        switch (bl.ayq()) {
            case 11:
                this.idK.cbt();
                return null;
            case 12:
                this.idK.cbw();
                return null;
            case 13:
                this.idK.cbu();
                return null;
            case 14:
                this.idK.cbv();
                return null;
            case 21:
                m22623y(adr).bxk();
                return null;
            case 22:
                m22623y(adr).bxl();
                return null;
            case 23:
                m22623y(adr).bxm();
                return null;
            case 24:
                m22623y(adr).bxn();
                return null;
            default:
                return null;
        }
    }

    /* renamed from: y */
    private C1408Ue m22623y(aDR adr) {
        this.idJ.lock();
        try {
            C1408Ue ue = this.idI.get(adr);
            if (ue == null) {
                ue = this.idL.mo279a(new C1912a(adr));
                this.idI.put(adr, ue);
            }
            return ue;
        } finally {
            this.idJ.unlock();
        }
    }

    /* renamed from: a.aif$b */
    /* compiled from: a */
    class C1913b implements C1408Ue {
        C1913b() {
        }

        public void bxm() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(C6215aif.this.cVz.mo6892hE(), (Blocking) C6215aif.this.m22622e(C6215aif.idG));
        }

        public void bxk() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(C6215aif.this.cVz.mo6892hE(), (Blocking) C6215aif.this.m22622e(C6215aif.idE));
        }

        public void bxl() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(C6215aif.this.cVz.mo6892hE(), (Blocking) C6215aif.this.m22622e(C6215aif.idF));
        }

        public void bxn() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(C6215aif.this.cVz.mo6892hE(), (Blocking) C6215aif.this.m22622e(C6215aif.idH));
        }
    }

    /* renamed from: a.aif$a */
    class C1912a implements C6972axl {
        private final /* synthetic */ aDR fMT;

        C1912a(aDR adr) {
            this.fMT = adr;
        }

        public void cbt() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(this.fMT.bgt(), (Blocking) C6215aif.this.m22622e((byte) 11));
        }

        public void cbu() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(this.fMT.bgt(), (Blocking) C6215aif.this.m22622e(C6215aif.idC));
        }

        public void cbv() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(this.fMT.bgt(), (Blocking) C6215aif.this.m22622e(C6215aif.idD));
        }

        public void cbw() {
            C6215aif.this.cVz.mo6866PM().bGU().mo15549b(this.fMT.bgt(), (Blocking) C6215aif.this.m22622e(C6215aif.idB));
        }
    }
}
