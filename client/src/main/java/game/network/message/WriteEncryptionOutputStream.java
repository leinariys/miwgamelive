package game.network.message;

import java.io.IOException;
import java.io.OutputStream;

/* renamed from: a.aFj  reason: case insensitive filesystem */
/* compiled from: a */
public class WriteEncryptionOutputStream extends OutputStream {

    private final OutputStream out;
    /* renamed from: oZ */
    private int anInt = 48151623;

    public WriteEncryptionOutputStream(OutputStream outputStream) {
        this.out = outputStream;
    }

    /* renamed from: aC */
    public static int calculationSaltKey(int i, int i2) {
        return (9932467 * i) + 7890661 + (i2 & 255);
    }

    public void close() throws IOException {
        this.out.close();
    }

    public void flush() throws IOException {
        this.out.flush();
    }

    public void write(int i) throws IOException {
        this.out.write(((i & 255) ^ (this.anInt & 255)) & 255);
        this.anInt = calculationSaltKey(this.anInt, i);
    }
}
