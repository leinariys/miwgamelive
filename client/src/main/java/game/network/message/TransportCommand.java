package game.network.message;

import game.network.Transport;

import java.io.EOFException;
import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: a */
public class TransportCommand extends Transport implements Serializable {

    private static AtomicInteger integer = new AtomicInteger();

    public TransportCommand() {
        int incrementAndGet = integer.incrementAndGet();
        setMessageId(incrementAndGet);
        setThreadId(incrementAndGet);
    }

    public TransportCommand(ByteMessageReader amf) throws EOFException {
        super(amf);
    }

    public TransportResponse createResponse() {
        TransportResponse transportResponse = new TransportResponse();
        transportResponse.setMessageId(getMessageId());
        transportResponse.setThreadId(getThreadId());
        return transportResponse;
    }

    public TransportCommand createNestedCommand() {
        TransportCommand transportCommand = new TransportCommand();
        transportCommand.setThreadId(getThreadId());
        return transportCommand;
    }

    public int getType() {
        return TRANSPORT_COMMAND;
    }
}
