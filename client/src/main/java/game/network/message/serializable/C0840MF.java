package game.network.message.serializable;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import org.mozilla1.javascript.ScriptRuntime;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import java.io.Serializable;

/* renamed from: a.MF */
/* compiled from: a */
public class C0840MF implements Serializable {
    public static final float ANGLE_PRECISION = 5.0E-5f;
    public static final float TRACE_PRECISION = 1.0E-5f;
    private static final long serialVersionUID = -8143563609245891093L;

    /* renamed from: w */
    public final float f1096w;

    /* renamed from: x */
    public final float f1097x;

    /* renamed from: y */
    public final float f1098y;

    /* renamed from: z */
    public final float f1099z;

    public C0840MF() {
        this.f1096w = 1.0f;
        this.f1099z = 0.0f;
        this.f1098y = 0.0f;
        this.f1097x = 0.0f;
    }

    public C0840MF(C0840MF mf) {
        this.f1096w = mf.f1096w;
        this.f1097x = mf.f1097x;
        this.f1098y = mf.f1098y;
        this.f1099z = mf.f1099z;
    }

    public C0840MF(float[] fArr) {
        this(fArr[0], fArr[1], fArr[2], fArr[3]);
    }

    public C0840MF(float f, float f2, float f3, float f4) {
        this.f1096w = f;
        this.f1097x = f2;
        this.f1098y = f3;
        this.f1099z = f4;
    }

    public C0840MF(Vec3f vec3f, double d) {
        double d2 = d / 2.0d;
        double sin = Math.sin(d2);
        double cos = Math.cos(d2);
        Vec3f vec3f2 = new Vec3f((Vector3f) vec3f);
        vec3f2.normalize();
        this.f1096w = (float) cos;
        this.f1097x = (float) (((double) vec3f2.x) * sin);
        this.f1098y = (float) (((double) vec3f2.y) * sin);
        this.f1099z = (float) (((double) vec3f2.z) * sin);
    }

    public C0840MF(double d, double d2, double d3) {
        double sin = Math.sin(d2 / 2.0d);
        double cos = Math.cos(d2 / 2.0d);
        double sin2 = Math.sin(d3 / 2.0d);
        double cos2 = Math.cos(d3 / 2.0d);
        double sin3 = Math.sin(d / 2.0d);
        double cos3 = Math.cos(d / 2.0d);
        double d4 = cos * cos2;
        double d5 = sin * sin2;
        this.f1097x = (float) ((sin3 * d4) - (cos3 * d5));
        this.f1098y = (float) ((cos3 * sin * cos2) + (sin3 * cos * sin2));
        this.f1099z = (float) (((cos * cos3) * sin2) - ((sin * sin3) * cos2));
        this.f1096w = (float) ((cos3 * d4) + (sin3 * d5));
    }

    public C0840MF(Matrix4fWrap ajk) {
        float f = 1.0f + ajk.m00 + ajk.m11 + ajk.m22;
        if (f > 1.0E-5f) {
            float sqrt = ((float) Math.sqrt((double) f)) * 2.0f;
            this.f1097x = (ajk.m21 - ajk.m12) / sqrt;
            this.f1098y = (ajk.m02 - ajk.m20) / sqrt;
            this.f1099z = (ajk.m10 - ajk.m01) / sqrt;
            this.f1096w = sqrt * 0.25f;
        } else if (ajk.m00 > ajk.m11 && ajk.m00 > ajk.m22) {
            float sqrt2 = ((float) Math.sqrt(((((double) ajk.m00) + 1.0d) - ((double) ajk.m11)) - ((double) ajk.m22))) * 2.0f;
            this.f1097x = 0.25f * sqrt2;
            this.f1098y = (ajk.m10 + ajk.m01) / sqrt2;
            this.f1099z = (ajk.m02 + ajk.m20) / sqrt2;
            this.f1096w = (ajk.m12 - ajk.m21) / sqrt2;
        } else if (ajk.m11 > ajk.m22) {
            float sqrt3 = ((float) Math.sqrt(((((double) ajk.m11) + 1.0d) - ((double) ajk.m00)) - ((double) ajk.m22))) * 2.0f;
            this.f1097x = (ajk.m10 + ajk.m01) / sqrt3;
            this.f1098y = 0.25f * sqrt3;
            this.f1099z = (ajk.m21 + ajk.m12) / sqrt3;
            this.f1096w = (ajk.m02 - ajk.m20) / sqrt3;
        } else {
            float sqrt4 = ((float) Math.sqrt(((((double) ajk.m22) + 1.0d) - ((double) ajk.m00)) - ((double) ajk.m11))) * 2.0f;
            this.f1097x = (ajk.m02 + ajk.m20) / sqrt4;
            this.f1098y = (ajk.m21 + ajk.m12) / sqrt4;
            this.f1099z = 0.25f * sqrt4;
            this.f1096w = (ajk.m01 - ajk.m10) / sqrt4;
        }
    }

    /* renamed from: e */
    public static C0840MF m6939e(Vec3f vec3f, double d) {
        return new C0840MF(vec3f, d);
    }

    /* renamed from: f */
    public static C0840MF m6941f(Vec3f vec3f, double d) {
        return new C0840MF(vec3f, (3.141592653589793d * d) / 180.0d);
    }

    /* renamed from: f */
    public static C0840MF m6940f(double d, double d2, double d3) {
        return new C0840MF(d, d2, d3);
    }

    /* renamed from: g */
    public static C0840MF m6942g(double d, double d2, double d3) {
        return new C0840MF((d * 3.141592653589793d) / 180.0d, (d2 * 3.141592653589793d) / 180.0d, (3.141592653589793d * d3) / 180.0d);
    }

    /* renamed from: a */
    private static Vec3f m6937a(Vec3f vec3f, C0840MF mf) {
        if (isZero(vec3f.x) && isZero(vec3f.y) && isZero(vec3f.z)) {
            return new Vec3f((Vector3f) vec3f);
        }
        float f = (((-mf.f1097x) * vec3f.x) - (mf.f1098y * vec3f.y)) - (mf.f1099z * vec3f.z);
        float f2 = ((mf.f1096w * vec3f.x) + (mf.f1098y * vec3f.z)) - (mf.f1099z * vec3f.y);
        float f3 = ((mf.f1096w * vec3f.y) + (mf.f1099z * vec3f.x)) - (mf.f1097x * vec3f.z);
        float f4 = ((mf.f1096w * vec3f.z) + (mf.f1097x * vec3f.y)) - (mf.f1098y * vec3f.x);
        float f5 = (mf.f1099z * mf.f1099z) + (mf.f1096w * mf.f1096w) + (mf.f1097x * mf.f1097x) + (mf.f1098y * mf.f1098y);
        Vec3f vec3f2 = new Vec3f();
        vec3f2.x = ((((mf.f1096w * f2) - (mf.f1097x * f)) - (mf.f1099z * f3)) + (mf.f1098y * f4)) / f5;
        vec3f2.y = ((((mf.f1096w * f3) - (mf.f1098y * f)) - (mf.f1097x * f4)) + (mf.f1099z * f2)) / f5;
        vec3f2.z = ((((mf.f1096w * f4) - (mf.f1099z * f)) - (mf.f1098y * f2)) + (mf.f1097x * f3)) / f5;
        if (((double) Math.abs(((((f * mf.f1096w) + (f2 * mf.f1097x)) + (mf.f1098y * f3)) + (mf.f1099z * f4)) / f5)) <= 0.1d) {
            return vec3f2;
        }
        vec3f2.x = vec3f.x;
        vec3f2.y = vec3f.y;
        vec3f2.z = vec3f.z;
        return vec3f2;
    }

    /* renamed from: a */
    public static void m6938a(C0840MF mf, Vec3f vec3f, Tuple3f tuple3f) {
        if (!isZero(vec3f.x) || !isZero(vec3f.y) || !isZero(vec3f.z)) {
            float f = (((-mf.f1097x) * vec3f.x) - (mf.f1098y * vec3f.y)) - (mf.f1099z * vec3f.z);
            float f2 = ((mf.f1096w * vec3f.x) + (mf.f1098y * vec3f.z)) - (mf.f1099z * vec3f.y);
            float f3 = ((mf.f1096w * vec3f.y) + (mf.f1099z * vec3f.x)) - (mf.f1097x * vec3f.z);
            float f4 = ((mf.f1096w * vec3f.z) + (mf.f1097x * vec3f.y)) - (mf.f1098y * vec3f.x);
            float f5 = (mf.f1096w * mf.f1096w) + (mf.f1097x * mf.f1097x) + (mf.f1098y * mf.f1098y) + (mf.f1099z * mf.f1099z);
            tuple3f.x = ((((mf.f1096w * f2) - (mf.f1097x * f)) - (mf.f1099z * f3)) + (mf.f1098y * f4)) / f5;
            tuple3f.y = ((((mf.f1096w * f3) - (mf.f1098y * f)) - (mf.f1097x * f4)) + (mf.f1099z * f2)) / f5;
            tuple3f.z = ((((mf.f1096w * f4) - (mf.f1099z * f)) - (mf.f1098y * f2)) + (mf.f1097x * f3)) / f5;
            if (((double) Math.abs(((((f * mf.f1096w) + (f2 * mf.f1097x)) + (mf.f1098y * f3)) + (mf.f1099z * f4)) / f5)) > 0.1d) {
                tuple3f.x = vec3f.x;
                tuple3f.y = vec3f.y;
                tuple3f.z = vec3f.z;
                return;
            }
            return;
        }
        tuple3f.x = vec3f.x;
        tuple3f.y = vec3f.y;
        tuple3f.x = vec3f.z;
    }

    /* renamed from: a */
    public static C0840MF m6936a(C0840MF mf, C0840MF mf2, float f) {
        C0840MF mf3;
        float sin;
        C0840MF bhs = mf.bhs();
        C0840MF bhs2 = mf2.bhs();
        double d = (double) ((bhs.f1096w * bhs2.f1096w) + (bhs.f1097x * bhs2.f1097x) + (bhs.f1098y * bhs2.f1098y) + (bhs.f1099z * bhs2.f1099z));
        if (d < ScriptRuntime.NaN) {
            mf3 = new C0840MF(-bhs2.f1096w, -bhs2.f1097x, -bhs2.f1098y, -bhs2.f1099z);
            d = -d;
        } else {
            mf3 = bhs2;
        }
        if (d > 0.9998999834060669d) {
            sin = 1.0f - f;
        } else {
            float sqrt = (float) Math.sqrt(1.0d - (d * d));
            float atan2 = (float) Math.atan2((double) sqrt, d);
            float f2 = 1.0f / sqrt;
            sin = (float) (Math.sin((double) ((1.0f - f) * atan2)) * ((double) f2));
            f = (float) (Math.sin((double) (atan2 * f)) * ((double) f2));
        }
        return new C0840MF((bhs.f1096w * sin) + (mf3.f1096w * f), (bhs.f1097x * sin) + (mf3.f1097x * f), (bhs.f1098y * sin) + (mf3.f1098y * f), (sin * bhs.f1099z) + (mf3.f1099z * f));
    }

    static boolean isZero(float f) {
        return f < 1.0E-5f && f > -1.0E-5f;
    }

    public double bho() {
        return Math.sqrt((double) ((this.f1096w * this.f1096w) + (this.f1097x * this.f1097x) + (this.f1098y * this.f1098y) + (this.f1099z * this.f1099z)));
    }

    public double bhp() {
        return (double) ((this.f1096w * this.f1096w) + (this.f1097x * this.f1097x) + (this.f1098y * this.f1098y) + (this.f1099z * this.f1099z));
    }

    public C0840MF bhq() {
        return new C0840MF(this.f1096w, -this.f1097x, -this.f1098y, -this.f1099z);
    }

    public C0840MF bhr() {
        double bho = bho();
        return new C0840MF((float) (((double) this.f1096w) / bho), -((float) (((double) this.f1097x) / bho)), -((float) (((double) this.f1098y) / bho)), -((float) (((double) this.f1099z) / bho)));
    }

    public C0840MF bhs() {
        double bhp = bhp();
        if (isZero(1.0f - ((float) bhp))) {
            return this;
        }
        double sqrt = Math.sqrt(bhp);
        return new C0840MF((float) (((double) this.f1096w) / sqrt), (float) (((double) this.f1097x) / sqrt), (float) (((double) this.f1098y) / sqrt), (float) (((double) this.f1099z) / sqrt));
    }

    public boolean isNormalized() {
        if (isZero(1.0f - ((float) bhp()))) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public C0840MF mo3886a(C0840MF mf) {
        return new C0840MF((((this.f1096w * mf.f1096w) - (this.f1097x * mf.f1097x)) - (this.f1098y * mf.f1098y)) - (this.f1099z * mf.f1099z), (((this.f1096w * mf.f1097x) + (this.f1097x * mf.f1096w)) + (this.f1098y * mf.f1099z)) - (this.f1099z * mf.f1098y), (((this.f1096w * mf.f1098y) + (this.f1098y * mf.f1096w)) + (this.f1099z * mf.f1097x)) - (this.f1097x * mf.f1099z), (((this.f1096w * mf.f1099z) + (this.f1099z * mf.f1096w)) + (this.f1097x * mf.f1098y)) - (this.f1098y * mf.f1097x));
    }

    /* renamed from: b */
    public C0840MF mo3890b(C0840MF mf) {
        return mo3886a(mf).bhs();
    }

    /* renamed from: gm */
    public C0840MF mo3911gm(float f) {
        return new C0840MF(this.f1096w * f, this.f1097x * f, this.f1098y * f, this.f1099z * f);
    }

    public float bht() {
        return ((float) Math.acos((double) bhs().f1096w)) * 2.0f;
    }

    public float bhu() {
        return (float) (((double) (bht() * 180.0f)) / 3.141592653589793d);
    }

    public Vec3f bhv() {
        Vec3f vec3f = new Vec3f();
        C0840MF bhs = bhs();
        float sqrt = (float) Math.sqrt((double) (1.0f - (bhs.f1096w * bhs.f1096w)));
        if (Math.abs(sqrt) < 5.0E-5f) {
            vec3f.x = 1.0f;
            vec3f.z = 0.0f;
            vec3f.y = 0.0f;
        } else {
            vec3f.x = bhs.f1097x / sqrt;
            vec3f.y = bhs.f1098y / sqrt;
            vec3f.z = bhs.f1099z / sqrt;
        }
        return vec3f;
    }

    public float[] bhw() {
        float[] fArr = new float[3];
        double d = (double) ((this.f1097x * this.f1098y) + (this.f1099z * this.f1096w));
        if (d > 0.4999500000012631d) {
            fArr[1] = (float) (2.0d * Math.atan2((double) this.f1097x, (double) this.f1096w));
            fArr[2] = 1.5707964f;
            fArr[0] = 0.0f;
        } else if (d < -0.4999500000012631d) {
            fArr[1] = (float) (-2.0d * Math.atan2((double) this.f1097x, (double) this.f1096w));
            fArr[2] = -1.5707964f;
            fArr[0] = 0.0f;
        } else {
            double d2 = (double) (this.f1097x * this.f1097x);
            double d3 = (double) (this.f1098y * this.f1098y);
            double d4 = (double) (this.f1099z * this.f1099z);
            fArr[0] = (float) Math.atan2((double) (((2.0f * this.f1097x) * this.f1096w) - ((2.0f * this.f1098y) * this.f1099z)), (1.0d - (d2 * 2.0d)) - (2.0d * d4));
            fArr[1] = (float) Math.atan2((double) (((2.0f * this.f1098y) * this.f1096w) - ((2.0f * this.f1097x) * this.f1099z)), (1.0d - (d3 * 2.0d)) - (d4 * 2.0d));
            fArr[2] = (float) Math.asin(d * 2.0d);
        }
        return fArr;
    }

    public float[] bhx() {
        float[] bhw = bhw();
        bhw[0] = (float) (((double) (bhw[0] * 180.0f)) / 3.141592653589793d);
        bhw[1] = (float) (((double) (bhw[1] * 180.0f)) / 3.141592653589793d);
        bhw[2] = (float) (((double) (bhw[2] * 180.0f)) / 3.141592653589793d);
        return bhw;
    }

    /* renamed from: c */
    public C0840MF mo3906c(C0840MF mf) {
        return new C0840MF(this.f1096w + mf.f1096w, this.f1097x + mf.f1097x, this.f1098y + mf.f1098y, this.f1099z + mf.f1099z);
    }

    public C0840MF bhy() {
        return new C0840MF(-this.f1096w, -this.f1097x, -this.f1098y, -this.f1099z);
    }

    /* renamed from: d */
    public boolean mo3908d(C0840MF mf) {
        return bhs().equals(mf.bhs());
    }

    public Matrix4fWrap bhz() {
        Matrix4fWrap ajk = new Matrix4fWrap();
        C0840MF bhs = bhs();
        float f = bhs.f1097x * bhs.f1097x;
        float f2 = bhs.f1097x * bhs.f1098y;
        float f3 = bhs.f1097x * bhs.f1099z;
        float f4 = bhs.f1096w * bhs.f1097x;
        float f5 = bhs.f1098y * bhs.f1098y;
        float f6 = bhs.f1098y * bhs.f1099z;
        float f7 = bhs.f1098y * bhs.f1096w;
        float f8 = bhs.f1099z * bhs.f1099z;
        float f9 = bhs.f1096w * bhs.f1099z;
        ajk.m00 = 1.0f - ((f5 + f8) * 2.0f);
        ajk.m01 = (f2 - f9) * 2.0f;
        ajk.m02 = (f3 + f7) * 2.0f;
        ajk.m10 = (f9 + f2) * 2.0f;
        ajk.m11 = 1.0f - ((f + f8) * 2.0f);
        ajk.m12 = (f6 - f4) * 2.0f;
        ajk.m20 = (f3 - f7) * 2.0f;
        ajk.m21 = (f6 + f4) * 2.0f;
        ajk.m22 = 1.0f - ((f + f5) * 2.0f);
        ajk.m32 = 0.0f;
        ajk.m31 = 0.0f;
        ajk.m30 = 0.0f;
        ajk.m23 = 0.0f;
        ajk.m13 = 0.0f;
        ajk.m03 = 0.0f;
        ajk.m33 = 1.0f;
        return ajk;
    }

    /* renamed from: d */
    public void mo3907d(Matrix4fWrap ajk) {
        float f = this.f1096w;
        float f2 = this.f1097x;
        float f3 = this.f1098y;
        float f4 = this.f1099z;
        double d = (double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4));
        if (!isZero(1.0f - ((float) d))) {
            double sqrt = 1.0d / Math.sqrt(d);
            f = (float) (((double) f) * sqrt);
            f2 = (float) (((double) f2) * sqrt);
            f3 = (float) (((double) f3) * sqrt);
            f4 = (float) (sqrt * ((double) f4));
        }
        float f5 = f2 * f2;
        float f6 = f2 * f3;
        float f7 = f2 * f4;
        float f8 = f2 * f;
        float f9 = f3 * f3;
        float f10 = f3 * f4;
        float f11 = f3 * f;
        float f12 = f4 * f4;
        float f13 = f4 * f;
        ajk.m00 = 1.0f - ((f9 + f12) * 2.0f);
        ajk.m01 = (f6 - f13) * 2.0f;
        ajk.m02 = (f7 + f11) * 2.0f;
        ajk.m10 = (f13 + f6) * 2.0f;
        ajk.m11 = 1.0f - ((f5 + f12) * 2.0f);
        ajk.m12 = (f10 - f8) * 2.0f;
        ajk.m20 = (f7 - f11) * 2.0f;
        ajk.m21 = (f10 + f8) * 2.0f;
        ajk.m22 = 1.0f - ((f5 + f9) * 2.0f);
        ajk.m32 = 0.0f;
        ajk.m31 = 0.0f;
        ajk.m30 = 0.0f;
        ajk.m23 = 0.0f;
        ajk.m13 = 0.0f;
        ajk.m03 = 0.0f;
        ajk.m33 = 1.0f;
    }

    /* renamed from: e */
    public void mo3909e(Matrix4fWrap ajk) {
        float f = this.f1096w;
        float f2 = this.f1097x;
        float f3 = this.f1098y;
        float f4 = this.f1099z;
        double d = (double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4));
        if (!isZero(1.0f - ((float) d))) {
            double sqrt = 1.0d / Math.sqrt(d);
            f = (float) (((double) f) * sqrt);
            f2 = (float) (((double) f2) * sqrt);
            f3 = (float) (((double) f3) * sqrt);
            f4 = (float) (sqrt * ((double) f4));
        }
        float f5 = f2 * f2;
        float f6 = f2 * f3;
        float f7 = f2 * f4;
        float f8 = f2 * f;
        float f9 = f3 * f3;
        float f10 = f3 * f4;
        float f11 = f3 * f;
        float f12 = f4 * f4;
        float f13 = f4 * f;
        ajk.m00 = 1.0f - ((f9 + f12) * 2.0f);
        ajk.m01 = (f6 - f13) * 2.0f;
        ajk.m02 = (f7 + f11) * 2.0f;
        ajk.m10 = (f13 + f6) * 2.0f;
        ajk.m11 = 1.0f - ((f5 + f12) * 2.0f);
        ajk.m12 = (f10 - f8) * 2.0f;
        ajk.m20 = (f7 - f11) * 2.0f;
        ajk.m21 = (f10 + f8) * 2.0f;
        ajk.m22 = 1.0f - ((f5 + f9) * 2.0f);
    }

    /* renamed from: X */
    public Vec3f mo3883X(Vec3f vec3f) {
        return m6937a(vec3f, this);
    }

    /* renamed from: Y */
    public Vec3f mo3884Y(Vec3f vec3f) {
        if (isZero(vec3f.x) && isZero(vec3f.y) && isZero(vec3f.z)) {
            return new Vec3f((Vector3f) vec3f);
        }
        try {
            return m6937a(vec3f, bhq());
        } catch (Exception e) {
            System.err.println("[ERROR] Quaternionf : rotateByConjugate: conjugate->" + bhq().toString() + ", vec->" + vec3f.toString() + ". Returning same vec ");
            return new Vec3f((Vector3f) vec3f);
        }
    }

    /* renamed from: a */
    public void mo3888a(Vec3f vec3f, Tuple3f tuple3f) {
        m6938a(this, vec3f, tuple3f);
    }

    /* renamed from: Z */
    public Vec3f mo3885Z(Vec3f vec3f) {
        float f = this.f1096w;
        float f2 = this.f1097x;
        float f3 = this.f1098y;
        float f4 = this.f1099z;
        float sqrt = (float) Math.sqrt((double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4)));
        float f5 = f / sqrt;
        float f6 = f2 / sqrt;
        float f7 = f3 / sqrt;
        float f8 = f4 / sqrt;
        float f9 = f5 * f6;
        float f10 = f5 * f7;
        float f11 = f5 * f8;
        float f12 = (-f6) * f6;
        float f13 = f6 * f7;
        float f14 = f6 * f8;
        float f15 = (-f7) * f7;
        float f16 = f7 * f8;
        float f17 = f8 * (-f8);
        float f18 = vec3f.x;
        float f19 = vec3f.y;
        float f20 = vec3f.z;
        return new Vec3f((2.0f * (((f15 + f16) * f18) + ((f13 - f11) * f19) + ((f10 + f14) * f20))) + f18, ((((f11 + f13) * f18) + ((f17 + f12) * f19) + ((f16 - f9) * f20)) * 2.0f) + f19, ((((f14 - f10) * f18) + ((f16 + f9) * f19) + ((f12 + f15) * f20)) * 2.0f) + f20);
    }

    /* renamed from: G */
    public Vec3d mo3882G(Vec3d ajr) {
        float f = this.f1096w;
        float f2 = this.f1097x;
        float f3 = this.f1098y;
        float f4 = this.f1099z;
        float sqrt = (float) Math.sqrt((double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4)));
        float f5 = f / sqrt;
        float f6 = f2 / sqrt;
        float f7 = f3 / sqrt;
        float f8 = f4 / sqrt;
        float f9 = f5 * f6;
        float f10 = f5 * f7;
        float f11 = f5 * f8;
        float f12 = (-f6) * f6;
        float f13 = f6 * f7;
        float f14 = f6 * f8;
        float f15 = (-f7) * f7;
        float f16 = f7 * f8;
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        return new Vec3d((2.0d * ((((double) (f15 + f16)) * d) + (((double) (f13 - f11)) * d2) + (((double) (f10 + f14)) * d3))) + d, (((((double) (f12 + ((-f8) * f8))) * d2) + (((double) (f11 + f13)) * d) + (((double) (f16 - f9)) * d3)) * 2.0d) + d2, (((((double) (f14 - f10)) * d) + (((double) (f9 + f16)) * d2) + (((double) (f12 + f15)) * d3)) * 2.0d) + d3);
    }

    /* renamed from: q */
    public void mo3914q(Vec3f vec3f, Vec3f vec3f2) {
        float f = this.f1096w;
        float f2 = this.f1097x;
        float f3 = this.f1098y;
        float f4 = this.f1099z;
        float sqrt = (float) Math.sqrt((double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4)));
        float f5 = f / sqrt;
        float f6 = f2 / sqrt;
        float f7 = f3 / sqrt;
        float f8 = f4 / sqrt;
        float f9 = f5 * f6;
        float f10 = f5 * f7;
        float f11 = f5 * f8;
        float f12 = (-f6) * f6;
        float f13 = f6 * f7;
        float f14 = f6 * f8;
        float f15 = (-f7) * f7;
        float f16 = f7 * f8;
        float f17 = vec3f.x;
        float f18 = vec3f.y;
        float f19 = vec3f.z;
        vec3f2.x = (2.0f * (((f15 + f16) * f17) + ((f13 - f11) * f18) + ((f10 + f14) * f19))) + f17;
        vec3f2.y = ((((f11 + f13) * f17) + (((f8 * (-f8)) + f12) * f18) + ((f16 - f9) * f19)) * 2.0f) + f18;
        vec3f2.z = ((((f14 - f10) * f17) + ((f16 + f9) * f18) + ((f12 + f15) * f19)) * 2.0f) + f19;
    }

    /* renamed from: a */
    public C0840MF mo3887a(C0840MF mf, float f) {
        return m6936a(this, mf, f);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C0840MF)) {
            return false;
        }
        C0840MF mf = (C0840MF) obj;
        if (!isZero(this.f1096w - mf.f1096w) || !isZero(this.f1097x - mf.f1097x) || !isZero(this.f1098y - mf.f1098y) || !isZero(this.f1099z - mf.f1099z)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "[ (" + this.f1096w + ") + (" + this.f1097x + ")*i + (" + this.f1098y + ")*j + (" + this.f1099z + ")*k ]";
    }

    public String bhA() {
        String valueOf;
        String valueOf2;
        String valueOf3;
        String str = "0.0";
        if (isZero(this.f1096w)) {
            valueOf = str;
        } else {
            valueOf = String.valueOf(this.f1096w);
        }
        if (isZero(this.f1097x)) {
            valueOf2 = str;
        } else {
            valueOf2 = String.valueOf(this.f1097x);
        }
        if (isZero(this.f1098y)) {
            valueOf3 = str;
        } else {
            valueOf3 = String.valueOf(this.f1098y);
        }
        if (!isZero(this.f1099z)) {
            str = String.valueOf(this.f1099z);
        }
        return "[ (" + valueOf + ") + (" + valueOf2 + ")*i + (" + valueOf3 + ")*j + (" + str + ")*k ]";
    }

    public String bhB() {
        return "[ Axis: " + bhv() + " Angle: " + bhu() + "° ]";
    }

    public String bhC() {
        float[] bhx = bhx();
        return "[ Roll: " + bhx[2] + " Pitch: " + bhx[0] + " Yaw: " + bhx[1] + " ]";
    }

    public int hashCode() {
        return (int) ((((((this.f1096w * 31.0f) + this.f1097x) * 31.0f) + this.f1098y) * 31.0f) + this.f1099z);
    }

    public boolean anA() {
        float f = this.f1096w + this.f1097x + this.f1098y + this.f1099z;
        return (Float.isNaN(f) || f == Float.NEGATIVE_INFINITY || f == Float.POSITIVE_INFINITY) ? false : true;
    }
}
