package game.network.message.serializable;

import logic.res.html.C0911NN;

import java.io.Serializable;

/* renamed from: a.Lx */
/* compiled from: a */
public class C0828Lx implements C0911NN, Serializable {

    private final Object value;
    private C0829a dvk;

    public C0828Lx(Object obj) {
        this.value = obj;
    }

    public boolean evaluate(Object obj) {
        if (this.value instanceof Class) {
            return ((Class) this.value).isInstance(obj);
        }
        if (obj == null && this.value == null) {
            return true;
        }
        if (obj == null || this.value == null) {
            return false;
        }
        if (this.dvk == C0829a.LIKE) {
            return ((String) obj).toLowerCase().indexOf(((String) this.value).toLowerCase()) != -1;
        }
        if (this.dvk == C0829a.EQUALS) {
            return obj.equals(this.value);
        }
        return false;
    }

    public void bfL() {
        this.dvk = C0829a.LIKE;
    }

    public void bfM() {
        this.dvk = C0829a.EQUALS;
    }

    /* renamed from: a.Lx$a */
    private enum C0829a implements Serializable {
        LIKE,
        EQUALS
    }
}
