package game.network.message.serializable;

import logic.baa.C1616Xf;
import logic.baa.aDJ;
import p001a.C0665JT;
import p001a.C3971xY;
import p001a.C6417amZ;
import p001a.C6859auz;

import java.io.Serializable;
import java.util.*;

/* renamed from: a.aMe  reason: case insensitive filesystem */
/* compiled from: a */
public class C5512aMe implements C0665JT, C6417amZ, C6859auz, C3971xY, Serializable {


    /* renamed from: Uk */
    private C1616Xf f3385Uk;
    private long agR;
    private Map<String, Object> apQ = new HashMap();
    private C5985aeJ fqt;
    private C5985aeJ iom;
    private C5512aMe ion;
    private Set<C5512aMe> ioo;
    private Class<? extends aDJ> iop;
    private C5512aMe ioq;
    private C5512aMe ior;
    private C5512aMe ios;

    public <T> T get(String str) {
        T t = this.apQ.get(str);
        if (t instanceof C5512aMe) {
            return ((C5512aMe) t).diP();
        }
        return t;
    }

    private C5512aMe diP() {
        return this.ion == null ? this : this.ion.diP();
    }

    public void setField(String str, Object obj) {
        if (obj == null) {
            this.apQ.remove(str);
        } else {
            this.apQ.put(str, obj);
        }
    }

    /* renamed from: Ej */
    public long mo10072Ej() {
        return this.agR;
    }

    /* renamed from: lh */
    public void mo10093lh(long j) {
        this.agR = j;
    }

    public C5985aeJ diQ() {
        return this.iom;
    }

    /* renamed from: b */
    public void mo10078b(C5985aeJ aej) {
        this.iom = aej;
    }

    public Class<?> diR() {
        if (this.iop != null) {
            return this.iop;
        }
        return bUE().getClazz();
    }

    /* renamed from: g */
    public void mo10090g(C1616Xf xf) {
        this.f3385Uk = xf;
        if (this.ioq != null) {
            this.ioq.f3385Uk = xf;
        }
        if (this.ior != null) {
            this.ior.f3385Uk = xf;
        }
        if (this.ios != null) {
            this.ios.f3385Uk = xf;
        }
    }

    public C5985aeJ bUE() {
        if (this.fqt == null) {
            this.fqt = this.iom.bUE();
            if (this.fqt == null) {
                this.fqt = this.iom;
            }
        }
        return this.fqt;
    }

    /* renamed from: yz */
    public C1616Xf mo10098yz() {
        return diP().f3385Uk;
    }

    public String getVersion() {
        return this.iom.getVersion();
    }

    public String toString() {
        return "ScriptDataHolder:" + mo10072Ej() + ":" + this.iom;
    }

    /* renamed from: kf */
    public C6859auz mo10092kf(String str) {
        return (C6859auz) get(str);
    }

    /* renamed from: nf */
    public Object mo10094nf(String str) {
        Object obj = get(str);
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    public void setClass(Class<? extends aDJ> cls) {
        this.iop = cls;
    }

    public String bav() {
        return diQ().getClassName();
    }

    /* renamed from: i */
    public boolean mo3116i(int i, int i2, int i3) {
        return this.iom.bUC().mo20589a(i, i2, i3) > 0;
    }

    /* renamed from: j */
    public boolean mo3117j(int i, int i2, int i3) {
        return this.iom.bUC().mo20589a(i, i2, i3) < 0;
    }

    /* renamed from: c */
    public void mo10081c(C5512aMe ame) {
        this.ioq = ame;
    }

    /* renamed from: d */
    public void mo10083d(C5512aMe ame) {
        this.ior = ame;
    }

    /* renamed from: e */
    public void mo10088e(C5512aMe ame) {
        this.ios = ame;
    }

    public C0665JT api() {
        return this.ioq;
    }

    public C0665JT apg() {
        return this.ior;
    }

    public C0665JT aph() {
        return this.ios;
    }

    public boolean ape() {
        return this.ior == this;
    }

    public boolean apf() {
        return this.ios == this;
    }

    public Object baw() {
        return this.f3385Uk;
    }

    /* renamed from: c */
    public void mo10080c(C0665JT jt) {
        this.ion = (C5512aMe) jt;
        C5512aMe diP = diP();
        if (this.ioo != null) {
            if (this.ioo.contains(diP)) {
                this.ion = null;
                throw new RuntimeException("Clycle detected at " + this);
            }
            if (diP.ioo == null) {
                this.ioo = new HashSet();
            }
            diP.ioo.addAll(this.ioo);
            this.ioo = null;
        } else if (diP.ioo == null) {
            diP.ioo = new HashSet();
        }
        diP.ioo.add(this);
        if (this.ioq != null) {
            diP.ioo.add(this.ioq);
        }
        if (this.ios != null) {
            diP.ioo.add(this.ios);
        }
        if (this.ior != null) {
            diP.ioo.add(this.ior);
        }
    }

    public boolean diS() {
        return this.ion != null;
    }

    public Collection<C0665JT> ckr() {
        return this.ioo;
    }

    public String bau() {
        return bUE().getClassName();
    }

    public void free() {
        this.apQ.clear();
        this.iom = null;
        this.ion = null;
        this.ioo = null;
        this.ioq = null;
        this.ior = null;
        this.ios = null;
    }

    public boolean diT() {
        return this.apQ == null;
    }

    /* renamed from: eE */
    public C0665JT mo3113eE(String str) {
        return (C0665JT) get(str);
    }

    /* renamed from: iU */
    public boolean mo10091iU(String str) {
        if (this.iom == null || this.iom.bUD().get(str) == null) {
            return false;
        }
        return true;
    }
}
