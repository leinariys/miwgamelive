package game.network.message.serializable;

import com.hoplon.geometry.Vec3f;
import p001a.C0647JL;
import p001a.C1245SP;
import p001a.C6537aop;

import javax.vecmath.Vector3f;
import java.io.Serializable;

/* renamed from: a.adJ  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5933adJ implements Serializable {
    /* renamed from: jR */
    static final /* synthetic */ boolean f4297jR = (!C5933adJ.class.desiredAssertionStatus());
    private static /* synthetic */ int[] fnk;
    public final Vec3f scaling = new Vec3f(1.0f, 1.0f, 1.0f);
    /* renamed from: BD */
    private C5737aUv f4298BD = new C5737aUv();

    static /* synthetic */ int[] bUd() {
        int[] iArr = fnk;
        if (iArr == null) {
            iArr = new int[C6537aop.values().length];
            try {
                iArr[C6537aop.PHY_DOUBLE.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C6537aop.PHY_FIXEDPOINT88.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C6537aop.PHY_FLOAT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C6537aop.PHY_INTEGER.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C6537aop.PHY_SHORT.ordinal()] = 4;
            } catch (NoSuchFieldError e5) {
            }
            fnk = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public abstract void mo11091a(C5737aUv auv, int i);

    /* renamed from: b */
    public abstract void mo11094b(C5737aUv auv, int i);

    public abstract int bUc();

    /* renamed from: qg */
    public abstract void mo11097qg(int i);

    /* renamed from: qh */
    public abstract void mo11098qh(int i);

    /* renamed from: qi */
    public abstract void mo11099qi(int i);

    /* renamed from: qj */
    public abstract void mo11100qj(int i);

    /* renamed from: a */
    public void mo12767a(C1245SP sp, Vec3f vec3f, Vec3f vec3f2) {
        int i = 0;
        int bUc = bUc();
        Vec3f[] vec3fArr = {new Vec3f(), new Vec3f(), new Vec3f()};
        Vec3f vec3f3 = new Vec3f((Vector3f) getScaling());
        for (int i2 = 0; i2 < bUc; i2++) {
            mo11094b(this.f4298BD, i2);
            i += this.f4298BD.iXn * 3;
            switch (bUd()[this.f4298BD.iXo.ordinal()]) {
                case 3:
                    for (int i3 = 0; i3 < this.f4298BD.iXn; i3++) {
                        int i4 = this.f4298BD.iXm * i3;
                        int i5 = this.f4298BD.iXl.getInt(i4 + 0) * this.f4298BD.stride;
                        vec3fArr[0].set(this.f4298BD.iXi.getFloat(i5 + 0) * vec3f3.x, this.f4298BD.iXi.getFloat(i5 + 4) * vec3f3.y, this.f4298BD.iXi.getFloat(i5 + 8) * vec3f3.z);
                        int i6 = this.f4298BD.iXl.getInt(i4 + 4) * this.f4298BD.stride;
                        vec3fArr[1].set(this.f4298BD.iXi.getFloat(i6 + 0) * vec3f3.x, this.f4298BD.iXi.getFloat(i6 + 4) * vec3f3.y, this.f4298BD.iXi.getFloat(i6 + 8) * vec3f3.z);
                        int i7 = this.f4298BD.iXl.getInt(i4 + 8) * this.f4298BD.stride;
                        vec3fArr[2].set(this.f4298BD.iXi.getFloat(i7 + 0) * vec3f3.x, this.f4298BD.iXi.getFloat(i7 + 4) * vec3f3.y, this.f4298BD.iXi.getFloat(i7 + 8) * vec3f3.z);
                        sp.mo819b(vec3fArr, i2, i3);
                    }
                    break;
                case 4:
                    for (int i8 = 0; i8 < this.f4298BD.iXn; i8++) {
                        int i9 = this.f4298BD.iXm * i8;
                        int i10 = (this.f4298BD.iXl.getShort(i9 + 0) & 65535) * this.f4298BD.stride;
                        vec3fArr[0].set(this.f4298BD.iXi.getFloat(i10 + 0) * vec3f3.x, this.f4298BD.iXi.getFloat(i10 + 4) * vec3f3.y, this.f4298BD.iXi.getFloat(i10 + 8) * vec3f3.z);
                        int i11 = (this.f4298BD.iXl.getShort(i9 + 2) & 65535) * this.f4298BD.stride;
                        vec3fArr[1].set(this.f4298BD.iXi.getFloat(i11 + 0) * vec3f3.x, this.f4298BD.iXi.getFloat(i11 + 4) * vec3f3.y, this.f4298BD.iXi.getFloat(i11 + 8) * vec3f3.z);
                        int i12 = (this.f4298BD.iXl.getShort(i9 + 4) & 65535) * this.f4298BD.stride;
                        vec3fArr[2].set(this.f4298BD.iXi.getFloat(i12 + 0) * vec3f3.x, this.f4298BD.iXi.getFloat(i12 + 4) * vec3f3.y, this.f4298BD.iXi.getFloat(i12 + 8) * vec3f3.z);
                        sp.mo819b(vec3fArr, i2, i8);
                    }
                    break;
                default:
                    if (!(f4297jR || this.f4298BD.iXo == C6537aop.PHY_INTEGER || this.f4298BD.iXo == C6537aop.PHY_SHORT)) {
                        throw new AssertionError();
                    }
            }
            mo11098qh(i2);
        }
        this.f4298BD.dAi();
    }

    /* renamed from: x */
    public void mo12770x(Vec3f vec3f, Vec3f vec3f2) {
        C1868a aVar = new C1868a((C1868a) null);
        vec3f.set(-1.0E30f, -1.0E30f, -1.0E30f);
        vec3f2.set(1.0E30f, 1.0E30f, 1.0E30f);
        mo12767a(aVar, vec3f, vec3f2);
        vec3f.set(aVar.cam);
        vec3f2.set(aVar.can);
    }

    public Vec3f getScaling() {
        return this.scaling;
    }

    public void setScaling(Vec3f vec3f) {
        this.scaling.set(vec3f);
    }

    /* renamed from: a.adJ$a */
    private static class C1868a implements C1245SP {
        public final Vec3f cam;
        public final Vec3f can;

        private C1868a() {
            this.cam = new Vec3f(1.0E30f, 1.0E30f, 1.0E30f);
            this.can = new Vec3f(-1.0E30f, -1.0E30f, -1.0E30f);
        }

        /* synthetic */ C1868a(C1868a aVar) {
            this();
        }

        /* renamed from: b */
        public void mo819b(Vec3f[] vec3fArr, int i, int i2) {
            C0647JL.m5605o(this.cam, vec3fArr[0]);
            C0647JL.m5606p(this.can, vec3fArr[0]);
            C0647JL.m5605o(this.cam, vec3fArr[1]);
            C0647JL.m5606p(this.can, vec3fArr[1]);
            C0647JL.m5605o(this.cam, vec3fArr[2]);
            C0647JL.m5606p(this.can, vec3fArr[2]);
        }
    }
}
