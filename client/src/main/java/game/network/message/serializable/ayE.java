package game.network.message.serializable;

import org.w3c.css.sac.Selector;
import org.w3c.css.sac.SelectorList;

import java.io.Serializable;
import java.util.ArrayList;

/* renamed from: a.ayE */
/* compiled from: a */
public class ayE extends ArrayList<Selector> implements SelectorList, Serializable {


    public int getLength() {
        return size();
    }

    /* renamed from: wg */
    public Selector item(int i) {
        return (Selector) get(i);
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        int length = getLength();
        for (int i = 0; i < length; i++) {
            stringBuffer.append(item(i).toString());
            if (i < length - 1) {
                stringBuffer.append(", ");
            }
        }
        return stringBuffer.toString();
    }
}
