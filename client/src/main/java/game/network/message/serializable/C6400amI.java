package game.network.message.serializable;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import p001a.C0763Kt;
import p001a.C1003Om;

import javax.vecmath.Vector3f;
import java.io.Serializable;

/* renamed from: a.amI  reason: case insensitive filesystem */
/* compiled from: a */
public class C6400amI implements C1003Om, Serializable {
    public static final float cZi = 0.70710677f;
    public static final float cZj = 0.7853982f;
    private static final float fRt = 1.5707964f;

    private final Vec3f gbl = new Vec3f();
    private final Vec3f gbm = new Vec3f();
    private final Vec3f gbn = new Vec3f(0.0f, 0.0f, 0.0f);
    private final Vec3f gbo = new Vec3f(0.0f, 0.0f, 0.0f);
    private final Vec3f gbp = new Vec3f(0.0f, 0.0f, 0.0f);
    private final Vec3f gbq = new Vec3f(0.0f, 0.0f, 0.0f);
    private final Quat4fWrap orientation = new Quat4fWrap();
    /* renamed from: rE */
    private final Vec3f f4905rE = new Vec3f();
    /* renamed from: rF */
    private final Vec3f f4906rF = new Vec3f();
    private final TransformWrap transform = new TransformWrap();
    private long gbk;
    private transient C0763Kt stack = C0763Kt.bcE();

    /* renamed from: b */
    public void mo14819b(C1003Om om) {
        this.transform.mo17344b(om.mo4490IQ());
        blk().mo9484aA(om.blk());
        this.f4906rF.set(om.blm());
        this.f4905rE.set(om.bln());
        this.gbl.set(om.blo());
        this.gbm.set(om.blp());
        this.orientation.mo15242f(om.bll());
        this.gbn.set(om.bls());
        this.gbq.set(om.blt());
        this.gbp.set(om.blr());
        this.gbo.set(om.blq());
    }

    public Vec3f bln() {
        return this.f4905rE;
    }

    public Vec3f blm() {
        return this.f4906rF;
    }

    public Vec3f blo() {
        return this.gbl;
    }

    public Vec3f blp() {
        return this.gbm;
    }

    public Quat4fWrap bll() {
        return this.orientation;
    }

    public Vec3d blk() {
        return this.transform.position;
    }

    public String toString() {
        return "P=" + blk() + " O=" + bll() + " LV=" + blm() + " LA" + blo();
    }

    /* renamed from: gC */
    public void mo14822gC(long j) {
        this.gbk = j;
    }

    public Vec3f blq() {
        return this.gbo;
    }

    public Vec3f blr() {
        return this.gbp;
    }

    public boolean anA() {
        return blk().anA() && this.orientation.anA() && this.f4906rF.anA() && this.f4905rE.anA() && this.gbl.anA() && this.gbm.anA();
    }

    /* renamed from: b */
    public void mo14818b(float f, C1003Om om) {
        mo14820c(f, om);
        m23868a(this.transform, this.gbo, this.gbq, f, om.mo4490IQ());
        om.bll().mo15236e(om.mo4490IQ().orientation);
        om.mo4490IQ().orientation.mo13947a((Vector3f) om.blt(), (Vector3f) om.bln());
        om.mo4490IQ().orientation.mo13947a((Vector3f) om.blq(), (Vector3f) om.blm());
        om.mo4490IQ().orientation.mo13947a((Vector3f) om.blr(), (Vector3f) om.blo());
    }

    /* renamed from: a */
    public void mo14817a(C1003Om om, float f) {
        if (f != 0.0f) {
            if (f == 1.0f) {
                mo14819b(om);
                return;
            }
            Vec3d blk = om.blk();
            blk().set((blk.x * ((double) f)) + (((double) (1.0f - f)) * blk().x), (blk.y * ((double) f)) + (((double) (1.0f - f)) * blk().y), (blk.z * ((double) f)) + (((double) (1.0f - f)) * blk().z));
            this.orientation.mo15206a(om.bll(), f);
            this.transform.orientation.set(this.orientation);
        }
    }

    /* renamed from: c */
    public void mo14820c(float f, C1003Om om) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            om.blq().scaleAdd(f, this.gbp, this.gbo);
            om.blt().scaleAdd(f, this.stack.bcH().mo4458ac(this.gbn), this.gbq);
            float length = om.blt().length();
            if (length * f > 1.5707964f) {
                om.blt().scale((1.5707964f / f) / length);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    private void m23868a(TransformWrap bcVar, Vec3f vec3f, Vec3f vec3f2, float f, TransformWrap bcVar2) {
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        this.stack.bcN().push();
        try {
            bcVar2.position.mo9478a(f, vec3f, bcVar.position);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            float length = vec3f2.length();
            if (length * f > 0.7853982f) {
                length = 0.7853982f / f;
            }
            if (length < 0.001f) {
                vec3f3.scale((0.5f * f) - (((((f * f) * f) * 0.020833334f) * length) * length), vec3f2);
            } else {
                vec3f3.scale(((float) Math.sin((double) ((0.5f * length) * f))) / length, vec3f2);
            }
            Quat4fWrap a = this.stack.bcN().mo11472a(vec3f3.x, vec3f3.y, vec3f3.z, (float) Math.cos((double) (length * f * 0.5f)));
            Quat4fWrap f2 = ((Quat4fWrap) this.stack.bcN().get()).mo15241f(bcVar.orientation);
            Quat4fWrap aoy = (Quat4fWrap) this.stack.bcN().get();
            aoy.mul(a, f2);
            aoy.normalize();
            bcVar2.mo17335a(aoy);
        } finally {
            this.stack.bcH().pop();
            this.stack.bcN().pop();
        }
    }

    public Vec3f blt() {
        return this.gbq;
    }

    /* renamed from: IQ */
    public TransformWrap mo4490IQ() {
        return this.transform;
    }

    /* renamed from: ab */
    public void mo4491ab(Vec3f vec3f) {
        this.gbo.set(vec3f);
    }

    public Vec3f bls() {
        return this.gbn;
    }

    public void cjS() {
        mo4490IQ().orientation.transform(bln(), blt());
        mo4490IQ().orientation.transform(blp(), bls());
        mo4490IQ().orientation.transform(blm(), blq());
        mo4490IQ().orientation.transform(blo(), blr());
    }

    public long getTimestamp() {
        return this.gbk;
    }
}
