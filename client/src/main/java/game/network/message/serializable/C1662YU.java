package game.network.message.serializable;

import game.network.exception.CreatListenerChannelException;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import logic.res.html.C0911NN;
import logic.res.html.C1867ad;
import logic.res.html.MessageContainer;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* renamed from: a.YU */
/* compiled from: a */
public class C1662YU<T> implements C1867ad<T>, Serializable {

    private final List<T> eMX;
    /* renamed from: Ul */
    private C5663aRz f2174Ul;
    private Set<C0828Lx> eMV;
    private Set<C1662YU<T>> eMW;

    public C1662YU(C1867ad<T> adVar, C5663aRz arz) {
        this.eMV = MessageContainer.dgI();
        this.eMW = MessageContainer.dgI();
        this.eMX = null;
        this.f2174Ul = arz;
    }

    public C1662YU(List<T> list) {
        this.eMV = MessageContainer.dgI();
        this.eMW = MessageContainer.dgI();
        this.eMX = list;
    }

    public C1662YU(Set<T> set) {
        this.eMV = MessageContainer.dgI();
        this.eMW = MessageContainer.dgI();
        throw new CreatListenerChannelException();
    }

    /* renamed from: c */
    public C0911NN mo7225c(T t) {
        C0828Lx lx = new C0828Lx(t);
        this.eMV.add(lx);
        return lx;
    }

    /* renamed from: a */
    public C1867ad<T> mo7224a(C5663aRz arz) {
        C1662YU yu = new C1662YU(this, arz);
        this.eMW.add(yu);
        return yu;
    }

    /* renamed from: dg */
    public Iterator<T> mo7226dg() {
        try {
            return bHX();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Iterator<T> bHX() {
        return new C1663a(this.eMX.iterator());
    }

    /* access modifiers changed from: private */
    public boolean evaluate(Object obj) {
        for (C0828Lx evaluate : this.eMV) {
            if (!evaluate.evaluate(obj)) {
                return false;
            }
        }
        for (C1662YU next : this.eMW) {
            if (obj instanceof C1616Xf) {
                Object w = ((C1616Xf) obj).mo6767w(next.f2174Ul);
                if (!(w instanceof Collection)) {
                    return next.evaluate(w);
                }
                Iterator it = ((Collection) w).iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (next.evaluate(it.next())) {
                            return true;
                        }
                    }
                }
            } else {
                throw new UnsupportedOperationException("Querying a non-ScriptObject list supports only 'constraint'");
            }
        }
        return true;
    }

    /* renamed from: a.YU$a */
    class C1663a implements Iterator<T> {
        private final /* synthetic */ Iterator hfP;
        private T hPK = null;

        C1663a(Iterator it) {
            this.hfP = it;
        }

        public boolean hasNext() {
            while (this.hfP.hasNext()) {
                this.hPK = this.hfP.next();
                try {
                    if (C1662YU.this.evaluate(this.hPK)) {
                        return true;
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
            this.hPK = null;
            return false;
        }

        public T next() {
            return this.hPK;
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
