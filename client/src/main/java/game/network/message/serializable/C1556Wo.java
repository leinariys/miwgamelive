package game.network.message.serializable;

import logic.res.code.C5663aRz;
import logic.res.html.C1867ad;

import java.io.Serializable;
import java.util.Map;

/* renamed from: a.Wo */
/* compiled from: a */
public interface C1556Wo<K, V> extends Map<K, V>, Cloneable, Serializable {
    /* renamed from: XC */
    C1867ad<V> mo5505XC();

    /* renamed from: c */
    void mo5508c(C5663aRz arz);

    Object clone();

    /* renamed from: ym */
    C5663aRz mo5521ym();
}
