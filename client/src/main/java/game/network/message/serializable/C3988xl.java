package game.network.message.serializable;

import game.network.message.externalizable.C2631hn;
import logic.data.mbean.C0677Jd;
import logic.res.html.C0029AO;
import logic.res.html.MessageContainer;
import p001a.C0651JP;
import p001a.C2234dA;
import p001a.C6630aqe;
import p001a.C6786ate;

import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.xl */
/* compiled from: a */
public abstract class C3988xl<T> implements C0029AO<T>, C2234dA<T>, Serializable {

    public final transient C0677Jd bFL;
    public final transient C2631hn<T> bFO;
    public transient boolean bFM;
    public transient boolean bFN;
    public transient Collection<T> collection;
    private transient List<C2991mi<T>> bFH;

    public C3988xl(C0677Jd jd, C2631hn<T> hnVar) {
        this.bFL = jd;
        this.bFO = hnVar;
        this.collection = hnVar.getCollection();
    }

    /* access modifiers changed from: protected */
    public abstract void anC();

    public abstract C0029AO<T> anE();

    public boolean add(T t) {
        anC();
        if (!this.collection.add(t)) {
            return false;
        }
        mo22982a(new C6630aqe(C0651JP.ADD, t));
        return true;
    }

    public boolean remove(Object obj) {
        anC();
        if (!this.collection.remove(obj)) {
            return false;
        }
        mo22982a(new C6630aqe(C0651JP.REMOVE, obj));
        return true;
    }

    public boolean contains(Object obj) {
        return this.collection.contains(obj);
    }

    /* access modifiers changed from: package-private */
    public int anD() {
        List anB = anB();
        if (anB == null) {
            return 0;
        }
        return anB.size();
    }

    public int size() {
        return this.collection.size();
    }

    public boolean addAll(Collection<? extends T> collection2) {
        anC();
        this.collection.addAll(collection2);
        mo22982a(new C6630aqe(C0651JP.ADD_COLLECTION, collection2));
        return collection2.size() > 0;
    }

    public void clear() {
        anC();
        this.collection.clear();
        if (this.bFH != null) {
            this.bFH.clear();
        }
        mo22982a(new C6786ate(C0651JP.CLEAR));
    }

    public boolean containsAll(Collection<?> collection2) {
        return this.collection.containsAll(collection2);
    }

    public boolean isEmpty() {
        return this.collection.isEmpty();
    }

    public Object[] toArray() {
        return this.collection.toArray();
    }

    public <T> T[] toArray(T[] tArr) {
        return this.collection.toArray(tArr);
    }

    public boolean removeAll(Collection<?> collection2) {
        anC();
        List dgK = MessageContainer.init();
        for (Object next : collection2) {
            if (this.collection.remove(next)) {
                dgK.add(next);
            }
        }
        if (dgK.size() <= 0) {
            return false;
        }
        mo22982a(new C6630aqe(C0651JP.REMOVE_COLLECTION, dgK));
        return true;
    }

    public boolean retainAll(Collection<?> collection2) {
        anC();
        List<VALUE> w = MessageContainer.m16004w(this.collection);
        boolean retainAll = this.collection.retainAll(collection2);
        w.removeAll(this.collection);
        if (w.size() > 0) {
            mo22982a(new C6630aqe(C0651JP.REMOVE_COLLECTION, w));
        }
        return retainAll;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo22982a(C2991mi<T> miVar) {
        if (this.bFH == null) {
            this.bFH = MessageContainer.init();
            this.bFL.mo3223v(this.bFO.mo19378ym());
        }
        this.bFH.add(miVar);
    }

    public final List<C2991mi<T>> anB() {
        return this.bFH;
    }

    /* renamed from: l */
    public void mo17726l(T t) {
        anC();
        if (this.collection.add(t)) {
            mo22982a(new C6630aqe(C0651JP.ADD, t));
        }
    }

    /* renamed from: m */
    public boolean mo17727m(T t) {
        anC();
        if (!this.collection.remove(t)) {
            return false;
        }
        mo22982a(new C6630aqe(C0651JP.REMOVE, t));
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Object mo22987b(ObjectOutput objectOutput) {
        return anE();
    }

    public String toString() {
        Iterator it = iterator();
        if (!it.hasNext()) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        while (true) {
            Object next = it.next();
            if (next == this) {
                next = "(this Collection)";
            }
            sb.append(next);
            if (!it.hasNext()) {
                return sb.append(']').toString();
            }
            sb.append(", ");
        }
    }
}
