package game.network.message.serializable;

import java.io.Serializable;

/* renamed from: a.Yo */
/* compiled from: a */
public class C1684Yo implements Serializable {
    private static final long serialVersionUID = -8937443609372774009L;
    private boolean eLS;
    private String name;

    public C1684Yo(String str) {
        this(str, false);
    }

    public C1684Yo(String str, boolean z) {
        this.eLS = false;
        this.name = str;
        this.eLS = z;
    }

    public boolean bHt() {
        return this.eLS;
    }

    /* renamed from: dO */
    public void mo7267dO(boolean z) {
        this.eLS = z;
    }

    public String getName() {
        return this.name;
    }
}
