package game.network.message.serializable;

import com.hoplon.geometry.Vec3f;
import game.script.Actor;
import game.script.item.Weapon;

import java.io.Serializable;

/* renamed from: a.Rw */
/* compiled from: a */
public class C1216Rw implements Serializable {

    private final Vec3f eai = new Vec3f();
    private boolean eaj;
    private boolean eak;
    private boolean eal;
    private Actor eam;
    private Weapon ean;
    private boolean eao;
    private float eap;
    private float roll;

    public void clear() {
        this.eai.set(0.0f, 0.0f, 0.0f);
        this.eaj = false;
        this.eak = false;
        this.eao = false;
        this.eal = false;
        this.eam = null;
        this.ean = null;
        this.roll = 0.0f;
        this.eap = 0.0f;
    }

    public boolean brt() {
        return this.eaj;
    }

    /* renamed from: dk */
    public void mo5306dk(boolean z) {
        this.eaj = z;
    }

    public boolean bru() {
        return this.eal;
    }

    /* renamed from: dl */
    public void mo5307dl(boolean z) {
        this.eal = z;
    }

    public boolean brv() {
        return this.eak;
    }

    /* renamed from: dm */
    public void mo5308dm(boolean z) {
        this.eak = z;
    }

    public Vec3f brw() {
        return this.eai;
    }

    public Weapon brx() {
        return this.ean;
    }

    /* renamed from: r */
    public void mo5312r(Weapon adv) {
        this.ean = adv;
    }

    /* renamed from: dn */
    public void mo5309dn(boolean z) {
        this.eao = z;
    }

    public boolean bry() {
        return this.eao;
    }

    public Actor brz() {
        return this.eam;
    }

    /* renamed from: aF */
    public void mo5296aF(Actor cr) {
        this.eam = cr;
    }

    public float getRoll() {
        return this.roll;
    }

    public void setRoll(float f) {
        this.roll = f;
    }

    public float brA() {
        return this.eap;
    }

    /* renamed from: hr */
    public void mo5311hr(float f) {
        this.eap = f;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("AIShipCommand -> Force: ");
        stringBuffer.append(this.eai);
        if (this.eaj) {
            stringBuffer.append(" startFiring");
        } else if (this.eak) {
            stringBuffer.append(" stopFiring");
        }
        if (this.ean != null) {
            stringBuffer.append(" SelectWeapon: " + this.ean.aqz().mo19891ke());
        }
        if (this.eao) {
            stringBuffer.append(" Cruise Speed Start");
        }
        return stringBuffer.toString();
    }
}
