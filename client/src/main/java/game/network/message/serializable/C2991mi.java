package game.network.message.serializable;

import p001a.C0651JP;

import java.io.Serializable;

/* renamed from: a.mi */
/* compiled from: a */
public interface C2991mi<T> extends Serializable {

    C0651JP aPk();

    int index();

    Object value();

    /* renamed from: a.mi$a */
    public enum C2992a {
        VOID,
        VALUE,
        INDEXED,
        INDEXED_VALUE
    }
}
