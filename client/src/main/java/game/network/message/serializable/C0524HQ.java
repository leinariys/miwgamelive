package game.network.message.serializable;

import java.io.Serializable;
import java.util.*;

/* renamed from: a.HQ */
/* compiled from: a */
public class C0524HQ<K, V> extends AbstractMap<K, V> implements Map<K, V>, Cloneable, Serializable {
    public static final int MAXIMUM_CAPACITY = 1073741824;

    public transient int capacity;
    public transient int hcJ;
    public transient C0525a<K, V>[] hcK;
    public transient C0525a<K, V> hcL;
    public transient C0524HQ<K, V>.EntrySet hcM;
    public transient float loadFactor;
    public transient int modCount;
    public int size;
    public transient int threshold;

    public C0524HQ() {
        this(4, 0.5f);
    }

    public C0524HQ(int i, float f) {
        if (i < 0) {
            throw new IllegalArgumentException("Illegal initial capacity: " + i);
        }
        i = i > 1073741824 ? 1073741824 : i;
        if (f <= 0.0f || Float.isNaN(f)) {
            throw new IllegalArgumentException("Illegal load factor: " + f);
        }
        resize(i);
        this.loadFactor = f;
    }

    /* renamed from: eq */
    public static boolean m5143eq(Object obj, Object obj2) {
        return obj == obj2 || obj.equals(obj2);
    }

    /* renamed from: i */
    public static boolean m5144i(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* access modifiers changed from: private */
    public Iterator<Map.Entry<K, V>> cHo() {
        return new C0527c();
    }

    /* access modifiers changed from: protected */
    public int hash(Object obj) {
        if (obj == null) {
            return 0;
        }
        int hashCode = obj.hashCode();
        int i = hashCode ^ ((hashCode >>> 20) ^ (hashCode >>> 12));
        return (i >>> 4) ^ ((i >>> 7) ^ i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: wJ */
    public int mo2587wJ(int i) {
        return i >> this.hcJ;
    }

    public V put(K k, V v) {
        int hash = hash(k);
        int wJ = mo2587wJ(hash);
        C0525a<K, V> aVar = this.hcK[wJ];
        if (aVar != null) {
            while (true) {
                if (hash != aVar.hash || !m5143eq(k, aVar.key)) {
                    C0525a<K, V> aVar2 = aVar.ddz;
                    if (aVar2 == null) {
                        C0525a<K, V> aVar3 = new C0525a<>(hash, k, v, aVar);
                        aVar.ddz = aVar3;
                        this.hcL = aVar3;
                        this.modCount++;
                        return null;
                    }
                    aVar = aVar2;
                } else {
                    V v2 = aVar.value;
                    aVar.value = v;
                    this.modCount++;
                    return v2;
                }
            }
        } else {
            C0525a<K, V> aVar4 = new C0525a<>(hash, k, v, this.hcL);
            this.hcK[wJ] = aVar4;
            this.hcL = aVar4;
            this.modCount++;
            return null;
        }
    }

    public V get(Object obj) {
        if (obj == null) {
            for (C0525a<K, V> aVar = this.hcK[0]; aVar != null; aVar = aVar.ddz) {
                if (aVar.hash == 0 && aVar.key == null) {
                    return aVar.value;
                }
            }
            return null;
        }
        int hash = hash(obj);
        for (C0525a<K, V> aVar2 = this.hcK[mo2587wJ(hash)]; aVar2 != null; aVar2 = aVar2.ddz) {
            if (hash == aVar2.hash && m5143eq(obj, aVar2.key)) {
                return aVar2.value;
            }
        }
        return null;
    }

    /* renamed from: aB */
    public C0525a<K, V> mo2577aB(Object obj) {
        if (obj == null) {
            for (C0525a<K, V> aVar = this.hcK[0]; aVar != null; aVar = aVar.ddz) {
                if (aVar.hash == 0 && aVar.key == null) {
                    return aVar;
                }
            }
            return null;
        }
        int hash = hash(obj);
        for (C0525a<K, V> aVar2 = this.hcK[mo2587wJ(hash)]; aVar2 != null; aVar2 = aVar2.ddz) {
            if (hash == aVar2.hash && m5143eq(obj, aVar2.key)) {
                return aVar2;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C0525a<K, V> mo2575a(C0525a<K, V> aVar) {
        int hash = aVar.key != null ? hash(aVar.key) : 0;
        int wJ = mo2587wJ(hash);
        C0525a<K, V> aVar2 = this.hcK[wJ];
        C0525a<K, V> aVar3 = aVar2;
        C0525a<K, V> aVar4 = aVar2;
        while (aVar3 != null) {
            if (hash != aVar3.hash || !aVar3.equals(aVar)) {
                aVar4 = aVar3;
                aVar3 = aVar3.ddz;
            } else {
                if (aVar4 == aVar3) {
                    this.hcK[wJ] = aVar3.ddz;
                }
                if (aVar3.ddB != null) {
                    aVar3.ddB.ddA = aVar3.ddA;
                }
                if (aVar3.ddA != null) {
                    aVar3.ddA.ddB = aVar3.ddB;
                }
                if (this.hcL == aVar3) {
                    if (aVar3.ddB != null) {
                        this.hcL = aVar3.ddB;
                    } else if (aVar3.ddA != null) {
                        this.hcL = aVar3.ddA;
                    } else {
                        this.hcL = null;
                    }
                }
                this.modCount++;
                return aVar3;
            }
        }
        return null;
    }

    public boolean containsKey(Object obj) {
        if (obj == null) {
            for (C0525a<K, V> aVar = this.hcK[0]; aVar != null; aVar = aVar.ddz) {
                if (aVar.hash == 0 && aVar.key == null) {
                    return true;
                }
            }
            return false;
        }
        int hash = hash(obj);
        for (C0525a<K, V> aVar2 = this.hcK[mo2587wJ(hash)]; aVar2 != null; aVar2 = aVar2.ddz) {
            if (hash == aVar2.hash && m5143eq(obj, aVar2.key)) {
                return true;
            }
        }
        return false;
    }

    public boolean containsValue(Object obj) {
        if (obj == null) {
            for (C0525a<K, V> aVar = this.hcL; aVar != null; aVar = aVar.ddB) {
                if (aVar.value == null) {
                    return true;
                }
            }
        } else {
            for (C0525a<K, V> aVar2 = this.hcL; aVar2 != null; aVar2 = aVar2.ddB) {
                if (m5143eq(obj, aVar2.value)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.hcM != null) {
            return this.hcM;
        }
        C0526b bVar = new C0526b(this, (C0526b) null);
        this.hcM = bVar;
        return bVar;
    }

    public V remove(Object obj) {
        int hash = obj != null ? hash(obj) : 0;
        int wJ = mo2587wJ(hash);
        C0525a<K, V> aVar = this.hcK[wJ];
        C0525a<K, V> aVar2 = aVar;
        C0525a<K, V> aVar3 = aVar;
        while (aVar2 != null) {
            if (hash != aVar2.hash || !m5144i(obj, aVar2.key)) {
                aVar3 = aVar2;
                aVar2 = aVar2.ddz;
            } else {
                if (aVar3 == aVar2) {
                    this.hcK[wJ] = aVar2.ddz;
                }
                if (aVar2.ddB != null) {
                    aVar2.ddB.ddA = aVar2.ddA;
                }
                if (aVar2.ddA != null) {
                    aVar2.ddA.ddB = aVar2.ddB;
                }
                if (this.hcL == aVar2) {
                    if (aVar2.ddB != null) {
                        this.hcL = aVar2.ddB;
                    } else if (aVar2.ddA != null) {
                        this.hcL = aVar2.ddA;
                    } else {
                        this.hcL = null;
                    }
                }
                this.modCount++;
                return aVar2.value;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void resize(int i) {
        if (this.capacity != i) {
            int i2 = 0;
            int i3 = 1;
            while (i3 < i && i2 < 30) {
                i3 = 1 << i2;
                i2++;
            }
            C0525a<K, V>[] aVarArr = new C0525a[i3];
            for (C0525a<K, V> aVar = this.hcL; aVar != null; aVar = aVar.ddB) {
                int wJ = mo2587wJ(aVar.hash);
                aVar.ddz = null;
                C0525a<K, V> aVar2 = aVarArr[wJ];
                if (aVar2 != null) {
                    while (aVar2.ddz != null) {
                        aVar2 = aVar2.ddz;
                    }
                    aVar2.ddz = aVar;
                } else {
                    aVarArr[wJ] = aVar;
                }
            }
            this.hcJ = 32 - i2;
            this.capacity = i;
            this.hcK = aVarArr;
            this.threshold = (int) (((float) this.capacity) * this.loadFactor);
        }
    }

    public void clear() {
        this.size = 0;
        this.hcL = null;
        C0525a<K, V>[] aVarArr = this.hcK;
        int length = aVarArr.length;
        for (int i = 0; i < length; i++) {
            aVarArr[i] = null;
        }
        this.modCount++;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo2576a(int i, K k, V v) {
        int wJ = mo2587wJ(i);
        C0525a<K, V> aVar = this.hcK[wJ];
        if (aVar != null) {
            while (aVar.ddz != null) {
                aVar = aVar.ddz;
            }
            C0525a<K, V> aVar2 = new C0525a<>(i, k, v, aVar);
            aVar.ddz = aVar2;
            this.hcL = aVar2;
        } else {
            this.hcK[wJ] = new C0525a<>(i, k, v, aVar);
        }
        this.modCount++;
        int i2 = this.size;
        this.size = i2 + 1;
        if (i2 >= this.threshold) {
            resize(this.hcK.length * 2);
        }
    }

    /* renamed from: a.HQ$a */
    public static class C0525a<K, V> implements Map.Entry<K, V> {
        public C0525a<K, V> ddA;
        public C0525a<K, V> ddB;
        public C0525a<K, V> ddz;
        public int hash;
        public K key;
        public V value;

        public C0525a(int i, K k, V v, C0525a<K, V> aVar) {
            this.hash = i;
            this.key = k;
            this.value = v;
            if (aVar != null) {
                this.ddA = aVar.ddA;
                aVar.ddA = this;
                this.ddB = aVar;
            }
        }

        public int aVj() {
            return this.hash;
        }

        public K getKey() {
            return this.key;
        }

        public V getValue() {
            return this.value;
        }

        public V setValue(V v) {
            V v2 = this.value;
            this.value = v;
            return v2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            if (!C0524HQ.m5144i(getKey(), entry.getKey()) || !C0524HQ.m5144i(getValue(), entry.getValue())) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: a.HQ$b */
    /* compiled from: a */
    private final class C0526b extends AbstractSet<Map.Entry<K, V>> {
        private C0526b() {
        }

        /* synthetic */ C0526b(C0524HQ hq, C0526b bVar) {
            this();
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return C0524HQ.this.cHo();
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            C0525a aB = C0524HQ.this.mo2577aB(entry.getKey());
            if (aB == null || !aB.equals(entry)) {
                return false;
            }
            return true;
        }

        public boolean remove(Object obj) {
            if ((obj instanceof C0525a) && C0524HQ.this.mo2575a((C0525a) obj) != null) {
                return true;
            }
            return false;
        }

        public int size() {
            return C0524HQ.this.size;
        }

        public void clear() {
            C0524HQ.this.clear();
        }
    }

    /* renamed from: a.HQ$c */
    /* compiled from: a */
    public class C0527c implements Iterator<Map.Entry<K, V>> {
        C0525a<K, V> fLI;
        C0525a<K, V> fLJ;
        int fLK;

        public C0527c() {
        }

        public boolean hasNext() {
            return this.fLJ != null;
        }

        public Map.Entry<K, V> next() {
            this.fLI = this.fLJ;
            if (this.fLI != null) {
                this.fLJ = this.fLI.ddA;
            }
            return this.fLI;
        }

        public void remove() {
            if (this.fLI == null) {
                throw new IllegalStateException();
            } else if (C0524HQ.this.modCount != this.fLK) {
                throw new ConcurrentModificationException();
            } else {
                K k = this.fLI.key;
                this.fLI = null;
                C0524HQ.this.remove(k);
                this.fLK = C0524HQ.this.modCount;
            }
        }
    }
}
