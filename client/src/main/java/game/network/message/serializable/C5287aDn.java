package game.network.message.serializable;

import game.script.Character;
import logic.res.html.C0029AO;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.aDn  reason: case insensitive filesystem */
/* compiled from: a */
public class C5287aDn implements Serializable {
    static Map<String, Class<?>> hzR = new HashMap();

    static {
        hzR.put("boolean", Boolean.TYPE);
        hzR.put("int", Integer.TYPE);
        hzR.put("byte", Byte.TYPE);
        hzR.put("short", Short.TYPE);
        hzR.put("long", Long.TYPE);
        hzR.put("char", Character.TYPE);
        hzR.put("double", Double.TYPE);
        hzR.put("float", Float.TYPE);
    }

    private final String name;
    private final String type;
    private boolean dlS;
    private boolean dzk;
    private Class<?> hzM;
    private String[] hzN;
    private boolean hzO;
    private boolean hzP;
    private Class<?>[] hzQ;

    public C5287aDn(String str, String str2) {
        this.hzP = C1556Wo.class.getName().equals(str2);
        if (!this.hzP) {
            this.hzO = C2686iZ.class.getName().equals(str2);
            this.dlS = this.hzO | C3438ra.class.getName().equals(str2) | C0029AO.class.getName().equals(str2);
        }
        this.name = str;
        this.type = str2;
        this.dzk = hzR.containsKey(str2);
        if (this.dzk) {
            this.hzM = hzR.get(str2);
        }
    }

    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    /* renamed from: aF */
    public void mo8492aF(Class<?> cls) {
        this.hzM = cls;
    }

    public Class<?> cTB() {
        if (this.hzM != null) {
            return this.hzM;
        }
        Class<?> cls = Class.forName(this.type);
        this.hzM = cls;
        return cls;
    }

    /* renamed from: lU */
    public void mo8502lU(String str) {
        if (str != null) {
            this.hzN = str.split(",");
        } else {
            this.hzN = new String[0];
        }
    }

    public String[] cTC() {
        return this.hzN;
    }

    public boolean isCollection() {
        return this.dlS;
    }

    public boolean isPrimitive() {
        return this.dzk;
    }

    /* renamed from: En */
    public boolean mo8491En() {
        return this.hzP;
    }

    public Class<?>[] cTD() {
        if (this.hzQ == null && this.hzN != null) {
            this.hzQ = new Class[this.hzN.length];
            for (int i = 0; i < this.hzN.length; i++) {
                this.hzQ[i] = Class.forName(this.hzN[i]);
            }
        }
        return this.hzQ;
    }

    public boolean cTE() {
        return this.hzO;
    }

    public String toString() {
        return "Field: " + getType() + " " + getName();
    }

    /* renamed from: c */
    public void mo8493c(Class<?>[] clsArr) {
        if (clsArr == null) {
            clsArr = new Class[0];
        }
        this.hzQ = clsArr;
        this.hzN = new String[this.hzQ.length];
        for (int i = 0; i < this.hzQ.length; i++) {
            this.hzN[i] = this.hzQ[i].getName();
        }
    }
}
