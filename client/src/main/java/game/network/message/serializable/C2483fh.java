package game.network.message.serializable;

import game.io.IExternalIO;
import game.network.exception.CreatListenerChannelException;
import game.network.message.WriterBitsData;
import p001a.C1296TB;
import util.Syst;

import java.io.*;
import java.util.IdentityHashMap;

/* renamed from: a.fh */
/* compiled from: a */
public class C2483fh extends ObjectOutputStream implements C1296TB, Serializable {


    /* renamed from: Jj */
    public WriterBitsData f7415Jj;

    /* renamed from: Jk */
    public int f7416Jk;

    /* renamed from: Jl */
    public IdentityHashMap<Object, Integer> f7417Jl;

    /* renamed from: Jm */
    public Object[] f7418Jm;

    /* renamed from: Jn */
    public Object[] f7419Jn;

    /* renamed from: Jo */
    public int f7420Jo;

    /* renamed from: Jp */
    public boolean f7421Jp = true;

    /* renamed from: Jq */
    public Object f7422Jq;

    /* renamed from: Jr */
    public Object f7423Jr;

    /* renamed from: Js */
    public Object f7424Js;

    /* renamed from: Jt */
    public Object f7425Jt;

    /* renamed from: Ju */
    public Object f7426Ju;

    /* renamed from: Jv */
    public Object f7427Jv;

    /* renamed from: Jw */
    public Object f7428Jw;

    /* renamed from: Jx */
    public Object f7429Jx;

    /* renamed from: Jy */
    public Object f7430Jy;

    /* renamed from: Jz */
    public C0874Mg f7431Jz;
    public int cacheSize;

    public C2483fh(WriterBitsData oUVar) {
        this.f7415Jj = oUVar;
    }

    /* renamed from: pd */
    public boolean mo18791pd() {
        return this.f7421Jp;
    }

    /* renamed from: E */
    public void mo18779E(boolean z) {
        this.f7421Jp = z;
    }

    /* renamed from: ab */
    public void writeString(String str) {
        this.f7416Jk++;
        mo18788h(str, 134);
    }

    /* renamed from: pe */
    public void mo16277pe() {
        this.f7416Jk--;
        if (this.f7421Jp) {
            mo7783aJ(135);
        }
    }

    @Deprecated
    /* renamed from: c */
    public void mo16268c(String str, int i) {
        mo18788h(str, 1);
        this.f7415Jj.write(i);
    }

    /* renamed from: a */
    public void mo16264a(String str, byte[] bArr) {
        mo18784b(str, 171, bArr.length);
        this.f7415Jj.write(bArr);
    }

    /* renamed from: a */
    public void mo16265a(String str, byte[] bArr, int i, int i2) {
        mo18784b(str, 171, i2);
        this.f7415Jj.write(bArr, i, i2);
    }

    /* renamed from: a */
    public void mo16261a(String str, IExternalIO afa) {
        if (afa == null) {
            m31251ac(str);
        } else if (!mo18783a(str, (Object) afa, false)) {
            mo18790i(str, 145);
            int i = this.f7416Jk;
            this.f7416Jk = i + 1;
            mo18787e(afa.getClass());
            mo18793t(afa);
            afa.writeExternal(this);
            mo16277pe();
            if (i != this.f7416Jk) {
                throw new IllegalStateException("Invalid nesting! Should be " + i + " but was " + this.f7416Jk);
            }
        }
    }

    /* renamed from: a */
    public void mo16259a(String str, int i, int i2) {
        mo18784b(str, 7, i);
        this.f7415Jj.writeBits(i, i2);
    }

    /* renamed from: a */
    public void writeBoolean(String str, boolean z) {
        mo18788h(str, 0);
        this.f7415Jj.writeBoolean(z);
    }

    /* renamed from: d */
    public void writeByte(String str, int i) {
        mo18788h(str, 1);
        this.f7415Jj.writeByte(i);
    }

    /* renamed from: g */
    public void mo16274g(String str, String str2) {
        mo18784b(str, 171, str2.length());
        this.f7415Jj.writeBytes(str2);
    }

    /* renamed from: e */
    public void writeChar(String str, int i) {
        mo18788h(str, 3);
        this.f7415Jj.writeChar(i);
    }

    /* renamed from: h */
    public void mo16275h(String str, String str2) {
    }

    /* renamed from: a */
    public void writeDouble(String str, double d) {
        mo18788h(str, 128);
        this.f7415Jj.writeDouble(d);
    }

    /* renamed from: a */
    public void mo16262a(String str, Enum<?> enumR) {
        if (enumR == null) {
            m31251ac(str);
        } else if (!mo18783a(str, (Object) enumR, false)) {
            mo18790i(str, 133);
            mo18787e(enumR.getClass());
            this.f7415Jj.writeInt(enumR.ordinal());
            if (this.f7421Jp) {
                mo7784ad(enumR.name());
            }
            mo18793t(enumR);
        }
    }

    /* renamed from: a */
    public void writeFloat(String str, float f) throws IOException {
        mo18788h(str, 6);
        this.f7415Jj.writeFloat(f);
    }

    /* renamed from: f */
    public void writeInt(String str, int i) {
        mo18788h(str, 4);
        this.f7415Jj.writeInt(i);
    }

    /* renamed from: a */
    public void writeLong(String str, long j) {
        mo18788h(str, 5);
        this.f7415Jj.writeLong(j);
    }

    /* renamed from: g */
    public void mo16273g(String str, Object obj) {
        if (obj == null) {
            m31251ac(str);
        } else if (mo18783a(str, obj, false)) {
        } else {
            if (obj instanceof Class) {
                if (!this.f7421Jp) {
                    mo7783aJ(144);
                }
                mo16267b(str, (Class) obj);
            } else if (obj instanceof Enum) {
                if (!this.f7421Jp) {
                    mo7783aJ(133);
                }
                mo16262a(str, (Enum<?>) (Enum) obj);
            } else if (obj instanceof IExternalIO) {
                mo16261a(str, (IExternalIO) obj);
            } else if (obj instanceof Number) {
                if (obj instanceof Integer) {
                    if (!this.f7421Jp) {
                        mo7783aJ(4);
                    }
                    writeInt(str, ((Integer) obj).intValue());
                } else if (obj instanceof Long) {
                    if (!this.f7421Jp) {
                        mo7783aJ(5);
                    }
                    writeLong(str, ((Long) obj).longValue());
                } else if (obj instanceof Float) {
                    if (!this.f7421Jp) {
                        mo7783aJ(6);
                    }
                    writeFloat(str, ((Float) obj).floatValue());
                } else if (obj instanceof Double) {
                    if (!this.f7421Jp) {
                        mo7783aJ(128);
                    }
                    writeDouble(str, ((Double) obj).doubleValue());
                } else if (obj instanceof Short) {
                    if (!this.f7421Jp) {
                        mo7783aJ(2);
                    }
                    writeShort(str, (int) ((Short) obj).shortValue());
                } else if (obj instanceof Byte) {
                    if (!this.f7421Jp) {
                        mo7783aJ(1);
                    }
                    writeByte(str, ((Byte) obj).byteValue());
                }
            } else if (obj instanceof Boolean) {
                if (!this.f7421Jp) {
                    mo7783aJ(0);
                }
                writeBoolean(str, ((Boolean) obj).booleanValue());
            } else if (obj instanceof String) {
                if (!this.f7421Jp) {
                    mo7783aJ(131);
                }
                mo16276i(str, (String) obj);
            } else {
                C0874Mg G = C0874Mg.m7123G(obj.getClass());
                if (G.isArray()) {
                    mo18789h(str, obj);
                } else {
                    mo18781a(str, obj, G);
                }
            }
        }
    }

    /* renamed from: ac */
    private void m31251ac(String str) {
        if (!this.f7421Jp) {
            mo7783aJ(129);
        } else {
            mo18788h(str, 129);
        }
    }

    /* renamed from: b */
    public void mo16267b(String str, Class<?> cls) {
        mo18788h(str, 144);
        mo18787e(cls);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aJ */
    public void mo7783aJ(int i) {
        if (i < 8) {
            this.f7415Jj.writeBits(4, i);
        } else {
            this.f7415Jj.writeBits(8, i);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo18781a(String str, Object obj, C0874Mg mg) {
        m31252r(obj);
        Class<?> cls = obj.getClass();
        int i = this.f7416Jk;
        this.f7416Jk = i + 1;
        if (mg.isExternalizable()) {
            mo18790i(str, 146);
            mo18787e(cls);
            mo18793t(obj);
            ((Externalizable) obj).writeExternal(this);
        } else {
            mo18790i(str, 147);
            mo18787e(cls);
            mo18793t(obj);
            do {
                if (mg.bgR()) {
                    Object obj2 = this.f7430Jy;
                    C0874Mg mg2 = this.f7431Jz;
                    this.f7430Jy = obj;
                    this.f7431Jz = mg;
                    try {
                        mg.mo4018a(obj, (ObjectOutputStream) this);
                    } finally {
                        this.f7430Jy = obj2;
                        this.f7431Jz = mg2;
                    }
                } else {
                    mo18780a(obj, mg);
                }
                mg = mg.bgT();
            } while (mg != null);
        }
        mo16277pe();
        if (i != this.f7416Jk) {
            throw new IllegalStateException("Invalid nesting! Should be " + i + " but was " + this.f7416Jk);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo18780a(Object obj, C0874Mg mg) {
        if (mg.bgN() > 0) {
            mg.mo4016a(obj, this);
        }
        if (mg.bgO() > 0) {
            int i = this.f7420Jo;
            if (this.f7419Jn == null) {
                this.f7419Jn = new Object[mg.bgO()];
                this.f7420Jo = mg.bgO();
            } else {
                this.f7420Jo += mg.bgO();
                if (this.f7420Jo >= this.f7419Jn.length) {
                    this.f7419Jn = Syst.copyOf((T[]) this.f7419Jn, this.f7420Jo + mg.bgO());
                }
            }
            int i2 = this.f7420Jo;
            try {
                mg.mo4019a(obj, this.f7419Jn, i);
                int i3 = 0;
                int i4 = i;
                while (i4 < i2) {
                    Object obj2 = this.f7419Jn[i4];
                    String name = mg.mo4035lK(i3).getName();
                    if (obj2 == null) {
                        mo16273g(name, obj2);
                    } else {
                        C0874Mg G = C0874Mg.m7123G(obj2.getClass());
                        if (obj2 instanceof String) {
                            if (!this.f7421Jp) {
                                mo7783aJ(131);
                            }
                            mo16276i(name, (String) obj2);
                        } else if (G.isArray()) {
                            mo18789h(name, obj2);
                        } else {
                            mo16273g(name, obj2);
                        }
                    }
                    i4++;
                    i3++;
                }
                this.f7420Jo = i;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: r */
    private void m31252r(Object obj) {
        if (!(obj instanceof Serializable) && !(obj instanceof Externalizable)) {
            throw new NotSerializableException(obj.getClass().getName());
        }
    }

    /* renamed from: h */
    public void mo18789h(String str, Object obj) {
        int i = 0;
        if (obj == null) {
            m31251ac(str);
        } else if (!mo18783a(str, obj, false)) {
            Class<?> cls = obj.getClass();
            if (!cls.getComponentType().isPrimitive()) {
                mo18790i(str, 170);
                this.f7416Jk++;
                mo18787e(C0874Mg.m7123G(cls).getComponentType());
                Object[] objArr = (Object[]) obj;
                this.f7415Jj.writeInt(objArr.length);
                mo18793t(obj);
                int length = objArr.length;
                while (i < length) {
                    mo16273g((String) null, objArr[i]);
                    i++;
                }
                mo16277pe();
            } else if (cls == int[].class) {
                mo18790i(str, 164);
                int[] iArr = (int[]) obj;
                this.f7415Jj.writeInt(iArr.length);
                mo18793t(obj);
                int length2 = iArr.length;
                while (i < length2) {
                    this.f7415Jj.writeInt(iArr[i]);
                    i++;
                }
            } else if (cls == long[].class) {
                mo18790i(str, 165);
                long[] jArr = (long[]) obj;
                this.f7415Jj.writeInt(jArr.length);
                mo18793t(obj);
                int length3 = jArr.length;
                while (i < length3) {
                    this.f7415Jj.writeLong(jArr[i]);
                    i++;
                }
            } else if (cls == byte[].class) {
                mo18790i(str, 161);
                byte[] bArr = (byte[]) obj;
                this.f7415Jj.writeInt(bArr.length);
                mo18793t(obj);
                int length4 = bArr.length;
                while (i < length4) {
                    this.f7415Jj.writeByte(bArr[i]);
                    i++;
                }
            } else if (cls == boolean[].class) {
                mo18790i(str, 160);
                boolean[] zArr = (boolean[]) obj;
                this.f7415Jj.writeInt(zArr.length);
                mo18793t(obj);
                int length5 = zArr.length;
                while (i < length5) {
                    this.f7415Jj.writeBoolean(zArr[i]);
                    i++;
                }
            } else if (cls == char[].class) {
                mo18790i(str, 163);
                char[] cArr = (char[]) obj;
                this.f7415Jj.writeInt(cArr.length);
                mo18793t(obj);
                int length6 = cArr.length;
                while (i < length6) {
                    this.f7415Jj.writeChar(cArr[i]);
                    i++;
                }
            } else if (cls == double[].class) {
                mo18790i(str, 169);
                double[] dArr = (double[]) obj;
                mo18793t(obj);
                int length7 = dArr.length;
                while (i < length7) {
                    this.f7415Jj.writeDouble(dArr[i]);
                    i++;
                }
            } else if (cls == float[].class) {
                mo18790i(str, 166);
                float[] fArr = (float[]) obj;
                this.f7415Jj.writeInt(fArr.length);
                mo18793t(obj);
                int length8 = fArr.length;
                while (i < length8) {
                    this.f7415Jj.writeFloat(fArr[i]);
                    i++;
                }
            } else if (cls == short[].class) {
                mo18790i(str, 162);
                short[] sArr = (short[]) obj;
                this.f7415Jj.writeInt(sArr.length);
                mo18793t(obj);
                int length9 = sArr.length;
                while (i < length9) {
                    this.f7415Jj.writeShort(sArr[i]);
                    i++;
                }
            }
        }
    }

    /* renamed from: g */
    public void writeShort(String str, int i) {
        mo18788h(str, 2);
        this.f7415Jj.writeShort(i);
    }

    /* renamed from: i */
    public void mo16276i(String str, String str2) {
        if (!this.f7421Jp) {
            mo7784ad(str2);
        } else if (str2 == null) {
            m31251ac(str);
        } else if (!mo18783a(str, (Object) str2, false)) {
            mo18788h(str, 131);
            this.f7415Jj.writeUTF(str2);
            mo18793t(str2);
        }
    }

    public void writeUTF(String str) {
        mo16276i((String) null, str);
    }

    public void write(int i) {
        writeByte((String) null, i);
    }

    public void write(byte[] bArr) {
        mo16264a((String) null, bArr);
    }

    public void write(byte[] bArr, int i, int i2) {
        mo16265a((String) null, bArr, i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ad */
    public void mo7784ad(String str) {
        if (!mo18782a((Object) str, true)) {
            this.f7415Jj.writeUTF(str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo18783a(String str, Object obj, boolean z) {
        int s = mo18792s(obj);
        if (s >= 0) {
            mo18790i(str, 130);
            this.f7415Jj.writeInt(s);
            return true;
        }
        if (z) {
            mo18793t(obj);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo18782a(Object obj, boolean z) {
        if (obj == null) {
            this.f7415Jj.writeBits(1, 0);
            return true;
        }
        this.f7415Jj.writeBits(1, 1);
        int s = mo18792s(obj);
        if (s >= 0) {
            this.f7415Jj.writeBits(1, 0);
            this.f7415Jj.writeInt(s);
            return true;
        }
        if (z) {
            mo18793t(obj);
        }
        this.f7415Jj.writeBits(1, 1);
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo18788h(String str, int i) {
        if (this.f7421Jp) {
            mo7783aJ(i);
            mo7784ad(str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo18790i(String str, int i) {
        if (this.f7421Jp) {
            mo7783aJ(i);
            mo7784ad(str);
            return;
        }
        mo7783aJ(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo18784b(String str, int i, int i2) {
        if (this.f7421Jp) {
            mo18788h(str, i);
            this.f7415Jj.writeInt(i2);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo18787e(Class<?> cls) {
        if (!mo18782a((Object) cls, false)) {
            mo7784ad(cls.getName());
            mo18793t(cls);
        }
    }

    public void clear() {
        if (this.cacheSize > 0) {
            if (this.cacheSize <= 8) {
                this.f7429Jx = null;
                this.f7428Jw = null;
                this.f7427Jv = null;
                this.f7426Ju = null;
                this.f7425Jt = null;
                this.f7424Js = null;
                this.f7423Jr = null;
                this.f7422Jq = null;
            }
            if (this.cacheSize <= 64) {
                int i = this.cacheSize;
                while (true) {
                    i--;
                    if (i < 0) {
                        break;
                    }
                    this.f7418Jm[i] = 0;
                }
            } else {
                this.f7417Jl.clear();
            }
            this.cacheSize = 0;
        }
        this.f7420Jo = 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: s */
    public int mo18792s(Object obj) {
        if (this.cacheSize <= 8) {
            if (this.f7422Jq == obj) {
                return 1;
            }
            if (this.f7423Jr == obj) {
                return 2;
            }
            if (this.f7424Js == obj) {
                return 3;
            }
            if (this.f7425Jt == obj) {
                return 4;
            }
            if (this.f7426Ju == obj) {
                return 5;
            }
            if (this.f7427Jv == obj) {
                return 6;
            }
            if (this.f7428Jw == obj) {
                return 7;
            }
            if (this.f7429Jx != obj) {
                return -1;
            }
            return 8;
        } else if (this.cacheSize <= 64) {
            for (int i = this.cacheSize; i > 0; i--) {
                if (this.f7418Jm[i] == obj) {
                    return i;
                }
            }
            return -1;
        } else {
            Integer num = this.f7417Jl.get(obj);
            if (num == null) {
                return -1;
            }
            return num.intValue();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public void mo18793t(Object obj) {
        if (this.cacheSize < 8) {
            int i = this.cacheSize + 1;
            this.cacheSize = i;
            switch (i) {
                case 1:
                    this.f7422Jq = obj;
                    return;
                case 2:
                    this.f7423Jr = obj;
                    return;
                case 3:
                    this.f7424Js = obj;
                    return;
                case 4:
                    this.f7425Jt = obj;
                    return;
                case 5:
                    this.f7426Ju = obj;
                    return;
                case 6:
                    this.f7427Jv = obj;
                    return;
                case 7:
                    this.f7428Jw = obj;
                    return;
                case 8:
                    this.f7429Jx = obj;
                    return;
                default:
                    return;
            }
        } else if (this.cacheSize < 64) {
            if (this.cacheSize == 8) {
                if (this.f7418Jm == null) {
                    this.f7418Jm = new Object[65];
                }
                this.f7418Jm[1] = this.f7422Jq;
                this.f7418Jm[2] = this.f7423Jr;
                this.f7418Jm[3] = this.f7424Js;
                this.f7418Jm[4] = this.f7425Jt;
                this.f7418Jm[5] = this.f7426Ju;
                this.f7418Jm[6] = this.f7427Jv;
                this.f7418Jm[7] = this.f7428Jw;
                this.f7418Jm[8] = this.f7429Jx;
                this.f7429Jx = null;
                this.f7428Jw = null;
                this.f7427Jv = null;
                this.f7426Ju = null;
                this.f7425Jt = null;
                this.f7424Js = null;
                this.f7423Jr = null;
                this.f7422Jq = null;
            }
            Object[] objArr = this.f7418Jm;
            int i2 = this.cacheSize + 1;
            this.cacheSize = i2;
            objArr[i2] = obj;
        } else {
            if (this.cacheSize == 64) {
                if (this.f7417Jl == null) {
                    this.f7417Jl = new IdentityHashMap<>();
                }
                for (int i3 = 1; i3 <= 64; i3++) {
                    this.f7417Jl.put(this.f7418Jm[i3], Integer.valueOf(i3));
                    this.f7418Jm[i3] = null;
                }
            }
            IdentityHashMap<Object, Integer> identityHashMap = this.f7417Jl;
            int i4 = this.cacheSize + 1;
            this.cacheSize = i4;
            identityHashMap.put(obj, Integer.valueOf(i4));
        }
    }

    public void close() {
        this.f7415Jj.close();
    }

    public void flush() {
        this.f7415Jj.flush();
    }

    public void writeObjectOverride(Object obj) {
        mo16273g((String) null, obj);
    }

    public void writeFields() {
        throw new CreatListenerChannelException();
    }

    /* access modifiers changed from: protected */
    public void writeStreamHeader() {
    }

    /* access modifiers changed from: protected */
    public void writeClassDescriptor(ObjectStreamClass objectStreamClass) {
        super.writeClassDescriptor(objectStreamClass);
    }

    public void writeUnshared(Object obj) {
        throw new CreatListenerChannelException();
    }

    public void defaultWriteObject() {
        if (this.f7430Jy == null || this.f7431Jz == null) {
            throw new NotActiveException("not in call to writeObject");
        }
        mo18780a(this.f7430Jy, this.f7431Jz);
    }

    public void writeBoolean(boolean z) {
        writeBoolean((String) null, z);
    }

    public void writeByte(int i) {
        writeByte((String) null, i);
    }

    public void writeBytes(String str) {
        mo16274g((String) null, str);
    }

    public void writeChar(int i) {
        writeChar((String) null, i);
    }

    public void writeChars(String str) {
        mo16275h((String) null, str);
    }

    public void writeDouble(double d) {
        writeDouble((String) null, d);
    }

    public void writeFloat(float f) {
        writeFloat((String) null, f);
    }

    public void writeInt(int i) {
        writeInt((String) null, i);
    }

    public void writeLong(long j) {
        writeLong((String) null, j);
    }

    public void writeShort(int i) {
        writeShort((String) null, i);
    }
}
