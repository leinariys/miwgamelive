package game.network.message.serializable;

import p001a.C6537aop;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;

/* renamed from: a.wW */
/* compiled from: a */
public class C3901wW implements Serializable {
    public int bFf;
    public ByteBuffer bFg;
    public int bFh;
    public ByteBuffer bFi;
    public int bFj;
    public C6537aop bFk;
    public int numVertices;

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(this.bFf);
        objectOutputStream.writeInt(this.bFh);
        objectOutputStream.writeInt(this.numVertices);
        objectOutputStream.writeInt(this.bFj);
        objectOutputStream.writeInt(this.bFg.array().length);
        objectOutputStream.write(this.bFg.array());
        objectOutputStream.writeInt(this.bFi.array().length);
        objectOutputStream.write(this.bFi.array());
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.bFk = C6537aop.PHY_INTEGER;
        this.bFf = objectInputStream.readInt();
        this.bFh = objectInputStream.readInt();
        this.numVertices = objectInputStream.readInt();
        this.bFj = objectInputStream.readInt();
        byte[] bArr = new byte[objectInputStream.readInt()];
        objectInputStream.read(bArr);
        this.bFg = ByteBuffer.allocate(bArr.length);
        this.bFg.put(bArr);
        byte[] bArr2 = new byte[objectInputStream.readInt()];
        objectInputStream.read(bArr2);
        this.bFi = ByteBuffer.allocate(bArr2.length);
        this.bFi.put(bArr2);
    }
}
