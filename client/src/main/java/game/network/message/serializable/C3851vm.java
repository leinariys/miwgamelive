package game.network.message.serializable;

import org.w3c.css.sac.CharacterDataSelector;

import java.io.Serializable;

/* renamed from: a.vm */
/* compiled from: a */
public class C3851vm implements CharacterDataSelector, Serializable {
    private String bzc;

    public C3851vm(String str) {
        this.bzc = str;
    }

    public short getSelectorType() {
        return 6;
    }

    public String getData() {
        return this.bzc;
    }

    public String toString() {
        return getData();
    }
}
