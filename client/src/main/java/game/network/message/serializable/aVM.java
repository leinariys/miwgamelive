package game.network.message.serializable;

import p001a.C5317aEr;

import java.io.Serializable;

/* renamed from: a.aVM */
/* compiled from: a */
public class aVM implements Serializable {
    public final short[] jbD = new short[3];
    public final short[] jbE = new short[3];
    public int jbF;
    public int jbG;

    /* renamed from: a */
    public void mo11769a(C5317aEr aer, int i) {
        this.jbD[0] = (short) aer.mo8675az(i, 0);
        this.jbD[1] = (short) aer.mo8675az(i, 1);
        this.jbD[2] = (short) aer.mo8675az(i, 2);
        this.jbE[0] = (short) aer.mo8673aA(i, 0);
        this.jbE[1] = (short) aer.mo8673aA(i, 1);
        this.jbE[2] = (short) aer.mo8673aA(i, 2);
    }
}
