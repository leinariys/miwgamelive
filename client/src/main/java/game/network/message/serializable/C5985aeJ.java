package game.network.message.serializable;

import logic.baa.aOW;
import p001a.C0943Nq;
import p001a.C1260Sc;
import p001a.C3971xY;
import p001a.C6859auz;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.aeJ  reason: case insensitive filesystem */
/* compiled from: a */
public class C5985aeJ implements Serializable {

    private final String className;
    private Class<?> clazz;
    private Map<String, C5287aDn> fqs = new HashMap();
    private C5985aeJ fqt;
    private int fqu;
    private Method fqv;
    private boolean fqw;
    private C2993mj fqx = C2993mj.aCc;
    private boolean fqy;
    private Method fqz;
    private String version;

    public C5985aeJ(String str) {
        this.className = str;
    }

    /* renamed from: a */
    public void mo13030a(C5287aDn adn) {
        this.fqs.put(adn.getName(), adn);
    }

    public C2993mj bUC() {
        return this.fqx;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String str) {
        this.version = str;
        this.fqx = new C2993mj(str);
    }

    public String getClassName() {
        return this.className;
    }

    public Map<String, C5287aDn> bUD() {
        return this.fqs;
    }

    /* renamed from: a */
    public void mo13031a(C5985aeJ aej) {
        this.fqt = aej;
    }

    public C5985aeJ bUE() {
        return this.fqt;
    }

    public Class<?> getClazz() {
        if (this.clazz != null) {
            return this.clazz;
        }
        Class<?> cls = Class.forName(this.className);
        this.clazz = cls;
        return cls;
    }

    public String toString() {
        if (this.fqt != null) {
            return String.valueOf(this.className) + "=>" + this.fqt.getClassName();
        }
        return this.className;
    }

    public void bUF() {
        this.fqu++;
    }

    public int bUG() {
        return this.fqu;
    }

    public Method bUH() {
        if (!this.fqw) {
            for (Class clazz2 = getClazz(); clazz2 != Object.class; clazz2 = clazz2.getSuperclass()) {
                this.fqw = true;
                try {
                    this.fqv = clazz2.getDeclaredMethod(aOW.iAt, new Class[]{C6859auz.class});
                } catch (NoSuchMethodException e) {
                }
                if (this.fqv != null) {
                    this.fqv.setAccessible(true);
                    if (Modifier.isStatic(this.fqv.getModifiers())) {
                        return this.fqv;
                    }
                    Method method = this.fqv;
                    this.fqv = null;
                    throw new IllegalArgumentException("Preload method must be static: " + method);
                }
            }
        }
        return this.fqv;
    }

    public Method bUI() {
        if (!this.fqy) {
            for (Class clazz2 = getClazz(); clazz2 != Object.class; clazz2 = clazz2.getSuperclass()) {
                this.fqy = true;
                try {
                    this.fqz = clazz2.getDeclaredMethod(C0943Nq.dDv, new Class[]{C1260Sc.class, C3971xY.class});
                } catch (NoSuchMethodException e) {
                }
                if (this.fqz != null) {
                    this.fqz.setAccessible(true);
                    if (Modifier.isStatic(this.fqz.getModifiers())) {
                        return this.fqz;
                    }
                    Method method = this.fqz;
                    this.fqz = null;
                    throw new IllegalArgumentException("PreMerger method must be static: " + method);
                }
            }
        }
        return this.fqz;
    }
}
