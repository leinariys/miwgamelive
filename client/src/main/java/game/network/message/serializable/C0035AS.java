package game.network.message.serializable;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.externalizable.C2631hn;
import gnu.trove.THashSet;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import logic.res.html.C1867ad;
import logic.res.html.DataClassSerializer;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;

import java.io.ObjectInput;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/* renamed from: a.AS */
/* compiled from: a */
public class C0035AS<T> extends C2631hn<T> implements C2686iZ<T>, Serializable {

    static LogPrinter log = LogPrinter.m10275K(C0035AS.class);
    public Set<T> set;

    public C0035AS() {
    }

    public C0035AS(C1616Xf xf, C5663aRz arz) {
        super(xf, arz);
    }

    public C0035AS(C1616Xf xf, C5663aRz arz, Set<T> set2) {
        super(xf, arz);
        if (set2 != null && set2.size() > 0) {
            this.set = new THashSet(set2);
        }
    }

    /* access modifiers changed from: protected */
    public Set<T> getSet() {
        if (this.set != null) {
            return this.set;
        }
        THashSet tHashSet = new THashSet();
        this.set = tHashSet;
        return tHashSet;
    }

    public Collection<T> getCollection() {
        return getSet();
    }

    public boolean contains(Object obj) {
        if (this.set == null) {
            return false;
        }
        return super.contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        if (this.set == null) {
            return false;
        }
        return super.containsAll(collection);
    }

    public boolean removeAll(Collection<?> collection) {
        if (this.set == null) {
            return false;
        }
        return super.removeAll(collection);
    }

    public boolean remove(Object obj) {
        if (this.set == null) {
            return false;
        }
        return super.remove(obj);
    }

    public boolean retainAll(Collection<?> collection) {
        if (this.set == null) {
            return false;
        }
        return super.retainAll(collection);
    }

    public int size() {
        if (this.set == null) {
            return 0;
        }
        return super.size();
    }

    /* renamed from: XC */
    public C1867ad<T> mo271XC() {
        return new C1662YU(getSet());
    }

    public Set<T> aya() {
        if (this.set != null) {
            return this.set;
        }
        THashSet tHashSet = new THashSet();
        this.set = tHashSet;
        return tHashSet;
    }

    public Object clone() {
        return new C0035AS(this.f8051Uk, mo19378ym(), this.set);
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        int readInt = objectInput.readInt();
        if (readInt > 0) {
            this.set = MessageContainer.dgI();
            DataClassSerializer kd = this.f8053Um;
            for (int i = 0; i < readInt; i++) {
                this.set.add(kd.inputReadObject(objectInput));
            }
        }
    }

    public void readExternal(IReadExternal vm) {
        int gX = vm.readInt("count");
        if (gX > 0) {
            this.set = MessageContainer.dgI();
            for (int i = 0; i < gX; i++) {
                this.set.add(vm.mo6366hd((String) null));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0064 A[LOOP:0: B:10:0x003f->B:14:0x0064, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeExternal(java.io.ObjectOutput r5) {
        /*
            r4 = this;
            super.writeExternal((java.io.ObjectOutput) r5)
            java.util.Set<T> r1 = r4.set
            if (r1 == 0) goto L_0x006e
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x006e
            boolean r0 = p001a.C6129agx.m22158d(r5)
            if (r0 == 0) goto L_0x0062
            java.util.Iterator r0 = r1.iterator()
            boolean r0 = p001a.C3186or.m36906a(r0)
            if (r0 == 0) goto L_0x0046
            a.Jz r0 = p001a.C6129agx.m22159e(r5)
            a.arE r2 = r0.bxy()
            java.util.TreeSet r0 = new java.util.TreeSet
            a.or r3 = new a.or
            r3.<init>(r2)
            r0.<init>(r3)
            r0.addAll(r1)
        L_0x0032:
            int r1 = r0.size()
            r5.writeInt(r1)
            a.Kd r1 = r4.f8053Um
            java.util.Iterator r2 = r0.iterator()
        L_0x003f:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0064
        L_0x0045:
            return
        L_0x0046:
            a.UR r0 = log
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Deterministic mode not supported for "
            r2.<init>(r3)
            java.util.Iterator r3 = r1.iterator()
            java.lang.Object r3 = r3.next()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.warn(r2)
        L_0x0062:
            r0 = r1
            goto L_0x0032
        L_0x0064:
            java.lang.Object r0 = r2.next()
            java.lang.Object r0 = (java.lang.Object) r0
            r1.mo2360a((java.io.ObjectOutput) r5, (java.lang.Object) r0)
            goto L_0x003f
        L_0x006e:
            r0 = 0
            r5.writeInt(r0)
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: game.network.message.serializable.C0035AS.writeExternal(java.io.ObjectOutput):void");
    }

    public void writeExternal(IWriteExternal att) {
        Set<T> set2 = this.set;
        if (set2 == null || set2.isEmpty()) {
            att.writeInt("count", 0);
            return;
        }
        att.writeInt("count", set2.size());
        for (T g : set2) {
            att.mo16273g((String) null, (Object) g);
        }
    }

    /* renamed from: c */
    public void mo286c(ObjectInput objectInput) {
        super.mo286c(objectInput);
        int readInt = objectInput.readInt();
        if (readInt > 0) {
            this.set = MessageContainer.dgI();
            DataClassSerializer kd = this.f8053Um;
            for (int i = 0; i < readInt; i++) {
                this.set.add(kd.inputReadObject(objectInput));
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0064 A[LOOP:0: B:10:0x003f->B:14:0x0064, LOOP_END] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo284a(java.io.ObjectOutput r5) {
        /*
            r4 = this;
            super.mo284a(r5)
            java.util.Set<T> r1 = r4.set
            if (r1 == 0) goto L_0x006e
            boolean r0 = r1.isEmpty()
            if (r0 != 0) goto L_0x006e
            boolean r0 = p001a.C6129agx.m22158d(r5)
            if (r0 == 0) goto L_0x0062
            java.util.Iterator r0 = r1.iterator()
            boolean r0 = p001a.C3186or.m36906a(r0)
            if (r0 == 0) goto L_0x0046
            a.Jz r0 = p001a.C6129agx.m22159e(r5)
            a.arE r2 = r0.bxy()
            java.util.TreeSet r0 = new java.util.TreeSet
            a.or r3 = new a.or
            r3.<init>(r2)
            r0.<init>(r3)
            r0.addAll(r1)
        L_0x0032:
            int r1 = r0.size()
            r5.writeInt(r1)
            a.Kd r1 = r4.f8053Um
            java.util.Iterator r2 = r0.iterator()
        L_0x003f:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0064
        L_0x0045:
            return
        L_0x0046:
            a.UR r0 = log
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Deterministic mode not supported for "
            r2.<init>(r3)
            java.util.Iterator r3 = r1.iterator()
            java.lang.Object r3 = r3.next()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.warn(r2)
        L_0x0062:
            r0 = r1
            goto L_0x0032
        L_0x0064:
            java.lang.Object r0 = r2.next()
            java.lang.Object r0 = (java.lang.Object) r0
            r1.mo2360a((java.io.ObjectOutput) r5, (java.lang.Object) r0)
            goto L_0x003f
        L_0x006e:
            r0 = 0
            r5.writeInt(r0)
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: game.network.message.serializable.C0035AS.mo284a(java.io.ObjectOutput):void");
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("[");
        if (this.set != null) {
            Iterator it = getCollection().iterator();
            while (it.hasNext()) {
                stringBuffer.append(it.next());
                if (it.hasNext()) {
                    stringBuffer.append(", ");
                }
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    public boolean equals(Object obj) {
        if (obj instanceof C0035AS) {
            if (this.set != null) {
                return this.set.equals(((C0035AS) obj).getSet());
            }
            if (((C0035AS) obj).getSet() == null) {
                return true;
            }
            return ((C0035AS) obj).getSet().size() == 0;
        } else if (this.set == null) {
            return obj == null;
        } else {
            return this.set.equals(obj);
        }
    }
}
