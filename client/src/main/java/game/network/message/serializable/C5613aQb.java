package game.network.message.serializable;

import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.channel.client.ClientConnect;

import java.io.Serializable;

/* renamed from: a.aQb  reason: case insensitive filesystem */
/* compiled from: a */
public class C5613aQb implements IExternalIO, Serializable {

    public int eMp = -1;
    public long enC = System.currentTimeMillis();
    public int iDL = -1;
    public ClientConnect iDM = null;
    private long iDK = 0;
    private long iDN;

    public long dpb() {
        return this.iDN;
    }

    /* renamed from: lK */
    public void mo11033lK(long j) {
        this.iDN = j;
    }

    public long dpc() {
        return this.iDK;
    }

    public long getTransactionTime() {
        return this.enC;
    }

    public void setTransactionTime(long j) {
        this.enC = j;
    }

    public ClientConnect dpd() {
        return this.iDM;
    }

    /* renamed from: p */
    public void mo11035p(ClientConnect bVar) {
        this.iDM = bVar;
    }

    public int dpe() {
        return this.iDL;
    }

    /* renamed from: Ae */
    public void mo11025Ae(int i) {
        this.iDL = i;
    }

    public int cmf() {
        return this.eMp;
    }

    /* renamed from: Af */
    public void mo11026Af(int i) {
        this.eMp = i;
    }

    /* renamed from: lL */
    public void mo11034lL(long j) {
        this.iDK = j;
    }

    public void readExternal(IReadExternal vm) {
    }

    public void writeExternal(IWriteExternal att) {
    }
}
