package game.network.message.serializable;

import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.externalizable.ObjectId;
import logic.baa.C1616Xf;

import java.io.Serializable;

/* renamed from: a.AD */
/* compiled from: a */
public final class C0010AD implements IExternalIO, Serializable {

    private Class<? extends C1616Xf> agS;

    /* renamed from: qT */
    private ObjectId f9qT;

    public C0010AD() {
    }

    public C0010AD(ObjectId apo, Class<? extends C1616Xf> cls) {
        this.f9qT = apo;
        this.agS = cls;
    }

    /* renamed from: hC */
    public ObjectId mo36hC() {
        return this.f9qT;
    }

    /* renamed from: hD */
    public Class<? extends C1616Xf> mo37hD() {
        return this.agS;
    }

    public String toString() {
        return "ObjectCreation " + (this.agS != null ? this.agS.getName() : "no-class") + ":" + this.f9qT;
    }

    public void readExternal(IReadExternal vm) {
        this.f9qT = new ObjectId(vm.readLong("oid"));
        this.agS = (Class) vm.mo6366hd("scriptClass");
    }

    public void writeExternal(IWriteExternal att) {
        att.writeLong("oid", this.f9qT.getId());
        att.mo16273g("scriptClass", (Object) this.agS);
    }
}
