package game.network.message.serializable;

import logic.res.html.C0029AO;

import java.io.Serializable;
import java.util.Set;

/* renamed from: a.iZ */
/* compiled from: a */
public interface C2686iZ<T> extends Set<T>, C0029AO<T>, Serializable {
}
