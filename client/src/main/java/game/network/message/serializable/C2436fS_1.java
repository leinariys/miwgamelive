package game.network.message.serializable;

import game.io.IExternalIO;
import game.network.exception.CreatListenerChannelException;
import game.network.message.IReadProto;
import game.script.Character;
import org.mozilla1.classfile.C0147Bi;
import p001a.C5539aNf;
import p001a.CountFailedReadOrWriteException;
import p001a.ayF;
import util.Syst;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

/* renamed from: a.fS */
/* compiled from: a */
public class C2436fS_1 extends ObjectInputStream implements C5539aNf, Serializable {
    public static final boolean[][] gVs = ((boolean[][]) Array.newInstance(Boolean.TYPE, new int[]{256, 256}));

    private static boolean debug = false;
    private static /* synthetic */ int[] gVt = null;

    static {
        for (int i = 0; i < 256; i++) {
            m31042d(i, i, true);
            if (i != 134) {
                m31042d(i, 130, true);
            }
        }
        for (int i2 = 0; i2 < 7; i2++) {
            for (int i3 = 0; i3 < 7; i3++) {
                m31042d(i2, i3, true);
            }
        }
    }

    /* renamed from: Jk */
    public int f7356Jk;
    /* renamed from: Jn */
    public Object[] f7357Jn;
    /* renamed from: Jo */
    public int f7358Jo;
    /* renamed from: Jp */
    public boolean f7359Jp = true;
    /* renamed from: Jy */
    public Object f7360Jy;
    /* renamed from: Jz */
    public C0874Mg f7361Jz;
    /* renamed from: TO */
    public ArrayList<C2437a> f7362TO = new ArrayList<>();
    public IReadProto agC;
    public ArrayList<Object> agE = new ArrayList<>();
    public C2438b gVr = new C2438b(134, "<initial>");
    private boolean agD;

    public C2436fS_1(IReadProto ie) {
        this.agC = ie;
        mo18681t((Object) null);
    }

    static /* synthetic */ int[] cDQ() {
        int[] iArr = gVt;
        if (iArr == null) {
            iArr = new int[C2439c.values().length];
            try {
                iArr[C2439c.FINISH_READ.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C2439c.READ_FULLY.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C2439c.READ_PARTIAL.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            gVt = iArr;
        }
        return iArr;
    }

    /* renamed from: d */
    public static final void m31042d(int i, int i2, boolean z) {
        boolean[] zArr = gVs[i];
        gVs[i2][i] = z;
        zArr[i2] = z;
    }

    /* renamed from: hg */
    public boolean mo6369hg(String str) {
        if (!this.f7359Jp) {
            return true;
        }
        C2438b bVar = (C2438b) mo18683z(str, 134);
        if (bVar == null) {
            return false;
        }
        this.gVr = bVar;
        return true;
    }

    /* renamed from: pe */
    public void mo6370pe() throws IOException {
        if (this.f7359Jp) {
            if (mo18683z((String) null, 135) == null) {
                throw new IOException("Space end not found");
            }
            this.gVr.gid = null;
            this.gVr = this.gVr.gic;
        }
    }

    /* renamed from: he */
    public <T extends IExternalIO> T mo6367he(String str) {
        if (this.f7359Jp) {
            return (T) mo18683z(str, 130).getObject();
        }
        try {
            int cDM = cDM();
            if (cDM == 130) {
                return (IExternalIO) m31044wh(this.agC.readInt());
            }
            if (cDM != 129) {
                return (T) mo18647b((C2438b) null, C2439c.READ_FULLY);
            }
            return null;
        } catch (InstantiationException e) {
            throw new CountFailedReadOrWriteException("Error reading " + str, e);
        } catch (IllegalAccessException e2) {
            throw new CountFailedReadOrWriteException("Error reading " + str, e2);
        }
    }

    /* renamed from: kI */
    public <T> T mo18656kI(String str) {
        return mo18682y(str, 130);
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public <T> T mo18682y(String str, int i) {
        if (this.f7359Jp) {
            return (T) mo18683z(str, i).f7364Op;
        }
        return mo18654d((C2438b) null, C2439c.READ_FULLY);
    }

    /* renamed from: kJ */
    public <T> T mo18657kJ(String str) {
        if (!this.f7359Jp) {
            return mo18646b((C2437a) null);
        }
        return (T) mo18683z(str, 130).getObject();
    }

    /* renamed from: r */
    public int mo6371r(String str, int i) {
        if (!this.f7359Jp) {
            return this.agC.readBits(i);
        }
        return (int) (mo18683z(str, 7).f7365Oq & ((long) ((1 << i) - 1)));
    }

    /* renamed from: gR */
    public boolean mo6354gR(String str) throws IOException {
        if (!this.f7359Jp) {
            return this.agC.readBoolean();
        }
        if (mo18683z(str, 0).f7365Oq != 0) {
            return true;
        }
        return false;
    }

    /* renamed from: gS */
    public byte readByte(String str) throws IOException {
        if (!this.f7359Jp) {
            return this.agC.readByte();
        }
        return (byte) ((int) mo18683z(str, 1).f7365Oq);
    }

    /* renamed from: gW */
    public char readChar(String str) throws IOException {
        if (!this.f7359Jp) {
            return this.agC.readChar();
        }
        return (char) ((int) mo18683z(str, 3).f7365Oq);
    }

    /* renamed from: ha */
    public double readDouble(String str) {
        if (!this.f7359Jp) {
            return this.agC.readDouble();
        }
        return mo18683z(str, 128).f7366Or;
    }

    /* renamed from: hf */
    public <T extends Enum<?>> T mo6368hf(String str) {
        if (this.f7359Jp) {
            return (Enum) mo18683z(str, 133).f7364Op;
        }
        switch (cDM()) {
            case 129:
                return null;
            case 130:
                return (Enum) m31044wh(this.agC.readInt());
            default:
                return mo18642a((C2437a) null);
        }
    }

    /* renamed from: gZ */
    public float readFloat(String str) throws IOException {
        if (!this.f7359Jp) {
            return this.agC.readFloat();
        }
        return (float) mo18683z(str, 6).f7366Or;
    }

    /* renamed from: c */
    public void mo6352c(String str, byte[] bArr) {
        if (!this.f7359Jp) {
            this.agC.readFully(bArr);
            return;
        }
        C2437a z = mo18683z(str, 171);
        int length = bArr.length;
        int i = (int) z.f7365Oq;
        if (i < length) {
            throw new IOException("Insufficient data: " + i + " < " + length);
        } else if (z.f7364Op == null) {
            this.agC.readFully(bArr);
            if (length < i) {
                this.agC.skipBytes(i - length);
            }
        } else {
            System.arraycopy((byte[]) z.f7364Op, 0, bArr, 0, length);
        }
    }

    /* renamed from: c */
    public void mo6353c(String str, byte[] bArr, int i, int i2) {
        if (!this.f7359Jp) {
            this.agC.readFully(bArr, i, i2);
            return;
        }
        C2437a z = mo18683z(str, 171);
        int i3 = (int) z.f7365Oq;
        if (i3 < i2) {
            throw new IOException("Insufficient data");
        } else if (z.f7364Op == null) {
            this.agC.readFully(bArr, i, i2);
            if (i2 < i3) {
                this.agC.skipBytes(i3 - i2);
            }
        } else {
            System.arraycopy((byte[]) z.f7364Op, 0, bArr, i, i2);
        }
    }

    /* renamed from: gX */
    public int readInt(String str) {
        if (!this.f7359Jp) {
            return this.agC.readInt();
        }
        return (int) mo18683z(str, 4).f7365Oq;
    }

    @Deprecated
    /* renamed from: hb */
    public String mo6364hb(String str) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: gY */
    public long readLong(String str) {
        if (!this.f7359Jp) {
            return this.agC.readLong();
        }
        return mo18683z(str, 5).f7365Oq;
    }

    /* JADX WARNING: type inference failed for: r0v10, types: [T, java.lang.Object, double[]] */
    /* JADX WARNING: type inference failed for: r0v11, types: [T, float[], java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v14, types: [long[], T, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v15, types: [T, int[], java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v18, types: [T, java.lang.Object, byte[]] */
    /* JADX WARNING: type inference failed for: r0v20, types: [T, byte[]] */
    /* renamed from: hd */
    public <T> T mo6366hd(String str) throws IOException {
        int i = 0;
        if (this.f7359Jp) {
            return (T) mo18683z(str, 130).getObject();
        }
        int cDM = cDM();
        switch (cDM) {
            case 0:
                return (T) Boolean.valueOf(this.agC.readBoolean());
            case 1:
                return (T) Byte.valueOf(this.agC.readByte());
            case 2:
                return (T) Short.valueOf(this.agC.readShort());
            case 3:
                return Character.valueOf(this.agC.readChar());
            case 4:
                return (T) Integer.valueOf(this.agC.readInt());
            case 5:
                return (T) Long.valueOf(this.agC.readLong());
            case 6:
                return (T) Float.valueOf(this.agC.readFloat());
            case 7:
                throw new IllegalStateException();
            case 128:
                return (T) Double.valueOf(this.agC.readDouble());
            case 129:
                return null;
            case 130:
                return (T) m31044wh(this.agC.readInt());
            case 131:
                return (T) cDN();
            case 133:
                return (T) mo6368hf(str);
            case 134:
                throw new IllegalStateException("Found space end instead of an object.");
            case 135:
                throw new IllegalStateException("Found space end instead of an object.");
            case 144:
                return (T) cDO();
            case 145:
                return mo18682y(str, 145);
            case 146:
                return mo18649c((C2438b) null, C2439c.READ_FULLY);
            case 147:
                return mo18682y(str, 145);
            case 160:
                int readInt = readInt();
                boolean[] zArr = new boolean[readInt];
                while (i < readInt) {
                    zArr[i] = this.agC.readBoolean();
                    i++;
                }
                mo18681t(zArr);
                return (T) zArr;
            case 161:
                byte[] r0 = new byte[readInt()];
                this.agC.readFully(r0);
                mo18681t(r0);
                return (T) r0;
            case 162:
                int readInt2 = readInt();
                short[] sArr = new short[readInt2];
                while (i < readInt2) {
                    sArr[i] = this.agC.readShort();
                    i++;
                }
                mo18681t(sArr);
                return (T) sArr;
            case 163:
                int readInt3 = readInt();
                char[] cArr = new char[readInt3];
                while (i < readInt3) {
                    cArr[i] = this.agC.readChar();
                    i++;
                }
                mo18681t(cArr);
                return (T) cArr;
            case 164:
                int readInt4 = readInt();
                int[] r02 = new int[readInt4];
                while (i < readInt4) {
                    r02[i] = this.agC.readInt();
                    i++;
                }
                mo18681t(r02);
                return r02;
            case 165:
                int readInt5 = readInt();
                long[] r03 = new long[readInt5];
                while (i < readInt5) {
                    r03[i] = this.agC.readLong();
                    i++;
                }
                mo18681t(r03);
                return r03;
            case 166:
                int readInt6 = readInt();
                float[] r04 = new float[readInt6];
                while (i < readInt6) {
                    r04[i] = this.agC.readFloat();
                    i++;
                }
                mo18681t(r04);
                return (T) r04;
            case 169:
                int readInt7 = readInt();
                double[] r05 = new double[readInt7];
                while (i < readInt7) {
                    r05[i] = this.agC.readDouble();
                    i++;
                }
                mo18681t(r05);
                return (T) r05;
            case 170:
                return mo18657kJ(str);
            case 171:
                byte[] r06 = new byte[readInt()];
                this.agC.readFully(r06);
                return (T) r06;
            default:
                throw new IllegalStateException("Illegal type: " + ayF.names[cDM]);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Enum<?> mo18642a(C2437a aVar) {
        Enum<?> enumR;
        Class cDO = cDO();
        int readInt = this.agC.readInt();
        if (aVar != null) {
            enumR = Enum.valueOf(cDO, cDN());
            aVar.f7364Op = enumR;
            long j = (long) readInt;
            aVar.f7365Oq = j;
            aVar.f7366Or = (double) j;
        } else {
            enumR = ((Enum[]) cDO.getEnumConstants())[readInt];
        }
        mo18681t(enumR);
        return enumR;
    }

    /* renamed from: gU */
    public short mo6357gU(String str) throws IOException {
        if (!this.f7359Jp) {
            return this.agC.readShort();
        }
        return (short) ((int) mo18683z(str, 2).f7365Oq);
    }

    /* renamed from: hc */
    public String mo6365hc(String str) {
        if (!this.f7359Jp) {
            return cDN();
        }
        return String.valueOf(mo18683z(str, 130).f7364Op);
    }

    /* renamed from: gT */
    public int mo6356gT(String str) {
        if (!this.f7359Jp) {
            return this.agC.readByte() & 255;
        }
        return (int) (mo18683z(str, 1).f7365Oq & 255);
    }

    /* renamed from: gV */
    public int mo6358gV(String str) {
        if (!this.f7359Jp) {
            return this.agC.readShort() & 65535;
        }
        return (int) (mo18683z(str, 2).f7365Oq & 65535);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x03dc, code lost:
        if (m31041aE(r16, r3.key) == false) goto L_0x03de;
     */
    /* renamed from: z */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C2436fS_1.C2437a mo18683z(java.lang.String r16, int r17) {
        /*
            r15 = this;
            r14 = -1
            r4 = 0
            r9 = 1
            r8 = 0
            boolean r2 = r15.f7359Jp
            if (r2 != 0) goto L_0x000e
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            r2.<init>()
            throw r2
        L_0x000e:
            a.fS$b r5 = r15.gVr
            boolean r2 = debug     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x0036
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007b }
            java.lang.String r6 = "find : "
            r3.<init>(r6)     // Catch:{ Exception -> 0x007b }
            r0 = r17
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x007b }
            java.lang.String r6 = " "
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x007b }
            r0 = r16
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x007b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x007b }
            r2.println(r3)     // Catch:{ Exception -> 0x007b }
        L_0x0036:
            a.fS$b r2 = r15.gVr     // Catch:{ Exception -> 0x007b }
            a.fS$a r3 = r2.gid     // Catch:{ Exception -> 0x007b }
            r2 = r4
        L_0x003b:
            if (r3 != 0) goto L_0x0049
            r6 = r3
        L_0x003e:
            a.fS$b r2 = r15.gVr     // Catch:{ Exception -> 0x007b }
            boolean r2 = r2.done     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x013f
            a.fS$a r3 = r15.mo7913A(r16, r17)
        L_0x0048:
            return r3
        L_0x0049:
            java.lang.String r6 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r6 = r15.m31041aE(r0, r6)     // Catch:{ Exception -> 0x007b }
            if (r6 == 0) goto L_0x0139
            int r6 = r3.type     // Catch:{ Exception -> 0x007b }
            r0 = r17
            boolean r6 = r15.mo18644an(r6, r0)     // Catch:{ Exception -> 0x007b }
            if (r6 == 0) goto L_0x0139
            if (r2 == 0) goto L_0x00c2
            a.fS$a r4 = r3.f7363Oo     // Catch:{ Exception -> 0x007b }
            r2.f7363Oo = r4     // Catch:{ Exception -> 0x007b }
        L_0x0063:
            int r2 = r3.type     // Catch:{ Exception -> 0x007b }
            switch(r2) {
                case 130: goto L_0x0069;
                case 145: goto L_0x0103;
                case 146: goto L_0x00e6;
                case 147: goto L_0x00c9;
                case 240: goto L_0x0120;
                default: goto L_0x0068;
            }     // Catch:{ Exception -> 0x007b }
        L_0x0068:
            goto L_0x0048
        L_0x0069:
            java.lang.Object r2 = r3.f7364Op     // Catch:{ Exception -> 0x007b }
            if (r2 != 0) goto L_0x0048
            long r6 = r3.f7365Oq     // Catch:{ Exception -> 0x007b }
            int r2 = (int) r6     // Catch:{ Exception -> 0x007b }
            java.lang.Object r2 = r15.m31044wh(r2)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            r6 = 0
            r3.f7365Oq = r6     // Catch:{ Exception -> 0x007b }
            goto L_0x0048
        L_0x007b:
            r2 = move-exception
            r3 = r2
            r2 = r5
        L_0x007e:
            if (r2 != 0) goto L_0x0499
            a.adk r4 = new a.adk
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r2 = "Error looking for "
            r5.<init>(r2)
            if (r17 < 0) goto L_0x04b8
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String[] r6 = p001a.ayF.names
            r6 = r6[r17]
            java.lang.String r6 = java.lang.String.valueOf(r6)
            r2.<init>(r6)
            java.lang.String r6 = ":"
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r6 = java.lang.Integer.toHexString(r17)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r2 = r2.toString()
        L_0x00aa:
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " "
            java.lang.StringBuilder r2 = r2.append(r5)
            r0 = r16
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            r4.<init>(r2, r3)
            throw r4
        L_0x00c2:
            a.fS$b r2 = r15.gVr     // Catch:{ Exception -> 0x007b }
            a.fS$a r4 = r3.f7363Oo     // Catch:{ Exception -> 0x007b }
            r2.gid = r4     // Catch:{ Exception -> 0x007b }
            goto L_0x0063
        L_0x00c9:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            boolean r2 = r2.gif     // Catch:{ Exception -> 0x007b }
            if (r2 != 0) goto L_0x0048
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            a.fS$c r4 = p001a.C2436fS.C2439c.FINISH_READ     // Catch:{ Exception -> 0x007b }
            java.lang.Object r2 = r15.mo18654d((p001a.C2436fS.C2438b) r2, (p001a.C2436fS.C2439c) r4)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r4 = 1
            r2.gif = r4     // Catch:{ Exception -> 0x007b }
            goto L_0x0048
        L_0x00e6:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            boolean r2 = r2.gif     // Catch:{ Exception -> 0x007b }
            if (r2 != 0) goto L_0x0048
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            a.fS$c r4 = p001a.C2436fS.C2439c.FINISH_READ     // Catch:{ Exception -> 0x007b }
            java.lang.Object r2 = r15.mo18649c((p001a.C2436fS.C2438b) r2, (p001a.C2436fS.C2439c) r4)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r4 = 1
            r2.gif = r4     // Catch:{ Exception -> 0x007b }
            goto L_0x0048
        L_0x0103:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            boolean r2 = r2.gif     // Catch:{ Exception -> 0x007b }
            if (r2 != 0) goto L_0x0048
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            a.fS$c r4 = p001a.C2436fS.C2439c.FINISH_READ     // Catch:{ Exception -> 0x007b }
            java.lang.Object r2 = r15.mo18647b((p001a.C2436fS.C2438b) r2, (p001a.C2436fS.C2439c) r4)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r4 = 1
            r2.gif = r4     // Catch:{ Exception -> 0x007b }
            goto L_0x0048
        L_0x0120:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            boolean r2 = r2.gif     // Catch:{ Exception -> 0x007b }
            if (r2 != 0) goto L_0x0048
            a.fS$b r3 = (p001a.C2436fS.C2438b) r3     // Catch:{ Exception -> 0x007b }
            a.fS$c r2 = p001a.C2436fS.C2439c.FINISH_READ     // Catch:{ Exception -> 0x007b }
            a.fS$a r3 = r15.mo10814a(r3, r2)     // Catch:{ Exception -> 0x007b }
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r4 = 1
            r2.gif = r4     // Catch:{ Exception -> 0x007b }
            goto L_0x0048
        L_0x0139:
            a.fS$a r6 = r3.f7363Oo     // Catch:{ Exception -> 0x007b }
            r2 = r3
            r3 = r6
            goto L_0x003b
        L_0x013f:
            int r7 = r15.cDM()     // Catch:{ Exception -> 0x007b }
            r2 = 135(0x87, float:1.89E-43)
            if (r7 == r2) goto L_0x01a9
            java.lang.String r2 = r15.m31039Eh()     // Catch:{ Exception -> 0x007b }
        L_0x014b:
            a.fS$a r3 = r15.mo18653d((int) r7, (java.lang.String) r2)     // Catch:{ Exception -> 0x007b }
            boolean r2 = debug     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x0173
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007b }
            java.lang.String r11 = " "
            r10.<init>(r11)     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r10 = r10.append(r7)     // Catch:{ Exception -> 0x007b }
            java.lang.String r11 = " : "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x007b }
            java.lang.String r11 = r3.key     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x007b }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x007b }
            r2.println(r10)     // Catch:{ Exception -> 0x007b }
        L_0x0173:
            switch(r7) {
                case 0: goto L_0x01ab;
                case 1: goto L_0x01ec;
                case 2: goto L_0x0212;
                case 3: goto L_0x0226;
                case 4: goto L_0x01f9;
                case 5: goto L_0x0206;
                case 6: goto L_0x0233;
                case 7: goto L_0x039e;
                case 128: goto L_0x026e;
                case 129: goto L_0x01e8;
                case 130: goto L_0x0241;
                case 131: goto L_0x03b2;
                case 133: goto L_0x03c1;
                case 134: goto L_0x03c6;
                case 135: goto L_0x0487;
                case 144: goto L_0x021f;
                case 145: goto L_0x0414;
                case 146: goto L_0x045e;
                case 147: goto L_0x0439;
                case 160: goto L_0x0336;
                case 161: goto L_0x029f;
                case 162: goto L_0x02d6;
                case 163: goto L_0x02f6;
                case 164: goto L_0x02b6;
                case 165: goto L_0x0316;
                case 166: goto L_0x0356;
                case 169: goto L_0x0376;
                case 170: goto L_0x0396;
                case 171: goto L_0x027b;
                case 240: goto L_0x03eb;
                default: goto L_0x0176;
            }     // Catch:{ Exception -> 0x007b }
        L_0x0176:
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007b }
            java.lang.String r4 = "Illegal type: "
            r3.<init>(r4)     // Catch:{ Exception -> 0x007b }
            java.lang.String[] r4 = p001a.ayF.names     // Catch:{ Exception -> 0x007b }
            r4 = r4[r7]     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007b }
            java.lang.String r4 = " expecting "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007b }
            java.lang.String[] r4 = p001a.ayF.names     // Catch:{ Exception -> 0x007b }
            r4 = r4[r17]     // Catch:{ Exception -> 0x007b }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007b }
            java.lang.String r4 = " "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x007b }
            r0 = r16
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x007b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x007b }
            r2.<init>(r3)     // Catch:{ Exception -> 0x007b }
            throw r2     // Catch:{ Exception -> 0x007b }
        L_0x01a9:
            r2 = r4
            goto L_0x014b
        L_0x01ab:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            boolean r2 = r2.readBoolean()     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x01e6
            r2 = r9
        L_0x01b4:
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
        L_0x01ba:
            r0 = r17
            if (r0 == r14) goto L_0x01d2
            int r2 = r3.type     // Catch:{ Exception -> 0x007b }
            r0 = r17
            boolean r2 = r15.mo18644an(r2, r0)     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x01d2
            java.lang.String r2 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r2 = r15.m31041aE(r0, r2)     // Catch:{ Exception -> 0x007b }
            if (r2 != 0) goto L_0x0048
        L_0x01d2:
            if (r6 != 0) goto L_0x0494
            a.fS$b r2 = r15.gVr     // Catch:{ Exception -> 0x007b }
            r2.gid = r3     // Catch:{ Exception -> 0x007b }
            r2 = r3
        L_0x01d9:
            int r3 = r3.type     // Catch:{ Exception -> 0x007b }
            r6 = 135(0x87, float:1.89E-43)
            if (r3 != r6) goto L_0x04be
            r0 = r17
            if (r0 != r14) goto L_0x04be
            r3 = r4
            goto L_0x0048
        L_0x01e6:
            r2 = r8
            goto L_0x01b4
        L_0x01e8:
            r2 = 0
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x01ec:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            byte r2 = r2.readByte()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x01f9:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r2 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0206:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            long r10 = r2.readLong()     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0212:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            short r2 = r2.readShort()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x021f:
            java.lang.Class r2 = r15.cDO()     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0226:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            char r2 = r2.readChar()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0233:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            float r2 = r2.readFloat()     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r2     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r10     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0241:
            r2 = 130(0x82, float:1.82E-43)
            r0 = r17
            boolean r2 = r15.mo18644an(r0, r2)     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x0263
            java.lang.String r2 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r2 = r15.m31041aE(r0, r2)     // Catch:{ Exception -> 0x007b }
            if (r2 == 0) goto L_0x0263
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r2 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            java.lang.Object r2 = r15.m31044wh(r2)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0263:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r2 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x026e:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            double r10 = r2.readDouble()     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r10     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x027b:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r2 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            r7 = 171(0xab, float:2.4E-43)
            r0 = r17
            if (r0 != r7) goto L_0x0294
            java.lang.String r7 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r7 = r15.m31041aE(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 != 0) goto L_0x0048
        L_0x0294:
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x007b }
            a.Ie r7 = r15.agC     // Catch:{ Exception -> 0x007b }
            r7.readFully(r2)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x029f:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r2 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x007b }
            a.Ie r7 = r15.agC     // Catch:{ Exception -> 0x007b }
            r7.readFully(r2)     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r2)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x02b6:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r7     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            int[] r10 = new int[r7]     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r10)     // Catch:{ Exception -> 0x007b }
            r2 = r8
        L_0x02c5:
            if (r2 < r7) goto L_0x02cb
            r3.f7364Op = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x02cb:
            a.Ie r11 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r11 = r11.readInt()     // Catch:{ Exception -> 0x007b }
            r10[r2] = r11     // Catch:{ Exception -> 0x007b }
            int r2 = r2 + 1
            goto L_0x02c5
        L_0x02d6:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r7     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            short[] r10 = new short[r7]     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r10)     // Catch:{ Exception -> 0x007b }
            r2 = r8
        L_0x02e5:
            if (r2 < r7) goto L_0x02eb
            r3.f7364Op = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x02eb:
            a.Ie r11 = r15.agC     // Catch:{ Exception -> 0x007b }
            short r11 = r11.readShort()     // Catch:{ Exception -> 0x007b }
            r10[r2] = r11     // Catch:{ Exception -> 0x007b }
            int r2 = r2 + 1
            goto L_0x02e5
        L_0x02f6:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r7     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            char[] r10 = new char[r7]     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r10)     // Catch:{ Exception -> 0x007b }
            r2 = r8
        L_0x0305:
            if (r2 < r7) goto L_0x030b
            r3.f7364Op = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x030b:
            a.Ie r11 = r15.agC     // Catch:{ Exception -> 0x007b }
            char r11 = r11.readChar()     // Catch:{ Exception -> 0x007b }
            r10[r2] = r11     // Catch:{ Exception -> 0x007b }
            int r2 = r2 + 1
            goto L_0x0305
        L_0x0316:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r7     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            long[] r10 = new long[r7]     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r10)     // Catch:{ Exception -> 0x007b }
            r2 = r8
        L_0x0325:
            if (r2 < r7) goto L_0x032b
            r3.f7364Op = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x032b:
            a.Ie r11 = r15.agC     // Catch:{ Exception -> 0x007b }
            long r12 = r11.readLong()     // Catch:{ Exception -> 0x007b }
            r10[r2] = r12     // Catch:{ Exception -> 0x007b }
            int r2 = r2 + 1
            goto L_0x0325
        L_0x0336:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r7     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            boolean[] r10 = new boolean[r7]     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r10)     // Catch:{ Exception -> 0x007b }
            r2 = r8
        L_0x0345:
            if (r2 < r7) goto L_0x034b
            r3.f7364Op = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x034b:
            a.Ie r11 = r15.agC     // Catch:{ Exception -> 0x007b }
            boolean r11 = r11.readBoolean()     // Catch:{ Exception -> 0x007b }
            r10[r2] = r11     // Catch:{ Exception -> 0x007b }
            int r2 = r2 + 1
            goto L_0x0345
        L_0x0356:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r7     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            float[] r10 = new float[r7]     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r10)     // Catch:{ Exception -> 0x007b }
            r2 = r8
        L_0x0365:
            if (r2 < r7) goto L_0x036b
            r3.f7364Op = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x036b:
            a.Ie r11 = r15.agC     // Catch:{ Exception -> 0x007b }
            float r11 = r11.readFloat()     // Catch:{ Exception -> 0x007b }
            r10[r2] = r11     // Catch:{ Exception -> 0x007b }
            int r2 = r2 + 1
            goto L_0x0365
        L_0x0376:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r7 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r7     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double[] r10 = new double[r7]     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r10)     // Catch:{ Exception -> 0x007b }
            r2 = r8
        L_0x0385:
            if (r2 < r7) goto L_0x038b
            r3.f7364Op = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x038b:
            a.Ie r11 = r15.agC     // Catch:{ Exception -> 0x007b }
            double r12 = r11.readDouble()     // Catch:{ Exception -> 0x007b }
            r10[r2] = r12     // Catch:{ Exception -> 0x007b }
            int r2 = r2 + 1
            goto L_0x0385
        L_0x0396:
            java.lang.Object r2 = r15.mo18646b(r3)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x039e:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r2 = r2.readInt()     // Catch:{ Exception -> 0x007b }
            a.Ie r7 = r15.agC     // Catch:{ Exception -> 0x007b }
            int r2 = r7.mo8532a(r2)     // Catch:{ Exception -> 0x007b }
            long r10 = (long) r2     // Catch:{ Exception -> 0x007b }
            r3.f7365Oq = r10     // Catch:{ Exception -> 0x007b }
            double r10 = (double) r10     // Catch:{ Exception -> 0x007b }
            r3.f7366Or = r10     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x03b2:
            a.Ie r2 = r15.agC     // Catch:{ Exception -> 0x007b }
            java.lang.String r2 = r2.readUTF()     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            java.lang.Object r2 = r3.f7364Op     // Catch:{ Exception -> 0x007b }
            r15.mo18681t(r2)     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x03c1:
            r15.mo18642a((p001a.C2436fS.C2437a) r3)     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x03c6:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            a.fS$b r7 = r15.gVr     // Catch:{ Exception -> 0x007b }
            r2.gic = r7     // Catch:{ Exception -> 0x007b }
            r7 = 134(0x86, float:1.88E-43)
            r0 = r17
            if (r0 != r7) goto L_0x03de
            java.lang.String r7 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r7 = r15.m31041aE(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 != 0) goto L_0x01ba
        L_0x03de:
            r15.gVr = r2     // Catch:{ Exception -> 0x007b }
            r7 = 0
            r10 = -1
            r15.mo18683z(r7, r10)     // Catch:{ Exception -> 0x007b }
            a.fS$b r2 = r2.gic     // Catch:{ Exception -> 0x007b }
            r15.gVr = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x03eb:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            a.fS$b r7 = r15.gVr     // Catch:{ Exception -> 0x007b }
            r2.gic = r7     // Catch:{ Exception -> 0x007b }
            r15.gVr = r2     // Catch:{ Exception -> 0x007b }
            r7 = 147(0x93, float:2.06E-43)
            r0 = r17
            boolean r7 = r15.mo18644an(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 == 0) goto L_0x0411
            java.lang.String r3 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r3 = r15.m31041aE(r0, r3)     // Catch:{ Exception -> 0x007b }
            if (r3 == 0) goto L_0x0411
            a.fS$c r3 = p001a.C2436fS.C2439c.READ_FULLY     // Catch:{ Exception -> 0x007b }
        L_0x040b:
            a.fS$a r3 = r15.mo10814a(r2, r3)     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0411:
            a.fS$c r3 = p001a.C2436fS.C2439c.READ_PARTIAL     // Catch:{ Exception -> 0x007b }
            goto L_0x040b
        L_0x0414:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r7 = 147(0x93, float:2.06E-43)
            r0 = r17
            boolean r7 = r15.mo18644an(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 == 0) goto L_0x0436
            java.lang.String r7 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r7 = r15.m31041aE(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 == 0) goto L_0x0436
            a.fS$c r7 = p001a.C2436fS.C2439c.READ_FULLY     // Catch:{ Exception -> 0x007b }
        L_0x042e:
            java.lang.Object r2 = r15.mo18647b((p001a.C2436fS.C2438b) r2, (p001a.C2436fS.C2439c) r7)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0436:
            a.fS$c r7 = p001a.C2436fS.C2439c.READ_PARTIAL     // Catch:{ Exception -> 0x007b }
            goto L_0x042e
        L_0x0439:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r7 = 147(0x93, float:2.06E-43)
            r0 = r17
            boolean r7 = r15.mo18644an(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 == 0) goto L_0x045b
            java.lang.String r7 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r7 = r15.m31041aE(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 == 0) goto L_0x045b
            a.fS$c r7 = p001a.C2436fS.C2439c.READ_FULLY     // Catch:{ Exception -> 0x007b }
        L_0x0453:
            java.lang.Object r2 = r15.mo18654d((p001a.C2436fS.C2438b) r2, (p001a.C2436fS.C2439c) r7)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x045b:
            a.fS$c r7 = p001a.C2436fS.C2439c.READ_PARTIAL     // Catch:{ Exception -> 0x007b }
            goto L_0x0453
        L_0x045e:
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r0 = r3
            a.fS$b r0 = (p001a.C2436fS.C2438b) r0     // Catch:{ Exception -> 0x007b }
            r2 = r0
            r7 = 147(0x93, float:2.06E-43)
            r0 = r17
            boolean r7 = r15.mo18644an(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 == 0) goto L_0x0484
            java.lang.String r7 = r3.key     // Catch:{ Exception -> 0x007b }
            r0 = r16
            boolean r7 = r15.m31041aE(r0, r7)     // Catch:{ Exception -> 0x007b }
            if (r7 == 0) goto L_0x0484
            a.fS$c r7 = p001a.C2436fS.C2439c.READ_FULLY     // Catch:{ Exception -> 0x007b }
        L_0x047c:
            java.lang.Object r2 = r15.mo18649c((p001a.C2436fS.C2438b) r2, (p001a.C2436fS.C2439c) r7)     // Catch:{ Exception -> 0x007b }
            r3.f7364Op = r2     // Catch:{ Exception -> 0x007b }
            goto L_0x01ba
        L_0x0484:
            a.fS$c r7 = p001a.C2436fS.C2439c.READ_PARTIAL     // Catch:{ Exception -> 0x007b }
            goto L_0x047c
        L_0x0487:
            a.fS$b r2 = r15.gVr     // Catch:{ Exception -> 0x007b }
            r7 = 1
            r2.done = r7     // Catch:{ Exception -> 0x007b }
            r2 = 135(0x87, float:1.89E-43)
            r0 = r17
            if (r0 != r2) goto L_0x01ba
            goto L_0x0048
        L_0x0494:
            r6.f7363Oo = r3     // Catch:{ Exception -> 0x007b }
            r2 = r3
            goto L_0x01d9
        L_0x0499:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = r2.key
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r4.<init>(r5)
            r5 = 47
            java.lang.StringBuilder r4 = r4.append(r5)
            r0 = r16
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r16 = r4.toString()
            a.fS$b r2 = r2.gic
            goto L_0x007e
        L_0x04b8:
            java.lang.String r2 = java.lang.String.valueOf(r17)
            goto L_0x00aa
        L_0x04be:
            r6 = r2
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2436fS.mo18683z(java.lang.String, int):a.fS$a");
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public C2437a mo7913A(String str, int i) {
        for (C2438b bVar = this.gVr; bVar != null; bVar = bVar.gic) {
            str = String.valueOf(bVar.key) + C0147Bi.cla + str;
        }
        throw new IOException("Could not find " + (i >= 0 ? ayF.names[i] : String.valueOf(i)) + " " + str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C2437a mo10814a(C2438b bVar, C2439c cVar) {
        return bVar;
    }

    /* access modifiers changed from: protected */
    public int cDM() {
        int a = this.agC.readBits(4);
        return (a & 8) == 0 ? a : (a << 4) | this.agC.readBits(4);
    }

    /* access modifiers changed from: protected */
    /* renamed from: an */
    public boolean mo18644an(int i, int i2) {
        return gVs[i & 255][i2 & 255];
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Object mo18647b(C2438b bVar, C2439c cVar) {
        Object bgP;
        switch (cDQ()[cVar.ordinal()]) {
            case 1:
                bgP = C0874Mg.m7123G(cDO()).bgP();
                mo18681t(bgP);
                break;
            case 2:
                mo18643a(bVar);
                return null;
            default:
                bgP = C0874Mg.m7123G(bVar.clazz).bgP();
                bVar.f7364Op = bgP;
                break;
        }
        if (!this.f7359Jp) {
            ((IExternalIO) bgP).readExternal(this);
            return bgP;
        }
        bVar.gif = true;
        bVar.gic = this.gVr;
        this.gVr = bVar;
        ((IExternalIO) bgP).readExternal(this);
        mo6370pe();
        return bgP;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public <T> T mo18649c(C2438b bVar, C2439c cVar) {
        T bgP;
        switch (cDQ()[cVar.ordinal()]) {
            case 1:
                try {
                    bgP = C0874Mg.m7123G(cDO()).bgP();
                    mo18681t(bgP);
                    break;
                } catch (InstantiationException e) {
                    throw new CountFailedReadOrWriteException((Throwable) e);
                }
            case 2:
                mo18643a(bVar);
                return null;
            default:
                try {
                    bgP = C0874Mg.m7123G(bVar.clazz).bgP();
                    bVar.f7364Op = bgP;
                    break;
                } catch (InstantiationException e2) {
                    throw new CountFailedReadOrWriteException((Throwable) e2);
                }
        }
        if (this.f7359Jp) {
            bVar.gif = true;
            bVar.gic = this.gVr;
            this.gVr = bVar;
        }
        ((Externalizable) bgP).readExternal(this);
        if (!this.f7359Jp) {
            return bgP;
        }
        mo6370pe();
        return bgP;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T mo18654d(C2436fS_1.C2438b r6, C2436fS_1.C2439c r7) {
        /*
            r5 = this;
            int[] r0 = cDQ()
            int r1 = r7.ordinal()
            r0 = r0[r1]
            switch(r0) {
                case 1: goto L_0x0051;
                case 2: goto L_0x0068;
                default: goto L_0x000d;
            }
        L_0x000d:
            java.lang.Class<?> r2 = r6.clazz
            a.Mg r1 = game.network.message.serializable.C0874Mg.m7123G(r2)
            java.lang.Object r0 = r1.bgP()     // Catch:{ InstantiationException -> 0x006d }
            r6.f7364Op = r0     // Catch:{ InstantiationException -> 0x006d }
        L_0x0019:
            boolean r3 = r5.f7359Jp
            if (r3 == 0) goto L_0x0026
            r3 = 1
            r6.gif = r3
            a.fS$b r3 = r5.gVr
            r6.gic = r3
            r5.gVr = r6
        L_0x0026:
            boolean r3 = r1.bgS()
            if (r3 == 0) goto L_0x007a
            java.lang.Object r3 = r5.f7360Jy
            a.Mg r4 = r5.f7361Jz
            r5.f7360Jy = r0
            r5.f7361Jz = r1
            r1.mo4017a((java.lang.Object) r0, (java.io.ObjectInputStream) r5)     // Catch:{ all -> 0x0074 }
            r5.f7360Jy = r3
            r5.f7361Jz = r4
        L_0x003b:
            java.lang.Class r2 = r2.getSuperclass()
            a.Mg r1 = game.network.message.serializable.C0874Mg.m7123G(r2)
            if (r2 == 0) goto L_0x0049
            java.lang.Class<java.lang.Object> r3 = java.lang.Object.class
            if (r2 != r3) goto L_0x0026
        L_0x0049:
            boolean r1 = r5.f7359Jp
            if (r1 == 0) goto L_0x0050
            r5.mo6370pe()
        L_0x0050:
            return r0
        L_0x0051:
            java.lang.Class r2 = r5.cDO()
            a.Mg r1 = game.network.message.serializable.C0874Mg.m7123G(r2)
            java.lang.Object r0 = r1.bgP()     // Catch:{ InstantiationException -> 0x0061 }
            r5.mo18681t(r0)
            goto L_0x0019
        L_0x0061:
            r0 = move-exception
            a.adk r1 = new a.adk
            r1.<init>((java.lang.Throwable) r0)
            throw r1
        L_0x0068:
            r5.mo18643a((p001a.C2436fS.C2438b) r6)
            r0 = 0
            goto L_0x0050
        L_0x006d:
            r0 = move-exception
            a.adk r1 = new a.adk
            r1.<init>((java.lang.Throwable) r0)
            throw r1
        L_0x0074:
            r0 = move-exception
            r5.f7360Jy = r3
            r5.f7361Jz = r4
            throw r0
        L_0x007a:
            r5.mo18648b((java.lang.Object) r0, (game.network.message.serializable.C0874Mg) r1)
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2436fS.mo18654d(a.fS$b, a.fS$c):java.lang.Object");
    }

    /* renamed from: a */
    public void mo18643a(C2438b bVar) {
        bVar.gic = this.gVr;
        bVar.gie = this.agE.size();
        bVar.clazz = cDO();
        mo18681t(bVar);
        this.gVr = bVar;
        mo18683z((String) null, -1);
        this.gVr = bVar.gic;
    }

    public void defaultReadObject() {
        if (this.f7361Jz == null) {
            throw new NotActiveException("not in call to readObject");
        }
        mo18648b(this.f7360Jy, this.f7361Jz);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo18648b(Object obj, C0874Mg mg) {
        if (mg.bgN() > 0) {
            mg.mo4015a(obj, this);
        }
        if (mg.bgO() > 0) {
            int i = this.f7358Jo;
            int bgO = mg.bgO();
            if (this.f7357Jn == null) {
                this.f7357Jn = new Object[bgO];
                this.f7358Jo = bgO;
            } else {
                this.f7358Jo += bgO;
                if (this.f7358Jo > this.f7357Jn.length) {
                    this.f7357Jn = Syst.copyOf((T[]) this.f7357Jn, this.f7358Jo + bgO);
                }
            }
            int i2 = 0;
            while (i2 < bgO) {
                String name = mg.mo4035lK(i2).getName();
                try {
                    this.f7357Jn[i2 + i] = mo6366hd(name);
                    i2++;
                } catch (Exception e) {
                    throw new CountFailedReadOrWriteException("Error reading field " + name, e);
                }
            }
            mg.mo4020b(obj, this.f7357Jn, i);
            this.f7358Jo = i;
        }
    }

    /* JADX WARNING: type inference failed for: r0v2, types: [T, java.lang.Object[], java.lang.Object] */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public <T> T mo18646b(C2437a aVar) {
        Class<?> cDO = cDO();
        int readInt = this.agC.readInt();
        if (this.f7359Jp) {
            if (!(aVar instanceof C2438b)) {
                System.out.println();
            }
            C2438b bVar = (C2438b) aVar;
            bVar.gic = this.gVr;
            this.gVr = bVar;
        }
        Object r0 = (Object[]) Array.newInstance(cDO, readInt);
        mo18681t(r0);
        for (int i = 0; i < readInt; i++) {
            r0[i] = mo6366hd((String) null);
        }
        if (this.f7359Jp) {
            mo6370pe();
        }
        return r0;
    }

    /* renamed from: aE */
    private boolean m31041aE(String str, String str2) {
        if (str == str2) {
            return true;
        }
        if (str == null || str2 == null) {
            return false;
        }
        return str.equals(str2);
    }

    /* access modifiers changed from: protected */
    public String cDN() {
        String str = (String) m31040Ei();
        if (!this.agD) {
            return str;
        }
        String readUTF = this.agC.readUTF();
        mo18681t(readUTF);
        return readUTF;
    }

    /* access modifiers changed from: protected */
    public Class<?> cDO() {
        Class<?> cls = (Class) m31040Ei();
        if (this.agD) {
            String cDN = cDN();
            try {
                cls = Class.forName(cDN);
            } catch (ClassNotFoundException e) {
                cls = m31043kK(cDN);
                if (cls == null) {
                    throw e;
                }
            }
            mo18681t(cls);
        }
        return cls;
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public void mo18681t(Object obj) {
        this.agE.add(obj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C2437a mo18653d(int i, String str) {
        switch (i) {
            case 134:
            case 145:
            case 146:
            case 147:
            case 170:
            case 240:
                return new C2438b(i, str);
            default:
                return new C2437a(i, str);
        }
    }

    /* renamed from: Eh */
    private String m31039Eh() {
        return cDN();
    }

    /* renamed from: Ei */
    private Object m31040Ei() {
        if (this.agC.readBits(1) == 0) {
            this.agD = false;
            return null;
        } else if (this.agC.readBits(1) == 0) {
            this.agD = false;
            return m31044wh(this.agC.readInt());
        } else {
            this.agD = true;
            return null;
        }
    }

    private Object cDP() {
        if (this.agC.readBits(1) == 0) {
            this.agD = false;
            return m31044wh(this.agC.readInt());
        }
        this.agD = true;
        return null;
    }

    /* renamed from: wh */
    private Object m31044wh(int i) {
        Object obj = this.agE.get(i);
        if (!(obj instanceof C2437a)) {
            return obj;
        }
        C2437a aVar = (C2437a) obj;
        try {
            switch (aVar.type) {
                case 145:
                    if (!((C2438b) aVar).gif) {
                        aVar.f7364Op = mo18647b((C2438b) aVar, C2439c.FINISH_READ);
                        ((C2438b) aVar).gif = true;
                        break;
                    }
                    break;
                case 146:
                    if (!((C2438b) aVar).gif) {
                        aVar.f7364Op = mo18649c((C2438b) aVar, C2439c.FINISH_READ);
                        ((C2438b) aVar).gif = true;
                        break;
                    }
                    break;
                case 147:
                    if (!((C2438b) aVar).gif) {
                        aVar.f7364Op = mo18654d((C2438b) aVar, C2439c.FINISH_READ);
                        ((C2438b) aVar).gif = true;
                        break;
                    }
                    break;
                case 240:
                    if (!((C2438b) aVar).gif) {
                        C2437a a = mo10814a((C2438b) aVar, C2439c.FINISH_READ);
                        ((C2438b) a).gif = true;
                        aVar = a;
                        break;
                    }
                    break;
            }
            return aVar.f7364Op;
        } catch (Exception e) {
            throw new CountFailedReadOrWriteException((Throwable) e);
        }
    }

    /* renamed from: pd */
    public boolean mo18658pd() {
        return this.f7359Jp;
    }

    /* renamed from: E */
    public void mo18641E(boolean z) {
        this.f7359Jp = z;
    }

    public void close() {
        this.agC.close();
    }

    /* renamed from: kK */
    private Class<?> m31043kK(String str) {
        if (str.equals("int")) {
            return Integer.TYPE;
        }
        if (str.equals("long")) {
            return Long.TYPE;
        }
        if (str.equals("byte")) {
            return Byte.TYPE;
        }
        if (str.equals("boolean")) {
            return Boolean.TYPE;
        }
        if (str.equals("char")) {
            return Character.TYPE;
        }
        if (str.equals("float")) {
            return Float.TYPE;
        }
        if (str.equals("double")) {
            return Double.TYPE;
        }
        if (str.equals("short")) {
            return Short.TYPE;
        }
        return null;
    }

    private boolean isPrimitive(Class<?> cls) {
        if (cls.equals(Integer.TYPE) || cls.equals(Long.TYPE) || cls.equals(Byte.TYPE) || cls.equals(Boolean.TYPE) || cls.equals(Character.TYPE) || cls.equals(Float.TYPE) || cls.equals(Double.TYPE) || cls.equals(Short.TYPE)) {
            return true;
        }
        return false;
    }

    public int available() {
        return 1;
    }

    public int read() {
        return readByte((String) null);
    }

    public int read(byte[] bArr) {
        mo6352c((String) null, bArr);
        return bArr.length;
    }

    public int read(byte[] bArr, int i, int i2) {
        mo6353c((String) null, bArr, i, i2);
        return i2;
    }

    /* access modifiers changed from: protected */
    public Object readObjectOverride() {
        return mo6366hd((String) null);
    }

    /* access modifiers changed from: protected */
    public void readStreamHeader() {
    }

    public ObjectInputStream.GetField readFields() {
        throw new CreatListenerChannelException();
    }

    public long skip(long j) {
        return 0;
    }

    public boolean readBoolean() {
        return mo6354gR((String) null);
    }

    public byte readByte() {
        return readByte((String) null);
    }

    public char readChar() {
        return readChar((String) null);
    }

    public double readDouble() {
        return readDouble((String) null);
    }

    public float readFloat() {
        return readFloat((String) null);
    }

    public void readFully(byte[] bArr) {
        mo6352c((String) null, bArr);
    }

    public void readFully(byte[] bArr, int i, int i2) {
        mo6353c((String) null, bArr, i, i2);
    }

    public int readInt() {
        return readInt((String) null);
    }

    public String readLine() {
        return null;
    }

    public long readLong() {
        return readLong((String) null);
    }

    public short readShort() {
        return mo6357gU((String) null);
    }

    public int readUnsignedByte() {
        return mo6356gT((String) null);
    }

    public int readUnsignedShort() {
        return mo6358gV((String) null);
    }

    public int skipBytes(int i) {
        return 0;
    }

    public String readUTF() {
        return mo6365hc((String) null);
    }

    /* renamed from: a.fS$c */
    /* compiled from: a */
    public enum C2439c {
        READ_FULLY,
        READ_PARTIAL,
        FINISH_READ
    }

    /* renamed from: a.fS$b */
    /* compiled from: a */
    public static class C2438b extends C2437a {
        public int gie;
        public boolean gif;
        Class<?> clazz;
        boolean done;
        C2438b gic;
        C2437a gid;

        public C2438b(int i, String str) {
            super(i, str);
        }

        public String toString() {
            if (this.key != null) {
                return String.valueOf(ayF.names[this.type]) + " " + this.key + " " + getObject();
            }
            return String.valueOf(ayF.names[this.type]) + " " + getObject();
        }

        public Object cnu() {
            return this.f7364Op;
        }
    }

    /* renamed from: a.fS$a */
    public static class C2437a {

        /* renamed from: Os */
        public int f7367Os;
        /* renamed from: Oo */
        C2437a f7363Oo;
        /* renamed from: Op */
        Object f7364Op;
        /* renamed from: Oq */
        long f7365Oq;
        /* renamed from: Or */
        double f7366Or;
        String key;
        int type;

        public C2437a(int i, String str) {
            this.type = i;
            this.key = str;
        }

        /* renamed from: w */
        public void mo18687w(Object obj) {
            this.f7364Op = obj;
        }

        public void setType(int i) {
            this.type = i;
        }

        public Object getObject() {
            if (this.f7364Op == null) {
                switch (this.type) {
                    case 0:
                        Boolean valueOf = Boolean.valueOf(this.f7365Oq != 0);
                        this.f7364Op = valueOf;
                        return valueOf;
                    case 1:
                        Byte valueOf2 = Byte.valueOf((byte) ((int) this.f7365Oq));
                        this.f7364Op = valueOf2;
                        return valueOf2;
                    case 2:
                        Short valueOf3 = Short.valueOf((short) ((int) this.f7365Oq));
                        this.f7364Op = valueOf3;
                        return valueOf3;
                    case 3:
                        Character valueOf4 = Character.valueOf((char) ((int) this.f7365Oq));
                        this.f7364Op = valueOf4;
                        return valueOf4;
                    case 4:
                        Integer valueOf5 = Integer.valueOf((int) this.f7365Oq);
                        this.f7364Op = valueOf5;
                        return valueOf5;
                    case 5:
                        Long valueOf6 = Long.valueOf(this.f7365Oq);
                        this.f7364Op = valueOf6;
                        return valueOf6;
                    case 6:
                        Float valueOf7 = Float.valueOf((float) this.f7366Or);
                        this.f7364Op = valueOf7;
                        return valueOf7;
                    case 128:
                        Double valueOf8 = Double.valueOf(this.f7366Or);
                        this.f7364Op = valueOf8;
                        return valueOf8;
                    case 129:
                    case 131:
                        return null;
                }
            }
            return this.f7364Op;
        }

        public String toString() {
            if (this.key != null) {
                return String.valueOf(ayF.names[this.type]) + " " + this.key + " " + getObject();
            }
            return String.valueOf(ayF.names[this.type]) + " " + getObject();
        }
    }
}
