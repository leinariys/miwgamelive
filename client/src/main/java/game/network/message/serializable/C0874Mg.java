package game.network.message.serializable;

import game.script.Character;
import p001a.CountFailedReadOrWriteException;
import sun.misc.Unsafe;

import java.io.*;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.Mg */
/* compiled from: a */
public class C0874Mg implements Serializable {
    public static final ObjectStreamField[] NO_FIELDS = new ObjectStreamField[0];
    private static final long serialVersionUID = -5784337950608855326L;
    private static ConcurrentHashMap<Class<?>, C0874Mg> dzv = new ConcurrentHashMap<>();
    private final Class<?> clazz;
    private final Class<?> dze;
    private final boolean dzk;
    private final boolean dzl;
    private final boolean dzm;
    private Constructor constructor;
    private ObjectStreamField[] dzf;
    private int dzg;
    private int dzh;
    private int dzi;
    private C0875a dzj;
    private char dzn;
    private Method dzo;
    private Method dzp;
    private Method dzq;
    private Method dzr;
    private ObjectStreamField[] dzs;
    private boolean dzt;
    private C0874Mg dzu;

    private C0874Mg(Class<?> cls) {
        this.clazz = cls;
        this.dze = cls.getComponentType();
        this.dzl = cls.isArray();
        this.dzk = cls.isPrimitive();
        this.dzm = Externalizable.class.isAssignableFrom(cls);
        if (this.dzm) {
            this.constructor = m7121E(cls);
        }
        if (this.dzk) {
            if (cls == Integer.TYPE) {
                this.dzn = 'I';
            } else if (cls == Long.TYPE) {
                this.dzn = 'J';
            } else if (cls == Short.TYPE) {
                this.dzn = 'S';
            } else if (cls == Character.TYPE) {
                this.dzn = 'C';
            } else if (cls == Byte.TYPE) {
                this.dzn = 'B';
            } else if (cls == Boolean.TYPE) {
                this.dzn = 'Z';
            } else if (cls == Double.TYPE) {
                this.dzn = 'D';
            } else if (cls == Float.TYPE) {
                this.dzn = 'F';
            }
        } else if (isArray()) {
            this.dzn = '[';
        } else {
            this.dzn = 'L';
        }
        try {
            bgM();
            this.dzo = m7124a(cls, "writeObject", Void.TYPE, ObjectOutputStream.class);
            this.dzp = m7124a(cls, "readObject", Void.TYPE, ObjectInputStream.class);
            this.dzj = new C0875a(this.dzf, this.dzs);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    private static Method m7124a(Class cls, String str, Class cls2, Class... clsArr) {
        try {
            Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
            declaredMethod.setAccessible(true);
            int modifiers = declaredMethod.getModifiers();
            if (declaredMethod.getReturnType() == cls2 && (modifiers & 8) == 0 && (modifiers & 2) != 0) {
                return declaredMethod;
            }
            return null;
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    /* renamed from: F */
    private static ObjectStreamField[] m7122F(Class<?> cls) {
        ObjectStreamField[] objectStreamFieldArr;
        int i = 0;
        Class<ObjectStreamField> cls2 = ObjectStreamField.class;
        try {
            Constructor<ObjectStreamField> declaredConstructor = cls2.getDeclaredConstructor(new Class[]{Field.class, Boolean.TYPE, Boolean.TYPE});
            declaredConstructor.setAccessible(true);
            if (!Serializable.class.isAssignableFrom(cls) || Externalizable.class.isAssignableFrom(cls) || Proxy.isProxyClass(cls) || cls.isInterface()) {
                objectStreamFieldArr = NO_FIELDS;
            } else {
                Field[] declaredFields = cls.getDeclaredFields();
                ArrayList arrayList = new ArrayList();
                while (true) {
                    int i2 = i;
                    if (i2 >= declaredFields.length) {
                        break;
                    }
                    if ((declaredFields[i2].getModifiers() & 136) == 0) {
                        try {
                            arrayList.add(declaredConstructor.newInstance(new Object[]{declaredFields[i2], false, true}));
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    }
                    i = i2 + 1;
                }
                int size = arrayList.size();
                if (size == 0) {
                    objectStreamFieldArr = NO_FIELDS;
                } else {
                    objectStreamFieldArr = (ObjectStreamField[]) arrayList.toArray(new ObjectStreamField[size]);
                }
            }
            Arrays.sort(objectStreamFieldArr);
            return objectStreamFieldArr;
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }

    /* renamed from: G */
    public static C0874Mg m7123G(Class<?> cls) {
        C0874Mg mg = dzv.get(cls);
        if (mg != null) {
            return mg;
        }
        C0874Mg mg2 = new C0874Mg(cls);
        dzv.put(cls, mg2);
        return mg2;
    }

    /* renamed from: E */
    private Constructor m7121E(Class cls) {
        try {
            Constructor declaredConstructor = cls.getDeclaredConstructor((Class[]) null);
            declaredConstructor.setAccessible(true);
            if ((declaredConstructor.getModifiers() & 1) != 0) {
                return declaredConstructor;
            }
            return null;
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    private void bgM() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        int i;
        this.dzf = m7122F(this.clazz);
        if (this.dzf != null && this.dzf.length != 0) {
            Method declaredMethod = this.dzf[0].getClass().getDeclaredMethod("setOffset", new Class[]{Integer.TYPE});
            declaredMethod.setAccessible(true);
            for (ObjectStreamField objectStreamField : this.dzf) {
                switch (objectStreamField.getTypeCode()) {
                    case 'B':
                    case 'Z':
                        int i2 = this.dzg;
                        this.dzg = i2 + 1;
                        declaredMethod.invoke(objectStreamField, new Object[]{Integer.valueOf(i2)});
                        this.dzh++;
                        break;
                    case 'C':
                    case 'S':
                        declaredMethod.invoke(objectStreamField, new Object[]{Integer.valueOf(this.dzg)});
                        this.dzg += 2;
                        this.dzh++;
                        break;
                    case 'D':
                    case 'J':
                        declaredMethod.invoke(objectStreamField, new Object[]{Integer.valueOf(this.dzg)});
                        this.dzg += 8;
                        this.dzh++;
                        break;
                    case 'F':
                    case 'I':
                        declaredMethod.invoke(objectStreamField, new Object[]{Integer.valueOf(this.dzg)});
                        this.dzg += 4;
                        this.dzh++;
                        break;
                    case 'L':
                    case '[':
                        int i3 = this.dzi;
                        this.dzi = i3 + 1;
                        declaredMethod.invoke(objectStreamField, new Object[]{Integer.valueOf(i3)});
                        break;
                    default:
                        throw new InternalError();
                }
            }
            this.dzs = new ObjectStreamField[this.dzi];
            ObjectStreamField[] objectStreamFieldArr = this.dzf;
            int length = objectStreamFieldArr.length;
            int i4 = 0;
            int i5 = 0;
            while (i4 < length) {
                ObjectStreamField objectStreamField2 = objectStreamFieldArr[i4];
                switch (objectStreamField2.getTypeCode()) {
                    case 'L':
                    case '[':
                        i = i5 + 1;
                        this.dzs[i5] = objectStreamField2;
                        break;
                    default:
                        i = i5;
                        break;
                }
                i4++;
                i5 = i;
            }
        }
    }

    public int bgN() {
        return this.dzg;
    }

    /* renamed from: a */
    public void mo4016a(Object obj, C2483fh fhVar) {
        this.dzj.mo4037a(obj, fhVar);
    }

    /* renamed from: a */
    public void mo4015a(Object obj, C2436fS_1 fSVar) {
        this.dzj.mo4036a(obj, fSVar);
    }

    public int bgO() {
        return this.dzi;
    }

    /* renamed from: a */
    public void mo4019a(Object obj, Object[] objArr, int i) {
        this.dzj.mo4038a(obj, objArr, i);
    }

    /* renamed from: b */
    public void mo4020b(Object obj, Object[] objArr, int i) {
        this.dzj.mo4040b(obj, objArr, i);
    }

    /* renamed from: lH */
    public String mo4032lH(int i) {
        return this.dzf[this.dzh + i].getName();
    }

    /* renamed from: lI */
    public Class<?> mo4033lI(int i) {
        return this.dzf[this.dzh + i].getType();
    }

    /* renamed from: lJ */
    public String mo4034lJ(int i) {
        return this.dzf[this.dzh + i].getTypeString();
    }

    public Object bgP() {
        if (this.constructor == null) {
            return this.dzj.allocateInstance(this.clazz);
        }
        try {
            return this.constructor.newInstance(new Object[0]);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e2) {
            throw new RuntimeException(e2);
        } catch (InvocationTargetException e3) {
            throw new RuntimeException(e3);
        }
    }

    public Class<?> getComponentType() {
        return this.dze;
    }

    public boolean isExternalizable() {
        return this.dzm;
    }

    public Class<?> bgQ() {
        return this.dze;
    }

    public boolean isPrimitive() {
        return this.dzk;
    }

    public boolean isArray() {
        return this.dzl;
    }

    public boolean bgR() {
        return this.dzo != null;
    }

    public boolean bgS() {
        return this.dzp != null;
    }

    /* renamed from: a */
    public void mo4018a(Object obj, ObjectOutputStream objectOutputStream) {
        if (this.dzo == null) {
            throw new UnsupportedOperationException();
        }
        try {
            this.dzo.invoke(obj, new Object[]{objectOutputStream});
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof IOException) {
                throw ((IOException) targetException);
            }
            throw new CountFailedReadOrWriteException(targetException);
        } catch (IllegalAccessException e2) {
            throw new CountFailedReadOrWriteException((Throwable) e2);
        }
    }

    /* renamed from: a */
    public void mo4017a(Object obj, ObjectInputStream objectInputStream) {
        if (this.dzp == null) {
            throw new UnsupportedOperationException();
        }
        try {
            this.dzp.invoke(obj, new Object[]{objectInputStream});
        } catch (InvocationTargetException e) {
            Throwable targetException = e.getTargetException();
            if (targetException instanceof IOException) {
                throw ((IOException) targetException);
            }
            throw new CountFailedReadOrWriteException(targetException);
        } catch (IllegalAccessException e2) {
            throw new CountFailedReadOrWriteException((Throwable) e2);
        }
    }

    public C0874Mg bgT() {
        Class<? super Object> superclass;
        if (this.dzt) {
            return this.dzu;
        }
        this.dzt = true;
        if (this.clazz == Object.class || (superclass = this.clazz.getSuperclass()) == Object.class) {
            return null;
        }
        C0874Mg G = m7123G(superclass);
        this.dzu = G;
        return G;
    }

    /* renamed from: lK */
    public ObjectStreamField mo4035lK(int i) {
        return this.dzs[i];
    }

    /* renamed from: a.Mg$a */
    private static class C0875a {
        private static Unsafe unsafe;
        private final ObjectStreamField[] dzf;
        private final int dzh;
        private final ObjectStreamField[] dzs;
        private final long[] fWS;
        private final long[] fWT;
        private final char[] fWU;
        private final int[] offsets;
        private final Class<?>[] types;

        C0875a(ObjectStreamField[] objectStreamFieldArr, ObjectStreamField[] objectStreamFieldArr2) {
            long j;
            long j2;
            Class<?> cls;
            this.dzf = objectStreamFieldArr;
            this.dzs = objectStreamFieldArr2;
            int length = objectStreamFieldArr.length;
            this.fWS = new long[length];
            this.offsets = new int[length];
            this.fWU = new char[length];
            ArrayList arrayList = new ArrayList();
            try {
                Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
                declaredField.setAccessible(true);
                unsafe = (Unsafe) declaredField.get((Object) null);
                int i = 0;
                while (i < length) {
                    ObjectStreamField objectStreamField = objectStreamFieldArr[i];
                    try {
                        Method declaredMethod = ObjectStreamField.class.getDeclaredMethod("getField", new Class[0]);
                        declaredMethod.setAccessible(true);
                        try {
                            Field field = (Field) declaredMethod.invoke(objectStreamField, new Object[0]);
                            long[] jArr = this.fWS;
                            if (field != null) {
                                j2 = unsafe.objectFieldOffset(field);
                            } else {
                                j2 = -1;
                            }
                            jArr[i] = j2;
                            this.offsets[i] = objectStreamField.getOffset();
                            this.fWU[i] = objectStreamField.getTypeCode();
                            if (!objectStreamField.isPrimitive()) {
                                if (field != null) {
                                    cls = field.getType();
                                } else {
                                    cls = null;
                                }
                                arrayList.add(cls);
                            }
                            i++;
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    } catch (Exception e2) {
                        throw new RuntimeException(e2);
                    }
                }
                if (objectStreamFieldArr2 != null) {
                    this.fWT = new long[objectStreamFieldArr2.length];
                    int i2 = 0;
                    while (i2 < objectStreamFieldArr2.length) {
                        ObjectStreamField objectStreamField2 = objectStreamFieldArr2[i2];
                        try {
                            Method declaredMethod2 = ObjectStreamField.class.getDeclaredMethod("getField", new Class[0]);
                            declaredMethod2.setAccessible(true);
                            try {
                                Field field2 = (Field) declaredMethod2.invoke(objectStreamField2, new Object[0]);
                                long[] jArr2 = this.fWT;
                                if (field2 != null) {
                                    j = unsafe.objectFieldOffset(field2);
                                } else {
                                    j = -1;
                                }
                                jArr2[i2] = j;
                                i2++;
                            } catch (Exception e3) {
                                throw new RuntimeException(e3);
                            }
                        } catch (Exception e4) {
                            throw new RuntimeException(e4);
                        }
                    }
                } else {
                    this.fWT = null;
                }
                this.types = (Class[]) arrayList.toArray(new Class[arrayList.size()]);
                this.dzh = length - this.types.length;
            } catch (Exception e5) {
                throw new RuntimeException(e5);
            }
        }

        /* access modifiers changed from: package-private */
        public Object allocateInstance(Class<?> cls) {
            return unsafe.allocateInstance(cls);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo4037a(Object obj, C2483fh fhVar) {
            if (obj == null) {
                throw new NullPointerException();
            }
            for (int i = 0; i < this.dzh; i++) {
                long j = this.fWS[i];
                switch (this.fWU[i]) {
                    case 'B':
                        fhVar.writeByte(this.dzf[i].getName(), unsafe.getByte(obj, j));
                        break;
                    case 'C':
                        fhVar.writeChar(this.dzf[i].getName(), unsafe.getChar(obj, j));
                        break;
                    case 'D':
                        fhVar.writeDouble(this.dzf[i].getName(), unsafe.getDouble(obj, j));
                        break;
                    case 'F':
                        fhVar.writeFloat(this.dzf[i].getName(), unsafe.getFloat(obj, j));
                        break;
                    case 'I':
                        fhVar.writeInt(this.dzf[i].getName(), unsafe.getInt(obj, j));
                        break;
                    case 'J':
                        fhVar.writeLong(this.dzf[i].getName(), unsafe.getLong(obj, j));
                        break;
                    case 'S':
                        fhVar.writeShort(this.dzf[i].getName(), (int) unsafe.getShort(obj, j));
                        break;
                    case 'Z':
                        fhVar.writeBoolean(this.dzf[i].getName(), unsafe.getBoolean(obj, j));
                        break;
                    default:
                        throw new InternalError();
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo4036a(Object obj, C2436fS_1 fSVar) throws IOException {
            if (obj == null) {
                throw new NullPointerException();
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.dzh) {
                    long j = this.fWS[i2];
                    if (j != -1) {
                        switch (this.fWU[i2]) {
                            case 'B':
                                unsafe.putByte(obj, j, fSVar.readByte(this.dzf[i2].getName()));
                                break;
                            case 'C':
                                unsafe.putChar(obj, j, fSVar.readChar(this.dzf[i2].getName()));
                                break;
                            case 'D':
                                unsafe.putDouble(obj, j, fSVar.readDouble(this.dzf[i2].getName()));
                                break;
                            case 'F':
                                unsafe.putFloat(obj, j, fSVar.readFloat(this.dzf[i2].getName()));
                                break;
                            case 'I':
                                unsafe.putInt(obj, j, fSVar.readInt(this.dzf[i2].getName()));
                                break;
                            case 'J':
                                unsafe.putLong(obj, j, fSVar.readLong(this.dzf[i2].getName()));
                                break;
                            case 'S':
                                unsafe.putShort(obj, j, fSVar.mo6357gU(this.dzf[i2].getName()));
                                break;
                            case 'Z':
                                unsafe.putBoolean(obj, j, fSVar.mo6354gR(this.dzf[i2].getName()));
                                break;
                            default:
                                throw new InternalError();
                        }
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo4038a(Object obj, Object[] objArr, int i) {
            if (obj == null || this.dzs == null) {
                throw new NullPointerException();
            }
            int i2 = 0;
            while (i2 < this.dzs.length) {
                objArr[i] = unsafe.getObject(obj, this.fWT[i2]);
                i2++;
                i++;
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo4040b(Object obj, Object[] objArr, int i) {
            if (obj == null) {
                throw new NullPointerException();
            }
            int i2 = 0;
            while (i2 < this.dzs.length) {
                long j = this.fWT[i2];
                if (j != -1) {
                    unsafe.putObject(obj, j, objArr[i]);
                }
                i2++;
                i++;
            }
        }
    }
}
