package game.network.message.serializable;

import java.io.Serializable;

/* renamed from: a.mj */
/* compiled from: a */
public class C2993mj implements Serializable {
    public static final C2993mj aCc = new C2993mj(0, 0, 0);

    public final int aCd;
    public final int major;
    public final int minor;

    public C2993mj(int i, int i2, int i3) {
        this.major = i;
        this.minor = i2;
        this.aCd = i3;
    }

    public C2993mj(String str) {
        int i;
        int i2;
        int i3 = 0;
        if ("".equals(str)) {
            this.major = 0;
            this.minor = 0;
            this.aCd = 0;
            return;
        }
        String[] split = str.split("[.]");
        if (split.length > 0) {
            i = Integer.parseInt(split[0]);
        } else {
            i = 0;
        }
        this.major = i;
        if (split.length > 1) {
            i2 = Integer.parseInt(split[1]);
        } else {
            i2 = 0;
        }
        this.minor = i2;
        this.aCd = split.length > 2 ? Integer.parseInt(split[2]) : i3;
    }

    /* renamed from: a */
    public int mo20589a(int i, int i2, int i3) {
        int i4 = this.major - i;
        if (i4 != 0) {
            return i4;
        }
        int i5 = this.minor - i2;
        if (i5 == 0) {
            return this.aCd - i3;
        }
        return i5;
    }
}
