package game.network.message.serializable;

import game.script.ship.Station;
import p001a.C6927awP;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: a.jo */
/* compiled from: a */
public class C2774jo extends C5553aNt implements C6927awP, Serializable {

    C0570Ht ajw = new C0570Ht();
    Station ajx;
    Station ajy;

    /* renamed from: Fl */
    public Station mo3130Fl() {
        return this.ajx;
    }

    /* renamed from: Fm */
    public Station mo3131Fm() {
        return this.ajy;
    }

    /* renamed from: e */
    public void mo3137e(Station bf) {
        this.ajx = bf;
    }

    /* renamed from: f */
    public void mo3138f(Station bf) {
        this.ajy = bf;
    }

    /* renamed from: Fn */
    public C0570Ht mo3132Fo() {
        return this.ajw;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeObject(this.ajx);
        objectOutputStream.writeObject(this.ajy);
        objectOutputStream.writeObject(this.ajw);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.ajx = (Station) objectInputStream.readObject();
        this.ajy = (Station) objectInputStream.readObject();
        this.ajw = (C0570Ht) objectInputStream.readObject();
    }
}
