package game.network.message.serializable;

import java.io.Serializable;

/* renamed from: a.ajn  reason: case insensitive filesystem */
/* compiled from: a */
public class C6275ajn implements Serializable {


    /* renamed from: cy */
    private int f4718cy;

    /* renamed from: db */
    private long f4719db;

    public C6275ajn(long j, int i) {
        this.f4719db = j;
        this.f4718cy = i;
    }

    public String toString() {
        return "[ " + this.f4719db + " / " + this.f4718cy + " ]";
    }

    /* renamed from: rZ */
    public long mo14177rZ() {
        return this.f4719db;
    }

    public int getAmount() {
        return this.f4718cy;
    }
}
