package game.network.message.serializable;

import java.io.Serializable;

/* renamed from: a.aTA */
/* compiled from: a */
public class ReceivedTime implements Serializable {

    private transient long receivedTimeMilis;

    public final long receivedTimeMilis() {
        return this.receivedTimeMilis;
    }

    public final void setReceivedTimeMilis(long j) {
        this.receivedTimeMilis = j;
    }
}
