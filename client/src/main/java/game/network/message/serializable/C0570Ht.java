package game.network.message.serializable;

import game.network.exception.CreatListenerChannelException;
import game.script.item.Item;
import game.script.item.ItemType;
import logic.baa.C5473aKr;
import logic.res.code.C5663aRz;
import p001a.C1493Vz;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/* renamed from: a.Ht */
/* compiled from: a */
public class C0570Ht implements C1493Vz, Serializable {

    public int dby = 0;
    private Set<Item> dbz = new HashSet();

    /* renamed from: s */
    public Item mo2693s(Item auq) {
        this.dby++;
        this.dbz.add(auq);
        return auq;
    }

    public Set<Item> aRz() {
        return this.dbz;
    }

    public void removeAll() {
        this.dby++;
        this.dbz.clear();
    }

    /* renamed from: d */
    public boolean mo2690d(Item auq) {
        this.dby++;
        return this.dbz.remove(auq);
    }

    public int aRA() {
        return this.dby;
    }

    /* renamed from: k */
    public void mo2691k(Collection<? extends Item> collection) {
        throw new CreatListenerChannelException();
    }

    /* renamed from: t */
    public boolean mo2694t(Item auq) {
        return false;
    }

    /* renamed from: wE */
    public float mo2696wE() {
        return 0.0f;
    }

    public float aRB() {
        return 0.0f;
    }

    /* renamed from: a */
    public void mo2685a(C5663aRz arz, C5473aKr<?, ?> akr) {
    }

    /* renamed from: b */
    public void mo2689b(C5663aRz arz, C5473aKr<?, ?> akr) {
    }

    /* renamed from: u */
    public Item mo2695u(ItemType jCVar) {
        return null;
    }
}
