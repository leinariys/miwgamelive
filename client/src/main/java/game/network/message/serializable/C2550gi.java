package game.network.message.serializable;

import p001a.C5404aIa;
import p001a.C6030afC;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.*;

/* renamed from: a.gi */
/* compiled from: a */
public class C2550gi implements Serializable {

    /* renamed from: Pz */
    private static final String f7731Pz = "The Nestable implementation passed to the NestableDelegate(Nestable) constructor must extend java.lang.Throwable";
    private static final long serialVersionUID = 6482132960111293828L;
    /* renamed from: PB */
    private static boolean f7729PB = true;
    /* renamed from: PC */
    private static boolean f7730PC = true;
    /* renamed from: PA */
    private Throwable f7732PA = null;

    public C2550gi(C5404aIa aia) {
        if (aia instanceof Throwable) {
            this.f7732PA = (Throwable) aia;
            return;
        }
        throw new IllegalArgumentException(f7731Pz);
    }

    /* renamed from: bg */
    public String mo19061bg(int i) {
        Throwable bh = mo19062bh(i);
        if (C5404aIa.class.isInstance(bh)) {
            return ((C5404aIa) bh).mo6733bg(0);
        }
        return bh.getMessage();
    }

    public String getMessage(String str) {
        String message;
        StringBuffer stringBuffer = new StringBuffer();
        if (str != null) {
            stringBuffer.append(str);
        }
        Throwable cause = C6030afC.getCause(this.f7732PA);
        if (!(cause == null || (message = cause.getMessage()) == null)) {
            if (str != null) {
                stringBuffer.append(": ");
            }
            stringBuffer.append(message);
        }
        if (stringBuffer.length() > 0) {
            return stringBuffer.toString();
        }
        return null;
    }

    public String[] getMessages() {
        String message;
        Throwable[] vo = mo19070vo();
        String[] strArr = new String[vo.length];
        for (int i = 0; i < vo.length; i++) {
            if (C5404aIa.class.isInstance(vo[i])) {
                message = ((C5404aIa) vo[i]).mo6733bg(0);
            } else {
                message = vo[i].getMessage();
            }
            strArr[i] = message;
        }
        return strArr;
    }

    /* renamed from: bh */
    public Throwable mo19062bh(int i) {
        if (i == 0) {
            return this.f7732PA;
        }
        return mo19070vo()[i];
    }

    /* renamed from: vn */
    public int mo19069vn() {
        return C6030afC.m21477g(this.f7732PA);
    }

    /* renamed from: vo */
    public Throwable[] mo19070vo() {
        return C6030afC.m21478h(this.f7732PA);
    }

    /* renamed from: a */
    public int mo19059a(Class cls, int i) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("The start index was out of bounds: " + i);
        }
        Throwable[] h = C6030afC.m21478h(this.f7732PA);
        if (i >= h.length) {
            throw new IndexOutOfBoundsException("The start index was out of bounds: " + i + " >= " + h.length);
        }
        while (i < h.length) {
            if (h[i].getClass().equals(cls)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public void printStackTrace(PrintStream printStream) {
        synchronized (printStream) {
            PrintWriter printWriter = new PrintWriter(printStream, false);
            printStackTrace(printWriter);
            printWriter.flush();
        }
    }

    public void printStackTrace(PrintWriter printWriter) {
        Throwable th = this.f7732PA;
        if (!C6030afC.bVB()) {
            ArrayList arrayList = new ArrayList();
            while (th != null) {
                arrayList.add(mo19060b(th));
                th = C6030afC.getCause(th);
            }
            String str = "Caused by: ";
            if (!f7729PB) {
                str = "Rethrown as: ";
                Collections.reverse(arrayList);
            }
            String str2 = str;
            if (f7730PC) {
                mo19063d(arrayList);
            }
            synchronized (printWriter) {
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    for (String println : (String[]) it.next()) {
                        printWriter.println(println);
                    }
                    if (it.hasNext()) {
                        printWriter.print(str2);
                    }
                }
            }
        } else if (th instanceof C5404aIa) {
            ((C5404aIa) th).mo6735c(printWriter);
        } else {
            th.printStackTrace(printWriter);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public String[] mo19060b(Throwable th) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter, true);
        if (th instanceof C5404aIa) {
            ((C5404aIa) th).mo6735c(printWriter);
        } else {
            th.printStackTrace(printWriter);
        }
        return C6030afC.m21480iF(stringWriter.getBuffer().toString());
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo19063d(List<String[]> list) {
        for (int size = list.size() - 1; size > 0; size--) {
            String[] strArr = list.get(size);
            ArrayList arrayList = new ArrayList(Arrays.asList(strArr));
            C6030afC.m21474d(arrayList, new ArrayList(Arrays.asList(list.get(size - 1))));
            int length = strArr.length - arrayList.size();
            if (length > 0) {
                arrayList.add("\t... " + length + " more");
                list.set(size, (String[]) arrayList.toArray(new String[arrayList.size()]));
            }
        }
    }
}
