package game.network.message.serializable;

import game.network.message.externalizable.C0722KO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* renamed from: a.Yw */
/* compiled from: a */
public final class C1695Yw implements Serializable {

    private int dby = 0;
    private List<C0010AD> eMg;
    private List<C0010AD> eMh;
    private List<C0722KO> eMi;
    private C5613aQb eMj;

    public C5613aQb bHC() {
        if (this.eMj == null) {
            this.eMj = new C5613aQb();
        }
        return this.eMj;
    }

    /* renamed from: a */
    public void mo7300a(C5613aQb aqb) {
        this.eMj = aqb;
    }

    public List<C0010AD> bHD() {
        if (this.eMg == null) {
            this.eMg = new LinkedList();
        }
        return this.eMg;
    }

    public int bHE() {
        if (this.eMg == null) {
            return 0;
        }
        return this.eMg.size();
    }

    public int bHF() {
        return this.dby;
    }

    public List<C0722KO> bHG() {
        if (this.eMi == null) {
            this.eMi = new ArrayList();
        }
        return this.eMi;
    }

    /* renamed from: a */
    public void mo7299a(C0722KO ko) {
        bHG().add(ko);
        this.dby++;
    }

    /* renamed from: a */
    public void mo7298a(C0010AD ad) {
        bHD().add(ad);
    }

    public boolean bHH() {
        return this.eMg != null;
    }

    public boolean bHI() {
        return this.eMi != null;
    }

    /* renamed from: v */
    public void mo7310v(List<C0010AD> list) {
        this.eMg = list;
    }

    /* renamed from: w */
    public void mo7311w(List<C0722KO> list) {
        this.eMi = list;
    }

    public boolean bHJ() {
        return this.eMh != null;
    }

    public List<C0010AD> bHK() {
        if (this.eMh == null) {
            this.eMh = new ArrayList();
        }
        return this.eMh;
    }

    /* renamed from: x */
    public void mo7312x(List<C0010AD> list) {
        this.eMh = list;
    }
}
