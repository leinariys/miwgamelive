package game.network.message.serializable;

import p001a.C1647YG;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: a.aNt  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5553aNt implements C1647YG, Serializable {
    private long ccc;
    private long ccf;
    private C6158aha isP = new C6158aha();

    public long aAA() {
        return this.ccc;
    }

    public long aAE() {
        return this.ccf;
    }

    /* renamed from: eg */
    public void mo704eg(long j) {
        this.ccc = j;
    }

    public void setTimeToLive(long j) {
        this.ccf = j;
    }

    /* renamed from: b */
    public void mo701b(C5857abl abl) {
        this.isP = (C6158aha) abl;
    }

    public C5857abl aAT() {
        return this.isP;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeLong(this.ccc);
        objectOutputStream.writeLong(this.ccf);
        objectOutputStream.writeObject(this.isP);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.ccc = objectInputStream.readLong();
        this.ccf = objectInputStream.readLong();
        this.isP = (C6158aha) objectInputStream.readObject();
    }
}
