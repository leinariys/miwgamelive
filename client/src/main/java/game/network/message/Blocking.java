package game.network.message;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.C6302akO;
import game.network.message.serializable.ReceivedTime;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.kU */
/* compiled from: a */
public abstract class Blocking extends ReceivedTime {
    private boolean blocking;

    public boolean isBlocking() {
        return this.blocking;
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }

    public void readExternal(ObjectInput objectInput) throws IOException {
        this.blocking = objectInput.readBoolean();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeBoolean(this.blocking);
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        return null;
    }
}
