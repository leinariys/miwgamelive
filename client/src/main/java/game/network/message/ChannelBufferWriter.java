package game.network.message;

import java.io.IOException;
import java.nio.channels.Channel;

/* renamed from: a.aBC */
/* compiled from: a */
public abstract class ChannelBufferWriter {
    /* renamed from: a */
    public abstract boolean writeMessageToBuffer(ByteMessageCompres messageCompres);

    /* renamed from: a */
    public abstract boolean writeBufferToChannel(Channel channel) throws IOException;

    /* renamed from: gE */
    public abstract boolean isBufferReadyToBeRead();

    /* renamed from: gF */
    public abstract void prepareForReading();

    /* renamed from: gG */
    public abstract int getMetricWriteByte();

    /* renamed from: gH */
    public abstract int getCountWriteMessage();

    /* renamed from: gI */
    public abstract int getAllOriginalByteLength();

    /* renamed from: gJ */
    public abstract int getMetricМessagesEmpty();

    /* renamed from: gK */
    public abstract int getMetricMessageFilled();
}
