package game.network.message;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.manager.aHN;
import logic.baa.C1616Xf;
import logic.res.html.DataClassSerializer;
import p001a.C1507WB;
import p001a.C5927adD;
import p001a.C6744aso;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.GZ */
/* compiled from: a */
public class C0474GZ extends DataClassSerializer {
    public static final C0474GZ dah = new C0474GZ();

    /* renamed from: c */
    public static boolean m3432c(ObjectOutput objectOutput) {
        if (!(objectOutput instanceof C6744aso) || (((C6744aso) objectOutput).mo2091PN() & 1) != 0) {
            return true;
        }
        return false;
    }

    /* renamed from: h */
    public static boolean m3434h(ObjectInput objectInput) {
        if (!(objectInput instanceof aHN) || (((aHN) objectInput).mo9185PN() & 1) != 0) {
            return true;
        }
        return false;
    }

    /* renamed from: c */
    public static boolean m3431c(IWriteExternal att) {
        if (!(att instanceof C1507WB) || (((C1507WB) att).mo6492PN() & 1) != 0) {
            return true;
        }
        return false;
    }

    /* renamed from: b */
    public static boolean m3429b(IReadExternal vm) {
        if (!(vm instanceof C5927adD) || (((C5927adD) vm).mo12747PN() & 1) != 0) {
            return true;
        }
        return false;
    }

    /* renamed from: d */
    public static boolean m3433d(IWriteExternal att) {
        if (att instanceof C1507WB) {
            return ((C1507WB) att).mo6494Xd();
        }
        return true;
    }

    /* renamed from: c */
    public static boolean m3430c(IReadExternal vm) {
        if (vm instanceof C5927adD) {
            return ((C5927adD) vm).mo12748Xd();
        }
        return true;
    }

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) {
        if (objectInput instanceof aHN) {
            return ((aHN) objectInput).mo9183PL();
        }
        return objectInput.readObject();
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) {
        if (objectOutput instanceof C6744aso) {
            ((C6744aso) objectOutput).mo2095f((C1616Xf) obj);
        } else {
            objectOutput.writeObject(obj);
        }
    }

    /* renamed from: a */
    public Object mo2358a(String str, IReadExternal vm) {
        if (vm instanceof C5927adD) {
            return ((C5927adD) vm).mo12749ix(str);
        }
        return super.mo2357a(vm);
    }

    /* renamed from: a */
    public Object mo2357a(IReadExternal vm) {
        if (vm instanceof C5927adD) {
            return ((C5927adD) vm).mo12749ix("value");
        }
        return super.mo2357a(vm);
    }

    /* renamed from: a */
    public void mo2361a(String str, IWriteExternal att, Object obj) {
        if (att instanceof C6744aso) {
            ((C1507WB) att).mo6495a(str, (C1616Xf) obj);
        } else {
            super.serializeData(att, obj);
        }
    }

    /* renamed from: a */
    public void serializeData(IWriteExternal att, Object obj) {
        if (att instanceof C6744aso) {
            ((C1507WB) att).mo6495a("value", (C1616Xf) obj);
        } else {
            super.serializeData(att, obj);
        }
    }
}
