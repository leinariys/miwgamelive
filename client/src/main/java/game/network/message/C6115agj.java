package game.network.message;

/* renamed from: a.agj  reason: case insensitive filesystem */
/* compiled from: a */
public class C6115agj {
    private long fxB;
    private long fxC;
    private long fxD;
    private long fxE;
    private long fxF;
    private long fxG = 0;

    public C6115agj(long j) {
        this.fxB = j;
    }

    /* renamed from: hf */
    public float mo13456hf(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.fxD == 0) {
            this.fxD = currentTimeMillis;
            this.fxC = j;
            this.fxF = currentTimeMillis;
            this.fxE = j;
            return (float) this.fxG;
        }
        if (currentTimeMillis >= this.fxF + this.fxB) {
            this.fxD = this.fxF;
            this.fxC = this.fxE;
            this.fxF = currentTimeMillis;
            this.fxE = j;
        }
        if (currentTimeMillis >= this.fxD + this.fxB) {
            this.fxG = (long) ((((float) (j - this.fxC)) * 1000.0f) / ((float) (currentTimeMillis - this.fxD)));
        }
        return (float) this.fxG;
    }

    public float bWB() {
        return (float) this.fxG;
    }
}
