package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.anf  reason: case insensitive filesystem */
/* compiled from: a */
public class C6475anf extends C1506WA implements Externalizable {

    private float gcA;
    private boolean gcB;
    private float gcz;

    public C6475anf() {
    }

    public C6475anf(float f, float f2) {
        this.gcz = f;
        this.gcA = f2;
        this.gcB = false;
    }

    public C6475anf(float f, float f2, boolean z) {
        this.gcz = f;
        this.gcA = f2;
        this.gcB = z;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public float cks() {
        return this.gcz;
    }

    public float ckt() {
        return this.gcA;
    }

    public boolean isAbsolute() {
        return this.gcB;
    }
}
