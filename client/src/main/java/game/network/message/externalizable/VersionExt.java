package game.network.message.externalizable;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aUa  reason: case insensitive filesystem */
/* compiled from: a */
public final class VersionExt implements Externalizable {
    private String version;

    public VersionExt() {
    }

    public VersionExt(String str) {
        this.version = str;
    }

    public String getVersion() {
        return this.version;
    }

    public void readExternal(ObjectInput objectInput) {
        this.version = objectInput.readUTF();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeUTF(this.version);
    }
}
