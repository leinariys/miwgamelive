package game.network.message.externalizable;

import game.network.message.C0474GZ;
import game.script.item.Item;
import game.script.item.ItemLocation;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.abi  reason: case insensitive filesystem */
/* compiled from: a */
public class C5854abi extends C1506WA implements Externalizable {
    private static final long serialVersionUID = 1730730387062606464L;
    private Item aoR;
    private ItemLocation apb;

    public C5854abi() {
        this.aoR = null;
        this.apb = null;
    }

    public C5854abi(ItemLocation aag, Item auq) {
        this.aoR = auq;
        this.apb = aag;
    }

    public void readExternal(ObjectInput objectInput) {
        this.aoR = (Item) C0474GZ.dah.inputReadObject(objectInput);
        this.apb = (ItemLocation) C0474GZ.dah.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        C0474GZ.dah.serializeData(objectOutput, (Object) this.aoR);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.apb);
    }

    /* renamed from: XH */
    public Item mo12511XH() {
        return this.aoR;
    }

    public ItemLocation bNh() {
        return this.apb;
    }
}
