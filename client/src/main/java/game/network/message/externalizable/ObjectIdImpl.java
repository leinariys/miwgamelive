package game.network.message.externalizable;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aBr  reason: case insensitive filesystem */
/* compiled from: a */
public class ObjectIdImpl extends C6302akO {

    private ObjectId objectId;

    public ObjectIdImpl() {
    }

    public ObjectIdImpl(ObjectId apo) {
        this.objectId = apo;
    }

    public ObjectId getObjectId() {
        return this.objectId;
    }

    public void readExternal(ObjectInput objectInput) {
        this.objectId = new ObjectId(objectInput.readLong());
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeLong(this.objectId.getId());
    }
}
