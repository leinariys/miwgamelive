package game.network.message.externalizable;

import game.script.player.Player;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aiV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6205aiV extends C1506WA implements Externalizable {

    private Player gcc;
    private C1909a gcd;

    public C6205aiV() {
    }

    public C6205aiV(C1909a aVar, Player aku) {
        this.gcd = aVar;
        this.gcc = aku;
    }

    public Player cke() {
        return this.gcc;
    }

    public C1909a ckf() {
        return this.gcd;
    }

    public void readExternal(ObjectInput objectInput) {
        this.gcc = (Player) objectInput.readObject();
        this.gcd = (C1909a) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.gcc);
        objectOutput.writeObject(this.gcd);
    }

    /* renamed from: a.aiV$a */
    public enum C1909a {
        TRADE,
        FRIEND,
        PARTY,
        CORP_HIRE,
        CORP_CREATION
    }
}
