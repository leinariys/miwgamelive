package game.network.message.externalizable;

import logic.ui.item.ListJ;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.*;

/* renamed from: a.Op */
/* compiled from: a */
public class C1006Op<T> implements List<T>, Externalizable {
    List<T> list;

    public C1006Op() {
    }

    public C1006Op(List<T> list2) {
        this.list = list2;
    }

    public C1006Op(Collection<T> collection) {
        if (collection instanceof ListJ) {
            this.list = (List) collection;
        } else {
            this.list = new ArrayList(collection);
        }
    }

    public boolean add(T t) {
        return this.list.add(t);
    }

    public void add(int i, T t) {
        this.list.add(i, t);
    }

    public boolean addAll(Collection<? extends T> collection) {
        return this.list.addAll(collection);
    }

    public boolean addAll(int i, Collection<? extends T> collection) {
        return this.list.addAll(i, collection);
    }

    public void clear() {
        this.list.clear();
    }

    public boolean contains(Object obj) {
        return this.list.contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.list.containsAll(collection);
    }

    public T get(int i) {
        return this.list.get(i);
    }

    public int indexOf(Object obj) {
        return this.list.indexOf(obj);
    }

    public boolean isEmpty() {
        return this.list.isEmpty();
    }

    public Iterator<T> iterator() {
        return this.list.iterator();
    }

    public int lastIndexOf(Object obj) {
        return this.list.lastIndexOf(obj);
    }

    public ListIterator<T> listIterator() {
        return this.list.listIterator();
    }

    public ListIterator<T> listIterator(int i) {
        return this.list.listIterator(i);
    }

    public boolean remove(Object obj) {
        return this.list.remove(obj);
    }

    public T remove(int i) {
        return this.list.remove(i);
    }

    public boolean removeAll(Collection<?> collection) {
        return this.list.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        return this.list.retainAll(collection);
    }

    public T set(int i, T t) {
        return this.list.set(i, t);
    }

    public int size() {
        return this.list.size();
    }

    public List<T> subList(int i, int i2) {
        return this.list.subList(i, i2);
    }

    public Object[] toArray() {
        return this.list.toArray();
    }

    public <V> V[] toArray(V[] vArr) {
        return this.list.toArray(vArr);
    }

    public void readExternal(ObjectInput objectInput) throws IOException {
        int readInt = objectInput.readInt();
        this.list = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            this.list.add(objectInput.readObject());
        }
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        if (this.list == null) {
            objectOutput.writeInt(0);
            return;
        }
        int size = this.list.size();
        objectOutput.writeInt(size);
        for (int i = 0; i < size; i++) {
            objectOutput.writeObject(this.list.get(i));
        }
    }
}
