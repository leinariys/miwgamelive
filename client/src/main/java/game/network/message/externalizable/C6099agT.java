package game.network.message.externalizable;

import p001a.C6272ajk;
import p001a.C6625aqZ;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Collection;

/* renamed from: a.agT  reason: case insensitive filesystem */
/* compiled from: a */
final class C6099agT implements C6272ajk, Externalizable {
    private Collection<C6625aqZ> ddl;
    private Collection<C6950awp> fFt;

    public C6099agT() {
    }

    C6099agT(Collection<C6625aqZ> collection, Collection<C6950awp> collection2) {
        this.ddl = collection;
        this.fFt = collection2;
    }

    public Collection<C6950awp> bXD() {
        return this.fFt;
    }

    public Collection<C6625aqZ> bXE() {
        return this.ddl;
    }

    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.ddl = (Collection) objectInput.readObject();
        this.fFt = (Collection) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeObject(this.ddl);
        objectOutput.writeObject(this.fFt);
    }
}
