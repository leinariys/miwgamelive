package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.WW */
/* compiled from: a */
public class C1532WW extends C1506WA implements Externalizable {

    private C1533a eAi;
    private Object eAj;

    public C1532WW() {
    }

    public C1532WW(C1533a aVar, Object obj) {
        this.eAi = aVar;
        this.eAj = obj;
    }

    public C1533a bEY() {
        return this.eAi;
    }

    public Object bEZ() {
        return this.eAj;
    }

    public void readExternal(ObjectInput objectInput) {
        this.eAi = (C1533a) objectInput.readObject();
        this.eAj = objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.eAi);
        objectOutput.writeObject(this.eAj);
    }

    /* renamed from: a.WW$a */
    public enum C1533a {
        OPENED,
        CLOSED
    }
}
