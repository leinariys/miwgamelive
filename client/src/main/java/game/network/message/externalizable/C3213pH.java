package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.C0474GZ;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import p001a.C3582se;
import p001a.CountFailedReadOrWriteException;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.pH */
/* compiled from: a */
public class C3213pH extends C5581aOv implements Externalizable {

    private C0677Jd aTC;

    public C3213pH() {
    }

    public C3213pH(C0677Jd jd) {
        this.aTC = jd;
    }

    /* renamed from: Wo */
    public C0677Jd mo21129Wo() {
        return this.aTC;
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.aTC.mo11944yn();
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        C1616Xf xf = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        try {
            this.aTC = (C0677Jd) xf.mo13W();
            this.aTC.mo3204i(objectInput);
        } catch (Exception e) {
            throw new CountFailedReadOrWriteException("Error deserializing " + xf.bFf().bFY(), e);
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.aTC.mo11944yn());
        this.aTC.mo3159a(objectOutput, 32);
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        return ((C3582se) this.aTC.mo11944yn().bFf()).mo5601b((C5581aOv) this);
    }
}
