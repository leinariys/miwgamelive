package game.network.message.externalizable;

import game.script.mission.actions.RecurrentMessageRequestMissionAction;
import game.script.npcchat.actions.RecurrentMessageRequestSpeechAction;
import game.script.resource.Asset;
import logic.aaa.C1506WA;
import taikodom.infra.script.I18NString;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aNj  reason: case insensitive filesystem */
/* compiled from: a */
public class C5543aNj extends C1506WA implements Externalizable {
    private static final long serialVersionUID = 8966198927476993526L;
    private String handle;
    private Asset irs;
    private I18NString irt;

    public C5543aNj() {
    }

    public C5543aNj(String str, I18NString i18NString, Asset tCVar) {
        this.handle = str;
        this.irt = i18NString;
        this.irs = tCVar;
    }

    public C5543aNj(RecurrentMessageRequestMissionAction aul) {
        this.handle = aul.mo16329YP();
        this.irt = aul.mo16330YR();
        this.irs = aul.mo16331YT();
    }

    public C5543aNj(RecurrentMessageRequestSpeechAction rIVar) {
        this.handle = rIVar.mo21577YP();
        this.irt = rIVar.mo21578YR();
        this.irs = rIVar.mo21579YT();
    }

    public Asset djL() {
        return this.irs;
    }

    public I18NString djM() {
        return this.irt;
    }

    public String getHandle() {
        return this.handle;
    }

    public void readExternal(ObjectInput objectInput) {
        this.handle = (String) objectInput.readObject();
        this.irt = (I18NString) objectInput.readObject();
        this.irs = (Asset) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.handle);
        objectOutput.writeObject(this.irt);
        objectOutput.writeObject(this.irs);
    }
}
