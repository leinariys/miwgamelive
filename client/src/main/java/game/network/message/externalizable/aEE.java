package game.network.message.externalizable;

import game.geometry.Vec3d;
import logic.aaa.C1506WA;
import taikodom.render.scene.SceneObject;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aEE */
/* compiled from: a */
public class aEE extends C1506WA implements Externalizable {

    private Vec3d ftj;
    private boolean hHw;
    private SceneObject target;

    public aEE() {
    }

    public aEE(boolean z, SceneObject sceneObject, Vec3d ajr) {
        this.hHw = z;
        this.target = sceneObject;
        this.ftj = ajr;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public boolean cXT() {
        return this.hHw;
    }

    public SceneObject getTarget() {
        return this.target;
    }

    public Vec3d bVs() {
        return this.ftj;
    }
}
