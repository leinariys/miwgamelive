package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aUU */
/* compiled from: a */
public class aUU extends C1506WA implements Externalizable {
    public static final short iXP = 0;
    public static final short iXQ = 1;
    public static final short iXR = 2;
    public static final short iXS = 5;
    private short iXT;

    public aUU() {
    }

    public aUU(short s) {
        this.iXT = s;
    }

    public short dAD() {
        return this.iXT;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeShort(this.iXT);
    }

    public void readExternal(ObjectInput objectInput) {
        this.iXT = objectInput.readShort();
    }
}
