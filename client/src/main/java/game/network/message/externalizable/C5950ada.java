package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.ada  reason: case insensitive filesystem */
/* compiled from: a */
public class C5950ada extends C1506WA implements Externalizable {
    private String message;

    public C5950ada() {
        this.message = "";
    }

    public C5950ada(String str) {
        this.message = str;
    }

    public String getMessage() {
        return this.message;
    }

    public void readExternal(ObjectInput objectInput) throws IOException {
        this.message = objectInput.readLine();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeChars(this.message);
    }
}
