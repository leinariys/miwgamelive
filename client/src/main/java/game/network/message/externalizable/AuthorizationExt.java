package game.network.message.externalizable;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Sj */
/* compiled from: a */
public final class AuthorizationExt implements Externalizable {
    private boolean isForcingNewUser;
    private String password;
    private String username;

    public AuthorizationExt() {
    }

    public AuthorizationExt(String str, String str2, boolean z) {
        this.username = str;
        this.password = str2;
        this.isForcingNewUser = z;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public boolean isForcingNewUser() {
        return this.isForcingNewUser;
    }

    public void readExternal(ObjectInput objectInput) throws IOException {
        this.username = objectInput.readUTF();
        this.password = objectInput.readUTF();
        this.isForcingNewUser = objectInput.readBoolean();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeUTF(this.username);
        objectOutput.writeUTF(this.password);
        objectOutput.writeBoolean(this.isForcingNewUser);
    }
}
