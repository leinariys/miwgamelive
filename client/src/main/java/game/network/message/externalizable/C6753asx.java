package game.network.message.externalizable;

import com.hoplon.geometry.Vec3f;
import p001a.C5917act;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.asx  reason: case insensitive filesystem */
/* compiled from: a */
public class C6753asx implements Externalizable {

    /* renamed from: dQ */
    Vec3f f5293dQ = new Vec3f(1000.0f, 1000.0f, 1000.0f);

    /* renamed from: dR */
    float f5294dR = 1000.0f;

    /* renamed from: dS */
    Vec3f f5295dS = new Vec3f(1000.0f, 1000.0f, 1000.0f);

    /* renamed from: dT */
    float f5296dT = 1000.0f;

    /* renamed from: dY */
    Vec3f f5297dY = new Vec3f(0.0f, 0.0f, -1.0f);
    Vec3f fbJ = new Vec3f(0.0f, 0.0f, 0.0f);
    Vec3f fbK = new Vec3f(0.0f, 0.0f, 0.0f);
    boolean fbN;

    public C6753asx() {
    }

    public C6753asx(C5917act act) {
        this.fbN = act.aXW();
        this.f5293dQ = act.f4279dQ;
        this.f5294dR = act.f4280dR;
        this.fbJ = act.fbJ;
        this.fbK = act.fbK;
        this.f5296dT = act.f4282dT;
        this.f5295dS = act.f4281dS;
        this.f5297dY = act.f4283dY;
    }

    public void readExternal(ObjectInput objectInput) {
        this.fbN = objectInput.readBoolean();
        this.f5293dQ.x = objectInput.readFloat();
        this.f5293dQ.y = objectInput.readFloat();
        this.f5293dQ.z = objectInput.readFloat();
        this.f5294dR = objectInput.readFloat();
        this.fbJ.x = objectInput.readFloat();
        this.fbJ.y = objectInput.readFloat();
        this.fbJ.z = objectInput.readFloat();
        this.fbK.x = objectInput.readFloat();
        this.fbK.y = objectInput.readFloat();
        this.fbK.z = objectInput.readFloat();
        this.f5296dT = objectInput.readFloat();
        Vec3f vec3f = this.f5295dS;
        Vec3f vec3f2 = this.f5295dS;
        Vec3f vec3f3 = this.f5295dS;
        float readFloat = objectInput.readFloat();
        vec3f3.z = readFloat;
        vec3f2.y = readFloat;
        vec3f.x = readFloat;
        this.f5297dY.set(this.fbK);
        this.f5297dY.cox();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.fbN);
        objectOutput.writeFloat(this.f5293dQ.x);
        objectOutput.writeFloat(this.f5293dQ.y);
        objectOutput.writeFloat(this.f5293dQ.z);
        objectOutput.writeFloat(this.f5294dR);
        objectOutput.writeFloat(this.fbJ.x);
        objectOutput.writeFloat(this.fbJ.y);
        objectOutput.writeFloat(this.fbJ.z);
        objectOutput.writeFloat(this.fbK.x);
        objectOutput.writeFloat(this.fbK.y);
        objectOutput.writeFloat(this.fbK.z);
        objectOutput.writeFloat(this.f5296dT);
        objectOutput.writeFloat(this.f5295dS.z);
    }
}
