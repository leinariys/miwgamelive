package game.network.message.externalizable;

import logic.aaa.C1506WA;
import p001a.C5956adg;

import java.awt.*;
import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.agw  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6128agw extends C1506WA implements Externalizable {
    private static /* synthetic */ int[] fyr;
    public Object[] args;
    public boolean fyn;
    public C1890a fyo;
    public boolean fyp;
    public boolean fyq;

    public C6128agw() {
    }

    public C6128agw(boolean z, C1890a aVar, Object... objArr) {
        this.fyn = z;
        this.fyo = aVar;
        this.args = objArr;
        this.fyp = false;
        this.fyq = false;
    }

    public C6128agw(boolean z, boolean z2, C1890a aVar, Object... objArr) {
        this.fyn = z;
        this.fyo = aVar;
        this.args = objArr;
        this.fyp = z2;
        this.fyq = false;
    }

    public C6128agw(boolean z, boolean z2, boolean z3, C1890a aVar, Object... objArr) {
        this.fyn = z;
        this.fyo = aVar;
        this.args = objArr;
        this.fyp = z2;
        this.fyq = z3;
    }

    public C6128agw(boolean z, Object... objArr) {
        this(z, false, C1890a.WHITE, objArr);
    }

    public C6128agw(boolean z, boolean z2, Object... objArr) {
        this(z, z2, C1890a.WHITE, objArr);
    }

    static /* synthetic */ int[] bWQ() {
        int[] iArr = fyr;
        if (iArr == null) {
            iArr = new int[C1890a.values().length];
            try {
                iArr[C1890a.RED.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C1890a.WHITE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C1890a.YELLOW.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            fyr = iArr;
        }
        return iArr;
    }

    public abstract String getMessage();

    public String bWP() {
        return C5956adg.format(getMessage(), this.args);
    }

    public Color getColor() {
        switch (bWQ()[this.fyo.ordinal()]) {
            case 2:
                return Color.RED;
            case 3:
                return Color.YELLOW;
            default:
                return Color.WHITE;
        }
    }

    public void readExternal(ObjectInput objectInput) {
        this.args = (Object[]) objectInput.readObject();
        this.fyn = objectInput.readBoolean();
        this.fyp = objectInput.readBoolean();
        this.fyq = objectInput.readBoolean();
        this.fyo = C1890a.values()[objectInput.readInt()];
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.args);
        objectOutput.writeBoolean(this.fyn);
        objectOutput.writeBoolean(this.fyp);
        objectOutput.writeBoolean(this.fyq);
        objectOutput.writeInt(this.fyo.ordinal());
    }

    /* renamed from: a.agw$a */
    public enum C1890a {
        WHITE,
        RED,
        YELLOW
    }
}
