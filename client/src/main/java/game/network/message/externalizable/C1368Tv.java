package game.network.message.externalizable;

import game.script.player.Player;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Tv */
/* compiled from: a */
public class C1368Tv extends C1506WA implements Externalizable {
    private Player auM;
    private Player auN;
    private C1369a auO;
    private String message;

    public C1368Tv() {
    }

    public C1368Tv(Player aku, Player aku2, C1369a aVar, String str) {
        this.auN = aku;
        this.auM = aku2;
        this.auO = aVar;
        this.message = str;
    }

    /* renamed from: KH */
    public C1369a mo5759KH() {
        return this.auO;
    }

    public String getMessage() {
        return this.message;
    }

    /* renamed from: KJ */
    public Player mo5760KJ() {
        return this.auN;
    }

    /* renamed from: KK */
    public Player mo5761KK() {
        return this.auM;
    }

    public void readExternal(ObjectInput objectInput) {
        this.auN = (Player) objectInput.readObject();
        this.auM = (Player) objectInput.readObject();
        this.auO = C1369a.values()[objectInput.readInt()];
        this.message = (String) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.auN);
        objectOutput.writeObject(this.auM);
        objectOutput.writeInt(this.auO.ordinal());
        objectOutput.writeObject(this.message);
    }

    /* renamed from: a.Tv$a */
    public enum C1369a {
        LOCAL("local"),
        PARTY("party"),
        CORPORATION("corp"),
        PRIVATE("pvt"),
        STATION("station"),
        ACTIONS("actions"),
        SYSTEM("sys"),
        USER("user");

        public final String name;

        private C1369a(String str) {
            this.name = str;
        }
    }
}
