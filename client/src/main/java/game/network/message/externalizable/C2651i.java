package game.network.message.externalizable;

import game.script.ship.Ship;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.i */
/* compiled from: a */
public class C2651i extends C1506WA implements Externalizable {


    /* renamed from: N */
    private Ship f8090N;

    /* renamed from: O */
    private Ship f8091O;

    public C2651i() {
    }

    public C2651i(Ship fAVar, Ship fAVar2) {
        this.f8091O = fAVar;
        this.f8090N = fAVar2;
    }

    /* renamed from: a */
    public Ship mo19441a() {
        return this.f8090N;
    }

    /* renamed from: b */
    public Ship mo19442b() {
        return this.f8091O;
    }

    public void readExternal(ObjectInput objectInput) {
        this.f8091O = (Ship) objectInput.readObject();
        this.f8090N = (Ship) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.f8091O);
        objectOutput.writeObject(this.f8090N);
    }
}
