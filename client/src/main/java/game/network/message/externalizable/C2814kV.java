package game.network.message.externalizable;

import game.network.message.C0474GZ;
import game.script.Actor;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.kV */
/* compiled from: a */
public class C2814kV extends C1506WA implements Externalizable {


    /* renamed from: Lo */
    private Actor f8426Lo;
    private float amount;
    private C2815a bem;

    public C2814kV() {
    }

    public C2814kV(float f, C2815a aVar, Actor cr) {
        this.amount = f;
        this.bem = aVar;
        this.f8426Lo = cr;
    }

    public float getAmount() {
        return this.amount;
    }

    public C2815a aag() {
        return this.bem;
    }

    /* renamed from: rf */
    public Actor mo20073rf() {
        return this.f8426Lo;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeFloat(this.amount);
        objectOutput.writeObject(this.bem);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f8426Lo);
    }

    public void readExternal(ObjectInput objectInput) {
        this.amount = objectInput.readFloat();
        this.bem = (C2815a) objectInput.readObject();
        this.f8426Lo = (Actor) C0474GZ.dah.inputReadObject(objectInput);
    }

    /* renamed from: a.kV$a */
    public enum C2815a {
        SHIELD,
        HULL
    }
}
