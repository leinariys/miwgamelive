package game.network.message.externalizable;

import com.hoplon.geometry.Vec3f;
import p001a.C0832M;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.ajE  reason: case insensitive filesystem */
/* compiled from: a */
public class C6240ajE implements Externalizable {

    /* renamed from: dQ */
    Vec3f f4684dQ;

    /* renamed from: dR */
    float f4685dR;

    /* renamed from: dS */
    Vec3f f4686dS;

    /* renamed from: dT */
    float f4687dT;

    /* renamed from: dU */
    Vec3f f4688dU;

    /* renamed from: dV */
    Vec3f f4689dV;

    /* renamed from: dW */
    float f4690dW;

    /* renamed from: dX */
    float f4691dX;

    public C6240ajE() {
    }

    public C6240ajE(C0832M m) {
        this.f4684dQ = m.f1067dQ;
        this.f4685dR = m.f1068dR;
        this.f4686dS = m.f1069dS;
        this.f4687dT = m.f1070dT;
        this.f4688dU = m.f1071dU;
        this.f4689dV = m.f1072dV;
        this.f4690dW = m.f1073dW;
        this.f4691dX = m.f1074dX;
    }

    public void readExternal(ObjectInput objectInput) {
        this.f4684dQ = new Vec3f();
        this.f4684dQ.x = objectInput.readFloat();
        this.f4684dQ.y = objectInput.readFloat();
        this.f4684dQ.z = objectInput.readFloat();
        this.f4685dR = objectInput.readFloat();
        this.f4686dS = new Vec3f();
        this.f4686dS.x = objectInput.readFloat();
        this.f4686dS.y = objectInput.readFloat();
        this.f4686dS.z = objectInput.readFloat();
        this.f4687dT = objectInput.readFloat();
        this.f4688dU = new Vec3f();
        this.f4688dU.x = objectInput.readFloat();
        this.f4688dU.y = objectInput.readFloat();
        this.f4688dU.z = objectInput.readFloat();
        this.f4689dV = new Vec3f();
        this.f4689dV.x = objectInput.readFloat();
        this.f4689dV.y = objectInput.readFloat();
        this.f4689dV.z = objectInput.readFloat();
        this.f4690dW = objectInput.readFloat();
        this.f4691dX = objectInput.readFloat();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeFloat(this.f4684dQ.x);
        objectOutput.writeFloat(this.f4684dQ.y);
        objectOutput.writeFloat(this.f4684dQ.z);
        objectOutput.writeFloat(this.f4685dR);
        objectOutput.writeFloat(this.f4686dS.x);
        objectOutput.writeFloat(this.f4686dS.y);
        objectOutput.writeFloat(this.f4686dS.z);
        objectOutput.writeFloat(this.f4687dT);
        objectOutput.writeFloat(this.f4688dU.x);
        objectOutput.writeFloat(this.f4688dU.y);
        objectOutput.writeFloat(this.f4688dU.z);
        objectOutput.writeFloat(this.f4689dV.x);
        objectOutput.writeFloat(this.f4689dV.y);
        objectOutput.writeFloat(this.f4689dV.z);
        objectOutput.writeFloat(this.f4690dW);
        objectOutput.writeFloat(this.f4691dX);
    }
}
