package game.network.message.externalizable;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aut  reason: case insensitive filesystem */
/* compiled from: a */
public class C6853aut implements Externalizable {

    private final Matrix4fWrap transform = new Matrix4fWrap();
    private String name;

    public C6853aut() {
    }

    public C6853aut(String str, Matrix4fWrap ajk) {
        this.name = str;
        this.transform.set(ajk);
    }

    public C6853aut(String str, Vec3f vec3f, Quat4fWrap aoy) {
        aoy.mo15237e(this.transform);
        this.transform.setTranslation(vec3f);
    }

    public String toString() {
        return "Bone: " + this.name + " " + this.transform;
    }

    public void readExternal(ObjectInput objectInput) {
        this.name = objectInput.readUTF();
        this.transform.m00 = objectInput.readFloat();
        this.transform.m01 = objectInput.readFloat();
        this.transform.m02 = objectInput.readFloat();
        this.transform.m03 = objectInput.readFloat();
        this.transform.m10 = objectInput.readFloat();
        this.transform.m11 = objectInput.readFloat();
        this.transform.m12 = objectInput.readFloat();
        this.transform.m13 = objectInput.readFloat();
        this.transform.m20 = objectInput.readFloat();
        this.transform.m21 = objectInput.readFloat();
        this.transform.m22 = objectInput.readFloat();
        this.transform.m23 = objectInput.readFloat();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeUTF(this.name);
        objectOutput.writeFloat(this.transform.m00);
        objectOutput.writeFloat(this.transform.m01);
        objectOutput.writeFloat(this.transform.m02);
        objectOutput.writeFloat(this.transform.m03);
        objectOutput.writeFloat(this.transform.m10);
        objectOutput.writeFloat(this.transform.m11);
        objectOutput.writeFloat(this.transform.m12);
        objectOutput.writeFloat(this.transform.m13);
        objectOutput.writeFloat(this.transform.m20);
        objectOutput.writeFloat(this.transform.m21);
        objectOutput.writeFloat(this.transform.m22);
        objectOutput.writeFloat(this.transform.m23);
    }

    public Matrix4fWrap getTransform() {
        return this.transform;
    }

    public String getName() {
        return this.name;
    }

    /* renamed from: gL */
    public Vec3f mo16402gL() {
        return this.transform.ceH();
    }

    public Quat4fWrap getOrientation() {
        return new Quat4fWrap(this.transform);
    }

    public boolean equals(Object obj) {
        if (obj instanceof C6853aut) {
            return this.transform.equals(((C6853aut) obj).transform) && this.name.equals(((C6853aut) obj).name);
        }
        return super.equals(obj);
    }
}
