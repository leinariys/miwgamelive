package game.network.message.externalizable;

import game.network.message.C0474GZ;
import game.script.Actor;
import game.script.item.Item;
import game.script.ship.Station;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.abA  reason: case insensitive filesystem */
/* compiled from: a */
public class C5820abA implements Externalizable {
    private Item aoR;
    private Actor eZy;

    /* renamed from: iH */
    private Station f4124iH;

    public C5820abA() {
    }

    public C5820abA(Station bf, Actor cr, Item auq) {
        this.eZy = cr;
        this.f4124iH = bf;
        this.aoR = auq;
    }

    /* renamed from: eT */
    public Station mo12361eT() {
        return this.f4124iH;
    }

    /* renamed from: XH */
    public Item mo12359XH() {
        return this.aoR;
    }

    public Actor bNF() {
        return this.eZy;
    }

    public void readExternal(ObjectInput objectInput) {
        this.f4124iH = (Station) C0474GZ.dah.inputReadObject(objectInput);
        this.eZy = (Actor) C0474GZ.dah.inputReadObject(objectInput);
        this.aoR = (Item) C0474GZ.dah.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f4124iH);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.eZy);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.aoR);
    }
}
