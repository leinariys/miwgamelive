package game.network.message.externalizable;

import game.script.mission.actions.RecurrentMessageRequestMissionAction;
import game.script.npcchat.actions.RecurrentMessageRequestSpeechAction;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.apL  reason: case insensitive filesystem */
/* compiled from: a */
public class C6559apL extends C1506WA implements Externalizable {
    private static final long serialVersionUID = -2344060328412214217L;
    private String handle;

    public C6559apL() {
    }

    public C6559apL(String str) {
        this.handle = str;
    }

    public C6559apL(RecurrentMessageRequestMissionAction aul) {
        this.handle = aul.mo16329YP();
    }

    public C6559apL(RecurrentMessageRequestSpeechAction rIVar) {
        this.handle = rIVar.mo21577YP();
    }

    public String getHandle() {
        return this.handle;
    }

    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.handle = (String) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeObject(this.handle);
    }
}
