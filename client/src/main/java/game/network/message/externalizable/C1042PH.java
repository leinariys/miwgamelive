package game.network.message.externalizable;

import com.hoplon.geometry.Vec3f;
import game.script.Actor;
import logic.aaa.C1506WA;
import logic.thred.C6339akz;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.PH */
/* compiled from: a */
public class C1042PH extends C1506WA implements Externalizable {


    /* renamed from: Oj */
    private Vec3f f1360Oj;
    private Actor bte;
    private Actor dSa;
    private float dSb;
    private C6339akz dSc;
    private Vec3f relativePosition;

    public C1042PH() {
    }

    public C1042PH(Actor cr, Actor cr2, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        this.bte = cr;
        this.dSa = cr2;
        this.f1360Oj = vec3f;
        this.relativePosition = vec3f2;
        this.dSb = f;
        this.dSc = akz;
    }

    public Actor bnT() {
        return this.bte;
    }

    public Actor bnU() {
        return this.dSa;
    }

    /* renamed from: vO */
    public Vec3f mo4650vO() {
        return this.f1360Oj;
    }

    public Vec3f bnV() {
        return this.relativePosition;
    }

    public float bnW() {
        return this.dSb;
    }

    public C6339akz bnX() {
        return this.dSc;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("ActorCollisionEvent should be called only on the client side.");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("ActorCollisionEvent should be called only on the client side.");
    }
}
