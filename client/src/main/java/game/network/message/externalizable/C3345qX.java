package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.C0474GZ;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;
import p001a.C1875af;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.qX */
/* compiled from: a */
public class C3345qX extends C5581aOv implements Externalizable {


    /* renamed from: Uk */
    private C1616Xf f8934Uk;
    private boolean aZj;

    public C3345qX() {
    }

    public C3345qX(C1616Xf xf, boolean z) {
        this.f8934Uk = xf;
        this.aZj = z;
    }

    /* renamed from: Yg */
    public boolean mo21371Yg() {
        return this.aZj;
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.f8934Uk;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.aZj = objectInput.readBoolean();
        this.f8934Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeBoolean(this.aZj);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f8934Uk);
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        return ((C1875af) this.f8934Uk.bFf()).mo13201a(bVar, (C5581aOv) this);
    }
}
