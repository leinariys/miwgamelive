package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aNH */
/* compiled from: a */
public class aNH extends C1506WA implements Externalizable {
    private static final long serialVersionUID = -183288616135870439L;
    private final boolean visible;

    public aNH() {
        this.visible = true;
    }

    public aNH(boolean z) {
        this.visible = z;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public void readExternal(ObjectInput objectInput) {
    }

    public void writeExternal(ObjectOutput objectOutput) {
    }
}
