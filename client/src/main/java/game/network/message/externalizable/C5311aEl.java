package game.network.message.externalizable;

import game.script.npc.NPCType;
import game.script.resource.Asset;
import logic.aaa.C1506WA;
import p001a.C3248pc;
import p001a.C5566aOg;
import taikodom.infra.script.I18NString;

import java.awt.event.ActionListener;
import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aEl  reason: case insensitive filesystem */
/* compiled from: a */
public class C5311aEl extends C1506WA implements Externalizable {
    private Asset bfO;
    private NPCType bhT;
    private ActionListener hFi;
    private long hFj;
    private I18NString hFk;
    private long timeout;

    public C5311aEl() {
    }

    public C5311aEl(NPCType aed, I18NString i18NString, long j, Asset tCVar) {
        this.bhT = aed;
        this.hFk = i18NString;
        this.timeout = j;
        this.bfO = tCVar;
    }

    public ActionListener cXr() {
        return this.hFi;
    }

    public long cXs() {
        return this.hFj;
    }

    public I18NString cXt() {
        return this.hFk;
    }

    public NPCType cXu() {
        return this.bhT;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    public Asset abx() {
        return this.bfO;
    }

    public long getTimeout() {
        return this.timeout;
    }

    public boolean cGZ() {
        if (getTimeout() != 0 && cXs() + getTimeout() < System.currentTimeMillis()) {
            return true;
        }
        return false;
    }

    public void readExternal(ObjectInput objectInput) {
        this.bhT = (NPCType) objectInput.readObject();
        this.hFk = (I18NString) objectInput.readObject();
        this.timeout = objectInput.readLong();
        this.hFj = objectInput.readLong();
        this.hFi = (ActionListener) objectInput.readObject();
        this.bfO = (Asset) objectInput.readObject();
    }

    /* renamed from: d */
    public void mo8664d(ActionListener actionListener) {
        this.hFi = actionListener;
    }

    public void cXv() {
        this.hFj = System.currentTimeMillis();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C5566aOg
    /* renamed from: U */
    public void mo8656U(Asset tCVar) {
        this.bfO = tCVar;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.bhT);
        objectOutput.writeObject(this.hFk);
        objectOutput.writeLong(this.timeout);
        objectOutput.writeLong(this.hFj);
        objectOutput.writeObject(this.hFi);
        objectOutput.writeObject(this.bfO);
    }
}
