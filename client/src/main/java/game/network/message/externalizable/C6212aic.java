package game.network.message.externalizable;

import game.network.manager.DefaultMagicDataClassSerializer;
import game.network.message.Blocking;
import p001a.C1469Vd;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;

/* renamed from: a.aic  reason: case insensitive filesystem */
/* compiled from: a */
public class C6212aic extends C1469Vd implements Externalizable {

    private Object bgK;
    private Blocking[] fMF;

    public C6212aic() {
    }

    public C6212aic(Object obj, List<Blocking> list) {
        this.bgK = obj;
        if (list != null) {
            this.fMF = (Blocking[]) list.toArray(new Blocking[list.size()]);
        }
    }

    public Object getResult() {
        return this.bgK;
    }

    public Blocking[] aWo() {
        return this.fMF;
    }

    public void readExternal(ObjectInput objectInput) {
        if (!objectInput.readBoolean()) {
            this.fMF = null;
        } else {
            int readInt = objectInput.readInt();
            Blocking[] kUVarArr = new Blocking[readInt];
            this.fMF = kUVarArr;
            for (int i = 0; i < readInt; i++) {
                kUVarArr[i] = (Blocking) DefaultMagicDataClassSerializer.dataClassSerializer.inputReadObject(objectInput);
            }
        }
        this.bgK = objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        if (this.fMF == null) {
            objectOutput.writeBoolean(false);
        } else {
            objectOutput.writeBoolean(true);
            objectOutput.writeInt(r2);
            for (Blocking a : this.fMF) {
                DefaultMagicDataClassSerializer.dataClassSerializer.serializeData(objectOutput, (Object) a);
            }
        }
        objectOutput.writeObject(this.bgK);
    }
}
