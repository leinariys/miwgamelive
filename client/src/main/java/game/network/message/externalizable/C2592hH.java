package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.C0474GZ;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;
import p001a.C0755Kn;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.hH */
/* compiled from: a */
public class C2592hH extends C5581aOv implements Externalizable {


    /* renamed from: Uk */
    private C1616Xf f7859Uk;

    public C2592hH() {
    }

    public C2592hH(C1616Xf xf) {
        this.f7859Uk = xf;
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.f7859Uk;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f7859Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f7859Uk);
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        return new C0755Kn(jz.bGE().mo6901yn());
    }
}
