package game.network.message.externalizable;

import game.script.missile.MissileWeapon;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Lk */
/* compiled from: a */
public class C0813Lk extends C1506WA implements Externalizable {
    private boolean dtt;
    private MissileWeapon dtu;

    public C0813Lk() {
    }

    public C0813Lk(boolean z, MissileWeapon bp) {
        this.dtt = z;
        this.dtu = bp;
    }

    public boolean isLocked() {
        return this.dtt;
    }

    public MissileWeapon bfc() {
        return this.dtu;
    }

    public void readExternal(ObjectInput objectInput) {
        this.dtt = objectInput.readBoolean();
        this.dtu = (MissileWeapon) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.dtt);
        objectOutput.writeObject(this.dtu);
    }
}
