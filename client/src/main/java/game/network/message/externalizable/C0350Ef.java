package game.network.message.externalizable;

import game.script.player.Player;
import game.script.trade.Trade;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Ef */
/* compiled from: a */
public class C0350Ef extends C1506WA implements Externalizable {
    private static final long serialVersionUID = 6139259691842003329L;
    private Player elO;
    private Trade fky;
    private C0351a fvw;

    public C0350Ef() {
    }

    public C0350Ef(Player aku, C0351a aVar, Trade td) {
        this.elO = aku;
        this.fvw = aVar;
        this.fky = td;
    }

    public Player buL() {
        return this.elO;
    }

    public C0351a bVH() {
        return this.fvw;
    }

    public Trade bVI() {
        return this.fky;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        try {
            objectOutput.writeObject(this.elO);

            objectOutput.writeObject(this.fvw);
            objectOutput.writeObject(this.fky);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readExternal(ObjectInput objectInput) {
        try {
            this.elO = (Player) objectInput.readObject();

            this.fvw = (C0351a) objectInput.readObject();
            this.fky = (Trade) objectInput.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a.Ef$a */
    public enum C0351a {
        INVITED,
        ACCEPTED
    }
}
