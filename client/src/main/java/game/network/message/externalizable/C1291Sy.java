package game.network.message.externalizable;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.exception.CreatListenerChannelException;
import game.network.message.C0474GZ;
import game.network.message.serializable.C1556Wo;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.res.code.C5663aRz;
import logic.res.html.C1867ad;
import logic.res.html.DataClassSerializer;
import logic.thred.LogPrinter;
import org.apache.commons.collections.map.AbstractHashedMap;
import org.apache.commons.collections.map.HashedMap;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.util.Iterator;
import java.util.Map;

/* renamed from: a.Sy */
/* compiled from: a */
public class C1291Sy extends HashedMap implements C1556Wo, Externalizable {

    static LogPrinter log = LogPrinter.m10275K(C1291Sy.class);
    /* renamed from: Uk */
    private C1616Xf f1582Uk;

    /* renamed from: Ul */
    private C5663aRz f1583Ul;
    private boolean gFp = false;

    public C1291Sy() {
    }

    public C1291Sy(C1616Xf xf, C5663aRz arz) {
        this.f1582Uk = xf;
        this.f1583Ul = arz;
    }

    public C1291Sy(C1616Xf xf, C5663aRz arz, Map map) {
        this.f1582Uk = xf;
        this.f1583Ul = arz;
        this.gFp = true;
        try {
            putAll(map);
        } finally {
            this.gFp = false;
        }
    }

    /* access modifiers changed from: protected */
    public AbstractHashedMap.HashEntry createEntry(AbstractHashedMap.HashEntry hashEntry, int i, Object obj, Object obj2) {
        return new C1292a(hashEntry, i, obj, obj2);
    }

    /* access modifiers changed from: protected */
    public void cyw() {
        if (!this.gFp && !this.f1582Uk.bFf().bFT()) {
            ((C0677Jd) this.f1582Uk.bFf().mo5608dq()).mo3223v(this.f1583Ul);
        }
    }

    /* access modifiers changed from: protected */
    public void updateEntry(AbstractHashedMap.HashEntry hashEntry, Object obj) {
        C1291Sy.super.updateEntry(hashEntry, obj);
    }

    public Object put(Object obj, Object obj2) {
        if (this.f1582Uk == null || this.f1582Uk.bFf() == null) {
            return null;
        }
        if (!this.f1582Uk.bFf().bFT() && this.f1582Uk.bFf().mo6887du()) {
            return null;
        }
        if (obj == null || this.f1583Ul.mo11295Eq()[0].isInstance(obj)) {
            return C1291Sy.super.put(obj, obj2);
        }
        throw new RuntimeException("Wrong key type: " + obj.getClass() + " != " + this.f1583Ul.mo11295Eq()[0]);
    }

    /* access modifiers changed from: protected */
    public void addMapping(int i, int i2, Object obj, Object obj2) {
        C1291Sy.super.addMapping(i, i2, obj, obj2);
        cyw();
    }

    /* access modifiers changed from: protected */
    public void removeMapping(AbstractHashedMap.HashEntry hashEntry, int i, AbstractHashedMap.HashEntry hashEntry2) {
        if (this.f1582Uk.bFf().bFT() || !this.f1582Uk.bFf().mo6887du()) {
            C1291Sy.super.removeMapping(hashEntry, i, hashEntry2);
            cyw();
        }
    }

    /* renamed from: h */
    public void mo5513h(Object obj, Object obj2) {
        C1291Sy.super.put(obj, obj2);
    }

    /* renamed from: av */
    public Object mo5507av(Object obj) {
        return C1291Sy.super.remove(obj);
    }

    public void clear() {
        if (this.f1582Uk.bFf().bFT() || !this.f1582Uk.bFf().mo6887du()) {
            cyw();
            C1291Sy.super.clear();
        }
    }

    /* renamed from: XC */
    public C1867ad<Object> mo5505XC() {
        throw new CreatListenerChannelException();
    }

    /* renamed from: ym */
    public C5663aRz mo5521ym() {
        return this.f1583Ul;
    }

    /* renamed from: c */
    public void mo5508c(C5663aRz arz) {
        this.f1583Ul = arz;
    }

    public Object clone() {
        return new C1291Sy(this.f1582Uk, this.f1583Ul, this);
    }

    public void readExternal(ObjectInput objectInput) {
        DataClassSerializer kd;
        DataClassSerializer kd2;
        this.f1582Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        this.f1583Ul = this.f1582Uk.mo25c()[objectInput.readShort()];
        this.gFp = true;
        try {
            int readInt = objectInput.readInt();
            if (readInt != 0) {
                C1291Sy.super.clear();
                DataClassSerializer[] Eu = this.f1583Ul.mo11298Eu();
                if (Eu == null || Eu.length != 2) {
                    kd = DataClassSerializer.dnL;
                    kd2 = DataClassSerializer.dnL;
                } else {
                    kd = Eu[0];
                    kd2 = Eu[1];
                }
                for (int i = 0; i < readInt; i++) {
                    C1291Sy.super.put(kd.inputReadObject(objectInput), kd2.inputReadObject(objectInput));
                }
                this.gFp = false;
            }
        } finally {
            this.gFp = false;
        }
    }

    public void readExternal(IReadExternal vm) {
        int gX = vm.readInt("count");
        if (gX != 0) {
            this.gFp = true;
            try {
                C1291Sy.super.clear();
                for (int i = 0; i < gX; i++) {
                    C1291Sy.super.put(vm.mo6366hd("key"), vm.mo6366hd("value"));
                }
            } finally {
                this.gFp = false;
            }
        }
    }

    /* JADX WARNING: type inference failed for: r2v11, types: [java.util.TreeMap, java.util.SortedMap] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0097 A[LOOP:0: B:17:0x0062->B:25:0x0097, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0068 A[EDGE_INSN: B:26:0x0068->B:19:0x0068 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:28:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void writeExternal(java.io.ObjectOutput r7) {
        /*
            r6 = this;
            r1 = 0
            a.GZ r0 = game.network.message.C0474GZ.dah
            a.Xf r2 = r6.f1582Uk
            r0.mo2360a((java.io.ObjectOutput) r7, (java.lang.Object) r2)
            a.aRz r0 = r6.f1583Ul
            int r0 = r0.mo7395hq()
            r7.writeShort(r0)
            if (r6 == 0) goto L_0x001d
            int r0 = r6.size()
        L_0x0017:
            r7.writeInt(r0)
            if (r0 != 0) goto L_0x001f
        L_0x001c:
            return
        L_0x001d:
            r0 = r1
            goto L_0x0017
        L_0x001f:
            boolean r2 = p001a.C6129agx.m22158d(r7)
            if (r2 == 0) goto L_0x0090
            java.util.Set r2 = r6.keySet()
            java.util.Iterator r2 = r2.iterator()
            boolean r2 = p001a.C3186or.m36906a(r2)
            if (r2 == 0) goto L_0x0070
            a.Jz r2 = p001a.C6129agx.m22159e(r7)
            a.arE r3 = r2.bxy()
            java.util.TreeMap r2 = new java.util.TreeMap
            a.or r4 = new a.or
            r4.<init>(r3)
            r2.<init>(r4)
            r2.putAll(r6)
        L_0x0048:
            a.aRz r3 = r6.f1583Ul
            a.Kd[] r4 = r3.mo11298Eu()
            if (r4 == 0) goto L_0x0092
            int r3 = r4.length
            r5 = 2
            if (r3 != r5) goto L_0x0092
            r3 = r4[r1]
            r1 = 1
            r1 = r4[r1]
        L_0x0059:
            java.util.Set r2 = r2.entrySet()
            java.util.Iterator r4 = r2.iterator()
            r2 = r0
        L_0x0062:
            boolean r0 = r4.hasNext()
            if (r0 != 0) goto L_0x0097
            if (r2 == 0) goto L_0x001c
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            r0.<init>()
            throw r0
        L_0x0070:
            a.UR r2 = log
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "Deterministic mode not supported for "
            r3.<init>(r4)
            java.util.Set r4 = r6.keySet()
            java.util.Iterator r4 = r4.iterator()
            java.lang.Object r4 = r4.next()
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.warn(r3)
        L_0x0090:
            r2 = r6
            goto L_0x0048
        L_0x0092:
            a.Kd r3 = logic.res.html.C0744Kd.dnL
            a.Kd r1 = logic.res.html.C0744Kd.dnL
            goto L_0x0059
        L_0x0097:
            java.lang.Object r0 = r4.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r5 = r0.getKey()
            r3.mo2360a((java.io.ObjectOutput) r7, (java.lang.Object) r5)
            java.lang.Object r0 = r0.getValue()
            r1.mo2360a((java.io.ObjectOutput) r7, (java.lang.Object) r0)
            int r0 = r2 + -1
            r2 = r0
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: game.network.message.externalizable.C1291Sy.writeExternal(java.io.ObjectOutput):void");
    }

    public void writeExternal(IWriteExternal att) {
        int i;
        int size = this != null ? size() : 0;
        att.writeInt("count", size);
        if (size != 0) {
            Iterator it = entrySet().iterator();
            while (true) {
                i = size;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry entry = (Map.Entry) it.next();
                att.mo16273g("key", entry.getKey());
                att.mo16273g("value", entry.getValue());
                size = i - 1;
            }
            if (i != 0) {
                throw new IllegalStateException();
            }
        }
    }

    /* renamed from: a.Sy$a */
    public class C1292a extends AbstractHashedMap.HashEntry {
        public C1292a(AbstractHashedMap.HashEntry hashEntry, int i, Object obj, Object obj2) {
            super(hashEntry, i, obj, obj2);
        }

        public Object setValue(Object obj) {
            C1291Sy.this.cyw();
            return C1291Sy.super.setValue(obj);
        }
    }
}
