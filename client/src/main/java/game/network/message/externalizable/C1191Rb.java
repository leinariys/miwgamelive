package game.network.message.externalizable;

import game.script.player.Player;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Rb */
/* compiled from: a */
public class C1191Rb extends C1506WA implements Externalizable {
    private static final long serialVersionUID = 470187373860689024L;
    private Player elO;
    private C1192a elP;

    public C1191Rb() {
    }

    public C1191Rb(Player aku, C1192a aVar) {
        this.elO = aku;
        this.elP = aVar;
    }

    public Player buL() {
        return this.elO;
    }

    public C1192a bwG() {
        return this.elP;
    }

    public void readExternal(ObjectInput objectInput) {
        this.elO = (Player) objectInput.readObject();
        this.elP = (C1192a) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.elO);
        objectOutput.writeObject(this.elP);
    }

    /* renamed from: a.Rb$a */
    public enum C1192a {
        OTHER_REFUSED,
        PLAYER_OFFLINE,
        PLAYER_UNKNOWN,
        PLAYER_CANCELED,
        PLAYER_HAS_NO_STORAGE,
        PLAYER_HAS_NOT_ENOUGH_MONEY,
        PLAYER_CANNOT_RECEIVE_ALL,
        SUCCESSFUL,
        SAME_PLAYER,
        OTHER_BUSY,
        NOT_IN_SAME_STATION,
        NOT_IN_SAME_NODE,
        PLAYER_BLOCKED,
        TESTER_INTERACTION_DENIED,
        PLAYER_TOO_FAR
    }
}
