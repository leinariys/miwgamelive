package game.network.message.externalizable;

import game.script.ship.speedBoost.SpeedBoost;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.acB  reason: case insensitive filesystem */
/* compiled from: a */
public class C5873acB extends C1506WA implements Externalizable {

    private final SpeedBoost.C2286b fcg;
    private final SpeedBoost fch;

    public C5873acB() {
        this.fcg = null;
        this.fch = null;
    }

    public C5873acB(SpeedBoost dcVar) {
        this.fcg = dcVar.biH();
        this.fch = dcVar;
    }

    public SpeedBoost.C2286b bPb() {
        return this.fcg;
    }

    public SpeedBoost bPc() {
        return this.fch;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
