package game.network.message.externalizable;

import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.C0474GZ;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.res.html.C6663arL;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.KO */
/* compiled from: a */
public final class C0722KO implements IExternalIO, Externalizable {

    private Class<? extends C1616Xf> agS;
    private C0677Jd dqR;
    private transient C6663arL infra = null;

    /* renamed from: qT */
    private ObjectId f949qT;

    public C0722KO() {
    }

    public C0722KO(ObjectId apo, C0677Jd jd, Class<? extends C1616Xf> cls) {
        this.f949qT = apo;
        this.dqR = jd;
        this.agS = cls;
        this.infra = jd.mo11944yn().bFf();
    }

    public C6663arL bdg() {
        return this.infra;
    }

    /* renamed from: a */
    public void mo3510a(C6663arL arl) {
        this.infra = arl;
    }

    /* renamed from: hC */
    public ObjectId mo3514hC() {
        return this.f949qT;
    }

    public C0677Jd bdh() {
        return this.dqR;
    }

    /* renamed from: hD */
    public Class<? extends C1616Xf> mo3515hD() {
        return this.agS;
    }

    public String toString() {
        return "TransactionChange " + this.agS.getName() + ":" + this.f949qT + " " + this.dqR;
    }

    /* renamed from: d */
    public void mo3513d(C0677Jd jd) {
        this.dqR = jd;
    }

    public void readExternal(IReadExternal vm) {
        C1616Xf xf = (C1616Xf) C0474GZ.dah.mo2357a(vm);
        this.f949qT = xf.bFf().getObjectId();
        this.agS = xf.getClass();
        this.dqR = (C0677Jd) xf.mo13W();
        this.dqR.mo3192d(vm);
    }

    public void writeExternal(IWriteExternal att) {
        C0474GZ.dah.serializeData(att, (Object) this.dqR.mo11944yn());
        this.dqR.mo3157a(att, 16);
    }

    public void readExternal(ObjectInput objectInput) {
        C1616Xf xf = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        this.f949qT = xf.bFf().getObjectId();
        this.agS = xf.getClass();
        this.dqR = (C0677Jd) xf.mo13W();
        this.dqR.mo3204i(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        C0474GZ.dah.serializeData(objectOutput, (Object) this.dqR.mo11944yn());
        this.dqR.mo3159a(objectOutput, 65552);
    }
}
