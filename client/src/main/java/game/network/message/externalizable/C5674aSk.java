package game.network.message.externalizable;

import game.script.player.Player;
import game.script.support.Ticket;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aSk  reason: case insensitive filesystem */
/* compiled from: a */
public class C5674aSk extends C1506WA implements Externalizable {

    private Ticket asm;
    private Player iMx;
    private String text;

    public C5674aSk() {
    }

    public C5674aSk(Ticket fLVar, Player aku, String str) {
        this.asm = fLVar;
        this.iMx = aku;
        this.text = str;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String str) {
        this.text = str;
    }

    /* renamed from: Jr */
    public Ticket mo11410Jr() {
        return this.asm;
    }

    /* renamed from: a */
    public void mo11411a(Ticket fLVar) {
        this.asm = fLVar;
    }

    public Player due() {
        return this.iMx;
    }

    /* renamed from: cC */
    public void mo11412cC(Player aku) {
        this.iMx = aku;
    }

    public void readExternal(ObjectInput objectInput) {
        this.text = (String) objectInput.readObject();
        this.asm = (Ticket) objectInput.readObject();
        this.iMx = (Player) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.text);
        objectOutput.writeObject(this.asm);
        objectOutput.writeObject(this.iMx);
    }
}
