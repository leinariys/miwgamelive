package game.network.message.externalizable;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.serializable.C6400amI;
import p001a.C1003Om;
import p001a.C3735uM;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.GR */
/* compiled from: a */
public class C0465GR implements Externalizable {
    public static final int cZJ = 1;
    public static final int cZK = 2;
    public static final int cZL = 4;
    public static final int cZM = 16;
    public static final int cZN = 23;
    double cZO;
    double cZP;
    double cZQ;
    float cZR;
    float cZS;
    float cZT;
    float cZU;
    float cZV;
    float cZW;
    float cZX;
    float cZY;
    float cZZ;
    float daa;
    int mask;
    private Externalizable dab;

    public C0465GR() {
    }

    public C0465GR(int i, C1003Om om, Externalizable externalizable) {
        this.mask = i;
        this.dab = externalizable;
        if ((i & 1) != 0) {
            Vec3d blk = om.blk();
            this.cZO = blk.x;
            this.cZP = blk.y;
            this.cZQ = blk.z;
        }
        if ((i & 2) != 0) {
            Quat4fWrap bll = om.bll();
            this.cZR = bll.w;
            this.cZS = bll.x;
            this.cZT = bll.y;
            this.cZU = bll.z;
        }
        if ((i & 4) != 0) {
            Vec3f blm = om.blm();
            this.cZV = blm.x;
            this.cZW = blm.y;
            this.cZX = blm.z;
        }
        if ((i & 16) != 0) {
            Vec3f bln = om.bln();
            this.cZY = bln.x;
            this.cZZ = bln.y;
            this.daa = bln.z;
        }
    }

    public int getMask() {
        return this.mask;
    }

    public void readExternal(ObjectInput objectInput) {
        this.mask = objectInput.readByte();
        if ((this.mask & 1) != 0) {
            this.cZO = objectInput.readDouble();
            this.cZP = objectInput.readDouble();
            this.cZQ = objectInput.readDouble();
        }
        if ((this.mask & 2) != 0) {
            this.cZR = objectInput.readFloat();
            this.cZS = objectInput.readFloat();
            this.cZT = objectInput.readFloat();
            this.cZU = objectInput.readFloat();
        }
        if ((this.mask & 4) != 0) {
            this.cZV = objectInput.readFloat();
            this.cZW = objectInput.readFloat();
            this.cZX = objectInput.readFloat();
        }
        if ((this.mask & 16) != 0) {
            this.cZY = objectInput.readFloat();
            this.cZZ = objectInput.readFloat();
            this.daa = objectInput.readFloat();
        }
        if (objectInput.readBoolean()) {
            this.dab = (Externalizable) objectInput.readObject();
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeByte(this.mask);
        if ((this.mask & 1) != 0) {
            objectOutput.writeDouble(this.cZO);
            objectOutput.writeDouble(this.cZP);
            objectOutput.writeDouble(this.cZQ);
        }
        if ((this.mask & 2) != 0) {
            objectOutput.writeFloat(this.cZR);
            objectOutput.writeFloat(this.cZS);
            objectOutput.writeFloat(this.cZT);
            objectOutput.writeFloat(this.cZU);
        }
        if ((this.mask & 4) != 0) {
            objectOutput.writeFloat(this.cZV);
            objectOutput.writeFloat(this.cZW);
            objectOutput.writeFloat(this.cZX);
        }
        if ((this.mask & 16) != 0) {
            objectOutput.writeFloat(this.cZY);
            objectOutput.writeFloat(this.cZZ);
            objectOutput.writeFloat(this.daa);
        }
        objectOutput.writeBoolean(this.dab != null);
        if (this.dab != null) {
            objectOutput.writeObject(this.dab);
        }
    }

    /* renamed from: a */
    public void mo2341a(C6400amI ami, C3735uM uMVar) {
        if ((this.mask & 1) != 0) {
            Vec3d blk = ami.blk();
            blk.x = this.cZO;
            blk.y = this.cZP;
            blk.z = this.cZQ;
        }
        if ((this.mask & 2) != 0) {
            Quat4fWrap bll = ami.bll();
            bll.w = this.cZR;
            bll.x = this.cZS;
            bll.y = this.cZT;
            bll.z = this.cZU;
            ami.mo4490IQ().orientation.set(bll);
            ami.bll().mo15242f(bll);
        }
        if ((this.mask & 4) != 0) {
            Vec3f blm = ami.blm();
            blm.x = this.cZV;
            blm.y = this.cZW;
            blm.z = this.cZX;
            ami.mo4490IQ().orientation.transform(blm, ami.blq());
        }
        if ((this.mask & 16) != 0) {
            Vec3f bln = ami.bln();
            bln.set(this.cZY, this.cZZ, this.daa);
            ami.mo4490IQ().orientation.transform(bln, ami.blt());
        }
        if (this.dab != null && uMVar != null) {
            uMVar.mo3861a(this.dab);
        }
    }

    public String toString() {
        String str = "SolidStatePacketData[mask=" + this.mask;
        if ((this.mask & 1) != 0) {
            str = String.valueOf(str) + "; position=" + this.cZO + "," + this.cZP + "," + this.cZQ;
        }
        if ((this.mask & 2) != 0) {
            str = String.valueOf(str) + "; orientation=" + this.cZR + "," + this.cZS + "," + this.cZT + "," + this.cZU;
        }
        if ((this.mask & 4) != 0) {
            str = String.valueOf(str) + "; linearVelocity=" + this.cZV + "," + this.cZW + "," + this.cZX;
        }
        if ((this.mask & 16) != 0) {
            str = String.valueOf(str) + "; angularVelocity=" + this.cZY + "," + this.cZZ + "," + this.daa;
        }
        return String.valueOf(str) + "]";
    }
}
