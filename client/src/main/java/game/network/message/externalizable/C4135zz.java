package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.zz */
/* compiled from: a */
public class C4135zz extends C1506WA implements Externalizable {
    private C4136a bZC;
    private boolean bZD;

    public C4135zz(C4136a aVar, boolean z) {
        this.bZC = aVar;
        this.bZD = z;
    }

    public C4135zz() {
        this.bZC = C4136a.LAST;
        this.bZD = false;
    }

    public C4136a arT() {
        return this.bZC;
    }

    public boolean arU() {
        return this.bZD;
    }

    public void readExternal(ObjectInput objectInput) {
        this.bZC = (C4136a) objectInput.readObject();
        this.bZD = objectInput.readBoolean();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.bZC);
        objectOutput.writeBoolean(this.bZD);
    }

    /* renamed from: a.zz$a */
    public enum C4136a {
        LAST,
        DIRECTION_CHANGE,
        GO_TO,
        GO_TO_INTERACT,
        FOLLOW,
        ORBIT,
        LOST_TARGET
    }
}
