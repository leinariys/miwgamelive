package game.network.message.externalizable;

import game.script.player.Player;
import logic.aaa.C1506WA;
import taikodom.infra.script.I18NString;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.kG */
/* compiled from: a */
public class C2797kG extends C1506WA implements Externalizable {
    private Player auM;
    private Player auN;
    private C1368Tv.C1369a auO;
    private I18NString auP;
    private Object[] auQ;

    public C2797kG() {
    }

    public C2797kG(Player aku, Player aku2, C1368Tv.C1369a aVar, I18NString i18NString, Object... objArr) {
        this.auM = aku2;
        this.auN = aku;
        this.auO = aVar;
        this.auP = i18NString;
        this.auQ = objArr;
    }

    /* renamed from: KH */
    public C1368Tv.C1369a mo20031KH() {
        return this.auO;
    }

    /* renamed from: KI */
    public I18NString mo20032KI() {
        return this.auP;
    }

    /* renamed from: KJ */
    public Player mo20033KJ() {
        return this.auN;
    }

    /* renamed from: KK */
    public Player mo20034KK() {
        return this.auM;
    }

    /* renamed from: KL */
    public Object[] mo20035KL() {
        return this.auQ;
    }

    public void readExternal(ObjectInput objectInput) {
        this.auN = (Player) objectInput.readObject();
        this.auM = (Player) objectInput.readObject();
        this.auO = C1368Tv.C1369a.values()[objectInput.readInt()];
        this.auP = (I18NString) objectInput.readObject();
        this.auQ = (Object[]) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.auN);
        objectOutput.writeObject(this.auM);
        objectOutput.writeInt(this.auO.ordinal());
        objectOutput.writeObject(this.auP);
        objectOutput.writeObject(this.auQ);
    }
}
