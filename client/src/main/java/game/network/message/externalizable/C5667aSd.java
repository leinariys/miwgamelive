package game.network.message.externalizable;

import logic.aaa.C1506WA;
import p001a.C2776jq;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aSd  reason: case insensitive filesystem */
/* compiled from: a */
public class C5667aSd extends C1506WA implements Externalizable {

    private boolean aBE;
    private String iLU;
    private C2776jq iLV;

    public C5667aSd() {
    }

    public C5667aSd(String str, boolean z) {
        this.iLU = str;
        this.aBE = z;
    }

    public C5667aSd(String str, C2776jq jqVar, boolean z) {
        this.iLU = str;
        this.iLV = jqVar;
        this.aBE = z;
    }

    public boolean isPressed() {
        return this.aBE;
    }

    public String duc() {
        return this.iLU;
    }

    public void readExternal(ObjectInput objectInput) {
        this.iLU = (String) objectInput.readObject();
        this.aBE = objectInput.readBoolean();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.iLU);
        objectOutput.writeBoolean(this.aBE);
    }

    public String toString() {
        return "Command:  " + this.iLU + " " + this.aBE;
    }

    public C2776jq dud() {
        return this.iLV;
    }
}
