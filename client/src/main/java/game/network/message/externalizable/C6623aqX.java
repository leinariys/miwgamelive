package game.network.message.externalizable;

import logic.aaa.C1506WA;
import taikodom.infra.script.I18NString;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aqX  reason: case insensitive filesystem */
/* compiled from: a */
public class C6623aqX extends C1506WA implements Externalizable {

    private I18NString auP;
    private Object[] dmZ;
    private C0939Nn.C0940a grl;

    public C6623aqX() {
    }

    public C6623aqX(C0939Nn.C0940a aVar, I18NString i18NString, Object... objArr) {
        this.grl = aVar;
        this.auP = i18NString;
        this.dmZ = objArr;
    }

    public C0939Nn.C0940a crM() {
        return this.grl;
    }

    /* renamed from: KI */
    public I18NString mo15606KI() {
        return this.auP;
    }

    public Object[] getParams() {
        return this.dmZ;
    }

    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.auP = (I18NString) objectInput.readObject();
        this.dmZ = (Object[]) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeObject(this.auP);
        objectOutput.writeObject(this.dmZ);
    }
}
