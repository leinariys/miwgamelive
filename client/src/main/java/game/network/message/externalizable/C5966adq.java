package game.network.message.externalizable;

import game.script.item.Component;
import game.script.item.Item;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.adq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5966adq extends C1506WA implements Externalizable {

    private Component ciQ;
    private Item fhv;

    public C5966adq() {
    }

    public C5966adq(Component abl, Item auq) {
        this.fhv = auq;
        this.ciQ = abl;
    }

    public Component bHV() {
        return this.ciQ;
    }

    public Item bRf() {
        return this.fhv;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
