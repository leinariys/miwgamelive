package game.network.message.externalizable;

import game.network.message.Blocking;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aTe  reason: case insensitive filesystem */
/* compiled from: a */
public class C5694aTe extends Blocking implements Externalizable {

    private Object iOo;
    private String str;

    public C5694aTe() {
    }

    public C5694aTe(String str2, Object obj) {
        this.str = str2;
        this.iOo = obj;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeUTF(this.str != null ? this.str : "");
        objectOutput.writeObject(this.iOo);
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.str = objectInput.readUTF();
        this.iOo = objectInput.readObject();
    }

    public Object dvz() {
        return this.iOo;
    }

    public String getStr() {
        return this.str;
    }
}
