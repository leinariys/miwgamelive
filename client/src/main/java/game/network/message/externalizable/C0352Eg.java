package game.network.message.externalizable;

import game.script.player.Player;
import logic.aaa.C1506WA;
import p001a.C5956adg;
import taikodom.infra.script.I18NString;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Eg */
/* compiled from: a */
public class C0352Eg extends C1506WA implements Externalizable {
    private I18NString cRw;
    private Object[] cRx;

    public C0352Eg() {
    }

    public C0352Eg(Player aku, I18NString i18NString, Object... objArr) {
        this.cRw = i18NString;
        this.cRx = objArr;
    }

    public String getMessage() {
        if (this.cRw != null) {
            return C5956adg.format(this.cRw.get(), this.cRx);
        }
        return "Missing LD content";
    }

    public void readExternal(ObjectInput objectInput) {
        this.cRw = (I18NString) objectInput.readObject();
        this.cRx = (Object[]) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.cRw);
        objectOutput.writeObject(this.cRx);
    }
}
