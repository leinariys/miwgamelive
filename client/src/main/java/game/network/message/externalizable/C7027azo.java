package game.network.message.externalizable;

import game.network.message.C0474GZ;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.azo  reason: case insensitive filesystem */
/* compiled from: a */
public class C7027azo extends C5581aOv implements Externalizable {


    /* renamed from: Uk */
    private C1616Xf f5703Uk;
    private byte gYL;

    public C7027azo() {
    }

    public C7027azo(C1616Xf xf, int i) {
        this.f5703Uk = xf;
        this.gYL = (byte) i;
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.f5703Uk;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f5703Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        this.gYL = objectInput.readByte();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f5703Uk);
        objectOutput.writeByte(cFD());
    }

    public byte cFD() {
        return this.gYL;
    }
}
