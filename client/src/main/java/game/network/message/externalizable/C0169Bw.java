package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.C0474GZ;
import logic.baa.C1616Xf;
import p001a.C1875af;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Bw */
/* compiled from: a */
public class C0169Bw extends Blocking implements Externalizable {


    /* renamed from: Uk */
    private C1616Xf f261Uk;
    private long cmb;

    public C0169Bw() {
    }

    public C0169Bw(C1616Xf xf, long j) {
        this.f261Uk = xf;
        this.cmb = j;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f261Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        this.cmb = objectInput.readLong();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f261Uk);
        objectOutput.writeLong(this.cmb);
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        ((C1875af) this.f261Uk.bFf()).mo13207a(bVar, this.cmb);
        return null;
    }
}
