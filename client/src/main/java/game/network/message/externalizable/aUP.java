package game.network.message.externalizable;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.AbstractList;

/* renamed from: a.aUP */
/* compiled from: a */
public class aUP<T> extends AbstractList<T> implements Externalizable {

    private T iXJ;

    public aUP() {
    }

    public aUP(T t) {
        this.iXJ = t;
    }

    public T get(int i) {
        if (i == 0) {
            return this.iXJ;
        }
        throw new IndexOutOfBoundsException(String.valueOf(i));
    }

    public int size() {
        return 1;
    }

    public void readExternal(ObjectInput objectInput) {
        this.iXJ = objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.iXJ);
    }
}
