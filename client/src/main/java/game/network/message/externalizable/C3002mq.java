package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.mq */
/* compiled from: a */
public class C3002mq extends C1506WA implements Externalizable {
    public static final short bti = 0;
    public static final short btj = 1;
    private short btk;
    private C3003a btl;
    private boolean btm;

    public C3002mq() {
    }

    public C3002mq(short s) {
        this(s, (C3003a) null);
    }

    public C3002mq(short s, C3003a aVar, boolean z) {
        this.btk = s;
        this.btl = aVar;
        this.btm = z;
    }

    public C3002mq(short s, C3003a aVar) {
        this(s, aVar, false);
    }

    public short aiH() {
        return this.btk;
    }

    public C3003a aiI() {
        return this.btl;
    }

    public boolean aiJ() {
        return this.btm;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Don't use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Don't use this class serverside");
    }

    /* renamed from: a.mq$a */
    public interface C3003a {
        void execute();
    }
}
