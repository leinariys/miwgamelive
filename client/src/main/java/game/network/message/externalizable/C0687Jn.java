package game.network.message.externalizable;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Jn */
/* compiled from: a */
public class C0687Jn extends C1506WA implements Externalizable {

    public Vec3d djt;
    public Vec3f relativePosition;

    public C0687Jn() {
    }

    public C0687Jn(Vec3f vec3f, Vec3d ajr) {
        this.relativePosition = vec3f;
        this.djt = ajr;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
