package game.network.message.externalizable;

import game.network.message.C0474GZ;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Bl */
/* compiled from: a */
public class C0152Bl extends C5581aOv implements Externalizable {

    byte cli;
    /* renamed from: Uk */
    private C1616Xf f250Uk;

    public C0152Bl() {
    }

    public C0152Bl(C1616Xf xf, byte b) {
        this.f250Uk = xf;
        this.cli = b;
    }

    public int ayq() {
        return this.cli;
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.f250Uk;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f250Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        this.cli = objectInput.readByte();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f250Uk);
        objectOutput.writeByte(this.cli);
    }
}
