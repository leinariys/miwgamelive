package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.C0474GZ;
import game.script.simulation.Space;
import p001a.C1355Tk;
import p001a.C6729asZ;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collection;

/* renamed from: a.vr */
/* compiled from: a */
public class C3856vr extends Blocking implements Externalizable {

    private Collection<C6950awp> bze;

    /* renamed from: rL */
    private Space f9437rL;

    public C3856vr() {
    }

    public C3856vr(Space ea, Collection<C6950awp> collection) {
        this.f9437rL = ea;
        this.bze = collection;
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f9437rL);
        try {
            objectOutput.writeInt(this.bze.size());
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (C6950awp writeObject : this.bze) {
            try {
                objectOutput.writeObject(writeObject);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f9437rL = (Space) C0474GZ.dah.inputReadObject(objectInput);
        int readInt = 0;
        try {
            readInt = objectInput.readInt();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.bze = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            try {
                this.bze.add((C6950awp) objectInput.readObject());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        C1355Tk cKn = this.f9437rL.cKn();
        if (cKn != null) {
            cKn.mo13317s(this.bze);
            return null;
        }
        for (Object awp : new ArrayList(this.bze)) {
            if (awp instanceof C6729asZ) {
                this.bze.remove(awp);
            }
        }
        if (!this.f9437rL.bGX() || this.bze.size() <= 0) {
            return null;
        }
        this.f9437rL.mo1897b(this);
        return null;
    }
}
