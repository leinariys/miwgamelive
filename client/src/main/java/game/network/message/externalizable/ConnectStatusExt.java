package game.network.message.externalizable;

import game.network.channel.Connect;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aQP */
/* compiled from: a */
public class ConnectStatusExt implements Externalizable {
    private boolean iFB;
    private Connect.Status iFC;

    public ConnectStatusExt() {
    }

    public ConnectStatusExt(boolean z, Connect.Status aVar) {
        this.iFB = z;
        this.iFC = aVar;
    }

    public Connect.Status dqe() {
        return this.iFC;
    }

    public boolean dqf() {
        return this.iFB;
    }

    public void readExternal(ObjectInput objectInput) {
        this.iFB = objectInput.readBoolean();
        if (!this.iFB) {
            this.iFC = Connect.Status.values()[objectInput.readByte()];
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.iFB);
        if (!this.iFB) {
            objectOutput.writeByte(this.iFC.ordinal());
        }
    }
}
