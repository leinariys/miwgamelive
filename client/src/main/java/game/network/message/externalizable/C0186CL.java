package game.network.message.externalizable;

import game.script.player.Player;
import game.script.ship.Ship;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.CL */
/* compiled from: a */
public class C0186CL extends C1506WA implements Externalizable {

    /* renamed from: P */
    private Player f282P;
    private Ship aOO;

    public C0186CL() {
    }

    public C0186CL(Player aku, Ship fAVar) {
        this.f282P = aku;
        this.aOO = fAVar;
    }

    /* renamed from: dL */
    public Player mo954dL() {
        return this.f282P;
    }

    /* renamed from: al */
    public Ship mo953al() {
        return this.aOO;
    }

    public void readExternal(ObjectInput objectInput) {
        this.f282P = (Player) objectInput.readObject();
        this.aOO = (Ship) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.f282P);
        objectOutput.writeObject(this.aOO);
    }
}
