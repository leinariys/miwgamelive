package game.network.message.externalizable;

import game.script.Actor;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.apI  reason: case insensitive filesystem */
/* compiled from: a */
public class C6556apI extends C1506WA implements Externalizable {

    /* renamed from: Lo */
    public Actor f5091Lo;
    public float gnh;

    public C6556apI() {
    }

    public C6556apI(float f, Actor cr) {
        this.gnh = f;
        this.f5091Lo = cr;
    }

    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.gnh = objectInput.readFloat();
        this.f5091Lo = (Actor) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeFloat(this.gnh);
        objectOutput.writeObject(this.f5091Lo);
    }
}
