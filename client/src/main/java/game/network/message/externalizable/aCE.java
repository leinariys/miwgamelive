package game.network.message.externalizable;

import game.network.message.C0474GZ;
import logic.baa.C1616Xf;
import logic.res.html.C2491fm;
import logic.res.html.DataClassSerializer;
import p001a.C0495Gr;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;

/* renamed from: a.aCE */
/* compiled from: a */
public final class aCE implements C0495Gr, Externalizable {

    private transient C2491fm aVR;
    private transient Object[] args;
    private transient C1616Xf hwA;
    private transient Object[] hwB;

    public aCE() {
    }

    public aCE(C1616Xf xf, C2491fm fmVar) {
        this.hwA = xf;
        this.aVR = fmVar;
        this.args = null;
    }

    public aCE(C1616Xf xf, C2491fm fmVar, Object[] objArr) {
        this.hwA = xf;
        this.aVR = fmVar;
        this.args = objArr;
    }

    public Object[] getArgs() {
        return this.args;
    }

    public C2491fm aQK() {
        return this.aVR;
    }

    public C1616Xf aQL() {
        return this.hwA;
    }

    public String toString() {
        return "MethodInvoker[name=" + this.aVR.name() + ", args=" + Arrays.toString(getArgs()) + "]";
    }

    public void writeExternal(ObjectOutput objectOutput) {
        Object[] objArr;
        C0474GZ.dah.serializeData(objectOutput, (Object) this.hwA);
        DataClassSerializer.m6486a(objectOutput, this.hwA.mo12U().length, this.aVR.mo7395hq());
        DataClassSerializer[] pM = this.aVR.mo4767pM();
        if (this.hwB == null) {
            objArr = this.args;
        } else {
            objArr = this.hwB;
        }
        for (int i = 0; i < pM.length; i++) {
            pM[i].serializeData(objectOutput, objArr[i]);
        }
    }

    public void readExternal(ObjectInput objectInput) {
        this.hwA = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        this.aVR = this.hwA.mo12U()[DataClassSerializer.m6487b(objectInput, this.hwA.mo12U().length)];
        DataClassSerializer[] pM = this.aVR.mo4767pM();
        Object[] objArr = new Object[pM.length];
        this.args = objArr;
        int i = 0;
        while (i < pM.length) {
            try {
                objArr[i] = pM[i].inputReadObject(objectInput);
                i++;
            } catch (RuntimeException e) {
                throw new RuntimeException("Error reading parameters of " + this.hwA.getClass() + ":" + this.hwA.bFf().getObjectId() + " " + this.aVR.mo4752hD() + "." + this.aVR.name() + ":" + this.aVR.mo7395hq() + " argument " + i + " : " + this.aVR.getParameterTypes()[i], e);
            } catch (IOException e2) {
                throw new RuntimeException("Error reading parameters of " + this.hwA.getClass() + ":" + this.hwA.bFf().getObjectId() + " " + this.aVR.mo4752hD() + "." + this.aVR.name() + ":" + this.aVR.mo7395hq() + " argument " + i + " : " + this.aVR.getParameterTypes()[i], e2);
            }
        }
    }

    public Object[] aQM() {
        if (this.hwB == null && this.args != null) {
            this.hwB = new Object[this.args.length];
            System.arraycopy(this.args, 0, this.hwB, 0, this.args.length);
        }
        return this.hwB;
    }

    /* renamed from: l */
    public void mo2418l(Object[] objArr) {
        this.hwB = objArr;
    }

    /* renamed from: m */
    public void mo7998m(C1616Xf xf) {
        this.hwA = xf;
    }

    /* renamed from: hq */
    public int mo2417hq() {
        return this.aVR.mo7395hq();
    }
}
