package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aNJ */
/* compiled from: a */
public class aNJ extends C1506WA implements Externalizable {
    public String message;

    public aNJ() {
    }

    public aNJ(String str) {
        this.message = str;
    }

    public aNJ(String str, Object... objArr) {
        this.message = String.format(str, objArr);
    }

    public void readExternal(ObjectInput objectInput) {
        this.message = objectInput.readUTF();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeUTF(this.message);
    }
}
