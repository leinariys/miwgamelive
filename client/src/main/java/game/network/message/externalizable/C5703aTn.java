package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;
import p001a.C0495Gr;
import p001a.C3582se;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aTn  reason: case insensitive filesystem */
/* compiled from: a */
public class C5703aTn extends C5581aOv implements Externalizable {

    private aCE iOx;

    public C5703aTn() {
    }

    public C5703aTn(C0495Gr gr) {
        this.iOx = (aCE) gr;
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.iOx.aQL();
    }

    public C0495Gr dvF() {
        return this.iOx;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.iOx = new aCE();
        this.iOx.readExternal(objectInput);
        this.iOx.aQK().mo4775pU();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        this.iOx.aQK().mo4776pV();
        super.writeExternal(objectOutput);
        this.iOx.writeExternal(objectOutput);
    }

    public String toString() {
        return String.valueOf(getClass().getName()) + "-" + this.iOx;
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        return ((C3582se) this.iOx.aQL().bFf()).mo5593a(bVar, this);
    }
}
