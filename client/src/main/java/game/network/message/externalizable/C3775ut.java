package game.network.message.externalizable;

import game.network.message.Blocking;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.ut */
/* compiled from: a */
public class C3775ut extends Blocking implements Externalizable {


    /* renamed from: PZ */
    private long f9375PZ = 0;

    /* renamed from: Qa */
    private long f9376Qa = 0;

    /* renamed from: Tm */
    private long f9377Tm = 0;

    public C3775ut() {
    }

    public C3775ut(long j, long j2, long j3) {
        this.f9377Tm = j;
        this.f9375PZ = j2;
        this.f9376Qa = j3;
    }

    /* renamed from: xU */
    public long mo22464xU() {
        return this.f9377Tm;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f9377Tm = objectInput.readLong();
        this.f9375PZ = objectInput.readLong();
        this.f9376Qa = objectInput.readLong();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeLong(this.f9377Tm);
        objectOutput.writeLong(this.f9375PZ);
        objectOutput.writeLong(this.f9376Qa);
    }

    /* renamed from: vK */
    public long mo22462vK() {
        return this.f9375PZ;
    }

    /* renamed from: vL */
    public long mo22463vL() {
        return this.f9376Qa;
    }
}
