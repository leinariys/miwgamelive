package game.network.message.externalizable;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.message.serializable.C1662YU;
import game.network.message.serializable.C3438ra;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import logic.res.html.C1867ad;
import logic.res.html.DataClassSerializer;
import logic.ui.item.ListJ;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.ListIterator;

/* renamed from: a.qK */
/* compiled from: a */
public class C3328qK<T> extends C2631hn<T> implements C3438ra<T>, Externalizable {

    public ListJ<T> aWo;

    public C3328qK() {
    }

    public C3328qK(C1616Xf xf, C5663aRz arz) {
        super(xf, arz);
    }

    public C3328qK(C1616Xf xf, C5663aRz arz, ListJ<T> list) {
        super(xf, arz);
        if (list != null && list.size() > 0) {
            this.aWo = new ArrayList(list);
        }
    }

    public Collection<T> getCollection() {
        if (this.aWo != null) {
            return this.aWo;
        }
        ArrayList arrayList = new ArrayList(0);
        this.aWo = arrayList;
        return arrayList;
    }

    public void add(int i, T t) {
        if (!this.f8051Uk.bFf().mo6887du()) {
            throw new IllegalStateException("Illegal outside transaction");
        }
    }

    public boolean addAll(int i, Collection<? extends T> collection) {
        if (this.f8051Uk.bFf().mo6887du()) {
            return false;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public T get(int i) {
        return mo21318XD().get(i);
    }

    public int indexOf(Object obj) {
        if (this.aWo == null) {
            return -1;
        }
        return this.aWo.indexOf(obj);
    }

    public int lastIndexOf(Object obj) {
        if (this.aWo == null) {
            return -1;
        }
        return this.aWo.lastIndexOf(obj);
    }

    public ListIterator<T> listIterator() {
        if (this.aWo == null) {
            return Collections.emptyList().listIterator();
        }
        return Collections.unmodifiableList(mo21318XD()).listIterator();
    }

    public ListIterator<T> listIterator(int i) {
        return Collections.unmodifiableList(mo21318XD()).listIterator(i);
    }

    public T set(int i, T t) {
        if (this.f8051Uk.bFf().mo6887du()) {
            return null;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public T remove(int i) {
        if (this.f8051Uk.bFf().mo6887du()) {
            return null;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public ListJ<T> subList(int i, int i2) {
        return mo21318XD().subList(i, i2);
    }

    /* renamed from: j */
    public void mo19375j(Collection<T> collection) {
        clear();
        addAll(collection);
    }

    /* renamed from: XC */
    public C1867ad<T> mo271XC() {
        return new C1662YU(mo21318XD());
    }

    /* renamed from: XD */
    public ListJ<T> mo21318XD() {
        if (this.aWo != null) {
            return this.aWo;
        }
        ArrayList arrayList = new ArrayList(0);
        this.aWo = arrayList;
        return arrayList;
    }

    public Object clone() {
        return new C3328qK(this.f8051Uk, mo19378ym(), this.aWo);
    }

    public int size() {
        if (this.aWo == null) {
            return 0;
        }
        return super.size();
    }

    public boolean contains(Object obj) {
        if (this.aWo == null) {
            return false;
        }
        return super.contains(obj);
    }

    public boolean remove(Object obj) {
        if (this.aWo == null) {
            return false;
        }
        return super.remove(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        if (this.aWo == null) {
            return false;
        }
        return super.removeAll(collection);
    }

    public boolean containsAll(Collection<?> collection) {
        if (this.aWo == null) {
            return false;
        }
        return super.containsAll(collection);
    }

    /* renamed from: XE */
    public Object mo21319XE() {
        return new C3328qK(this.f8051Uk, mo19378ym(), this.aWo);
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        int readInt = objectInput.readInt();
        DataClassSerializer kd = this.f8053Um;
        if (readInt > 0) {
            this.aWo = new ArrayList(readInt);
            for (int i = 0; i < readInt; i++) {
                this.aWo.add(kd.inputReadObject(objectInput));
            }
        }
    }

    public void readExternal(IReadExternal vm) {
        int gX = vm.readInt("count");
        if (gX > 0) {
            this.aWo = new ArrayList(gX);
            for (int i = 0; i < gX; i++) {
                this.aWo.add(vm.mo6366hd((String) null));
            }
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        if (this.aWo != null) {
            int size = this.aWo.size();
            objectOutput.writeInt(size);
            DataClassSerializer kd = this.f8053Um;
            for (int i = 0; i < size; i++) {
                kd.serializeData(objectOutput, (Object) this.aWo.get(i));
            }
            return;
        }
        objectOutput.writeInt(0);
    }

    public void writeExternal(IWriteExternal att) {
        if (this.aWo != null) {
            int size = this.aWo.size();
            att.writeInt("count", size);
            for (int i = 0; i < size; i++) {
                att.mo16273g((String) null, (Object) this.aWo.get(i));
            }
            return;
        }
        att.writeInt("count", 0);
    }

    /* renamed from: c */
    public void mo286c(ObjectInput objectInput) {
        super.mo286c(objectInput);
        int readInt = objectInput.readInt();
        DataClassSerializer kd = this.f8053Um;
        if (readInt > 0) {
            this.aWo = new ArrayList(readInt);
            for (int i = 0; i < readInt; i++) {
                this.aWo.add(kd.inputReadObject(objectInput));
            }
        }
    }

    /* renamed from: a */
    public void mo284a(ObjectOutput objectOutput) {
        super.mo284a(objectOutput);
        if (this.aWo != null) {
            int size = this.aWo.size();
            objectOutput.writeInt(size);
            DataClassSerializer kd = this.f8053Um;
            for (int i = 0; i < size; i++) {
                kd.serializeData(objectOutput, (Object) this.aWo.get(i));
            }
            return;
        }
        objectOutput.writeInt(0);
    }

    public boolean equals(Object obj) {
        if (obj instanceof C3328qK) {
            if (this.aWo != null) {
                return this.aWo.equals(((C3328qK) obj).mo21318XD());
            }
            if (((C3328qK) obj).mo21318XD() == null) {
                return true;
            }
            return ((C3328qK) obj).mo21318XD().size() == 0;
        } else if (this.aWo == null) {
            return obj == null;
        } else {
            return this.aWo.equals(obj);
        }
    }
}
