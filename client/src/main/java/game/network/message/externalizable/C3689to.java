package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.to */
/* compiled from: a */
public class C3689to extends C1506WA implements Externalizable {

    private boolean bme;
    private C3690a bmf;

    public C3689to() {
        this(C3690a.ESCAPE_CALL);
    }

    public C3689to(C3690a aVar) {
        this.bme = false;
        this.bmf = aVar;
    }

    public void adC() {
        this.bme = true;
    }

    public boolean adD() {
        return this.bme;
    }

    public C3690a adE() {
        return this.bmf;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Не используйте этот класс на стороне сервера");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Не используйте этот класс на стороне сервера");
    }

    /* renamed from: a.to$a */
    public enum C3690a {
        ESCAPE_CALL,
        SHIP_LAUNCH
    }
}
