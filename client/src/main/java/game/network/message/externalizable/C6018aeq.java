package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aeq  reason: case insensitive filesystem */
/* compiled from: a */
public class C6018aeq extends C1506WA implements Externalizable {

    private String command;
    private String param;

    public C6018aeq() {
    }

    public C6018aeq(String str, String str2) {
        this.command = str;
        this.param = str2;
    }

    public String getEvent() {
        return this.command;
    }

    public String bUu() {
        return this.param;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r3) {
        /*
            r2 = this;
            r0 = 0
            boolean r1 = r3 instanceof game.network.message.externalizable.C6018aeq
            if (r1 == 0) goto L_0x0020
            a.aeq r3 = (game.network.message.externalizable.C6018aeq) r3
            java.lang.String r1 = r2.param
            if (r1 == 0) goto L_0x0021
            java.lang.String r1 = r3.param
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0020
        L_0x0013:
            java.lang.String r1 = r2.command
            if (r1 == 0) goto L_0x0026
            java.lang.String r1 = r3.command
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0020
        L_0x001f:
            r0 = 1
        L_0x0020:
            return r0
        L_0x0021:
            java.lang.String r1 = r3.param
            if (r1 != 0) goto L_0x0020
            goto L_0x0013
        L_0x0026:
            java.lang.String r1 = r3.command
            if (r1 != 0) goto L_0x0020
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: game.network.message.externalizable.C6018aeq.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.param != null ? this.param.hashCode() : 0) * 31;
        if (this.command != null) {
            i = this.command.hashCode();
        }
        return hashCode + i;
    }

    public void readExternal(ObjectInput objectInput) {
        this.command = (String) objectInput.readObject();
        this.param = (String) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.command);
        objectOutput.writeObject(this.param);
    }
}
