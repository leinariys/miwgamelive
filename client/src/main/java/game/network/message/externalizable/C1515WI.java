package game.network.message.externalizable;

import game.script.item.Weapon;
import logic.aaa.C1506WA;
import p001a.C3904wY;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.WI */
/* compiled from: a */
public class C1515WI extends C1506WA implements Externalizable {


    /* renamed from: NX */
    private C3904wY f1990NX;
    private boolean bZD;

    /* renamed from: oX */
    private Weapon f1991oX;

    public C1515WI() {
    }

    public C1515WI(boolean z, C3904wY wYVar, Weapon adv) {
        this.bZD = z;
        this.f1990NX = wYVar;
        this.f1991oX = adv;
    }

    public boolean arU() {
        return this.bZD;
    }

    public C3904wY bDI() {
        if (this.f1990NX != null || this.f1991oX == null) {
            return this.f1990NX;
        }
        return this.f1991oX.mo7860sM();
    }

    /* renamed from: gB */
    public Weapon mo6501gB() {
        return this.f1991oX;
    }

    public void readExternal(ObjectInput objectInput) {
        this.bZD = objectInput.readBoolean();
        this.f1991oX = (Weapon) objectInput.readObject();
        int readInt = objectInput.readInt();
        if (readInt < C3904wY.values().length) {
            this.f1990NX = C3904wY.values()[readInt];
        } else {
            this.f1990NX = null;
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.bZD);
        objectOutput.writeObject(this.f1991oX);
        objectOutput.writeInt(this.f1990NX.ordinal());
    }
}
