package game.network.message.externalizable;

import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aSC */
/* compiled from: a */
public class aSC extends C1506WA implements Externalizable {

    /* renamed from: OV */
    private ShipType f3731OV;
    private MissionTemplate baM;
    private WeaponType eUZ;

    /* renamed from: iH */
    private Station f3732iH;

    public aSC() {
    }

    public aSC(Station bf, MissionTemplate avh, ShipType ng, WeaponType apt) {
        this.f3732iH = bf;
        this.baM = avh;
        this.f3731OV = ng;
        this.eUZ = apt;
    }

    /* renamed from: eT */
    public Station mo11331eT() {
        return this.f3732iH;
    }

    public MissionTemplate bfA() {
        return this.baM;
    }

    /* renamed from: uX */
    public ShipType mo11333uX() {
        return this.f3731OV;
    }

    public WeaponType duK() {
        return this.eUZ;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
