package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.C0474GZ;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;
import p001a.C3582se;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;

/* renamed from: a.Is */
/* compiled from: a */
public class C0626Is extends C5581aOv implements Externalizable {


    /* renamed from: Uk */
    private C1616Xf f739Uk;
    private C0500Gw dfA;
    private C6212aic dfz;

    /* renamed from: id */
    private int f740id;

    public C0626Is() {
    }

    public C0626Is(C1616Xf xf, int i, Object obj, List<Blocking> list) {
        this(xf, i, obj, (Throwable) null, list);
    }

    public C0626Is(C1616Xf xf, int i, Object obj, Throwable th, List<Blocking> list) {
        this.f740id = i;
        if (th != null) {
            this.dfA = new C0500Gw(th);
        } else {
            this.dfz = new C6212aic(obj, list);
        }
        this.f739Uk = xf;
    }

    public int getId() {
        return this.f740id;
    }

    public void readExternal(ObjectInput objectInput) {
        this.f740id = objectInput.readInt();
        this.f739Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        if (objectInput.readBoolean()) {
            this.dfz = new C6212aic();
            this.dfz.readExternal(objectInput);
        } else {
            this.dfA = new C0500Gw();
            this.dfA.readExternal(objectInput);
        }
        super.readExternal(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeInt(this.f740id);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f739Uk);
        if (this.dfz != null) {
            objectOutput.writeBoolean(true);
            this.dfz.writeExternal(objectOutput);
        } else {
            objectOutput.writeBoolean(false);
            this.dfA.writeExternal(objectOutput);
        }
        super.writeExternal(objectOutput);
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.f739Uk;
    }

    public Object getResult() {
        if (this.dfz != null) {
            return this.dfz.getResult();
        }
        return null;
    }

    public Blocking[] aWo() {
        if (this.dfz != null) {
            return this.dfz.aWo();
        }
        return null;
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        ((C3582se) this.f739Uk.bFf()).mo5605c(bVar, this);
        return null;
    }

    public Throwable getException() {
        if (this.dfA != null) {
            return this.dfA.getCause();
        }
        return null;
    }
}
