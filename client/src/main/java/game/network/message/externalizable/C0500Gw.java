package game.network.message.externalizable;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Gw */
/* compiled from: a */
public class C0500Gw extends C6302akO {

    public static int cYS = 1;
    private int cYR;
    private Throwable cause;

    public C0500Gw() {
    }

    public C0500Gw(int i) {
        this.cYR = i;
    }

    public C0500Gw(Throwable th) {
        this.cause = th;
    }

    public Throwable getCause() {
        return this.cause;
    }

    public void readExternal(ObjectInput objectInput) {
        this.cYR = objectInput.readInt();
        if (objectInput.readBoolean()) {
            this.cause = (Throwable) objectInput.readObject();
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeInt(this.cYR);
        if (this.cause != null) {
            objectOutput.writeBoolean(true);
            objectOutput.writeObject(this.cause);
            return;
        }
        objectOutput.writeBoolean(false);
    }

    public int aQW() {
        return this.cYR;
    }

    /* renamed from: hq */
    public void mo2434hq(int i) {
        this.cYR = i;
    }
}
