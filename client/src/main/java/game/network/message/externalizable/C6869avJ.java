package game.network.message.externalizable;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.avJ  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6869avJ implements Externalizable {
    public static final int ALL = 3;
    public static final int LEFT = 1;
    public static final int NONE = 0;
    public static final int RIGHT = 2;

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("This class is not supposed to be serialized");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("This class is not supposed to be serialized");
    }
}
