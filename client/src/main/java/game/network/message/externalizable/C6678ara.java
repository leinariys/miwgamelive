package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.C0474GZ;
import logic.baa.C1616Xf;
import p001a.C1875af;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.ara  reason: case insensitive filesystem */
/* compiled from: a */
public class C6678ara extends Blocking implements Externalizable {


    /* renamed from: Uk */
    private C1616Xf f5232Uk;

    public C6678ara() {
    }

    public C6678ara(C1616Xf xf) {
        this.f5232Uk = xf;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f5232Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f5232Uk);
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        ((C1875af) this.f5232Uk.bFf()).mo13215b(bVar);
        return null;
    }
}
