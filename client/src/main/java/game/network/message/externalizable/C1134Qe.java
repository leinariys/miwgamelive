package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.C0474GZ;
import game.network.message.C5581aOv;
import logic.baa.C1616Xf;
import logic.thred.LogPrinter;
import p001a.C1875af;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Qe */
/* compiled from: a */
public class C1134Qe extends C5581aOv implements Externalizable {

    static LogPrinter logger = LogPrinter.m10275K(C1134Qe.class);
    /* renamed from: Uk */
    private C1616Xf f1452Uk;
    private int version;

    public C1134Qe() {
    }

    public C1134Qe(C1616Xf xf, int i) {
        this.f1452Uk = xf;
        this.version = i;
    }

    /* renamed from: yz */
    public C1616Xf mo808yz() {
        return this.f1452Uk;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.f1452Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        this.version = objectInput.readInt();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f1452Uk);
        objectOutput.writeInt(this.version);
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        C1875af afVar = (C1875af) this.f1452Uk.bFf();
        try {
            jz.bGJ().mo6442f(bVar).mo8400e(afVar);
            if (((long) this.version) == afVar.cVj().aBt()) {
                return null;
            }
            jz.bGU().mo15549b(bVar, (Blocking) new C5887acP(afVar.cVj()));
            return null;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
