package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.oI */
/* compiled from: a */
public class C3131oI extends C1506WA implements Externalizable {
    private static final long serialVersionUID = 3330622574580337692L;

    /* renamed from: dB */
    private C3132a f8754dB;

    public C3131oI() {
    }

    public C3131oI(C3132a aVar) {
        this.f8754dB = aVar;
    }

    public String toString() {
        return this.f8754dB == null ? "<NO TRANSITION>" : this.f8754dB.toString();
    }

    /* renamed from: Us */
    public C3132a mo20956Us() {
        return this.f8754dB;
    }

    public void readExternal(ObjectInput objectInput) {
        this.f8754dB = C3132a.values()[objectInput.readInt()];
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeInt(this.f8754dB.ordinal());
    }

    /* renamed from: a.oI$a */
    public enum C3132a {
        ONI_CHECKLIST_PERSONAL_MENU,
        ONI_CHECKLIST_LEFT_SIDE_MENU,
        ONI_CHECKLIST_RIGHT_SIDE_MENU,
        ONI_CHECKLIST_STATION_OPTIONS,
        ONI_CHECKLIST_PDA,
        ONI_CHECKLIST_CHAT,
        ONI_CHECKLIST_WEAPON_SELECT,
        ONI_CHECKLIST_EQUIPMENT_SELECT,
        ONI_CHECKLIST_OTHER_ELEMENTS,
        OPEN_AVATAR_CREATION,
        CREATED_AVATAR,
        SHIP_INSTALLED,
        UNLOCK_HANGAR,
        HIGHLIGHT_HANGAR,
        FIRST_LAUNCH,
        HIGHLIGHT_MARKET,
        OPENED_MARKET,
        HIGHLIGHT_MISSION_ITEM,
        SELECTED_MISSION_ITEM,
        HIGHLIGHT_DESTINATION_CONTAINER,
        SELECTED_DESTINATION_CONTAINER,
        BOUGHT_MISSION_ITEM,
        HIGHLIGHT_OUTPOST,
        HIGHLIGHT_DOCK_BUTTON,
        HIGHLIGHT_SHIP_WINDOW,
        OPENED_SHIP_WINDOW,
        HIGHLIGHT_SHIP_CONTAINER,
        CAREER_CHOOSER,
        CAREER_CHOSEN,
        FINISHED_NURSERY
    }
}
