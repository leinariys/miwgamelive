package game.network.message.externalizable;

import logic.aaa.C1506WA;

import javax.swing.*;
import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aTX */
/* compiled from: a */
public class aTX extends C1506WA implements Externalizable {

    private final String css;
    private final Action hJo;
    private final Object iWe;
    private final int order;

    public aTX() {
        this.hJo = null;
        this.order = 0;
        this.css = "";
        this.iWe = null;
    }

    public aTX(Action action, int i, String str, Object obj) {
        this.hJo = action;
        this.order = i;
        this.css = str;
        this.iWe = obj;
    }

    public Action getAction() {
        return this.hJo;
    }

    public int getOrder() {
        return this.order;
    }

    public String dzn() {
        return this.css;
    }

    public Object dzo() {
        return this.iWe;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("This class is not supposed to be serialized");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("This class is not supposed to be serialized");
    }
}
