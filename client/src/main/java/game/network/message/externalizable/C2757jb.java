package game.network.message.externalizable;

import game.network.message.Blocking;
import p001a.C5496aLo;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.jb */
/* compiled from: a */
public abstract class C2757jb extends Blocking implements Externalizable {

    private long agR;
    private Class<?> agS;

    public C2757jb() {
    }

    public C2757jb(long j, Class<?> cls) {
        this.agR = j;
        this.agS = cls;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.agR = objectInput.readLong();
        this.agS = (Class) C5496aLo.ilg.inputReadObject(objectInput);
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeLong(this.agR);
        C5496aLo.ilg.serializeData(objectOutput, this.agS);
    }

    /* renamed from: Ej */
    public long mo19957Ej() {
        return this.agR;
    }

    /* renamed from: hD */
    public Class<?> mo19958hD() {
        return this.agS;
    }
}
