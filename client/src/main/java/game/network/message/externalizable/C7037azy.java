package game.network.message.externalizable;

import game.network.manager.DefaultMagicDataClassSerializer;
import game.network.message.Blocking;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.azy  reason: case insensitive filesystem */
/* compiled from: a */
public class C7037azy extends Blocking implements Externalizable {

    private Blocking[] gZJ;

    public C7037azy() {
    }

    public C7037azy(Blocking[] kUVarArr) {
        this.gZJ = kUVarArr;
    }

    public Blocking[] cFV() {
        return this.gZJ;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        int readInt = objectInput.readInt();
        Blocking[] kUVarArr = new Blocking[readInt];
        this.gZJ = kUVarArr;
        for (int i = 0; i < readInt; i++) {
            kUVarArr[i] = (Blocking) DefaultMagicDataClassSerializer.dataClassSerializer.inputReadObject(objectInput);
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeInt(r2);
        for (Blocking a : this.gZJ) {
            DefaultMagicDataClassSerializer.dataClassSerializer.serializeData(objectOutput, (Object) a);
        }
    }
}
