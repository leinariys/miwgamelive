package game.network.message.externalizable;

import game.script.mission.MissionTemplate;
import game.script.npc.NPC;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aUd  reason: case insensitive filesystem */
/* compiled from: a */
public class C5719aUd extends C1506WA implements Externalizable {

    public MissionTemplate baM;
    public NPC cqw;

    public C5719aUd() {
    }

    public C5719aUd(MissionTemplate avh) {
        this(avh, (NPC) null);
    }

    public C5719aUd(MissionTemplate avh, NPC auf) {
        this.baM = avh;
        this.cqw = auf;
    }

    public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
        this.baM = (MissionTemplate) objectInput.readObject();
        this.cqw = (NPC) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeObject(this.baM);
        objectOutput.writeObject(this.cqw);
    }
}
