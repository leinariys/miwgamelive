package game.network.message.externalizable;

import game.script.npc.NPC;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Vi */
/* compiled from: a */
public class C1475Vi extends C1506WA implements Externalizable {
    private NPC cqw;

    public C1475Vi() {
    }

    public C1475Vi(NPC auf) {
        this.cqw = auf;
    }

    public NPC bAj() {
        return this.cqw;
    }

    public void readExternal(ObjectInput objectInput) {
        this.cqw = (NPC) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeObject(this.cqw);
    }
}
