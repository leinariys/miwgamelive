package game.network.message.externalizable;

import logic.aaa.C1506WA;

import javax.swing.*;
import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aFs  reason: case insensitive filesystem */
/* compiled from: a */
public class C5344aFs extends C1506WA implements Externalizable {

    private final boolean aTI;
    private final Action hJo;
    private final int order;

    public C5344aFs() {
        this.hJo = null;
        this.order = 0;
        this.aTI = false;
    }

    public C5344aFs(Action action, int i, boolean z) {
        this.hJo = action;
        this.order = i;
        this.aTI = z;
    }

    public Action getAction() {
        return this.hJo;
    }

    public int getOrder() {
        return this.order;
    }

    public boolean cYK() {
        return this.aTI;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("This class is not supposed to be serialized");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("This class is not supposed to be serialized");
    }
}
