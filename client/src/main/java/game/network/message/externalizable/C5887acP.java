package game.network.message.externalizable;

import game.engine.DataGameEvent;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.C0474GZ;
import logic.baa.C1616Xf;
import logic.data.mbean.C0677Jd;
import logic.data.mbean.C6064afk;
import p001a.C3582se;
import p001a.CountFailedReadOrWriteException;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.acP  reason: case insensitive filesystem */
/* compiled from: a */
public class C5887acP extends Blocking implements Externalizable {

    private List<C6064afk> list;

    public C5887acP() {
    }

    public C5887acP(C6064afk afk) {
        this.list = new ArrayList(1);
        this.list.add(afk);
    }

    public C5887acP(List<C6064afk> list2) {
        this.list = list2;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        int readInt = objectInput.readInt();
        this.list = new ArrayList(readInt);
        for (int i = 0; i < readInt; i++) {
            C0677Jd jd = (C0677Jd) ((C1616Xf) C0474GZ.dah.inputReadObject(objectInput)).mo13W();
            jd.mo3158a(objectInput, 0);
            this.list.add(jd);
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeInt(this.list.size());
        for (C6064afk next : this.list) {
            try {
                C0474GZ.dah.serializeData(objectOutput, (Object) next.mo11944yn());
                ((C0677Jd) next).mo3186b(objectOutput, 0);
            } catch (Exception e) {
                throw new CountFailedReadOrWriteException("Error serializing " + next.mo11944yn().bFf().bFY(), e);
            }
        }
    }

    /* renamed from: a */
    public C6302akO mo876a(ClientConnect bVar, DataGameEvent jz) {
        for (C6064afk next : this.list) {
            ((C3582se) next.mo11944yn().bFf()).mo21982c(next);
        }
        return null;
    }
}
