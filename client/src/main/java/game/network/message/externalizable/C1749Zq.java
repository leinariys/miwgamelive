package game.network.message.externalizable;

import game.script.item.Item;
import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Zq */
/* compiled from: a */
public class C1749Zq extends C1506WA implements Externalizable {

    private Item aoR;
    private boolean eNz;

    public C1749Zq() {
    }

    public C1749Zq(boolean z, Item auq) {
        this.eNz = z;
        this.aoR = auq;
    }

    public boolean bIs() {
        return this.eNz;
    }

    /* renamed from: XH */
    public Item mo7492XH() {
        return this.aoR;
    }

    public void readExternal(ObjectInput objectInput) {
        this.eNz = objectInput.readBoolean();
        this.aoR = (Item) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeBoolean(this.eNz);
        objectOutput.writeObject(this.aoR);
    }
}
