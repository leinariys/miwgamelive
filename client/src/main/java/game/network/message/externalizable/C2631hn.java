package game.network.message.externalizable;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.WhoAmI;
import game.network.message.C0474GZ;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import logic.res.html.C0029AO;
import logic.res.html.DataClassSerializer;
import p001a.C2234dA;
import p001a.C5726aUk;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: a.hn */
/* compiled from: a */
public abstract class C2631hn<T> implements C0029AO<T>, C2234dA<T>, Externalizable {


    /* renamed from: Uk */
    public transient C1616Xf f8051Uk;

    /* renamed from: Ul */
    public transient C5663aRz f8052Ul;

    /* renamed from: Um */
    public transient DataClassSerializer f8053Um;

    /* renamed from: Un */
    private boolean f8054Un;

    public C2631hn() {
    }

    public C2631hn(C1616Xf xf, C5663aRz arz) {
        this.f8051Uk = xf;
        this.f8052Ul = arz;
        this.f8054Un = (xf.bFf().mo6866PM().getWhoAmI() == WhoAmI.CLIENT && xf.isUseClientOnly()) || xf.bFn();
    }

    public abstract Object clone();

    public abstract Collection<T> getCollection();

    /* renamed from: ym */
    public C5663aRz mo19378ym() {
        return this.f8052Ul;
    }

    /* renamed from: c */
    public void mo19371c(C5663aRz arz) {
        this.f8052Ul = arz;
        if (this.f8051Uk != null) {
            this.f8054Un = (this.f8051Uk.bFf().mo6866PM().getWhoAmI() == WhoAmI.CLIENT && (this.f8051Uk.isUseClientOnly() || arz.mo7389hk())) || this.f8051Uk.bFn();
        }
        DataClassSerializer[] Eu = mo19378ym().mo11298Eu();
        if (Eu == null || Eu.length <= 0) {
            this.f8053Um = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
        } else {
            this.f8053Um = Eu[0];
        }
    }

    public void readExternal(ObjectInput objectInput) {
        this.f8051Uk = (C1616Xf) C0474GZ.dah.inputReadObject(objectInput);
        mo19371c(this.f8051Uk.mo25c()[objectInput.readShort()]);
        DataClassSerializer[] Eu = mo19378ym().mo11298Eu();
        if (Eu == null || Eu.length <= 0) {
            this.f8053Um = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
        } else {
            this.f8053Um = Eu[0];
        }
    }

    public void readExternal(IReadExternal vm) {
        String property = System.getProperty("FIX_COLLECTIONS");
        if (property != null && property.equalsIgnoreCase("TRUE")) {
            vm.mo6357gU("unknow");
        }
        DataClassSerializer[] Eu = mo19378ym().mo11298Eu();
        if (Eu == null || Eu.length <= 0) {
            this.f8053Um = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
        } else {
            this.f8053Um = Eu[0];
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        C0474GZ.dah.serializeData(objectOutput, (Object) this.f8051Uk);
        objectOutput.writeShort(this.f8052Ul.mo7395hq());
        if (this.f8053Um == null) {
            DataClassSerializer[] Eu = mo19378ym().mo11298Eu();
            if (Eu == null || Eu.length <= 0) {
                this.f8053Um = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
            } else {
                this.f8053Um = Eu[0];
            }
        }
    }

    public void writeExternal(IWriteExternal att) {
        if (this.f8053Um == null) {
            DataClassSerializer[] Eu = mo19378ym().mo11298Eu();
            if (Eu == null || Eu.length <= 0) {
                this.f8053Um = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
            } else {
                this.f8053Um = Eu[0];
            }
        }
    }

    /* renamed from: c */
    public void mo286c(ObjectInput objectInput) {
        DataClassSerializer[] Eu = mo19378ym().mo11298Eu();
        if (Eu == null || Eu.length <= 0) {
            this.f8053Um = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
        } else {
            this.f8053Um = Eu[0];
        }
    }

    /* renamed from: a */
    public void mo284a(ObjectOutput objectOutput) {
        if (this.f8053Um == null) {
            DataClassSerializer[] Eu = mo19378ym().mo11298Eu();
            if (Eu == null || Eu.length <= 0) {
                this.f8053Um = C5726aUk.m18699b(Object.class, (Class<?>[]) null);
            } else {
                this.f8053Um = Eu[0];
            }
        }
    }

    public Iterator<T> iterator() {
        return new C2632a(getCollection().iterator());
    }

    public boolean retainAll(Collection<?> collection) {
        if (mo19380yo()) {
            return getCollection().retainAll(collection);
        }
        if (this.f8051Uk.bFf().mo6887du()) {
            return false;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public boolean removeAll(Collection<?> collection) {
        if (mo19380yo()) {
            return getCollection().removeAll(collection);
        }
        if (this.f8051Uk.bFf().mo6887du()) {
            return false;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public boolean remove(Object obj) {
        if (mo19380yo()) {
            return getCollection().remove(obj);
        }
        if (this.f8051Uk.bFf().mo6887du()) {
            return false;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public int size() {
        return getCollection().size();
    }

    public boolean isEmpty() {
        return getCollection().isEmpty();
    }

    public <E> E[] toArray(E[] eArr) {
        return getCollection().toArray(eArr);
    }

    public Object[] toArray() {
        return getCollection().toArray();
    }

    public String toString() {
        return getCollection().toString();
    }

    /* renamed from: yn */
    public C1616Xf mo19379yn() {
        return this.f8051Uk;
    }

    public void clear() {
        if (mo19380yo()) {
            getCollection().clear();
        } else if (!this.f8051Uk.bFf().mo6887du()) {
            throw new IllegalStateException("Illegal outside transaction");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: yo */
    public boolean mo19380yo() {
        return this.f8054Un || this.f8051Uk.bFf().mo6866PM().bGo();
    }

    public boolean contains(Object obj) {
        return getCollection().contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        return getCollection().containsAll(collection);
    }

    public boolean add(T t) {
        if (mo19380yo()) {
            return getCollection().add(t);
        }
        if (this.f8051Uk.bFf().mo6887du()) {
            return false;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public boolean addAll(Collection<? extends T> collection) {
        if (mo19380yo()) {
            return getCollection().addAll(collection);
        }
        if (this.f8051Uk.bFf().mo6887du()) {
            return false;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    /* renamed from: l */
    public void mo17726l(T t) {
        getCollection().add(t);
    }

    /* renamed from: m */
    public boolean mo17727m(T t) {
        return getCollection().remove(t);
    }

    /* renamed from: j */
    public void mo19375j(Collection<T> collection) {
        if (mo19380yo()) {
            clear();
            addAll(collection);
        } else if (!this.f8051Uk.bFf().mo6887du()) {
            throw new IllegalStateException("Illegal outside transaction");
        }
    }

    /* renamed from: a.hn$a */
    class C2632a implements Iterator<T> {
        private final /* synthetic */ Iterator iAs;

        C2632a(Iterator it) {
            this.iAs = it;
        }

        public boolean hasNext() {
            return this.iAs.hasNext();
        }

        public T next() {
            return this.iAs.next();
        }

        public void remove() {
            if (C2631hn.this.mo19380yo()) {
                this.iAs.remove();
            } else if (!C2631hn.this.f8051Uk.bFf().mo6887du()) {
                throw new IllegalStateException("Illegal outside transaction");
            }
        }
    }
}
