package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Nn */
/* compiled from: a */
public class C0939Nn extends C1506WA implements Externalizable {

    private Object[] dmZ;
    private C0940a grl;
    private String message;

    public C0939Nn() {
    }

    public C0939Nn(C0940a aVar, String str, Object... objArr) {
        this.grl = aVar;
        this.message = str;
        this.dmZ = objArr;
    }

    public C0940a crM() {
        return this.grl;
    }

    public String getMessage() {
        return this.message;
    }

    public Object[] getParams() {
        return this.dmZ;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    /* renamed from: a.Nn$a */
    public enum C0940a {
        INFO("Info"),
        CONFIRM("Question"),
        WARNING("Warning"),
        ERROR("Error");

        public final String title;

        private C0940a(String str) {
            this.title = str;
        }
    }
}
