package game.network.message.externalizable;

import logic.aaa.C1506WA;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aQH */
/* compiled from: a */
public class aQH extends C1506WA implements Externalizable {
    public static final short iEV = 0;
    public static final short iEW = 1;
    public static final short iEX = 2;
    public static final short iEY = 3;

    private short btk;
    private String iEZ;

    public aQH() {
    }

    public aQH(short s, String str) {
        this.btk = s;
        this.iEZ = str;
    }

    public short aiH() {
        return this.btk;
    }

    public String dqc() {
        return this.iEZ;
    }

    public void readExternal(ObjectInput objectInput) {
        this.btk = objectInput.readShort();
        this.iEZ = objectInput.readLine();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeShort(this.btk);
        objectOutput.writeChars(this.iEZ);
    }
}
