package game.network.message.externalizable;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.apO  reason: case insensitive filesystem */
/* compiled from: a */
public class ObjectId implements Externalizable {
    private transient int hashCode;

    /* renamed from: id */
    private long id;

    public ObjectId(long id) {
        this.id = id;
        this.hashCode = getHashCode();
    }

    public long getId() {
        return this.id;
    }

    public int hashCode() {
        return this.hashCode;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        if (this.id != ((ObjectId) obj).id) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "OID[id=" + this.id + "]";
    }

    public void readExternal(ObjectInput objectInput) throws IOException {
        this.id = objectInput.readLong();
        this.hashCode = getHashCode();
    }

    private int getHashCode() {
        return ((int) (this.id ^ (this.id >>> 32))) + 31;
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeLong(this.id);
    }
}
