package game.network.message;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channel;

/* renamed from: a.bL */
/* compiled from: a */
public class ChannelWriterCompression extends ChannelBufferWriter {
    /* renamed from: pa */
    private final int sizeBlock;
    private ByteBuffer buffer;
    /* renamed from: oZ */
    private int anInt;
    /* renamed from: pb */
    private int metricWriteByte;

    /* renamed from: pd */
    private int allOriginalByteLength;

    /* renamed from: pe */
    private int countWriteMessage;

    /* renamed from: pf */
    private boolean bufferReadyToBeRead;

    /* renamed from: pg */
    private int metricМessagesEmpty;

    /* renamed from: ph */
    private int metricMessageFilled;

    public ChannelWriterCompression(int i, int sizeBlock) {
        this.anInt = 48151623;
        this.buffer = ByteBuffer.allocate(1024);
        this.sizeBlock = sizeBlock;
        if (sizeBlock >= 536870911) {
            throw new IllegalArgumentException("Max block sise too big " + sizeBlock);
        }
    }

    public ChannelWriterCompression() {
        this.anInt = 48151623;
        this.buffer = ByteBuffer.allocate(1024);
        this.sizeBlock = 8388608;  //    DDSLoader.DDSD_DEPTH
    }

    /* renamed from: gE */
    public boolean isBufferReadyToBeRead() {
        return this.bufferReadyToBeRead;
    }

    /* renamed from: a */
    public boolean writeMessageToBuffer(ByteMessageCompres messageCompres) {
        int offset = messageCompres.getOffset();
        int length = messageCompres.getLength();
        byte[] message = messageCompres.getMessage();
        this.allOriginalByteLength += (messageCompres.isCompressed() ? messageCompres.getOriginalLength() : length);
        this.countWriteMessage++;
        if (!bufferIncrease(this.buffer.position() + length + 6)) {
            return false;
        }
        int position = this.buffer.position();
        writeLengthSpecial(messageCompres.isCompressed(), length, messageCompres.isSpecial());
        this.buffer.put(message, offset, length);
        encryptionByteXorSalt(this.buffer.array(), position, this.buffer.position());
        return true;
    }

    /* renamed from: gF */
    public void prepareForReading() {
        if (this.buffer.position() > 0) {
            this.buffer.flip();
            this.bufferReadyToBeRead = true;
        }
    }

    /* renamed from: Y */
    private boolean bufferIncrease(int i) {
        if (this.buffer.capacity() < i) {
            if (this.buffer.capacity() > 1048576 && this.buffer.position() > 0) {
                return false;
            }
            ByteBuffer allocate = ByteBuffer.allocate(Math.max(i, this.buffer.capacity() << 1));
            this.buffer.flip();
            allocate.put(this.buffer);
            this.buffer = allocate;
        }
        return true;
    }

    /* renamed from: a */
    private void encryptionByteXorSalt(byte[] bArr, int i, int i2) {
        while (i < i2) {
            byte b = bArr[i];
            bArr[i] = (byte) (((b & 255) ^ (this.anInt & 255)) & 255);
            this.anInt = WriteEncryptionOutputStream.calculationSaltKey(this.anInt, b);
            i++;
        }
    }

    /* renamed from: a */
    private void formationSpecialMessage(boolean z, int i, boolean z2) {
        if (i <= 31) {
            this.buffer.put(3, (byte) ((z ? 128 : 0) | i));
            this.buffer.position(3);
        } else if (i <= 8191) {
            this.buffer.putShort(2, (short) ((z ? 40960 : 8192) | i));//    DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY
            this.buffer.position(2);
        } else if (i <= 2097151) {
            this.buffer.put(1, (byte) ((z ? 192 : 64) | (i >> 16)));
            this.buffer.putShort(2, (short) (65535 & i));
            this.buffer.position(1);
        } else {
            this.buffer.putInt(0, (z ? -536870912 : 1610612736) | i);
            this.buffer.position(0);
        }
        if (!z2) {
            return;
        }
        if (this.buffer.position() == 0) {
            throw new IllegalArgumentException("Invalid special message size: " + i);
        }
        this.buffer.position(this.buffer.position() - 1);
        this.buffer.put(this.buffer.position(), (byte) 0);
    }

    /* renamed from: b */
    private void writeLengthSpecial(boolean z, int i, boolean z2) {
        int i2 = 0;
        if (z2) {
            this.buffer.put((byte) 0);
        }
        if (i <= 31) {
            ByteBuffer byteBuffer = this.buffer;
            if (z) {
                i2 = 128;
            }
            byteBuffer.put((byte) (i2 | i));
        } else if (i <= 8191) {
            this.buffer.putShort((short) ((z ? 40960 : 8192) | i)); //   DDSLoader.DDSCAPS2_CUBEMAP_NEGATIVEY
        } else if (i <= 2097151) {
            this.buffer.put((byte) ((z ? 192 : 64) | (i >> 16)));
            this.buffer.putShort((short) (65535 & i));
        } else {
            this.buffer.putInt((z ? -536870912 : 1610612736) | i);
        }
    }

    /* renamed from: a */
    public boolean writeBufferToChannel(Channel channel) throws IOException {
        int write = ((ByteChannel) channel).write(this.buffer);
        if (write > 0) {
            this.metricWriteByte += write;
            this.metricMessageFilled++;
        } else {
            this.metricМessagesEmpty++;
        }
        if (!this.buffer.hasRemaining()) {
            this.buffer.clear();
            this.bufferReadyToBeRead = false;
        }
        if (write > 0) {
            return true;
        }
        return false;
    }

    /* renamed from: gG */
    public int getMetricWriteByte() {
        return this.metricWriteByte;
    }

    /* renamed from: gH */
    public int getCountWriteMessage() {
        return this.countWriteMessage;
    }

    /* renamed from: gI */
    public int getAllOriginalByteLength() {
        return this.allOriginalByteLength;
    }

    /* renamed from: gJ */
    public int getMetricМessagesEmpty() {
        return this.metricМessagesEmpty;
    }

    /* renamed from: gK */
    public int getMetricMessageFilled() {
        return this.metricMessageFilled;
    }
}
