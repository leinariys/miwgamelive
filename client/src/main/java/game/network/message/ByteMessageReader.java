package game.network.message;

import game.engine.SocketMessage;
import logic.thred.LogPrinter;
import util.Syst;

import java.io.EOFException;
import java.io.InputStream;
import java.io.UTFDataFormatException;

/* renamed from: a.aMf  reason: case insensitive filesystem */
/* compiled from: a */
public class ByteMessageReader extends InputStream implements IReadProto {
    int typeByte = 0;
    int unsignedByte = 0;
    private byte[] buf;
    private int count;
    private char[] ioz = null;
    private int mark = 0;
    private int pos;

    private ByteMessageReader() {
    }

    public ByteMessageReader(byte[] bArr) {
        this.buf = bArr;
        this.pos = 0;
        this.count = bArr.length;
    }

    public ByteMessageReader(byte[] bArr, int i, int i2) {
        this.buf = bArr;
        this.pos = i;
        this.mark = i;
        this.count = Math.min(i + i2, bArr.length);
    }

    /* renamed from: a */
    public int readBits(int i) throws EOFException {
        int i2 = 0;
        while (i > 0) {
            if (this.typeByte == 0) {
                this.unsignedByte = readUnsignedByte();
                this.typeByte = 8;
            }
            if (i > this.typeByte) {
                i2 |= (this.unsignedByte & SocketMessage.MASK_INT[this.typeByte]) << (i - this.typeByte);
                i -= this.typeByte;
                this.typeByte = 0;
            } else if (i == this.typeByte) {
                int i3 = i2 | (this.unsignedByte & SocketMessage.MASK_INT[i]);
                this.typeByte -= i;
                return i3;
            } else {
                int i4 = i2 | ((this.unsignedByte >> (this.typeByte - i)) & SocketMessage.MASK_INT[i]);
                this.typeByte -= i;
                return i4;
            }
        }
        return i2;
    }

    public int read() {
        if (this.pos >= this.count) {
            return -1;
        }
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i] & 255;
    }

    private void ensure(int i) throws EOFException {
        if (i > this.count) {
            throw new EOFException();
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        int i3;
        if (this.pos >= this.count) {
            return -1;
        }
        if (this.pos + i2 > this.count) {
            i3 = this.count - this.pos;
        } else {
            i3 = i2;
        }
        if (i3 <= 0) {
            return 0;
        }
        System.arraycopy(this.buf, this.pos, bArr, i, i3);
        this.pos += i3;
        return i3;
    }

    public long skip(long j) {
        long j2;
        if (((long) this.pos) + j > ((long) this.count)) {
            j2 = (long) (this.count - this.pos);
        } else {
            j2 = j;
        }
        if (j2 < 0) {
            return 0;
        }
        this.pos = (int) (((long) this.pos) + j2);
        return j2;
    }

    public int available() {
        return this.count - this.pos;
    }

    public boolean markSupported() {
        return true;
    }

    public void mark(int i) {
        this.mark = this.pos;
    }

    public void reset() {
        this.pos = this.mark;
        this.unsignedByte = 0;
        this.typeByte = 0;
    }

    public void setPas(int i) {
        this.pos = i;
    }

    public ByteMessageReader cloneByteMessageReader() {
        ByteMessageReader amf = new ByteMessageReader();
        amf.buf = new byte[this.buf.length];
        System.arraycopy(this.buf, 0, amf.buf, 0, this.buf.length);
        amf.count = this.count;
        amf.pos = this.pos;
        amf.mark = this.mark;
        amf.unsignedByte = this.unsignedByte;
        amf.typeByte = this.typeByte;
        return amf;
    }

    public void close() {
    }

    public void readFully(byte[] bArr) throws EOFException {
        readFully(bArr, 0, bArr.length);
    }

    public void readFully(byte[] bArr, int i, int i2) throws EOFException {
        if (read(bArr, i, i2) < 0) {
            throw new EOFException();
        }
    }

    public int skipBytes(int i) {
        return (int) skip((long) i);
    }

    public boolean readBoolean() throws EOFException {
        return readBits(1) != 0;
    }

    public byte readByte() throws EOFException {
        ensure(this.pos + 1);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i];
    }

    public int readUnsignedByte() throws EOFException {
        ensure(this.pos + 1);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        return bArr[i] & 255;
    }

    public short readShort() throws EOFException {
        boolean z = true;
        if (readBits(1) != 0) {
            return readShort0();
        }
        if (readBits(1) != 1) {
            z = false;
        }
        short read = (short) (read() & 255);
        if (z) {
            return (short) (-read);
        }
        return read;
    }

    public short readShort0() throws EOFException {
        ensure(this.pos + 2);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        return (short) (((bArr[i] & 255) << 8) + ((bArr2[i2] & 255) << 0));
    }

    public int readUnsignedShort() throws EOFException {
        return readShort() & 65535;
    }

    public int readUnsignedShort0() throws EOFException {
        ensure(this.pos + 2);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        return ((bArr[i] & 255) << 8) + ((bArr2[i2] & 255) << 0);
    }

    public char readChar() throws EOFException {
        if (readBits(1) == 1) {
            return (char) readShort0();
        }
        return (char) read();
    }

    public int readInt() throws EOFException {
        boolean z = true;
        switch (readBits(2)) {
            case 0:
                if (readBits(1) != 1) {
                    z = false;
                }
                int read = read() & 255;
                if (z) {
                    return -read;
                }
                return read;
            case 1:
                if (readBits(1) != 1) {
                    z = false;
                }
                int cjC = readUnsignedShort0();
                if (z) {
                    return -cjC;
                }
                return cjC;
            case 2:
                if (readBits(1) != 1) {
                    z = false;
                }
                int cjF = read3bytes();
                if (z) {
                    return -cjF;
                }
                return cjF;
            default:
                return readInt0();
        }
    }

    public int readPositiveInt() throws EOFException {
        switch (readBits(2)) {
            case 0:
                return read() & 255;
            case 1:
                return readUnsignedShort0();
            case 2:
                return read3bytes();
            default:
                return readInt0();
        }
    }

    public int readInt0() throws EOFException {
        ensure(this.pos + 4);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        byte[] bArr3 = this.buf;
        int i3 = this.pos;
        this.pos = i3 + 1;
        byte[] bArr4 = this.buf;
        int i4 = this.pos;
        this.pos = i4 + 1;
        return ((bArr[i] & 255) << C6215aif.idH) + ((bArr2[i2] & 255) << LogPrinter.eqN) + ((bArr3[i3] & 255) << 8) + ((bArr4[i4] & 255) << 0);
    }

    public int read3bytes() throws EOFException {
        ensure(this.pos + 3);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        byte[] bArr3 = this.buf;
        int i3 = this.pos;
        this.pos = i3 + 1;
        return ((bArr[i] & 255) << LogPrinter.eqN) + ((bArr2[i2] & 255) << 8) + ((bArr3[i3] & 255) << 0);
    }

    public long readLong() throws EOFException {
        boolean z = true;
        switch (readBits(3)) {
            case 0:
                if (readBits(1) != 1) {
                    z = false;
                }
                long read = (long) (read() & 255);
                if (z) {
                    return -read;
                }
                return read;
            case 1:
                if (readBits(1) != 1) {
                    z = false;
                }
                long cjC = (long) readUnsignedShort0();
                if (z) {
                    return -cjC;
                }
                return cjC;
            case 2:
                if (readBits(1) != 1) {
                    z = false;
                }
                long cjF = (long) read3bytes();
                if (z) {
                    return -cjF;
                }
                return cjF;
            case 3:
                if (readBits(1) != 1) {
                    z = false;
                }
                long cjE = ((long) readInt0()) & 4294967295L;
                if (z) {
                    return -cjE;
                }
                return cjE;
            case 4:
                if (readBits(1) != 1) {
                    z = false;
                }
                long diX = readLong5();
                if (z) {
                    return -diX;
                }
                return diX;
            case 5:
                if (readBits(1) != 1) {
                    z = false;
                }
                long diW = readLong6();
                if (z) {
                    return -diW;
                }
                return diW;
            case 6:
                if (readBits(1) != 1) {
                    z = false;
                }
                long diV = readLong7();
                if (z) {
                    return -diV;
                }
                return diV;
            default:
                return readLong0();
        }
    }

    public long readLong0() throws EOFException {
        ensure(this.pos + 8);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        byte[] bArr3 = this.buf;
        int i3 = this.pos;
        this.pos = i3 + 1;
        byte[] bArr4 = this.buf;
        int i4 = this.pos;
        this.pos = i4 + 1;
        byte[] bArr5 = this.buf;
        int i5 = this.pos;
        this.pos = i5 + 1;
        byte[] bArr6 = this.buf;
        int i6 = this.pos;
        this.pos = i6 + 1;
        byte[] bArr7 = this.buf;
        int i7 = this.pos;
        this.pos = i7 + 1;
        byte[] bArr8 = this.buf;
        int i8 = this.pos;
        this.pos = i8 + 1;
        return (((long) ((bArr2[i2] & 255) & 255)) << 48) + (((long) (bArr[i] & 255)) << 56) + (((long) ((bArr3[i3] & 255) & 255)) << 40) + (((long) ((bArr4[i4] & 255) & 255)) << 32) + (((long) ((bArr5[i5] & 255) & 255)) << 24) + ((long) (((bArr6[i6] & 255) & 255) << LogPrinter.eqN)) + ((long) (((bArr7[i7] & 255) & 255) << 8)) + ((long) (((bArr8[i8] & 255) & 255) << 0));
    }

    private long readLong7() throws EOFException {
        ensure(this.pos + 7);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        byte[] bArr3 = this.buf;
        int i3 = this.pos;
        this.pos = i3 + 1;
        byte[] bArr4 = this.buf;
        int i4 = this.pos;
        this.pos = i4 + 1;
        byte[] bArr5 = this.buf;
        int i5 = this.pos;
        this.pos = i5 + 1;
        byte[] bArr6 = this.buf;
        int i6 = this.pos;
        this.pos = i6 + 1;
        byte[] bArr7 = this.buf;
        int i7 = this.pos;
        this.pos = i7 + 1;
        return (((long) ((bArr2[i2] & 255) & 255)) << 40) + (((long) ((bArr[i] & 255) & 255)) << 48) + (((long) ((bArr3[i3] & 255) & 255)) << 32) + (((long) ((bArr4[i4] & 255) & 255)) << 24) + ((long) (((bArr5[i5] & 255) & 255) << LogPrinter.eqN)) + ((long) (((bArr6[i6] & 255) & 255) << 8)) + ((long) (((bArr7[i7] & 255) & 255) << 0));
    }

    private long readLong6() throws EOFException {
        ensure(this.pos + 6);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        byte[] bArr3 = this.buf;
        int i3 = this.pos;
        this.pos = i3 + 1;
        byte[] bArr4 = this.buf;
        int i4 = this.pos;
        this.pos = i4 + 1;
        byte[] bArr5 = this.buf;
        int i5 = this.pos;
        this.pos = i5 + 1;
        byte[] bArr6 = this.buf;
        int i6 = this.pos;
        this.pos = i6 + 1;
        return (((long) ((bArr2[i2] & 255) & 255)) << 32) + (((long) ((bArr[i] & 255) & 255)) << 40) + (((long) ((bArr3[i3] & 255) & 255)) << 24) + ((long) (((bArr4[i4] & 255) & 255) << LogPrinter.eqN)) + ((long) (((bArr5[i5] & 255) & 255) << 8)) + ((long) (((bArr6[i6] & 255) & 255) << 0));
    }

    private long readLong5() throws EOFException {
        ensure(this.pos + 5);
        byte[] bArr = this.buf;
        int i = this.pos;
        this.pos = i + 1;
        byte[] bArr2 = this.buf;
        int i2 = this.pos;
        this.pos = i2 + 1;
        byte[] bArr3 = this.buf;
        int i3 = this.pos;
        this.pos = i3 + 1;
        byte[] bArr4 = this.buf;
        int i4 = this.pos;
        this.pos = i4 + 1;
        byte[] bArr5 = this.buf;
        int i5 = this.pos;
        this.pos = i5 + 1;
        return (((long) ((bArr2[i2] & 255) & 255)) << 24) + (((long) ((bArr[i] & 255) & 255)) << 32) + ((long) (((bArr3[i3] & 255) & 255) << LogPrinter.eqN)) + ((long) (((bArr4[i4] & 255) & 255) << 8)) + ((long) (((bArr5[i5] & 255) & 255) << 0));
    }

    public float readFloat() throws EOFException {
        if (readBits(1) == 0) {
            return Float.intBitsToFloat(readInt0());
        }
        switch (readBits(2)) {
            case 0:
                return (float) readInt();
            case 1:
                return SocketMessage.fVk.mo8283cF(readByte());
            case 2:
                return SocketMessage.fVj.mo8283cF(readShort0());
            case 3:
                return SocketMessage.fVl.mo8283cF(read3bytes());
            default:
                return 0.0f;
        }
    }

    public double readDouble() throws EOFException {
        if (readBits(1) == 1) {
            return (double) readFloat();
        }
        return Double.longBitsToDouble(readLong0());
    }

    public String readLine() {
        if (this.ioz == null) {
            this.ioz = new char[128];
        }
        int length = this.ioz.length;
        int count = 0;
        char[] cArr = this.ioz;
        Label_0173:
        while (true) {
            int read = read();
            switch (read) {
                case -1:
                    if (count == 0) {
                        return null;
                    }
                    break Label_0173;
                case 10:
                    break Label_0173;
                case 13:
                    int read2 = read();
                    if (read2 != 10 && read2 != -1) {
                        this.pos--;
                        break Label_0173;
                    }
                    break Label_0173;
                default:
                    length--;
                    if (length < 0) {
                        this.ioz = new char[(count + 128)];
                        length = (this.ioz.length - count) - 1;
                        System.arraycopy(cArr, 0, this.ioz, 0, count);
                        cArr = this.ioz;
                    }
                    cArr[count] = (char) read;
                    count++;
            }
        }
        return String.copyValueOf(this.ioz, 0, count);
    }

    public String readUTF() throws UTFDataFormatException, EOFException {
        final int unsignedShort = this.readUnsignedShort();
        this.ensure(this.pos + unsignedShort);
        if (this.ioz == null || this.ioz.length < unsignedShort) {
            this.ioz = new char[unsignedShort * 2];
        }
        int i = 0;
        int count = 0;
        final int pos = this.pos;
        final byte[] buf = this.buf;
        this.pos += unsignedShort;
        while (true) {
            while (i < unsignedShort) {
                final int n = buf[pos + i] & 0xFF;
                if (n > 127) {
                    while (i < unsignedShort) {
                        final int n2 = buf[pos + i] & 0xFF;
                        switch (n2 >> 4) {
                            case 0:
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                            case 6:
                            case 7: {
                                ++i;
                                this.ioz[count++] = (char) n2;
                                continue;
                            }
                            case 12:
                            case 13: {
                                i += 2;
                                if (i > unsignedShort) {
                                    throw new UTFDataFormatException("malformed input: partial character at end");
                                }
                                final byte b = buf[pos + i - 1];
                                if ((b & 0xC0) != 0x80) {
                                    throw new UTFDataFormatException("malformed input around byte " + i);
                                }
                                this.ioz[count++] = (char) ((n2 & 0x1F) << 6 | (b & 0x3F));
                                continue;
                            }
                            case 14: {
                                i += 3;
                                if (i > unsignedShort) {
                                    throw new UTFDataFormatException("malformed input: partial character at end");
                                }
                                final byte b2 = buf[pos + i - 2];
                                final byte b3 = buf[pos + i - 1];
                                if ((b2 & 0xC0) != 0x80 || (b3 & 0xC0) != 0x80) {
                                    throw new UTFDataFormatException("malformed input around byte " + (i - 1));
                                }
                                this.ioz[count++] = (char) ((n2 & 0xF) << 12 | (b2 & 0x3F) << 6 | (b3 & 0x3F) << 0);
                                continue;
                            }
                            default: {
                                throw new UTFDataFormatException("malformed input around byte " + i);
                            }
                        }
                    }
                    return new String(this.ioz, 0, count);
                }
                ++i;
                this.ioz[count++] = (char) n;
            }
            continue;
        }
    }

    public String toString() {
        return Syst.byteToStringLog(this.buf, 0, this.buf.length, " ", 16);
    }
}
