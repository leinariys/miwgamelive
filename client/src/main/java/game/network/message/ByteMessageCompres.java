package game.network.message;

import game.engine.SocketMessage;
import game.network.channel.client.ClientConnect;

/* renamed from: a.JB */
/* compiled from: a */
public final class ByteMessageCompres extends ByteMessage {
    private boolean compressed;
    private int originalLength;
    private boolean special;

    public ByteMessageCompres(byte[] bArr, int i, int i2) {
        super(bArr, i, i2);
    }

    public ByteMessageCompres(ClientConnect bVar, byte[] bArr) {
        super(bVar, bArr);
    }

    public ByteMessageCompres(byte[] message) {
        super(message);
    }

    public ByteMessageCompres(SocketMessage ala) {
        super(ala);
    }

    public boolean isSpecial() {
        return this.special;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

    public boolean isCompressed() {
        return this.compressed;
    }

    /* renamed from: cT */
    public void setCompressed(boolean compressed) {
        this.compressed = compressed;
    }

    public int getOriginalLength() {
        return this.originalLength;
    }

    /* renamed from: le */
    public void setOriginalLength(int i) {
        this.originalLength = i;
    }
}
