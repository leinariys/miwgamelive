package game.network;

import game.engine.SocketMessage;
import game.network.message.ByteMessageReader;

import java.io.EOFException;
import java.io.Serializable;

/* renamed from: a.xu */
/* compiled from: a */
public abstract class Transport implements Serializable {
    public static final int BROADCAST_COMMAND = 2;
    public static final int BROADCAST_RESPONSE = 3;
    public static final int TRANSPORT_COMMAND = 0;
    public static final int TRANSPORT_RESPONSE = 1;

    /* renamed from: in */
    private final ByteMessageReader byteMessageReader;
    private final SocketMessage out;
    private boolean finished;
    private boolean hasWritten;
    private int messageId;
    private transient long timeReadMesage;
    private int threadId;

    public Transport() {
        this.threadId = 0;
        this.out = new SocketMessage(128);
        this.byteMessageReader = null;
    }

    public Transport(ByteMessageReader amf) throws EOFException {
        this.threadId = 0;
        this.messageId = amf.readInt();
        if (amf.readBoolean()) {
            this.threadId = this.messageId;
        } else {
            this.threadId = amf.readInt();
        }
        this.byteMessageReader = amf;
        this.out = null;
    }

    public abstract int getType();

    public final int getMessageId() {
        return this.messageId;
    }

    public final void setMessageId(int i) {
        if (this.hasWritten) {
            throw new IllegalStateException("Cannot change message id after writing.");
        }
        this.messageId = i;
    }

    public final int getThreadId() {
        return this.threadId;
    }

    public final void setThreadId(int i) {
        if (this.hasWritten) {
            throw new IllegalStateException("Cannot change thread id after writing.");
        }
        this.threadId = i;
    }

    public final boolean isBlocking() {
        return getThreadId() != 0;
    }

    public final long getTimeReadMesage() {
        return this.timeReadMesage;
    }

    public final void setTimeReadMesage(long j) {
        this.timeReadMesage = j;
    }

    public final ByteMessageReader getByteMessageReader() {
        return this.byteMessageReader;
    }

    public final SocketMessage getOutputStream() {
        this.hasWritten = true;
        if (getType() > BROADCAST_RESPONSE) {
            throw new IllegalArgumentException("Invalid transport message type (bigger than 3)");
        }
        this.out.writeBits(BROADCAST_COMMAND, getType());
        this.out.writeInt(getMessageId());
        if (getMessageId() == getThreadId()) {
            this.out.writeBoolean(true);
        } else {
            this.out.writeBoolean(false);
            this.out.writeInt(getThreadId());
        }
        return this.out;
    }

    public final byte[] toByteArray() {
        finish();
        return this.out.toByteArray();
    }

    public final byte[] getOutBuffer() {
        finish();
        return this.out.getMessage();
    }

    private void finish() {
        if (!this.finished) {
            this.finished = true;
        }
    }

    public final int getOutLength() {
        return this.out.getCount();
    }

    public String toString() {
        StringBuilder append = new StringBuilder(getClass().getName()).append(" ").append(this.messageId).append(" ").append(this.threadId);
        if (this.out != null) {
            append.append("\r\n").append(this.out);
        } else if (this.byteMessageReader != null) {
            append.append("\r\n").append(this.byteMessageReader);
        }
        return append.toString();
    }
}
