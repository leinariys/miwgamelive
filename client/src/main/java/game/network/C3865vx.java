package game.network;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.vx */
/* compiled from: a */
public class C3865vx implements Comparable<C3865vx> {

    final String bzv = "                                                                                                                                                        ";
    /* renamed from: UM */
    List<Integer> f9439UM;
    /* renamed from: UN */
    List<Boolean> f9440UN;
    String[] bzt;
    List<String[]> bzu = new ArrayList();
    String bzw;

    public C3865vx(List<?>[] listArr) {
        Object obj;
        Object obj2;
        int i = 1;
        for (List<?> list : listArr) {
            if (list.size() > i) {
                i = list.size();
            }
        }
        this.bzt = new String[listArr.length];
        for (int i2 = 0; i2 < this.bzt.length; i2++) {
            if (listArr[i2].size() >= 1) {
                obj2 = listArr[i2].get(0);
            } else {
                obj2 = null;
            }
            this.bzt[i2] = obj2 == null ? "" : obj2.toString();
        }
        for (int i3 = 1; i3 < i; i3++) {
            String[] strArr = new String[listArr.length];
            this.bzu.add(strArr);
            for (int i4 = 0; i4 < listArr.length; i4++) {
                if (listArr[i4].size() > i3) {
                    obj = listArr[i4].get(i3);
                } else {
                    obj = null;
                }
                strArr[i4] = obj == null ? "" : obj.toString();
            }
        }
    }

    /* renamed from: a */
    public String mo22680a(List<Boolean> list, List<Integer> list2) {
        return mo22681a(this.bzt, list, list2);
    }

    /* renamed from: a */
    public String mo22681a(String[] strArr, List<Boolean> list, List<Integer> list2) {
        String str;
        this.f9440UN = list;
        this.f9439UM = list2;
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= strArr.length) {
                return sb.toString();
            }
            if (strArr[i2] == null) {
                strArr[i2] = "";
            }
            String substring = "                                                                                                                                                        ".substring((strArr[i2].length() + "                                                                                                                                                        ".length()) - list2.get(i2).intValue());
            if (list.get(i2).booleanValue()) {
                str = String.valueOf(substring) + strArr[i2];
            } else {
                str = String.valueOf(strArr[i2]) + substring;
            }
            sb.append(" | ").append(str);
            i = i2 + 1;
        }
    }

    public String toString() {
        try {
            if (this.bzw == null) {
                this.bzw = mo22681a(this.bzt, this.f9440UN, this.f9439UM);
            }
            return this.bzw;
        } catch (RuntimeException e) {
            return "NOT READY!";
        }
    }

    /* renamed from: b */
    public String mo22682b(StringBuilder sb) {
        sb.append("\n").append(toString());
        for (String[] a : this.bzu) {
            sb.append("\n").append(mo22681a(a, this.f9440UN, this.f9439UM));
        }
        return sb.toString();
    }

    /* renamed from: a */
    public int compareTo(C3865vx vxVar) {
        return toString().compareTo(vxVar.toString());
    }
}
