package game.network;

import org.apache.commons.jxpath.ClassFunctions;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.ii */
/* compiled from: a */
public interface C2695ii {
    /* renamed from: Bh */
    ClassFunctions mo10861Bh();

    /* renamed from: Y */
    void mo10862Y(boolean z);

    /* renamed from: a */
    String mo10863a(C0165Bs bs);

    /* renamed from: a */
    String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map);

    /* renamed from: a */
    void mo10865a(ClassFunctions classFunctions);

    /* renamed from: b */
    String mo10866b(C0165Bs bs);
}
