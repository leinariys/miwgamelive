package game.network.channel.server;

import game.network.channel.InteractionSocket;
import game.network.channel.client.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/* renamed from: a.kz */
/* compiled from: a */
public class HandlerServerState extends HandlerServer {
    private ClientConnect.Attitude attitude;
    private InteractionSocket interactionSocket;
    private int[] ip1;
    private int[] ip2;
    private boolean authentication;

    public HandlerServerState(ServerSocketChannel serverSocketChannel, ClientConnect.Attitude attitude, InteractionSocket dmVar, int[] ip1, int[] ip2) {
        this(serverSocketChannel, attitude, dmVar, ip1, ip2, true);
    }

    public HandlerServerState(ServerSocketChannel serverSocketChannel, ClientConnect.Attitude attitude, InteractionSocket interactionSocket, int[] ip1, int[] ip2, boolean authentication) {
        super(serverSocketChannel);
        this.authentication = true;
        this.attitude = attitude;
        this.interactionSocket = interactionSocket;
        this.authentication = authentication;
        if (ip1 == null || ip1.length != 4) {
            this.ip1 = null;
        } else {
            this.ip1 = ip1;
        }
        if (ip2 == null || ip2.length != 4) {
            this.ip2 = null;
        } else {
            this.ip2 = ip2;
        }
    }

    public HandlerServerState(ServerSocketChannel serverSocketChannel, ClientConnect.Attitude attitude, InteractionSocket interactionSocket) {
        super(serverSocketChannel);
        this.authentication = true;
        this.attitude = attitude;
        this.interactionSocket = interactionSocket;
        this.ip1 = null;
        this.ip2 = null;
    }

    /* renamed from: KB */
    public InteractionSocket getInteractionSocket() {
        return this.interactionSocket;
    }

    /* renamed from: a */
    public void setInteractionSocket(InteractionSocket dmVar) {
        this.interactionSocket = dmVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public HandlerClient initHandlerClientState(SocketChannel socketChannel) {
        ClientConnectImpl a = ClientConnectBuilder.build(this.attitude, socketChannel.socket().getInetAddress());
        a.setInfo(socketChannel.toString());
        HandlerClientState lz = new HandlerClientState(a, this.interactionSocket, socketChannel, this.serverSocketChannel.socket().getLocalPort(), this.authentication);
        a.setHandlerClientState(lz);
        return lz;
    }

    /* access modifiers changed from: protected */
    /* renamed from: KC */
    public void addChannel() throws IOException {
        SocketChannel accept = this.serverSocketChannel.accept();
        InetSocketAddress inetSocketAddress = (InetSocketAddress) accept.socket().getRemoteSocketAddress();
        if (this.ip1 == null && this.ip2 == null) {
            accept.configureBlocking(false);
            this.selectableChannelThread.сhannelRegister(initHandlerClientState(accept));
            return;
        }
        byte[] address = inetSocketAddress.getAddress().getAddress();
        for (int i = 0; i < 4; i++) {
            if ((address[i] & this.ip2[i]) != (this.ip1[i] & this.ip2[i])) {
                accept.close();
                return;
            }
        }
        accept.configureBlocking(false);
        this.selectableChannelThread.сhannelRegister(initHandlerClientState(accept));
    }

    /* renamed from: KD */
    public int[] getIp1() {
        return this.ip1;
    }

    /* renamed from: KE */
    public int[] getIp2() {
        return this.ip2;
    }
}
