package game.network.channel.server;

import game.network.channel.HandlerChannel;
import game.network.channel.client.HandlerClient;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/* renamed from: a.awd  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class HandlerServer extends HandlerChannel {
    ServerSocketChannel serverSocketChannel;

    public HandlerServer(ServerSocketChannel serverSocketChannel) {
        this.serverSocketChannel = serverSocketChannel;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract HandlerClient initHandlerClientState(SocketChannel socketChannel);

    /* renamed from: b */
    public void isReadyKeyChannelForOperations(SelectionKey selectionKey) throws IOException {
        if (selectionKey.isAcceptable()) {
            addChannel();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: KC */
    public void addChannel() throws IOException {
        SocketChannel clientChannel = this.serverSocketChannel.accept();
        clientChannel.configureBlocking(false);
        this.selectableChannelThread.сhannelRegister((HandlerChannel) initHandlerClientState(clientChannel));
    }

    /* access modifiers changed from: package-private */
    public SelectableChannel getChannel() {
        return this.serverSocketChannel;
    }
}
