package game.network.channel;

import game.network.message.ByteMessage;

/* renamed from: a.UV */
/* compiled from: a */
public interface MessageProcessing {
    void close();

    /* renamed from: jG */
    ByteMessage getTransportMessage() throws InterruptedException;
}
