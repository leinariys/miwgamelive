package game.network.channel;

import game.network.channel.client.ClientConnect;
import game.network.exception.ChechUserException;

/* renamed from: a.om */
/* compiled from: a */
public interface Connect {

    /* renamed from: a */
    void chechUser(ClientConnect bVar, String userName, String pass, boolean isForcingNewUser) throws ChechUserException;

    /* renamed from: d */
    void userDisconnected(ClientConnect bVar);

    /* renamed from: a.om$a */
    public enum Status {
        OK,
        INVALID_USERNAME_PASSWORD,
        USER_ALREADY_LOGGED_IN,
        ERROR,
        USER_NOT_ACTIVATED,
        USER_BANNED,
        INVALID_HANDSHAKE,
        HAS_NOT_CREDITS,
        USER_DELETED,
        SERVER_FULL
    }
}
