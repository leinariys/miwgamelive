package game.network.channel;

import game.network.thread.SelectableChannelThread;

import java.io.IOException;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

/* renamed from: a.XF */
/* compiled from: a */
public abstract class HandlerChannel {
    /* renamed from: yQ */
    public SelectableChannelThread selectableChannelThread;
    private SelectionKey selectionKey;

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void isReadyKeyChannelForOperations(SelectionKey selectionKey) throws IOException;

    /* access modifiers changed from: package-private */
    public abstract SelectableChannel getChannel();

    /* access modifiers changed from: protected */
    public final void addSelectionKeyToThread() {
        this.selectableChannelThread.signalReadyReadAndWrite(this.selectionKey);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void initHandlerChannel(SelectionKey selectionKey, SelectableChannelThread selectableChannelThread) {
        this.selectionKey = selectionKey;
        this.selectableChannelThread = selectableChannelThread;
    }

    /* access modifiers changed from: package-private */
    public int prochentResultingKey() {
        if (getChannel() instanceof SocketChannel) {
            return 5; //SelectionKey.OP_READ + SelectionKey.OP_WRITE
        }
        if (getChannel() instanceof ServerSocketChannel) {
            return SelectionKey.OP_ACCEPT;
        }
        throw new IllegalStateException();
    }

    public void sendHandshake1() throws IOException {
    }

    public void stepDisconnected() {
    }

    public SelectionKey getSelectionKey() {
        return this.selectionKey;
    }

    /* renamed from: c */
    public void setSelectionKey(SelectionKey selectionKey) {
        this.selectionKey = selectionKey;
    }
}
