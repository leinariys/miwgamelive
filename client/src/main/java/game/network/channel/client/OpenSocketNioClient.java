package game.network.channel.client;

import game.engine.SocketMessage;
import game.network.channel.Connect;
import game.network.channel.HandlerChannel;
import game.network.channel.InteractionSocket;
import game.network.exception.*;
import game.network.message.ByteMessageCompres;
import game.network.message.ByteMessageReader;
import logic.thred.LogPrinter;

import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: a.aqx  reason: case insensitive filesystem */
/* compiled from: a */
public class OpenSocketNioClient extends InteractionSocket {
    static LogPrinter logger = LogPrinter.m10275K(OpenSocketNioClient.class);

    /* renamed from: xK */
    private static /* synthetic */ int[] f5212xK;
    private final boolean authentication;
    private boolean isForcingNewUser;
    private int countServerFull;
    private int idStatus;
    private ClientConnectImpl gqI;
    private HandlerClientState gqJ;
    private CyclicBarrier cyclicBarrier;
    private String password;
    private String username;

    /* renamed from: xI */
    private long clientTimeAuthorization;

    public OpenSocketNioClient(String host, int port, String userName, String password, boolean isForcingNewUser, String ckientVersion) throws IOExceptionServer {
        this(host, port, userName, password, isForcingNewUser, ckientVersion, true);
    }

    public OpenSocketNioClient(String host, int port, String userName, String password, boolean isForcingNewUser, String ckientVersion, boolean authentication) throws IOExceptionServer {
        this.cyclicBarrier = new CyclicBarrier(2);
        this.authentication = authentication;
        this.username = userName;
        this.password = password;
        this.isForcingNewUser = isForcingNewUser;
        logger.info("Opening channel");

        try (SocketChannel channel = SocketChannel.open()) {
            logger.info("Trying to connect to " + host + ":" + port);
            channel.connect(new InetSocketAddress(host, port));
            this.ipRemoteHost = InetAddress.getByName(host);
            channel.configureBlocking(false);
            logger.info("Channel opened");
            this.versionString = ckientVersion;
            this.gqI = ClientConnectBuilder.build(ClientConnect.Attitude.PARENT, channel.socket().getInetAddress());

            this.gqJ = new HandlerClientState(this.gqI, this, channel, port);
            this.selectableChannelThread.сhannelRegister((HandlerChannel) this.gqJ);
            this.selectableChannelThread.start();

            try {
                logger.info("Waiting connection.");
                this.cyclicBarrier.await(30, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e2) {
                e2.printStackTrace();
            } catch (TimeoutException e3) {
                throw new IOExceptionServer("Timeout waiting for network layer, server is not responding.");
            }
            if (authentication) {
                switch (Connect.Status.values()[this.idStatus].ordinal()) {
                    case 0:
                        break;
                    case 1:
                        throw new InvalidUsernamePasswordException();
                    case 2:
                        throw new ClientAlreadyConnectedException();
                    case 3:
                        throw new ServerLoginException();
                    case 4:
                        throw new UserAccountNotActivatedException();
                    case 5:
                        throw new UserBannedException();
                    case 6:
                        throw new InvalidHandshakeException();
                    case 7:
                        throw new HasNotCreditsException();
                    case 8:
                        throw new UserDeletedException();
                    case 9:
                        throw new ServerFullException(this.countServerFull);
                    default:
                        throw new RuntimeException("TcpClientNetwork protocol error!");
                }
            }
        } catch (Exception e4) {
            throw new IOExceptionServer(String.valueOf(host) + ":" + port, (IOException) e4);
        }
    }

    /* renamed from: a */
    public void receivedByteMessage(HandlerClientState clientState, ByteMessageCompres messageCompres) throws IOException, InterruptedException {
        if (clientState.getCurrentStepLoging() != HandlerClientState.StepLoging.UP) {
            logger.info("Received: " + messageCompres.getLength() + " bytes " + clientState.getCurrentStepLoging());
        }
        super.receivedByteMessage(clientState, messageCompres);
    }

    /* renamed from: a */
    public void sendTransportMessage(ClientConnect bVar, byte[] bArr, int i, int i2) throws IOException {
        if (this.gqJ.getCurrentStepLoging() != HandlerClientState.StepLoging.UP) {
            logger.info("Sending: " + i2 + " bytes " + this.gqJ.getCurrentStepLoging());
        }
        if (bVar == null) {
            bVar = this.gqI;
        }
        super.sendTransportMessage(bVar, bArr, i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void processingAuthentication(HandlerClientState lz, ByteMessageCompres jb) throws EOFException {
        ByteMessageReader amf = new ByteMessageReader(jb.getMessage(), jb.getOffset(), jb.getLength());
        amf.readInt();//StepLoging
        int сonnectStatus = amf.readInt();
        this.countServerFull = amf.readInt() + 1;
        this.idStatus = сonnectStatus;
        if (сonnectStatus == Connect.Status.OK.ordinal()) {
            processingUp(lz);
            return;
        }
        lz.setCurrentStepLoging(HandlerClientState.StepLoging.CLOSING);
        try {
            this.cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e2) {
            e2.printStackTrace();
        }
        lz.setStepClosing();
        close();
    }

    /* renamed from: f */
    private void processingUp(HandlerClientState lz) {
        lz.setCurrentStepLoging(HandlerClientState.StepLoging.UP);
        addAuthorizationSocket(lz);
        this.clientTimeAuthorization = System.currentTimeMillis();
        try {
            this.cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: R */
    public void setStatusInvalidHandshake(String str) {
        super.setStatusInvalidHandshake(str);
        this.idStatus = Connect.Status.INVALID_HANDSHAKE.ordinal();
        awaitCyclicBarrier();
    }

    private void awaitCyclicBarrier() {
        if (!this.cyclicBarrier.isBroken()) {
            try {
                this.cyclicBarrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e2) {
                e2.printStackTrace();
            }
        } else {
            System.out.println("broken barrier");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void processingHandshake2(HandlerClientState lz, ByteMessageCompres byteMessage) throws IOException {
        if (checkMessageHandshake2(HandlerClientState.StepLoging.HANDSHAKE2, byteMessage) != 0) {
            lz.setStepClosing();
            this.idStatus = Connect.Status.INVALID_HANDSHAKE.ordinal();
            awaitCyclicBarrier();
        } else if (this.authentication) {
            SocketMessage ala = new SocketMessage();
            ala.writeUTF(this.username);
            ala.writeUTF(this.password);
            ala.writeBoolean(this.isForcingNewUser);
            byte[] byteArray = ala.toByteArray();
            lz.setCurrentStepLoging(HandlerClientState.StepLoging.AUTHENTICATION);
            saveMessageToFileBeforeSending(lz, new ByteMessageCompres(byteArray));
        } else {
            processingUp(lz);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void saveMessageToFileBeforeSending(HandlerClientState lz, ByteMessageCompres message) throws IOException {
        if (lz.getCurrentStepLoging() != HandlerClientState.StepLoging.UP) {
            logger.info("Sending: " + message.getLength() + " bytes " + lz.getCurrentStepLoging());
        }
        super.saveMessageToFileBeforeSending(lz, message);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void signalDisconnected(HandlerClientState lz) {
        try {
            super.signalDisconnected(lz);
            /*
            try {
            } catch (IOException e) {
                e.printStackTrace();
            }*/
        } finally {
            //try {
            this.selectableChannelThread.close();
            //} catch (IOException e2) {
            //    e2.printStackTrace();
            //}
        }
    }

    /* renamed from: jf */
    public long metricReadByte() {
        return (long) this.gqJ.getMetricReadByte();
    }

    /* renamed from: je */
    public long metricByteLength() {
        return (long) this.gqJ.getByteLength();
    }

    /* renamed from: jc */
    public long metricCountReadMessage() {
        return (long) this.gqJ.getCountReadMessage();
    }

    /* renamed from: jg */
    public long metricWriteByte() {
        return (long) this.gqJ.getMetricWriteByte();
    }

    /* renamed from: jd */
    public long metricAllOriginalByteLength() {
        return (long) this.gqJ.getAllOriginalByteLength();
    }

    /* renamed from: jb */
    public long metricCountWriteMessage() {
        return (long) this.gqJ.getCountWriteMessage();
    }

    /* renamed from: ja */
    public long metricClientTimeAuthorization() {
        return this.clientTimeAuthorization;
    }
}
