package game.network.channel.client;

import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: a.mP */
/* compiled from: a */
public class ClientConnectBuilder {
    private static final AtomicInteger customerCounter = new AtomicInteger();

    /* renamed from: a */
    public static synchronized ClientConnectImpl build(ClientConnect.Attitude attitude, InetAddress inetAddress) {
        ClientConnectImpl clientConnect;
        synchronized (ClientConnectBuilder.class) {
            clientConnect = new ClientConnectImpl(attitude, customerCounter.incrementAndGet(), inetAddress);
        }
        return clientConnect;
    }
}
