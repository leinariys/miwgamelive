package game.network.channel.client;

import game.network.channel.HandlerChannel;

import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/* renamed from: a.yV */
/* compiled from: a */
public abstract class HandlerClient extends HandlerChannel {
    private final SocketChannel channel;

    public HandlerClient(SocketChannel socketChannel) {
        this.channel = socketChannel;
    }

    /* renamed from: a */
    public abstract void eventIsOpConnect(SelectionKey selectionKey);

    /* access modifiers changed from: package-private */
    public SelectableChannel getChannel() {
        return this.channel;
    }
}
