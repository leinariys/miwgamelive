package game.network.channel;

import game.network.channel.client.HandlerClient;
import game.network.thread.SelectableChannelThread;
import gnu.trove.TObjectProcedure;

import java.nio.channels.SelectionKey;

/* renamed from: a.aCX */
/* compiled from: a */
public class MessageHandler implements TObjectProcedure<SelectionKey> {
    final /* synthetic */ SelectableChannelThread selectableChannelThread;

    public MessageHandler(SelectableChannelThread agx) {
        this.selectableChannelThread = agx;
    }

    /* renamed from: e */
    public boolean execute(SelectionKey selectionKey) {
        try {
            if ((selectionKey.interestOps() & 4) == 0) {//SelectionKey.OP_CONNECT
                ((HandlerClient) selectionKey.attachment()).eventIsOpConnect(selectionKey);
                return true;
            }
            selectionKey.interestOps(5);//SelectionKey.OP_READ + SelectionKey.OP_WRITE
            return true;
        } catch (Throwable th) {
            this.selectableChannelThread.exceptionHandling(th, selectionKey);
            return true;
        }
    }
}
