package game.network;

import logic.thred.LogPrinter;
import logic.ui.item.ListJ;
import org.apache.commons.jxpath.ClassFunctions;
import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.logging.Log;
import p001a.C0764Ku;
import p001a.C7004ayr;

import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/* renamed from: a.Bs */
/* compiled from: a */
public abstract class C0165Bs {
    public static final Log clO = LogPrinter.setClass(C0165Bs.class);
    public static Class<? extends C0165Bs> clP = C0165Bs.class;
    public JXPathContext clQ;
    //Имя временной переменной для хранения возврата команды.
    @C2881lP("Nome da Variável Temporária para guardar o retorno do Comando.")
    public String clR;
    //Показать эти атрибуты. Значение по умолчанию - "имя".
    @C2881lP("Show this Attributes. The default value is 'name'")
    @C3221pP
    public List<String> clS = new ArrayList();
    public String clT;
    public C3725uE clW;
    //Установить метки для выходовУстановить метки для выходов
    @C2881lP("Set Labels to Outputs")
    @C3221pP
    public List<String> labels = new ArrayList();
    public Log log = clO;
    public Object root;
    private List<Field> clU = new ArrayList();
    private List<Field> clV = new ArrayList();

    public C0165Bs() {
        this.clS.add("name");
        this.clT = getClass().getName().replace("Command", "");
        this.clT = this.clT.replace(clP.getPackage().getName(), "").substring(1);
    }

    /* renamed from: a */
    public static C0165Bs m1346a(C3725uE uEVar, String str, Object obj, Map<String, Object> map) {
        return m1347a(uEVar, str, obj, map, (Class<? extends C0165Bs>) C0165Bs.class);
    }

    /* renamed from: a */
    public static C0165Bs m1347a(C3725uE uEVar, String str, Object obj, Map<String, Object> map, Class<? extends C0165Bs> cls) {
        try {
            C0165Bs bs = (C0165Bs) cls.getClassLoader().loadClass(String.valueOf(cls.getPackage().getName()) + "." + (String.valueOf(str) + "Command")).newInstance();
            bs.clQ = JXPathContext.newContext(obj);
            bs.clW = uEVar;
            bs.m1353g(map);
            bs.root = obj;
            bs.log = LogPrinter.setClass(bs.getClass());
            return bs;
        } catch (Throwable th) {
            th.printStackTrace();
            throw new C7004ayr(th);
        }
    }

    /* renamed from: a */
    public static String m1348a(String str, C3725uE uEVar, Object obj, Map<String, Object> map, Class<? extends C0165Bs> cls) {
        if (cls != null) {
            clP = cls;
        }
        ArrayList arrayList = new ArrayList();
        String str2 = "";
        try {
            StringTokenizer stringTokenizer = new StringTokenizer(str, " ");
            String nextToken = stringTokenizer.nextToken();
            if (stringTokenizer.hasMoreTokens()) {
                str2 = stringTokenizer.nextToken();
            }
            while (stringTokenizer.hasMoreTokens()) {
                arrayList.add(stringTokenizer.nextToken());
            }
            String[] strArr = new String[arrayList.size()];
            C0165Bs a = m1347a(uEVar, str2, obj, (Map<String, Object>) new TreeMap(), clP);
            if (!nextToken.equals("exec")) {
                return a.ayS();
            }
            a.mo868e((String[]) arrayList.toArray(strArr));
            return a.ayP();
        } catch (Exception e) {
            clO.error(e);
            return e.getMessage();
        }
    }

    public abstract String ayL();

    /* access modifiers changed from: protected */
    public abstract void ayM();

    /* access modifiers changed from: protected */
    public abstract void ayN();

    /* access modifiers changed from: protected */
    public abstract void ayO();

    /* JADX INFO: finally extract failed */
    public String ayP() {
        ayM();
        try {
            ayN();
            String ayL = ayL();
            ayO();
            if (this.clR != null) {
                this.clW.ajt().put(this.clR, ayL);
            }
            return ayL;
        } catch (Throwable th) {
            ayO();
            throw th;
        }
    }

    public String ayQ() {
        return this.clT;
    }

    /* renamed from: g */
    private void m1353g(Map<String, Object> map) {
        m1349a((Class<? extends C0165Bs>) getClass(), map);
    }

    /* renamed from: e */
    private Field m1352e(Class<? extends Object> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Exception e) {
            Class<? super Object> superclass = (Class<? super Object>) cls.getSuperclass();
            if (superclass != Object.class) {
                return m1352e(superclass, str);
            }
            e.printStackTrace();
            throw new RuntimeException("Error: Invalid field '" + str + "'", e);
        }
    }

    /* renamed from: a */
    private void m1349a(Class<? extends C0165Bs> cls, Map<String, Object> map) {
        for (Map.Entry next : map.entrySet()) {
            m1351a((Map.Entry<String, Object>) next, m1352e(cls, (String) next.getKey()));
        }
    }

    /* renamed from: a */
    private void m1351a(Map.Entry<String, Object> entry, Field field) {
        boolean isAccessible = field.isAccessible();
        field.setAccessible(true);
        try {
            field.set(this, entry.getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        field.setAccessible(isAccessible);
    }

    /* renamed from: e */
    public void mo868e(String[] strArr) {
        Field field;
        ayR();
        ArrayList arrayList = new ArrayList();
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            String str = strArr[i];
            StringTokenizer stringTokenizer = new StringTokenizer(str, "=");
            try {
                String nextToken = stringTokenizer.nextToken();
                String decode = URLDecoder.decode(stringTokenizer.nextToken(), "UTF-8");
                try {
                    if (nextToken.equals("store") || nextToken.equals("output") || nextToken.equals("labels")) {
                        field = C0165Bs.class.getDeclaredField(nextToken);
                    } else {
                        field = getClass().getDeclaredField(nextToken);
                    }
                    if (field == null) {
                        throw new C7004ayr("Invalid parameter name: " + nextToken + " (" + str + ")");
                    }
                    try {
                        m1350a((List<Field>) arrayList, field, decode);
                        i++;
                    } catch (C7004ayr e) {
                        throw e;
                    } catch (Exception e2) {
                        throw new C7004ayr("Fail setting parameter value (" + str + ").", e2);
                    }
                } catch (Exception e3) {
                    throw new C7004ayr("Invalid parameter name: " + nextToken + " (" + str + ")", e3);
                }
            } catch (Exception e4) {
                throw new C7004ayr(str, e4);
            }
        }
        if (this.clU.size() > arrayList.size()) {
            throw new C7004ayr("Not found some required parameters. " + getClass());
        }
    }

    private void ayR() {
        for (Field field : getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(C0764Ku.class)) {
                this.clU.add(field);
            }
        }
    }

    /* renamed from: a */
    private void m1350a(List<Field> list, Field field, String str) throws IllegalAccessException, ParseException {
        if (field.getAnnotation(C3221pP.class) == null) {
            list.add(field);
        }
        boolean isAccessible = field.isAccessible();
        field.setAccessible(true);
        if (!str.startsWith("${") || !str.endsWith("}")) {
            Class<?> type = field.getType();
            if (type == String.class) {
                field.set(this, str);
            } else if (type == Integer.class || type == Integer.TYPE) {
                field.set(this, new Integer(str));
            } else if (type == Double.class || type == Double.TYPE) {
                field.set(this, new Double(str));
            } else if (type == Boolean.class || type == Boolean.TYPE) {
                field.set(this, new Boolean(str));
            } else if (type == Long.class || type == Long.TYPE) {
                field.set(this, new Long(str));
            } else if (type == Date.class) {
                C2884lS lSVar = (C2884lS) field.getAnnotation(C2884lS.class);
                field.set(this, new SimpleDateFormat(lSVar == null ? "dd/MM/yyyy" : lSVar.value()).parse(str));
            } else if (type == ListJ.class) {
                field.set(this, Arrays.asList(str.substring(1).split(str.substring(0, 1))));
            }
            field.setAccessible(isAccessible);
            return;
        }
        String substring = str.substring(2, str.length() - 1);
        if (this.clW.ajt().containsKey(substring)) {
            field.set(this, this.clW.ajt().get(substring));
            return;
        }
        throw new C7004ayr("Not Found Variable Named '" + substring + "'.");
    }

    /* renamed from: a */
    public String mo847a(String str, List<String> list) {
        return mo848a(str, list, true);
    }

    /* renamed from: d */
    public String mo865d(Iterator<?> it) {
        int i = 0;
        HashMap hashMap = new HashMap();
        if (this.clS.size() != this.labels.size()) {
            while (true) {
                int i2 = i;
                if (i2 >= this.clS.size()) {
                    break;
                }
                hashMap.put(this.clS.get(i2), this.clS.get(i2));
                i = i2 + 1;
            }
        } else {
            while (true) {
                int i3 = i;
                if (i3 >= this.clS.size()) {
                    break;
                }
                hashMap.put(this.clS.get(i3), this.labels.get(i3));
                i = i3 + 1;
            }
        }
        return this.clW.aju().mo2073a(it, this.clS, hashMap);
    }

    public String ayS() {
        return this.clW.aju().mo10863a(this);
    }

    public String ayT() {
        return this.clW.aju().mo10866b(this);
    }

    public C3725uE ayU() {
        return this.clW;
    }

    /* renamed from: a */
    public void mo849a(C3725uE uEVar) {
        this.clW = uEVar;
    }

    public List<Field> ayV() {
        return this.clU;
    }

    public List<Field> ayW() {
        return this.clV;
    }

    /* renamed from: a */
    public String mo848a(String str, List<String> list, boolean z) {
        String str2 = "";
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Iterator<String> it = list.iterator();
        while (true) {
            String str3 = str2;
            if (!it.hasNext()) {
                sb.append("]");
                return sb.toString();
            }
            sb.append(str3).append(str).append("='").append(it.next()).append("'");
            str2 = " or ";
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public String mo863b(String str, List<String> list) {
        String a = mo848a(str, list, false);
        if (a.length() > 0) {
            return " and (" + a + ")";
        }
        return a;
    }

    /* access modifiers changed from: protected */
    public void ayX() {
        mo864b(new ClassFunctions(getClass(), "tkd"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: dG */
    public void mo866dG(String str) {
        mo864b(new ClassFunctions(getClass(), str));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo864b(ClassFunctions classFunctions) {
        this.clQ.setFunctions(classFunctions);
        this.clW.aju().mo10865a(classFunctions);
    }

    public void ayY() {
        this.clS.clear();
        this.labels.clear();
    }

    /* renamed from: x */
    public void mo869x(String str, String str2) {
        this.clS.add(str);
        this.labels.add(str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: dH */
    public void mo867dH(String str) {
        this.clS.add(str);
        this.labels.add(str);
    }
}
