package game.network.manager;

import game.engine.DataGameEvent;
import game.engine.SocketMessage;
import game.io.IExternalIO;
import logic.baa.C1616Xf;
import p001a.C5496aLo;
import p001a.C6744aso;
import p001a.aAD;

import java.io.*;

/* renamed from: a.FM */
/* compiled from: a */
public class C0387FM extends OutputStream implements C6744aso, ObjectOutput {
    private final DataGameEvent aGA;
    private boolean aGB;
    private int aGC;
    private boolean aVN;
    private DataOutput cWs;
    private aAD cWt;
    private boolean cWu;
    private OutputStream out;

    public C0387FM(DataGameEvent jz, DataOutput dataOutput, OutputStream outputStream, boolean z, boolean z2) {
        this.aGB = false;
        this.cWu = false;
        this.aGC = 255;
        this.aVN = false;
        this.aGA = jz;
        this.cWs = dataOutput;
        this.out = outputStream;
        this.cWu = z2;
        this.aGB = z;
    }

    public C0387FM(DataGameEvent jz, SocketMessage ala, boolean z, boolean z2, int i, boolean z3) {
        this(jz, ala, ala, z, z2);
        this.aGC = i;
        this.aVN = z3;
    }

    /* renamed from: f */
    public void mo2095f(C1616Xf xf) throws IOException {
        if (xf == null) {
            this.aGA.bxy().mo12008a(this, -1);
            return;
        }
        Class<?> cls = xf.getClass();
        int magicNumber = this.aGA.bxy().getMagicNumber(cls);
        if (magicNumber == -1) {
            throw new IllegalStateException("No magic number for game.script " + cls);
        }
        this.aGA.bxy().mo12008a(this, magicNumber);
        writeLong(xf.bFf().getObjectId().getId());
    }

    /* renamed from: PM */
    public DataGameEvent mo2090PM() {
        return this.aGA;
    }

    public void close() throws IOException {
        this.out.close();
    }

    public void flush() throws IOException {
        this.out.flush();
    }

    public void write(byte[] bArr, int i, int i2) throws IOException {
        this.cWs.write(bArr, i, i2);
    }

    public void write(byte[] bArr) throws IOException {
        this.cWs.write(bArr);
    }

    public void write(int i) throws IOException {
        this.cWs.write(i);
    }

    public void writeBoolean(boolean z) throws IOException {
        this.cWs.writeBoolean(z);
    }

    public void writeByte(int i) throws IOException {
        this.cWs.writeByte(i);
    }

    public void writeBytes(String str) throws IOException {
        this.cWs.writeBytes(str);
    }

    public void writeChar(int i) throws IOException {
        this.cWs.writeChar(i);
    }

    public void writeChars(String str) throws IOException {
        this.cWs.writeChars(str);
    }

    public void writeDouble(double d) throws IOException {
        this.cWs.writeDouble(d);
    }

    public void writeFloat(float f) throws IOException {
        this.cWs.writeFloat(f);
    }

    public void writeInt(int i) throws IOException {
        this.cWs.writeInt(i);
    }

    public void writeLong(long j) throws IOException {
        this.cWs.writeLong(j);
    }

    public void writeShort(int i) throws IOException {
        this.cWs.writeShort(i);
    }

    public void writeUTF(String str) throws IOException {
        this.cWs.writeUTF(str);
    }

    public void writeObject(Object obj) {
        if (this.cWt == null) {
            if (obj == null) {
                this.cWs.writeBoolean(false);
                return;
            }
            this.cWs.writeBoolean(true);
            if (obj instanceof Externalizable) {
                this.cWs.writeBoolean(true);
                C5496aLo.ilg.serializeData(this, obj.getClass());
                ((Externalizable) obj).writeExternal(this);
                return;
            }
            this.cWs.writeBoolean(false);
            this.cWt = new aAD(this.aGA, this.out, this.aGB, this.cWu, this.aGC, this.aVN);
            this.cWs = this.cWt;
            this.out = this.cWt;
        }
        this.cWt.writeObject(obj);
        this.cWt.flush();
    }

    /* renamed from: PN */
    public int mo2091PN() {
        return this.aGC;
    }

    /* renamed from: Xc */
    public boolean mo2092Xc() {
        return this.aVN;
    }

    /* renamed from: a */
    public void mo2093a(String str, IExternalIO afa) {
    }
}
