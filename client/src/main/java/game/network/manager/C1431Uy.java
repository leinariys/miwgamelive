package game.network.manager;

import game.engine.DataGameEvent;
import game.network.WhoAmI;
import game.network.exception.C1728ZX;
import game.network.message.Blocking;
import game.network.message.C5581aOv;
import game.network.message.externalizable.C3213pH;
import game.network.message.externalizable.C5703aTn;
import game.network.message.externalizable.C5887acP;
import game.network.message.externalizable.C6212aic;
import logic.baa.C1616Xf;
import logic.bbb.aDR;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import p001a.*;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.Uy */
/* compiled from: a */
public class C1431Uy {
    private static final int eoB = 20;
    static LogPrinter logger = LogPrinter.m10275K(C1431Uy.class);
    private final DataGameEvent aGA;
    private Thread eoC;
    private Map<C2491fm, C0495Gr> eoD = new HashMap();
    private ReentrantLock eoE = new ReentrantLock();

    public C1431Uy(DataGameEvent jz) {
        this.aGA = jz;
        this.eoC = new C1433b("Repository time based commit thread");
        this.eoC.setDaemon(true);
        this.eoC.start();
    }

    /* renamed from: u */
    public static List<Blocking> m10411u(List<Blocking> list) {
        if (list == null) {
            return null;
        }
        Collections.sort(list, new C1434c());
        return list;
    }

    public static boolean bxL() {
        return aWq.dDA() != null;
    }

    public void dispose() {
        if (this.eoC != null) {
            Thread thread = this.eoC;
            this.eoC = null;
            try {
                thread.interrupt();
                thread.join();
            } catch (InterruptedException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void bxK() {
        while (this.eoC == Thread.currentThread()) {
            try {
                Thread.sleep(1000);
                this.eoE.lock();
                try {
                    if (!this.eoD.isEmpty()) {
                        ArrayList<C0495Gr> arrayList = new ArrayList<>(this.eoD.values());
                        this.eoD.clear();
                        this.eoE.unlock();
                        HashMap hashMap = new HashMap();
                        for (C0495Gr a : arrayList) {
                            m10410a((Map<aDR, List<Blocking>>) hashMap, a);
                        }
                        this.aGA.bGU().mo15541C(hashMap);
                    }
                } finally {
                    this.eoE.unlock();
                }
            } catch (InterruptedException e) {
                return;
            }
        }
    }

    /* renamed from: a */
    private void m10410a(Map<aDR, List<Blocking>> map, C0495Gr gr) {
        C1619Xi xi = new C1619Xi();
        C1875af afVar = (C1875af) gr.aQL().bFf();
        for (C6014aem aem : afVar.mo13225dp()) {
            gr.mo2418l((Object[]) null);
            if (afVar.mo13210a(aem.eIu, (C1619Xi<C1616Xf>) xi, gr)) {
                List list = map.get(aem.eIu);
                if (list == null) {
                    list = new LinkedList();
                    map.put(aem.eIu, list);
                }
                list.add(new C5703aTn(gr));
            }
        }
    }

    /* renamed from: a */
    public void mo5931a(aWq awq, C0495Gr gr) {
        mo5930a(gr, (aDR) null, awq, (C5581aOv) null);
    }

    /* renamed from: b */
    public void mo5932b(aWq awq, C0495Gr gr) {
        this.eoE.lock();
        try {
            this.eoD.put(gr.aQK(), gr);
        } finally {
            this.eoE.unlock();
        }
    }

    /* renamed from: a */
    public Object mo5929a(C3582se seVar, C0495Gr gr) {
        Object obj = null;
        C2491fm aQK = gr.aQK();
        if (seVar.bHa()) {
            aWq dDA = aWq.dDA();
            if (dDA != null) {
                if (dDA.dDE() || dDA.readOnly) {
                    if (aQK.mo4793py() && aQK.mo4783po()) {
                        obj = seVar.mo6901yn().mo14a(gr);
                    }
                } else if (aQK.mo4793py() && aQK.mo4783po()) {
                    dDA.mo12076e(gr);
                }
            } else if (aQK.mo4783po()) {
                obj = mo5928a((aDR) null, gr, true);
            }
            if (aQK.mo4790pv()) {
                if (dDA == null || dDA.dDE()) {
                    ((C1875af) seVar).mo13213b(gr);
                } else {
                    dDA.mo12083i(new C1432a(dDA, gr));
                }
            }
            if (!aQK.mo4792px()) {
                return obj;
            }
            if (dDA != null) {
                dDA.mo12083i(new C1435d(dDA, gr));
                return obj;
            }
            //Не вызывает метод разгрузки без транзакции
            logger.warn("Wont call offload method without a transaction");
            return obj;
        } else if (!aQK.mo4791pw()) {
            return null;
        } else {
            if (!seVar.mo21973a((C1619Xi<C1616Xf>) new C1619Xi(), gr)) {
                return gr.aQK().mo4768pN();
            }
            C5703aTn atn = new C5703aTn(gr);
            if (!aQK.isBlocking()) {
                boolean z = C5587aPb.iAI;
                seVar.mo21970a((Blocking) atn);
                return null;
            }
            try {
                if (C5587aPb.iAI) {
                    logger.info("!!SYNC SEND!! " + seVar.mo6901yn().getClass().getName() + " " + gr.aQK());
                }
                C6212aic aic = (C6212aic) seVar.mo21980c((C5581aOv) atn);
                seVar.mo21969a(seVar.mo6892hE(), aic.aWo());
                return aic.getResult();
            } catch (C1728ZX e) {
                throw e.getCause();
            }
        }
    }

    /* renamed from: a */
    public Object mo5928a(aDR adr, C0495Gr gr, boolean z) {
        aWq awq;
        Object bgt;
        aWq awq2;
        long nanoTime;
        int dDU;
        C1830aW aWVar;
        List<Blocking> u;
        if (bxL()) {
            //Если мы уже находимся в транзакции, это не должно вызываться
            throw new RuntimeException("If we're already in a transaction, this shouldnt be called.");
        }
        aWq awq3 = null;
        m10409PM().bGs();
        C2491fm aQK = gr.aQK();
        long nanoTime2 = this.aGA.bHd() ? System.nanoTime() : 0;
        int i = 0;
        while (i < 20) {
            if (awq3 != null) {
                awq3.dDO();
            }
            if (i > 10) {
                PoolThread.sleep((long) (100.0d + (Math.random() * 100.0d)));
            }
            aWq b = aWq.m19285b(this.aGA.bHd() ? currentTimeMillis() : 0, adr, gr);
            try {
                nanoTime = this.aGA.bHd() ? System.nanoTime() : 0;
                Object a = gr.aQL().mo14a(gr);
                dDU = b.dDU();
                if (this.aGA.bHd()) {
                    aQK.mo4715A(System.nanoTime() - nanoTime);
                }
                if (this.aGA.bHe()) {
                    aQK.mo4717aK(b.env);
                    aQK.mo4718aL(b.eny);
                    b.env = 0;
                    b.eny = 0;
                    aQK.mo4719aM(b.enx);
                    aQK.mo4720aN(b.jgd);
                    aQK.mo4721aO(b.enr);
                    aQK.mo4722aP(dDU);
                }
                if (b.mo12081g(this.aGA) == -1) {
                    awq2 = b;
                    if (awq2 != null) {
                        awq2.dispose();
                        awq2 = null;
                    }
                    i++;
                    awq3 = awq2;
                } else {
                    if (!z || adr == null) {
                        aWVar = a;
                    } else {
                        if (b.dDF() == null) {
                            u = null;
                        } else {
                            u = m10411u(b.dDF().remove(adr));
                        }
                        aWVar = new C1830aW(u, a);
                    }
                    if (b.dDF() != null && b.dDF().size() > 0) {
                        this.aGA.bGU().mo15541C(b.dDF());
                    }
                    if (this.aGA.bHe()) {
                        aQK.mo4725aS(b.dDU() - dDU);
                        aQK.mo4726aT(b.env);
                        aQK.mo4727aU(b.eny);
                        aQK.mo4723aQ(b.jgb);
                        aQK.mo4724aR(b.jgc);
                    }
                    if (b != null) {
                        b.dispose();
                    }
                    if (this.aGA.bHd()) {
                        long nanoTime3 = System.nanoTime() - nanoTime2;
                        gr.aQK().mo4716B(nanoTime3);
                        m10409PM().mo3445gf(nanoTime3);
                    } else {
                        m10409PM().mo3445gf(0);
                    }
                    return aWVar;
                }
            } catch (C6510aoO e) {
                try {
                    b.dDO();
                    this.aGA.bGr().hap.incrementAndGet();
                    b.dispose();
                    awq2 = null;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Exception e2) {
                boolean z2 = e2.getClass().getAnnotation(C6546aoy.class) != null;
                b.dDO();
                if (!z2) {
                    LogPrinter ur = logger;
                    StringBuilder append = new StringBuilder("Transaction canceled coz an exception happened: ").append(e2.getMessage()).append(" (Call from ");
                    if (adr == null) {
                        bgt = "(dont know)";
                    } else {
                        bgt = adr.bgt();
                    }
                    ur.info(append.append(bgt).append(", method ").append(gr.aQL().bFf().bFY()).append(".").append(aQK.name()).append(")").toString(), e2);
                    if (b.dDB()) {
                        logger.warn("Infra methods were called in a canceled transaction:\n" + b.dDC());
                    }
                }
                b.dispose();
                awq = null;
                throw e2;
            } catch (Throwable th2) {
                int dDU2 = b.dDU();
                if (this.aGA.bHd()) {
                    aQK.mo4715A(System.nanoTime() - nanoTime);
                }
                if (this.aGA.bHe()) {
                    aQK.mo4717aK(b.env);
                    aQK.mo4718aL(b.eny);
                    b.env = 0;
                    b.eny = 0;
                    aQK.mo4719aM(b.enx);
                    aQK.mo4720aN(b.jgd);
                    aQK.mo4721aO(b.enr);
                    aQK.mo4722aP(dDU2);
                }
                throw th2;
            }
        }
        try {
            throw new RuntimeException("The transaction could not run after 20 tries. bailing. (Call from " + (adr == null ? "(dont know)" : adr.bgt()) + ", method " + aQK.name() + ")");
        } catch (Throwable th3) {
            if (this.aGA.bHd()) {
                long nanoTime4 = System.nanoTime() - nanoTime2;
                gr.aQK().mo4716B(nanoTime4);
                m10409PM().mo3445gf(nanoTime4);
            } else {
                m10409PM().mo3445gf(0);
            }
            throw th3;
        }
        if (awq != null) {
            awq.dispose();
        }
        throw th;
    }

    /* renamed from: PM */
    private DataGameEvent m10409PM() {
        return this.aGA;
    }

    public final Date bxM() {
        return new Date(currentTimeMillis());
    }

    public final long currentTimeMillis() {
        return m10409PM().currentTimeMillis();
    }

    public final Date bxN() {
        aWq dDA = aWq.dDA();
        if (dDA != null) {
            return dDA.bxM();
        }
        //Вы можете вызвать этот метод только из транзакции.
        throw new RuntimeException("You can only call this method from within a transaction.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo5930a(C0495Gr gr, aDR adr, aWq awq, C5581aOv aov) {
        C5581aOv aov2;
        C3582se seVar = (C3582se) gr.aQL().bFf();
        C3538sJ hj = gr.aQK().mo7388hj();
        if (hj != null) {
            C6147ahP[] hi = gr.aQK().mo7387hi();
            if (hi != null && hi.length > 0) {
                //ReplicationRules игнорируется для метода "+ gr +", потому что метод уже объявляет область видимости.
                logger.warn("ReplicationRules ignored for method " + gr + " coz the method already declares a scope.");
            }
            C2049bZ bZVar = new C2049bZ();
            bZVar.mo17304c(gr);
            bZVar.mo17303a(C6913awB.C2004a.METHOD);
            bZVar.mo17302a(WhoAmI.CLIENT);
            Collection<aDR> a = hj.mo4799a(seVar.mo6901yn(), bZVar);
            if (a != null) {
                for (aDR next : a) {
                    gr.mo2418l((Object[]) null);
                    if (adr == null || (next != adr && !next.equals(adr))) {
                        if (aov == null) {
                            aov = new C5703aTn(gr);
                        }
                        if (awq == null) {
                            this.aGA.bGU().mo15549b(next.bgt(), (Blocking) aov);
                        } else {
                            awq.mo12048a(next, (Blocking) aov);
                        }
                    }
                }
                return;
            }
            return;
        }
        C6014aem[] dp = ((C1875af) seVar).mo13225dp();
        C1619Xi xi = new C1619Xi();
        C5581aOv aov3 = aov;
        for (C6014aem aem : dp) {
            aDR adr2 = aem.eIu;
            gr.mo2418l((Object[]) null);
            if ((adr == null || (adr2 != adr && !adr2.equals(adr))) && ((C1875af) seVar).mo13210a(adr2, (C1619Xi<C1616Xf>) xi, gr) && (!gr.aQK().mo7389hk() || !adr2.cWs() || !gr.aQK().mo4780pZ())) {
                if (aov3 == null) {
                    aov2 = new C5703aTn(gr);
                } else {
                    aov2 = aov3;
                }
                gr.aQK().mo4776pV();
                if (awq == null) {
                    this.aGA.bGU().mo15549b(adr2.bgt(), (Blocking) aov2);
                    aov3 = aov2;
                } else {
                    awq.mo12048a(aem.eIu, (Blocking) aov2);
                    aov3 = aov2;
                }
            }
        }
    }

    /* renamed from: a.Uy$b */
    /* compiled from: a */
    class C1433b extends Thread {
        C1433b(String str) {
            super(str);
        }

        public void run() {
            C1431Uy.this.bxK();
        }
    }

    /* renamed from: a.Uy$a */
    class C1432a implements Runnable {
        private final /* synthetic */ C0495Gr dgP;
        private final /* synthetic */ aWq eNa;

        C1432a(aWq awq, C0495Gr gr) {
            this.eNa = awq;
            this.dgP = gr;
        }

        public void run() {
            C1431Uy.this.mo5931a(this.eNa, this.dgP);
        }
    }

    /* renamed from: a.Uy$d */
    /* compiled from: a */
    class C1435d implements Runnable {
        private final /* synthetic */ C0495Gr dgP;
        private final /* synthetic */ aWq eNa;

        C1435d(aWq awq, C0495Gr gr) {
            this.eNa = awq;
            this.dgP = gr;
        }

        public void run() {
            C1431Uy.this.mo5931a(this.eNa, this.dgP);
        }
    }

    /* renamed from: a.Uy$c */
    /* compiled from: a */
    class C1434c implements Comparator<Blocking> {
        C1434c() {
        }

        /* renamed from: aV */
        public int mo5941aV(Object obj) {
            if (obj instanceof C5887acP) {
                return 0;
            }
            if (obj instanceof C3213pH) {
                return 1;
            }
            return 2;
        }

        /* renamed from: a */
        public int compare(Blocking kUVar, Blocking kUVar2) {
            return mo5941aV(kUVar) - mo5941aV(kUVar2);
        }
    }
}
