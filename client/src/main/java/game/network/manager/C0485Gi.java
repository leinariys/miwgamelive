package game.network.manager;

import game.network.WhoAmI;
import game.network.build.ManagerMessageTransportImpl;
import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.C3775ut;
import game.network.message.externalizable.C6302akO;
import p001a.C2564gr;
import p001a.SingletonCurrentTimeMilliImpl;
import p001a.axB;

/* renamed from: a.Gi */
/* compiled from: a */
public class C0485Gi extends axB<C3775ut> {
    final /* synthetic */ ManagerMessageTransportImpl cXM;

    C0485Gi(ManagerMessageTransportImpl aqi) {
        this.cXM = aqi;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C6302akO mo2392b(ClientConnect bVar, C3775ut utVar) {
        if (this.cXM.dataGameEvent.getWhoAmI() != WhoAmI.SERVER) {
            return null;
        }
        bVar.setPing(utVar.mo22464xU());
        return new C2564gr(SingletonCurrentTimeMilliImpl.currentTimeMillis(), utVar.mo22462vK(), utVar.mo22463vL());
    }
}
