package game.network.manager;

import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.C6302akO;
import game.network.message.serializable.ReceivedTime;

/* renamed from: a.aL */
/* compiled from: a */
public interface C1810aL {
    /* renamed from: a */
    <M extends ReceivedTime> C6302akO mo3456a(ClientConnect bVar, M m);

    Class<? extends ReceivedTime> getMessageClass();
}
