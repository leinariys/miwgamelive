package game.network.manager;

import game.network.build.ManagerMessageTransportImpl;
import game.network.channel.client.ClientConnect;
import game.network.message.Blocking;
import game.network.message.externalizable.C6302akO;
import game.network.message.externalizable.C7037azy;
import p001a.axB;

/* renamed from: a.Gn */
/* compiled from: a */
public class C0490Gn extends axB<C7037azy> {
    final /* synthetic */ ManagerMessageTransportImpl cXM;

    C0490Gn(ManagerMessageTransportImpl aqi) {
        this.cXM = aqi;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C6302akO mo2392b(ClientConnect bVar, C7037azy azy) {
        this.cXM.dataGameEvent.bGx();
        try {
            for (Blocking d : azy.cFV()) {
                this.cXM.mo15556d(bVar, d);
            }
            this.cXM.dataGameEvent.bGy();
            return null;
        } catch (Throwable th) {
            this.cXM.dataGameEvent.bGy();
            throw th;
        }
    }
}
