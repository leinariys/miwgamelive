package game.network.manager;

import logic.res.html.DataClassSerializer;
import p001a.C6744aso;
import p001a.CountFailedReadOrWriteException;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.ajI  reason: case insensitive filesystem */
/* compiled from: a */
public class MagicDataClassSerializer extends DataClassSerializer {
    public static MagicDataClassSerializer fRF = new MagicDataClassSerializer();

    /* renamed from: b */
    public Object inputReadObject(ObjectInput objectInput) throws IOException {
        if (!(objectInput instanceof aHN)) {
            return super.inputReadObject(objectInput);
        }
        short readShort = objectInput.readShort();
        if (readShort == -1) {
            return null;
        }
        short s = (short) (readShort & 65535);
        Class<?> tS = ((aHN) objectInput).mo9184PM().bxy().mo12017tS(s);
        if (tS == null) {
            throw new RuntimeException("Could not find class for magic:" + s);
        }
        try {
            Externalizable externalizable = (Externalizable) tS.newInstance();
            externalizable.readExternal(objectInput);
            return externalizable;
        } catch (InstantiationException e) {
            throw new CountFailedReadOrWriteException("Error trying to read an " + tS, e);
        } catch (IllegalAccessException e2) {
            throw new CountFailedReadOrWriteException("Error trying to read an " + tS, e2);
        } catch (Exception e3) {
            throw new CountFailedReadOrWriteException("Error trying to read an " + tS, e3);
        }
    }

    /* renamed from: a */
    public void serializeData(ObjectOutput objectOutput, Object obj) throws IOException {
        if (!(objectOutput instanceof C6744aso)) {
            super.serializeData(objectOutput, obj);
        } else if (obj == null) {
            objectOutput.writeShort(-1);
        } else {
            int magicNumber = ((C6744aso) objectOutput).mo2090PM().bxy().getMagicNumber(obj.getClass());
            if (magicNumber == -1) {
                throw new IllegalArgumentException(obj.getClass() + " must be precached!");
            }
            objectOutput.writeShort(magicNumber);
            ((Externalizable) obj).writeExternal(objectOutput);
        }
    }
}
