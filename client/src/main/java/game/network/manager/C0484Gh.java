package game.network.manager;

import game.network.WhoAmI;
import game.network.build.ManagerMessageTransportImpl;
import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.C0275DY;
import game.network.message.externalizable.C6302akO;
import game.network.message.externalizable.ObjectId;
import logic.bbb.aDR;
import p001a.C3582se;
import p001a.axB;

/* renamed from: a.Gh */
/* compiled from: a */
public class C0484Gh extends axB<C0275DY> {
    final /* synthetic */ ManagerMessageTransportImpl cXM;

    C0484Gh(ManagerMessageTransportImpl aqi) {
        this.cXM = aqi;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C6302akO mo2392b(ClientConnect bVar, C0275DY dy) {
        aDR f;
        if (this.cXM.dataGameEvent.getWhoAmI() != WhoAmI.SERVER || (f = this.cXM.dataGameEvent.bGJ().mo6442f(bVar)) == null) {
            return null;
        }
        C3582se a = this.cXM.dataGameEvent.bGA().mo15459a(new ObjectId(dy.mo19957Ej()));
        if (a != null) {
            try {
                f.mo8403g(a);
                return null;
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        } else if (!ManagerMessageTransportImpl.logger.isDebugEnabled()) {
            return null;
        } else {
            ManagerMessageTransportImpl.logger.debug("Safely ignoring wrong remove from cache request. " + dy.mo19958hD().getName() + ":" + dy.mo19957Ej());
            return null;
        }
    }
}
