package game.network.manager;

import game.engine.DataGameEvent;
import game.io.IExternalIO;
import game.network.exception.C2571gy;
import game.network.message.ByteMessageReader;
import logic.baa.C1616Xf;
import p001a.C3086na;
import p001a.C5496aLo;

import java.io.DataInput;
import java.io.Externalizable;
import java.io.InputStream;
import java.io.ObjectInput;

/* renamed from: a.aQQ     /  ObjectInputStream  */
/* compiled from: a */
public class ContainerBinaryData extends InputStream implements aHN, ObjectInput {
    private final DataGameEvent dataGameEvent;
    private boolean aGB;
    private int aGC;
    private DataInput dataInput;
    private C3086na iFE;

    /* renamed from: in */
    private InputStream inputStream;

    public ContainerBinaryData(DataGameEvent jz, DataInput dataInput, InputStream inputStream, boolean z, boolean z2) {
        this.aGB = false;
        this.aGC = 255;
        this.dataGameEvent = jz;
        this.dataInput = dataInput;
        this.inputStream = inputStream;
        this.aGB = z;
    }

    public ContainerBinaryData(DataGameEvent jz, ByteMessageReader messageReader, boolean z, boolean z2, int i) {
        this(jz, (DataInput) messageReader, (InputStream) messageReader, z, z2);
        this.aGC = i;
    }

    /* renamed from: PL */
    public C1616Xf mo9183PL() {
        int a = this.dataGameEvent.bxy().mo12007a(this);
        if (a == -1) {
            return null;
        }
        long readLong = readLong();
        try {
            return this.dataGameEvent.mo3421c(a, readLong);
        } catch (Exception e) {
            throw new C2571gy(e, "Error deserializing game.script: " + this.dataGameEvent.bxy().mo12017tS(a) + ":" + readLong);
        }
    }

    /* renamed from: PM */
    public DataGameEvent mo9184PM() {
        return this.dataGameEvent;
    }

    public int read() {
        return this.inputStream.read();
    }

    public int available() {
        return this.inputStream.available();
    }

    public void close() {
        this.inputStream.close();
    }

    public synchronized void mark(int i) {
        this.inputStream.mark(i);
    }

    public boolean markSupported() {
        return this.inputStream.markSupported();
    }

    public int read(byte[] bArr, int i, int i2) {
        return this.inputStream.read(bArr, i, i2);
    }

    public int read(byte[] bArr) {
        return this.inputStream.read(bArr);
    }

    public void reset() {
        this.inputStream.reset();
    }

    public long skip(long j) {
        return this.inputStream.skip(j);
    }

    public boolean readBoolean() {
        return this.dataInput.readBoolean();
    }

    public byte readByte() {
        return this.dataInput.readByte();
    }

    public char readChar() {
        return this.dataInput.readChar();
    }

    public double readDouble() {
        return this.dataInput.readDouble();
    }

    public float readFloat() {
        return this.dataInput.readFloat();
    }

    public void readFully(byte[] bArr) {
        this.dataInput.readFully(bArr);
    }

    public void readFully(byte[] bArr, int i, int i2) {
        this.dataInput.readFully(bArr, i, i2);
    }

    public int readInt() {
        return this.dataInput.readInt();
    }

    public String readLine() {
        return this.dataInput.readLine();
    }

    public long readLong() {
        return this.dataInput.readLong();
    }

    public short readShort() {
        return this.dataInput.readShort();
    }

    public String readUTF() {
        return this.dataInput.readUTF();
    }

    public int readUnsignedByte() {
        return this.dataInput.readUnsignedByte();
    }

    public int readUnsignedShort() {
        return this.dataInput.readUnsignedShort();
    }

    public int skipBytes(int i) {
        return this.dataInput.skipBytes(i);
    }

    public Object readObject() {
        if (this.iFE == null) {
            if (!this.dataInput.readBoolean()) {
                return null;
            }
            if (this.dataInput.readBoolean()) {
                try {
                    Externalizable externalizable = (Externalizable) ((Class) C5496aLo.ilg.inputReadObject(this)).newInstance();
                    externalizable.readExternal(this);
                    return externalizable;
                } catch (InstantiationException e) {
                    throw new RuntimeException(e);
                } catch (IllegalAccessException e2) {
                    throw new RuntimeException(e2);
                }
            } else {
                this.iFE = new C3086na(this.dataGameEvent, this.inputStream, this.aGB, this.aGC);
                this.inputStream = this.iFE;
                this.dataInput = this.iFE;
            }
        }
        return this.iFE.readObject();
    }

    /* renamed from: PN */
    public int mo9185PN() {
        return this.aGC;
    }

    /* renamed from: he */
    public <T extends IExternalIO> T mo10923he(String str) {
        return null;
    }
}
