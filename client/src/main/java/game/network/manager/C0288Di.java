package game.network.manager;

import game.engine.DataGameEvent;
import game.engine.SocketMessage;
import game.network.message.ByteMessageReader;
import logic.thred.LogPrinter;
import p001a.ByteMessageReaderDebug;
import p001a.C3086na;
import p001a.aAD;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: a.Di */
/* compiled from: a */
public class C0288Di {
    static LogPrinter logger = LogPrinter.m10275K(C0288Di.class);

    /* renamed from: a */
    public static byte[] m2486a(DataGameEvent jz, Object obj) {
        SocketMessage ala = new SocketMessage();
        m2484a(jz, obj, ala);
        return ala.toByteArray();
    }

    /* renamed from: a */
    public static void m2484a(DataGameEvent jz, Object obj, OutputStream outputStream) {
        try {
            aAD aad = new aAD(jz, outputStream);
            aad.writeObject(obj);
            aad.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (RuntimeException e2) {
            throw e2;
        }
    }

    /* renamed from: a */
    public static Object m2483a(DataGameEvent jz, byte[] bArr) {
        return m2482a(jz, (InputStream) new ByteMessageReader(bArr));
    }

    /* renamed from: a */
    public static Object m2482a(DataGameEvent jz, InputStream inputStream) {
        try {
            return new C3086na(jz, inputStream).readObject();
        } catch (Error e) {
            errorDeserializing(inputStream, (Throwable) e);
            throw e;
        } catch (RuntimeException e2) {
            errorDeserializing(inputStream, (Throwable) e2);
            throw e2;
        } catch (Exception e3) {
            errorDeserializing(inputStream, (Throwable) e3);
            throw new RuntimeException(e3);
        }
    }

    /* renamed from: a */
    public static void errorDeserializing(InputStream inputStream, Throwable th) {
        logger.error("Error deserializing: \n" + inputStream + "\n" + th);
        if (inputStream instanceof ByteMessageReaderDebug) {
            logger.error(((ByteMessageReaderDebug) inputStream).dump());
        }
    }

    /* renamed from: b */
    public static <T> T m2487b(DataGameEvent jz, T t) {
        return m2483a(jz, m2486a(jz, (Object) t));
    }
}
