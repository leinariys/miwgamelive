package game.network;

import org.apache.commons.jxpath.JXPathContext;
import util.C0831Lz;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: a.kt */
/* compiled from: a */
public class C2840kt extends aQD {
    /* renamed from: a */
    public String mo2073a(Iterator<?> it, List<String> list, Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        String[] strArr = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            strArr[i] = map.get(list.get(i));
        }
        C2574hA hAVar = new C2574hA();
        hAVar.mo19169b(strArr);
        while (it.hasNext()) {
            JXPathContext newContext = JXPathContext.newContext(it.next());
            newContext.setFunctions(this.iEM);
            List[] listArr = new List[list.size()];
            for (int i2 = 0; i2 < list.size(); i2++) {
                List selectNodes = newContext.selectNodes(list.get(i2));
                if (listArr[i2] == null) {
                    if (this.iEL) {
                        listArr[i2] = new C0831Lz();
                    } else {
                        listArr[i2] = new ArrayList();
                    }
                }
                for (Object next : selectNodes) {
                    if (next == null) {
                        next = "";
                    }
                    listArr[i2].add(next.toString());
                }
            }
            hAVar.mo19168a((List<?>[]) listArr);
        }
        return hAVar.mo19167a(sb);
    }
}
