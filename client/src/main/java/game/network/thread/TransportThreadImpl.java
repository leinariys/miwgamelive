package game.network.thread;

import game.DataNetworkInformation;
import game.network.GameScriptClasses;
import game.network.Transport;
import game.network.build.ManagerMessageTransport;
import game.network.build.TransportThread;
import game.network.channel.MessageProcessing;
import game.network.channel.ProcesTransportMessage;
import game.network.channel.client.ClientConnect;
import game.network.exception.TransportIsDisposedException;
import game.network.message.ByteMessage;
import game.network.message.ByteMessageReader;
import game.network.message.TransportCommand;
import game.network.message.TransportResponse;
import game.network.metric.MetricThreadId;
import logic.res.html.MessageContainer;
import logic.res.html.axI;
import logic.thred.C2316dw;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import org.apache.commons.logging.Log;
import taikodom.render.textures.DDSLoader;

import java.io.EOFException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

/* renamed from: a.ea */
/* compiled from: a */
public class TransportThreadImpl implements TransportThread {
    /* access modifiers changed from: private */
    public static final Log logTransportReader = LogPrinter.m10275K(TransportReaderRunnable.class);
    /* access modifiers changed from: private */
    public static final Log logTransportWriter = LogPrinter.m10275K(TransportWriterRunnable.class);
    static LogPrinter logger = LogPrinter.m10275K(TransportThreadImpl.class);
    private final ManagerMessageTransport managerMessageTransport;
    private final Object clientConnect;
    /* access modifiers changed from: private */
    public MessageProcessing messageProcessing;
    /* access modifiers changed from: private */
    public ProcesTransportMessage procesTransportMessage;
    /* access modifiers changed from: private */
    public ArrayBlockingQueue<Object> messageToWriter;
    /* access modifiers changed from: private */
    public Map<Integer, MessagesTransportWaitingResponse> messageEnvelope = new HashMap();
    /* access modifiers changed from: private */
    public Map<Integer, MessagesTransportWaitingResponse> messagesSent = new HashMap();
    public Thread transportReaderThread;
    public Thread transportWriterThread;
    /* access modifiers changed from: private */
    public ThreadLocal<Integer> integerThreadLocal = new ThreadLocalInteger(this);
    /* access modifiers changed from: private */
    public MetricThreadId metricThreadId;
    private boolean disposed;
    private boolean isServer;

    public TransportThreadImpl(ManagerMessageTransport zCVar, Object clientConnect, MessageProcessing uv, ProcesTransportMessage agp) {
        this.managerMessageTransport = zCVar;
        this.clientConnect = clientConnect;
        this.messageProcessing = uv;
        this.procesTransportMessage = agp;
        this.messageToWriter = new ArrayBlockingQueue<>(DDSLoader.DDSD_MIPMAPCOUNT);
        PoolThread.init(new TransportReaderRunnable(), "Distribuitor Transport Reader " + hashCode());
        PoolThread.init(new TransportWriterRunnable(), "Distribuitor Transport Writer " + hashCode());
    }

    /* renamed from: a */
    public static Transport buildMessageTransport(ByteMessageReader amf) throws EOFException {
        switch (amf.readBits(2)) {
            case 0:
                return new TransportCommand(amf);
            case 1:
                return new TransportResponse(amf);
            default:
                throw new IllegalArgumentException();
        }
    }

    public Object getClientConnect() {
        return this.clientConnect;
    }

    /* access modifiers changed from: protected */
    public boolean isDisposed() {
        return this.disposed;
    }

    public void dispose() {
        if (!this.disposed) {
            logger.info("disposing transport");
            this.disposed = true;
            if (this.transportReaderThread != null) {
                this.transportReaderThread.interrupt();
                this.transportReaderThread = null;
            }
            if (this.transportWriterThread != null) {
                this.transportWriterThread.interrupt();
                this.transportWriterThread = null;
            }
            try {
                this.messageProcessing.close();
            } catch (IOException e) {
            }
            try {
                this.procesTransportMessage.close();
            } catch (IOException e2) {
            }
            synchronized (this.messagesSent) {
                for (MessagesTransportWaitingResponse next : this.messagesSent.values()) {
                    synchronized (next) {
                        next.notify();
                    }
                }
            }
        }
    }

    /* renamed from: HX */
    public int getThreadInteger() {
        return this.integerThreadLocal.get().intValue();
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void sendingTransportCommand(ClientConnect clientConnect, TransportCommand transportCommand) {
        this.integerThreadLocal.set(Integer.valueOf(transportCommand.getThreadId()));
        TransportResponse response = this.managerMessageTransport.preparingResponseMessage(clientConnect, transportCommand);
        if (response != null) {
            response.setMessageId(transportCommand.getMessageId());
            sendTransportMessageToTarget(clientConnect, response);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void addToPoolExecuterCommand(ClientConnect bVar, TransportCommand transportCommand) {
        this.managerMessageTransport.addToPoolExecuterCommand(bVar, transportCommand);
    }

    /* renamed from: a */
    public MessagesTransportWaitingResponse creatMessagesTransportWaitingResponse(ClientConnect bVar, TransportCommand transportCommand, boolean z) {
        return new MessagesTransportWaitingResponse(bVar, transportCommand, z);
    }

    /* renamed from: b */
    public TransportResponse sendTransportMessageAndWaitForResponse(ClientConnect bVar, TransportCommand transportCommand) {
        MessagesTransportWaitingResponse response = creatMessagesTransportWaitingResponse(bVar, transportCommand, true);
        response.sendTransportMessage();
        return response.waitForResponse();
    }

    /* access modifiers changed from: private */
    public void checkDispose() {
        if (isDisposed()) {
            throw new TransportIsDisposedException("Transport is disposed.");
        }
    }

    /* renamed from: a */
    public void sendTransportMessageInSeparateThread(ClientConnect bVar, TransportCommand transportCommand) {
        if (!this.isServer || transportCommand.isBlocking()) {
            creatMessagesTransportWaitingResponse(bVar, transportCommand, false).sendTransportMessage();
            return;
        }
        try {
            if (transportCommand.getType() == 2) {
                this.procesTransportMessage.sendTransportMessage(transportCommand.getOutBuffer(), 0, transportCommand.getOutLength());
            } else {
                this.procesTransportMessage.sendTransportMessage(bVar, transportCommand.getOutBuffer(), 0, transportCommand.getOutLength());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: c */
    public void sendTransportMessageToTarget(ClientConnect bVar, TransportResponse transportResponse) {
        checkDispose();
        if (this.integerThreadLocal.get() == null || this.integerThreadLocal.get().intValue() == 0) {
            throw new RuntimeException("At this point, threadId should have been be properly set");
        } else if (this.isServer) {
            try {
                this.messageToWriter.put(new MessagesTransportNotWaitingResponse(bVar, transportResponse));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            try {
                this.procesTransportMessage.sendTransportMessage(bVar, transportResponse.getOutBuffer(), 0, transportResponse.getOutLength());
            } catch (IOException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    /* renamed from: HY */
    public void pendingClose() {
        Thread thread = this.transportReaderThread;
        Thread thread2 = this.transportWriterThread;
        dispose();
        if (thread != null) {
            try {
                thread.join(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return;
            }
        }
        if (thread2 != null) {
            thread2.join(1000);
        }
    }

    /* access modifiers changed from: private */
    public int randomUniqueIdentifier() {
        return (int) (Math.random() * ((double) getClientConnect().hashCode()) * ((double) Thread.currentThread().hashCode()));
    }

    /* renamed from: a */
    public Object getGameScriptClasses(GameScriptClasses aec) {
        return aec;
    }

    /* renamed from: a */
    public Object getDataNetworkInformation(DataNetworkInformation amr) {
        return amr;
    }

    public MetricThreadId getMetricThreadId() {
        return this.metricThreadId;
    }

    /* renamed from: a */
    public void setMetricThreadId(MetricThreadId fh) {
        this.metricThreadId = fh;
    }

    public boolean isServer() {
        return this.isServer;
    }

    /* renamed from: ak */
    public void setIsServer(boolean isServer) {
        this.isServer = isServer;
    }

    /* renamed from: b */
    public void sendTransportResponse(ClientConnect bVar, TransportResponse transportResponse) {
        sendTransportMessageToTarget(bVar, transportResponse);
    }

    /* renamed from: a.ea$b */
    /* compiled from: a */
    public class MessagesTransportNotWaitingResponse {
        ClientConnect clientConnect;
        TransportResponse transportResponse;

        public MessagesTransportNotWaitingResponse(ClientConnect clientConnect, TransportResponse transportResponse) {
            this.clientConnect = clientConnect;
            this.transportResponse = transportResponse;
        }
    }

    /* renamed from: a.ea$a */
    private class TransportReaderRunnable implements Runnable {
        public void run() {
            String str;
            TransportThreadImpl.this.transportReaderThread = Thread.currentThread();
            while (isRunning()) {
                try {
                    if (TransportThreadImpl.logTransportReader.isDebugEnabled()) {
                        TransportThreadImpl.logTransportReader.debug("waiting...");
                    }
                    ByteMessage byteMessage = TransportThreadImpl.this.messageProcessing.getTransportMessage();
                    if (byteMessage != null) {
                        if (TransportThreadImpl.logTransportReader.isDebugEnabled()) {
                            TransportThreadImpl.logTransportReader.debug("payload:" + byteMessage.getMessage());
                        }
                        try {
                            Transport transport = TransportThreadImpl.buildMessageTransport(new ByteMessageReader(byteMessage.getMessage(), byteMessage.getOffset(), byteMessage.getLength()));
                            transport.setTimeReadMesage(System.currentTimeMillis());
                            if (TransportThreadImpl.logTransportReader.isDebugEnabled()) {
                                TransportThreadImpl.logTransportReader.debug("incoming msg: " + transport.getThreadId() + " - " + transport.getClass().getName());
                            }
                            try {
                                if (transport instanceof TransportResponse) {
                                    readTransportResponse(byteMessage.getFromClientConnect(), (TransportResponse) transport);
                                } else if (transport instanceof TransportCommand) {
                                    readTransportCommand(byteMessage.getFromClientConnect(), (TransportCommand) transport);
                                }
                            } catch (RuntimeException e) {
                                e.printStackTrace();
                            }
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                } catch (C2316dw e2) {
                    TransportThreadImpl.logTransportReader.warn("transport reader interrupted");
                    return;
                } catch (EOFException e3) {
                    if (isRunning()) {
                        e3.printStackTrace();
                    }
                    return;
                } catch (IOException e4) {
                    if (isRunning()) {
                        e4.printStackTrace();
                    }
                    return;
                } catch (ClassNotFoundException e5) {
                    if (isRunning()) {
                        e5.printStackTrace();
                    }
                    return;
                } catch (InterruptedException e6) {
                    TransportThreadImpl.logger.info("Transport interrupted");
                    return;
                } catch (axI e7) {
                    TransportThreadImpl.logTransportReader.info("queue disposed");
                    return;
                } catch (Exception e8) {
                    TransportThreadImpl.logTransportReader.error("Uncaught exception: ", e8);
                    return;
                } finally {
                    str = "stopped";
                    TransportThreadImpl.logTransportReader.info(str);
                    TransportThreadImpl.this.dispose();
                }
            }
        }

        private boolean isRunning() {
            return TransportThreadImpl.this.transportReaderThread == Thread.currentThread();
        }

        /* renamed from: a */
        private void readTransportResponse(ClientConnect bVar, TransportResponse transportResponse) {
            MessagesTransportWaitingResponse dVar;
            synchronized (TransportThreadImpl.this.messagesSent) {
                MessagesTransportWaitingResponse dVar2 = (MessagesTransportWaitingResponse) TransportThreadImpl.this.messagesSent.remove(Integer.valueOf(transportResponse.getThreadId()));
                if (dVar2 == null) {
                    TransportThreadImpl.this.integerThreadLocal.set(0);
                } else {
                    MessagesTransportWaitingResponse cZC = dVar2.getTransportWaitingResponse();
                    if (cZC == null) {
                        TransportThreadImpl.this.integerThreadLocal.set(0);
                    } else {
                        TransportThreadImpl.this.messagesSent.put(Integer.valueOf(transportResponse.getThreadId()), cZC);
                    }
                }
            }
            synchronized (TransportThreadImpl.this.messageEnvelope) {
                dVar = (MessagesTransportWaitingResponse) TransportThreadImpl.this.messageEnvelope.remove(Integer.valueOf(transportResponse.getMessageId()));
                TransportThreadImpl.this.messageEnvelope.notify();
            }
            if (dVar == null) {
                throw new RuntimeException("No envelope found for messageId " + transportResponse.getMessageId() + "\n" + transportResponse);
            }
            dVar.setTargetReadResponse(bVar, transportResponse);
        }

        /* renamed from: c */
        private void readTransportCommand(ClientConnect bVar, TransportCommand transportCommand) {
            MessagesTransportWaitingResponse dVar = null;
            if (transportCommand.isBlocking()) {
                synchronized (TransportThreadImpl.this.messagesSent) {
                    if (TransportThreadImpl.this.messagesSent.containsKey(Integer.valueOf(transportCommand.getThreadId()))) {
                        if (TransportThreadImpl.logTransportReader.isDebugEnabled()) {
                            TransportThreadImpl.logTransportReader.debug("Thread " + transportCommand.getThreadId() + " has a waiting call, using the same thread");
                        }
                        dVar = (MessagesTransportWaitingResponse) TransportThreadImpl.this.messagesSent.get(Integer.valueOf(transportCommand.getThreadId()));
                    }
                }
            }
            if (dVar == null) {
                TransportThreadImpl.this.addToPoolExecuterCommand(bVar, transportCommand);
            } else {
                dVar.setReadTransportCommand(transportCommand);
            }
        }
    }


    /* renamed from: a.ea$c */
    /* compiled from: a */
    private class TransportWriterRunnable implements Runnable {
        public void run() {
            TransportThreadImpl.this.transportWriterThread = Thread.currentThread();
            try {
                final List<MessagesTransportWaitingResponse> mesConts = MessageContainer.init();
                while (this.isRunning()) {
                    TransportThreadImpl.logTransportWriter.debug((Object) "waiting...");
                    mesConts.clear();
                    if (TransportThreadImpl.this.messageToWriter.size() > 0) {
                        TransportThreadImpl.this.messageToWriter.drainTo(Collections.singleton(mesConts));
                    } else {
                        mesConts.add((MessagesTransportWaitingResponse) TransportThreadImpl.this.messageToWriter.take());
                    }
                    for (final Object next : mesConts) {
                        if (next instanceof MessagesTransportWaitingResponse) {
                            final MessagesTransportWaitingResponse mesContain = (MessagesTransportWaitingResponse) next;
                            final TransportCommand cZx = mesContain.getTransportCommand();
                            if (mesContain.isBlocking()) {
                                synchronized (TransportThreadImpl.this.messageEnvelope) {
                                    TransportThreadImpl.this.messageEnvelope.put(cZx.getMessageId(), mesContain);
                                }
                                // monitorexit(TransportThreadImpl.e(this.Bk))
                                synchronized (TransportThreadImpl.this.messagesSent) {
                                    if (TransportThreadImpl.this.messagesSent.containsKey(cZx.getThreadId())) {
                                        mesContain.setTransportWaitingResponse(TransportThreadImpl.this.messagesSent.get(cZx.getThreadId()));
                                    }
                                    TransportThreadImpl.this.messagesSent.put(cZx.getThreadId(), mesContain);
                                }
                                // monitorexit(TransportThreadImpl.c(this.Bk))
                            }
                            try {
                                if (mesContain.isSendServer()) {
                                    TransportThreadImpl.this.procesTransportMessage.sendTransportMessage(cZx.getOutBuffer(), 0, cZx.getOutLength());
                                } else {
                                    TransportThreadImpl.this.procesTransportMessage.sendTransportMessage(mesContain.connect, cZx.getOutBuffer(), 0, cZx.getOutLength());
                                }
                            } catch (IOException ex) {
                                TransportThreadImpl.logger.warn("Error writting message", ex);
                            }
                        } else {
                            if (!(next instanceof MessagesTransportNotWaitingResponse)) {
                                continue;
                            }
                            final MessagesTransportNotWaitingResponse b = (MessagesTransportNotWaitingResponse) next;
                            final TransportResponse dyr = b.transportResponse;
                            TransportThreadImpl.this.procesTransportMessage.sendTransportMessage(b.clientConnect, dyr.getOutBuffer(), 0, dyr.getOutLength());
                        }
                    }
                }
            } catch (axI axI) {
                TransportThreadImpl.logTransportWriter.warn((Object) "queue disposed");
            } catch (C2316dw dw) {
                TransportThreadImpl.logger.info("transport writer interrupted");
            } catch (IOException ex2) {
                ex2.printStackTrace();
            } catch (InterruptedException ex4) {
                TransportThreadImpl.logger.warn("writer interrupted");
                TransportThreadImpl.this.dispose();
            } catch (Exception ex3) {
                TransportThreadImpl.logTransportReader.error((Object) "Uncaught exception: ", (Throwable) ex3);
            } finally {
                TransportThreadImpl.logTransportWriter.info((Object) "stopped");
                TransportThreadImpl.this.dispose();
            }
            TransportThreadImpl.logTransportWriter.info((Object) "stopped");
            TransportThreadImpl.this.dispose();
        }

        private boolean isRunning() {
            return TransportThreadImpl.this.transportWriterThread == Thread.currentThread();
        }
    }


    /* renamed from: a.ea$d */
    /* compiled from: a */
    private class MessagesTransportWaitingResponse {
        /* access modifiers changed from: private */
        public final ClientConnect connect;
        private final TransportCommand transportCommand;
        private boolean blocking;
        private TransportResponse readTransportResponse = null;
        private ClientConnect clientConnectReadResponse;
        private TransportCommand readTransportCommand;
        private MessagesTransportWaitingResponse transportWaitingResponse = null;
        private boolean isSendServer = false;
        private Thread thread;

        public MessagesTransportWaitingResponse(ClientConnect connect, TransportCommand transportCommand, boolean blocking) {
            this.connect = connect;
            this.transportCommand = transportCommand;
            this.blocking = blocking;
            this.thread = Thread.currentThread();
        }

        public boolean isBlocking() {
            return this.blocking;
        }

        public boolean isSendServer() {
            return this.isSendServer;
        }

        public Thread getThread() {
            return this.thread;
        }

        public TransportCommand getTransportCommand() {
            return this.transportCommand;
        }

        public TransportResponse getReadTransportResponse() {
            return this.readTransportResponse;
        }

        /* access modifiers changed from: private */
        /* renamed from: d */
        public synchronized void setTargetReadResponse(ClientConnect bVar, TransportResponse readTransportResponse) {
            this.clientConnectReadResponse = bVar;
            this.readTransportResponse = readTransportResponse;
            notify();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public synchronized void setReadTransportCommand(TransportCommand transportCommand) {
            this.readTransportCommand = transportCommand;
            notify();
        }

        public void sendTransportMessage() {
            TransportThreadImpl.this.checkDispose();
            if (isBlocking() && (TransportThreadImpl.this.integerThreadLocal.get() == null || ((Integer) TransportThreadImpl.this.integerThreadLocal.get()).intValue() != this.transportCommand.getThreadId())) {
                TransportThreadImpl.this.integerThreadLocal.set(Integer.valueOf(this.transportCommand.getThreadId()));
            }
            if (TransportThreadImpl.this.metricThreadId != null) {
                TransportThreadImpl.this.metricThreadId.addThreadIdTrue(this.transportCommand.getThreadId());
            }
            try {
                TransportThreadImpl.this.messageToWriter.put(this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public void sendTransportMessageToServer() {
            TransportThreadImpl.this.checkDispose();
            this.isSendServer = true;
            if (this.transportCommand.getThreadId() == 0) {
                if (TransportThreadImpl.this.integerThreadLocal.get() == null || ((Integer) TransportThreadImpl.this.integerThreadLocal.get()).intValue() == 0) {
                    TransportThreadImpl.this.integerThreadLocal.set(Integer.valueOf(TransportThreadImpl.this.randomUniqueIdentifier()));
                }
                this.transportCommand.setThreadId(((Integer) TransportThreadImpl.this.integerThreadLocal.get()).intValue());
            }
            if (TransportThreadImpl.this.metricThreadId != null) {
                TransportThreadImpl.this.metricThreadId.addThreadIdTrue(this.transportCommand.getThreadId());
            }
            try {
                TransportThreadImpl.this.messageToWriter.put(this);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        public synchronized TransportResponse waitForResponse() {
            while (!hasResponse()) {
                if (this.readTransportCommand == null && !hasResponse()) {
                    if (TransportThreadImpl.this.isDisposed()) {
                        throw new RuntimeException("Transport disposed.");
                    }
                    PoolThread.wait(this);
                    if (TransportThreadImpl.this.isDisposed()) {
                        throw new RuntimeException("Transport disposed.");
                    }
                }
                if (this.readTransportCommand != null) {
                    TransportThreadImpl.this.sendingTransportCommand(this.connect, this.readTransportCommand);
                    this.readTransportCommand = null;
                }
            }
            return this.readTransportResponse;
        }

        public synchronized boolean hasResponse() {
            return this.readTransportResponse != null;
        }

        public MessagesTransportWaitingResponse getTransportWaitingResponse() {
            return this.transportWaitingResponse;
        }

        /* renamed from: a */
        public void setTransportWaitingResponse(MessagesTransportWaitingResponse dVar) {
            this.transportWaitingResponse = dVar;
        }
    }
}
