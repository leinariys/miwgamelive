package game.network.thread;

/* renamed from: a.aII */
/* compiled from: a */
class ThreadLocalInteger extends ThreadLocal<Integer> {

    /* renamed from: Bk */
    final /* synthetic */ TransportThreadImpl transportThread;

    ThreadLocalInteger(TransportThreadImpl thread) {
        this.transportThread = thread;
    }

    /* access modifiers changed from: protected */
    public Integer initialValue() {
        return Integer.valueOf(this.transportThread.randomUniqueIdentifier());
    }
}
