package game.network.exception;

import java.io.IOException;

/* renamed from: a.cU */
/* compiled from: a */
public class IOExceptionServer extends IOException {

    /* renamed from: xA */
    private IOException iOException;

    public IOExceptionServer(String message) {
        super(message);
    }

    public IOExceptionServer(IOException iOException) {
        this.iOException = iOException;
    }

    public IOExceptionServer(String message, IOException iOException) {
        super(message);
        this.iOException = iOException;
    }

    public Throwable getCause() {
        return this.iOException;
    }
}
