package game.network.exception;

/* renamed from: a.mH */
/* compiled from: a */
public class TransportIsDisposedException extends IsDisposedException {


    public TransportIsDisposedException() {
    }

    public TransportIsDisposedException(String str, Throwable th) {
        super(str, th);
    }

    public TransportIsDisposedException(String str) {
        super(str);
    }

    public TransportIsDisposedException(Throwable th) {
        super(th);
    }
}
