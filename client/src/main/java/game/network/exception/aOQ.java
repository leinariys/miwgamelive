package game.network.exception;

/* renamed from: a.aOQ */
/* compiled from: a */
public class aOQ extends RuntimeException {
    private static final long serialVersionUID = 4976966871291883059L;

    public aOQ() {
    }

    public aOQ(String str, Throwable th) {
        super(str, th);
    }

    public aOQ(String str) {
        super(str);
    }

    public aOQ(Throwable th) {
        super(th);
    }
}
