package game.network.metric;

import logic.res.html.*;
import logic.thred.PoolThread;
import logic.thred.ThreadWrapper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.Vector;

/* renamed from: a.yW */
/* compiled from: a */
public class VectorImpl implements MetricVector {
    private Vector<ThreadTimestamp> vector = new Vector<>();
    private String name;

    /* renamed from: a */
    public C1639Xz addC1639Xzfalse(C6663arL arl, C2491fm fmVar) {
        C1639Xz xz = new C1639Xz(arl, fmVar);
        this.vector.add(xz);
        return xz;
    }

    /* renamed from: b */
    public void addC1639Xztrue(C6663arL arl, C2491fm fmVar) {
        this.vector.add(new C1639Xz(arl, fmVar, true));
    }

    /* renamed from: a */
    public void addC3029nD(C1639Xz xz, Exception exc) {
        this.vector.add(new C3029nD(xz, exc));
    }

    /* renamed from: a */
    public void addC6739asj(C1639Xz xz) {
        this.vector.add(new C6739asj(xz));
    }

    public List<ThreadTimestamp> getVector() {
        return this.vector;
    }

    /* renamed from: hd */
    public void addThreadIdTrue(int i) {
        this.vector.add(new aDF(true, i));
    }

    /* renamed from: he */
    public void addThreadIdFalse(int i) {
        this.vector.add(new aDF(false, i));
    }

    /* renamed from: hF */
    public void writeVectorToFile(String str) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(str));
            objectOutputStream.writeObject(this.name);
            objectOutputStream.writeObject(getVector());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object getName() {
        return this.name;
    }

    public void setName(String str) {
        this.name = str;
    }

    /* renamed from: hG */
    public void profilerWriteVectorToFile(String str) {
        new WriteVectorToFileRunnable("Profiler", str).start();
    }

    /* renamed from: a.yW$a */
    class WriteVectorToFileRunnable extends ThreadWrapper {
        private final /* synthetic */ String pathFile;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        WriteVectorToFileRunnable(String nameThread, String pathFile) {
            super(nameThread);
            this.pathFile = pathFile;
        }

        public void run() {
            while (true) {
                try {
                    PoolThread.sleep(30000);
                    VectorImpl.this.writeVectorToFile(this.pathFile);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
