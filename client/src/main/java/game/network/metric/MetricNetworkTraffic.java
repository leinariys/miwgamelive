package game.network.metric;

/* compiled from: a */
public interface MetricNetworkTraffic {
    float getIncomingBytesPerSecond();

    float getIncomingMessagesPerSecond();

    int getNumberOfConnections();

    float getOutgoingBytesPerSecond();

    float getOutgoingMessagesPerSecond();

    float[] getServerOutgoingMessagesStats();
}
