package game.server;

import game.network.build.IServerConnect;
import lombok.extern.slf4j.Slf4j;
import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.JavaScriptException;
import org.mozilla1.javascript.Scriptable;
import org.mozilla1.javascript.Undefined;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.Socket;
import java.net.UnknownHostException;

@Slf4j
/* renamed from: a.Hd */
/* compiled from: a */
public class ServerConsoleConnect {
    private final int PORT = 5458;
    /* access modifiers changed from: private */
    public IServerConsole serverConsole;
    /* access modifiers changed from: private */
    public Socket sock;
    /* access modifiers changed from: private */
    public boolean started = false;
    /* renamed from: TR */
    private Scriptable scriptable;
    private ThreadLocal<Context> threadLocal = new ThreadLocal<Context>() {
        @Override
        public Context initialValue() {
            return ServerConsoleConnect.optimization();
        }
    };

    public ServerConsoleConnect() {
        try {
            this.sock = new Socket(IServerConnect.DEFAULT_HOST, PORT);
        } catch (UnknownHostException e) {
            log.error("Problems during socket connection: Unknown host");
        } catch (IOException e2) {
            log.error("Problems during socket connection");
        }
    }

    /**
     * Уровни от 0 до 9 указывают, что файлы классов могут быть созданы.
     * Более высокие уровни оптимизации приносят компромисс между производительностью
     * времени компиляции и производительностью во время выполнения.
     *
     * @return
     */
    /* access modifiers changed from: private */
    public static Context optimization() {
        Context bkP = Context.call();
        bkP.setOptimizationLevel(0);
        return bkP;
    }

    /* renamed from: BL */
    public void mo2658BL() {
        if (this.scriptable == null) {
            Context contextJs = this.threadLocal.get();
            this.scriptable = contextJs.initStandardObjects();
            try {
                contextJs.evaluateReader(this.scriptable, (Reader) new InputStreamReader(ServerConsoleConnect.class.getClassLoader().getResourceAsStream("taikodom/addon/console/serverconsole.js")), "<ServerConsole>", 1, (Object) null);
            } catch (JavaScriptException e) {
                e.printStackTrace();
            }
            this.scriptable.put("_sconsole_", this.scriptable, (Object) this);
            this.scriptable.put("out", this.scriptable, (Object) System.out);
        }
    }

    /* renamed from: ev */
    public Object mo2662ev(String str) {
        mo2658BL();
        Object a = this.threadLocal.get().evaluateString(this.scriptable, str, "<ServerConsole>", 1, (Object) null);
        if (!(a instanceof Undefined)) {
            return a;
        }
        return null;
    }

    /* renamed from: cP */
    public void mo2660cP(String str) {
        try {
            OutputStream outputStream = this.sock.getOutputStream();
            outputStream.write((String.valueOf(str) + "\n").getBytes("UTF-8"));
            outputStream.flush();
            receivedMessage();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void receivedMessage() {
        if (!this.started) {
            new ThreadServerConsole().start();
        }
    }

    public void clear() {
        if (this.serverConsole != null) {
            this.serverConsole.clearConsole();
        }
    }

    /* renamed from: a */
    public void setServerConsole(IServerConsole serverConsole) {
        this.serverConsole = serverConsole;
    }

    /* renamed from: a.Hd$a */
    class ThreadServerConsole extends Thread {

        public void run() {
            try {
                ServerConsoleConnect.this.started = true;
                while (ServerConsoleConnect.this.started) {
                    StringBuffer stringBuffer = new StringBuffer();
                    while (ServerConsoleConnect.this.sock.getInputStream().available() > 0) {
                        stringBuffer.append((char) ServerConsoleConnect.this.sock.getInputStream().read());
                    }
                    if (stringBuffer.length() > 0 && ServerConsoleConnect.this.serverConsole != null) {
                        ServerConsoleConnect.this.serverConsole.printTerminal(stringBuffer.toString());
                    }
                    Thread.sleep(100);
                }
            } catch (Exception e) {
                e.printStackTrace();
                ServerConsoleConnect.this.started = false;
            }
        }
    }
}
