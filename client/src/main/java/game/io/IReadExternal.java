package game.io;

import java.io.IOException;

/* renamed from: a.Vm */
/* compiled from: a */
public interface IReadExternal {
    /* renamed from: c */
    void mo6352c(String str, byte[] bArr);

    /* renamed from: c */
    void mo6353c(String str, byte[] bArr, int i, int i2);

    /* renamed from: gR */
    boolean mo6354gR(String str) throws IOException;

    /* renamed from: gS */
    byte readByte(String str) throws IOException;

    /* renamed from: gT */
    int mo6356gT(String str);

    /* renamed from: gU */
    short mo6357gU(String str) throws IOException;

    /* renamed from: gV */
    int mo6358gV(String str);

    /* renamed from: gW */
    char readChar(String str) throws IOException;

    /* renamed from: gX */
    int readInt(String str);

    /* renamed from: gY */
    long readLong(String str);

    /* renamed from: gZ */
    float readFloat(String str) throws IOException;

    /* renamed from: ha */
    double readDouble(String str);

    /* renamed from: hb */
    String mo6364hb(String str);

    /* renamed from: hc */
    String mo6365hc(String str);

    /* renamed from: hd */
    <T> T mo6366hd(String str) throws IOException;

    /* renamed from: he */
    <T extends IExternalIO> T mo6367he(String str);

    /* renamed from: hf */
    <T extends Enum<?>> T mo6368hf(String str);

    /* renamed from: hg */
    boolean mo6369hg(String str);

    /* renamed from: pe */
    void mo6370pe() throws IOException;

    /* renamed from: r */
    int mo6371r(String str, int i);
}
