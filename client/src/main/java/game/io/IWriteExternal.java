package game.io;

import java.io.IOException;

/* renamed from: a.att  reason: case insensitive filesystem */
/* compiled from: a */
public interface IWriteExternal {
    /* renamed from: a */
    void writeDouble(String str, double d);

    /* renamed from: a */
    void writeFloat(String str, float f) throws IOException;

    /* renamed from: a */
    void mo16259a(String str, int i, int i2);

    /* renamed from: a */
    void writeLong(String str, long j);

    /* renamed from: a */
    void mo16261a(String str, IExternalIO afa);

    /* renamed from: a */
    void mo16262a(String str, Enum<?> enumR);

    /* renamed from: a */
    void writeBoolean(String str, boolean z);

    /* renamed from: a */
    void mo16264a(String str, byte[] bArr);

    /* renamed from: a */
    void mo16265a(String str, byte[] bArr, int i, int i2);

    /* renamed from: ab */
    void writeString(String str);

    /* renamed from: b */
    void mo16267b(String str, Class<?> cls);

    /* renamed from: c */
    void mo16268c(String str, int i);

    /* renamed from: d */
    void writeByte(String str, int i);

    /* renamed from: e */
    void writeChar(String str, int i);

    /* renamed from: f */
    void writeInt(String str, int i);

    /* renamed from: g */
    void writeShort(String str, int i);

    /* renamed from: g */
    void mo16273g(String str, Object obj);

    /* renamed from: g */
    void mo16274g(String str, String str2);

    /* renamed from: h */
    void mo16275h(String str, String str2);

    /* renamed from: i */
    void mo16276i(String str, String str2);

    /* renamed from: pe */
    void mo16277pe();
}
