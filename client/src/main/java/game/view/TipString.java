package game.view;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2533gT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aae  reason: case insensitive filesystem */
/* compiled from: a */
public class TipString extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4080bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4081bM = null;
    /* renamed from: bN */
    public static final C2491fm f4082bN = null;
    /* renamed from: bO */
    public static final C2491fm f4083bO = null;
    /* renamed from: bP */
    public static final C2491fm f4084bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4085bQ = null;
    public static final C5663aRz eSo = null;
    public static final C2491fm eSp = null;
    public static final C2491fm eSq = null;
    public static final C2491fm eSr = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "269861c359811258adf2e365b1baa452", aum = 0)

    /* renamed from: Rs */
    private static I18NString f4078Rs;
    @C0064Am(aul = "80544aaab9942bc36c9a270cf91e2047", aum = 1)

    /* renamed from: bK */
    private static UUID f4079bK;
    @C0064Am(aul = "8961c82aa1c6fe53c18ba4fa443f62e8", aum = 2)
    private static String handle;

    static {
        m19541V();
    }

    public TipString() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TipString(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19541V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 7;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(TipString.class, "269861c359811258adf2e365b1baa452", i);
        eSo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TipString.class, "80544aaab9942bc36c9a270cf91e2047", i2);
        f4080bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TipString.class, "8961c82aa1c6fe53c18ba4fa443f62e8", i3);
        f4081bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(TipString.class, "b2c640e752889cdc0111d25328cfc3df", i5);
        eSp = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(TipString.class, "c189e785a8b342b286912d8335ab7a2f", i6);
        eSq = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(TipString.class, "8e09ffbf256d823eadb71a90559cdbce", i7);
        eSr = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(TipString.class, "ab7e1214f30dddb76f6be13d670a1311", i8);
        f4084bP = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(TipString.class, "88856721dbf1309d9df5d10fb1eaff5b", i9);
        f4085bQ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(TipString.class, "e65fb55406238821966760d63216887d", i10);
        f4082bN = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(TipString.class, "965251e0e432aa2c74463d2953a57dbc", i11);
        f4083bO = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TipString.class, C2533gT.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m19542a(String str) {
        bFf().mo5608dq().mo3197f(f4081bM, str);
    }

    /* renamed from: a */
    private void m19543a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4080bL, uuid);
    }

    /* renamed from: an */
    private UUID m19544an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4080bL);
    }

    /* renamed from: ao */
    private String m19545ao() {
        return (String) bFf().mo5608dq().mo3214p(f4081bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "88856721dbf1309d9df5d10fb1eaff5b", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m19548b(String str) {
        throw new aWi(new aCE(this, f4085bQ, new Object[]{str}));
    }

    private I18NString bJJ() {
        return (I18NString) bFf().mo5608dq().mo3214p(eSo);
    }

    /* renamed from: c */
    private void m19550c(UUID uuid) {
        switch (bFf().mo6893i(f4083bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4083bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4083bO, new Object[]{uuid}));
                break;
        }
        m19549b(uuid);
    }

    /* renamed from: kn */
    private void m19551kn(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(eSo, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tip label")
    @C0064Am(aul = "c189e785a8b342b286912d8335ab7a2f", aum = 0)
    @C5566aOg
    /* renamed from: ko */
    private void m19552ko(I18NString i18NString) {
        throw new aWi(new aCE(this, eSq, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2533gT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return bJK();
            case 1:
                m19552ko((I18NString) args[0]);
                return null;
            case 2:
                return bJM();
            case 3:
                return m19547ar();
            case 4:
                m19548b((String) args[0]);
                return null;
            case 5:
                return m19546ap();
            case 6:
                m19549b((UUID) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4082bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4082bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4082bN, new Object[0]));
                break;
        }
        return m19546ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tip label")
    public I18NString bJL() {
        switch (bFf().mo6893i(eSp)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, eSp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eSp, new Object[0]));
                break;
        }
        return bJK();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C0909NL
    @ClientOnly
    public String get() {
        switch (bFf().mo6893i(eSr)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, eSr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eSr, new Object[0]));
                break;
        }
        return bJM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4084bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4084bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4084bP, new Object[0]));
                break;
        }
        return m19547ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4085bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4085bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4085bQ, new Object[]{str}));
                break;
        }
        m19548b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tip label")
    @C5566aOg
    /* renamed from: kp */
    public void mo12226kp(I18NString i18NString) {
        switch (bFf().mo6893i(eSq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eSq, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eSq, new Object[]{i18NString}));
                break;
        }
        m19552ko(i18NString);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m19543a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tip label")
    @C0064Am(aul = "b2c640e752889cdc0111d25328cfc3df", aum = 0)
    private I18NString bJK() {
        return bJJ();
    }

    @C0064Am(aul = "8e09ffbf256d823eadb71a90559cdbce", aum = 0)
    @C0909NL
    @ClientOnly
    private String bJM() {
        return bJJ().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "ab7e1214f30dddb76f6be13d670a1311", aum = 0)
    /* renamed from: ar */
    private String m19547ar() {
        return m19545ao();
    }

    @C0064Am(aul = "e65fb55406238821966760d63216887d", aum = 0)
    /* renamed from: ap */
    private UUID m19546ap() {
        return m19544an();
    }

    @C0064Am(aul = "965251e0e432aa2c74463d2953a57dbc", aum = 0)
    /* renamed from: b */
    private void m19549b(UUID uuid) {
        m19543a(uuid);
    }
}
