package game.geometry;

import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;

import javax.vecmath.*;
import java.io.IOException;
import java.nio.FloatBuffer;

/* renamed from: a.aJF */
/* compiled from: a */
public class Vector4fWrap extends Vector4f implements IExternalIO {
    public Vector4fWrap() {
    }

    public Vector4fWrap(float[] fArr) {
        super(fArr);
    }

    public Vector4fWrap(Vector4f vector4f) {
        super(vector4f);
    }

    public Vector4fWrap(Vector4d vector4d) {
        super(vector4d);
    }

    public Vector4fWrap(Tuple4f tuple4f) {
        super(tuple4f);
    }

    public Vector4fWrap(Tuple4d tuple4d) {
        super(tuple4d);
    }

    public Vector4fWrap(Tuple3f tuple3f) {
        super(tuple3f);
    }

    public Vector4fWrap(float f, float f2, float f3, float f4) {
        super(f, f2, f3, f4);
    }

    public Vector4fWrap nScale(float f) {
        return new Vector4fWrap(this.x * f, this.y * f, this.z * f, this.w * f);
    }

    public Vector4fWrap sScale(float f) {
        this.x *= f;
        this.y *= f;
        this.z *= f;
        this.w *= f;
        return this;
    }

    public Vector4fWrap nSub(Tuple4f tuple4f) {
        Vector4fWrap ajf = new Vector4fWrap((Vector4f) this);
        ajf.sub(tuple4f);
        return ajf;
    }

    public FloatBuffer fillBuffer(FloatBuffer floatBuffer) {
        floatBuffer.clear();
        floatBuffer.put(this.x);
        floatBuffer.put(this.y);
        floatBuffer.put(this.z);
        floatBuffer.put(this.w);
        floatBuffer.flip();
        return floatBuffer;
    }

    public void readExternal(IReadExternal vm) throws IOException {
        this.x = vm.readFloat("x");
        this.y = vm.readFloat("y");
        this.z = vm.readFloat("z");
        this.w = vm.readFloat("w");
    }

    public void writeExternal(IWriteExternal att) throws IOException {
        att.writeFloat("x", this.x);
        att.writeFloat("y", this.y);
        att.writeFloat("z", this.z);
        att.writeFloat("w", this.w);
    }
}
