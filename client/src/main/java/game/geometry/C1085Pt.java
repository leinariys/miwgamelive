package game.geometry;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.Pt */
/* compiled from: a */
public class C1085Pt implements C6600aqA {
    public final Vec3f dQv = new Vec3f();
    public float dQw = 0.0f;

    public Vec3f bny() {
        return this.dQv;
    }

    public float getRadius() {
        return this.dQw;
    }

    public void setRadius(float f) {
        this.dQw = f;
    }

    /* renamed from: ad */
    public void mo4862ad(Vec3f vec3f) {
        this.dQv.set(vec3f);
    }

    /* renamed from: a */
    public void mo4861a(Matrix3fWrap ajd, Vec3d ajr, Vec3d ajr2) {
        float f = this.dQw;
        Vec3f vec3f = this.dQv;
        double d = (double) ((ajd.m00 * vec3f.x) + (ajd.m01 * vec3f.y) + (ajd.m02 * vec3f.z));
        double d2 = (double) ((ajd.m10 * vec3f.y) + (ajd.m11 * vec3f.y) + (ajd.m12 * vec3f.z));
        double d3 = (double) ((vec3f.z * ajd.m22) + (ajd.m20 * vec3f.z) + (ajd.m21 * vec3f.y));
        ajr.x = ((double) f) + d;
        ajr.y = ((double) f) + d2;
        ajr.z = ((double) f) + d3;
        ajr2.x = d - ((double) f);
        ajr2.y = d2 - ((double) f);
        ajr2.z = d3 - ((double) f);
    }

    /* renamed from: i */
    public void mo4865i(float f, float f2, float f3) {
        this.dQv.set(f, f2, f3);
    }
}
