package game.geometry;

import com.hoplon.geometry.Vec3f;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import org.mozilla1.javascript.ScriptRuntime;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aJR */
/* compiled from: a */
public class Vec3d extends Vector3d implements IExternalIO, IGeometryD {

    public Vec3d() {
    }

    public Vec3d(Vec3d ajr) {
        this.x = ajr.x;
        this.y = ajr.y;
        this.z = ajr.z;
    }

    public Vec3d(Vec3dWrap iv) {
        this.x = iv.x;
        this.y = iv.y;
        this.z = iv.z;
    }

    public Vec3d(double d, double d2, double d3) {
        this.x = d;
        this.y = d2;
        this.z = d3;
    }

    public Vec3d(Vec3f vec3f) {
        this.x = (double) vec3f.x;
        this.y = (double) vec3f.y;
        this.z = (double) vec3f.z;
    }

    /* renamed from: n */
    public static double m15653n(Vec3d ajr, Vec3d ajr2) {
        return Math.sqrt(((ajr.x - ajr2.x) * (ajr.x - ajr2.x)) + ((ajr.y - ajr2.y) * (ajr.y - ajr2.y)) + ((ajr.z - ajr2.z) * (ajr.z - ajr2.z)));
    }

    /* renamed from: o */
    public static double m15654o(Vec3d ajr, Vec3d ajr2) {
        return ((ajr.x - ajr2.x) * (ajr.x - ajr2.x)) + ((ajr.y - ajr2.y) * (ajr.y - ajr2.y)) + ((ajr.z - ajr2.z) * (ajr.z - ajr2.z));
    }

    /* renamed from: p */
    public static float m15655p(Vec3d ajr, Vec3d ajr2) {
        return (float) (((ajr.x - ajr2.x) * (ajr.x - ajr2.x)) + ((ajr.y - ajr2.y) * (ajr.y - ajr2.y)) + ((ajr.z - ajr2.z) * (ajr.z - ajr2.z)));
    }

    /* renamed from: f */
    public static Vec3d m15650f(Tuple3d tuple3d, Tuple3d tuple3d2) {
        return new Vec3d(tuple3d.x - tuple3d2.x, tuple3d.y - tuple3d2.y, tuple3d.z - tuple3d2.z);
    }

    /* renamed from: c */
    public static Vec3d m15645c(Vec3d ajr, Vec3d ajr2, Vec3d ajr3) {
        Vec3d f = ajr3.mo9517f((Tuple3d) ajr);
        Vec3d f2 = ajr2.mo9517f((Tuple3d) ajr);
        f2.cox();
        double length = ajr.mo9517f((Tuple3d) ajr2).length();
        double az = f2.mo9493az(f);
        if (az <= ScriptRuntime.NaN) {
            return ajr;
        }
        if (az >= length) {
            return ajr2;
        }
        return ajr.mo9504d((Tuple3d) f2.mo9476Z(az));
    }

    /* renamed from: i */
    public static Vec3d m15651i(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        return new Vec3d((double) ((ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3)), (double) ((ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3)), (double) ((f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3)));
    }

    /* renamed from: e */
    public static Vec3d m15648e(Matrix4fWrap ajk, Tuple3d tuple3d) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        return new Vec3d((((double) ajk.m00) * d) + (((double) ajk.m01) * d2) + (((double) ajk.m02) * d3), (((double) ajk.m10) * d) + (((double) ajk.m11) * d2) + (((double) ajk.m12) * d3), (((double) ajk.m22) * d3) + (d * ((double) ajk.m20)) + (d2 * ((double) ajk.m21)));
    }

    /* renamed from: f */
    public static Vec3d m15649f(Matrix4fWrap ajk, Tuple3d tuple3d) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        return new Vec3d((((double) ajk.m00) * d) + (((double) ajk.m01) * d2) + (((double) ajk.m02) * d3) + ((double) ajk.m03), (((double) ajk.m10) * d) + (((double) ajk.m11) * d2) + (((double) ajk.m12) * d3) + ((double) ajk.m13), ((double) ajk.m23) + (d * ((double) ajk.m20)) + (d2 * ((double) ajk.m21)) + (((double) ajk.m22) * d3));
    }

    /* renamed from: j */
    public static Vec3d m15652j(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        return new Vec3d((double) ((ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3) + ajk.m03), (double) ((ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3) + ajk.m13), (double) ((f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3) + ajk.m23));
    }

    /* renamed from: d */
    public static Vec3d m15646d(Vec3d ajr, Vec3d ajr2, Vec3d ajr3) {
        Vec3d f = ajr2.mo9517f((Tuple3d) ajr);
        double length = f.length();
        Vec3d Z = f.mo9476Z(1.0d / length);
        double az = Z.mo9493az(ajr3.mo9517f((Tuple3d) ajr));
        if (az <= ScriptRuntime.NaN) {
            return new Vec3d(ajr);
        }
        if (az >= 1.0d) {
            return new Vec3d(ajr2);
        }
        return ajr.mo9504d((Tuple3d) Z.mo9476Z(length * az));
    }

    /* renamed from: e */
    public static double m15647e(Vec3d ajr, Vec3d ajr2, Vec3d ajr3) {
        return m15653n(m15646d(ajr, ajr2, ajr3), ajr3);
    }

    /* renamed from: ax */
    public double mo9491ax(Vec3d ajr) {
        return Math.sqrt(((this.x - ajr.x) * (this.x - ajr.x)) + ((this.y - ajr.y) * (this.y - ajr.y)) + ((this.z - ajr.z) * (this.z - ajr.z)));
    }

    /* renamed from: Z */
    public Vec3d mo9476Z(double d) {
        Vec3d ajr = new Vec3d();
        ajr.x = this.x * d;
        ajr.y = this.y * d;
        ajr.z = this.z * d;
        return ajr;
    }

    /* renamed from: aa */
    public Vec3d mo9487aa(double d) {
        scale(d);
        return this;
    }

    /* renamed from: d */
    public Vec3d mo9504d(Tuple3d tuple3d) {
        Vec3d ajr = new Vec3d();
        ajr.x = this.x + tuple3d.x;
        ajr.y = this.y + tuple3d.y;
        ajr.z = this.z + tuple3d.z;
        return ajr;
    }

    /* renamed from: e */
    public Vec3d mo9512e(Tuple3d tuple3d) {
        Vec3d.super.add(tuple3d);
        return this;
    }

    /* renamed from: q */
    public Vec3d mo9531q(Tuple3f tuple3f) {
        Vec3d ajr = new Vec3d();
        ajr.x = this.x + ((double) tuple3f.x);
        ajr.y = this.y + ((double) tuple3f.y);
        ajr.z = this.z + ((double) tuple3f.z);
        return ajr;
    }

    /* renamed from: r */
    public Vec3d mo9533r(Tuple3f tuple3f) {
        this.x += (double) tuple3f.x;
        this.y += (double) tuple3f.y;
        this.z += (double) tuple3f.z;
        return this;
    }

    public void add(Tuple3f tuple3f) {
        this.x += (double) tuple3f.x;
        this.y += (double) tuple3f.y;
        this.z += (double) tuple3f.z;
    }

    /* renamed from: f */
    public Vec3d mo9517f(Tuple3d tuple3d) {
        return new Vec3d(this.x - tuple3d.x, this.y - tuple3d.y, this.z - tuple3d.z);
    }

    /* renamed from: s */
    public Vec3d mo9539s(Tuple3f tuple3f) {
        return new Vec3d(this.x - ((double) tuple3f.x), this.y - ((double) tuple3f.y), this.z - ((double) tuple3f.z));
    }

    /* renamed from: g */
    public Vec3d mo9520g(Tuple3d tuple3d) {
        this.x -= tuple3d.x;
        this.y -= tuple3d.y;
        this.z -= tuple3d.z;
        return this;
    }

    /* renamed from: e */
    public Vec3d mo9513e(Tuple3d tuple3d, Tuple3d tuple3d2) {
        this.x = tuple3d.x - tuple3d2.x;
        this.y = tuple3d.y - tuple3d2.y;
        this.z = tuple3d.z - tuple3d2.z;
        return this;
    }

    /* renamed from: t */
    public Vec3d mo9544t(Tuple3f tuple3f) {
        this.x -= (double) tuple3f.x;
        this.y -= (double) tuple3f.y;
        this.z -= (double) tuple3f.z;
        return this;
    }

    public void sub(Tuple3f tuple3f) {
        this.x -= (double) tuple3f.x;
        this.y -= (double) tuple3f.y;
        this.z -= (double) tuple3f.z;
    }

    /* renamed from: o */
    public Vec3d mo9529o(double d, double d2, double d3) {
        this.x -= d;
        this.y -= d2;
        this.z -= d3;
        return this;
    }

    /* renamed from: p */
    public Vec3d mo9530p(double d, double d2, double d3) {
        Vec3d ajr = new Vec3d();
        ajr.x = this.x - d;
        ajr.y = this.y - d2;
        ajr.z = this.z - d3;
        return ajr;
    }

    /* renamed from: q */
    public void mo9532q(double d, double d2, double d3) {
        this.x -= d;
        this.y -= d2;
        this.z -= d3;
    }

    public void cox() {
        double lengthSquared = lengthSquared();
        if (lengthSquared > ScriptRuntime.NaN && lengthSquared != 1.0d) {
            double sqrt = Math.sqrt(lengthSquared);
            this.x /= sqrt;
            this.y /= sqrt;
            this.z /= sqrt;
        }
    }

    /* renamed from: b */
    public Vec3d mo9495b(Vector3d vector3d) {
        return new Vec3d((this.y * vector3d.z) - (this.z * vector3d.y), (this.z * vector3d.x) - (this.x * vector3d.z), (this.x * vector3d.y) - (this.y * vector3d.x));
    }

    /* renamed from: e */
    public Vec3d mo9514e(Vector3f vector3f) {
        return new Vec3d((this.y * ((double) vector3f.z)) - (this.z * ((double) vector3f.y)), (this.z * ((double) vector3f.x)) - (this.x * ((double) vector3f.z)), (this.x * ((double) vector3f.y)) - (this.y * ((double) vector3f.x)));
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public Vec3d dfW() {
        double length = length();
        if (length <= ScriptRuntime.NaN) {
            return new Vec3d(this);
        }
        double d = 1.0d / length;
        return new Vec3d(this.x * d, this.y * d, this.z * d);
    }

    public Vec3d dfX() {
        double length = length();
        if (length > ScriptRuntime.NaN) {
            double d = 1.0d / length;
            this.x *= d;
            this.y *= d;
            this.z = d * this.z;
        }
        return this;
    }

    public Vec3d dfY() {
        return new Vec3d(this);
    }

    /* renamed from: ay */
    public void mo9492ay(Vec3d ajr) {
        ajr.x = this.x;
        ajr.y = this.y;
        ajr.z = this.z;
    }

    /* renamed from: a */
    public double mo9477a(IGeometryD tiVar) {
        return (this.x * tiVar.getX()) + (this.y * tiVar.getY()) + (this.z * tiVar.getZ());
    }

    /* renamed from: b */
    public double mo9494b(IGeometryF teVar) {
        return (this.x * ((double) teVar.getX())) + (this.y * ((double) teVar.getY())) + (this.z * ((double) teVar.getZ()));
    }

    /* renamed from: a */
    public double dot(Vec3dWrap iv) {
        return (this.x * iv.x) + (this.y * iv.y) + (this.z * iv.z);
    }

    /* renamed from: az */
    public double mo9493az(Vec3d ajr) {
        return (this.x * ajr.x) + (this.y * ajr.y) + (this.z * ajr.z);
    }

    public double get(int i) {
        switch (i) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public void set(int i, double d) {
        switch (i) {
            case 0:
                this.x = d;
                return;
            case 1:
                this.y = d;
                return;
            case 2:
                this.z = d;
                return;
            default:
                throw new IllegalArgumentException();
        }
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    /* renamed from: aA */
    public void mo9484aA(Vec3d ajr) {
        this.x = ajr.x;
        this.y = ajr.y;
        this.z = ajr.z;
    }

    /* renamed from: bG */
    public void mo9497bG(Vec3f vec3f) {
        this.x = (double) vec3f.x;
        this.y = (double) vec3f.y;
        this.z = (double) vec3f.z;
    }

    public Vec3f dfV() {
        return new Vec3f((float) this.x, (float) this.y, (float) this.z);
    }

    /* renamed from: aB */
    public Vec3f mo9485aB(Vec3d ajr) {
        return new Vec3f((float) (this.x - ajr.x), (float) (this.y - ajr.y), (float) (this.z - ajr.z));
    }

    /* renamed from: bH */
    public Vec3f mo9498bH(Vec3f vec3f) {
        return new Vec3f((float) (this.x - ((double) vec3f.x)), (float) (this.y - ((double) vec3f.y)), (float) (this.z - ((double) vec3f.z)));
    }

    /* renamed from: r */
    public Vec3f mo9534r(double d, double d2, double d3) {
        return new Vec3f((float) (this.x - d), (float) (this.y - d2), (float) (this.z - d3));
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Vec3d)) {
            return false;
        }
        Vec3d ajr = (Vec3d) obj;
        if (ajr.x == this.x && ajr.y == this.y && ajr.z == this.z) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this.x) + Double.doubleToLongBits(this.y) + Double.doubleToLongBits(this.z);
        return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
    }

    /* renamed from: E */
    public void mo9475E(float f, float f2, float f3) {
        this.x += (double) f;
        this.y += (double) f2;
        this.z += (double) f3;
    }

    /* renamed from: ab */
    public void mo9488ab(double d) {
        this.x *= d;
        this.y *= d;
        this.z *= d;
    }

    /* renamed from: s */
    public Vec3d mo9537s(double d, double d2, double d3) {
        this.x = d;
        this.y = d2;
        this.z = d3;
        return this;
    }

    /* renamed from: h */
    public Vec3d mo9522h(Tuple3d tuple3d) {
        this.x = tuple3d.x;
        this.y = tuple3d.y;
        this.z = tuple3d.z;
        return this;
    }

    /* renamed from: u */
    public Vec3d mo9547u(Tuple3f tuple3f) {
        this.x = (double) tuple3f.x;
        this.y = (double) tuple3f.y;
        this.z = (double) tuple3f.z;
        return this;
    }

    /* renamed from: t */
    public float mo9542t(double d, double d2, double d3) {
        float f = (float) (this.x - d);
        float f2 = (float) (this.y - d2);
        float f3 = (float) (this.z - d3);
        return (f * f) + (f2 * f2) + (f3 * f3);
    }

    public Vec3d dfZ() {
        return new Vec3d(-this.x, -this.y, -this.z);
    }

    public Vec3d dga() {
        negate();
        return this;
    }

    /* renamed from: aC */
    public float mo9486aC(Vec3d ajr) {
        return m15655p(this, ajr);
    }

    public boolean anA() {
        float f = (float) (this.x + this.y + this.z);
        return (Float.isNaN(f) || f == Float.NEGATIVE_INFINITY || f == Float.POSITIVE_INFINITY) ? false : true;
    }

    public void readExternal(ObjectInput objectInput) throws IOException {
        this.x = objectInput.readDouble();
        this.y = objectInput.readDouble();
        this.z = objectInput.readDouble();
    }

    public void writeExternal(ObjectOutput objectOutput) throws IOException {
        objectOutput.writeDouble(this.x);
        objectOutput.writeDouble(this.y);
        objectOutput.writeDouble(this.z);
    }

    /* renamed from: a */
    public boolean mo9483a(Vec3d ajr, double d) {
        return Math.abs(ajr.x - this.x) <= d && Math.abs(ajr.y - this.y) <= d && Math.abs(ajr.z - this.z) <= d;
    }

    /* renamed from: a */
    public void mo9482a(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        this.x = (double) ((ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3));
        this.y = (double) ((ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3));
        this.z = (double) ((f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3));
    }

    /* renamed from: a */
    public void mo9480a(Matrix3fWrap ajd, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        this.x = (double) ((ajd.m00 * f) + (ajd.m01 * f2) + (ajd.m02 * f3));
        this.y = (double) ((ajd.m10 * f) + (ajd.m11 * f2) + (ajd.m12 * f3));
        this.z = (double) ((f * ajd.m20) + (f2 * ajd.m21) + (ajd.m22 * f3));
    }

    /* renamed from: a */
    public void mo9479a(Matrix3fWrap ajd, Tuple3d tuple3d) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        this.x = (((double) ajd.m00) * d) + (((double) ajd.m01) * d2) + (((double) ajd.m02) * d3);
        this.y = (((double) ajd.m10) * d) + (((double) ajd.m11) * d2) + (((double) ajd.m12) * d3);
        this.z = (d * ((double) ajd.m20)) + (d2 * ((double) ajd.m21)) + (((double) ajd.m22) * d3);
    }

    /* renamed from: a */
    public void mo9481a(Matrix4fWrap ajk, Tuple3d tuple3d) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        this.x = (((double) ajk.m00) * d) + (((double) ajk.m01) * d2) + (((double) ajk.m02) * d3);
        this.y = (((double) ajk.m10) * d) + (((double) ajk.m11) * d2) + (((double) ajk.m12) * d3);
        this.z = (d * ((double) ajk.m20)) + (d2 * ((double) ajk.m21)) + (((double) ajk.m22) * d3);
    }

    /* renamed from: g */
    public Vec3d mo9519g(Matrix4fWrap ajk, Tuple3f tuple3f) {
        mo9482a(ajk, tuple3f);
        return this;
    }

    /* renamed from: b */
    public void mo9496b(Matrix4fWrap ajk, Tuple3d tuple3d) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        this.x = (((double) ajk.m00) * d) + (((double) ajk.m01) * d2) + (((double) ajk.m02) * d3) + ((double) ajk.m03);
        this.y = (((double) ajk.m10) * d) + (((double) ajk.m11) * d2) + (((double) ajk.m12) * d3) + ((double) ajk.m13);
        this.z = (d * ((double) ajk.m20)) + (d2 * ((double) ajk.m21)) + (((double) ajk.m22) * d3) + ((double) ajk.m23);
    }

    /* renamed from: c */
    public Vec3d mo9499c(Matrix4fWrap ajk, Tuple3d tuple3d) {
        mo9496b(ajk, tuple3d);
        return this;
    }

    /* renamed from: d */
    public Vec3d mo9503d(Matrix4fWrap ajk, Tuple3d tuple3d) {
        mo9481a(ajk, tuple3d);
        return this;
    }

    /* renamed from: e */
    public void mo9515e(Matrix4fWrap ajk, Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        this.x = (double) ((ajk.m00 * f) + (ajk.m01 * f2) + (ajk.m02 * f3) + ajk.m03);
        this.y = (double) ((ajk.m10 * f) + (ajk.m11 * f2) + (ajk.m12 * f3) + ajk.m13);
        this.z = (double) ((f * ajk.m20) + (f2 * ajk.m21) + (ajk.m22 * f3) + ajk.m23);
    }

    /* renamed from: h */
    public Vec3d mo9521h(Matrix4fWrap ajk, Tuple3f tuple3f) {
        mo9515e(ajk, tuple3f);
        return this;
    }

    /* renamed from: i */
    public Vec3d mo9525i(Tuple3d tuple3d) {
        return new Vec3d(this.x * tuple3d.x, this.y * tuple3d.y, this.z * tuple3d.z);
    }

    /* renamed from: j */
    public Vec3d mo9527j(Tuple3d tuple3d) {
        this.x *= tuple3d.x;
        this.y *= tuple3d.y;
        this.z *= tuple3d.z;
        return this;
    }

    /* renamed from: k */
    public void mo9528k(Tuple3d tuple3d) {
        this.x *= tuple3d.x;
        this.y *= tuple3d.y;
        this.z *= tuple3d.z;
    }

    /* renamed from: v */
    public Vec3d mo9550v(Tuple3f tuple3f) {
        return new Vec3d(this.x * ((double) tuple3f.x), this.y * ((double) tuple3f.y), this.z * ((double) tuple3f.z));
    }

    /* renamed from: w */
    public Vec3d mo9554w(Tuple3f tuple3f) {
        this.x *= (double) tuple3f.x;
        this.y *= (double) tuple3f.y;
        this.z *= (double) tuple3f.z;
        return this;
    }

    /* renamed from: h */
    public void mo9523h(Tuple3f tuple3f) {
        this.x *= (double) tuple3f.x;
        this.y *= (double) tuple3f.y;
        this.z *= (double) tuple3f.z;
    }

    /* renamed from: c */
    public void mo9501c(Vector3f vector3f) {
        mo9551v((double) vector3f.x, (double) vector3f.y, (double) vector3f.z);
    }

    /* renamed from: c */
    public Vec3d mo9500c(Vector3d vector3d) {
        return mo9546u(vector3d.x, vector3d.y, vector3d.z);
    }

    /* renamed from: d */
    public void mo9505d(Vector3d vector3d) {
        mo9551v(vector3d.x, vector3d.y, vector3d.z);
    }

    /* renamed from: f */
    public Vec3d mo9518f(Vector3f vector3f) {
        return mo9546u((double) vector3f.x, (double) vector3f.y, (double) vector3f.z);
    }

    /* renamed from: u */
    public Vec3d mo9546u(double d, double d2, double d3) {
        double d4 = (this.y * d3) - (this.z * d2);
        double d5 = (this.z * d) - (this.x * d3);
        this.x = d4;
        this.y = d5;
        this.z = (this.x * d2) - (this.y * d);
        return this;
    }

    /* renamed from: v */
    public void mo9551v(double d, double d2, double d3) {
        double d4 = (this.y * d3) - (this.z * d2);
        double d5 = (this.z * d) - (this.x * d3);
        this.x = d4;
        this.y = d5;
        this.z = (this.x * d2) - (this.y * d);
    }

    /* renamed from: w */
    public Vec3d mo9552w(double d, double d2, double d3) {
        return new Vec3d((this.y * d3) - (this.z * d2), (this.z * d) - (this.x * d3), (this.x * d2) - (this.y * d));
    }

    /* renamed from: s */
    public Vec3d mo9538s(Matrix4fWrap ajk) {
        return m15649f(ajk, (Tuple3d) this);
    }

    /* renamed from: t */
    public Vec3d mo9543t(Matrix4fWrap ajk) {
        return new Vec3d((this.z * ((double) ajk.m02)) + (this.x * ((double) ajk.m00)) + (this.y * ((double) ajk.m01)), (this.z * ((double) ajk.m12)) + (this.x * ((double) ajk.m10)) + (this.y * ((double) ajk.m11)), (this.z * ((double) ajk.m22)) + (this.x * ((double) ajk.m20)) + (this.y * ((double) ajk.m21)));
    }

    /* renamed from: u */
    public void mo9548u(Matrix4fWrap ajk) {
        double d = (this.x * ((double) ajk.m00)) + (this.y * ((double) ajk.m01)) + (this.z * ((double) ajk.m02)) + ((double) ajk.m03);
        double d2 = (this.x * ((double) ajk.m10)) + (this.y * ((double) ajk.m11)) + (this.z * ((double) ajk.m12)) + ((double) ajk.m13);
        this.x = d;
        this.y = d2;
        this.z = (this.x * ((double) ajk.m20)) + (this.y * ((double) ajk.m21)) + (this.z * ((double) ajk.m22)) + ((double) ajk.m23);
    }

    /* renamed from: r */
    public void mo9535r(Matrix4fWrap ajk) {
        double d = (this.x * ((double) ajk.m00)) + (this.y * ((double) ajk.m01)) + (this.z * ((double) ajk.m02));
        double d2 = (this.x * ((double) ajk.m10)) + (this.y * ((double) ajk.m11)) + (this.z * ((double) ajk.m12));
        this.x = d;
        this.y = d2;
        this.z = (this.x * ((double) ajk.m20)) + (this.y * ((double) ajk.m21)) + (this.z * ((double) ajk.m22));
    }

    /* renamed from: v */
    public Vec3d mo9549v(Matrix4fWrap ajk) {
        mo9548u(ajk);
        return this;
    }

    /* renamed from: w */
    public Vec3d mo9553w(Matrix4fWrap ajk) {
        mo9535r(ajk);
        return this;
    }

    /* renamed from: a */
    public void mo9478a(float f, Tuple3f tuple3f, Tuple3d tuple3d) {
        this.x = ((double) (tuple3f.x * f)) + tuple3d.x;
        this.y = ((double) (tuple3f.y * f)) + tuple3d.y;
        this.z = ((double) (tuple3f.z * f)) + tuple3d.z;
    }

    public boolean isValid() {
        return !Double.isNaN(this.x) && !Double.isInfinite(this.x) && !Double.isNaN(this.y) && !Double.isInfinite(this.y) && !Double.isNaN(this.z) && !Double.isInfinite(this.z);
    }

    /* renamed from: x */
    public void mo9556x(double d, double d2, double d3) {
        this.x += d;
        this.y += d2;
        this.z += d3;
    }

    /* renamed from: y */
    public Vec3d mo9557y(double d, double d2, double d3) {
        return new Vec3d(this.x + d, this.y + d2, this.z + d3);
    }

    /* renamed from: z */
    public Vec3d mo9558z(double d, double d2, double d3) {
        this.x += d;
        this.y += d2;
        this.z += d3;
        return this;
    }

    public void readExternal(IReadExternal vm) {
        this.x = vm.readDouble("x");
        this.y = vm.readDouble("y");
        this.z = vm.readDouble("z");
    }

    public void writeExternal(IWriteExternal att) {
        att.writeDouble("x", this.x);
        att.writeDouble("y", this.y);
        att.writeDouble("z", this.z);
    }
}
