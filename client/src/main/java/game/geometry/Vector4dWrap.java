package game.geometry;

import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;

import javax.vecmath.*;
import java.nio.DoubleBuffer;

/* renamed from: a.aJD */
/* compiled from: a */
public class Vector4dWrap extends Vector4d implements IExternalIO {

    public Vector4dWrap() {
    }

    public Vector4dWrap(double[] dArr) {
        super(dArr);
    }

    public Vector4dWrap(Vector4f vector4f) {
        super(vector4f);
    }

    public Vector4dWrap(Vector4d vector4d) {
        super(vector4d);
    }

    public Vector4dWrap(Tuple4f tuple4f) {
        super(tuple4f);
    }

    public Vector4dWrap(Tuple4d tuple4d) {
        super(tuple4d);
    }

    public Vector4dWrap(Tuple3d tuple3d) {
        super(tuple3d);
    }

    public Vector4dWrap(double d, double d2, double d3, double d4) {
        super(d, d2, d3, d4);
    }

    /* renamed from: Y */
    public Vector4dWrap mo9455Y(double d) {
        return new Vector4dWrap(this.x * d, this.y * d, this.z * d, this.w * d);
    }

    /* renamed from: mR */
    public Vector4dWrap mo9457mR(float f) {
        this.x *= (double) f;
        this.y *= (double) f;
        this.z *= (double) f;
        this.w *= (double) f;
        return this;
    }

    /* renamed from: a */
    public DoubleBuffer mo9456a(DoubleBuffer doubleBuffer) {
        doubleBuffer.clear();
        doubleBuffer.put(this.x);
        doubleBuffer.put(this.y);
        doubleBuffer.put(this.z);
        doubleBuffer.put(this.w);
        doubleBuffer.flip();
        return doubleBuffer;
    }

    public void readExternal(IReadExternal vm) {
        this.x = vm.readDouble("x");
        this.y = vm.readDouble("y");
        this.z = vm.readDouble("z");
        this.w = vm.readDouble("w");
    }

    public void writeExternal(IWriteExternal att) {
        att.writeDouble("x", this.x);
        att.writeDouble("y", this.y);
        att.writeDouble("z", this.z);
        att.writeDouble("w", this.w);
    }
}
