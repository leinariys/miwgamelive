package game.geometry;

import com.hoplon.geometry.Vec3f;
import org.mozilla1.javascript.ScriptRuntime;

/* renamed from: a.IV */
/* compiled from: a */
public final class Vec3dWrap implements IGeometryD {


    /* renamed from: x */
    public final double x;

    /* renamed from: y */
    public final double y;

    /* renamed from: z */
    public final double z;

    public Vec3dWrap(Vec3dWrap iv) {
        this.x = iv.x;
        this.y = iv.y;
        this.z = iv.z;
    }

    public Vec3dWrap(Vec3d ajr) {
        this.x = ajr.x;
        this.y = ajr.y;
        this.z = ajr.z;
    }

    public Vec3dWrap(double d, double d2, double d3) {
        this.x = d;
        this.y = d2;
        this.z = d3;
    }

    public Vec3dWrap(Vec3f vec3f) {
        this.x = (double) vec3f.x;
        this.y = (double) vec3f.y;
        this.z = (double) vec3f.z;
    }

    /* renamed from: a */
    public static double m5398a(Vec3dWrap iv, Vec3dWrap iv2) {
        return Math.pow(((iv.x - iv2.x) * (iv.x - iv2.x)) + ((iv.y - iv2.y) * (iv.y - iv2.y)) + ((iv.z - iv2.z) * (iv.z - iv2.z)), 0.5d);
    }

    /* renamed from: b */
    public static double m5400b(Vec3dWrap iv, Vec3dWrap iv2) {
        return ((iv.x - iv2.x) * (iv.x - iv2.x)) + ((iv.y - iv2.y) * (iv.y - iv2.y)) + ((iv.z - iv2.z) * (iv.z - iv2.z));
    }

    /* renamed from: a */
    public static Vec3dWrap m5399a(Vec3dWrap iv, Vec3dWrap iv2, Vec3dWrap iv3) {
        Vec3dWrap d = iv3.mo2832d(iv);
        Vec3dWrap aXh = iv2.mo2832d(iv).aXh();
        double length = iv.mo2832d(iv2).length();
        double a = aXh.dot(d);
        if (a <= ScriptRuntime.NaN) {
            return iv;
        }
        if (a >= length) {
            return iv2;
        }
        return iv.mo2831c(aXh.mo2824A(a));
    }

    /* renamed from: A */
    public Vec3dWrap mo2824A(double d) {
        return new Vec3dWrap(this.x * d, this.y * d, this.z * d);
    }

    /* renamed from: b */
    public Vec3dWrap mo2830b(Vec3dWrap iv) {
        return new Vec3dWrap(this.x * iv.x, this.y * iv.y, this.z * iv.z);
    }

    /* renamed from: c */
    public Vec3dWrap mo2831c(Vec3dWrap iv) {
        return new Vec3dWrap(this.x + iv.x, this.y + iv.y, this.z + iv.z);
    }

    /* renamed from: d */
    public Vec3dWrap mo2832d(Vec3dWrap iv) {
        return new Vec3dWrap(this.x - iv.x, this.y - iv.y, this.z - iv.z);
    }

    public double lengthSquared() {
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
    }

    /* renamed from: e */
    public Vec3dWrap mo2833e(Vec3dWrap iv) {
        return new Vec3dWrap((this.y * iv.z) - (this.z * iv.y), (this.z * iv.x) - (this.x * iv.z), (this.x * iv.y) - (this.y * iv.x));
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public Vec3dWrap aXh() {
        double d = (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
        if (d <= ScriptRuntime.NaN || d == 1.0d) {
            return this;
        }
        double sqrt = Math.sqrt(d);
        return new Vec3dWrap(this.x / sqrt, this.y / sqrt, this.z / sqrt);
    }

    public double length() {
        return Math.sqrt((this.x * this.x) + (this.y * this.y) + (this.z * this.z));
    }

    /* renamed from: a */
    public double dot(Vec3dWrap iv) {
        return (this.x * iv.x) + (this.y * iv.y) + (this.z * iv.z);
    }

    public double get(int i) {
        switch (i) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }
}
