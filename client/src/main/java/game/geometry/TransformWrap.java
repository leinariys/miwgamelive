package game.geometry;

import com.hoplon.geometry.Vec3f;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import org.mozilla1.javascript.ScriptRuntime;

import java.io.Serializable;
import java.nio.FloatBuffer;

/* renamed from: a.bc */
/* compiled from: a */
public class TransformWrap implements IExternalIO, Serializable {


    /* renamed from: mX */
    public Matrix3fWrap orientation = new Matrix3fWrap();
    public Vec3d position = new Vec3d();

    public TransformWrap() {
        this.orientation.setIdentity();
    }

    public TransformWrap(Matrix4fWrap ajk) {
        set(ajk);
    }

    public TransformWrap(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, double d, double d2, double d3) {
        this.orientation.m00 = f;
        this.orientation.m10 = f4;
        this.orientation.m20 = f7;
        this.orientation.m01 = f2;
        this.orientation.m11 = f5;
        this.orientation.m21 = f8;
        this.orientation.m02 = f3;
        this.orientation.m12 = f6;
        this.orientation.m22 = f9;
        this.position.x = d;
        this.position.y = d2;
        this.position.z = d3;
    }

    public TransformWrap(TransformWrap bcVar) {
        mo17344b(bcVar);
    }

    public TransformWrap(Quat4fWrap aoy, Vec3d ajr) {
        setOrientation(aoy);
        setTranslation(ajr);
    }

    /* renamed from: a */
    private static void m27925a(TransformWrap bcVar, TransformWrap bcVar2, TransformWrap bcVar3) {
        Matrix3fWrap ajd = bcVar.orientation;
        Matrix3fWrap ajd2 = bcVar2.orientation;
        float f = (ajd.m00 * ajd2.m00) + (ajd.m01 * ajd2.m10) + (ajd.m02 * ajd2.m20);
        float f2 = (ajd.m10 * ajd2.m00) + (ajd.m11 * ajd2.m10) + (ajd.m12 * ajd2.m20);
        float f3 = (ajd.m20 * ajd2.m00) + (ajd.m21 * ajd2.m10) + (ajd.m22 * ajd2.m20);
        float f4 = (ajd.m00 * ajd2.m01) + (ajd.m01 * ajd2.m11) + (ajd.m02 * ajd2.m21);
        float f5 = (ajd.m10 * ajd2.m01) + (ajd.m11 * ajd2.m11) + (ajd.m12 * ajd2.m21);
        float f6 = (ajd.m20 * ajd2.m01) + (ajd.m21 * ajd2.m11) + (ajd.m22 * ajd2.m21);
        float f7 = (ajd.m00 * ajd2.m02) + (ajd.m01 * ajd2.m12) + (ajd.m02 * ajd2.m22);
        float f8 = (ajd.m10 * ajd2.m02) + (ajd.m11 * ajd2.m12) + (ajd.m12 * ajd2.m22);
        float f9 = (ajd2.m22 * ajd.m22) + (ajd.m20 * ajd2.m02) + (ajd.m21 * ajd2.m12);
        Vec3d ajr = bcVar2.position;
        Vec3d ajr2 = bcVar.position;
        double d = (((double) ajd.m00) * ajr.x) + (((double) ajd.m01) * ajr.y) + (((double) ajd.m02) * ajr.z) + ajr2.x;
        double d2 = (((double) ajd.m10) * ajr.x) + (((double) ajd.m11) * ajr.y) + (((double) ajd.m12) * ajr.z) + ajr2.y;
        double d3 = ajr2.z;
        Matrix3fWrap ajd3 = bcVar3.orientation;
        ajd3.m00 = f;
        ajd3.m10 = f2;
        ajd3.m20 = f3;
        ajd3.m01 = f4;
        ajd3.m11 = f5;
        ajd3.m21 = f6;
        ajd3.m02 = f7;
        ajd3.m12 = f8;
        ajd3.m22 = f9;
        bcVar3.position.x = d;
        bcVar3.position.y = d2;
        bcVar3.position.z = d3 + (((double) ajd.m20) * ajr.x) + (((double) ajd.m21) * ajr.y) + (((double) ajd.m22) * ajr.z);
    }

    static boolean isZero(float f) {
        return f < 1.0E-5f && f > -1.0E-5f;
    }

    public void transform(Vector3f vector3f, Vector3f vector3f2) {
        float f = (float) (((double) ((vector3f.x * this.orientation.m10) + (vector3f.y * this.orientation.m11) + (vector3f.z * this.orientation.m12))) + this.position.y);
        vector3f2.z = (float) (((double) ((vector3f.x * this.orientation.m20) + (vector3f.y * this.orientation.m21) + (vector3f.z * this.orientation.m22))) + this.position.z);
        vector3f2.y = f;
        vector3f2.x = (float) (((double) ((vector3f.x * this.orientation.m00) + (vector3f.y * this.orientation.m01) + (vector3f.z * this.orientation.m02))) + this.position.x);
    }

    public void transform(Vector3f vector3f) {
        transform(vector3f, vector3f);
    }

    /* renamed from: a */
    public void mo17339a(Vector3f vector3f, Vector3d vector3d) {
        vector3d.x = ((double) ((vector3f.x * this.orientation.m00) + (vector3f.y * this.orientation.m01) + (vector3f.z * this.orientation.m02))) + this.position.x;
        vector3d.y = ((double) ((vector3f.x * this.orientation.m10) + (vector3f.y * this.orientation.m11) + (vector3f.z * this.orientation.m12))) + this.position.y;
        vector3d.z = ((double) ((vector3f.x * this.orientation.m20) + (vector3f.y * this.orientation.m21) + (vector3f.z * this.orientation.m22))) + this.position.z;
    }

    public void multiply3x3(Vec3f vec3f, Vec3f vec3f2) {
        vec3f2.mo23472a(this.orientation, (Tuple3f) vec3f);
    }

    public void multiply3x3(Vec3d ajr, Vec3d ajr2) {
        ajr2.mo9479a(this.orientation, (Tuple3d) ajr);
    }

    public void multiply3x3(Vec3f vec3f, Vec3d ajr) {
        ajr.mo9480a(this.orientation, (Tuple3f) vec3f);
    }

    /* renamed from: a */
    public void mo17332a(Vec3d ajr, Vec3d ajr2) {
        float f = 1.0f / (((this.orientation.m00 * this.orientation.m00) + (this.orientation.m10 * this.orientation.m10)) + (this.orientation.m20 * this.orientation.m20));
        float f2 = 1.0f / (((this.orientation.m01 * this.orientation.m01) + (this.orientation.m11 * this.orientation.m11)) + (this.orientation.m21 * this.orientation.m21));
        float f3 = 1.0f / (((this.orientation.m02 * this.orientation.m02) + (this.orientation.m12 * this.orientation.m12)) + (this.orientation.m22 * this.orientation.m22));
        float f4 = this.orientation.m00 * f;
        float f5 = this.orientation.m10 * f2;
        float f6 = this.orientation.m20 * f3;
        float f7 = this.orientation.m01 * f;
        float f8 = this.orientation.m11 * f2;
        float f9 = this.orientation.m21 * f3;
        float f10 = f * this.orientation.m02;
        float f11 = f2 * this.orientation.m12;
        float f12 = f3 * this.orientation.m22;
        double d = -((this.position.x * ((double) this.orientation.m02)) + (this.position.y * ((double) this.orientation.m12)) + (this.position.z * ((double) this.orientation.m22)));
        double d2 = ajr.x;
        double d3 = ajr.y;
        double d4 = ajr.z;
        ajr2.x = (((double) f6) * d4) + (((double) f4) * d2) + (((double) f5) * d3) + (-((this.position.x * ((double) this.orientation.m00)) + (this.position.y * ((double) this.orientation.m10)) + (this.position.z * ((double) this.orientation.m20))));
        ajr2.y = (((double) f7) * d2) + (((double) f8) * d3) + (((double) f9) * d4) + (-((this.position.x * ((double) this.orientation.m01)) + (this.position.y * ((double) this.orientation.m11)) + (this.position.z * ((double) this.orientation.m21))));
        ajr2.z = (((double) f11) * d3) + (((double) f10) * d2) + (((double) f12) * d4) + d;
    }

    /* renamed from: a */
    public void mo17333a(Vec3d ajr, Vec3f vec3f) {
        float f = 1.0f / (((this.orientation.m00 * this.orientation.m00) + (this.orientation.m10 * this.orientation.m10)) + (this.orientation.m20 * this.orientation.m20));
        float f2 = 1.0f / (((this.orientation.m01 * this.orientation.m01) + (this.orientation.m11 * this.orientation.m11)) + (this.orientation.m21 * this.orientation.m21));
        float f3 = 1.0f / (((this.orientation.m02 * this.orientation.m02) + (this.orientation.m12 * this.orientation.m12)) + (this.orientation.m22 * this.orientation.m22));
        float f4 = this.orientation.m00 * f;
        float f5 = this.orientation.m10 * f2;
        float f6 = this.orientation.m20 * f3;
        float f7 = this.orientation.m01 * f;
        float f8 = this.orientation.m11 * f2;
        float f9 = this.orientation.m21 * f3;
        float f10 = f * this.orientation.m02;
        float f11 = f2 * this.orientation.m12;
        float f12 = f3 * this.orientation.m22;
        double d = -((this.position.x * ((double) this.orientation.m02)) + (this.position.y * ((double) this.orientation.m12)) + (this.position.z * ((double) this.orientation.m22)));
        double d2 = ajr.x;
        double d3 = ajr.y;
        double d4 = ajr.z;
        vec3f.x = (float) ((((double) f6) * d4) + (((double) f4) * d2) + (((double) f5) * d3) + (-((this.position.x * ((double) this.orientation.m00)) + (this.position.y * ((double) this.orientation.m10)) + (this.position.z * ((double) this.orientation.m20)))));
        vec3f.y = (float) ((((double) f7) * d2) + (((double) f8) * d3) + (((double) f9) * d4) + (-((this.position.x * ((double) this.orientation.m01)) + (this.position.y * ((double) this.orientation.m11)) + (this.position.z * ((double) this.orientation.m21)))));
        vec3f.z = (float) ((((double) f11) * d3) + (((double) f10) * d2) + (((double) f12) * d4) + d);
    }

    /* renamed from: b */
    public void mo17345b(Vec3f vec3f, Vec3f vec3f2) {
        float f = 1.0f / (((this.orientation.m00 * this.orientation.m00) + (this.orientation.m10 * this.orientation.m10)) + (this.orientation.m20 * this.orientation.m20));
        float f2 = 1.0f / (((this.orientation.m01 * this.orientation.m01) + (this.orientation.m11 * this.orientation.m11)) + (this.orientation.m21 * this.orientation.m21));
        float f3 = 1.0f / (((this.orientation.m02 * this.orientation.m02) + (this.orientation.m12 * this.orientation.m12)) + (this.orientation.m22 * this.orientation.m22));
        float f4 = this.orientation.m00 * f;
        float f5 = this.orientation.m10 * f2;
        float f6 = this.orientation.m20 * f3;
        float f7 = this.orientation.m01 * f;
        float f8 = this.orientation.m11 * f2;
        float f9 = this.orientation.m21 * f3;
        float f10 = f * this.orientation.m02;
        float f11 = f2 * this.orientation.m12;
        float f12 = f3 * this.orientation.m22;
        double d = -((this.position.x * ((double) this.orientation.m02)) + (this.position.y * ((double) this.orientation.m12)) + (this.position.z * ((double) this.orientation.m22)));
        double d2 = (double) vec3f.x;
        double d3 = (double) vec3f.y;
        double d4 = (double) vec3f.z;
        vec3f2.x = (float) ((((double) f6) * d4) + (((double) f4) * d2) + (((double) f5) * d3) + (-((this.position.x * ((double) this.orientation.m00)) + (this.position.y * ((double) this.orientation.m10)) + (this.position.z * ((double) this.orientation.m20)))));
        vec3f2.y = (float) ((((double) f7) * d2) + (((double) f8) * d3) + (((double) f9) * d4) + (-((this.position.x * ((double) this.orientation.m01)) + (this.position.y * ((double) this.orientation.m11)) + (this.position.z * ((double) this.orientation.m21)))));
        vec3f2.z = (float) ((((double) f11) * d3) + (((double) f10) * d2) + (((double) f12) * d4) + d);
    }

    /* renamed from: a */
    public void mo17338a(Vec3f vec3f, Vec3d ajr) {
        float f = 1.0f / (((this.orientation.m00 * this.orientation.m00) + (this.orientation.m10 * this.orientation.m10)) + (this.orientation.m20 * this.orientation.m20));
        float f2 = 1.0f / (((this.orientation.m01 * this.orientation.m01) + (this.orientation.m11 * this.orientation.m11)) + (this.orientation.m21 * this.orientation.m21));
        float f3 = 1.0f / (((this.orientation.m02 * this.orientation.m02) + (this.orientation.m12 * this.orientation.m12)) + (this.orientation.m22 * this.orientation.m22));
        float f4 = this.orientation.m00 * f;
        float f5 = this.orientation.m10 * f2;
        float f6 = this.orientation.m20 * f3;
        float f7 = this.orientation.m01 * f;
        float f8 = this.orientation.m11 * f2;
        float f9 = this.orientation.m21 * f3;
        float f10 = f * this.orientation.m02;
        float f11 = f2 * this.orientation.m12;
        float f12 = f3 * this.orientation.m22;
        double d = -((this.position.x * ((double) this.orientation.m02)) + (this.position.y * ((double) this.orientation.m12)) + (this.position.z * ((double) this.orientation.m22)));
        double d2 = (double) vec3f.x;
        double d3 = (double) vec3f.y;
        double d4 = (double) vec3f.z;
        ajr.x = (double) ((float) ((((double) f6) * d4) + (((double) f4) * d2) + (((double) f5) * d3) + (-((this.position.x * ((double) this.orientation.m00)) + (this.position.y * ((double) this.orientation.m10)) + (this.position.z * ((double) this.orientation.m20))))));
        ajr.y = (double) ((float) ((((double) f7) * d2) + (((double) f8) * d3) + (((double) f9) * d4) + (-((this.position.x * ((double) this.orientation.m01)) + (this.position.y * ((double) this.orientation.m11)) + (this.position.z * ((double) this.orientation.m21))))));
        ajr.z = (double) ((float) ((((double) f11) * d3) + (((double) f10) * d2) + (((double) f12) * d4) + d));
    }

    public void getX(Vec3f vec3f) {
        vec3f.x = this.orientation.m00;
        vec3f.y = this.orientation.m10;
        vec3f.z = this.orientation.m20;
    }

    public void getY(Vec3f vec3f) {
        vec3f.x = this.orientation.m01;
        vec3f.y = this.orientation.m11;
        vec3f.z = this.orientation.m21;
    }

    public void getZ(Vec3f vec3f) {
        vec3f.x = this.orientation.m02;
        vec3f.y = this.orientation.m12;
        vec3f.z = this.orientation.m22;
    }

    public void getTranslation(Vec3d ajr) {
        ajr.x = this.position.x;
        ajr.y = this.position.y;
        ajr.z = this.position.z;
    }

    public void getTranslation(Vec3f vec3f) {
        vec3f.x = (float) this.position.x;
        vec3f.y = (float) this.position.y;
        vec3f.z = (float) this.position.z;
    }

    public void setX(Vec3f vec3f) {
        this.orientation.m00 = vec3f.x;
        this.orientation.m10 = vec3f.y;
        this.orientation.m20 = vec3f.z;
    }

    public void setX(float f, float f2, float f3) {
        this.orientation.m00 = f;
        this.orientation.m10 = f2;
        this.orientation.m20 = f3;
    }

    public void setY(Vec3f vec3f) {
        this.orientation.m01 = vec3f.x;
        this.orientation.m11 = vec3f.y;
        this.orientation.m21 = vec3f.z;
    }

    public void setY(float f, float f2, float f3) {
        this.orientation.m01 = f;
        this.orientation.m11 = f2;
        this.orientation.m21 = f3;
    }

    public void setZ(Vec3f vec3f) {
        this.orientation.m02 = vec3f.x;
        this.orientation.m12 = vec3f.y;
        this.orientation.m22 = vec3f.z;
    }

    public void setZ(float f, float f2, float f3) {
        this.orientation.m02 = f;
        this.orientation.m12 = f2;
        this.orientation.m22 = f3;
    }

    public void setTranslation(double d, double d2, double d3) {
        this.position.x = d;
        this.position.y = d2;
        this.position.z = d3;
    }

    public void setTranslation(Vec3d ajr) {
        this.position.x = ajr.x;
        this.position.y = ajr.y;
        this.position.z = ajr.z;
    }

    public void setTranslation(Vec3f vec3f) {
        this.position.x = (double) vec3f.x;
        this.position.y = (double) vec3f.y;
        this.position.z = (double) vec3f.z;
    }

    public boolean hasRotation() {
        float asin;
        float atan2;
        float atan22;
        float f = -this.orientation.m12;
        if (f <= -1.0f) {
            asin = -1.570796f;
        } else if (((double) f) >= 1.0d) {
            asin = 1.570796f;
        } else {
            asin = (float) Math.asin((double) f);
        }
        if (f > 0.9999f) {
            atan2 = (float) Math.atan2((double) (-this.orientation.m02), (double) this.orientation.m00);
            atan22 = 0.0f;
        } else {
            atan2 = (float) Math.atan2((double) this.orientation.m02, (double) this.orientation.m22);
            atan22 = (float) Math.atan2((double) this.orientation.m10, (double) this.orientation.m11);
        }
        if (atan2 == 0.0f && atan22 == 0.0f && asin == 0.0f) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public void mo17336a(TransformWrap bcVar) {
        Matrix3fWrap ajd = this.orientation;
        float f = 1.0f / (((ajd.m00 * ajd.m00) + (ajd.m10 * ajd.m10)) + (ajd.m20 * ajd.m20));
        float f2 = 1.0f / (((ajd.m01 * ajd.m01) + (ajd.m11 * ajd.m11)) + (ajd.m21 * ajd.m21));
        float f3 = 1.0f / (((ajd.m02 * ajd.m02) + (ajd.m12 * ajd.m12)) + (ajd.m22 * ajd.m22));
        Matrix3fWrap ajd2 = bcVar.orientation;
        ajd2.m00 = ajd.m00 * f;
        ajd2.m01 = ajd.m10 * f2;
        ajd2.m02 = ajd.m20 * f3;
        ajd2.m10 = ajd.m01 * f;
        ajd2.m11 = ajd.m11 * f2;
        ajd2.m12 = ajd.m21 * f3;
        ajd2.m20 = ajd.m02 * f;
        ajd2.m21 = ajd.m12 * f2;
        ajd2.m22 = ajd.m22 * f3;
        double d = this.position.x * ((double) f);
        double d2 = this.position.y * ((double) f2);
        double d3 = ((double) f3) * this.position.z;
        bcVar.position.x = -((((double) ajd.m00) * d) + (((double) ajd.m10) * d2) + (((double) ajd.m20) * d3));
        bcVar.position.y = -((((double) ajd.m01) * d) + (((double) ajd.m11) * d2) + (((double) ajd.m21) * d3));
        bcVar.position.z = -((d3 * ((double) ajd.m22)) + (d * ((double) ajd.m02)) + (d2 * ((double) ajd.m12)));
    }

    public float determinant3x3() {
        Matrix3fWrap ajd = this.orientation;
        return (((ajd.m10 * ajd.m21) - (ajd.m20 * ajd.m11)) * ajd.m02) + (ajd.m00 * ((ajd.m11 * ajd.m22) - (ajd.m12 * ajd.m21))) + (ajd.m01 * ((ajd.m12 * ajd.m20) - (ajd.m10 * ajd.m22)));
    }

    public boolean isValidRotation() {
        float abs = Math.abs(1.0f - determinant3x3());
        if (((double) abs) < 1.0E-6d || abs != abs) {
            return false;
        }
        return true;
    }

    public String toString() {
        return String.format(" %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n", new Object[]{Float.valueOf(this.orientation.m00), Float.valueOf(this.orientation.m01), Float.valueOf(this.orientation.m02), Double.valueOf(this.position.x), Float.valueOf(this.orientation.m10), Float.valueOf(this.orientation.m11), Float.valueOf(this.orientation.m12), Double.valueOf(this.position.y), Float.valueOf(this.orientation.m20), Float.valueOf(this.orientation.m21), Float.valueOf(this.orientation.m22), Double.valueOf(this.position.z), Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(0.0f), Float.valueOf(1.0f)});
    }

    /* renamed from: b */
    public void mo17344b(TransformWrap bcVar) {
        this.orientation.set(bcVar.orientation);
        this.position.mo9484aA(bcVar.position);
    }

    public void set(Matrix4fWrap ajk) {
        this.orientation.m00 = ajk.m00;
        this.orientation.m10 = ajk.m10;
        this.orientation.m20 = ajk.m20;
        this.orientation.m01 = ajk.m01;
        this.orientation.m11 = ajk.m11;
        this.orientation.m21 = ajk.m21;
        this.orientation.m02 = ajk.m02;
        this.orientation.m12 = ajk.m12;
        this.orientation.m22 = ajk.m22;
        this.position.x = (double) ajk.m03;
        this.position.y = (double) ajk.m13;
        this.position.z = (double) ajk.m23;
    }

    public void set3x3(Matrix4fWrap ajk) {
        this.orientation.m00 = ajk.m00;
        this.orientation.m10 = ajk.m10;
        this.orientation.m20 = ajk.m20;
        this.orientation.m01 = ajk.m01;
        this.orientation.m11 = ajk.m11;
        this.orientation.m21 = ajk.m21;
        this.orientation.m02 = ajk.m02;
        this.orientation.m12 = ajk.m12;
        this.orientation.m22 = ajk.m22;
    }

    /* renamed from: a */
    public TransformWrap mo17327a(Matrix4fWrap ajk) {
        set3x3(ajk);
        return this;
    }

    /* renamed from: c */
    public TransformWrap mo17346c(TransformWrap bcVar) {
        mo17344b(bcVar);
        return this;
    }

    public void multiply3x4(Vec3d ajr, Vec3d ajr2) {
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        ajr2.x = (((double) this.orientation.m00) * d) + (((double) this.orientation.m01) * d2) + (((double) this.orientation.m02) * d3) + this.position.x;
        ajr2.y = (((double) this.orientation.m10) * d) + (((double) this.orientation.m11) * d2) + (((double) this.orientation.m12) * d3) + this.position.y;
        ajr2.z = (d * ((double) this.orientation.m20)) + (d2 * ((double) this.orientation.m21)) + (((double) this.orientation.m22) * d3) + this.position.z;
    }

    public void multiply3x4(Vec3f vec3f, Vec3d ajr) {
        double d = (double) vec3f.x;
        double d2 = (double) vec3f.y;
        double d3 = (double) vec3f.z;
        ajr.x = (((double) this.orientation.m00) * d) + (((double) this.orientation.m01) * d2) + (((double) this.orientation.m02) * d3) + this.position.x;
        ajr.y = (((double) this.orientation.m10) * d) + (((double) this.orientation.m11) * d2) + (((double) this.orientation.m12) * d3) + this.position.y;
        ajr.z = (d * ((double) this.orientation.m20)) + (d2 * ((double) this.orientation.m21)) + (((double) this.orientation.m22) * d3) + this.position.z;
    }

    /* renamed from: a */
    public void mo17330a(Vector4fWrap ajf, Vector4fWrap ajf2) {
        float f = ajf.x;
        float f2 = ajf.y;
        float f3 = ajf.z;
        ajf2.x = (float) (((double) ((this.orientation.m00 * f) + (this.orientation.m01 * f2) + (this.orientation.m02 * f3))) + this.position.x);
        ajf2.y = (float) (((double) ((this.orientation.m10 * f) + (this.orientation.m11 * f2) + (this.orientation.m12 * f3))) + this.position.y);
        ajf2.z = (float) (((double) ((f * this.orientation.m20) + (f2 * this.orientation.m21) + (this.orientation.m22 * f3))) + this.position.z);
        ajf2.w = 1.0f;
    }

    /* renamed from: a */
    public void mo17329a(Vector4dWrap ajd, Vector4dWrap ajd2) {
        double d = ajd.x;
        double d2 = ajd.y;
        double d3 = ajd.z;
        ajd2.x = (((double) this.orientation.m00) * d) + (((double) this.orientation.m01) * d2) + (((double) this.orientation.m02) * d3) + this.position.x;
        ajd2.y = (((double) this.orientation.m10) * d) + (((double) this.orientation.m11) * d2) + (((double) this.orientation.m12) * d3) + this.position.y;
        ajd2.z = (d * ((double) this.orientation.m20)) + (d2 * ((double) this.orientation.m21)) + (((double) this.orientation.m22) * d3) + this.position.z;
        ajd2.w = 1.0d;
    }

    public void normalizedMultiply3x4(Vec3f vec3f, Vec3d ajr) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        float sqrt = (float) (1.0d / Math.sqrt((double) (((this.orientation.m00 * this.orientation.m00) + (this.orientation.m01 * this.orientation.m01)) + (this.orientation.m02 * this.orientation.m02))));
        float sqrt2 = (float) (1.0d / Math.sqrt((double) (((this.orientation.m10 * this.orientation.m10) + (this.orientation.m11 * this.orientation.m11)) + (this.orientation.m02 * this.orientation.m12))));
        float sqrt3 = (float) (1.0d / Math.sqrt((double) (((this.orientation.m20 * this.orientation.m20) + (this.orientation.m21 * this.orientation.m21)) + (this.orientation.m22 * this.orientation.m22))));
        ajr.x = (double) ((this.orientation.m00 * f * sqrt) + (this.orientation.m01 * f2 * sqrt2) + (this.orientation.m02 * f3 * sqrt3));
        ajr.y = (double) ((this.orientation.m10 * f * sqrt) + (this.orientation.m11 * f2 * sqrt2) + (this.orientation.m12 * f3 * sqrt3));
        ajr.z = (double) ((f * this.orientation.m20 * sqrt) + (f2 * this.orientation.m12 * sqrt2) + (this.orientation.m22 * f3 * sqrt3));
    }

    public void normalizedMultiply3x4(Vec3f vec3f, Vec3f vec3f2) {
        float f = vec3f.x;
        float f2 = vec3f.y;
        float f3 = vec3f.z;
        float sqrt = (float) (1.0d / Math.sqrt((double) (((this.orientation.m00 * this.orientation.m00) + (this.orientation.m01 * this.orientation.m01)) + (this.orientation.m02 * this.orientation.m02))));
        float sqrt2 = (float) (1.0d / Math.sqrt((double) (((this.orientation.m10 * this.orientation.m10) + (this.orientation.m11 * this.orientation.m11)) + (this.orientation.m02 * this.orientation.m12))));
        float sqrt3 = (float) (1.0d / Math.sqrt((double) (((this.orientation.m20 * this.orientation.m20) + (this.orientation.m21 * this.orientation.m21)) + (this.orientation.m22 * this.orientation.m22))));
        vec3f2.x = (this.orientation.m00 * f * sqrt) + (this.orientation.m01 * f2 * sqrt2) + (this.orientation.m02 * f3 * sqrt3);
        vec3f2.y = (this.orientation.m10 * f * sqrt) + (this.orientation.m11 * f2 * sqrt2) + (this.orientation.m12 * f3 * sqrt3);
        vec3f2.z = (f * this.orientation.m20 * sqrt) + (f2 * this.orientation.m12 * sqrt2) + (this.orientation.m22 * f3 * sqrt3);
    }

    /* renamed from: y */
    public void mo17391y(float f) {
        this.orientation.rotateX(0.017453292f * f);
    }

    /* renamed from: z */
    public void mo17392z(float f) {
        this.orientation.rotateY(0.017453292f * f);
    }

    /* renamed from: A */
    public void mo17323A(float f) {
        this.orientation.rotateZ(0.017453292f * f);
    }

    public void setIdentity() {
        this.orientation.setIdentity();
        this.position.set(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
    }

    public void asColumnMajorBuffer(FloatBuffer floatBuffer) {
        floatBuffer.put(this.orientation.m00);
        floatBuffer.put(this.orientation.m10);
        floatBuffer.put(this.orientation.m20);
        floatBuffer.put(0.0f);
        floatBuffer.put(this.orientation.m01);
        floatBuffer.put(this.orientation.m11);
        floatBuffer.put(this.orientation.m21);
        floatBuffer.put(0.0f);
        floatBuffer.put(this.orientation.m02);
        floatBuffer.put(this.orientation.m12);
        floatBuffer.put(this.orientation.m22);
        floatBuffer.put(0.0f);
        floatBuffer.put((float) this.position.x);
        floatBuffer.put((float) this.position.y);
        floatBuffer.put((float) this.position.z);
        floatBuffer.put(1.0f);
    }

    public void setOrientation(Quat4fWrap aoy) {
        float f = aoy.w;
        float f2 = aoy.x;
        float f3 = aoy.y;
        float f4 = aoy.z;
        double d = (double) ((f * f) + (f2 * f2) + (f3 * f3) + (f4 * f4));
        if (!isZero(1.0f - ((float) d))) {
            double sqrt = 1.0d / Math.sqrt(d);
            f = (float) (((double) f) * sqrt);
            f2 = (float) (((double) f2) * sqrt);
            f3 = (float) (((double) f3) * sqrt);
            f4 = (float) (sqrt * ((double) f4));
        }
        float f5 = f2 * f2;
        float f6 = f2 * f3;
        float f7 = f2 * f4;
        float f8 = f2 * f;
        float f9 = f3 * f3;
        float f10 = f3 * f4;
        float f11 = f3 * f;
        float f12 = f4 * f4;
        float f13 = f4 * f;
        this.orientation.m00 = 1.0f - ((f9 + f12) * 2.0f);
        this.orientation.m01 = (f6 - f13) * 2.0f;
        this.orientation.m02 = (f7 + f11) * 2.0f;
        this.orientation.m10 = (f13 + f6) * 2.0f;
        this.orientation.m11 = 1.0f - ((f5 + f12) * 2.0f);
        this.orientation.m12 = (f10 - f8) * 2.0f;
        this.orientation.m20 = (f7 - f11) * 2.0f;
        this.orientation.m21 = (f10 + f8) * 2.0f;
        this.orientation.m22 = 1.0f - ((f5 + f9) * 2.0f);
    }

    /* renamed from: a */
    public void mo17331a(Vec3d ajr) {
        this.orientation.transform(ajr);
        ajr.add(this.position);
    }

    public void inverse() {
        this.orientation.transpose();
        this.position.scale(-1.0d);
        this.orientation.transform(this.position);
    }

    /* renamed from: d */
    public void mo17348d(TransformWrap bcVar) {
        mo17344b(bcVar);
        inverse();
    }

    /* renamed from: e */
    public void mo17350e(TransformWrap bcVar) {
        m27925a(this, bcVar, this);
    }

    /* renamed from: a */
    public void mo17337a(TransformWrap bcVar, TransformWrap bcVar2) {
        m27925a(bcVar, bcVar2, this);
    }

    /* renamed from: c */
    public Quat4f mo17347c(Quat4f quat4f) {
        quat4f.set(this.orientation);
        return quat4f;
    }

    /* renamed from: a */
    public void mo17335a(Quat4fWrap aoy) {
        setOrientation(aoy);
    }

    public Quat4fWrap toQuaternion() {
        return new Quat4fWrap(this.orientation);
    }

    public Vec3f getVectorX() {
        Vec3f vec3f = new Vec3f();
        getX(vec3f);
        return vec3f;
    }

    public Vec3f getVectorY() {
        Vec3f vec3f = new Vec3f();
        getY(vec3f);
        return vec3f;
    }

    public Vec3f getVectorZ() {
        Vec3f vec3f = new Vec3f();
        getZ(vec3f);
        return vec3f;
    }

    /* renamed from: f */
    public double mo17351f(TransformWrap bcVar) {
        return Math.sqrt(mo17352g(bcVar));
    }

    /* renamed from: g */
    public double mo17352g(TransformWrap bcVar) {
        double d = this.position.x - bcVar.position.x;
        double d2 = this.position.y - bcVar.position.y;
        double d3 = this.position.z - bcVar.position.z;
        return (d * d) + (d2 * d2) + (d3 * d3);
    }

    /* renamed from: a */
    public void mo17334a(Matrix3fWrap ajd) {
        this.orientation.set(ajd);
    }

    /* renamed from: gk */
    public double mo17361gk() {
        return Math.sqrt((double) ((this.orientation.m00 * this.orientation.m00) + (this.orientation.m10 * this.orientation.m10) + (this.orientation.m20 * this.orientation.m20)));
    }

    /* renamed from: gm */
    public double mo17362gm() {
        return Math.sqrt((double) ((this.orientation.m01 * this.orientation.m01) + (this.orientation.m11 * this.orientation.m11) + (this.orientation.m21 * this.orientation.m21)));
    }

    /* renamed from: gn */
    public double mo17363gn() {
        return Math.sqrt((double) ((this.orientation.m02 * this.orientation.m02) + (this.orientation.m12 * this.orientation.m12) + (this.orientation.m22 * this.orientation.m22)));
    }

    /* renamed from: B */
    public void mo17324B(float f) {
        this.orientation.m00 *= f;
        this.orientation.m10 *= f;
        this.orientation.m20 *= f;
    }

    /* renamed from: C */
    public void mo17325C(float f) {
        this.orientation.m01 *= f;
        this.orientation.m11 *= f;
        this.orientation.m21 *= f;
    }

    /* renamed from: D */
    public void mo17326D(float f) {
        this.orientation.m02 *= f;
        this.orientation.m12 *= f;
        this.orientation.m22 *= f;
    }

    /* renamed from: b */
    public void mo17343b(Vec3d ajr, Vec3d ajr2) {
        ajr2.x = (ajr.x * ((double) this.orientation.m00)) + (ajr.y * ((double) this.orientation.m01)) + (ajr.z * ((double) this.orientation.m02)) + this.position.x;
        ajr2.y = (ajr.x * ((double) this.orientation.m10)) + (ajr.y * ((double) this.orientation.m11)) + (ajr.z * ((double) this.orientation.m12)) + this.position.y;
        ajr2.z = (ajr.x * ((double) this.orientation.m20)) + (ajr.y * ((double) this.orientation.m21)) + (ajr.z * ((double) this.orientation.m22)) + this.position.z;
    }

    /* renamed from: b */
    public void mo17342b(Vector4fWrap ajf, Vector4fWrap ajf2) {
        TransformWrap bcVar = new TransformWrap();
        mo17336a(bcVar);
        bcVar.mo17330a(ajf, ajf2);
    }

    /* renamed from: b */
    public void mo17341b(Vector4dWrap ajd, Vector4dWrap ajd2) {
        TransformWrap bcVar = new TransformWrap();
        mo17336a(bcVar);
        bcVar.mo17329a(ajd, ajd2);
    }

    /* renamed from: a */
    public void mo17328a(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        this.orientation.m00 = f;
        this.orientation.m10 = f2;
        this.orientation.m20 = f3;
        this.orientation.m01 = f4;
        this.orientation.m11 = f5;
        this.orientation.m21 = f6;
        this.orientation.m02 = f7;
        this.orientation.m12 = f8;
        this.orientation.m22 = f9;
    }

    public void readExternal(IReadExternal vm) {
        this.orientation = (Matrix3fWrap) vm.mo6367he("orientation");
        this.position = (Vec3d) vm.mo6367he("position");
    }

    public void writeExternal(IWriteExternal att) {
        att.mo16261a("orientation", (IExternalIO) this.orientation);
        att.mo16261a("position", (IExternalIO) this.position);
    }
}
