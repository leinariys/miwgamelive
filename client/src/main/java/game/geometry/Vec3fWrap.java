package game.geometry;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.IW */
/* compiled from: a */
public final class Vec3fWrap implements IGeometryF {


    /* renamed from: x */
    public final float x;

    /* renamed from: y */
    public final float y;

    /* renamed from: z */
    public final float z;

    public Vec3fWrap(Vec3fWrap iw) {
        this.x = iw.x;
        this.y = iw.y;
        this.z = iw.z;
    }

    public Vec3fWrap(float f, float f2, float f3) {
        this.x = f;
        this.y = f2;
        this.z = f3;
    }

    public Vec3fWrap(Vec3f vec3f) {
        this.x = vec3f.x;
        this.y = vec3f.y;
        this.z = vec3f.z;
    }

    /* renamed from: a */
    public static float m5407a(Vec3fWrap iw, Vec3fWrap iw2) {
        return (float) Math.pow((double) (((iw.x - iw2.x) * (iw.x - iw2.x)) + ((iw.y - iw2.y) * (iw.y - iw2.y)) + ((iw.z - iw2.z) * (iw.z - iw2.z))), 0.5d);
    }

    /* renamed from: b */
    public static float m5409b(Vec3fWrap iw, Vec3fWrap iw2) {
        return ((iw.x - iw2.x) * (iw.x - iw2.x)) + ((iw.y - iw2.y) * (iw.y - iw2.y)) + ((iw.z - iw2.z) * (iw.z - iw2.z));
    }

    /* renamed from: a */
    public static Vec3fWrap m5408a(Vec3fWrap iw, Vec3fWrap iw2, Vec3fWrap iw3) {
        Vec3fWrap d = iw3.mo2847d(iw);
        Vec3fWrap aXi = iw2.mo2847d(iw).aXi();
        float length = iw.mo2847d(iw2).length();
        float a = aXi.dot(d);
        if (a <= 0.0f) {
            return iw;
        }
        if (a >= length) {
            return iw2;
        }
        return iw.mo2846c(aXi.mo2849fH(a));
    }

    /* renamed from: fH */
    public Vec3fWrap mo2849fH(float f) {
        return new Vec3fWrap(this.x * f, this.y * f, this.z * f);
    }

    /* renamed from: b */
    public Vec3fWrap mo2845b(Vec3fWrap iw) {
        return new Vec3fWrap(this.x * iw.x, this.y * iw.y, this.z * iw.z);
    }

    /* renamed from: c */
    public Vec3fWrap mo2846c(Vec3fWrap iw) {
        return new Vec3fWrap(this.x + iw.x, this.y + iw.y, this.z + iw.z);
    }

    /* renamed from: d */
    public Vec3fWrap mo2847d(Vec3fWrap iw) {
        return new Vec3fWrap(this.x - iw.x, this.y - iw.y, this.z - iw.z);
    }

    public float lengthSquared() {
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
    }

    /* renamed from: e */
    public Vec3fWrap mo2848e(Vec3fWrap iw) {
        return new Vec3fWrap((this.y * iw.z) - (this.z * iw.y), (this.z * iw.x) - (this.x * iw.z), (this.x * iw.y) - (this.y * iw.x));
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public Vec3fWrap aXi() {
        float f = (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
        if (f <= 0.0f || f == 1.0f) {
            return this;
        }
        float sqrt = (float) Math.sqrt((double) f);
        return new Vec3fWrap(this.x / sqrt, this.y / sqrt, this.z / sqrt);
    }

    public float length() {
        return (float) Math.sqrt((double) ((this.x * this.x) + (this.y * this.y) + (this.z * this.z)));
    }

    /* renamed from: a */
    public float dot(Vec3fWrap iw) {
        return (this.x * iw.x) + (this.y * iw.y) + (this.z * iw.z);
    }

    /* renamed from: a */
    public float mo2840a(IGeometryF teVar) {
        return (this.x * teVar.getX()) + (this.y * teVar.getY()) + (this.z * teVar.getZ());
    }

    /* renamed from: T */
    public float mo2838T(Vec3f vec3f) {
        return (this.x * vec3f.x) + (this.y * vec3f.y) + (this.z * vec3f.z);
    }

    public float get(int i) {
        switch (i) {
            case 0:
                return this.x;
            case 1:
                return this.y;
            case 2:
                return this.z;
            default:
                throw new IllegalArgumentException();
        }
    }

    public float getX() {
        return this.x;
    }

    public float getY() {
        return this.y;
    }

    public float getZ() {
        return this.z;
    }
}
