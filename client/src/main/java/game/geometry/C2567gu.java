package game.geometry;

import com.hoplon.geometry.Vec3f;

import javax.vecmath.Vector3f;

/* renamed from: a.gu */
/* compiled from: a */
public class C2567gu {

    /* renamed from: Qn */
    private final Vec3f f7788Qn;

    /* renamed from: Qo */
    private final Vec3f f7789Qo;

    /* renamed from: Qp */
    private final Vec3f f7790Qp;

    /* renamed from: Qq */
    private float f7791Qq = Float.POSITIVE_INFINITY;

    /* renamed from: Qr */
    private C5712aTw f7792Qr = new C5712aTw();
    private boolean intersected = false;

    public C2567gu(Vec3f vec3f, Vec3f vec3f2) {
        this.f7788Qn = new Vec3f((Vector3f) vec3f);
        this.f7789Qo = new Vec3f((Vector3f) vec3f2);
        this.f7790Qp = vec3f2.mo23506j(vec3f);
    }

    public void set(C2567gu guVar) {
        setIntersected(guVar.intersected);
        mo19134ac(guVar.f7791Qq);
        mo19133a(guVar.f7792Qr);
    }

    public void set(aDM adm) {
        setIntersected(adm.isIntersected());
        mo19134ac((float) adm.getParamIntersection());
        this.f7792Qr.mo11566a(adm.getPlane());
    }

    public final boolean isIntersected() {
        return this.intersected;
    }

    public final void setIntersected(boolean z) {
        this.intersected = z;
    }

    /* renamed from: vM */
    public final float mo19142vM() {
        return this.f7791Qq;
    }

    /* renamed from: ac */
    public final void mo19134ac(float f) {
        this.f7791Qq = f;
    }

    /* renamed from: vN */
    public final C5712aTw mo19143vN() {
        return this.f7792Qr;
    }

    /* renamed from: a */
    public final void mo19133a(C5712aTw atw) {
        this.f7792Qr.mo11568b(atw);
    }

    /* renamed from: b */
    public void mo19135b(float f, float f2, float f3) {
        this.f7792Qr.mo11567b(f, f2, f3);
    }

    /* renamed from: l */
    public void mo19138l(Vec3f vec3f) {
        this.f7792Qr.mo11572l(vec3f);
    }

    /* renamed from: vO */
    public Vec3f mo19144vO() {
        return this.f7792Qr.mo11576vO();
    }

    /* renamed from: vP */
    public final Vec3f mo19145vP() {
        return this.f7788Qn;
    }

    /* renamed from: vQ */
    public final Vec3f mo19146vQ() {
        return this.f7789Qo;
    }

    /* renamed from: vR */
    public final Vec3f mo19147vR() {
        return this.f7790Qp;
    }

    /* renamed from: c */
    public void mo19136c(Vec3f vec3f, Vec3f vec3f2) {
        this.f7788Qn.set(vec3f);
        this.f7789Qo.set(vec3f2);
        this.f7790Qp.set(vec3f2.mo23506j(vec3f));
    }
}
