package game.geometry;

/* renamed from: a.ti */
/* compiled from: a */
public interface IGeometryD {
    /* renamed from: a */
    double dot(Vec3dWrap iv);

    double getX();

    double getY();

    double getZ();

    double get(int i);

    double length();

    double lengthSquared();
}
