package game.geometry;

import com.hoplon.geometry.Vec3f;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import util.Maths;

import javax.vecmath.*;
import java.io.IOException;
import java.nio.FloatBuffer;

/* renamed from: a.ajK  reason: case insensitive filesystem */
/* compiled from: a */
public class Matrix4fWrap extends Matrix4f implements IExternalIO {

    public Matrix4fWrap() {
        setIdentity();
    }

    public Matrix4fWrap(float[] fArr) {
        super(fArr[0], fArr[4], fArr[8], fArr[12], fArr[1], fArr[5], fArr[9], fArr[13], fArr[2], fArr[6], fArr[10], fArr[14], fArr[3], fArr[7], fArr[11], fArr[15]);
    }

    public Matrix4fWrap(Matrix4d matrix4d) {
        super(matrix4d);
    }

    public Matrix4fWrap(Matrix4fWrap ajk) {
        super(ajk);
    }

    public Matrix4fWrap(Quat4f quat4f, Vector3f vector3f, float f) {
        super(quat4f, vector3f, f);
    }

    public Matrix4fWrap(Matrix3f matrix3f, Vector3f vector3f, float f) {
        super(matrix3f, vector3f, f);
    }

    public Matrix4fWrap(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16) {
        super(f, f5, f9, f13, f2, f6, f10, f14, f3, f7, f11, f15, f4, f8, f12, f16);
    }

    /* renamed from: b */
    public static Matrix4fWrap m22814b(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        Matrix4fWrap ajk3 = new Matrix4fWrap();
        m22815b(ajk, ajk2, ajk3);
        return ajk3;
    }

    /* renamed from: b */
    private static Matrix4fWrap m22815b(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3) {
        float f = (ajk.m00 * ajk2.m00) + (ajk.m01 * ajk2.m10) + (ajk.m02 * ajk2.m20);
        float f2 = (ajk.m10 * ajk2.m00) + (ajk.m11 * ajk2.m10) + (ajk.m12 * ajk2.m20);
        float f3 = (ajk.m20 * ajk2.m00) + (ajk.m21 * ajk2.m10) + (ajk.m22 * ajk2.m20);
        float f4 = (ajk.m00 * ajk2.m01) + (ajk.m01 * ajk2.m11) + (ajk.m02 * ajk2.m21);
        float f5 = (ajk.m10 * ajk2.m01) + (ajk.m11 * ajk2.m11) + (ajk.m12 * ajk2.m21);
        float f6 = (ajk.m20 * ajk2.m01) + (ajk.m21 * ajk2.m11) + (ajk.m22 * ajk2.m21);
        float f7 = (ajk.m00 * ajk2.m02) + (ajk.m01 * ajk2.m12) + (ajk.m02 * ajk2.m22);
        float f8 = (ajk.m10 * ajk2.m02) + (ajk.m11 * ajk2.m12) + (ajk.m12 * ajk2.m22);
        float f9 = (ajk.m20 * ajk2.m02) + (ajk.m21 * ajk2.m12) + (ajk.m22 * ajk2.m22);
        float f10 = (ajk.m00 * ajk2.m03) + (ajk.m01 * ajk2.m13) + (ajk.m02 * ajk2.m23) + ajk.m03;
        float f11 = (ajk.m10 * ajk2.m03) + (ajk.m11 * ajk2.m13) + (ajk.m12 * ajk2.m23) + ajk.m13;
        ajk3.m00 = f;
        ajk3.m10 = f2;
        ajk3.m20 = f3;
        ajk3.m01 = f4;
        ajk3.m11 = f5;
        ajk3.m21 = f6;
        ajk3.m02 = f7;
        ajk3.m12 = f8;
        ajk3.m22 = f9;
        ajk3.m03 = f10;
        ajk3.m13 = f11;
        ajk3.m23 = (ajk.m20 * ajk2.m03) + (ajk.m21 * ajk2.m13) + (ajk.m22 * ajk2.m23) + ajk.m23;
        ajk3.m30 = 0.0f;
        ajk3.m31 = 0.0f;
        ajk3.m32 = 0.0f;
        ajk3.m33 = 1.0f;
        return ajk3;
    }

    /* renamed from: c */
    public static Matrix4fWrap m22816c(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3) {
        float f = (ajk.m00 * ajk2.m00) + (ajk.m01 * ajk2.m10) + (ajk.m02 * ajk2.m20) + (ajk.m03 * ajk2.m30);
        float f2 = (ajk.m10 * ajk2.m00) + (ajk.m11 * ajk2.m10) + (ajk.m12 * ajk2.m20) + (ajk.m13 * ajk2.m30);
        float f3 = (ajk.m20 * ajk2.m00) + (ajk.m21 * ajk2.m10) + (ajk.m22 * ajk2.m20) + (ajk.m23 * ajk2.m30);
        float f4 = (ajk.m30 * ajk2.m00) + (ajk.m31 * ajk2.m10) + (ajk.m32 * ajk2.m20) + (ajk.m33 * ajk2.m30);
        float f5 = (ajk.m00 * ajk2.m01) + (ajk.m01 * ajk2.m11) + (ajk.m02 * ajk2.m21) + (ajk.m03 * ajk2.m31);
        float f6 = (ajk.m10 * ajk2.m01) + (ajk.m11 * ajk2.m11) + (ajk.m12 * ajk2.m21) + (ajk.m13 * ajk2.m31);
        float f7 = (ajk.m20 * ajk2.m01) + (ajk.m21 * ajk2.m11) + (ajk.m22 * ajk2.m21) + (ajk.m23 * ajk2.m31);
        float f8 = (ajk.m30 * ajk2.m01) + (ajk.m31 * ajk2.m11) + (ajk.m32 * ajk2.m21) + (ajk.m33 * ajk2.m31);
        float f9 = (ajk.m00 * ajk2.m02) + (ajk.m01 * ajk2.m12) + (ajk.m02 * ajk2.m22) + (ajk.m03 * ajk2.m32);
        float f10 = (ajk.m10 * ajk2.m02) + (ajk.m11 * ajk2.m12) + (ajk.m12 * ajk2.m22) + (ajk.m13 * ajk2.m32);
        float f11 = (ajk.m20 * ajk2.m02) + (ajk.m21 * ajk2.m12) + (ajk.m22 * ajk2.m22) + (ajk.m23 * ajk2.m32);
        float f12 = (ajk.m30 * ajk2.m02) + (ajk.m31 * ajk2.m12) + (ajk.m32 * ajk2.m22) + (ajk.m33 * ajk2.m32);
        float f13 = (ajk.m00 * ajk2.m03) + (ajk.m01 * ajk2.m13) + (ajk.m02 * ajk2.m23) + (ajk.m03 * ajk2.m33);
        float f14 = (ajk.m10 * ajk2.m03) + (ajk.m11 * ajk2.m13) + (ajk.m12 * ajk2.m23) + (ajk.m13 * ajk2.m33);
        float f15 = (ajk.m20 * ajk2.m03) + (ajk.m21 * ajk2.m13) + (ajk.m22 * ajk2.m23) + (ajk.m23 * ajk2.m33);
        ajk3.m00 = f;
        ajk3.m10 = f2;
        ajk3.m20 = f3;
        ajk3.m30 = f4;
        ajk3.m01 = f5;
        ajk3.m11 = f6;
        ajk3.m21 = f7;
        ajk3.m31 = f8;
        ajk3.m02 = f9;
        ajk3.m12 = f10;
        ajk3.m22 = f11;
        ajk3.m32 = f12;
        ajk3.m03 = f13;
        ajk3.m13 = f14;
        ajk3.m23 = f15;
        ajk3.m33 = (ajk.m30 * ajk2.m03) + (ajk.m31 * ajk2.m13) + (ajk.m32 * ajk2.m23) + (ajk.m33 * ajk2.m33);
        return ajk3;
    }

    /* renamed from: d */
    public static void m22817d(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3) {
        ajk3.m00 = (ajk.m00 * ajk2.m00) + (ajk.m01 * ajk2.m10) + (ajk.m02 * ajk2.m20);
        ajk3.m10 = (ajk.m10 * ajk2.m00) + (ajk.m11 * ajk2.m10) + (ajk.m12 * ajk2.m20);
        ajk3.m20 = (ajk.m20 * ajk2.m00) + (ajk.m21 * ajk2.m10) + (ajk.m22 * ajk2.m20);
        ajk3.m01 = (ajk.m00 * ajk2.m01) + (ajk.m01 * ajk2.m11) + (ajk.m02 * ajk2.m21);
        ajk3.m11 = (ajk.m10 * ajk2.m01) + (ajk.m11 * ajk2.m11) + (ajk.m12 * ajk2.m21);
        ajk3.m21 = (ajk.m20 * ajk2.m01) + (ajk.m21 * ajk2.m11) + (ajk.m22 * ajk2.m21);
        ajk3.m02 = (ajk.m00 * ajk2.m02) + (ajk.m01 * ajk2.m12) + (ajk.m02 * ajk2.m22);
        ajk3.m12 = (ajk.m10 * ajk2.m02) + (ajk.m11 * ajk2.m12) + (ajk.m12 * ajk2.m22);
        ajk3.m22 = (ajk.m20 * ajk2.m02) + (ajk.m21 * ajk2.m12) + (ajk.m22 * ajk2.m22);
        ajk3.m03 = (ajk.m00 * ajk2.m03) + (ajk.m01 * ajk2.m13) + (ajk.m02 * ajk2.m23) + ajk.m03;
        ajk3.m13 = (ajk.m10 * ajk2.m03) + (ajk.m11 * ajk2.m13) + (ajk.m12 * ajk2.m23) + ajk.m13;
        ajk3.m23 = (ajk.m20 * ajk2.m03) + (ajk.m21 * ajk2.m13) + (ajk.m22 * ajk2.m23) + ajk.m23;
    }

    /* renamed from: g */
    public static Matrix4fWrap m22820g(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        Matrix4fWrap ajk3 = new Matrix4fWrap();
        m22818e(ajk, ajk2, ajk3);
        return ajk3;
    }

    /* renamed from: e */
    private static void m22818e(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3) {
        float f = (ajk.m00 * ajk2.m00) + (ajk.m01 * ajk2.m10) + (ajk.m02 * ajk2.m20);
        float f2 = (ajk.m10 * ajk2.m00) + (ajk.m11 * ajk2.m10) + (ajk.m12 * ajk2.m20);
        float f3 = (ajk.m20 * ajk2.m00) + (ajk.m21 * ajk2.m10) + (ajk.m22 * ajk2.m20);
        float f4 = (ajk.m00 * ajk2.m01) + (ajk.m01 * ajk2.m11) + (ajk.m02 * ajk2.m21);
        float f5 = (ajk.m10 * ajk2.m01) + (ajk.m11 * ajk2.m11) + (ajk.m12 * ajk2.m21);
        float f6 = (ajk.m20 * ajk2.m01) + (ajk.m21 * ajk2.m11) + (ajk.m22 * ajk2.m21);
        float f7 = (ajk.m00 * ajk2.m02) + (ajk.m01 * ajk2.m12) + (ajk.m02 * ajk2.m22);
        float f8 = (ajk.m10 * ajk2.m02) + (ajk.m11 * ajk2.m12) + (ajk.m12 * ajk2.m22);
        ajk3.m00 = f;
        ajk3.m10 = f2;
        ajk3.m20 = f3;
        ajk3.m01 = f4;
        ajk3.m11 = f5;
        ajk3.m21 = f6;
        ajk3.m02 = f7;
        ajk3.m12 = f8;
        ajk3.m22 = (ajk.m20 * ajk2.m02) + (ajk.m21 * ajk2.m12) + (ajk.m22 * ajk2.m22);
    }

    /* renamed from: h */
    private static void m22821h(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        float f = ajk.m00;
        float f2 = ajk.m01;
        float f3 = ajk.m02;
        float f4 = ajk.m03;
        float f5 = ajk.m10;
        float f6 = ajk.m11;
        float f7 = ajk.m12;
        float f8 = ajk.m13;
        float f9 = ajk.m20;
        float f10 = ajk.m21;
        float f11 = ajk.m22;
        float f12 = ajk.m23;
        float f13 = 1.0f / (((f * f) + (f5 * f5)) + (f9 * f9));
        float f14 = 1.0f / (((f2 * f2) + (f6 * f6)) + (f10 * f10));
        float f15 = 1.0f / (((f3 * f3) + (f7 * f7)) + (f11 * f11));
        ajk2.m00 = f * f13;
        ajk2.m01 = f5 * f14;
        ajk2.m02 = f9 * f15;
        ajk2.m10 = f2 * f13;
        ajk2.m11 = f6 * f14;
        ajk2.m12 = f10 * f15;
        ajk2.m20 = f3 * f13;
        ajk2.m21 = f7 * f14;
        ajk2.m22 = f11 * f15;
        float f16 = f4 * f13;
        float f17 = f8 * f14;
        float f18 = f12 * f15;
        ajk2.m03 = -((f * f16) + (f5 * f17) + (f18 * f9));
        ajk2.m13 = -((f16 * f2) + (f17 * f6) + (f18 * f10));
        ajk2.m23 = -((f16 * f3) + (f17 * f7) + (f18 * f11));
    }

    /* renamed from: f */
    public static void m22819f(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3) {
        float f = ajk.m00;
        float f2 = ajk.m01;
        float f3 = ajk.m02;
        float f4 = ajk.m03;
        float f5 = ajk.m10;
        float f6 = ajk.m11;
        float f7 = ajk.m12;
        float f8 = ajk.m13;
        float f9 = ajk.m20;
        float f10 = ajk.m21;
        float f11 = ajk.m22;
        float f12 = 1.0f / (((f * f) + (f5 * f5)) + (f9 * f9));
        float f13 = 1.0f / (((f2 * f2) + (f6 * f6)) + (f10 * f10));
        float f14 = 1.0f / (((f3 * f3) + (f7 * f7)) + (f11 * f11));
        float f15 = f * f12;
        float f16 = f5 * f13;
        float f17 = f9 * f14;
        float f18 = f2 * f12;
        float f19 = f6 * f13;
        float f20 = f10 * f14;
        float f21 = f3 * f12;
        float f22 = f7 * f13;
        float f23 = f11 * f14;
        float f24 = f4 * f12;
        float f25 = f8 * f13;
        float f26 = ajk.m23 * f14;
        float f27 = (ajk2.m00 * f15) + (ajk2.m10 * f16) + (ajk2.m20 * f17);
        float f28 = (ajk2.m00 * f18) + (ajk2.m10 * f19) + (ajk2.m20 * f20);
        float f29 = (ajk2.m00 * f21) + (ajk2.m10 * f22) + (ajk2.m20 * f23);
        float f30 = (ajk2.m01 * f15) + (ajk2.m11 * f16) + (ajk2.m21 * f17);
        float f31 = (ajk2.m01 * f18) + (ajk2.m11 * f19) + (ajk2.m21 * f20);
        float f32 = (ajk2.m01 * f21) + (ajk2.m11 * f22) + (ajk2.m21 * f23);
        float f33 = (ajk2.m02 * f15) + (ajk2.m12 * f16) + (ajk2.m22 * f17);
        float f34 = (ajk2.m02 * f18) + (ajk2.m12 * f19) + (ajk2.m22 * f20);
        float f35 = (ajk2.m02 * f21) + (ajk2.m12 * f22) + (ajk2.m22 * f23);
        float f36 = (-((f * f24) + (f5 * f25) + (f26 * f9))) + (ajk2.m03 * f15) + (ajk2.m13 * f16) + (ajk2.m23 * f17);
        float f37 = (-((f2 * f24) + (f25 * f6) + (f26 * f10))) + (ajk2.m03 * f18) + (ajk2.m13 * f19) + (ajk2.m23 * f20);
        ajk3.m00 = f27;
        ajk3.m10 = f28;
        ajk3.m20 = f29;
        ajk3.m01 = f30;
        ajk3.m11 = f31;
        ajk3.m21 = f32;
        ajk3.m02 = f33;
        ajk3.m12 = f34;
        ajk3.m22 = f35;
        ajk3.m03 = f36;
        ajk3.m13 = f37;
        ajk3.m23 = (-((f3 * f24) + (f25 * f7) + (f26 * f11))) + (ajk2.m03 * f21) + (ajk2.m13 * f22) + (ajk2.m23 * f23);
        ajk3.m30 = 0.0f;
        ajk3.m31 = 0.0f;
        ajk3.m32 = 0.0f;
        ajk3.m33 = 1.0f;
    }

    /* renamed from: c */
    public void mo14008c(float[] fArr) {
        this.m00 = fArr[0];
        this.m01 = fArr[1];
        this.m02 = fArr[2];
        this.m03 = fArr[3];
        this.m10 = fArr[4];
        this.m11 = fArr[5];
        this.m12 = fArr[6];
        this.m13 = fArr[7];
        this.m20 = fArr[8];
        this.m21 = fArr[9];
        this.m22 = fArr[10];
        this.m23 = fArr[11];
        this.m30 = fArr[12];
        this.m31 = fArr[13];
        this.m32 = fArr[14];
        this.m33 = fArr[15];
    }

    public float[] ceB() {
        return new float[]{this.m00, this.m01, this.m02, this.m03, this.m10, this.m11, this.m12, this.m13, this.m20, this.m21, this.m22, this.m23, this.m30, this.m31, this.m32, this.m33};
    }

    /* renamed from: a */
    public Vec3d mo13981a(Tuple3d tuple3d) {
        return new Vec3d((tuple3d.x * ((double) this.m00)) + (tuple3d.y * ((double) this.m01)) + (tuple3d.z * ((double) this.m02)) + ((double) this.m03), (tuple3d.x * ((double) this.m10)) + (tuple3d.y * ((double) this.m11)) + (tuple3d.z * ((double) this.m12)) + ((double) this.m13), (tuple3d.x * ((double) this.m20)) + (tuple3d.y * ((double) this.m21)) + (tuple3d.z * ((double) this.m22)) + ((double) this.m23));
    }

    /* renamed from: a */
    public Vec3f mo13982a(Tuple3f tuple3f) {
        Vec3f vec3f = new Vec3f();
        mo13987a(tuple3f, (Tuple3f) vec3f);
        return vec3f;
    }

    /* renamed from: b */
    public void mo13999b(Tuple3d tuple3d) {
        mo13986a(tuple3d, tuple3d);
    }

    /* renamed from: b */
    public void mo14001b(Tuple3f tuple3f) {
        mo13987a(tuple3f, tuple3f);
    }

    /* renamed from: a */
    public void mo13987a(Tuple3f tuple3f, Tuple3f tuple3f2) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        tuple3f2.x = (this.m00 * f) + (this.m01 * f2) + (this.m02 * f3);
        tuple3f2.y = (this.m10 * f) + (this.m11 * f2) + (this.m12 * f3);
        tuple3f2.z = (f * this.m20) + (f2 * this.m21) + (this.m22 * f3);
    }

    /* renamed from: a */
    public void mo13986a(Tuple3d tuple3d, Tuple3d tuple3d2) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        tuple3d2.x = (((double) this.m00) * d) + (((double) this.m01) * d2) + (((double) this.m02) * d3);
        tuple3d2.y = (((double) this.m10) * d) + (((double) this.m11) * d2) + (((double) this.m12) * d3);
        tuple3d2.z = (d * ((double) this.m20)) + (d2 * ((double) this.m21)) + (((double) this.m22) * d3);
    }

    /* renamed from: c */
    public Vec3f mo14004c(Tuple3f tuple3f) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        return new Vec3f((this.m00 * f) + (this.m01 * f2) + (this.m02 * f3) + this.m03, (this.m10 * f) + (this.m11 * f2) + (this.m12 * f3) + this.m13, (f * this.m20) + (f2 * this.m21) + (this.m22 * f3) + this.m23);
    }

    /* renamed from: c */
    public void mo14007c(Tuple3d tuple3d) {
        mo14000b(tuple3d, tuple3d);
    }

    /* renamed from: d */
    public void mo14026d(Tuple3f tuple3f) {
        mo14002b(tuple3f, tuple3f);
    }

    /* renamed from: b */
    public void mo14000b(Tuple3d tuple3d, Tuple3d tuple3d2) {
        double d = tuple3d.x;
        double d2 = tuple3d.y;
        double d3 = tuple3d.z;
        tuple3d2.x = (((double) this.m00) * d) + (((double) this.m01) * d2) + (((double) this.m02) * d3) + ((double) this.m03);
        tuple3d2.y = (((double) this.m10) * d) + (((double) this.m11) * d2) + (((double) this.m12) * d3) + ((double) this.m13);
        tuple3d2.z = (d * ((double) this.m20)) + (d2 * ((double) this.m21)) + (((double) this.m22) * d3) + ((double) this.m23);
    }

    /* renamed from: b */
    public void mo14002b(Tuple3f tuple3f, Tuple3f tuple3f2) {
        float f = tuple3f.x;
        float f2 = tuple3f.y;
        float f3 = tuple3f.z;
        tuple3f2.x = (this.m00 * f) + (this.m01 * f2) + (this.m02 * f3) + this.m03;
        tuple3f2.y = (this.m10 * f) + (this.m11 * f2) + (this.m12 * f3) + this.m13;
        tuple3f2.z = (f * this.m20) + (f2 * this.m21) + (this.m22 * f3) + this.m23;
    }

    public Vec3f ceC() {
        return new Vec3f(this.m00, this.m10, this.m20);
    }

    public Vec3f ceD() {
        return new Vec3f(this.m01, this.m11, this.m21);
    }

    public Vec3f ceE() {
        return new Vec3f(this.m02, this.m12, this.m22);
    }

    public String toString() {
        return String.format(" %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n %4.4f %4.4f %4.4f %4.4f\n", new Object[]{Float.valueOf(this.m00), Float.valueOf(this.m01), Float.valueOf(this.m02), Float.valueOf(this.m03), Float.valueOf(this.m10), Float.valueOf(this.m11), Float.valueOf(this.m12), Float.valueOf(this.m13), Float.valueOf(this.m20), Float.valueOf(this.m21), Float.valueOf(this.m22), Float.valueOf(this.m23), Float.valueOf(this.m30), Float.valueOf(this.m31), Float.valueOf(this.m32), Float.valueOf(this.m33)});
    }

    public String ceG() {
        return String.format(" %4d %4d %4d %4d\n %4d %4d %4d %4d\n %4d %4d %4d %4d\n %4d %4d %4d %4d\n", new Object[]{Integer.valueOf(Float.floatToRawIntBits(this.m00)), Integer.valueOf(Float.floatToRawIntBits(this.m01)), Integer.valueOf(Float.floatToRawIntBits(this.m02)), Integer.valueOf(Float.floatToRawIntBits(this.m03)), Integer.valueOf(Float.floatToRawIntBits(this.m10)), Integer.valueOf(Float.floatToRawIntBits(this.m11)), Integer.valueOf(Float.floatToRawIntBits(this.m12)), Integer.valueOf(Float.floatToRawIntBits(this.m13)), Integer.valueOf(Float.floatToRawIntBits(this.m20)), Integer.valueOf(Float.floatToRawIntBits(this.m21)), Integer.valueOf(Float.floatToRawIntBits(this.m22)), Integer.valueOf(Float.floatToRawIntBits(this.m23)), Integer.valueOf(Float.floatToRawIntBits(this.m30)), Integer.valueOf(Float.floatToRawIntBits(this.m31)), Integer.valueOf(Float.floatToRawIntBits(this.m32)), Integer.valueOf(Float.floatToRawIntBits(this.m33))});
    }

    public void rotateX(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m11 = cos;
        ajk.m12 = -sin;
        ajk.m21 = sin;
        ajk.m22 = cos;
        set(m22814b(this, ajk));
    }

    /* renamed from: jI */
    public void mo14040jI(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m11 = cos;
        ajk.m12 = -sin;
        ajk.m21 = sin;
        ajk.m22 = cos;
        set(m22814b(ajk, this));
    }

    public void rotateY(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m00 = cos;
        ajk.m02 = sin;
        ajk.m20 = -sin;
        ajk.m22 = cos;
        set(m22814b(this, ajk));
    }

    public void rotateZ(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m00 = cos;
        ajk.m01 = -sin;
        ajk.m10 = sin;
        ajk.m11 = cos;
        set(m22814b(this, ajk));
    }

    /* renamed from: jJ */
    public void mo14041jJ(float f) {
        float f2 = 0.017453292f * f;
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.setIdentity();
        float sin = (float) Math.sin((double) f2);
        float cos = (float) Math.cos((double) f2);
        ajk.m00 = cos;
        ajk.m01 = -sin;
        ajk.m10 = sin;
        ajk.m11 = cos;
        set(m22814b(ajk, this));
    }

    /* renamed from: aD */
    public void mo13988aD(Vec3f vec3f) {
        this.m00 = vec3f.x;
        this.m10 = vec3f.y;
        this.m20 = vec3f.z;
    }

    public void setX(float f, float f2, float f3) {
        this.m00 = f;
        this.m10 = f2;
        this.m20 = f3;
    }

    public void setY(float f, float f2, float f3) {
        this.m01 = f;
        this.m11 = f2;
        this.m21 = f3;
    }

    public void setZ(float f, float f2, float f3) {
        this.m02 = f;
        this.m12 = f2;
        this.m22 = f3;
    }

    /* renamed from: o */
    public void mo14048o(float f, float f2, float f3) {
        this.m03 = f;
        this.m13 = f2;
        this.m23 = f3;
    }

    /* renamed from: aE */
    public void mo13989aE(Vec3f vec3f) {
        setColumn(1, vec3f.x, vec3f.y, vec3f.z, 0.0f);
    }

    /* renamed from: aF */
    public void mo13990aF(Vec3f vec3f) {
        setColumn(2, vec3f.x, vec3f.y, vec3f.z, 0.0f);
    }

    public void setTranslation(Vec3f vec3f) {
        setColumn(3, vec3f.x, vec3f.y, vec3f.z, 1.0f);
    }

    /* renamed from: aG */
    public Matrix4fWrap mo13991aG(Vec3f vec3f) {
        setColumn(3, vec3f.x, vec3f.y, vec3f.z, 1.0f);
        return this;
    }

    /* renamed from: p */
    public void mo14049p(float f, float f2, float f3) {
        this.m03 = f;
        this.m13 = f2;
        this.m23 = f3;
    }

    public void setTranslation(Vec3d ajr) {
        this.m03 = (float) ajr.x;
        this.m13 = (float) ajr.y;
        this.m23 = (float) ajr.z;
    }

    public Vec3f ceH() {
        return new Vec3f(this.m03, this.m13, this.m23);
    }

    public void getTranslation(Vec3f vec3f) {
        vec3f.x = this.m03;
        vec3f.y = this.m13;
        vec3f.z = this.m23;
    }

    /* renamed from: c */
    public Matrix4fWrap mo14003c(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        m22815b(ajk, ajk2, this);
        return this;
    }

    /* renamed from: d */
    public void mo14025d(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        m22815b(ajk, ajk2, this);
    }

    /* renamed from: e */
    public Matrix4fWrap mo14028e(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        m22818e(ajk, ajk2, this);
        return this;
    }

    /* renamed from: f */
    public void mo14031f(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        m22818e(ajk, ajk2, this);
    }

    /* renamed from: g */
    public Matrix4fWrap mo14032g(Matrix4fWrap ajk) {
        m22818e(ajk, this, this);
        return this;
    }

    /* renamed from: h */
    public Matrix4fWrap mo14034h(Matrix4fWrap ajk) {
        m22815b(this, ajk, this);
        return this;
    }

    /* renamed from: a */
    public void mo13983a(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16) {
        this.m00 = f;
        this.m01 = f5;
        this.m02 = f9;
        this.m03 = f13;
        this.m10 = f2;
        this.m11 = f6;
        this.m12 = f10;
        this.m13 = f14;
        this.m20 = f3;
        this.m21 = f7;
        this.m22 = f11;
        this.m23 = f15;
        this.m30 = f4;
        this.m31 = f8;
        this.m32 = f12;
        this.m33 = f16;
    }

    /* renamed from: b */
    public Matrix4fWrap mo13997b(float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13, float f14, float f15, float f16) {
        this.m00 = f;
        this.m01 = f2;
        this.m02 = f3;
        this.m03 = f4;
        this.m10 = f5;
        this.m11 = f6;
        this.m12 = f7;
        this.m13 = f8;
        this.m20 = f9;
        this.m21 = f10;
        this.m22 = f11;
        this.m23 = f12;
        this.m30 = f13;
        this.m31 = f14;
        this.m32 = f15;
        this.m33 = f16;
        return this;
    }

    /* renamed from: aH */
    public Vec3f mo13992aH(Vec3f vec3f) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        float f13 = vec3f.x;
        float f14 = vec3f.y;
        float f15 = vec3f.z;
        return new Vec3f((f4 * f13) + (f5 * f14) + (f15 * f6) + (-((this.m03 * this.m00) + (this.m13 * this.m10) + (this.m23 * this.m20))), (f13 * f7) + (f14 * f8) + (f15 * f9) + (-((this.m03 * this.m01) + (this.m13 * this.m11) + (this.m23 * this.m21))), (f10 * f13) + (f11 * f14) + (f15 * f12) + (-((this.m03 * this.m02) + (this.m13 * this.m12) + (this.m23 * this.m22))));
    }

    /* renamed from: aI */
    public Vec3f mo13993aI(Vec3f vec3f) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        float f13 = vec3f.x;
        float f14 = vec3f.y;
        float f15 = vec3f.z;
        return new Vec3f((f4 * f13) + (f5 * f14) + (f15 * f6), (f13 * f7) + (f14 * f8) + (f15 * f9), (f10 * f13) + (f11 * f14) + (f15 * f12));
    }

    public boolean hasRotation() {
        float asin;
        float atan2;
        float atan22;
        float f = -this.m12;
        if (f <= -1.0f) {
            asin = -1.570796f;
        } else if (((double) f) >= 1.0d) {
            asin = 1.570796f;
        } else {
            asin = (float) Math.asin((double) f);
        }
        if (f > 0.9999f) {
            atan2 = (float) Math.atan2((double) (-this.m02), (double) this.m00);
            atan22 = 0.0f;
        } else {
            atan2 = (float) Math.atan2((double) this.m02, (double) this.m22);
            atan22 = (float) Math.atan2((double) this.m10, (double) this.m11);
        }
        if (atan2 == 0.0f && atan22 == 0.0f && asin == 0.0f) {
            return false;
        }
        return true;
    }

    public Matrix4fWrap ceI() {
        Matrix4fWrap ajk = new Matrix4fWrap();
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        ajk.m00 = this.m00 * f;
        ajk.m01 = this.m10 * f2;
        ajk.m02 = this.m20 * f3;
        ajk.m10 = this.m01 * f;
        ajk.m11 = this.m11 * f2;
        ajk.m12 = this.m21 * f3;
        ajk.m20 = this.m02 * f;
        ajk.m21 = this.m12 * f2;
        ajk.m22 = this.m22 * f3;
        float f4 = f * this.m03;
        float f5 = f2 * this.m13;
        float f6 = f3 * this.m23;
        ajk.m03 = -((this.m00 * f4) + (this.m10 * f5) + (this.m20 * f6));
        ajk.m13 = -((this.m01 * f4) + (this.m11 * f5) + (this.m21 * f6));
        ajk.m23 = -((f4 * this.m02) + (f5 * this.m12) + (this.m22 * f6));
        return ajk;
    }

    @Deprecated
    /* renamed from: i */
    public void mo14037i(Matrix4fWrap ajk) {
        m22821h(this, ajk);
    }

    /* renamed from: j */
    public void mo14039j(Matrix4fWrap ajk) {
        m22821h(ajk, this);
    }

    public void ceJ() {
        m22821h(this, this);
    }

    /* renamed from: k */
    public Matrix4fWrap mo14043k(Matrix4fWrap ajk) {
        m22821h(ajk, this);
        return this;
    }

    public Matrix4fWrap ceK() {
        m22821h(this, this);
        return this;
    }

    public void ceL() {
        m22821h(this, this);
    }

    /* renamed from: l */
    public Matrix4fWrap mo14044l(Matrix4fWrap ajk) {
        m22819f(this, ajk, this);
        return this;
    }

    /* renamed from: a */
    public void mo13984a(FloatBuffer floatBuffer) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        floatBuffer.put(this.m00 * f);
        floatBuffer.put(this.m01 * f);
        floatBuffer.put(this.m02 * f);
        floatBuffer.put(0.0f);
        floatBuffer.put(this.m10 * f2);
        floatBuffer.put(this.m11 * f2);
        floatBuffer.put(this.m12 * f2);
        floatBuffer.put(0.0f);
        floatBuffer.put(this.m20 * f3);
        floatBuffer.put(this.m21 * f3);
        floatBuffer.put(this.m22 * f3);
        floatBuffer.put(0.0f);
        float f4 = f * this.m03;
        float f5 = f2 * this.m13;
        float f6 = f3 * this.m23;
        floatBuffer.put(-((this.m00 * f4) + (this.m10 * f5) + (this.m20 * f6)));
        floatBuffer.put(-((this.m01 * f4) + (this.m11 * f5) + (this.m21 * f6)));
        floatBuffer.put(-((f4 * this.m02) + (f5 * this.m12) + (this.m22 * f6)));
        floatBuffer.put(1.0f);
    }

    /* renamed from: b */
    public FloatBuffer mo13998b(FloatBuffer floatBuffer) {
        floatBuffer.clear();
        asColumnMajorBuffer(floatBuffer);
        floatBuffer.flip();
        return floatBuffer;
    }

    public void asColumnMajorBuffer(FloatBuffer floatBuffer) {
        floatBuffer.put(this.m00);
        floatBuffer.put(this.m10);
        floatBuffer.put(this.m20);
        floatBuffer.put(this.m30);
        floatBuffer.put(this.m01);
        floatBuffer.put(this.m11);
        floatBuffer.put(this.m21);
        floatBuffer.put(this.m31);
        floatBuffer.put(this.m02);
        floatBuffer.put(this.m12);
        floatBuffer.put(this.m22);
        floatBuffer.put(this.m32);
        floatBuffer.put(this.m03);
        floatBuffer.put(this.m13);
        floatBuffer.put(this.m23);
        floatBuffer.put(this.m33);
    }

    /* renamed from: aJ */
    public void mo13994aJ(Vec3f vec3f) {
        mo13978B(vec3f, vec3f);
    }

    /* renamed from: V */
    public void mo13980V(Vec3d ajr) {
        mo14029e(ajr, ajr);
    }

    /* renamed from: B */
    public void mo13978B(Vec3f vec3f, Vec3f vec3f2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = this.m02 * f;
        float f11 = this.m12 * f2;
        float f12 = this.m22 * f3;
        float f13 = f * this.m03;
        float f14 = f2 * this.m13;
        float f15 = f3 * this.m23;
        float f16 = vec3f.x;
        float f17 = vec3f.y;
        float f18 = vec3f.z;
        vec3f2.x = (f4 * f16) + (f5 * f17) + (f18 * f6) + (-((this.m00 * f13) + (this.m10 * f14) + (this.m20 * f15)));
        vec3f2.y = (f16 * f7) + (f17 * f8) + (f18 * f9) + (-((this.m01 * f13) + (this.m11 * f14) + (this.m21 * f15)));
        vec3f2.z = (-((f13 * this.m02) + (f14 * this.m12) + (this.m22 * f15))) + (f16 * f10) + (f17 * f11) + (f18 * f12);
    }

    public float determinant3x3() {
        return (this.m00 * ((this.m11 * this.m22) - (this.m12 * this.m21))) + (this.m01 * ((this.m12 * this.m20) - (this.m10 * this.m22))) + (this.m02 * ((this.m10 * this.m21) - (this.m11 * this.m20)));
    }

    public boolean isValidRotation() {
        float abs = Math.abs(1.0f - determinant3x3());
        if (((double) abs) < 1.0E-6d || abs != abs) {
            return false;
        }
        return true;
    }

    public void ceM() {
        set(0.0f);
        this.m00 = 1.0f;
        this.m11 = 1.0f;
        this.m22 = 1.0f;
        this.m33 = 1.0f;
    }

    public Matrix4fWrap ceN() {
        ceM();
        return this;
    }

    /* renamed from: e */
    public void mo14029e(Vec3d ajr, Vec3d ajr2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        ajr2.x = (((double) f6) * d3) + (((double) f4) * d) + (((double) f5) * d2);
        ajr2.y = (((double) f7) * d) + (((double) f8) * d2) + (((double) f9) * d3);
        ajr2.z = (((double) f11) * d2) + (((double) f10) * d) + (((double) (f3 * this.m22)) * d3);
    }

    /* renamed from: C */
    public void mo13979C(Vec3f vec3f, Vec3f vec3f2) {
        float f = 1.0f / (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20));
        float f2 = 1.0f / (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21));
        float f3 = 1.0f / (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22));
        float f4 = this.m00 * f;
        float f5 = this.m10 * f2;
        float f6 = this.m20 * f3;
        float f7 = this.m01 * f;
        float f8 = this.m11 * f2;
        float f9 = this.m21 * f3;
        float f10 = f * this.m02;
        float f11 = f2 * this.m12;
        float f12 = f3 * this.m22;
        float f13 = vec3f.x;
        float f14 = vec3f.y;
        float f15 = vec3f.z;
        vec3f2.x = (f4 * f13) + (f5 * f14) + (f15 * f6);
        vec3f2.y = (f13 * f7) + (f14 * f8) + (f15 * f9);
        vec3f2.z = (f10 * f13) + (f11 * f14) + (f15 * f12);
    }

    /* renamed from: i */
    public Matrix4fWrap mo14036i(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        mo14025d(ajk2, ajk);
        ceL();
        return this;
    }

    public boolean ceO() {
        return mo14042jK(1.0E-6f);
    }

    /* renamed from: jK */
    public boolean mo14042jK(float f) {
        if (Math.abs((this.m00 * this.m01) + (this.m10 * this.m11) + (this.m20 * this.m21)) <= f && Math.abs((this.m00 * this.m02) + (this.m10 * this.m12) + (this.m20 * this.m22)) <= f && Math.abs((this.m01 * this.m02) + (this.m11 * this.m12) + (this.m21 * this.m22)) <= f && Math.abs(1.0f - (((this.m00 * this.m00) + (this.m10 * this.m10)) + (this.m20 * this.m20))) <= f && Math.abs(1.0f - (((this.m01 * this.m01) + (this.m11 * this.m11)) + (this.m21 * this.m21))) <= f && Math.abs(1.0f - (((this.m02 * this.m02) + (this.m12 * this.m12)) + (this.m22 * this.m22))) <= f) {
            return true;
        }
        return false;
    }

    public boolean ceP() {
        float f = this.m00;
        float f2 = this.m10;
        float f3 = this.m20;
        float f4 = this.m01;
        float f5 = this.m11;
        float f6 = this.m21;
        return (((f * f5) - (f2 * f4)) * this.m22) + ((((f2 * f6) - (f3 * f5)) * this.m02) + (((f3 * f4) - (f6 * f)) * this.m12)) > 0.0f;
    }

    /* renamed from: m */
    public Matrix4fWrap mo14045m(Matrix4fWrap ajk) {
        set(ajk);
        return this;
    }

    /* renamed from: a */
    public void mo13985a(Matrix3f matrix3f) {
        this.m00 = matrix3f.m00;
        this.m01 = matrix3f.m01;
        this.m02 = matrix3f.m02;
        this.m10 = matrix3f.m10;
        this.m11 = matrix3f.m11;
        this.m12 = matrix3f.m12;
        this.m20 = matrix3f.m20;
        this.m21 = matrix3f.m21;
        this.m22 = matrix3f.m22;
    }

    /* renamed from: n */
    public Matrix4fWrap mo14046n(Matrix4fWrap ajk) {
        invert(ajk);
        return this;
    }

    public Matrix4fWrap ceQ() {
        invert();
        return this;
    }

    public Matrix4fWrap ceR() {
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.invert(this);
        return ajk;
    }

    /* renamed from: c */
    public void mo14006c(Vector4fWrap ajf, Vector4fWrap ajf2) {
        float f = ajf.x;
        float f2 = ajf.y;
        float f3 = ajf.z;
        float f4 = ajf.w;
        ajf2.x = (this.m00 * f) + (this.m01 * f2) + (this.m02 * f3) + (this.m03 * f4);
        ajf2.y = (this.m10 * f) + (this.m11 * f2) + (this.m12 * f3) + (this.m13 * f4);
        ajf2.z = (this.m20 * f) + (this.m21 * f2) + (this.m22 * f3) + (this.m23 * f4);
        ajf2.w = (f * this.m30) + (f2 * this.m31) + (this.m32 * f3) + (this.m33 * f4);
    }

    /* renamed from: c */
    public void mo14005c(Vector4dWrap ajd, Vector4dWrap ajd2) {
        double d = ajd.x;
        double d2 = ajd.y;
        double d3 = ajd.z;
        double d4 = ajd.w;
        ajd2.x = (((double) this.m00) * d) + (((double) this.m01) * d2) + (((double) this.m02) * d3) + (((double) this.m03) * d4);
        ajd2.y = (((double) this.m10) * d) + (((double) this.m11) * d2) + (((double) this.m12) * d3) + (((double) this.m13) * d4);
        ajd2.z = (((double) this.m20) * d) + (((double) this.m21) * d2) + (((double) this.m22) * d3) + (((double) this.m23) * d4);
        ajd2.w = (d * ((double) this.m30)) + (d2 * ((double) this.m31)) + (((double) this.m32) * d3) + (((double) this.m33) * d4);
    }

    /* renamed from: f */
    public void mo14030f(float f, float f2, float f3, float f4, float f5, float f6) {
        this.m00 = 2.0f / (f - f2);
        this.m10 = 0.0f;
        this.m20 = 0.0f;
        this.m30 = 0.0f;
        this.m01 = 0.0f;
        this.m11 = 2.0f / (f4 - f3);
        this.m21 = 0.0f;
        this.m31 = 0.0f;
        this.m02 = 0.0f;
        this.m12 = 0.0f;
        this.m22 = -2.0f / (f6 - f5);
        this.m32 = 0.0f;
        this.m03 = (-(f + f2)) / (f - f2);
        this.m13 = (-(f4 + f3)) / (f4 - f3);
        this.m23 = (-(f6 + f5)) / (f6 - f5);
        this.m33 = 1.0f;
    }

    /* renamed from: n */
    public void mo14047n(float f, float f2, float f3, float f4) {
        float O = (float) (((double) f3) * Maths.tanPi180((double) (f / 2.0f)));
        float f5 = -O;
        float f6 = f5 * f2;
        float f7 = O * f2;
        mo13983a((2.0f * f3) / (f7 - f6), 0.0f, 0.0f, 0.0f, 0.0f, (2.0f * f3) / (O - f5), 0.0f, 0.0f, (f7 + f6) / (f7 - f6), (O + f5) / (O - f5), -((f4 + f3) / (f4 - f3)), -1.0f, 0.0f, 0.0f, -(((2.0f * f4) * f3) / (f4 - f3)), 0.0f);
    }

    public void readExternal(IReadExternal vm) throws IOException {
        this.m00 = vm.readFloat("m00");
        this.m01 = vm.readFloat("m01");
        this.m02 = vm.readFloat("m02");
        this.m03 = vm.readFloat("m03");
        this.m10 = vm.readFloat("m10");
        this.m11 = vm.readFloat("m11");
        this.m12 = vm.readFloat("m12");
        this.m13 = vm.readFloat("m13");
        this.m20 = vm.readFloat("m20");
        this.m21 = vm.readFloat("m21");
        this.m22 = vm.readFloat("m22");
        this.m23 = vm.readFloat("m23");
        this.m30 = vm.readFloat("m30");
        this.m31 = vm.readFloat("m31");
        this.m32 = vm.readFloat("m32");
        this.m33 = vm.readFloat("m33");
    }

    public void writeExternal(IWriteExternal att) throws IOException {
        att.writeFloat("m00", this.m00);
        att.writeFloat("m01", this.m01);
        att.writeFloat("m02", this.m02);
        att.writeFloat("m03", this.m03);
        att.writeFloat("m10", this.m10);
        att.writeFloat("m11", this.m11);
        att.writeFloat("m12", this.m12);
        att.writeFloat("m13", this.m13);
        att.writeFloat("m20", this.m20);
        att.writeFloat("m21", this.m21);
        att.writeFloat("m22", this.m22);
        att.writeFloat("m23", this.m23);
        att.writeFloat("m30", this.m30);
        att.writeFloat("m31", this.m31);
        att.writeFloat("m32", this.m32);
        att.writeFloat("m33", this.m33);
    }

    public boolean anA() {
        float f = this.m00 + this.m01 + this.m02 + this.m03 + this.m10 + this.m11 + this.m12 + this.m13 + this.m20 + this.m21 + this.m22 + this.m23 + this.m30 + this.m31 + this.m32 + this.m33;
        return !Float.isNaN(f) && !Float.isInfinite(f);
    }
}
