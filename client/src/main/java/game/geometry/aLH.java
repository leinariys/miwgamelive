package game.geometry;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.io.IExternalIO;
import game.io.IReadExternal;
import game.io.IWriteExternal;
import org.mozilla1.javascript.ScriptRuntime;

import javax.vecmath.Tuple3d;
import java.io.Serializable;

/* renamed from: a.aLH */
/* compiled from: a */
public class aLH implements IExternalIO, Serializable {
    static final Vec3d cRA = new Vec3d();
    static final Vec3d cRB = new Vec3d();
    static final Vec3d cRz = new Vec3d();
    static final Vec3d imm = new Vec3d();
    static final Vec3d imn = new Vec3d();
    static final Vec3d imo = new Vec3d();
    static final Vec3d imp = new Vec3d();
    static final Vec3d imq = new Vec3d();

    private Vec3d imk;
    private Vec3d iml;

    public aLH() {
        this.imk = new Vec3d();
        this.iml = new Vec3d();
        this.imk.set(Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY);
        this.iml.set(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    public aLH(Vec3d ajr, Vec3d ajr2) {
        this.imk = new Vec3d();
        this.iml = new Vec3d();
        this.imk.mo9484aA(ajr2);
        this.iml.mo9484aA(ajr);
    }

    public aLH(double d, double d2, double d3, double d4, double d5, double d6) {
        this(new Vec3d(d, d2, d3), new Vec3d(d4, d5, d6));
    }

    public aLH(double[] dArr) {
        this.imk = new Vec3d();
        this.iml = new Vec3d();
        this.iml.set(dArr[0], dArr[1], dArr[2]);
        this.imk.set(dArr[3], dArr[4], dArr[5]);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        aLH alh = (aLH) obj;
        if (!this.imk.equals(alh.imk) || !this.iml.equals(alh.iml)) {
            return false;
        }
        return true;
    }

    /* renamed from: aD */
    public void mo9839aD(Vec3d ajr) {
        this.iml.mo9484aA(ajr);
    }

    public Vec3d dim() {
        return this.iml;
    }

    /* renamed from: aE */
    public void mo9840aE(Vec3d ajr) {
        this.imk.mo9484aA(ajr);
    }

    public Vec3d din() {
        return this.imk;
    }

    public float dio() {
        float f = (float) (this.imk.x - this.iml.x);
        if (((double) f) > 1.0E26d) {
            return 0.0f;
        }
        return Math.max(0.0f, f);
    }

    public float dip() {
        float f = (float) (this.imk.y - this.iml.y);
        if (((double) f) > 1.0E26d) {
            return 0.0f;
        }
        return Math.max(0.0f, f);
    }

    public float diq() {
        float f = (float) (this.imk.z - this.iml.z);
        if (((double) f) > 1.0E26d) {
            return 0.0f;
        }
        return Math.max(0.0f, f);
    }

    public float dir() {
        float dio = dio();
        float dip = dip();
        return (float) Math.sqrt((double) ((dio * dio) + (dip * dip)));
    }

    public float dis() {
        float dip = dip();
        float diq = diq();
        return (float) Math.sqrt((double) ((dip * dip) + (diq * diq)));
    }

    public float clX() {
        float dio = dio();
        float dip = dip();
        float diq = diq();
        return (dio * dio) + (dip * dip) + (diq * diq);
    }

    public float length() {
        return (float) Math.sqrt((double) lengthSquared());
    }

    public float lengthSquared() {
        float dio = dio();
        float dip = dip();
        float diq = diq();
        return (dio * dio) + (dip * dip) + (diq * diq);
    }

    public float dit() {
        return (float) Math.max(this.iml.length(), this.imk.length());
    }

    public float diu() {
        return (float) Math.max(this.iml.lengthSquared(), this.imk.lengthSquared());
    }

    /* renamed from: a */
    public boolean mo9836a(C1085Pt pt) {
        float f = 0.0f;
        if (((double) pt.bny().x) < this.iml.x) {
            float f2 = (float) (((double) pt.bny().x) - this.iml.x);
            f = f2 * f2;
        }
        if (((double) pt.bny().y) < this.iml.y) {
            float f3 = (float) (((double) pt.bny().y) - this.iml.y);
            f += f3 * f3;
        }
        if (((double) pt.bny().z) < this.iml.z) {
            float f4 = (float) (((double) pt.bny().z) - this.iml.z);
            f += f4 * f4;
        }
        if (((double) pt.bny().x) > this.imk.x) {
            float f5 = (float) (((double) pt.bny().x) - this.imk.x);
            f += f5 * f5;
        }
        if (((double) pt.bny().y) > this.imk.y) {
            float f6 = (float) (((double) pt.bny().y) - this.imk.y);
            f += f6 * f6;
        }
        if (((double) pt.bny().z) > this.imk.z) {
            float f7 = (float) (((double) pt.bny().z) - this.imk.z);
            f += f7 * f7;
        }
        return f <= pt.getRadius() * pt.getRadius();
    }

    /* renamed from: a */
    public boolean mo9838a(aVS avs) {
        double d = ScriptRuntime.NaN;
        if (avs.mo11775zO().x < this.iml.x) {
            double d2 = avs.mo11775zO().x - this.iml.x;
            d = d2 * d2;
        }
        if (avs.mo11775zO().y < this.iml.y) {
            double d3 = avs.mo11775zO().y - this.iml.y;
            d += d3 * d3;
        }
        if (avs.mo11775zO().z < this.iml.z) {
            double d4 = avs.mo11775zO().z - this.iml.z;
            d += d4 * d4;
        }
        if (avs.mo11775zO().x > this.imk.x) {
            double d5 = avs.mo11775zO().x - this.imk.x;
            d += d5 * d5;
        }
        if (avs.mo11775zO().y > this.imk.y) {
            double d6 = avs.mo11775zO().y - this.imk.y;
            d += d6 * d6;
        }
        if (avs.mo11775zO().z > this.imk.z) {
            double d7 = avs.mo11775zO().z - this.imk.z;
            d += d7 * d7;
        }
        return d <= avs.dCJ() * avs.dCJ();
    }

    /* renamed from: a */
    public boolean mo9837a(aLH alh) {
        return mo9863e(alh);
    }

    /* renamed from: e */
    public boolean mo9863e(aLH alh) {
        Vec3d din = alh.din();
        Vec3d ajr = this.iml;
        if (ajr.x > din.x) {
            return false;
        }
        Vec3d dim = alh.dim();
        Vec3d ajr2 = this.imk;
        if (ajr2.x < dim.x || ajr.y > din.y || ajr2.y < dim.y || ajr.z > din.z || ajr2.z < dim.z) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public aDM mo9831a(Vec3d ajr, Vec3d ajr2, boolean z) {
        double d;
        boolean z2;
        double d2;
        boolean z3;
        double d3;
        double d4;
        aDM adm = new aDM(ajr, ajr2);
        double d5 = ScriptRuntime.NaN;
        if (ajr.x < this.iml.x) {
            double d6 = this.iml.x - ajr.x;
            if (d6 > adm.getRayDelta().x) {
                return adm;
            }
            d = d6 / adm.getRayDelta().x;
            d5 = -1.0d;
            z2 = false;
        } else if (ajr.x > this.imk.x) {
            double d7 = this.imk.x - ajr.x;
            if (d7 < adm.getRayDelta().x) {
                return adm;
            }
            d = d7 / adm.getRayDelta().x;
            d5 = 1.0d;
            z2 = false;
        } else {
            d = -1.0d;
            z2 = true;
        }
        double d8 = ScriptRuntime.NaN;
        if (ajr.y < this.iml.y) {
            double d9 = this.iml.y - ajr.y;
            if (d9 > adm.getRayDelta().y) {
                return adm;
            }
            double d10 = d9 / adm.getRayDelta().y;
            z3 = false;
            d8 = -1.0d;
            d2 = d10;
        } else if (ajr.y > this.imk.y) {
            double d11 = this.imk.y - ajr.y;
            if (d11 < adm.getRayDelta().y) {
                return adm;
            }
            double d12 = d11 / adm.getRayDelta().y;
            z3 = false;
            d8 = 1.0d;
            d2 = d12;
        } else {
            d2 = -1.0d;
            z3 = z2;
        }
        double d13 = ScriptRuntime.NaN;
        if (ajr.z < this.iml.z) {
            double d14 = this.iml.z - ajr.z;
            if (d14 > adm.getRayDelta().z) {
                return adm;
            }
            d3 = d14 / adm.getRayDelta().z;
            z3 = false;
            d13 = -1.0d;
        } else if (ajr.z > this.imk.z) {
            double d15 = this.imk.z - ajr.z;
            if (d15 < adm.getRayDelta().z) {
                return adm;
            }
            d3 = d15 / adm.getRayDelta().z;
            z3 = false;
            d13 = 1.0d;
        } else {
            d3 = -1.0d;
        }
        if (!z3) {
            boolean z4 = false;
            if (d2 > d) {
                z4 = true;
                d = d2;
            }
            if (d3 > d) {
                d4 = d3;
                z4 = true;
            } else {
                d4 = d;
            }
            if (!(z4)) {
                double d16 = ajr.y + (adm.getRayDelta().y * d4);
                if (d16 >= this.iml.y && d16 <= this.imk.y) {
                    double d17 = ajr.z + (adm.getRayDelta().z * d4);
                    if (d17 >= this.iml.z && d17 <= this.imk.z) {
                        adm.getNormal().set(d5, ScriptRuntime.NaN, ScriptRuntime.NaN);
                    } else {
                        return adm;
                    }
                } else {
                    return adm;
                }
            } else if (z4) {
                double d18 = ajr.x + (adm.getRayDelta().x * d4);
                if (d18 >= this.iml.x && d18 <= this.imk.x) {
                    double d19 = ajr.z + (adm.getRayDelta().z * d4);
                    if (d19 >= this.iml.z && d19 <= this.imk.z) {
                        adm.getNormal().set(ScriptRuntime.NaN, d8, ScriptRuntime.NaN);
                    } else {
                        return adm;
                    }
                } else {
                    return adm;
                }
            } else if (z4) {
                double d20 = ajr.x + (adm.getRayDelta().x * d4);
                if (d20 >= this.iml.x && d20 <= this.imk.x) {
                    double d21 = ajr.y + (adm.getRayDelta().y * d4);
                    if (d21 >= this.iml.y && d21 <= this.imk.y) {
                        adm.getNormal().set(ScriptRuntime.NaN, ScriptRuntime.NaN, d13);
                    } else {
                        return adm;
                    }
                } else {
                    return adm;
                }
            }
            adm.setParamIntersection(d4);
            adm.setIntersected(true);
            return adm;
        } else if (!z) {
            return adm;
        } else {
            adm.setNormal(adm.getRayDelta().dfZ().dfX());
            adm.setParamIntersection(ScriptRuntime.NaN);
            adm.setIntersected(true);
            return adm;
        }
    }

    public void reset() {
        Vec3d ajr = this.iml;
        Vec3d ajr2 = this.iml;
        this.iml.z = Double.POSITIVE_INFINITY;
        ajr2.y = Double.POSITIVE_INFINITY;
        ajr.x = Double.POSITIVE_INFINITY;
        Vec3d ajr3 = this.imk;
        Vec3d ajr4 = this.imk;
        this.imk.z = Double.NEGATIVE_INFINITY;
        ajr4.y = Double.NEGATIVE_INFINITY;
        ajr3.x = Double.NEGATIVE_INFINITY;
    }

    /* renamed from: aF */
    public void mo9841aF(Vec3d ajr) {
        Vec3d ajr2 = this.imk;
        Vec3d ajr3 = this.iml;
        double d = ajr.x;
        ajr3.x = d;
        ajr2.x = d;
        Vec3d ajr4 = this.imk;
        Vec3d ajr5 = this.iml;
        double d2 = ajr.y;
        ajr5.y = d2;
        ajr4.y = d2;
        Vec3d ajr6 = this.imk;
        Vec3d ajr7 = this.iml;
        double d3 = ajr.z;
        ajr7.z = d3;
        ajr6.z = d3;
    }

    /* renamed from: aG */
    public void mo9842aG(Vec3d ajr) {
        if (ajr.x < this.iml.x) {
            this.iml.x = ajr.x;
        }
        if (ajr.y < this.iml.y) {
            this.iml.y = ajr.y;
        }
        if (ajr.z < this.iml.z) {
            this.iml.z = ajr.z;
        }
        if (ajr.x > this.imk.x) {
            this.imk.x = ajr.x;
        }
        if (ajr.y > this.imk.y) {
            this.imk.y = ajr.y;
        }
        if (ajr.z > this.imk.z) {
            this.imk.z = ajr.z;
        }
    }

    public void addPoint(Vec3f vec3f) {
        if (((double) vec3f.x) < this.iml.x) {
            this.iml.x = (double) vec3f.x;
        }
        if (((double) vec3f.y) < this.iml.y) {
            this.iml.y = (double) vec3f.y;
        }
        if (((double) vec3f.z) < this.iml.z) {
            this.iml.z = (double) vec3f.z;
        }
        if (((double) vec3f.x) > this.imk.x) {
            this.imk.x = (double) vec3f.x;
        }
        if (((double) vec3f.y) > this.imk.y) {
            this.imk.y = (double) vec3f.y;
        }
        if (((double) vec3f.z) > this.imk.z) {
            this.imk.z = (double) vec3f.z;
        }
    }

    /* renamed from: a */
    public void mo9832a(Matrix4fWrap ajk, aLH alh) {
        Vec3d ajr = new Vec3d(this.imk.x, this.imk.y, this.imk.z);
        Vec3d ajr2 = new Vec3d(this.imk.x, this.imk.y, this.iml.z);
        Vec3d ajr3 = new Vec3d(this.imk.x, this.iml.y, this.iml.z);
        Vec3d ajr4 = new Vec3d(this.imk.x, this.iml.y, this.imk.z);
        Vec3d ajr5 = new Vec3d(this.iml.x, this.imk.y, this.imk.z);
        Vec3d ajr6 = new Vec3d(this.iml.x, this.imk.y, this.iml.z);
        Vec3d ajr7 = new Vec3d(this.iml.x, this.iml.y, this.iml.z);
        Vec3d ajr8 = new Vec3d(this.iml.x, this.iml.y, this.imk.z);
        alh.reset();
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr));
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr2));
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr3));
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr4));
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr5));
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr6));
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr7));
        alh.mo9842aG(ajk.mo13981a((Tuple3d) ajr8));
    }

    /* renamed from: b */
    public void mo9848b(Matrix4fWrap ajk, aLH alh) {
        float abs = Math.abs(ajk.m00);
        float abs2 = Math.abs(ajk.m01);
        float abs3 = Math.abs(ajk.m02);
        float abs4 = Math.abs(ajk.m10);
        float abs5 = Math.abs(ajk.m11);
        float abs6 = Math.abs(ajk.m12);
        float abs7 = Math.abs(ajk.m20);
        float abs8 = Math.abs(ajk.m21);
        float abs9 = Math.abs(ajk.m22);
        float dio = dio();
        float dip = dip();
        float diq = diq();
        float f = (((abs * dio) + (abs2 * dip)) + (diq * abs3)) / 2.0f;
        float f2 = (((dio * abs4) + (dip * abs5)) + (diq * abs6)) / 2.0f;
        float f3 = (((dio * abs7) + (dip * abs8)) + (diq * abs9)) / 2.0f;
        float f4 = ajk.m03;
        float f5 = ajk.m13;
        float f6 = ajk.m23;
        alh.imk.x = (double) (f4 + f);
        alh.imk.y = (double) (f5 + f2);
        alh.imk.z = (double) (f6 + f3);
        alh.iml.x = (double) (f4 - f);
        alh.iml.y = (double) (f5 - f2);
        alh.iml.z = (double) (f6 - f3);
    }

    /* renamed from: a */
    public void mo9833a(Quat4fWrap aoy, Vec3d ajr, aLH alh) {
        alh.reset();
        float f = aoy.x;
        float f2 = aoy.y;
        float f3 = aoy.z;
        float f4 = aoy.w;
        float f5 = (f4 * f4) + (f * f) + (f2 * f2) + (f3 * f3);
        if (!Quat4fWrap.isZero(1.0f - f5)) {
            float sqrt = (float) Math.sqrt((double) f5);
            f /= sqrt;
            f2 /= sqrt;
            f3 /= sqrt;
            f4 /= sqrt;
        }
        float f6 = f * f;
        float f7 = f * f2;
        float f8 = f * f3;
        float f9 = f * f4;
        float f10 = f2 * f2;
        float f11 = f2 * f3;
        float f12 = f2 * f4;
        float f13 = f3 * f3;
        float f14 = f4 * f3;
        float abs = Math.abs(1.0f - (2.0f * (f10 + f13)));
        float abs2 = Math.abs(2.0f * (f7 - f14));
        float abs3 = Math.abs(2.0f * (f8 + f12));
        float abs4 = Math.abs((f14 + f7) * 2.0f);
        float abs5 = Math.abs(1.0f - ((f13 + f6) * 2.0f));
        float abs6 = Math.abs(2.0f * (f11 - f9));
        float abs7 = Math.abs((f8 - f12) * 2.0f);
        float abs8 = Math.abs((f9 + f11) * 2.0f);
        float abs9 = Math.abs(1.0f - ((f6 + f10) * 2.0f));
        float dio = dio();
        float dip = dip();
        float diq = diq();
        float f15 = (((abs * dio) + (abs2 * dip)) + (diq * abs3)) / 2.0f;
        float f16 = (((abs4 * dio) + (abs5 * dip)) + (diq * abs6)) / 2.0f;
        float f17 = (((abs7 * dio) + (abs8 * dip)) + (diq * abs9)) / 2.0f;
        double d = ajr.x;
        double d2 = ajr.y;
        double d3 = ajr.z;
        alh.imk.x = ((double) f15) + d;
        alh.imk.y = ((double) f16) + d2;
        alh.imk.z = ((double) f17) + d3;
        alh.iml.x = d - ((double) f15);
        alh.iml.y = d2 - ((double) f16);
        alh.iml.z = d3 - ((double) f17);
    }

    public String toString() {
        return "[(" + this.iml.x + ", " + this.iml.y + ", " + this.iml.z + ") -> (" + this.imk.x + ", " + this.imk.y + ", " + this.imk.z + ")]";
    }

    public int hashCode() {
        return this.iml.hashCode() * this.imk.hashCode();
    }

    /* renamed from: div */
    public aLH clone() {
        return new aLH(this.iml, this.imk);
    }

    /* renamed from: zO */
    public Vec3d mo9875zO() {
        new Vec3d();
        Vec3d d = this.imk.mo9504d((Tuple3d) this.iml);
        d.scale(0.5d);
        return d;
    }

    /* renamed from: aH */
    public void mo9843aH(Vec3d ajr) {
        ajr.add(this.imk, this.iml);
        ajr.scale(0.5d);
    }

    /* renamed from: A */
    public void mo9824A(double d, double d2, double d3) {
        this.imk.x = d;
        this.imk.y = d2;
        this.imk.z = d3;
    }

    /* renamed from: B */
    public void mo9825B(double d, double d2, double d3) {
        this.iml.x = d;
        this.iml.y = d2;
        this.iml.z = d3;
    }

    /* renamed from: f */
    public void mo9865f(aLH alh) {
        Vec3d din = alh.din();
        Vec3d dim = alh.dim();
        mo9824A(din.x, din.y, din.z);
        mo9825B(dim.x, dim.y, dim.z);
    }

    /* renamed from: a */
    public void mo9835a(BoundingBox boundingBox, Vec3d ajr) {
        Vec3f dqQ = boundingBox.dqQ();
        Vec3f dqP = boundingBox.dqP();
        this.imk.x = ((double) dqQ.x) + ajr.x;
        this.imk.y = ((double) dqQ.y) + ajr.y;
        this.imk.z = ((double) dqQ.z) + ajr.z;
        this.iml.x = ((double) dqP.x) + ajr.x;
        this.iml.y = ((double) dqP.y) + ajr.y;
        this.iml.z = ((double) dqP.z) + ajr.z;
    }

    /* renamed from: aI */
    public void mo9844aI(Vec3d ajr) {
        this.iml.x += ajr.x;
        this.iml.y += ajr.y;
        this.iml.z += ajr.z;
        this.imk.x += ajr.x;
        this.imk.y += ajr.y;
        this.imk.z += ajr.z;
    }

    /* renamed from: b */
    public void mo9849b(IGeometryD tiVar) {
        this.iml.x += tiVar.getX();
        this.iml.y += tiVar.getY();
        this.iml.z += tiVar.getZ();
        this.imk.x += tiVar.getX();
        this.imk.y += tiVar.getY();
        this.imk.z += tiVar.getZ();
    }

    /* renamed from: g */
    public void mo9867g(aLH alh) {
        this.imk.mo9484aA(alh.din());
        this.iml.mo9484aA(alh.dim());
    }

    /* renamed from: c */
    public void mo9850c(BoundingBox boundingBox) {
        this.iml.mo9497bG(boundingBox.dqP());
        this.imk.mo9497bG(boundingBox.dqQ());
    }

    /* renamed from: h */
    public void mo9868h(aLH alh) {
        mo9842aG(alh.imk);
        mo9842aG(alh.iml);
    }

    /* renamed from: a */
    public void mo9834a(TransformWrap bcVar, aLH alh) {
        cRz.set(this.imk.x, this.imk.y, this.imk.z);
        cRA.set(this.imk.x, this.imk.y, this.iml.z);
        cRB.set(this.imk.x, this.iml.y, this.iml.z);
        imm.set(this.imk.x, this.iml.y, this.imk.z);
        imn.set(this.iml.x, this.imk.y, this.imk.z);
        imo.set(this.iml.x, this.imk.y, this.iml.z);
        imp.set(this.iml.x, this.iml.y, this.iml.z);
        imq.set(this.iml.x, this.iml.y, this.imk.z);
        alh.reset();
        bcVar.mo17331a(cRz);
        alh.mo9842aG(cRz);
        bcVar.mo17331a(cRA);
        alh.mo9842aG(cRA);
        bcVar.mo17331a(cRB);
        alh.mo9842aG(cRB);
        bcVar.mo17331a(imm);
        alh.mo9842aG(imm);
        bcVar.mo17331a(imn);
        alh.mo9842aG(imn);
        bcVar.mo17331a(imo);
        alh.mo9842aG(imo);
        bcVar.mo17331a(imp);
        alh.mo9842aG(imp);
        bcVar.mo17331a(imq);
        alh.mo9842aG(imq);
    }

    /* renamed from: g */
    public void mo9866g(float f, float f2, float f3, float f4, float f5, float f6) {
        this.iml.set((double) f, (double) f2, (double) f3);
        this.imk.set((double) f4, (double) f5, (double) f6);
    }

    /* renamed from: C */
    public void mo9826C(double d, double d2, double d3) {
        if (d < this.iml.x) {
            this.iml.x = d;
        }
        if (d2 < this.iml.y) {
            this.iml.y = d2;
        }
        if (d3 < this.iml.z) {
            this.iml.z = d3;
        }
        if (d > this.imk.x) {
            this.imk.x = d;
        }
        if (d2 > this.imk.y) {
            this.imk.y = d2;
        }
        if (d3 > this.imk.z) {
            this.imk.z = d3;
        }
    }

    /* renamed from: aJ */
    public void mo9845aJ(Vec3d ajr) {
        this.iml.x -= ajr.x;
        this.iml.y -= ajr.y;
        this.iml.z -= ajr.z;
        this.imk.x -= ajr.x;
        this.imk.y -= ajr.y;
        this.imk.z -= ajr.z;
    }

    /* renamed from: aK */
    public void mo9846aK(Vec3d ajr) {
        this.iml.add(ajr);
        this.imk.add(ajr);
    }

    /* renamed from: D */
    public void mo9827D(double d, double d2, double d3) {
        this.iml.mo9556x(d, d2, d3);
        this.imk.mo9556x(d, d2, d3);
    }

    /* renamed from: E */
    public void mo9828E(double d, double d2, double d3) {
        this.iml.x -= d;
        this.iml.y -= d2;
        this.iml.z -= d3;
        this.imk.x += d;
        this.imk.y += d2;
        this.imk.z += d3;
    }

    /* renamed from: F */
    public void mo9829F(double d, double d2, double d3) {
        this.iml.x *= d;
        this.iml.y *= d2;
        this.iml.z *= d3;
        this.imk.x *= d;
        this.imk.y *= d2;
        this.imk.z *= d3;
    }

    public void scale(double d) {
        mo9829F(d, d, d);
    }

    public void readExternal(IReadExternal vm) {
        this.iml = (Vec3d) vm.mo6367he("mins");
        this.imk = (Vec3d) vm.mo6367he("maxes");
    }

    public void writeExternal(IWriteExternal att) {
        att.mo16261a("mins", (IExternalIO) this.iml);
        att.mo16261a("maxes", (IExternalIO) this.imk);
    }

    /* renamed from: G */
    public void mo9830G(double d, double d2, double d3) {
        this.iml.mo9529o(d, d2, d3);
        this.imk.mo9558z(d, d2, d3);
    }
}
