package game.script;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6466anW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aHu  reason: case insensitive filesystem */
/* compiled from: a */
public class BlockStatus extends TaskletImpl implements C1616Xf {

    /* renamed from: JD */
    public static final C2491fm f3009JD = null;
    public static final C5663aRz _f_enabled = null;
    public static final C2491fm _f_isEnabled_0020_0028_0029Z = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm fIw = null;
    public static final C5663aRz hXo = null;
    public static final C2491fm hXp = null;
    /* renamed from: hz */
    public static final C5663aRz f3011hz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "203c3c3fc1e083830eebeb70817162cb", aum = 1)

    /* renamed from: P */
    private static Player f3010P;
    @C0064Am(aul = "a2bdcbc6ab40034c70f4388c01b06b23", aum = 2)
    private static boolean enabled;
    @C0064Am(aul = "6de40260b478a81790fb648c060ab5cb", aum = 0)
    private static C2546ge gfG;

    static {
        m15308V();
    }

    public BlockStatus() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BlockStatus(C5540aNg ang) {
        super(ang);
    }

    public BlockStatus(C2546ge geVar, Player aku, long j) {
        super((C5540aNg) null);
        super._m_script_init(geVar, aku, j);
    }

    /* renamed from: V */
    static void m15308V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaskletImpl._m_fieldCount + 3;
        _m_methodCount = TaskletImpl._m_methodCount + 4;
        int i = TaskletImpl._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(BlockStatus.class, "6de40260b478a81790fb648c060ab5cb", i);
        hXo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BlockStatus.class, "203c3c3fc1e083830eebeb70817162cb", i2);
        f3011hz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BlockStatus.class, "a2bdcbc6ab40034c70f4388c01b06b23", i3);
        _f_enabled = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
        int i5 = TaskletImpl._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 4)];
        C2491fm a = C4105zY.m41624a(BlockStatus.class, "2af125f79d76abd793883a619a3cabf0", i5);
        fIw = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(BlockStatus.class, "864c9840a64b43cb3cda51c4cfeab07b", i6);
        _f_isEnabled_0020_0028_0029Z = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(BlockStatus.class, "23e0435c31e38d7742581dbdf0b9cddc", i7);
        hXp = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(BlockStatus.class, "e13115ec95b8261e687b914a1d1dd7e1", i8);
        f3009JD = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BlockStatus.class, C6466anW.class, _m_fields, _m_methods);
    }

    /* renamed from: J */
    private void m15307J(boolean z) {
        bFf().mo5608dq().mo3153a(_f_enabled, z);
    }

    /* renamed from: a */
    private void m15309a(Player aku) {
        bFf().mo5608dq().mo3197f(f3011hz, aku);
    }

    /* renamed from: a */
    private void m15310a(C2546ge geVar) {
        bFf().mo5608dq().mo3197f(hXo, geVar);
    }

    /* renamed from: dG */
    private Player m15311dG() {
        return (Player) bFf().mo5608dq().mo3214p(f3011hz);
    }

    private C2546ge ddm() {
        return (C2546ge) bFf().mo5608dq().mo3214p(hXo);
    }

    private void ddo() {
        switch (bFf().mo6893i(hXp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hXp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hXp, new Object[0]));
                break;
        }
        ddn();
    }

    /* renamed from: rm */
    private boolean m15313rm() {
        return bFf().mo5608dq().mo3201h(_f_enabled);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6466anW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
            case 0:
                bYv();
                return null;
            case 1:
                return new Boolean(m15314rx());
            case 2:
                ddn();
                return null;
            case 3:
                m15312pi();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void disable() {
        switch (bFf().mo6893i(fIw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIw, new Object[0]));
                break;
        }
        bYv();
    }

    public boolean isEnabled() {
        switch (bFf().mo6893i(_f_isEnabled_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m15314rx();
    }

    /* access modifiers changed from: protected */
    /* renamed from: pj */
    public void mo667pj() {
        switch (bFf().mo6893i(f3009JD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3009JD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3009JD, new Object[0]));
                break;
        }
        m15312pi();
    }

    /* renamed from: a */
    public void mo9280a(C2546ge geVar, Player aku, long j) {
        super.mo4706l(aku);
        m15310a(geVar);
        m15309a(aku);
        if (j > 0) {
            mo4702d((float) (60 * j), 1);
        }
        m15307J(true);
    }

    @C0064Am(aul = "2af125f79d76abd793883a619a3cabf0", aum = 0)
    private void bYv() {
        m15307J(false);
    }

    @C0064Am(aul = "864c9840a64b43cb3cda51c4cfeab07b", aum = 0)
    /* renamed from: rx */
    private boolean m15314rx() {
        return m15313rm();
    }

    @C0064Am(aul = "23e0435c31e38d7742581dbdf0b9cddc", aum = 0)
    private void ddn() {
        if (m15313rm()) {
            m15311dG().mo14418e(ddm());
        }
    }

    @C0064Am(aul = "e13115ec95b8261e687b914a1d1dd7e1", aum = 0)
    /* renamed from: pi */
    private void m15312pi() {
        ddo();
    }
}
