package game.script;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C0571Hu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aIq  reason: case insensitive filesystem */
/* compiled from: a */
public class TemporaryFeatureTest extends aDJ implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bkF = null;
    public static final C5663aRz ibW = null;
    public static final C5663aRz ibX = null;
    public static final C2491fm ibY = null;
    public static final C2491fm ibZ = null;
    public static final C2491fm ica = null;
    public static final C2491fm icb = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7ae4be2989830a1ab4b655b81b5e9226", aum = 0)
    private static boolean dbD;
    @C0064Am(aul = "ab9a9a579c3b6cf2e5dfa6ea46519441", aum = 1)
    private static boolean dbE;

    static {
        m15482V();
    }

    public TemporaryFeatureTest() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TemporaryFeatureTest(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15482V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 6;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(TemporaryFeatureTest.class, "7ae4be2989830a1ab4b655b81b5e9226", i);
        ibW = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TemporaryFeatureTest.class, "ab9a9a579c3b6cf2e5dfa6ea46519441", i2);
        ibX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(TemporaryFeatureTest.class, "aba7e6ec5a788ae264c692d1ebedc62b", i4);
        _f_onResurrect_0020_0028_0029V = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(TemporaryFeatureTest.class, "c5b3851a114b0cec0a6f1d3d8fd4f4c2", i5);
        bkF = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(TemporaryFeatureTest.class, "3ef02e2c8aa1824609eaf6fd94e9c50b", i6);
        ibY = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(TemporaryFeatureTest.class, "c80532f529e42268f4ee65548fca736e", i7);
        ibZ = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(TemporaryFeatureTest.class, "94f564b3b2768a5bc2e7039b65c9b25b", i8);
        ica = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(TemporaryFeatureTest.class, "65fdd44b35ed60dc3d2618390cae5d76", i9);
        icb = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TemporaryFeatureTest.class, C0571Hu.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "c5b3851a114b0cec0a6f1d3d8fd4f4c2", aum = 0)
    @C5566aOg
    private void adk() {
        throw new aWi(new aCE(this, bkF, new Object[0]));
    }

    private boolean dep() {
        return bFf().mo5608dq().mo3201h(ibW);
    }

    private boolean deq() {
        return bFf().mo5608dq().mo3201h(ibX);
    }

    /* renamed from: jC */
    private void m15484jC(boolean z) {
        bFf().mo5608dq().mo3153a(ibW, z);
    }

    /* renamed from: jD */
    private void m15485jD(boolean z) {
        bFf().mo5608dq().mo3153a(ibX, z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0571Hu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m15483aG();
                return null;
            case 1:
                adk();
                return null;
            case 2:
                return new Boolean(der());
            case 3:
                m15486jE(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return new Boolean(det());
            case 5:
                m15487jG(((Boolean) args[0]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m15483aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public boolean des() {
        switch (bFf().mo6893i(ibY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ibY, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ibY, new Object[0]));
                break;
        }
        return der();
    }

    public boolean deu() {
        switch (bFf().mo6893i(ica)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ica, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ica, new Object[0]));
                break;
        }
        return det();
    }

    /* renamed from: jF */
    public void mo9399jF(boolean z) {
        switch (bFf().mo6893i(ibZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ibZ, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ibZ, new Object[]{new Boolean(z)}));
                break;
        }
        m15486jE(z);
    }

    @C5566aOg
    public void reset() {
        switch (bFf().mo6893i(bkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                break;
        }
        adk();
    }

    public void setVersion(boolean z) {
        switch (bFf().mo6893i(icb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icb, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icb, new Object[]{new Boolean(z)}));
                break;
        }
        m15487jG(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "aba7e6ec5a788ae264c692d1ebedc62b", aum = 0)
    /* renamed from: aG */
    private void m15483aG() {
        reset();
        super.mo70aH();
    }

    @C0064Am(aul = "3ef02e2c8aa1824609eaf6fd94e9c50b", aum = 0)
    private boolean der() {
        return dep();
    }

    @C0064Am(aul = "c80532f529e42268f4ee65548fca736e", aum = 0)
    /* renamed from: jE */
    private void m15486jE(boolean z) {
        m15484jC(z);
    }

    @C0064Am(aul = "94f564b3b2768a5bc2e7039b65c9b25b", aum = 0)
    private boolean det() {
        return deq();
    }

    @C0064Am(aul = "65fdd44b35ed60dc3d2618390cae5d76", aum = 0)
    /* renamed from: jG */
    private void m15487jG(boolean z) {
        m15485jD(z);
    }
}
