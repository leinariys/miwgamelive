package game.script;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import logic.baa.*;
import logic.data.mbean.C3723uC;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.zl */
/* compiled from: a */
public class ItemTypeTableItem extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f9725bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9726bM = null;
    /* renamed from: bN */
    public static final C2491fm f9727bN = null;
    /* renamed from: bO */
    public static final C2491fm f9728bO = null;
    /* renamed from: bP */
    public static final C2491fm f9729bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9730bQ = null;
    /* renamed from: cA */
    public static final C2491fm f9731cA = null;
    /* renamed from: cB */
    public static final C2491fm f9732cB = null;
    /* renamed from: cC */
    public static final C2491fm f9733cC = null;
    /* renamed from: cD */
    public static final C2491fm f9734cD = null;
    /* renamed from: cx */
    public static final C5663aRz f9736cx = null;
    /* renamed from: cz */
    public static final C5663aRz f9738cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3638137f8188575a1bb821c9688f61ae", aum = 2)

    /* renamed from: bK */
    private static UUID f9724bK;
    @C0064Am(aul = "79abd2644160e05ac0e2777d5da48484", aum = 0)

    /* renamed from: cw */
    private static ItemType f9735cw;
    @C0064Am(aul = "1b2c59af0d4bb98655f23f9c00269ad7", aum = 1)

    /* renamed from: cy */
    private static int f9737cy;
    @C0064Am(aul = "e20c09b69a12e508a2ffe31484e7db9d", aum = 3)
    private static String handle;

    static {
        m41813V();
    }

    public ItemTypeTableItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemTypeTableItem(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41813V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 8;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ItemTypeTableItem.class, "79abd2644160e05ac0e2777d5da48484", i);
        f9736cx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemTypeTableItem.class, "1b2c59af0d4bb98655f23f9c00269ad7", i2);
        f9738cz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemTypeTableItem.class, "3638137f8188575a1bb821c9688f61ae", i3);
        f9725bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemTypeTableItem.class, "e20c09b69a12e508a2ffe31484e7db9d", i4);
        f9726bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(ItemTypeTableItem.class, "21e927e4e34c33e3ea6722270d8af243", i6);
        f9727bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemTypeTableItem.class, "728b6a811c3a2471dc51dcea7d91c20a", i7);
        f9728bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemTypeTableItem.class, "386a87f5bb86cffd6d7011eb7cf70a56", i8);
        f9729bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemTypeTableItem.class, "4545c8f4fddd9369b99e6607b1650228", i9);
        f9730bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemTypeTableItem.class, "63d5335cd7561967d28a93f789a3db79", i10);
        f9731cA = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemTypeTableItem.class, "369baa22bc6c76a28251c4de8b773a4a", i11);
        f9732cB = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemTypeTableItem.class, "18fb7e8fb8ab2d4a056090fb18ad71bd", i12);
        f9733cC = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemTypeTableItem.class, "18d6068d96c3cf10eef13ef82a05c964", i13);
        f9734cD = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemTypeTableItem.class, C3723uC.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m41814a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f9736cx, jCVar);
    }

    /* renamed from: a */
    private void m41815a(String str) {
        bFf().mo5608dq().mo3197f(f9726bM, str);
    }

    /* renamed from: a */
    private void m41816a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9725bL, uuid);
    }

    /* renamed from: an */
    private UUID m41817an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9725bL);
    }

    /* renamed from: ao */
    private String m41818ao() {
        return (String) bFf().mo5608dq().mo3214p(f9726bM);
    }

    /* renamed from: av */
    private ItemType m41821av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f9736cx);
    }

    /* renamed from: aw */
    private int m41822aw() {
        return bFf().mo5608dq().mo3212n(f9738cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C0064Am(aul = "18d6068d96c3cf10eef13ef82a05c964", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m41825b(ItemType jCVar) {
        throw new aWi(new aCE(this, f9734cD, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "4545c8f4fddd9369b99e6607b1650228", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m41826b(String str) {
        throw new aWi(new aCE(this, f9730bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m41828c(UUID uuid) {
        switch (bFf().mo6893i(f9728bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9728bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9728bO, new Object[]{uuid}));
                break;
        }
        m41827b(uuid);
    }

    /* renamed from: f */
    private void m41829f(int i) {
        bFf().mo5608dq().mo3183b(f9738cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "369baa22bc6c76a28251c4de8b773a4a", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m41830g(int i) {
        throw new aWi(new aCE(this, f9732cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3723uC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m41819ap();
            case 1:
                m41827b((UUID) args[0]);
                return null;
            case 2:
                return m41820ar();
            case 3:
                m41826b((String) args[0]);
                return null;
            case 4:
                return new Integer(m41823ax());
            case 5:
                m41830g(((Integer) args[0]).intValue());
                return null;
            case 6:
                return m41824ay();
            case 7:
                m41825b((ItemType) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9727bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9727bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9727bN, new Object[0]));
                break;
        }
        return m41819ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    /* renamed from: az */
    public ItemType mo23385az() {
        switch (bFf().mo6893i(f9733cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f9733cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9733cC, new Object[0]));
                break;
        }
        return m41824ay();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C5566aOg
    /* renamed from: c */
    public void mo23386c(ItemType jCVar) {
        switch (bFf().mo6893i(f9734cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9734cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9734cD, new Object[]{jCVar}));
                break;
        }
        m41825b(jCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f9731cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f9731cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9731cA, new Object[0]));
                break;
        }
        return m41823ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f9732cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9732cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9732cB, new Object[]{new Integer(i)}));
                break;
        }
        m41830g(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9729bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9729bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9729bP, new Object[0]));
                break;
        }
        return m41820ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9730bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9730bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9730bQ, new Object[]{str}));
                break;
        }
        m41826b(str);
    }

    @C0064Am(aul = "21e927e4e34c33e3ea6722270d8af243", aum = 0)
    /* renamed from: ap */
    private UUID m41819ap() {
        return m41817an();
    }

    @C0064Am(aul = "728b6a811c3a2471dc51dcea7d91c20a", aum = 0)
    /* renamed from: b */
    private void m41827b(UUID uuid) {
        m41816a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m41816a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "386a87f5bb86cffd6d7011eb7cf70a56", aum = 0)
    /* renamed from: ar */
    private String m41820ar() {
        return m41818ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "63d5335cd7561967d28a93f789a3db79", aum = 0)
    /* renamed from: ax */
    private int m41823ax() {
        return m41822aw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    @C0064Am(aul = "18fb7e8fb8ab2d4a056090fb18ad71bd", aum = 0)
    /* renamed from: ay */
    private ItemType m41824ay() {
        return m41821av();
    }
}
