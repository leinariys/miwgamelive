package game.script.billing;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C3419rM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.arx  reason: case insensitive filesystem */
/* compiled from: a */
public class ItemBillingDefaults extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5265bL = null;
    /* renamed from: bN */
    public static final C2491fm f5266bN = null;
    /* renamed from: bO */
    public static final C2491fm f5267bO = null;
    /* renamed from: bP */
    public static final C2491fm f5268bP = null;
    public static final C5663aRz gsf = null;
    public static final C2491fm gsg = null;
    public static final C2491fm gsh = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6a9f40a3496491801e1013a42f577494", aum = 1)

    /* renamed from: bK */
    private static UUID f5264bK;
    @C0064Am(aul = "6c3c7e1009aca6d3dc01d45d9cb15e83", aum = 0)
    private static long bcm;

    static {
        m25606V();
    }

    public ItemBillingDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemBillingDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25606V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 5;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ItemBillingDefaults.class, "6c3c7e1009aca6d3dc01d45d9cb15e83", i);
        gsf = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemBillingDefaults.class, "6a9f40a3496491801e1013a42f577494", i2);
        f5265bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(ItemBillingDefaults.class, "ea55b1a829a411ae6daf219656b15f5a", i4);
        f5266bN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemBillingDefaults.class, "328d3f09fa55aaa60bd701ea9611bba2", i5);
        f5267bO = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemBillingDefaults.class, "5007691eb3a4f4defbc526db4fbf94c9", i6);
        f5268bP = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemBillingDefaults.class, "81ab413ad623ac044330d7334d9135de", i7);
        gsg = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemBillingDefaults.class, "3ac428b78d29f5e7a0292221a64a80eb", i8);
        gsh = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemBillingDefaults.class, C3419rM.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m25607a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5265bL, uuid);
    }

    /* renamed from: an */
    private UUID m25608an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5265bL);
    }

    /* renamed from: c */
    private void m25612c(UUID uuid) {
        switch (bFf().mo6893i(f5267bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5267bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5267bO, new Object[]{uuid}));
                break;
        }
        m25611b(uuid);
    }

    private long csh() {
        return bFf().mo5608dq().mo3213o(gsf);
    }

    /* renamed from: ir */
    private void m25613ir(long j) {
        bFf().mo5608dq().mo3184b(gsf, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hoplons To Taels Conversion Tax")
    @C0064Am(aul = "3ac428b78d29f5e7a0292221a64a80eb", aum = 0)
    @C5566aOg
    /* renamed from: is */
    private void m25614is(long j) {
        throw new aWi(new aCE(this, gsh, new Object[]{new Long(j)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3419rM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m25609ap();
            case 1:
                m25611b((UUID) args[0]);
                return null;
            case 2:
                return m25610ar();
            case 3:
                return new Long(csi());
            case 4:
                m25614is(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5266bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5266bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5266bN, new Object[0]));
                break;
        }
        return m25609ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hoplons To Taels Conversion Tax")
    public long bmr() {
        switch (bFf().mo6893i(gsg)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gsg, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gsg, new Object[0]));
                break;
        }
        return csi();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String getHandle() {
        switch (bFf().mo6893i(f5268bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5268bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5268bP, new Object[0]));
                break;
        }
        return m25610ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hoplons To Taels Conversion Tax")
    @C5566aOg
    /* renamed from: it */
    public void mo15885it(long j) {
        switch (bFf().mo6893i(gsh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gsh, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gsh, new Object[]{new Long(j)}));
                break;
        }
        m25614is(j);
    }

    @C0064Am(aul = "ea55b1a829a411ae6daf219656b15f5a", aum = 0)
    /* renamed from: ap */
    private UUID m25609ap() {
        return m25608an();
    }

    @C0064Am(aul = "328d3f09fa55aaa60bd701ea9611bba2", aum = 0)
    /* renamed from: b */
    private void m25611b(UUID uuid) {
        m25607a(uuid);
    }

    @C0064Am(aul = "5007691eb3a4f4defbc526db4fbf94c9", aum = 0)
    /* renamed from: ar */
    private String m25610ar() {
        return "item_billing_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m25607a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hoplons To Taels Conversion Tax")
    @C0064Am(aul = "81ab413ad623ac044330d7334d9135de", aum = 0)
    private long csi() {
        return csh();
    }
}
