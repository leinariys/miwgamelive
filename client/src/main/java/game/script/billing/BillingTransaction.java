package game.script.billing;

import game.network.message.externalizable.aCE;
import game.script.bank.Bank;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5980aeE;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.aRr  reason: case insensitive filesystem */
/* compiled from: a */
public class BillingTransaction extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: cz */
    public static final C5663aRz f3726cz = null;
    public static final C2491fm gtF = null;
    public static final C5663aRz hyM = null;
    public static final C2491fm hzr = null;
    public static final C5663aRz iHs = null;
    public static final C5663aRz iHt = null;
    public static final C2491fm iHu = null;
    public static final C2491fm iHv = null;
    public static final C2491fm iHw = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c386166bdae60231c4a616a73be33681", aum = 2)
    private static long axi;
    @C0064Am(aul = "bd1ee111c0124d2b1bc41635d6c993c0", aum = 1)
    private static long fpP;
    @C0064Am(aul = "eac8a13e5cd5a5aaa9904c5658dfb52d", aum = 3)
    private static Billing fpQ;
    @C0064Am(aul = "c0d6b3904da8b8d020d3cce1314033e7", aum = 0)
    private static long when;

    static {
        m17994V();
    }

    public BillingTransaction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BillingTransaction(C5540aNg ang) {
        super(ang);
    }

    public BillingTransaction(Billing ace, long j, long j2) {
        super((C5540aNg) null);
        super._m_script_init(ace, j, j2);
    }

    /* renamed from: V */
    static void m17994V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 5;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(BillingTransaction.class, "c0d6b3904da8b8d020d3cce1314033e7", i);
        iHs = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BillingTransaction.class, "bd1ee111c0124d2b1bc41635d6c993c0", i2);
        iHt = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BillingTransaction.class, "c386166bdae60231c4a616a73be33681", i3);
        f3726cz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(BillingTransaction.class, "eac8a13e5cd5a5aaa9904c5658dfb52d", i4);
        hyM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 5)];
        C2491fm a = C4105zY.m41624a(BillingTransaction.class, "b13461d6d2edfc9bebc1a0e558c08d3d", i6);
        hzr = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(BillingTransaction.class, "1319b2a178137b7c9af4057c1126f80f", i7);
        gtF = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(BillingTransaction.class, "9c0d9ec73ca68b2282e9ffbc3d216661", i8);
        iHu = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(BillingTransaction.class, "2663f80f08063ce8ff4ee2b0ef529187", i9);
        iHv = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(BillingTransaction.class, "51b8ffcfa6ecbd465bd89bd7f4edfe5a", i10);
        iHw = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BillingTransaction.class, C5980aeE.class, _m_fields, _m_methods);
    }

    /* renamed from: LS */
    private long m17993LS() {
        return bFf().mo5608dq().mo3213o(f3726cz);
    }

    /* renamed from: a */
    private void m17995a(Billing ace) {
        bFf().mo5608dq().mo3197f(hyM, ace);
    }

    private Billing cSE() {
        return (Billing) bFf().mo5608dq().mo3214p(hyM);
    }

    /* renamed from: cj */
    private void m17996cj(long j) {
        bFf().mo5608dq().mo3184b(f3726cz, j);
    }

    private long drE() {
        return bFf().mo5608dq().mo3213o(iHs);
    }

    private long drF() {
        return bFf().mo5608dq().mo3213o(iHt);
    }

    /* renamed from: lR */
    private void m17997lR(long j) {
        bFf().mo5608dq().mo3184b(iHs, j);
    }

    /* renamed from: lS */
    private void m17998lS(long j) {
        bFf().mo5608dq().mo3184b(iHt, j);
    }

    /* renamed from: NI */
    public void mo11221NI() {
        switch (bFf().mo6893i(iHw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iHw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iHw, new Object[0]));
                break;
        }
        drJ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: Sh */
    public long mo11222Sh() {
        switch (bFf().mo6893i(gtF)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gtF, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gtF, new Object[0]));
                break;
        }
        return csx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5980aeE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cTf();
            case 1:
                return new Long(csx());
            case 2:
                return new Long(drG());
            case 3:
                return new Long(drH());
            case 4:
                drJ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Billing cTg() {
        switch (bFf().mo6893i(hzr)) {
            case 0:
                return null;
            case 2:
                return (Billing) bFf().mo5606d(new aCE(this, hzr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzr, new Object[0]));
                break;
        }
        return cTf();
    }

    public long drI() {
        switch (bFf().mo6893i(iHv)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iHv, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iHv, new Object[0]));
                break;
        }
        return drH();
    }

    public long getWhen() {
        switch (bFf().mo6893i(iHu)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iHu, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iHu, new Object[0]));
                break;
        }
        return drG();
    }

    /* renamed from: a */
    public void mo11223a(Billing ace, long j, long j2) {
        super.mo10S();
        m17995a(ace);
        m17998lS(j);
        m17996cj(j2);
        m17997lR(cVr());
    }

    @C0064Am(aul = "b13461d6d2edfc9bebc1a0e558c08d3d", aum = 0)
    private Billing cTf() {
        return cSE();
    }

    @C0064Am(aul = "1319b2a178137b7c9af4057c1126f80f", aum = 0)
    private long csx() {
        return m17993LS();
    }

    @C0064Am(aul = "9c0d9ec73ca68b2282e9ffbc3d216661", aum = 0)
    private long drG() {
        return drE();
    }

    @C0064Am(aul = "2663f80f08063ce8ff4ee2b0ef529187", aum = 0)
    private long drH() {
        return drF();
    }

    @C0064Am(aul = "51b8ffcfa6ecbd465bd89bd7f4edfe5a", aum = 0)
    private void drJ() {
        ala().aIU().mo12602b(cTg().bOx(), m17993LS(), Bank.C1861a.HOPLONS_CREDIT, "Billing transaction", 0);
        mo8359ma("Billing transaction confirmed.");
    }
}
