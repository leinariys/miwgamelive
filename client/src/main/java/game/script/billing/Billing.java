package game.script.billing;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.player.User;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6565apR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.ace  reason: case insensitive filesystem */
/* compiled from: a */
public class Billing extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz avF = null;
    public static final C5663aRz dfn = null;
    public static final C2491fm faA = null;
    public static final C2491fm faB = null;
    public static final C5663aRz faq = null;
    public static final C2491fm fas = null;
    public static final C2491fm fat = null;
    public static final C2491fm fau = null;
    public static final C2491fm fav = null;
    public static final C2491fm faw = null;
    public static final C2491fm fax = null;
    public static final C2491fm fay = null;
    public static final C2491fm faz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a4e3f3ea2fef7b6573775b0d2d7fbe6c", aum = 2)
    private static C3438ra<BillingTransaction> dfm;
    @C0064Am(aul = "7c4d1075b6c8f52bf73bb34595155e30", aum = 0)
    private static long fap;
    @C0064Am(aul = "64d5084565e2d0757b5a45c400cce698", aum = 1)
    private static User far;

    static {
        m20524V();
    }

    public Billing() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Billing(User adk) {
        super((C5540aNg) null);
        super._m_script_init(adk);
    }

    public Billing(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20524V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Billing.class, "7c4d1075b6c8f52bf73bb34595155e30", i);
        faq = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Billing.class, "64d5084565e2d0757b5a45c400cce698", i2);
        avF = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Billing.class, "a4e3f3ea2fef7b6573775b0d2d7fbe6c", i3);
        dfn = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 11)];
        C2491fm a = C4105zY.m41624a(Billing.class, "d676d7267af269f6c25dd58216575ca5", i5);
        fas = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Billing.class, "d1addfc73a0eb6307cd55140b9555fc7", i6);
        fat = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Billing.class, "fee9baebf2225b6e68294b6cf0d97d8c", i7);
        fau = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Billing.class, "c555fa0ce0d67c77d00b418f3eb23b7f", i8);
        fav = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Billing.class, "339ec2c445ed406470d2960466fd0f86", i9);
        faw = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Billing.class, "1941285f7b21c79895044ed619907a82", i10);
        _f_tick_0020_0028F_0029V = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Billing.class, "16a460c4614827414eb4ab2752808468", i11);
        fax = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(Billing.class, "ae5dc6f52e072446185ec0057bfefd54", i12);
        fay = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(Billing.class, "7efa24124ee405d8009ebdbc3185b174", i13);
        faz = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(Billing.class, "ced4fdaf5bbb05de0f10eb66f7530d01", i14);
        faA = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(Billing.class, "b87bc568b3b84ee60b6e614df366aea2", i15);
        faB = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Billing.class, C6565apR.class, _m_fields, _m_methods);
    }

    private C3438ra aWc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dfn);
    }

    private long bOm() {
        return bFf().mo5608dq().mo3213o(faq);
    }

    private User bOn() {
        return (User) bFf().mo5608dq().mo3214p(avF);
    }

    /* renamed from: ba */
    private void m20525ba(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dfn, raVar);
    }

    /* renamed from: gx */
    private void m20527gx(long j) {
        bFf().mo5608dq().mo3184b(faq, j);
    }

    /* renamed from: t */
    private void m20530t(User adk) {
        bFf().mo5608dq().mo3197f(avF, adk);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m20523U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6565apR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return bOo();
            case 1:
                return new Long(bOq());
            case 2:
                m20528gy(((Long) args[0]).longValue());
                return null;
            case 3:
                return new Boolean(bOs());
            case 4:
                bOu();
                return null;
            case 5:
                m20523U(((Float) args[0]).floatValue());
                return null;
            case 6:
                return bOw();
            case 7:
                m20531v((User) args[0]);
                return null;
            case 8:
                bOy();
                return null;
            case 9:
                return new Boolean(m20526gA(((Long) args[0]).longValue()));
            case 10:
                return new Boolean(m20529k(((Long) args[0]).longValue(), ((Long) args[1]).longValue()));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C3438ra<BillingTransaction> bOp() {
        switch (bFf().mo6893i(fas)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fas, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fas, new Object[0]));
                break;
        }
        return bOo();
    }

    public long bOr() {
        switch (bFf().mo6893i(fat)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, fat, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, fat, new Object[0]));
                break;
        }
        return bOq();
    }

    public boolean bOt() {
        switch (bFf().mo6893i(fav)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fav, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fav, new Object[0]));
                break;
        }
        return bOs();
    }

    public void bOv() {
        switch (bFf().mo6893i(faw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, faw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, faw, new Object[0]));
                break;
        }
        bOu();
    }

    public User bOx() {
        switch (bFf().mo6893i(fax)) {
            case 0:
                return null;
            case 2:
                return (User) bFf().mo5606d(new aCE(this, fax, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fax, new Object[0]));
                break;
        }
        return bOw();
    }

    public void bOz() {
        switch (bFf().mo6893i(faz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, faz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, faz, new Object[0]));
                break;
        }
        bOy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: gB */
    public boolean mo12673gB(long j) {
        switch (bFf().mo6893i(faA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, faA, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, faA, new Object[]{new Long(j)}));
                break;
        }
        return m20526gA(j);
    }

    /* renamed from: gz */
    public void mo12674gz(long j) {
        switch (bFf().mo6893i(fau)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fau, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fau, new Object[]{new Long(j)}));
                break;
        }
        m20528gy(j);
    }

    /* renamed from: l */
    public boolean mo12675l(long j, long j2) {
        switch (bFf().mo6893i(faB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, faB, new Object[]{new Long(j), new Long(j2)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, faB, new Object[]{new Long(j), new Long(j2)}));
                break;
        }
        return m20529k(j, j2);
    }

    /* renamed from: w */
    public void mo12677w(User adk) {
        switch (bFf().mo6893i(fay)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fay, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fay, new Object[]{adk}));
                break;
        }
        m20531v(adk);
    }

    /* renamed from: u */
    public void mo12676u(User adk) {
        super.mo10S();
        mo12677w(adk);
    }

    @C0064Am(aul = "d676d7267af269f6c25dd58216575ca5", aum = 0)
    private C3438ra<BillingTransaction> bOo() {
        return aWc();
    }

    @C0064Am(aul = "d1addfc73a0eb6307cd55140b9555fc7", aum = 0)
    private long bOq() {
        return bOm();
    }

    @C0064Am(aul = "fee9baebf2225b6e68294b6cf0d97d8c", aum = 0)
    /* renamed from: gy */
    private void m20528gy(long j) {
        m20527gx(j);
        if (bOn().cSV() != null) {
            bOv();
        }
    }

    @C0064Am(aul = "c555fa0ce0d67c77d00b418f3eb23b7f", aum = 0)
    private boolean bOs() {
        if (bOr() > cVr()) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "339ec2c445ed406470d2960466fd0f86", aum = 0)
    private void bOu() {
    }

    @C0064Am(aul = "1941285f7b21c79895044ed619907a82", aum = 0)
    /* renamed from: U */
    private void m20523U(float f) {
        bOv();
    }

    @C0064Am(aul = "16a460c4614827414eb4ab2752808468", aum = 0)
    private User bOw() {
        return bOn();
    }

    @C0064Am(aul = "ae5dc6f52e072446185ec0057bfefd54", aum = 0)
    /* renamed from: v */
    private void m20531v(User adk) {
        m20530t(adk);
    }

    @C0064Am(aul = "7efa24124ee405d8009ebdbc3185b174", aum = 0)
    private void bOy() {
        bOv();
    }

    @C0064Am(aul = "ced4fdaf5bbb05de0f10eb66f7530d01", aum = 0)
    /* renamed from: gA */
    private boolean m20526gA(long j) {
        for (BillingTransaction drI : bOp()) {
            if (drI.drI() == j) {
                return false;
            }
        }
        return true;
    }

    @C0064Am(aul = "b87bc568b3b84ee60b6e614df366aea2", aum = 0)
    /* renamed from: k */
    private boolean m20529k(long j, long j2) {
        if (bOx() == null) {
            mo8358lY("Null owner when trying to create a billing transaction.");
            return false;
        } else if (bOx().mo5156Qw() == null) {
            mo8358lY("Null bank account for user when trying to create a billing transaction.");
            return false;
        } else if (!mo12673gB(j)) {
            mo8358lY("Invalid transaction id when trying to create a billing transaction.");
            return false;
        } else {
            BillingTransaction arr = (BillingTransaction) bFf().mo6865M(BillingTransaction.class);
            arr.mo11223a(this, j, j2);
            arr.mo11221NI();
            bOp().add(arr);
            return true;
        }
    }
}
