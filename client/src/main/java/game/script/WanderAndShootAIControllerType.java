package game.script;

import game.network.message.externalizable.aCE;
import game.script.ai.npc.AIControllerType;
import game.script.ai.npc.WanderAndShootAIController;
import logic.baa.*;
import logic.data.mbean.C3999xv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.K */
/* compiled from: a */
public class WanderAndShootAIControllerType extends AIControllerType implements C1616Xf {
    public static final C5663aRz _f_shootAngle = null;
    public static final C5663aRz _f_shootPredictMovimet = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dF */
    public static final C5663aRz f920dF = null;
    /* renamed from: dG */
    public static final C2491fm f921dG = null;
    /* renamed from: dH */
    public static final C2491fm f922dH = null;
    /* renamed from: dI */
    public static final C2491fm f923dI = null;
    /* renamed from: dJ */
    public static final C2491fm f924dJ = null;
    /* renamed from: dK */
    public static final C2491fm f925dK = null;
    /* renamed from: dL */
    public static final C2491fm f926dL = null;
    /* renamed from: dM */
    public static final C2491fm f927dM = null;
    /* renamed from: dN */
    public static final C2491fm f928dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "218e375f8f8ab6ce928fda8d053b3fe9", aum = 2)

    /* renamed from: dE */
    private static float f919dE;
    @C0064Am(aul = "ae4e0bbfd73bdc7d6a198dbc77874976", aum = 0)
    private static float shootAngle;
    @C0064Am(aul = "e02f27cab17049288f288ebeb8cb528f", aum = 1)
    private static boolean shootPredictMovimet;

    static {
        m6230V();
    }

    public WanderAndShootAIControllerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WanderAndShootAIControllerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6230V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIControllerType._m_fieldCount + 3;
        _m_methodCount = AIControllerType._m_methodCount + 8;
        int i = AIControllerType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(WanderAndShootAIControllerType.class, "ae4e0bbfd73bdc7d6a198dbc77874976", i);
        _f_shootAngle = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WanderAndShootAIControllerType.class, "e02f27cab17049288f288ebeb8cb528f", i2);
        _f_shootPredictMovimet = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WanderAndShootAIControllerType.class, "218e375f8f8ab6ce928fda8d053b3fe9", i3);
        f920dF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_fields, (Object[]) _m_fields);
        int i5 = AIControllerType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(WanderAndShootAIControllerType.class, "096d359fdcf0de967746e4fce48ffc67", i5);
        f921dG = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(WanderAndShootAIControllerType.class, "6b9da8d76bdd7708b68cafc764a93230", i6);
        f922dH = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(WanderAndShootAIControllerType.class, "b7f275dfd0ce97c55079729679a3e941", i7);
        f923dI = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(WanderAndShootAIControllerType.class, "5dc5ae62e35df9b6c7a7cb3ad30c53cc", i8);
        f924dJ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(WanderAndShootAIControllerType.class, "af54796ac010ec689303194097627f9f", i9);
        f925dK = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(WanderAndShootAIControllerType.class, "8d371ef36b2ba1281bf27b17778b4426", i10);
        f926dL = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(WanderAndShootAIControllerType.class, "7d78e36d1b1f6dff9ab8437daacae9a9", i11);
        f927dM = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(WanderAndShootAIControllerType.class, "01b8d0e9878e738587f6e42721fd8490", i12);
        f928dN = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WanderAndShootAIControllerType.class, C3999xv.class, _m_fields, _m_methods);
    }

    /* renamed from: aI */
    private float m6231aI() {
        return bFf().mo5608dq().mo3211m(_f_shootAngle);
    }

    /* renamed from: aJ */
    private boolean m6232aJ() {
        return bFf().mo5608dq().mo3201h(_f_shootPredictMovimet);
    }

    /* renamed from: aK */
    private float m6233aK() {
        return bFf().mo5608dq().mo3211m(f920dF);
    }

    /* renamed from: b */
    private void m6239b(boolean z) {
        bFf().mo5608dq().mo3153a(_f_shootPredictMovimet, z);
    }

    /* renamed from: i */
    private void m6241i(float f) {
        bFf().mo5608dq().mo3150a(_f_shootAngle, f);
    }

    /* renamed from: j */
    private void m6242j(float f) {
        bFf().mo5608dq().mo3150a(f920dF, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3999xv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIControllerType._m_methodCount) {
            case 0:
                return new Float(m6234aL());
            case 1:
                m6243k(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Boolean(m6235aN());
            case 3:
                m6240c(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return new Float(m6236aP());
            case 5:
                m6244m(((Float) args[0]).floatValue());
                return null;
            case 6:
                return m6237aR();
            case 7:
                return m6238aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Angle to start shooting (º)")
    /* renamed from: aM */
    public float mo3458aM() {
        switch (bFf().mo6893i(f921dG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f921dG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f921dG, new Object[0]));
                break;
        }
        return m6234aL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Predict target moviment")
    /* renamed from: aO */
    public boolean mo3459aO() {
        switch (bFf().mo6893i(f923dI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f923dI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f923dI, new Object[0]));
                break;
        }
        return m6235aN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wander - Factor beetween going ahead and turning\nMust be beetween 1 (full ahead) and 0 (lots of turns)")
    /* renamed from: aQ */
    public float mo3460aQ() {
        switch (bFf().mo6893i(f925dK)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f925dK, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f925dK, new Object[0]));
                break;
        }
        return m6236aP();
    }

    /* renamed from: aS */
    public WanderAndShootAIController mo3461aS() {
        switch (bFf().mo6893i(f927dM)) {
            case 0:
                return null;
            case 2:
                return (WanderAndShootAIController) bFf().mo5606d(new aCE(this, f927dM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f927dM, new Object[0]));
                break;
        }
        return m6237aR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f928dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f928dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f928dN, new Object[0]));
                break;
        }
        return m6238aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Predict target moviment")
    /* renamed from: d */
    public void mo3462d(boolean z) {
        switch (bFf().mo6893i(f924dJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f924dJ, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f924dJ, new Object[]{new Boolean(z)}));
                break;
        }
        m6240c(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Angle to start shooting (º)")
    /* renamed from: l */
    public void mo3463l(float f) {
        switch (bFf().mo6893i(f922dH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f922dH, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f922dH, new Object[]{new Float(f)}));
                break;
        }
        m6243k(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wander - Factor beetween going ahead and turning\nMust be beetween 1 (full ahead) and 0 (lots of turns)")
    /* renamed from: n */
    public void mo3464n(float f) {
        switch (bFf().mo6893i(f926dL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f926dL, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f926dL, new Object[]{new Float(f)}));
                break;
        }
        m6244m(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m6241i(10.0f);
        m6239b(false);
        m6242j(0.5f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Angle to start shooting (º)")
    @C0064Am(aul = "096d359fdcf0de967746e4fce48ffc67", aum = 0)
    /* renamed from: aL */
    private float m6234aL() {
        return m6231aI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Angle to start shooting (º)")
    @C0064Am(aul = "6b9da8d76bdd7708b68cafc764a93230", aum = 0)
    /* renamed from: k */
    private void m6243k(float f) {
        m6241i(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Predict target moviment")
    @C0064Am(aul = "b7f275dfd0ce97c55079729679a3e941", aum = 0)
    /* renamed from: aN */
    private boolean m6235aN() {
        return m6232aJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Predict target moviment")
    @C0064Am(aul = "5dc5ae62e35df9b6c7a7cb3ad30c53cc", aum = 0)
    /* renamed from: c */
    private void m6240c(boolean z) {
        m6239b(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wander - Factor beetween going ahead and turning\nMust be beetween 1 (full ahead) and 0 (lots of turns)")
    @C0064Am(aul = "af54796ac010ec689303194097627f9f", aum = 0)
    /* renamed from: aP */
    private float m6236aP() {
        return m6233aK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wander - Factor beetween going ahead and turning\nMust be beetween 1 (full ahead) and 0 (lots of turns)")
    @C0064Am(aul = "8d371ef36b2ba1281bf27b17778b4426", aum = 0)
    /* renamed from: m */
    private void m6244m(float f) {
        m6242j(f);
    }

    @C0064Am(aul = "7d78e36d1b1f6dff9ab8437daacae9a9", aum = 0)
    /* renamed from: aR */
    private WanderAndShootAIController m6237aR() {
        return (WanderAndShootAIController) mo745aU();
    }

    @C0064Am(aul = "01b8d0e9878e738587f6e42721fd8490", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6238aT() {
        T t = (WanderAndShootAIController) bFf().mo6865M(WanderAndShootAIController.class);
        t.mo19272a(this);
        return t;
    }
}
