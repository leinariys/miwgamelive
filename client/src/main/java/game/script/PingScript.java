package game.script;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aOE;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.rW */
/* compiled from: a */
public class PingScript extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bev = null;
    public static final C2491fm bew = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m38384V();
    }

    public PingScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PingScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38384V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 2;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(PingScript.class, "cfb39748f0fb165edaa0ffed6061b9b9", i);
        bev = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(PingScript.class, "65e779f7d39bf989fe2abff7d4129366", i2);
        bew = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PingScript.class, aOE.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "cfb39748f0fb165edaa0ffed6061b9b9", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: cN */
    private void m38385cN(long j) {
        throw new aWi(new aCE(this, bev, new Object[]{new Long(j)}));
    }

    /* renamed from: cQ */
    private void m38387cQ(long j) {
        switch (bFf().mo6893i(bew)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bew, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bew, new Object[]{new Long(j)}));
                break;
        }
        m38386cP(j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aOE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m38385cN(((Long) args[0]).longValue());
                return null;
            case 1:
                m38386cP(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: cO */
    public void mo21681cO(long j) {
        switch (bFf().mo6893i(bev)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bev, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bev, new Object[]{new Long(j)}));
                break;
        }
        m38385cN(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "65e779f7d39bf989fe2abff7d4129366", aum = 0)
    /* renamed from: cP */
    private void m38386cP(long j) {
        try {
            aPA().getUserConnection().bgt().setPing(j);
        } catch (NullPointerException e) {
            mo8359ma("NPE when trying to update ping for player: " + getPlayer());
        }
    }
}
