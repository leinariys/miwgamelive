package game.script.itemgen;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.Item;
import game.script.mission.Mission;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C6849aup;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import p001a.*;

import java.util.*;

@C5829abJ("1.10.1")
@C5511aMd
@C6485anp
/* renamed from: a.oq */
/* compiled from: a */
public class ItemGenTable extends aDJ implements C0468GU, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f8786Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aPo = null;
    public static final C5663aRz aPq = null;
    public static final C2491fm aPr = null;
    public static final C2491fm aPs = null;
    public static final C2491fm aPt = null;
    public static final C2491fm aPu = null;
    public static final C2491fm aPv = null;
    public static final C2491fm aPw = null;
    /* renamed from: bL */
    public static final C5663aRz f8788bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8789bM = null;
    /* renamed from: bN */
    public static final C2491fm f8790bN = null;
    /* renamed from: bO */
    public static final C2491fm f8791bO = null;
    /* renamed from: bP */
    public static final C2491fm f8792bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8793bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f3d354e2dfff439b3a043c2710d45773", aum = 0)
    private static int aPn;
    @C0064Am(aul = "30675382919dd52e3207dde59048b09a", aum = 1)
    private static C3438ra<ItemGenSet> aPp;
    @C0064Am(aul = "d41b0e53cc1803dbc511387492da9258", aum = 2)

    /* renamed from: bK */
    private static UUID f8787bK;
    @C0064Am(aul = "c21eaef056fa24da46cbc7c875a28c4d", aum = 3)
    private static String handle;

    static {
        m36876V();
    }

    public ItemGenTable() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemGenTable(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36876V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 11;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ItemGenTable.class, "f3d354e2dfff439b3a043c2710d45773", i);
        aPo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemGenTable.class, "30675382919dd52e3207dde59048b09a", i2);
        aPq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemGenTable.class, "d41b0e53cc1803dbc511387492da9258", i3);
        f8788bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemGenTable.class, "c21eaef056fa24da46cbc7c875a28c4d", i4);
        f8789bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 11)];
        C2491fm a = C4105zY.m41624a(ItemGenTable.class, "502c741a8ad390515f6398265ec9a161", i6);
        f8790bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemGenTable.class, "5576435647537093ac9caf7498a6b952", i7);
        f8791bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemGenTable.class, "2edb691dd09cebf319904676ac75c410", i8);
        f8792bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemGenTable.class, "eb4ff41aa2db499f43cbaaebf52e4b43", i9);
        f8793bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemGenTable.class, "cb08c7b4feb24d12dd8a6579ada2652c", i10);
        aPr = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemGenTable.class, "9d9ae0454a12090ff5605f25caaa4c86", i11);
        aPs = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemGenTable.class, "d017e0ef9deefc8928886f5ddce798cd", i12);
        aPt = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemGenTable.class, "8595f7ad946b3898ffc9e8591e34bdfa", i13);
        aPu = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemGenTable.class, "e0770100cfbe117acaa789d444f28d63", i14);
        aPv = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemGenTable.class, "c3db57e334b545822cf32a89326769cc", i15);
        aPw = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemGenTable.class, "3d989082be20c6bae4181d40265bbbca", i16);
        f8786Do = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemGenTable.class, C6849aup.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m36870B(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aPq, raVar);
    }

    /* renamed from: TS */
    private int m36872TS() {
        return bFf().mo5608dq().mo3212n(aPo);
    }

    /* renamed from: TT */
    private C3438ra m36873TT() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aPq);
    }

    /* renamed from: a */
    private void m36879a(String str) {
        bFf().mo5608dq().mo3197f(f8789bM, str);
    }

    /* renamed from: a */
    private void m36880a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8788bL, uuid);
    }

    /* renamed from: an */
    private UUID m36881an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8788bL);
    }

    /* renamed from: ao */
    private String m36882ao() {
        return (String) bFf().mo5608dq().mo3214p(f8789bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "eb4ff41aa2db499f43cbaaebf52e4b43", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m36885b(String str) {
        throw new aWi(new aCE(this, f8793bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m36888c(UUID uuid) {
        switch (bFf().mo6893i(f8791bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8791bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8791bO, new Object[]{uuid}));
                break;
        }
        m36886b(uuid);
    }

    /* renamed from: dx */
    private void m36889dx(int i) {
        bFf().mo5608dq().mo3183b(aPo, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Generation Multiplier")
    @C0064Am(aul = "9d9ae0454a12090ff5605f25caaa4c86", aum = 0)
    @C5566aOg
    /* renamed from: dy */
    private void m36890dy(int i) {
        throw new aWi(new aCE(this, aPs, new Object[]{new Integer(i)}));
    }

    /* renamed from: P */
    public Set<Item> mo21070P(Player aku) {
        switch (bFf().mo6893i(aPw)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, aPw, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, aPw, new Object[]{aku}));
                break;
        }
        return m36871O(aku);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Generation Multiplier")
    /* renamed from: TV */
    public int mo21071TV() {
        switch (bFf().mo6893i(aPr)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aPr, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPr, new Object[0]));
                break;
        }
        return m36874TU();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemGenSets")
    /* renamed from: TX */
    public List<ItemGenSet> mo21072TX() {
        switch (bFf().mo6893i(aPv)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aPv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPv, new Object[0]));
                break;
        }
        return m36875TW();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6849aup(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m36883ap();
            case 1:
                m36886b((UUID) args[0]);
                return null;
            case 2:
                return m36884ar();
            case 3:
                m36885b((String) args[0]);
                return null;
            case 4:
                return new Integer(m36874TU());
            case 5:
                m36890dy(((Integer) args[0]).intValue());
                return null;
            case 6:
                m36878a((ItemGenSet) args[0]);
                return null;
            case 7:
                m36887c((ItemGenSet) args[0]);
                return null;
            case 8:
                return m36875TW();
            case 9:
                return m36871O((Player) args[0]);
            case 10:
                m36877a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8790bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8790bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8790bN, new Object[0]));
                break;
        }
        return m36883ap();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8786Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8786Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8786Do, new Object[]{jt}));
                break;
        }
        m36877a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemGenSets")
    /* renamed from: b */
    public void mo21073b(ItemGenSet alq) {
        switch (bFf().mo6893i(aPt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPt, new Object[]{alq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPt, new Object[]{alq}));
                break;
        }
        m36878a(alq);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemGenSets")
    /* renamed from: d */
    public void mo21074d(ItemGenSet alq) {
        switch (bFf().mo6893i(aPu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPu, new Object[]{alq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPu, new Object[]{alq}));
                break;
        }
        m36887c(alq);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Generation Multiplier")
    @C5566aOg
    /* renamed from: dz */
    public void mo21075dz(int i) {
        switch (bFf().mo6893i(aPs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPs, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPs, new Object[]{new Integer(i)}));
                break;
        }
        m36890dy(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8792bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8792bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8792bP, new Object[0]));
                break;
        }
        return m36884ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8793bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8793bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8793bQ, new Object[]{str}));
                break;
        }
        m36885b(str);
    }

    @C0064Am(aul = "502c741a8ad390515f6398265ec9a161", aum = 0)
    /* renamed from: ap */
    private UUID m36883ap() {
        return m36881an();
    }

    @C0064Am(aul = "5576435647537093ac9caf7498a6b952", aum = 0)
    /* renamed from: b */
    private void m36886b(UUID uuid) {
        m36880a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "2edb691dd09cebf319904676ac75c410", aum = 0)
    /* renamed from: ar */
    private String m36884ar() {
        return m36882ao();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m36889dx(1);
        m36880a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Generation Multiplier")
    @C0064Am(aul = "cb08c7b4feb24d12dd8a6579ada2652c", aum = 0)
    /* renamed from: TU */
    private int m36874TU() {
        return m36872TS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemGenSets")
    @C0064Am(aul = "d017e0ef9deefc8928886f5ddce798cd", aum = 0)
    /* renamed from: a */
    private void m36878a(ItemGenSet alq) {
        m36873TT().add(alq);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemGenSets")
    @C0064Am(aul = "8595f7ad946b3898ffc9e8591e34bdfa", aum = 0)
    /* renamed from: c */
    private void m36887c(ItemGenSet alq) {
        m36873TT().remove(alq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemGenSets")
    @C0064Am(aul = "e0770100cfbe117acaa789d444f28d63", aum = 0)
    /* renamed from: TW */
    private List<ItemGenSet> m36875TW() {
        return Collections.unmodifiableList(m36873TT());
    }

    @C0064Am(aul = "c3db57e334b545822cf32a89326769cc", aum = 0)
    /* renamed from: O */
    private Set<Item> m36871O(Player aku) {
        Mission E;
        Set<Item> dgI = MessageContainer.dgI();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= mo21071TV()) {
                return dgI;
            }
            for (ItemGenSet alq : m36873TT()) {
                if (aku == null || !(alq instanceof MissionItemGenSet)) {
                    alq.mo9886c(dgI);
                } else {
                    MissionItemGenSet auf = (MissionItemGenSet) alq;
                    LDScriptingController bQG = aku.bQG();
                    if (!(bQG == null || (E = bQG.mo20250E(auf.bfA())) == null || !E.mo123fT(alq.getHandle()))) {
                        Set<Item> dgI2 = MessageContainer.dgI();
                        alq.mo9886c(dgI2);
                        for (Item auq : dgI2) {
                            E.mo63G(auq);
                            auq.mo16393gL(true);
                            E.mo118d(auq.bAP(), auq.mo302Iq());
                        }
                        dgI.addAll(dgI2);
                    }
                }
            }
            i = i2 + 1;
        }
    }

    @C0064Am(aul = "3d989082be20c6bae4181d40265bbbca", aum = 0)
    /* renamed from: a */
    private void m36877a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && m36872TS() == 0) {
            m36889dx(1);
        }
    }
}
