package game.script.itemgen;

import game.network.message.externalizable.aCE;
import game.script.item.BulkItemType;
import game.script.item.Item;
import game.script.item.ItemType;
import logic.baa.*;
import logic.data.mbean.C0292Dm;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;

import java.util.Collection;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.lW */
/* compiled from: a */
public class ItemGenEntry extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aAM = null;
    public static final C5663aRz aAO = null;
    public static final C5663aRz aAQ = null;
    public static final C2491fm aAR = null;
    public static final C2491fm aAS = null;
    public static final C2491fm aAT = null;
    public static final C2491fm aAU = null;
    public static final C2491fm aAV = null;
    public static final C2491fm aAW = null;
    public static final C2491fm aAX = null;
    public static final C2491fm aAY = null;
    public static final C2491fm aAZ = null;
    /* renamed from: bL */
    public static final C5663aRz f8537bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8538bM = null;
    /* renamed from: bN */
    public static final C2491fm f8539bN = null;
    /* renamed from: bO */
    public static final C2491fm f8540bO = null;
    /* renamed from: bP */
    public static final C2491fm f8541bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8542bQ = null;
    /* renamed from: cC */
    public static final C2491fm f8543cC = null;
    /* renamed from: cD */
    public static final C2491fm f8544cD = null;
    /* renamed from: cx */
    public static final C5663aRz f8546cx = null;
    public static final long serialVersionUID = 0;
    private static final Random RANDOM = new Random(System.currentTimeMillis());
    private static final double aAJ = 0.5d;
    private static final double aAK = 0.2d;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "5b530feefca46dde291da74f1bcd9112", aum = 0)
    private static int aAL;
    @C0064Am(aul = "e87fd72ea701cd8cfa204638235ecd62", aum = 1)
    private static int aAN;
    @C0064Am(aul = "641f551a4297e67e0f2b7ba8d8d3f265", aum = 2)
    private static float aAP;
    @C0064Am(aul = "5286b07fa07a9115184b90bb27152aa0", aum = 4)

    /* renamed from: bK */
    private static UUID f8536bK;
    @C0064Am(aul = "a4b441b6a72ddce1c29a62513a7a7a84", aum = 3)

    /* renamed from: cw */
    private static ItemType f8545cw;
    @C0064Am(aul = "a8043b98dd016a27186cb1937c42c00d", aum = 5)
    private static String handle;

    static {
        m34890V();
    }

    public ItemGenEntry() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemGenEntry(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34890V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 6;
        _m_methodCount = aDJ._m_methodCount + 16;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ItemGenEntry.class, "5b530feefca46dde291da74f1bcd9112", i);
        aAM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemGenEntry.class, "e87fd72ea701cd8cfa204638235ecd62", i2);
        aAO = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemGenEntry.class, "641f551a4297e67e0f2b7ba8d8d3f265", i3);
        aAQ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemGenEntry.class, "a4b441b6a72ddce1c29a62513a7a7a84", i4);
        f8546cx = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ItemGenEntry.class, "5286b07fa07a9115184b90bb27152aa0", i5);
        f8537bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ItemGenEntry.class, "a8043b98dd016a27186cb1937c42c00d", i6);
        f8538bM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i8 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 16)];
        C2491fm a = C4105zY.m41624a(ItemGenEntry.class, "182a3d2a9f485ada38dfc7c79e1abe54", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemGenEntry.class, "86e94d316b0876231a50de4c193d1676", i9);
        f8539bN = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemGenEntry.class, "4281540b002d1ec9a3767b05e23d5326", i10);
        f8540bO = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemGenEntry.class, "010b86c59c5598688cdf317779479527", i11);
        f8541bP = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemGenEntry.class, "ac14d75a2ccc42564c655ac6c7d65a37", i12);
        f8542bQ = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemGenEntry.class, "a717a58087287fb50bb1088620b40d76", i13);
        aAR = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemGenEntry.class, "c4a687b1ba015c5e29ab30469d77c476", i14);
        aAS = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemGenEntry.class, "3b63462da1222135be05c239fe341466", i15);
        aAT = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemGenEntry.class, "514e0aecdd120b1e4b1e1cb1dedbde75", i16);
        aAU = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemGenEntry.class, "5ca04b005e280a0608216ad04157d2ba", i17);
        aAV = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemGenEntry.class, "e27370b88dd1834f29a117b7a0cc95fd", i18);
        aAW = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemGenEntry.class, "eba95dee98e37eff3af43642f7e12a3c", i19);
        f8543cC = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ItemGenEntry.class, "2c421de87fd8219eeb1dce9cf3760b4f", i20);
        f8544cD = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(ItemGenEntry.class, "e4b515cc7201db92a178007447c00f44", i21);
        aAX = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(ItemGenEntry.class, "9097b122f130a4387a41f4fec8dd00b0", i22);
        aAY = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(ItemGenEntry.class, "f70a8b21435686649970a52e9829c1e1", i23);
        aAZ = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemGenEntry.class, C0292Dm.class, _m_fields, _m_methods);
    }

    /* renamed from: MJ */
    private int m34884MJ() {
        return bFf().mo5608dq().mo3212n(aAM);
    }

    /* renamed from: MK */
    private int m34885MK() {
        return bFf().mo5608dq().mo3212n(aAO);
    }

    /* renamed from: ML */
    private float m34886ML() {
        return bFf().mo5608dq().mo3211m(aAQ);
    }

    /* renamed from: a */
    private void m34892a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f8546cx, jCVar);
    }

    /* renamed from: a */
    private void m34893a(String str) {
        bFf().mo5608dq().mo3197f(f8538bM, str);
    }

    /* renamed from: a */
    private void m34894a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8537bL, uuid);
    }

    /* renamed from: an */
    private UUID m34895an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8537bL);
    }

    /* renamed from: ao */
    private String m34896ao() {
        return (String) bFf().mo5608dq().mo3214p(f8538bM);
    }

    /* renamed from: av */
    private ItemType m34900av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f8546cx);
    }

    /* renamed from: b */
    private double m34902b(double d, double d2) {
        switch (bFf().mo6893i(aAZ)) {
            case 0:
                return ScriptRuntime.NaN;
            case 2:
                return ((Double) bFf().mo5606d(new aCE(this, aAZ, new Object[]{new Double(d), new Double(d2)}))).doubleValue();
            case 3:
                bFf().mo5606d(new aCE(this, aAZ, new Object[]{new Double(d), new Double(d2)}));
                break;
        }
        return m34891a(d, d2);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "ac14d75a2ccc42564c655ac6c7d65a37", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m34904b(String str) {
        throw new aWi(new aCE(this, f8542bQ, new Object[]{str}));
    }

    /* renamed from: bd */
    private void m34907bd(float f) {
        bFf().mo5608dq().mo3150a(aAQ, f);
    }

    /* renamed from: c */
    private void m34909c(UUID uuid) {
        switch (bFf().mo6893i(f8540bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8540bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8540bO, new Object[]{uuid}));
                break;
        }
        m34906b(uuid);
    }

    /* renamed from: cN */
    private void m34910cN(int i) {
        bFf().mo5608dq().mo3183b(aAM, i);
    }

    /* renamed from: cO */
    private void m34911cO(int i) {
        bFf().mo5608dq().mo3183b(aAO, i);
    }

    /* renamed from: p */
    private int m34915p(int i, int i2) {
        switch (bFf().mo6893i(aAY)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aAY, new Object[]{new Integer(i), new Integer(i2)}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aAY, new Object[]{new Integer(i), new Integer(i2)}));
                break;
        }
        return m34914o(i, i2);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Quantity")
    /* renamed from: MN */
    public int mo20232MN() {
        switch (bFf().mo6893i(aAR)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aAR, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aAR, new Object[0]));
                break;
        }
        return m34887MM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Quantity")
    /* renamed from: MP */
    public int mo20233MP() {
        switch (bFf().mo6893i(aAT)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aAT, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aAT, new Object[0]));
                break;
        }
        return m34888MO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Percentage")
    /* renamed from: MR */
    public float mo20234MR() {
        switch (bFf().mo6893i(aAV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aAV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aAV, new Object[0]));
                break;
        }
        return m34889MQ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0292Dm(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m34899au();
            case 1:
                return m34897ap();
            case 2:
                m34906b((UUID) args[0]);
                return null;
            case 3:
                return m34898ar();
            case 4:
                m34904b((String) args[0]);
                return null;
            case 5:
                return new Integer(m34887MM());
            case 6:
                m34912cP(((Integer) args[0]).intValue());
                return null;
            case 7:
                return new Integer(m34888MO());
            case 8:
                m34913cR(((Integer) args[0]).intValue());
                return null;
            case 9:
                return new Float(m34889MQ());
            case 10:
                m34908be(((Float) args[0]).floatValue());
                return null;
            case 11:
                return m34901ay();
            case 12:
                m34903b((ItemType) args[0]);
                return null;
            case 13:
                m34905b((Set<Item>) (Set) args[0]);
                return null;
            case 14:
                return new Integer(m34914o(((Integer) args[0]).intValue(), ((Integer) args[1]).intValue()));
            case 15:
                return new Double(m34891a(((Double) args[0]).doubleValue(), ((Double) args[1]).doubleValue()));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8539bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8539bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8539bN, new Object[0]));
                break;
        }
        return m34897ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    /* renamed from: az */
    public ItemType mo20235az() {
        switch (bFf().mo6893i(f8543cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f8543cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8543cC, new Object[0]));
                break;
        }
        return m34901ay();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Percentage")
    /* renamed from: bf */
    public void mo20236bf(float f) {
        switch (bFf().mo6893i(aAW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAW, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAW, new Object[]{new Float(f)}));
                break;
        }
        m34908be(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ItemType")
    /* renamed from: c */
    public void mo20237c(ItemType jCVar) {
        switch (bFf().mo6893i(f8544cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8544cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8544cD, new Object[]{jCVar}));
                break;
        }
        m34903b(jCVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo20238c(Set<Item> set) {
        switch (bFf().mo6893i(aAX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAX, new Object[]{set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAX, new Object[]{set}));
                break;
        }
        m34905b(set);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Quantity")
    /* renamed from: cQ */
    public void mo20239cQ(int i) {
        switch (bFf().mo6893i(aAS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAS, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAS, new Object[]{new Integer(i)}));
                break;
        }
        m34912cP(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Quantity")
    /* renamed from: cS */
    public void mo20240cS(int i) {
        switch (bFf().mo6893i(aAU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAU, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAU, new Object[]{new Integer(i)}));
                break;
        }
        m34913cR(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8541bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8541bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8541bP, new Object[0]));
                break;
        }
        return m34898ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8542bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8542bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8542bQ, new Object[]{str}));
                break;
        }
        m34904b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m34899au();
    }

    @C0064Am(aul = "182a3d2a9f485ada38dfc7c79e1abe54", aum = 0)
    /* renamed from: au */
    private String m34899au() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("[");
        stringBuffer.append(m34900av() == null ? "null" : m34900av().getHandle());
        stringBuffer.append(", [");
        stringBuffer.append(m34884MJ());
        stringBuffer.append("..");
        stringBuffer.append(m34885MK());
        stringBuffer.append("], ");
        stringBuffer.append(m34886ML());
        stringBuffer.append("%");
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    @C0064Am(aul = "86e94d316b0876231a50de4c193d1676", aum = 0)
    /* renamed from: ap */
    private UUID m34897ap() {
        return m34895an();
    }

    @C0064Am(aul = "4281540b002d1ec9a3767b05e23d5326", aum = 0)
    /* renamed from: b */
    private void m34906b(UUID uuid) {
        m34894a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m34894a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "010b86c59c5598688cdf317779479527", aum = 0)
    /* renamed from: ar */
    private String m34898ar() {
        return m34896ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Quantity")
    @C0064Am(aul = "a717a58087287fb50bb1088620b40d76", aum = 0)
    /* renamed from: MM */
    private int m34887MM() {
        return m34884MJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Quantity")
    @C0064Am(aul = "c4a687b1ba015c5e29ab30469d77c476", aum = 0)
    /* renamed from: cP */
    private void m34912cP(int i) {
        m34910cN(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Quantity")
    @C0064Am(aul = "3b63462da1222135be05c239fe341466", aum = 0)
    /* renamed from: MO */
    private int m34888MO() {
        return m34885MK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Quantity")
    @C0064Am(aul = "514e0aecdd120b1e4b1e1cb1dedbde75", aum = 0)
    /* renamed from: cR */
    private void m34913cR(int i) {
        m34911cO(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Percentage")
    @C0064Am(aul = "5ca04b005e280a0608216ad04157d2ba", aum = 0)
    /* renamed from: MQ */
    private float m34889MQ() {
        return m34886ML();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Percentage")
    @C0064Am(aul = "e27370b88dd1834f29a117b7a0cc95fd", aum = 0)
    /* renamed from: be */
    private void m34908be(float f) {
        m34907bd(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    @C0064Am(aul = "eba95dee98e37eff3af43642f7e12a3c", aum = 0)
    /* renamed from: ay */
    private ItemType m34901ay() {
        return m34900av();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ItemType")
    @C0064Am(aul = "2c421de87fd8219eeb1dce9cf3760b4f", aum = 0)
    /* renamed from: b */
    private void m34903b(ItemType jCVar) {
        m34892a(jCVar);
    }

    @C0064Am(aul = "e4b515cc7201db92a178007447c00f44", aum = 0)
    /* renamed from: b */
    private void m34905b(Set<Item> set) {
        int p = m34915p(mo20233MP(), mo20232MN());
        if (p > 0) {
            if (m34900av() instanceof BulkItemType) {
                set.addAll(((BulkItemType) m34900av()).mo13881o(p));
                return;
            }
            for (int i = 0; i < p; i++) {
                set.add((Item) m34900av().mo7459NK());
            }
        }
    }

    @C0064Am(aul = "9097b122f130a4387a41f4fec8dd00b0", aum = 0)
    /* renamed from: o */
    private int m34914o(int i, int i2) {
        double d = ScriptRuntime.NaN;
        double b = m34902b(aAJ, aAK);
        if (b >= ScriptRuntime.NaN) {
            if (b > 1.0d) {
                d = 1.0d;
            } else {
                d = b;
            }
        }
        return (int) Math.round((d * ((double) (i - i2))) + ((double) i2));
    }

    @C0064Am(aul = "f70a8b21435686649970a52e9829c1e1", aum = 0)
    /* renamed from: a */
    private double m34891a(double d, double d2) {
        double nextDouble;
        double d3;
        do {
            nextDouble = (RANDOM.nextDouble() * 2.0d) - 1.0d;
            double nextDouble2 = (RANDOM.nextDouble() * 2.0d) - 1.0d;
            d3 = (nextDouble2 * nextDouble2) + (nextDouble * nextDouble);
        } while (d3 >= 1.0d);
        return (nextDouble * Math.sqrt((-2.0d * Math.log(d3)) / d3) * d2) + d;
    }
}
