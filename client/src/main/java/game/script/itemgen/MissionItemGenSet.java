package game.script.itemgen;

import game.network.message.externalizable.aCE;
import game.script.mission.MissionTemplate;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aVK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.auf  reason: case insensitive filesystem */
/* compiled from: a */
public class MissionItemGenSet extends ItemGenSet implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eRV = null;
    public static final C2491fm eRW = null;
    public static final C2491fm eRX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a5989efd6f51eff1b4ec7bbbeac57cc4", aum = 0)
    private static MissionTemplate baM;

    static {
        m26375V();
    }

    public MissionItemGenSet() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionItemGenSet(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26375V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemGenSet._m_fieldCount + 1;
        _m_methodCount = ItemGenSet._m_methodCount + 2;
        int i = ItemGenSet._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(MissionItemGenSet.class, "a5989efd6f51eff1b4ec7bbbeac57cc4", i);
        eRV = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemGenSet._m_fields, (Object[]) _m_fields);
        int i3 = ItemGenSet._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(MissionItemGenSet.class, "eddf0837ba1206c8bcccc6ff4bcc1e46", i3);
        eRW = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionItemGenSet.class, "0e68cd9cb614f335d9782f7410f57621", i4);
        eRX = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemGenSet._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionItemGenSet.class, aVK.class, _m_fields, _m_methods);
    }

    private MissionTemplate bJH() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(eRV);
    }

    /* renamed from: e */
    private void m26376e(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(eRV, avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Template")
    @C0064Am(aul = "0e68cd9cb614f335d9782f7410f57621", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m26377f(MissionTemplate avh) {
        throw new aWi(new aCE(this, eRX, new Object[]{avh}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aVK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemGenSet._m_methodCount) {
            case 0:
                return bJI();
            case 1:
                m26377f((MissionTemplate) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Template")
    public MissionTemplate bfA() {
        switch (bFf().mo6893i(eRW)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, eRW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eRW, new Object[0]));
                break;
        }
        return bJI();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Template")
    @C5566aOg
    /* renamed from: g */
    public void mo16367g(MissionTemplate avh) {
        switch (bFf().mo6893i(eRX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eRX, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eRX, new Object[]{avh}));
                break;
        }
        m26377f(avh);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Template")
    @C0064Am(aul = "eddf0837ba1206c8bcccc6ff4bcc1e46", aum = 0)
    private MissionTemplate bJI() {
        return bJH();
    }
}
