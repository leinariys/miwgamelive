package game.script.itemgen;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.item.Item;
import logic.baa.*;
import logic.data.mbean.C5389aHl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.aLQ */
/* compiled from: a */
public class ItemGenSet extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aAX = null;
    public static final C2491fm aJH = null;
    /* renamed from: bL */
    public static final C5663aRz f3308bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3309bM = null;
    /* renamed from: bN */
    public static final C2491fm f3310bN = null;
    /* renamed from: bO */
    public static final C2491fm f3311bO = null;
    /* renamed from: bP */
    public static final C2491fm f3312bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3313bQ = null;
    public static final C5663aRz inC = null;
    public static final C2491fm inD = null;
    public static final C2491fm inE = null;
    public static final C2491fm inF = null;
    public static final C2491fm inG = null;
    public static final long serialVersionUID = 0;
    private static final Random RANDOM = new Random(System.currentTimeMillis());
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a97d2123c0f7240bc232c149168377b1", aum = 2)

    /* renamed from: bK */
    private static UUID f3307bK;
    @C0064Am(aul = "e7832864a76c1941e4bcffa1de2903ad", aum = 1)
    private static C2686iZ<ItemGenEntry> hWz;
    @C0064Am(aul = "e9e6d1183c202bb2d390712b21ad9a85", aum = 0)
    private static String handle;

    static {
        m16156V();
    }

    public ItemGenSet() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemGenSet(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16156V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 11;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ItemGenSet.class, "e9e6d1183c202bb2d390712b21ad9a85", i);
        f3309bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemGenSet.class, "e7832864a76c1941e4bcffa1de2903ad", i2);
        inC = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemGenSet.class, "a97d2123c0f7240bc232c149168377b1", i3);
        f3308bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 11)];
        C2491fm a = C4105zY.m41624a(ItemGenSet.class, "d067fdb89707d61308a1430dfbdaf3a7", i5);
        f3310bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemGenSet.class, "dc5453b682c9b64ee97c017731dd7216", i6);
        f3311bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemGenSet.class, "3ea8b918e484dd196f99c7a3043e313c", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemGenSet.class, "879c31608bd07afddb36c1ce1478f1eb", i8);
        f3312bP = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemGenSet.class, "a99d868d40ebd04c3f59fb2a647e6e8d", i9);
        f3313bQ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemGenSet.class, "ba5c23633e589877e235f0f007c68262", i10);
        inD = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemGenSet.class, "d6ae4477e1e07769be3816aa25f52ba5", i11);
        inE = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemGenSet.class, "9d67616e86858f68b701fd41dcfdcc9f", i12);
        inF = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemGenSet.class, "2d9684e92770cb5bf86c594d40eed0db", i13);
        aAX = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemGenSet.class, "5c5e203f02c144748dbd50e1eb2da022", i14);
        aJH = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemGenSet.class, "edbabc9bda4a5fe473af8891717ef80d", i15);
        inG = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemGenSet.class, C5389aHl.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m16158a(String str) {
        bFf().mo5608dq().mo3197f(f3309bM, str);
    }

    /* renamed from: a */
    private void m16159a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3308bL, uuid);
    }

    /* renamed from: an */
    private UUID m16160an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3308bL);
    }

    /* renamed from: ao */
    private String m16161ao() {
        return (String) bFf().mo5608dq().mo3214p(f3309bM);
    }

    /* renamed from: ao */
    private void m16162ao(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(inC, iZVar);
    }

    /* renamed from: c */
    private void m16170c(UUID uuid) {
        switch (bFf().mo6893i(f3311bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3311bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3311bO, new Object[]{uuid}));
                break;
        }
        m16168b(uuid);
    }

    private C2686iZ diA() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(inC);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5389aHl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m16163ap();
            case 1:
                m16168b((UUID) args[0]);
                return null;
            case 2:
                return m16165au();
            case 3:
                return m16164ar();
            case 4:
                m16166b((String) args[0]);
                return null;
            case 5:
                m16157a((ItemGenEntry) args[0]);
                return null;
            case 6:
                m16169c((ItemGenEntry) args[0]);
                return null;
            case 7:
                return diB();
            case 8:
                m16167b((Set<Item>) (Set) args[0]);
                return null;
            case 9:
                return new Integer(m16155QD());
            case 10:
                return new Float(diD());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3310bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3310bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3310bN, new Object[0]));
                break;
        }
        return m16163ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemGenEntries")
    /* renamed from: b */
    public void mo9885b(ItemGenEntry lWVar) {
        switch (bFf().mo6893i(inD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, inD, new Object[]{lWVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, inD, new Object[]{lWVar}));
                break;
        }
        m16157a(lWVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo9886c(Set<Item> set) {
        switch (bFf().mo6893i(aAX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAX, new Object[]{set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAX, new Object[]{set}));
                break;
        }
        m16167b(set);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemGenEntries")
    /* renamed from: d */
    public void mo9887d(ItemGenEntry lWVar) {
        switch (bFf().mo6893i(inE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, inE, new Object[]{lWVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, inE, new Object[]{lWVar}));
                break;
        }
        m16169c(lWVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemGenEntries")
    public Set<ItemGenEntry> diC() {
        switch (bFf().mo6893i(inF)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, inF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, inF, new Object[0]));
                break;
        }
        return diB();
    }

    public float diE() {
        switch (bFf().mo6893i(inG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, inG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, inG, new Object[0]));
                break;
        }
        return diD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3312bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3312bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3312bP, new Object[0]));
                break;
        }
        return m16164ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3313bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3313bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3313bQ, new Object[]{str}));
                break;
        }
        m16166b(str);
    }

    public int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m16155QD();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m16165au();
    }

    @C0064Am(aul = "d067fdb89707d61308a1430dfbdaf3a7", aum = 0)
    /* renamed from: ap */
    private UUID m16163ap() {
        return m16160an();
    }

    @C0064Am(aul = "dc5453b682c9b64ee97c017731dd7216", aum = 0)
    /* renamed from: b */
    private void m16168b(UUID uuid) {
        m16159a(uuid);
    }

    @C0064Am(aul = "3ea8b918e484dd196f99c7a3043e313c", aum = 0)
    /* renamed from: au */
    private String m16165au() {
        return "[" + m16161ao() + "]";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        setHandle(C0468GU.dac);
        m16159a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "879c31608bd07afddb36c1ce1478f1eb", aum = 0)
    /* renamed from: ar */
    private String m16164ar() {
        return m16161ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "a99d868d40ebd04c3f59fb2a647e6e8d", aum = 0)
    /* renamed from: b */
    private void m16166b(String str) {
        m16158a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemGenEntries")
    @C0064Am(aul = "ba5c23633e589877e235f0f007c68262", aum = 0)
    /* renamed from: a */
    private void m16157a(ItemGenEntry lWVar) {
        diA().add(lWVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemGenEntries")
    @C0064Am(aul = "d6ae4477e1e07769be3816aa25f52ba5", aum = 0)
    /* renamed from: c */
    private void m16169c(ItemGenEntry lWVar) {
        diA().remove(lWVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemGenEntries")
    @C0064Am(aul = "9d67616e86858f68b701fd41dcfdcc9f", aum = 0)
    private Set<ItemGenEntry> diB() {
        return Collections.unmodifiableSet(diA());
    }

    @C0064Am(aul = "2d9684e92770cb5bf86c594d40eed0db", aum = 0)
    /* renamed from: b */
    private void m16167b(Set<Item> set) {
        double nextDouble = RANDOM.nextDouble() * 100.0d;
        Iterator it = diA().iterator();
        while (true) {
            double d = nextDouble;
            if (it.hasNext()) {
                ItemGenEntry lWVar = (ItemGenEntry) it.next();
                if (d <= ((double) lWVar.mo20234MR())) {
                    lWVar.mo20238c(set);
                    return;
                }
                nextDouble = d - ((double) lWVar.mo20234MR());
            } else {
                return;
            }
        }
    }

    @C0064Am(aul = "5c5e203f02c144748dbd50e1eb2da022", aum = 0)
    /* renamed from: QD */
    private int m16155QD() {
        return diA().size();
    }

    @C0064Am(aul = "edbabc9bda4a5fe473af8891717ef80d", aum = 0)
    private float diD() {
        float f = 0.0f;
        Iterator it = diA().iterator();
        while (true) {
            float f2 = f;
            if (!it.hasNext()) {
                return f2;
            }
            f = ((ItemGenEntry) it.next()).mo20234MR() + f2;
        }
    }
}
