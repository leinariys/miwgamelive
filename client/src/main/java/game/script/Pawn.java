package game.script;

import game.network.message.externalizable.aCE;
import game.script.aggro.AggroTable;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.link.C0471GX;
import logic.data.mbean.aLU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;

@C5511aMd
@C6485anp
/* renamed from: a.avi  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class Pawn extends Actor implements C1616Xf, C2046bW {

    /* renamed from: Mq */
    public static final C2491fm f5453Mq = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bff = null;
    public static final C2491fm bpJ = null;
    public static final C2491fm bqI = null;
    public static final C2491fm bqL = null;
    public static final C2491fm bqM = null;
    public static final C2491fm coN = null;
    public static final C5663aRz gGi = null;
    public static final C5663aRz gGk = null;
    public static final C5663aRz gGm = null;
    public static final C2491fm gGn = null;
    public static final C2491fm gGo = null;
    public static final C2491fm gGp = null;
    public static final C2491fm gGq = null;
    public static final C2491fm gGr = null;
    public static final C2491fm gGs = null;
    public static final C2491fm gGt = null;
    public static final C2491fm gGu = null;
    public static final C2491fm gGv = null;
    public static final C2491fm gGw = null;
    public static final C2491fm gGx = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uZ */
    public static final C2491fm f5454uZ = null;
    /* renamed from: vk */
    public static final C2491fm f5455vk = null;
    /* renamed from: zr */
    public static final C2491fm f5456zr = null;
    private static final Log logger = LogPrinter.setClass(Pawn.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ec9e899e49495cc9bd643c0c1e8d8276", aum = 0)
    private static boolean gGh;
    @C0064Am(aul = "9247267203fe61987eaa761b44215e88", aum = 1)
    private static long gGj;
    @C0064Am(aul = "43a45c1f13c65ffc35ad4160a8103338", aum = 2)
    private static AggroTable gGl;

    static {
        m26649V();
    }

    public Pawn() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Pawn(C5540aNg ang) {
        super(ang);
    }

    public Pawn(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m26649V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 3;
        _m_methodCount = Actor._m_methodCount + 22;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Pawn.class, "ec9e899e49495cc9bd643c0c1e8d8276", i);
        gGi = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Pawn.class, "9247267203fe61987eaa761b44215e88", i2);
        gGk = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Pawn.class, "43a45c1f13c65ffc35ad4160a8103338", i3);
        gGm = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i5 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 22)];
        C2491fm a = C4105zY.m41624a(Pawn.class, "185e73a03c94ba34ee630ec62981d84e", i5);
        gGn = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Pawn.class, "9c1d6bbcd7b8a2aa6f629b93fdc86507", i6);
        bpJ = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Pawn.class, "7a260ad83f94a9d770b12b201c175cdb", i7);
        gGo = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Pawn.class, "474d8f75516ce53f8b1d3e44e56fea1a", i8);
        bff = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Pawn.class, "9e3ec085652de2b873aa59bed50e5d7a", i9);
        coN = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Pawn.class, "85a3fa209a41846712f2b396fb899690", i10);
        gGp = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Pawn.class, "377b473a354c3aac859fb9f26696eb77", i11);
        bqI = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(Pawn.class, "c8aacd94775c93f483a817650af576b4", i12);
        bqL = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(Pawn.class, "abdffab5c3d934b4a01110dbc04b3b28", i13);
        f5454uZ = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(Pawn.class, "4cfa56f814ca28116a465d0755c95ba4", i14);
        gGq = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(Pawn.class, "a519bbf30198c05337fbceb34ad1f6d7", i15);
        gGr = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(Pawn.class, "653db2a4b6b1d1d7117bf8f7d8a75a3a", i16);
        f5455vk = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(Pawn.class, C0471GX.dad, i17);
        gGs = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(Pawn.class, "d537a008e6bf91ccda989da306c3bb55", i18);
        gGt = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(Pawn.class, "a65878b85d460350103ea1b0d2e867cd", i19);
        f5453Mq = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(Pawn.class, "c38159988607cb5090a0b35e3159dd4c", i20);
        _f_dispose_0020_0028_0029V = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(Pawn.class, "68ab34199658ca17a6d7a8c7715a8ff7", i21);
        gGu = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        C2491fm a18 = C4105zY.m41624a(Pawn.class, "fb089ed0949ba2a2b86777c1e5c06584", i22);
        gGv = a18;
        fmVarArr[i22] = a18;
        int i23 = i22 + 1;
        C2491fm a19 = C4105zY.m41624a(Pawn.class, "fe2b6824ba7b78780a5ca1bdac6401e6", i23);
        gGw = a19;
        fmVarArr[i23] = a19;
        int i24 = i23 + 1;
        C2491fm a20 = C4105zY.m41624a(Pawn.class, "1a82fe4661c91de7333d7b52d9246b09", i24);
        f5456zr = a20;
        fmVarArr[i24] = a20;
        int i25 = i24 + 1;
        C2491fm a21 = C4105zY.m41624a(Pawn.class, "79a4c1e20d12b075967ba764c0b90a25", i25);
        gGx = a21;
        fmVarArr[i25] = a21;
        int i26 = i25 + 1;
        C2491fm a22 = C4105zY.m41624a(Pawn.class, "aa8f5651afbfc56dccc9c3e8e6cf77e8", i26);
        bqM = a22;
        fmVarArr[i26] = a22;
        int i27 = i26 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Pawn.class, aLU.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "377b473a354c3aac859fb9f26696eb77", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m26653a(C5260aCm acm, Pawn avi) {
        throw new aWi(new aCE(this, bqI, new Object[]{acm, avi}));
    }

    @C0064Am(aul = "9c1d6bbcd7b8a2aa6f629b93fdc86507", aum = 0)
    private Controller afJ() {
        throw new aWi(new aCE(this, bpJ, new Object[0]));
    }

    @C0064Am(aul = "185e73a03c94ba34ee630ec62981d84e", aum = 0)
    @C6580apg
    /* renamed from: bb */
    private void m26654bb(Actor cr) {
        bFf().mo5600a((Class<? extends C4062yl>) C0471GX.C0472a.class, (C0495Gr) new aCE(this, gGn, new Object[]{cr}));
    }

    @C0064Am(aul = "c8aacd94775c93f483a817650af576b4", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m26655c(C5260aCm acm, Pawn avi) {
        throw new aWi(new aCE(this, bqL, new Object[]{acm, avi}));
    }

    /* renamed from: c */
    private void m26656c(AggroTable dqVar) {
        bFf().mo5608dq().mo3197f(gGm, dqVar);
    }

    private boolean cyF() {
        return bFf().mo5608dq().mo3201h(gGi);
    }

    private long cyG() {
        return bFf().mo5608dq().mo3213o(gGk);
    }

    private AggroTable cyH() {
        return (AggroTable) bFf().mo5608dq().mo3214p(gGm);
    }

    /* renamed from: gR */
    private void m26658gR(boolean z) {
        bFf().mo5608dq().mo3153a(gGi, z);
    }

    /* renamed from: iX */
    private void m26661iX(long j) {
        bFf().mo5608dq().mo3184b(gGk, j);
    }

    /* renamed from: m */
    private void m26663m(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C0471GX.C0472a) it.next()).mo2353b(this, (Actor) gr.getArgs()[0]);
        }
    }

    @C4034yP
    @C2499fr(mo18855qf = {"b4248dc0637bb4cbfc146f6209da6d2d"})
    @ClientOnly
    /* renamed from: r */
    private void m26665r(float f, float f2, float f3, float f4) {
        switch (bFf().mo6893i(gGr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGr, new Object[]{new Float(f), new Float(f2), new Float(f3), new Float(f4)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGr, new Object[]{new Float(f), new Float(f2), new Float(f3), new Float(f4)}));
                break;
        }
        m26664q(f, f2, f3, f4);
    }

    @C0064Am(aul = "68ab34199658ca17a6d7a8c7715a8ff7", aum = 0)
    @C5566aOg
    /* renamed from: s */
    private void m26666s(float f, float f2, float f3, float f4) {
        throw new aWi(new aCE(this, gGu, new Object[]{new Float(f), new Float(f2), new Float(f3), new Float(f4)}));
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: v */
    private void m26668v(float f, float f2, float f3, float f4) {
        switch (bFf().mo6893i(gGw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGw, new Object[]{new Float(f), new Float(f2), new Float(f3), new Float(f4)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGw, new Object[]{new Float(f), new Float(f2), new Float(f3), new Float(f4)}));
                break;
        }
        m26667u(f, f2, f3, f4);
    }

    /* renamed from: Fe */
    public void mo957Fe() {
        switch (bFf().mo6893i(bqM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                break;
        }
        ahp();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aLU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                m26654bb((Actor) args[0]);
                return null;
            case 1:
                return afJ();
            case 2:
                return new Long(cyI());
            case 3:
                return new Integer(aaL());
            case 4:
                return new Boolean(azS());
            case 5:
                return new Boolean(cyK());
            case 6:
                m26653a((C5260aCm) args[0], (Pawn) args[1]);
                return null;
            case 7:
                m26655c((C5260aCm) args[0], (Pawn) args[1]);
                return null;
            case 8:
                m26650a(((Long) args[0]).longValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 9:
                return cyL();
            case 10:
                m26664q(((Float) args[0]).floatValue(), ((Float) args[1]).floatValue(), ((Float) args[2]).floatValue(), ((Float) args[3]).floatValue());
                return null;
            case 11:
                return new Boolean(m26662iv());
            case 12:
                return new Boolean(m26660i((C1722ZT) args[0]));
            case 13:
                m26659gS(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                m26652a((StellarSystem) args[0]);
                return null;
            case 15:
                m26657fg();
                return null;
            case 16:
                m26666s(((Float) args[0]).floatValue(), ((Float) args[1]).floatValue(), ((Float) args[2]).floatValue(), ((Float) args[3]).floatValue());
                return null;
            case 17:
                cyM();
                return null;
            case 18:
                m26667u(((Float) args[0]).floatValue(), ((Float) args[1]).floatValue(), ((Float) args[2]).floatValue(), ((Float) args[3]).floatValue());
                return null;
            case 19:
                m26651a((Actor) args[0], ((Float) args[1]).floatValue());
                return null;
            case 20:
                return cyO();
            case 21:
                ahp();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                m26663m(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    public int aaM() {
        switch (bFf().mo6893i(bff)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bff, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bff, new Object[0]));
                break;
        }
        return aaL();
    }

    @C5307aEh
    /* renamed from: b */
    public void mo991b(long j, boolean z) {
        switch (bFf().mo6893i(f5454uZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5454uZ, new Object[]{new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5454uZ, new Object[]{new Long(j), new Boolean(z)}));
                break;
        }
        m26650a(j, z);
    }

    /* renamed from: b */
    public void mo16605b(Actor cr, float f) {
        switch (bFf().mo6893i(f5456zr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5456zr, new Object[]{cr, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5456zr, new Object[]{cr, new Float(f)}));
                break;
        }
        m26651a(cr, f);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f5453Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5453Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5453Mq, new Object[]{jj}));
                break;
        }
        m26652a(jj);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public void mo16606b(C5260aCm acm, Pawn avi) {
        switch (bFf().mo6893i(bqI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqI, new Object[]{acm, avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqI, new Object[]{acm, avi}));
                break;
        }
        m26653a(acm, avi);
    }

    public Pawn bJk() {
        switch (bFf().mo6893i(gGq)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, gGq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gGq, new Object[0]));
                break;
        }
        return cyL();
    }

    /* access modifiers changed from: protected */
    @C6580apg
    /* renamed from: bc */
    public void mo16608bc(Actor cr) {
        switch (bFf().mo6893i(gGn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGn, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGn, new Object[]{cr}));
                break;
        }
        m26654bb(cr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public long cyJ() {
        switch (bFf().mo6893i(gGo)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gGo, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gGo, new Object[0]));
                break;
        }
        return cyI();
    }

    @C1253SX
    public void cyN() {
        switch (bFf().mo6893i(gGv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGv, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGv, new Object[0]));
                break;
        }
        cyM();
    }

    public AggroTable cyP() {
        switch (bFf().mo6893i(gGx)) {
            case 0:
                return null;
            case 2:
                return (AggroTable) bFf().mo5606d(new aCE(this, gGx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gGx, new Object[0]));
                break;
        }
        return cyO();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: d */
    public void mo16612d(C5260aCm acm, Pawn avi) {
        switch (bFf().mo6893i(bqL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqL, new Object[]{acm, avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqL, new Object[]{acm, avi}));
                break;
        }
        m26655c(acm, avi);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m26657fg();
    }

    /* renamed from: ge */
    public void mo16613ge(boolean z) {
        switch (bFf().mo6893i(gGt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGt, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGt, new Object[]{new Boolean(z)}));
                break;
        }
        m26659gS(z);
    }

    /* renamed from: hb */
    public Controller mo2998hb() {
        switch (bFf().mo6893i(bpJ)) {
            case 0:
                return null;
            case 2:
                return (Controller) bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
                break;
        }
        return afJ();
    }

    public boolean isAlive() {
        switch (bFf().mo6893i(coN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coN, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coN, new Object[0]));
                break;
        }
        return azS();
    }

    public boolean isDead() {
        switch (bFf().mo6893i(gGp)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gGp, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gGp, new Object[0]));
                break;
        }
        return cyK();
    }

    /* renamed from: iw */
    public boolean mo1080iw() {
        switch (bFf().mo6893i(f5455vk)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5455vk, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5455vk, new Object[0]));
                break;
        }
        return m26662iv();
    }

    @C5472aKq
    /* renamed from: j */
    public boolean mo16616j(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(gGs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gGs, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gGs, new Object[]{zt}));
                break;
        }
        return m26660i(zt);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: t */
    public void mo16617t(float f, float f2, float f3, float f4) {
        switch (bFf().mo6893i(gGu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGu, new Object[]{new Float(f), new Float(f2), new Float(f3), new Float(f4)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGu, new Object[]{new Float(f), new Float(f2), new Float(f3), new Float(f4)}));
                break;
        }
        m26666s(f, f2, f3, f4);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    @C0064Am(aul = "7a260ad83f94a9d770b12b201c175cdb", aum = 0)
    private long cyI() {
        return cyG();
    }

    @C0064Am(aul = "474d8f75516ce53f8b1d3e44e56fea1a", aum = 0)
    private int aaL() {
        return Math.round(mo1090qZ().length());
    }

    @C0064Am(aul = "9e3ec085652de2b873aa59bed50e5d7a", aum = 0)
    private boolean azS() {
        return !cyF();
    }

    @C0064Am(aul = "85a3fa209a41846712f2b396fb899690", aum = 0)
    private boolean cyK() {
        return cyF();
    }

    @C0064Am(aul = "abdffab5c3d934b4a01110dbc04b3b28", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m26650a(long j, boolean z) {
        super.mo991b(j, z);
        if (!cLZ()) {
            mo8358lY("Sending Simulation data Update for a Pawn (" + getName() + ") that has no simulated");
        } else if (bGX() && mo2998hb() == getPlayer().dxc()) {
            mo2998hb().mo3291d(j, z);
        }
    }

    @C0064Am(aul = "4cfa56f814ca28116a465d0755c95ba4", aum = 0)
    private Pawn cyL() {
        return this;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "a519bbf30198c05337fbceb34ad1f6d7", aum = 0)
    @C2499fr(mo18855qf = {"b4248dc0637bb4cbfc146f6209da6d2d"})
    /* renamed from: q */
    private void m26664q(float f, float f2, float f3, float f4) {
        if (!C1298TD.m9830t(this).bFT() && cLZ()) {
            m26668v(f, f2, f3, f4);
        }
    }

    @C0064Am(aul = "653db2a4b6b1d1d7117bf8f7d8a75a3a", aum = 0)
    /* renamed from: iv */
    private boolean m26662iv() {
        if (cMo()) {
            return false;
        }
        boolean iw = super.mo1080iw();
        if (!iw) {
            logger.warn("Not added to simulation because Simulated was not added");
            return iw;
        } else if (bJk().isStatic()) {
            return iw;
        } else {
            cMq().mo2560as(bJk().mo963VH());
            cMq().mo2561at(bJk().mo962VF());
            cMq().mo2563av(bJk().mo1091ra());
            cMq().mo2562au(bJk().mo1092rb());
            return iw;
        }
    }

    @C0064Am(aul = "7d1e2837f0ce17fa04881fe33acee534", aum = 0)
    @C5472aKq
    /* renamed from: i */
    private boolean m26660i(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        }
        Player dL = zt.bFq().mo15388dL();
        if (dL == null) {
            return false;
        }
        return dL.dxc() != mo2998hb();
    }

    @C0064Am(aul = "d537a008e6bf91ccda989da306c3bb55", aum = 0)
    /* renamed from: gS */
    private void m26659gS(boolean z) {
        if (z) {
            m26661iX(cVr());
        }
        m26658gR(z);
    }

    @C0064Am(aul = "a65878b85d460350103ea1b0d2e867cd", aum = 0)
    /* renamed from: a */
    private void m26652a(StellarSystem jj) {
        if (isDead()) {
            //Невозможно создать мертвую пешку ("+ getClass (). GetSimpleName () +")
            throw new RuntimeException("Can't spawn a dead Pawn (" + getClass().getSimpleName() + ") " + bFY());
        }
        super.mo993b(jj);
    }

    @C0064Am(aul = "c38159988607cb5090a0b35e3159dd4c", aum = 0)
    /* renamed from: fg */
    private void m26657fg() {
        if (cyH() != null) {
            cyH().dispose();
        }
        super.dispose();
    }

    @C0064Am(aul = "fb089ed0949ba2a2b86777c1e5c06584", aum = 0)
    @C1253SX
    private void cyM() {
        m26668v(mo963VH(), mo962VF(), mo1091ra(), mo1092rb());
        Controller hb = mo2998hb();
        if (hb != null) {
            hb.aYo();
        }
    }

    @C4034yP
    @C0064Am(aul = "fe2b6824ba7b78780a5ca1bdac6401e6", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: u */
    private void m26667u(float f, float f2, float f3, float f4) {
        if (!cLZ() || bJk().isStatic()) {
            //Pawn никогда не должен вызывать этот метод, когда он статичен или не моделируется
            mo6317hy("Pawn should never call this method while static or not being simulated");
            return;
        }
        cMq().mo2560as(f);
        cMq().mo2561at(f2);
        cMq().mo2563av(f3);
        cMq().mo2562au(f4);
    }

    @C0064Am(aul = "1a82fe4661c91de7333d7b52d9246b09", aum = 0)
    /* renamed from: a */
    private void m26651a(Actor cr, float f) {
    }

    @C0064Am(aul = "79a4c1e20d12b075967ba764c0b90a25", aum = 0)
    private AggroTable cyO() {
        if (cyH() == null && bHa()) {
            AggroTable dqVar = (AggroTable) bFf().mo6865M(AggroTable.class);
            dqVar.mo17924b(this);
            m26656c(dqVar);
        }
        return cyH();
    }

    @C0064Am(aul = "aa8f5651afbfc56dccc9c3e8e6cf77e8", aum = 0)
    private void ahp() {
        super.mo957Fe();
        if (mo2998hb() == ald().alb()) {
            ald().getEventManager().mo13975h(new C2640ht(cHg()));
        }
    }
}
