package game.script.missiontemplate;

import game.network.message.externalizable.aCE;
import game.script.mission.TableNPCSpawn;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C6069afp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;

@C5829abJ("1.1.0")
@C5511aMd
@C6485anp
/* renamed from: a.HT */
/* compiled from: a */
public class ReconWaypointDat extends WaypointDat implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f663Do = null;
    /* renamed from: QW */
    public static final C5663aRz f665QW = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ddI = null;
    public static final C5663aRz ddK = null;
    public static final C5663aRz ddM = null;
    public static final C2491fm ddN = null;
    public static final C2491fm ddO = null;
    public static final C2491fm ddP = null;
    public static final C2491fm ddQ = null;
    public static final C2491fm ddR = null;
    public static final C2491fm ddS = null;
    public static final C2491fm ddT = null;
    public static final C2491fm ddU = null;
    public static final C2491fm ddV = null;
    public static final C2491fm ddW = null;
    public static final C2491fm ddX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "842c1410a91b25f67e43d1c852875c24", aum = 0)

    /* renamed from: QV */
    private static float f664QV;
    @C0064Am(aul = "e35ae85fc5f0e00dc04f3b922b2112fc", aum = 1)
    private static int ddH;
    @C0064Am(aul = "784ad1e8ea2b5f99b69f037800499550", aum = 2)
    private static TableNPCSpawn ddJ;
    @C0064Am(aul = "7fe7012a4580b9a033a0db5781c7abf0", aum = 3)
    private static int ddL;

    static {
        m5160V();
    }

    public ReconWaypointDat() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ReconWaypointDat(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5160V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = WaypointDat._m_fieldCount + 4;
        _m_methodCount = WaypointDat._m_methodCount + 12;
        int i = WaypointDat._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ReconWaypointDat.class, "842c1410a91b25f67e43d1c852875c24", i);
        f665QW = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ReconWaypointDat.class, "e35ae85fc5f0e00dc04f3b922b2112fc", i2);
        ddI = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ReconWaypointDat.class, "784ad1e8ea2b5f99b69f037800499550", i3);
        ddK = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ReconWaypointDat.class, "7fe7012a4580b9a033a0db5781c7abf0", i4);
        ddM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) WaypointDat._m_fields, (Object[]) _m_fields);
        int i6 = WaypointDat._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 12)];
        C2491fm a = C4105zY.m41624a(ReconWaypointDat.class, "8db10b06007656f3462bd4ef161a6bc2", i6);
        ddN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ReconWaypointDat.class, "0b4cb4ccbe3a6f38879f7f7ce0624dde", i7);
        ddO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ReconWaypointDat.class, "ce861f8627156d125bcf8a776d459a48", i8);
        ddP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ReconWaypointDat.class, "3224967f90ab5041b6cecfbc4384f4b6", i9);
        ddQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ReconWaypointDat.class, "920ad84153f851af8d58861cc56757d6", i10);
        ddR = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ReconWaypointDat.class, "fbfdfbbf70e5fb0a1f333e71b651c26d", i11);
        ddS = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ReconWaypointDat.class, "f3e469d5d20c1dca221dbadf05677907", i12);
        ddT = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ReconWaypointDat.class, "5ab1ea58ab6494e22650702d7c15392b", i13);
        ddU = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ReconWaypointDat.class, "de3cbef6f86d20b51a65646ee1c9c7df", i14);
        ddV = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ReconWaypointDat.class, "07d20efa94e484b95dcae58156cb371d", i15);
        ddW = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(ReconWaypointDat.class, "c6577296dd73f6718320b1f24f717417", i16);
        ddX = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(ReconWaypointDat.class, "9c91add4895bb67a890636e464d42956", i17);
        f663Do = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) WaypointDat._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ReconWaypointDat.class, C6069afp.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m5162a(TableNPCSpawn aoj) {
        bFf().mo5608dq().mo3197f(ddK, aoj);
    }

    private int aVk() {
        return bFf().mo5608dq().mo3212n(ddI);
    }

    private TableNPCSpawn aVl() {
        return (TableNPCSpawn) bFf().mo5608dq().mo3214p(ddK);
    }

    private int aVm() {
        return bFf().mo5608dq().mo3212n(ddM);
    }

    @C0064Am(aul = "f3e469d5d20c1dca221dbadf05677907", aum = 0)
    @C5566aOg
    private NPCType aVt() {
        throw new aWi(new aCE(this, ddT, new Object[0]));
    }

    @C0064Am(aul = "5ab1ea58ab6494e22650702d7c15392b", aum = 0)
    @C5566aOg
    private List<NPCType> aVv() {
        throw new aWi(new aCE(this, ddU, new Object[0]));
    }

    /* renamed from: ad */
    private void m5163ad(float f) {
        bFf().mo5608dq().mo3150a(f665QW, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Selectable TableNPCSpawn")
    @C0064Am(aul = "fbfdfbbf70e5fb0a1f333e71b651c26d", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m5164b(TableNPCSpawn aoj) {
        throw new aWi(new aCE(this, ddS, new Object[]{aoj}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Selection Distance")
    @C0064Am(aul = "0b4cb4ccbe3a6f38879f7f7ce0624dde", aum = 0)
    @C5566aOg
    /* renamed from: fw */
    private void m5165fw(float f) {
        throw new aWi(new aCE(this, ddO, new Object[]{new Float(f)}));
    }

    /* renamed from: kB */
    private void m5166kB(int i) {
        bFf().mo5608dq().mo3183b(ddI, i);
    }

    /* renamed from: kC */
    private void m5167kC(int i) {
        bFf().mo5608dq().mo3183b(ddM, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Selectable Amount")
    @C0064Am(aul = "3224967f90ab5041b6cecfbc4384f4b6", aum = 0)
    @C5566aOg
    /* renamed from: kD */
    private void m5168kD(int i) {
        throw new aWi(new aCE(this, ddQ, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recon Amount")
    @C0064Am(aul = "07d20efa94e484b95dcae58156cb371d", aum = 0)
    @C5566aOg
    /* renamed from: kF */
    private void m5169kF(int i) {
        throw new aWi(new aCE(this, ddW, new Object[]{new Integer(i)}));
    }

    /* renamed from: we */
    private float m5170we() {
        return bFf().mo5608dq().mo3211m(f665QW);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6069afp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - WaypointDat._m_methodCount) {
            case 0:
                return new Float(aVn());
            case 1:
                m5165fw(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Integer(aVp());
            case 3:
                m5168kD(((Integer) args[0]).intValue());
                return null;
            case 4:
                return aVr();
            case 5:
                m5164b((TableNPCSpawn) args[0]);
                return null;
            case 6:
                return aVt();
            case 7:
                return aVv();
            case 8:
                return new Integer(aVx());
            case 9:
                m5169kF(((Integer) args[0]).intValue());
                return null;
            case 10:
                return new Boolean(aVz());
            case 11:
                m5161a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aVA() {
        switch (bFf().mo6893i(ddX)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ddX, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ddX, new Object[0]));
                break;
        }
        return aVz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Selection Distance")
    public float aVo() {
        switch (bFf().mo6893i(ddN)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ddN, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ddN, new Object[0]));
                break;
        }
        return aVn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Selectable Amount")
    public int aVq() {
        switch (bFf().mo6893i(ddP)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ddP, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ddP, new Object[0]));
                break;
        }
        return aVp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Selectable TableNPCSpawn")
    public TableNPCSpawn aVs() {
        switch (bFf().mo6893i(ddR)) {
            case 0:
                return null;
            case 2:
                return (TableNPCSpawn) bFf().mo5606d(new aCE(this, ddR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ddR, new Object[0]));
                break;
        }
        return aVr();
    }

    @C5566aOg
    public NPCType aVu() {
        switch (bFf().mo6893i(ddT)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, ddT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ddT, new Object[0]));
                break;
        }
        return aVt();
    }

    @C5566aOg
    public List<NPCType> aVw() {
        switch (bFf().mo6893i(ddU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, ddU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ddU, new Object[0]));
                break;
        }
        return aVv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recon Amount")
    public int aVy() {
        switch (bFf().mo6893i(ddV)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ddV, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ddV, new Object[0]));
                break;
        }
        return aVx();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f663Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f663Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f663Do, new Object[]{jt}));
                break;
        }
        m5161a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Selectable TableNPCSpawn")
    @C5566aOg
    /* renamed from: c */
    public void mo2622c(TableNPCSpawn aoj) {
        switch (bFf().mo6893i(ddS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ddS, new Object[]{aoj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ddS, new Object[]{aoj}));
                break;
        }
        m5164b(aoj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Selection Distance")
    @C5566aOg
    /* renamed from: fx */
    public void mo2623fx(float f) {
        switch (bFf().mo6893i(ddO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ddO, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ddO, new Object[]{new Float(f)}));
                break;
        }
        m5165fw(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Selectable Amount")
    @C5566aOg
    /* renamed from: kE */
    public void mo2624kE(int i) {
        switch (bFf().mo6893i(ddQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ddQ, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ddQ, new Object[]{new Integer(i)}));
                break;
        }
        m5168kD(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recon Amount")
    @C5566aOg
    /* renamed from: kG */
    public void mo2625kG(int i) {
        switch (bFf().mo6893i(ddW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ddW, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ddW, new Object[]{new Integer(i)}));
                break;
        }
        m5169kF(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Selection Distance")
    @C0064Am(aul = "8db10b06007656f3462bd4ef161a6bc2", aum = 0)
    private float aVn() {
        return m5170we();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Selectable Amount")
    @C0064Am(aul = "ce861f8627156d125bcf8a776d459a48", aum = 0)
    private int aVp() {
        return aVk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Selectable TableNPCSpawn")
    @C0064Am(aul = "920ad84153f851af8d58861cc56757d6", aum = 0)
    private TableNPCSpawn aVr() {
        return aVl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recon Amount")
    @C0064Am(aul = "de3cbef6f86d20b51a65646ee1c9c7df", aum = 0)
    private int aVx() {
        return aVm();
    }

    @C0064Am(aul = "c6577296dd73f6718320b1f24f717417", aum = 0)
    private boolean aVz() {
        return aVk() > 0;
    }

    @C0064Am(aul = "9c91add4895bb67a890636e464d42956", aum = 0)
    /* renamed from: a */
    private void m5161a(C0665JT jt) {
        if (jt.mo3117j(1, 0, 0)) {
            m5167kC(-1);
        } else if (jt.mo3117j(1, 1, 0)) {
            mo17522bA(true);
        }
    }
}
