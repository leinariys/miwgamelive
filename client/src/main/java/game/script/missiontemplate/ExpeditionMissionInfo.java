package game.script.missiontemplate;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6353alN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.PK */
/* compiled from: a */
public class ExpeditionMissionInfo extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1363bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1364bM = null;
    /* renamed from: bN */
    public static final C2491fm f1365bN = null;
    /* renamed from: bO */
    public static final C2491fm f1366bO = null;
    /* renamed from: bP */
    public static final C2491fm f1367bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1368bQ = null;
    public static final C5663aRz dSf = null;
    public static final C5663aRz dSh = null;
    public static final C2491fm dSi = null;
    public static final C2491fm dSj = null;
    public static final C2491fm dSk = null;
    public static final C2491fm dSl = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "75113634b12f9201513d909680bce820", aum = 2)

    /* renamed from: bK */
    private static UUID f1362bK;
    @C0064Am(aul = "f0e6d605a33cea9a87dfaacd49dc0109", aum = 0)
    private static String dSe;
    @C0064Am(aul = "44f2f153d3ea4049e79417d6cd2ba84c", aum = 1)
    private static int dSg;
    @C0064Am(aul = "08026068f59b67975795a864ac00eb5c", aum = 3)
    private static String handle;

    static {
        m8377V();
    }

    public ExpeditionMissionInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ExpeditionMissionInfo(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8377V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 8;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ExpeditionMissionInfo.class, "f0e6d605a33cea9a87dfaacd49dc0109", i);
        dSf = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ExpeditionMissionInfo.class, "44f2f153d3ea4049e79417d6cd2ba84c", i2);
        dSh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ExpeditionMissionInfo.class, "75113634b12f9201513d909680bce820", i3);
        f1363bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ExpeditionMissionInfo.class, "08026068f59b67975795a864ac00eb5c", i4);
        f1364bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(ExpeditionMissionInfo.class, "f112d4d2145d7dd7a46cb3764150d1bb", i6);
        f1365bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ExpeditionMissionInfo.class, "c010dca6f2f8c2cdae6cc742e7540d51", i7);
        f1366bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ExpeditionMissionInfo.class, "f2df44b3d3395cbb60013d9f13e24651", i8);
        f1367bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ExpeditionMissionInfo.class, "a53249de48a12f589e7d4efd3c45839d", i9);
        f1368bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ExpeditionMissionInfo.class, "043e0d716e24323e18897a3661a1645c", i10);
        dSi = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ExpeditionMissionInfo.class, "bf376fcfea7e445166e9f3e2f1da8b18", i11);
        dSj = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ExpeditionMissionInfo.class, "848e3b7ae507a468877e0517cc9764ed", i12);
        dSk = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ExpeditionMissionInfo.class, "685baf352694d1cad3bee2e970ee5db0", i13);
        dSl = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ExpeditionMissionInfo.class, C6353alN.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m8378a(String str) {
        bFf().mo5608dq().mo3197f(f1364bM, str);
    }

    /* renamed from: a */
    private void m8379a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1363bL, uuid);
    }

    /* renamed from: an */
    private UUID m8380an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1363bL);
    }

    /* renamed from: ao */
    private String m8381ao() {
        return (String) bFf().mo5608dq().mo3214p(f1364bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "a53249de48a12f589e7d4efd3c45839d", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m8384b(String str) {
        throw new aWi(new aCE(this, f1368bQ, new Object[]{str}));
    }

    private String bnY() {
        return (String) bFf().mo5608dq().mo3214p(dSf);
    }

    private int bnZ() {
        return bFf().mo5608dq().mo3212n(dSh);
    }

    /* renamed from: c */
    private void m8386c(UUID uuid) {
        switch (bFf().mo6893i(f1366bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1366bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1366bO, new Object[]{uuid}));
                break;
        }
        m8385b(uuid);
    }

    /* renamed from: gn */
    private void m8387gn(String str) {
        bFf().mo5608dq().mo3197f(dSf, str);
    }

    /* renamed from: mF */
    private void m8389mF(int i) {
        bFf().mo5608dq().mo3183b(dSh, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6353alN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m8382ap();
            case 1:
                m8385b((UUID) args[0]);
                return null;
            case 2:
                return m8383ar();
            case 3:
                m8384b((String) args[0]);
                return null;
            case 4:
                return boa();
            case 5:
                m8388go((String) args[0]);
                return null;
            case 6:
                return new Integer(boc());
            case 7:
                m8390mG(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1365bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1365bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1365bN, new Object[0]));
                break;
        }
        return m8382ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Parameter Key")
    public String bob() {
        switch (bFf().mo6893i(dSi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dSi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dSi, new Object[0]));
                break;
        }
        return boa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cycle Counter")
    public int bod() {
        switch (bFf().mo6893i(dSk)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dSk, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dSk, new Object[0]));
                break;
        }
        return boc();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1367bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1367bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1367bP, new Object[0]));
                break;
        }
        return m8383ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1368bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1368bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1368bQ, new Object[]{str}));
                break;
        }
        m8384b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Parameter Key")
    /* renamed from: gp */
    public void mo4658gp(String str) {
        switch (bFf().mo6893i(dSj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dSj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dSj, new Object[]{str}));
                break;
        }
        m8388go(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cycle Counter")
    /* renamed from: mH */
    public void mo4659mH(int i) {
        switch (bFf().mo6893i(dSl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dSl, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dSl, new Object[]{new Integer(i)}));
                break;
        }
        m8390mG(i);
    }

    @C0064Am(aul = "f112d4d2145d7dd7a46cb3764150d1bb", aum = 0)
    /* renamed from: ap */
    private UUID m8382ap() {
        return m8380an();
    }

    @C0064Am(aul = "c010dca6f2f8c2cdae6cc742e7540d51", aum = 0)
    /* renamed from: b */
    private void m8385b(UUID uuid) {
        m8379a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "f2df44b3d3395cbb60013d9f13e24651", aum = 0)
    /* renamed from: ar */
    private String m8383ar() {
        return m8381ao();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m8379a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Parameter Key")
    @C0064Am(aul = "043e0d716e24323e18897a3661a1645c", aum = 0)
    private String boa() {
        return bnY();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Parameter Key")
    @C0064Am(aul = "bf376fcfea7e445166e9f3e2f1da8b18", aum = 0)
    /* renamed from: go */
    private void m8388go(String str) {
        m8387gn(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cycle Counter")
    @C0064Am(aul = "848e3b7ae507a468877e0517cc9764ed", aum = 0)
    private int boc() {
        return bnZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cycle Counter")
    @C0064Am(aul = "685baf352694d1cad3bee2e970ee5db0", aum = 0)
    /* renamed from: mG */
    private void m8390mG(int i) {
        m8389mF(i);
    }
}
