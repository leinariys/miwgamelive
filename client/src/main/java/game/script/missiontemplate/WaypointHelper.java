package game.script.missiontemplate;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.*;
import logic.data.mbean.C2344eG;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import javax.vecmath.Tuple3d;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.DQ */
/* compiled from: a */
public class WaypointHelper extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aSk = null;
    public static final C2491fm aSl = null;
    public static final C5663aRz awh = null;
    public static final C2491fm bcC = null;
    public static final C5663aRz bcu = null;
    public static final C2491fm cQA = null;
    public static final C2491fm cQB = null;
    public static final C2491fm cQC = null;
    public static final C2491fm cQD = null;
    public static final C2491fm cQE = null;
    public static final C2491fm cQF = null;
    public static final C2491fm cQG = null;
    public static final C5663aRz cQw = null;
    public static final C5663aRz cQx = null;
    public static final C5663aRz cQy = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vp */
    public static final C2491fm f418vp = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e41b5c2194d359625f53d245ea36c20e", aum = 1)

    /* renamed from: Dw */
    private static long f414Dw;
    @C0064Am(aul = "6a187c8620a777001a02d1a080acab9e", aum = 2)

    /* renamed from: Dx */
    private static long f415Dx;
    @C0064Am(aul = "f9308d95aa5aa47f09ff978770db5397", aum = 3)

    /* renamed from: Dy */
    private static C3438ra<PositionDat> f416Dy;
    @C0064Am(aul = "ad6a9e359b9f4d5361a1c481b95786fb", aum = 4)

    /* renamed from: Dz */
    private static boolean f417Dz;
    private static /* synthetic */ int[] cQz;
    @C0064Am(aul = "a768d4a2c172cd6f92814280caca9ef4", aum = 0)
    private static Vec3d position;

    static {
        m2394V();
    }

    public WaypointHelper() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WaypointHelper(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2394V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 11;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(WaypointHelper.class, "a768d4a2c172cd6f92814280caca9ef4", i);
        bcu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WaypointHelper.class, "e41b5c2194d359625f53d245ea36c20e", i2);
        awh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WaypointHelper.class, "6a187c8620a777001a02d1a080acab9e", i3);
        cQw = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WaypointHelper.class, "f9308d95aa5aa47f09ff978770db5397", i4);
        cQx = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WaypointHelper.class, "ad6a9e359b9f4d5361a1c481b95786fb", i5);
        cQy = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 11)];
        C2491fm a = C4105zY.m41624a(WaypointHelper.class, "cde8d31ea68fcd151385c74e2748a88c", i7);
        bcC = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(WaypointHelper.class, "94349a45fd267adc94fe829cfd157ad4", i8);
        f418vp = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(WaypointHelper.class, "bd8d07fa6d33a8dc6404266d15781fbb", i9);
        aSk = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(WaypointHelper.class, "6cc2b8626819fdc98e33bd297377448a", i10);
        aSl = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(WaypointHelper.class, "8cb3ce0a059247e4121e8d8029ab9b1b", i11);
        cQA = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(WaypointHelper.class, "05d2707d6505d863d9a04a704817e043", i12);
        cQB = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(WaypointHelper.class, "43d5d6faad24a48cf265337a4e2d7c33", i13);
        cQC = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(WaypointHelper.class, "87ee796d42eebd343c057f3eabf42b70", i14);
        cQD = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(WaypointHelper.class, "d2b051bfdc1e0497d21468708a209b77", i15);
        cQE = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(WaypointHelper.class, "934c18fab62b509b34b476690d7205d1", i16);
        cQF = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(WaypointHelper.class, "4e7a701463f03abf1386b9f419a20fb9", i17);
        cQG = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WaypointHelper.class, C2344eG.class, _m_fields, _m_methods);
    }

    static /* synthetic */ int[] aMN() {
        int[] iArr = cQz;
        if (iArr == null) {
            iArr = new int[WaypointDat.C2121a.values().length];
            try {
                iArr[WaypointDat.C2121a.RANDOM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[WaypointDat.C2121a.RELATIVE.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[WaypointDat.C2121a.STATIC.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            cQz = iArr;
        }
        return iArr;
    }

    @C5566aOg
    /* renamed from: a */
    public static Vec3d m2400a(WaypointDat cDVar, Vec3d ajr) {
        switch (aMN()[cDVar.bvu().ordinal()]) {
            case 1:
                return m2399a(cDVar.getPosition(), (float) cDVar.mo17519Vg());
            case 2:
                return cDVar.getPosition();
            case 3:
                if (ajr != null) {
                    return ajr.mo9504d((Tuple3d) cDVar.getPosition());
                }
                return cDVar.getPosition();
            default:
                return cDVar.getPosition();
        }
    }

    /* renamed from: a */
    private static Vec3d m2399a(Vec3d ajr, float f) {
        return new Vec3d(ajr).mo9531q(new Vec3f((((float) Math.random()) * 2.0f) - 1.0f, (((float) Math.random()) * 2.0f) - 1.0f, (((float) Math.random()) * 2.0f) - 1.0f).dfO().mo23510mS(((float) Math.random()) * f * 0.9f));
    }

    /* renamed from: Vc */
    private long m2395Vc() {
        return bFf().mo5608dq().mo3213o(awh);
    }

    /* renamed from: YX */
    private Vec3d m2397YX() {
        return (Vec3d) bFf().mo5608dq().mo3214p(bcu);
    }

    private long aMF() {
        return bFf().mo5608dq().mo3213o(cQw);
    }

    private C3438ra aMG() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cQx);
    }

    private boolean aMH() {
        return bFf().mo5608dq().mo3201h(cQy);
    }

    /* renamed from: aX */
    private void m2401aX(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cQx, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C0064Am(aul = "94349a45fd267adc94fe829cfd157ad4", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2402c(Vec3d ajr) {
        throw new aWi(new aCE(this, f418vp, new Object[]{ajr}));
    }

    /* renamed from: cE */
    private void m2403cE(boolean z) {
        bFf().mo5608dq().mo3153a(cQy, z);
    }

    /* renamed from: cH */
    private void m2405cH(long j) {
        bFf().mo5608dq().mo3184b(awh, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radius for Random")
    @C0064Am(aul = "6cc2b8626819fdc98e33bd297377448a", aum = 0)
    @C5566aOg
    /* renamed from: cI */
    private void m2406cI(long j) {
        throw new aWi(new aCE(this, aSl, new Object[]{new Long(j)}));
    }

    /* renamed from: em */
    private void m2408em(long j) {
        bFf().mo5608dq().mo3184b(cQw, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Radius")
    @C0064Am(aul = "05d2707d6505d863d9a04a704817e043", aum = 0)
    @C5566aOg
    /* renamed from: en */
    private void m2409en(long j) {
        throw new aWi(new aCE(this, cQB, new Object[]{new Long(j)}));
    }

    /* renamed from: r */
    private void m2411r(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(bcu, ajr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radius for Random")
    /* renamed from: Vg */
    public long mo1550Vg() {
        switch (bFf().mo6893i(aSk)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, aSk, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, aSk, new Object[0]));
                break;
        }
        return m2396Vf();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2344eG(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m2398Zd();
            case 1:
                m2402c((Vec3d) args[0]);
                return null;
            case 2:
                return new Long(m2396Vf());
            case 3:
                m2406cI(((Long) args[0]).longValue());
                return null;
            case 4:
                return new Long(aMI());
            case 5:
                m2409en(((Long) args[0]).longValue());
                return null;
            case 6:
                return aMK();
            case 7:
                m2407e((PositionDat) args[0]);
                return null;
            case 8:
                m2410g((PositionDat) args[0]);
                return null;
            case 9:
                m2404cF(((Boolean) args[0]).booleanValue());
                return null;
            case 10:
                return new Boolean(aMM());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Radius")
    public long aMJ() {
        switch (bFf().mo6893i(cQA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cQA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cQA, new Object[0]));
                break;
        }
        return aMI();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoint Relative Positions")
    public List<PositionDat> aML() {
        switch (bFf().mo6893i(cQC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cQC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQC, new Object[0]));
                break;
        }
        return aMK();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radius for Random")
    @C5566aOg
    /* renamed from: cJ */
    public void mo1553cJ(long j) {
        switch (bFf().mo6893i(aSl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                break;
        }
        m2406cI(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Radius")
    @C5566aOg
    /* renamed from: eo */
    public void mo1554eo(long j) {
        switch (bFf().mo6893i(cQB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQB, new Object[]{new Long(j)}));
                break;
        }
        m2409en(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoint Relative Positions")
    /* renamed from: f */
    public void mo1555f(PositionDat vx) {
        switch (bFf().mo6893i(cQD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQD, new Object[]{vx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQD, new Object[]{vx}));
                break;
        }
        m2407e(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    public Vec3d getPosition() {
        switch (bFf().mo6893i(bcC)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bcC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcC, new Object[0]));
                break;
        }
        return m2398Zd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C5566aOg
    public void setPosition(Vec3d ajr) {
        switch (bFf().mo6893i(f418vp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f418vp, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f418vp, new Object[]{ajr}));
                break;
        }
        m2402c(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoint Relative Positions")
    /* renamed from: h */
    public void mo1557h(PositionDat vx) {
        switch (bFf().mo6893i(cQE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQE, new Object[]{vx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQE, new Object[]{vx}));
                break;
        }
        m2410g(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Is Static")
    public boolean isStatic() {
        switch (bFf().mo6893i(cQG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cQG, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cQG, new Object[0]));
                break;
        }
        return aMM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Is Static")
    public void setStatic(boolean z) {
        switch (bFf().mo6893i(cQF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQF, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQF, new Object[]{new Boolean(z)}));
                break;
        }
        m2404cF(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    @C0064Am(aul = "cde8d31ea68fcd151385c74e2748a88c", aum = 0)
    /* renamed from: Zd */
    private Vec3d m2398Zd() {
        return m2397YX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radius for Random")
    @C0064Am(aul = "bd8d07fa6d33a8dc6404266d15781fbb", aum = 0)
    /* renamed from: Vf */
    private long m2396Vf() {
        return m2395Vc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Radius")
    @C0064Am(aul = "8cb3ce0a059247e4121e8d8029ab9b1b", aum = 0)
    private long aMI() {
        return aMF();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoint Relative Positions")
    @C0064Am(aul = "43d5d6faad24a48cf265337a4e2d7c33", aum = 0)
    private List<PositionDat> aMK() {
        return Collections.unmodifiableList(aMG());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoint Relative Positions")
    @C0064Am(aul = "87ee796d42eebd343c057f3eabf42b70", aum = 0)
    /* renamed from: e */
    private void m2407e(PositionDat vx) {
        aMG().add(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoint Relative Positions")
    @C0064Am(aul = "d2b051bfdc1e0497d21468708a209b77", aum = 0)
    /* renamed from: g */
    private void m2410g(PositionDat vx) {
        aMG().remove(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Is Static")
    @C0064Am(aul = "934c18fab62b509b34b476690d7205d1", aum = 0)
    /* renamed from: cF */
    private void m2404cF(boolean z) {
        m2403cE(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Is Static")
    @C0064Am(aul = "4e7a701463f03abf1386b9f419a20fb9", aum = 0)
    private boolean aMM() {
        return aMH();
    }
}
