package game.script.missiontemplate;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6614aqO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.VX */
/* compiled from: a */
public class PositionDat extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1876bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1877bM = null;
    /* renamed from: bN */
    public static final C2491fm f1878bN = null;
    /* renamed from: bO */
    public static final C2491fm f1879bO = null;
    /* renamed from: bP */
    public static final C2491fm f1880bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1881bQ = null;
    public static final C2491fm bcC = null;
    public static final C5663aRz bcu = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vp */
    public static final C2491fm f1882vp = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "51644a6d4fb54453a22459c6d2f7a27e", aum = 1)

    /* renamed from: bK */
    private static UUID f1875bK;
    @C0064Am(aul = "a073bf68ba27ac37762c090bff46a394", aum = 2)
    private static String handle;
    @C0064Am(aul = "2f0920b2f54bca6d67d10200ac9d784f", aum = 0)
    private static Vec3d position;

    static {
        m10664V();
    }

    public PositionDat() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PositionDat(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10664V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 6;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(PositionDat.class, "2f0920b2f54bca6d67d10200ac9d784f", i);
        bcu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PositionDat.class, "51644a6d4fb54453a22459c6d2f7a27e", i2);
        f1876bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PositionDat.class, "a073bf68ba27ac37762c090bff46a394", i3);
        f1877bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(PositionDat.class, "3a72aab4aa357061fd49602092c0659a", i5);
        f1878bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(PositionDat.class, "99982bb9e6155c739cf1d33126228062", i6);
        f1879bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(PositionDat.class, "c15e8d12d8aae8034b4ef6da96219e47", i7);
        f1880bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(PositionDat.class, "6da145ae351dd724ac920f422f226e14", i8);
        f1881bQ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(PositionDat.class, "46bcced1fa56051318f5d97a9f2713e0", i9);
        bcC = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(PositionDat.class, "00f24283a3bbe399ce3c43b3be67db58", i10);
        f1882vp = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PositionDat.class, C6614aqO.class, _m_fields, _m_methods);
    }

    /* renamed from: YX */
    private Vec3d m10665YX() {
        return (Vec3d) bFf().mo5608dq().mo3214p(bcu);
    }

    /* renamed from: a */
    private void m10667a(String str) {
        bFf().mo5608dq().mo3197f(f1877bM, str);
    }

    /* renamed from: a */
    private void m10668a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1876bL, uuid);
    }

    /* renamed from: an */
    private UUID m10669an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1876bL);
    }

    /* renamed from: ao */
    private String m10670ao() {
        return (String) bFf().mo5608dq().mo3214p(f1877bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "6da145ae351dd724ac920f422f226e14", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m10673b(String str) {
        throw new aWi(new aCE(this, f1881bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m10676c(UUID uuid) {
        switch (bFf().mo6893i(f1879bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1879bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1879bO, new Object[]{uuid}));
                break;
        }
        m10674b(uuid);
    }

    /* renamed from: r */
    private void m10677r(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(bcu, ajr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6614aqO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m10671ap();
            case 1:
                m10674b((UUID) args[0]);
                return null;
            case 2:
                return m10672ar();
            case 3:
                m10673b((String) args[0]);
                return null;
            case 4:
                return m10666Zd();
            case 5:
                m10675c((Vec3d) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1878bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1878bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1878bN, new Object[0]));
                break;
        }
        return m10671ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1880bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1880bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1880bP, new Object[0]));
                break;
        }
        return m10672ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1881bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1881bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1881bQ, new Object[]{str}));
                break;
        }
        m10673b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    public Vec3d getPosition() {
        switch (bFf().mo6893i(bcC)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bcC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcC, new Object[0]));
                break;
        }
        return m10666Zd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    public void setPosition(Vec3d ajr) {
        switch (bFf().mo6893i(f1882vp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1882vp, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1882vp, new Object[]{ajr}));
                break;
        }
        m10675c(ajr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "3a72aab4aa357061fd49602092c0659a", aum = 0)
    /* renamed from: ap */
    private UUID m10671ap() {
        return m10669an();
    }

    @C0064Am(aul = "99982bb9e6155c739cf1d33126228062", aum = 0)
    /* renamed from: b */
    private void m10674b(UUID uuid) {
        m10668a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "c15e8d12d8aae8034b4ef6da96219e47", aum = 0)
    /* renamed from: ar */
    private String m10672ar() {
        return m10670ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    @C0064Am(aul = "46bcced1fa56051318f5d97a9f2713e0", aum = 0)
    /* renamed from: Zd */
    private Vec3d m10666Zd() {
        return m10665YX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C0064Am(aul = "00f24283a3bbe399ce3c43b3be67db58", aum = 0)
    /* renamed from: c */
    private void m10675c(Vec3d ajr) {
        m10677r(ajr);
    }
}
