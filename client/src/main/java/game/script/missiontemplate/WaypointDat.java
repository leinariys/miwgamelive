package game.script.missiontemplate;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.mission.TableNPCSpawn;
import game.script.npc.NPCType;
import game.script.ship.ShipType;
import logic.baa.*;
import logic.data.mbean.C5275aDb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.cD */
/* compiled from: a */
public class WaypointDat extends aDJ implements C0468GU, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f6002Do = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aSk = null;
    public static final C2491fm aSl = null;
    public static final C5663aRz awh = null;
    public static final C5663aRz bCf = null;
    public static final C2491fm bCl = null;
    public static final C2491fm bCm = null;
    /* renamed from: bL */
    public static final C5663aRz f6006bL = null;
    /* renamed from: bM */
    public static final C5663aRz f6007bM = null;
    /* renamed from: bN */
    public static final C2491fm f6008bN = null;
    /* renamed from: bO */
    public static final C2491fm f6009bO = null;
    /* renamed from: bP */
    public static final C2491fm f6010bP = null;
    /* renamed from: bQ */
    public static final C2491fm f6011bQ = null;
    public static final C2491fm bcC = null;
    public static final C5663aRz bcu = null;
    /* renamed from: cA */
    public static final C2491fm f6012cA = null;
    /* renamed from: cB */
    public static final C2491fm f6013cB = null;
    public static final C2491fm cQA = null;
    public static final C2491fm cQB = null;
    public static final C5663aRz cQw = null;
    /* renamed from: cz */
    public static final C5663aRz f6015cz = null;
    public static final C5663aRz ejH = null;
    public static final C5663aRz ejJ = null;
    public static final C5663aRz ejL = null;
    public static final C5663aRz ejN = null;
    public static final C2491fm ejO = null;
    public static final C2491fm ejP = null;
    public static final C2491fm ejQ = null;
    public static final C2491fm ejR = null;
    public static final C2491fm ejS = null;
    public static final C2491fm ejT = null;
    public static final C2491fm ejU = null;
    public static final C2491fm ejV = null;
    public static final C2491fm ejW = null;
    public static final C2491fm ejX = null;
    public static final C2491fm ejY = null;
    public static final C2491fm ejZ = null;
    public static final C2491fm eka = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vp */
    public static final C2491fm f6016vp = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6335c5a6efee71efc18cabd4a6152886", aum = 1)

    /* renamed from: Dw */
    private static long f6003Dw;
    @C0064Am(aul = "cef9eeb6737196a9d0c131089144b1a7", aum = 2)

    /* renamed from: Dx */
    private static long f6004Dx;
    @C0064Am(aul = "5e0d11c863c400a4c1766a0da24123eb", aum = 8)
    private static boolean aLE;
    @C0064Am(aul = "c67bd85c5cfaf5d4e93bd5e50a5eb1fb", aum = 9)

    /* renamed from: bK */
    private static UUID f6005bK;
    @C0064Am(aul = "0e0247ac0832cd61ab59c7daf6421997", aum = 3)

    /* renamed from: cy */
    private static int f6014cy;
    @C0064Am(aul = "50ec17bd592dcdb9bde366beb29bdc14", aum = 4)
    private static TableNPCSpawn ejG;
    @C0064Am(aul = "05fe859c91277c191aa1ebd0bb430041", aum = 5)
    private static C2121a ejI;
    @C0064Am(aul = "711472b8e68216b03e8ad73240c83ac4", aum = 6)
    private static int ejK;
    @C0064Am(aul = "7f61313ad0b0798ea2aefeadb60b47fd", aum = 7)
    private static TableNPCSpawn ejM;
    @C0064Am(aul = "e2c87ced50f5d308845bda2ed9091bef", aum = 10)
    private static String handle;
    @C0064Am(aul = "6af8db00be789d52a9859370de698a9d", aum = 0)
    private static Vec3d position;

    static {
        m28221V();
    }

    public WaypointDat() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WaypointDat(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28221V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 11;
        _m_methodCount = aDJ._m_methodCount + 29;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 11)];
        C5663aRz b = C5640aRc.m17844b(WaypointDat.class, "6af8db00be789d52a9859370de698a9d", i);
        bcu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WaypointDat.class, "6335c5a6efee71efc18cabd4a6152886", i2);
        awh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WaypointDat.class, "cef9eeb6737196a9d0c131089144b1a7", i3);
        cQw = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WaypointDat.class, "0e0247ac0832cd61ab59c7daf6421997", i4);
        f6015cz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WaypointDat.class, "50ec17bd592dcdb9bde366beb29bdc14", i5);
        ejH = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(WaypointDat.class, "05fe859c91277c191aa1ebd0bb430041", i6);
        ejJ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(WaypointDat.class, "711472b8e68216b03e8ad73240c83ac4", i7);
        ejL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(WaypointDat.class, "7f61313ad0b0798ea2aefeadb60b47fd", i8);
        ejN = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(WaypointDat.class, "5e0d11c863c400a4c1766a0da24123eb", i9);
        bCf = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(WaypointDat.class, "c67bd85c5cfaf5d4e93bd5e50a5eb1fb", i10);
        f6006bL = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(WaypointDat.class, "e2c87ced50f5d308845bda2ed9091bef", i11);
        f6007bM = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i13 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i13 + 29)];
        C2491fm a = C4105zY.m41624a(WaypointDat.class, "26d17adc825c2cd205f66720a3369861", i13);
        f6008bN = a;
        fmVarArr[i13] = a;
        int i14 = i13 + 1;
        C2491fm a2 = C4105zY.m41624a(WaypointDat.class, "e5a0d59c6ae3926e3da0722cc9de9a2c", i14);
        f6009bO = a2;
        fmVarArr[i14] = a2;
        int i15 = i14 + 1;
        C2491fm a3 = C4105zY.m41624a(WaypointDat.class, "3339ed0cfcd65f821687807b5caf74df", i15);
        f6010bP = a3;
        fmVarArr[i15] = a3;
        int i16 = i15 + 1;
        C2491fm a4 = C4105zY.m41624a(WaypointDat.class, "333374484f711a76d180918195ac5400", i16);
        f6011bQ = a4;
        fmVarArr[i16] = a4;
        int i17 = i16 + 1;
        C2491fm a5 = C4105zY.m41624a(WaypointDat.class, "acd49a36e051a533c594cfcda7f2d366", i17);
        cQA = a5;
        fmVarArr[i17] = a5;
        int i18 = i17 + 1;
        C2491fm a6 = C4105zY.m41624a(WaypointDat.class, "b5be7dadaef3827cabd9a5759a12d522", i18);
        cQB = a6;
        fmVarArr[i18] = a6;
        int i19 = i18 + 1;
        C2491fm a7 = C4105zY.m41624a(WaypointDat.class, "1ce939be43a7c071685f0b6ae95cc2d1", i19);
        ejO = a7;
        fmVarArr[i19] = a7;
        int i20 = i19 + 1;
        C2491fm a8 = C4105zY.m41624a(WaypointDat.class, "25557b43b5f4de449f428a6e66c3da88", i20);
        ejP = a8;
        fmVarArr[i20] = a8;
        int i21 = i20 + 1;
        C2491fm a9 = C4105zY.m41624a(WaypointDat.class, "feebee54e583816b91713023441328ff", i21);
        f6012cA = a9;
        fmVarArr[i21] = a9;
        int i22 = i21 + 1;
        C2491fm a10 = C4105zY.m41624a(WaypointDat.class, "bc5431ef6d913f571cb1250eb1be5b7a", i22);
        f6013cB = a10;
        fmVarArr[i22] = a10;
        int i23 = i22 + 1;
        C2491fm a11 = C4105zY.m41624a(WaypointDat.class, "159e8cdfd78369da955ce38e7afc6c58", i23);
        bcC = a11;
        fmVarArr[i23] = a11;
        int i24 = i23 + 1;
        C2491fm a12 = C4105zY.m41624a(WaypointDat.class, "89f59f3b9593adc28c744145d6300dbf", i24);
        f6016vp = a12;
        fmVarArr[i24] = a12;
        int i25 = i24 + 1;
        C2491fm a13 = C4105zY.m41624a(WaypointDat.class, "0a601c6e44cd183009c262a2c8140510", i25);
        aSk = a13;
        fmVarArr[i25] = a13;
        int i26 = i25 + 1;
        C2491fm a14 = C4105zY.m41624a(WaypointDat.class, "f360ee692733a4ac1484b4ea39d52f18", i26);
        aSl = a14;
        fmVarArr[i26] = a14;
        int i27 = i26 + 1;
        C2491fm a15 = C4105zY.m41624a(WaypointDat.class, "7ecb010aa34a2be0f42c1c8331392270", i27);
        ejQ = a15;
        fmVarArr[i27] = a15;
        int i28 = i27 + 1;
        C2491fm a16 = C4105zY.m41624a(WaypointDat.class, "7f6f183ad388880247ef9b8050fb973a", i28);
        ejR = a16;
        fmVarArr[i28] = a16;
        int i29 = i28 + 1;
        C2491fm a17 = C4105zY.m41624a(WaypointDat.class, "7b5cd8b60a190603c55646bfbf7061cd", i29);
        ejS = a17;
        fmVarArr[i29] = a17;
        int i30 = i29 + 1;
        C2491fm a18 = C4105zY.m41624a(WaypointDat.class, "647d3a35c7db9f921078a8eda90f9d88", i30);
        ejT = a18;
        fmVarArr[i30] = a18;
        int i31 = i30 + 1;
        C2491fm a19 = C4105zY.m41624a(WaypointDat.class, "b708ee1e40a5fabfcc77ace61610c43a", i31);
        ejU = a19;
        fmVarArr[i31] = a19;
        int i32 = i31 + 1;
        C2491fm a20 = C4105zY.m41624a(WaypointDat.class, "a7e37ad009a45cd473da0dc55d24be2d", i32);
        ejV = a20;
        fmVarArr[i32] = a20;
        int i33 = i32 + 1;
        C2491fm a21 = C4105zY.m41624a(WaypointDat.class, "adf84e258e69bca68592767ebc9cabf6", i33);
        ejW = a21;
        fmVarArr[i33] = a21;
        int i34 = i33 + 1;
        C2491fm a22 = C4105zY.m41624a(WaypointDat.class, "00b0ab236105cfc473cae6649a4cd9c1", i34);
        ejX = a22;
        fmVarArr[i34] = a22;
        int i35 = i34 + 1;
        C2491fm a23 = C4105zY.m41624a(WaypointDat.class, "c6c0a9320aabb634e16429dda1ac2b37", i35);
        ejY = a23;
        fmVarArr[i35] = a23;
        int i36 = i35 + 1;
        C2491fm a24 = C4105zY.m41624a(WaypointDat.class, "8d9093055d5f9260d4a5278dc11f3efe", i36);
        ejZ = a24;
        fmVarArr[i36] = a24;
        int i37 = i36 + 1;
        C2491fm a25 = C4105zY.m41624a(WaypointDat.class, "25552f322a66c2e0a6f37aff0540a087", i37);
        bCl = a25;
        fmVarArr[i37] = a25;
        int i38 = i37 + 1;
        C2491fm a26 = C4105zY.m41624a(WaypointDat.class, "74e6915d4737848d18ad3f8443419f9a", i38);
        bCm = a26;
        fmVarArr[i38] = a26;
        int i39 = i38 + 1;
        C2491fm a27 = C4105zY.m41624a(WaypointDat.class, "cba61d650d4abcc69f6a047f8ba95201", i39);
        eka = a27;
        fmVarArr[i39] = a27;
        int i40 = i39 + 1;
        C2491fm a28 = C4105zY.m41624a(WaypointDat.class, "b2152181baff9053de555160132a3c72", i40);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a28;
        fmVarArr[i40] = a28;
        int i41 = i40 + 1;
        C2491fm a29 = C4105zY.m41624a(WaypointDat.class, "76734fa087daad9e2f4279f34048f360", i41);
        f6002Do = a29;
        fmVarArr[i41] = a29;
        int i42 = i41 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WaypointDat.class, C5275aDb.class, _m_fields, _m_methods);
    }

    /* renamed from: Vc */
    private long m28222Vc() {
        return bFf().mo5608dq().mo3213o(awh);
    }

    /* renamed from: YX */
    private Vec3d m28224YX() {
        return (Vec3d) bFf().mo5608dq().mo3214p(bcu);
    }

    /* renamed from: a */
    private void m28227a(C2121a aVar) {
        bFf().mo5608dq().mo3197f(ejJ, aVar);
    }

    /* renamed from: a */
    private void m28228a(String str) {
        bFf().mo5608dq().mo3197f(f6007bM, str);
    }

    /* renamed from: a */
    private void m28229a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f6006bL, uuid);
    }

    private long aMF() {
        return bFf().mo5608dq().mo3213o(cQw);
    }

    private boolean alQ() {
        return bFf().mo5608dq().mo3201h(bCf);
    }

    /* renamed from: an */
    private UUID m28230an() {
        return (UUID) bFf().mo5608dq().mo3214p(f6006bL);
    }

    /* renamed from: ao */
    private String m28231ao() {
        return (String) bFf().mo5608dq().mo3214p(f6007bM);
    }

    /* renamed from: aw */
    private int m28235aw() {
        return bFf().mo5608dq().mo3212n(f6015cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type")
    @C0064Am(aul = "25557b43b5f4de449f428a6e66c3da88", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m28237b(C2121a aVar) {
        throw new aWi(new aCE(this, ejP, new Object[]{aVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "333374484f711a76d180918195ac5400", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m28238b(String str) {
        throw new aWi(new aCE(this, f6011bQ, new Object[]{str}));
    }

    @C0064Am(aul = "b708ee1e40a5fabfcc77ace61610c43a", aum = 0)
    @C5566aOg
    private List<NPCType> bvB() {
        throw new aWi(new aCE(this, ejU, new Object[0]));
    }

    @C0064Am(aul = "a7e37ad009a45cd473da0dc55d24be2d", aum = 0)
    @C5566aOg
    private List<NPCType> bvD() {
        throw new aWi(new aCE(this, ejV, new Object[0]));
    }

    private TableNPCSpawn bvp() {
        return (TableNPCSpawn) bFf().mo5608dq().mo3214p(ejH);
    }

    private C2121a bvq() {
        return (C2121a) bFf().mo5608dq().mo3214p(ejJ);
    }

    private int bvr() {
        return bFf().mo5608dq().mo3212n(ejL);
    }

    private TableNPCSpawn bvs() {
        return (TableNPCSpawn) bFf().mo5608dq().mo3214p(ejN);
    }

    @C0064Am(aul = "7b5cd8b60a190603c55646bfbf7061cd", aum = 0)
    @C5566aOg
    private Vec3d bvx() {
        throw new aWi(new aCE(this, ejS, new Object[0]));
    }

    @C0064Am(aul = "647d3a35c7db9f921078a8eda90f9d88", aum = 0)
    @C5566aOg
    private NPCType bvz() {
        throw new aWi(new aCE(this, ejT, new Object[0]));
    }

    /* renamed from: by */
    private void m28240by(boolean z) {
        bFf().mo5608dq().mo3153a(bCf, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows path of beacons")
    @C0064Am(aul = "74e6915d4737848d18ad3f8443419f9a", aum = 0)
    @C5566aOg
    /* renamed from: bz */
    private void m28241bz(boolean z) {
        throw new aWi(new aCE(this, bCm, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C0064Am(aul = "89f59f3b9593adc28c744145d6300dbf", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m28242c(Vec3d ajr) {
        throw new aWi(new aCE(this, f6016vp, new Object[]{ajr}));
    }

    /* renamed from: c */
    private void m28243c(UUID uuid) {
        switch (bFf().mo6893i(f6009bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6009bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6009bO, new Object[]{uuid}));
                break;
        }
        m28239b(uuid);
    }

    /* renamed from: cH */
    private void m28244cH(long j) {
        bFf().mo5608dq().mo3184b(awh, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radius")
    @C0064Am(aul = "f360ee692733a4ac1484b4ea39d52f18", aum = 0)
    @C5566aOg
    /* renamed from: cI */
    private void m28245cI(long j) {
        throw new aWi(new aCE(this, aSl, new Object[]{new Long(j)}));
    }

    /* renamed from: em */
    private void m28246em(long j) {
        bFf().mo5608dq().mo3184b(cQw, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Radius")
    @C0064Am(aul = "b5be7dadaef3827cabd9a5759a12d522", aum = 0)
    @C5566aOg
    /* renamed from: en */
    private void m28247en(long j) {
        throw new aWi(new aCE(this, cQB, new Object[]{new Long(j)}));
    }

    /* renamed from: f */
    private void m28248f(int i) {
        bFf().mo5608dq().mo3183b(f6015cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Primary Amount")
    @C0064Am(aul = "bc5431ef6d913f571cb1250eb1be5b7a", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m28249g(int i) {
        throw new aWi(new aCE(this, f6013cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: g */
    private void m28250g(TableNPCSpawn aoj) {
        bFf().mo5608dq().mo3197f(ejH, aoj);
    }

    /* renamed from: h */
    private void m28251h(TableNPCSpawn aoj) {
        bFf().mo5608dq().mo3197f(ejN, aoj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Primary TableNPCSpawn")
    @C0064Am(aul = "7f6f183ad388880247ef9b8050fb973a", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m28252i(TableNPCSpawn aoj) {
        throw new aWi(new aCE(this, ejR, new Object[]{aoj}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Secondary TableNPCSpawn")
    @C0064Am(aul = "8d9093055d5f9260d4a5278dc11f3efe", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m28253k(TableNPCSpawn aoj) {
        throw new aWi(new aCE(this, ejZ, new Object[]{aoj}));
    }

    @C0064Am(aul = "cba61d650d4abcc69f6a047f8ba95201", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private boolean m28254m(ShipType ng) {
        throw new aWi(new aCE(this, eka, new Object[]{ng}));
    }

    /* renamed from: nq */
    private void m28255nq(int i) {
        bFf().mo5608dq().mo3183b(ejL, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Secondary Amount")
    @C0064Am(aul = "00b0ab236105cfc473cae6649a4cd9c1", aum = 0)
    @C5566aOg
    /* renamed from: nr */
    private void m28256nr(int i) {
        throw new aWi(new aCE(this, ejX, new Object[]{new Integer(i)}));
    }

    /* renamed from: r */
    private void m28257r(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(bcu, ajr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radius")
    /* renamed from: Vg */
    public long mo17519Vg() {
        switch (bFf().mo6893i(aSk)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, aSk, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, aSk, new Object[0]));
                break;
        }
        return m28223Vf();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5275aDb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m28232ap();
            case 1:
                m28239b((UUID) args[0]);
                return null;
            case 2:
                return m28233ar();
            case 3:
                m28238b((String) args[0]);
                return null;
            case 4:
                return new Long(aMI());
            case 5:
                m28247en(((Long) args[0]).longValue());
                return null;
            case 6:
                return bvt();
            case 7:
                m28237b((C2121a) args[0]);
                return null;
            case 8:
                return new Integer(m28236ax());
            case 9:
                m28249g(((Integer) args[0]).intValue());
                return null;
            case 10:
                return m28225Zd();
            case 11:
                m28242c((Vec3d) args[0]);
                return null;
            case 12:
                return new Long(m28223Vf());
            case 13:
                m28245cI(((Long) args[0]).longValue());
                return null;
            case 14:
                return bvv();
            case 15:
                m28252i((TableNPCSpawn) args[0]);
                return null;
            case 16:
                return bvx();
            case 17:
                return bvz();
            case 18:
                return bvB();
            case 19:
                return bvD();
            case 20:
                return new Integer(bvF());
            case 21:
                m28256nr(((Integer) args[0]).intValue());
                return null;
            case 22:
                return bvH();
            case 23:
                m28253k((TableNPCSpawn) args[0]);
                return null;
            case 24:
                return new Boolean(alV());
            case 25:
                m28241bz(((Boolean) args[0]).booleanValue());
                return null;
            case 26:
                return new Boolean(m28254m((ShipType) args[0]));
            case 27:
                return m28234au();
            case 28:
                m28226a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Radius")
    public long aMJ() {
        switch (bFf().mo6893i(cQA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cQA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cQA, new Object[0]));
                break;
        }
        return aMI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows path of beacons")
    public boolean alW() {
        switch (bFf().mo6893i(bCl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bCl, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bCl, new Object[0]));
                break;
        }
        return alV();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f6008bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f6008bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6008bN, new Object[0]));
                break;
        }
        return m28232ap();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f6002Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6002Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6002Do, new Object[]{jt}));
                break;
        }
        m28226a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows path of beacons")
    @C5566aOg
    /* renamed from: bA */
    public void mo17522bA(boolean z) {
        switch (bFf().mo6893i(bCm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCm, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCm, new Object[]{new Boolean(z)}));
                break;
        }
        m28241bz(z);
    }

    @C5566aOg
    public NPCType bvA() {
        switch (bFf().mo6893i(ejT)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, ejT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejT, new Object[0]));
                break;
        }
        return bvz();
    }

    @C5566aOg
    public List<NPCType> bvC() {
        switch (bFf().mo6893i(ejU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, ejU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejU, new Object[0]));
                break;
        }
        return bvB();
    }

    @C5566aOg
    public List<NPCType> bvE() {
        switch (bFf().mo6893i(ejV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, ejV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejV, new Object[0]));
                break;
        }
        return bvD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Secondary Amount")
    public int bvG() {
        switch (bFf().mo6893i(ejW)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ejW, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ejW, new Object[0]));
                break;
        }
        return bvF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Secondary TableNPCSpawn")
    public TableNPCSpawn bvI() {
        switch (bFf().mo6893i(ejY)) {
            case 0:
                return null;
            case 2:
                return (TableNPCSpawn) bFf().mo5606d(new aCE(this, ejY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejY, new Object[0]));
                break;
        }
        return bvH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    public C2121a bvu() {
        switch (bFf().mo6893i(ejO)) {
            case 0:
                return null;
            case 2:
                return (C2121a) bFf().mo5606d(new aCE(this, ejO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejO, new Object[0]));
                break;
        }
        return bvt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Primary TableNPCSpawn")
    public TableNPCSpawn bvw() {
        switch (bFf().mo6893i(ejQ)) {
            case 0:
                return null;
            case 2:
                return (TableNPCSpawn) bFf().mo5606d(new aCE(this, ejQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejQ, new Object[0]));
                break;
        }
        return bvv();
    }

    @C5566aOg
    public Vec3d bvy() {
        switch (bFf().mo6893i(ejS)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, ejS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejS, new Object[0]));
                break;
        }
        return bvx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type")
    @C5566aOg
    /* renamed from: c */
    public void mo17531c(C2121a aVar) {
        switch (bFf().mo6893i(ejP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ejP, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ejP, new Object[]{aVar}));
                break;
        }
        m28237b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radius")
    @C5566aOg
    /* renamed from: cJ */
    public void mo17532cJ(long j) {
        switch (bFf().mo6893i(aSl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                break;
        }
        m28245cI(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Radius")
    @C5566aOg
    /* renamed from: eo */
    public void mo17533eo(long j) {
        switch (bFf().mo6893i(cQB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQB, new Object[]{new Long(j)}));
                break;
        }
        m28247en(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Primary Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f6012cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f6012cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6012cA, new Object[0]));
                break;
        }
        return m28236ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Primary Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f6013cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6013cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6013cB, new Object[]{new Integer(i)}));
                break;
        }
        m28249g(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f6010bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f6010bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6010bP, new Object[0]));
                break;
        }
        return m28233ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f6011bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6011bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6011bQ, new Object[]{str}));
                break;
        }
        m28238b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    public Vec3d getPosition() {
        switch (bFf().mo6893i(bcC)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bcC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcC, new Object[0]));
                break;
        }
        return m28225Zd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C5566aOg
    public void setPosition(Vec3d ajr) {
        switch (bFf().mo6893i(f6016vp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6016vp, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6016vp, new Object[]{ajr}));
                break;
        }
        m28242c(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Primary TableNPCSpawn")
    @C5566aOg
    /* renamed from: j */
    public void mo17536j(TableNPCSpawn aoj) {
        switch (bFf().mo6893i(ejR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ejR, new Object[]{aoj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ejR, new Object[]{aoj}));
                break;
        }
        m28252i(aoj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Secondary TableNPCSpawn")
    @C5566aOg
    /* renamed from: l */
    public void mo17537l(TableNPCSpawn aoj) {
        switch (bFf().mo6893i(ejZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ejZ, new Object[]{aoj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ejZ, new Object[]{aoj}));
                break;
        }
        m28253k(aoj);
    }

    @C5566aOg
    /* renamed from: n */
    public boolean mo17538n(ShipType ng) {
        switch (bFf().mo6893i(eka)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eka, new Object[]{ng}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eka, new Object[]{ng}));
                break;
        }
        return m28254m(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Secondary Amount")
    @C5566aOg
    /* renamed from: ns */
    public void mo17539ns(int i) {
        switch (bFf().mo6893i(ejX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ejX, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ejX, new Object[]{new Integer(i)}));
                break;
        }
        m28256nr(i);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m28234au();
    }

    @C0064Am(aul = "26d17adc825c2cd205f66720a3369861", aum = 0)
    /* renamed from: ap */
    private UUID m28232ap() {
        return m28230an();
    }

    @C0064Am(aul = "e5a0d59c6ae3926e3da0722cc9de9a2c", aum = 0)
    /* renamed from: b */
    private void m28239b(UUID uuid) {
        m28229a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m28229a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "3339ed0cfcd65f821687807b5caf74df", aum = 0)
    /* renamed from: ar */
    private String m28233ar() {
        return m28231ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Radius")
    @C0064Am(aul = "acd49a36e051a533c594cfcda7f2d366", aum = 0)
    private long aMI() {
        return aMF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "1ce939be43a7c071685f0b6ae95cc2d1", aum = 0)
    private C2121a bvt() {
        return bvq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Primary Amount")
    @C0064Am(aul = "feebee54e583816b91713023441328ff", aum = 0)
    /* renamed from: ax */
    private int m28236ax() {
        return m28235aw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    @C0064Am(aul = "159e8cdfd78369da955ce38e7afc6c58", aum = 0)
    /* renamed from: Zd */
    private Vec3d m28225Zd() {
        return m28224YX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radius")
    @C0064Am(aul = "0a601c6e44cd183009c262a2c8140510", aum = 0)
    /* renamed from: Vf */
    private long m28223Vf() {
        return m28222Vc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Primary TableNPCSpawn")
    @C0064Am(aul = "7ecb010aa34a2be0f42c1c8331392270", aum = 0)
    private TableNPCSpawn bvv() {
        return bvp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Secondary Amount")
    @C0064Am(aul = "adf84e258e69bca68592767ebc9cabf6", aum = 0)
    private int bvF() {
        return bvr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Secondary TableNPCSpawn")
    @C0064Am(aul = "c6c0a9320aabb634e16429dda1ac2b37", aum = 0)
    private TableNPCSpawn bvH() {
        return bvs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows path of beacons")
    @C0064Am(aul = "25552f322a66c2e0a6f37aff0540a087", aum = 0)
    private boolean alV() {
        return alQ();
    }

    @C0064Am(aul = "b2152181baff9053de555160132a3c72", aum = 0)
    /* renamed from: au */
    private String m28234au() {
        return "WaypointDat";
    }

    @C0064Am(aul = "76734fa087daad9e2f4279f34048f360", aum = 0)
    /* renamed from: a */
    private void m28226a(C0665JT jt) {
        if (jt.mo3117j(1, 0, 0)) {
            m28240by(true);
        }
    }

    /* renamed from: a.cD$a */
    public enum C2121a {
        RANDOM,
        STATIC,
        RELATIVE
    }
}
