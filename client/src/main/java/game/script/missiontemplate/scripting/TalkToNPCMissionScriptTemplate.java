package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.mission.scripting.TalkToNPCMissionScript;
import game.script.npcchat.PlayerSpeech;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aAV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.TalkToNPCMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.SA */
/* compiled from: a */
public class TalkToNPCMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aBr = null;
    public static final C5663aRz eeC = null;
    public static final C2491fm eeD = null;
    public static final C2491fm eeE = null;
    public static final C2491fm eeF = null;
    public static final C2491fm eeG = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f1525xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7814ab0109f0522991ed6149f3619943", aum = 0)
    private static C3438ra<PlayerSpeech> eeB;

    static {
        m9291V();
    }

    public TalkToNPCMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TalkToNPCMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9291V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 1;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 6;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(TalkToNPCMissionScriptTemplate.class, "7814ab0109f0522991ed6149f3619943", i);
        eeC = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i3 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
        C2491fm a = C4105zY.m41624a(TalkToNPCMissionScriptTemplate.class, "2d504d38356766b1113e50fe9b3e621f", i3);
        eeD = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(TalkToNPCMissionScriptTemplate.class, "95aed565b1dbd1871fb7b12c137c98a8", i4);
        eeE = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(TalkToNPCMissionScriptTemplate.class, "060a1dca1dd5db60fcb68e283c58b3b5", i5);
        eeF = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(TalkToNPCMissionScriptTemplate.class, "64b924741c97e4129ed0e772a5a88d7c", i6);
        eeG = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(TalkToNPCMissionScriptTemplate.class, "273f20af39b798fd1483961d97c0aaee", i7);
        aBr = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(TalkToNPCMissionScriptTemplate.class, "069186b27debad429ed4aff488ba6f84", i8);
        f1525xF = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TalkToNPCMissionScriptTemplate.class, aAV.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "273f20af39b798fd1483961d97c0aaee", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m9290MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: br */
    private void m9292br(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eeC, raVar);
    }

    private C3438ra btO() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eeC);
    }

    @C0064Am(aul = "64b924741c97e4129ed0e772a5a88d7c", aum = 0)
    @C5566aOg
    private TalkToNPCMissionScript btR() {
        throw new aWi(new aCE(this, eeG, new Object[0]));
    }

    @C0064Am(aul = "069186b27debad429ed4aff488ba6f84", aum = 0)
    /* renamed from: iR */
    private Mission m9295iR() {
        return btS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m9290MZ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aAV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                return btP();
            case 1:
                m9293g((PlayerSpeech) args[0]);
                return null;
            case 2:
                m9294i((PlayerSpeech) args[0]);
                return null;
            case 3:
                return btR();
            case 4:
                m9290MZ();
                return null;
            case 5:
                return m9295iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Target Speeches")
    public List<PlayerSpeech> btQ() {
        switch (bFf().mo6893i(eeD)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, eeD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eeD, new Object[0]));
                break;
        }
        return btP();
    }

    @C5566aOg
    public TalkToNPCMissionScript btS() {
        switch (bFf().mo6893i(eeG)) {
            case 0:
                return null;
            case 2:
                return (TalkToNPCMissionScript) bFf().mo5606d(new aCE(this, eeG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eeG, new Object[0]));
                break;
        }
        return btR();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Target Speeches")
    /* renamed from: h */
    public void mo5328h(PlayerSpeech av) {
        switch (bFf().mo6893i(eeE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eeE, new Object[]{av}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eeE, new Object[]{av}));
                break;
        }
        m9293g(av);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f1525xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f1525xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1525xF, new Object[0]));
                break;
        }
        return m9295iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Target Speeches")
    /* renamed from: j */
    public void mo5329j(PlayerSpeech av) {
        switch (bFf().mo6893i(eeF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eeF, new Object[]{av}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eeF, new Object[]{av}));
                break;
        }
        m9294i(av);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Target Speeches")
    @C0064Am(aul = "2d504d38356766b1113e50fe9b3e621f", aum = 0)
    private List<PlayerSpeech> btP() {
        return Collections.unmodifiableList(btO());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Target Speeches")
    @C0064Am(aul = "95aed565b1dbd1871fb7b12c137c98a8", aum = 0)
    /* renamed from: g */
    private void m9293g(PlayerSpeech av) {
        btO().add(av);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Target Speeches")
    @C0064Am(aul = "060a1dca1dd5db60fcb68e283c58b3b5", aum = 0)
    /* renamed from: i */
    private void m9294i(PlayerSpeech av) {
        btO().remove(av);
    }
}
