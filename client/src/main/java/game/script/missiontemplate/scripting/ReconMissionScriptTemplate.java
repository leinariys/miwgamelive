package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.mission.scripting.ReconMissionScript;
import game.script.missiontemplate.ReconWaypointDat;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5243aBv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.ReconMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.md */
/* compiled from: a */
public class ReconMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final String aBn = "recons";
    public static final C5663aRz aBo = null;
    public static final C5663aRz aBq = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C2491fm aBu = null;
    public static final C2491fm aBv = null;
    public static final C2491fm aBw = null;
    public static final C2491fm aBx = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f8657xF = null;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "8773c705a6f6c914a0d9110ec7640b7b", aum = 1)
    private static C3438ra<ReconWaypointDat> aBp;
    @C0064Am(aul = "6d2327adfcbae92aa4bc4b2586804ab7", aum = 0)

    /* renamed from: rI */
    private static StellarSystem f8656rI;

    static {
        m35807V();
    }

    public ReconMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ReconMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35807V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 2;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 8;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ReconMissionScriptTemplate.class, "6d2327adfcbae92aa4bc4b2586804ab7", i);
        aBo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ReconMissionScriptTemplate.class, "8773c705a6f6c914a0d9110ec7640b7b", i2);
        aBq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i4 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 8)];
        C2491fm a = C4105zY.m41624a(ReconMissionScriptTemplate.class, "336303d4544a2c8184a30d987a4181fd", i4);
        aBr = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ReconMissionScriptTemplate.class, "611b5913723a8d2f6dc2930ed88385ba", i5);
        aBs = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ReconMissionScriptTemplate.class, "2083b1f2300971261d546e015052dc65", i6);
        aBt = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ReconMissionScriptTemplate.class, "6221e6fab4f77794ae86843deafac095", i7);
        aBu = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ReconMissionScriptTemplate.class, "d5f294a636b0045823207b5eb55272a6", i8);
        aBv = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ReconMissionScriptTemplate.class, "476fb6f30d79a99ec76aebc76498287d", i9);
        aBw = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(ReconMissionScriptTemplate.class, "5805b43ee3d5c72aeafa74e9f2fe6a4c", i10);
        aBx = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(ReconMissionScriptTemplate.class, "c842365a1d112414dbbb44302b1414d2", i11);
        f8657xF = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ReconMissionScriptTemplate.class, C5243aBv.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m35801MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    /* renamed from: MY */
    private C3438ra m35802MY() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aBq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "336303d4544a2c8184a30d987a4181fd", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m35803MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    @C0064Am(aul = "5805b43ee3d5c72aeafa74e9f2fe6a4c", aum = 0)
    @C5566aOg
    /* renamed from: Nf */
    private ReconMissionScript m35806Nf() {
        throw new aWi(new aCE(this, aBx, new Object[0]));
    }

    /* renamed from: c */
    private void m35810c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "2083b1f2300971261d546e015052dc65", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m35811d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    @C0064Am(aul = "c842365a1d112414dbbb44302b1414d2", aum = 0)
    /* renamed from: iR */
    private Mission m35812iR() {
        return mo20578Ng();
    }

    /* renamed from: z */
    private void m35813z(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aBq, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m35803MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo20576Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m35804Nb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    /* renamed from: Ne */
    public List<ReconWaypointDat> mo20577Ne() {
        switch (bFf().mo6893i(aBu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aBu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBu, new Object[0]));
                break;
        }
        return m35805Nd();
    }

    @C5566aOg
    /* renamed from: Ng */
    public ReconMissionScript mo20578Ng() {
        switch (bFf().mo6893i(aBx)) {
            case 0:
                return null;
            case 2:
                return (ReconMissionScript) bFf().mo5606d(new aCE(this, aBx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBx, new Object[0]));
                break;
        }
        return m35806Nf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5243aBv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m35803MZ();
                return null;
            case 1:
                return m35804Nb();
            case 2:
                m35811d((StellarSystem) args[0]);
                return null;
            case 3:
                return m35805Nd();
            case 4:
                m35808a((ReconWaypointDat) args[0]);
                return null;
            case 5:
                m35809c((ReconWaypointDat) args[0]);
                return null;
            case 6:
                return m35806Nf();
            case 7:
                return m35812iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    /* renamed from: b */
    public void mo20579b(ReconWaypointDat ht) {
        switch (bFf().mo6893i(aBv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBv, new Object[]{ht}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBv, new Object[]{ht}));
                break;
        }
        m35808a(ht);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    /* renamed from: d */
    public void mo20580d(ReconWaypointDat ht) {
        switch (bFf().mo6893i(aBw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBw, new Object[]{ht}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBw, new Object[]{ht}));
                break;
        }
        m35809c(ht);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo20581e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m35811d(jj);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f8657xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f8657xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8657xF, new Object[0]));
                break;
        }
        return m35812iR();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "611b5913723a8d2f6dc2930ed88385ba", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m35804Nb() {
        return m35801MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    @C0064Am(aul = "6221e6fab4f77794ae86843deafac095", aum = 0)
    /* renamed from: Nd */
    private List<ReconWaypointDat> m35805Nd() {
        return Collections.unmodifiableList(m35802MY());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    @C0064Am(aul = "d5f294a636b0045823207b5eb55272a6", aum = 0)
    /* renamed from: a */
    private void m35808a(ReconWaypointDat ht) {
        m35802MY().add(ht);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    @C0064Am(aul = "476fb6f30d79a99ec76aebc76498287d", aum = 0)
    /* renamed from: c */
    private void m35809c(ReconWaypointDat ht) {
        m35802MY().remove(ht);
    }
}
