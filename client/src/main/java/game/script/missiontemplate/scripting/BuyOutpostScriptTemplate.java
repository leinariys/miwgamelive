package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.mission.scripting.BuyOutpostMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.ship.OutpostType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3891wN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.xZ */
/* compiled from: a */
public class BuyOutpostScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aBr = null;
    public static final C5663aRz bJh = null;
    public static final C5663aRz bJi = null;
    public static final C2491fm bJj = null;
    public static final C2491fm bJk = null;
    public static final C2491fm bJl = null;
    public static final C2491fm bJm = null;
    public static final C2491fm bJn = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f9557xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0dc5789f7f333cfe724ba3281edc2e31", aum = 0)
    private static OutpostType bEQ;
    @C0064Am(aul = "940b13a74ace4aed0077169a14bc06ac", aum = 1)
    private static String bER;

    static {
        m41045V();
    }

    public BuyOutpostScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BuyOutpostScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41045V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 2;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 7;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BuyOutpostScriptTemplate.class, "0dc5789f7f333cfe724ba3281edc2e31", i);
        bJh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BuyOutpostScriptTemplate.class, "940b13a74ace4aed0077169a14bc06ac", i2);
        bJi = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i4 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(BuyOutpostScriptTemplate.class, "f1bc98898ce9b73ddd9f1fd3fedd3388", i4);
        aBr = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BuyOutpostScriptTemplate.class, "0d43feab3a460a17674656f6d576c1fa", i5);
        bJj = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BuyOutpostScriptTemplate.class, "b08b2071ad9f643c2522f5339e9b404c", i6);
        bJk = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BuyOutpostScriptTemplate.class, "ddb675a454be1aa11d50fa28a2734ac7", i7);
        bJl = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BuyOutpostScriptTemplate.class, "1c12e9fc37f35a1f02602e9b3d73b693", i8);
        bJm = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BuyOutpostScriptTemplate.class, "6eb0bedfceda805229dd8bda4e5a7855", i9);
        bJn = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(BuyOutpostScriptTemplate.class, "fbdd9bbc08b4387539f8368353f1711b", i10);
        f9557xF = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BuyOutpostScriptTemplate.class, C3891wN.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "f1bc98898ce9b73ddd9f1fd3fedd3388", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m41044MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: a */
    private void m41046a(OutpostType kz) {
        bFf().mo5608dq().mo3197f(bJh, kz);
    }

    private OutpostType apj() {
        return (OutpostType) bFf().mo5608dq().mo3214p(bJh);
    }

    private String apk() {
        return (String) bFf().mo5608dq().mo3214p(bJi);
    }

    @C0064Am(aul = "0d43feab3a460a17674656f6d576c1fa", aum = 0)
    @C5566aOg
    private BuyOutpostMissionScript apl() {
        throw new aWi(new aCE(this, bJj, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Outpost Type")
    @C0064Am(aul = "ddb675a454be1aa11d50fa28a2734ac7", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m41047b(OutpostType kz) {
        throw new aWi(new aCE(this, bJl, new Object[]{kz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech Deliver Handle")
    @C0064Am(aul = "6eb0bedfceda805229dd8bda4e5a7855", aum = 0)
    @C5566aOg
    /* renamed from: cA */
    private void m41048cA(String str) {
        throw new aWi(new aCE(this, bJn, new Object[]{str}));
    }

    /* renamed from: cz */
    private void m41049cz(String str) {
        bFf().mo5608dq().mo3197f(bJi, str);
    }

    @C0064Am(aul = "fbdd9bbc08b4387539f8368353f1711b", aum = 0)
    /* renamed from: iR */
    private Mission m41050iR() {
        return apm();
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m41044MZ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3891wN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m41044MZ();
                return null;
            case 1:
                return apl();
            case 2:
                return apn();
            case 3:
                m41047b((OutpostType) args[0]);
                return null;
            case 4:
                return app();
            case 5:
                m41048cA((String) args[0]);
                return null;
            case 6:
                return m41050iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    public BuyOutpostMissionScript apm() {
        switch (bFf().mo6893i(bJj)) {
            case 0:
                return null;
            case 2:
                return (BuyOutpostMissionScript) bFf().mo5606d(new aCE(this, bJj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJj, new Object[0]));
                break;
        }
        return apl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Type")
    public OutpostType apo() {
        switch (bFf().mo6893i(bJk)) {
            case 0:
                return null;
            case 2:
                return (OutpostType) bFf().mo5606d(new aCE(this, bJk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJk, new Object[0]));
                break;
        }
        return apn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech Deliver Handle")
    public String apq() {
        switch (bFf().mo6893i(bJm)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bJm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJm, new Object[0]));
                break;
        }
        return app();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Outpost Type")
    @C5566aOg
    /* renamed from: c */
    public void mo22943c(OutpostType kz) {
        switch (bFf().mo6893i(bJl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJl, new Object[]{kz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJl, new Object[]{kz}));
                break;
        }
        m41047b(kz);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech Deliver Handle")
    @C5566aOg
    /* renamed from: cB */
    public void mo22944cB(String str) {
        switch (bFf().mo6893i(bJn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJn, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJn, new Object[]{str}));
                break;
        }
        m41048cA(str);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f9557xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f9557xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9557xF, new Object[0]));
                break;
        }
        return m41050iR();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Type")
    @C0064Am(aul = "b08b2071ad9f643c2522f5339e9b404c", aum = 0)
    private OutpostType apn() {
        return apj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech Deliver Handle")
    @C0064Am(aul = "1c12e9fc37f35a1f02602e9b3d73b693", aum = 0)
    private String app() {
        return apk();
    }
}
