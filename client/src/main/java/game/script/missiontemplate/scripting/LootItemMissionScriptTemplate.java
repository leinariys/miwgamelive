package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.TableNPCSpawn;
import game.script.mission.scripting.LootItemMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.missiontemplate.WaypointDat;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6962axb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.RQ */
/* compiled from: a */
public class LootItemMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C2491fm ebA = null;
    public static final C2491fm ebB = null;
    public static final C2491fm ebC = null;
    public static final C2491fm ebD = null;
    public static final C2491fm ebE = null;
    public static final C2491fm ebF = null;
    public static final C2491fm ebG = null;
    public static final C2491fm ebH = null;
    public static final C2491fm ebI = null;
    public static final C2491fm ebJ = null;
    public static final C2491fm ebK = null;
    public static final C2491fm ebL = null;
    public static final C2491fm ebM = null;
    public static final C2491fm ebN = null;
    public static final String ebh = "lootItemMissionWaypoint";
    public static final String ebi = "lootItemMissionKill";
    public static final String ebj = "lootItemMissionLoot";
    public static final String ebk = "lootItemMissionDeliver";
    public static final C5663aRz ebm = null;
    public static final C5663aRz ebn = null;
    public static final C5663aRz ebp = null;
    public static final C5663aRz ebr = null;
    public static final C5663aRz ebt = null;
    public static final C5663aRz ebv = null;
    public static final C5663aRz ebx = null;
    public static final C2491fm eby = null;
    public static final C2491fm ebz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f1491xF = null;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "fb3aa6b66ffd584ea303c5065a6852e1", aum = 2)
    private static ItemType dhK = null;
    @C0064Am(aul = "e1359e93e39944429586279491c8cb4f", aum = 0)
    private static TableNPCSpawn ebl;
    @C0064Am(aul = "14cee2ad98cd01cc5eca3aae63edb56c", aum = 3)
    private static WaypointDat ebo;
    @C0064Am(aul = "5f29b7de0d78fe422d1f0f8ecdad6ab3", aum = 4)
    private static boolean ebq;
    @C0064Am(aul = "641c287848f5e38371a41cd4d6158f85", aum = 5)
    private static String ebs;
    @C0064Am(aul = "0bd59e4fcd1a45be72bda034b12ce886", aum = 6)
    private static ItemType ebu;
    @C0064Am(aul = "5b5d7e4c6a153d17bfbb5a74fb2a27d1", aum = 7)
    private static C3438ra<WaypointDat> ebw;
    @C0064Am(aul = "7738750c7e38b25acba958d70d882225", aum = 1)

    /* renamed from: rI */
    private static StellarSystem f1490rI;

    static {
        m9132V();
    }

    public LootItemMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LootItemMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9132V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 8;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 20;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "e1359e93e39944429586279491c8cb4f", i);
        ebm = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "7738750c7e38b25acba958d70d882225", i2);
        aBo = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "fb3aa6b66ffd584ea303c5065a6852e1", i3);
        ebn = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "14cee2ad98cd01cc5eca3aae63edb56c", i4);
        ebp = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "5f29b7de0d78fe422d1f0f8ecdad6ab3", i5);
        ebr = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "641c287848f5e38371a41cd4d6158f85", i6);
        ebt = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "0bd59e4fcd1a45be72bda034b12ce886", i7);
        ebv = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(LootItemMissionScriptTemplate.class, "5b5d7e4c6a153d17bfbb5a74fb2a27d1", i8);
        ebx = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i10 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 20)];
        C2491fm a = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "d141044c5a3c58b11da2b54bc0e49103", i10);
        aBr = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "641c550437b98a6d52ebe9e292090091", i11);
        eby = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "2c1ec02ed03c30e60c554d6e7b277c3e", i12);
        ebz = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "a9f23891070450bcce788ae09617dede", i13);
        ebA = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "d9e21ba49fcfc718c058fb1aa618036a", i14);
        ebB = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "ac130c488317cdc2cd62b31bd454d5a2", i15);
        aBs = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "a9183d552468497e7252b9cd0feb47ed", i16);
        aBt = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "214e7cc02bde537b3993e31febac8620", i17);
        ebC = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "8990859d8ae9002183879f46644fa22a", i18);
        ebD = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "a0ceebe3eeeb4d4edd135aa4bb3b49ee", i19);
        ebE = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "51ccfc8b71c5da49ca345ec7adb0ae27", i20);
        ebF = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "a40e65e45cd434e550788bdb98952414", i21);
        ebG = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "4d53155f729d23b913a5e46f5cdb386e", i22);
        ebH = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "5f2a9140f79d685fb68378bc72041b41", i23);
        ebI = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "361c0bae2968d7e026ed6a742a78fda3", i24);
        ebJ = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "0122d5670a988d14af846430e18e5691", i25);
        ebK = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "d8c317db101a48aeb3d6b6644d6cd598", i26);
        ebL = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "3422015d846a6b78e5181f860e23028e", i27);
        ebM = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "91237754c1daa6f8138a7176e8dacf02", i28);
        ebN = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(LootItemMissionScriptTemplate.class, "3a90581b6cbb0039d25ed4ddbd6d3ffd", i29);
        f1491xF = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LootItemMissionScriptTemplate.class, C6962axb.class, _m_fields, _m_methods);
    }

    /* renamed from: C */
    private void m9125C(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(ebn, jCVar);
    }

    /* renamed from: D */
    private void m9126D(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(ebv, jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item to be Looted")
    @C0064Am(aul = "2c1ec02ed03c30e60c554d6e7b277c3e", aum = 0)
    @C5566aOg
    /* renamed from: E */
    private void m9127E(ItemType jCVar) {
        throw new aWi(new aCE(this, ebz, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item to be Delivered (optional)")
    @C0064Am(aul = "0122d5670a988d14af846430e18e5691", aum = 0)
    @C5566aOg
    /* renamed from: G */
    private void m9128G(ItemType jCVar) {
        throw new aWi(new aCE(this, ebK, new Object[]{jCVar}));
    }

    /* renamed from: MX */
    private StellarSystem m9129MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "d141044c5a3c58b11da2b54bc0e49103", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m9130MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: bp */
    private void m9133bp(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(ebx, raVar);
    }

    private TableNPCSpawn brM() {
        return (TableNPCSpawn) bFf().mo5608dq().mo3214p(ebm);
    }

    private ItemType brN() {
        return (ItemType) bFf().mo5608dq().mo3214p(ebn);
    }

    private WaypointDat brO() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(ebp);
    }

    private boolean brP() {
        return bFf().mo5608dq().mo3201h(ebr);
    }

    private String brQ() {
        return (String) bFf().mo5608dq().mo3214p(ebt);
    }

    private ItemType brR() {
        return (ItemType) bFf().mo5608dq().mo3214p(ebv);
    }

    private C3438ra brS() {
        return (C3438ra) bFf().mo5608dq().mo3214p(ebx);
    }

    @C0064Am(aul = "a0ceebe3eeeb4d4edd135aa4bb3b49ee", aum = 0)
    @C5566aOg
    private LootItemMissionScript brZ() {
        throw new aWi(new aCE(this, ebE, new Object[0]));
    }

    /* renamed from: c */
    private void m9134c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "a9183d552468497e7252b9cd0feb47ed", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m9135d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    /* renamed from: d */
    private void m9136d(TableNPCSpawn aoj) {
        bFf().mo5608dq().mo3197f(ebm, aoj);
    }

    /* renamed from: dq */
    private void m9137dq(boolean z) {
        bFf().mo5608dq().mo3153a(ebr, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Lootable NPC On Accept")
    @C0064Am(aul = "a40e65e45cd434e550788bdb98952414", aum = 0)
    @C5566aOg
    /* renamed from: dr */
    private void m9138dr(boolean z) {
        throw new aWi(new aCE(this, ebG, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lootable NPC")
    @C0064Am(aul = "d9e21ba49fcfc718c058fb1aa618036a", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m9139e(TableNPCSpawn aoj) {
        throw new aWi(new aCE(this, ebB, new Object[]{aoj}));
    }

    /* renamed from: gv */
    private void m9140gv(String str) {
        bFf().mo5608dq().mo3197f(ebt, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech Handle")
    @C0064Am(aul = "5f2a9140f79d685fb68378bc72041b41", aum = 0)
    @C5566aOg
    /* renamed from: gw */
    private void m9141gw(String str) {
        throw new aWi(new aCE(this, ebI, new Object[]{str}));
    }

    @C0064Am(aul = "3a90581b6cbb0039d25ed4ddbd6d3ffd", aum = 0)
    /* renamed from: iR */
    private Mission m9142iR() {
        return bsa();
    }

    /* renamed from: s */
    private void m9143s(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(ebp, cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint")
    @C0064Am(aul = "8990859d8ae9002183879f46644fa22a", aum = 0)
    @C5566aOg
    /* renamed from: t */
    private void m9144t(WaypointDat cDVar) {
        throw new aWi(new aCE(this, ebD, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Patrol Waypoints")
    @C0064Am(aul = "3422015d846a6b78e5181f860e23028e", aum = 0)
    @C5566aOg
    /* renamed from: v */
    private void m9145v(WaypointDat cDVar) {
        throw new aWi(new aCE(this, ebM, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Patrol Waypoints")
    @C0064Am(aul = "91237754c1daa6f8138a7176e8dacf02", aum = 0)
    @C5566aOg
    /* renamed from: x */
    private void m9146x(WaypointDat cDVar) {
        throw new aWi(new aCE(this, ebN, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item to be Looted")
    @C5566aOg
    /* renamed from: F */
    public void mo5176F(ItemType jCVar) {
        switch (bFf().mo6893i(ebz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebz, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebz, new Object[]{jCVar}));
                break;
        }
        m9127E(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item to be Delivered (optional)")
    @C5566aOg
    /* renamed from: H */
    public void mo5177H(ItemType jCVar) {
        switch (bFf().mo6893i(ebK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebK, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebK, new Object[]{jCVar}));
                break;
        }
        m9128G(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m9130MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo5178Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m9131Nb();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6962axb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m9130MZ();
                return null;
            case 1:
                return brT();
            case 2:
                m9127E((ItemType) args[0]);
                return null;
            case 3:
                return brV();
            case 4:
                m9139e((TableNPCSpawn) args[0]);
                return null;
            case 5:
                return m9131Nb();
            case 6:
                m9135d((StellarSystem) args[0]);
                return null;
            case 7:
                return brX();
            case 8:
                m9144t((WaypointDat) args[0]);
                return null;
            case 9:
                return brZ();
            case 10:
                return new Boolean(bsb());
            case 11:
                m9138dr(((Boolean) args[0]).booleanValue());
                return null;
            case 12:
                return bsd();
            case 13:
                m9141gw((String) args[0]);
                return null;
            case 14:
                return bsf();
            case 15:
                m9128G((ItemType) args[0]);
                return null;
            case 16:
                return bsh();
            case 17:
                m9145v((WaypointDat) args[0]);
                return null;
            case 18:
                m9146x((WaypointDat) args[0]);
                return null;
            case 19:
                return m9142iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item to be Looted")
    public ItemType brU() {
        switch (bFf().mo6893i(eby)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, eby, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eby, new Object[0]));
                break;
        }
        return brT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lootable NPC")
    public TableNPCSpawn brW() {
        switch (bFf().mo6893i(ebA)) {
            case 0:
                return null;
            case 2:
                return (TableNPCSpawn) bFf().mo5606d(new aCE(this, ebA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebA, new Object[0]));
                break;
        }
        return brV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint")
    public WaypointDat brY() {
        switch (bFf().mo6893i(ebC)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, ebC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebC, new Object[0]));
                break;
        }
        return brX();
    }

    @C5566aOg
    public LootItemMissionScript bsa() {
        switch (bFf().mo6893i(ebE)) {
            case 0:
                return null;
            case 2:
                return (LootItemMissionScript) bFf().mo5606d(new aCE(this, ebE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebE, new Object[0]));
                break;
        }
        return brZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Lootable NPC On Accept")
    public boolean bsc() {
        switch (bFf().mo6893i(ebF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ebF, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ebF, new Object[0]));
                break;
        }
        return bsb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech Handle")
    public String bse() {
        switch (bFf().mo6893i(ebH)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ebH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebH, new Object[0]));
                break;
        }
        return bsd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item to be Delivered (optional)")
    public ItemType bsg() {
        switch (bFf().mo6893i(ebJ)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, ebJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebJ, new Object[0]));
                break;
        }
        return bsf();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Patrol Waypoints")
    public List<WaypointDat> bsi() {
        switch (bFf().mo6893i(ebL)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, ebL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebL, new Object[0]));
                break;
        }
        return bsh();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Lootable NPC On Accept")
    @C5566aOg
    /* renamed from: ds */
    public void mo5187ds(boolean z) {
        switch (bFf().mo6893i(ebG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebG, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebG, new Object[]{new Boolean(z)}));
                break;
        }
        m9138dr(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo5188e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m9135d(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lootable NPC")
    @C5566aOg
    /* renamed from: f */
    public void mo5189f(TableNPCSpawn aoj) {
        switch (bFf().mo6893i(ebB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebB, new Object[]{aoj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebB, new Object[]{aoj}));
                break;
        }
        m9139e(aoj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech Handle")
    @C5566aOg
    /* renamed from: gx */
    public void mo5190gx(String str) {
        switch (bFf().mo6893i(ebI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebI, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebI, new Object[]{str}));
                break;
        }
        m9141gw(str);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f1491xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f1491xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1491xF, new Object[0]));
                break;
        }
        return m9142iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint")
    @C5566aOg
    /* renamed from: u */
    public void mo5191u(WaypointDat cDVar) {
        switch (bFf().mo6893i(ebD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebD, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebD, new Object[]{cDVar}));
                break;
        }
        m9144t(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Patrol Waypoints")
    @C5566aOg
    /* renamed from: w */
    public void mo5192w(WaypointDat cDVar) {
        switch (bFf().mo6893i(ebM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebM, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebM, new Object[]{cDVar}));
                break;
        }
        m9145v(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Patrol Waypoints")
    @C5566aOg
    /* renamed from: y */
    public void mo5193y(WaypointDat cDVar) {
        switch (bFf().mo6893i(ebN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebN, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebN, new Object[]{cDVar}));
                break;
        }
        m9146x(cDVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item to be Looted")
    @C0064Am(aul = "641c550437b98a6d52ebe9e292090091", aum = 0)
    private ItemType brT() {
        return brN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lootable NPC")
    @C0064Am(aul = "a9f23891070450bcce788ae09617dede", aum = 0)
    private TableNPCSpawn brV() {
        return brM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "ac130c488317cdc2cd62b31bd454d5a2", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m9131Nb() {
        return m9129MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint")
    @C0064Am(aul = "214e7cc02bde537b3993e31febac8620", aum = 0)
    private WaypointDat brX() {
        return brO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Lootable NPC On Accept")
    @C0064Am(aul = "51ccfc8b71c5da49ca345ec7adb0ae27", aum = 0)
    private boolean bsb() {
        return brP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech Handle")
    @C0064Am(aul = "4d53155f729d23b913a5e46f5cdb386e", aum = 0)
    private String bsd() {
        return brQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item to be Delivered (optional)")
    @C0064Am(aul = "361c0bae2968d7e026ed6a742a78fda3", aum = 0)
    private ItemType bsf() {
        return brR();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Patrol Waypoints")
    @C0064Am(aul = "d8c317db101a48aeb3d6b6644d6cd598", aum = 0)
    private List<WaypointDat> bsh() {
        return Collections.unmodifiableList(brS());
    }
}
