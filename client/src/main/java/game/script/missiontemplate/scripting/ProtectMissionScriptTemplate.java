package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.mission.scripting.ProtectMissionScript;
import game.script.missiontemplate.WaypointDat;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5993aeR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Dp */
/* compiled from: a */
public class ProtectMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C5663aRz cEB = null;
    public static final C5663aRz cED = null;
    public static final C5663aRz cEF = null;
    public static final C5663aRz cEH = null;
    public static final C5663aRz cEJ = null;
    public static final C5663aRz cEL = null;
    public static final C5663aRz cEN = null;
    public static final C5663aRz cEP = null;
    public static final C5663aRz cER = null;
    public static final C5663aRz cET = null;
    public static final C2491fm cEU = null;
    public static final C2491fm cEV = null;
    public static final C2491fm cEW = null;
    public static final C2491fm cEX = null;
    public static final C2491fm cEY = null;
    public static final C2491fm cEZ = null;
    public static final String cEj = "protect_obj_handle";
    public static final C5663aRz cEl = null;
    public static final C5663aRz cEn = null;
    public static final C5663aRz cEp = null;
    public static final C5663aRz cEr = null;
    public static final C5663aRz cEt = null;
    public static final C5663aRz cEv = null;
    public static final C5663aRz cEx = null;
    public static final C5663aRz cEz = null;
    public static final C2491fm cFA = null;
    public static final C2491fm cFB = null;
    public static final C2491fm cFC = null;
    public static final C2491fm cFD = null;
    public static final C2491fm cFE = null;
    public static final C2491fm cFF = null;
    public static final C2491fm cFa = null;
    public static final C2491fm cFb = null;
    public static final C2491fm cFc = null;
    public static final C2491fm cFd = null;
    public static final C2491fm cFe = null;
    public static final C2491fm cFf = null;
    public static final C2491fm cFg = null;
    public static final C2491fm cFh = null;
    public static final C2491fm cFi = null;
    public static final C2491fm cFj = null;
    public static final C2491fm cFk = null;
    public static final C2491fm cFl = null;
    public static final C2491fm cFm = null;
    public static final C2491fm cFn = null;
    public static final C2491fm cFo = null;
    public static final C2491fm cFp = null;
    public static final C2491fm cFq = null;
    public static final C2491fm cFr = null;
    public static final C2491fm cFs = null;
    public static final C2491fm cFt = null;
    public static final C2491fm cFu = null;
    public static final C2491fm cFv = null;
    public static final C2491fm cFw = null;
    public static final C2491fm cFx = null;
    public static final C2491fm cFy = null;
    public static final C2491fm cFz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f438xF = null;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "c28188c2b325bc636b8e03cfe4f5988b", aum = 9)
    private static I18NString cEA = null;
    @C0064Am(aul = "94e665e6e9ca7cd0d52f99796faa4082", aum = 10)
    private static int cEC = 0;
    @C0064Am(aul = "89ac625e7572268f1c84022d9001934a", aum = 11)
    private static boolean cEE = false;
    @C0064Am(aul = "2b377bdb2ddd5b046391cb4c45c4e647", aum = 12)
    private static I18NString cEG = null;
    @C0064Am(aul = "d995a4fd37b3da8fc0c1872173575efa", aum = 13)
    private static boolean cEI = false;
    @C0064Am(aul = "cbba63929505c1c195071c529b08a773", aum = 14)
    private static I18NString cEK = null;
    @C0064Am(aul = "0e19114e379ff732bb823c1ff984d50c", aum = 15)
    private static boolean cEM = false;
    @C0064Am(aul = "b9d198980194af0e11c92452c9ddf2f4", aum = 16)
    private static boolean cEO = false;
    @C0064Am(aul = "73a6819e349bcffd7adca23283c8c2a2", aum = 17)
    private static boolean cEQ = false;
    @C0064Am(aul = "6f4dd42b96549842116c25cca28c9949", aum = 18)
    private static boolean cES = false;
    @C0064Am(aul = "e838511b0e135f0f236eecfd5feaf4d4", aum = 1)
    private static WaypointDat cEk;
    @C0064Am(aul = "c3c8fa7b420640dab1fb40ffc9dfe73c", aum = 2)
    private static C3438ra<WaypointDat> cEm;
    @C0064Am(aul = "f061195211c816dec7753b4b1c2c64e6", aum = 3)
    private static float cEo;
    @C0064Am(aul = "946c8699a5092858a98750b74ba185c9", aum = 4)
    private static float cEq;
    @C0064Am(aul = "2eb64e5910ba3d3d8d4705dc936bfe81", aum = 5)
    private static boolean cEs;
    @C0064Am(aul = "30940d18ec4fa07e985ce4b5128a484c", aum = 6)
    private static I18NString cEu;
    @C0064Am(aul = "53dcf26168160e8020a6f41f4bc33195", aum = 7)
    private static I18NString cEw;
    @C0064Am(aul = "4285a63100c7a52f1a765538b22485bd", aum = 8)
    private static I18NString cEy;
    @C0064Am(aul = "9f5da287cdfe101163e33d01a7571eb7", aum = 0)

    /* renamed from: rI */
    private static StellarSystem f437rI;

    static {
        m2518V();
    }

    public ProtectMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProtectMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2518V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 19;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 42;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 19)];
        C5663aRz b = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "9f5da287cdfe101163e33d01a7571eb7", i);
        aBo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "e838511b0e135f0f236eecfd5feaf4d4", i2);
        cEl = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "c3c8fa7b420640dab1fb40ffc9dfe73c", i3);
        cEn = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "f061195211c816dec7753b4b1c2c64e6", i4);
        cEp = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "946c8699a5092858a98750b74ba185c9", i5);
        cEr = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "2eb64e5910ba3d3d8d4705dc936bfe81", i6);
        cEt = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "30940d18ec4fa07e985ce4b5128a484c", i7);
        cEv = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "53dcf26168160e8020a6f41f4bc33195", i8);
        cEx = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "4285a63100c7a52f1a765538b22485bd", i9);
        cEz = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "c28188c2b325bc636b8e03cfe4f5988b", i10);
        cEB = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "94e665e6e9ca7cd0d52f99796faa4082", i11);
        cED = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "89ac625e7572268f1c84022d9001934a", i12);
        cEF = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "2b377bdb2ddd5b046391cb4c45c4e647", i13);
        cEH = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "d995a4fd37b3da8fc0c1872173575efa", i14);
        cEJ = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "cbba63929505c1c195071c529b08a773", i15);
        cEL = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "0e19114e379ff732bb823c1ff984d50c", i16);
        cEN = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "b9d198980194af0e11c92452c9ddf2f4", i17);
        cEP = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "73a6819e349bcffd7adca23283c8c2a2", i18);
        cER = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(ProtectMissionScriptTemplate.class, "6f4dd42b96549842116c25cca28c9949", i19);
        cET = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i21 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i21 + 42)];
        C2491fm a = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "c4ecb3110f2c414b269e0fe1b1301823", i21);
        cEU = a;
        fmVarArr[i21] = a;
        int i22 = i21 + 1;
        C2491fm a2 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "723f5e69b644ce014b618122effbb311", i22);
        cEV = a2;
        fmVarArr[i22] = a2;
        int i23 = i22 + 1;
        C2491fm a3 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "c07aaeb97339f0b4f0079120f4160b98", i23);
        cEW = a3;
        fmVarArr[i23] = a3;
        int i24 = i23 + 1;
        C2491fm a4 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "0b29419f68aa97148893a28191fecdde", i24);
        cEX = a4;
        fmVarArr[i24] = a4;
        int i25 = i24 + 1;
        C2491fm a5 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "8aee2e7c712126ed839b744855c187a1", i25);
        cEY = a5;
        fmVarArr[i25] = a5;
        int i26 = i25 + 1;
        C2491fm a6 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "e24251e6af50f79e5823e5f054bfa960", i26);
        cEZ = a6;
        fmVarArr[i26] = a6;
        int i27 = i26 + 1;
        C2491fm a7 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "e748681733490349b64812c44072182c", i27);
        cFa = a7;
        fmVarArr[i27] = a7;
        int i28 = i27 + 1;
        C2491fm a8 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "3c092c54c04ecee7c709e95ff398b8a2", i28);
        cFb = a8;
        fmVarArr[i28] = a8;
        int i29 = i28 + 1;
        C2491fm a9 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "5f7426ba939f7c228c0d5ece8c7b79cb", i29);
        aBs = a9;
        fmVarArr[i29] = a9;
        int i30 = i29 + 1;
        C2491fm a10 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "87559db523f82ccf902dd22812836956", i30);
        aBt = a10;
        fmVarArr[i30] = a10;
        int i31 = i30 + 1;
        C2491fm a11 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "11be632fe5e12b5d746125c4e1d01611", i31);
        cFc = a11;
        fmVarArr[i31] = a11;
        int i32 = i31 + 1;
        C2491fm a12 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "16e9f7a8a3d3097f58b9e6778ff431f3", i32);
        cFd = a12;
        fmVarArr[i32] = a12;
        int i33 = i32 + 1;
        C2491fm a13 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "e3fdce8a0a870d46131d75a24182af8d", i33);
        cFe = a13;
        fmVarArr[i33] = a13;
        int i34 = i33 + 1;
        C2491fm a14 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "0a05a2ae8115543fa16ff81112376544", i34);
        cFf = a14;
        fmVarArr[i34] = a14;
        int i35 = i34 + 1;
        C2491fm a15 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "c66b455cd3e4a1dc64cd04aa10701f10", i35);
        cFg = a15;
        fmVarArr[i35] = a15;
        int i36 = i35 + 1;
        C2491fm a16 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "b55139bd6a70f79e09b70b10130ee06c", i36);
        cFh = a16;
        fmVarArr[i36] = a16;
        int i37 = i36 + 1;
        C2491fm a17 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "1e245df8392c8cd541056ea7b19b0f22", i37);
        cFi = a17;
        fmVarArr[i37] = a17;
        int i38 = i37 + 1;
        C2491fm a18 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "396c455933d3561e8a3c4e84acf6eabf", i38);
        cFj = a18;
        fmVarArr[i38] = a18;
        int i39 = i38 + 1;
        C2491fm a19 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "e170cc193d4c3ef4ff62af7dbd8945b5", i39);
        cFk = a19;
        fmVarArr[i39] = a19;
        int i40 = i39 + 1;
        C2491fm a20 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "185d7a87dfe2e9a5d2134a9c98e1f687", i40);
        cFl = a20;
        fmVarArr[i40] = a20;
        int i41 = i40 + 1;
        C2491fm a21 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "c578dea7f68415a4fd59e4a2a4a65df4", i41);
        cFm = a21;
        fmVarArr[i41] = a21;
        int i42 = i41 + 1;
        C2491fm a22 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "8e27b0d8be69072ebdfc15a3ee573aaa", i42);
        cFn = a22;
        fmVarArr[i42] = a22;
        int i43 = i42 + 1;
        C2491fm a23 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "6c2c3bbcca8431374222fde9134f6de3", i43);
        cFo = a23;
        fmVarArr[i43] = a23;
        int i44 = i43 + 1;
        C2491fm a24 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "8ecd3db593467910cda4a3486b9e7f9b", i44);
        cFp = a24;
        fmVarArr[i44] = a24;
        int i45 = i44 + 1;
        C2491fm a25 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "f45d83a6805d7416cfc2726bb3abea17", i45);
        cFq = a25;
        fmVarArr[i45] = a25;
        int i46 = i45 + 1;
        C2491fm a26 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "a305f06889f4188c2b1fd11997de656f", i46);
        cFr = a26;
        fmVarArr[i46] = a26;
        int i47 = i46 + 1;
        C2491fm a27 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "ecb0d76728b33bae3f406d7744a82853", i47);
        cFs = a27;
        fmVarArr[i47] = a27;
        int i48 = i47 + 1;
        C2491fm a28 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "8c991d96704cba4cbfd8c9814a3c8197", i48);
        cFt = a28;
        fmVarArr[i48] = a28;
        int i49 = i48 + 1;
        C2491fm a29 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "18ce381d54666c8a2f528596e8c42aaf", i49);
        cFu = a29;
        fmVarArr[i49] = a29;
        int i50 = i49 + 1;
        C2491fm a30 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "c375bda2a8e30ae18ca8bb3cd5fbfa6a", i50);
        cFv = a30;
        fmVarArr[i50] = a30;
        int i51 = i50 + 1;
        C2491fm a31 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "5f0648f9ff3d3800be0bc61154aeda59", i51);
        cFw = a31;
        fmVarArr[i51] = a31;
        int i52 = i51 + 1;
        C2491fm a32 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "89cc4ceec0ac4c2fc573d3edb2d36cd4", i52);
        cFx = a32;
        fmVarArr[i52] = a32;
        int i53 = i52 + 1;
        C2491fm a33 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "1b3ffc5de79cac0a19499d9625152c10", i53);
        cFy = a33;
        fmVarArr[i53] = a33;
        int i54 = i53 + 1;
        C2491fm a34 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "4b610defb2cfca5c2fae48ceb2956c2e", i54);
        cFz = a34;
        fmVarArr[i54] = a34;
        int i55 = i54 + 1;
        C2491fm a35 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "57688423313a3d7d789b234146d8c2f6", i55);
        cFA = a35;
        fmVarArr[i55] = a35;
        int i56 = i55 + 1;
        C2491fm a36 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "c388812d6adaafa42b8c71e9f3fa831f", i56);
        cFB = a36;
        fmVarArr[i56] = a36;
        int i57 = i56 + 1;
        C2491fm a37 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "3f0d07fdb294367c9844ce06db25bd6e", i57);
        cFC = a37;
        fmVarArr[i57] = a37;
        int i58 = i57 + 1;
        C2491fm a38 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "7c91de5efb588c4973884e49fdf97857", i58);
        cFD = a38;
        fmVarArr[i58] = a38;
        int i59 = i58 + 1;
        C2491fm a39 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "4dbcc5a6788e1d04b47429dfb0c88801", i59);
        cFE = a39;
        fmVarArr[i59] = a39;
        int i60 = i59 + 1;
        C2491fm a40 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "e9f1bfff517a27dce11f6f45ed79c55b", i60);
        cFF = a40;
        fmVarArr[i60] = a40;
        int i61 = i60 + 1;
        C2491fm a41 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "69c720fb3d2c28c107514273a40d62ec", i61);
        aBr = a41;
        fmVarArr[i61] = a41;
        int i62 = i61 + 1;
        C2491fm a42 = C4105zY.m41624a(ProtectMissionScriptTemplate.class, "6c6e43952689af5479c1a1fbd7653cd8", i62);
        f438xF = a42;
        fmVarArr[i62] = a42;
        int i63 = i62 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProtectMissionScriptTemplate.class, C5993aeR.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m2515MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "69c720fb3d2c28c107514273a40d62ec", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m2516MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    @C0064Am(aul = "89cc4ceec0ac4c2fc573d3edb2d36cd4", aum = 0)
    @C5566aOg
    private ProtectMissionScript aEX() {
        throw new aWi(new aCE(this, cFx, new Object[0]));
    }

    private WaypointDat aEd() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(cEl);
    }

    private C3438ra aEe() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cEn);
    }

    private float aEf() {
        return bFf().mo5608dq().mo3211m(cEp);
    }

    private float aEg() {
        return bFf().mo5608dq().mo3211m(cEr);
    }

    private boolean aEh() {
        return bFf().mo5608dq().mo3201h(cEt);
    }

    private I18NString aEi() {
        return (I18NString) bFf().mo5608dq().mo3214p(cEv);
    }

    private I18NString aEj() {
        return (I18NString) bFf().mo5608dq().mo3214p(cEx);
    }

    private I18NString aEk() {
        return (I18NString) bFf().mo5608dq().mo3214p(cEz);
    }

    private I18NString aEl() {
        return (I18NString) bFf().mo5608dq().mo3214p(cEB);
    }

    private int aEm() {
        return bFf().mo5608dq().mo3212n(cED);
    }

    private boolean aEn() {
        return bFf().mo5608dq().mo3201h(cEF);
    }

    private I18NString aEo() {
        return (I18NString) bFf().mo5608dq().mo3214p(cEH);
    }

    private boolean aEp() {
        return bFf().mo5608dq().mo3201h(cEJ);
    }

    private I18NString aEq() {
        return (I18NString) bFf().mo5608dq().mo3214p(cEL);
    }

    private boolean aEr() {
        return bFf().mo5608dq().mo3201h(cEN);
    }

    private boolean aEs() {
        return bFf().mo5608dq().mo3201h(cEP);
    }

    private boolean aEt() {
        return bFf().mo5608dq().mo3201h(cER);
    }

    private boolean aEu() {
        return bFf().mo5608dq().mo3201h(cET);
    }

    /* renamed from: ag */
    private void m2519ag(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cEn, raVar);
    }

    /* renamed from: c */
    private void m2520c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    /* renamed from: ce */
    private void m2521ce(boolean z) {
        bFf().mo5608dq().mo3153a(cEt, z);
    }

    /* renamed from: cf */
    private void m2522cf(boolean z) {
        bFf().mo5608dq().mo3153a(cEF, z);
    }

    /* renamed from: cg */
    private void m2523cg(boolean z) {
        bFf().mo5608dq().mo3153a(cEJ, z);
    }

    /* renamed from: ch */
    private void m2524ch(boolean z) {
        bFf().mo5608dq().mo3153a(cEN, z);
    }

    /* renamed from: ci */
    private void m2525ci(boolean z) {
        bFf().mo5608dq().mo3153a(cEP, z);
    }

    /* renamed from: cj */
    private void m2526cj(boolean z) {
        bFf().mo5608dq().mo3153a(cER, z);
    }

    /* renamed from: ck */
    private void m2527ck(boolean z) {
        bFf().mo5608dq().mo3153a(cET, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Escort Shotdown Message")
    @C0064Am(aul = "0b29419f68aa97148893a28191fecdde", aum = 0)
    @C5566aOg
    /* renamed from: cl */
    private void m2528cl(boolean z) {
        throw new aWi(new aCE(this, cEX, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Protected Shotdown Message")
    @C0064Am(aul = "3c092c54c04ecee7c709e95ff398b8a2", aum = 0)
    @C5566aOg
    /* renamed from: cn */
    private void m2529cn(boolean z) {
        throw new aWi(new aCE(this, cFb, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attacks in Waves")
    @C0064Am(aul = "e170cc193d4c3ef4ff62af7dbd8945b5", aum = 0)
    @C5566aOg
    /* renamed from: cp */
    private void m2530cp(boolean z) {
        throw new aWi(new aCE(this, cFk, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Attacker Spawn Countdown")
    @C0064Am(aul = "4b610defb2cfca5c2fae48ceb2956c2e", aum = 0)
    @C5566aOg
    /* renamed from: cr */
    private void m2531cr(boolean z) {
        throw new aWi(new aCE(this, cFz, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Attacker Spawn Message")
    @C0064Am(aul = "c388812d6adaafa42b8c71e9f3fa831f", aum = 0)
    @C5566aOg
    /* renamed from: ct */
    private void m2532ct(boolean z) {
        throw new aWi(new aCE(this, cFB, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Protected Spawn Countdown")
    @C0064Am(aul = "7c91de5efb588c4973884e49fdf97857", aum = 0)
    @C5566aOg
    /* renamed from: cv */
    private void m2533cv(boolean z) {
        throw new aWi(new aCE(this, cFD, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Protected Spawn Message")
    @C0064Am(aul = "e9f1bfff517a27dce11f6f45ed79c55b", aum = 0)
    @C5566aOg
    /* renamed from: cx */
    private void m2534cx(boolean z) {
        throw new aWi(new aCE(this, cFF, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "87559db523f82ccf902dd22812836956", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2535d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    /* renamed from: eC */
    private void m2536eC(float f) {
        bFf().mo5608dq().mo3150a(cEp, f);
    }

    /* renamed from: eD */
    private void m2537eD(float f) {
        bFf().mo5608dq().mo3150a(cEr, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to Spawn Attackers (sec)")
    @C0064Am(aul = "ecb0d76728b33bae3f406d7744a82853", aum = 0)
    @C5566aOg
    /* renamed from: eE */
    private void m2538eE(float f) {
        throw new aWi(new aCE(this, cFs, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to Spawn Protected (sec)")
    @C0064Am(aul = "18ce381d54666c8a2f528596e8c42aaf", aum = 0)
    @C5566aOg
    /* renamed from: eG */
    private void m2539eG(float f) {
        throw new aWi(new aCE(this, cFu, new Object[]{new Float(f)}));
    }

    /* renamed from: g */
    private void m2540g(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(cEl, cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Shotdown Message")
    @C0064Am(aul = "e24251e6af50f79e5823e5f054bfa960", aum = 0)
    @C5566aOg
    /* renamed from: gA */
    private void m2541gA(I18NString i18NString) {
        throw new aWi(new aCE(this, cEZ, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attackers Spawn Countdown Message")
    @C0064Am(aul = "16e9f7a8a3d3097f58b9e6778ff431f3", aum = 0)
    @C5566aOg
    /* renamed from: gC */
    private void m2542gC(I18NString i18NString) {
        throw new aWi(new aCE(this, cFd, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attackers Spawn Message")
    @C0064Am(aul = "0a05a2ae8115543fa16ff81112376544", aum = 0)
    @C5566aOg
    /* renamed from: gE */
    private void m2543gE(I18NString i18NString) {
        throw new aWi(new aCE(this, cFf, new Object[]{i18NString}));
    }

    /* renamed from: gG */
    private void m2544gG(int i) {
        bFf().mo5608dq().mo3183b(cED, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Spawn Countdown Message")
    @C0064Am(aul = "c578dea7f68415a4fd59e4a2a4a65df4", aum = 0)
    @C5566aOg
    /* renamed from: gG */
    private void m2545gG(I18NString i18NString) {
        throw new aWi(new aCE(this, cFm, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Minimum Surviving Protected")
    @C0064Am(aul = "5f0648f9ff3d3800be0bc61154aeda59", aum = 0)
    @C5566aOg
    /* renamed from: gH */
    private void m2546gH(int i) {
        throw new aWi(new aCE(this, cFw, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Spawn Message")
    @C0064Am(aul = "6c2c3bbcca8431374222fde9134f6de3", aum = 0)
    @C5566aOg
    /* renamed from: gI */
    private void m2547gI(I18NString i18NString) {
        throw new aWi(new aCE(this, cFo, new Object[]{i18NString}));
    }

    /* renamed from: gs */
    private void m2548gs(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cEv, i18NString);
    }

    /* renamed from: gt */
    private void m2549gt(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cEx, i18NString);
    }

    /* renamed from: gu */
    private void m2550gu(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cEz, i18NString);
    }

    /* renamed from: gv */
    private void m2551gv(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cEB, i18NString);
    }

    /* renamed from: gw */
    private void m2552gw(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cEH, i18NString);
    }

    /* renamed from: gx */
    private void m2553gx(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cEL, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Escort Shotdown Message")
    @C0064Am(aul = "723f5e69b644ce014b618122effbb311", aum = 0)
    @C5566aOg
    /* renamed from: gy */
    private void m2554gy(I18NString i18NString) {
        throw new aWi(new aCE(this, cEV, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Attackers Waypoints")
    @C0064Am(aul = "b55139bd6a70f79e09b70b10130ee06c", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m2555h(WaypointDat cDVar) {
        throw new aWi(new aCE(this, cFh, new Object[]{cDVar}));
    }

    @C0064Am(aul = "6c6e43952689af5479c1a1fbd7653cd8", aum = 0)
    /* renamed from: iR */
    private Mission m2556iR() {
        return aEY();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Attackers Waypoints")
    @C0064Am(aul = "1e245df8392c8cd541056ea7b19b0f22", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m2557j(WaypointDat cDVar) {
        throw new aWi(new aCE(this, cFi, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Waypoint")
    @C0064Am(aul = "f45d83a6805d7416cfc2726bb3abea17", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m2558l(WaypointDat cDVar) {
        throw new aWi(new aCE(this, cFq, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m2516MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo1678Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m2517Nb();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5993aeR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                return aEv();
            case 1:
                m2554gy((I18NString) args[0]);
                return null;
            case 2:
                return new Boolean(aEx());
            case 3:
                m2528cl(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return aEz();
            case 5:
                m2541gA((I18NString) args[0]);
                return null;
            case 6:
                return new Boolean(aEB());
            case 7:
                m2529cn(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                return m2517Nb();
            case 9:
                m2535d((StellarSystem) args[0]);
                return null;
            case 10:
                return aED();
            case 11:
                m2542gC((I18NString) args[0]);
                return null;
            case 12:
                return aEF();
            case 13:
                m2543gE((I18NString) args[0]);
                return null;
            case 14:
                return aEH();
            case 15:
                m2555h((WaypointDat) args[0]);
                return null;
            case 16:
                m2557j((WaypointDat) args[0]);
                return null;
            case 17:
                return new Boolean(aEJ());
            case 18:
                m2530cp(((Boolean) args[0]).booleanValue());
                return null;
            case 19:
                return aEL();
            case 20:
                m2545gG((I18NString) args[0]);
                return null;
            case 21:
                return aEN();
            case 22:
                m2547gI((I18NString) args[0]);
                return null;
            case 23:
                return aEP();
            case 24:
                m2558l((WaypointDat) args[0]);
                return null;
            case 25:
                return new Float(aER());
            case 26:
                m2538eE(((Float) args[0]).floatValue());
                return null;
            case 27:
                return new Float(aET());
            case 28:
                m2539eG(((Float) args[0]).floatValue());
                return null;
            case 29:
                return new Integer(aEV());
            case 30:
                m2546gH(((Integer) args[0]).intValue());
                return null;
            case 31:
                return aEX();
            case 32:
                return new Boolean(aEZ());
            case 33:
                m2531cr(((Boolean) args[0]).booleanValue());
                return null;
            case 34:
                return new Boolean(aFb());
            case 35:
                m2532ct(((Boolean) args[0]).booleanValue());
                return null;
            case 36:
                return new Boolean(aFd());
            case 37:
                m2533cv(((Boolean) args[0]).booleanValue());
                return null;
            case 38:
                return new Boolean(aFf());
            case 39:
                m2534cx(((Boolean) args[0]).booleanValue());
                return null;
            case 40:
                m2516MZ();
                return null;
            case 41:
                return m2556iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Shotdown Message")
    public I18NString aEA() {
        switch (bFf().mo6893i(cEY)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cEY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cEY, new Object[0]));
                break;
        }
        return aEz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Protected Shotdown Message")
    public boolean aEC() {
        switch (bFf().mo6893i(cFa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cFa, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFa, new Object[0]));
                break;
        }
        return aEB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attackers Spawn Countdown Message")
    public I18NString aEE() {
        switch (bFf().mo6893i(cFc)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cFc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cFc, new Object[0]));
                break;
        }
        return aED();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attackers Spawn Message")
    public I18NString aEG() {
        switch (bFf().mo6893i(cFe)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cFe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cFe, new Object[0]));
                break;
        }
        return aEF();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Attackers Waypoints")
    public C3438ra<WaypointDat> aEI() {
        switch (bFf().mo6893i(cFg)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, cFg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cFg, new Object[0]));
                break;
        }
        return aEH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attacks in Waves")
    public boolean aEK() {
        switch (bFf().mo6893i(cFj)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cFj, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFj, new Object[0]));
                break;
        }
        return aEJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Spawn Countdown Message")
    public I18NString aEM() {
        switch (bFf().mo6893i(cFl)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cFl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cFl, new Object[0]));
                break;
        }
        return aEL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Spawn Message")
    public I18NString aEO() {
        switch (bFf().mo6893i(cFn)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cFn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cFn, new Object[0]));
                break;
        }
        return aEN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Waypoint")
    public WaypointDat aEQ() {
        switch (bFf().mo6893i(cFp)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, cFp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cFp, new Object[0]));
                break;
        }
        return aEP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to Spawn Attackers (sec)")
    public float aES() {
        switch (bFf().mo6893i(cFr)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cFr, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFr, new Object[0]));
                break;
        }
        return aER();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to Spawn Protected (sec)")
    public float aEU() {
        switch (bFf().mo6893i(cFt)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cFt, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFt, new Object[0]));
                break;
        }
        return aET();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Minimum Surviving Protected")
    public int aEW() {
        switch (bFf().mo6893i(cFv)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cFv, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFv, new Object[0]));
                break;
        }
        return aEV();
    }

    @C5566aOg
    public ProtectMissionScript aEY() {
        switch (bFf().mo6893i(cFx)) {
            case 0:
                return null;
            case 2:
                return (ProtectMissionScript) bFf().mo5606d(new aCE(this, cFx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cFx, new Object[0]));
                break;
        }
        return aEX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Escort Shotdown Message")
    public I18NString aEw() {
        switch (bFf().mo6893i(cEU)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cEU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cEU, new Object[0]));
                break;
        }
        return aEv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Escort Shotdown Message")
    public boolean aEy() {
        switch (bFf().mo6893i(cEW)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cEW, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cEW, new Object[0]));
                break;
        }
        return aEx();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Attacker Spawn Countdown")
    public boolean aFa() {
        switch (bFf().mo6893i(cFy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cFy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFy, new Object[0]));
                break;
        }
        return aEZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Attacker Spawn Message")
    public boolean aFc() {
        switch (bFf().mo6893i(cFA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cFA, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFA, new Object[0]));
                break;
        }
        return aFb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Protected Spawn Countdown")
    public boolean aFe() {
        switch (bFf().mo6893i(cFC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cFC, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFC, new Object[0]));
                break;
        }
        return aFd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Protected Spawn Message")
    public boolean aFg() {
        switch (bFf().mo6893i(cFE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cFE, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cFE, new Object[0]));
                break;
        }
        return aFf();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Escort Shotdown Message")
    @C5566aOg
    /* renamed from: cm */
    public void mo1698cm(boolean z) {
        switch (bFf().mo6893i(cEX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cEX, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cEX, new Object[]{new Boolean(z)}));
                break;
        }
        m2528cl(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Protected Shotdown Message")
    @C5566aOg
    /* renamed from: co */
    public void mo1699co(boolean z) {
        switch (bFf().mo6893i(cFb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFb, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFb, new Object[]{new Boolean(z)}));
                break;
        }
        m2529cn(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attacks in Waves")
    @C5566aOg
    /* renamed from: cq */
    public void mo1700cq(boolean z) {
        switch (bFf().mo6893i(cFk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFk, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFk, new Object[]{new Boolean(z)}));
                break;
        }
        m2530cp(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Attacker Spawn Countdown")
    @C5566aOg
    /* renamed from: cs */
    public void mo1701cs(boolean z) {
        switch (bFf().mo6893i(cFz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFz, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFz, new Object[]{new Boolean(z)}));
                break;
        }
        m2531cr(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Attacker Spawn Message")
    @C5566aOg
    /* renamed from: cu */
    public void mo1702cu(boolean z) {
        switch (bFf().mo6893i(cFB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFB, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFB, new Object[]{new Boolean(z)}));
                break;
        }
        m2532ct(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Protected Spawn Countdown")
    @C5566aOg
    /* renamed from: cw */
    public void mo1703cw(boolean z) {
        switch (bFf().mo6893i(cFD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFD, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFD, new Object[]{new Boolean(z)}));
                break;
        }
        m2533cv(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shows Protected Spawn Message")
    @C5566aOg
    /* renamed from: cy */
    public void mo1704cy(boolean z) {
        switch (bFf().mo6893i(cFF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFF, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFF, new Object[]{new Boolean(z)}));
                break;
        }
        m2534cx(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo1705e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m2535d(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to Spawn Attackers (sec)")
    @C5566aOg
    /* renamed from: eF */
    public void mo1706eF(float f) {
        switch (bFf().mo6893i(cFs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFs, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFs, new Object[]{new Float(f)}));
                break;
        }
        m2538eE(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to Spawn Protected (sec)")
    @C5566aOg
    /* renamed from: eH */
    public void mo1707eH(float f) {
        switch (bFf().mo6893i(cFu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFu, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFu, new Object[]{new Float(f)}));
                break;
        }
        m2539eG(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Shotdown Message")
    @C5566aOg
    /* renamed from: gB */
    public void mo1708gB(I18NString i18NString) {
        switch (bFf().mo6893i(cEZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cEZ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cEZ, new Object[]{i18NString}));
                break;
        }
        m2541gA(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attackers Spawn Countdown Message")
    @C5566aOg
    /* renamed from: gD */
    public void mo1709gD(I18NString i18NString) {
        switch (bFf().mo6893i(cFd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFd, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFd, new Object[]{i18NString}));
                break;
        }
        m2542gC(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attackers Spawn Message")
    @C5566aOg
    /* renamed from: gF */
    public void mo1710gF(I18NString i18NString) {
        switch (bFf().mo6893i(cFf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFf, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFf, new Object[]{i18NString}));
                break;
        }
        m2543gE(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Spawn Countdown Message")
    @C5566aOg
    /* renamed from: gH */
    public void mo1711gH(I18NString i18NString) {
        switch (bFf().mo6893i(cFm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFm, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFm, new Object[]{i18NString}));
                break;
        }
        m2545gG(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Minimum Surviving Protected")
    @C5566aOg
    /* renamed from: gI */
    public void mo1712gI(int i) {
        switch (bFf().mo6893i(cFw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFw, new Object[]{new Integer(i)}));
                break;
        }
        m2546gH(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Spawn Message")
    @C5566aOg
    /* renamed from: gJ */
    public void mo1713gJ(I18NString i18NString) {
        switch (bFf().mo6893i(cFo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFo, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFo, new Object[]{i18NString}));
                break;
        }
        m2547gI(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Escort Shotdown Message")
    @C5566aOg
    /* renamed from: gz */
    public void mo1714gz(I18NString i18NString) {
        switch (bFf().mo6893i(cEV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cEV, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cEV, new Object[]{i18NString}));
                break;
        }
        m2554gy(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Attackers Waypoints")
    @C5566aOg
    /* renamed from: i */
    public void mo1715i(WaypointDat cDVar) {
        switch (bFf().mo6893i(cFh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFh, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFh, new Object[]{cDVar}));
                break;
        }
        m2555h(cDVar);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f438xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f438xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f438xF, new Object[0]));
                break;
        }
        return m2556iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Attackers Waypoints")
    @C5566aOg
    /* renamed from: k */
    public void mo1717k(WaypointDat cDVar) {
        switch (bFf().mo6893i(cFi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFi, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFi, new Object[]{cDVar}));
                break;
        }
        m2557j(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Protected Waypoint")
    @C5566aOg
    /* renamed from: m */
    public void mo1718m(WaypointDat cDVar) {
        switch (bFf().mo6893i(cFq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFq, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFq, new Object[]{cDVar}));
                break;
        }
        m2558l(cDVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Escort Shotdown Message")
    @C0064Am(aul = "c4ecb3110f2c414b269e0fe1b1301823", aum = 0)
    private I18NString aEv() {
        return aEq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Escort Shotdown Message")
    @C0064Am(aul = "c07aaeb97339f0b4f0079120f4160b98", aum = 0)
    private boolean aEx() {
        return aEp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Shotdown Message")
    @C0064Am(aul = "8aee2e7c712126ed839b744855c187a1", aum = 0)
    private I18NString aEz() {
        return aEo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Protected Shotdown Message")
    @C0064Am(aul = "e748681733490349b64812c44072182c", aum = 0)
    private boolean aEB() {
        return aEn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "5f7426ba939f7c228c0d5ece8c7b79cb", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m2517Nb() {
        return m2515MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attackers Spawn Countdown Message")
    @C0064Am(aul = "11be632fe5e12b5d746125c4e1d01611", aum = 0)
    private I18NString aED() {
        return aEk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attackers Spawn Message")
    @C0064Am(aul = "e3fdce8a0a870d46131d75a24182af8d", aum = 0)
    private I18NString aEF() {
        return aEl();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Attackers Waypoints")
    @C0064Am(aul = "c66b455cd3e4a1dc64cd04aa10701f10", aum = 0)
    private C3438ra<WaypointDat> aEH() {
        return aEe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attacks in Waves")
    @C0064Am(aul = "396c455933d3561e8a3c4e84acf6eabf", aum = 0)
    private boolean aEJ() {
        return aEh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Spawn Countdown Message")
    @C0064Am(aul = "185d7a87dfe2e9a5d2134a9c98e1f687", aum = 0)
    private I18NString aEL() {
        return aEi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Spawn Message")
    @C0064Am(aul = "8e27b0d8be69072ebdfc15a3ee573aaa", aum = 0)
    private I18NString aEN() {
        return aEj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Protected Waypoint")
    @C0064Am(aul = "8ecd3db593467910cda4a3486b9e7f9b", aum = 0)
    private WaypointDat aEP() {
        return aEd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to Spawn Attackers (sec)")
    @C0064Am(aul = "a305f06889f4188c2b1fd11997de656f", aum = 0)
    private float aER() {
        return aEg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to Spawn Protected (sec)")
    @C0064Am(aul = "8c991d96704cba4cbfd8c9814a3c8197", aum = 0)
    private float aET() {
        return aEf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Minimum Surviving Protected")
    @C0064Am(aul = "c375bda2a8e30ae18ca8bb3cd5fbfa6a", aum = 0)
    private int aEV() {
        return aEm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Attacker Spawn Countdown")
    @C0064Am(aul = "1b3ffc5de79cac0a19499d9625152c10", aum = 0)
    private boolean aEZ() {
        return aEr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Attacker Spawn Message")
    @C0064Am(aul = "57688423313a3d7d789b234146d8c2f6", aum = 0)
    private boolean aFb() {
        return aEt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Protected Spawn Countdown")
    @C0064Am(aul = "3f0d07fdb294367c9844ce06db25bd6e", aum = 0)
    private boolean aFd() {
        return aEs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shows Protected Spawn Message")
    @C0064Am(aul = "4dbcc5a6788e1d04b47429dfb0c88801", aum = 0)
    private boolean aFf() {
        return aEu();
    }
}
