package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.missiontemplate.WaypointDat;
import logic.baa.*;
import logic.data.mbean.C2118cB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.axG */
/* compiled from: a */
public class AssaultMissionWave extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5541bL = null;
    /* renamed from: bM */
    public static final C5663aRz f5542bM = null;
    /* renamed from: bN */
    public static final C2491fm f5543bN = null;
    /* renamed from: bO */
    public static final C2491fm f5544bO = null;
    /* renamed from: bP */
    public static final C2491fm f5545bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5546bQ = null;
    public static final C5663aRz gQl = null;
    public static final C5663aRz gQm = null;
    public static final C5663aRz gQn = null;
    public static final C2491fm gQo = null;
    public static final C2491fm gQp = null;
    public static final C2491fm gQq = null;
    public static final C2491fm gQr = null;
    public static final C2491fm gQs = null;
    public static final C2491fm gQt = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "df24ba68526a248ce0f013824d4e04de", aum = 3)

    /* renamed from: bK */
    private static UUID f5540bK;
    @C0064Am(aul = "d40fc21d2dfba45745a51f5a6f4349a6", aum = 4)
    private static String handle;
    @C0064Am(aul = "53aa18add6286cb7c306440b7d97ca09", aum = 0)

    /* renamed from: vu */
    private static WaypointDat f5547vu;
    @C0064Am(aul = "be08965ed8b3ecf3388fc4e1c95a5136", aum = 1)

    /* renamed from: vv */
    private static C2007a f5548vv;
    @C0064Am(aul = "d22b1997bafc0a804632b95d5b3108c2", aum = 2)

    /* renamed from: vw */
    private static long f5549vw;

    static {
        m27008V();
    }

    public AssaultMissionWave() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AssaultMissionWave(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27008V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(AssaultMissionWave.class, "53aa18add6286cb7c306440b7d97ca09", i);
        gQl = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AssaultMissionWave.class, "be08965ed8b3ecf3388fc4e1c95a5136", i2);
        gQm = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AssaultMissionWave.class, "d22b1997bafc0a804632b95d5b3108c2", i3);
        gQn = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AssaultMissionWave.class, "df24ba68526a248ce0f013824d4e04de", i4);
        f5541bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AssaultMissionWave.class, "d40fc21d2dfba45745a51f5a6f4349a6", i5);
        f5542bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(AssaultMissionWave.class, "dc5305ff6378286290b6af82c097ce74", i7);
        f5543bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(AssaultMissionWave.class, "0a6630860aacdb917e598f63df99bcb1", i8);
        f5544bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(AssaultMissionWave.class, "69c594ffe54500e80fa84e9f1960dd76", i9);
        f5545bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(AssaultMissionWave.class, "685785bd30bec4197b0df3730f4e3ee9", i10);
        f5546bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(AssaultMissionWave.class, "eaeb05bcf851fd5089a470fadb37bce6", i11);
        gQo = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(AssaultMissionWave.class, "425a5521c90acb3973051d584463e0a3", i12);
        gQp = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(AssaultMissionWave.class, "a7d4c8f545ff8b5cd2e7d9f39e62c30b", i13);
        gQq = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(AssaultMissionWave.class, "6d3ed886dad0144caa7e684e47fce306", i14);
        gQr = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(AssaultMissionWave.class, "487d47f244b81f8bfb11f9fdf00dab1e", i15);
        gQs = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(AssaultMissionWave.class, "aaf420ce932334c50de3c5261393ad39", i16);
        gQt = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AssaultMissionWave.class, C2118cB.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m27006B(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(gQl, cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wave Waypoint")
    @C0064Am(aul = "425a5521c90acb3973051d584463e0a3", aum = 0)
    @C5566aOg
    /* renamed from: C */
    private void m27007C(WaypointDat cDVar) {
        throw new aWi(new aCE(this, gQp, new Object[]{cDVar}));
    }

    /* renamed from: a */
    private void m27009a(C2007a aVar) {
        bFf().mo5608dq().mo3197f(gQm, aVar);
    }

    /* renamed from: a */
    private void m27010a(String str) {
        bFf().mo5608dq().mo3197f(f5542bM, str);
    }

    /* renamed from: a */
    private void m27011a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5541bL, uuid);
    }

    /* renamed from: an */
    private UUID m27012an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5541bL);
    }

    /* renamed from: ao */
    private String m27013ao() {
        return (String) bFf().mo5608dq().mo3214p(f5542bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wave type")
    @C0064Am(aul = "aaf420ce932334c50de3c5261393ad39", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m27016b(C2007a aVar) {
        throw new aWi(new aCE(this, gQt, new Object[]{aVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "685785bd30bec4197b0df3730f4e3ee9", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m27017b(String str) {
        throw new aWi(new aCE(this, f5546bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m27019c(UUID uuid) {
        switch (bFf().mo6893i(f5544bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5544bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5544bO, new Object[]{uuid}));
                break;
        }
        m27018b(uuid);
    }

    private WaypointDat cCN() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(gQl);
    }

    private C2007a cCO() {
        return (C2007a) bFf().mo5608dq().mo3214p(gQm);
    }

    private long cCP() {
        return bFf().mo5608dq().mo3213o(gQn);
    }

    /* renamed from: jb */
    private void m27020jb(long j) {
        bFf().mo5608dq().mo3184b(gQn, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time offset (seconds)")
    @C0064Am(aul = "6d3ed886dad0144caa7e684e47fce306", aum = 0)
    @C5566aOg
    /* renamed from: jc */
    private void m27021jc(long j) {
        throw new aWi(new aCE(this, gQr, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wave Waypoint")
    @C5566aOg
    /* renamed from: D */
    public void mo16740D(WaypointDat cDVar) {
        switch (bFf().mo6893i(gQp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQp, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQp, new Object[]{cDVar}));
                break;
        }
        m27007C(cDVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2118cB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m27014ap();
            case 1:
                m27018b((UUID) args[0]);
                return null;
            case 2:
                return m27015ar();
            case 3:
                m27017b((String) args[0]);
                return null;
            case 4:
                return cCQ();
            case 5:
                m27007C((WaypointDat) args[0]);
                return null;
            case 6:
                return new Long(cCS());
            case 7:
                m27021jc(((Long) args[0]).longValue());
                return null;
            case 8:
                return cCU();
            case 9:
                m27016b((C2007a) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5543bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5543bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5543bN, new Object[0]));
                break;
        }
        return m27014ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wave type")
    @C5566aOg
    /* renamed from: c */
    public void mo16741c(C2007a aVar) {
        switch (bFf().mo6893i(gQt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQt, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQt, new Object[]{aVar}));
                break;
        }
        m27016b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wave Waypoint")
    public WaypointDat cCR() {
        switch (bFf().mo6893i(gQo)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, gQo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gQo, new Object[0]));
                break;
        }
        return cCQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time offset (seconds)")
    public long cCT() {
        switch (bFf().mo6893i(gQq)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gQq, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gQq, new Object[0]));
                break;
        }
        return cCS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wave type")
    public C2007a cCV() {
        switch (bFf().mo6893i(gQs)) {
            case 0:
                return null;
            case 2:
                return (C2007a) bFf().mo5606d(new aCE(this, gQs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gQs, new Object[0]));
                break;
        }
        return cCU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5545bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5545bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5545bP, new Object[0]));
                break;
        }
        return m27015ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5546bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5546bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5546bQ, new Object[]{str}));
                break;
        }
        m27017b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time offset (seconds)")
    @C5566aOg
    /* renamed from: jd */
    public void mo16745jd(long j) {
        switch (bFf().mo6893i(gQr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQr, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQr, new Object[]{new Long(j)}));
                break;
        }
        m27021jc(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "dc5305ff6378286290b6af82c097ce74", aum = 0)
    /* renamed from: ap */
    private UUID m27014ap() {
        return m27012an();
    }

    @C0064Am(aul = "0a6630860aacdb917e598f63df99bcb1", aum = 0)
    /* renamed from: b */
    private void m27018b(UUID uuid) {
        m27011a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "69c594ffe54500e80fa84e9f1960dd76", aum = 0)
    /* renamed from: ar */
    private String m27015ar() {
        return m27013ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wave Waypoint")
    @C0064Am(aul = "eaeb05bcf851fd5089a470fadb37bce6", aum = 0)
    private WaypointDat cCQ() {
        return cCN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time offset (seconds)")
    @C0064Am(aul = "a7d4c8f545ff8b5cd2e7d9f39e62c30b", aum = 0)
    private long cCS() {
        return cCP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wave type")
    @C0064Am(aul = "487d47f244b81f8bfb11f9fdf00dab1e", aum = 0)
    private C2007a cCU() {
        return cCO();
    }

    /* renamed from: a.axG$a */
    public enum C2007a {
        FRIEND_DEFENCE,
        FRIEND_OFFENCE,
        ENEMY_DEFENCE,
        ENEMY_OFFENCE
    }
}
