package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.network.message.serializable.C5512aMe;
import game.script.ItemTypeTableItem;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.scripting.ItemDeliveryMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.missiontemplate.WaypointDat;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.mbean.C6835aub;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.1.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.ItemDeliveryMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.aOO */
/* compiled from: a */
public class ItemDeliveryMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f3458Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C5663aRz bwE = null;
    public static final C5663aRz bwF = null;
    public static final C2491fm bwG = null;
    public static final C2491fm bwH = null;
    public static final C2491fm bwI = null;
    public static final C2491fm bwJ = null;
    public static final C2491fm bwK = null;
    public static final C2491fm bwL = null;
    public static final C2491fm ebC = null;
    public static final C2491fm ebD = null;
    public static final C2491fm ebH = null;
    public static final C2491fm ebI = null;
    public static final C5663aRz ebp = null;
    public static final C5663aRz ebt = null;
    public static final C2491fm iAe = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f3462xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "65df85ddc6c7e54714649e8ee5c5c00f", aum = 4)
    private static WaypointDat ebo;
    @C0064Am(aul = "6603a34789fb75a39900a12c1bfb7a65", aum = 0)
    private static String ebs;
    @C0064Am(aul = "46bd32fc5407a3c6cb8a6b785fa4bedb", aum = 1)

    /* renamed from: iI */
    private static C3438ra<ItemTypeTableItem> f3459iI;
    @C0064Am(aul = "08958227a3ffa684b003dc2303f5a767", aum = 2)

    /* renamed from: iJ */
    private static C3438ra<ItemTypeTableItem> f3460iJ;
    @C0064Am(aul = "c51c2913ad9f296a4b06bb235fdcf766", aum = 3)

    /* renamed from: rI */
    private static StellarSystem f3461rI;

    static {
        m16843V();
    }

    public ItemDeliveryMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemDeliveryMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16843V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 5;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 16;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ItemDeliveryMissionScriptTemplate.class, "6603a34789fb75a39900a12c1bfb7a65", i);
        ebt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemDeliveryMissionScriptTemplate.class, "46bd32fc5407a3c6cb8a6b785fa4bedb", i2);
        bwE = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemDeliveryMissionScriptTemplate.class, "08958227a3ffa684b003dc2303f5a767", i3);
        bwF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemDeliveryMissionScriptTemplate.class, "c51c2913ad9f296a4b06bb235fdcf766", i4);
        aBo = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ItemDeliveryMissionScriptTemplate.class, "65df85ddc6c7e54714649e8ee5c5c00f", i5);
        ebp = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i7 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 16)];
        C2491fm a = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "f7f740ffc1d8dea5ca5c5552b7e5f114", i7);
        aBr = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "2dba2b35da3c63bda771c20fb4ab73e0", i8);
        aBs = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "9a85611dab2573574aa3bfb6f623d305", i9);
        aBt = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "e838a1612b4002c978b7add67cb3391e", i10);
        ebC = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "66f880f7cf37d64acb1a85262337f78b", i11);
        ebD = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "1d964cd8a13569e60d5d4327965409c4", i12);
        bwG = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "a575fb9f4d4550dc1f8157eb83b614e5", i13);
        bwH = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "607651766122ae4d77ba3d5e6fb94929", i14);
        bwI = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "226c2513955322fa4480c041e743f256", i15);
        bwJ = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "41fd568ff05b4d2e085e515b04f419ef", i16);
        bwK = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "a6a872f7e3a47336d8567ba7df1ea09d", i17);
        bwL = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "3adc7febd8c0325ec138f1a105e28cdd", i18);
        ebH = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "44fdbee4a864dcd86655bbdf6e4e9033", i19);
        ebI = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "bf8f6836cdfd2d73c412219f8338eb0f", i20);
        iAe = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "95d0fc685e568821ea21bed0c68fad3c", i21);
        f3458Do = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(ItemDeliveryMissionScriptTemplate.class, "eeeaed7a2dc496f884326ea48c8a2b86", i22);
        f3462xF = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemDeliveryMissionScriptTemplate.class, C6835aub.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m16838MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "f7f740ffc1d8dea5ca5c5552b7e5f114", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m16839MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: P */
    private void m16841P(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bwE, raVar);
    }

    /* renamed from: Q */
    private void m16842Q(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bwF, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Delivery Item Types")
    @C0064Am(aul = "1d964cd8a13569e60d5d4327965409c4", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m16845a(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bwG, new Object[]{zlVar}));
    }

    private C3438ra ajB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bwE);
    }

    private C3438ra ajC() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bwF);
    }

    private WaypointDat brO() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(ebp);
    }

    private String brQ() {
        return (String) bFf().mo5608dq().mo3214p(ebt);
    }

    /* renamed from: c */
    private void m16846c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Delivery Item Types")
    @C0064Am(aul = "a575fb9f4d4550dc1f8157eb83b614e5", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m16847c(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bwH, new Object[]{zlVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "9a85611dab2573574aa3bfb6f623d305", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m16848d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    @C0064Am(aul = "bf8f6836cdfd2d73c412219f8338eb0f", aum = 0)
    @C5566aOg
    private ItemDeliveryMissionScript dnS() {
        throw new aWi(new aCE(this, iAe, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Receive Item Types")
    @C0064Am(aul = "226c2513955322fa4480c041e743f256", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m16849e(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bwJ, new Object[]{zlVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Receive Item Types")
    @C0064Am(aul = "41fd568ff05b4d2e085e515b04f419ef", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m16850g(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bwK, new Object[]{zlVar}));
    }

    /* renamed from: gv */
    private void m16851gv(String str) {
        bFf().mo5608dq().mo3197f(ebt, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "SpeechHandle")
    @C0064Am(aul = "44fdbee4a864dcd86655bbdf6e4e9033", aum = 0)
    @C5566aOg
    /* renamed from: gw */
    private void m16852gw(String str) {
        throw new aWi(new aCE(this, ebI, new Object[]{str}));
    }

    @C0064Am(aul = "eeeaed7a2dc496f884326ea48c8a2b86", aum = 0)
    /* renamed from: iR */
    private Mission m16853iR() {
        return dnT();
    }

    /* renamed from: s */
    private void m16854s(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(ebp, cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint")
    @C0064Am(aul = "66f880f7cf37d64acb1a85262337f78b", aum = 0)
    @C5566aOg
    /* renamed from: t */
    private void m16855t(WaypointDat cDVar) {
        throw new aWi(new aCE(this, ebD, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m16839MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo10584Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m16840Nb();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6835aub(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m16839MZ();
                return null;
            case 1:
                return m16840Nb();
            case 2:
                m16848d((StellarSystem) args[0]);
                return null;
            case 3:
                return brX();
            case 4:
                m16855t((WaypointDat) args[0]);
                return null;
            case 5:
                m16845a((ItemTypeTableItem) args[0]);
                return null;
            case 6:
                m16847c((ItemTypeTableItem) args[0]);
                return null;
            case 7:
                return ajD();
            case 8:
                m16849e((ItemTypeTableItem) args[0]);
                return null;
            case 9:
                m16850g((ItemTypeTableItem) args[0]);
                return null;
            case 10:
                return ajF();
            case 11:
                return bsd();
            case 12:
                m16852gw((String) args[0]);
                return null;
            case 13:
                return dnS();
            case 14:
                m16844a((C0665JT) args[0]);
                return null;
            case 15:
                return m16853iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Delivery Item Types")
    public List<ItemTypeTableItem> ajE() {
        switch (bFf().mo6893i(bwI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bwI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwI, new Object[0]));
                break;
        }
        return ajD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Receive Item Types")
    public List<ItemTypeTableItem> ajG() {
        switch (bFf().mo6893i(bwL)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bwL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwL, new Object[0]));
                break;
        }
        return ajF();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f3458Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3458Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3458Do, new Object[]{jt}));
                break;
        }
        m16844a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Delivery Item Types")
    @C5566aOg
    /* renamed from: b */
    public void mo10587b(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwG, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwG, new Object[]{zlVar}));
                break;
        }
        m16845a(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint")
    public WaypointDat brY() {
        switch (bFf().mo6893i(ebC)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, ebC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebC, new Object[0]));
                break;
        }
        return brX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "SpeechHandle")
    public String bse() {
        switch (bFf().mo6893i(ebH)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ebH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ebH, new Object[0]));
                break;
        }
        return bsd();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Delivery Item Types")
    @C5566aOg
    /* renamed from: d */
    public void mo10590d(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwH, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwH, new Object[]{zlVar}));
                break;
        }
        m16847c(zlVar);
    }

    @C5566aOg
    public ItemDeliveryMissionScript dnT() {
        switch (bFf().mo6893i(iAe)) {
            case 0:
                return null;
            case 2:
                return (ItemDeliveryMissionScript) bFf().mo5606d(new aCE(this, iAe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iAe, new Object[0]));
                break;
        }
        return dnS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo10592e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m16848d(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Receive Item Types")
    @C5566aOg
    /* renamed from: f */
    public void mo10593f(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwJ, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwJ, new Object[]{zlVar}));
                break;
        }
        m16849e(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "SpeechHandle")
    @C5566aOg
    /* renamed from: gx */
    public void mo10594gx(String str) {
        switch (bFf().mo6893i(ebI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebI, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebI, new Object[]{str}));
                break;
        }
        m16852gw(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Receive Item Types")
    @C5566aOg
    /* renamed from: h */
    public void mo10595h(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwK, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwK, new Object[]{zlVar}));
                break;
        }
        m16850g(zlVar);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f3462xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f3462xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3462xF, new Object[0]));
                break;
        }
        return m16853iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint")
    @C5566aOg
    /* renamed from: u */
    public void mo10596u(WaypointDat cDVar) {
        switch (bFf().mo6893i(ebD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebD, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebD, new Object[]{cDVar}));
                break;
        }
        m16855t(cDVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "2dba2b35da3c63bda771c20fb4ab73e0", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m16840Nb() {
        return m16838MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint")
    @C0064Am(aul = "e838a1612b4002c978b7add67cb3391e", aum = 0)
    private WaypointDat brX() {
        return brO();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Delivery Item Types")
    @C0064Am(aul = "607651766122ae4d77ba3d5e6fb94929", aum = 0)
    private List<ItemTypeTableItem> ajD() {
        return Collections.unmodifiableList(ajB());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Receive Item Types")
    @C0064Am(aul = "a6a872f7e3a47336d8567ba7df1ea09d", aum = 0)
    private List<ItemTypeTableItem> ajF() {
        return Collections.unmodifiableList(ajC());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "SpeechHandle")
    @C0064Am(aul = "3adc7febd8c0325ec138f1a105e28cdd", aum = 0)
    private String bsd() {
        return brQ();
    }

    @C0064Am(aul = "95d0fc685e568821ea21bed0c68fad3c", aum = 0)
    /* renamed from: a */
    private void m16844a(C0665JT jt) {
        ItemType jCVar = null;
        if ("".equals(jt.getVersion())) {
            int intValue = ((Integer) jt.get("time")).intValue();
            if (intValue != 0) {
                super.mo7895wQ(intValue);
            }
        } else if ("1.0.0".equals(jt.getVersion())) {
            C5512aMe ame = (C5512aMe) jt.get("deliveryItemType");
            ItemType jCVar2 = ame != null ? (ItemType) ame.baw() : null;
            C5512aMe ame2 = (C5512aMe) jt.get("receiveItemType");
            if (ame2 != null) {
                jCVar = (ItemType) ame2.baw();
            }
            if (jCVar2 != null) {
                int intValue2 = ((Integer) jt.get("deliveryQuantity")).intValue();
                ItemTypeTableItem zlVar = (ItemTypeTableItem) bFf().mo6865M(ItemTypeTableItem.class);
                zlVar.mo10S();
                zlVar.setAmount(intValue2);
                zlVar.mo23386c(jCVar2);
                ajB().add(zlVar);
            }
            if (jCVar != null) {
                int intValue3 = ((Integer) jt.get("receiveQuantity")).intValue();
                ItemTypeTableItem zlVar2 = (ItemTypeTableItem) bFf().mo6865M(ItemTypeTableItem.class);
                zlVar2.mo10S();
                zlVar2.setAmount(intValue3);
                zlVar2.mo23386c(jCVar);
                ajC().add(zlVar2);
            }
        }
    }
}
