package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.mission.scripting.PatrolMissionScript;
import game.script.missiontemplate.WaypointDat;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.mbean.C1425Us;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.PatrolMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.mR */
/* compiled from: a */
public class PatrolMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f8649Do = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C5663aRz aBq = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C2491fm aBu = null;
    public static final C5663aRz aFT = null;
    public static final C5663aRz aFV = null;
    public static final C5663aRz aFX = null;
    public static final C2491fm aFY = null;
    public static final C2491fm aFZ = null;
    public static final C2491fm aGa = null;
    public static final C2491fm aGb = null;
    public static final C2491fm aGc = null;
    public static final C2491fm aGd = null;
    public static final C2491fm aGe = null;
    public static final C2491fm aGf = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f8651xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "16c626cc7a70a52f36339a4495408f70", aum = 1)
    private static C3438ra<WaypointDat> aBp;
    @C0064Am(aul = "7904f42eb5a10c85b57d7e6c18f0461a", aum = 2)
    private static boolean aFS;
    @C0064Am(aul = "adc8cf7dc1542e6552e257e6d855d187", aum = 3)
    private static boolean aFU;
    @C0064Am(aul = "33824a6539c46ecba93e1145223d5a48", aum = 4)
    private static int aFW;
    @C0064Am(aul = "0673405582bd29f0bad90d5e1da64068", aum = 0)

    /* renamed from: rI */
    private static StellarSystem f8650rI;

    static {
        m35719V();
    }

    public PatrolMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PatrolMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35719V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 5;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 15;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(PatrolMissionScriptTemplate.class, "0673405582bd29f0bad90d5e1da64068", i);
        aBo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PatrolMissionScriptTemplate.class, "16c626cc7a70a52f36339a4495408f70", i2);
        aBq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PatrolMissionScriptTemplate.class, "7904f42eb5a10c85b57d7e6c18f0461a", i3);
        aFT = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PatrolMissionScriptTemplate.class, "adc8cf7dc1542e6552e257e6d855d187", i4);
        aFV = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PatrolMissionScriptTemplate.class, "33824a6539c46ecba93e1145223d5a48", i5);
        aFX = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i7 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 15)];
        C2491fm a = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "98ccf943d1a263da21bc51fd04b0396a", i7);
        aBr = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "d7f3b87b31033ca746008429fa4d92d2", i8);
        aFY = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "790ac8c88020033eb3a8a20c825bf036", i9);
        aFZ = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "9b980734a831a78399a931c3d815e564", i10);
        aGa = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "d6b353c5c4804dd7ad46468526a512d7", i11);
        aBs = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "f2fbc71ef1f35c40823c83abed694416", i12);
        aBt = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "52b95a787c5c75a0d83813a3bacc270c", i13);
        aBu = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "66262b21ab2d54c0e57bd0ddb0cc0195", i14);
        aGb = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "6b234812d3100b2b17084249312084cb", i15);
        aGc = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "dbcf1f47ff43690387a82626bffb90b5", i16);
        aGd = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "b8ae684482d4445fafe00fba28d9d23a", i17);
        aGe = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "dd404e851e1deb458b9148f7695996dc", i18);
        aGf = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "23ed96ad2e1f6a14db2b9615de838d77", i19);
        f8649Do = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "054810bd61b30b8235c286dbe8c18de0", i20);
        _f_onResurrect_0020_0028_0029V = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(PatrolMissionScriptTemplate.class, "76be38dc49654cd19f6183820629321d", i21);
        f8651xF = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PatrolMissionScriptTemplate.class, C1425Us.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m35707MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    /* renamed from: MY */
    private C3438ra m35708MY() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aBq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "98ccf943d1a263da21bc51fd04b0396a", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m35709MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: Pn */
    private boolean m35712Pn() {
        return bFf().mo5608dq().mo3201h(aFT);
    }

    /* renamed from: Po */
    private boolean m35713Po() {
        return bFf().mo5608dq().mo3201h(aFV);
    }

    /* renamed from: Pp */
    private int m35714Pp() {
        return bFf().mo5608dq().mo3212n(aFX);
    }

    @C0064Am(aul = "dd404e851e1deb458b9148f7695996dc", aum = 0)
    @C5566aOg
    /* renamed from: Pw */
    private PatrolMissionScript m35718Pw() {
        throw new aWi(new aCE(this, aGf, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    @C0064Am(aul = "66262b21ab2d54c0e57bd0ddb0cc0195", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m35721a(WaypointDat cDVar) {
        throw new aWi(new aCE(this, aGb, new Object[]{cDVar}));
    }

    /* renamed from: aA */
    private void m35722aA(boolean z) {
        bFf().mo5608dq().mo3153a(aFT, z);
    }

    /* renamed from: aB */
    private void m35723aB(boolean z) {
        bFf().mo5608dq().mo3153a(aFV, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fail on Death")
    @C0064Am(aul = "9b980734a831a78399a931c3d815e564", aum = 0)
    @C5566aOg
    /* renamed from: aC */
    private void m35724aC(boolean z) {
        throw new aWi(new aCE(this, aGa, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Is Body Count")
    @C0064Am(aul = "b8ae684482d4445fafe00fba28d9d23a", aum = 0)
    @C5566aOg
    /* renamed from: aE */
    private void m35725aE(boolean z) {
        throw new aWi(new aCE(this, aGe, new Object[]{new Boolean(z)}));
    }

    /* renamed from: c */
    private void m35727c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    @C0064Am(aul = "6b234812d3100b2b17084249312084cb", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m35728c(WaypointDat cDVar) {
        throw new aWi(new aCE(this, aGc, new Object[]{cDVar}));
    }

    /* renamed from: cY */
    private void m35729cY(int i) {
        bFf().mo5608dq().mo3183b(aFX, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "f2fbc71ef1f35c40823c83abed694416", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m35730d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    @C0064Am(aul = "76be38dc49654cd19f6183820629321d", aum = 0)
    /* renamed from: iR */
    private Mission m35731iR() {
        return mo20548Px();
    }

    /* renamed from: z */
    private void m35732z(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aBq, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m35709MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo20543Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m35710Nb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    /* renamed from: Ne */
    public List<WaypointDat> mo20544Ne() {
        switch (bFf().mo6893i(aBu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aBu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBu, new Object[0]));
                break;
        }
        return m35711Nd();
    }

    /* renamed from: Pr */
    public int mo20545Pr() {
        switch (bFf().mo6893i(aFY)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aFY, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFY, new Object[0]));
                break;
        }
        return m35715Pq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fail on Death")
    /* renamed from: Pt */
    public boolean mo20546Pt() {
        switch (bFf().mo6893i(aFZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aFZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFZ, new Object[0]));
                break;
        }
        return m35716Ps();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Is Body Count")
    /* renamed from: Pv */
    public boolean mo20547Pv() {
        switch (bFf().mo6893i(aGd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aGd, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGd, new Object[0]));
                break;
        }
        return m35717Pu();
    }

    @C5566aOg
    /* renamed from: Px */
    public PatrolMissionScript mo20548Px() {
        switch (bFf().mo6893i(aGf)) {
            case 0:
                return null;
            case 2:
                return (PatrolMissionScript) bFf().mo5606d(new aCE(this, aGf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aGf, new Object[0]));
                break;
        }
        return m35718Pw();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1425Us(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m35709MZ();
                return null;
            case 1:
                return new Integer(m35715Pq());
            case 2:
                return new Boolean(m35716Ps());
            case 3:
                m35724aC(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return m35710Nb();
            case 5:
                m35730d((StellarSystem) args[0]);
                return null;
            case 6:
                return m35711Nd();
            case 7:
                m35721a((WaypointDat) args[0]);
                return null;
            case 8:
                m35728c((WaypointDat) args[0]);
                return null;
            case 9:
                return new Boolean(m35717Pu());
            case 10:
                m35725aE(((Boolean) args[0]).booleanValue());
                return null;
            case 11:
                return m35718Pw();
            case 12:
                m35720a((C0665JT) args[0]);
                return null;
            case 13:
                m35726aG();
                return null;
            case 14:
                return m35731iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fail on Death")
    @C5566aOg
    /* renamed from: aD */
    public void mo20549aD(boolean z) {
        switch (bFf().mo6893i(aGa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGa, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGa, new Object[]{new Boolean(z)}));
                break;
        }
        m35724aC(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Is Body Count")
    @C5566aOg
    /* renamed from: aF */
    public void mo20550aF(boolean z) {
        switch (bFf().mo6893i(aGe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGe, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGe, new Object[]{new Boolean(z)}));
                break;
        }
        m35725aE(z);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m35726aG();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8649Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8649Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8649Do, new Object[]{jt}));
                break;
        }
        m35720a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    @C5566aOg
    /* renamed from: b */
    public void mo20551b(WaypointDat cDVar) {
        switch (bFf().mo6893i(aGb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGb, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGb, new Object[]{cDVar}));
                break;
        }
        m35721a(cDVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    @C5566aOg
    /* renamed from: d */
    public void mo20552d(WaypointDat cDVar) {
        switch (bFf().mo6893i(aGc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGc, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGc, new Object[]{cDVar}));
                break;
        }
        m35728c(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo20553e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m35730d(jj);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f8651xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f8651xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8651xF, new Object[0]));
                break;
        }
        return m35731iR();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "d7f3b87b31033ca746008429fa4d92d2", aum = 0)
    /* renamed from: Pq */
    private int m35715Pq() {
        return m35714Pp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fail on Death")
    @C0064Am(aul = "790ac8c88020033eb3a8a20c825bf036", aum = 0)
    /* renamed from: Ps */
    private boolean m35716Ps() {
        return m35712Pn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "d6b353c5c4804dd7ad46468526a512d7", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m35710Nb() {
        return m35707MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    @C0064Am(aul = "52b95a787c5c75a0d83813a3bacc270c", aum = 0)
    /* renamed from: Nd */
    private List<WaypointDat> m35711Nd() {
        return Collections.unmodifiableList(m35708MY());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Is Body Count")
    @C0064Am(aul = "dbcf1f47ff43690387a82626bffb90b5", aum = 0)
    /* renamed from: Pu */
    private boolean m35717Pu() {
        return m35713Po();
    }

    @C0064Am(aul = "23ed96ad2e1f6a14db2b9615de838d77", aum = 0)
    /* renamed from: a */
    private void m35720a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && jt.get("bodyCountMission") != null) {
            mo20550aF(((Boolean) jt.get("bodyCountMission")).booleanValue());
        }
    }

    @C0064Am(aul = "054810bd61b30b8235c286dbe8c18de0", aum = 0)
    /* renamed from: aG */
    private void m35726aG() {
        super.mo70aH();
    }
}
