package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.mission.scripting.ReconNpcMissionScript;
import game.script.missiontemplate.PositionDat;
import game.script.npc.NPCType;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.mbean.C3047nQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.1.0")
@C5511aMd
@C6485anp
/* renamed from: a.auP  reason: case insensitive filesystem */
/* compiled from: a */
public class ReconNpcMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f5378Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C5663aRz bCb = null;
    public static final C5663aRz bCf = null;
    public static final C2491fm bCg = null;
    public static final C2491fm bCh = null;
    public static final C2491fm bCi = null;
    public static final C2491fm bCl = null;
    public static final C2491fm bCm = null;
    /* renamed from: cA */
    public static final C2491fm f5379cA = null;
    /* renamed from: cB */
    public static final C2491fm f5380cB = null;
    /* renamed from: cz */
    public static final C5663aRz f5382cz = null;
    public static final String gFh = "recons";
    public static final C5663aRz gFi = null;
    public static final C5663aRz gFj = null;
    public static final C2491fm gFk = null;
    public static final C2491fm gFl = null;
    public static final C2491fm gFm = null;
    public static final C2491fm gFn = null;
    public static final C2491fm gFo = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f5384xF = null;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "3743458f0e6d1b6400a16dc5c8ba6299", aum = 1)
    private static int aLC = 0;
    @C0064Am(aul = "91b7df2e1895a12cee2346120e2a789c", aum = 2)
    private static C3438ra<NPCType> aLD = null;
    @C0064Am(aul = "9cf6089c263afb2437a2ea7a29be79cc", aum = 3)
    private static boolean aLE = false;
    @C0064Am(aul = "ec4ab5f26e3a4a57ba145cad9cd987e8", aum = 5)
    private static PositionDat aLF = null;
    @C0064Am(aul = "2ac3c230010e3524b0c3247e4b5d0489", aum = 0)

    /* renamed from: cy */
    private static int f5381cy = 0;
    @C0064Am(aul = "eb936652800fdb7247cc2700a4a2d3c2", aum = 4)

    /* renamed from: rI */
    private static StellarSystem f5383rI;

    static {
        m26280V();
    }

    public ReconNpcMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ReconNpcMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26280V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 6;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 17;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ReconNpcMissionScriptTemplate.class, "2ac3c230010e3524b0c3247e4b5d0489", i);
        f5382cz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ReconNpcMissionScriptTemplate.class, "3743458f0e6d1b6400a16dc5c8ba6299", i2);
        gFi = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ReconNpcMissionScriptTemplate.class, "91b7df2e1895a12cee2346120e2a789c", i3);
        bCb = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ReconNpcMissionScriptTemplate.class, "9cf6089c263afb2437a2ea7a29be79cc", i4);
        bCf = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ReconNpcMissionScriptTemplate.class, "eb936652800fdb7247cc2700a4a2d3c2", i5);
        aBo = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ReconNpcMissionScriptTemplate.class, "ec4ab5f26e3a4a57ba145cad9cd987e8", i6);
        gFj = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i8 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 17)];
        C2491fm a = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "aa85fa1668df6bf45c08d47499273661", i8);
        bCh = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "b61bc2c92a795d2b194974c8fb517056", i9);
        aBr = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "d2cae8da14f1320db105179a9b501eac", i10);
        gFk = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "5e23cae219b62149f6ee58d9ba66d222", i11);
        f5379cA = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "d3414348c18a92a29db9e922c1d675a7", i12);
        gFl = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "6953ea68a71dc24c75944c3eeb875867", i13);
        bCg = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "39a700ba395ef5884cf2eefe930b3326", i14);
        aBs = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "f19e288e2609bec0d0deb8d97e1037f6", i15);
        gFm = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "f2f14517383494c556dc82583eac90bd", i16);
        bCl = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "3bcdede73bef0dd71d6114eff2fea259", i17);
        f5378Do = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "25fade8e6d1d92e77ec0a3cb1e680f41", i18);
        bCi = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "4320d298e1ba885ea3aed0c67a915cba", i19);
        f5380cB = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "a1faf342001bebee0a14437d05cca613", i20);
        gFn = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "3445b5cecba1a24f05ac64d5dc2afab9", i21);
        bCm = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "8bff08a02f4e240ac3ecbf503afc184e", i22);
        aBt = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "a10b904f1716e22b4c19e9a55f4f000e", i23);
        gFo = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(ReconNpcMissionScriptTemplate.class, "3ae607a8163faaea3199313d22f6c70f", i24);
        f5384xF = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ReconNpcMissionScriptTemplate.class, C3047nQ.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m26276MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "b61bc2c92a795d2b194974c8fb517056", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m26277MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: S */
    private void m26279S(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bCb, raVar);
    }

    private C3438ra alN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bCb);
    }

    private boolean alQ() {
        return bFf().mo5608dq().mo3201h(bCf);
    }

    /* renamed from: aw */
    private int m26282aw() {
        return bFf().mo5608dq().mo3212n(f5382cz);
    }

    /* renamed from: by */
    private void m26284by(boolean z) {
        bFf().mo5608dq().mo3153a(bCf, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show path of beacons")
    @C0064Am(aul = "3445b5cecba1a24f05ac64d5dc2afab9", aum = 0)
    @C5566aOg
    /* renamed from: bz */
    private void m26285bz(boolean z) {
        throw new aWi(new aCE(this, bCm, new Object[]{new Boolean(z)}));
    }

    /* renamed from: c */
    private void m26286c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    private int cyo() {
        return bFf().mo5608dq().mo3212n(gFi);
    }

    private PositionDat cyp() {
        return (PositionDat) bFf().mo5608dq().mo3214p(gFj);
    }

    @C0064Am(aul = "d2cae8da14f1320db105179a9b501eac", aum = 0)
    @C5566aOg
    private ReconNpcMissionScript cyq() {
        throw new aWi(new aCE(this, gFk, new Object[0]));
    }

    /* renamed from: f */
    private void m26289f(int i) {
        bFf().mo5608dq().mo3183b(f5382cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "4320d298e1ba885ea3aed0c67a915cba", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m26290g(int i) {
        throw new aWi(new aCE(this, f5380cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: i */
    private void m26292i(PositionDat vx) {
        bFf().mo5608dq().mo3197f(gFj, vx);
    }

    @C0064Am(aul = "3ae607a8163faaea3199313d22f6c70f", aum = 0)
    /* renamed from: iR */
    private Mission m26293iR() {
        return cyr();
    }

    /* renamed from: uK */
    private void m26295uK(int i) {
        bFf().mo5608dq().mo3183b(gFi, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m26277MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo16343Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m26278Nb();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3047nQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m26288e((NPCType) args[0]);
                return null;
            case 1:
                m26277MZ();
                return null;
            case 2:
                return cyq();
            case 3:
                return new Integer(m26283ax());
            case 4:
                return new Integer(cys());
            case 5:
                return alR();
            case 6:
                return m26278Nb();
            case 7:
                return cyu();
            case 8:
                return new Boolean(alV());
            case 9:
                m26281a((C0665JT) args[0]);
                return null;
            case 10:
                m26291g((NPCType) args[0]);
                return null;
            case 11:
                m26290g(((Integer) args[0]).intValue());
                return null;
            case 12:
                m26296uL(((Integer) args[0]).intValue());
                return null;
            case 13:
                m26285bz(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                m26287d((StellarSystem) args[0]);
                return null;
            case 15:
                m26294j((PositionDat) args[0]);
                return null;
            case 16:
                return m26293iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Npc Types")
    public List<NPCType> alS() {
        switch (bFf().mo6893i(bCg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bCg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bCg, new Object[0]));
                break;
        }
        return alR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show path of beacons")
    public boolean alW() {
        switch (bFf().mo6893i(bCl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bCl, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bCl, new Object[0]));
                break;
        }
        return alV();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f5378Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5378Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5378Do, new Object[]{jt}));
                break;
        }
        m26281a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show path of beacons")
    @C5566aOg
    /* renamed from: bA */
    public void mo16346bA(boolean z) {
        switch (bFf().mo6893i(bCm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCm, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCm, new Object[]{new Boolean(z)}));
                break;
        }
        m26285bz(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public ReconNpcMissionScript cyr() {
        switch (bFf().mo6893i(gFk)) {
            case 0:
                return null;
            case 2:
                return (ReconNpcMissionScript) bFf().mo5606d(new aCE(this, gFk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gFk, new Object[0]));
                break;
        }
        return cyq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Maximum distance")
    public int cyt() {
        switch (bFf().mo6893i(gFl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gFl, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gFl, new Object[0]));
                break;
        }
        return cys();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint position")
    public PositionDat cyv() {
        switch (bFf().mo6893i(gFm)) {
            case 0:
                return null;
            case 2:
                return (PositionDat) bFf().mo5606d(new aCE(this, gFm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gFm, new Object[0]));
                break;
        }
        return cyu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    /* renamed from: e */
    public void mo16350e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m26287d(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Npc Types")
    /* renamed from: f */
    public void mo16351f(NPCType aed) {
        switch (bFf().mo6893i(bCh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCh, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCh, new Object[]{aed}));
                break;
        }
        m26288e(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f5379cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f5379cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5379cA, new Object[0]));
                break;
        }
        return m26283ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f5380cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5380cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5380cB, new Object[]{new Integer(i)}));
                break;
        }
        m26290g(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Npc Types")
    /* renamed from: h */
    public void mo16353h(NPCType aed) {
        switch (bFf().mo6893i(bCi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCi, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCi, new Object[]{aed}));
                break;
        }
        m26291g(aed);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f5384xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f5384xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5384xF, new Object[0]));
                break;
        }
        return m26293iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint position")
    /* renamed from: k */
    public void mo16354k(PositionDat vx) {
        switch (bFf().mo6893i(gFo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gFo, new Object[]{vx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gFo, new Object[]{vx}));
                break;
        }
        m26294j(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Maximum distance")
    /* renamed from: uM */
    public void mo16356uM(int i) {
        switch (bFf().mo6893i(gFn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gFn, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gFn, new Object[]{new Integer(i)}));
                break;
        }
        m26296uL(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Npc Types")
    @C0064Am(aul = "aa85fa1668df6bf45c08d47499273661", aum = 0)
    /* renamed from: e */
    private void m26288e(NPCType aed) {
        alN().add(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "5e23cae219b62149f6ee58d9ba66d222", aum = 0)
    /* renamed from: ax */
    private int m26283ax() {
        return m26282aw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Maximum distance")
    @C0064Am(aul = "d3414348c18a92a29db9e922c1d675a7", aum = 0)
    private int cys() {
        return cyo();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Npc Types")
    @C0064Am(aul = "6953ea68a71dc24c75944c3eeb875867", aum = 0)
    private List<NPCType> alR() {
        return Collections.unmodifiableList(alN());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "39a700ba395ef5884cf2eefe930b3326", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m26278Nb() {
        return m26276MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint position")
    @C0064Am(aul = "f19e288e2609bec0d0deb8d97e1037f6", aum = 0)
    private PositionDat cyu() {
        return cyp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show path of beacons")
    @C0064Am(aul = "f2f14517383494c556dc82583eac90bd", aum = 0)
    private boolean alV() {
        return alQ();
    }

    @C0064Am(aul = "3bcdede73bef0dd71d6114eff2fea259", aum = 0)
    /* renamed from: a */
    private void m26281a(C0665JT jt) {
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Npc Types")
    @C0064Am(aul = "25fade8e6d1d92e77ec0a3cb1e680f41", aum = 0)
    /* renamed from: g */
    private void m26291g(NPCType aed) {
        alN().remove(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Maximum distance")
    @C0064Am(aul = "a1faf342001bebee0a14437d05cca613", aum = 0)
    /* renamed from: uL */
    private void m26296uL(int i) {
        m26295uK(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "8bff08a02f4e240ac3ecbf503afc184e", aum = 0)
    /* renamed from: d */
    private void m26287d(StellarSystem jj) {
        m26286c(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint position")
    @C0064Am(aul = "a10b904f1716e22b4c19e9a55f4f000e", aum = 0)
    /* renamed from: j */
    private void m26294j(PositionDat vx) {
        m26292i(vx);
    }
}
