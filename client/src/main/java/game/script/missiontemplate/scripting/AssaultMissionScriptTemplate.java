package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.scripting.AssaultMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.missiontemplate.WaypointDat;
import game.script.npc.NPCType;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5414aIk;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.amd  reason: case insensitive filesystem */
/* compiled from: a */
public class AssaultMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C5663aRz aBq = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C2491fm aBu = null;
    public static final C2491fm aGb = null;
    public static final C2491fm aGc = null;
    public static final C5663aRz bHJ = null;
    public static final C5663aRz bHL = null;
    public static final C2491fm fZA = null;
    public static final C2491fm fZB = null;
    public static final C2491fm fZC = null;
    public static final C2491fm fZD = null;
    public static final C2491fm fZE = null;
    public static final C2491fm fZF = null;
    public static final C2491fm fZG = null;
    public static final String fZl = "kickStartSpeechHandle";
    public static final String fZm = "waypointsHandle";
    public static final String fZn = "assaultHandle";
    public static final C5663aRz fZp = null;
    public static final C5663aRz fZs = null;
    public static final C5663aRz fZu = null;
    public static final C2491fm fZv = null;
    public static final C2491fm fZw = null;
    public static final C2491fm fZx = null;
    public static final C2491fm fZy = null;
    public static final C2491fm fZz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f4920xF = null;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "d58d8b2d0a613e554caa325a494b4a8a", aum = 1)
    private static C3438ra<WaypointDat> aBp = null;
    @C0064Am(aul = "eea48e93b2ab8d34edbe39c5e713bcec", aum = 2)
    private static WaypointDat bHK = null;
    @C0064Am(aul = "cf6e96f075fc72ae78b85c761f4c3272", aum = 0)
    private static String fZo;
    @C0064Am(aul = "c2446e815dc9a4003064e4e1494c546f", aum = 3)
    private static NPCType fZq;
    @C0064Am(aul = "d19c473609123bb12ac941ab8703af5c", aum = 5)
    private static C3438ra<AssaultMissionWave> fZr;
    @C0064Am(aul = "d67ddd28e7552c85b17c14fd0ff39a68", aum = 6)
    private static boolean fZt;
    @C0064Am(aul = "859f50d8dab39ec580da6ed154314c2a", aum = 4)

    /* renamed from: rI */
    private static StellarSystem f4919rI;

    static {
        m23920V();
    }

    public AssaultMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AssaultMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m23920V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 7;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 19;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(AssaultMissionScriptTemplate.class, "cf6e96f075fc72ae78b85c761f4c3272", i);
        fZp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AssaultMissionScriptTemplate.class, "d58d8b2d0a613e554caa325a494b4a8a", i2);
        aBq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AssaultMissionScriptTemplate.class, "eea48e93b2ab8d34edbe39c5e713bcec", i3);
        bHL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AssaultMissionScriptTemplate.class, "c2446e815dc9a4003064e4e1494c546f", i4);
        bHJ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AssaultMissionScriptTemplate.class, "859f50d8dab39ec580da6ed154314c2a", i5);
        aBo = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AssaultMissionScriptTemplate.class, "d19c473609123bb12ac941ab8703af5c", i6);
        fZs = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AssaultMissionScriptTemplate.class, "d67ddd28e7552c85b17c14fd0ff39a68", i7);
        fZu = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i9 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 19)];
        C2491fm a = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "3bc132cd06f294fa833403818ab8552a", i9);
        fZv = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "a879aecdf3ecf10de1464ba3caf41656", i10);
        fZw = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "05d70d67d45fdff536ed38bc9182cd76", i11);
        fZx = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "8ca5d12682ebcb3b401e1cf94b0e3b40", i12);
        fZy = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "20c391409f8f238a4ff717a053cfd786", i13);
        aBs = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "b8b0e28adbd9eeb6f31e924e5845beb8", i14);
        aBt = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "2b1f838b4a3d6dfdb41f60273c599d29", i15);
        fZz = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "1e4079b6e04755ab53ef699dc56fbe0a", i16);
        fZA = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "3c6953d9129e78809096defd474d33f8", i17);
        aBu = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "3d7518061c56efed8d0c0aa3c739f833", i18);
        aGb = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "2a5099bc36a2be33c96335e941e9cb2b", i19);
        aGc = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "4a1711bea1e2d37ab0d9740f08f55623", i20);
        fZB = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "2bc0e21f41371df16c6f19dfc0442dec", i21);
        fZC = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "cd0fc1524ec5c8ab19927393e369ea25", i22);
        fZD = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "4b441224eb89aec542711551de61379b", i23);
        fZE = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "bcbfd933467a995242622cc8ffb7c2ea", i24);
        fZF = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "fbbfa5c56472e9f9e00fa3752a7279c9", i25);
        fZG = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "2a7e667eedd29532964c4e04803c6a34", i26);
        aBr = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        C2491fm a19 = C4105zY.m41624a(AssaultMissionScriptTemplate.class, "6625d7e8b0f26370511f4a85ccf5b5de", i27);
        f4920xF = a19;
        fmVarArr[i27] = a19;
        int i28 = i27 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AssaultMissionScriptTemplate.class, C5414aIk.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m23915MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    /* renamed from: MY */
    private C3438ra m23916MY() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aBq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "2a7e667eedd29532964c4e04803c6a34", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m23917MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    private WaypointDat aoK() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(bHL);
    }

    /* renamed from: bT */
    private void m23923bT(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fZs, raVar);
    }

    /* renamed from: c */
    private void m23924c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C0064Am(aul = "cd0fc1524ec5c8ab19927393e369ea25", aum = 0)
    @C5566aOg
    private AssaultMissionScript ciG() {
        throw new aWi(new aCE(this, fZD, new Object[0]));
    }

    private String ciu() {
        return (String) bFf().mo5608dq().mo3214p(fZp);
    }

    private NPCType civ() {
        return (NPCType) bFf().mo5608dq().mo3214p(bHJ);
    }

    private C3438ra ciw() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fZs);
    }

    private boolean cix() {
        return bFf().mo5608dq().mo3201h(fZu);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "b8b0e28adbd9eeb6f31e924e5845beb8", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m23926d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    /* renamed from: e */
    private void m23928e(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(bHL, cDVar);
    }

    /* renamed from: ff */
    private void m23929ff(boolean z) {
        bFf().mo5608dq().mo3153a(fZu, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Create secondary NPCs with Waypoints")
    @C0064Am(aul = "a879aecdf3ecf10de1464ba3caf41656", aum = 0)
    @C5566aOg
    /* renamed from: fg */
    private void m23930fg(boolean z) {
        throw new aWi(new aCE(this, fZw, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "6625d7e8b0f26370511f4a85ccf5b5de", aum = 0)
    /* renamed from: iR */
    private Mission m23931iR() {
        return ciH();
    }

    /* renamed from: jn */
    private void m23932jn(String str) {
        bFf().mo5608dq().mo3197f(fZp, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Kick Start Speech Handle")
    @C0064Am(aul = "1e4079b6e04755ab53ef699dc56fbe0a", aum = 0)
    @C5566aOg
    /* renamed from: jo */
    private void m23933jo(String str) {
        throw new aWi(new aCE(this, fZA, new Object[]{str}));
    }

    /* renamed from: r */
    private void m23934r(NPCType aed) {
        bFf().mo5608dq().mo3197f(bHJ, aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Assaulted NPC")
    @C0064Am(aul = "2bc0e21f41371df16c6f19dfc0442dec", aum = 0)
    @C5566aOg
    /* renamed from: s */
    private void m23935s(NPCType aed) {
        throw new aWi(new aCE(this, fZC, new Object[]{aed}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Assault Waypoint")
    @C0064Am(aul = "8ca5d12682ebcb3b401e1cf94b0e3b40", aum = 0)
    @C5566aOg
    /* renamed from: z */
    private void m23936z(WaypointDat cDVar) {
        throw new aWi(new aCE(this, fZy, new Object[]{cDVar}));
    }

    /* renamed from: z */
    private void m23937z(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aBq, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Assault Waypoint")
    @C5566aOg
    /* renamed from: A */
    public void mo14861A(WaypointDat cDVar) {
        switch (bFf().mo6893i(fZy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fZy, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fZy, new Object[]{cDVar}));
                break;
        }
        m23936z(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m23917MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo14862Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m23918Nb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    /* renamed from: Ne */
    public List<WaypointDat> mo14863Ne() {
        switch (bFf().mo6893i(aBu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aBu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBu, new Object[0]));
                break;
        }
        return m23919Nd();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5414aIk(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                return new Boolean(ciy());
            case 1:
                m23930fg(((Boolean) args[0]).booleanValue());
                return null;
            case 2:
                return ciA();
            case 3:
                m23936z((WaypointDat) args[0]);
                return null;
            case 4:
                return m23918Nb();
            case 5:
                m23926d((StellarSystem) args[0]);
                return null;
            case 6:
                return ciC();
            case 7:
                m23933jo((String) args[0]);
                return null;
            case 8:
                return m23919Nd();
            case 9:
                m23921a((WaypointDat) args[0]);
                return null;
            case 10:
                m23925c((WaypointDat) args[0]);
                return null;
            case 11:
                return ciE();
            case 12:
                m23935s((NPCType) args[0]);
                return null;
            case 13:
                return ciG();
            case 14:
                return ciI();
            case 15:
                m23922b((AssaultMissionWave) args[0]);
                return null;
            case 16:
                m23927d((AssaultMissionWave) args[0]);
                return null;
            case 17:
                m23917MZ();
                return null;
            case 18:
                return m23931iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    /* renamed from: b */
    public void mo14864b(WaypointDat cDVar) {
        switch (bFf().mo6893i(aGb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGb, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGb, new Object[]{cDVar}));
                break;
        }
        m23921a(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waves")
    /* renamed from: c */
    public void mo14865c(AssaultMissionWave axg) {
        switch (bFf().mo6893i(fZF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fZF, new Object[]{axg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fZF, new Object[]{axg}));
                break;
        }
        m23922b(axg);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Assault Waypoint")
    public WaypointDat ciB() {
        switch (bFf().mo6893i(fZx)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, fZx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fZx, new Object[0]));
                break;
        }
        return ciA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Kick Start Speech Handle")
    public String ciD() {
        switch (bFf().mo6893i(fZz)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fZz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fZz, new Object[0]));
                break;
        }
        return ciC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Assaulted NPC")
    public NPCType ciF() {
        switch (bFf().mo6893i(fZB)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, fZB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fZB, new Object[0]));
                break;
        }
        return ciE();
    }

    @C5566aOg
    public AssaultMissionScript ciH() {
        switch (bFf().mo6893i(fZD)) {
            case 0:
                return null;
            case 2:
                return (AssaultMissionScript) bFf().mo5606d(new aCE(this, fZD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fZD, new Object[0]));
                break;
        }
        return ciG();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waves")
    public List<AssaultMissionWave> ciJ() {
        switch (bFf().mo6893i(fZE)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fZE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fZE, new Object[0]));
                break;
        }
        return ciI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Create secondary NPCs with Waypoints")
    public boolean ciz() {
        switch (bFf().mo6893i(fZv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fZv, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fZv, new Object[0]));
                break;
        }
        return ciy();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    /* renamed from: d */
    public void mo14872d(WaypointDat cDVar) {
        switch (bFf().mo6893i(aGc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGc, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGc, new Object[]{cDVar}));
                break;
        }
        m23925c(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo14873e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m23926d(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waves")
    /* renamed from: e */
    public void mo14874e(AssaultMissionWave axg) {
        switch (bFf().mo6893i(fZG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fZG, new Object[]{axg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fZG, new Object[]{axg}));
                break;
        }
        m23927d(axg);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Create secondary NPCs with Waypoints")
    @C5566aOg
    /* renamed from: fh */
    public void mo14875fh(boolean z) {
        switch (bFf().mo6893i(fZw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fZw, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fZw, new Object[]{new Boolean(z)}));
                break;
        }
        m23930fg(z);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f4920xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f4920xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4920xF, new Object[0]));
                break;
        }
        return m23931iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Kick Start Speech Handle")
    @C5566aOg
    /* renamed from: jp */
    public void mo14876jp(String str) {
        switch (bFf().mo6893i(fZA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fZA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fZA, new Object[]{str}));
                break;
        }
        m23933jo(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Assaulted NPC")
    @C5566aOg
    /* renamed from: t */
    public void mo14877t(NPCType aed) {
        switch (bFf().mo6893i(fZC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fZC, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fZC, new Object[]{aed}));
                break;
        }
        m23935s(aed);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Create secondary NPCs with Waypoints")
    @C0064Am(aul = "3bc132cd06f294fa833403818ab8552a", aum = 0)
    private boolean ciy() {
        return cix();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Assault Waypoint")
    @C0064Am(aul = "05d70d67d45fdff536ed38bc9182cd76", aum = 0)
    private WaypointDat ciA() {
        return aoK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "20c391409f8f238a4ff717a053cfd786", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m23918Nb() {
        return m23915MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Kick Start Speech Handle")
    @C0064Am(aul = "2b1f838b4a3d6dfdb41f60273c599d29", aum = 0)
    private String ciC() {
        return ciu();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    @C0064Am(aul = "3c6953d9129e78809096defd474d33f8", aum = 0)
    /* renamed from: Nd */
    private List<WaypointDat> m23919Nd() {
        return Collections.unmodifiableList(m23916MY());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    @C0064Am(aul = "3d7518061c56efed8d0c0aa3c739f833", aum = 0)
    /* renamed from: a */
    private void m23921a(WaypointDat cDVar) {
        m23916MY().add(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    @C0064Am(aul = "2a5099bc36a2be33c96335e941e9cb2b", aum = 0)
    /* renamed from: c */
    private void m23925c(WaypointDat cDVar) {
        m23916MY().remove(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Assaulted NPC")
    @C0064Am(aul = "4a1711bea1e2d37ab0d9740f08f55623", aum = 0)
    private NPCType ciE() {
        return civ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waves")
    @C0064Am(aul = "4b441224eb89aec542711551de61379b", aum = 0)
    private List<AssaultMissionWave> ciI() {
        return Collections.unmodifiableList(ciw());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waves")
    @C0064Am(aul = "bcbfd933467a995242622cc8ffb7c2ea", aum = 0)
    /* renamed from: b */
    private void m23922b(AssaultMissionWave axg) {
        ciw().add(axg);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waves")
    @C0064Am(aul = "fbbfa5c56472e9f9e00fa3752a7279c9", aum = 0)
    /* renamed from: d */
    private void m23927d(AssaultMissionWave axg) {
        ciw().remove(axg);
    }
}
