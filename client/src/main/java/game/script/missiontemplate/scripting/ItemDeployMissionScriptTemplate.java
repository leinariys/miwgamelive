package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.network.message.serializable.C5512aMe;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.scripting.ItemDeployMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C1938am;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.1.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.ItemDeployMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.uL */
/* compiled from: a */
public class ItemDeployMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f9342Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aBr = null;
    public static final C5663aRz bwE = null;
    public static final C5663aRz bwF = null;
    public static final C2491fm bwG = null;
    public static final C2491fm bwH = null;
    public static final C2491fm bwI = null;
    public static final C2491fm bwJ = null;
    public static final C2491fm bwK = null;
    public static final C2491fm bwL = null;
    public static final C2491fm bwM = null;
    public static final C2491fm bwN = null;
    public static final C2491fm bwO = null;
    /* renamed from: kX */
    public static final C5663aRz f9346kX = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f9347xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1d0c4aa2e19c70bf0948b93ef2c1b992", aum = 0)

    /* renamed from: iH */
    private static Station f9343iH;
    @C0064Am(aul = "863300f7a492251de8e992315b4b0e03", aum = 1)

    /* renamed from: iI */
    private static C3438ra<ItemTypeTableItem> f9344iI;
    @C0064Am(aul = "5df698cd3333dbaa13e417d26392de63", aum = 2)

    /* renamed from: iJ */
    private static C3438ra<ItemTypeTableItem> f9345iJ;

    static {
        m39941V();
    }

    public ItemDeployMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemDeployMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39941V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 3;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 12;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ItemDeployMissionScriptTemplate.class, "1d0c4aa2e19c70bf0948b93ef2c1b992", i);
        f9346kX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemDeployMissionScriptTemplate.class, "863300f7a492251de8e992315b4b0e03", i2);
        bwE = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemDeployMissionScriptTemplate.class, "5df698cd3333dbaa13e417d26392de63", i3);
        bwF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i5 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 12)];
        C2491fm a = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "b703a52be73417bd6f3759b62d8e6d97", i5);
        aBr = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "63c837a531bcd2dbcfbb6760ab533b48", i6);
        bwG = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "184c16c97836e6242229134aef0d8a9d", i7);
        bwH = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "854791d0ecee02bfffbc810866a60aed", i8);
        bwI = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "d94d319c64a8f26e705e56a78a83dcd3", i9);
        bwJ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "677679732b012420bc2ea5e39d080104", i10);
        bwK = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "0ec686aca1ddd8eb3d10a7625b036ee2", i11);
        bwL = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "38092d8349b7804b4bb1a81c57454742", i12);
        bwM = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "12b749a16e8a029c94ed8191322d325d", i13);
        bwN = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "d3fce0478d4db8f6dad0b6dcb54da559", i14);
        bwO = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "1269a61144c2cb08ec7fb1c7500aa026", i15);
        f9342Do = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemDeployMissionScriptTemplate.class, "01cf4bc5a053f35aa34694598121221b", i16);
        f9347xF = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemDeployMissionScriptTemplate.class, C1938am.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "b703a52be73417bd6f3759b62d8e6d97", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m39938MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: P */
    private void m39939P(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bwE, raVar);
    }

    /* renamed from: Q */
    private void m39940Q(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bwF, raVar);
    }

    /* renamed from: a */
    private void m39942a(Station bf) {
        bFf().mo5608dq().mo3197f(f9346kX, bf);
    }

    private C3438ra ajB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bwE);
    }

    private C3438ra ajC() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bwF);
    }

    @C0064Am(aul = "d3fce0478d4db8f6dad0b6dcb54da559", aum = 0)
    @C5566aOg
    private ItemDeployMissionScript<ItemDeployMissionScriptTemplate> ajJ() {
        throw new aWi(new aCE(this, bwO, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Delivery Item Types")
    @C0064Am(aul = "184c16c97836e6242229134aef0d8a9d", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m39945c(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bwH, new Object[]{zlVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Receive Item Types")
    @C0064Am(aul = "d94d319c64a8f26e705e56a78a83dcd3", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m39946e(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bwJ, new Object[]{zlVar}));
    }

    /* renamed from: eF */
    private Station m39947eF() {
        return (Station) bFf().mo5608dq().mo3214p(f9346kX);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Receive Item Types")
    @C0064Am(aul = "677679732b012420bc2ea5e39d080104", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m39948g(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bwK, new Object[]{zlVar}));
    }

    @C0064Am(aul = "01cf4bc5a053f35aa34694598121221b", aum = 0)
    /* renamed from: iR */
    private Mission m39949iR() {
        return ajK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Station")
    @C0064Am(aul = "12b749a16e8a029c94ed8191322d325d", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m39950n(Station bf) {
        throw new aWi(new aCE(this, bwN, new Object[]{bf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m39938MZ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1938am(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m39938MZ();
                return null;
            case 1:
                m39944a((ItemTypeTableItem) args[0]);
                return null;
            case 2:
                m39945c((ItemTypeTableItem) args[0]);
                return null;
            case 3:
                return ajD();
            case 4:
                m39946e((ItemTypeTableItem) args[0]);
                return null;
            case 5:
                m39948g((ItemTypeTableItem) args[0]);
                return null;
            case 6:
                return ajF();
            case 7:
                return ajH();
            case 8:
                m39950n((Station) args[0]);
                return null;
            case 9:
                return ajJ();
            case 10:
                m39943a((C0665JT) args[0]);
                return null;
            case 11:
                return m39949iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Delivery Item Types")
    public List<ItemTypeTableItem> ajE() {
        switch (bFf().mo6893i(bwI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bwI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwI, new Object[0]));
                break;
        }
        return ajD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Receive Item Types")
    public List<ItemTypeTableItem> ajG() {
        switch (bFf().mo6893i(bwL)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bwL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwL, new Object[0]));
                break;
        }
        return ajF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Station")
    public Station ajI() {
        switch (bFf().mo6893i(bwM)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, bwM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwM, new Object[0]));
                break;
        }
        return ajH();
    }

    @C5566aOg
    public ItemDeployMissionScript<ItemDeployMissionScriptTemplate> ajK() {
        switch (bFf().mo6893i(bwO)) {
            case 0:
                return null;
            case 2:
                return (ItemDeployMissionScript) bFf().mo5606d(new aCE(this, bwO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwO, new Object[0]));
                break;
        }
        return ajJ();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9342Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9342Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9342Do, new Object[]{jt}));
                break;
        }
        m39943a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Delivery Item Types")
    /* renamed from: b */
    public void mo22359b(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwG, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwG, new Object[]{zlVar}));
                break;
        }
        m39944a(zlVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Delivery Item Types")
    @C5566aOg
    /* renamed from: d */
    public void mo22360d(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwH, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwH, new Object[]{zlVar}));
                break;
        }
        m39945c(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Receive Item Types")
    @C5566aOg
    /* renamed from: f */
    public void mo22361f(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwJ, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwJ, new Object[]{zlVar}));
                break;
        }
        m39946e(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Receive Item Types")
    @C5566aOg
    /* renamed from: h */
    public void mo22362h(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bwK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwK, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwK, new Object[]{zlVar}));
                break;
        }
        m39948g(zlVar);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f9347xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f9347xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9347xF, new Object[0]));
                break;
        }
        return m39949iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Station")
    @C5566aOg
    /* renamed from: o */
    public void mo22363o(Station bf) {
        switch (bFf().mo6893i(bwN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwN, new Object[]{bf}));
                break;
        }
        m39950n(bf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Delivery Item Types")
    @C0064Am(aul = "63c837a531bcd2dbcfbb6760ab533b48", aum = 0)
    /* renamed from: a */
    private void m39944a(ItemTypeTableItem zlVar) {
        ajB().add(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Delivery Item Types")
    @C0064Am(aul = "854791d0ecee02bfffbc810866a60aed", aum = 0)
    private List<ItemTypeTableItem> ajD() {
        return Collections.unmodifiableList(ajB());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Receive Item Types")
    @C0064Am(aul = "0ec686aca1ddd8eb3d10a7625b036ee2", aum = 0)
    private List<ItemTypeTableItem> ajF() {
        return Collections.unmodifiableList(ajC());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Station")
    @C0064Am(aul = "38092d8349b7804b4bb1a81c57454742", aum = 0)
    private Station ajH() {
        return m39947eF();
    }

    @C0064Am(aul = "1269a61144c2cb08ec7fb1c7500aa026", aum = 0)
    /* renamed from: a */
    private void m39943a(C0665JT jt) {
        ItemType jCVar = null;
        if ("".equals(jt.getVersion())) {
            int intValue = ((Integer) jt.get("time")).intValue();
            if (intValue != 0) {
                super.mo7895wQ(intValue);
            }
        } else if ("1.0.0".equals(jt.getVersion())) {
            C5512aMe ame = (C5512aMe) jt.get("deliveryItemType");
            ItemType jCVar2 = ame != null ? (ItemType) ame.baw() : null;
            C5512aMe ame2 = (C5512aMe) jt.get("receiveItemType");
            if (ame2 != null) {
                jCVar = (ItemType) ame2.baw();
            }
            if (jCVar2 != null) {
                int intValue2 = ((Integer) jt.get("deliveryQuantity")).intValue();
                ItemTypeTableItem zlVar = (ItemTypeTableItem) bFf().mo6865M(ItemTypeTableItem.class);
                zlVar.mo10S();
                zlVar.setAmount(intValue2);
                zlVar.mo23386c(jCVar2);
                ajB().add(zlVar);
            }
            if (jCVar != null) {
                int intValue3 = ((Integer) jt.get("receiveQuantity")).intValue();
                ItemTypeTableItem zlVar2 = (ItemTypeTableItem) bFf().mo6865M(ItemTypeTableItem.class);
                zlVar2.mo10S();
                zlVar2.setAmount(intValue3);
                zlVar2.mo23386c(jCVar);
                ajC().add(zlVar2);
            }
        }
    }
}
