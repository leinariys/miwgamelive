package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.mission.scripting.FollowMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.missiontemplate.WaypointDat;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5906aci;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.FollowMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.aAB */
/* compiled from: a */
public class FollowMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C5663aRz heB = null;
    public static final C5663aRz heC = null;
    public static final C5663aRz heD = null;
    public static final C2491fm heE = null;
    public static final C2491fm heF = null;
    public static final C2491fm heG = null;
    public static final C2491fm heH = null;
    public static final C2491fm heI = null;
    public static final C2491fm heJ = null;
    public static final C2491fm heK = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f2289xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c46c38121d7f4ff38bad871f7e773487", aum = 1)
    private static WaypointDat faR;
    @C0064Am(aul = "45e64b1b71601688a4df1e43512802d9", aum = 2)
    private static WaypointDat faS;
    @C0064Am(aul = "3780b216758b3d4e3abdd2918970e2af", aum = 3)
    private static WaypointDat faT;
    @C0064Am(aul = "37fe301fcdd15017c9c2345d87b4d8fa", aum = 0)

    /* renamed from: rI */
    private static StellarSystem f2288rI;

    static {
        m12425V();
    }

    public FollowMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FollowMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12425V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 4;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 11;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(FollowMissionScriptTemplate.class, "37fe301fcdd15017c9c2345d87b4d8fa", i);
        aBo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(FollowMissionScriptTemplate.class, "c46c38121d7f4ff38bad871f7e773487", i2);
        heB = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(FollowMissionScriptTemplate.class, "45e64b1b71601688a4df1e43512802d9", i3);
        heC = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(FollowMissionScriptTemplate.class, "3780b216758b3d4e3abdd2918970e2af", i4);
        heD = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i6 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 11)];
        C2491fm a = C4105zY.m41624a(FollowMissionScriptTemplate.class, "4cb56905f5304c3ee9b2fc7133d9a9dc", i6);
        heE = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "2a0b1ca9e378b2d4d250469da2c0d12a", i7);
        heF = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "c2c16db0c4d7a7886ecfa76f2f2ce69d", i8);
        heG = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "40e8469a5dfce58be96bddc8d5b0d06b", i9);
        heH = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "90447765398aa6af62f25550a875fa01", i10);
        heI = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "ad8afbcc21bd8dc01891ceaf79d08b3a", i11);
        heJ = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "2d255a2452201b8684d397296fb04b53", i12);
        aBs = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "8129eaff12eab7a20c899d1b93fa889f", i13);
        aBt = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "d9c10a608e5eae8f323a9f911ef502c5", i14);
        heK = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "c63a0a76fb0c36ee4ba57f553e820efd", i15);
        aBr = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(FollowMissionScriptTemplate.class, "7d4d16e8995f0a83bf0a73616d16bbc9", i16);
        f2289xF = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FollowMissionScriptTemplate.class, C5906aci.class, _m_fields, _m_methods);
    }

    /* renamed from: E */
    private void m12416E(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(heB, cDVar);
    }

    /* renamed from: F */
    private void m12417F(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(heC, cDVar);
    }

    /* renamed from: G */
    private void m12418G(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(heD, cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Spawn Waypoint Dat")
    @C0064Am(aul = "2a0b1ca9e378b2d4d250469da2c0d12a", aum = 0)
    @C5566aOg
    /* renamed from: H */
    private void m12419H(WaypointDat cDVar) {
        throw new aWi(new aCE(this, heF, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Destination Waypoint Dat")
    @C0064Am(aul = "40e8469a5dfce58be96bddc8d5b0d06b", aum = 0)
    @C5566aOg
    /* renamed from: J */
    private void m12420J(WaypointDat cDVar) {
        throw new aWi(new aCE(this, heH, new Object[]{cDVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Attack Spot Waypoint Dat")
    @C0064Am(aul = "ad8afbcc21bd8dc01891ceaf79d08b3a", aum = 0)
    @C5566aOg
    /* renamed from: L */
    private void m12421L(WaypointDat cDVar) {
        throw new aWi(new aCE(this, heJ, new Object[]{cDVar}));
    }

    /* renamed from: MX */
    private StellarSystem m12422MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "c63a0a76fb0c36ee4ba57f553e820efd", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m12423MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: c */
    private void m12426c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    private WaypointDat cIl() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(heB);
    }

    private WaypointDat cIm() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(heC);
    }

    private WaypointDat cIn() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(heD);
    }

    @C0064Am(aul = "d9c10a608e5eae8f323a9f911ef502c5", aum = 0)
    @C5566aOg
    private FollowMissionScript cIu() {
        throw new aWi(new aCE(this, heK, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "8129eaff12eab7a20c899d1b93fa889f", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m12427d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    @C0064Am(aul = "7d4d16e8995f0a83bf0a73616d16bbc9", aum = 0)
    /* renamed from: iR */
    private Mission m12428iR() {
        return cIv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Spawn Waypoint Dat")
    @C5566aOg
    /* renamed from: I */
    public void mo7600I(WaypointDat cDVar) {
        switch (bFf().mo6893i(heF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, heF, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, heF, new Object[]{cDVar}));
                break;
        }
        m12419H(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Destination Waypoint Dat")
    @C5566aOg
    /* renamed from: K */
    public void mo7601K(WaypointDat cDVar) {
        switch (bFf().mo6893i(heH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, heH, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, heH, new Object[]{cDVar}));
                break;
        }
        m12420J(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Attack Spot Waypoint Dat")
    @C5566aOg
    /* renamed from: M */
    public void mo7602M(WaypointDat cDVar) {
        switch (bFf().mo6893i(heJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, heJ, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, heJ, new Object[]{cDVar}));
                break;
        }
        m12421L(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m12423MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo7603Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m12424Nb();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5906aci(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                return cIo();
            case 1:
                m12419H((WaypointDat) args[0]);
                return null;
            case 2:
                return cIq();
            case 3:
                m12420J((WaypointDat) args[0]);
                return null;
            case 4:
                return cIs();
            case 5:
                m12421L((WaypointDat) args[0]);
                return null;
            case 6:
                return m12424Nb();
            case 7:
                m12427d((StellarSystem) args[0]);
                return null;
            case 8:
                return cIu();
            case 9:
                m12423MZ();
                return null;
            case 10:
                return m12428iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Spawn Waypoint Dat")
    public WaypointDat cIp() {
        switch (bFf().mo6893i(heE)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, heE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, heE, new Object[0]));
                break;
        }
        return cIo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Destination Waypoint Dat")
    public WaypointDat cIr() {
        switch (bFf().mo6893i(heG)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, heG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, heG, new Object[0]));
                break;
        }
        return cIq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Attack Spot Waypoint Dat")
    public WaypointDat cIt() {
        switch (bFf().mo6893i(heI)) {
            case 0:
                return null;
            case 2:
                return (WaypointDat) bFf().mo5606d(new aCE(this, heI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, heI, new Object[0]));
                break;
        }
        return cIs();
    }

    @C5566aOg
    public FollowMissionScript cIv() {
        switch (bFf().mo6893i(heK)) {
            case 0:
                return null;
            case 2:
                return (FollowMissionScript) bFf().mo5606d(new aCE(this, heK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, heK, new Object[0]));
                break;
        }
        return cIu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo7608e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m12427d(jj);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f2289xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f2289xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2289xF, new Object[0]));
                break;
        }
        return m12428iR();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Spawn Waypoint Dat")
    @C0064Am(aul = "4cb56905f5304c3ee9b2fc7133d9a9dc", aum = 0)
    private WaypointDat cIo() {
        return cIl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Destination Waypoint Dat")
    @C0064Am(aul = "c2c16db0c4d7a7886ecfa76f2f2ce69d", aum = 0)
    private WaypointDat cIq() {
        return cIm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Attack Spot Waypoint Dat")
    @C0064Am(aul = "90447765398aa6af62f25550a875fa01", aum = 0)
    private WaypointDat cIs() {
        return cIn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "2d255a2452201b8684d397296fb04b53", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m12424Nb() {
        return m12422MX();
    }
}
