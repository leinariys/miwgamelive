package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.scripting.KillingNpcMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.missiontemplate.PositionDat;
import game.script.npc.NPCType;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.mbean.C6702ary;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.1.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.KillingNpcMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.wg */
/* compiled from: a */
public class KillingNpcMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f9486Do = null;

    /* renamed from: MS */
    public static final C5663aRz f9487MS = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final String bCa = "killings";
    public static final C5663aRz bCb = null;
    public static final C5663aRz bCe = null;
    public static final C5663aRz bCf = null;
    public static final C2491fm bCg = null;
    public static final C2491fm bCh = null;
    public static final C2491fm bCi = null;
    public static final C2491fm bCj = null;
    public static final C2491fm bCk = null;
    public static final C2491fm bCl = null;
    public static final C2491fm bCm = null;
    public static final C2491fm bCn = null;
    public static final C2491fm bCo = null;
    public static final C2491fm bCp = null;
    public static final C2491fm bCq = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f9489xF = null;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "78580882799a4244f3a78ebe86d5eeb7", aum = 0)
    private static C3438ra<NPCType> aLD = null;
    @C0064Am(aul = "fc367570b91f0bb950f7a0426036d143", aum = 4)
    private static boolean aLE = false;
    @C0064Am(aul = "15d50d5d7b64099b6586f00d95670e12", aum = 1)
    private static int bCc;
    @C0064Am(aul = "814554636bfd6c6eaa926a753a1442fb", aum = 3)
    private static C3438ra<PositionDat> bCd;
    @C0064Am(aul = "85cbfeda096819afcfb7f544b32815fe", aum = 2)

    /* renamed from: rI */
    private static StellarSystem f9488rI;

    static {
        m40683V();
    }

    public KillingNpcMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public KillingNpcMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40683V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 5;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 16;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(KillingNpcMissionScriptTemplate.class, "78580882799a4244f3a78ebe86d5eeb7", i);
        bCb = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(KillingNpcMissionScriptTemplate.class, "15d50d5d7b64099b6586f00d95670e12", i2);
        f9487MS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(KillingNpcMissionScriptTemplate.class, "85cbfeda096819afcfb7f544b32815fe", i3);
        aBo = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(KillingNpcMissionScriptTemplate.class, "814554636bfd6c6eaa926a753a1442fb", i4);
        bCe = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(KillingNpcMissionScriptTemplate.class, "fc367570b91f0bb950f7a0426036d143", i5);
        bCf = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i7 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 16)];
        C2491fm a = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "69990dae14ebc6d2f6fd00cba604df29", i7);
        aBr = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "c5458c257d56be6b9513062dca951d62", i8);
        bCg = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "595e50a82e6c1b8b2827f5bf7c08a7ac", i9);
        bCh = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "6ee36bffa138648d938ca787f46a1a41", i10);
        bCi = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "97cdc5b4a4df0d262d0c986a29bf9eda", i11);
        bCj = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "1db5576d5acf2c92c7ce4b4676430f13", i12);
        bCk = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "6107359d64fbdb12e18e0ae7651e416e", i13);
        bCl = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "328030e7c063001d3beedd2d4f9d7948", i14);
        bCm = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "9bd7b0309451efe1f3ac42fddbe78873", i15);
        bCn = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "c24cd0fb8b0147d5e1cc2c3377179ac9", i16);
        bCo = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "d6c2bc3f4255ac31ce672eef1adb04ef", i17);
        bCp = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "0ec545a96bd295f8ed239201f9b7ff09", i18);
        aBs = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "4b387bebb0cf6937e7f4427ff97c332a", i19);
        aBt = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "352175496c2b3494d14a18f16febb018", i20);
        bCq = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "e256aa8f4fc64c4681333321b488926b", i21);
        f9486Do = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(KillingNpcMissionScriptTemplate.class, "7de8ecec2f8ed602945c413a5f0c86f8", i22);
        f9489xF = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(KillingNpcMissionScriptTemplate.class, C6702ary.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m40678MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "69990dae14ebc6d2f6fd00cba604df29", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m40679MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: S */
    private void m40681S(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bCb, raVar);
    }

    /* renamed from: T */
    private void m40682T(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bCe, raVar);
    }

    private C3438ra alN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bCb);
    }

    private int alO() {
        return bFf().mo5608dq().mo3212n(f9487MS);
    }

    private C3438ra alP() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bCe);
    }

    private boolean alQ() {
        return bFf().mo5608dq().mo3201h(bCf);
    }

    @C0064Am(aul = "352175496c2b3494d14a18f16febb018", aum = 0)
    @C5566aOg
    private KillingNpcMissionScript alZ() {
        throw new aWi(new aCE(this, bCq, new Object[0]));
    }

    /* renamed from: by */
    private void m40686by(boolean z) {
        bFf().mo5608dq().mo3153a(bCf, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show path of beacons")
    @C0064Am(aul = "328030e7c063001d3beedd2d4f9d7948", aum = 0)
    @C5566aOg
    /* renamed from: bz */
    private void m40687bz(boolean z) {
        throw new aWi(new aCE(this, bCm, new Object[]{new Boolean(z)}));
    }

    /* renamed from: c */
    private void m40688c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    /* renamed from: fl */
    private void m40692fl(int i) {
        bFf().mo5608dq().mo3183b(f9487MS, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ammount")
    @C0064Am(aul = "1db5576d5acf2c92c7ce4b4676430f13", aum = 0)
    @C5566aOg
    /* renamed from: fm */
    private void m40693fm(int i) {
        throw new aWi(new aCE(this, bCk, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "7de8ecec2f8ed602945c413a5f0c86f8", aum = 0)
    /* renamed from: iR */
    private Mission m40695iR() {
        return ama();
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m40679MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo22778Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m40680Nb();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6702ary(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m40679MZ();
                return null;
            case 1:
                return alR();
            case 2:
                m40691e((NPCType) args[0]);
                return null;
            case 3:
                m40694g((NPCType) args[0]);
                return null;
            case 4:
                return new Integer(alT());
            case 5:
                m40693fm(((Integer) args[0]).intValue());
                return null;
            case 6:
                return new Boolean(alV());
            case 7:
                m40687bz(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                return alX();
            case 9:
                m40685a((PositionDat) args[0]);
                return null;
            case 10:
                m40689c((PositionDat) args[0]);
                return null;
            case 11:
                return m40680Nb();
            case 12:
                m40690d((StellarSystem) args[0]);
                return null;
            case 13:
                return alZ();
            case 14:
                m40684a((C0665JT) args[0]);
                return null;
            case 15:
                return m40695iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Npc Types")
    public List<NPCType> alS() {
        switch (bFf().mo6893i(bCg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bCg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bCg, new Object[0]));
                break;
        }
        return alR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ammount")
    public int alU() {
        switch (bFf().mo6893i(bCj)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bCj, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bCj, new Object[0]));
                break;
        }
        return alT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show path of beacons")
    public boolean alW() {
        switch (bFf().mo6893i(bCl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bCl, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bCl, new Object[0]));
                break;
        }
        return alV();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoint positions")
    public List<PositionDat> alY() {
        switch (bFf().mo6893i(bCn)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bCn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bCn, new Object[0]));
                break;
        }
        return alX();
    }

    @C5566aOg
    public KillingNpcMissionScript ama() {
        switch (bFf().mo6893i(bCq)) {
            case 0:
                return null;
            case 2:
                return (KillingNpcMissionScript) bFf().mo5606d(new aCE(this, bCq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bCq, new Object[0]));
                break;
        }
        return alZ();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9486Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9486Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9486Do, new Object[]{jt}));
                break;
        }
        m40684a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoint positions")
    /* renamed from: b */
    public void mo22784b(PositionDat vx) {
        switch (bFf().mo6893i(bCo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCo, new Object[]{vx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCo, new Object[]{vx}));
                break;
        }
        m40685a(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show path of beacons")
    @C5566aOg
    /* renamed from: bA */
    public void mo22785bA(boolean z) {
        switch (bFf().mo6893i(bCm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCm, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCm, new Object[]{new Boolean(z)}));
                break;
        }
        m40687bz(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoint positions")
    /* renamed from: d */
    public void mo22786d(PositionDat vx) {
        switch (bFf().mo6893i(bCp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCp, new Object[]{vx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCp, new Object[]{vx}));
                break;
        }
        m40689c(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    /* renamed from: e */
    public void mo22787e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m40690d(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Npc Types")
    /* renamed from: f */
    public void mo22788f(NPCType aed) {
        switch (bFf().mo6893i(bCh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCh, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCh, new Object[]{aed}));
                break;
        }
        m40691e(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ammount")
    @C5566aOg
    /* renamed from: fn */
    public void mo22789fn(int i) {
        switch (bFf().mo6893i(bCk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCk, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCk, new Object[]{new Integer(i)}));
                break;
        }
        m40693fm(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Npc Types")
    /* renamed from: h */
    public void mo22790h(NPCType aed) {
        switch (bFf().mo6893i(bCi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bCi, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bCi, new Object[]{aed}));
                break;
        }
        m40694g(aed);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f9489xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f9489xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9489xF, new Object[0]));
                break;
        }
        return m40695iR();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Npc Types")
    @C0064Am(aul = "c5458c257d56be6b9513062dca951d62", aum = 0)
    private List<NPCType> alR() {
        return Collections.unmodifiableList(alN());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Npc Types")
    @C0064Am(aul = "595e50a82e6c1b8b2827f5bf7c08a7ac", aum = 0)
    /* renamed from: e */
    private void m40691e(NPCType aed) {
        alN().add(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Npc Types")
    @C0064Am(aul = "6ee36bffa138648d938ca787f46a1a41", aum = 0)
    /* renamed from: g */
    private void m40694g(NPCType aed) {
        alN().remove(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ammount")
    @C0064Am(aul = "97cdc5b4a4df0d262d0c986a29bf9eda", aum = 0)
    private int alT() {
        return alO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show path of beacons")
    @C0064Am(aul = "6107359d64fbdb12e18e0ae7651e416e", aum = 0)
    private boolean alV() {
        return alQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoint positions")
    @C0064Am(aul = "9bd7b0309451efe1f3ac42fddbe78873", aum = 0)
    private List<PositionDat> alX() {
        return Collections.unmodifiableList(alP());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoint positions")
    @C0064Am(aul = "c24cd0fb8b0147d5e1cc2c3377179ac9", aum = 0)
    /* renamed from: a */
    private void m40685a(PositionDat vx) {
        alP().add(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoint positions")
    @C0064Am(aul = "d6c2bc3f4255ac31ce672eef1adb04ef", aum = 0)
    /* renamed from: c */
    private void m40689c(PositionDat vx) {
        alP().remove(vx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "0ec545a96bd295f8ed239201f9b7ff09", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m40680Nb() {
        return m40678MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "4b387bebb0cf6937e7f4427ff97c332a", aum = 0)
    /* renamed from: d */
    private void m40690d(StellarSystem jj) {
        m40688c(jj);
    }

    @C0064Am(aul = "e256aa8f4fc64c4681333321b488926b", aum = 0)
    /* renamed from: a */
    private void m40684a(C0665JT jt) {
        if (jt.mo3117j(1, 1, 0)) {
            m40686by(true);
        }
    }
}
