package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.ItemTypeTableItem;
import game.script.mission.Mission;
import game.script.mission.scripting.MiningMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6929awR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.we */
/* compiled from: a */
public class MiningMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aBr = null;
    public static final C5663aRz bBM = null;
    public static final C5663aRz bBO = null;
    public static final C5663aRz bBQ = null;
    public static final C2491fm bBR = null;
    public static final C2491fm bBS = null;
    public static final C2491fm bBT = null;
    public static final C2491fm bBU = null;
    public static final C2491fm bBV = null;
    public static final C2491fm bBW = null;
    public static final C2491fm bwM = null;
    public static final C2491fm bwN = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f9484xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "34bba9a17f285ce74ee99dd119500aff", aum = 0)
    private static String bBL;
    @C0064Am(aul = "a08975ac5598c68a566254f0f55d5a91", aum = 1)
    private static Station bBN;
    @C0064Am(aul = "e3d80ce90a457c522291c99688793df1", aum = 2)
    private static C3438ra<ItemTypeTableItem> bBP;

    static {
        m40657V();
    }

    public MiningMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MiningMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40657V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 3;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 10;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(MiningMissionScriptTemplate.class, "34bba9a17f285ce74ee99dd119500aff", i);
        bBM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MiningMissionScriptTemplate.class, "a08975ac5598c68a566254f0f55d5a91", i2);
        bBO = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MiningMissionScriptTemplate.class, "e3d80ce90a457c522291c99688793df1", i3);
        bBQ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i5 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 10)];
        C2491fm a = C4105zY.m41624a(MiningMissionScriptTemplate.class, "390abb7fe2a48b223e76af01091769b7", i5);
        aBr = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "5163f8e2c23392729cf734f5fafe65e2", i6);
        bBR = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "a5736bf27dd11e9e7fa3fbdd778ec83f", i7);
        bBS = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "67b81a1085317a6b33aa2e3667b8df88", i8);
        bBT = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "d074aa56fcd79f0f7741900a406f1970", i9);
        bBU = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "3cbc8aaea726b20315e5d19633650596", i10);
        bBV = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "4cf5d333ffc2b002375aca26e6f7125c", i11);
        bwM = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "5852e7d5210973fe106e21b9f249733d", i12);
        bwN = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "4846f426209d352db65f72464c61869c", i13);
        bBW = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(MiningMissionScriptTemplate.class, "87d842cbf8395cab5198cce4138ab46f", i14);
        f9484xF = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MiningMissionScriptTemplate.class, C6929awR.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "390abb7fe2a48b223e76af01091769b7", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m40655MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    /* renamed from: R */
    private void m40656R(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bBQ, raVar);
    }

    private String alE() {
        return (String) bFf().mo5608dq().mo3214p(bBM);
    }

    private Station alF() {
        return (Station) bFf().mo5608dq().mo3214p(bBO);
    }

    private C3438ra alG() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bBQ);
    }

    /* renamed from: ct */
    private void m40658ct(String str) {
        bFf().mo5608dq().mo3197f(bBM, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delivery Speech Handle (optional)")
    @C0064Am(aul = "3cbc8aaea726b20315e5d19633650596", aum = 0)
    @C5566aOg
    /* renamed from: cu */
    private void m40659cu(String str) {
        throw new aWi(new aCE(this, bBV, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mining Item Types")
    @C0064Am(aul = "5163f8e2c23392729cf734f5fafe65e2", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m40660i(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bBR, new Object[]{zlVar}));
    }

    @C0064Am(aul = "87d842cbf8395cab5198cce4138ab46f", aum = 0)
    /* renamed from: iR */
    private Mission m40661iR() {
        return alM();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mining Item Types")
    @C0064Am(aul = "a5736bf27dd11e9e7fa3fbdd778ec83f", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m40662k(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, bBS, new Object[]{zlVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Station (optional)")
    @C0064Am(aul = "5852e7d5210973fe106e21b9f249733d", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m40663n(Station bf) {
        throw new aWi(new aCE(this, bwN, new Object[]{bf}));
    }

    /* renamed from: v */
    private void m40664v(Station bf) {
        bFf().mo5608dq().mo3197f(bBO, bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m40655MZ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6929awR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                m40655MZ();
                return null;
            case 1:
                m40660i((ItemTypeTableItem) args[0]);
                return null;
            case 2:
                m40662k((ItemTypeTableItem) args[0]);
                return null;
            case 3:
                return alH();
            case 4:
                return alJ();
            case 5:
                m40659cu((String) args[0]);
                return null;
            case 6:
                return ajH();
            case 7:
                m40663n((Station) args[0]);
                return null;
            case 8:
                return alL();
            case 9:
                return m40661iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Station (optional)")
    public Station ajI() {
        switch (bFf().mo6893i(bwM)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, bwM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwM, new Object[0]));
                break;
        }
        return ajH();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mining Item Types")
    public List<ItemTypeTableItem> alI() {
        switch (bFf().mo6893i(bBT)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bBT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bBT, new Object[0]));
                break;
        }
        return alH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delivery Speech Handle (optional)")
    public String alK() {
        switch (bFf().mo6893i(bBU)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bBU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bBU, new Object[0]));
                break;
        }
        return alJ();
    }

    public MiningMissionScript alM() {
        switch (bFf().mo6893i(bBW)) {
            case 0:
                return null;
            case 2:
                return (MiningMissionScript) bFf().mo5606d(new aCE(this, bBW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bBW, new Object[0]));
                break;
        }
        return alL();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delivery Speech Handle (optional)")
    @C5566aOg
    /* renamed from: cv */
    public void mo22774cv(String str) {
        switch (bFf().mo6893i(bBV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bBV, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bBV, new Object[]{str}));
                break;
        }
        m40659cu(str);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f9484xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f9484xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9484xF, new Object[0]));
                break;
        }
        return m40661iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mining Item Types")
    @C5566aOg
    /* renamed from: j */
    public void mo22775j(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bBR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bBR, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bBR, new Object[]{zlVar}));
                break;
        }
        m40660i(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mining Item Types")
    @C5566aOg
    /* renamed from: l */
    public void mo22776l(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(bBS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bBS, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bBS, new Object[]{zlVar}));
                break;
        }
        m40662k(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Station (optional)")
    @C5566aOg
    /* renamed from: o */
    public void mo22777o(Station bf) {
        switch (bFf().mo6893i(bwN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwN, new Object[]{bf}));
                break;
        }
        m40663n(bf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mining Item Types")
    @C0064Am(aul = "67b81a1085317a6b33aa2e3667b8df88", aum = 0)
    private List<ItemTypeTableItem> alH() {
        return Collections.unmodifiableList(alG());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delivery Speech Handle (optional)")
    @C0064Am(aul = "d074aa56fcd79f0f7741900a406f1970", aum = 0)
    private String alJ() {
        return alE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Station (optional)")
    @C0064Am(aul = "4cf5d333ffc2b002375aca26e6f7125c", aum = 0)
    private Station ajH() {
        return alF();
    }

    @C0064Am(aul = "4846f426209d352db65f72464c61869c", aum = 0)
    private MiningMissionScript alL() {
        MiningMissionScript aei = (MiningMissionScript) bFf().mo6865M(MiningMissionScript.class);
        aei.mo10S();
        return aei;
    }
}
