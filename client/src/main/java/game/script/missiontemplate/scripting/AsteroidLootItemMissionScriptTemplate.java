package game.script.missiontemplate.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.TableNPCSpawn;
import game.script.mission.scripting.AsteroidLootItemMissionScript;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.missiontemplate.WaypointDat;
import game.script.ship.Station;
import game.script.space.AsteroidType;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0605IZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.AsteroidLootItemMissionTemplate")
@C6485anp
@C5511aMd
/* renamed from: a.ang  reason: case insensitive filesystem */
/* compiled from: a */
public class AsteroidLootItemMissionScriptTemplate extends MissionScriptTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C5663aRz aBq = null;
    public static final C2491fm aBr = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C2491fm aBu = null;
    public static final C2491fm aGb = null;
    public static final C2491fm aGc = null;
    public static final C5663aRz ebn = null;
    public static final C2491fm eby = null;
    public static final C2491fm ebz = null;
    public static final C5663aRz gcC = null;
    public static final C5663aRz gcD = null;
    public static final C5663aRz gcE = null;
    public static final C2491fm gcF = null;
    public static final C2491fm gcG = null;
    public static final C2491fm gcH = null;
    public static final C2491fm gcI = null;
    public static final C2491fm gcJ = null;
    public static final C2491fm gcK = null;
    public static final C2491fm gcL = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f4981xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "90dfb6cd0bfbd1d93179387915700630", aum = 1)
    private static C3438ra<WaypointDat> aBp;
    @C0064Am(aul = "8e7588b5c2058ba3d4740eb3bf3f1d9d", aum = 0)
    private static TableNPCSpawn dhI;
    @C0064Am(aul = "ae1d244e42d4baec820326e2ba1b7575", aum = 3)
    private static Station dhJ;
    @C0064Am(aul = "650cca073d8b86c054d18437fb3fcbc5", aum = 4)
    private static ItemType dhK;
    @C0064Am(aul = "c1bd94f840b72c80a961359f0ba89fc5", aum = 5)
    private static AsteroidType dhL;
    @C0064Am(aul = "829dbf397aef3d48cc90084a95f9d000", aum = 2)

    /* renamed from: rI */
    private static StellarSystem f4980rI;

    static {
        m24231V();
    }

    public AsteroidLootItemMissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidLootItemMissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24231V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScriptTemplate._m_fieldCount + 6;
        _m_methodCount = MissionScriptTemplate._m_methodCount + 16;
        int i = MissionScriptTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(AsteroidLootItemMissionScriptTemplate.class, "8e7588b5c2058ba3d4740eb3bf3f1d9d", i);
        gcC = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AsteroidLootItemMissionScriptTemplate.class, "90dfb6cd0bfbd1d93179387915700630", i2);
        aBq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AsteroidLootItemMissionScriptTemplate.class, "829dbf397aef3d48cc90084a95f9d000", i3);
        aBo = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AsteroidLootItemMissionScriptTemplate.class, "ae1d244e42d4baec820326e2ba1b7575", i4);
        gcD = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AsteroidLootItemMissionScriptTemplate.class, "650cca073d8b86c054d18437fb3fcbc5", i5);
        ebn = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AsteroidLootItemMissionScriptTemplate.class, "c1bd94f840b72c80a961359f0ba89fc5", i6);
        gcE = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_fields, (Object[]) _m_fields);
        int i8 = MissionScriptTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 16)];
        C2491fm a = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "132b2086506f6f0095a7a1bb411383f8", i8);
        eby = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "d14e10146f191d878554588b2db6dce1", i9);
        ebz = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "9b90e84124daf2519345edb3103885d8", i10);
        gcF = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "a16b72be9d13acc56103b87e11b417dc", i11);
        gcG = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "a58fb4c51dc7ed2208a2459439ea8b87", i12);
        gcH = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "c6ec31cccb4d56902c2baad46249eded", i13);
        gcI = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "d00864a445b548a317571a3e1d0eed1e", i14);
        aBu = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "7a6c2ee1cfb5b89a2a22dcca828eacb8", i15);
        aGb = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "bd6fe291a6faf6d99f3b51de00caa6fc", i16);
        aGc = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "6afb0ab5768feda9bf0fbd6c1b9f60d5", i17);
        aBs = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "39aeb823d741f1706c9634ee6479e0cf", i18);
        aBt = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "a93c7896bce260dcaee1c7b4139b4d10", i19);
        gcJ = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "883031df3a73d27feadaa62d135863a9", i20);
        gcK = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "ab3c8fd77ff70d5d46c433de089199be", i21);
        gcL = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "c52137253d8f3c9ca0489a7ea79aa2df", i22);
        aBr = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(AsteroidLootItemMissionScriptTemplate.class, "02db911a69731caaf34f76e773b3c783", i23);
        f4981xF = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScriptTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidLootItemMissionScriptTemplate.class, C0605IZ.class, _m_fields, _m_methods);
    }

    /* renamed from: C */
    private void m24224C(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(ebn, jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item to be looted")
    @C0064Am(aul = "d14e10146f191d878554588b2db6dce1", aum = 0)
    @C5566aOg
    /* renamed from: E */
    private void m24225E(ItemType jCVar) {
        throw new aWi(new aCE(this, ebz, new Object[]{jCVar}));
    }

    /* renamed from: MX */
    private StellarSystem m24226MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    /* renamed from: MY */
    private C3438ra m24227MY() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aBq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C0064Am(aul = "c52137253d8f3c9ca0489a7ea79aa2df", aum = 0)
    @C5566aOg
    /* renamed from: MZ */
    private void m24228MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    @C0064Am(aul = "7a6c2ee1cfb5b89a2a22dcca828eacb8", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m24232a(WaypointDat cDVar) {
        throw new aWi(new aCE(this, aGb, new Object[]{cDVar}));
    }

    /* renamed from: aj */
    private void m24233aj(Station bf) {
        bFf().mo5608dq().mo3197f(gcD, bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Station")
    @C0064Am(aul = "a16b72be9d13acc56103b87e11b417dc", aum = 0)
    @C5566aOg
    /* renamed from: ak */
    private void m24234ak(Station bf) {
        throw new aWi(new aCE(this, gcG, new Object[]{bf}));
    }

    private ItemType brN() {
        return (ItemType) bFf().mo5608dq().mo3214p(ebn);
    }

    /* renamed from: c */
    private void m24235c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    @C0064Am(aul = "bd6fe291a6faf6d99f3b51de00caa6fc", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m24236c(WaypointDat cDVar) {
        throw new aWi(new aCE(this, aGc, new Object[]{cDVar}));
    }

    @C0064Am(aul = "ab3c8fd77ff70d5d46c433de089199be", aum = 0)
    @C5566aOg
    private AsteroidLootItemMissionScript ckC() {
        throw new aWi(new aCE(this, gcL, new Object[0]));
    }

    private TableNPCSpawn cku() {
        return (TableNPCSpawn) bFf().mo5608dq().mo3214p(gcC);
    }

    private Station ckv() {
        return (Station) bFf().mo5608dq().mo3214p(gcD);
    }

    private AsteroidType ckw() {
        return (AsteroidType) bFf().mo5608dq().mo3214p(gcE);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lootable NPC")
    @C0064Am(aul = "a58fb4c51dc7ed2208a2459439ea8b87", aum = 0)
    @C5566aOg
    private TableNPCSpawn ckz() {
        throw new aWi(new aCE(this, gcH, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C0064Am(aul = "39aeb823d741f1706c9634ee6479e0cf", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m24237d(StellarSystem jj) {
        throw new aWi(new aCE(this, aBt, new Object[]{jj}));
    }

    @C0064Am(aul = "02db911a69731caaf34f76e773b3c783", aum = 0)
    /* renamed from: iR */
    private Mission m24238iR() {
        return ckD();
    }

    /* renamed from: m */
    private void m24239m(AsteroidType aqn) {
        bFf().mo5608dq().mo3197f(gcE, aqn);
    }

    /* renamed from: m */
    private void m24240m(TableNPCSpawn aoj) {
        bFf().mo5608dq().mo3197f(gcC, aoj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lootable Asteroid")
    @C0064Am(aul = "883031df3a73d27feadaa62d135863a9", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m24241n(AsteroidType aqn) {
        throw new aWi(new aCE(this, gcK, new Object[]{aqn}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lootable NPC")
    @C0064Am(aul = "c6ec31cccb4d56902c2baad46249eded", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m24242n(TableNPCSpawn aoj) {
        throw new aWi(new aCE(this, gcI, new Object[]{aoj}));
    }

    /* renamed from: z */
    private void m24243z(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aBq, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item to be looted")
    @C5566aOg
    /* renamed from: F */
    public void mo15008F(ItemType jCVar) {
        switch (bFf().mo6893i(ebz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ebz, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ebz, new Object[]{jCVar}));
                break;
        }
        m24225E(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "Check Mission Objectives")
    @C5566aOg
    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m24228MZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo15009Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m24229Nb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    /* renamed from: Ne */
    public List<WaypointDat> mo15010Ne() {
        switch (bFf().mo6893i(aBu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aBu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBu, new Object[0]));
                break;
        }
        return m24230Nd();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0605IZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScriptTemplate._m_methodCount) {
            case 0:
                return brT();
            case 1:
                m24225E((ItemType) args[0]);
                return null;
            case 2:
                return ckx();
            case 3:
                m24234ak((Station) args[0]);
                return null;
            case 4:
                return ckz();
            case 5:
                m24242n((TableNPCSpawn) args[0]);
                return null;
            case 6:
                return m24230Nd();
            case 7:
                m24232a((WaypointDat) args[0]);
                return null;
            case 8:
                m24236c((WaypointDat) args[0]);
                return null;
            case 9:
                return m24229Nb();
            case 10:
                m24237d((StellarSystem) args[0]);
                return null;
            case 11:
                return ckB();
            case 12:
                m24241n((AsteroidType) args[0]);
                return null;
            case 13:
                return ckC();
            case 14:
                m24228MZ();
                return null;
            case 15:
                return m24238iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Station")
    @C5566aOg
    /* renamed from: al */
    public void mo15011al(Station bf) {
        switch (bFf().mo6893i(gcG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gcG, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gcG, new Object[]{bf}));
                break;
        }
        m24234ak(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Waypoints")
    @C5566aOg
    /* renamed from: b */
    public void mo15012b(WaypointDat cDVar) {
        switch (bFf().mo6893i(aGb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGb, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGb, new Object[]{cDVar}));
                break;
        }
        m24232a(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item to be looted")
    public ItemType brU() {
        switch (bFf().mo6893i(eby)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, eby, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eby, new Object[0]));
                break;
        }
        return brT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lootable Asteroid")
    public AsteroidType cii() {
        switch (bFf().mo6893i(gcJ)) {
            case 0:
                return null;
            case 2:
                return (AsteroidType) bFf().mo5606d(new aCE(this, gcJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gcJ, new Object[0]));
                break;
        }
        return ckB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lootable NPC")
    @C5566aOg
    public TableNPCSpawn ckA() {
        switch (bFf().mo6893i(gcH)) {
            case 0:
                return null;
            case 2:
                return (TableNPCSpawn) bFf().mo5606d(new aCE(this, gcH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gcH, new Object[0]));
                break;
        }
        return ckz();
    }

    @C5566aOg
    public AsteroidLootItemMissionScript ckD() {
        switch (bFf().mo6893i(gcL)) {
            case 0:
                return null;
            case 2:
                return (AsteroidLootItemMissionScript) bFf().mo5606d(new aCE(this, gcL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gcL, new Object[0]));
                break;
        }
        return ckC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Station")
    public Station cky() {
        switch (bFf().mo6893i(gcF)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, gcF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gcF, new Object[0]));
                break;
        }
        return ckx();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Waypoints")
    @C5566aOg
    /* renamed from: d */
    public void mo15018d(WaypointDat cDVar) {
        switch (bFf().mo6893i(aGc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGc, new Object[]{cDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGc, new Object[]{cDVar}));
                break;
        }
        m24236c(cDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stellar System")
    @C5566aOg
    /* renamed from: e */
    public void mo15019e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m24237d(jj);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f4981xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f4981xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4981xF, new Object[0]));
                break;
        }
        return m24238iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lootable Asteroid")
    @C5566aOg
    /* renamed from: l */
    public void mo15020l(AsteroidType aqn) {
        switch (bFf().mo6893i(gcK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gcK, new Object[]{aqn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gcK, new Object[]{aqn}));
                break;
        }
        m24241n(aqn);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lootable NPC")
    @C5566aOg
    /* renamed from: o */
    public void mo15021o(TableNPCSpawn aoj) {
        switch (bFf().mo6893i(gcI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gcI, new Object[]{aoj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gcI, new Object[]{aoj}));
                break;
        }
        m24242n(aoj);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item to be looted")
    @C0064Am(aul = "132b2086506f6f0095a7a1bb411383f8", aum = 0)
    private ItemType brT() {
        return brN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Station")
    @C0064Am(aul = "9b90e84124daf2519345edb3103885d8", aum = 0)
    private Station ckx() {
        return ckv();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Waypoints")
    @C0064Am(aul = "d00864a445b548a317571a3e1d0eed1e", aum = 0)
    /* renamed from: Nd */
    private List<WaypointDat> m24230Nd() {
        return Collections.unmodifiableList(m24227MY());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "6afb0ab5768feda9bf0fbd6c1b9f60d5", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m24229Nb() {
        return m24226MX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lootable Asteroid")
    @C0064Am(aul = "a93c7896bce260dcaee1c7b4139b4d10", aum = 0)
    private AsteroidType ckB() {
        return ckw();
    }
}
