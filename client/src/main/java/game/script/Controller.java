package game.script;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.space.Gate;
import logic.baa.*;
import logic.data.mbean.C2272dV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Jx */
/* compiled from: a */
public abstract class Controller extends TaikodomObject implements C1616Xf {
    public static final C5663aRz _f_controllable = null;
    /* renamed from: _f_enterGate_0020_0028Ltaikodom_002fgame_002fscript_002fspace_002fGate_003b_0029V */
    public static final C2491fm f887xcedc4f07 = null;
    public static final C2491fm _f_exitGateServer_0020_0028_0029V = null;
    public static final C2491fm _f_forceMaxSpeedOnClient_0020_0028_0029V = null;
    /* renamed from: _f_getPawn_0020_0028_0029Ltaikodom_002fgame_002fscript_002fPawn_003b */
    public static final C2491fm f888xdc759b82 = null;
    /* renamed from: _f_onActorSpawn_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029V */
    public static final C2491fm f889x6b6d29da = null;
    /* renamed from: _f_onActorUnspawn_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029V */
    public static final C2491fm f890xa1dc95e1 = null;
    /* renamed from: _f_onDamageDealt_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003bLtaikodom_002fgame_002fDamage_003bLcom_002fhoplon_002fgeometry_002fVec3f_003b_0029V */
    public static final C2491fm f891x402d3b93 = null;
    /* renamed from: _f_onDamageTaken_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003bLtaikodom_002fgame_002fDamage_003bLcom_002fhoplon_002fgeometry_002fVec3f_003b_0029V */
    public static final C2491fm f892xbe599734 = null;
    public static final C2491fm _f_onEnterCruiseSpeed_0020_0028_0029V = null;
    public static final C2491fm _f_onExitCruiseSpeed_0020_0028_0029V = null;
    /* renamed from: _f_onKill_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029V */
    public static final C2491fm f893xb55723f2 = null;
    public static final C2491fm _f_onPawnExplode_0020_0028_0029V = null;
    public static final C2491fm _f_onPawnSimulatorDataUpdate_0020_0028JZ_0029V = null;
    public static final C2491fm _f_onPawnSpawn_0020_0028_0029V = null;
    public static final C2491fm _f_onPawnUnspawn_0020_0028_0029V = null;
    public static final C2491fm _f_onSimulationStatusUpdate_0020_0028_0029V = null;
    public static final C2491fm _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V = null;
    /* renamed from: _f_setPawn_0020_0028Ltaikodom_002fgame_002fscript_002fPawn_003b_0029V */
    public static final C2491fm f894xbccafd70 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C3248pc
    @C0064Am(aul = "b7dcd8c1af585ef173b8223ec6b008c6", aum = 0)
    @C5256aCi
    private static Pawn controllable;

    static {
        m6047V();
    }

    public Controller() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Controller(C5540aNg ang) {
        super(ang);
    }

    public Controller(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m6047V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 1;
        _m_methodCount = TaikodomObject._m_methodCount + 18;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(Controller.class, "b7dcd8c1af585ef173b8223ec6b008c6", i);
        _f_controllable = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i3 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 18)];
        C2491fm a = C4105zY.m41624a(Controller.class, "a65a2dc2c248145b0c149f02cff01d33", i3);
        f888xdc759b82 = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(Controller.class, "79dd6d5c433764e78dde10dae7a364cb", i4);
        f894xbccafd70 = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(Controller.class, "335ad439dd1f3561af838131e536ff7e", i5);
        f889x6b6d29da = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(Controller.class, "aecd77019ac04bedd88e2cda5fcd3a98", i6);
        f890xa1dc95e1 = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(Controller.class, "8f3e62527f9d23432156f62716e94e43", i7);
        _f_onPawnSpawn_0020_0028_0029V = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(Controller.class, "5bb55466fe1261bec75f9eb4d2045411", i8);
        _f_onPawnUnspawn_0020_0028_0029V = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(Controller.class, "9b048e7b85e9499de4c7c859543c3917", i9);
        _f_onPawnSimulatorDataUpdate_0020_0028JZ_0029V = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(Controller.class, "0add4463025f71adc07839c480d11308", i10);
        f892xbe599734 = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(Controller.class, "0dd362ab499d7848e93dd33f70c2c064", i11);
        f891x402d3b93 = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(Controller.class, "7110bf4231e93c96f5e658ffc1ffef46", i12);
        f893xb55723f2 = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(Controller.class, "408caf6bd103b89ab268f0b88048ce75", i13);
        _f_onPawnExplode_0020_0028_0029V = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(Controller.class, "daa045148c6e81d16d9fb3f2739182db", i14);
        f887xcedc4f07 = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(Controller.class, "84836541792607f30a16e6b6937f3f88", i15);
        _f_exitGateServer_0020_0028_0029V = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        C2491fm a14 = C4105zY.m41624a(Controller.class, "f62807550ad22e643978ade778c73ee2", i16);
        _f_onEnterCruiseSpeed_0020_0028_0029V = a14;
        fmVarArr[i16] = a14;
        int i17 = i16 + 1;
        C2491fm a15 = C4105zY.m41624a(Controller.class, "469ee1ac09240495834e57d59cf24b0d", i17);
        _f_onExitCruiseSpeed_0020_0028_0029V = a15;
        fmVarArr[i17] = a15;
        int i18 = i17 + 1;
        C2491fm a16 = C4105zY.m41624a(Controller.class, "07c219f38ad23f64251ace6485a0b465", i18);
        _f_forceMaxSpeedOnClient_0020_0028_0029V = a16;
        fmVarArr[i18] = a16;
        int i19 = i18 + 1;
        C2491fm a17 = C4105zY.m41624a(Controller.class, "7193b7b2db41c73993b6f27a3d2b56be", i19);
        _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V = a17;
        fmVarArr[i19] = a17;
        int i20 = i19 + 1;
        C2491fm a18 = C4105zY.m41624a(Controller.class, "f8cccc344cb6846a3c98850fb763f1a7", i20);
        _f_onSimulationStatusUpdate_0020_0028_0029V = a18;
        fmVarArr[i20] = a18;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Controller.class, C2272dV.class, _m_fields, _m_methods);
    }

    private Pawn aXY() {
        return (Pawn) bFf().mo5608dq().mo3214p(_f_controllable);
    }

    /* renamed from: c */
    private void m6054c(Pawn avi) {
        bFf().mo5608dq().mo3197f(_f_controllable, avi);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2272dV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return aXZ();
            case 1:
                m6055d((Pawn) args[0]);
                return null;
            case 2:
                m6049aq((Actor) args[0]);
                return null;
            case 3:
                m6050as((Actor) args[0]);
                return null;
            case 4:
                aYb();
                return null;
            case 5:
                aYd();
                return null;
            case 6:
                m6052c(((Long) args[0]).longValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 7:
                m6048a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2]);
                return null;
            case 8:
                m6053c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2]);
                return null;
            case 9:
                m6051au((Actor) args[0]);
                return null;
            case 10:
                aYf();
                return null;
            case 11:
                m6057m((Gate) args[0]);
                return null;
            case 12:
                aYh();
                return null;
            case 13:
                aYj();
                return null;
            case 14:
                aYk();
                return null;
            case 15:
                aYl();
                return null;
            case 16:
                m6056fI(((Float) args[0]).floatValue());
                return null;
            case 17:
                aYn();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Pawn aYa() {
        switch (bFf().mo6893i(f888xdc759b82)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, f888xdc759b82, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f888xdc759b82, new Object[0]));
                break;
        }
        return aXZ();
    }

    @C4034yP
    public void aYc() {
        switch (bFf().mo6893i(_f_onPawnSpawn_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnSpawn_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnSpawn_0020_0028_0029V, new Object[0]));
                break;
        }
        aYb();
    }

    @C4034yP
    public void aYe() {
        switch (bFf().mo6893i(_f_onPawnUnspawn_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnUnspawn_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnUnspawn_0020_0028_0029V, new Object[0]));
                break;
        }
        aYd();
    }

    @C4034yP
    public void aYg() {
        switch (bFf().mo6893i(_f_onPawnExplode_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                break;
        }
        aYf();
    }

    public void aYi() {
        switch (bFf().mo6893i(_f_exitGateServer_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_exitGateServer_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_exitGateServer_0020_0028_0029V, new Object[0]));
                break;
        }
        aYh();
    }

    public void aYm() {
        switch (bFf().mo6893i(_f_forceMaxSpeedOnClient_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_forceMaxSpeedOnClient_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_forceMaxSpeedOnClient_0020_0028_0029V, new Object[0]));
                break;
        }
        aYl();
    }

    public void aYo() {
        switch (bFf().mo6893i(_f_onSimulationStatusUpdate_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onSimulationStatusUpdate_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onSimulationStatusUpdate_0020_0028_0029V, new Object[0]));
                break;
        }
        aYn();
    }

    @C4034yP
    /* renamed from: ar */
    public void mo3287ar(Actor cr) {
        switch (bFf().mo6893i(f889x6b6d29da)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f889x6b6d29da, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f889x6b6d29da, new Object[]{cr}));
                break;
        }
        m6049aq(cr);
    }

    @C4034yP
    /* renamed from: at */
    public void mo3288at(Actor cr) {
        switch (bFf().mo6893i(f890xa1dc95e1)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f890xa1dc95e1, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f890xa1dc95e1, new Object[]{cr}));
                break;
        }
        m6050as(cr);
    }

    @C4034yP
    /* renamed from: av */
    public void mo3289av(Actor cr) {
        switch (bFf().mo6893i(f893xb55723f2)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f893xb55723f2, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f893xb55723f2, new Object[]{cr}));
                break;
        }
        m6051au(cr);
    }

    @C4034yP
    /* renamed from: b */
    public void mo3290b(Actor cr, C5260aCm acm, Vec3f vec3f) {
        switch (bFf().mo6893i(f892xbe599734)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f892xbe599734, new Object[]{cr, acm, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f892xbe599734, new Object[]{cr, acm, vec3f}));
                break;
        }
        m6048a(cr, acm, vec3f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C4034yP
    /* renamed from: d */
    public void mo3291d(long j, boolean z) {
        switch (bFf().mo6893i(_f_onPawnSimulatorDataUpdate_0020_0028JZ_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnSimulatorDataUpdate_0020_0028JZ_0029V, new Object[]{new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnSimulatorDataUpdate_0020_0028JZ_0029V, new Object[]{new Long(j), new Boolean(z)}));
                break;
        }
        m6052c(j, z);
    }

    @C4034yP
    /* renamed from: d */
    public void mo3292d(Actor cr, C5260aCm acm, Vec3f vec3f) {
        switch (bFf().mo6893i(f891x402d3b93)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f891x402d3b93, new Object[]{cr, acm, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f891x402d3b93, new Object[]{cr, acm, vec3f}));
                break;
        }
        m6053c(cr, acm, vec3f);
    }

    /* renamed from: e */
    public void mo3293e(Pawn avi) {
        switch (bFf().mo6893i(f894xbccafd70)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f894xbccafd70, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f894xbccafd70, new Object[]{avi}));
                break;
        }
        m6055d(avi);
    }

    /* renamed from: fJ */
    public void mo3294fJ(float f) {
        switch (bFf().mo6893i(_f_setDesiredLinearVelocityAtClient_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m6056fI(f);
    }

    @C4034yP
    /* renamed from: n */
    public void mo3295n(Gate fFVar) {
        switch (bFf().mo6893i(f887xcedc4f07)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f887xcedc4f07, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f887xcedc4f07, new Object[]{fFVar}));
                break;
        }
        m6057m(fFVar);
    }

    /* renamed from: xO */
    public void mo3296xO() {
        switch (bFf().mo6893i(_f_onEnterCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYj();
    }

    /* renamed from: xP */
    public void mo3297xP() {
        switch (bFf().mo6893i(_f_onExitCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYk();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    @C0064Am(aul = "a65a2dc2c248145b0c149f02cff01d33", aum = 0)
    private Pawn aXZ() {
        return aXY();
    }

    @C0064Am(aul = "79dd6d5c433764e78dde10dae7a364cb", aum = 0)
    /* renamed from: d */
    private void m6055d(Pawn avi) {
        m6054c(avi);
    }

    @C4034yP
    @C0064Am(aul = "335ad439dd1f3561af838131e536ff7e", aum = 0)
    /* renamed from: aq */
    private void m6049aq(Actor cr) {
    }

    @C4034yP
    @C0064Am(aul = "aecd77019ac04bedd88e2cda5fcd3a98", aum = 0)
    /* renamed from: as */
    private void m6050as(Actor cr) {
    }

    @C4034yP
    @C0064Am(aul = "8f3e62527f9d23432156f62716e94e43", aum = 0)
    private void aYb() {
    }

    @C4034yP
    @C0064Am(aul = "5bb55466fe1261bec75f9eb4d2045411", aum = 0)
    private void aYd() {
    }

    @C4034yP
    @C0064Am(aul = "9b048e7b85e9499de4c7c859543c3917", aum = 0)
    /* renamed from: c */
    private void m6052c(long j, boolean z) {
    }

    @C4034yP
    @C0064Am(aul = "0add4463025f71adc07839c480d11308", aum = 0)
    /* renamed from: a */
    private void m6048a(Actor cr, C5260aCm acm, Vec3f vec3f) {
    }

    @C4034yP
    @C0064Am(aul = "0dd362ab499d7848e93dd33f70c2c064", aum = 0)
    /* renamed from: c */
    private void m6053c(Actor cr, C5260aCm acm, Vec3f vec3f) {
    }

    @C4034yP
    @C0064Am(aul = "7110bf4231e93c96f5e658ffc1ffef46", aum = 0)
    /* renamed from: au */
    private void m6051au(Actor cr) {
    }

    @C4034yP
    @C0064Am(aul = "408caf6bd103b89ab268f0b88048ce75", aum = 0)
    private void aYf() {
    }

    @C4034yP
    @C0064Am(aul = "daa045148c6e81d16d9fb3f2739182db", aum = 0)
    /* renamed from: m */
    private void m6057m(Gate fFVar) {
    }

    @C0064Am(aul = "84836541792607f30a16e6b6937f3f88", aum = 0)
    private void aYh() {
    }

    @C0064Am(aul = "f62807550ad22e643978ade778c73ee2", aum = 0)
    private void aYj() {
    }

    @C0064Am(aul = "469ee1ac09240495834e57d59cf24b0d", aum = 0)
    private void aYk() {
    }

    @C0064Am(aul = "07c219f38ad23f64251ace6485a0b465", aum = 0)
    private void aYl() {
    }

    @C0064Am(aul = "7193b7b2db41c73993b6f27a3d2b56be", aum = 0)
    /* renamed from: fI */
    private void m6056fI(float f) {
    }

    @C0064Am(aul = "f8cccc344cb6846a3c98850fb763f1a7", aum = 0)
    private void aYn() {
    }
}
