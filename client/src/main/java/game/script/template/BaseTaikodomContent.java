package game.script.template;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6515aoT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C6968axh
@C5511aMd
@C6485anp
/* renamed from: a.Pz */
/* compiled from: a */
public abstract class BaseTaikodomContent extends C2961mJ implements C0468GU, C1616Xf {
    /* renamed from: _f_getTaikodom_0020_0028_0029Ltaikodom_002fgame_002fscript_002fTaikodom_003b */
    public static final C2491fm f1396xa236a942 = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1398bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1399bM = null;
    /* renamed from: bN */
    public static final C2491fm f1400bN = null;
    /* renamed from: bO */
    public static final C2491fm f1401bO = null;
    /* renamed from: bP */
    public static final C2491fm f1402bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1403bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ce6a427447221bc22a7949a2292a90bd", aum = 1)

    /* renamed from: bK */
    private static UUID f1397bK;
    @C0064Am(aul = "1ab375e32555b3fed47d4ae93b4472ea", aum = 0)
    private static String handle;

    static {
        m8677V();
    }

    public BaseTaikodomContent() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseTaikodomContent(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8677V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = C2961mJ._m_fieldCount + 2;
        _m_methodCount = C2961mJ._m_methodCount + 6;
        int i = C2961mJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BaseTaikodomContent.class, "1ab375e32555b3fed47d4ae93b4472ea", i);
        f1399bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BaseTaikodomContent.class, "ce6a427447221bc22a7949a2292a90bd", i2);
        f1398bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) C2961mJ._m_fields, (Object[]) _m_fields);
        int i4 = C2961mJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(BaseTaikodomContent.class, "8188727e9ebf28bc265be96289a878f0", i4);
        f1400bN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BaseTaikodomContent.class, "1026a0c0344de29c26cd4a7d15c20ec1", i5);
        f1401bO = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseTaikodomContent.class, "ad5e8ec626efba42fb810fb28fb48c14", i6);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BaseTaikodomContent.class, "91ea3fa3de78324754c20600d6004da6", i7);
        f1402bP = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BaseTaikodomContent.class, "40219431a3217849088c58e6a296c54a", i8);
        f1403bQ = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BaseTaikodomContent.class, "ce17a674cdabd5634cc620c675d6a82a", i9);
        f1396xa236a942 = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) C2961mJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseTaikodomContent.class, C6515aoT.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m8678a(String str) {
        bFf().mo5608dq().mo3197f(f1399bM, str);
    }

    /* renamed from: a */
    private void m8679a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1398bL, uuid);
    }

    /* renamed from: an */
    private UUID m8680an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1398bL);
    }

    /* renamed from: ao */
    private String m8681ao() {
        return (String) bFf().mo5608dq().mo3214p(f1399bM);
    }

    /* renamed from: c */
    private void m8687c(UUID uuid) {
        switch (bFf().mo6893i(f1401bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1401bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1401bO, new Object[]{uuid}));
                break;
        }
        m8686b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6515aoT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - C2961mJ._m_methodCount) {
            case 0:
                return m8682ap();
            case 1:
                m8686b((UUID) args[0]);
                return null;
            case 2:
                return m8684au();
            case 3:
                return m8683ar();
            case 4:
                m8685b((String) args[0]);
                return null;
            case 5:
                return aPD();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Taikodom ala() {
        switch (bFf().mo6893i(f1396xa236a942)) {
            case 0:
                return null;
            case 2:
                return (Taikodom) bFf().mo5606d(new aCE(this, f1396xa236a942, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1396xa236a942, new Object[0]));
                break;
        }
        return aPD();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1400bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1400bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1400bN, new Object[0]));
                break;
        }
        return m8682ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1402bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1402bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1402bP, new Object[0]));
                break;
        }
        return m8683ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1403bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1403bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1403bQ, new Object[]{str}));
                break;
        }
        m8685b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m8684au();
    }

    @C0064Am(aul = "8188727e9ebf28bc265be96289a878f0", aum = 0)
    /* renamed from: ap */
    private UUID m8682ap() {
        return m8680an();
    }

    @C0064Am(aul = "1026a0c0344de29c26cd4a7d15c20ec1", aum = 0)
    /* renamed from: b */
    private void m8686b(UUID uuid) {
        m8679a(uuid);
    }

    @C0064Am(aul = "ad5e8ec626efba42fb810fb28fb48c14", aum = 0)
    /* renamed from: au */
    private String m8684au() {
        return "[" + m8681ao() + "]";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        setHandle(C0468GU.dac);
        m8679a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "91ea3fa3de78324754c20600d6004da6", aum = 0)
    /* renamed from: ar */
    private String m8683ar() {
        return m8681ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "40219431a3217849088c58e6a296c54a", aum = 0)
    /* renamed from: b */
    private void m8685b(String str) {
        m8678a(str);
    }

    @C0064Am(aul = "ce17a674cdabd5634cc620c675d6a82a", aum = 0)
    private Taikodom aPD() {
        return (Taikodom) bFf().mo6866PM().bGz().bFV();
    }
}
