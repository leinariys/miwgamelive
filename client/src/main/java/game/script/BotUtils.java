package game.script;

import game.network.message.externalizable.aCE;
import game.script.cloning.CloningDefaults;
import game.script.login.UserConnection;
import game.script.mission.MissionTemplate;
import game.script.player.Player;
import game.script.player.User;
import game.script.space.Gate;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5299aDz;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.lang.reflect.Method;
import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aRf  reason: case insensitive filesystem */
/* compiled from: a */
public class BotUtils extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm iGA = null;
    public static final C2491fm iGB = null;
    public static final C2491fm iGC = null;
    public static final C2491fm iGD = null;
    public static final C2491fm iGE = null;
    public static final C2491fm iGF = null;
    public static final C2491fm iGG = null;
    public static final C5663aRz iGy = null;
    public static final C2491fm iGz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C5566aOg
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "facfe7dd7da527d09018dfe5b7bb5442", aum = 0)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static int hCd;

    static {
        m17856V();
    }

    public BotUtils() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BotUtils(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17856V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 1;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(BotUtils.class, "facfe7dd7da527d09018dfe5b7bb5442", i);
        iGy = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i3 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 8)];
        C2491fm a = C4105zY.m41624a(BotUtils.class, "2a791f1e377aaf42abdbde3d57b1b10d", i3);
        iGz = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(BotUtils.class, "411639c1fff97c9cd631b7d19d64f13c", i4);
        iGA = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(BotUtils.class, "d50c78e8967f9f704c277fe9b4dbc28f", i5);
        iGB = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(BotUtils.class, "090b8f5608081986fa8ac9efbdef3423", i6);
        iGC = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(BotUtils.class, "e53a6a91b0c9a6f77b16fa63e2267ed6", i7);
        iGD = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(BotUtils.class, "7a50e37acdc88060c078b2a7152a1ffa", i8);
        iGE = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(BotUtils.class, "8a3c401e300da6d801a383aec38e74a3", i9);
        iGF = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(BotUtils.class, "384df673345c65d4214c87d00501f7d9", i10);
        iGG = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BotUtils.class, C5299aDz.class, _m_fields, _m_methods);
    }

    /* renamed from: As */
    private void m17854As(int i) {
        bFf().mo5608dq().mo3183b(iGy, i);
    }

    @C0064Am(aul = "411639c1fff97c9cd631b7d19d64f13c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: E */
    private void m17855E(User adk) {
        throw new aWi(new aCE(this, iGA, new Object[]{adk}));
    }

    /* renamed from: cB */
    private void m17859cB(Player aku) {
        switch (bFf().mo6893i(iGB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGB, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGB, new Object[]{aku}));
                break;
        }
        m17858cA(aku);
    }

    private int drf() {
        return bFf().mo5608dq().mo3212n(iGy);
    }

    @C5566aOg
    @C0064Am(aul = "8a3c401e300da6d801a383aec38e74a3", aum = 0)
    @C2499fr
    private Collection<Gate> drg() {
        throw new aWi(new aCE(this, iGF, new Object[0]));
    }

    @C0064Am(aul = "384df673345c65d4214c87d00501f7d9", aum = 0)
    @C5566aOg
    @C2499fr
    private int dri() {
        throw new aWi(new aCE(this, iGG, new Object[0]));
    }

    @C0064Am(aul = "2a791f1e377aaf42abdbde3d57b1b10d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: g */
    private void m17860g(String str, int i, int i2) {
        throw new aWi(new aCE(this, iGz, new Object[]{str, new Integer(i), new Integer(i2)}));
    }

    private Method getMethod(Class<?> cls, String str, Class<?>... clsArr) {
        switch (bFf().mo6893i(iGE)) {
            case 0:
                return null;
            case 2:
                return (Method) bFf().mo5606d(new aCE(this, iGE, new Object[]{cls, str, clsArr}));
            case 3:
                bFf().mo5606d(new aCE(this, iGE, new Object[]{cls, str, clsArr}));
                break;
        }
        return m17857a(cls, str, clsArr);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "e53a6a91b0c9a6f77b16fa63e2267ed6", aum = 0)
    @C2499fr
    /* renamed from: j */
    private void m17861j(Player aku, String str) {
        throw new aWi(new aCE(this, iGD, new Object[]{aku, str}));
    }

    @C0064Am(aul = "090b8f5608081986fa8ac9efbdef3423", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: k */
    private void m17862k(UserConnection apk) {
        throw new aWi(new aCE(this, iGC, new Object[]{apk}));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: F */
    public void mo11175F(User adk) {
        switch (bFf().mo6893i(iGA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGA, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGA, new Object[]{adk}));
                break;
        }
        m17855E(adk);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5299aDz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m17860g((String) args[0], ((Integer) args[1]).intValue(), ((Integer) args[2]).intValue());
                return null;
            case 1:
                m17855E((User) args[0]);
                return null;
            case 2:
                m17858cA((Player) args[0]);
                return null;
            case 3:
                m17862k((UserConnection) args[0]);
                return null;
            case 4:
                m17861j((Player) args[0], (String) args[1]);
                return null;
            case 5:
                return m17857a((Class) args[0], (String) args[1], (Class[]) args[2]);
            case 6:
                return drg();
            case 7:
                return new Integer(dri());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    public Collection<Gate> drh() {
        switch (bFf().mo6893i(iGF)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, iGF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGF, new Object[0]));
                break;
        }
        return drg();
    }

    @C5566aOg
    @C2499fr
    public int drj() {
        switch (bFf().mo6893i(iGG)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, iGG, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, iGG, new Object[0]));
                break;
        }
        return dri();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: h */
    public void mo11178h(String str, int i, int i2) {
        switch (bFf().mo6893i(iGz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGz, new Object[]{str, new Integer(i), new Integer(i2)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGz, new Object[]{str, new Integer(i), new Integer(i2)}));
                break;
        }
        m17860g(str, i, i2);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: k */
    public void mo11179k(Player aku, String str) {
        switch (bFf().mo6893i(iGD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGD, new Object[]{aku, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGD, new Object[]{aku, str}));
                break;
        }
        m17861j(aku, str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: l */
    public void mo11180l(UserConnection apk) {
        switch (bFf().mo6893i(iGC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGC, new Object[]{apk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGC, new Object[]{apk}));
                break;
        }
        m17862k(apk);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "d50c78e8967f9f704c277fe9b4dbc28f", aum = 0)
    /* renamed from: cA */
    private void m17858cA(Player aku) {
        CloningDefaults xd = ala().aJe().mo19010xd();
        aku.mo14351b(xd.bSK(), xd.bTc(), xd.bTg(), (MissionTemplate) null);
    }

    @C0064Am(aul = "7a50e37acdc88060c078b2a7152a1ffa", aum = 0)
    /* renamed from: a */
    private Method m17857a(Class<?> cls, String str, Class<?>[] clsArr) {
        try {
            return cls.getDeclaredMethod(str, clsArr);
        } catch (SecurityException e) {
            return null;
        } catch (NoSuchMethodException e2) {
            Class<? super Object> superclass = cls.getSuperclass();
            if (superclass != Object.class) {
                return getMethod(superclass, str, clsArr);
            }
            return null;
        }
    }
}
