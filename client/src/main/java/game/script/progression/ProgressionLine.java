package game.script.progression;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C0232Cu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@C2712iu(mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
@C6485anp
/* renamed from: a.aFY */
/* compiled from: a */
public class ProgressionLine extends TaikodomObject implements C1616Xf {

    /* renamed from: BX */
    public static final C5663aRz f2818BX = null;

    /* renamed from: NY */
    public static final C5663aRz f2819NY = null;

    /* renamed from: QA */
    public static final C5663aRz f2820QA = null;
    /* renamed from: QC */
    public static final C5663aRz f2822QC = null;
    /* renamed from: Qz */
    public static final C5663aRz f2823Qz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_push_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm akP = null;
    public static final C5663aRz hOk = null;
    public static final C2491fm hOl = null;
    public static final C2491fm hOm = null;
    public static final C2491fm hOn = null;
    public static final C2491fm hOo = null;
    public static final C2491fm hOp = null;
    public static final C2491fm hOq = null;
    public static final C2491fm hOr = null;
    public static final C2491fm hOs = null;
    public static final C2491fm hOt = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6bbbcc3fe5ec5520dedc31e48d951093", aum = 2)

    /* renamed from: QB */
    private static ProgressionCellType f2821QB;
    @C0064Am(aul = "514b54186169510eff075df7818edf77", aum = 4)
    private static ProgressionCell cvk;
    @C0064Am(aul = "d569adc0b3ff0782f20359c812ca278a", aum = 3)

    /* renamed from: jn */
    private static Asset f2824jn;
    @C0064Am(aul = "f3bdeee83572fd8c4e3a936b1cbda959", aum = 5)

    /* renamed from: jp */
    private static CharacterProgression f2825jp;
    @C0064Am(aul = "df857849b750fc884fa505f6c32fb984", aum = 1)

    /* renamed from: nh */
    private static I18NString f2826nh;
    @C0064Am(aul = "cdb20ce81e16fe532aa210f3c89651c8", aum = 0)

    /* renamed from: ni */
    private static I18NString f2827ni;

    static {
        m14563V();
    }

    public ProgressionLine() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionLine(C5540aNg ang) {
        super(ang);
    }

    public ProgressionLine(ProgressionLineType gwVar) {
        super((C5540aNg) null);
        super._m_script_init(gwVar);
    }

    /* renamed from: V */
    static void m14563V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ProgressionLine.class, "cdb20ce81e16fe532aa210f3c89651c8", i);
        f2823Qz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionLine.class, "df857849b750fc884fa505f6c32fb984", i2);
        f2820QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionLine.class, "6bbbcc3fe5ec5520dedc31e48d951093", i3);
        f2822QC = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionLine.class, "d569adc0b3ff0782f20359c812ca278a", i4);
        f2819NY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProgressionLine.class, "514b54186169510eff075df7818edf77", i5);
        hOk = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ProgressionLine.class, "f3bdeee83572fd8c4e3a936b1cbda959", i6);
        f2818BX = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 13)];
        C2491fm a = C4105zY.m41624a(ProgressionLine.class, "d7d77914f51359f119d3e6f6345d8989", i8);
        hOl = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionLine.class, "7384e10b960afe79a2a8b771880908c7", i9);
        hOm = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionLine.class, "28a3a1b8f3c6532e4ad78ccfab915b7e", i10);
        hOn = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionLine.class, "db9086a97041b49fbd48ee41c168a598", i11);
        akP = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionLine.class, "ebe94507b61dfe6cc7b53a6bcb2f498a", i12);
        hOo = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionLine.class, "191fa26341911c2416da13be15573a25", i13);
        hOp = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionLine.class, "12676baa8c44e948732ea2cfea0438ad", i14);
        hOq = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionLine.class, "fc780e7ccde4bd6c821e9b9c2ab76520", i15);
        hOr = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionLine.class, "a69d7831910fddbe1d86cfc08caaacde", i16);
        hOs = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionLine.class, "7d3912d5707fd1e9c0ff11891dd8bf91", i17);
        hOt = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ProgressionLine.class, "d8181a34db3b6f3f5961a9f3eddde9ab", i18);
        _f_dispose_0020_0028_0029V = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ProgressionLine.class, "00db57319086cf316f5b05ba149f89d9", i19);
        _f_push_0020_0028_0029V = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ProgressionLine.class, "1dc7a5a9faca00dcc22e0dfa2ba67267", i20);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionLine.class, C0232Cu.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m14564a(CharacterProgression nKVar) {
        bFf().mo5608dq().mo3197f(f2818BX, nKVar);
    }

    /* renamed from: b */
    private void m14567b(LinkedList<C0335EX> linkedList, ProgressionCell jyVar) {
        switch (bFf().mo6893i(hOq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hOq, new Object[]{linkedList, jyVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hOq, new Object[]{linkedList, jyVar}));
                break;
        }
        m14565a(linkedList, jyVar);
    }

    /* renamed from: b */
    private void m14568b(LinkedList<C0335EX> linkedList, ProgressionCellType kgVar) {
        switch (bFf().mo6893i(hOr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hOr, new Object[]{linkedList, kgVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hOr, new Object[]{linkedList, kgVar}));
                break;
        }
        m14566a(linkedList, kgVar);
    }

    /* renamed from: bq */
    private void m14569bq(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: br */
    private void m14570br(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: c */
    private void m14571c(ProgressionCellType kgVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "db9086a97041b49fbd48ee41c168a598", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m14572c(CharacterProgression nKVar) {
        throw new aWi(new aCE(this, akP, new Object[]{nKVar}));
    }

    private ProgressionCell cZE() {
        return (ProgressionCell) bFf().mo5608dq().mo3214p(hOk);
    }

    @C0064Am(aul = "28a3a1b8f3c6532e4ad78ccfab915b7e", aum = 0)
    @C5566aOg
    private ProgressionCell cZH() {
        throw new aWi(new aCE(this, hOn, new Object[0]));
    }

    /* renamed from: d */
    private void m14574d(LinkedList<ProgressionCell> linkedList, ProgressionCell jyVar) {
        switch (bFf().mo6893i(hOt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hOt, new Object[]{linkedList, jyVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hOt, new Object[]{linkedList, jyVar}));
                break;
        }
        m14573c(linkedList, jyVar);
    }

    /* renamed from: h */
    private void m14576h(ProgressionCell jyVar) {
        bFf().mo5608dq().mo3197f(hOk, jyVar);
    }

    @C0064Am(aul = "7384e10b960afe79a2a8b771880908c7", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m14577i(ProgressionCell jyVar) {
        throw new aWi(new aCE(this, hOm, new Object[]{jyVar}));
    }

    /* renamed from: li */
    private CharacterProgression m14578li() {
        return (CharacterProgression) bFf().mo5608dq().mo3214p(f2818BX);
    }

    @C0064Am(aul = "1dc7a5a9faca00dcc22e0dfa2ba67267", aum = 0)
    /* renamed from: qW */
    private Object m14579qW() {
        return cZK();
    }

    /* renamed from: sG */
    private Asset m14580sG() {
        return ((ProgressionLineType) getType()).mo19152sK();
    }

    /* renamed from: t */
    private void m14581t(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: vS */
    private I18NString m14582vS() {
        return ((ProgressionLineType) getType()).mo19151rP();
    }

    /* renamed from: vT */
    private I18NString m14583vT() {
        return ((ProgressionLineType) getType()).mo19154vW();
    }

    /* renamed from: vU */
    private ProgressionCellType m14584vU() {
        return ((ProgressionLineType) getType()).mo19155vY();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0232Cu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cZF();
            case 1:
                m14577i((ProgressionCell) args[0]);
                return null;
            case 2:
                return cZH();
            case 3:
                m14572c((CharacterProgression) args[0]);
                return null;
            case 4:
                return cZJ();
            case 5:
                return cZL();
            case 6:
                m14565a((LinkedList<C0335EX>) (LinkedList) args[0], (ProgressionCell) args[1]);
                return null;
            case 7:
                m14566a((LinkedList<C0335EX>) (LinkedList) args[0], (ProgressionCellType) args[1]);
                return null;
            case 8:
                return cZN();
            case 9:
                m14573c((LinkedList) args[0], (ProgressionCell) args[1]);
                return null;
            case 10:
                m14575fg();
                return null;
            case 11:
                m14562Gd();
                return null;
            case 12:
                return m14579qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public ProgressionCell cZG() {
        switch (bFf().mo6893i(hOl)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCell) bFf().mo5606d(new aCE(this, hOl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hOl, new Object[0]));
                break;
        }
        return cZF();
    }

    @C5566aOg
    public ProgressionCell cZI() {
        switch (bFf().mo6893i(hOn)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCell) bFf().mo5606d(new aCE(this, hOn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hOn, new Object[0]));
                break;
        }
        return cZH();
    }

    public ProgressionLineType cZK() {
        switch (bFf().mo6893i(hOo)) {
            case 0:
                return null;
            case 2:
                return (ProgressionLineType) bFf().mo5606d(new aCE(this, hOo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hOo, new Object[0]));
                break;
        }
        return cZJ();
    }

    public List<C0335EX> cZM() {
        switch (bFf().mo6893i(hOp)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hOp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hOp, new Object[0]));
                break;
        }
        return cZL();
    }

    public List<ProgressionCell> cZO() {
        switch (bFf().mo6893i(hOs)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hOs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hOs, new Object[0]));
                break;
        }
        return cZN();
    }

    @C5566aOg
    /* renamed from: d */
    public void mo8789d(CharacterProgression nKVar) {
        switch (bFf().mo6893i(akP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akP, new Object[]{nKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akP, new Object[]{nKVar}));
                break;
        }
        m14572c(nKVar);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m14575fg();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m14579qW();
    }

    @C5566aOg
    /* renamed from: j */
    public void mo8790j(ProgressionCell jyVar) {
        switch (bFf().mo6893i(hOm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hOm, new Object[]{jyVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hOm, new Object[]{jyVar}));
                break;
        }
        m14577i(jyVar);
    }

    public void push() {
        switch (bFf().mo6893i(_f_push_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                break;
        }
        m14562Gd();
    }

    /* renamed from: m */
    public void mo8791m(ProgressionLineType gwVar) {
        super.mo967a((C2961mJ) gwVar);
    }

    @C0064Am(aul = "d7d77914f51359f119d3e6f6345d8989", aum = 0)
    private ProgressionCell cZF() {
        return cZE();
    }

    @C0064Am(aul = "ebe94507b61dfe6cc7b53a6bcb2f498a", aum = 0)
    private ProgressionLineType cZJ() {
        return (ProgressionLineType) super.getType();
    }

    @C0064Am(aul = "191fa26341911c2416da13be15573a25", aum = 0)
    private List<C0335EX> cZL() {
        LinkedList linkedList = new LinkedList();
        if (cZE() != null) {
            m14567b((LinkedList<C0335EX>) linkedList, cZE());
        } else {
            m14568b((LinkedList<C0335EX>) linkedList, m14584vU());
        }
        return linkedList;
    }

    @C0064Am(aul = "12676baa8c44e948732ea2cfea0438ad", aum = 0)
    /* renamed from: a */
    private void m14565a(LinkedList<C0335EX> linkedList, ProgressionCell jyVar) {
        linkedList.add(new C0335EX(jyVar));
        if (jyVar.mo20007FV() != null) {
            m14567b(linkedList, jyVar.mo20007FV());
        } else {
            m14568b(linkedList, jyVar.mo20004FP().mo7630Jv());
        }
    }

    @C0064Am(aul = "fc780e7ccde4bd6c821e9b9c2ab76520", aum = 0)
    /* renamed from: a */
    private void m14566a(LinkedList<C0335EX> linkedList, ProgressionCellType kgVar) {
        if (kgVar != null) {
            linkedList.add(new C0335EX(kgVar));
            if (kgVar.mo7630Jv() != null) {
                m14568b(linkedList, kgVar.mo7630Jv());
            }
        }
    }

    @C0064Am(aul = "a69d7831910fddbe1d86cfc08caaacde", aum = 0)
    private List<ProgressionCell> cZN() {
        LinkedList linkedList = new LinkedList();
        m14574d(linkedList, cZE());
        return linkedList;
    }

    @C0064Am(aul = "7d3912d5707fd1e9c0ff11891dd8bf91", aum = 0)
    /* renamed from: c */
    private void m14573c(LinkedList<ProgressionCell> linkedList, ProgressionCell jyVar) {
        if (jyVar != null) {
            linkedList.add(jyVar);
            m14574d(linkedList, jyVar.mo20007FV());
        }
    }

    @C0064Am(aul = "d8181a34db3b6f3f5961a9f3eddde9ab", aum = 0)
    /* renamed from: fg */
    private void m14575fg() {
        if (cZE() != null) {
            cZE().dispose();
        }
        m14576h((ProgressionCell) null);
        m14564a((CharacterProgression) null);
        super.dispose();
    }

    @C0064Am(aul = "00db57319086cf316f5b05ba149f89d9", aum = 0)
    /* renamed from: Gd */
    private void m14562Gd() {
        super.push();
        if (cZE() != null) {
            cZE().push();
        }
    }
}
