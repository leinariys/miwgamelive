package game.script.progression;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C6011aej;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.gw */
/* compiled from: a */
public class ProgressionLineType extends BaseTaikodomContent implements C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f7800MN = null;

    /* renamed from: NY */
    public static final C5663aRz f7801NY = null;

    /* renamed from: Ob */
    public static final C2491fm f7802Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f7803Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f7804QA = null;
    /* renamed from: QC */
    public static final C5663aRz f7806QC = null;
    /* renamed from: QD */
    public static final C2491fm f7807QD = null;
    /* renamed from: QE */
    public static final C2491fm f7808QE = null;
    /* renamed from: QF */
    public static final C2491fm f7809QF = null;
    /* renamed from: QG */
    public static final C2491fm f7810QG = null;
    /* renamed from: QH */
    public static final C2491fm f7811QH = null;
    /* renamed from: QI */
    public static final C2491fm f7812QI = null;
    /* renamed from: Qz */
    public static final C5663aRz f7813Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f7814dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bca430fca9845ebf5579aa95000e3053", aum = 2)

    /* renamed from: QB */
    private static ProgressionCellType f7805QB;
    @C0064Am(aul = "ce4efdf1ab82813bcfeade524e0eab5a", aum = 3)

    /* renamed from: jn */
    private static Asset f7815jn;
    @C0064Am(aul = "8781bf3fa8f813b0ab46a160616d8b86", aum = 1)

    /* renamed from: nh */
    private static I18NString f7816nh;
    @C0064Am(aul = "4191722388e29aa24f7103486b0bcc82", aum = 0)

    /* renamed from: ni */
    private static I18NString f7817ni;

    static {
        m32426V();
    }

    public ProgressionLineType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionLineType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32426V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 4;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 10;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ProgressionLineType.class, "4191722388e29aa24f7103486b0bcc82", i);
        f7813Qz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionLineType.class, "8781bf3fa8f813b0ab46a160616d8b86", i2);
        f7804QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionLineType.class, "bca430fca9845ebf5579aa95000e3053", i3);
        f7806QC = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionLineType.class, "ce4efdf1ab82813bcfeade524e0eab5a", i4);
        f7801NY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i6 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(ProgressionLineType.class, "6a6eb661891818a24d3e9a4359f3a68f", i6);
        f7800MN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionLineType.class, "a3a190873b667f57480ce87f1f31e04f", i7);
        f7807QD = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionLineType.class, "5c893964a312d885d1923419358ee7c8", i8);
        f7808QE = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionLineType.class, "2b4c07e573c73f35086c936e158463fe", i9);
        f7809QF = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionLineType.class, "d6b0c13996bef0600d78263bbcdbfef1", i10);
        f7810QG = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionLineType.class, "f90748d49ffc87689a707b83bb77a200", i11);
        f7811QH = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionLineType.class, "318e95f71bb62d2f1d2f6ab06aaf2cd3", i12);
        f7802Ob = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionLineType.class, "ba65989fd0f8e55201ddbf8157e46796", i13);
        f7803Oc = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionLineType.class, "f178452368fca7f419791fa4eb2c6124", i14);
        f7812QI = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionLineType.class, "4b8655580d2b3d25154b66f77b9249c4", i15);
        f7814dN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionLineType.class, C6011aej.class, _m_fields, _m_methods);
    }

    /* renamed from: bq */
    private void m32428bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f7813Qz, i18NString);
    }

    /* renamed from: br */
    private void m32429br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f7804QA, i18NString);
    }

    /* renamed from: c */
    private void m32432c(ProgressionCellType kgVar) {
        bFf().mo5608dq().mo3197f(f7806QC, kgVar);
    }

    /* renamed from: sG */
    private Asset m32435sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f7801NY);
    }

    /* renamed from: t */
    private void m32437t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f7801NY, tCVar);
    }

    /* renamed from: vS */
    private I18NString m32439vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f7813Qz);
    }

    /* renamed from: vT */
    private I18NString m32440vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f7804QA);
    }

    /* renamed from: vU */
    private ProgressionCellType m32441vU() {
        return (ProgressionCellType) bFf().mo5608dq().mo3214p(f7806QC);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6011aej(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m32434rO();
            case 1:
                m32430bs((I18NString) args[0]);
                return null;
            case 2:
                return m32442vV();
            case 3:
                m32431bu((I18NString) args[0]);
                return null;
            case 4:
                return m32443vX();
            case 5:
                m32433d((ProgressionCellType) args[0]);
                return null;
            case 6:
                return m32436sJ();
            case 7:
                m32438u((Asset) args[0]);
                return null;
            case 8:
                return m32444vZ();
            case 9:
                return m32427aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f7814dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f7814dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7814dN, new Object[0]));
                break;
        }
        return m32427aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    /* renamed from: bt */
    public void mo19148bt(I18NString i18NString) {
        switch (bFf().mo6893i(f7807QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7807QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7807QD, new Object[]{i18NString}));
                break;
        }
        m32430bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo19149bv(I18NString i18NString) {
        switch (bFf().mo6893i(f7809QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7809QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7809QF, new Object[]{i18NString}));
                break;
        }
        m32431bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Root Cell Type")
    /* renamed from: e */
    public void mo19150e(ProgressionCellType kgVar) {
        switch (bFf().mo6893i(f7811QH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7811QH, new Object[]{kgVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7811QH, new Object[]{kgVar}));
                break;
        }
        m32433d(kgVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo19151rP() {
        switch (bFf().mo6893i(f7800MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f7800MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7800MN, new Object[0]));
                break;
        }
        return m32434rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo19152sK() {
        switch (bFf().mo6893i(f7802Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f7802Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7802Ob, new Object[0]));
                break;
        }
        return m32436sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    /* renamed from: v */
    public void mo19153v(Asset tCVar) {
        switch (bFf().mo6893i(f7803Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7803Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7803Oc, new Object[]{tCVar}));
                break;
        }
        m32438u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo19154vW() {
        switch (bFf().mo6893i(f7808QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f7808QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7808QE, new Object[0]));
                break;
        }
        return m32442vV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Root Cell Type")
    /* renamed from: vY */
    public ProgressionCellType mo19155vY() {
        switch (bFf().mo6893i(f7810QG)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCellType) bFf().mo5606d(new aCE(this, f7810QG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7810QG, new Object[0]));
                break;
        }
        return m32443vX();
    }

    /* renamed from: wa */
    public ProgressionLine mo19156wa() {
        switch (bFf().mo6893i(f7812QI)) {
            case 0:
                return null;
            case 2:
                return (ProgressionLine) bFf().mo5606d(new aCE(this, f7812QI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7812QI, new Object[0]));
                break;
        }
        return m32444vZ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "6a6eb661891818a24d3e9a4359f3a68f", aum = 0)
    /* renamed from: rO */
    private I18NString m32434rO() {
        return m32439vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "a3a190873b667f57480ce87f1f31e04f", aum = 0)
    /* renamed from: bs */
    private void m32430bs(I18NString i18NString) {
        m32428bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "5c893964a312d885d1923419358ee7c8", aum = 0)
    /* renamed from: vV */
    private I18NString m32442vV() {
        return m32440vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "2b4c07e573c73f35086c936e158463fe", aum = 0)
    /* renamed from: bu */
    private void m32431bu(I18NString i18NString) {
        m32429br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Root Cell Type")
    @C0064Am(aul = "d6b0c13996bef0600d78263bbcdbfef1", aum = 0)
    /* renamed from: vX */
    private ProgressionCellType m32443vX() {
        return m32441vU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Root Cell Type")
    @C0064Am(aul = "f90748d49ffc87689a707b83bb77a200", aum = 0)
    /* renamed from: d */
    private void m32433d(ProgressionCellType kgVar) {
        m32432c(kgVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "318e95f71bb62d2f1d2f6ab06aaf2cd3", aum = 0)
    /* renamed from: sJ */
    private Asset m32436sJ() {
        return m32435sG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "ba65989fd0f8e55201ddbf8157e46796", aum = 0)
    /* renamed from: u */
    private void m32438u(Asset tCVar) {
        m32437t(tCVar);
    }

    @C0064Am(aul = "f178452368fca7f419791fa4eb2c6124", aum = 0)
    /* renamed from: vZ */
    private ProgressionLine m32444vZ() {
        return (ProgressionLine) mo745aU();
    }

    @C0064Am(aul = "4b8655580d2b3d25154b66f77b9249c4", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m32427aT() {
        T t = (ProgressionLine) bFf().mo6865M(ProgressionLine.class);
        t.mo8791m(this);
        return t;
    }
}
