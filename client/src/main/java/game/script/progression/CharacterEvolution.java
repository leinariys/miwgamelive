package game.script.progression;

import game.network.message.externalizable.C1368Tv;
import game.network.message.externalizable.C2797kG;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.Actor;
import game.script.Character;
import game.script.TaikodomObject;
import game.script.item.RawMaterial;
import game.script.item.RawMaterialType;
import game.script.mission.Mission;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.space.Loot;
import game.script.space.LootItems;
import logic.aaa.C0284Df;
import logic.aaa.C1506WA;
import logic.aaa.C1992at;
import logic.aaa.C6904avs;
import logic.baa.*;
import logic.data.link.C3396rD;
import logic.data.mbean.C0379FE;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@C5511aMd
@C6485anp
/* renamed from: a.eq */
/* compiled from: a */
public class CharacterEvolution extends TaikodomObject implements C1616Xf, C3396rD.C3397a {
    /* renamed from: BP */
    public static final C5663aRz f7039BP = null;
    /* renamed from: BR */
    public static final C5663aRz f7041BR = null;
    /* renamed from: BT */
    public static final C5663aRz f7043BT = null;
    /* renamed from: BV */
    public static final C5663aRz f7045BV = null;
    /* renamed from: BW */
    public static final int f7046BW = 1;
    /* renamed from: BX */
    public static final C5663aRz f7047BX = null;
    /* renamed from: BZ */
    public static final C5663aRz f7049BZ = null;
    /* renamed from: CA */
    public static final C2491fm f7050CA = null;
    /* renamed from: CB */
    public static final C2491fm f7051CB = null;
    /* renamed from: Ca */
    public static final C2491fm f7052Ca = null;
    /* renamed from: Cb */
    public static final C2491fm f7053Cb = null;
    /* renamed from: Cc */
    public static final C2491fm f7054Cc = null;
    /* renamed from: Cd */
    public static final C2491fm f7055Cd = null;
    /* renamed from: Ce */
    public static final C2491fm f7056Ce = null;
    /* renamed from: Cf */
    public static final C2491fm f7057Cf = null;
    /* renamed from: Cg */
    public static final C2491fm f7058Cg = null;
    /* renamed from: Ch */
    public static final C2491fm f7059Ch = null;
    /* renamed from: Ci */
    public static final C2491fm f7060Ci = null;
    /* renamed from: Cj */
    public static final C2491fm f7061Cj = null;
    /* renamed from: Ck */
    public static final C2491fm f7062Ck = null;
    /* renamed from: Cl */
    public static final C2491fm f7063Cl = null;
    /* renamed from: Cm */
    public static final C2491fm f7064Cm = null;
    /* renamed from: Cn */
    public static final C2491fm f7065Cn = null;
    /* renamed from: Co */
    public static final C2491fm f7066Co = null;
    /* renamed from: Cp */
    public static final C2491fm f7067Cp = null;
    /* renamed from: Cq */
    public static final C2491fm f7068Cq = null;
    /* renamed from: Cr */
    public static final C2491fm f7069Cr = null;
    /* renamed from: Cs */
    public static final C2491fm f7070Cs = null;
    /* renamed from: Ct */
    public static final C2491fm f7071Ct = null;
    /* renamed from: Cu */
    public static final C2491fm f7072Cu = null;
    /* renamed from: Cv */
    public static final C2491fm f7073Cv = null;
    /* renamed from: Cw */
    public static final C2491fm f7074Cw = null;
    /* renamed from: Cx */
    public static final C2491fm f7075Cx = null;
    /* renamed from: Cy */
    public static final C2491fm f7076Cy = null;
    /* renamed from: Cz */
    public static final C2491fm f7077Cz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: gQ */
    public static final C2491fm f7078gQ = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "dbe553999407b8f280e3d490b55111be", aum = 0)

    /* renamed from: BO */
    private static int f7038BO = 0;
    @C0064Am(aul = "7dd1faa63837a56ad8af8b39887836b1", aum = 1)

    /* renamed from: BQ */
    private static long f7040BQ = 0;
    @C0064Am(aul = "b0781ac42ed6319a1e31106db078228f", aum = 2)

    /* renamed from: BS */
    private static int f7042BS = 0;
    @C0064Am(aul = "35f4823bd662a858d3401e6411ea876b", aum = 3)

    /* renamed from: BU */
    private static ProgressionCareer f7044BU = null;
    @C0064Am(aul = "1c1a97eaf6bc29d2953f8bff49be7abb", aum = 5)

    /* renamed from: BY */
    private static C1556Wo<LootItems, List<RawMaterialType>> f7048BY = null;
    @C0064Am(aul = "4fe4f6c697dd716c75318312beffa45d", aum = 4)

    /* renamed from: jp */
    private static CharacterProgression f7079jp = null;

    static {
        m30000V();
    }

    public CharacterEvolution() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CharacterEvolution(C5540aNg ang) {
        super(ang);
    }

    public CharacterEvolution(CharacterProgression nKVar) {
        super((C5540aNg) null);
        super._m_script_init(nKVar);
    }

    /* renamed from: V */
    static void m30000V() {
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 31;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(CharacterEvolution.class, "dbe553999407b8f280e3d490b55111be", i);
        f7039BP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CharacterEvolution.class, "7dd1faa63837a56ad8af8b39887836b1", i2);
        f7041BR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CharacterEvolution.class, "b0781ac42ed6319a1e31106db078228f", i3);
        f7043BT = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CharacterEvolution.class, "35f4823bd662a858d3401e6411ea876b", i4);
        f7045BV = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CharacterEvolution.class, "4fe4f6c697dd716c75318312beffa45d", i5);
        f7047BX = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CharacterEvolution.class, "1c1a97eaf6bc29d2953f8bff49be7abb", i6);
        f7049BZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 31)];
        C2491fm a = C4105zY.m41624a(CharacterEvolution.class, "6b88b6b61078e157323940acbe063f91", i8);
        f7052Ca = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(CharacterEvolution.class, "206e663f8206583b34b10896fbfbc51a", i9);
        f7053Cb = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(CharacterEvolution.class, "d98f4f10bd05cda73b8abf826131bd27", i10);
        f7054Cc = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(CharacterEvolution.class, "aea9d28a24ca9c6339de0419b296fa1f", i11);
        f7055Cd = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(CharacterEvolution.class, "b77a0887434528c9f4fc267c6a21ff4c", i12);
        f7056Ce = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(CharacterEvolution.class, "3e0bc9d5164255091d53a16d48d6af0a", i13);
        f7057Cf = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(CharacterEvolution.class, "b7ee26a5376175b472c71beee90da7e0", i14);
        f7058Cg = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(CharacterEvolution.class, "e172f8753b850834dab938f98276afdb", i15);
        f7059Ch = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(CharacterEvolution.class, "7bb71983bd07ac176c940624be74853d", i16);
        f7060Ci = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(CharacterEvolution.class, "a5ec1a4cf83ac36b5dda634680d0cabd", i17);
        f7061Cj = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(CharacterEvolution.class, "66c283408c4d4e5380cf1dbe4d72b656", i18);
        f7062Ck = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(CharacterEvolution.class, "8e5dddfab04a7cfb16815d13fb2703a3", i19);
        f7063Cl = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(CharacterEvolution.class, "350d3c8a3ace6de9630c5f07b9a2fcc4", i20);
        f7064Cm = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(CharacterEvolution.class, "c052f45f6cf275e1b6f26ddf625a4832", i21);
        f7065Cn = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(CharacterEvolution.class, "988b6cf0110873130b3d5918e07701f3", i22);
        f7066Co = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(CharacterEvolution.class, "309cc0632eeeceb40dac9e480a338faf", i23);
        f7067Cp = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(CharacterEvolution.class, "32ba3a5b3ff4721bfe8a67a45c095a37", i24);
        _f_dispose_0020_0028_0029V = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        C2491fm a18 = C4105zY.m41624a(CharacterEvolution.class, "6367018678ce8e00ea58744c66a12551", i25);
        f7068Cq = a18;
        fmVarArr[i25] = a18;
        int i26 = i25 + 1;
        C2491fm a19 = C4105zY.m41624a(CharacterEvolution.class, "565b3b3aaf39fa4bd85624542937d622", i26);
        f7069Cr = a19;
        fmVarArr[i26] = a19;
        int i27 = i26 + 1;
        C2491fm a20 = C4105zY.m41624a(CharacterEvolution.class, "3c22720e64db8ae397e44798fed8e0e7", i27);
        f7070Cs = a20;
        fmVarArr[i27] = a20;
        int i28 = i27 + 1;
        C2491fm a21 = C4105zY.m41624a(CharacterEvolution.class, "f2b68b1c0f201eca97b84b084d288b5a", i28);
        f7071Ct = a21;
        fmVarArr[i28] = a21;
        int i29 = i28 + 1;
        C2491fm a22 = C4105zY.m41624a(CharacterEvolution.class, "49b7f6a3d466fe719a2d90575eb125b8", i29);
        f7072Cu = a22;
        fmVarArr[i29] = a22;
        int i30 = i29 + 1;
        C2491fm a23 = C4105zY.m41624a(CharacterEvolution.class, "03460666bcbc52e68bb9187e16fd8327", i30);
        f7073Cv = a23;
        fmVarArr[i30] = a23;
        int i31 = i30 + 1;
        C2491fm a24 = C4105zY.m41624a(CharacterEvolution.class, "91b516063263a9fcc1eacefb84a386c0", i31);
        f7074Cw = a24;
        fmVarArr[i31] = a24;
        int i32 = i31 + 1;
        C2491fm a25 = C4105zY.m41624a(CharacterEvolution.class, "b577a04181b368c572d4d8a723f08068", i32);
        f7075Cx = a25;
        fmVarArr[i32] = a25;
        int i33 = i32 + 1;
        C2491fm a26 = C4105zY.m41624a(CharacterEvolution.class, "e79016beaf439ae3126b41e5a2406c61", i33);
        f7076Cy = a26;
        fmVarArr[i33] = a26;
        int i34 = i33 + 1;
        C2491fm a27 = C4105zY.m41624a(CharacterEvolution.class, "73c4cacdee829795ac6b71da6e8edae9", i34);
        _f_onResurrect_0020_0028_0029V = a27;
        fmVarArr[i34] = a27;
        int i35 = i34 + 1;
        C2491fm a28 = C4105zY.m41624a(CharacterEvolution.class, "8c1a9d6767ab57eeebe74e123a4efe30", i35);
        f7077Cz = a28;
        fmVarArr[i35] = a28;
        int i36 = i35 + 1;
        C2491fm a29 = C4105zY.m41624a(CharacterEvolution.class, "a86e95af294230e31e963e1199436eb8", i36);
        f7050CA = a29;
        fmVarArr[i36] = a29;
        int i37 = i36 + 1;
        C2491fm a30 = C4105zY.m41624a(CharacterEvolution.class, "04c2d06bdcdd5729aff0a9373c16b77d", i37);
        f7051CB = a30;
        fmVarArr[i37] = a30;
        int i38 = i37 + 1;
        C2491fm a31 = C4105zY.m41624a(CharacterEvolution.class, "5a3ba54d21ffa73aacd0281959f00c65", i38);
        f7078gQ = a31;
        fmVarArr[i38] = a31;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CharacterEvolution.class, C0379FE.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "350d3c8a3ace6de9630c5f07b9a2fcc4", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30002a(Mission<?> af) {
        throw new aWi(new aCE(this, f7064Cm, new Object[]{af}));
    }

    @C0064Am(aul = "a86e95af294230e31e963e1199436eb8", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30003a(LootItems ez) {
        throw new aWi(new aCE(this, f7050CA, new Object[]{ez}));
    }

    @C0064Am(aul = "8e5dddfab04a7cfb16815d13fb2703a3", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30004a(RawMaterial abk, LootItems ez) {
        throw new aWi(new aCE(this, f7063Cl, new Object[]{abk, ez}));
    }

    @C0064Am(aul = "206e663f8206583b34b10896fbfbc51a", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30005a(Ship fAVar, float f) {
        throw new aWi(new aCE(this, f7053Cb, new Object[]{fAVar, new Float(f)}));
    }

    /* renamed from: a */
    private void m30006a(CharacterProgression nKVar) {
        bFf().mo5608dq().mo3197f(f7047BX, nKVar);
    }

    @C0064Am(aul = "988b6cf0110873130b3d5918e07701f3", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30007a(C4068yr yrVar) {
        throw new aWi(new aCE(this, f7066Co, new Object[]{yrVar}));
    }

    /* renamed from: ar */
    private void m30010ar(int i) {
        bFf().mo5608dq().mo3183b(f7039BP, i);
    }

    /* renamed from: as */
    private void m30011as(int i) {
        bFf().mo5608dq().mo3183b(f7043BT, i);
    }

    @C0064Am(aul = "a5ec1a4cf83ac36b5dda634680d0cabd", aum = 0)
    @C5566aOg
    /* renamed from: at */
    private void m30012at(int i) {
        throw new aWi(new aCE(this, f7061Cj, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "565b3b3aaf39fa4bd85624542937d622", aum = 0)
    @C5566aOg
    /* renamed from: ax */
    private void m30014ax(int i) {
        throw new aWi(new aCE(this, f7069Cr, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "f2b68b1c0f201eca97b84b084d288b5a", aum = 0)
    @C5566aOg
    /* renamed from: az */
    private void m30015az(int i) {
        throw new aWi(new aCE(this, f7071Ct, new Object[]{new Integer(i)}));
    }

    /* renamed from: b */
    private double m30016b(double d, float f) {
        switch (bFf().mo6893i(f7065Cn)) {
            case 0:
                return ScriptRuntime.NaN;
            case 2:
                return ((Double) bFf().mo5606d(new aCE(this, f7065Cn, new Object[]{new Double(d), new Float(f)}))).doubleValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7065Cn, new Object[]{new Double(d), new Float(f)}));
                break;
        }
        return m30001a(d, f);
    }

    @C0064Am(aul = "d98f4f10bd05cda73b8abf826131bd27", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m30017b(double d) {
        throw new aWi(new aCE(this, f7054Cc, new Object[]{new Double(d)}));
    }

    /* renamed from: b */
    private void m30018b(I18NString i18NString, double d, boolean z) {
        switch (bFf().mo6893i(f7058Cg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7058Cg, new Object[]{i18NString, new Double(d), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7058Cg, new Object[]{i18NString, new Double(d), new Boolean(z)}));
                break;
        }
        m30008a(i18NString, d, z);
    }

    @C0064Am(aul = "aea9d28a24ca9c6339de0419b296fa1f", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m30019d(double d) {
        throw new aWi(new aCE(this, f7055Cd, new Object[]{new Double(d)}));
    }

    @C0064Am(aul = "5a3ba54d21ffa73aacd0281959f00c65", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m30020dd() {
        throw new aWi(new aCE(this, f7078gQ, new Object[0]));
    }

    @C5566aOg
    /* renamed from: e */
    private void m30021e(double d) {
        switch (bFf().mo6893i(f7055Cd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7055Cd, new Object[]{new Double(d)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7055Cd, new Object[]{new Double(d)}));
                break;
        }
        m30019d(d);
    }

    /* renamed from: e */
    private void m30022e(ProgressionCareer sbVar) {
        bFf().mo5608dq().mo3197f(f7045BV, sbVar);
    }

    @C0064Am(aul = "3e0bc9d5164255091d53a16d48d6af0a", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m30023f(double d) {
        throw new aWi(new aCE(this, f7057Cf, new Object[]{new Double(d)}));
    }

    @C0064Am(aul = "91b516063263a9fcc1eacefb84a386c0", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m30024f(ProgressionCareer sbVar) {
        throw new aWi(new aCE(this, f7074Cw, new Object[]{sbVar}));
    }

    @C5566aOg
    /* renamed from: g */
    private void m30027g(double d) {
        switch (bFf().mo6893i(f7057Cf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7057Cf, new Object[]{new Double(d)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7057Cf, new Object[]{new Double(d)}));
                break;
        }
        m30023f(d);
    }

    /* renamed from: g */
    private void m30028g(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f7049BZ, wo);
    }

    /* renamed from: h */
    private double m30029h(int i, int i2) {
        switch (bFf().mo6893i(f7056Ce)) {
            case 0:
                return ScriptRuntime.NaN;
            case 2:
                return ((Double) bFf().mo5606d(new aCE(this, f7056Ce, new Object[]{new Integer(i), new Integer(i2)}))).doubleValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7056Ce, new Object[]{new Integer(i), new Integer(i2)}));
                break;
        }
        return m30026g(i, i2);
    }

    /* renamed from: lA */
    private Character m30031lA() {
        switch (bFf().mo6893i(f7075Cx)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, f7075Cx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7075Cx, new Object[0]));
                break;
        }
        return m30051lz();
    }

    /* renamed from: lE */
    private void m30034lE() {
        switch (bFf().mo6893i(f7077Cz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7077Cz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7077Cz, new Object[0]));
                break;
        }
        m30033lD();
    }

    /* renamed from: le */
    private int m30035le() {
        return bFf().mo5608dq().mo3212n(f7039BP);
    }

    /* renamed from: lf */
    private long m30036lf() {
        return bFf().mo5608dq().mo3213o(f7041BR);
    }

    /* renamed from: lg */
    private int m30037lg() {
        return bFf().mo5608dq().mo3212n(f7043BT);
    }

    /* renamed from: lh */
    private ProgressionCareer m30038lh() {
        return (ProgressionCareer) bFf().mo5608dq().mo3214p(f7045BV);
    }

    /* renamed from: li */
    private CharacterProgression m30039li() {
        return (CharacterProgression) bFf().mo5608dq().mo3214p(f7047BX);
    }

    /* renamed from: lj */
    private C1556Wo m30040lj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f7049BZ);
    }

    @C0064Am(aul = "e172f8753b850834dab938f98276afdb", aum = 0)
    @C5566aOg
    /* renamed from: lm */
    private void m30042lm() {
        throw new aWi(new aCE(this, f7059Ch, new Object[0]));
    }

    @C5566aOg
    /* renamed from: ln */
    private void m30043ln() {
        switch (bFf().mo6893i(f7059Ch)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7059Ch, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7059Ch, new Object[0]));
                break;
        }
        m30042lm();
    }

    @C0064Am(aul = "7bb71983bd07ac176c940624be74853d", aum = 0)
    @C5566aOg
    /* renamed from: lo */
    private void m30044lo() {
        throw new aWi(new aCE(this, f7060Ci, new Object[0]));
    }

    @C5566aOg
    /* renamed from: lp */
    private void m30045lp() {
        switch (bFf().mo6893i(f7060Ci)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7060Ci, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7060Ci, new Object[0]));
                break;
        }
        m30044lo();
    }

    @C0064Am(aul = "49b7f6a3d466fe719a2d90575eb125b8", aum = 0)
    @C5566aOg
    /* renamed from: lv */
    private void m30049lv() {
        throw new aWi(new aCE(this, f7072Cu, new Object[0]));
    }

    /* renamed from: r */
    private void m30052r(long j) {
        bFf().mo5608dq().mo3184b(f7041BR, j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0379FE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Integer(m30041lk());
            case 1:
                m30005a((Ship) args[0], ((Float) args[1]).floatValue());
                return null;
            case 2:
                m30017b(((Double) args[0]).doubleValue());
                return null;
            case 3:
                m30019d(((Double) args[0]).doubleValue());
                return null;
            case 4:
                return new Double(m30026g(((Integer) args[0]).intValue(), ((Integer) args[1]).intValue()));
            case 5:
                m30023f(((Double) args[0]).doubleValue());
                return null;
            case 6:
                m30008a((I18NString) args[0], ((Double) args[1]).doubleValue(), ((Boolean) args[2]).booleanValue());
                return null;
            case 7:
                m30042lm();
                return null;
            case 8:
                m30044lo();
                return null;
            case 9:
                m30012at(((Integer) args[0]).intValue());
                return null;
            case 10:
                return new Integer(m30013av(((Integer) args[0]).intValue()));
            case 11:
                m30004a((RawMaterial) args[0], (LootItems) args[1]);
                return null;
            case 12:
                m30002a((Mission<?>) (Mission) args[0]);
                return null;
            case 13:
                return new Double(m30001a(((Double) args[0]).doubleValue(), ((Float) args[1]).floatValue()));
            case 14:
                m30007a((C4068yr) args[0]);
                return null;
            case 15:
                return m30046lq();
            case 16:
                m30025fg();
                return null;
            case 17:
                return new Integer(m30047ls());
            case 18:
                m30014ax(((Integer) args[0]).intValue());
                return null;
            case 19:
                return new Long(m30048lu());
            case 20:
                m30015az(((Integer) args[0]).intValue());
                return null;
            case 21:
                m30049lv();
                return null;
            case 22:
                return m30050lx();
            case 23:
                m30024f((ProgressionCareer) args[0]);
                return null;
            case 24:
                return m30051lz();
            case 25:
                return m30032lB();
            case 26:
                m30009aG();
                return null;
            case 27:
                m30033lD();
                return null;
            case 28:
                m30003a((LootItems) args[0]);
                return null;
            case 29:
                m30030l((Actor) args[0]);
                return null;
            case 30:
                m30020dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: aA */
    public void mo18207aA(int i) {
        switch (bFf().mo6893i(f7071Ct)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7071Ct, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7071Ct, new Object[]{new Integer(i)}));
                break;
        }
        m30015az(i);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m30009aG();
    }

    @C5566aOg
    /* renamed from: au */
    public void mo18208au(int i) {
        switch (bFf().mo6893i(f7061Cj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7061Cj, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7061Cj, new Object[]{new Integer(i)}));
                break;
        }
        m30012at(i);
    }

    /* renamed from: aw */
    public int mo18209aw(int i) {
        switch (bFf().mo6893i(f7062Ck)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f7062Ck, new Object[]{new Integer(i)}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7062Ck, new Object[]{new Integer(i)}));
                break;
        }
        return m30013av(i);
    }

    @C5566aOg
    /* renamed from: ay */
    public void mo18210ay(int i) {
        switch (bFf().mo6893i(f7069Cr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7069Cr, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7069Cr, new Object[]{new Integer(i)}));
                break;
        }
        m30014ax(i);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18211b(Mission<?> af) {
        switch (bFf().mo6893i(f7064Cm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7064Cm, new Object[]{af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7064Cm, new Object[]{af}));
                break;
        }
        m30002a(af);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18212b(LootItems ez) {
        switch (bFf().mo6893i(f7050CA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7050CA, new Object[]{ez}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7050CA, new Object[]{ez}));
                break;
        }
        m30003a(ez);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18213b(RawMaterial abk, LootItems ez) {
        switch (bFf().mo6893i(f7063Cl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7063Cl, new Object[]{abk, ez}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7063Cl, new Object[]{abk, ez}));
                break;
        }
        m30004a(abk, ez);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18214b(Ship fAVar, float f) {
        switch (bFf().mo6893i(f7053Cb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7053Cb, new Object[]{fAVar, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7053Cb, new Object[]{fAVar, new Float(f)}));
                break;
        }
        m30005a(fAVar, f);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18216b(C4068yr yrVar) {
        switch (bFf().mo6893i(f7066Co)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7066Co, new Object[]{yrVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7066Co, new Object[]{yrVar}));
                break;
        }
        m30007a(yrVar);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo18217c(double d) {
        switch (bFf().mo6893i(f7054Cc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7054Cc, new Object[]{new Double(d)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7054Cc, new Object[]{new Double(d)}));
                break;
        }
        m30017b(d);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: de */
    public void mo18218de() {
        switch (bFf().mo6893i(f7078gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7078gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7078gQ, new Object[0]));
                break;
        }
        m30020dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m30025fg();
    }

    /* renamed from: en */
    public long mo18219en() {
        switch (bFf().mo6893i(f7070Cs)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7070Cs, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7070Cs, new Object[0]));
                break;
        }
        return m30048lu();
    }

    @C5566aOg
    /* renamed from: g */
    public void mo18220g(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(f7074Cw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7074Cw, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7074Cw, new Object[]{sbVar}));
                break;
        }
        m30024f(sbVar);
    }

    /* renamed from: lC */
    public C1556Wo<LootItems, List<RawMaterialType>> mo18221lC() {
        switch (bFf().mo6893i(f7076Cy)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, f7076Cy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7076Cy, new Object[0]));
                break;
        }
        return m30032lB();
    }

    /* renamed from: ll */
    public int mo18222ll() {
        switch (bFf().mo6893i(f7052Ca)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f7052Ca, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7052Ca, new Object[0]));
                break;
        }
        return m30041lk();
    }

    /* renamed from: lr */
    public Map<Integer, Integer> mo18223lr() {
        switch (bFf().mo6893i(f7067Cp)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, f7067Cp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7067Cp, new Object[0]));
                break;
        }
        return m30046lq();
    }

    /* renamed from: lt */
    public int mo18224lt() {
        switch (bFf().mo6893i(f7068Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f7068Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7068Cq, new Object[0]));
                break;
        }
        return m30047ls();
    }

    @C5566aOg
    /* renamed from: lw */
    public void mo18225lw() {
        switch (bFf().mo6893i(f7072Cu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7072Cu, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7072Cu, new Object[0]));
                break;
        }
        m30049lv();
    }

    /* renamed from: ly */
    public CharacterProgression mo18226ly() {
        switch (bFf().mo6893i(f7073Cv)) {
            case 0:
                return null;
            case 2:
                return (CharacterProgression) bFf().mo5606d(new aCE(this, f7073Cv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7073Cv, new Object[0]));
                break;
        }
        return m30050lx();
    }

    /* renamed from: m */
    public void mo18227m(Actor cr) {
        switch (bFf().mo6893i(f7051CB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7051CB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7051CB, new Object[]{cr}));
                break;
        }
        m30030l(cr);
    }

    /* renamed from: b */
    public void mo18215b(CharacterProgression nKVar) {
        super.mo10S();
        m30006a(nKVar);
        m30010ar(1);
        ProgressionDefaults cqL = ala().aJk().cqL();
        if (cqL != null) {
            m30011as(cqL.cgb());
        }
    }

    @C0064Am(aul = "6b88b6b61078e157323940acbe063f91", aum = 0)
    /* renamed from: lk */
    private int m30041lk() {
        return m30037lg();
    }

    @C0064Am(aul = "b77a0887434528c9f4fc267c6a21ff4c", aum = 0)
    /* renamed from: g */
    private double m30026g(int i, int i2) {
        if (i >= mo18224lt()) {
            return ((double) i2) * (1.0d + (0.05d * ((double) (i - mo18224lt()))));
        }
        return (double) ((1 - ((mo18224lt() - i) / 6)) * i2);
    }

    @C0064Am(aul = "b7ee26a5376175b472c71beee90da7e0", aum = 0)
    /* renamed from: a */
    private void m30008a(I18NString i18NString, double d, boolean z) {
        if (mo18226ly().mo20739lA() instanceof Player) {
            Player aku = (Player) mo18226ly().mo20739lA();
            if (z) {
                aku.mo14419f((C1506WA) new C6904avs(mo18224lt()));
            } else {
                aku.mo14419f((C1506WA) new C1992at(mo18219en()));
            }
            aku.mo14419f((C1506WA) new C0284Df(i18NString, false, Double.valueOf(d)));
            aku.mo14419f((C1506WA) new C2797kG((Player) null, aku, C1368Tv.C1369a.ACTIONS, i18NString, Double.valueOf(d)));
        }
    }

    @C0064Am(aul = "66c283408c4d4e5380cf1dbe4d72b656", aum = 0)
    /* renamed from: av */
    private int m30013av(int i) {
        if (mo18223lr().containsKey(Integer.valueOf(i))) {
            return mo18223lr().get(Integer.valueOf(i)).intValue();
        }
        return 0;
    }

    @C0064Am(aul = "c052f45f6cf275e1b6f26ddf625a4832", aum = 0)
    /* renamed from: a */
    private double m30001a(double d, float f) {
        if (f != 0.0f) {
            return d * ((double) f);
        }
        return d;
    }

    @C0064Am(aul = "309cc0632eeeceb40dac9e480a338faf", aum = 0)
    /* renamed from: lq */
    private Map<Integer, Integer> m30046lq() {
        return ala().aJk().cqL().mo14476lr();
    }

    @C0064Am(aul = "32ba3a5b3ff4721bfe8a67a45c095a37", aum = 0)
    /* renamed from: fg */
    private void m30025fg() {
        m30022e((ProgressionCareer) null);
        m30006a((CharacterProgression) null);
        m30040lj().clear();
        super.dispose();
    }

    @C0064Am(aul = "6367018678ce8e00ea58744c66a12551", aum = 0)
    /* renamed from: ls */
    private int m30047ls() {
        return m30035le();
    }

    @C0064Am(aul = "3c22720e64db8ae397e44798fed8e0e7", aum = 0)
    /* renamed from: lu */
    private long m30048lu() {
        return m30036lf();
    }

    @C0064Am(aul = "03460666bcbc52e68bb9187e16fd8327", aum = 0)
    /* renamed from: lx */
    private CharacterProgression m30050lx() {
        return m30039li();
    }

    @C0064Am(aul = "b577a04181b368c572d4d8a723f08068", aum = 0)
    /* renamed from: lz */
    private Character m30051lz() {
        return m30039li().mo20739lA();
    }

    @C0064Am(aul = "e79016beaf439ae3126b41e5a2406c61", aum = 0)
    /* renamed from: lB */
    private C1556Wo<LootItems, List<RawMaterialType>> m30032lB() {
        return m30040lj();
    }

    @C0064Am(aul = "73c4cacdee829795ac6b71da6e8edae9", aum = 0)
    /* renamed from: aG */
    private void m30009aG() {
        super.mo70aH();
        m30034lE();
    }

    @C0064Am(aul = "8c1a9d6767ab57eeebe74e123a4efe30", aum = 0)
    /* renamed from: lD */
    private void m30033lD() {
        m30040lj().clear();
    }

    @C0064Am(aul = "04c2d06bdcdd5729aff0a9373c16b77d", aum = 0)
    /* renamed from: l */
    private void m30030l(Actor cr) {
        if (cr instanceof Loot) {
            mo18212b(((Loot) cr).dtA());
        }
    }
}
