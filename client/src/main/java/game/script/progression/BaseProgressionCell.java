package game.script.progression;

import game.network.message.externalizable.aCE;
import game.script.template.BaseTaikodomContent;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5507aLz;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aAK */
/* compiled from: a */
public abstract class BaseProgressionCell extends BaseTaikodomContent implements C1616Xf {
    public static final C2491fm _f_push_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm asB = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m12541V();
    }

    public BaseProgressionCell() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseProgressionCell(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12541V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 0;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 2;
        _m_fields = new C5663aRz[(BaseTaikodomContent._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(BaseProgressionCell.class, "730cb579bf1592d03f52abef5d2aef80", i);
        asB = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseProgressionCell.class, "c3bcdc0e482e58a44996dca1fbac6ab4", i2);
        _f_push_0020_0028_0029V = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseProgressionCell.class, C5507aLz.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "730cb579bf1592d03f52abef5d2aef80", aum = 0)
    /* renamed from: Ju */
    private ProgressionCellType m12540Ju() {
        throw new aWi(new aCE(this, asB, new Object[0]));
    }

    /* renamed from: Jv */
    public ProgressionCellType mo7630Jv() {
        switch (bFf().mo6893i(asB)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCellType) bFf().mo5606d(new aCE(this, asB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, asB, new Object[0]));
                break;
        }
        return m12540Ju();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5507aLz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m12540Ju();
            case 1:
                m12539Gd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void push() {
        switch (bFf().mo6893i(_f_push_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                break;
        }
        m12539Gd();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "c3bcdc0e482e58a44996dca1fbac6ab4", aum = 0)
    /* renamed from: Gd */
    private void m12539Gd() {
        super.push();
        if (mo7630Jv() != null) {
            mo7630Jv().push();
        }
    }
}
