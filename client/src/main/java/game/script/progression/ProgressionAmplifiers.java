package game.script.progression;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.item.Amplifier;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aIC;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.QD */
/* compiled from: a */
public class ProgressionAmplifiers extends ItemLocation implements C1616Xf {

    /* renamed from: RA */
    public static final C2491fm f1415RA = null;

    /* renamed from: RD */
    public static final C2491fm f1416RD = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz avF = null;
    /* renamed from: ku */
    public static final C2491fm f1417ku = null;
    public static final long serialVersionUID = 0;
    private static final Log log = LogPrinter.setClass(Amplifier.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a7fae6cd030c1ee38f882ab3d18611a7", aum = 0)
    private static Character aKN;

    static {
        m8716V();
    }

    public ProgressionAmplifiers() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionAmplifiers(C5540aNg ang) {
        super(ang);
    }

    public ProgressionAmplifiers(Character acx) {
        super((C5540aNg) null);
        super._m_script_init(acx);
    }

    /* renamed from: V */
    static void m8716V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 1;
        _m_methodCount = ItemLocation._m_methodCount + 3;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ProgressionAmplifiers.class, "a7fae6cd030c1ee38f882ab3d18611a7", i);
        avF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i3 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(ProgressionAmplifiers.class, "001c1bc6b2f7edbe789d20f0f07315f5", i3);
        f1415RA = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionAmplifiers.class, "1dca261d00be246aa27009e87259874b", i4);
        f1416RD = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionAmplifiers.class, "4c28cfcd69b60822ed21725ff64cb853", i5);
        f1417ku = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionAmplifiers.class, aIC.class, _m_fields, _m_methods);
    }

    /* renamed from: QY */
    private Character m8715QY() {
        return (Character) bFf().mo5608dq().mo3214p(avF);
    }

    /* renamed from: g */
    private void m8718g(Character acx) {
        bFf().mo5608dq().mo3197f(avF, acx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aIC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                m8717a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 1:
                m8720m((Item) args[0]);
                return null;
            case 2:
                m8719g((Item) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f1415RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1415RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1415RA, new Object[]{auq, aag}));
                break;
        }
        m8717a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f1417ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1417ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1417ku, new Object[]{auq}));
                break;
        }
        m8719g(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f1416RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1416RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1416RD, new Object[]{auq}));
                break;
        }
        m8720m(auq);
    }

    /* renamed from: h */
    public void mo4931h(Character acx) {
        super.mo10S();
        m8718g(acx);
        log.debug("New owner: " + acx);
    }

    @C0064Am(aul = "001c1bc6b2f7edbe789d20f0f07315f5", aum = 0)
    /* renamed from: a */
    private void m8717a(Item auq, ItemLocation aag) {
        log.trace("Begin:onItemAdded");
        if (auq instanceof Amplifier) {
            ((Amplifier) auq).mo23333B(m8715QY().bQx());
            log.debug("New amplifier's ship: " + m8715QY().bQx());
        }
        logger.trace("End:onItemAdded");
    }

    @C0064Am(aul = "1dca261d00be246aa27009e87259874b", aum = 0)
    /* renamed from: m */
    private void m8720m(Item auq) {
        logger.trace("Begin:onItemRemoved");
        if (auq instanceof Amplifier) {
            ((Amplifier) auq).mo23333B((Ship) null);
            logger.debug("Removed amplifier's ship: " + m8715QY().bQx());
        }
        logger.trace("End:onItemRemoved");
    }

    @C0064Am(aul = "4c28cfcd69b60822ed21725ff64cb853", aum = 0)
    /* renamed from: g */
    private void m8719g(Item auq) {
    }
}
