package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.avatar.cloth.Cloth;
import game.script.item.WeaponType;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C3027nB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.sb */
/* compiled from: a */
public class ProgressionCareer extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f9122MN = null;

    /* renamed from: Pe */
    public static final C5663aRz f9123Pe = null;

    /* renamed from: QA */
    public static final C5663aRz f9124QA = null;

    /* renamed from: QD */
    public static final C2491fm f9125QD = null;

    /* renamed from: QE */
    public static final C2491fm f9126QE = null;

    /* renamed from: QF */
    public static final C2491fm f9127QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f9128Qz = null;
    /* renamed from: RH */
    public static final C5663aRz f9130RH = null;
    /* renamed from: Sw */
    public static final C2491fm f9131Sw = null;
    /* renamed from: Sx */
    public static final C2491fm f9132Sx = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f9134bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9135bM = null;
    /* renamed from: bN */
    public static final C2491fm f9136bN = null;
    /* renamed from: bO */
    public static final C2491fm f9137bO = null;
    /* renamed from: bP */
    public static final C2491fm f9138bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9139bQ = null;
    public static final C5663aRz beI = null;
    public static final C5663aRz beJ = null;
    public static final C5663aRz beK = null;
    public static final C5663aRz beL = null;
    public static final C5663aRz beM = null;
    public static final C5663aRz beN = null;
    public static final C5663aRz beO = null;
    public static final C5663aRz beP = null;
    public static final C5663aRz beQ = null;
    public static final C2491fm beR = null;
    public static final C2491fm beS = null;
    public static final C2491fm beT = null;
    public static final C2491fm beU = null;
    public static final C2491fm beV = null;
    public static final C2491fm beW = null;
    public static final C2491fm beX = null;
    public static final C2491fm beY = null;
    public static final C2491fm beZ = null;
    public static final C2491fm bfa = null;
    public static final C2491fm bfb = null;
    public static final C2491fm bfc = null;
    public static final C2491fm bfd = null;
    public static final C2491fm bfe = null;
    public static final C2491fm bff = null;
    public static final C2491fm bfg = null;
    public static final C2491fm bfh = null;
    public static final C2491fm bfi = null;
    public static final C2491fm bfj = null;
    public static final C2491fm bfk = null;
    public static final C2491fm bfl = null;
    public static final C2491fm bfm = null;
    public static final C2491fm bfn = null;
    public static final C2491fm bfo = null;
    public static final C2491fm bfp = null;
    public static final C2491fm bfq = null;
    public static final C2491fm bfr = null;
    public static final C2491fm bfs = null;
    public static final C2491fm bft = null;
    public static final C2491fm bfu = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uS */
    public static final C5663aRz f9142uS = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d08561ec8fa49e372d81c95f0ff48bca", aum = 2)

    /* renamed from: RG */
    private static Station f9129RG;
    @C0064Am(aul = "f7ee4ca1ff596d6af6bfa15df4ec9d42", aum = 4)
    private static C3438ra<WeaponType> aIA;
    @C0064Am(aul = "1111c80b8a768c5296a7fb2b0e544ed8", aum = 5)
    private static MPMultiplier aIB;
    @C0064Am(aul = "c75bb8860ed552ae6e6341938ff30d36", aum = 6)
    private static C3438ra<ProgressionLineType> aIC;
    @C0064Am(aul = "07675bda06c93ab7cdcfb190f90a5c2a", aum = 7)
    private static C3438ra<ProgressionLineType> aID;
    @C0064Am(aul = "28ea516e3aec04ac9e9035b7a0c3a627", aum = 9)
    private static Cloth aIE;
    @C0064Am(aul = "cdfead3a1a0868c8e513c18ca5df2367", aum = 10)
    private static String aIF;
    @C0064Am(aul = "35e8e483aeddfbd80eb07f388220b1d7", aum = 11)
    private static int aIG;
    @C0064Am(aul = "32c60747b634cf66c4295d0e2d325302", aum = 12)
    private static int aIH;
    @C0064Am(aul = "1db747889b79f7dde4f4c0913fca7d98", aum = 13)
    private static int aII;
    @C0064Am(aul = "f80ddd73e93ab9d4fc0d6dc42c087fb0", aum = 14)
    private static int aIJ;
    @C0064Am(aul = "d93bdd6bd69ad95e05adf923815cbb2a", aum = 3)
    private static C3438ra<ShipType> aIz;
    @C0064Am(aul = "c8669be0bc35bd0b2dd856e85ec2ed09", aum = 15)

    /* renamed from: bK */
    private static UUID f9133bK;
    @C0064Am(aul = "028a2b5f2c90dbd9d0460ab86c67aca6", aum = 8)
    private static String handle;
    @C0064Am(aul = "4037e155308a21510e570d182e47b2fb", aum = 1)

    /* renamed from: nh */
    private static I18NString f9140nh;
    @C0064Am(aul = "f4be6669de03d05c262115d8ba262601", aum = 0)

    /* renamed from: ni */
    private static I18NString f9141ni;

    static {
        m38911V();
    }

    public ProgressionCareer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionCareer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38911V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 16;
        _m_methodCount = TaikodomObject._m_methodCount + 41;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 16)];
        C5663aRz b = C5640aRc.m17844b(ProgressionCareer.class, "f4be6669de03d05c262115d8ba262601", i);
        f9128Qz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionCareer.class, "4037e155308a21510e570d182e47b2fb", i2);
        f9124QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionCareer.class, "d08561ec8fa49e372d81c95f0ff48bca", i3);
        f9130RH = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionCareer.class, "d93bdd6bd69ad95e05adf923815cbb2a", i4);
        beI = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProgressionCareer.class, "f7ee4ca1ff596d6af6bfa15df4ec9d42", i5);
        beJ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ProgressionCareer.class, "1111c80b8a768c5296a7fb2b0e544ed8", i6);
        f9123Pe = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ProgressionCareer.class, "c75bb8860ed552ae6e6341938ff30d36", i7);
        beK = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ProgressionCareer.class, "07675bda06c93ab7cdcfb190f90a5c2a", i8);
        beL = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ProgressionCareer.class, "028a2b5f2c90dbd9d0460ab86c67aca6", i9);
        f9135bM = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(ProgressionCareer.class, "28ea516e3aec04ac9e9035b7a0c3a627", i10);
        beM = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(ProgressionCareer.class, "cdfead3a1a0868c8e513c18ca5df2367", i11);
        beN = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(ProgressionCareer.class, "35e8e483aeddfbd80eb07f388220b1d7", i12);
        f9142uS = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(ProgressionCareer.class, "32c60747b634cf66c4295d0e2d325302", i13);
        beO = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(ProgressionCareer.class, "1db747889b79f7dde4f4c0913fca7d98", i14);
        beP = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(ProgressionCareer.class, "f80ddd73e93ab9d4fc0d6dc42c087fb0", i15);
        beQ = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(ProgressionCareer.class, "c8669be0bc35bd0b2dd856e85ec2ed09", i16);
        f9134bL = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i18 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i18 + 41)];
        C2491fm a = C4105zY.m41624a(ProgressionCareer.class, "00ef1a6d567ae40a90081f2b4c78c621", i18);
        f9136bN = a;
        fmVarArr[i18] = a;
        int i19 = i18 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionCareer.class, "905425279462c8f3d19eaa6ad006c193", i19);
        f9137bO = a2;
        fmVarArr[i19] = a2;
        int i20 = i19 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionCareer.class, "92ec25735f0b4a073f8250fe85ffdc32", i20);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i20] = a3;
        int i21 = i20 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionCareer.class, "2da786c41af0d1cae4704a6c9b5df4f2", i21);
        beR = a4;
        fmVarArr[i21] = a4;
        int i22 = i21 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionCareer.class, "57126bc39eaf2c8ea716bf1d3ec49059", i22);
        beS = a5;
        fmVarArr[i22] = a5;
        int i23 = i22 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionCareer.class, "896bcdc4eaaf0f0dba4cb323132cdf87", i23);
        f9138bP = a6;
        fmVarArr[i23] = a6;
        int i24 = i23 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionCareer.class, "9f1b22b929f31229e79821a9660fce34", i24);
        f9139bQ = a7;
        fmVarArr[i24] = a7;
        int i25 = i24 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionCareer.class, "9c1e3e74ae3e0cfe80398ff6b4ee4760", i25);
        f9122MN = a8;
        fmVarArr[i25] = a8;
        int i26 = i25 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionCareer.class, "29aed9cf79b0dcc6a48de78b217e951a", i26);
        f9125QD = a9;
        fmVarArr[i26] = a9;
        int i27 = i26 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionCareer.class, "12fd7edb96bc4e95d9e08cab93f9ffb0", i27);
        f9126QE = a10;
        fmVarArr[i27] = a10;
        int i28 = i27 + 1;
        C2491fm a11 = C4105zY.m41624a(ProgressionCareer.class, "a5c6be9a9d37b0da23dd8924958b164b", i28);
        f9127QF = a11;
        fmVarArr[i28] = a11;
        int i29 = i28 + 1;
        C2491fm a12 = C4105zY.m41624a(ProgressionCareer.class, "107d972909622ba4d19e59e1260cf8a9", i29);
        f9131Sw = a12;
        fmVarArr[i29] = a12;
        int i30 = i29 + 1;
        C2491fm a13 = C4105zY.m41624a(ProgressionCareer.class, "94822db325b0b632884ed738e3d1607d", i30);
        f9132Sx = a13;
        fmVarArr[i30] = a13;
        int i31 = i30 + 1;
        C2491fm a14 = C4105zY.m41624a(ProgressionCareer.class, "c79322f2a6940e7f04b26ef6c1de5560", i31);
        beT = a14;
        fmVarArr[i31] = a14;
        int i32 = i31 + 1;
        C2491fm a15 = C4105zY.m41624a(ProgressionCareer.class, "000e89ebaa6e8411026d3611ea5b4f9c", i32);
        beU = a15;
        fmVarArr[i32] = a15;
        int i33 = i32 + 1;
        C2491fm a16 = C4105zY.m41624a(ProgressionCareer.class, "d6d22528f955ca74a99da9c3153e45b4", i33);
        beV = a16;
        fmVarArr[i33] = a16;
        int i34 = i33 + 1;
        C2491fm a17 = C4105zY.m41624a(ProgressionCareer.class, "a7318fdd423465eb272373464ede1bca", i34);
        beW = a17;
        fmVarArr[i34] = a17;
        int i35 = i34 + 1;
        C2491fm a18 = C4105zY.m41624a(ProgressionCareer.class, "ea8c1743941dc05c1dc2cc7dedfe2ed8", i35);
        beX = a18;
        fmVarArr[i35] = a18;
        int i36 = i35 + 1;
        C2491fm a19 = C4105zY.m41624a(ProgressionCareer.class, "c81d7e642db64152bc34b4babbbe822b", i36);
        beY = a19;
        fmVarArr[i36] = a19;
        int i37 = i36 + 1;
        C2491fm a20 = C4105zY.m41624a(ProgressionCareer.class, "8a312d7105c53488bdbc19214d353122", i37);
        beZ = a20;
        fmVarArr[i37] = a20;
        int i38 = i37 + 1;
        C2491fm a21 = C4105zY.m41624a(ProgressionCareer.class, "1f8d1868fa3f9ac2575e1550ba435738", i38);
        bfa = a21;
        fmVarArr[i38] = a21;
        int i39 = i38 + 1;
        C2491fm a22 = C4105zY.m41624a(ProgressionCareer.class, "96aa45127aeb61e044b968966e7d1922", i39);
        bfb = a22;
        fmVarArr[i39] = a22;
        int i40 = i39 + 1;
        C2491fm a23 = C4105zY.m41624a(ProgressionCareer.class, "9791236ec755ee1f8187f85249b466f1", i40);
        bfc = a23;
        fmVarArr[i40] = a23;
        int i41 = i40 + 1;
        C2491fm a24 = C4105zY.m41624a(ProgressionCareer.class, "14289d4bc4ac2b288b6fd1a5a9ff0b53", i41);
        bfd = a24;
        fmVarArr[i41] = a24;
        int i42 = i41 + 1;
        C2491fm a25 = C4105zY.m41624a(ProgressionCareer.class, "91c765cc1038a26714bf8daeaa816b97", i42);
        bfe = a25;
        fmVarArr[i42] = a25;
        int i43 = i42 + 1;
        C2491fm a26 = C4105zY.m41624a(ProgressionCareer.class, "c470c06235b0e62c4c9a829709262a75", i43);
        bff = a26;
        fmVarArr[i43] = a26;
        int i44 = i43 + 1;
        C2491fm a27 = C4105zY.m41624a(ProgressionCareer.class, "65326427c90539bee52122f7fa226f73", i44);
        bfg = a27;
        fmVarArr[i44] = a27;
        int i45 = i44 + 1;
        C2491fm a28 = C4105zY.m41624a(ProgressionCareer.class, "74705ac2ab025c728dbef2aa5ff18b8e", i45);
        bfh = a28;
        fmVarArr[i45] = a28;
        int i46 = i45 + 1;
        C2491fm a29 = C4105zY.m41624a(ProgressionCareer.class, "2b6f5ab034f3a254d67e59e97feb23f7", i46);
        bfi = a29;
        fmVarArr[i46] = a29;
        int i47 = i46 + 1;
        C2491fm a30 = C4105zY.m41624a(ProgressionCareer.class, "1776609b03e778cc9c3395737d0447b9", i47);
        bfj = a30;
        fmVarArr[i47] = a30;
        int i48 = i47 + 1;
        C2491fm a31 = C4105zY.m41624a(ProgressionCareer.class, "2f0646b48ec9e988b1999354388cbf6d", i48);
        bfk = a31;
        fmVarArr[i48] = a31;
        int i49 = i48 + 1;
        C2491fm a32 = C4105zY.m41624a(ProgressionCareer.class, "87e03c935a365126eb36be9b4e3ec876", i49);
        bfl = a32;
        fmVarArr[i49] = a32;
        int i50 = i49 + 1;
        C2491fm a33 = C4105zY.m41624a(ProgressionCareer.class, "3ac19ee77b2bb19fba5d82dd27146f9b", i50);
        bfm = a33;
        fmVarArr[i50] = a33;
        int i51 = i50 + 1;
        C2491fm a34 = C4105zY.m41624a(ProgressionCareer.class, "071c0e140ca4493e8079228de8fedc56", i51);
        bfn = a34;
        fmVarArr[i51] = a34;
        int i52 = i51 + 1;
        C2491fm a35 = C4105zY.m41624a(ProgressionCareer.class, "a8d824861e8aad1fc1000b47cb23e5ea", i52);
        bfo = a35;
        fmVarArr[i52] = a35;
        int i53 = i52 + 1;
        C2491fm a36 = C4105zY.m41624a(ProgressionCareer.class, "003ce7b5186901b31b19cd189dc68e7e", i53);
        bfp = a36;
        fmVarArr[i53] = a36;
        int i54 = i53 + 1;
        C2491fm a37 = C4105zY.m41624a(ProgressionCareer.class, "8dd9ae48047cc9527dbe16cee4c6158c", i54);
        bfq = a37;
        fmVarArr[i54] = a37;
        int i55 = i54 + 1;
        C2491fm a38 = C4105zY.m41624a(ProgressionCareer.class, "1aaf68c810677cace23124fc17f562f8", i55);
        bfr = a38;
        fmVarArr[i55] = a38;
        int i56 = i55 + 1;
        C2491fm a39 = C4105zY.m41624a(ProgressionCareer.class, "16318e12b46f22ad1566eca7984de94c", i56);
        bfs = a39;
        fmVarArr[i56] = a39;
        int i57 = i56 + 1;
        C2491fm a40 = C4105zY.m41624a(ProgressionCareer.class, "0636b1012d44250e1bcae330adfd5426", i57);
        bft = a40;
        fmVarArr[i57] = a40;
        int i58 = i57 + 1;
        C2491fm a41 = C4105zY.m41624a(ProgressionCareer.class, "2b30778b715b708b552feab44d6dd5d9", i58);
        bfu = a41;
        fmVarArr[i58] = a41;
        int i59 = i58 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionCareer.class, C3027nB.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m38907F(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(beI, raVar);
    }

    /* renamed from: G */
    private void m38908G(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(beJ, raVar);
    }

    /* renamed from: H */
    private void m38909H(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(beK, raVar);
    }

    /* renamed from: I */
    private void m38910I(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(beL, raVar);
    }

    /* renamed from: a */
    private void m38912a(MPMultiplier tx) {
        bFf().mo5608dq().mo3197f(f9123Pe, tx);
    }

    /* renamed from: a */
    private void m38913a(Cloth wj) {
        bFf().mo5608dq().mo3197f(beM, wj);
    }

    /* renamed from: a */
    private void m38914a(String str) {
        bFf().mo5608dq().mo3197f(f9135bM, str);
    }

    /* renamed from: a */
    private void m38915a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9134bL, uuid);
    }

    private C3438ra aam() {
        return (C3438ra) bFf().mo5608dq().mo3214p(beI);
    }

    private C3438ra aan() {
        return (C3438ra) bFf().mo5608dq().mo3214p(beJ);
    }

    private MPMultiplier aao() {
        return (MPMultiplier) bFf().mo5608dq().mo3214p(f9123Pe);
    }

    private C3438ra aap() {
        return (C3438ra) bFf().mo5608dq().mo3214p(beK);
    }

    private C3438ra aaq() {
        return (C3438ra) bFf().mo5608dq().mo3214p(beL);
    }

    private Cloth aar() {
        return (Cloth) bFf().mo5608dq().mo3214p(beM);
    }

    private String aas() {
        return (String) bFf().mo5608dq().mo3214p(beN);
    }

    private int aat() {
        return bFf().mo5608dq().mo3212n(f9142uS);
    }

    private int aau() {
        return bFf().mo5608dq().mo3212n(beO);
    }

    private int aav() {
        return bFf().mo5608dq().mo3212n(beP);
    }

    private int aaw() {
        return bFf().mo5608dq().mo3212n(beQ);
    }

    /* renamed from: an */
    private UUID m38916an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9134bL);
    }

    /* renamed from: ao */
    private String m38917ao() {
        return (String) bFf().mo5608dq().mo3214p(f9135bM);
    }

    /* renamed from: b */
    private void m38921b(Station bf) {
        bFf().mo5608dq().mo3197f(f9130RH, bf);
    }

    /* renamed from: bm */
    private void m38926bm(String str) {
        bFf().mo5608dq().mo3197f(beN, str);
    }

    /* renamed from: bq */
    private void m38928bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9128Qz, i18NString);
    }

    /* renamed from: br */
    private void m38929br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9124QA, i18NString);
    }

    /* renamed from: c */
    private void m38933c(UUID uuid) {
        switch (bFf().mo6893i(f9137bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9137bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9137bO, new Object[]{uuid}));
                break;
        }
        m38925b(uuid);
    }

    /* renamed from: ek */
    private void m38937ek(int i) {
        bFf().mo5608dq().mo3183b(f9142uS, i);
    }

    /* renamed from: el */
    private void m38938el(int i) {
        bFf().mo5608dq().mo3183b(beO, i);
    }

    /* renamed from: em */
    private void m38939em(int i) {
        bFf().mo5608dq().mo3183b(beP, i);
    }

    /* renamed from: en */
    private void m38940en(int i) {
        bFf().mo5608dq().mo3183b(beQ, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Speed")
    @C0064Am(aul = "65326427c90539bee52122f7fa226f73", aum = 0)
    @C5566aOg
    /* renamed from: eo */
    private void m38941eo(int i) {
        throw new aWi(new aCE(this, bfg, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Armor")
    @C0064Am(aul = "2b6f5ab034f3a254d67e59e97feb23f7", aum = 0)
    @C5566aOg
    /* renamed from: eq */
    private void m38942eq(int i) {
        throw new aWi(new aCE(this, bfi, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Cargo")
    @C0064Am(aul = "2f0646b48ec9e988b1999354388cbf6d", aum = 0)
    @C5566aOg
    /* renamed from: es */
    private void m38943es(int i) {
        throw new aWi(new aCE(this, bfk, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Attack")
    @C0064Am(aul = "3ac19ee77b2bb19fba5d82dd27146f9b", aum = 0)
    @C5566aOg
    /* renamed from: eu */
    private void m38944eu(int i) {
        throw new aWi(new aCE(this, bfm, new Object[]{new Integer(i)}));
    }

    /* renamed from: vS */
    private I18NString m38951vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9128Qz);
    }

    /* renamed from: vT */
    private I18NString m38952vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9124QA);
    }

    /* renamed from: wG */
    private Station m38954wG() {
        return (Station) bFf().mo5608dq().mo3214p(f9130RH);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3027nB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m38918ap();
            case 1:
                m38925b((UUID) args[0]);
                return null;
            case 2:
                return m38920au();
            case 3:
                return aax();
            case 4:
                m38923b((Cloth) args[0]);
                return null;
            case 5:
                return m38919ar();
            case 6:
                m38924b((String) args[0]);
                return null;
            case 7:
                return m38950rO();
            case 8:
                m38930bs((I18NString) args[0]);
                return null;
            case 9:
                return m38953vV();
            case 10:
                m38931bu((I18NString) args[0]);
                return null;
            case 11:
                return m38955xg();
            case 12:
                m38932c((Station) args[0]);
                return null;
            case 13:
                m38934d((ShipType) args[0]);
                return null;
            case 14:
                m38945f((ShipType) args[0]);
                return null;
            case 15:
                return aaz();
            case 16:
                return aaB();
            case 17:
                m38935e((WeaponType) args[0]);
                return null;
            case 18:
                m38946g((WeaponType) args[0]);
                return null;
            case 19:
                return aaD();
            case 20:
                return aaF();
            case 21:
                return aaH();
            case 22:
                m38922b((MPMultiplier) args[0]);
                return null;
            case 23:
                return aaJ();
            case 24:
                m38927bn((String) args[0]);
                return null;
            case 25:
                return new Integer(aaL());
            case 26:
                m38941eo(((Integer) args[0]).intValue());
                return null;
            case 27:
                return new Integer(aaN());
            case 28:
                m38942eq(((Integer) args[0]).intValue());
                return null;
            case 29:
                return new Integer(aaP());
            case 30:
                m38943es(((Integer) args[0]).intValue());
                return null;
            case 31:
                return new Integer(aaR());
            case 32:
                m38944eu(((Integer) args[0]).intValue());
                return null;
            case 33:
                m38936e((ProgressionLineType) args[0]);
                return null;
            case 34:
                m38947g((ProgressionLineType) args[0]);
                return null;
            case 35:
                return aaT();
            case 36:
                return aaV();
            case 37:
                m38948i((ProgressionLineType) args[0]);
                return null;
            case 38:
                m38949k((ProgressionLineType) args[0]);
                return null;
            case 39:
                return aaX();
            case 40:
                return aaZ();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C3438ra<ShipType> aaA() {
        switch (bFf().mo6893i(beV)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, beV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, beV, new Object[0]));
                break;
        }
        return aaz();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Ships")
    public List<ShipType> aaC() {
        switch (bFf().mo6893i(beW)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, beW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, beW, new Object[0]));
                break;
        }
        return aaB();
    }

    public C3438ra<WeaponType> aaE() {
        switch (bFf().mo6893i(beZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, beZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, beZ, new Object[0]));
                break;
        }
        return aaD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Weapons")
    public List<WeaponType> aaG() {
        switch (bFf().mo6893i(bfa)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bfa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfa, new Object[0]));
                break;
        }
        return aaF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MP Multiplier")
    public MPMultiplier aaI() {
        switch (bFf().mo6893i(bfb)) {
            case 0:
                return null;
            case 2:
                return (MPMultiplier) bFf().mo5606d(new aCE(this, bfb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfb, new Object[0]));
                break;
        }
        return aaH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CSS Handle")
    public String aaK() {
        switch (bFf().mo6893i(bfd)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bfd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfd, new Object[0]));
                break;
        }
        return aaJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Speed")
    public int aaM() {
        switch (bFf().mo6893i(bff)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bff, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bff, new Object[0]));
                break;
        }
        return aaL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Armor")
    public int aaO() {
        switch (bFf().mo6893i(bfh)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bfh, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bfh, new Object[0]));
                break;
        }
        return aaN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Cargo")
    public int aaQ() {
        switch (bFf().mo6893i(bfj)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bfj, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bfj, new Object[0]));
                break;
        }
        return aaP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Attack")
    public int aaS() {
        switch (bFf().mo6893i(bfl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bfl, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bfl, new Object[0]));
                break;
        }
        return aaR();
    }

    public C3438ra<ProgressionLineType> aaU() {
        switch (bFf().mo6893i(bfp)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, bfp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfp, new Object[0]));
                break;
        }
        return aaT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Progression Lines")
    public List<ProgressionLineType> aaW() {
        switch (bFf().mo6893i(bfq)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bfq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfq, new Object[0]));
                break;
        }
        return aaV();
    }

    public C3438ra<ProgressionLineType> aaY() {
        switch (bFf().mo6893i(bft)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, bft, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bft, new Object[0]));
                break;
        }
        return aaX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Uniform")
    public Cloth aay() {
        switch (bFf().mo6893i(beR)) {
            case 0:
                return null;
            case 2:
                return (Cloth) bFf().mo5606d(new aCE(this, beR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, beR, new Object[0]));
                break;
        }
        return aax();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "PreSet Progression Lines")
    public List<ProgressionLineType> aba() {
        switch (bFf().mo6893i(bfu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bfu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfu, new Object[0]));
                break;
        }
        return aaZ();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9136bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9136bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9136bN, new Object[0]));
                break;
        }
        return m38918ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CSS Handle")
    /* renamed from: bo */
    public void mo21941bo(String str) {
        switch (bFf().mo6893i(bfe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfe, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfe, new Object[]{str}));
                break;
        }
        m38927bn(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    /* renamed from: bt */
    public void mo21942bt(I18NString i18NString) {
        switch (bFf().mo6893i(f9125QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9125QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9125QD, new Object[]{i18NString}));
                break;
        }
        m38930bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo21943bv(I18NString i18NString) {
        switch (bFf().mo6893i(f9127QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9127QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9127QF, new Object[]{i18NString}));
                break;
        }
        m38931bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MP Multiplier")
    /* renamed from: c */
    public void mo21944c(MPMultiplier tx) {
        switch (bFf().mo6893i(bfc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfc, new Object[]{tx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfc, new Object[]{tx}));
                break;
        }
        m38922b(tx);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Uniform")
    /* renamed from: c */
    public void mo21945c(Cloth wj) {
        switch (bFf().mo6893i(beS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beS, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beS, new Object[]{wj}));
                break;
        }
        m38923b(wj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station")
    /* renamed from: d */
    public void mo21946d(Station bf) {
        switch (bFf().mo6893i(f9132Sx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9132Sx, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9132Sx, new Object[]{bf}));
                break;
        }
        m38932c(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Ships")
    /* renamed from: e */
    public void mo21947e(ShipType ng) {
        switch (bFf().mo6893i(beT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beT, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beT, new Object[]{ng}));
                break;
        }
        m38934d(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Speed")
    @C5566aOg
    /* renamed from: ep */
    public void mo21948ep(int i) {
        switch (bFf().mo6893i(bfg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfg, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfg, new Object[]{new Integer(i)}));
                break;
        }
        m38941eo(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Armor")
    @C5566aOg
    /* renamed from: er */
    public void mo21949er(int i) {
        switch (bFf().mo6893i(bfi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfi, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfi, new Object[]{new Integer(i)}));
                break;
        }
        m38942eq(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Cargo")
    @C5566aOg
    /* renamed from: et */
    public void mo21950et(int i) {
        switch (bFf().mo6893i(bfk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfk, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfk, new Object[]{new Integer(i)}));
                break;
        }
        m38943es(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attribute Attack")
    @C5566aOg
    /* renamed from: ev */
    public void mo21951ev(int i) {
        switch (bFf().mo6893i(bfm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfm, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfm, new Object[]{new Integer(i)}));
                break;
        }
        m38944eu(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Weapons")
    /* renamed from: f */
    public void mo21952f(WeaponType apt) {
        switch (bFf().mo6893i(beX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beX, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beX, new Object[]{apt}));
                break;
        }
        m38935e(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Progression Lines")
    /* renamed from: f */
    public void mo21953f(ProgressionLineType gwVar) {
        switch (bFf().mo6893i(bfn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfn, new Object[]{gwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfn, new Object[]{gwVar}));
                break;
        }
        m38936e(gwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Ships")
    /* renamed from: g */
    public void mo21954g(ShipType ng) {
        switch (bFf().mo6893i(beU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beU, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beU, new Object[]{ng}));
                break;
        }
        m38945f(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9138bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9138bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9138bP, new Object[0]));
                break;
        }
        return m38919ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9139bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9139bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9139bQ, new Object[]{str}));
                break;
        }
        m38924b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Weapons")
    /* renamed from: h */
    public void mo21955h(WeaponType apt) {
        switch (bFf().mo6893i(beY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beY, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beY, new Object[]{apt}));
                break;
        }
        m38946g(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Progression Lines")
    /* renamed from: h */
    public void mo21956h(ProgressionLineType gwVar) {
        switch (bFf().mo6893i(bfo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfo, new Object[]{gwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfo, new Object[]{gwVar}));
                break;
        }
        m38947g(gwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "PreSet Progression Lines")
    /* renamed from: j */
    public void mo21957j(ProgressionLineType gwVar) {
        switch (bFf().mo6893i(bfr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfr, new Object[]{gwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfr, new Object[]{gwVar}));
                break;
        }
        m38948i(gwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "PreSet Progression Lines")
    /* renamed from: l */
    public void mo21958l(ProgressionLineType gwVar) {
        switch (bFf().mo6893i(bfs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfs, new Object[]{gwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfs, new Object[]{gwVar}));
                break;
        }
        m38949k(gwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo21959rP() {
        switch (bFf().mo6893i(f9122MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9122MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9122MN, new Object[0]));
                break;
        }
        return m38950rO();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m38920au();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo21961vW() {
        switch (bFf().mo6893i(f9126QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9126QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9126QE, new Object[0]));
                break;
        }
        return m38953vV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station")
    /* renamed from: xh */
    public Station mo21962xh() {
        switch (bFf().mo6893i(f9131Sw)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f9131Sw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9131Sw, new Object[0]));
                break;
        }
        return m38955xg();
    }

    @C0064Am(aul = "00ef1a6d567ae40a90081f2b4c78c621", aum = 0)
    /* renamed from: ap */
    private UUID m38918ap() {
        return m38916an();
    }

    @C0064Am(aul = "905425279462c8f3d19eaa6ad006c193", aum = 0)
    /* renamed from: b */
    private void m38925b(UUID uuid) {
        m38915a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m38915a(UUID.randomUUID());
    }

    @C0064Am(aul = "92ec25735f0b4a073f8250fe85ffdc32", aum = 0)
    /* renamed from: au */
    private String m38920au() {
        return "ProgressionCareer: " + m38951vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Uniform")
    @C0064Am(aul = "2da786c41af0d1cae4704a6c9b5df4f2", aum = 0)
    private Cloth aax() {
        return aar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Uniform")
    @C0064Am(aul = "57126bc39eaf2c8ea716bf1d3ec49059", aum = 0)
    /* renamed from: b */
    private void m38923b(Cloth wj) {
        m38913a(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "896bcdc4eaaf0f0dba4cb323132cdf87", aum = 0)
    /* renamed from: ar */
    private String m38919ar() {
        return m38917ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "9f1b22b929f31229e79821a9660fce34", aum = 0)
    /* renamed from: b */
    private void m38924b(String str) {
        m38914a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "9c1e3e74ae3e0cfe80398ff6b4ee4760", aum = 0)
    /* renamed from: rO */
    private I18NString m38950rO() {
        return m38951vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "29aed9cf79b0dcc6a48de78b217e951a", aum = 0)
    /* renamed from: bs */
    private void m38930bs(I18NString i18NString) {
        m38928bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "12fd7edb96bc4e95d9e08cab93f9ffb0", aum = 0)
    /* renamed from: vV */
    private I18NString m38953vV() {
        return m38952vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "a5c6be9a9d37b0da23dd8924958b164b", aum = 0)
    /* renamed from: bu */
    private void m38931bu(I18NString i18NString) {
        m38929br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station")
    @C0064Am(aul = "107d972909622ba4d19e59e1260cf8a9", aum = 0)
    /* renamed from: xg */
    private Station m38955xg() {
        return m38954wG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station")
    @C0064Am(aul = "94822db325b0b632884ed738e3d1607d", aum = 0)
    /* renamed from: c */
    private void m38932c(Station bf) {
        m38921b(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Ships")
    @C0064Am(aul = "c79322f2a6940e7f04b26ef6c1de5560", aum = 0)
    /* renamed from: d */
    private void m38934d(ShipType ng) {
        aam().add(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Ships")
    @C0064Am(aul = "000e89ebaa6e8411026d3611ea5b4f9c", aum = 0)
    /* renamed from: f */
    private void m38945f(ShipType ng) {
        aam().remove(ng);
    }

    @C0064Am(aul = "d6d22528f955ca74a99da9c3153e45b4", aum = 0)
    private C3438ra<ShipType> aaz() {
        return aam();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Ships")
    @C0064Am(aul = "a7318fdd423465eb272373464ede1bca", aum = 0)
    private List<ShipType> aaB() {
        return Collections.unmodifiableList(aam());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Weapons")
    @C0064Am(aul = "ea8c1743941dc05c1dc2cc7dedfe2ed8", aum = 0)
    /* renamed from: e */
    private void m38935e(WeaponType apt) {
        aan().add(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Weapons")
    @C0064Am(aul = "c81d7e642db64152bc34b4babbbe822b", aum = 0)
    /* renamed from: g */
    private void m38946g(WeaponType apt) {
        aan().remove(apt);
    }

    @C0064Am(aul = "8a312d7105c53488bdbc19214d353122", aum = 0)
    private C3438ra<WeaponType> aaD() {
        return aan();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Weapons")
    @C0064Am(aul = "1f8d1868fa3f9ac2575e1550ba435738", aum = 0)
    private List<WeaponType> aaF() {
        return Collections.unmodifiableList(aan());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MP Multiplier")
    @C0064Am(aul = "96aa45127aeb61e044b968966e7d1922", aum = 0)
    private MPMultiplier aaH() {
        return aao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MP Multiplier")
    @C0064Am(aul = "9791236ec755ee1f8187f85249b466f1", aum = 0)
    /* renamed from: b */
    private void m38922b(MPMultiplier tx) {
        m38912a(tx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CSS Handle")
    @C0064Am(aul = "14289d4bc4ac2b288b6fd1a5a9ff0b53", aum = 0)
    private String aaJ() {
        return aas();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CSS Handle")
    @C0064Am(aul = "91c765cc1038a26714bf8daeaa816b97", aum = 0)
    /* renamed from: bn */
    private void m38927bn(String str) {
        m38926bm(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Speed")
    @C0064Am(aul = "c470c06235b0e62c4c9a829709262a75", aum = 0)
    private int aaL() {
        return aat();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Armor")
    @C0064Am(aul = "74705ac2ab025c728dbef2aa5ff18b8e", aum = 0)
    private int aaN() {
        return aau();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Cargo")
    @C0064Am(aul = "1776609b03e778cc9c3395737d0447b9", aum = 0)
    private int aaP() {
        return aav();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attribute Attack")
    @C0064Am(aul = "87e03c935a365126eb36be9b4e3ec876", aum = 0)
    private int aaR() {
        return aaw();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Progression Lines")
    @C0064Am(aul = "071c0e140ca4493e8079228de8fedc56", aum = 0)
    /* renamed from: e */
    private void m38936e(ProgressionLineType gwVar) {
        aap().add(gwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Progression Lines")
    @C0064Am(aul = "a8d824861e8aad1fc1000b47cb23e5ea", aum = 0)
    /* renamed from: g */
    private void m38947g(ProgressionLineType gwVar) {
        aap().remove(gwVar);
    }

    @C0064Am(aul = "003ce7b5186901b31b19cd189dc68e7e", aum = 0)
    private C3438ra<ProgressionLineType> aaT() {
        return aap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Progression Lines")
    @C0064Am(aul = "8dd9ae48047cc9527dbe16cee4c6158c", aum = 0)
    private List<ProgressionLineType> aaV() {
        return Collections.unmodifiableList(aap());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "PreSet Progression Lines")
    @C0064Am(aul = "1aaf68c810677cace23124fc17f562f8", aum = 0)
    /* renamed from: i */
    private void m38948i(ProgressionLineType gwVar) {
        aaq().add(gwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "PreSet Progression Lines")
    @C0064Am(aul = "16318e12b46f22ad1566eca7984de94c", aum = 0)
    /* renamed from: k */
    private void m38949k(ProgressionLineType gwVar) {
        aaq().remove(gwVar);
    }

    @C0064Am(aul = "0636b1012d44250e1bcae330adfd5426", aum = 0)
    private C3438ra<ProgressionLineType> aaX() {
        return aaq();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "PreSet Progression Lines")
    @C0064Am(aul = "2b30778b715b708b552feab44d6dd5d9", aum = 0)
    private List<ProgressionLineType> aaZ() {
        return Collections.unmodifiableList(aaq());
    }
}
