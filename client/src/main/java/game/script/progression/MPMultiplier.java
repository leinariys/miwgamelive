package game.script.progression;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C5493aLl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C3437rZ(aak = "", aal = "taikodom.game.script.progression.PMMultiplier")
@C5511aMd
@C6485anp
/* renamed from: a.TX */
/* compiled from: a */
public class MPMultiplier extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: QA */
    public static final C5663aRz f1705QA = null;

    /* renamed from: QE */
    public static final C2491fm f1706QE = null;

    /* renamed from: QF */
    public static final C2491fm f1707QF = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1709bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1710bM = null;
    /* renamed from: bN */
    public static final C2491fm f1711bN = null;
    /* renamed from: bO */
    public static final C2491fm f1712bO = null;
    /* renamed from: bP */
    public static final C2491fm f1713bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1714bQ = null;
    public static final C5663aRz elX = null;
    public static final C5663aRz elZ = null;
    public static final C5663aRz emb = null;
    public static final C5663aRz emd = null;
    public static final C5663aRz emf = null;
    public static final C5663aRz emh = null;
    public static final C5663aRz emj = null;
    public static final C2491fm emk = null;
    public static final C2491fm eml = null;
    public static final C2491fm emm = null;
    public static final C2491fm emn = null;
    public static final C2491fm emo = null;
    public static final C2491fm emp = null;
    public static final C2491fm emq = null;
    public static final C2491fm emr = null;
    public static final C2491fm ems = null;
    public static final C2491fm emt = null;
    public static final C2491fm emu = null;
    public static final C2491fm emv = null;
    public static final C2491fm emw = null;
    public static final C2491fm emx = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "dd28f82c249eec40e529fa86496abfa6", aum = 9)

    /* renamed from: bK */
    private static UUID f1708bK;
    @C0064Am(aul = "28017181c1df31b18bf64d4ab0a06ee3", aum = 2)
    private static float elW;
    @C0064Am(aul = "421590aa1f7735c3c93862fe25ff421c", aum = 3)
    private static float elY;
    @C0064Am(aul = "4f8486b3199209e09c4e4000616defed", aum = 4)
    private static float ema;
    @C0064Am(aul = "ce94bc728293e88628a8b7d521bf8aea", aum = 5)
    private static float emc;
    @C0064Am(aul = "c7777c1c2e0f2f4ecb023c981c3581d9", aum = 6)
    private static float eme;
    @C0064Am(aul = "94a18e92d688a4683bd70be278e6544e", aum = 7)
    private static float emg;
    @C0064Am(aul = "33f498a071f08c848e9444a2b329cb57", aum = 8)
    private static float emi;
    @C0064Am(aul = "fcce95cdcad34bc2376ae6160e29f8cd", aum = 0)
    private static String handle;
    @C0064Am(aul = "cb7ff7537c6f7c9d951cbbcc78a6bf22", aum = 1)

    /* renamed from: nh */
    private static I18NString f1715nh;

    static {
        m9981V();
    }

    public MPMultiplier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MPMultiplier(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9981V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 10;
        _m_methodCount = TaikodomObject._m_methodCount + 21;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(MPMultiplier.class, "fcce95cdcad34bc2376ae6160e29f8cd", i);
        f1710bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MPMultiplier.class, "cb7ff7537c6f7c9d951cbbcc78a6bf22", i2);
        f1705QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MPMultiplier.class, "28017181c1df31b18bf64d4ab0a06ee3", i3);
        elX = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MPMultiplier.class, "421590aa1f7735c3c93862fe25ff421c", i4);
        elZ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MPMultiplier.class, "4f8486b3199209e09c4e4000616defed", i5);
        emb = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(MPMultiplier.class, "ce94bc728293e88628a8b7d521bf8aea", i6);
        emd = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(MPMultiplier.class, "c7777c1c2e0f2f4ecb023c981c3581d9", i7);
        emf = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(MPMultiplier.class, "94a18e92d688a4683bd70be278e6544e", i8);
        emh = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(MPMultiplier.class, "33f498a071f08c848e9444a2b329cb57", i9);
        emj = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(MPMultiplier.class, "dd28f82c249eec40e529fa86496abfa6", i10);
        f1709bL = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i12 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 21)];
        C2491fm a = C4105zY.m41624a(MPMultiplier.class, "a0942cbb8385d72a6e0064b8e1dea519", i12);
        f1711bN = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(MPMultiplier.class, "8007af58ef2e7c7e92041f7809a77c0a", i13);
        f1712bO = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(MPMultiplier.class, "83c9933dc36a6d9764d3155dcd041128", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(MPMultiplier.class, "20881be2d3fc0f99a79286d7b0f76a0e", i15);
        emk = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(MPMultiplier.class, "7bc5add1f130fe0b0d0557d846bac7c7", i16);
        eml = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(MPMultiplier.class, "1b5f497177d4586d7711180a4c0e81b8", i17);
        emm = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(MPMultiplier.class, "529ac5f09555bf825ba1a1fb93e052e7", i18);
        emn = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(MPMultiplier.class, "b2c69f541c123684701aca99389766b0", i19);
        emo = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(MPMultiplier.class, "7e7cb335354674130c543fc710a6f4af", i20);
        emp = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(MPMultiplier.class, "05e655770b9e163e231511c2f0f0c993", i21);
        emq = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(MPMultiplier.class, "c5bb393f3e0b320a7d803b949e271fd2", i22);
        emr = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(MPMultiplier.class, "8a59fd4d85fbcc414b52261594da8339", i23);
        ems = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(MPMultiplier.class, "392a0cca3804d20d14df8c3bcb4fc327", i24);
        emt = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(MPMultiplier.class, "7091aa03b11b8cc94338727df9a2cc92", i25);
        emu = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(MPMultiplier.class, "5b851033d841473146ce7f8c6c28ad50", i26);
        emv = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(MPMultiplier.class, "f447388efd56a7fbd05bfb545a73ef71", i27);
        emw = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(MPMultiplier.class, "73a18293c6b98d3a73573b2619916b1e", i28);
        emx = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(MPMultiplier.class, "0090d19d6a9ebac1ac90345a4acd79b7", i29);
        f1713bP = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(MPMultiplier.class, "387bd5f30a040bb17fe1f2de2674deea", i30);
        f1714bQ = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(MPMultiplier.class, "b09aa5642d80d7cb7b4a151050f9d30e", i31);
        f1706QE = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(MPMultiplier.class, "2f6fcc8d61f46ddaa6eace86e3cf622c", i32);
        f1707QF = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MPMultiplier.class, C5493aLl.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9982a(String str) {
        bFf().mo5608dq().mo3197f(f1710bM, str);
    }

    /* renamed from: a */
    private void m9983a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1709bL, uuid);
    }

    /* renamed from: an */
    private UUID m9984an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1709bL);
    }

    /* renamed from: ao */
    private String m9985ao() {
        return (String) bFf().mo5608dq().mo3214p(f1710bM);
    }

    /* renamed from: br */
    private void m9991br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f1705QA, i18NString);
    }

    private float bwI() {
        return bFf().mo5608dq().mo3211m(elX);
    }

    private float bwJ() {
        return bFf().mo5608dq().mo3211m(elZ);
    }

    private float bwK() {
        return bFf().mo5608dq().mo3211m(emb);
    }

    private float bwL() {
        return bFf().mo5608dq().mo3211m(emd);
    }

    private float bwM() {
        return bFf().mo5608dq().mo3211m(emf);
    }

    private float bwN() {
        return bFf().mo5608dq().mo3211m(emh);
    }

    private float bwO() {
        return bFf().mo5608dq().mo3211m(emj);
    }

    /* renamed from: c */
    private void m9993c(UUID uuid) {
        switch (bFf().mo6893i(f1712bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1712bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1712bO, new Object[]{uuid}));
                break;
        }
        m9990b(uuid);
    }

    /* renamed from: hF */
    private void m9994hF(float f) {
        bFf().mo5608dq().mo3150a(elX, f);
    }

    /* renamed from: hG */
    private void m9995hG(float f) {
        bFf().mo5608dq().mo3150a(elZ, f);
    }

    /* renamed from: hH */
    private void m9996hH(float f) {
        bFf().mo5608dq().mo3150a(emb, f);
    }

    /* renamed from: hI */
    private void m9997hI(float f) {
        bFf().mo5608dq().mo3150a(emd, f);
    }

    /* renamed from: hJ */
    private void m9998hJ(float f) {
        bFf().mo5608dq().mo3150a(emf, f);
    }

    /* renamed from: hK */
    private void m9999hK(float f) {
        bFf().mo5608dq().mo3150a(emh, f);
    }

    /* renamed from: hL */
    private void m10000hL(float f) {
        bFf().mo5608dq().mo3150a(emj, f);
    }

    /* renamed from: vT */
    private I18NString m10008vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f1705QA);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5493aLl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m9986ap();
            case 1:
                m9990b((UUID) args[0]);
                return null;
            case 2:
                return m9988au();
            case 3:
                return new Float(bwP());
            case 4:
                m10001hM(((Float) args[0]).floatValue());
                return null;
            case 5:
                return new Float(bwR());
            case 6:
                m10002hO(((Float) args[0]).floatValue());
                return null;
            case 7:
                return new Float(bwT());
            case 8:
                m10003hQ(((Float) args[0]).floatValue());
                return null;
            case 9:
                return new Float(bwV());
            case 10:
                m10004hS(((Float) args[0]).floatValue());
                return null;
            case 11:
                return new Float(bwX());
            case 12:
                m10005hU(((Float) args[0]).floatValue());
                return null;
            case 13:
                return new Float(bwZ());
            case 14:
                m10006hW(((Float) args[0]).floatValue());
                return null;
            case 15:
                return new Float(bxb());
            case 16:
                m10007hY(((Float) args[0]).floatValue());
                return null;
            case 17:
                return m9987ar();
            case 18:
                m9989b((String) args[0]);
                return null;
            case 19:
                return m10009vV();
            case 20:
                m9992bu((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1711bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1711bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1711bN, new Object[0]));
                break;
        }
        return m9986ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo5670bv(I18NString i18NString) {
        switch (bFf().mo6893i(f1707QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1707QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1707QF, new Object[]{i18NString}));
                break;
        }
        m9992bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Killing")
    public float bwQ() {
        switch (bFf().mo6893i(emk)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, emk, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, emk, new Object[0]));
                break;
        }
        return bwP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Battle Cruiser Killing")
    public float bwS() {
        switch (bFf().mo6893i(emm)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, emm, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, emm, new Object[0]));
                break;
        }
        return bwR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Scenery Killing")
    public float bwU() {
        switch (bFf().mo6893i(emo)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, emo, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, emo, new Object[0]));
                break;
        }
        return bwT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Extract")
    public float bwW() {
        switch (bFf().mo6893i(emq)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, emq, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, emq, new Object[0]));
                break;
        }
        return bwV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Combat Mission")
    public float bwY() {
        switch (bFf().mo6893i(ems)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ems, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ems, new Object[0]));
                break;
        }
        return bwX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Trade Mission")
    public float bxa() {
        switch (bFf().mo6893i(emu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, emu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, emu, new Object[0]));
                break;
        }
        return bwZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Added")
    public float bxc() {
        switch (bFf().mo6893i(emw)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, emw, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, emw, new Object[0]));
                break;
        }
        return bxb();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1713bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1713bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1713bP, new Object[0]));
                break;
        }
        return m9987ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1714bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1714bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1714bQ, new Object[]{str}));
                break;
        }
        m9989b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Killing")
    /* renamed from: hN */
    public void mo5678hN(float f) {
        switch (bFf().mo6893i(eml)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eml, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eml, new Object[]{new Float(f)}));
                break;
        }
        m10001hM(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Battle Cruiser Killing")
    /* renamed from: hP */
    public void mo5679hP(float f) {
        switch (bFf().mo6893i(emn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emn, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emn, new Object[]{new Float(f)}));
                break;
        }
        m10002hO(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Scenery Killing")
    /* renamed from: hR */
    public void mo5680hR(float f) {
        switch (bFf().mo6893i(emp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emp, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emp, new Object[]{new Float(f)}));
                break;
        }
        m10003hQ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Extract")
    /* renamed from: hT */
    public void mo5681hT(float f) {
        switch (bFf().mo6893i(emr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emr, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emr, new Object[]{new Float(f)}));
                break;
        }
        m10004hS(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Combat Mission")
    /* renamed from: hV */
    public void mo5682hV(float f) {
        switch (bFf().mo6893i(emt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emt, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emt, new Object[]{new Float(f)}));
                break;
        }
        m10005hU(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Trade Mission")
    /* renamed from: hX */
    public void mo5683hX(float f) {
        switch (bFf().mo6893i(emv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emv, new Object[]{new Float(f)}));
                break;
        }
        m10006hW(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Added")
    /* renamed from: hZ */
    public void mo5684hZ(float f) {
        switch (bFf().mo6893i(emx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emx, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emx, new Object[]{new Float(f)}));
                break;
        }
        m10007hY(f);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m9988au();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo5686vW() {
        switch (bFf().mo6893i(f1706QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1706QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1706QE, new Object[0]));
                break;
        }
        return m10009vV();
    }

    @C0064Am(aul = "a0942cbb8385d72a6e0064b8e1dea519", aum = 0)
    /* renamed from: ap */
    private UUID m9986ap() {
        return m9984an();
    }

    @C0064Am(aul = "8007af58ef2e7c7e92041f7809a77c0a", aum = 0)
    /* renamed from: b */
    private void m9990b(UUID uuid) {
        m9983a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m9983a(UUID.randomUUID());
    }

    @C0064Am(aul = "83c9933dc36a6d9764d3155dcd041128", aum = 0)
    /* renamed from: au */
    private String m9988au() {
        return "MPMultiplier: " + m10008vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Killing")
    @C0064Am(aul = "20881be2d3fc0f99a79286d7b0f76a0e", aum = 0)
    private float bwP() {
        return bwI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Killing")
    @C0064Am(aul = "7bc5add1f130fe0b0d0557d846bac7c7", aum = 0)
    /* renamed from: hM */
    private void m10001hM(float f) {
        m9994hF(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Battle Cruiser Killing")
    @C0064Am(aul = "1b5f497177d4586d7711180a4c0e81b8", aum = 0)
    private float bwR() {
        return bwJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Battle Cruiser Killing")
    @C0064Am(aul = "529ac5f09555bf825ba1a1fb93e052e7", aum = 0)
    /* renamed from: hO */
    private void m10002hO(float f) {
        m9995hG(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Scenery Killing")
    @C0064Am(aul = "b2c69f541c123684701aca99389766b0", aum = 0)
    private float bwT() {
        return bwK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Scenery Killing")
    @C0064Am(aul = "7e7cb335354674130c543fc710a6f4af", aum = 0)
    /* renamed from: hQ */
    private void m10003hQ(float f) {
        m9996hH(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Extract")
    @C0064Am(aul = "05e655770b9e163e231511c2f0f0c993", aum = 0)
    private float bwV() {
        return bwL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Extract")
    @C0064Am(aul = "c5bb393f3e0b320a7d803b949e271fd2", aum = 0)
    /* renamed from: hS */
    private void m10004hS(float f) {
        m9997hI(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Combat Mission")
    @C0064Am(aul = "8a59fd4d85fbcc414b52261594da8339", aum = 0)
    private float bwX() {
        return bwM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Combat Mission")
    @C0064Am(aul = "392a0cca3804d20d14df8c3bcb4fc327", aum = 0)
    /* renamed from: hU */
    private void m10005hU(float f) {
        m9998hJ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Trade Mission")
    @C0064Am(aul = "7091aa03b11b8cc94338727df9a2cc92", aum = 0)
    private float bwZ() {
        return bwN();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Trade Mission")
    @C0064Am(aul = "5b851033d841473146ce7f8c6c28ad50", aum = 0)
    /* renamed from: hW */
    private void m10006hW(float f) {
        m9999hK(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Added")
    @C0064Am(aul = "f447388efd56a7fbd05bfb545a73ef71", aum = 0)
    private float bxb() {
        return bwO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Added")
    @C0064Am(aul = "73a18293c6b98d3a73573b2619916b1e", aum = 0)
    /* renamed from: hY */
    private void m10007hY(float f) {
        m10000hL(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "0090d19d6a9ebac1ac90345a4acd79b7", aum = 0)
    /* renamed from: ar */
    private String m9987ar() {
        return m9985ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "387bd5f30a040bb17fe1f2de2674deea", aum = 0)
    /* renamed from: b */
    private void m9989b(String str) {
        m9982a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "b09aa5642d80d7cb7b4a151050f9d30e", aum = 0)
    /* renamed from: vV */
    private I18NString m10009vV() {
        return m10008vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "2f6fcc8d61f46ddaa6eace86e3cf622c", aum = 0)
    /* renamed from: bu */
    private void m9992bu(I18NString i18NString) {
        m9991br(i18NString);
    }
}
