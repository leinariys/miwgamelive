package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C0679Jf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.akg  reason: case insensitive filesystem */
/* compiled from: a */
public class ProgressionDefaults extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: Cp */
    public static final C2491fm f4793Cp = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4795bL = null;
    /* renamed from: bN */
    public static final C2491fm f4796bN = null;
    /* renamed from: bO */
    public static final C2491fm f4797bO = null;
    /* renamed from: bP */
    public static final C2491fm f4798bP = null;
    public static final C5663aRz fSM = null;
    public static final C5663aRz fSN = null;
    public static final C5663aRz fSO = null;
    public static final C5663aRz fSP = null;
    public static final C5663aRz fSQ = null;
    public static final C5663aRz fSR = null;
    public static final C2491fm fSS = null;
    public static final C2491fm fST = null;
    public static final C2491fm fSU = null;
    public static final C2491fm fSV = null;
    public static final C2491fm fSW = null;
    public static final C2491fm fSX = null;
    public static final C2491fm fSY = null;
    public static final C2491fm fSZ = null;
    public static final C2491fm fTa = null;
    public static final C2491fm fTb = null;
    public static final C2491fm fTc = null;
    public static final C2491fm fTd = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a7bc1d98e4532ca932b48b01e071a9c2", aum = 6)

    /* renamed from: bK */
    private static UUID f4794bK;
    @C0064Am(aul = "ed49d3c65bca5e154247df4b637a3c55", aum = 0)
    private static int diA;
    @C0064Am(aul = "1ee26b77d268c38add823d57ba1e55e4", aum = 1)
    private static int diB;
    @C0064Am(aul = "4c871cff6a96b62de89ee50e6191697a", aum = 2)
    private static C1556Wo<Integer, Integer> diC;
    @C0064Am(aul = "3b8247560bf3ebe79f62d5dc47081a43", aum = 3)
    private static C1556Wo<Integer, Integer> diD;
    @C0064Am(aul = "f44183b5cd399891e32033fc438feb1a", aum = 4)
    private static int diE;
    @C0064Am(aul = "211bad5e5be773ff8c60c19b1f0bba1c", aum = 5)
    private static int diF;

    static {
        m23494V();
    }

    public ProgressionDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m23494V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 7;
        _m_methodCount = TaikodomObject._m_methodCount + 16;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(ProgressionDefaults.class, "ed49d3c65bca5e154247df4b637a3c55", i);
        fSM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionDefaults.class, "1ee26b77d268c38add823d57ba1e55e4", i2);
        fSN = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionDefaults.class, "4c871cff6a96b62de89ee50e6191697a", i3);
        fSO = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionDefaults.class, "3b8247560bf3ebe79f62d5dc47081a43", i4);
        fSP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProgressionDefaults.class, "f44183b5cd399891e32033fc438feb1a", i5);
        fSQ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ProgressionDefaults.class, "211bad5e5be773ff8c60c19b1f0bba1c", i6);
        fSR = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ProgressionDefaults.class, "a7bc1d98e4532ca932b48b01e071a9c2", i7);
        f4795bL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i9 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 16)];
        C2491fm a = C4105zY.m41624a(ProgressionDefaults.class, "33c94a4555c4017261951d76699d8a03", i9);
        f4796bN = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionDefaults.class, "c618c37474cef9fd01cc9f752b12912a", i10);
        f4797bO = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionDefaults.class, "97595a6113fadcebdb162acc9164d7ac", i11);
        f4798bP = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionDefaults.class, "07bc5434b170e48b5f07a2368133b6af", i12);
        fSS = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionDefaults.class, "c11eea73209ce6acad693bff7b88240c", i13);
        fST = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionDefaults.class, "cd1e70d73d9c666ecf0fe08efe178805", i14);
        fSU = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionDefaults.class, "b39b0b6eb70320c183f37f0b0a295523", i15);
        fSV = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionDefaults.class, "da412b5cf6e399a58722a6e32cbf5b59", i16);
        fSW = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionDefaults.class, "6951316e48c3f19f1d3a2ba9c6cd67ae", i17);
        fSX = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionDefaults.class, "eea285f2158caae013d5744580ca0fed", i18);
        fSY = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(ProgressionDefaults.class, "3aeb568d6b15e4ea7b35ceff46c71eee", i19);
        fSZ = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(ProgressionDefaults.class, "50bc62370124c188085c36cd975d78d1", i20);
        fTa = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(ProgressionDefaults.class, "c9e01b8612f5229181646bd9e1ce1fce", i21);
        f4793Cp = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(ProgressionDefaults.class, "d93cb34c72cce9c3de3276dbfd42d248", i22);
        fTb = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(ProgressionDefaults.class, "0b4e218265591082d05d514bbeaed022", i23);
        fTc = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(ProgressionDefaults.class, "a3f52cb13fc82d4bd8f773b3a1b9275b", i24);
        fTd = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionDefaults.class, C0679Jf.class, _m_fields, _m_methods);
    }

    /* renamed from: V */
    private void m23495V(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(fSO, wo);
    }

    /* renamed from: W */
    private void m23496W(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(fSP, wo);
    }

    /* renamed from: a */
    private void m23497a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4795bL, uuid);
    }

    /* renamed from: an */
    private UUID m23498an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4795bL);
    }

    /* renamed from: c */
    private void m23502c(UUID uuid) {
        switch (bFf().mo6893i(f4797bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4797bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4797bO, new Object[]{uuid}));
                break;
        }
        m23501b(uuid);
    }

    private int cfQ() {
        return bFf().mo5608dq().mo3212n(fSM);
    }

    private int cfR() {
        return bFf().mo5608dq().mo3212n(fSN);
    }

    private C1556Wo cfS() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(fSO);
    }

    private C1556Wo cfT() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(fSP);
    }

    private int cfU() {
        return bFf().mo5608dq().mo3212n(fSQ);
    }

    private int cfV() {
        return bFf().mo5608dq().mo3212n(fSR);
    }

    /* renamed from: rN */
    private void m23504rN(int i) {
        bFf().mo5608dq().mo3183b(fSM, i);
    }

    /* renamed from: rO */
    private void m23505rO(int i) {
        bFf().mo5608dq().mo3183b(fSN, i);
    }

    /* renamed from: rP */
    private void m23506rP(int i) {
        bFf().mo5608dq().mo3183b(fSQ, i);
    }

    /* renamed from: rQ */
    private void m23507rQ(int i) {
        bFf().mo5608dq().mo3183b(fSR, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Base MP Kills")
    @C0064Am(aul = "cd1e70d73d9c666ecf0fe08efe178805", aum = 0)
    @C5566aOg
    /* renamed from: rT */
    private void m23509rT(int i) {
        throw new aWi(new aCE(this, fSU, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Merit Points Pda Entry")
    @C0064Am(aul = "da412b5cf6e399a58722a6e32cbf5b59", aum = 0)
    @C5566aOg
    /* renamed from: rV */
    private void m23510rV(int i) {
        throw new aWi(new aCE(this, fSW, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Initial Points")
    @C0064Am(aul = "eea285f2158caae013d5744580ca0fed", aum = 0)
    @C5566aOg
    /* renamed from: rX */
    private void m23511rX(int i) {
        throw new aWi(new aCE(this, fSY, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Challenge Rating")
    @C0064Am(aul = "a3f52cb13fc82d4bd8f773b3a1b9275b", aum = 0)
    @C5566aOg
    /* renamed from: rZ */
    private void m23512rZ(int i) {
        throw new aWi(new aCE(this, fTd, new Object[]{new Integer(i)}));
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Ratings Table")
    @C0064Am(aul = "50bc62370124c188085c36cd975d78d1", aum = 0)
    /* renamed from: x */
    private void m23513x(Map<Integer, Integer> map) {
        throw new aWi(new aCE(this, fTa, new Object[]{map}));
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Ratings Points Table")
    @C0064Am(aul = "d93cb34c72cce9c3de3276dbfd42d248", aum = 0)
    /* renamed from: z */
    private void m23514z(Map<Integer, Integer> map) {
        throw new aWi(new aCE(this, fTb, new Object[]{map}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Ratings Points Table")
    @C5566aOg
    /* renamed from: A */
    public void mo14470A(Map<Integer, Integer> map) {
        switch (bFf().mo6893i(fTb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fTb, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fTb, new Object[]{map}));
                break;
        }
        m23514z(map);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0679Jf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m23499ap();
            case 1:
                m23501b((UUID) args[0]);
                return null;
            case 2:
                return m23500ar();
            case 3:
                return new Long(m23508rR(((Integer) args[0]).intValue()));
            case 4:
                return new Integer(cfW());
            case 5:
                m23509rT(((Integer) args[0]).intValue());
                return null;
            case 6:
                return new Integer(cfY());
            case 7:
                m23510rV(((Integer) args[0]).intValue());
                return null;
            case 8:
                return new Integer(cga());
            case 9:
                m23511rX(((Integer) args[0]).intValue());
                return null;
            case 10:
                return cgc();
            case 11:
                m23513x((Map) args[0]);
                return null;
            case 12:
                return m23503lq();
            case 13:
                m23514z((Map) args[0]);
                return null;
            case 14:
                return new Integer(cge());
            case 15:
                m23512rZ(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4796bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4796bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4796bN, new Object[0]));
                break;
        }
        return m23499ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Base MP Kills")
    public int cfX() {
        switch (bFf().mo6893i(fST)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fST, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fST, new Object[0]));
                break;
        }
        return cfW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Merit Points Pda Entry")
    public int cfZ() {
        switch (bFf().mo6893i(fSV)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fSV, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fSV, new Object[0]));
                break;
        }
        return cfY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Initial Points")
    public int cgb() {
        switch (bFf().mo6893i(fSX)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fSX, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fSX, new Object[0]));
                break;
        }
        return cga();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Ratings Table")
    public Map<Integer, Integer> cgd() {
        switch (bFf().mo6893i(fSZ)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, fSZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fSZ, new Object[0]));
                break;
        }
        return cgc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Challenge Rating")
    public int cgf() {
        switch (bFf().mo6893i(fTc)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fTc, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fTc, new Object[0]));
                break;
        }
        return cge();
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4798bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4798bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4798bP, new Object[0]));
                break;
        }
        return m23500ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Ratings Points Table")
    /* renamed from: lr */
    public Map<Integer, Integer> mo14476lr() {
        switch (bFf().mo6893i(f4793Cp)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, f4793Cp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4793Cp, new Object[0]));
                break;
        }
        return m23503lq();
    }

    /* renamed from: rS */
    public long mo14477rS(int i) {
        switch (bFf().mo6893i(fSS)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, fSS, new Object[]{new Integer(i)}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, fSS, new Object[]{new Integer(i)}));
                break;
        }
        return m23508rR(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Base MP Kills")
    @C5566aOg
    /* renamed from: rU */
    public void mo14478rU(int i) {
        switch (bFf().mo6893i(fSU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSU, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSU, new Object[]{new Integer(i)}));
                break;
        }
        m23509rT(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Merit Points Pda Entry")
    @C5566aOg
    /* renamed from: rW */
    public void mo14479rW(int i) {
        switch (bFf().mo6893i(fSW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSW, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSW, new Object[]{new Integer(i)}));
                break;
        }
        m23510rV(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Initial Points")
    @C5566aOg
    /* renamed from: rY */
    public void mo14480rY(int i) {
        switch (bFf().mo6893i(fSY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSY, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSY, new Object[]{new Integer(i)}));
                break;
        }
        m23511rX(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Challenge Rating")
    @C5566aOg
    /* renamed from: sa */
    public void mo14481sa(int i) {
        switch (bFf().mo6893i(fTd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fTd, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fTd, new Object[]{new Integer(i)}));
                break;
        }
        m23512rZ(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Ratings Table")
    @C5566aOg
    /* renamed from: y */
    public void mo14482y(Map<Integer, Integer> map) {
        switch (bFf().mo6893i(fTa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fTa, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fTa, new Object[]{map}));
                break;
        }
        m23513x(map);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "33c94a4555c4017261951d76699d8a03", aum = 0)
    /* renamed from: ap */
    private UUID m23499ap() {
        return m23498an();
    }

    @C0064Am(aul = "c618c37474cef9fd01cc9f752b12912a", aum = 0)
    /* renamed from: b */
    private void m23501b(UUID uuid) {
        m23497a(uuid);
    }

    @C0064Am(aul = "97595a6113fadcebdb162acc9164d7ac", aum = 0)
    /* renamed from: ar */
    private String m23500ar() {
        return "progression_defaults";
    }

    @C0064Am(aul = "07bc5434b170e48b5f07a2368133b6af", aum = 0)
    /* renamed from: rR */
    private long m23508rR(int i) {
        int i2 = i + 1;
        if (cgd().containsKey(Integer.valueOf(i2))) {
            return (long) cgd().get(Integer.valueOf(i2)).intValue();
        }
        return 0;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Base MP Kills")
    @C0064Am(aul = "c11eea73209ce6acad693bff7b88240c", aum = 0)
    private int cfW() {
        return cfV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Merit Points Pda Entry")
    @C0064Am(aul = "b39b0b6eb70320c183f37f0b0a295523", aum = 0)
    private int cfY() {
        return cfU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Initial Points")
    @C0064Am(aul = "6951316e48c3f19f1d3a2ba9c6cd67ae", aum = 0)
    private int cga() {
        return cfQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Ratings Table")
    @C0064Am(aul = "3aeb568d6b15e4ea7b35ceff46c71eee", aum = 0)
    private Map<Integer, Integer> cgc() {
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(cfS());
        return treeMap;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Ratings Points Table")
    @C0064Am(aul = "c9e01b8612f5229181646bd9e1ce1fce", aum = 0)
    /* renamed from: lq */
    private Map<Integer, Integer> m23503lq() {
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(cfT());
        return Collections.unmodifiableMap(treeMap);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Challenge Rating")
    @C0064Am(aul = "0b4e218265591082d05d514bbeaed022", aum = 0)
    private int cge() {
        return cfR();
    }
}
