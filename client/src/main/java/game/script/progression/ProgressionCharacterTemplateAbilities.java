package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5227aBf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.oh */
/* compiled from: a */
public class ProgressionCharacterTemplateAbilities extends TaikodomObject implements C1616Xf {

    /* renamed from: BP */
    public static final C5663aRz f8779BP = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMR = null;
    public static final C2491fm akU = null;
    public static final C5663aRz akv = null;
    public static final C2491fm asA = null;
    public static final C2491fm asy = null;
    public static final C2491fm asz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fd56c5c3e1a19eb009abc09a57234029", aum = 1)
    private static Integer aMQ;
    @C0064Am(aul = "5f329e2889557b109b8c2cee7058a725", aum = 0)

    /* renamed from: ji */
    private static C3438ra<ProgressionAbility> f8780ji;

    static {
        m36768V();
    }

    public ProgressionCharacterTemplateAbilities() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionCharacterTemplateAbilities(C5540aNg ang) {
        super(ang);
    }

    public ProgressionCharacterTemplateAbilities(Integer num) {
        super((C5540aNg) null);
        super._m_script_init(num);
    }

    /* renamed from: V */
    static void m36768V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ProgressionCharacterTemplateAbilities.class, "5f329e2889557b109b8c2cee7058a725", i);
        akv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionCharacterTemplateAbilities.class, "fd56c5c3e1a19eb009abc09a57234029", i2);
        f8779BP = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(ProgressionCharacterTemplateAbilities.class, "918749382c5748ce9bcbe18ac878c18f", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionCharacterTemplateAbilities.class, "67fbd53e1d3ee78718a23330be622fb9", i5);
        asy = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionCharacterTemplateAbilities.class, "40eb1e7bad601f5c1f4b858296da57f1", i6);
        asz = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionCharacterTemplateAbilities.class, "b4518044caba9d6fc03bda34289d6e05", i7);
        akU = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionCharacterTemplateAbilities.class, "469bf84bf90c0fc5987c9c9cb4b48ade", i8);
        asA = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionCharacterTemplateAbilities.class, "6606529f8b8235ecdb656f1cfc4a72c2", i9);
        aMR = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionCharacterTemplateAbilities.class, C5227aBf.class, _m_fields, _m_methods);
    }

    /* renamed from: Fv */
    private C3438ra m36763Fv() {
        return (C3438ra) bFf().mo5608dq().mo3214p(akv);
    }

    /* renamed from: So */
    private Integer m36766So() {
        return (Integer) bFf().mo5608dq().mo3214p(f8779BP);
    }

    /* renamed from: a */
    private void m36769a(Integer num) {
        bFf().mo5608dq().mo3197f(f8779BP, num);
    }

    /* renamed from: v */
    private void m36773v(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(akv, raVar);
    }

    /* renamed from: Gc */
    public C3438ra<ProgressionAbility> mo21021Gc() {
        switch (bFf().mo6893i(akU)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, akU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, akU, new Object[0]));
                break;
        }
        return m36764Gb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Abilities")
    /* renamed from: Jt */
    public List<ProgressionAbility> mo21022Jt() {
        switch (bFf().mo6893i(asA)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, asA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, asA, new Object[0]));
                break;
        }
        return m36765Js();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: Sq */
    public Integer mo21023Sq() {
        switch (bFf().mo6893i(aMR)) {
            case 0:
                return null;
            case 2:
                return (Integer) bFf().mo5606d(new aCE(this, aMR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMR, new Object[0]));
                break;
        }
        return m36767Sp();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5227aBf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m36770au();
            case 1:
                m36771e((ProgressionAbility) args[0]);
                return null;
            case 2:
                m36772g((ProgressionAbility) args[0]);
                return null;
            case 3:
                return m36764Gb();
            case 4:
                return m36765Js();
            case 5:
                return m36767Sp();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Abilities")
    /* renamed from: f */
    public void mo21025f(ProgressionAbility lk) {
        switch (bFf().mo6893i(asy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, asy, new Object[]{lk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, asy, new Object[]{lk}));
                break;
        }
        m36771e(lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Abilities")
    /* renamed from: h */
    public void mo21026h(ProgressionAbility lk) {
        switch (bFf().mo6893i(asz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, asz, new Object[]{lk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, asz, new Object[]{lk}));
                break;
        }
        m36772g(lk);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m36770au();
    }

    /* renamed from: b */
    public void mo21024b(Integer num) {
        super.mo10S();
        m36769a(num);
    }

    @C0064Am(aul = "918749382c5748ce9bcbe18ac878c18f", aum = 0)
    /* renamed from: au */
    private String m36770au() {
        return "CR: " + String.format("%03d", new Object[]{m36766So()}) + " - Abilities: " + m36763Fv().size();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Abilities")
    @C0064Am(aul = "67fbd53e1d3ee78718a23330be622fb9", aum = 0)
    /* renamed from: e */
    private void m36771e(ProgressionAbility lk) {
        m36763Fv().add(lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Abilities")
    @C0064Am(aul = "40eb1e7bad601f5c1f4b858296da57f1", aum = 0)
    /* renamed from: g */
    private void m36772g(ProgressionAbility lk) {
        m36763Fv().remove(lk);
    }

    @C0064Am(aul = "b4518044caba9d6fc03bda34289d6e05", aum = 0)
    /* renamed from: Gb */
    private C3438ra<ProgressionAbility> m36764Gb() {
        return m36763Fv();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Abilities")
    @C0064Am(aul = "469bf84bf90c0fc5987c9c9cb4b48ade", aum = 0)
    /* renamed from: Js */
    private List<ProgressionAbility> m36765Js() {
        return Collections.unmodifiableList(m36763Fv());
    }

    @C0064Am(aul = "6606529f8b8235ecdb656f1cfc4a72c2", aum = 0)
    /* renamed from: Sp */
    private Integer m36767Sp() {
        return m36766So();
    }
}
