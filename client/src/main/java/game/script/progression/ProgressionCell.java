package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C1767aB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = BaseProgressionCell.class)
@C5511aMd
@C6485anp
/* renamed from: a.jy */
/* compiled from: a */
public class ProgressionCell extends TaikodomObject implements C1616Xf {

    /* renamed from: BX */
    public static final C5663aRz f8347BX = null;

    /* renamed from: Mt */
    public static final C2491fm f8348Mt = null;

    /* renamed from: NY */
    public static final C5663aRz f8349NY = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C5663aRz _f_enabled = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_isEnabled_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_push_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz akA = null;
    public static final C5663aRz akB = null;
    public static final C5663aRz akC = null;
    public static final C5663aRz akD = null;
    public static final C2491fm akE = null;
    public static final C2491fm akF = null;
    public static final C2491fm akG = null;
    public static final C2491fm akH = null;
    public static final C2491fm akI = null;
    public static final C2491fm akJ = null;
    public static final C2491fm akK = null;
    public static final C2491fm akL = null;
    public static final C2491fm akM = null;
    public static final C2491fm akN = null;
    public static final C2491fm akO = null;
    public static final C2491fm akP = null;
    public static final C2491fm akQ = null;
    public static final C2491fm akR = null;
    public static final C2491fm akS = null;
    public static final C2491fm akT = null;
    public static final C2491fm akU = null;
    public static final C5663aRz akv = null;
    public static final C5663aRz akw = null;
    public static final C5663aRz akx = null;
    public static final C5663aRz aky = null;
    public static final C5663aRz akz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e237e1d9ab76f5c27edc5868a572ef4b", aum = 9)
    private static boolean enabled;
    @C0064Am(aul = "66657be20507af492985aea572471f48", aum = 0)

    /* renamed from: ji */
    private static C3438ra<ProgressionAbility> f8350ji;
    @C0064Am(aul = "b8af008df6e2b67dc5f188dc25189783", aum = 1)

    /* renamed from: jj */
    private static ProgressionCellType f8351jj;
    @C0064Am(aul = "5e0aeef701a3deee9503d3c6c4b9c706", aum = 2)

    /* renamed from: jk */
    private static int f8352jk;
    @C0064Am(aul = "45f808e7a7bb47aa564619cab99198ed", aum = 3)

    /* renamed from: jl */
    private static int f8353jl;
    @C0064Am(aul = "437525d32be43c3dba383723af2a2f2d", aum = 4)

    /* renamed from: jm */
    private static ProgressionAbility f8354jm;
    @C0064Am(aul = "0aa8ba4aeadeb76394dde6c6963151ef", aum = 5)

    /* renamed from: jn */
    private static Asset f8355jn;
    @C0064Am(aul = "25767d3d05d43210941f6852105fe5c8", aum = 6)

    /* renamed from: jo */
    private static ProgressionCell f8356jo;
    @C0064Am(aul = "3598deecefec2815773d6f89700088cf", aum = 7)

    /* renamed from: jp */
    private static CharacterProgression f8357jp;
    @C0064Am(aul = "8461580b6a7aa21381231fd1f6307f2e", aum = 8)

    /* renamed from: jq */
    private static boolean f8358jq;
    @C0064Am(aul = "a5731e5c694dffa19cb6c2a61918157a", aum = 10)

    /* renamed from: jr */
    private static boolean f8359jr;
    @C0064Am(aul = "509e9ba42cb1ddace8bc6d3a6aaf6dd9", aum = 11)

    /* renamed from: js */
    private static boolean f8360js;

    static {
        m34246V();
    }

    public ProgressionCell() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionCell(C5540aNg ang) {
        super(ang);
    }

    public ProgressionCell(ProgressionCellType kgVar) {
        super((C5540aNg) null);
        super._m_script_init(kgVar);
    }

    /* renamed from: V */
    static void m34246V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 12;
        _m_methodCount = TaikodomObject._m_methodCount + 23;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(ProgressionCell.class, "66657be20507af492985aea572471f48", i);
        akv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionCell.class, "b8af008df6e2b67dc5f188dc25189783", i2);
        akw = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionCell.class, "5e0aeef701a3deee9503d3c6c4b9c706", i3);
        akx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionCell.class, "45f808e7a7bb47aa564619cab99198ed", i4);
        aky = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProgressionCell.class, "437525d32be43c3dba383723af2a2f2d", i5);
        akz = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ProgressionCell.class, "0aa8ba4aeadeb76394dde6c6963151ef", i6);
        f8349NY = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ProgressionCell.class, "25767d3d05d43210941f6852105fe5c8", i7);
        akA = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ProgressionCell.class, "3598deecefec2815773d6f89700088cf", i8);
        f8347BX = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ProgressionCell.class, "8461580b6a7aa21381231fd1f6307f2e", i9);
        akB = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(ProgressionCell.class, "e237e1d9ab76f5c27edc5868a572ef4b", i10);
        _f_enabled = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(ProgressionCell.class, "a5731e5c694dffa19cb6c2a61918157a", i11);
        akC = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(ProgressionCell.class, "509e9ba42cb1ddace8bc6d3a6aaf6dd9", i12);
        akD = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i14 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 23)];
        C2491fm a = C4105zY.m41624a(ProgressionCell.class, "91be9e8b62a18160a6a3e757e425b2aa", i14);
        akE = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionCell.class, "028a9b824b33fa2641a31f7f6bac1fe2", i15);
        akF = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionCell.class, "1a7b35e192b2b28e14f855da7fcd28be", i16);
        akG = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionCell.class, "9279a06ddae9e0bd5dfda738809d776c", i17);
        akH = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionCell.class, "6c02b60428df88765f5b8575336976f4", i18);
        akI = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionCell.class, "98fac5bcfe982ecdb7f195d55b8da13e", i19);
        akJ = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionCell.class, "a7255908459ea5808180ce0d8042a67b", i20);
        _f_isEnabled_0020_0028_0029Z = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionCell.class, "0e96bfffe70c6bc2587a8c149add7451", i21);
        f8348Mt = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionCell.class, "602589a58e8e853101e497ca7bcf4094", i22);
        akK = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionCell.class, "d826b3046c43fadb3fb57d43b8710b30", i23);
        akL = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(ProgressionCell.class, "3be4dd5af18517980b4345ff5ae33ae2", i24);
        akM = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(ProgressionCell.class, "1e75a8a0091a8f55dc598d24be3a1512", i25);
        akN = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(ProgressionCell.class, "29b1cd2cedf2a16b247e32d74aa15059", i26);
        akO = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(ProgressionCell.class, "952108e236fa62cca354e1996d8c3fb5", i27);
        akP = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(ProgressionCell.class, "4fc84617d232ddecaa9c80a19237c0a2", i28);
        akQ = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(ProgressionCell.class, "7d55a18afe96d3730d8c33add75ba5fa", i29);
        akR = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(ProgressionCell.class, "c768fdc539480d7b946c85d4d88c6371", i30);
        akS = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(ProgressionCell.class, "bd0f6160748ae79fe9ebfea4c334dc0a", i31);
        akT = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(ProgressionCell.class, "75fe728ef24987b1820ab5c8745dcce6", i32);
        _f_dispose_0020_0028_0029V = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(ProgressionCell.class, "26fa753122e1263945cb631e1cc8965c", i33);
        _f_onResurrect_0020_0028_0029V = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(ProgressionCell.class, "918a8f9e163f839e54709395a9e15f19", i34);
        akU = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(ProgressionCell.class, "26fa5cee115110d721029ec87f890d59", i35);
        _f_push_0020_0028_0029V = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(ProgressionCell.class, "b6579365f17c532b1206c4575233cac2", i36);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionCell.class, C1767aB.class, _m_fields, _m_methods);
    }

    /* renamed from: FA */
    private ProgressionCell m34218FA() {
        return (ProgressionCell) bFf().mo5608dq().mo3214p(akA);
    }

    /* renamed from: FB */
    private boolean m34219FB() {
        return bFf().mo5608dq().mo3201h(akB);
    }

    /* renamed from: FC */
    private boolean m34220FC() {
        return bFf().mo5608dq().mo3201h(akC);
    }

    /* renamed from: FD */
    private boolean m34221FD() {
        return bFf().mo5608dq().mo3201h(akD);
    }

    @C0064Am(aul = "91be9e8b62a18160a6a3e757e425b2aa", aum = 0)
    @C5566aOg
    /* renamed from: FE */
    private void m34222FE() {
        throw new aWi(new aCE(this, akE, new Object[0]));
    }

    /* renamed from: FH */
    private void m34224FH() {
        switch (bFf().mo6893i(akF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akF, new Object[0]));
                break;
        }
        m34223FG();
    }

    @C0064Am(aul = "1a7b35e192b2b28e14f855da7fcd28be", aum = 0)
    @C5566aOg
    /* renamed from: FI */
    private void m34225FI() {
        throw new aWi(new aCE(this, akG, new Object[0]));
    }

    /* renamed from: FL */
    private void m34227FL() {
        switch (bFf().mo6893i(akH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akH, new Object[0]));
                break;
        }
        m34226FK();
    }

    @C0064Am(aul = "7d55a18afe96d3730d8c33add75ba5fa", aum = 0)
    @C5566aOg
    /* renamed from: FW */
    private void m34233FW() {
        throw new aWi(new aCE(this, akR, new Object[0]));
    }

    /* renamed from: Fv */
    private C3438ra m34236Fv() {
        return ((ProgressionCellType) getType()).mo20103Gc();
    }

    /* renamed from: Fw */
    private ProgressionCellType m34237Fw() {
        return ((ProgressionCellType) getType()).mo7630Jv();
    }

    /* renamed from: Fx */
    private int m34238Fx() {
        return ((ProgressionCellType) getType()).mo20107Jx();
    }

    /* renamed from: Fy */
    private int m34239Fy() {
        return ((ProgressionCellType) getType()).mo20108Jz();
    }

    /* renamed from: Fz */
    private ProgressionAbility m34240Fz() {
        return ((ProgressionCellType) getType()).mo20104JB();
    }

    /* renamed from: Ga */
    private boolean m34241Ga() {
        switch (bFf().mo6893i(akT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, akT, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, akT, new Object[0]));
                break;
        }
        return m34235FZ();
    }

    /* renamed from: J */
    private void m34244J(boolean z) {
        bFf().mo5608dq().mo3153a(_f_enabled, z);
    }

    /* renamed from: a */
    private void m34247a(ProgressionAbility lk) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m34248a(ProgressionCell jyVar) {
        bFf().mo5608dq().mo3197f(akA, jyVar);
    }

    /* renamed from: a */
    private void m34249a(CharacterProgression nKVar) {
        bFf().mo5608dq().mo3197f(f8347BX, nKVar);
    }

    /* renamed from: ab */
    private void m34251ab(boolean z) {
        bFf().mo5608dq().mo3153a(akB, z);
    }

    /* renamed from: ac */
    private void m34252ac(boolean z) {
        bFf().mo5608dq().mo3153a(akC, z);
    }

    /* renamed from: ad */
    private void m34253ad(boolean z) {
        bFf().mo5608dq().mo3153a(akD, z);
    }

    /* renamed from: cf */
    private void m34258cf(int i) {
        throw new C6039afL();
    }

    /* renamed from: cg */
    private void m34259cg(int i) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m34260f(ProgressionCellType kgVar) {
        throw new C6039afL();
    }

    /* renamed from: li */
    private CharacterProgression m34262li() {
        return (CharacterProgression) bFf().mo5608dq().mo3214p(f8347BX);
    }

    @C0064Am(aul = "b6579365f17c532b1206c4575233cac2", aum = 0)
    /* renamed from: qW */
    private Object m34263qW() {
        return mo20004FP();
    }

    /* renamed from: rm */
    private boolean m34264rm() {
        return bFf().mo5608dq().mo3201h(_f_enabled);
    }

    /* renamed from: sG */
    private Asset m34266sG() {
        return ((ProgressionCellType) getType()).mo20115sK();
    }

    /* renamed from: t */
    private void m34267t(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: v */
    private void m34268v(C3438ra raVar) {
        throw new C6039afL();
    }

    @C5566aOg
    /* renamed from: FF */
    public void mo20001FF() {
        switch (bFf().mo6893i(akE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akE, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akE, new Object[0]));
                break;
        }
        m34222FE();
    }

    @C5566aOg
    /* renamed from: FJ */
    public void mo20002FJ() {
        switch (bFf().mo6893i(akG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akG, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akG, new Object[0]));
                break;
        }
        m34225FI();
    }

    /* renamed from: FN */
    public boolean mo20003FN() {
        switch (bFf().mo6893i(akI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, akI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, akI, new Object[0]));
                break;
        }
        return m34228FM();
    }

    /* renamed from: FP */
    public ProgressionCellType mo20004FP() {
        switch (bFf().mo6893i(akK)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCellType) bFf().mo5606d(new aCE(this, akK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, akK, new Object[0]));
                break;
        }
        return m34229FO();
    }

    /* renamed from: FR */
    public boolean mo20005FR() {
        switch (bFf().mo6893i(akL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, akL, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, akL, new Object[0]));
                break;
        }
        return m34230FQ();
    }

    /* renamed from: FT */
    public boolean mo20006FT() {
        switch (bFf().mo6893i(akN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, akN, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, akN, new Object[0]));
                break;
        }
        return m34231FS();
    }

    /* renamed from: FV */
    public ProgressionCell mo20007FV() {
        switch (bFf().mo6893i(akQ)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCell) bFf().mo5606d(new aCE(this, akQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, akQ, new Object[0]));
                break;
        }
        return m34232FU();
    }

    /* renamed from: FY */
    public boolean mo20008FY() {
        switch (bFf().mo6893i(akS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, akS, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, akS, new Object[0]));
                break;
        }
        return m34234FX();
    }

    /* renamed from: Gc */
    public C3438ra<ProgressionAbility> mo20009Gc() {
        switch (bFf().mo6893i(akU)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, akU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, akU, new Object[0]));
                break;
        }
        return m34242Gb();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1767aB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m34222FE();
                return null;
            case 1:
                m34223FG();
                return null;
            case 2:
                m34225FI();
                return null;
            case 3:
                m34226FK();
                return null;
            case 4:
                return new Boolean(m34228FM());
            case 5:
                m34254ae(((Boolean) args[0]).booleanValue());
                return null;
            case 6:
                return new Boolean(m34265rx());
            case 7:
                m34245M(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                return m34229FO();
            case 9:
                return new Boolean(m34230FQ());
            case 10:
                m34255ag(((Boolean) args[0]).booleanValue());
                return null;
            case 11:
                return new Boolean(m34231FS());
            case 12:
                m34256ai(((Boolean) args[0]).booleanValue());
                return null;
            case 13:
                m34257c((CharacterProgression) args[0]);
                return null;
            case 14:
                return m34232FU();
            case 15:
                m34233FW();
                return null;
            case 16:
                return new Boolean(m34234FX());
            case 17:
                return new Boolean(m34235FZ());
            case 18:
                m34261fg();
                return null;
            case 19:
                m34250aG();
                return null;
            case 20:
                return m34242Gb();
            case 21:
                m34243Gd();
                return null;
            case 22:
                return m34263qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m34250aG();
    }

    /* renamed from: af */
    public void mo20010af(boolean z) {
        switch (bFf().mo6893i(akJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akJ, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akJ, new Object[]{new Boolean(z)}));
                break;
        }
        m34254ae(z);
    }

    /* renamed from: ah */
    public void mo20011ah(boolean z) {
        switch (bFf().mo6893i(akM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akM, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akM, new Object[]{new Boolean(z)}));
                break;
        }
        m34255ag(z);
    }

    /* renamed from: aj */
    public void mo20012aj(boolean z) {
        switch (bFf().mo6893i(akO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akO, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akO, new Object[]{new Boolean(z)}));
                break;
        }
        m34256ai(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo20013d(CharacterProgression nKVar) {
        switch (bFf().mo6893i(akP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akP, new Object[]{nKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akP, new Object[]{nKVar}));
                break;
        }
        m34257c(nKVar);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m34261fg();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m34263qW();
    }

    @C5566aOg
    public void install() {
        switch (bFf().mo6893i(akR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, akR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, akR, new Object[0]));
                break;
        }
        m34233FW();
    }

    public boolean isEnabled() {
        switch (bFf().mo6893i(_f_isEnabled_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m34265rx();
    }

    public void setEnabled(boolean z) {
        switch (bFf().mo6893i(f8348Mt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8348Mt, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8348Mt, new Object[]{new Boolean(z)}));
                break;
        }
        m34245M(z);
    }

    public void push() {
        switch (bFf().mo6893i(_f_push_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                break;
        }
        m34243Gd();
    }

    /* renamed from: g */
    public void mo20014g(ProgressionCellType kgVar) {
        super.mo967a((C2961mJ) kgVar);
    }

    @C0064Am(aul = "028a9b824b33fa2641a31f7f6bac1fe2", aum = 0)
    /* renamed from: FG */
    private void m34223FG() {
        if (m34237Fw() != null) {
            m34227FL();
            m34218FA().mo20012aj(true);
        }
    }

    @C0064Am(aul = "9279a06ddae9e0bd5dfda738809d776c", aum = 0)
    /* renamed from: FK */
    private void m34226FK() {
        if (m34237Fw() != null && m34218FA() == null) {
            m34248a(m34237Fw().mo20105JD());
            m34218FA().mo20013d(m34262li());
            m34218FA().push();
        }
    }

    @C0064Am(aul = "6c02b60428df88765f5b8575336976f4", aum = 0)
    /* renamed from: FM */
    private boolean m34228FM() {
        return m34219FB();
    }

    @C0064Am(aul = "98fac5bcfe982ecdb7f195d55b8da13e", aum = 0)
    /* renamed from: ae */
    private void m34254ae(boolean z) {
        m34251ab(z);
    }

    @C0064Am(aul = "a7255908459ea5808180ce0d8042a67b", aum = 0)
    /* renamed from: rx */
    private boolean m34265rx() {
        return m34264rm();
    }

    @C0064Am(aul = "0e96bfffe70c6bc2587a8c149add7451", aum = 0)
    /* renamed from: M */
    private void m34245M(boolean z) {
        m34244J(z);
    }

    @C0064Am(aul = "602589a58e8e853101e497ca7bcf4094", aum = 0)
    /* renamed from: FO */
    private ProgressionCellType m34229FO() {
        return (ProgressionCellType) super.getType();
    }

    @C0064Am(aul = "d826b3046c43fadb3fb57d43b8710b30", aum = 0)
    /* renamed from: FQ */
    private boolean m34230FQ() {
        return m34220FC();
    }

    @C0064Am(aul = "3be4dd5af18517980b4345ff5ae33ae2", aum = 0)
    /* renamed from: ag */
    private void m34255ag(boolean z) {
        m34252ac(z);
    }

    @C0064Am(aul = "1e75a8a0091a8f55dc598d24be3a1512", aum = 0)
    /* renamed from: FS */
    private boolean m34231FS() {
        return m34221FD();
    }

    @C0064Am(aul = "29b1cd2cedf2a16b247e32d74aa15059", aum = 0)
    /* renamed from: ai */
    private void m34256ai(boolean z) {
        m34253ad(z);
    }

    @C0064Am(aul = "952108e236fa62cca354e1996d8c3fb5", aum = 0)
    /* renamed from: c */
    private void m34257c(CharacterProgression nKVar) {
        m34249a(nKVar);
    }

    @C0064Am(aul = "4fc84617d232ddecaa9c80a19237c0a2", aum = 0)
    /* renamed from: FU */
    private ProgressionCell m34232FU() {
        return m34218FA();
    }

    @C0064Am(aul = "c768fdc539480d7b946c85d4d88c6371", aum = 0)
    /* renamed from: FX */
    private boolean m34234FX() {
        if ((isEnabled() || mo20006FT()) && !mo20003FN() && !mo20005FR() && m34262li().mo20722Rp().mo18222ll() >= 1 && mo20004FP().mo20107Jx() <= m34262li().mo20740lt() && ((long) mo20004FP().mo20108Jz()) <= m34262li().mo20722Rp().mo18219en() && m34241Ga()) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "bd0f6160748ae79fe9ebfea4c334dc0a", aum = 0)
    /* renamed from: FZ */
    private boolean m34235FZ() {
        if (mo20004FP().mo20104JB() == null) {
            return true;
        }
        for (ProgressionCell next : m34262li().mo20725Rv()) {
            if ((next.mo20003FN() || next.mo20005FR()) && next.mo20004FP().mo20103Gc().contains(mo20004FP().mo20104JB())) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "75fe728ef24987b1820ab5c8745dcce6", aum = 0)
    /* renamed from: fg */
    private void m34261fg() {
        if (m34218FA() != null) {
            m34218FA().dispose();
        }
        m34248a((ProgressionCell) null);
        m34249a((CharacterProgression) null);
        super.dispose();
    }

    @C0064Am(aul = "26fa753122e1263945cb631e1cc8965c", aum = 0)
    /* renamed from: aG */
    private void m34250aG() {
        m34227FL();
        super.mo70aH();
    }

    @C0064Am(aul = "918a8f9e163f839e54709395a9e15f19", aum = 0)
    /* renamed from: Gb */
    private C3438ra<ProgressionAbility> m34242Gb() {
        return m34236Fv();
    }

    @C0064Am(aul = "26fa5cee115110d721029ec87f890d59", aum = 0)
    /* renamed from: Gd */
    private void m34243Gd() {
        super.push();
        if (m34218FA() != null) {
            m34218FA().push();
        }
    }
}
