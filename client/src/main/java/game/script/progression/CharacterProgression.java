package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.TaikodomObject;
import game.script.item.*;
import game.script.space.LootItems;
import logic.baa.*;
import logic.data.mbean.aTJ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.nK */
/* compiled from: a */
public class CharacterProgression extends TaikodomObject implements C1616Xf {
    /* renamed from: BV */
    public static final C5663aRz f8700BV = null;
    /* renamed from: Cl */
    public static final C2491fm f8701Cl = null;
    /* renamed from: Co */
    public static final C2491fm f8702Co = null;
    /* renamed from: Cq */
    public static final C2491fm f8703Cq = null;
    /* renamed from: Cs */
    public static final C2491fm f8704Cs = null;
    /* renamed from: Cw */
    public static final C2491fm f8705Cw = null;
    /* renamed from: Cx */
    public static final C2491fm f8706Cx = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final int aKM = 1;
    public static final C5663aRz aKP = null;
    public static final C5663aRz aKR = null;
    public static final C5663aRz aKT = null;
    public static final C2491fm aKU = null;
    public static final C2491fm aKV = null;
    public static final C2491fm aKW = null;
    public static final C2491fm aKX = null;
    public static final C2491fm aKY = null;
    public static final C2491fm aKZ = null;
    public static final C2491fm aLa = null;
    public static final C2491fm aLb = null;
    public static final C2491fm aLc = null;
    public static final C2491fm aLd = null;
    public static final C2491fm aLe = null;
    public static final C2491fm aLf = null;
    public static final C2491fm aLg = null;
    public static final C2491fm aLh = null;
    public static final C2491fm aLi = null;
    public static final C2491fm aLj = null;
    public static final C2491fm aLk = null;
    public static final C2491fm aLl = null;
    public static final C2491fm aLm = null;
    public static final C2491fm aLn = null;
    public static final C2491fm aLo = null;
    public static final C2491fm aLp = null;
    public static final C2491fm aLq = null;
    public static final C5663aRz avF = null;
    /* renamed from: gQ */
    public static final C2491fm f8707gQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "2c62be179e7b3c4a748a72fded623fef", aum = 1)

    /* renamed from: BU */
    private static ProgressionCareer f8699BU = null;
    @C0064Am(aul = "a2597cf387a8903fb213c5aa9c53c7cf", aum = 0)
    private static Character aKN;
    @C0064Am(aul = "098a76c15e902e20bbf995c6af8378bd", aum = 2)
    private static CharacterEvolution aKO;
    @C0064Am(aul = "4770e2dcecc74c6db288321681dc24cc", aum = 3)
    private static C3438ra<ProgressionLine> aKQ;
    @C0064Am(aul = "5a1c61db4888f8027e899cd59c857f08", aum = 4)
    @C5566aOg
    private static ProgressionAmplifiers aKS;

    static {
        m36003V();
    }

    public CharacterProgression() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CharacterProgression(C5540aNg ang) {
        super(ang);
    }

    public CharacterProgression(Character acx) {
        super((C5540aNg) null);
        super._m_script_init(acx);
    }

    /* renamed from: V */
    static void m36003V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 32;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(CharacterProgression.class, "a2597cf387a8903fb213c5aa9c53c7cf", i);
        avF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CharacterProgression.class, "2c62be179e7b3c4a748a72fded623fef", i2);
        f8700BV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CharacterProgression.class, "098a76c15e902e20bbf995c6af8378bd", i3);
        aKP = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CharacterProgression.class, "4770e2dcecc74c6db288321681dc24cc", i4);
        aKR = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CharacterProgression.class, "5a1c61db4888f8027e899cd59c857f08", i5);
        aKT = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 32)];
        C2491fm a = C4105zY.m41624a(CharacterProgression.class, "a0f7ebe3bc97a0d77ab7320adf45632d", i7);
        f8706Cx = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(CharacterProgression.class, "656cbd1812351c71a6261d900e3e67c0", i8);
        f8705Cw = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(CharacterProgression.class, "2065c5d7591cbedb7faa9cd0c9f7157b", i9);
        aKU = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(CharacterProgression.class, "9b0c1617170fa19e8c541cbeac8b9f9a", i10);
        _f_onResurrect_0020_0028_0029V = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(CharacterProgression.class, "da35a8cd21cb45860a72fa13a2ff917f", i11);
        aKV = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(CharacterProgression.class, "20b96c087bc516351fd83ff3a65fbc65", i12);
        aKW = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(CharacterProgression.class, "093aa7ce7a2a3bfccd5eaa066eb17f33", i13);
        aKX = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(CharacterProgression.class, "d5c3d740e4a64dcd50c22b0e52e68e67", i14);
        aKY = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(CharacterProgression.class, "550b420b7ceaaabaa69ea7ab89a4ae21", i15);
        aKZ = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(CharacterProgression.class, "ccd6a3e05bad5fd358dc275d3e5884ae", i16);
        aLa = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(CharacterProgression.class, "9b4add1a05424d0adcf2c6a1ca82338b", i17);
        aLb = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(CharacterProgression.class, "fcbc63f9a70c02be1982b3f9a3b50633", i18);
        aLc = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(CharacterProgression.class, "5522312c06b544a0f4f9497df3a4fbd9", i19);
        aLd = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(CharacterProgression.class, "e6b144d3a9359a9c013f54a4b97de44d", i20);
        aLe = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(CharacterProgression.class, "cc691e45191e03e4bb0d3ad5bd4c3a50", i21);
        _f_dispose_0020_0028_0029V = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(CharacterProgression.class, "1eb07f26df9727b2339eeb7683940eca", i22);
        f8703Cq = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        C2491fm a17 = C4105zY.m41624a(CharacterProgression.class, "08eaffc1ee12c3770f6876dc4bb5d24f", i23);
        f8704Cs = a17;
        fmVarArr[i23] = a17;
        int i24 = i23 + 1;
        C2491fm a18 = C4105zY.m41624a(CharacterProgression.class, "3cd133008feb5d033b1eee8466f676bb", i24);
        aLf = a18;
        fmVarArr[i24] = a18;
        int i25 = i24 + 1;
        C2491fm a19 = C4105zY.m41624a(CharacterProgression.class, "3c0a246933aab01791b8e134bf850b24", i25);
        aLg = a19;
        fmVarArr[i25] = a19;
        int i26 = i25 + 1;
        C2491fm a20 = C4105zY.m41624a(CharacterProgression.class, "6aa86c8857eb2b218135ea7466a7909e", i26);
        aLh = a20;
        fmVarArr[i26] = a20;
        int i27 = i26 + 1;
        C2491fm a21 = C4105zY.m41624a(CharacterProgression.class, "87ec7c42d1427f56b8989f63af12f9d1", i27);
        aLi = a21;
        fmVarArr[i27] = a21;
        int i28 = i27 + 1;
        C2491fm a22 = C4105zY.m41624a(CharacterProgression.class, "4dfec271ef5e5e72c31af87ed9b68fd2", i28);
        aLj = a22;
        fmVarArr[i28] = a22;
        int i29 = i28 + 1;
        C2491fm a23 = C4105zY.m41624a(CharacterProgression.class, "d33e7a3a7c699d81f087dffd0b749355", i29);
        aLk = a23;
        fmVarArr[i29] = a23;
        int i30 = i29 + 1;
        C2491fm a24 = C4105zY.m41624a(CharacterProgression.class, "472525385742a3095085697559917c49", i30);
        aLl = a24;
        fmVarArr[i30] = a24;
        int i31 = i30 + 1;
        C2491fm a25 = C4105zY.m41624a(CharacterProgression.class, "679c88228902dbc63c9fc0cf2d80563e", i31);
        aLm = a25;
        fmVarArr[i31] = a25;
        int i32 = i31 + 1;
        C2491fm a26 = C4105zY.m41624a(CharacterProgression.class, "c5aeb935a1c3e4e5f42aeb96cdbf97dd", i32);
        aLn = a26;
        fmVarArr[i32] = a26;
        int i33 = i32 + 1;
        C2491fm a27 = C4105zY.m41624a(CharacterProgression.class, "2b3b5eff2c71f583d6758bbc72b72907", i33);
        f8702Co = a27;
        fmVarArr[i33] = a27;
        int i34 = i33 + 1;
        C2491fm a28 = C4105zY.m41624a(CharacterProgression.class, "608faff34d1656cc05b4658532a701a9", i34);
        f8701Cl = a28;
        fmVarArr[i34] = a28;
        int i35 = i34 + 1;
        C2491fm a29 = C4105zY.m41624a(CharacterProgression.class, "4b61f945e35797b93d1a724c406efacc", i35);
        aLo = a29;
        fmVarArr[i35] = a29;
        int i36 = i35 + 1;
        C2491fm a30 = C4105zY.m41624a(CharacterProgression.class, "b8c383382c174edcab359ad35f8b23ec", i36);
        aLp = a30;
        fmVarArr[i36] = a30;
        int i37 = i36 + 1;
        C2491fm a31 = C4105zY.m41624a(CharacterProgression.class, "22221d156018403dc6b43e205ac95a81", i37);
        aLq = a31;
        fmVarArr[i37] = a31;
        int i38 = i37 + 1;
        C2491fm a32 = C4105zY.m41624a(CharacterProgression.class, "46fca6794dd138c89182497e05c3b8c4", i38);
        f8707gQ = a32;
        fmVarArr[i38] = a32;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CharacterProgression.class, aTJ.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m35980A(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aKR, raVar);
    }

    /* renamed from: QY */
    private Character m35981QY() {
        return (Character) bFf().mo5608dq().mo3214p(avF);
    }

    /* renamed from: QZ */
    private CharacterEvolution m35982QZ() {
        return (CharacterEvolution) bFf().mo5608dq().mo3214p(aKP);
    }

    /* renamed from: RD */
    private List<AmplifierType> m35985RD() {
        switch (bFf().mo6893i(aLp)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aLp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLp, new Object[0]));
                break;
        }
        return m35984RC();
    }

    /* renamed from: Ra */
    private C3438ra m35987Ra() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aKR);
    }

    /* renamed from: Rb */
    private ProgressionAmplifiers m35988Rb() {
        return (ProgressionAmplifiers) bFf().mo5608dq().mo3214p(aKT);
    }

    @C0064Am(aul = "da35a8cd21cb45860a72fa13a2ff917f", aum = 0)
    @C5566aOg
    /* renamed from: Rc */
    private void m35989Rc() {
        throw new aWi(new aCE(this, aKV, new Object[0]));
    }

    @C5566aOg
    /* renamed from: Rd */
    private void m35990Rd() {
        switch (bFf().mo6893i(aKV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKV, new Object[0]));
                break;
        }
        m35989Rc();
    }

    @C0064Am(aul = "fcbc63f9a70c02be1982b3f9a3b50633", aum = 0)
    @C5566aOg
    /* renamed from: Re */
    private void m35991Re() {
        throw new aWi(new aCE(this, aLc, new Object[0]));
    }

    @C0064Am(aul = "5522312c06b544a0f4f9497df3a4fbd9", aum = 0)
    @C5566aOg
    /* renamed from: Rg */
    private void m35992Rg() {
        throw new aWi(new aCE(this, aLd, new Object[0]));
    }

    @C5566aOg
    /* renamed from: Rh */
    private void m35993Rh() {
        switch (bFf().mo6893i(aLd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLd, new Object[0]));
                break;
        }
        m35992Rg();
    }

    @C0064Am(aul = "87ec7c42d1427f56b8989f63af12f9d1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: Rq */
    private void m35998Rq() {
        throw new aWi(new aCE(this, aLi, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "4dfec271ef5e5e72c31af87ed9b68fd2", aum = 0)
    @C2499fr
    /* renamed from: Rs */
    private void m35999Rs() {
        throw new aWi(new aCE(this, aLj, new Object[0]));
    }

    /* renamed from: a */
    private void m36004a(ProgressionAmplifiers qd) {
        bFf().mo5608dq().mo3197f(aKT, qd);
    }

    @C0064Am(aul = "608faff34d1656cc05b4658532a701a9", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m36005a(RawMaterial abk, LootItems ez) {
        throw new aWi(new aCE(this, f8701Cl, new Object[]{abk, ez}));
    }

    @C0064Am(aul = "2065c5d7591cbedb7faa9cd0c9f7157b", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m36006a(ProgressionCharacterTemplate aof) {
        throw new aWi(new aCE(this, aKU, new Object[]{aof}));
    }

    /* renamed from: a */
    private void m36007a(CharacterEvolution eqVar) {
        bFf().mo5608dq().mo3197f(aKP, eqVar);
    }

    @C0064Am(aul = "2b3b5eff2c71f583d6758bbc72b72907", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m36008a(C4068yr yrVar) {
        throw new aWi(new aCE(this, f8702Co, new Object[]{yrVar}));
    }

    /* renamed from: b */
    private boolean m36011b(ProgressionLineType gwVar) {
        switch (bFf().mo6893i(aKW)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aKW, new Object[]{gwVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aKW, new Object[]{gwVar}));
                break;
        }
        return m36009a(gwVar);
    }

    /* renamed from: d */
    private ProgressionLine m36014d(ProgressionLineType gwVar) {
        switch (bFf().mo6893i(aKX)) {
            case 0:
                return null;
            case 2:
                return (ProgressionLine) bFf().mo5606d(new aCE(this, aKX, new Object[]{gwVar}));
            case 3:
                bFf().mo5606d(new aCE(this, aKX, new Object[]{gwVar}));
                break;
        }
        return m36012c(gwVar);
    }

    @C0064Am(aul = "46fca6794dd138c89182497e05c3b8c4", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m36015dd() {
        throw new aWi(new aCE(this, f8707gQ, new Object[0]));
    }

    /* renamed from: e */
    private void m36016e(ProgressionCareer sbVar) {
        bFf().mo5608dq().mo3197f(f8700BV, sbVar);
    }

    @C0064Am(aul = "550b420b7ceaaabaa69ea7ab89a4ae21", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: e */
    private boolean m36017e(ProgressionCell jyVar) {
        throw new aWi(new aCE(this, aKZ, new Object[]{jyVar}));
    }

    @C0064Am(aul = "656cbd1812351c71a6261d900e3e67c0", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m36018f(ProgressionCareer sbVar) {
        throw new aWi(new aCE(this, f8705Cw, new Object[]{sbVar}));
    }

    /* renamed from: g */
    private void m36020g(Character acx) {
        bFf().mo5608dq().mo3197f(avF, acx);
    }

    @C0064Am(aul = "ccd6a3e05bad5fd358dc275d3e5884ae", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m36021g(ProgressionCell jyVar) {
        throw new aWi(new aCE(this, aLa, new Object[]{jyVar}));
    }

    @C0064Am(aul = "9b4add1a05424d0adcf2c6a1ca82338b", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m36022k(ProgressionAbility lk) {
        throw new aWi(new aCE(this, aLb, new Object[]{lk}));
    }

    /* renamed from: lh */
    private ProgressionCareer m36023lh() {
        return (ProgressionCareer) bFf().mo5608dq().mo3214p(f8700BV);
    }

    /* renamed from: RB */
    public void mo20716RB() {
        switch (bFf().mo6893i(aLo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLo, new Object[0]));
                break;
        }
        m35983RA();
    }

    /* renamed from: RF */
    public void mo20717RF() {
        switch (bFf().mo6893i(aLq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLq, new Object[0]));
                break;
        }
        m35986RE();
    }

    @C5566aOg
    /* renamed from: Rf */
    public void mo20718Rf() {
        switch (bFf().mo6893i(aLc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLc, new Object[0]));
                break;
        }
        m35991Re();
    }

    /* renamed from: Rj */
    public ProgressionAmplifiers mo20719Rj() {
        switch (bFf().mo6893i(aLe)) {
            case 0:
                return null;
            case 2:
                return (ProgressionAmplifiers) bFf().mo5606d(new aCE(this, aLe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLe, new Object[0]));
                break;
        }
        return m35994Ri();
    }

    /* renamed from: Rl */
    public C3438ra<ProgressionLine> mo20720Rl() {
        switch (bFf().mo6893i(aLf)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, aLf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLf, new Object[0]));
                break;
        }
        return m35995Rk();
    }

    /* renamed from: Rn */
    public ProgressionCareer mo20721Rn() {
        switch (bFf().mo6893i(aLg)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCareer) bFf().mo5606d(new aCE(this, aLg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLg, new Object[0]));
                break;
        }
        return m35996Rm();
    }

    /* renamed from: Rp */
    public CharacterEvolution mo20722Rp() {
        switch (bFf().mo6893i(aLh)) {
            case 0:
                return null;
            case 2:
                return (CharacterEvolution) bFf().mo5606d(new aCE(this, aLh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLh, new Object[0]));
                break;
        }
        return m35997Ro();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: Rr */
    public void mo20723Rr() {
        switch (bFf().mo6893i(aLi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLi, new Object[0]));
                break;
        }
        m35998Rq();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: Rt */
    public void mo20724Rt() {
        switch (bFf().mo6893i(aLj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLj, new Object[0]));
                break;
        }
        m35999Rs();
    }

    /* renamed from: Rv */
    public List<ProgressionCell> mo20725Rv() {
        switch (bFf().mo6893i(aLk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aLk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLk, new Object[0]));
                break;
        }
        return m36000Ru();
    }

    /* renamed from: Rx */
    public List<ProgressionCell> mo20726Rx() {
        switch (bFf().mo6893i(aLl)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aLl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLl, new Object[0]));
                break;
        }
        return m36001Rw();
    }

    /* renamed from: Rz */
    public List<ProgressionAbility> mo20727Rz() {
        switch (bFf().mo6893i(aLm)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aLm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLm, new Object[0]));
                break;
        }
        return m36002Ry();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aTJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m36026lz();
            case 1:
                m36018f((ProgressionCareer) args[0]);
                return null;
            case 2:
                m36006a((ProgressionCharacterTemplate) args[0]);
                return null;
            case 3:
                m36010aG();
                return null;
            case 4:
                m35989Rc();
                return null;
            case 5:
                return new Boolean(m36009a((ProgressionLineType) args[0]));
            case 6:
                return m36012c((ProgressionLineType) args[0]);
            case 7:
                return new Boolean(m36013c((ProgressionCell) args[0]));
            case 8:
                return new Boolean(m36017e((ProgressionCell) args[0]));
            case 9:
                m36021g((ProgressionCell) args[0]);
                return null;
            case 10:
                m36022k((ProgressionAbility) args[0]);
                return null;
            case 11:
                m35991Re();
                return null;
            case 12:
                m35992Rg();
                return null;
            case 13:
                return m35994Ri();
            case 14:
                m36019fg();
                return null;
            case 15:
                return new Integer(m36024ls());
            case 16:
                return new Long(m36025lu());
            case 17:
                return m35995Rk();
            case 18:
                return m35996Rm();
            case 19:
                return m35997Ro();
            case 20:
                m35998Rq();
                return null;
            case 21:
                m35999Rs();
                return null;
            case 22:
                return m36000Ru();
            case 23:
                return m36001Rw();
            case 24:
                return m36002Ry();
            case 25:
                return new Boolean(m36027m((ProgressionAbility) args[0]));
            case 26:
                m36008a((C4068yr) args[0]);
                return null;
            case 27:
                m36005a((RawMaterial) args[0], (LootItems) args[1]);
                return null;
            case 28:
                m35983RA();
                return null;
            case 29:
                return m35984RC();
            case 30:
                m35986RE();
                return null;
            case 31:
                m36015dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m36010aG();
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20728b(RawMaterial abk, LootItems ez) {
        switch (bFf().mo6893i(f8701Cl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8701Cl, new Object[]{abk, ez}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8701Cl, new Object[]{abk, ez}));
                break;
        }
        m36005a(abk, ez);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20729b(ProgressionCharacterTemplate aof) {
        switch (bFf().mo6893i(aKU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKU, new Object[]{aof}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKU, new Object[]{aof}));
                break;
        }
        m36006a(aof);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20730b(ProgressionCell jyVar) {
        switch (bFf().mo6893i(aLa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLa, new Object[]{jyVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLa, new Object[]{jyVar}));
                break;
        }
        m36021g(jyVar);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20731b(C4068yr yrVar) {
        switch (bFf().mo6893i(f8702Co)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8702Co, new Object[]{yrVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8702Co, new Object[]{yrVar}));
                break;
        }
        m36008a(yrVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @Deprecated
    @ClientOnly
    /* renamed from: d */
    public boolean mo20732d(ProgressionCell jyVar) {
        switch (bFf().mo6893i(aKY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aKY, new Object[]{jyVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aKY, new Object[]{jyVar}));
                break;
        }
        return m36013c(jyVar);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo20733de() {
        switch (bFf().mo6893i(f8707gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8707gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8707gQ, new Object[0]));
                break;
        }
        m36015dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m36019fg();
    }

    /* renamed from: en */
    public long mo20734en() {
        switch (bFf().mo6893i(f8704Cs)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f8704Cs, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8704Cs, new Object[0]));
                break;
        }
        return m36025lu();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: f */
    public boolean mo20735f(ProgressionCell jyVar) {
        switch (bFf().mo6893i(aKZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aKZ, new Object[]{jyVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aKZ, new Object[]{jyVar}));
                break;
        }
        return m36017e(jyVar);
    }

    @C5566aOg
    /* renamed from: g */
    public void mo20736g(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(f8705Cw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8705Cw, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8705Cw, new Object[]{sbVar}));
                break;
        }
        m36018f(sbVar);
    }

    @C5566aOg
    /* renamed from: l */
    public void mo20738l(ProgressionAbility lk) {
        switch (bFf().mo6893i(aLb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLb, new Object[]{lk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLb, new Object[]{lk}));
                break;
        }
        m36022k(lk);
    }

    /* renamed from: lA */
    public Character mo20739lA() {
        switch (bFf().mo6893i(f8706Cx)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, f8706Cx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8706Cx, new Object[0]));
                break;
        }
        return m36026lz();
    }

    /* renamed from: lt */
    public int mo20740lt() {
        switch (bFf().mo6893i(f8703Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f8703Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8703Cq, new Object[0]));
                break;
        }
        return m36024ls();
    }

    /* renamed from: n */
    public boolean mo20741n(ProgressionAbility lk) {
        switch (bFf().mo6893i(aLn)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aLn, new Object[]{lk}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aLn, new Object[]{lk}));
                break;
        }
        return m36027m(lk);
    }

    /* renamed from: h */
    public void mo20737h(Character acx) {
        super.mo10S();
        m36020g(acx);
        CharacterEvolution eqVar = (CharacterEvolution) bFf().mo6865M(CharacterEvolution.class);
        eqVar.mo18215b(this);
        m36007a(eqVar);
        ProgressionAmplifiers qd = (ProgressionAmplifiers) bFf().mo6865M(ProgressionAmplifiers.class);
        qd.mo4931h(acx);
        m36004a(qd);
    }

    @C0064Am(aul = "a0f7ebe3bc97a0d77ab7320adf45632d", aum = 0)
    /* renamed from: lz */
    private Character m36026lz() {
        return m35981QY();
    }

    @C0064Am(aul = "9b0c1617170fa19e8c541cbeac8b9f9a", aum = 0)
    /* renamed from: aG */
    private void m36010aG() {
        if (m36023lh() != null) {
            m35990Rd();
        }
        super.mo70aH();
    }

    @C0064Am(aul = "20b96c087bc516351fd83ff3a65fbc65", aum = 0)
    /* renamed from: a */
    private boolean m36009a(ProgressionLineType gwVar) {
        for (ProgressionLine cZK : mo20720Rl()) {
            if (cZK.cZK().equals(gwVar)) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "093aa7ce7a2a3bfccd5eaa066eb17f33", aum = 0)
    /* renamed from: c */
    private ProgressionLine m36012c(ProgressionLineType gwVar) {
        ProgressionLine wa = gwVar.mo19156wa();
        wa.push();
        wa.mo8789d(this);
        wa.cZI();
        m35987Ra().add(wa);
        return wa;
    }

    @C0064Am(aul = "d5c3d740e4a64dcd50c22b0e52e68e67", aum = 0)
    @Deprecated
    @ClientOnly
    /* renamed from: c */
    private boolean m36013c(ProgressionCell jyVar) {
        if (m35982QZ().mo18222ll() >= 1) {
            return mo20735f(jyVar);
        }
        return false;
    }

    @C0064Am(aul = "e6b144d3a9359a9c013f54a4b97de44d", aum = 0)
    /* renamed from: Ri */
    private ProgressionAmplifiers m35994Ri() {
        return m35988Rb();
    }

    @C0064Am(aul = "cc691e45191e03e4bb0d3ad5bd4c3a50", aum = 0)
    /* renamed from: fg */
    private void m36019fg() {
        if (m35988Rb() != null) {
            m35988Rb().dispose();
        }
        m36004a((ProgressionAmplifiers) null);
        m36016e((ProgressionCareer) null);
        if (m35982QZ() != null) {
            m35982QZ().dispose();
        }
        m36007a((CharacterEvolution) null);
        m36020g((Character) null);
        for (ProgressionLine dispose : m35987Ra()) {
            dispose.dispose();
        }
        m35987Ra().clear();
        super.dispose();
    }

    @C0064Am(aul = "1eb07f26df9727b2339eeb7683940eca", aum = 0)
    /* renamed from: ls */
    private int m36024ls() {
        if (mo20722Rp() != null) {
            return mo20722Rp().mo18224lt();
        }
        return 0;
    }

    @C0064Am(aul = "08eaffc1ee12c3770f6876dc4bb5d24f", aum = 0)
    /* renamed from: lu */
    private long m36025lu() {
        if (mo20722Rp() != null) {
            return mo20722Rp().mo18219en();
        }
        return 0;
    }

    @C0064Am(aul = "3cd133008feb5d033b1eee8466f676bb", aum = 0)
    /* renamed from: Rk */
    private C3438ra<ProgressionLine> m35995Rk() {
        return m35987Ra();
    }

    @C0064Am(aul = "3c0a246933aab01791b8e134bf850b24", aum = 0)
    /* renamed from: Rm */
    private ProgressionCareer m35996Rm() {
        return m36023lh();
    }

    @C0064Am(aul = "6aa86c8857eb2b218135ea7466a7909e", aum = 0)
    /* renamed from: Ro */
    private CharacterEvolution m35997Ro() {
        return m35982QZ();
    }

    @C0064Am(aul = "d33e7a3a7c699d81f087dffd0b749355", aum = 0)
    /* renamed from: Ru */
    private List<ProgressionCell> m36000Ru() {
        ArrayList arrayList = new ArrayList();
        for (ProgressionLine cZO : mo20720Rl()) {
            arrayList.addAll(cZO.cZO());
        }
        return arrayList;
    }

    @C0064Am(aul = "472525385742a3095085697559917c49", aum = 0)
    /* renamed from: Rw */
    private List<ProgressionCell> m36001Rw() {
        ArrayList arrayList = new ArrayList();
        for (ProgressionCell next : mo20725Rv()) {
            if (next.mo20003FN()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "679c88228902dbc63c9fc0cf2d80563e", aum = 0)
    /* renamed from: Ry */
    private List<ProgressionAbility> m36002Ry() {
        ArrayList arrayList = new ArrayList();
        for (ProgressionCell FP : mo20726Rx()) {
            arrayList.addAll(FP.mo20004FP().mo20103Gc());
        }
        return arrayList;
    }

    @C0064Am(aul = "c5aeb935a1c3e4e5f42aeb96cdbf97dd", aum = 0)
    /* renamed from: m */
    private boolean m36027m(ProgressionAbility lk) {
        if (mo20727Rz().contains(lk)) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "4b61f945e35797b93d1a724c406efacc", aum = 0)
    /* renamed from: RA */
    private void m35983RA() {
        List<AmplifierType> RD = m35985RD();
        Iterator<Item> iterator = m35988Rb().getIterator();
        ArrayList<Amplifier> arrayList = new ArrayList<>();
        while (iterator.hasNext()) {
            Amplifier zXVar = (Amplifier) iterator.next();
            if (!RD.contains(zXVar.cJS())) {
                if (zXVar.mo7855al() != null && !zXVar.mo7855al().isDisposed()) {
                    zXVar.mo1953Ku();
                }
                arrayList.add(zXVar);
            }
        }
        for (Amplifier d : arrayList) {
            m35988Rb().mo2690d(d);
        }
    }

    @C0064Am(aul = "b8c383382c174edcab359ad35f8b23ec", aum = 0)
    /* renamed from: RC */
    private List<AmplifierType> m35984RC() {
        ArrayList arrayList = new ArrayList();
        for (ProgressionCell Gc : mo20726Rx()) {
            for (ProgressionAbility bgb : Gc.mo20009Gc()) {
                for (AmplifierType add : bgb.bgb()) {
                    arrayList.add(add);
                }
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "22221d156018403dc6b43e205ac95a81", aum = 0)
    /* renamed from: RE */
    private void m35986RE() {
        for (AmplifierType next : m35985RD()) {
            if (!m35988Rb().mo7616U(next)) {
                try {
                    m35988Rb().mo2695u((ItemType) next);
                } catch (C2293dh e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
