package game.script.progression;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C6412amU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aMo  reason: case insensitive filesystem */
/* compiled from: a */
public class ProgressionSuitability extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f3386MN = null;

    /* renamed from: QA */
    public static final C5663aRz f3387QA = null;

    /* renamed from: QD */
    public static final C2491fm f3388QD = null;

    /* renamed from: QE */
    public static final C2491fm f3389QE = null;

    /* renamed from: QF */
    public static final C2491fm f3390QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f3391Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f3393bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3394bM = null;
    /* renamed from: bN */
    public static final C2491fm f3395bN = null;
    /* renamed from: bO */
    public static final C2491fm f3396bO = null;
    /* renamed from: bP */
    public static final C2491fm f3397bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3398bQ = null;
    public static final C5663aRz ioP = null;
    public static final C2491fm ioQ = null;
    public static final C2491fm ioR = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f9b792add03fdca5552692572887a6d1", aum = 4)

    /* renamed from: bK */
    private static UUID f3392bK;
    @C0064Am(aul = "4e280ab58e21f613d31eff03ad981810", aum = 2)
    private static boolean gbX;
    @C0064Am(aul = "7752fb51382e64353d3f2f7aaedbb3cf", aum = 3)
    private static String handle;
    @C0064Am(aul = "3cc8684b917dc785946165761022a42d", aum = 1)

    /* renamed from: nh */
    private static I18NString f3399nh;
    @C0064Am(aul = "93f752970c2bf994518e5e9208372b7a", aum = 0)

    /* renamed from: ni */
    private static I18NString f3400ni;

    static {
        m16452V();
    }

    public ProgressionSuitability() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionSuitability(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16452V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ProgressionSuitability.class, "93f752970c2bf994518e5e9208372b7a", i);
        f3391Qz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionSuitability.class, "3cc8684b917dc785946165761022a42d", i2);
        f3387QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionSuitability.class, "4e280ab58e21f613d31eff03ad981810", i3);
        ioP = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionSuitability.class, "7752fb51382e64353d3f2f7aaedbb3cf", i4);
        f3394bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProgressionSuitability.class, "f9b792add03fdca5552692572887a6d1", i5);
        f3393bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(ProgressionSuitability.class, "1668869a4ab84e75120d3dfb29b2237a", i7);
        f3395bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionSuitability.class, "fa7559f5b3f7803f77ebe9c930e45960", i8);
        f3396bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionSuitability.class, "f2da4434cf2ab813e92a9b25c7ed6be3", i9);
        f3397bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionSuitability.class, "b4ca32e392b8a6d8709b0cd82f12f973", i10);
        f3398bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionSuitability.class, "8e483870cf4846779a9fe0a724e7d589", i11);
        f3386MN = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionSuitability.class, "2a5212182d1050f87655020b4fd31bdf", i12);
        f3388QD = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionSuitability.class, "c320bc7b5c67fffa7c366a415425134e", i13);
        f3389QE = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionSuitability.class, "b3f192c22bd15793aa77f4e34c21a414", i14);
        f3390QF = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionSuitability.class, "83eb130b726e46b826644dfcf5423dbd", i15);
        ioQ = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionSuitability.class, "dbfe0ffac60a95ce027bb9391a8ca1f8", i16);
        ioR = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionSuitability.class, C6412amU.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m16453a(String str) {
        bFf().mo5608dq().mo3197f(f3394bM, str);
    }

    /* renamed from: a */
    private void m16454a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3393bL, uuid);
    }

    /* renamed from: an */
    private UUID m16455an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3393bL);
    }

    /* renamed from: ao */
    private String m16456ao() {
        return (String) bFf().mo5608dq().mo3214p(f3394bM);
    }

    /* renamed from: bq */
    private void m16461bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3391Qz, i18NString);
    }

    /* renamed from: br */
    private void m16462br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3387QA, i18NString);
    }

    /* renamed from: c */
    private void m16465c(UUID uuid) {
        switch (bFf().mo6893i(f3396bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3396bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3396bO, new Object[]{uuid}));
                break;
        }
        m16460b(uuid);
    }

    private boolean dje() {
        return bFf().mo5608dq().mo3201h(ioP);
    }

    /* renamed from: jO */
    private void m16466jO(boolean z) {
        bFf().mo5608dq().mo3153a(ioP, z);
    }

    /* renamed from: vS */
    private I18NString m16469vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3391Qz);
    }

    /* renamed from: vT */
    private I18NString m16470vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3387QA);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6412amU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m16457ap();
            case 1:
                m16460b((UUID) args[0]);
                return null;
            case 2:
                return m16458ar();
            case 3:
                m16459b((String) args[0]);
                return null;
            case 4:
                return m16468rO();
            case 5:
                m16463bs((I18NString) args[0]);
                return null;
            case 6:
                return m16471vV();
            case 7:
                m16464bu((I18NString) args[0]);
                return null;
            case 8:
                return new Boolean(djf());
            case 9:
                m16467jP(((Boolean) args[0]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3395bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3395bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3395bN, new Object[0]));
                break;
        }
        return m16457ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    /* renamed from: bt */
    public void mo10169bt(I18NString i18NString) {
        switch (bFf().mo6893i(f3388QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3388QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3388QD, new Object[]{i18NString}));
                break;
        }
        m16463bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo10170bv(I18NString i18NString) {
        switch (bFf().mo6893i(f3390QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3390QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3390QF, new Object[]{i18NString}));
                break;
        }
        m16464bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activation")
    public boolean djg() {
        switch (bFf().mo6893i(ioQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ioQ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ioQ, new Object[0]));
                break;
        }
        return djf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3397bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3397bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3397bP, new Object[0]));
                break;
        }
        return m16458ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3398bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3398bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3398bQ, new Object[]{str}));
                break;
        }
        m16459b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activation")
    /* renamed from: jQ */
    public void mo10172jQ(boolean z) {
        switch (bFf().mo6893i(ioR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ioR, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ioR, new Object[]{new Boolean(z)}));
                break;
        }
        m16467jP(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo10173rP() {
        switch (bFf().mo6893i(f3386MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3386MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3386MN, new Object[0]));
                break;
        }
        return m16468rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo10175vW() {
        switch (bFf().mo6893i(f3389QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3389QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3389QE, new Object[0]));
                break;
        }
        return m16471vV();
    }

    @C0064Am(aul = "1668869a4ab84e75120d3dfb29b2237a", aum = 0)
    /* renamed from: ap */
    private UUID m16457ap() {
        return m16455an();
    }

    @C0064Am(aul = "fa7559f5b3f7803f77ebe9c930e45960", aum = 0)
    /* renamed from: b */
    private void m16460b(UUID uuid) {
        m16454a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m16454a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "f2da4434cf2ab813e92a9b25c7ed6be3", aum = 0)
    /* renamed from: ar */
    private String m16458ar() {
        return m16456ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "b4ca32e392b8a6d8709b0cd82f12f973", aum = 0)
    /* renamed from: b */
    private void m16459b(String str) {
        m16453a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "8e483870cf4846779a9fe0a724e7d589", aum = 0)
    /* renamed from: rO */
    private I18NString m16468rO() {
        return m16469vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "2a5212182d1050f87655020b4fd31bdf", aum = 0)
    /* renamed from: bs */
    private void m16463bs(I18NString i18NString) {
        m16461bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "c320bc7b5c67fffa7c366a415425134e", aum = 0)
    /* renamed from: vV */
    private I18NString m16471vV() {
        return m16470vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "b3f192c22bd15793aa77f4e34c21a414", aum = 0)
    /* renamed from: bu */
    private void m16464bu(I18NString i18NString) {
        m16462br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activation")
    @C0064Am(aul = "83eb130b726e46b826644dfcf5423dbd", aum = 0)
    private boolean djf() {
        return dje();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activation")
    @C0064Am(aul = "dbfe0ffac60a95ce027bb9391a8ca1f8", aum = 0)
    /* renamed from: jP */
    private void m16467jP(boolean z) {
        m16466jO(z);
    }
}
