package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.ayU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.aof  reason: case insensitive filesystem */
/* compiled from: a */
public class ProgressionCharacterTemplate extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: QA */
    public static final C5663aRz f5067QA = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJO = null;
    public static final C2491fm aJP = null;
    /* renamed from: bL */
    public static final C5663aRz f5069bL = null;
    /* renamed from: bM */
    public static final C5663aRz f5070bM = null;
    /* renamed from: bN */
    public static final C2491fm f5071bN = null;
    /* renamed from: bO */
    public static final C2491fm f5072bO = null;
    /* renamed from: bP */
    public static final C2491fm f5073bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5074bQ = null;
    public static final C5663aRz ghd = null;
    public static final C5663aRz ghf = null;
    public static final C2491fm ghg = null;
    public static final C2491fm ghh = null;
    public static final C2491fm ghi = null;
    public static final C2491fm ghj = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "cc854d97fef664c60e3af5b37b0e7810", aum = 3)

    /* renamed from: bK */
    private static UUID f5068bK;
    @C0064Am(aul = "dce0a2077770797891b60837676ca70b", aum = 1)
    private static String description;
    @C0064Am(aul = "c5f9cfa5fcb86fd3844663a85a62e32c", aum = 0)
    private static ProgressionCareer ghc;
    @C0064Am(aul = "12e709fb49a1d73df4187a28ca5ed184", aum = 2)
    private static C1556Wo<Integer, ProgressionCharacterTemplateAbilities> ghe;
    @C0064Am(aul = "ebe47c34a51ac1b1c073d3586c42c44c", aum = 4)
    private static String handle;

    static {
        m24649V();
    }

    public ProgressionCharacterTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionCharacterTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24649V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ProgressionCharacterTemplate.class, "c5f9cfa5fcb86fd3844663a85a62e32c", i);
        ghd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionCharacterTemplate.class, "dce0a2077770797891b60837676ca70b", i2);
        f5067QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionCharacterTemplate.class, "12e709fb49a1d73df4187a28ca5ed184", i3);
        ghf = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionCharacterTemplate.class, "cc854d97fef664c60e3af5b37b0e7810", i4);
        f5069bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProgressionCharacterTemplate.class, "ebe47c34a51ac1b1c073d3586c42c44c", i5);
        f5070bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(ProgressionCharacterTemplate.class, "56d23302be5c5c25d89f5c78cd8af3a9", i7);
        f5071bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "e2bb34c82e5868909dc3e5f0e0c84873", i8);
        f5072bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "f49f10e15cb65f198900074a669cc340", i9);
        f5073bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "496174539c357d24f93243bc8058802c", i10);
        f5074bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "2f2ef3706522396469196fa4b96e2f8c", i11);
        _f_onResurrect_0020_0028_0029V = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "afe6208957a0b9289d690774e5cffc2c", i12);
        ghg = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "3bda587f88f05e536b4098a4626196fb", i13);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "0f791a21fba27ac1ffa7f2dc48871d44", i14);
        ghh = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "d3795b1a781af2322dcc850185cd6704", i15);
        aJP = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "ae4e322b56db2e6b972e78228b983a66", i16);
        aJO = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "eef4ee88e188073e7c5190a613e5c6b9", i17);
        ghi = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(ProgressionCharacterTemplate.class, "dc9e5e079884c264b27ad7584f159ff0", i18);
        ghj = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionCharacterTemplate.class, ayU.class, _m_fields, _m_methods);
    }

    /* renamed from: Qj */
    private String m24648Qj() {
        return (String) bFf().mo5608dq().mo3214p(f5067QA);
    }

    /* renamed from: X */
    private void m24650X(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(ghf, wo);
    }

    /* renamed from: a */
    private void m24651a(String str) {
        bFf().mo5608dq().mo3197f(f5070bM, str);
    }

    /* renamed from: a */
    private void m24652a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5069bL, uuid);
    }

    /* renamed from: aM */
    private void m24654aM(String str) {
        bFf().mo5608dq().mo3197f(f5067QA, str);
    }

    /* renamed from: an */
    private UUID m24656an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5069bL);
    }

    /* renamed from: ao */
    private String m24657ao() {
        return (String) bFf().mo5608dq().mo3214p(f5070bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "496174539c357d24f93243bc8058802c", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m24661b(String str) {
        throw new aWi(new aCE(this, f5074bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m24663c(UUID uuid) {
        switch (bFf().mo6893i(f5072bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5072bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5072bO, new Object[]{uuid}));
                break;
        }
        m24662b(uuid);
    }

    private ProgressionCareer cna() {
        return (ProgressionCareer) bFf().mo5608dq().mo3214p(ghd);
    }

    private C1556Wo cnb() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(ghf);
    }

    private void cnd() {
        switch (bFf().mo6893i(ghg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ghg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ghg, new Object[0]));
                break;
        }
        cnc();
    }

    /* renamed from: p */
    private void m24664p(ProgressionCareer sbVar) {
        bFf().mo5608dq().mo3197f(ghd, sbVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new ayU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m24658ap();
            case 1:
                m24662b((UUID) args[0]);
                return null;
            case 2:
                return m24659ar();
            case 3:
                m24661b((String) args[0]);
                return null;
            case 4:
                m24653aG();
                return null;
            case 5:
                cnc();
                return null;
            case 6:
                return m24660au();
            case 7:
                return cne();
            case 8:
                return m24647QE();
            case 9:
                m24655aQ((String) args[0]);
                return null;
            case 10:
                return cng();
            case 11:
                m24665q((ProgressionCareer) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m24653aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5071bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5071bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5071bN, new Object[0]));
                break;
        }
        return m24658ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Challenge Rating Abilities")
    public List<ProgressionCharacterTemplateAbilities> cnf() {
        switch (bFf().mo6893i(ghh)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, ghh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ghh, new Object[0]));
                break;
        }
        return cne();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Career")
    public ProgressionCareer cnh() {
        switch (bFf().mo6893i(ghi)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCareer) bFf().mo5606d(new aCE(this, ghi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ghi, new Object[0]));
                break;
        }
        return cng();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    public String getDescription() {
        switch (bFf().mo6893i(aJP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aJP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJP, new Object[0]));
                break;
        }
        return m24647QE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    public void setDescription(String str) {
        switch (bFf().mo6893i(aJO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJO, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJO, new Object[]{str}));
                break;
        }
        m24655aQ(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5073bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5073bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5073bP, new Object[0]));
                break;
        }
        return m24659ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5074bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5074bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5074bQ, new Object[]{str}));
                break;
        }
        m24661b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Career")
    /* renamed from: r */
    public void mo15297r(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(ghj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ghj, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ghj, new Object[]{sbVar}));
                break;
        }
        m24665q(sbVar);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m24660au();
    }

    @C0064Am(aul = "56d23302be5c5c25d89f5c78cd8af3a9", aum = 0)
    /* renamed from: ap */
    private UUID m24658ap() {
        return m24656an();
    }

    @C0064Am(aul = "e2bb34c82e5868909dc3e5f0e0c84873", aum = 0)
    /* renamed from: b */
    private void m24662b(UUID uuid) {
        m24652a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "f49f10e15cb65f198900074a669cc340", aum = 0)
    /* renamed from: ar */
    private String m24659ar() {
        return m24657ao();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        cnd();
        m24652a(UUID.randomUUID());
    }

    @C0064Am(aul = "2f2ef3706522396469196fa4b96e2f8c", aum = 0)
    /* renamed from: aG */
    private void m24653aG() {
        cnd();
        super.mo70aH();
    }

    @C0064Am(aul = "afe6208957a0b9289d690774e5cffc2c", aum = 0)
    private void cnc() {
        for (Integer next : ala().aJk().cqL().cgd().keySet()) {
            if (!cnb().containsKey(next)) {
                C1556Wo cnb = cnb();
                ProgressionCharacterTemplateAbilities ohVar = (ProgressionCharacterTemplateAbilities) bFf().mo6865M(ProgressionCharacterTemplateAbilities.class);
                ohVar.mo21024b(next);
                cnb.put(next, ohVar);
            }
        }
    }

    @C0064Am(aul = "3bda587f88f05e536b4098a4626196fb", aum = 0)
    /* renamed from: au */
    private String m24660au() {
        return "ProgressionCharacterTemplate: " + m24648Qj();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Challenge Rating Abilities")
    @C0064Am(aul = "0f791a21fba27ac1ffa7f2dc48871d44", aum = 0)
    private List<ProgressionCharacterTemplateAbilities> cne() {
        return Collections.unmodifiableList(new ArrayList(cnb().values()));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "d3795b1a781af2322dcc850185cd6704", aum = 0)
    /* renamed from: QE */
    private String m24647QE() {
        return m24648Qj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "ae4e322b56db2e6b972e78228b983a66", aum = 0)
    /* renamed from: aQ */
    private void m24655aQ(String str) {
        m24654aM(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Career")
    @C0064Am(aul = "eef4ee88e188073e7c5190a613e5c6b9", aum = 0)
    private ProgressionCareer cng() {
        return cna();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Career")
    @C0064Am(aul = "dc9e5e079884c264b27ad7584f159ff0", aum = 0)
    /* renamed from: q */
    private void m24665q(ProgressionCareer sbVar) {
        m24664p(sbVar);
    }
}
