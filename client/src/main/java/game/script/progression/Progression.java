package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C3506rj;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aqd  reason: case insensitive filesystem */
/* compiled from: a */
public class Progression extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5177bL = null;
    /* renamed from: bM */
    public static final C5663aRz f5178bM = null;
    /* renamed from: bN */
    public static final C2491fm f5179bN = null;
    /* renamed from: bO */
    public static final C2491fm f5180bO = null;
    /* renamed from: bP */
    public static final C2491fm f5181bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5182bQ = null;
    /* renamed from: dA */
    public static final C2491fm f5183dA = null;
    /* renamed from: du */
    public static final C5663aRz f5185du = null;
    /* renamed from: dv */
    public static final C2491fm f5186dv = null;
    /* renamed from: dw */
    public static final C2491fm f5187dw = null;
    /* renamed from: dz */
    public static final C2491fm f5188dz = null;
    public static final C2491fm goA = null;
    public static final C2491fm goB = null;
    public static final C2491fm goC = null;
    public static final C2491fm goD = null;
    public static final C2491fm goE = null;
    public static final C2491fm goF = null;
    public static final C2491fm goG = null;
    public static final C2491fm goH = null;
    public static final C2491fm goI = null;
    public static final C2491fm goJ = null;
    public static final C2491fm goK = null;
    public static final C2491fm goL = null;
    public static final C2491fm goM = null;
    public static final C5663aRz gov = null;
    public static final C5663aRz gow = null;
    public static final C5663aRz gox = null;
    public static final C5663aRz goy = null;
    public static final C2491fm goz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "435faf4fc8eafadb13363d834f7b3b18", aum = 0)
    private static ProgressionDefaults aZS;
    @C0064Am(aul = "82218fb9b69e2da6388e3acb6c8efe16", aum = 2)
    private static C3438ra<MPMultiplier> aZT;
    @C0064Am(aul = "4d8257dae1e9985b22029b94a99b895a", aum = 3)
    private static C3438ra<ProgressionAbility> aZU;
    @C0064Am(aul = "4a1a2bb7777d3e84e48fe5b9b126f2fb", aum = 4)
    private static C3438ra<ProgressionCharacterTemplate> aZV;
    @C0064Am(aul = "c695b0f386d7e5dc92fd596511fa0825", aum = 6)

    /* renamed from: bK */
    private static UUID f5176bK;
    @C0064Am(aul = "05f926b2fe21a9ae2ca3b41dab91da0f", aum = 1)

    /* renamed from: dt */
    private static C3438ra<ProgressionCareer> f5184dt;
    @C0064Am(aul = "4e2e0d3cf6c3d1304f2b37623bb6faf9", aum = 5)
    private static String handle;

    static {
        m25156V();
    }

    public Progression() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Progression(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25156V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 7;
        _m_methodCount = TaikodomObject._m_methodCount + 22;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(Progression.class, "435faf4fc8eafadb13363d834f7b3b18", i);
        gov = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Progression.class, "05f926b2fe21a9ae2ca3b41dab91da0f", i2);
        f5185du = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Progression.class, "82218fb9b69e2da6388e3acb6c8efe16", i3);
        gow = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Progression.class, "4d8257dae1e9985b22029b94a99b895a", i4);
        gox = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Progression.class, "4a1a2bb7777d3e84e48fe5b9b126f2fb", i5);
        goy = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Progression.class, "4e2e0d3cf6c3d1304f2b37623bb6faf9", i6);
        f5178bM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Progression.class, "c695b0f386d7e5dc92fd596511fa0825", i7);
        f5177bL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i9 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 22)];
        C2491fm a = C4105zY.m41624a(Progression.class, "1d38b6eb7f31253f5ed27b845b2444ae", i9);
        f5179bN = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(Progression.class, "2014e2a79cd9e5ebbe4721885a53f667", i10);
        f5180bO = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(Progression.class, "1efc017ac4226a8420ed18922219a539", i11);
        f5181bP = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(Progression.class, "0a26e1d6f7fb1e8ca761be10e52d2cd0", i12);
        f5182bQ = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(Progression.class, "625f683d9926a744a9ab1f1fb1174b4e", i13);
        goz = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(Progression.class, "4494354ef97617e87ce9b73bbb4fce24", i14);
        goA = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(Progression.class, "edeea31450bceed14663f71da40d1ecf", i15);
        f5186dv = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(Progression.class, "87f0abfa055562b7030232f9f0fced52", i16);
        f5187dw = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(Progression.class, "98911d4dda4f1654eff06b484a8e1b96", i17);
        f5188dz = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(Progression.class, "e97b646b7bce93943df9040b4a2fcbbe", i18);
        f5183dA = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(Progression.class, "b62d91e9639020fd2c56fd823efd3ee0", i19);
        goB = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(Progression.class, "a34511e78d8557cfc7afdcb446361123", i20);
        goC = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(Progression.class, "fd21c4aec74767a7ed933922cdd84b4c", i21);
        goD = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(Progression.class, "432cfdaa0b6702255814c93881ecde4f", i22);
        goE = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(Progression.class, "b8f71b8f38ea4414e4b66e7e443fb4be", i23);
        goF = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(Progression.class, "e13b39dc51aeb7774a325508548443cc", i24);
        goG = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(Progression.class, "4680d94d007795d661e71ea60314dd0c", i25);
        goH = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(Progression.class, "1a0ab7c34fabff711ada25b5423a4982", i26);
        goI = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        C2491fm a19 = C4105zY.m41624a(Progression.class, "4d609c3e66e5b0e5f62b63e97e138108", i27);
        goJ = a19;
        fmVarArr[i27] = a19;
        int i28 = i27 + 1;
        C2491fm a20 = C4105zY.m41624a(Progression.class, "8a139c88cee6d38332abdd4c744466d8", i28);
        goK = a20;
        fmVarArr[i28] = a20;
        int i29 = i28 + 1;
        C2491fm a21 = C4105zY.m41624a(Progression.class, "578f71c4d15755c15948c4fc71113e2a", i29);
        goL = a21;
        fmVarArr[i29] = a21;
        int i30 = i29 + 1;
        C2491fm a22 = C4105zY.m41624a(Progression.class, "5531e8503c4f39ae2f4c7fbd27ae10e2", i30);
        goM = a22;
        fmVarArr[i30] = a22;
        int i31 = i30 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Progression.class, C3506rj.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m25157a(ProgressionDefaults akg) {
        bFf().mo5608dq().mo3197f(gov, akg);
    }

    /* renamed from: a */
    private void m25158a(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f5185du, raVar);
    }

    /* renamed from: a */
    private void m25160a(String str) {
        bFf().mo5608dq().mo3197f(f5178bM, str);
    }

    /* renamed from: a */
    private void m25161a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5177bL, uuid);
    }

    /* renamed from: aB */
    private C3438ra m25162aB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f5185du);
    }

    /* renamed from: an */
    private UUID m25165an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5177bL);
    }

    /* renamed from: ao */
    private String m25166ao() {
        return (String) bFf().mo5608dq().mo3214p(f5178bM);
    }

    /* renamed from: c */
    private void m25173c(UUID uuid) {
        switch (bFf().mo6893i(f5180bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5180bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5180bO, new Object[]{uuid}));
                break;
        }
        m25171b(uuid);
    }

    /* renamed from: cf */
    private void m25174cf(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gow, raVar);
    }

    /* renamed from: cg */
    private void m25175cg(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gox, raVar);
    }

    /* renamed from: ch */
    private void m25176ch(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(goy, raVar);
    }

    private ProgressionDefaults cqG() {
        return (ProgressionDefaults) bFf().mo5608dq().mo3214p(gov);
    }

    private C3438ra cqH() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gow);
    }

    private C3438ra cqI() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gox);
    }

    private C3438ra cqJ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(goy);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3506rj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m25167ap();
            case 1:
                m25171b((UUID) args[0]);
                return null;
            case 2:
                return m25168ar();
            case 3:
                m25170b((String) args[0]);
                return null;
            case 4:
                return cqK();
            case 5:
                m25169b((ProgressionDefaults) args[0]);
                return null;
            case 6:
                m25159a((ProgressionCareer) args[0]);
                return null;
            case 7:
                m25172c((ProgressionCareer) args[0]);
                return null;
            case 8:
                return m25163aC();
            case 9:
                return m25164aE();
            case 10:
                m25177d((MPMultiplier) args[0]);
                return null;
            case 11:
                m25178f((MPMultiplier) args[0]);
                return null;
            case 12:
                return cqM();
            case 13:
                return cqO();
            case 14:
                m25181o((ProgressionAbility) args[0]);
                return null;
            case 15:
                m25182q((ProgressionAbility) args[0]);
                return null;
            case 16:
                return cqQ();
            case 17:
                return cqS();
            case 18:
                m25179f((ProgressionCharacterTemplate) args[0]);
                return null;
            case 19:
                m25180h((ProgressionCharacterTemplate) args[0]);
                return null;
            case 20:
                return cqU();
            case 21:
                return cqW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aD */
    public C3438ra<ProgressionCareer> mo15612aD() {
        switch (bFf().mo6893i(f5188dz)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, f5188dz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5188dz, new Object[0]));
                break;
        }
        return m25163aC();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Careers")
    /* renamed from: aF */
    public List<ProgressionCareer> mo15613aF() {
        switch (bFf().mo6893i(f5183dA)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f5183dA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5183dA, new Object[0]));
                break;
        }
        return m25164aE();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5179bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5179bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5179bN, new Object[0]));
                break;
        }
        return m25167ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Careers")
    /* renamed from: b */
    public void mo15614b(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(f5186dv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5186dv, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5186dv, new Object[]{sbVar}));
                break;
        }
        m25159a(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Defaults")
    /* renamed from: c */
    public void mo15615c(ProgressionDefaults akg) {
        switch (bFf().mo6893i(goA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, goA, new Object[]{akg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, goA, new Object[]{akg}));
                break;
        }
        m25169b(akg);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Defaults")
    public ProgressionDefaults cqL() {
        switch (bFf().mo6893i(goz)) {
            case 0:
                return null;
            case 2:
                return (ProgressionDefaults) bFf().mo5606d(new aCE(this, goz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, goz, new Object[0]));
                break;
        }
        return cqK();
    }

    public C3438ra<MPMultiplier> cqN() {
        switch (bFf().mo6893i(goD)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, goD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, goD, new Object[0]));
                break;
        }
        return cqM();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "MP Multipliers")
    public List<MPMultiplier> cqP() {
        switch (bFf().mo6893i(goE)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, goE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, goE, new Object[0]));
                break;
        }
        return cqO();
    }

    public C3438ra<ProgressionAbility> cqR() {
        switch (bFf().mo6893i(goH)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, goH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, goH, new Object[0]));
                break;
        }
        return cqQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Abilities")
    public List<ProgressionAbility> cqT() {
        switch (bFf().mo6893i(goI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, goI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, goI, new Object[0]));
                break;
        }
        return cqS();
    }

    public C3438ra<ProgressionCharacterTemplate> cqV() {
        switch (bFf().mo6893i(goL)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, goL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, goL, new Object[0]));
                break;
        }
        return cqU();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Character Templates")
    public List<ProgressionCharacterTemplate> cqX() {
        switch (bFf().mo6893i(goM)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, goM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, goM, new Object[0]));
                break;
        }
        return cqW();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Careers")
    /* renamed from: d */
    public void mo15623d(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(f5187dw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5187dw, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5187dw, new Object[]{sbVar}));
                break;
        }
        m25172c(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "MP Multipliers")
    /* renamed from: e */
    public void mo15624e(MPMultiplier tx) {
        switch (bFf().mo6893i(goB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, goB, new Object[]{tx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, goB, new Object[]{tx}));
                break;
        }
        m25177d(tx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "MP Multipliers")
    /* renamed from: g */
    public void mo15625g(MPMultiplier tx) {
        switch (bFf().mo6893i(goC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, goC, new Object[]{tx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, goC, new Object[]{tx}));
                break;
        }
        m25178f(tx);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Character Templates")
    /* renamed from: g */
    public void mo15626g(ProgressionCharacterTemplate aof) {
        switch (bFf().mo6893i(goJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, goJ, new Object[]{aof}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, goJ, new Object[]{aof}));
                break;
        }
        m25179f(aof);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5181bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5181bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5181bP, new Object[0]));
                break;
        }
        return m25168ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5182bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5182bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5182bQ, new Object[]{str}));
                break;
        }
        m25170b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Character Templates")
    /* renamed from: i */
    public void mo15627i(ProgressionCharacterTemplate aof) {
        switch (bFf().mo6893i(goK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, goK, new Object[]{aof}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, goK, new Object[]{aof}));
                break;
        }
        m25180h(aof);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Abilities")
    /* renamed from: p */
    public void mo15628p(ProgressionAbility lk) {
        switch (bFf().mo6893i(goF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, goF, new Object[]{lk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, goF, new Object[]{lk}));
                break;
        }
        m25181o(lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Abilities")
    /* renamed from: r */
    public void mo15629r(ProgressionAbility lk) {
        switch (bFf().mo6893i(goG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, goG, new Object[]{lk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, goG, new Object[]{lk}));
                break;
        }
        m25182q(lk);
    }

    @C0064Am(aul = "1d38b6eb7f31253f5ed27b845b2444ae", aum = 0)
    /* renamed from: ap */
    private UUID m25167ap() {
        return m25165an();
    }

    @C0064Am(aul = "2014e2a79cd9e5ebbe4721885a53f667", aum = 0)
    /* renamed from: b */
    private void m25171b(UUID uuid) {
        m25161a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m25161a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "1efc017ac4226a8420ed18922219a539", aum = 0)
    /* renamed from: ar */
    private String m25168ar() {
        return m25166ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "0a26e1d6f7fb1e8ca761be10e52d2cd0", aum = 0)
    /* renamed from: b */
    private void m25170b(String str) {
        m25160a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Defaults")
    @C0064Am(aul = "625f683d9926a744a9ab1f1fb1174b4e", aum = 0)
    private ProgressionDefaults cqK() {
        return cqG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Defaults")
    @C0064Am(aul = "4494354ef97617e87ce9b73bbb4fce24", aum = 0)
    /* renamed from: b */
    private void m25169b(ProgressionDefaults akg) {
        m25157a(akg);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Careers")
    @C0064Am(aul = "edeea31450bceed14663f71da40d1ecf", aum = 0)
    /* renamed from: a */
    private void m25159a(ProgressionCareer sbVar) {
        m25162aB().add(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Careers")
    @C0064Am(aul = "87f0abfa055562b7030232f9f0fced52", aum = 0)
    /* renamed from: c */
    private void m25172c(ProgressionCareer sbVar) {
        m25162aB().remove(sbVar);
    }

    @C0064Am(aul = "98911d4dda4f1654eff06b484a8e1b96", aum = 0)
    /* renamed from: aC */
    private C3438ra<ProgressionCareer> m25163aC() {
        return m25162aB();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Careers")
    @C0064Am(aul = "e97b646b7bce93943df9040b4a2fcbbe", aum = 0)
    /* renamed from: aE */
    private List<ProgressionCareer> m25164aE() {
        return Collections.unmodifiableList(m25162aB());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "MP Multipliers")
    @C0064Am(aul = "b62d91e9639020fd2c56fd823efd3ee0", aum = 0)
    /* renamed from: d */
    private void m25177d(MPMultiplier tx) {
        cqH().add(tx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "MP Multipliers")
    @C0064Am(aul = "a34511e78d8557cfc7afdcb446361123", aum = 0)
    /* renamed from: f */
    private void m25178f(MPMultiplier tx) {
        cqH().remove(tx);
    }

    @C0064Am(aul = "fd21c4aec74767a7ed933922cdd84b4c", aum = 0)
    private C3438ra<MPMultiplier> cqM() {
        return cqH();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "MP Multipliers")
    @C0064Am(aul = "432cfdaa0b6702255814c93881ecde4f", aum = 0)
    private List<MPMultiplier> cqO() {
        return Collections.unmodifiableList(cqH());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Abilities")
    @C0064Am(aul = "b8f71b8f38ea4414e4b66e7e443fb4be", aum = 0)
    /* renamed from: o */
    private void m25181o(ProgressionAbility lk) {
        cqI().add(lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Abilities")
    @C0064Am(aul = "e13b39dc51aeb7774a325508548443cc", aum = 0)
    /* renamed from: q */
    private void m25182q(ProgressionAbility lk) {
        cqI().remove(lk);
    }

    @C0064Am(aul = "4680d94d007795d661e71ea60314dd0c", aum = 0)
    private C3438ra<ProgressionAbility> cqQ() {
        return cqI();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Abilities")
    @C0064Am(aul = "1a0ab7c34fabff711ada25b5423a4982", aum = 0)
    private List<ProgressionAbility> cqS() {
        return Collections.unmodifiableList(cqI());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Character Templates")
    @C0064Am(aul = "4d609c3e66e5b0e5f62b63e97e138108", aum = 0)
    /* renamed from: f */
    private void m25179f(ProgressionCharacterTemplate aof) {
        cqJ().add(aof);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Character Templates")
    @C0064Am(aul = "8a139c88cee6d38332abdd4c744466d8", aum = 0)
    /* renamed from: h */
    private void m25180h(ProgressionCharacterTemplate aof) {
        cqJ().remove(aof);
    }

    @C0064Am(aul = "578f71c4d15755c15948c4fc71113e2a", aum = 0)
    private C3438ra<ProgressionCharacterTemplate> cqU() {
        return cqJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Character Templates")
    @C0064Am(aul = "5531e8503c4f39ae2f4c7fbd27ae10e2", aum = 0)
    private List<ProgressionCharacterTemplate> cqW() {
        return Collections.unmodifiableList(cqJ());
    }
}
