package game.script.progression;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.C1210Rs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.LK */
/* compiled from: a */
public class ProgressionAbility extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f1022MN = null;

    /* renamed from: QA */
    public static final C5663aRz f1023QA = null;

    /* renamed from: QD */
    public static final C2491fm f1024QD = null;

    /* renamed from: QE */
    public static final C2491fm f1025QE = null;

    /* renamed from: QF */
    public static final C2491fm f1026QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f1027Qz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1029bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1030bM = null;
    /* renamed from: bN */
    public static final C2491fm f1031bN = null;
    /* renamed from: bO */
    public static final C2491fm f1032bO = null;
    /* renamed from: bP */
    public static final C2491fm f1033bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1034bQ = null;
    public static final C5663aRz dxr = null;
    public static final C5663aRz dxt = null;
    public static final C2491fm dxu = null;
    public static final C2491fm dxv = null;
    public static final C2491fm dxw = null;
    public static final C2491fm dxx = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bd2dda7a3d35eeae40efebdf6e32d560", aum = 5)

    /* renamed from: bK */
    private static UUID f1028bK;
    @C0064Am(aul = "18bb11ec538148b37ac7366418eeb50c", aum = 3)
    private static C3438ra<AmplifierType> dxq;
    @C0064Am(aul = "6958eeba71dd47c40c0a12c7bbbdf152", aum = 4)
    private static I18NString dxs;
    @C0064Am(aul = "c0e2c08df5ea8d2fdc36db5623d93f64", aum = 0)
    private static String handle;
    @C0064Am(aul = "2bdfd12a74a57d0502b1b267790107b2", aum = 2)

    /* renamed from: nh */
    private static I18NString f1035nh;
    @C0064Am(aul = "dd9798136dd22d597e5ec103a589c7f1", aum = 1)

    /* renamed from: ni */
    private static I18NString f1036ni;

    static {
        m6676V();
    }

    public ProgressionAbility() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionAbility(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6676V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 14;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ProgressionAbility.class, "c0e2c08df5ea8d2fdc36db5623d93f64", i);
        f1030bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProgressionAbility.class, "dd9798136dd22d597e5ec103a589c7f1", i2);
        f1027Qz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProgressionAbility.class, "2bdfd12a74a57d0502b1b267790107b2", i3);
        f1023QA = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProgressionAbility.class, "18bb11ec538148b37ac7366418eeb50c", i4);
        dxr = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProgressionAbility.class, "6958eeba71dd47c40c0a12c7bbbdf152", i5);
        dxt = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ProgressionAbility.class, "bd2dda7a3d35eeae40efebdf6e32d560", i6);
        f1029bL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 14)];
        C2491fm a = C4105zY.m41624a(ProgressionAbility.class, "1cebb678da53b18fafd25c3217c8c3bf", i8);
        f1031bN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionAbility.class, "3e2ece5f09f9e065a7fc80ea0226bfd7", i9);
        f1032bO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionAbility.class, "0b564e43840cf09e7a375c04ff31c657", i10);
        _f_dispose_0020_0028_0029V = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionAbility.class, "3c9a649e915100bef2bec56235474e7d", i11);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionAbility.class, "11ad2c3c861394e698a6bdf5ab89e261", i12);
        f1022MN = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ProgressionAbility.class, "b35f34826378f3686514f5599904f5c6", i13);
        f1024QD = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ProgressionAbility.class, "17e5316e96c0c4a413f1745830c2efdb", i14);
        f1025QE = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ProgressionAbility.class, "1d7cd7191d4b9c66d90b00b0d1e54ca8", i15);
        f1026QF = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ProgressionAbility.class, "de4859e7085036961fea757b71256741", i16);
        dxu = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ProgressionAbility.class, "56009efbeaae2a86b23331c34e98ebbf", i17);
        dxv = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ProgressionAbility.class, "1cc18c75396c13ebd70e22ff67d2535f", i18);
        dxw = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ProgressionAbility.class, "95be662c9e8c958628f8a5ef3f0f0d86", i19);
        dxx = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ProgressionAbility.class, "886ed2218281ad47f44cd77a8c636f33", i20);
        f1033bP = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(ProgressionAbility.class, "9107296ce4a72e1e135c8aad688d5273", i21);
        f1034bQ = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionAbility.class, C1210Rs.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m6677a(String str) {
        bFf().mo5608dq().mo3197f(f1030bM, str);
    }

    /* renamed from: a */
    private void m6678a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1029bL, uuid);
    }

    /* renamed from: an */
    private UUID m6679an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1029bL);
    }

    /* renamed from: ao */
    private String m6680ao() {
        return (String) bFf().mo5608dq().mo3214p(f1030bM);
    }

    private C3438ra bfY() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dxr);
    }

    private I18NString bfZ() {
        return (I18NString) bFf().mo5608dq().mo3214p(dxt);
    }

    /* renamed from: bg */
    private void m6686bg(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dxr, raVar);
    }

    /* renamed from: bq */
    private void m6687bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f1027Qz, i18NString);
    }

    /* renamed from: br */
    private void m6688br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f1023QA, i18NString);
    }

    /* renamed from: c */
    private void m6691c(UUID uuid) {
        switch (bFf().mo6893i(f1032bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1032bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1032bO, new Object[]{uuid}));
                break;
        }
        m6685b(uuid);
    }

    /* renamed from: iF */
    private void m6693iF(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(dxt, i18NString);
    }

    /* renamed from: vS */
    private I18NString m6697vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f1027Qz);
    }

    /* renamed from: vT */
    private I18NString m6698vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f1023QA);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1210Rs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m6681ap();
            case 1:
                m6685b((UUID) args[0]);
                return null;
            case 2:
                m6692fg();
                return null;
            case 3:
                return m6683au();
            case 4:
                return m6696rO();
            case 5:
                m6689bs((I18NString) args[0]);
                return null;
            case 6:
                return m6699vV();
            case 7:
                m6690bu((I18NString) args[0]);
                return null;
            case 8:
                m6694j((AmplifierType) args[0]);
                return null;
            case 9:
                m6695l((AmplifierType) args[0]);
                return null;
            case 10:
                return bga();
            case 11:
                return bgc();
            case 12:
                return m6682ar();
            case 13:
                m6684b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1031bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1031bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1031bN, new Object[0]));
                break;
        }
        return m6681ap();
    }

    public C3438ra<AmplifierType> bgb() {
        switch (bFf().mo6893i(dxw)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dxw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dxw, new Object[0]));
                break;
        }
        return bga();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bonus")
    public List<AmplifierType> bgd() {
        switch (bFf().mo6893i(dxx)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dxx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dxx, new Object[0]));
                break;
        }
        return bgc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    /* renamed from: bt */
    public void mo3728bt(I18NString i18NString) {
        switch (bFf().mo6893i(f1024QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1024QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1024QD, new Object[]{i18NString}));
                break;
        }
        m6689bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo3729bv(I18NString i18NString) {
        switch (bFf().mo6893i(f1026QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1026QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1026QF, new Object[]{i18NString}));
                break;
        }
        m6690bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m6692fg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1033bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1033bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1033bP, new Object[0]));
                break;
        }
        return m6682ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1034bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1034bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1034bQ, new Object[]{str}));
                break;
        }
        m6684b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bonus")
    /* renamed from: k */
    public void mo3730k(AmplifierType iDVar) {
        switch (bFf().mo6893i(dxu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dxu, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dxu, new Object[]{iDVar}));
                break;
        }
        m6694j(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bonus")
    /* renamed from: m */
    public void mo3731m(AmplifierType iDVar) {
        switch (bFf().mo6893i(dxv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dxv, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dxv, new Object[]{iDVar}));
                break;
        }
        m6695l(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo3732rP() {
        switch (bFf().mo6893i(f1022MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1022MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1022MN, new Object[0]));
                break;
        }
        return m6696rO();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m6683au();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo3734vW() {
        switch (bFf().mo6893i(f1025QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1025QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1025QE, new Object[0]));
                break;
        }
        return m6699vV();
    }

    @C0064Am(aul = "1cebb678da53b18fafd25c3217c8c3bf", aum = 0)
    /* renamed from: ap */
    private UUID m6681ap() {
        return m6679an();
    }

    @C0064Am(aul = "3e2ece5f09f9e065a7fc80ea0226bfd7", aum = 0)
    /* renamed from: b */
    private void m6685b(UUID uuid) {
        m6678a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m6678a(UUID.randomUUID());
    }

    @C0064Am(aul = "0b564e43840cf09e7a375c04ff31c657", aum = 0)
    /* renamed from: fg */
    private void m6692fg() {
        super.dispose();
    }

    @C0064Am(aul = "3c9a649e915100bef2bec56235474e7d", aum = 0)
    /* renamed from: au */
    private String m6683au() {
        return "Progression Ability: " + m6697vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "11ad2c3c861394e698a6bdf5ab89e261", aum = 0)
    /* renamed from: rO */
    private I18NString m6696rO() {
        return m6697vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "b35f34826378f3686514f5599904f5c6", aum = 0)
    /* renamed from: bs */
    private void m6689bs(I18NString i18NString) {
        m6687bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "17e5316e96c0c4a413f1745830c2efdb", aum = 0)
    /* renamed from: vV */
    private I18NString m6699vV() {
        return m6698vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "1d7cd7191d4b9c66d90b00b0d1e54ca8", aum = 0)
    /* renamed from: bu */
    private void m6690bu(I18NString i18NString) {
        m6688br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bonus")
    @C0064Am(aul = "de4859e7085036961fea757b71256741", aum = 0)
    /* renamed from: j */
    private void m6694j(AmplifierType iDVar) {
        bfY().add(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bonus")
    @C0064Am(aul = "56009efbeaae2a86b23331c34e98ebbf", aum = 0)
    /* renamed from: l */
    private void m6695l(AmplifierType iDVar) {
        bfY().remove(iDVar);
    }

    @C0064Am(aul = "1cc18c75396c13ebd70e22ff67d2535f", aum = 0)
    private C3438ra<AmplifierType> bga() {
        return bfY();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bonus")
    @C0064Am(aul = "95be662c9e8c958628f8a5ef3f0f0d86", aum = 0)
    private List<AmplifierType> bgc() {
        return Collections.unmodifiableList(bfY());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "886ed2218281ad47f44cd77a8c636f33", aum = 0)
    /* renamed from: ar */
    private String m6682ar() {
        return m6680ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "9107296ce4a72e1e135c8aad688d5273", aum = 0)
    /* renamed from: b */
    private void m6684b(String str) {
        m6677a(str);
    }
}
