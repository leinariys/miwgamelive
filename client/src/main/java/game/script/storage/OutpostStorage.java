package game.script.storage;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaskletImpl;
import game.script.corporation.Corporation;
import game.script.corporation.NLSCorporation;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.ship.Outpost;
import logic.baa.*;
import logic.data.mbean.C1733Zc;
import logic.data.mbean.C5863abr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@C5829abJ("1.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.Io */
/* compiled from: a */
public class OutpostStorage extends ItemLocation implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f730Do = null;

    /* renamed from: RA */
    public static final C2491fm f731RA = null;

    /* renamed from: RD */
    public static final C2491fm f732RD = null;

    /* renamed from: Rx */
    public static final C2491fm f733Rx = null;

    /* renamed from: Rz */
    public static final C2491fm f734Rz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dfk = null;
    public static final C5663aRz dfl = null;
    public static final C5663aRz dfn = null;
    public static final C2491fm dfp = null;
    public static final C2491fm dfq = null;
    public static final C2491fm dfr = null;
    public static final C2491fm dfs = null;
    public static final C2491fm dft = null;
    public static final C2491fm dfu = null;
    public static final C2491fm dfv = null;
    /* renamed from: ku */
    public static final C2491fm f735ku = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zl */
    public static final C5663aRz f736zl = null;
    private static final int dfi = 7200;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "962818fbc9ccc1b9b25613f85e64117f", aum = 1)
    private static Outpost baT = null;
    @C0064Am(aul = "64d2d9e5381429fecf044c88ebdad275", aum = 0)
    private static float dfj;
    @C0064Am(aul = "84c9c05efa302e3b90fb5df4cf38bb43", aum = 2)
    private static C3438ra<StorageTransaction> dfm;
    @C0064Am(aul = "6409bc61b3bf330005ee0bbae10a7793", aum = 3)
    private static TransactionCleaner dfo;

    static {
        m5439V();
    }

    public OutpostStorage() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OutpostStorage(C5540aNg ang) {
        super(ang);
    }

    public OutpostStorage(Outpost qZVar) {
        super((C5540aNg) null);
        super._m_script_init(qZVar);
    }

    /* renamed from: V */
    static void m5439V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 4;
        _m_methodCount = ItemLocation._m_methodCount + 14;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(OutpostStorage.class, "64d2d9e5381429fecf044c88ebdad275", i);
        dfk = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostStorage.class, "962818fbc9ccc1b9b25613f85e64117f", i2);
        dfl = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostStorage.class, "84c9c05efa302e3b90fb5df4cf38bb43", i3);
        dfn = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OutpostStorage.class, "6409bc61b3bf330005ee0bbae10a7793", i4);
        f736zl = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i6 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 14)];
        C2491fm a = C4105zY.m41624a(OutpostStorage.class, "50fbafb470e9042ab0d40b2448c14039", i6);
        dfp = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostStorage.class, "8b31f434f3e46cc547592c9f28808973", i7);
        dfq = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostStorage.class, "22f4ca8f7cd7c4c66d3db567ca023d6a", i8);
        f734Rz = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostStorage.class, "acefee571aadc17faf1e0910343768f3", i9);
        dfr = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostStorage.class, "c1226e6ef65ece32833ab65593499249", i10);
        dfs = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostStorage.class, "fcaba756df84c44dc605fc85c709dfa1", i11);
        dft = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostStorage.class, "c0ffda660e017809e4565e94337f068c", i12);
        dfu = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(OutpostStorage.class, "d2f21e1cafc18f696a407982b8deca5a", i13);
        f730Do = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(OutpostStorage.class, "57c63e6ed9b97344c1e406ea863f0e24", i14);
        _f_dispose_0020_0028_0029V = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(OutpostStorage.class, "59cf33c6b57f7210f8d7721aa7630871", i15);
        f733Rx = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(OutpostStorage.class, "2d7c64329b9dd42ad6fe261be155ca2e", i16);
        dfv = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(OutpostStorage.class, "3fedb6be63a787dfdcbf1ffc4a4de828", i17);
        f731RA = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(OutpostStorage.class, "0bf9723fdb2774e1ce837029d0cc023b", i18);
        f732RD = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(OutpostStorage.class, "c2ba59862e99a09dfc0a40304da87b47", i19);
        f735ku = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostStorage.class, C1733Zc.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m5440a(TransactionCleaner aVar) {
        bFf().mo5608dq().mo3197f(f736zl, aVar);
    }

    private float aWa() {
        return bFf().mo5608dq().mo3211m(dfk);
    }

    private Outpost aWb() {
        return (Outpost) bFf().mo5608dq().mo3214p(dfl);
    }

    private C3438ra aWc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dfn);
    }

    private TransactionCleaner aWd() {
        return (TransactionCleaner) bFf().mo5608dq().mo3214p(f736zl);
    }

    private boolean aWh() {
        switch (bFf().mo6893i(dfr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dfr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dfr, new Object[0]));
                break;
        }
        return aWg();
    }

    private boolean aWj() {
        switch (bFf().mo6893i(dfs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dfs, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dfs, new Object[0]));
                break;
        }
        return aWi();
    }

    /* access modifiers changed from: private */
    public void aWl() {
        switch (bFf().mo6893i(dft)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dft, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dft, new Object[0]));
                break;
        }
        aWk();
    }

    /* renamed from: ba */
    private void m5444ba(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dfn, raVar);
    }

    /* renamed from: e */
    private void m5445e(Outpost qZVar) {
        bFf().mo5608dq().mo3197f(dfl, qZVar);
    }

    /* renamed from: fy */
    private void m5447fy(float f) {
        bFf().mo5608dq().mo3150a(dfk, f);
    }

    @C0064Am(aul = "8b31f434f3e46cc547592c9f28808973", aum = 0)
    @C5566aOg
    /* renamed from: fz */
    private void m5448fz(float f) {
        throw new aWi(new aCE(this, dfq, new Object[]{new Float(f)}));
    }

    /* renamed from: v */
    private boolean m5453v(Item auq) {
        switch (bFf().mo6893i(dfv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dfv, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dfv, new Object[]{auq}));
                break;
        }
        return m5452u(auq);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1733Zc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                return new Float(aWe());
            case 1:
                m5448fz(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(m5454wD());
            case 3:
                return new Boolean(aWg());
            case 4:
                return new Boolean(aWi());
            case 5:
                aWk();
                return null;
            case 6:
                return aWm();
            case 7:
                m5442a((C0665JT) args[0]);
                return null;
            case 8:
                m5446fg();
                return null;
            case 9:
                return new Boolean(m5450k((Item) args[0]));
            case 10:
                return new Boolean(m5452u((Item) args[0]));
            case 11:
                m5443a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 12:
                m5451m((Item) args[0]);
                return null;
            case 13:
                m5449g((Item) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public float aWf() {
        switch (bFf().mo6893i(dfp)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dfp, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dfp, new Object[0]));
                break;
        }
        return aWe();
    }

    public List<StorageTransaction> aWn() {
        switch (bFf().mo6893i(dfu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dfu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dfu, new Object[0]));
                break;
        }
        return aWm();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f730Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f730Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f730Do, new Object[]{jt}));
                break;
        }
        m5442a(jt);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f731RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f731RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f731RA, new Object[]{auq, aag}));
                break;
        }
        m5443a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m5446fg();
    }

    @C5566aOg
    /* renamed from: fA */
    public void mo2877fA(float f) {
        switch (bFf().mo6893i(dfq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dfq, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dfq, new Object[]{new Float(f)}));
                break;
        }
        m5448fz(f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f735ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f735ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f735ku, new Object[]{auq}));
                break;
        }
        m5449g(auq);
    }

    /* renamed from: l */
    public boolean mo2878l(Item auq) {
        switch (bFf().mo6893i(f733Rx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f733Rx, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f733Rx, new Object[]{auq}));
                break;
        }
        return m5450k(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f732RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f732RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f732RD, new Object[]{auq}));
                break;
        }
        m5451m(auq);
    }

    /* renamed from: wE */
    public float mo2696wE() {
        switch (bFf().mo6893i(f734Rz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f734Rz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f734Rz, new Object[0]));
                break;
        }
        return m5454wD();
    }

    /* renamed from: f */
    public void mo2876f(Outpost qZVar) {
        super.mo10S();
        m5445e(qZVar);
        TransactionCleaner aVar = (TransactionCleaner) bFf().mo6865M(TransactionCleaner.class);
        aVar.mo2879a(this, (aDJ) this);
        m5440a(aVar);
        aWd().mo4704hk(7200.0f);
    }

    @C0064Am(aul = "50fbafb470e9042ab0d40b2448c14039", aum = 0)
    private float aWe() {
        return aWa();
    }

    @C0064Am(aul = "22f4ca8f7cd7c4c66d3db567ca023d6a", aum = 0)
    /* renamed from: wD */
    private float m5454wD() {
        return aWf();
    }

    @C0064Am(aul = "acefee571aadc17faf1e0910343768f3", aum = 0)
    private boolean aWg() {
        Corporation bYd = aWb().bYd();
        if (bYd == null) {
            return false;
        }
        return bYd.mo10718b(aPA(), C6704asA.ACCESS_CORP_STORAGE);
    }

    @C0064Am(aul = "c1226e6ef65ece32833ab65593499249", aum = 0)
    private boolean aWi() {
        Corporation bYd = aWb().bYd();
        if (bYd == null) {
            return false;
        }
        Player aPA = aPA();
        if (aPA == null) {
            return true;
        }
        return bYd.mo10718b(aPA, C6704asA.REMOVE_FROM_CORP_STORAGE);
    }

    @C0064Am(aul = "fcaba756df84c44dc605fc85c709dfa1", aum = 0)
    private void aWk() {
        Iterator it = aWc().iterator();
        long cVr = cVr();
        while (it.hasNext() && cVr - ((StorageTransaction) it.next()).getTimestamp() > StorageTransaction.ffI) {
            it.remove();
        }
    }

    @C0064Am(aul = "c0ffda660e017809e4565e94337f068c", aum = 0)
    private List<StorageTransaction> aWm() {
        return Collections.unmodifiableList(aWc());
    }

    @C0064Am(aul = "d2f21e1cafc18f696a407982b8deca5a", aum = 0)
    /* renamed from: a */
    private void m5442a(C0665JT jt) {
        if (jt.mo3117j(1, 1, 0) && aWd() == null) {
            TransactionCleaner aVar = (TransactionCleaner) bFf().mo6865M(TransactionCleaner.class);
            aVar.mo2879a(this, (aDJ) this);
            m5440a(aVar);
        }
    }

    @C0064Am(aul = "57c63e6ed9b97344c1e406ea863f0e24", aum = 0)
    /* renamed from: fg */
    private void m5446fg() {
        super.dispose();
        m5445e((Outpost) null);
    }

    @C0064Am(aul = "59cf33c6b57f7210f8d7721aa7630871", aum = 0)
    /* renamed from: k */
    private boolean m5450k(Item auq) {
        return aWj();
    }

    @C0064Am(aul = "2d7c64329b9dd42ad6fe261be155ca2e", aum = 0)
    /* renamed from: u */
    private boolean m5452u(Item auq) {
        if (!aWh()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "3fedb6be63a787dfdcbf1ffc4a4de828", aum = 0)
    /* renamed from: a */
    private void m5443a(Item auq, ItemLocation aag) {
        if (auq.mo302Iq() > 0 && aPA() != null) {
            C3438ra aWc = aWc();
            StorageTransaction vdVar = (StorageTransaction) bFf().mo6865M(StorageTransaction.class);
            vdVar.mo22611a(aPA().getName(), cVr(), auq.bAP(), auq.mo302Iq(), StorageTransaction.C3839a.ADDITION);
            aWc.add(vdVar);
        }
    }

    @C0064Am(aul = "0bf9723fdb2774e1ce837029d0cc023b", aum = 0)
    /* renamed from: m */
    private void m5451m(Item auq) {
        if (auq.mo302Iq() > 0 && aPA() != null) {
            C3438ra aWc = aWc();
            StorageTransaction vdVar = (StorageTransaction) bFf().mo6865M(StorageTransaction.class);
            vdVar.mo22611a(aPA().getName(), cVr(), auq.bAP(), auq.mo302Iq(), StorageTransaction.C3839a.REMOVAL);
            aWc.add(vdVar);
        }
    }

    @C0064Am(aul = "c2ba59862e99a09dfc0a40304da87b47", aum = 0)
    /* renamed from: g */
    private void m5449g(Item auq) {
        if (!m5453v(auq)) {
            throw new C2293dh(((NLSCorporation) ala().aIY().mo6310c(NLSManager.C1472a.CORPORATION)).mo18099ot());
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.Io$a */
    public class TransactionCleaner extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f737JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f738aT = null;

        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "bf8714ebd88496432df3d84145694b82", aum = 0)
        static /* synthetic */ OutpostStorage eYb = null;

        static {
            m5470V();
        }

        public TransactionCleaner() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public TransactionCleaner(OutpostStorage io, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(io, adj);
        }

        public TransactionCleaner(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m5470V() {
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(TransactionCleaner.class, "bf8714ebd88496432df3d84145694b82", i);
            f738aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(TransactionCleaner.class, "e6f1f0b702c44d756bc96bfda37f6f05", i3);
            f737JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(TransactionCleaner.class, C5863abr.class, _m_fields, _m_methods);
        }

        /* renamed from: c */
        private void m5471c(OutpostStorage io) {
            bFf().mo5608dq().mo3197f(f738aT, io);
        }

        private OutpostStorage ddF() {
            return (OutpostStorage) bFf().mo5608dq().mo3214p(f738aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5863abr(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m5472pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f737JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f737JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f737JD, new Object[0]));
                    break;
            }
            m5472pi();
        }

        /* renamed from: a */
        public void mo2879a(OutpostStorage io, aDJ adj) {
            m5471c(io);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "e6f1f0b702c44d756bc96bfda37f6f05", aum = 0)
        /* renamed from: pi */
        private void m5472pi() {
            ddF().aWl();
        }
    }
}
