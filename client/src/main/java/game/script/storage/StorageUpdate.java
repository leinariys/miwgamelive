package game.script.storage;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C0801Lb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.LX */
/* compiled from: a */
public class StorageUpdate extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dyu = null;
    public static final C5663aRz dyv = null;
    public static final C2491fm dyw = null;
    public static final C2491fm dyx = null;
    public static final C2491fm dyy = null;
    public static final C2491fm dyz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C3248pc
    @C0064Am(aul = "6cba1e3608dbc88cb02f71e5d02cfded", aum = 0)
    @C2537gW
    private static float dsY;
    @C3248pc
    @C0064Am(aul = "71ad059edc3a705bb07422665e4a2070", aum = 1)
    @C2537gW
    private static long dsZ;

    static {
        m6785V();
    }

    public StorageUpdate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StorageUpdate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6785V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 4;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(StorageUpdate.class, "6cba1e3608dbc88cb02f71e5d02cfded", i);
        dyu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StorageUpdate.class, "71ad059edc3a705bb07422665e4a2070", i2);
        dyv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(StorageUpdate.class, "a9042591e476118e5a5805ae766c3ea9", i4);
        dyw = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(StorageUpdate.class, "fe6fd4294cad7df7f9e156e93835b33b", i5);
        dyx = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(StorageUpdate.class, "26dc2e15b7e5a93ba399bb9be2e89730", i6);
        dyy = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(StorageUpdate.class, "1e67ade9c5415677f543405a2b2eefde", i7);
        dyz = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StorageUpdate.class, C0801Lb.class, _m_fields, _m_methods);
    }

    private float bgi() {
        return bFf().mo5608dq().mo3211m(dyu);
    }

    private long bgj() {
        return bFf().mo5608dq().mo3213o(dyv);
    }

    /* renamed from: eZ */
    private void m6786eZ(long j) {
        bFf().mo5608dq().mo3184b(dyv, j);
    }

    /* renamed from: gj */
    private void m6788gj(float f) {
        bFf().mo5608dq().mo3150a(dyu, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0801Lb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return new Long(bgk());
            case 1:
                m6787fa(((Long) args[0]).longValue());
                return null;
            case 2:
                return new Float(bgm());
            case 3:
                m6789gk(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public long bgl() {
        switch (bFf().mo6893i(dyw)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dyw, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dyw, new Object[0]));
                break;
        }
        return bgk();
    }

    public float bgn() {
        switch (bFf().mo6893i(dyy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dyy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dyy, new Object[0]));
                break;
        }
        return bgm();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: fb */
    public void mo3766fb(long j) {
        switch (bFf().mo6893i(dyx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dyx, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dyx, new Object[]{new Long(j)}));
                break;
        }
        m6787fa(j);
    }

    /* renamed from: gl */
    public void mo3767gl(float f) {
        switch (bFf().mo6893i(dyz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dyz, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dyz, new Object[]{new Float(f)}));
                break;
        }
        m6789gk(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a9042591e476118e5a5805ae766c3ea9", aum = 0)
    private long bgk() {
        return bgj();
    }

    @C0064Am(aul = "fe6fd4294cad7df7f9e156e93835b33b", aum = 0)
    /* renamed from: fa */
    private void m6787fa(long j) {
        m6786eZ(j);
    }

    @C0064Am(aul = "26dc2e15b7e5a93ba399bb9be2e89730", aum = 0)
    private float bgm() {
        return bgi();
    }

    @C0064Am(aul = "1e67ade9c5415677f543405a2b2eefde", aum = 0)
    /* renamed from: gk */
    private void m6789gk(float f) {
        m6788gj(f);
    }
}
