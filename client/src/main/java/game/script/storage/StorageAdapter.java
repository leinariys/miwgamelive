package game.script.storage;

import game.network.message.externalizable.aCE;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1344Tc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ZY */
/* compiled from: a */
public class StorageAdapter extends GameObjectAdapter<StorageAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aVS = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m12089V();
    }

    public StorageAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StorageAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12089V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 1;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(StorageAdapter.class, "e2245d7e0e25cf627b25ca763eff3dbc", i);
        aVS = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StorageAdapter.class, C1344Tc.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1344Tc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m12090c((Storage) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public float mo7432d(Storage qzVar) {
        switch (bFf().mo6893i(aVS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aVS, new Object[]{qzVar}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aVS, new Object[]{qzVar}));
                break;
        }
        return m12090c(qzVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "e2245d7e0e25cf627b25ca763eff3dbc", aum = 0)
    /* renamed from: c */
    private float m12090c(Storage qzVar) {
        return ((StorageAdapter) mo16071IG()).mo7432d(qzVar);
    }
}
