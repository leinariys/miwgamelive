package game.script.storage;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.item.ItemType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0482Gf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Date;

@C5511aMd
@C6485anp
/* renamed from: a.vd */
/* compiled from: a */
public class StorageTransaction extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: cA */
    public static final C2491fm f9425cA = null;
    /* renamed from: cB */
    public static final C2491fm f9426cB = null;
    /* renamed from: cC */
    public static final C2491fm f9427cC = null;
    /* renamed from: cD */
    public static final C2491fm f9428cD = null;
    /* renamed from: cx */
    public static final C5663aRz f9430cx = null;
    /* renamed from: cz */
    public static final C5663aRz f9432cz = null;
    public static final int ffH = 5;
    public static final long ffI = 432000000;
    public static final C5663aRz ffJ = null;
    public static final C5663aRz ffK = null;
    public static final C5663aRz ffL = null;
    public static final C2491fm ffM = null;
    public static final C2491fm ffN = null;
    public static final C2491fm ffO = null;
    public static final C2491fm ffP = null;
    public static final C2491fm ffQ = null;
    public static final C2491fm ffR = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "6130d0f569b0090c019f9db00d1468a0", aum = 0)

    /* renamed from: Af */
    private static String f9424Af = null;
    @C0064Am(aul = "b060eb2c79af4af80aec43e098520591", aum = 4)
    private static C3839a cXC = null;
    @C0064Am(aul = "73fed8ed9576502d3c2e16d5e5b8b31d", aum = 2)

    /* renamed from: cw */
    private static ItemType f9429cw = null;
    @C0064Am(aul = "ce0495e25166c22279b4dd5f572cfa93", aum = 3)

    /* renamed from: cy */
    private static int f9431cy = 0;
    @C0064Am(aul = "720460d2b2682c555b9fd8db34f6e23d", aum = 1)
    private static long timestamp;

    static {
        m40309V();
    }

    public StorageTransaction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StorageTransaction(C5540aNg ang) {
        super(ang);
    }

    public StorageTransaction(String str, long j, ItemType jCVar, int i, C3839a aVar) {
        super((C5540aNg) null);
        super._m_script_init(str, j, jCVar, i, aVar);
    }

    /* renamed from: V */
    static void m40309V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(StorageTransaction.class, "6130d0f569b0090c019f9db00d1468a0", i);
        ffJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StorageTransaction.class, "720460d2b2682c555b9fd8db34f6e23d", i2);
        ffK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(StorageTransaction.class, "73fed8ed9576502d3c2e16d5e5b8b31d", i3);
        f9430cx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(StorageTransaction.class, "ce0495e25166c22279b4dd5f572cfa93", i4);
        f9432cz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(StorageTransaction.class, "b060eb2c79af4af80aec43e098520591", i5);
        ffL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 11)];
        C2491fm a = C4105zY.m41624a(StorageTransaction.class, "23a55cc5475b7b06818126cec1bd6865", i7);
        ffM = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(StorageTransaction.class, "ebbf7a25f14799a21037931ecd3410d7", i8);
        ffN = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(StorageTransaction.class, "d78d3dbe8f11184e93adbe035e564c52", i9);
        ffO = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(StorageTransaction.class, "0486dc6cc49321077b052f131580b1ec", i10);
        ffP = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(StorageTransaction.class, "8f79c97ef6685d9cfb1179790f82490d", i11);
        f9427cC = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(StorageTransaction.class, "1e478d36a9de2e147b20dd64527a642c", i12);
        f9428cD = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(StorageTransaction.class, "7912624c7bca5ea263f031ff93d9374f", i13);
        f9425cA = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(StorageTransaction.class, "1e46d377b2d7110f05005228e298f57b", i14);
        f9426cB = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(StorageTransaction.class, "428a192aeae3e74136f63c70716c1a2f", i15);
        ffQ = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(StorageTransaction.class, "59d8595da07119df1bc2ef95dc110196", i16);
        ffR = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(StorageTransaction.class, "623119dadae18aa9d22680e69bff0468", i17);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StorageTransaction.class, C0482Gf.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m40310a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f9430cx, jCVar);
    }

    /* renamed from: a */
    private void m40311a(C3839a aVar) {
        bFf().mo5608dq().mo3197f(ffL, aVar);
    }

    /* renamed from: av */
    private ItemType m40313av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f9430cx);
    }

    /* renamed from: aw */
    private int m40314aw() {
        return bFf().mo5608dq().mo3212n(f9432cz);
    }

    @C0064Am(aul = "1e478d36a9de2e147b20dd64527a642c", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m40317b(ItemType jCVar) {
        throw new aWi(new aCE(this, f9428cD, new Object[]{jCVar}));
    }

    @C0064Am(aul = "59d8595da07119df1bc2ef95dc110196", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m40318b(C3839a aVar) {
        throw new aWi(new aCE(this, ffR, new Object[]{aVar}));
    }

    private String bQf() {
        return (String) bFf().mo5608dq().mo3214p(ffJ);
    }

    private long bQg() {
        return bFf().mo5608dq().mo3213o(ffK);
    }

    private C3839a bQh() {
        return (C3839a) bFf().mo5608dq().mo3214p(ffL);
    }

    /* renamed from: f */
    private void m40319f(int i) {
        bFf().mo5608dq().mo3183b(f9432cz, i);
    }

    @C0064Am(aul = "1e46d377b2d7110f05005228e298f57b", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m40320g(int i) {
        throw new aWi(new aCE(this, f9426cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: gL */
    private void m40321gL(long j) {
        bFf().mo5608dq().mo3184b(ffK, j);
    }

    @C0064Am(aul = "0486dc6cc49321077b052f131580b1ec", aum = 0)
    @C5566aOg
    /* renamed from: gM */
    private void m40322gM(long j) {
        throw new aWi(new aCE(this, ffP, new Object[]{new Long(j)}));
    }

    /* renamed from: it */
    private void m40323it(String str) {
        bFf().mo5608dq().mo3197f(ffJ, str);
    }

    @C0064Am(aul = "ebbf7a25f14799a21037931ecd3410d7", aum = 0)
    @C5566aOg
    /* renamed from: iu */
    private void m40324iu(String str) {
        throw new aWi(new aCE(this, ffN, new Object[]{str}));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0482Gf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return bQi();
            case 1:
                m40324iu((String) args[0]);
                return null;
            case 2:
                return new Long(bQk());
            case 3:
                m40322gM(((Long) args[0]).longValue());
                return null;
            case 4:
                return m40316ay();
            case 5:
                m40317b((ItemType) args[0]);
                return null;
            case 6:
                return new Integer(m40315ax());
            case 7:
                m40320g(((Integer) args[0]).intValue());
                return null;
            case 8:
                return bQl();
            case 9:
                m40318b((C3839a) args[0]);
                return null;
            case 10:
                return m40312au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: az */
    public ItemType mo22612az() {
        switch (bFf().mo6893i(f9427cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f9427cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9427cC, new Object[0]));
                break;
        }
        return m40316ay();
    }

    public String bQj() {
        switch (bFf().mo6893i(ffM)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ffM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ffM, new Object[0]));
                break;
        }
        return bQi();
    }

    public C3839a bQm() {
        switch (bFf().mo6893i(ffQ)) {
            case 0:
                return null;
            case 2:
                return (C3839a) bFf().mo5606d(new aCE(this, ffQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ffQ, new Object[0]));
                break;
        }
        return bQl();
    }

    @C5566aOg
    /* renamed from: c */
    public void mo22615c(ItemType jCVar) {
        switch (bFf().mo6893i(f9428cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9428cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9428cD, new Object[]{jCVar}));
                break;
        }
        m40317b(jCVar);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo22616c(C3839a aVar) {
        switch (bFf().mo6893i(ffR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffR, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffR, new Object[]{aVar}));
                break;
        }
        m40318b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: gC */
    public void mo22617gC(long j) {
        switch (bFf().mo6893i(ffP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffP, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffP, new Object[]{new Long(j)}));
                break;
        }
        m40322gM(j);
    }

    public int getAmount() {
        switch (bFf().mo6893i(f9425cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f9425cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9425cA, new Object[0]));
                break;
        }
        return m40315ax();
    }

    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f9426cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9426cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9426cB, new Object[]{new Integer(i)}));
                break;
        }
        m40320g(i);
    }

    public long getTimestamp() {
        switch (bFf().mo6893i(ffO)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ffO, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ffO, new Object[0]));
                break;
        }
        return bQk();
    }

    @C5566aOg
    /* renamed from: iv */
    public void mo22620iv(String str) {
        switch (bFf().mo6893i(ffN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffN, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffN, new Object[]{str}));
                break;
        }
        m40324iu(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m40312au();
    }

    /* renamed from: a */
    public void mo22611a(String str, long j, ItemType jCVar, int i, C3839a aVar) {
        super.mo10S();
        m40323it(str);
        m40321gL(j);
        m40310a(jCVar);
        m40319f(i);
        m40311a(aVar);
    }

    @C0064Am(aul = "23a55cc5475b7b06818126cec1bd6865", aum = 0)
    private String bQi() {
        return bQf();
    }

    @C0064Am(aul = "d78d3dbe8f11184e93adbe035e564c52", aum = 0)
    private long bQk() {
        return bQg();
    }

    @C0064Am(aul = "8f79c97ef6685d9cfb1179790f82490d", aum = 0)
    /* renamed from: ay */
    private ItemType m40316ay() {
        return m40313av();
    }

    @C0064Am(aul = "7912624c7bca5ea263f031ff93d9374f", aum = 0)
    /* renamed from: ax */
    private int m40315ax() {
        return m40314aw();
    }

    @C0064Am(aul = "428a192aeae3e74136f63c70716c1a2f", aum = 0)
    private C3839a bQl() {
        return bQh();
    }

    @C0064Am(aul = "623119dadae18aa9d22680e69bff0468", aum = 0)
    /* renamed from: au */
    private String m40312au() {
        return "Date: " + new Date(bQg()) + " Player: " + bQf() + " Item: " + m40313av().mo19891ke().get() + " Amount: " + m40314aw() + " Transaction Type: " + bQh();
    }

    /* renamed from: a.vd$a */
    public enum C3839a {
        ADDITION,
        REMOVAL
    }
}
