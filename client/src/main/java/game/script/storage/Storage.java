package game.script.storage;

import game.network.message.externalizable.aCE;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.ship.Station;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2652iA;
import logic.data.mbean.C3363qi;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.qz */
/* compiled from: a */
public class Storage extends ItemLocation implements C1616Xf, C6872avM {

    /* renamed from: RA */
    public static final C2491fm f8961RA = null;

    /* renamed from: RD */
    public static final C2491fm f8962RD = null;

    /* renamed from: Rx */
    public static final C2491fm f8963Rx = null;

    /* renamed from: Rz */
    public static final C2491fm f8964Rz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C2491fm avL = null;
    public static final C5663aRz bvd = null;
    public static final C5663aRz bve = null;
    public static final C2491fm bvf = null;
    public static final C2491fm bvg = null;
    public static final C2491fm bvh = null;
    public static final C2491fm bvi = null;
    /* renamed from: kX */
    public static final C5663aRz f8966kX = null;
    /* renamed from: ku */
    public static final C2491fm f8967ku = null;
    /* renamed from: lm */
    public static final C2491fm f8968lm = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7780a2feeb9343a342f41d0e2109b45b", aum = 2)
    @C5566aOg
    private static AdapterLikedList<StorageAdapter> aOw;
    @C0064Am(aul = "25e5c50fcba74a193a4d593a39df7b6e", aum = 1)
    private static Player aUB;
    @C0064Am(aul = "504d86b1fd223dd73062e3460e75b7a8", aum = 3)
    private static float aUC;
    @C0064Am(aul = "2d7d1308e495ae1114bc08403bd1137a", aum = 0)

    /* renamed from: iH */
    private static Station f8965iH;

    static {
        m37903V();
    }

    public Storage() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Storage(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37903V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 4;
        _m_methodCount = ItemLocation._m_methodCount + 19;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(Storage.class, "2d7d1308e495ae1114bc08403bd1137a", i);
        f8966kX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Storage.class, "25e5c50fcba74a193a4d593a39df7b6e", i2);
        bvd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Storage.class, "7780a2feeb9343a342f41d0e2109b45b", i3);
        aOx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Storage.class, "504d86b1fd223dd73062e3460e75b7a8", i4);
        bve = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i6 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 19)];
        C2491fm a = C4105zY.m41624a(Storage.class, "5f529372fd29e0cb6b5f21309e391e5a", i6);
        aPe = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(Storage.class, "5eab0a93f1cf3b2fc4090433ee8ba62f", i7);
        aPf = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(Storage.class, "f4c74d05edfaa70731d79509ed7d5e53", i8);
        bvf = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(Storage.class, "0b96e596bd1be5fb33017a27b2ebaeb0", i9);
        f8968lm = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(Storage.class, "d6425a0468621717338763b37d45a83a", i10);
        avL = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(Storage.class, "62ab2d180098eb85333833f0171491fd", i11);
        bvg = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(Storage.class, "5ce0839756f3bd8800b673fed84f4dee", i12);
        bvh = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(Storage.class, "03104a4ca69c2f03f7c2ef65e07d835f", i13);
        f8964Rz = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(Storage.class, "727b53adeaffbcdbd34a6c7adb64bd2a", i14);
        bvi = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(Storage.class, "95f34c4416583cf6c472c978a471f242", i15);
        f8963Rx = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(Storage.class, "773c9e2668a1a988538efddff2b76dca", i16);
        _f_dispose_0020_0028_0029V = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(Storage.class, "ced2779efa9f9ca7ba64a5a880c89d5e", i17);
        _f_onResurrect_0020_0028_0029V = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(Storage.class, "7ff00fcc322c05d85330ad51142e536c", i18);
        f8961RA = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(Storage.class, "7fe94d7e60022989e37b265c7746a711", i19);
        f8962RD = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(Storage.class, "040c60498f9f409583749f9612e25425", i20);
        f8967ku = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(Storage.class, "839429836b514574941ec2140c4c9c40", i21);
        aPl = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(Storage.class, "40369fe20328f451836b7ec72d944581", i22);
        aPj = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(Storage.class, "b03f7440486dafd0f255e6a61af36b24", i23);
        aPk = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        C2491fm a19 = C4105zY.m41624a(Storage.class, "0a70140021841ff8f12287c66669b67d", i24);
        aPm = a19;
        fmVarArr[i24] = a19;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Storage.class, C3363qi.class, _m_fields, _m_methods);
    }

    /* renamed from: TH */
    private void m37895TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m37894TG();
    }

    /* renamed from: TP */
    private void m37900TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m37899TO();
    }

    /* renamed from: Te */
    private AdapterLikedList m37902Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: Z */
    private void m37904Z(Player aku) {
        bFf().mo5608dq().mo3197f(bvd, aku);
    }

    /* renamed from: a */
    private void m37905a(Station bf) {
        bFf().mo5608dq().mo3197f(f8966kX, bf);
    }

    /* renamed from: a */
    private void m37907a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    @C0064Am(aul = "5ce0839756f3bd8800b673fed84f4dee", aum = 0)
    @C5566aOg
    /* renamed from: aa */
    private void m37909aa(Player aku) {
        throw new aWi(new aCE(this, bvh, new Object[]{aku}));
    }

    private Player aje() {
        return (Player) bFf().mo5608dq().mo3214p(bvd);
    }

    private float ajf() {
        return bFf().mo5608dq().mo3211m(bve);
    }

    @C0064Am(aul = "727b53adeaffbcdbd34a6c7adb64bd2a", aum = 0)
    @C5566aOg
    @C2499fr
    private void ajk() {
        throw new aWi(new aCE(this, bvi, new Object[0]));
    }

    /* renamed from: dN */
    private void m37910dN(float f) {
        bFf().mo5608dq().mo3150a(bve, f);
    }

    /* renamed from: eF */
    private Station m37911eF() {
        return (Station) bFf().mo5608dq().mo3214p(f8966kX);
    }

    @C0064Am(aul = "d6425a0468621717338763b37d45a83a", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m37914g(Station bf) {
        throw new aWi(new aCE(this, avL, new Object[]{bf}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TJ */
    public AdapterLikedList<StorageAdapter> mo21538TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m37896TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m37897TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m37898TM();
    }

    /* renamed from: TR */
    public void mo21539TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m37901TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3363qi(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                m37894TG();
                return null;
            case 1:
                return m37896TI();
            case 2:
                return new Float(ajg());
            case 3:
                return m37912eS();
            case 4:
                m37914g((Station) args[0]);
                return null;
            case 5:
                return aji();
            case 6:
                m37909aa((Player) args[0]);
                return null;
            case 7:
                return new Float(m37918wD());
            case 8:
                ajk();
                return null;
            case 9:
                return new Boolean(m37916k((Item) args[0]));
            case 10:
                m37913fg();
                return null;
            case 11:
                m37908aG();
                return null;
            case 12:
                m37906a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 13:
                m37917m((Item) args[0]);
                return null;
            case 14:
                m37915g((Item) args[0]);
                return null;
            case 15:
                m37899TO();
                return null;
            case 16:
                m37897TK();
                return null;
            case 17:
                m37898TM();
                return null;
            case 18:
                m37901TQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m37908aG();
    }

    @C5566aOg
    /* renamed from: ab */
    public void mo21540ab(Player aku) {
        switch (bFf().mo6893i(bvh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bvh, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bvh, new Object[]{aku}));
                break;
        }
        m37909aa(aku);
    }

    public float ajh() {
        switch (bFf().mo6893i(bvf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bvf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bvf, new Object[0]));
                break;
        }
        return ajg();
    }

    public Player ajj() {
        switch (bFf().mo6893i(bvg)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, bvg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bvg, new Object[0]));
                break;
        }
        return aji();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f8961RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8961RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8961RA, new Object[]{auq, aag}));
                break;
        }
        m37906a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m37913fg();
    }

    /* renamed from: eT */
    public Station mo21543eT() {
        switch (bFf().mo6893i(f8968lm)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f8968lm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8968lm, new Object[0]));
                break;
        }
        return m37912eS();
    }

    @C5566aOg
    /* renamed from: h */
    public void mo21544h(Station bf) {
        switch (bFf().mo6893i(avL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avL, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avL, new Object[]{bf}));
                break;
        }
        m37914g(bf);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f8967ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8967ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8967ku, new Object[]{auq}));
                break;
        }
        m37915g(auq);
    }

    /* renamed from: l */
    public boolean mo2878l(Item auq) {
        switch (bFf().mo6893i(f8963Rx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8963Rx, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8963Rx, new Object[]{auq}));
                break;
        }
        return m37916k(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f8962RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8962RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8962RD, new Object[]{auq}));
                break;
        }
        m37917m(auq);
    }

    @C5566aOg
    @C2499fr
    public void pushState() {
        switch (bFf().mo6893i(bvi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bvi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bvi, new Object[0]));
                break;
        }
        ajk();
    }

    /* renamed from: wE */
    public float mo2696wE() {
        switch (bFf().mo6893i(f8964Rz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f8964Rz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8964Rz, new Object[0]));
                break;
        }
        return m37918wD();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m37895TH();
    }

    @C0064Am(aul = "5f529372fd29e0cb6b5f21309e391e5a", aum = 0)
    /* renamed from: TG */
    private void m37894TG() {
        if (m37902Te() == null) {
            AdapterLikedList zAVar = (AdapterLikedList) bFf().mo6865M(AdapterLikedList.class);
            BaseAdapter aVar = (BaseAdapter) bFf().mo6865M(BaseAdapter.class);
            aVar.mo21547b(this);
            zAVar.mo23260c(aVar);
            m37907a(zAVar);
            m37902Te().mo23259b((C6872avM) this);
            m37900TP();
        }
    }

    @C0064Am(aul = "5eab0a93f1cf3b2fc4090433ee8ba62f", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<StorageAdapter> m37896TI() {
        if (m37902Te() == null) {
            m37895TH();
        }
        return m37902Te();
    }

    @C0064Am(aul = "f4c74d05edfaa70731d79509ed7d5e53", aum = 0)
    private float ajg() {
        return ala().aJe().mo19017xr();
    }

    @C0064Am(aul = "0b96e596bd1be5fb33017a27b2ebaeb0", aum = 0)
    /* renamed from: eS */
    private Station m37912eS() {
        return m37911eF();
    }

    @C0064Am(aul = "62ab2d180098eb85333833f0171491fd", aum = 0)
    private Player aji() {
        return aje();
    }

    @C0064Am(aul = "03104a4ca69c2f03f7c2ef65e07d835f", aum = 0)
    /* renamed from: wD */
    private float m37918wD() {
        return ajf();
    }

    @C0064Am(aul = "95f34c4416583cf6c472c978a471f242", aum = 0)
    /* renamed from: k */
    private boolean m37916k(Item auq) {
        return aje().bQB() && super.mo2878l(auq);
    }

    @C0064Am(aul = "773c9e2668a1a988538efddff2b76dca", aum = 0)
    /* renamed from: fg */
    private void m37913fg() {
        super.dispose();
        m37911eF().mo652l(this);
        m37904Z((Player) null);
        m37905a((Station) null);
    }

    @C0064Am(aul = "ced2779efa9f9ca7ba64a5a880c89d5e", aum = 0)
    /* renamed from: aG */
    private void m37908aG() {
        super.mo70aH();
        if (m37902Te() == null) {
            m37895TH();
        }
    }

    @C0064Am(aul = "7ff00fcc322c05d85330ad51142e536c", aum = 0)
    /* renamed from: a */
    private void m37906a(Item auq, ItemLocation aag) {
        if (aje() != null) {
            aje().mo12641b(auq, aag, this);
        }
    }

    @C0064Am(aul = "7fe94d7e60022989e37b265c7746a711", aum = 0)
    /* renamed from: m */
    private void m37917m(Item auq) {
        if (aje() != null) {
            aje().mo12651d(auq, this);
        }
    }

    @C0064Am(aul = "040c60498f9f409583749f9612e25425", aum = 0)
    /* renamed from: g */
    private void m37915g(Item auq) {
        if (!aje().bQB()) {
            throw new C2293dh(((NLSItem) ala().aIY().mo6310c(NLSManager.C1472a.ITEM)).bto());
        }
    }

    @C0064Am(aul = "839429836b514574941ec2140c4c9c40", aum = 0)
    /* renamed from: TO */
    private void m37899TO() {
        m37910dN(((StorageAdapter) m37902Te().ase()).mo7432d(this));
    }

    @C0064Am(aul = "40369fe20328f451836b7ec72d944581", aum = 0)
    /* renamed from: TK */
    private void m37897TK() {
        m37900TP();
    }

    @C0064Am(aul = "b03f7440486dafd0f255e6a61af36b24", aum = 0)
    /* renamed from: TM */
    private void m37898TM() {
        m37900TP();
    }

    @C0064Am(aul = "0a70140021841ff8f12287c66669b67d", aum = 0)
    /* renamed from: TQ */
    private void m37901TQ() {
        if (m37902Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m37902Te().mo23259b((C6872avM) this);
        m37900TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.qz$a */
    public class BaseAdapter extends StorageAdapter implements C1616Xf {

        /* renamed from: PF */
        public static final C2491fm f8969PF = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8971aT = null;
        public static final C2491fm aVS = null;
        public static final C2491fm aVT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "6b98775580ab13c626d3895b0a8dc1ce", aum = 0)

        /* renamed from: Yp */
        static /* synthetic */ Storage f8970Yp;

        static {
            m37939V();
        }

        public BaseAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseAdapter(Storage qzVar) {
            super((C5540aNg) null);
            super._m_script_init(qzVar);
        }

        /* renamed from: V */
        static void m37939V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = StorageAdapter._m_fieldCount + 1;
            _m_methodCount = StorageAdapter._m_methodCount + 3;
            int i = StorageAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseAdapter.class, "6b98775580ab13c626d3895b0a8dc1ce", i);
            f8971aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) StorageAdapter._m_fields, (Object[]) _m_fields);
            int i3 = StorageAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
            C2491fm a = C4105zY.m41624a(BaseAdapter.class, "238f7d23877f21b8c9314c5b1e23f762", i3);
            aVS = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseAdapter.class, "9486d92a2289444083c23d9849fad98a", i4);
            aVT = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseAdapter.class, "209cff4e5d6ca990fa7d32a0693d8aa2", i5);
            f8969PF = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) StorageAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseAdapter.class, C2652iA.class, _m_fields, _m_methods);
        }

        /* renamed from: Xh */
        private Storage m37940Xh() {
            return (Storage) bFf().mo5608dq().mo3214p(f8971aT);
        }

        @C0064Am(aul = "209cff4e5d6ca990fa7d32a0693d8aa2", aum = 0)
        /* renamed from: a */
        private void m37942a(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            mo21546b((StorageAdapter) ato, (StorageAdapter) ato2, (StorageAdapter) ato3);
        }

        /* renamed from: a */
        private void m37943a(Storage qzVar) {
            bFf().mo5608dq().mo3197f(f8971aT, qzVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2652iA(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - StorageAdapter._m_methodCount) {
                case 0:
                    return new Float(m37944c((Storage) args[0]));
                case 1:
                    m37941a((StorageAdapter) args[0], (StorageAdapter) args[1], (StorageAdapter) args[2]);
                    return null;
                case 2:
                    m37942a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public void mo21546b(StorageAdapter zy, StorageAdapter zy2, StorageAdapter zy3) {
            switch (bFf().mo6893i(aVT)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aVT, new Object[]{zy, zy2, zy3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aVT, new Object[]{zy, zy2, zy3}));
                    break;
            }
            m37941a(zy, zy2, zy3);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ void mo12950b(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            switch (bFf().mo6893i(f8969PF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f8969PF, new Object[]{ato, ato2, ato3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f8969PF, new Object[]{ato, ato2, ato3}));
                    break;
            }
            m37942a(ato, ato2, ato3);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo7432d(Storage qzVar) {
            switch (bFf().mo6893i(aVS)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aVS, new Object[]{qzVar}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aVS, new Object[]{qzVar}));
                    break;
            }
            return m37944c(qzVar);
        }

        /* renamed from: b */
        public void mo21547b(Storage qzVar) {
            m37943a(qzVar);
            super.mo10S();
        }

        @C0064Am(aul = "238f7d23877f21b8c9314c5b1e23f762", aum = 0)
        /* renamed from: c */
        private float m37944c(Storage qzVar) {
            if (qzVar == m37940Xh()) {
                return m37940Xh().ajh();
            }
            throw new IllegalStateException("Original storage cant be diferent from adapter list owner.");
        }

        @C0064Am(aul = "9486d92a2289444083c23d9849fad98a", aum = 0)
        /* renamed from: a */
        private void m37941a(StorageAdapter zy, StorageAdapter zy2, StorageAdapter zy3) {
            if (zy != null) {
                throw new IllegalArgumentException();
            } else if (zy3 != this) {
                throw new IllegalArgumentException();
            } else {
                super.mo12950b(zy, zy2, zy3);
            }
        }
    }
}
