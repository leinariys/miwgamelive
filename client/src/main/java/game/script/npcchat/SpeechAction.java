package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C6352alM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.zk */
/* compiled from: a */
public abstract class SpeechAction extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f9715CT = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bJ */
    public static final C5663aRz f9716bJ = null;
    /* renamed from: bL */
    public static final C5663aRz f9718bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9719bM = null;
    /* renamed from: bN */
    public static final C2491fm f9720bN = null;
    /* renamed from: bO */
    public static final C2491fm f9721bO = null;
    /* renamed from: bP */
    public static final C2491fm f9722bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9723bQ = null;
    public static final C2491fm bYT = null;
    public static final C2491fm bYU = null;
    public static final C2491fm bYV = null;
    public static final C2491fm bYW = null;
    public static final C5663aRz bfP = null;
    public static final C2491fm bfV = null;
    public static final C2491fm bfX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2c1d7f5cecc09ddfe76f6e9926f6407a", aum = 2)

    /* renamed from: bK */
    private static UUID f9717bK;
    @C0064Am(aul = "39bae67318b450bacf7a3a5d769baa9d", aum = 0)
    private static PlayerSpeech bYS;
    @C0064Am(aul = "20bf6028f90e8c4ece14e613f2e5f5e9", aum = 1)
    private static Asset bfO;
    @C0064Am(aul = "1abc404b12cf1d04265ea7f0dd05015e", aum = 3)
    private static String handle;

    static {
        m41784V();
    }

    public SpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41784V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(SpeechAction.class, "39bae67318b450bacf7a3a5d769baa9d", i);
        f9716bJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpeechAction.class, "20bf6028f90e8c4ece14e613f2e5f5e9", i2);
        bfP = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpeechAction.class, "2c1d7f5cecc09ddfe76f6e9926f6407a", i3);
        f9718bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpeechAction.class, "1abc404b12cf1d04265ea7f0dd05015e", i4);
        f9719bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 12)];
        C2491fm a = C4105zY.m41624a(SpeechAction.class, "0e34f926e282933f1ee564d424f88213", i6);
        f9720bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(SpeechAction.class, "0b3986d29554ab0739bee79b730a16a9", i7);
        f9721bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeechAction.class, "8dcdb5269f1ceac11123fdec46924648", i8);
        f9722bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeechAction.class, "3aa04470ccd168d7ea9deb67786b36dd", i9);
        f9723bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeechAction.class, "dde59d1794ee2b61f2a545efa7bd32cc", i10);
        bYT = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(SpeechAction.class, "98d1a0587260419ac69e143107a016fd", i11);
        bYU = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(SpeechAction.class, "e728b8643a010affaaf4a99d1cf7c999", i12);
        bYV = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(SpeechAction.class, "32786ed9454f847bd19a1acb110bb17b", i13);
        bfV = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(SpeechAction.class, "e94f4b5256b10ac9877b1fc89651abe9", i14);
        bfX = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(SpeechAction.class, "05c2718f260169d29c5936f87e28cbf3", i15);
        f9715CT = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(SpeechAction.class, "521092b45bdec372940b05418662e18d", i16);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(SpeechAction.class, "79d2949dde9391edcf0dbe32b881d1f9", i17);
        bYW = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeechAction.class, C6352alM.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m41782S(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bfP, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C0064Am(aul = "e94f4b5256b10ac9877b1fc89651abe9", aum = 0)
    @C5566aOg
    /* renamed from: T */
    private void m41783T(Asset tCVar) {
        throw new aWi(new aCE(this, bfX, new Object[]{tCVar}));
    }

    /* renamed from: a */
    private void m41785a(String str) {
        bFf().mo5608dq().mo3197f(f9719bM, str);
    }

    /* renamed from: a */
    private void m41786a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9718bL, uuid);
    }

    @C0064Am(aul = "79d2949dde9391edcf0dbe32b881d1f9", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m41787a(I18NString i18NString, Object[] objArr) {
        throw new aWi(new aCE(this, bYW, new Object[]{i18NString, objArr}));
    }

    private Asset abq() {
        return (Asset) bFf().mo5608dq().mo3214p(bfP);
    }

    /* renamed from: an */
    private UUID m41788an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9718bL);
    }

    /* renamed from: ao */
    private String m41789ao() {
        return (String) bFf().mo5608dq().mo3214p(f9719bM);
    }

    private PlayerSpeech arN() {
        return (PlayerSpeech) bFf().mo5608dq().mo3214p(f9716bJ);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "3aa04470ccd168d7ea9deb67786b36dd", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m41793b(String str) {
        throw new aWi(new aCE(this, f9723bQ, new Object[]{str}));
    }

    @C0064Am(aul = "05c2718f260169d29c5936f87e28cbf3", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m41795c(C5426aIw aiw) {
        throw new aWi(new aCE(this, f9715CT, new Object[]{aiw}));
    }

    /* renamed from: c */
    private void m41796c(UUID uuid) {
        switch (bFf().mo6893i(f9721bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9721bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9721bO, new Object[]{uuid}));
                break;
        }
        m41794b(uuid);
    }

    /* renamed from: d */
    private void m41797d(PlayerSpeech av) {
        bFf().mo5608dq().mo3197f(f9716bJ, av);
    }

    @C0064Am(aul = "dde59d1794ee2b61f2a545efa7bd32cc", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m41798e(PlayerSpeech av) {
        throw new aWi(new aCE(this, bYT, new Object[]{av}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C5566aOg
    /* renamed from: U */
    public void mo23378U(Asset tCVar) {
        switch (bFf().mo6893i(bfX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfX, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfX, new Object[]{tCVar}));
                break;
        }
        m41783T(tCVar);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6352alM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m41790ap();
            case 1:
                m41794b((UUID) args[0]);
                return null;
            case 2:
                return m41791ar();
            case 3:
                m41793b((String) args[0]);
                return null;
            case 4:
                m41798e((PlayerSpeech) args[0]);
                return null;
            case 5:
                return arO();
            case 6:
                m41799g((C5426aIw) args[0]);
                return null;
            case 7:
                return abw();
            case 8:
                m41783T((Asset) args[0]);
                return null;
            case 9:
                m41795c((C5426aIw) args[0]);
                return null;
            case 10:
                return m41792au();
            case 11:
                m41787a((I18NString) args[0], (Object[]) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    public Asset abx() {
        switch (bFf().mo6893i(bfV)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, bfV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfV, new Object[0]));
                break;
        }
        return abw();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9720bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9720bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9720bN, new Object[0]));
                break;
        }
        return m41790ap();
    }

    public final PlayerSpeech arP() {
        switch (bFf().mo6893i(bYU)) {
            case 0:
                return null;
            case 2:
                return (PlayerSpeech) bFf().mo5606d(new aCE(this, bYU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bYU, new Object[0]));
                break;
        }
        return arO();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final void mo23381b(I18NString i18NString, Object... objArr) {
        switch (bFf().mo6893i(bYW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYW, new Object[]{i18NString, objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYW, new Object[]{i18NString, objArr}));
                break;
        }
        m41787a(i18NString, objArr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f9715CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9715CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9715CT, new Object[]{aiw}));
                break;
        }
        m41795c(aiw);
    }

    @C5566aOg
    /* renamed from: f */
    public final void mo23382f(PlayerSpeech av) {
        switch (bFf().mo6893i(bYT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYT, new Object[]{av}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYT, new Object[]{av}));
                break;
        }
        m41798e(av);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9722bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9722bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9722bP, new Object[0]));
                break;
        }
        return m41791ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9723bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9723bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9723bQ, new Object[]{str}));
                break;
        }
        m41793b(str);
    }

    /* renamed from: h */
    public void mo23383h(C5426aIw aiw) {
        switch (bFf().mo6893i(bYV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYV, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYV, new Object[]{aiw}));
                break;
        }
        m41799g(aiw);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m41792au();
    }

    @C0064Am(aul = "0e34f926e282933f1ee564d424f88213", aum = 0)
    /* renamed from: ap */
    private UUID m41790ap() {
        return m41788an();
    }

    @C0064Am(aul = "0b3986d29554ab0739bee79b730a16a9", aum = 0)
    /* renamed from: b */
    private void m41794b(UUID uuid) {
        m41786a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "8dcdb5269f1ceac11123fdec46924648", aum = 0)
    /* renamed from: ar */
    private String m41791ar() {
        return m41789ao();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m41786a(UUID.randomUUID());
    }

    @C0064Am(aul = "98d1a0587260419ac69e143107a016fd", aum = 0)
    private PlayerSpeech arO() {
        return arN();
    }

    @C0064Am(aul = "e728b8643a010affaaf4a99d1cf7c999", aum = 0)
    /* renamed from: g */
    private void m41799g(C5426aIw aiw) {
        if (abx() != null) {
            aPA().dxc().mo22075bR(abx());
        }
        mo2354d(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    @C0064Am(aul = "32786ed9454f847bd19a1acb110bb17b", aum = 0)
    private Asset abw() {
        return abq();
    }

    @C0064Am(aul = "521092b45bdec372940b05418662e18d", aum = 0)
    /* renamed from: au */
    private String m41792au() {
        return String.valueOf(getClass().getSimpleName()) + ": Speech [" + arN() + "]";
    }
}
