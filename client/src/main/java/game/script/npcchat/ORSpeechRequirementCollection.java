package game.script.npcchat;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6013ael;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.mI */
/* compiled from: a */
public class ORSpeechRequirementCollection extends SpeechRequirementCollection implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m35471V();
    }

    public ORSpeechRequirementCollection() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ORSpeechRequirementCollection(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35471V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirementCollection._m_fieldCount + 0;
        _m_methodCount = SpeechRequirementCollection._m_methodCount + 1;
        _m_fields = new C5663aRz[(SpeechRequirementCollection._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) SpeechRequirementCollection._m_fields, (Object[]) _m_fields);
        int i = SpeechRequirementCollection._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(ORSpeechRequirementCollection.class, "bf89f146ec890c9c303dfc0d9c43eb69", i);
        aDQ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirementCollection._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ORSpeechRequirementCollection.class, C6013ael.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6013ael(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirementCollection._m_methodCount) {
            case 0:
                return new Boolean(m35472e((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: f */
    public boolean mo14981f(C5426aIw aiw) {
        switch (bFf().mo6893i(aDQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aDQ, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aDQ, new Object[]{aiw}));
                break;
        }
        return m35472e(aiw);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "bf89f146ec890c9c303dfc0d9c43eb69", aum = 0)
    /* renamed from: e */
    private boolean m35472e(C5426aIw aiw) {
        for (SpeechRequirement b : cmc()) {
            if (b.mo2483b(aiw)) {
                return true;
            }
        }
        for (SpeechRequirementCollection f : cme()) {
            if (f.mo14981f(aiw)) {
                return true;
            }
        }
        return false;
    }
}
