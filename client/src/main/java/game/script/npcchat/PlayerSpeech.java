package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1700ZA;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.Av */
/* compiled from: a */
public class PlayerSpeech extends AbstractSpeech implements C1616Xf, C7022azj {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz chB = null;
    public static final C2491fm chC = null;
    public static final C2491fm chD = null;
    public static final C2491fm chE = null;
    public static final C2491fm chF = null;
    public static final C2491fm chG = null;
    public static final C2491fm chH = null;
    public static final C2491fm chI = null;
    public static final C2491fm chJ = null;
    public static final C2491fm chK = null;
    public static final C2491fm chL = null;
    public static final C2491fm chM = null;
    public static final C2491fm chN = null;
    public static final C5663aRz chx = null;
    public static final C5663aRz chz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "75ff800d2921235844692c1316a7ab68", aum = 2)
    private static C3438ra<SpeechAction> chA;
    @C0064Am(aul = "f090480999d47e75377c659baf7f439c", aum = 0)
    private static int chw;
    @C0064Am(aul = "03db3c899e128cc0128541f924bd7324", aum = 1)
    private static C3438ra<NPCSpeech> chy;

    static {
        m645V();
    }

    public PlayerSpeech() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerSpeech(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m645V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AbstractSpeech._m_fieldCount + 3;
        _m_methodCount = AbstractSpeech._m_methodCount + 13;
        int i = AbstractSpeech._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(PlayerSpeech.class, "f090480999d47e75377c659baf7f439c", i);
        chx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerSpeech.class, "03db3c899e128cc0128541f924bd7324", i2);
        chz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerSpeech.class, "75ff800d2921235844692c1316a7ab68", i3);
        chB = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AbstractSpeech._m_fields, (Object[]) _m_fields);
        int i5 = AbstractSpeech._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 13)];
        C2491fm a = C4105zY.m41624a(PlayerSpeech.class, "7940aa24ad8e90128b03c27c5fa0ccd2", i5);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerSpeech.class, "18a318b673af950a46bb00bc0b6acb3d", i6);
        chC = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerSpeech.class, "1b68dbe90838cd5ca4010c1d0d6c3653", i7);
        chD = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerSpeech.class, "ea44b43f9b4f1b8f015cf5cab27ad3ba", i8);
        chE = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerSpeech.class, "9045a8daa119d58e4b54b47adca48c09", i9);
        chF = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerSpeech.class, "89895111467eb5a73a7e8788b8c2adef", i10);
        chG = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerSpeech.class, "a4e7fe5443cccd48cf9921776da9f4d4", i11);
        chH = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerSpeech.class, "573c25f6197b5b8e23b229776e5d56f0", i12);
        chI = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerSpeech.class, "1a6071ac695298553cc24bf66666d857", i13);
        chJ = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerSpeech.class, "72ae5ddb78a6f6572d6df3d681cd2941", i14);
        chK = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerSpeech.class, "f9d0218651fdcc4ff52c8b5f725204f5", i15);
        chL = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerSpeech.class, "e3c1a1afa031f74119dd60d20d83eb2b", i16);
        chM = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerSpeech.class, "850b664cb3095ae46eeeba11e50db538", i17);
        chN = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AbstractSpeech._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerSpeech.class, C1700ZA.class, _m_fields, _m_methods);
    }

    @C5566aOg
    /* renamed from: n */
    private static void m658n(List<NPCSpeech> list) {
        if (list.isEmpty()) {
            throw new IllegalArgumentException("The speech list is empty");
        } else if (list.size() > 1) {
            throw new IllegalStateException("The speech list has more than one object");
        }
    }

    @C0064Am(aul = "e3c1a1afa031f74119dd60d20d83eb2b", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private List<PlayerSpeech> m646a(NPCSpeech agl, Player aku) {
        throw new aWi(new aCE(this, chM, new Object[]{agl, aku}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCSpeeches")
    @C0064Am(aul = "1b68dbe90838cd5ca4010c1d0d6c3653", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m647a(NPCSpeech agl) {
        throw new aWi(new aCE(this, chD, new Object[]{agl}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Actions")
    @C0064Am(aul = "89895111467eb5a73a7e8788b8c2adef", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m648a(SpeechAction zkVar) {
        throw new aWi(new aCE(this, chG, new Object[]{zkVar}));
    }

    /* renamed from: aa */
    private void m649aa(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(chz, raVar);
    }

    /* renamed from: ab */
    private void m650ab(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(chB, raVar);
    }

    @C0064Am(aul = "72ae5ddb78a6f6572d6df3d681cd2941", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ad */
    private void m651ad(Player aku) {
        throw new aWi(new aCE(this, chK, new Object[]{aku}));
    }

    private int avJ() {
        return bFf().mo5608dq().mo3212n(chx);
    }

    private C3438ra avK() {
        return (C3438ra) bFf().mo5608dq().mo3214p(chz);
    }

    private C3438ra avL() {
        return (C3438ra) bFf().mo5608dq().mo3214p(chB);
    }

    @C5566aOg
    @C0064Am(aul = "f9d0218651fdcc4ff52c8b5f725204f5", aum = 0)
    @C2499fr(mo18857qh = 1)
    private Map<NPCSpeech, List<PlayerSpeech>> avS() {
        throw new aWi(new aCE(this, chL, new Object[0]));
    }

    @C5566aOg
    /* renamed from: b */
    private List<PlayerSpeech> m653b(NPCSpeech agl, Player aku) {
        switch (bFf().mo6893i(chM)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, chM, new Object[]{agl, aku}));
            case 3:
                bFf().mo5606d(new aCE(this, chM, new Object[]{agl, aku}));
                break;
        }
        return m646a(agl, aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCSpeeches")
    @C0064Am(aul = "ea44b43f9b4f1b8f015cf5cab27ad3ba", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m654c(NPCSpeech agl) {
        throw new aWi(new aCE(this, chE, new Object[]{agl}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Actions")
    @C0064Am(aul = "a4e7fe5443cccd48cf9921776da9f4d4", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m655c(SpeechAction zkVar) {
        throw new aWi(new aCE(this, chH, new Object[]{zkVar}));
    }

    /* renamed from: fW */
    private void m656fW(int i) {
        bFf().mo5608dq().mo3183b(chx, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Order")
    @C0064Am(aul = "1a6071ac695298553cc24bf66666d857", aum = 0)
    @C5566aOg
    /* renamed from: fX */
    private void m657fX(int i) {
        throw new aWi(new aCE(this, chJ, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "850b664cb3095ae46eeeba11e50db538", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m659o(List<? extends PlayerSpeech> list) {
        throw new aWi(new aCE(this, chN, new Object[]{list}));
    }

    @C5566aOg
    /* renamed from: p */
    private void m660p(List<? extends PlayerSpeech> list) {
        switch (bFf().mo6893i(chN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chN, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chN, new Object[]{list}));
                break;
        }
        m659o(list);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1700ZA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AbstractSpeech._m_methodCount) {
            case 0:
                return m652au();
            case 1:
                return avM();
            case 2:
                m647a((NPCSpeech) args[0]);
                return null;
            case 3:
                m654c((NPCSpeech) args[0]);
                return null;
            case 4:
                return avO();
            case 5:
                m648a((SpeechAction) args[0]);
                return null;
            case 6:
                m655c((SpeechAction) args[0]);
                return null;
            case 7:
                return new Integer(avQ());
            case 8:
                m657fX(((Integer) args[0]).intValue());
                return null;
            case 9:
                m651ad((Player) args[0]);
                return null;
            case 10:
                return avS();
            case 11:
                return m646a((NPCSpeech) args[0], (Player) args[1]);
            case 12:
                m659o((List) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ae */
    public void mo533ae(Player aku) {
        switch (bFf().mo6893i(chK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chK, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chK, new Object[]{aku}));
                break;
        }
        m651ad(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCSpeeches")
    public List<NPCSpeech> avN() {
        switch (bFf().mo6893i(chC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, chC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, chC, new Object[0]));
                break;
        }
        return avM();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Actions")
    public List<SpeechAction> avP() {
        switch (bFf().mo6893i(chF)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, chF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, chF, new Object[0]));
                break;
        }
        return avO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Order")
    public int avR() {
        switch (bFf().mo6893i(chI)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, chI, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, chI, new Object[0]));
                break;
        }
        return avQ();
    }

    @C5566aOg
    @C2499fr(mo18857qh = 1)
    public Map<NPCSpeech, List<PlayerSpeech>> avT() {
        switch (bFf().mo6893i(chL)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, chL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, chL, new Object[0]));
                break;
        }
        return avS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCSpeeches")
    @C5566aOg
    /* renamed from: b */
    public void mo538b(NPCSpeech agl) {
        switch (bFf().mo6893i(chD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chD, new Object[]{agl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chD, new Object[]{agl}));
                break;
        }
        m647a(agl);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Actions")
    @C5566aOg
    /* renamed from: b */
    public void mo539b(SpeechAction zkVar) {
        switch (bFf().mo6893i(chG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chG, new Object[]{zkVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chG, new Object[]{zkVar}));
                break;
        }
        m648a(zkVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCSpeeches")
    @C5566aOg
    /* renamed from: d */
    public void mo540d(NPCSpeech agl) {
        switch (bFf().mo6893i(chE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chE, new Object[]{agl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chE, new Object[]{agl}));
                break;
        }
        m654c(agl);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Actions")
    @C5566aOg
    /* renamed from: d */
    public void mo541d(SpeechAction zkVar) {
        switch (bFf().mo6893i(chH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chH, new Object[]{zkVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chH, new Object[]{zkVar}));
                break;
        }
        m655c(zkVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Order")
    @C5566aOg
    /* renamed from: fY */
    public void mo542fY(int i) {
        switch (bFf().mo6893i(chJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chJ, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chJ, new Object[]{new Integer(i)}));
                break;
        }
        m657fX(i);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m652au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "7940aa24ad8e90128b03c27c5fa0ccd2", aum = 0)
    /* renamed from: au */
    private String m652au() {
        return "O[" + hYY.format((long) avR()) + "] " + getHandle();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCSpeeches")
    @C0064Am(aul = "18a318b673af950a46bb00bc0b6acb3d", aum = 0)
    private List<NPCSpeech> avM() {
        return Collections.unmodifiableList(avK());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Actions")
    @C0064Am(aul = "9045a8daa119d58e4b54b47adca48c09", aum = 0)
    private List<SpeechAction> avO() {
        return Collections.unmodifiableList(avL());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Order")
    @C0064Am(aul = "573c25f6197b5b8e23b229776e5d56f0", aum = 0)
    private int avQ() {
        return avJ();
    }

    /* renamed from: a.Av$b */
    /* compiled from: a */
    class C0081b implements Comparator<NPCSpeech> {
        C0081b() {
        }

        /* renamed from: a */
        public int compare(NPCSpeech agl, NPCSpeech agl2) {
            int compareTo = Integer.valueOf(agl.getPriority()).compareTo(Integer.valueOf(agl2.getPriority()));
            return compareTo != 0 ? compareTo : agl.ddT().get().compareTo(agl2.ddT().get());
        }
    }

    /* renamed from: a.Av$a */
    class C0080a implements Comparator<PlayerSpeech> {
        C0080a() {
        }

        /* renamed from: a */
        public int compare(PlayerSpeech av, PlayerSpeech av2) {
            return Integer.valueOf(av.avR()).compareTo(Integer.valueOf(av2.avR()));
        }
    }
}
