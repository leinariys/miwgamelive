package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6399amH;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.agL  reason: case insensitive filesystem */
/* compiled from: a */
public class NPCSpeech extends AbstractSpeech implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bfP = null;
    public static final C2491fm bfV = null;
    public static final C2491fm bfX = null;
    public static final C2491fm dTl = null;
    public static final C2491fm eyN = null;
    public static final C2491fm eyO = null;
    public static final C5663aRz eyu = null;
    public static final C5663aRz fEp = null;
    public static final C5663aRz fEq = null;
    public static final C5663aRz fEs = null;
    public static final C2491fm fEt = null;
    public static final C2491fm fEu = null;
    public static final C2491fm fEv = null;
    public static final C2491fm fEw = null;
    public static final C2491fm fEx = null;
    public static final C2491fm fEy = null;
    public static final C2491fm fEz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e83bfd05bb5e140722c65645fab4efc0", aum = 2)
    private static Asset bfO;
    @C0064Am(aul = "6ddc5077439122799da4122c7c5113d3", aum = 3)
    private static boolean dIR;
    @C0064Am(aul = "987a5e9e379d45c84477dac84fef25c0", aum = 1)
    private static C3438ra<PlayerSpeech> fEo;
    @C0064Am(aul = "77d7b3dfb3e576bf39784b26f0e415c2", aum = 4)
    private static boolean fEr;
    @C0064Am(aul = "0fc00f8d0ea959a4e9b217266bf5f5c5", aum = 0)
    private static int priority;

    static {
        m21982V();
    }

    public NPCSpeech() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCSpeech(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m21982V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AbstractSpeech._m_fieldCount + 5;
        _m_methodCount = AbstractSpeech._m_methodCount + 13;
        int i = AbstractSpeech._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(NPCSpeech.class, "0fc00f8d0ea959a4e9b217266bf5f5c5", i);
        eyu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCSpeech.class, "987a5e9e379d45c84477dac84fef25c0", i2);
        fEp = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPCSpeech.class, "e83bfd05bb5e140722c65645fab4efc0", i3);
        bfP = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NPCSpeech.class, "6ddc5077439122799da4122c7c5113d3", i4);
        fEq = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NPCSpeech.class, "77d7b3dfb3e576bf39784b26f0e415c2", i5);
        fEs = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AbstractSpeech._m_fields, (Object[]) _m_fields);
        int i7 = AbstractSpeech._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 13)];
        C2491fm a = C4105zY.m41624a(NPCSpeech.class, "81c5dea238683c4b0e55704a08c33ef6", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCSpeech.class, "4770b7deb0ee37826ef9403829ab0988", i8);
        dTl = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCSpeech.class, "0caf147beaa90b4f07af4fa15d38a846", i9);
        eyO = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCSpeech.class, "cfd5b7ff134508a2702c86b04f4774b0", i10);
        eyN = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCSpeech.class, "7a21fdefc9e9d8c38758f9f030691f5b", i11);
        fEt = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCSpeech.class, "8aeeac8b561b4ecd1780e75c850d30d7", i12);
        fEu = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCSpeech.class, "c1b61bb0903ce3bcfaf580e9b2ea465b", i13);
        fEv = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCSpeech.class, "73ec4db4e160e3f3acaa524ddaade10a", i14);
        bfV = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(NPCSpeech.class, "9fd84a9b36f8c38f441387ef927606e8", i15);
        bfX = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(NPCSpeech.class, "758fc529a93c3164c2287f7c8900fcb8", i16);
        fEw = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(NPCSpeech.class, "c397749fc3e0b4f1a00a5da237089009", i17);
        fEx = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(NPCSpeech.class, "d20576e62dfd669f9686efa1a76ee468", i18);
        fEy = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(NPCSpeech.class, "2e1c2d9b5adcce73b5694721477ca625", i19);
        fEz = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AbstractSpeech._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCSpeech.class, C6399amH.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m21980S(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bfP, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C0064Am(aul = "9fd84a9b36f8c38f441387ef927606e8", aum = 0)
    @C5566aOg
    /* renamed from: T */
    private void m21981T(Asset tCVar) {
        throw new aWi(new aCE(this, bfX, new Object[]{tCVar}));
    }

    private Asset abq() {
        return (Asset) bFf().mo5608dq().mo3214p(bfP);
    }

    private int bDJ() {
        return bFf().mo5608dq().mo3212n(eyu);
    }

    /* renamed from: bQ */
    private void m21985bQ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fEp, raVar);
    }

    private C3438ra bXh() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fEp);
    }

    private boolean bXi() {
        return bFf().mo5608dq().mo3201h(fEq);
    }

    private boolean bXj() {
        return bFf().mo5608dq().mo3201h(fEs);
    }

    /* renamed from: el */
    private void m21986el(boolean z) {
        bFf().mo5608dq().mo3153a(fEq, z);
    }

    /* renamed from: em */
    private void m21987em(boolean z) {
        bFf().mo5608dq().mo3153a(fEs, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Popup on dock")
    @C0064Am(aul = "c397749fc3e0b4f1a00a5da237089009", aum = 0)
    @C5566aOg
    /* renamed from: en */
    private void m21988en(boolean z) {
        throw new aWi(new aCE(this, fEx, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unclosable window")
    @C0064Am(aul = "2e1c2d9b5adcce73b5694721477ca625", aum = 0)
    @C5566aOg
    /* renamed from: ep */
    private void m21989ep(boolean z) {
        throw new aWi(new aCE(this, fEz, new Object[]{new Boolean(z)}));
    }

    /* renamed from: oY */
    private void m21992oY(int i) {
        bFf().mo5608dq().mo3183b(eyu, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C5566aOg
    /* renamed from: U */
    public void mo13348U(Asset tCVar) {
        switch (bFf().mo6893i(bfX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfX, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfX, new Object[]{tCVar}));
                break;
        }
        m21981T(tCVar);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6399amH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AbstractSpeech._m_methodCount) {
            case 0:
                return m21984au();
            case 1:
                return new Integer(m21983W(args[0]));
            case 2:
                return new Integer(bDT());
            case 3:
                m21993oZ(((Integer) args[0]).intValue());
                return null;
            case 4:
                return bXk();
            case 5:
                m21990m((PlayerSpeech) args[0]);
                return null;
            case 6:
                m21991o((PlayerSpeech) args[0]);
                return null;
            case 7:
                return abw();
            case 8:
                m21981T((Asset) args[0]);
                return null;
            case 9:
                return new Boolean(bXm());
            case 10:
                m21988en(((Boolean) args[0]).booleanValue());
                return null;
            case 11:
                return new Boolean(bXo());
            case 12:
                m21989ep(((Boolean) args[0]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    public Asset abx() {
        switch (bFf().mo6893i(bfV)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, bfV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfV, new Object[0]));
                break;
        }
        return abw();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "PlayerSpeeches")
    public List<PlayerSpeech> bXl() {
        switch (bFf().mo6893i(fEt)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fEt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fEt, new Object[0]));
                break;
        }
        return bXk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Popup on dock")
    public boolean bXn() {
        switch (bFf().mo6893i(fEw)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fEw, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fEw, new Object[0]));
                break;
        }
        return bXm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unclosable window")
    public boolean bXp() {
        switch (bFf().mo6893i(fEy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fEy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fEy, new Object[0]));
                break;
        }
        return bXo();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public int compareTo(Object obj) {
        switch (bFf().mo6893i(dTl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dTl, new Object[]{obj}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTl, new Object[]{obj}));
                break;
        }
        return m21983W(obj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Popup on dock")
    @C5566aOg
    /* renamed from: eo */
    public void mo13354eo(boolean z) {
        switch (bFf().mo6893i(fEx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fEx, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fEx, new Object[]{new Boolean(z)}));
                break;
        }
        m21988en(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unclosable window")
    @C5566aOg
    /* renamed from: eq */
    public void mo13355eq(boolean z) {
        switch (bFf().mo6893i(fEz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fEz, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fEz, new Object[]{new Boolean(z)}));
                break;
        }
        m21989ep(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Priority")
    public int getPriority() {
        switch (bFf().mo6893i(eyO)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eyO, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eyO, new Object[0]));
                break;
        }
        return bDT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Priority")
    public void setPriority(int i) {
        switch (bFf().mo6893i(eyN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyN, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyN, new Object[]{new Integer(i)}));
                break;
        }
        m21993oZ(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "PlayerSpeeches")
    /* renamed from: n */
    public void mo13357n(PlayerSpeech av) {
        switch (bFf().mo6893i(fEu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fEu, new Object[]{av}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fEu, new Object[]{av}));
                break;
        }
        m21990m(av);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "PlayerSpeeches")
    /* renamed from: p */
    public void mo13358p(PlayerSpeech av) {
        switch (bFf().mo6893i(fEv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fEv, new Object[]{av}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fEv, new Object[]{av}));
                break;
        }
        m21991o(av);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m21984au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "81c5dea238683c4b0e55704a08c33ef6", aum = 0)
    /* renamed from: au */
    private String m21984au() {
        return "P[" + hYY.format((long) bDJ()) + "] " + getHandle();
    }

    @C0064Am(aul = "4770b7deb0ee37826ef9403829ab0988", aum = 0)
    /* renamed from: W */
    private int m21983W(Object obj) {
        if (!(obj instanceof NPCSpeech)) {
            return -1;
        }
        return Integer.valueOf(getPriority()).compareTo(Integer.valueOf(((NPCSpeech) obj).getPriority()));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Priority")
    @C0064Am(aul = "0caf147beaa90b4f07af4fa15d38a846", aum = 0)
    private int bDT() {
        return bDJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Priority")
    @C0064Am(aul = "cfd5b7ff134508a2702c86b04f4774b0", aum = 0)
    /* renamed from: oZ */
    private void m21993oZ(int i) {
        m21992oY(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "PlayerSpeeches")
    @C0064Am(aul = "7a21fdefc9e9d8c38758f9f030691f5b", aum = 0)
    private List<PlayerSpeech> bXk() {
        return Collections.unmodifiableList(bXh());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "PlayerSpeeches")
    @C0064Am(aul = "8aeeac8b561b4ecd1780e75c850d30d7", aum = 0)
    /* renamed from: m */
    private void m21990m(PlayerSpeech av) {
        bXh().add(av);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "PlayerSpeeches")
    @C0064Am(aul = "c1b61bb0903ce3bcfaf580e9b2ea465b", aum = 0)
    /* renamed from: o */
    private void m21991o(PlayerSpeech av) {
        bXh().remove(av);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    @C0064Am(aul = "73ec4db4e160e3f3acaa524ddaade10a", aum = 0)
    private Asset abw() {
        return abq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Popup on dock")
    @C0064Am(aul = "758fc529a93c3164c2287f7c8900fcb8", aum = 0)
    private boolean bXm() {
        return bXi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unclosable window")
    @C0064Am(aul = "d20576e62dfd669f9686efa1a76ee468", aum = 0)
    private boolean bXo() {
        return bXj();
    }
}
