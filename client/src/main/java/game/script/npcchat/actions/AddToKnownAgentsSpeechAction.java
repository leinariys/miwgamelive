package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.agent.Agent;
import game.script.npc.NPC;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.ayG;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.XW */
/* compiled from: a */
public class AddToKnownAgentsSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f2118CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eKM = null;
    public static final C5663aRz eKO = null;
    public static final C2491fm eKP = null;
    public static final C2491fm eKQ = null;
    public static final C2491fm eKR = null;
    public static final C2491fm eKS = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b96adaf3fe77f56bfc0d79f7a3896170", aum = 0)
    @Deprecated
    private static NPC eKL;
    @C0064Am(aul = "cbe888eca4be4ff2a76f2f6b9ef29636", aum = 1)
    private static Agent eKN;

    static {
        m11524V();
    }

    public AddToKnownAgentsSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AddToKnownAgentsSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11524V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 2;
        _m_methodCount = SpeechAction._m_methodCount + 5;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(AddToKnownAgentsSpeechAction.class, "b96adaf3fe77f56bfc0d79f7a3896170", i);
        eKM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AddToKnownAgentsSpeechAction.class, "cbe888eca4be4ff2a76f2f6b9ef29636", i2);
        eKO = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i4 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(AddToKnownAgentsSpeechAction.class, "2827e278ccf7640a5bdcaca02f360210", i4);
        eKP = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(AddToKnownAgentsSpeechAction.class, "a861ca472d2ebf12e01b1436949f2e56", i5);
        eKQ = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(AddToKnownAgentsSpeechAction.class, "5b9763e04dc6f76db7c41c71e9110482", i6);
        eKR = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(AddToKnownAgentsSpeechAction.class, "8fec9417e724cac36af6af73c0a0a487", i7);
        eKS = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(AddToKnownAgentsSpeechAction.class, "c0feae9764250ce5c71621b3b34026d7", i8);
        f2118CT = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AddToKnownAgentsSpeechAction.class, ayG.class, _m_fields, _m_methods);
    }

    private NPC bHh() {
        return (NPC) bFf().mo5608dq().mo3214p(eKM);
    }

    private Agent bHi() {
        return (Agent) bFf().mo5608dq().mo3214p(eKO);
    }

    /* renamed from: f */
    private void m11526f(Agent abk) {
        bFf().mo5608dq().mo3197f(eKO, abk);
    }

    /* renamed from: g */
    private void m11527g(NPC auf) {
        bFf().mo5608dq().mo3197f(eKM, auf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Known Agent")
    @C0064Am(aul = "8fec9417e724cac36af6af73c0a0a487", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m11528g(Agent abk) {
        throw new aWi(new aCE(this, eKS, new Object[]{abk}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Known Agent (old)")
    @C0064Am(aul = "a861ca472d2ebf12e01b1436949f2e56", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m11529h(NPC auf) {
        throw new aWi(new aCE(this, eKQ, new Object[]{auf}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new ayG(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return bHj();
            case 1:
                m11529h((NPC) args[0]);
                return null;
            case 2:
                return bHl();
            case 3:
                m11528g((Agent) args[0]);
                return null;
            case 4:
                m11525c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Known Agent (old)")
    public NPC bHk() {
        switch (bFf().mo6893i(eKP)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, eKP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eKP, new Object[0]));
                break;
        }
        return bHj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Known Agent")
    public Agent bHm() {
        switch (bFf().mo6893i(eKR)) {
            case 0:
                return null;
            case 2:
                return (Agent) bFf().mo5606d(new aCE(this, eKR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eKR, new Object[0]));
                break;
        }
        return bHl();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f2118CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2118CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2118CT, new Object[]{aiw}));
                break;
        }
        m11525c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Known Agent")
    @C5566aOg
    /* renamed from: h */
    public void mo6746h(Agent abk) {
        switch (bFf().mo6893i(eKS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eKS, new Object[]{abk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eKS, new Object[]{abk}));
                break;
        }
        m11528g(abk);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Known Agent (old)")
    @C5566aOg
    /* renamed from: i */
    public void mo6747i(NPC auf) {
        switch (bFf().mo6893i(eKQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eKQ, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eKQ, new Object[]{auf}));
                break;
        }
        m11529h(auf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Known Agent (old)")
    @C0064Am(aul = "2827e278ccf7640a5bdcaca02f360210", aum = 0)
    private NPC bHj() {
        return bHh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Known Agent")
    @C0064Am(aul = "5b9763e04dc6f76db7c41c71e9110482", aum = 0)
    private Agent bHl() {
        return bHi();
    }

    @C0064Am(aul = "c0feae9764250ce5c71621b3b34026d7", aum = 0)
    /* renamed from: c */
    private void m11525c(C5426aIw aiw) {
        aiw.mo9430x(bHi());
    }
}
