package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1460VU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ew */
/* compiled from: a */
public class SetParamSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CR */
    public static final C5663aRz f7096CR = null;

    /* renamed from: CS */
    public static final C5663aRz f7097CS = null;

    /* renamed from: CT */
    public static final C2491fm f7098CT = null;

    /* renamed from: CU */
    public static final C2491fm f7099CU = null;

    /* renamed from: CV */
    public static final C2491fm f7100CV = null;

    /* renamed from: CW */
    public static final C2491fm f7101CW = null;

    /* renamed from: CX */
    public static final C2491fm f7102CX = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8153c0de1705ca0b9541870d1bd43005", aum = 0)
    private static String key;
    @C0064Am(aul = "36c98b541a2ca80bfbf386380ed17545", aum = 1)
    private static String value;

    static {
        m30128V();
    }

    public SetParamSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SetParamSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m30128V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 2;
        _m_methodCount = SpeechAction._m_methodCount + 5;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(SetParamSpeechAction.class, "8153c0de1705ca0b9541870d1bd43005", i);
        f7096CR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SetParamSpeechAction.class, "36c98b541a2ca80bfbf386380ed17545", i2);
        f7097CS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i4 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(SetParamSpeechAction.class, "8f74e2c90594287c156e73b1731f2afa", i4);
        f7098CT = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(SetParamSpeechAction.class, "142cb60d270c431a6afa6c1c8c4dca15", i5);
        f7099CU = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(SetParamSpeechAction.class, "13e6b27dd8eb693c998a994abbdc35b0", i6);
        f7100CV = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(SetParamSpeechAction.class, "2a0d797ffffe6af4e8243ef378ca1861", i7);
        f7101CW = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(SetParamSpeechAction.class, "8ea819aceeabffa79e6d0afb36adca82", i8);
        f7102CX = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SetParamSpeechAction.class, C1460VU.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m30127U(String str) {
        bFf().mo5608dq().mo3197f(f7096CR, str);
    }

    /* renamed from: V */
    private void m30129V(String str) {
        bFf().mo5608dq().mo3197f(f7097CS, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C0064Am(aul = "13e6b27dd8eb693c998a994abbdc35b0", aum = 0)
    @C5566aOg
    /* renamed from: W */
    private void m30130W(String str) {
        throw new aWi(new aCE(this, f7100CV, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C0064Am(aul = "8ea819aceeabffa79e6d0afb36adca82", aum = 0)
    @C5566aOg
    /* renamed from: X */
    private void m30131X(String str) {
        throw new aWi(new aCE(this, f7102CX, new Object[]{str}));
    }

    /* renamed from: lM */
    private String m30133lM() {
        return (String) bFf().mo5608dq().mo3214p(f7096CR);
    }

    /* renamed from: lN */
    private String m30134lN() {
        return (String) bFf().mo5608dq().mo3214p(f7097CS);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1460VU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                m30132c((C5426aIw) args[0]);
                return null;
            case 1:
                return m30135lO();
            case 2:
                m30130W((String) args[0]);
                return null;
            case 3:
                return m30136lP();
            case 4:
                m30131X((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f7098CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7098CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7098CT, new Object[]{aiw}));
                break;
        }
        m30132c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    public String getKey() {
        switch (bFf().mo6893i(f7099CU)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7099CU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7099CU, new Object[0]));
                break;
        }
        return m30135lO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C5566aOg
    public void setKey(String str) {
        switch (bFf().mo6893i(f7100CV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7100CV, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7100CV, new Object[]{str}));
                break;
        }
        m30130W(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    public String getValue() {
        switch (bFf().mo6893i(f7101CW)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7101CW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7101CW, new Object[0]));
                break;
        }
        return m30136lP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C5566aOg
    public void setValue(String str) {
        switch (bFf().mo6893i(f7102CX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7102CX, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7102CX, new Object[]{str}));
                break;
        }
        m30131X(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "8f74e2c90594287c156e73b1731f2afa", aum = 0)
    /* renamed from: c */
    private void m30132c(C5426aIw aiw) {
        aiw.setParameter(m30133lM(), m30134lN());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    @C0064Am(aul = "142cb60d270c431a6afa6c1c8c4dca15", aum = 0)
    /* renamed from: lO */
    private String m30135lO() {
        return m30133lM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    @C0064Am(aul = "2a0d797ffffe6af4e8243ef378ca1861", aum = 0)
    /* renamed from: lP */
    private String m30136lP() {
        return m30134lN();
    }
}
