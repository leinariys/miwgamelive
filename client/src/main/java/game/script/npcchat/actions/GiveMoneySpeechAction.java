package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6954awt;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.auY  reason: case insensitive filesystem */
/* compiled from: a */
public class GiveMoneySpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f5399CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm axj = null;
    public static final C2491fm axk = null;
    /* renamed from: cz */
    public static final C5663aRz f5400cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fc054baee1879ae441c2b190233e8265", aum = 0)
    private static long axi;

    static {
        m26337V();
    }

    public GiveMoneySpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GiveMoneySpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26337V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 3;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(GiveMoneySpeechAction.class, "fc054baee1879ae441c2b190233e8265", i);
        f5400cz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(GiveMoneySpeechAction.class, "36aa9a01689e9894d0f709a35e15dc6d", i3);
        axj = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(GiveMoneySpeechAction.class, "d535876a9e293966a455f4ed238ceac2", i4);
        axk = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(GiveMoneySpeechAction.class, "d277c2f408aa12331a2905b930e6844b", i5);
        f5399CT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GiveMoneySpeechAction.class, C6954awt.class, _m_fields, _m_methods);
    }

    /* renamed from: LS */
    private long m26335LS() {
        return bFf().mo5608dq().mo3213o(f5400cz);
    }

    /* renamed from: cj */
    private void m26339cj(long j) {
        bFf().mo5608dq().mo3184b(f5400cz, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Money Amount")
    @C0064Am(aul = "d535876a9e293966a455f4ed238ceac2", aum = 0)
    @C5566aOg
    /* renamed from: ck */
    private void m26340ck(long j) {
        throw new aWi(new aCE(this, axk, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Money Amount")
    /* renamed from: LU */
    public long mo16359LU() {
        switch (bFf().mo6893i(axj)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, axj, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, axj, new Object[0]));
                break;
        }
        return m26336LT();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6954awt(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return new Long(m26336LT());
            case 1:
                m26340ck(((Long) args[0]).longValue());
                return null;
            case 2:
                m26338c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Money Amount")
    @C5566aOg
    /* renamed from: cl */
    public void mo16360cl(long j) {
        switch (bFf().mo6893i(axk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, axk, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, axk, new Object[]{new Long(j)}));
                break;
        }
        m26340ck(j);
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f5399CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5399CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5399CT, new Object[]{aiw}));
                break;
        }
        m26338c(aiw);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Money Amount")
    @C0064Am(aul = "36aa9a01689e9894d0f709a35e15dc6d", aum = 0)
    /* renamed from: LT */
    private long m26336LT() {
        return m26335LS();
    }

    @C0064Am(aul = "d277c2f408aa12331a2905b930e6844b", aum = 0)
    /* renamed from: c */
    private void m26338c(C5426aIw aiw) {
        aiw.mo9422kz(m26335LS());
        mo23381b(((NLSSpeechActions) ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEE(), Long.valueOf(m26335LS()));
    }
}
