package game.script.npcchat.actions;

import game.network.message.externalizable.C3131oI;
import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechAction;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0578I;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.We */
/* compiled from: a */
public class NurseryTutorialSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f2041CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ega = null;
    public static final C2491fm egb = null;
    public static final C2491fm ewZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e33081d93d2d04d055dd929ce00a56d1", aum = 0)

    /* renamed from: dB */
    private static C3131oI.C3132a f2042dB;

    static {
        m11276V();
    }

    public NurseryTutorialSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NurseryTutorialSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11276V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 3;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(NurseryTutorialSpeechAction.class, "e33081d93d2d04d055dd929ce00a56d1", i);
        ega = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(NurseryTutorialSpeechAction.class, "7b7bb56e9426d2b54b3d62fdfe4bfc55", i3);
        egb = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(NurseryTutorialSpeechAction.class, "c3519bf3fd5d3a1d30afc382200c358e", i4);
        ewZ = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(NurseryTutorialSpeechAction.class, "f943fdfeba97f5581bc05420d1e2ad1e", i5);
        f2041CT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NurseryTutorialSpeechAction.class, C0578I.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m11277a(C3131oI.C3132a aVar) {
        bFf().mo5608dq().mo3197f(ega, aVar);
    }

    private C3131oI.C3132a buC() {
        return (C3131oI.C3132a) bFf().mo5608dq().mo3214p(ega);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutorial transition")
    @C0064Am(aul = "c3519bf3fd5d3a1d30afc382200c358e", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m11279d(C3131oI.C3132a aVar) {
        throw new aWi(new aCE(this, ewZ, new Object[]{aVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutorial transition")
    /* renamed from: Us */
    public C3131oI.C3132a mo6605Us() {
        switch (bFf().mo6893i(egb)) {
            case 0:
                return null;
            case 2:
                return (C3131oI.C3132a) bFf().mo5606d(new aCE(this, egb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, egb, new Object[0]));
                break;
        }
        return buD();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0578I(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return buD();
            case 1:
                m11279d((C3131oI.C3132a) args[0]);
                return null;
            case 2:
                m11278c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f2041CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2041CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2041CT, new Object[]{aiw}));
                break;
        }
        m11278c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutorial transition")
    @C5566aOg
    /* renamed from: e */
    public void mo6606e(C3131oI.C3132a aVar) {
        switch (bFf().mo6893i(ewZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ewZ, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ewZ, new Object[]{aVar}));
                break;
        }
        m11279d(aVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutorial transition")
    @C0064Am(aul = "7b7bb56e9426d2b54b3d62fdfe4bfc55", aum = 0)
    private C3131oI.C3132a buD() {
        return buC();
    }

    @C0064Am(aul = "f943fdfeba97f5581bc05420d1e2ad1e", aum = 0)
    /* renamed from: c */
    private void m11278c(C5426aIw aiw) {
        aPA().mo14419f((C1506WA) new C3131oI(buC()));
    }
}
