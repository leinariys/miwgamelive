package game.script.npcchat.actions;

import game.network.message.externalizable.C5543aNj;
import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechAction;
import game.script.resource.Asset;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6936awb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.rI */
/* compiled from: a */
public class RecurrentMessageRequestSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f8976CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bca = null;
    public static final C5663aRz bcc = null;
    public static final C5663aRz bce = null;
    public static final C2491fm bcf = null;
    public static final C2491fm bcg = null;
    public static final C2491fm bch = null;
    public static final C2491fm bci = null;
    public static final C2491fm bcj = null;
    public static final C2491fm bck = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "788ec08aea79579d633abe0e991c500f", aum = 0)
    private static String bbZ;
    @C0064Am(aul = "affb4d09f2fafd3a55c758ab17d80224", aum = 1)
    private static I18NString bcb;
    @C0064Am(aul = "4149d681acb7fb1c9152a01dc96b278d", aum = 2)
    private static Asset bcd;

    static {
        m38027V();
    }

    public RecurrentMessageRequestSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RecurrentMessageRequestSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38027V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 3;
        _m_methodCount = SpeechAction._m_methodCount + 7;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(RecurrentMessageRequestSpeechAction.class, "788ec08aea79579d633abe0e991c500f", i);
        bca = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(RecurrentMessageRequestSpeechAction.class, "affb4d09f2fafd3a55c758ab17d80224", i2);
        bcc = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(RecurrentMessageRequestSpeechAction.class, "4149d681acb7fb1c9152a01dc96b278d", i3);
        bce = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i5 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(RecurrentMessageRequestSpeechAction.class, "9f56880e43ae5bbe59e4e1aaa478cdad", i5);
        bcf = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(RecurrentMessageRequestSpeechAction.class, "4f9c62e542c8dad27bafa6a60044fec5", i6);
        bcg = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(RecurrentMessageRequestSpeechAction.class, "805ad723036fe325708a81e026377ef6", i7);
        bch = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(RecurrentMessageRequestSpeechAction.class, "86d1bdcd91634317359b943708ba9ee7", i8);
        bci = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(RecurrentMessageRequestSpeechAction.class, "10b390b1e93cb8b0677c9da0d21d537d", i9);
        bcj = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(RecurrentMessageRequestSpeechAction.class, "fd1eb534bec3371c9c877731aafb7dca", i10);
        bck = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(RecurrentMessageRequestSpeechAction.class, "1baabc732268c463ce46560322b6908e", i11);
        f8976CT = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RecurrentMessageRequestSpeechAction.class, C6936awb.class, _m_fields, _m_methods);
    }

    /* renamed from: P */
    private void m38025P(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bce, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Background Image")
    @C0064Am(aul = "fd1eb534bec3371c9c877731aafb7dca", aum = 0)
    @C5566aOg
    /* renamed from: Q */
    private void m38026Q(Asset tCVar) {
        throw new aWi(new aCE(this, bck, new Object[]{tCVar}));
    }

    /* renamed from: YL */
    private String m38028YL() {
        return (String) bFf().mo5608dq().mo3214p(bca);
    }

    /* renamed from: YM */
    private I18NString m38029YM() {
        return (I18NString) bFf().mo5608dq().mo3214p(bcc);
    }

    /* renamed from: YN */
    private Asset m38030YN() {
        return (Asset) bFf().mo5608dq().mo3214p(bce);
    }

    /* renamed from: bh */
    private void m38034bh(String str) {
        bFf().mo5608dq().mo3197f(bca, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C0064Am(aul = "4f9c62e542c8dad27bafa6a60044fec5", aum = 0)
    @C5566aOg
    /* renamed from: bi */
    private void m38035bi(String str) {
        throw new aWi(new aCE(this, bcg, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Header text")
    @C0064Am(aul = "86d1bdcd91634317359b943708ba9ee7", aum = 0)
    @C5566aOg
    /* renamed from: eA */
    private void m38037eA(I18NString i18NString) {
        throw new aWi(new aCE(this, bci, new Object[]{i18NString}));
    }

    /* renamed from: ez */
    private void m38038ez(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(bcc, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Background Image")
    @C5566aOg
    /* renamed from: R */
    public void mo21576R(Asset tCVar) {
        switch (bFf().mo6893i(bck)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bck, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bck, new Object[]{tCVar}));
                break;
        }
        m38026Q(tCVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6936awb(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    /* renamed from: YP */
    public String mo21577YP() {
        switch (bFf().mo6893i(bcf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bcf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcf, new Object[0]));
                break;
        }
        return m38031YO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Header text")
    /* renamed from: YR */
    public I18NString mo21578YR() {
        switch (bFf().mo6893i(bch)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, bch, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bch, new Object[0]));
                break;
        }
        return m38032YQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Background Image")
    /* renamed from: YT */
    public Asset mo21579YT() {
        switch (bFf().mo6893i(bcj)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, bcj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcj, new Object[0]));
                break;
        }
        return m38033YS();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return m38031YO();
            case 1:
                m38035bi((String) args[0]);
                return null;
            case 2:
                return m38032YQ();
            case 3:
                m38037eA((I18NString) args[0]);
                return null;
            case 4:
                return m38033YS();
            case 5:
                m38026Q((Asset) args[0]);
                return null;
            case 6:
                m38036c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C5566aOg
    /* renamed from: bj */
    public void mo21580bj(String str) {
        switch (bFf().mo6893i(bcg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                break;
        }
        m38035bi(str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f8976CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8976CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8976CT, new Object[]{aiw}));
                break;
        }
        m38036c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Header text")
    @C5566aOg
    /* renamed from: eB */
    public void mo21581eB(I18NString i18NString) {
        switch (bFf().mo6893i(bci)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bci, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bci, new Object[]{i18NString}));
                break;
        }
        m38037eA(i18NString);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    @C0064Am(aul = "9f56880e43ae5bbe59e4e1aaa478cdad", aum = 0)
    /* renamed from: YO */
    private String m38031YO() {
        return m38028YL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Header text")
    @C0064Am(aul = "805ad723036fe325708a81e026377ef6", aum = 0)
    /* renamed from: YQ */
    private I18NString m38032YQ() {
        return m38029YM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Background Image")
    @C0064Am(aul = "10b390b1e93cb8b0677c9da0d21d537d", aum = 0)
    /* renamed from: YS */
    private Asset m38033YS() {
        return m38030YN();
    }

    @C0064Am(aul = "1baabc732268c463ce46560322b6908e", aum = 0)
    /* renamed from: c */
    private void m38036c(C5426aIw aiw) {
        aPA().mo14419f((C1506WA) new C5543aNj(m38028YL(), m38029YM(), m38030YN()));
    }
}
