package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5348aFw;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.vZ */
/* compiled from: a */
public class PostNPCChatEventSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f9416CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bBB = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m40284V();
    }

    public PostNPCChatEventSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PostNPCChatEventSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40284V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 0;
        _m_methodCount = SpeechAction._m_methodCount + 2;
        _m_fields = new C5663aRz[(SpeechAction._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(PostNPCChatEventSpeechAction.class, "2bdefcf7eb66cfc3854bcaed09652f97", i);
        bBB = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(PostNPCChatEventSpeechAction.class, "a83e63932f51cc6d64132acdeb118cea", i2);
        f9416CT = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PostNPCChatEventSpeechAction.class, C5348aFw.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "2bdefcf7eb66cfc3854bcaed09652f97", aum = 0)
    @C5566aOg
    private void ali() {
        throw new aWi(new aCE(this, bBB, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5348aFw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                ali();
                return null;
            case 1:
                m40285c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public final void alj() {
        switch (bFf().mo6893i(bBB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bBB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bBB, new Object[0]));
                break;
        }
        ali();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f9416CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9416CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9416CT, new Object[]{aiw}));
                break;
        }
        m40285c(aiw);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a83e63932f51cc6d64132acdeb118cea", aum = 0)
    /* renamed from: c */
    private void m40285c(C5426aIw aiw) {
        alj();
    }
}
