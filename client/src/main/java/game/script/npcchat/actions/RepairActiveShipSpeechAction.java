package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6798atq;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.gK */
/* compiled from: a */
public class RepairActiveShipSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f7560CT = null;
    /* renamed from: Rb */
    public static final C5663aRz f7562Rb = null;
    /* renamed from: Rc */
    public static final C2491fm f7563Rc = null;
    /* renamed from: Rd */
    public static final C2491fm f7564Rd = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "25588bae7cca6de3ebdb7e708003910e", aum = 0)

    /* renamed from: Ra */
    private static long f7561Ra;

    static {
        m31898V();
    }

    public RepairActiveShipSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RepairActiveShipSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m31898V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 3;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(RepairActiveShipSpeechAction.class, "25588bae7cca6de3ebdb7e708003910e", i);
        f7562Rb = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(RepairActiveShipSpeechAction.class, "05e2c082ae06c38faaf872d6c9087ee6", i3);
        f7560CT = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(RepairActiveShipSpeechAction.class, "558f17fbe2019228bf7f840e16fae6db", i4);
        f7563Rc = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(RepairActiveShipSpeechAction.class, "e679a48a86f5e584bc958c67f8b63346", i5);
        f7564Rd = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RepairActiveShipSpeechAction.class, C6798atq.class, _m_fields, _m_methods);
    }

    /* renamed from: bg */
    private void m31899bg(long j) {
        bFf().mo5608dq().mo3184b(f7562Rb, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Repair Price")
    @C0064Am(aul = "e679a48a86f5e584bc958c67f8b63346", aum = 0)
    @C5566aOg
    /* renamed from: bh */
    private void m31900bh(long j) {
        throw new aWi(new aCE(this, f7564Rd, new Object[]{new Long(j)}));
    }

    /* renamed from: wh */
    private long m31902wh() {
        return bFf().mo5608dq().mo3213o(f7562Rb);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6798atq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                m31901c((C5426aIw) args[0]);
                return null;
            case 1:
                return new Long(m31903wi());
            case 2:
                m31900bh(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Repair Price")
    @C5566aOg
    /* renamed from: bi */
    public void mo18946bi(long j) {
        switch (bFf().mo6893i(f7564Rd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7564Rd, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7564Rd, new Object[]{new Long(j)}));
                break;
        }
        m31900bh(j);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f7560CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7560CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7560CT, new Object[]{aiw}));
                break;
        }
        m31901c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Repair Price")
    /* renamed from: wj */
    public long mo18947wj() {
        switch (bFf().mo6893i(f7563Rc)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7563Rc, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7563Rc, new Object[0]));
                break;
        }
        return m31903wi();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "05e2c082ae06c38faaf872d6c9087ee6", aum = 0)
    /* renamed from: c */
    private void m31901c(C5426aIw aiw) {
        if (aiw.cWT()) {
            aiw.mo9416eb(m31902wh());
            mo23381b(((NLSSpeechActions) ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEI(), Long.valueOf(m31902wh()));
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Repair Price")
    @C0064Am(aul = "558f17fbe2019228bf7f840e16fae6db", aum = 0)
    /* renamed from: wi */
    private long m31903wi() {
        return m31902wh();
    }
}
