package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aIW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.azN */
/* compiled from: a */
public class GiveOniMemorySpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f5674CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: cA */
    public static final C2491fm f5675cA = null;
    /* renamed from: cB */
    public static final C2491fm f5676cB = null;
    /* renamed from: cz */
    public static final C5663aRz f5678cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f4922cb0cae536ab96f9ad0aec276bc6", aum = 0)

    /* renamed from: cy */
    private static int f5677cy;

    static {
        m27642V();
    }

    public GiveOniMemorySpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GiveOniMemorySpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27642V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 3;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(GiveOniMemorySpeechAction.class, "f4922cb0cae536ab96f9ad0aec276bc6", i);
        f5678cz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(GiveOniMemorySpeechAction.class, "cd22189ce8481dd8e6c326480103aca0", i3);
        f5674CT = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(GiveOniMemorySpeechAction.class, "5a1841de6e432066ed1cd4943b902b5c", i4);
        f5675cA = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(GiveOniMemorySpeechAction.class, "933fe53546f355a52a7898a22722c222", i5);
        f5676cB = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GiveOniMemorySpeechAction.class, aIW.class, _m_fields, _m_methods);
    }

    /* renamed from: aw */
    private int m27643aw() {
        return bFf().mo5608dq().mo3212n(f5678cz);
    }

    /* renamed from: f */
    private void m27646f(int i) {
        bFf().mo5608dq().mo3183b(f5678cz, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aIW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                m27645c((C5426aIw) args[0]);
                return null;
            case 1:
                return new Integer(m27644ax());
            case 2:
                m27647g(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f5674CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5674CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5674CT, new Object[]{aiw}));
                break;
        }
        m27645c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f5675cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f5675cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5675cA, new Object[0]));
                break;
        }
        return m27644ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    public void setAmount(int i) {
        switch (bFf().mo6893i(f5676cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5676cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5676cB, new Object[]{new Integer(i)}));
                break;
        }
        m27647g(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "cd22189ce8481dd8e6c326480103aca0", aum = 0)
    /* renamed from: c */
    private void m27645c(C5426aIw aiw) {
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "5a1841de6e432066ed1cd4943b902b5c", aum = 0)
    /* renamed from: ax */
    private int m27644ax() {
        return m27643aw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "933fe53546f355a52a7898a22722c222", aum = 0)
    /* renamed from: g */
    private void m27647g(int i) {
        m27646f(i);
    }
}
