package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0477Gc;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.air  reason: case insensitive filesystem */
/* compiled from: a */
public class TakeItemSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f4667CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz fNj = null;
    public static final C5663aRz fNk = null;
    public static final C2491fm fNl = null;
    public static final C2491fm fNm = null;
    public static final C2491fm fNn = null;
    public static final C2491fm fNo = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "61db8cd4f54f209441a9205d43413b82", aum = 0)
    private static ItemType cXt;
    @C0064Am(aul = "8d7d2f88116c8ddcbbb3eab40cf09146", aum = 1)
    private static int cXu;

    static {
        m22680V();
    }

    public TakeItemSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TakeItemSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22680V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 2;
        _m_methodCount = SpeechAction._m_methodCount + 5;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(TakeItemSpeechAction.class, "61db8cd4f54f209441a9205d43413b82", i);
        fNj = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TakeItemSpeechAction.class, "8d7d2f88116c8ddcbbb3eab40cf09146", i2);
        fNk = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i4 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(TakeItemSpeechAction.class, "82d5ab38c0901881e05d2a99631108f1", i4);
        fNl = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(TakeItemSpeechAction.class, "7effcfe2438b5ce85e8d8a0f9e841d10", i5);
        fNm = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(TakeItemSpeechAction.class, "ceeeac4240ec615eb0b92d1099d4fb6c", i6);
        fNn = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(TakeItemSpeechAction.class, "51fbd1d1d0fafe586971cfcd16c77e7f", i7);
        fNo = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(TakeItemSpeechAction.class, "0844b47fb1c3edf07330a8b2ef0493db", i8);
        f4667CT = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TakeItemSpeechAction.class, C0477Gc.class, _m_fields, _m_methods);
    }

    /* renamed from: Q */
    private void m22678Q(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(fNj, jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delivery Item Type")
    @C0064Am(aul = "7effcfe2438b5ce85e8d8a0f9e841d10", aum = 0)
    @C5566aOg
    /* renamed from: R */
    private void m22679R(ItemType jCVar) {
        throw new aWi(new aCE(this, fNm, new Object[]{jCVar}));
    }

    private ItemType cbR() {
        return (ItemType) bFf().mo5608dq().mo3214p(fNj);
    }

    private int cbS() {
        return bFf().mo5608dq().mo3212n(fNk);
    }

    /* renamed from: rc */
    private void m22682rc(int i) {
        bFf().mo5608dq().mo3183b(fNk, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delivery Quantity")
    @C0064Am(aul = "51fbd1d1d0fafe586971cfcd16c77e7f", aum = 0)
    @C5566aOg
    /* renamed from: rd */
    private void m22683rd(int i) {
        throw new aWi(new aCE(this, fNo, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delivery Item Type")
    @C5566aOg
    /* renamed from: S */
    public void mo13819S(ItemType jCVar) {
        switch (bFf().mo6893i(fNm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fNm, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fNm, new Object[]{jCVar}));
                break;
        }
        m22679R(jCVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0477Gc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return cbT();
            case 1:
                m22679R((ItemType) args[0]);
                return null;
            case 2:
                return new Integer(cbV());
            case 3:
                m22683rd(((Integer) args[0]).intValue());
                return null;
            case 4:
                m22681c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delivery Item Type")
    public ItemType cbU() {
        switch (bFf().mo6893i(fNl)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, fNl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fNl, new Object[0]));
                break;
        }
        return cbT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delivery Quantity")
    public int cbW() {
        switch (bFf().mo6893i(fNn)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fNn, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fNn, new Object[0]));
                break;
        }
        return cbV();
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f4667CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4667CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4667CT, new Object[]{aiw}));
                break;
        }
        m22681c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delivery Quantity")
    @C5566aOg
    /* renamed from: re */
    public void mo13822re(int i) {
        switch (bFf().mo6893i(fNo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fNo, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fNo, new Object[]{new Integer(i)}));
                break;
        }
        m22683rd(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m22682rc(1);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delivery Item Type")
    @C0064Am(aul = "82d5ab38c0901881e05d2a99631108f1", aum = 0)
    private ItemType cbT() {
        return cbR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delivery Quantity")
    @C0064Am(aul = "ceeeac4240ec615eb0b92d1099d4fb6c", aum = 0)
    private int cbV() {
        return cbS();
    }

    @C0064Am(aul = "0844b47fb1c3edf07330a8b2ef0493db", aum = 0)
    /* renamed from: c */
    private void m22681c(C5426aIw aiw) {
        if (aiw.mo9417f(cbR(), cbS()) != 0) {
            mo23381b(((NLSSpeechActions) ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEy(), cbR().mo19891ke());
        }
    }
}
