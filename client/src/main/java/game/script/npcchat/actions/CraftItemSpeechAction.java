package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.item.BlueprintType;
import game.script.item.ItemType;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1145Qo;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.auc  reason: case insensitive filesystem */
/* compiled from: a */
public class CraftItemSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f5410CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gCI = null;
    public static final C2491fm gCJ = null;
    public static final C2491fm gCK = null;
    public static final C2491fm gCL = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a497271193044a70d7d1d6c133250b26", aum = 0)
    private static BlueprintType dVp;

    static {
        m26357V();
    }

    public CraftItemSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CraftItemSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26357V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 4;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(CraftItemSpeechAction.class, "a497271193044a70d7d1d6c133250b26", i);
        gCI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(CraftItemSpeechAction.class, "1da99cf3b788f67b00795a779a7c7ef3", i3);
        gCJ = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(CraftItemSpeechAction.class, "262e4a5858cd0c6eb9b9791ce5b924c6", i4);
        gCK = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(CraftItemSpeechAction.class, "604e0b9ce726d6f48192b150ec8a49a3", i5);
        gCL = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(CraftItemSpeechAction.class, "d62bace5bc810c18b6e91723a61d790b", i6);
        f5410CT = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CraftItemSpeechAction.class, C1145Qo.class, _m_fields, _m_methods);
    }

    private BlueprintType cwR() {
        return (BlueprintType) bFf().mo5608dq().mo3214p(gCI);
    }

    /* renamed from: f */
    private void m26359f(BlueprintType mr) {
        bFf().mo5608dq().mo3197f(gCI, mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Blueprint Type")
    @C0064Am(aul = "262e4a5858cd0c6eb9b9791ce5b924c6", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m26360g(BlueprintType mr) {
        throw new aWi(new aCE(this, gCK, new Object[]{mr}));
    }

    @C0064Am(aul = "604e0b9ce726d6f48192b150ec8a49a3", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m26361i(BlueprintType mr) {
        throw new aWi(new aCE(this, gCL, new Object[]{mr}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1145Qo(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return cwS();
            case 1:
                m26360g((BlueprintType) args[0]);
                return null;
            case 2:
                m26361i((BlueprintType) args[0]);
                return null;
            case 3:
                m26358c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Blueprint Type")
    public ItemType cwT() {
        switch (bFf().mo6893i(gCJ)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, gCJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gCJ, new Object[0]));
                break;
        }
        return cwS();
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f5410CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5410CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5410CT, new Object[]{aiw}));
                break;
        }
        m26358c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Blueprint Type")
    @C5566aOg
    /* renamed from: h */
    public void mo16364h(BlueprintType mr) {
        switch (bFf().mo6893i(gCK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gCK, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gCK, new Object[]{mr}));
                break;
        }
        m26360g(mr);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: j */
    public final void mo16365j(BlueprintType mr) {
        switch (bFf().mo6893i(gCL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gCL, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gCL, new Object[]{mr}));
                break;
        }
        m26361i(mr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Blueprint Type")
    @C0064Am(aul = "1da99cf3b788f67b00795a779a7c7ef3", aum = 0)
    private ItemType cwS() {
        return cwR();
    }

    @C0064Am(aul = "d62bace5bc810c18b6e91723a61d790b", aum = 0)
    /* renamed from: c */
    private void m26358c(C5426aIw aiw) {
        mo16365j(cwR());
    }
}
