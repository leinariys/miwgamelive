package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5446aJq;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aNc  reason: case insensitive filesystem */
/* compiled from: a */
public class GiveItemSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f3432CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMX = null;
    public static final C5663aRz aMZ = null;
    public static final C2491fm aNc = null;
    public static final C2491fm aNd = null;
    public static final C2491fm aNe = null;
    public static final C2491fm aNf = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2bc61e292a6530f85ca072d00fe076d9", aum = 0)
    private static ItemType aMW;
    @C0064Am(aul = "a210b5f3fe7eb2e9b713377366a691db", aum = 1)
    private static int aMY;

    static {
        m16717V();
    }

    public GiveItemSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GiveItemSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16717V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 2;
        _m_methodCount = SpeechAction._m_methodCount + 5;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(GiveItemSpeechAction.class, "2bc61e292a6530f85ca072d00fe076d9", i);
        aMX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(GiveItemSpeechAction.class, "a210b5f3fe7eb2e9b713377366a691db", i2);
        aMZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i4 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(GiveItemSpeechAction.class, "e34472064413fb96972b4ba00cea7bdc", i4);
        aNc = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(GiveItemSpeechAction.class, "299d47579fbe30ff7d1c6cc69887cbcb", i5);
        aNd = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(GiveItemSpeechAction.class, "2e9eb1dc21c70dd6163644b81a1ebc8b", i6);
        aNe = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(GiveItemSpeechAction.class, "ebb1ccf84f6ce97d254d640635550292", i7);
        aNf = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(GiveItemSpeechAction.class, "ec9e2407a597c2c640302754e8a8ab8d", i8);
        f3432CT = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GiveItemSpeechAction.class, C5446aJq.class, _m_fields, _m_methods);
    }

    /* renamed from: Sr */
    private ItemType m16713Sr() {
        return (ItemType) bFf().mo5608dq().mo3214p(aMX);
    }

    /* renamed from: Ss */
    private int m16714Ss() {
        return bFf().mo5608dq().mo3212n(aMZ);
    }

    /* renamed from: ds */
    private void m16719ds(int i) {
        bFf().mo5608dq().mo3183b(aMZ, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Quantity")
    @C0064Am(aul = "ebb1ccf84f6ce97d254d640635550292", aum = 0)
    @C5566aOg
    /* renamed from: dt */
    private void m16720dt(int i) {
        throw new aWi(new aCE(this, aNf, new Object[]{new Integer(i)}));
    }

    /* renamed from: k */
    private void m16721k(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(aMX, jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Item Type")
    @C0064Am(aul = "299d47579fbe30ff7d1c6cc69887cbcb", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m16722l(ItemType jCVar) {
        throw new aWi(new aCE(this, aNd, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Item Type")
    /* renamed from: Sv */
    public ItemType mo10492Sv() {
        switch (bFf().mo6893i(aNc)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, aNc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aNc, new Object[0]));
                break;
        }
        return m16715Su();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Quantity")
    /* renamed from: Sx */
    public int mo10493Sx() {
        switch (bFf().mo6893i(aNe)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aNe, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aNe, new Object[0]));
                break;
        }
        return m16716Sw();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5446aJq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return m16715Su();
            case 1:
                m16722l((ItemType) args[0]);
                return null;
            case 2:
                return new Integer(m16716Sw());
            case 3:
                m16720dt(((Integer) args[0]).intValue());
                return null;
            case 4:
                m16718c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f3432CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3432CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3432CT, new Object[]{aiw}));
                break;
        }
        m16718c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Quantity")
    @C5566aOg
    /* renamed from: du */
    public void mo10494du(int i) {
        switch (bFf().mo6893i(aNf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aNf, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aNf, new Object[]{new Integer(i)}));
                break;
        }
        m16720dt(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Item Type")
    @C5566aOg
    /* renamed from: m */
    public void mo10495m(ItemType jCVar) {
        switch (bFf().mo6893i(aNd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aNd, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aNd, new Object[]{jCVar}));
                break;
        }
        m16722l(jCVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m16719ds(1);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Item Type")
    @C0064Am(aul = "e34472064413fb96972b4ba00cea7bdc", aum = 0)
    /* renamed from: Su */
    private ItemType m16715Su() {
        return m16713Sr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Quantity")
    @C0064Am(aul = "2e9eb1dc21c70dd6163644b81a1ebc8b", aum = 0)
    /* renamed from: Sw */
    private int m16716Sw() {
        return m16714Ss();
    }

    @C0064Am(aul = "ec9e2407a597c2c640302754e8a8ab8d", aum = 0)
    /* renamed from: c */
    private void m16718c(C5426aIw aiw) {
        if (!aiw.mo9406b(m16713Sr(), m16714Ss(), (Mission) null)) {
            throw new C3512ro("Error giving item", C3512ro.C3513a.GIVE_NO_ROOM);
        }
        mo23381b(((NLSSpeechActions) ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEA(), m16713Sr().mo19891ke());
    }
}
