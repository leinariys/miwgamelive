package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.mission.MissionTemplate;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3523rw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ZU */
/* compiled from: a */
public class OpenMissionBriefingSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f2228CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eRV = null;
    public static final C2491fm eRW = null;
    public static final C2491fm eRX = null;
    public static final C2491fm eRY = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "60b06195413edd46a490786b1c7f61a4", aum = 0)
    private static MissionTemplate baM;

    static {
        m12053V();
    }

    public OpenMissionBriefingSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OpenMissionBriefingSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12053V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 4;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(OpenMissionBriefingSpeechAction.class, "60b06195413edd46a490786b1c7f61a4", i);
        eRV = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(OpenMissionBriefingSpeechAction.class, "81cf60a68644899e03119421821afceb", i3);
        eRW = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(OpenMissionBriefingSpeechAction.class, "c83657e081d3106748dcb6d076e6d8ab", i4);
        eRX = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(OpenMissionBriefingSpeechAction.class, "943b3b9aba0fbe7b02f46cbffed9dea1", i5);
        eRY = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(OpenMissionBriefingSpeechAction.class, "45cdb3746443c8ba49e4282c55cbc23a", i6);
        f2228CT = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OpenMissionBriefingSpeechAction.class, C3523rw.class, _m_fields, _m_methods);
    }

    private MissionTemplate bJH() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(eRV);
    }

    /* renamed from: e */
    private void m12055e(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(eRV, avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Template")
    @C0064Am(aul = "c83657e081d3106748dcb6d076e6d8ab", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m12056f(MissionTemplate avh) {
        throw new aWi(new aCE(this, eRX, new Object[]{avh}));
    }

    @C0064Am(aul = "943b3b9aba0fbe7b02f46cbffed9dea1", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m12057h(MissionTemplate avh) {
        throw new aWi(new aCE(this, eRY, new Object[]{avh}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3523rw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return bJI();
            case 1:
                m12056f((MissionTemplate) args[0]);
                return null;
            case 2:
                m12057h((MissionTemplate) args[0]);
                return null;
            case 3:
                m12054c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Template")
    public MissionTemplate bfA() {
        switch (bFf().mo6893i(eRW)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, eRW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eRW, new Object[0]));
                break;
        }
        return bJI();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f2228CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2228CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2228CT, new Object[]{aiw}));
                break;
        }
        m12054c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Template")
    @C5566aOg
    /* renamed from: g */
    public void mo7403g(MissionTemplate avh) {
        switch (bFf().mo6893i(eRX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eRX, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eRX, new Object[]{avh}));
                break;
        }
        m12056f(avh);
    }

    @C5566aOg
    /* renamed from: i */
    public final void mo7404i(MissionTemplate avh) {
        switch (bFf().mo6893i(eRY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eRY, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eRY, new Object[]{avh}));
                break;
        }
        m12057h(avh);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Template")
    @C0064Am(aul = "81cf60a68644899e03119421821afceb", aum = 0)
    private MissionTemplate bJI() {
        return bJH();
    }

    @C0064Am(aul = "45cdb3746443c8ba49e4282c55cbc23a", aum = 0)
    /* renamed from: c */
    private void m12054c(C5426aIw aiw) {
        if (bJH() == null) {
            mo6317hy("ERROR: Trying to open Mission Briefing with a null mission template");
        } else {
            mo7404i(bJH());
        }
    }
}
