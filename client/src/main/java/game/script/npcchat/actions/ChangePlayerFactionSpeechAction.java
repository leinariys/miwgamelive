package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.faction.Faction;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1665YW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Le */
/* compiled from: a */
public class ChangePlayerFactionSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f1053CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cnU = null;
    public static final C2491fm cov = null;
    public static final C2491fm cow = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "644b87d285e9ae28d5bfb41d9ea95919", aum = 0)
    private static Faction axU;

    static {
        m6819V();
    }

    public ChangePlayerFactionSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ChangePlayerFactionSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6819V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 3;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ChangePlayerFactionSpeechAction.class, "644b87d285e9ae28d5bfb41d9ea95919", i);
        cnU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(ChangePlayerFactionSpeechAction.class, "a0a279c14ca4630b90144b3fff939061", i3);
        cow = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ChangePlayerFactionSpeechAction.class, "e0a99082d796d2d88e5596f591aa742a", i4);
        cov = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ChangePlayerFactionSpeechAction.class, "a5915b5c7deb9f535c01edb2a69ed877", i5);
        f1053CT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ChangePlayerFactionSpeechAction.class, C1665YW.class, _m_fields, _m_methods);
    }

    private Faction azq() {
        return (Faction) bFf().mo5608dq().mo3214p(cnU);
    }

    /* renamed from: l */
    private void m6821l(Faction xm) {
        bFf().mo5608dq().mo3197f(cnU, xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    @C0064Am(aul = "a0a279c14ca4630b90144b3fff939061", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m6822m(Faction xm) {
        throw new aWi(new aCE(this, cow, new Object[]{xm}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1665YW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                m6822m((Faction) args[0]);
                return null;
            case 1:
                return azM();
            case 2:
                m6820c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    public Faction azN() {
        switch (bFf().mo6893i(cov)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, cov, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cov, new Object[0]));
                break;
        }
        return azM();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f1053CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1053CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1053CT, new Object[]{aiw}));
                break;
        }
        m6820c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    @C5566aOg
    /* renamed from: n */
    public void mo3806n(Faction xm) {
        switch (bFf().mo6893i(cow)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                break;
        }
        m6822m(xm);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    @C0064Am(aul = "e0a99082d796d2d88e5596f591aa742a", aum = 0)
    private Faction azM() {
        return azq();
    }

    @C0064Am(aul = "a5915b5c7deb9f535c01edb2a69ed877", aum = 0)
    /* renamed from: c */
    private void m6820c(C5426aIw aiw) {
        aiw.mo9402B(azq());
    }
}
