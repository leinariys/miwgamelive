package game.script.npcchat.actions;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3160oX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.GY */
/* compiled from: a */
public class RemoveParamSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CR */
    public static final C5663aRz f628CR = null;

    /* renamed from: CT */
    public static final C2491fm f629CT = null;

    /* renamed from: CU */
    public static final C2491fm f630CU = null;

    /* renamed from: CV */
    public static final C2491fm f631CV = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "70a5432f66090c9a6677621ec7098d0c", aum = 0)
    private static String key;

    static {
        m3416V();
    }

    public RemoveParamSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RemoveParamSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3416V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 3;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(RemoveParamSpeechAction.class, "70a5432f66090c9a6677621ec7098d0c", i);
        f628CR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(RemoveParamSpeechAction.class, "6783cd1ddd51f9b32f0d11bf07584711", i3);
        f629CT = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(RemoveParamSpeechAction.class, "3e8e7b99d926fa7c8fbdb03c9323927d", i4);
        f630CU = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(RemoveParamSpeechAction.class, "4ab0aa0d8c654b1cf128429329dbe3ab", i5);
        f631CV = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RemoveParamSpeechAction.class, C3160oX.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m3415U(String str) {
        bFf().mo5608dq().mo3197f(f628CR, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C0064Am(aul = "4ab0aa0d8c654b1cf128429329dbe3ab", aum = 0)
    @C5566aOg
    /* renamed from: W */
    private void m3417W(String str) {
        throw new aWi(new aCE(this, f631CV, new Object[]{str}));
    }

    /* renamed from: lM */
    private String m3419lM() {
        return (String) bFf().mo5608dq().mo3214p(f628CR);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3160oX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                m3418c((C5426aIw) args[0]);
                return null;
            case 1:
                return m3420lO();
            case 2:
                m3417W((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f629CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f629CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f629CT, new Object[]{aiw}));
                break;
        }
        m3418c(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    public String getKey() {
        switch (bFf().mo6893i(f630CU)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f630CU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f630CU, new Object[0]));
                break;
        }
        return m3420lO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C5566aOg
    public void setKey(String str) {
        switch (bFf().mo6893i(f631CV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f631CV, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f631CV, new Object[]{str}));
                break;
        }
        m3417W(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "6783cd1ddd51f9b32f0d11bf07584711", aum = 0)
    /* renamed from: c */
    private void m3418c(C5426aIw aiw) {
        aiw.mo9427mm(m3419lM());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    @C0064Am(aul = "3e8e7b99d926fa7c8fbdb03c9323927d", aum = 0)
    /* renamed from: lO */
    private String m3420lO() {
        return m3419lM();
    }
}
