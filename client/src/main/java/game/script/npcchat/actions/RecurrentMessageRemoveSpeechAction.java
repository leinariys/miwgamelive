package game.script.npcchat.actions;

import game.network.message.externalizable.C6559apL;
import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechAction;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6875avP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aCP */
/* compiled from: a */
public class RecurrentMessageRemoveSpeechAction extends SpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f2475CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bca = null;
    public static final C2491fm bcf = null;
    public static final C2491fm bcg = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bf9eaa0bc6ceb935a3e47018d65de57f", aum = 0)
    private static String bbZ;

    static {
        m13211V();
    }

    public RecurrentMessageRemoveSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RecurrentMessageRemoveSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13211V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechAction._m_fieldCount + 1;
        _m_methodCount = SpeechAction._m_methodCount + 3;
        int i = SpeechAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(RecurrentMessageRemoveSpeechAction.class, "bf9eaa0bc6ceb935a3e47018d65de57f", i);
        bca = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_fields, (Object[]) _m_fields);
        int i3 = SpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(RecurrentMessageRemoveSpeechAction.class, "1bb298431682fdf8901763da63070ae0", i3);
        bcf = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(RecurrentMessageRemoveSpeechAction.class, "8b77e01c81b826d2a28451347127eb7f", i4);
        bcg = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(RecurrentMessageRemoveSpeechAction.class, "ec98fdee22cbf1750beb1794c9618c48", i5);
        f2475CT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RecurrentMessageRemoveSpeechAction.class, C6875avP.class, _m_fields, _m_methods);
    }

    /* renamed from: YL */
    private String m13212YL() {
        return (String) bFf().mo5608dq().mo3214p(bca);
    }

    /* renamed from: bh */
    private void m13214bh(String str) {
        bFf().mo5608dq().mo3197f(bca, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C0064Am(aul = "8b77e01c81b826d2a28451347127eb7f", aum = 0)
    @C5566aOg
    /* renamed from: bi */
    private void m13215bi(String str) {
        throw new aWi(new aCE(this, bcg, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6875avP(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    /* renamed from: YP */
    public String mo8029YP() {
        switch (bFf().mo6893i(bcf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bcf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcf, new Object[0]));
                break;
        }
        return m13213YO();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechAction._m_methodCount) {
            case 0:
                return m13213YO();
            case 1:
                m13215bi((String) args[0]);
                return null;
            case 2:
                m13216c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C5566aOg
    /* renamed from: bj */
    public void mo8030bj(String str) {
        switch (bFf().mo6893i(bcg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                break;
        }
        m13215bi(str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f2475CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2475CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2475CT, new Object[]{aiw}));
                break;
        }
        m13216c(aiw);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    @C0064Am(aul = "1bb298431682fdf8901763da63070ae0", aum = 0)
    /* renamed from: YO */
    private String m13213YO() {
        return m13212YL();
    }

    @C0064Am(aul = "ec98fdee22cbf1750beb1794c9618c48", aum = 0)
    /* renamed from: c */
    private void m13216c(C5426aIw aiw) {
        aPA().mo14419f((C1506WA) new C6559apL(m13212YL()));
    }
}
