package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C5416aIm;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.x */
/* compiled from: a */
public abstract class SpeechRequirement extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bJ */
    public static final C5663aRz f9500bJ = null;
    /* renamed from: bL */
    public static final C5663aRz f9502bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9503bM = null;
    /* renamed from: bN */
    public static final C2491fm f9504bN = null;
    /* renamed from: bO */
    public static final C2491fm f9505bO = null;
    /* renamed from: bP */
    public static final C2491fm f9506bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9507bQ = null;
    /* renamed from: bR */
    public static final C2491fm f9508bR = null;
    /* renamed from: bS */
    public static final C2491fm f9509bS = null;
    /* renamed from: bT */
    public static final C2491fm f9510bT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "93a57bb2c3fc3cf76132cec8601d25bb", aum = 0)

    /* renamed from: bI */
    private static AbstractSpeech f9499bI;
    @C0064Am(aul = "df466a4578a1b8fbb13d2a05a2f278ad", aum = 1)

    /* renamed from: bK */
    private static UUID f9501bK;
    @C0064Am(aul = "e3e16d77b2ac35431479c37a4a680e11", aum = 2)
    private static String handle;

    static {
        m40780V();
    }

    public SpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40780V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(SpeechRequirement.class, "93a57bb2c3fc3cf76132cec8601d25bb", i);
        f9500bJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpeechRequirement.class, "df466a4578a1b8fbb13d2a05a2f278ad", i2);
        f9502bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpeechRequirement.class, "e3e16d77b2ac35431479c37a4a680e11", i3);
        f9503bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(SpeechRequirement.class, "cfde5bec9da3babc221c0ab3b446961b", i5);
        f9504bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(SpeechRequirement.class, "da90f1214a8b515a5b446d95834e906b", i6);
        f9505bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeechRequirement.class, "b9b5dbe961ab28467bc651dcb65c5758", i7);
        f9506bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeechRequirement.class, "c70da0eb84e19e65d418c37d2c5daef0", i8);
        f9507bQ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeechRequirement.class, "205688faf909c5c09e98814f573fd36a", i9);
        f9508bR = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(SpeechRequirement.class, "d9db85edd8f550e09f3ea4a722b35df8", i10);
        f9509bS = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(SpeechRequirement.class, "55f26bbcbe86d977875056ed579469d4", i11);
        f9510bT = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(SpeechRequirement.class, "d4c83020a7c51bec7f554def0e0e9bdc", i12);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeechRequirement.class, C5416aIm.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m40781a(AbstractSpeech aht) {
        bFf().mo5608dq().mo3197f(f9500bJ, aht);
    }

    /* renamed from: a */
    private void m40782a(String str) {
        bFf().mo5608dq().mo3197f(f9503bM, str);
    }

    /* renamed from: a */
    private void m40783a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9502bL, uuid);
    }

    @C0064Am(aul = "55f26bbcbe86d977875056ed579469d4", aum = 0)
    /* renamed from: a */
    private boolean m40784a(C5426aIw aiw) {
        throw new aWi(new aCE(this, f9510bT, new Object[]{aiw}));
    }

    /* renamed from: am */
    private AbstractSpeech m40785am() {
        return (AbstractSpeech) bFf().mo5608dq().mo3214p(f9500bJ);
    }

    /* renamed from: an */
    private UUID m40786an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9502bL);
    }

    /* renamed from: ao */
    private String m40787ao() {
        return (String) bFf().mo5608dq().mo3214p(f9503bM);
    }

    @C0064Am(aul = "205688faf909c5c09e98814f573fd36a", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m40792b(AbstractSpeech aht) {
        throw new aWi(new aCE(this, f9508bR, new Object[]{aht}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "c70da0eb84e19e65d418c37d2c5daef0", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m40793b(String str) {
        throw new aWi(new aCE(this, f9507bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m40795c(UUID uuid) {
        switch (bFf().mo6893i(f9505bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9505bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9505bO, new Object[]{uuid}));
                break;
        }
        m40794b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5416aIm(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m40788ap();
            case 1:
                m40794b((UUID) args[0]);
                return null;
            case 2:
                return m40789ar();
            case 3:
                m40793b((String) args[0]);
                return null;
            case 4:
                m40792b((AbstractSpeech) args[0]);
                return null;
            case 5:
                return m40790as();
            case 6:
                return new Boolean(m40784a((C5426aIw) args[0]));
            case 7:
                return m40791au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9504bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9504bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9504bN, new Object[0]));
                break;
        }
        return m40788ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech")
    /* renamed from: at */
    public final AbstractSpeech mo22847at() {
        switch (bFf().mo6893i(f9509bS)) {
            case 0:
                return null;
            case 2:
                return (AbstractSpeech) bFf().mo5606d(new aCE(this, f9509bS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9509bS, new Object[0]));
                break;
        }
        return m40790as();
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f9510bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9510bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9510bT, new Object[]{aiw}));
                break;
        }
        return m40784a(aiw);
    }

    @C5566aOg
    /* renamed from: c */
    public final void mo22848c(AbstractSpeech aht) {
        switch (bFf().mo6893i(f9508bR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9508bR, new Object[]{aht}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9508bR, new Object[]{aht}));
                break;
        }
        m40792b(aht);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9506bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9506bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9506bP, new Object[0]));
                break;
        }
        return m40789ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9507bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9507bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9507bQ, new Object[]{str}));
                break;
        }
        m40793b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m40791au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m40783a(UUID.randomUUID());
    }

    @C0064Am(aul = "cfde5bec9da3babc221c0ab3b446961b", aum = 0)
    /* renamed from: ap */
    private UUID m40788ap() {
        return m40786an();
    }

    @C0064Am(aul = "da90f1214a8b515a5b446d95834e906b", aum = 0)
    /* renamed from: b */
    private void m40794b(UUID uuid) {
        m40783a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "b9b5dbe961ab28467bc651dcb65c5758", aum = 0)
    /* renamed from: ar */
    private String m40789ar() {
        return m40787ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech")
    @C0064Am(aul = "d9db85edd8f550e09f3ea4a722b35df8", aum = 0)
    /* renamed from: as */
    private AbstractSpeech m40790as() {
        return m40785am();
    }

    @C0064Am(aul = "d4c83020a7c51bec7f554def0e0e9bdc", aum = 0)
    /* renamed from: au */
    private String m40791au() {
        return getClass().getSimpleName();
    }
}
