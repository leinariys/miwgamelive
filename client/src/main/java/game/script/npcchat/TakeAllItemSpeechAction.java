package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import game.script.npcchat.actions.TakeItemSpeechAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6825auR;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ym */
/* compiled from: a */
public class TakeAllItemSpeechAction extends TakeItemSpeechAction implements C1616Xf {

    /* renamed from: CT */
    public static final C2491fm f9603CT = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m41409V();
    }

    public TakeAllItemSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TakeAllItemSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41409V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TakeItemSpeechAction._m_fieldCount + 0;
        _m_methodCount = TakeItemSpeechAction._m_methodCount + 1;
        _m_fields = new C5663aRz[(TakeItemSpeechAction._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TakeItemSpeechAction._m_fields, (Object[]) _m_fields);
        int i = TakeItemSpeechAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(TakeAllItemSpeechAction.class, "f57f776323bd15d0fd283c8e29d151fc", i);
        f9603CT = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TakeItemSpeechAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TakeAllItemSpeechAction.class, C6825auR.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6825auR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TakeItemSpeechAction._m_methodCount) {
            case 0:
                m41410c((C5426aIw) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo2354d(C5426aIw aiw) {
        switch (bFf().mo6893i(f9603CT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9603CT, new Object[]{aiw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9603CT, new Object[]{aiw}));
                break;
        }
        m41410c(aiw);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f57f776323bd15d0fd283c8e29d151fc", aum = 0)
    /* renamed from: c */
    private void m41410c(C5426aIw aiw) {
        if (aiw.mo9417f(cbU(), cbW()) < cbW()) {
            throw new C3512ro("Player " + aiw.getName() + " has no enough items.", C3512ro.C3513a.INSUFICIENT_ITENS);
        }
        mo23381b(((NLSSpeechActions) ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEy(), cbU().mo19891ke());
    }
}
