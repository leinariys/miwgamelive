package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5590aPe;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Sn */
/* compiled from: a */
public class HasItemSpeechRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f1563bT = null;
    /* renamed from: cA */
    public static final C2491fm f1564cA = null;
    /* renamed from: cB */
    public static final C2491fm f1565cB = null;
    /* renamed from: cC */
    public static final C2491fm f1566cC = null;
    /* renamed from: cD */
    public static final C2491fm f1567cD = null;
    /* renamed from: cx */
    public static final C5663aRz f1569cx = null;
    /* renamed from: cz */
    public static final C5663aRz f1571cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a2924b2f9bbb2574f9617c911f629bfc", aum = 0)

    /* renamed from: cw */
    private static ItemType f1568cw;
    @C0064Am(aul = "822017a74782d5dba37554e54508aa3a", aum = 1)

    /* renamed from: cy */
    private static int f1570cy;

    static {
        m9587V();
    }

    public HasItemSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HasItemSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9587V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 2;
        _m_methodCount = SpeechRequirement._m_methodCount + 5;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(HasItemSpeechRequirement.class, "a2924b2f9bbb2574f9617c911f629bfc", i);
        f1569cx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HasItemSpeechRequirement.class, "822017a74782d5dba37554e54508aa3a", i2);
        f1571cz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i4 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(HasItemSpeechRequirement.class, "8165ca15a11f5fdee3e82eed316f07ab", i4);
        f1566cC = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(HasItemSpeechRequirement.class, "ee6541c1dcdfafcc08b3759f43e6e2dc", i5);
        f1567cD = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(HasItemSpeechRequirement.class, "5e98057def6206a84cfd12f6fb9e13b1", i6);
        f1564cA = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(HasItemSpeechRequirement.class, "ddaf2deb14d4ddf971932afdc8fc34f7", i7);
        f1565cB = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(HasItemSpeechRequirement.class, "74c1e1d4b94c81fa96eedb134a882e55", i8);
        f1563bT = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HasItemSpeechRequirement.class, C5590aPe.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9588a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f1569cx, jCVar);
    }

    /* renamed from: av */
    private ItemType m9590av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f1569cx);
    }

    /* renamed from: aw */
    private int m9591aw() {
        return bFf().mo5608dq().mo3212n(f1571cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C0064Am(aul = "ee6541c1dcdfafcc08b3759f43e6e2dc", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m9594b(ItemType jCVar) {
        throw new aWi(new aCE(this, f1567cD, new Object[]{jCVar}));
    }

    /* renamed from: f */
    private void m9595f(int i) {
        bFf().mo5608dq().mo3183b(f1571cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "ddaf2deb14d4ddf971932afdc8fc34f7", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m9596g(int i) {
        throw new aWi(new aCE(this, f1565cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5590aPe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return m9593ay();
            case 1:
                m9594b((ItemType) args[0]);
                return null;
            case 2:
                return new Integer(m9592ax());
            case 3:
                m9596g(((Integer) args[0]).intValue());
                return null;
            case 4:
                return new Boolean(m9589a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    /* renamed from: az */
    public ItemType mo5467az() {
        switch (bFf().mo6893i(f1566cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f1566cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1566cC, new Object[0]));
                break;
        }
        return m9593ay();
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f1563bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1563bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1563bT, new Object[]{aiw}));
                break;
        }
        return m9589a(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C5566aOg
    /* renamed from: c */
    public void mo5468c(ItemType jCVar) {
        switch (bFf().mo6893i(f1567cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1567cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1567cD, new Object[]{jCVar}));
                break;
        }
        m9594b(jCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f1564cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f1564cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1564cA, new Object[0]));
                break;
        }
        return m9592ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f1565cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1565cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1565cB, new Object[]{new Integer(i)}));
                break;
        }
        m9596g(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m9595f(1);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    @C0064Am(aul = "8165ca15a11f5fdee3e82eed316f07ab", aum = 0)
    /* renamed from: ay */
    private ItemType m9593ay() {
        return m9590av();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "5e98057def6206a84cfd12f6fb9e13b1", aum = 0)
    /* renamed from: ax */
    private int m9592ax() {
        return m9591aw();
    }

    @C0064Am(aul = "74c1e1d4b94c81fa96eedb134a882e55", aum = 0)
    /* renamed from: a */
    private boolean m9589a(C5426aIw aiw) {
        return aiw.mo9405b(m9590av(), m9591aw());
    }
}
