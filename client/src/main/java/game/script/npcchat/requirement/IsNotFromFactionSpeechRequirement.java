package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.faction.Faction;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aIM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.oN */
/* compiled from: a */
public class IsNotFromFactionSpeechRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aQn = null;
    public static final C2491fm aQo = null;
    public static final C2491fm aQp = null;
    public static final C2491fm aQq = null;
    /* renamed from: bT */
    public static final C2491fm f8759bT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "eb86d482d713166264323c7ae218894b", aum = 0)
    private static C3438ra<Faction> aQm;

    static {
        m36667V();
    }

    public IsNotFromFactionSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public IsNotFromFactionSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36667V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 4;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(IsNotFromFactionSpeechRequirement.class, "eb86d482d713166264323c7ae218894b", i);
        aQn = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(IsNotFromFactionSpeechRequirement.class, "e5f61210de28d46f97d0cf977826da40", i3);
        aQo = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(IsNotFromFactionSpeechRequirement.class, "92a40f2f95e31755f0bf0d8303dbcf76", i4);
        aQp = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(IsNotFromFactionSpeechRequirement.class, "355ccec08e888bfe16570a572e09c7e0", i5);
        aQq = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(IsNotFromFactionSpeechRequirement.class, "4c223f6527cb97862fa0eb8ac4644b57", i6);
        f8759bT = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(IsNotFromFactionSpeechRequirement.class, aIM.class, _m_fields, _m_methods);
    }

    /* renamed from: C */
    private void m36664C(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aQn, raVar);
    }

    /* renamed from: Ut */
    private C3438ra m36665Ut() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aQn);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Factions")
    @C0064Am(aul = "e5f61210de28d46f97d0cf977826da40", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m36669h(Faction xm) {
        throw new aWi(new aCE(this, aQo, new Object[]{xm}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Factions")
    @C0064Am(aul = "92a40f2f95e31755f0bf0d8303dbcf76", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m36670j(Faction xm) {
        throw new aWi(new aCE(this, aQp, new Object[]{xm}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Factions")
    /* renamed from: Uv */
    public List<Faction> mo20976Uv() {
        switch (bFf().mo6893i(aQq)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aQq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQq, new Object[0]));
                break;
        }
        return m36666Uu();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aIM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                m36669h((Faction) args[0]);
                return null;
            case 1:
                m36670j((Faction) args[0]);
                return null;
            case 2:
                return m36666Uu();
            case 3:
                return new Boolean(m36668a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f8759bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8759bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8759bT, new Object[]{aiw}));
                break;
        }
        return m36668a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Factions")
    @C5566aOg
    /* renamed from: i */
    public void mo20977i(Faction xm) {
        switch (bFf().mo6893i(aQo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aQo, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aQo, new Object[]{xm}));
                break;
        }
        m36669h(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Factions")
    @C5566aOg
    /* renamed from: k */
    public void mo20978k(Faction xm) {
        switch (bFf().mo6893i(aQp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aQp, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aQp, new Object[]{xm}));
                break;
        }
        m36670j(xm);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Factions")
    @C0064Am(aul = "355ccec08e888bfe16570a572e09c7e0", aum = 0)
    /* renamed from: Uu */
    private List<Faction> m36666Uu() {
        return Collections.unmodifiableList(m36665Ut());
    }

    @C0064Am(aul = "4c223f6527cb97862fa0eb8ac4644b57", aum = 0)
    /* renamed from: a */
    private boolean m36668a(C5426aIw aiw) {
        return !m36665Ut().contains(aiw.azN());
    }
}
