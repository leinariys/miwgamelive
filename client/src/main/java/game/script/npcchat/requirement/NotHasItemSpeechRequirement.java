package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5605aPt;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aqr  reason: case insensitive filesystem */
/* compiled from: a */
public class NotHasItemSpeechRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f5193bT = null;
    /* renamed from: cA */
    public static final C2491fm f5194cA = null;
    /* renamed from: cB */
    public static final C2491fm f5195cB = null;
    /* renamed from: cC */
    public static final C2491fm f5196cC = null;
    /* renamed from: cD */
    public static final C2491fm f5197cD = null;
    /* renamed from: cx */
    public static final C5663aRz f5199cx = null;
    /* renamed from: cz */
    public static final C5663aRz f5201cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "48a402c7a83db7b51fdd8b889b92324e", aum = 0)

    /* renamed from: cw */
    private static ItemType f5198cw;
    @C0064Am(aul = "b035f10861aa23b2cbd93203fb4a88c5", aum = 1)

    /* renamed from: cy */
    private static int f5200cy;

    static {
        m25242V();
    }

    public NotHasItemSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NotHasItemSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25242V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 2;
        _m_methodCount = SpeechRequirement._m_methodCount + 5;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(NotHasItemSpeechRequirement.class, "48a402c7a83db7b51fdd8b889b92324e", i);
        f5199cx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NotHasItemSpeechRequirement.class, "b035f10861aa23b2cbd93203fb4a88c5", i2);
        f5201cz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i4 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(NotHasItemSpeechRequirement.class, "ae6edd5798cb162b74a3f2c5ea5c83ee", i4);
        f5196cC = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(NotHasItemSpeechRequirement.class, "f0e3556640fe2b93489338a176e5c531", i5);
        f5197cD = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(NotHasItemSpeechRequirement.class, "2b970a7a74b85b4d6c7470a1d46a8b15", i6);
        f5194cA = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(NotHasItemSpeechRequirement.class, "47d5c5d9279647a762f07f9f2f39cf01", i7);
        f5195cB = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(NotHasItemSpeechRequirement.class, "cd9eacefb1c4327a1e4f68083f62546b", i8);
        f5193bT = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NotHasItemSpeechRequirement.class, C5605aPt.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m25243a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f5199cx, jCVar);
    }

    /* renamed from: av */
    private ItemType m25245av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f5199cx);
    }

    /* renamed from: aw */
    private int m25246aw() {
        return bFf().mo5608dq().mo3212n(f5201cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C0064Am(aul = "f0e3556640fe2b93489338a176e5c531", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m25249b(ItemType jCVar) {
        throw new aWi(new aCE(this, f5197cD, new Object[]{jCVar}));
    }

    /* renamed from: f */
    private void m25250f(int i) {
        bFf().mo5608dq().mo3183b(f5201cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "47d5c5d9279647a762f07f9f2f39cf01", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m25251g(int i) {
        throw new aWi(new aCE(this, f5195cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5605aPt(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return m25248ay();
            case 1:
                m25249b((ItemType) args[0]);
                return null;
            case 2:
                return new Integer(m25247ax());
            case 3:
                m25251g(((Integer) args[0]).intValue());
                return null;
            case 4:
                return new Boolean(m25244a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    /* renamed from: az */
    public ItemType mo15669az() {
        switch (bFf().mo6893i(f5196cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f5196cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5196cC, new Object[0]));
                break;
        }
        return m25248ay();
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f5193bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5193bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5193bT, new Object[]{aiw}));
                break;
        }
        return m25244a(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C5566aOg
    /* renamed from: c */
    public void mo15670c(ItemType jCVar) {
        switch (bFf().mo6893i(f5197cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5197cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5197cD, new Object[]{jCVar}));
                break;
        }
        m25249b(jCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f5194cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f5194cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5194cA, new Object[0]));
                break;
        }
        return m25247ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f5195cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5195cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5195cB, new Object[]{new Integer(i)}));
                break;
        }
        m25251g(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    @C0064Am(aul = "ae6edd5798cb162b74a3f2c5ea5c83ee", aum = 0)
    /* renamed from: ay */
    private ItemType m25248ay() {
        return m25245av();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "2b970a7a74b85b4d6c7470a1d46a8b15", aum = 0)
    /* renamed from: ax */
    private int m25247ax() {
        return m25246aw();
    }

    @C0064Am(aul = "cd9eacefb1c4327a1e4f68083f62546b", aum = 0)
    /* renamed from: a */
    private boolean m25244a(C5426aIw aiw) {
        return !aiw.mo9405b(m25245av(), m25246aw());
    }
}
