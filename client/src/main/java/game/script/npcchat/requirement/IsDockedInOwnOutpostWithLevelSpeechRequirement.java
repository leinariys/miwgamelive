package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.ship.Outpost;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1137Qh;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.RN */
/* compiled from: a */
public class IsDockedInOwnOutpostWithLevelSpeechRequirement extends IsDockedInOwnOutpostSpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f1486bT = null;
    public static final C5663aRz eaS = null;
    public static final C2491fm eaT = null;
    public static final C2491fm eaU = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "164da67e4587737e563e03de44812594", aum = 0)
    private static C2611hZ dVa;

    static {
        m9107V();
    }

    public IsDockedInOwnOutpostWithLevelSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public IsDockedInOwnOutpostWithLevelSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9107V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = IsDockedInOwnOutpostSpeechRequirement._m_fieldCount + 1;
        _m_methodCount = IsDockedInOwnOutpostSpeechRequirement._m_methodCount + 3;
        int i = IsDockedInOwnOutpostSpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(IsDockedInOwnOutpostWithLevelSpeechRequirement.class, "164da67e4587737e563e03de44812594", i);
        eaS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) IsDockedInOwnOutpostSpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = IsDockedInOwnOutpostSpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(IsDockedInOwnOutpostWithLevelSpeechRequirement.class, "2dcf1bdaa2488c2adff2aaa185ac4107", i3);
        f1486bT = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(IsDockedInOwnOutpostWithLevelSpeechRequirement.class, "b5765d02336bb5b8004e2c48d6d911d5", i4);
        eaT = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(IsDockedInOwnOutpostWithLevelSpeechRequirement.class, "0a85bd062773c1d67cc6d08a9f9baeec", i5);
        eaU = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) IsDockedInOwnOutpostSpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(IsDockedInOwnOutpostWithLevelSpeechRequirement.class, C1137Qh.class, _m_fields, _m_methods);
    }

    private C2611hZ brG() {
        return (C2611hZ) bFf().mo5608dq().mo3214p(eaS);
    }

    /* renamed from: h */
    private void m9109h(C2611hZ hZVar) {
        bFf().mo5608dq().mo3197f(eaS, hZVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Required Outpost Level")
    @C0064Am(aul = "0a85bd062773c1d67cc6d08a9f9baeec", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m9110i(C2611hZ hZVar) {
        throw new aWi(new aCE(this, eaU, new Object[]{hZVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1137Qh(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - IsDockedInOwnOutpostSpeechRequirement._m_methodCount) {
            case 0:
                return new Boolean(m9108a((C5426aIw) args[0]));
            case 1:
                return brH();
            case 2:
                m9110i((C2611hZ) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f1486bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1486bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1486bT, new Object[]{aiw}));
                break;
        }
        return m9108a(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Required Outpost Level")
    public C2611hZ brI() {
        switch (bFf().mo6893i(eaT)) {
            case 0:
                return null;
            case 2:
                return (C2611hZ) bFf().mo5606d(new aCE(this, eaT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eaT, new Object[0]));
                break;
        }
        return brH();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Required Outpost Level")
    @C5566aOg
    /* renamed from: j */
    public void mo5168j(C2611hZ hZVar) {
        switch (bFf().mo6893i(eaU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eaU, new Object[]{hZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eaU, new Object[]{hZVar}));
                break;
        }
        m9110i(hZVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "2dcf1bdaa2488c2adff2aaa185ac4107", aum = 0)
    /* renamed from: a */
    private boolean m9108a(C5426aIw aiw) {
        if (!super.mo2483b(aiw)) {
            return false;
        }
        if (brG() == null) {
            mo6317hy("Speech Requirement " + bFY() + " has null parameter");
            return false;
        }
        Outpost qZVar = (Outpost) aiw.cXi();
        if (qZVar.bXV() != Outpost.C3349b.SOLD) {
            return false;
        }
        return qZVar.mo21372Mb().ordinal() >= brG().ordinal();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Required Outpost Level")
    @C0064Am(aul = "b5765d02336bb5b8004e2c48d6d911d5", aum = 0)
    private C2611hZ brH() {
        return brG();
    }
}
