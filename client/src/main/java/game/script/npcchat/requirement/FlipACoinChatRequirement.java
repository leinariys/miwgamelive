package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5349aFx;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Random;

@C5511aMd
@C6485anp
/* renamed from: a.mU */
/* compiled from: a */
public class FlipACoinChatRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aGl = null;
    public static final C2491fm aGm = null;
    public static final C2491fm aGn = null;
    /* renamed from: bT */
    public static final C2491fm f8654bT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "387e5b41ad20e8cf0cbf9ba842f98c1e", aum = 0)
    private static float aGk;

    static {
        m35762V();
    }

    public FlipACoinChatRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FlipACoinChatRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35762V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 3;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(FlipACoinChatRequirement.class, "387e5b41ad20e8cf0cbf9ba842f98c1e", i);
        aGl = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(FlipACoinChatRequirement.class, "a23740ea11b70eefa7ee831d45ddc671", i3);
        aGm = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(FlipACoinChatRequirement.class, "8123df9eef71cbf2f9816d2228c796a4", i4);
        aGn = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(FlipACoinChatRequirement.class, "7a5f552707447924b8e76fe72d068304", i5);
        f8654bT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FlipACoinChatRequirement.class, C5349aFx.class, _m_fields, _m_methods);
    }

    /* renamed from: PA */
    private float m35760PA() {
        return bFf().mo5608dq().mo3211m(aGl);
    }

    /* renamed from: cs */
    private void m35764cs(float f) {
        bFf().mo5608dq().mo3150a(aGl, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ProbabilityOfTrue (100% >= x > 0%)")
    @C0064Am(aul = "8123df9eef71cbf2f9816d2228c796a4", aum = 0)
    @C5566aOg
    /* renamed from: ct */
    private void m35765ct(float f) {
        throw new aWi(new aCE(this, aGn, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ProbabilityOfTrue (100 >= x > 0)")
    /* renamed from: PC */
    public float mo20559PC() {
        switch (bFf().mo6893i(aGm)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aGm, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGm, new Object[0]));
                break;
        }
        return m35761PB();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5349aFx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return new Float(m35761PB());
            case 1:
                m35765ct(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Boolean(m35763a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f8654bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8654bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8654bT, new Object[]{aiw}));
                break;
        }
        return m35763a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ProbabilityOfTrue (100% >= x > 0%)")
    @C5566aOg
    /* renamed from: cu */
    public void mo20560cu(float f) {
        switch (bFf().mo6893i(aGn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGn, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGn, new Object[]{new Float(f)}));
                break;
        }
        m35765ct(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ProbabilityOfTrue (100 >= x > 0)")
    @C0064Am(aul = "a23740ea11b70eefa7ee831d45ddc671", aum = 0)
    /* renamed from: PB */
    private float m35761PB() {
        return m35760PA();
    }

    @C0064Am(aul = "7a5f552707447924b8e76fe72d068304", aum = 0)
    /* renamed from: a */
    private boolean m35763a(C5426aIw aiw) {
        return new Random().nextFloat() <= m35760PA() / 100.0f;
    }
}
