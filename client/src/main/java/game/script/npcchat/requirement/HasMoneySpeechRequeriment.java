package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aSN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ln */
/* compiled from: a */
public class HasMoneySpeechRequeriment extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm axj = null;
    public static final C2491fm axk = null;
    /* renamed from: bT */
    public static final C2491fm f8584bT = null;
    /* renamed from: cz */
    public static final C5663aRz f8585cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6fb1970e34a65737928b9d2d55481b1c", aum = 0)
    private static long axi;

    static {
        m35288V();
    }

    public HasMoneySpeechRequeriment() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HasMoneySpeechRequeriment(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35288V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 3;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(HasMoneySpeechRequeriment.class, "6fb1970e34a65737928b9d2d55481b1c", i);
        f8585cz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(HasMoneySpeechRequeriment.class, "4ad1d1398f464234da4840814fa44edf", i3);
        axj = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(HasMoneySpeechRequeriment.class, "14cd28ebba7acc97fb446b02278f433d", i4);
        axk = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(HasMoneySpeechRequeriment.class, "59b709d7e5c8d34a2a2341bfae606771", i5);
        f8584bT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HasMoneySpeechRequeriment.class, aSN.class, _m_fields, _m_methods);
    }

    /* renamed from: LS */
    private long m35286LS() {
        return bFf().mo5608dq().mo3213o(f8585cz);
    }

    /* renamed from: cj */
    private void m35290cj(long j) {
        bFf().mo5608dq().mo3184b(f8585cz, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Money Amount")
    @C0064Am(aul = "14cd28ebba7acc97fb446b02278f433d", aum = 0)
    @C5566aOg
    /* renamed from: ck */
    private void m35291ck(long j) {
        throw new aWi(new aCE(this, axk, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Money Amount")
    /* renamed from: LU */
    public long mo20405LU() {
        switch (bFf().mo6893i(axj)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, axj, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, axj, new Object[0]));
                break;
        }
        return m35287LT();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aSN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return new Long(m35287LT());
            case 1:
                m35291ck(((Long) args[0]).longValue());
                return null;
            case 2:
                return new Boolean(m35289a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f8584bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8584bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8584bT, new Object[]{aiw}));
                break;
        }
        return m35289a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Money Amount")
    @C5566aOg
    /* renamed from: cl */
    public void mo20406cl(long j) {
        switch (bFf().mo6893i(axk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, axk, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, axk, new Object[]{new Long(j)}));
                break;
        }
        m35291ck(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Money Amount")
    @C0064Am(aul = "4ad1d1398f464234da4840814fa44edf", aum = 0)
    /* renamed from: LT */
    private long m35287LT() {
        return m35286LS();
    }

    @C0064Am(aul = "59b709d7e5c8d34a2a2341bfae606771", aum = 0)
    /* renamed from: a */
    private boolean m35289a(C5426aIw aiw) {
        return aiw.mo9421kx(m35286LS());
    }
}
