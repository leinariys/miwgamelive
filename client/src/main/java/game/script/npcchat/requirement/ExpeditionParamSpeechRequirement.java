package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.missiontemplate.ExpeditionMissionInfo;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5549aNp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Sg */
/* compiled from: a */
public class ExpeditionParamSpeechRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f1549bT = null;
    public static final C5663aRz ecM = null;
    public static final C5663aRz ecO = null;
    public static final C2491fm ecP = null;
    public static final C2491fm ecQ = null;
    public static final C2491fm ecR = null;
    public static final C2491fm ecS = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1fef29c0be9a3097482007d6a96aa06f", aum = 0)
    private static int ecL;
    @C0064Am(aul = "9b705e627b3168bad0ad72fcf88f44fe", aum = 1)
    private static ExpeditionMissionInfo ecN;

    static {
        m9485V();
    }

    public ExpeditionParamSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ExpeditionParamSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9485V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 2;
        _m_methodCount = SpeechRequirement._m_methodCount + 5;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ExpeditionParamSpeechRequirement.class, "1fef29c0be9a3097482007d6a96aa06f", i);
        ecM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ExpeditionParamSpeechRequirement.class, "9b705e627b3168bad0ad72fcf88f44fe", i2);
        ecO = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i4 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(ExpeditionParamSpeechRequirement.class, "d7b514461f17d860f2ecf8b1e8312255", i4);
        f1549bT = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ExpeditionParamSpeechRequirement.class, "0d5730c9314e197f41f77660c7846141", i5);
        ecP = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ExpeditionParamSpeechRequirement.class, "2e473b1576e5897889bb662218e061c7", i6);
        ecQ = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ExpeditionParamSpeechRequirement.class, "871269904c082d3aba98829ffebe9a7d", i7);
        ecR = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ExpeditionParamSpeechRequirement.class, "bdb344102f96155181c6fff3ba8bc03e", i8);
        ecS = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ExpeditionParamSpeechRequirement.class, C5549aNp.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9486a(ExpeditionMissionInfo pk) {
        bFf().mo5608dq().mo3197f(ecO, pk);
    }

    private int bsE() {
        return bFf().mo5608dq().mo3212n(ecM);
    }

    private ExpeditionMissionInfo bsF() {
        return (ExpeditionMissionInfo) bFf().mo5608dq().mo3214p(ecO);
    }

    /* renamed from: ng */
    private void m9489ng(int i) {
        bFf().mo5608dq().mo3183b(ecM, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5549aNp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return new Boolean(m9487a((C5426aIw) args[0]));
            case 1:
                return new Integer(bsG());
            case 2:
                m9490nh(((Integer) args[0]).intValue());
                return null;
            case 3:
                return bsI();
            case 4:
                m9488b((ExpeditionMissionInfo) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f1549bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1549bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1549bT, new Object[]{aiw}));
                break;
        }
        return m9487a(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Desired Value")
    public int bsH() {
        switch (bFf().mo6893i(ecP)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ecP, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ecP, new Object[0]));
                break;
        }
        return bsG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Expedition Mission Info")
    public ExpeditionMissionInfo bsJ() {
        switch (bFf().mo6893i(ecR)) {
            case 0:
                return null;
            case 2:
                return (ExpeditionMissionInfo) bFf().mo5606d(new aCE(this, ecR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ecR, new Object[0]));
                break;
        }
        return bsI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Expedition Mission Info")
    /* renamed from: c */
    public void mo5430c(ExpeditionMissionInfo pk) {
        switch (bFf().mo6893i(ecS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ecS, new Object[]{pk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ecS, new Object[]{pk}));
                break;
        }
        m9488b(pk);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Desired Value")
    /* renamed from: ni */
    public void mo5431ni(int i) {
        switch (bFf().mo6893i(ecQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ecQ, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ecQ, new Object[]{new Integer(i)}));
                break;
        }
        m9490nh(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "d7b514461f17d860f2ecf8b1e8312255", aum = 0)
    /* renamed from: a */
    private boolean m9487a(C5426aIw aiw) {
        String parameter = aiw.getParameter(bsF().bob());
        if (parameter == null) {
            return false;
        }
        try {
            if (bsE() == Integer.parseInt(parameter) % bsF().bod()) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Desired Value")
    @C0064Am(aul = "0d5730c9314e197f41f77660c7846141", aum = 0)
    private int bsG() {
        return bsE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Desired Value")
    @C0064Am(aul = "2e473b1576e5897889bb662218e061c7", aum = 0)
    /* renamed from: nh */
    private void m9490nh(int i) {
        m9489ng(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Expedition Mission Info")
    @C0064Am(aul = "871269904c082d3aba98829ffebe9a7d", aum = 0)
    private ExpeditionMissionInfo bsI() {
        return bsF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Expedition Mission Info")
    @C0064Am(aul = "bdb344102f96155181c6fff3ba8bc03e", aum = 0)
    /* renamed from: b */
    private void m9488b(ExpeditionMissionInfo pk) {
        m9486a(pk);
    }
}
