package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5523aMp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.are  reason: case insensitive filesystem */
/* compiled from: a */
public class HasRoomForItemSpeechAction extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f5233bT = null;
    /* renamed from: cA */
    public static final C2491fm f5234cA = null;
    /* renamed from: cB */
    public static final C2491fm f5235cB = null;
    /* renamed from: cC */
    public static final C2491fm f5236cC = null;
    /* renamed from: cD */
    public static final C2491fm f5237cD = null;
    /* renamed from: cx */
    public static final C5663aRz f5239cx = null;
    /* renamed from: cz */
    public static final C5663aRz f5241cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "754b5c77dab2d36666b44a1a8de2cc6c", aum = 0)

    /* renamed from: cw */
    private static ItemType f5238cw;
    @C0064Am(aul = "4eb2bd4577cd28c77c377495f6745ba4", aum = 1)

    /* renamed from: cy */
    private static int f5240cy;

    static {
        m25515V();
    }

    public HasRoomForItemSpeechAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HasRoomForItemSpeechAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25515V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 2;
        _m_methodCount = SpeechRequirement._m_methodCount + 5;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(HasRoomForItemSpeechAction.class, "754b5c77dab2d36666b44a1a8de2cc6c", i);
        f5239cx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HasRoomForItemSpeechAction.class, "4eb2bd4577cd28c77c377495f6745ba4", i2);
        f5241cz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i4 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(HasRoomForItemSpeechAction.class, "94ed971a359a68761979d8e6646c4ad4", i4);
        f5236cC = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(HasRoomForItemSpeechAction.class, "e397239f8b4740965ed02f2e0062b8be", i5);
        f5237cD = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(HasRoomForItemSpeechAction.class, "e37c6c93f5df0f685f92cccec0f863b9", i6);
        f5234cA = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(HasRoomForItemSpeechAction.class, "8fe66a206c1c0ecdb6acdfb2b967e685", i7);
        f5235cB = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(HasRoomForItemSpeechAction.class, "1f36b1a9f1fb145de40df626aac0a7b8", i8);
        f5233bT = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HasRoomForItemSpeechAction.class, C5523aMp.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m25516a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f5239cx, jCVar);
    }

    /* renamed from: av */
    private ItemType m25518av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f5239cx);
    }

    /* renamed from: aw */
    private int m25519aw() {
        return bFf().mo5608dq().mo3212n(f5241cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C0064Am(aul = "e397239f8b4740965ed02f2e0062b8be", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m25522b(ItemType jCVar) {
        throw new aWi(new aCE(this, f5237cD, new Object[]{jCVar}));
    }

    /* renamed from: f */
    private void m25523f(int i) {
        bFf().mo5608dq().mo3183b(f5241cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "8fe66a206c1c0ecdb6acdfb2b967e685", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m25524g(int i) {
        throw new aWi(new aCE(this, f5235cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5523aMp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return m25521ay();
            case 1:
                m25522b((ItemType) args[0]);
                return null;
            case 2:
                return new Integer(m25520ax());
            case 3:
                m25524g(((Integer) args[0]).intValue());
                return null;
            case 4:
                return new Boolean(m25517a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    /* renamed from: az */
    public ItemType mo15845az() {
        switch (bFf().mo6893i(f5236cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f5236cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5236cC, new Object[0]));
                break;
        }
        return m25521ay();
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f5233bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5233bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5233bT, new Object[]{aiw}));
                break;
        }
        return m25517a(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C5566aOg
    /* renamed from: c */
    public void mo15846c(ItemType jCVar) {
        switch (bFf().mo6893i(f5237cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5237cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5237cD, new Object[]{jCVar}));
                break;
        }
        m25522b(jCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f5234cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f5234cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5234cA, new Object[0]));
                break;
        }
        return m25520ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f5235cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5235cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5235cB, new Object[]{new Integer(i)}));
                break;
        }
        m25524g(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    @C0064Am(aul = "94ed971a359a68761979d8e6646c4ad4", aum = 0)
    /* renamed from: ay */
    private ItemType m25521ay() {
        return m25518av();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "e37c6c93f5df0f685f92cccec0f863b9", aum = 0)
    /* renamed from: ax */
    private int m25520ax() {
        return m25519aw();
    }

    @C0064Am(aul = "1f36b1a9f1fb145de40df626aac0a7b8", aum = 0)
    /* renamed from: a */
    private boolean m25517a(C5426aIw aiw) {
        return aiw.mo9420h(m25518av(), m25519aw());
    }
}
