package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6881avV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aJk  reason: case insensitive filesystem */
/* compiled from: a */
public class ParamSpeechRequirement extends SpeechRequirement implements C1616Xf {

    /* renamed from: CR */
    public static final C5663aRz f3219CR = null;

    /* renamed from: CS */
    public static final C5663aRz f3220CS = null;

    /* renamed from: CU */
    public static final C2491fm f3221CU = null;

    /* renamed from: CV */
    public static final C2491fm f3222CV = null;

    /* renamed from: CW */
    public static final C2491fm f3223CW = null;

    /* renamed from: CX */
    public static final C2491fm f3224CX = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f3225bT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5d0bcb0d6e3cf4586cd7e5e0924dc6d1", aum = 0)
    private static String key;
    @C0064Am(aul = "cbe3e734a29e654902632c15b6bc9b7e", aum = 1)
    private static String value;

    static {
        m15821V();
    }

    public ParamSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ParamSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15821V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 2;
        _m_methodCount = SpeechRequirement._m_methodCount + 5;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ParamSpeechRequirement.class, "5d0bcb0d6e3cf4586cd7e5e0924dc6d1", i);
        f3219CR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ParamSpeechRequirement.class, "cbe3e734a29e654902632c15b6bc9b7e", i2);
        f3220CS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i4 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(ParamSpeechRequirement.class, "fa9cc4701d7d2a45c0dad2ee05a3eb09", i4);
        f3225bT = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ParamSpeechRequirement.class, "37a8309a48bb2b7a8928b9ad1cb62587", i5);
        f3221CU = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ParamSpeechRequirement.class, "ff2e928329b16f449eb832fd779befcc", i6);
        f3222CV = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ParamSpeechRequirement.class, "543d46d9a666dcdc24ef178edeff7950", i7);
        f3223CW = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ParamSpeechRequirement.class, "8537f9182a004ca676b1cf26f2e62865", i8);
        f3224CX = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ParamSpeechRequirement.class, C6881avV.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m15820U(String str) {
        bFf().mo5608dq().mo3197f(f3219CR, str);
    }

    /* renamed from: V */
    private void m15822V(String str) {
        bFf().mo5608dq().mo3197f(f3220CS, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C0064Am(aul = "ff2e928329b16f449eb832fd779befcc", aum = 0)
    @C5566aOg
    /* renamed from: W */
    private void m15823W(String str) {
        throw new aWi(new aCE(this, f3222CV, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C0064Am(aul = "8537f9182a004ca676b1cf26f2e62865", aum = 0)
    @C5566aOg
    /* renamed from: X */
    private void m15824X(String str) {
        throw new aWi(new aCE(this, f3224CX, new Object[]{str}));
    }

    /* renamed from: lM */
    private String m15826lM() {
        return (String) bFf().mo5608dq().mo3214p(f3219CR);
    }

    /* renamed from: lN */
    private String m15827lN() {
        return (String) bFf().mo5608dq().mo3214p(f3220CS);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6881avV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return new Boolean(m15825a((C5426aIw) args[0]));
            case 1:
                return m15828lO();
            case 2:
                m15823W((String) args[0]);
                return null;
            case 3:
                return m15829lP();
            case 4:
                m15824X((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f3225bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f3225bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3225bT, new Object[]{aiw}));
                break;
        }
        return m15825a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    public String getKey() {
        switch (bFf().mo6893i(f3221CU)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3221CU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3221CU, new Object[0]));
                break;
        }
        return m15828lO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C5566aOg
    public void setKey(String str) {
        switch (bFf().mo6893i(f3222CV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3222CV, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3222CV, new Object[]{str}));
                break;
        }
        m15823W(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    public String getValue() {
        switch (bFf().mo6893i(f3223CW)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3223CW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3223CW, new Object[0]));
                break;
        }
        return m15829lP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C5566aOg
    public void setValue(String str) {
        switch (bFf().mo6893i(f3224CX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3224CX, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3224CX, new Object[]{str}));
                break;
        }
        m15824X(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "fa9cc4701d7d2a45c0dad2ee05a3eb09", aum = 0)
    /* renamed from: a */
    private boolean m15825a(C5426aIw aiw) {
        String parameter = aiw.getParameter(m15826lM());
        if (parameter == null) {
            return false;
        }
        return parameter.equals(m15827lN());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    @C0064Am(aul = "37a8309a48bb2b7a8928b9ad1cb62587", aum = 0)
    /* renamed from: lO */
    private String m15828lO() {
        return m15826lM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    @C0064Am(aul = "543d46d9a666dcdc24ef178edeff7950", aum = 0)
    /* renamed from: lP */
    private String m15829lP() {
        return m15827lN();
    }
}
