package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.corporation.Corporation;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2238dE;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Nv */
/* compiled from: a */
public class IsCEOOfACorporation extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f1259bT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m7735V();
    }

    public IsCEOOfACorporation() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public IsCEOOfACorporation(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7735V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 0;
        _m_methodCount = SpeechRequirement._m_methodCount + 1;
        _m_fields = new C5663aRz[(SpeechRequirement._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(IsCEOOfACorporation.class, "c038bb4f4174bb212d8e2a84a9b40a60", i);
        f1259bT = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(IsCEOOfACorporation.class, C2238dE.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2238dE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return new Boolean(m7736a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f1259bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1259bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1259bT, new Object[]{aiw}));
                break;
        }
        return m7736a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "c038bb4f4174bb212d8e2a84a9b40a60", aum = 0)
    /* renamed from: a */
    private boolean m7736a(C5426aIw aiw) {
        Corporation bYd = aiw.bYd();
        if (bYd == null) {
            return false;
        }
        return bYd.mo10705Qs().getName().equals(aiw.getName());
    }
}
