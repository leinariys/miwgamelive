package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5919acv;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aOx  reason: case insensitive filesystem */
/* compiled from: a */
public class HasActiveShipSpeechRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f3490bT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m16975V();
    }

    public HasActiveShipSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HasActiveShipSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16975V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 0;
        _m_methodCount = SpeechRequirement._m_methodCount + 1;
        _m_fields = new C5663aRz[(SpeechRequirement._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(HasActiveShipSpeechRequirement.class, "4631231048b7b59b37f26538e64fbc33", i);
        f3490bT = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HasActiveShipSpeechRequirement.class, C5919acv.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5919acv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return new Boolean(m16976a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f3490bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f3490bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3490bT, new Object[]{aiw}));
                break;
        }
        return m16976a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "4631231048b7b59b37f26538e64fbc33", aum = 0)
    /* renamed from: a */
    private boolean m16976a(C5426aIw aiw) {
        return aiw.cWR();
    }
}
