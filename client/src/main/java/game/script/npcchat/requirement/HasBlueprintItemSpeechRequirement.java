package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.crafting.CraftingRecipe;
import game.script.item.BlueprintType;
import game.script.item.IngredientsCategory;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C4059yi;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.aBd  reason: case insensitive filesystem */
/* compiled from: a */
public class HasBlueprintItemSpeechRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f2427bT = null;
    public static final C2491fm cNH = null;
    public static final C5663aRz hfW = null;
    public static final C2491fm hfX = null;
    public static final C2491fm hfY = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f15b425e40900e4a925e634506b823ca", aum = 0)
    private static C3438ra<BlueprintType> bJI;

    static {
        m13016V();
    }

    public HasBlueprintItemSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HasBlueprintItemSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13016V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 4;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(HasBlueprintItemSpeechRequirement.class, "f15b425e40900e4a925e634506b823ca", i);
        hfW = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(HasBlueprintItemSpeechRequirement.class, "d92cde5a0c8bdc2df63d9739a72eab9b", i3);
        hfX = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(HasBlueprintItemSpeechRequirement.class, "c90c5914236a1775c8b7d0a8af6b64ba", i4);
        hfY = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(HasBlueprintItemSpeechRequirement.class, "21915764e1dedbb8f013fb595d2e878a", i5);
        cNH = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(HasBlueprintItemSpeechRequirement.class, "c9d8b3b59d83de25d3ecf679d602c126", i6);
        f2427bT = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HasBlueprintItemSpeechRequirement.class, C4059yi.class, _m_fields, _m_methods);
    }

    private C3438ra cIU() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hfW);
    }

    /* renamed from: cm */
    private void m13018cm(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hfW, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4059yi(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                m13019k((BlueprintType) args[0]);
                return null;
            case 1:
                m13020m((BlueprintType) args[0]);
                return null;
            case 2:
                return aKt();
            case 3:
                return new Boolean(m13017a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "BlueprintTypes")
    public List<BlueprintType> aKu() {
        switch (bFf().mo6893i(cNH)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNH, new Object[0]));
                break;
        }
        return aKt();
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f2427bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2427bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2427bT, new Object[]{aiw}));
                break;
        }
        return m13017a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "BlueprintTypes")
    /* renamed from: l */
    public void mo7958l(BlueprintType mr) {
        switch (bFf().mo6893i(hfX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hfX, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hfX, new Object[]{mr}));
                break;
        }
        m13019k(mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "BlueprintTypes")
    /* renamed from: n */
    public void mo7959n(BlueprintType mr) {
        switch (bFf().mo6893i(hfY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hfY, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hfY, new Object[]{mr}));
                break;
        }
        m13020m(mr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "BlueprintTypes")
    @C0064Am(aul = "d92cde5a0c8bdc2df63d9739a72eab9b", aum = 0)
    /* renamed from: k */
    private void m13019k(BlueprintType mr) {
        cIU().add(mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "BlueprintTypes")
    @C0064Am(aul = "c90c5914236a1775c8b7d0a8af6b64ba", aum = 0)
    /* renamed from: m */
    private void m13020m(BlueprintType mr) {
        cIU().remove(mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "BlueprintTypes")
    @C0064Am(aul = "21915764e1dedbb8f013fb595d2e878a", aum = 0)
    private List<BlueprintType> aKt() {
        return Collections.unmodifiableList(cIU());
    }

    @C0064Am(aul = "c9d8b3b59d83de25d3ecf679d602c126", aum = 0)
    /* renamed from: a */
    private boolean m13017a(C5426aIw aiw) {
        for (BlueprintType mr : cIU()) {
            Iterator it = mr.bhW().iterator();
            while (true) {
                if (it.hasNext()) {
                    for (CraftingRecipe next : ((IngredientsCategory) it.next()).apd()) {
                        if (!aiw.mo9405b(next.mo554az(), next.getAmount())) {
                            return false;
                        }
                    }
                    if (!aiw.mo9421kx(mr.bic())) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
