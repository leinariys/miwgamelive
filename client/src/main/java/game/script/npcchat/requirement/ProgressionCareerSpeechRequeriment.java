package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.npcchat.SpeechRequirement;
import game.script.progression.ProgressionCareer;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6367alb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.H */
/* compiled from: a */
public class ProgressionCareerSpeechRequeriment extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f647bT = null;
    /* renamed from: dA */
    public static final C2491fm f648dA = null;
    /* renamed from: du */
    public static final C5663aRz f650du = null;
    /* renamed from: dv */
    public static final C2491fm f651dv = null;
    /* renamed from: dw */
    public static final C2491fm f652dw = null;
    /* renamed from: dz */
    public static final C2491fm f653dz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ff5146a25098dc671ba9defb35207fa3", aum = 0)

    /* renamed from: dt */
    private static C3438ra<ProgressionCareer> f649dt;

    static {
        m3564V();
    }

    public ProgressionCareerSpeechRequeriment() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionCareerSpeechRequeriment(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3564V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 5;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ProgressionCareerSpeechRequeriment.class, "ff5146a25098dc671ba9defb35207fa3", i);
        f650du = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
        C2491fm a = C4105zY.m41624a(ProgressionCareerSpeechRequeriment.class, "99a1a01a051b3c9b8268f313026e3d9f", i3);
        f651dv = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionCareerSpeechRequeriment.class, "a5077af9565562c999a2f9456b4cd46b", i4);
        f652dw = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionCareerSpeechRequeriment.class, "3c5802add50e85dfc62f9d953d3abcbf", i5);
        f653dz = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionCareerSpeechRequeriment.class, "5a92f0b755db1d9d21ee62fe6bb3ea9c", i6);
        f648dA = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionCareerSpeechRequeriment.class, "31df0f4317c45e85ec2f628c77c23100", i7);
        f647bT = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionCareerSpeechRequeriment.class, C6367alb.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m3565a(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f650du, raVar);
    }

    /* renamed from: aB */
    private C3438ra m3568aB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f650du);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6367alb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                m3566a((ProgressionCareer) args[0]);
                return null;
            case 1:
                m3571c((ProgressionCareer) args[0]);
                return null;
            case 2:
                return m3569aC();
            case 3:
                return m3570aE();
            case 4:
                return new Boolean(m3567a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aD */
    public C3438ra<ProgressionCareer> mo2480aD() {
        switch (bFf().mo6893i(f653dz)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, f653dz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f653dz, new Object[0]));
                break;
        }
        return m3569aC();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Careers")
    /* renamed from: aF */
    public List<ProgressionCareer> mo2481aF() {
        switch (bFf().mo6893i(f648dA)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f648dA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f648dA, new Object[0]));
                break;
        }
        return m3570aE();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Careers")
    /* renamed from: b */
    public void mo2482b(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(f651dv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f651dv, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f651dv, new Object[]{sbVar}));
                break;
        }
        m3566a(sbVar);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f647bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f647bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f647bT, new Object[]{aiw}));
                break;
        }
        return m3567a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Careers")
    /* renamed from: d */
    public void mo2484d(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(f652dw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f652dw, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f652dw, new Object[]{sbVar}));
                break;
        }
        m3571c(sbVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Progression Careers")
    @C0064Am(aul = "99a1a01a051b3c9b8268f313026e3d9f", aum = 0)
    /* renamed from: a */
    private void m3566a(ProgressionCareer sbVar) {
        m3568aB().add(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Progression Careers")
    @C0064Am(aul = "a5077af9565562c999a2f9456b4cd46b", aum = 0)
    /* renamed from: c */
    private void m3571c(ProgressionCareer sbVar) {
        m3568aB().remove(sbVar);
    }

    @C0064Am(aul = "3c5802add50e85dfc62f9d953d3abcbf", aum = 0)
    /* renamed from: aC */
    private C3438ra<ProgressionCareer> m3569aC() {
        return m3568aB();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Progression Careers")
    @C0064Am(aul = "5a92f0b755db1d9d21ee62fe6bb3ea9c", aum = 0)
    /* renamed from: aE */
    private List<ProgressionCareer> m3570aE() {
        return Collections.unmodifiableList(m3568aB());
    }

    @C0064Am(aul = "31df0f4317c45e85ec2f628c77c23100", aum = 0)
    /* renamed from: a */
    private boolean m3567a(C5426aIw aiw) {
        return m3568aB().contains(aiw.mo9403Rn());
    }
}
