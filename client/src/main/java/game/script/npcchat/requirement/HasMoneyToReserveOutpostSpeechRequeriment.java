package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechRequirement;
import game.script.ship.OutpostType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1462VW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ayC */
/* compiled from: a */
public class HasMoneyToReserveOutpostSpeechRequeriment extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bJh = null;
    public static final C2491fm bJk = null;
    public static final C2491fm bJl = null;
    /* renamed from: bT */
    public static final C2491fm f5607bT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9ed5011576fba358aecff9935c8d5bfe", aum = 0)
    private static OutpostType bEQ;

    static {
        m27322V();
    }

    public HasMoneyToReserveOutpostSpeechRequeriment() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HasMoneyToReserveOutpostSpeechRequeriment(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27322V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 3;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(HasMoneyToReserveOutpostSpeechRequeriment.class, "9ed5011576fba358aecff9935c8d5bfe", i);
        bJh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(HasMoneyToReserveOutpostSpeechRequeriment.class, "9d237f13ca99a65976568743a3e3ab58", i3);
        bJk = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(HasMoneyToReserveOutpostSpeechRequeriment.class, "016b96f1f74c7a7188534c31ca647289", i4);
        bJl = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(HasMoneyToReserveOutpostSpeechRequeriment.class, "821c1a9665ad0418942209fdc1dadaa4", i5);
        f5607bT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HasMoneyToReserveOutpostSpeechRequeriment.class, C1462VW.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m27323a(OutpostType kz) {
        bFf().mo5608dq().mo3197f(bJh, kz);
    }

    private OutpostType apj() {
        return (OutpostType) bFf().mo5608dq().mo3214p(bJh);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Type")
    @C0064Am(aul = "016b96f1f74c7a7188534c31ca647289", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m27325b(OutpostType kz) {
        throw new aWi(new aCE(this, bJl, new Object[]{kz}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1462VW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return apn();
            case 1:
                m27325b((OutpostType) args[0]);
                return null;
            case 2:
                return new Boolean(m27324a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Type")
    public OutpostType apo() {
        switch (bFf().mo6893i(bJk)) {
            case 0:
                return null;
            case 2:
                return (OutpostType) bFf().mo5606d(new aCE(this, bJk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJk, new Object[0]));
                break;
        }
        return apn();
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f5607bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5607bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5607bT, new Object[]{aiw}));
                break;
        }
        return m27324a(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Type")
    @C5566aOg
    /* renamed from: c */
    public void mo16936c(OutpostType kz) {
        switch (bFf().mo6893i(bJl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJl, new Object[]{kz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJl, new Object[]{kz}));
                break;
        }
        m27325b(kz);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Type")
    @C0064Am(aul = "9d237f13ca99a65976568743a3e3ab58", aum = 0)
    private OutpostType apn() {
        return apj();
    }

    @C0064Am(aul = "821c1a9665ad0418942209fdc1dadaa4", aum = 0)
    /* renamed from: a */
    private boolean m27324a(C5426aIw aiw) {
        return aiw.mo9421kx(apj().mo3587rZ());
    }
}
