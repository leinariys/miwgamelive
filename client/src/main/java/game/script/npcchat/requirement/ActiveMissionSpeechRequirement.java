package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.mission.MissionTemplate;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0227Cr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.apC  reason: case insensitive filesystem */
/* compiled from: a */
public class ActiveMissionSpeechRequirement extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f5084bT = null;
    public static final C5663aRz eRV = null;
    public static final C2491fm gmQ = null;
    public static final C2491fm gmR = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "317605bcec70c23a1c31a214c76511ee", aum = 0)
    private static MissionTemplate baM;

    static {
        m24771V();
    }

    public ActiveMissionSpeechRequirement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ActiveMissionSpeechRequirement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24771V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 3;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ActiveMissionSpeechRequirement.class, "317605bcec70c23a1c31a214c76511ee", i);
        eRV = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(ActiveMissionSpeechRequirement.class, "2b9494d253cf16189eaa71d5746481e9", i3);
        gmQ = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ActiveMissionSpeechRequirement.class, "6c0327b10bca4c846e89791a30e238b0", i4);
        gmR = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ActiveMissionSpeechRequirement.class, "a6e6075cb064be6ab2762083c9d0b7e0", i5);
        f5084bT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ActiveMissionSpeechRequirement.class, C0227Cr.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MissionTemplate")
    @C0064Am(aul = "6c0327b10bca4c846e89791a30e238b0", aum = 0)
    @C5566aOg
    /* renamed from: B */
    private void m24770B(MissionTemplate avh) {
        throw new aWi(new aCE(this, gmR, new Object[]{avh}));
    }

    private MissionTemplate bJH() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(eRV);
    }

    /* renamed from: e */
    private void m24773e(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(eRV, avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MissionTemplate")
    @C5566aOg
    /* renamed from: C */
    public void mo15344C(MissionTemplate avh) {
        switch (bFf().mo6893i(gmR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gmR, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gmR, new Object[]{avh}));
                break;
        }
        m24770B(avh);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0227Cr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return cpW();
            case 1:
                m24770B((MissionTemplate) args[0]);
                return null;
            case 2:
                return new Boolean(m24772a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f5084bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5084bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5084bT, new Object[]{aiw}));
                break;
        }
        return m24772a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MissionTemplate")
    public MissionTemplate cpX() {
        switch (bFf().mo6893i(gmQ)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, gmQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gmQ, new Object[0]));
                break;
        }
        return cpW();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MissionTemplate")
    @C0064Am(aul = "2b9494d253cf16189eaa71d5746481e9", aum = 0)
    private MissionTemplate cpW() {
        return bJH();
    }

    @C0064Am(aul = "a6e6075cb064be6ab2762083c9d0b7e0", aum = 0)
    /* renamed from: a */
    private boolean m24772a(C5426aIw aiw) {
        return aiw.mo9426mk(bJH().getHandle());
    }
}
