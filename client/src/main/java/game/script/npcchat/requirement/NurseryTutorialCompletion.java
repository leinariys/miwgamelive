package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0800La;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.dD */
/* compiled from: a */
public class NurseryTutorialCompletion extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f6440bT = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zM */
    public static final C5663aRz f6442zM = null;
    /* renamed from: zN */
    public static final C2491fm f6443zN = null;
    /* renamed from: zO */
    public static final C2491fm f6444zO = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "25aee06f9ef29c3ea0ead80101e1d524", aum = 0)

    /* renamed from: zL */
    private static aMU f6441zL;

    static {
        m28730V();
    }

    public NurseryTutorialCompletion() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NurseryTutorialCompletion(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28730V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 3;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(NurseryTutorialCompletion.class, "25aee06f9ef29c3ea0ead80101e1d524", i);
        f6442zM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(NurseryTutorialCompletion.class, "f60b2a0ebf64b37289d35f7ae739b1f1", i3);
        f6443zN = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(NurseryTutorialCompletion.class, "968728a6ee1999d44920e5313882f182", i4);
        f6444zO = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(NurseryTutorialCompletion.class, "85595f16c952dced96fbdb2ec8a0eec0", i5);
        f6440bT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NurseryTutorialCompletion.class, C0800La.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m28731a(aMU amu) {
        bFf().mo5608dq().mo3197f(f6442zM, amu);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutorial completion")
    @C0064Am(aul = "968728a6ee1999d44920e5313882f182", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m28733b(aMU amu) {
        throw new aWi(new aCE(this, f6444zO, new Object[]{amu}));
    }

    /* renamed from: jY */
    private aMU m28734jY() {
        return (aMU) bFf().mo5608dq().mo3214p(f6442zM);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0800La(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return m28735jZ();
            case 1:
                m28733b((aMU) args[0]);
                return null;
            case 2:
                return new Boolean(m28732a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f6440bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f6440bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6440bT, new Object[]{aiw}));
                break;
        }
        return m28732a(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutorial completion")
    @C5566aOg
    /* renamed from: c */
    public void mo17728c(aMU amu) {
        switch (bFf().mo6893i(f6444zO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6444zO, new Object[]{amu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6444zO, new Object[]{amu}));
                break;
        }
        m28733b(amu);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutorial completion")
    /* renamed from: ka */
    public aMU mo17729ka() {
        switch (bFf().mo6893i(f6443zN)) {
            case 0:
                return null;
            case 2:
                return (aMU) bFf().mo5606d(new aCE(this, f6443zN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6443zN, new Object[0]));
                break;
        }
        return m28735jZ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutorial completion")
    @C0064Am(aul = "f60b2a0ebf64b37289d35f7ae739b1f1", aum = 0)
    /* renamed from: jZ */
    private aMU m28735jZ() {
        return m28734jY();
    }

    @C0064Am(aul = "85595f16c952dced96fbdb2ec8a0eec0", aum = 0)
    /* renamed from: a */
    private boolean m28732a(C5426aIw aiw) {
        aMU cXm = aiw.cXm();
        return (m28734jY() == null || cXm == null || !m28734jY().equals(cXm)) ? false : true;
    }
}
