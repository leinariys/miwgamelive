package game.script.npcchat.requirement;

import game.network.message.externalizable.aCE;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0921NV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ci */
/* compiled from: a */
public class ProgressionSpeechRequeriment extends SpeechRequirement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bT */
    public static final C2491fm f6211bT = null;
    /* renamed from: qY */
    public static final C5663aRz f6213qY = null;
    /* renamed from: qZ */
    public static final C2491fm f6214qZ = null;
    /* renamed from: ra */
    public static final C2491fm f6215ra = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "23cad2c8fb40ae605b3753f3f9763c14", aum = 0)

    /* renamed from: qX */
    private static int f6212qX;

    static {
        m28582V();
    }

    public ProgressionSpeechRequeriment() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionSpeechRequeriment(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28582V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpeechRequirement._m_fieldCount + 1;
        _m_methodCount = SpeechRequirement._m_methodCount + 3;
        int i = SpeechRequirement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ProgressionSpeechRequeriment.class, "23cad2c8fb40ae605b3753f3f9763c14", i);
        f6213qY = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_fields, (Object[]) _m_fields);
        int i3 = SpeechRequirement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(ProgressionSpeechRequeriment.class, "97f380ea598c39f60444c7d13f7f8029", i3);
        f6214qZ = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionSpeechRequeriment.class, "75805045318bba050152abee86c09093", i4);
        f6215ra = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionSpeechRequeriment.class, "d590e4d0bebb5efa1ba1476384769866", i5);
        f6211bT = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpeechRequirement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionSpeechRequeriment.class, C0921NV.class, _m_fields, _m_methods);
    }

    /* renamed from: aa */
    private void m28584aa(int i) {
        bFf().mo5608dq().mo3183b(f6213qY, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Minimum Challenge Rating")
    @C0064Am(aul = "75805045318bba050152abee86c09093", aum = 0)
    @C5566aOg
    /* renamed from: ab */
    private void m28585ab(int i) {
        throw new aWi(new aCE(this, f6215ra, new Object[]{new Integer(i)}));
    }

    /* renamed from: hG */
    private int m28586hG() {
        return bFf().mo5608dq().mo3212n(f6213qY);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0921NV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpeechRequirement._m_methodCount) {
            case 0:
                return new Integer(m28587hH());
            case 1:
                m28585ab(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Boolean(m28583a((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Minimum Challenge Rating")
    @C5566aOg
    /* renamed from: ac */
    public void mo17677ac(int i) {
        switch (bFf().mo6893i(f6215ra)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6215ra, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6215ra, new Object[]{new Integer(i)}));
                break;
        }
        m28585ab(i);
    }

    /* renamed from: b */
    public boolean mo2483b(C5426aIw aiw) {
        switch (bFf().mo6893i(f6211bT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f6211bT, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6211bT, new Object[]{aiw}));
                break;
        }
        return m28583a(aiw);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Minimum Challenge Rating")
    /* renamed from: hI */
    public int mo17678hI() {
        switch (bFf().mo6893i(f6214qZ)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f6214qZ, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6214qZ, new Object[0]));
                break;
        }
        return m28587hH();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Minimum Challenge Rating")
    @C0064Am(aul = "97f380ea598c39f60444c7d13f7f8029", aum = 0)
    /* renamed from: hH */
    private int m28587hH() {
        return m28586hG();
    }

    @C0064Am(aul = "d590e4d0bebb5efa1ba1476384769866", aum = 0)
    /* renamed from: a */
    private boolean m28583a(C5426aIw aiw) {
        return aiw.mo9423lt() >= mo17678hI();
    }
}
