package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C3224pS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.anT  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class SpeechRequirementCollection extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDQ = null;
    /* renamed from: bJ */
    public static final C5663aRz f4956bJ = null;
    /* renamed from: bL */
    public static final C5663aRz f4958bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4959bM = null;
    /* renamed from: bN */
    public static final C2491fm f4960bN = null;
    /* renamed from: bO */
    public static final C2491fm f4961bO = null;
    /* renamed from: bP */
    public static final C2491fm f4962bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4963bQ = null;
    /* renamed from: bR */
    public static final C2491fm f4964bR = null;
    /* renamed from: bS */
    public static final C2491fm f4965bS = null;
    public static final C2491fm gfA = null;
    public static final C2491fm gfB = null;
    public static final C2491fm gfC = null;
    public static final C2491fm gfD = null;
    public static final C2491fm gfE = null;
    public static final C2491fm gfF = null;
    public static final C5663aRz gfy = null;
    public static final C5663aRz gfz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9f6af750c66070889a21f96a2fbc4e97", aum = 0)
    private static C3438ra<SpeechRequirement> aTP;
    @C0064Am(aul = "f5c58dc70a6d12c0eb3856d61035b1e3", aum = 1)
    private static C3438ra<SpeechRequirementCollection> aTQ;
    @C0064Am(aul = "32aa093cf320cf3d030ec68011e1d2c6", aum = 2)

    /* renamed from: bI */
    private static AbstractSpeech f4955bI;
    @C0064Am(aul = "92653b6948371846d7e447d1334cbef7", aum = 3)

    /* renamed from: bK */
    private static UUID f4957bK;
    @C0064Am(aul = "a13e4c62ad8d277894a95070d15c616e", aum = 4)
    private static String handle;

    static {
        m24160V();
    }

    public SpeechRequirementCollection() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeechRequirementCollection(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24160V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(SpeechRequirementCollection.class, "9f6af750c66070889a21f96a2fbc4e97", i);
        gfy = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpeechRequirementCollection.class, "f5c58dc70a6d12c0eb3856d61035b1e3", i2);
        gfz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpeechRequirementCollection.class, "32aa093cf320cf3d030ec68011e1d2c6", i3);
        f4956bJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpeechRequirementCollection.class, "92653b6948371846d7e447d1334cbef7", i4);
        f4958bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpeechRequirementCollection.class, "a13e4c62ad8d277894a95070d15c616e", i5);
        f4959bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 13)];
        C2491fm a = C4105zY.m41624a(SpeechRequirementCollection.class, "122d37c0bfdbe20eb3e805baf82d87e0", i7);
        f4960bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(SpeechRequirementCollection.class, "9863dab83c50bdca6ef0e9013576d114", i8);
        f4961bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeechRequirementCollection.class, "23706e3bd1b0adfa2e873e04cb8b75cd", i9);
        f4962bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeechRequirementCollection.class, "7aafacfaf57d37611e3c7d3bfcc6c172", i10);
        f4963bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeechRequirementCollection.class, "23455fce4c881fd780b74600863a4d02", i11);
        f4964bR = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(SpeechRequirementCollection.class, "c32f97233d98cc469743b83cc6839652", i12);
        f4965bS = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(SpeechRequirementCollection.class, "24631cafe645a183747d57329e25ddd0", i13);
        gfA = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(SpeechRequirementCollection.class, "892155a1eb226753ebad0c6193600c4b", i14);
        gfB = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(SpeechRequirementCollection.class, "8fe6a571dec04c3064a5a9922f2833e6", i15);
        gfC = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(SpeechRequirementCollection.class, "79f49fd808082cd6e4beaa00df1e0de3", i16);
        gfD = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(SpeechRequirementCollection.class, "6447208eeeeb0ef66fa65807ab253165", i17);
        gfE = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(SpeechRequirementCollection.class, "7f8f6685d3f3d6f13fabb0d4bbebe403", i18);
        gfF = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(SpeechRequirementCollection.class, "2df17e41f9a05a34ec602cc48925a442", i19);
        aDQ = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeechRequirementCollection.class, C3224pS.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m24161a(AbstractSpeech aht) {
        bFf().mo5608dq().mo3197f(f4956bJ, aht);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Requirements")
    @C0064Am(aul = "24631cafe645a183747d57329e25ddd0", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m24162a(SpeechRequirement xVar) {
        throw new aWi(new aCE(this, gfA, new Object[]{xVar}));
    }

    /* renamed from: a */
    private void m24163a(String str) {
        bFf().mo5608dq().mo3197f(f4959bM, str);
    }

    /* renamed from: a */
    private void m24164a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4958bL, uuid);
    }

    /* renamed from: am */
    private AbstractSpeech m24165am() {
        return (AbstractSpeech) bFf().mo5608dq().mo3214p(f4956bJ);
    }

    /* renamed from: an */
    private UUID m24166an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4958bL);
    }

    /* renamed from: ao */
    private String m24167ao() {
        return (String) bFf().mo5608dq().mo3214p(f4959bM);
    }

    @C0064Am(aul = "23455fce4c881fd780b74600863a4d02", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m24171b(AbstractSpeech aht) {
        throw new aWi(new aCE(this, f4964bR, new Object[]{aht}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "7aafacfaf57d37611e3c7d3bfcc6c172", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m24172b(String str) {
        throw new aWi(new aCE(this, f4963bQ, new Object[]{str}));
    }

    /* renamed from: bX */
    private void m24174bX(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gfy, raVar);
    }

    /* renamed from: bY */
    private void m24175bY(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gfz, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Requirement Collections")
    @C0064Am(aul = "79f49fd808082cd6e4beaa00df1e0de3", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m24176c(SpeechRequirementCollection ant) {
        throw new aWi(new aCE(this, gfD, new Object[]{ant}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Requirements")
    @C0064Am(aul = "892155a1eb226753ebad0c6193600c4b", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m24177c(SpeechRequirement xVar) {
        throw new aWi(new aCE(this, gfB, new Object[]{xVar}));
    }

    /* renamed from: c */
    private void m24178c(UUID uuid) {
        switch (bFf().mo6893i(f4961bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4961bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4961bO, new Object[]{uuid}));
                break;
        }
        m24173b(uuid);
    }

    private C3438ra clZ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gfy);
    }

    private C3438ra cma() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gfz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Requirement Collections")
    @C0064Am(aul = "6447208eeeeb0ef66fa65807ab253165", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m24179e(SpeechRequirementCollection ant) {
        throw new aWi(new aCE(this, gfE, new Object[]{ant}));
    }

    @C0064Am(aul = "2df17e41f9a05a34ec602cc48925a442", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private boolean m24180e(C5426aIw aiw) {
        throw new aWi(new aCE(this, aDQ, new Object[]{aiw}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3224pS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m24168ap();
            case 1:
                m24173b((UUID) args[0]);
                return null;
            case 2:
                return m24169ar();
            case 3:
                m24172b((String) args[0]);
                return null;
            case 4:
                m24171b((AbstractSpeech) args[0]);
                return null;
            case 5:
                return m24170as();
            case 6:
                m24162a((SpeechRequirement) args[0]);
                return null;
            case 7:
                m24177c((SpeechRequirement) args[0]);
                return null;
            case 8:
                return cmb();
            case 9:
                m24176c((SpeechRequirementCollection) args[0]);
                return null;
            case 10:
                m24179e((SpeechRequirementCollection) args[0]);
                return null;
            case 11:
                return cmd();
            case 12:
                return new Boolean(m24180e((C5426aIw) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4960bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4960bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4960bN, new Object[0]));
                break;
        }
        return m24168ap();
    }

    /* renamed from: at */
    public final AbstractSpeech mo14973at() {
        switch (bFf().mo6893i(f4965bS)) {
            case 0:
                return null;
            case 2:
                return (AbstractSpeech) bFf().mo5606d(new aCE(this, f4965bS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4965bS, new Object[0]));
                break;
        }
        return m24170as();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Requirements")
    @C5566aOg
    /* renamed from: b */
    public void mo14974b(SpeechRequirement xVar) {
        switch (bFf().mo6893i(gfA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gfA, new Object[]{xVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gfA, new Object[]{xVar}));
                break;
        }
        m24162a(xVar);
    }

    @C5566aOg
    /* renamed from: c */
    public final void mo14975c(AbstractSpeech aht) {
        switch (bFf().mo6893i(f4964bR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4964bR, new Object[]{aht}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4964bR, new Object[]{aht}));
                break;
        }
        m24171b(aht);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Requirements")
    public List<SpeechRequirement> cmc() {
        switch (bFf().mo6893i(gfC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gfC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gfC, new Object[0]));
                break;
        }
        return cmb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Requirement Collections")
    public List<SpeechRequirementCollection> cme() {
        switch (bFf().mo6893i(gfF)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gfF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gfF, new Object[0]));
                break;
        }
        return cmd();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Requirement Collections")
    @C5566aOg
    /* renamed from: d */
    public void mo14978d(SpeechRequirementCollection ant) {
        switch (bFf().mo6893i(gfD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gfD, new Object[]{ant}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gfD, new Object[]{ant}));
                break;
        }
        m24176c(ant);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Requirements")
    @C5566aOg
    /* renamed from: d */
    public void mo14979d(SpeechRequirement xVar) {
        switch (bFf().mo6893i(gfB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gfB, new Object[]{xVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gfB, new Object[]{xVar}));
                break;
        }
        m24177c(xVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Requirement Collections")
    @C5566aOg
    /* renamed from: f */
    public void mo14980f(SpeechRequirementCollection ant) {
        switch (bFf().mo6893i(gfE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gfE, new Object[]{ant}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gfE, new Object[]{ant}));
                break;
        }
        m24179e(ant);
    }

    @C5566aOg
    /* renamed from: f */
    public boolean mo14981f(C5426aIw aiw) {
        switch (bFf().mo6893i(aDQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aDQ, new Object[]{aiw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aDQ, new Object[]{aiw}));
                break;
        }
        return m24180e(aiw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4962bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4962bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4962bP, new Object[0]));
                break;
        }
        return m24169ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4963bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4963bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4963bQ, new Object[]{str}));
                break;
        }
        m24172b(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m24164a(UUID.randomUUID());
    }

    @C0064Am(aul = "122d37c0bfdbe20eb3e805baf82d87e0", aum = 0)
    /* renamed from: ap */
    private UUID m24168ap() {
        return m24166an();
    }

    @C0064Am(aul = "9863dab83c50bdca6ef0e9013576d114", aum = 0)
    /* renamed from: b */
    private void m24173b(UUID uuid) {
        m24164a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "23706e3bd1b0adfa2e873e04cb8b75cd", aum = 0)
    /* renamed from: ar */
    private String m24169ar() {
        return m24167ao();
    }

    @C0064Am(aul = "c32f97233d98cc469743b83cc6839652", aum = 0)
    /* renamed from: as */
    private AbstractSpeech m24170as() {
        return m24165am();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Requirements")
    @C0064Am(aul = "8fe6a571dec04c3064a5a9922f2833e6", aum = 0)
    private List<SpeechRequirement> cmb() {
        return Collections.unmodifiableList(clZ());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Requirement Collections")
    @C0064Am(aul = "7f8f6685d3f3d6f13fabb0d4bbebe403", aum = 0)
    private List<SpeechRequirementCollection> cmd() {
        return Collections.unmodifiableList(cma());
    }
}
