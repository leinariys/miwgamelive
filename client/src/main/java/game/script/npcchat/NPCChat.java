package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.aAE;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.aaQ  reason: case insensitive filesystem */
/* compiled from: a */
public class NPCChat extends TaikodomObject implements C0468GU, C1616Xf, C7022azj {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4059bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4060bM = null;
    /* renamed from: bN */
    public static final C2491fm f4061bN = null;
    /* renamed from: bO */
    public static final C2491fm f4062bO = null;
    /* renamed from: bP */
    public static final C2491fm f4063bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4064bQ = null;
    public static final C2491fm chL = null;
    public static final C2491fm chM = null;
    public static final C2491fm chN = null;
    public static final C5663aRz geu = null;
    public static final C2491fm gev = null;
    public static final C2491fm gew = null;
    public static final C2491fm gex = null;
    public static final C2491fm gey = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "897827c4a52263986eab69ee03ae5f7a", aum = 1)

    /* renamed from: bK */
    private static UUID f4058bK;
    @C0064Am(aul = "8c445ee95fe521d52cbb444c5f899cb6", aum = 0)
    private static C3438ra<NPCSpeech> get;
    @C0064Am(aul = "f88dd54f32071278882ab2612f177966", aum = 2)
    private static String handle;

    static {
        m19440V();
    }

    public NPCChat() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCChat(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19440V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(NPCChat.class, "8c445ee95fe521d52cbb444c5f899cb6", i);
        geu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCChat.class, "897827c4a52263986eab69ee03ae5f7a", i2);
        f4059bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPCChat.class, "f88dd54f32071278882ab2612f177966", i3);
        f4060bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 12)];
        C2491fm a = C4105zY.m41624a(NPCChat.class, "cc6cca06734e8db480eceffac8a6a3c8", i5);
        f4061bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCChat.class, "deeb4546982b2973d566b02f8932f9e3", i6);
        f4062bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCChat.class, "be6759cf99c576248f5d760db1ae9dcd", i7);
        f4063bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCChat.class, "4955b2db91179627be20be979a8bf72f", i8);
        f4064bQ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCChat.class, "2c7b901939808c5e637160e088e0aee6", i9);
        gev = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCChat.class, "02d37752dce0a6109e82f22d37965125", i10);
        gew = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCChat.class, "f794cc42544d2289035401568d6212ad", i11);
        gex = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCChat.class, "015b193747dfb4ac1848f127276f890e", i12);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(NPCChat.class, "5b5908832626ecc923a2becea06e5852", i13);
        chL = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(NPCChat.class, "3d2394e183511f8e1e55fa0af14f44a1", i14);
        gey = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(NPCChat.class, "b2187cd6fb94c2fb335626023cc3e876", i15);
        chM = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(NPCChat.class, "f9c97d0da71e13a7d77cc226814ee33f", i16);
        chN = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCChat.class, aAE.class, _m_fields, _m_methods);
    }

    @C5566aOg
    /* renamed from: n */
    private static void m19456n(List<NPCSpeech> list) {
        if (list.isEmpty()) {
            throw new IllegalArgumentException("The speech list is empty");
        } else if (list.size() > 1) {
            throw new IllegalStateException("The speech list has more than one object");
        }
    }

    @C0064Am(aul = "b2187cd6fb94c2fb335626023cc3e876", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private List<PlayerSpeech> m19441a(NPCSpeech agl, Player aku) {
        throw new aWi(new aCE(this, chM, new Object[]{agl, aku}));
    }

    /* renamed from: a */
    private void m19442a(String str) {
        bFf().mo5608dq().mo3197f(f4060bM, str);
    }

    /* renamed from: a */
    private void m19443a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4059bL, uuid);
    }

    /* renamed from: an */
    private UUID m19444an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4059bL);
    }

    /* renamed from: ao */
    private String m19445ao() {
        return (String) bFf().mo5608dq().mo3214p(f4060bM);
    }

    @C5566aOg
    @C0064Am(aul = "5b5908832626ecc923a2becea06e5852", aum = 0)
    @C2499fr(mo18857qh = 1)
    private Map<NPCSpeech, List<PlayerSpeech>> avS() {
        throw new aWi(new aCE(this, chL, new Object[0]));
    }

    @C5566aOg
    /* renamed from: b */
    private List<PlayerSpeech> m19449b(NPCSpeech agl, Player aku) {
        switch (bFf().mo6893i(chM)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, chM, new Object[]{agl, aku}));
            case 3:
                bFf().mo5606d(new aCE(this, chM, new Object[]{agl, aku}));
                break;
        }
        return m19441a(agl, aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "4955b2db91179627be20be979a8bf72f", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m19450b(String str) {
        throw new aWi(new aCE(this, f4064bQ, new Object[]{str}));
    }

    /* renamed from: bW */
    private void m19452bW(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(geu, raVar);
    }

    /* renamed from: c */
    private void m19453c(UUID uuid) {
        switch (bFf().mo6893i(f4062bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4062bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4062bO, new Object[]{uuid}));
                break;
        }
        m19451b(uuid);
    }

    private C3438ra clD() {
        return (C3438ra) bFf().mo5608dq().mo3214p(geu);
    }

    @C5566aOg
    @C0064Am(aul = "3d2394e183511f8e1e55fa0af14f44a1", aum = 0)
    @C2499fr(mo18857qh = 1)
    private List<NPCSpeech> clG() {
        throw new aWi(new aCE(this, gey, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Root Speeches")
    @C0064Am(aul = "02d37752dce0a6109e82f22d37965125", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m19454e(NPCSpeech agl) {
        throw new aWi(new aCE(this, gew, new Object[]{agl}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Root Speeches")
    @C0064Am(aul = "f794cc42544d2289035401568d6212ad", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m19455g(NPCSpeech agl) {
        throw new aWi(new aCE(this, gex, new Object[]{agl}));
    }

    @C0064Am(aul = "f9c97d0da71e13a7d77cc226814ee33f", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m19457o(List<? extends PlayerSpeech> list) {
        throw new aWi(new aCE(this, chN, new Object[]{list}));
    }

    @C5566aOg
    /* renamed from: p */
    private void m19458p(List<? extends PlayerSpeech> list) {
        switch (bFf().mo6893i(chN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chN, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chN, new Object[]{list}));
                break;
        }
        m19457o(list);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aAE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m19446ap();
            case 1:
                m19451b((UUID) args[0]);
                return null;
            case 2:
                return m19447ar();
            case 3:
                m19450b((String) args[0]);
                return null;
            case 4:
                return clE();
            case 5:
                m19454e((NPCSpeech) args[0]);
                return null;
            case 6:
                m19455g((NPCSpeech) args[0]);
                return null;
            case 7:
                return m19448au();
            case 8:
                return avS();
            case 9:
                return clG();
            case 10:
                return m19441a((NPCSpeech) args[0], (Player) args[1]);
            case 11:
                m19457o((List) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4061bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4061bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4061bN, new Object[0]));
                break;
        }
        return m19446ap();
    }

    @C5566aOg
    @C2499fr(mo18857qh = 1)
    public Map<NPCSpeech, List<PlayerSpeech>> avT() {
        switch (bFf().mo6893i(chL)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, chL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, chL, new Object[0]));
                break;
        }
        return avS();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Root Speeches")
    public List<NPCSpeech> clF() {
        switch (bFf().mo6893i(gev)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gev, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gev, new Object[0]));
                break;
        }
        return clE();
    }

    @C5566aOg
    @C2499fr(mo18857qh = 1)
    public List<NPCSpeech> clH() {
        switch (bFf().mo6893i(gey)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gey, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gey, new Object[0]));
                break;
        }
        return clG();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Root Speeches")
    @C5566aOg
    /* renamed from: f */
    public void mo12197f(NPCSpeech agl) {
        switch (bFf().mo6893i(gew)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gew, new Object[]{agl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gew, new Object[]{agl}));
                break;
        }
        m19454e(agl);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4063bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4063bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4063bP, new Object[0]));
                break;
        }
        return m19447ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4064bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4064bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4064bQ, new Object[]{str}));
                break;
        }
        m19450b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Root Speeches")
    @C5566aOg
    /* renamed from: h */
    public void mo12198h(NPCSpeech agl) {
        switch (bFf().mo6893i(gex)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gex, new Object[]{agl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gex, new Object[]{agl}));
                break;
        }
        m19455g(agl);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m19448au();
    }

    @C0064Am(aul = "cc6cca06734e8db480eceffac8a6a3c8", aum = 0)
    /* renamed from: ap */
    private UUID m19446ap() {
        return m19444an();
    }

    @C0064Am(aul = "deeb4546982b2973d566b02f8932f9e3", aum = 0)
    /* renamed from: b */
    private void m19451b(UUID uuid) {
        m19443a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m19443a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "be6759cf99c576248f5d760db1ae9dcd", aum = 0)
    /* renamed from: ar */
    private String m19447ar() {
        return m19445ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Root Speeches")
    @C0064Am(aul = "2c7b901939808c5e637160e088e0aee6", aum = 0)
    private List<NPCSpeech> clE() {
        return Collections.unmodifiableList(clD());
    }

    @C0064Am(aul = "015b193747dfb4ac1848f127276f890e", aum = 0)
    /* renamed from: au */
    private String m19448au() {
        return "NPC Chat";
    }

    /* renamed from: a.aaQ$a */
    class C1842a implements Comparator<NPCSpeech> {
        C1842a() {
        }

        /* renamed from: a */
        public int compare(NPCSpeech agl, NPCSpeech agl2) {
            int compareTo = Integer.valueOf(agl.getPriority()).compareTo(Integer.valueOf(agl2.getPriority()));
            return compareTo != 0 ? compareTo : agl.ddT().get().compareTo(agl2.ddT().get());
        }
    }

    /* renamed from: a.aaQ$b */
    /* compiled from: a */
    class C1843b implements Comparator<PlayerSpeech> {
        C1843b() {
        }

        /* renamed from: a */
        public int compare(PlayerSpeech av, PlayerSpeech av2) {
            return Integer.valueOf(av.avR()).compareTo(Integer.valueOf(av2.avR()));
        }
    }
}
