package game.script.npcchat;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C0980OQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@C5829abJ("1.1.0")
@C5511aMd
@C6485anp
/* renamed from: a.aHT */
/* compiled from: a */
public abstract class AbstractSpeech extends TaikodomObject implements C0468GU, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f2919Do = null;

    /* renamed from: Lm */
    public static final C2491fm f2920Lm = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bJ */
    public static final C5663aRz f2921bJ = null;
    /* renamed from: bL */
    public static final C5663aRz f2923bL = null;
    /* renamed from: bM */
    public static final C5663aRz f2924bM = null;
    /* renamed from: bN */
    public static final C2491fm f2925bN = null;
    /* renamed from: bO */
    public static final C2491fm f2926bO = null;
    /* renamed from: bP */
    public static final C2491fm f2927bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2928bQ = null;
    public static final C5663aRz hYZ = null;
    public static final C2491fm hZa = null;
    public static final C2491fm hZb = null;
    public static final C2491fm hZc = null;
    public static final C2491fm hZd = null;
    public static final C2491fm hZe = null;
    public static final long serialVersionUID = 0;
    public static final DecimalFormat hYY = new DecimalFormat("00000");
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4eb290bac48a56c1431447b51b1e5857", aum = 3)

    /* renamed from: bK */
    private static UUID f2922bK;
    @C0064Am(aul = "7682579a7b6a3e977c972cce382a5b9d", aum = 1)
    private static I18NString dOM;
    @C0064Am(aul = "a420fc3bca8051d0204e38478d3561d8", aum = 2)
    private static SpeechRequirementCollection dON;
    @C0064Am(aul = "38d3a224b21ef9540e936914a7840a39", aum = 0)
    private static String handle;

    static {
        m15176V();
    }

    public AbstractSpeech() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AbstractSpeech(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15176V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(AbstractSpeech.class, "38d3a224b21ef9540e936914a7840a39", i);
        f2924bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AbstractSpeech.class, "7682579a7b6a3e977c972cce382a5b9d", i2);
        f2921bJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AbstractSpeech.class, "a420fc3bca8051d0204e38478d3561d8", i3);
        hYZ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AbstractSpeech.class, "4eb290bac48a56c1431447b51b1e5857", i4);
        f2923bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 11)];
        C2491fm a = C4105zY.m41624a(AbstractSpeech.class, "86a311503294a85bda4fad41e56665f3", i6);
        f2925bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(AbstractSpeech.class, "bb6f7f2a5a9ec6e0b7db5830f7372145", i7);
        f2926bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(AbstractSpeech.class, "11352f9000924bdf7f86b00d9a1d57de", i8);
        hZa = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(AbstractSpeech.class, "372488b1563311884b3d8f429c4beb56", i9);
        f2927bP = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(AbstractSpeech.class, "389c45274212206b5fe300614a51c3b5", i10);
        f2928bQ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(AbstractSpeech.class, "2ca96c2742d70458cbe03d16c8e0706c", i11);
        hZb = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(AbstractSpeech.class, "eabd4b3bb3b7a7fdaaf5c3098c0f1f64", i12);
        hZc = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(AbstractSpeech.class, "dea2ed3fd1d408947ccc72efd98b5f71", i13);
        hZd = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(AbstractSpeech.class, "01f26b237c440d09765995102439ab84", i14);
        hZe = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(AbstractSpeech.class, "52f0c865436b943134197ad9bffa1de4", i15);
        f2919Do = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(AbstractSpeech.class, "c59af44434bd1745c27689ecc412f7d8", i16);
        f2920Lm = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AbstractSpeech.class, C0980OQ.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m15178a(String str) {
        bFf().mo5608dq().mo3197f(f2924bM, str);
    }

    /* renamed from: a */
    private void m15179a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2923bL, uuid);
    }

    /* renamed from: an */
    private UUID m15180an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2923bL);
    }

    /* renamed from: ao */
    private String m15181ao() {
        return (String) bFf().mo5608dq().mo3214p(f2924bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "389c45274212206b5fe300614a51c3b5", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m15184b(String str) {
        throw new aWi(new aCE(this, f2928bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m15186c(UUID uuid) {
        switch (bFf().mo6893i(f2926bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2926bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2926bO, new Object[]{uuid}));
                break;
        }
        m15185b(uuid);
    }

    @C0064Am(aul = "11352f9000924bdf7f86b00d9a1d57de", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: cu */
    private boolean m15187cu(Player aku) {
        throw new aWi(new aCE(this, hZa, new Object[]{aku}));
    }

    private I18NString ddQ() {
        return (I18NString) bFf().mo5608dq().mo3214p(f2921bJ);
    }

    private SpeechRequirementCollection ddR() {
        return (SpeechRequirementCollection) bFf().mo5608dq().mo3214p(hYZ);
    }

    /* renamed from: g */
    private void m15188g(SpeechRequirementCollection ant) {
        bFf().mo5608dq().mo3197f(hYZ, ant);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Requirement Collection")
    @C0064Am(aul = "01f26b237c440d09765995102439ab84", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m15189h(SpeechRequirementCollection ant) {
        throw new aWi(new aCE(this, hZe, new Object[]{ant}));
    }

    /* renamed from: pI */
    private void m15190pI(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f2921bJ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech")
    @C0064Am(aul = "eabd4b3bb3b7a7fdaaf5c3098c0f1f64", aum = 0)
    @C5566aOg
    /* renamed from: pJ */
    private void m15191pJ(I18NString i18NString) {
        throw new aWi(new aCE(this, hZc, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0980OQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m15182ap();
            case 1:
                m15185b((UUID) args[0]);
                return null;
            case 2:
                return new Boolean(m15187cu((Player) args[0]));
            case 3:
                return m15183ar();
            case 4:
                m15184b((String) args[0]);
                return null;
            case 5:
                return ddS();
            case 6:
                m15191pJ((I18NString) args[0]);
                return null;
            case 7:
                return ddU();
            case 8:
                m15189h((SpeechRequirementCollection) args[0]);
                return null;
            case 9:
                m15177a((C0665JT) args[0]);
                return null;
            case 10:
                m15192qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2925bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2925bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2925bN, new Object[0]));
                break;
        }
        return m15182ap();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2919Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2919Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2919Do, new Object[]{jt}));
                break;
        }
        m15177a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: cv */
    public boolean mo9210cv(Player aku) {
        switch (bFf().mo6893i(hZa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hZa, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hZa, new Object[]{aku}));
                break;
        }
        return m15187cu(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech")
    public I18NString ddT() {
        switch (bFf().mo6893i(hZb)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hZb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hZb, new Object[0]));
                break;
        }
        return ddS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Requirement Collection")
    public SpeechRequirementCollection ddV() {
        switch (bFf().mo6893i(hZd)) {
            case 0:
                return null;
            case 2:
                return (SpeechRequirementCollection) bFf().mo5606d(new aCE(this, hZd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hZd, new Object[0]));
                break;
        }
        return ddU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2927bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2927bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2927bP, new Object[0]));
                break;
        }
        return m15183ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2928bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2928bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2928bQ, new Object[]{str}));
                break;
        }
        m15184b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Requirement Collection")
    @C5566aOg
    /* renamed from: i */
    public void mo9213i(SpeechRequirementCollection ant) {
        switch (bFf().mo6893i(hZe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hZe, new Object[]{ant}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hZe, new Object[]{ant}));
                break;
        }
        m15189h(ant);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech")
    @C5566aOg
    /* renamed from: pK */
    public void mo9214pK(I18NString i18NString) {
        switch (bFf().mo6893i(hZc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hZc, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hZc, new Object[]{i18NString}));
                break;
        }
        m15191pJ(i18NString);
    }

    /* renamed from: qV */
    public void mo9215qV() {
        switch (bFf().mo6893i(f2920Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2920Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2920Lm, new Object[0]));
                break;
        }
        m15192qU();
    }

    @C0064Am(aul = "86a311503294a85bda4fad41e56665f3", aum = 0)
    /* renamed from: ap */
    private UUID m15182ap() {
        return m15180an();
    }

    @C0064Am(aul = "bb6f7f2a5a9ec6e0b7db5830f7372145", aum = 0)
    /* renamed from: b */
    private void m15185b(UUID uuid) {
        m15179a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m15179a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "372488b1563311884b3d8f429c4beb56", aum = 0)
    /* renamed from: ar */
    private String m15183ar() {
        return m15181ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech")
    @C0064Am(aul = "2ca96c2742d70458cbe03d16c8e0706c", aum = 0)
    private I18NString ddS() {
        return ddQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Requirement Collection")
    @C0064Am(aul = "dea2ed3fd1d408947ccc72efd98b5f71", aum = 0)
    private SpeechRequirementCollection ddU() {
        return ddR();
    }

    @C0064Am(aul = "52f0c865436b943134197ad9bffa1de4", aum = 0)
    /* renamed from: a */
    private void m15177a(C0665JT jt) {
        List<C0665JT> list;
        if (jt.mo3117j(1, 1, 0) && (list = (List) jt.get("requirements")) != null && list.size() > 0) {
            ANDSpeechRequirementCollection ash = (ANDSpeechRequirementCollection) bFf().mo6865M(ANDSpeechRequirementCollection.class);
            ash.mo10S();
            m15188g(ash);
            for (C0665JT baw : list) {
                ddR().mo14974b((SpeechRequirement) baw.baw());
            }
        }
    }

    @C0064Am(aul = "c59af44434bd1745c27689ecc412f7d8", aum = 0)
    /* renamed from: qU */
    private void m15192qU() {
        super.push();
        ddR().push();
        for (SpeechRequirement push : ddR().cmc()) {
            push.push();
        }
        for (SpeechRequirementCollection push2 : ddR().cme()) {
            push2.push();
        }
    }
}
