package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Trigger;
import game.script.pda.TaikopediaEntry;
import game.script.space.StellarSystem;
import game.script.template.BaseTaikodomContent;
import logic.aaa.C0284Df;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.bbb.C6348alI;
import logic.data.mbean.C6979axs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5829abJ("1.2.1")
@C6485anp
@C2712iu(mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
/* renamed from: a.ld */
/* compiled from: a */
public abstract class SpaceZone extends Trigger implements C0468GU, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f8561Do = null;

    /* renamed from: Lc */
    public static final C2491fm f8562Lc = null;

    /* renamed from: Ld */
    public static final C2491fm f8563Ld = null;

    /* renamed from: Le */
    public static final C2491fm f8564Le = null;

    /* renamed from: Mq */
    public static final C2491fm f8565Mq = null;

    /* renamed from: Wp */
    public static final C2491fm f8566Wp = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm awA = null;
    public static final C2491fm awB = null;
    public static final C2491fm awC = null;
    public static final C2491fm awD = null;
    public static final C2491fm awE = null;
    public static final C5663aRz awd = null;
    public static final C5663aRz awg = null;
    public static final C5663aRz awh = null;
    public static final C5663aRz awj = null;
    public static final C5663aRz awl = null;
    public static final C5663aRz awn = null;
    public static final C2491fm awo = null;
    public static final C2491fm awp = null;
    public static final C2491fm awq = null;
    public static final C2491fm awr = null;
    public static final C2491fm aws = null;
    public static final C2491fm awt = null;
    public static final C2491fm awu = null;
    public static final C2491fm awv = null;
    public static final C2491fm aww = null;
    public static final C2491fm awx = null;
    public static final C2491fm awy = null;
    public static final C2491fm awz = null;
    /* renamed from: bL */
    public static final C5663aRz f8568bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8569bM = null;
    /* renamed from: bN */
    public static final C2491fm f8570bN = null;
    /* renamed from: bO */
    public static final C2491fm f8571bO = null;
    /* renamed from: bP */
    public static final C2491fm f8572bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8573bQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vi */
    public static final C2491fm f8575vi = null;
    /* renamed from: vs */
    public static final C2491fm f8576vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "33897cf41f4553f09c3da65005b44972", aum = 2)
    private static C3438ra<SpaceZoneEvent> awf;
    @C0064Am(aul = "8c5602f7876f853f6904447c31b9e984", aum = 4)
    private static boolean awi;
    @C0064Am(aul = "1d65826bf65c0802439fa19b780b0348", aum = 5)
    private static I18NString awk;
    @C0064Am(aul = "3aa9dc1bd66cb452e818285eb75167d9", aum = 6)
    private static I18NString awm;
    @C0064Am(aul = "9a38d9ac8e7f8d085b6bbc865ead7d89", aum = 7)

    /* renamed from: bK */
    private static UUID f8567bK;
    @C0064Am(aul = "2f3bbba9cde47eaa1999b4d22f6b3323", aum = 1)
    private static String handle;
    @C0064Am(aul = "ab5daea4a060a11e6902d16744025269", aum = 0)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f8574jT;
    @C0064Am(aul = "81f88f325b19bec1aa08d504793c8700", aum = 3)
    private static float radius;

    static {
        m35150V();
    }

    private transient Random awe;

    public SpaceZone() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpaceZone(C5540aNg ang) {
        super(ang);
    }

    public SpaceZone(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m35150V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Trigger._m_fieldCount + 8;
        _m_methodCount = Trigger._m_methodCount + 33;
        int i = Trigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(SpaceZone.class, "ab5daea4a060a11e6902d16744025269", i);
        awd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpaceZone.class, "2f3bbba9cde47eaa1999b4d22f6b3323", i2);
        f8569bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpaceZone.class, "33897cf41f4553f09c3da65005b44972", i3);
        awg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpaceZone.class, "81f88f325b19bec1aa08d504793c8700", i4);
        awh = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpaceZone.class, "8c5602f7876f853f6904447c31b9e984", i5);
        awj = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(SpaceZone.class, "1d65826bf65c0802439fa19b780b0348", i6);
        awl = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(SpaceZone.class, "3aa9dc1bd66cb452e818285eb75167d9", i7);
        awn = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(SpaceZone.class, "9a38d9ac8e7f8d085b6bbc865ead7d89", i8);
        f8568bL = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Trigger._m_fields, (Object[]) _m_fields);
        int i10 = Trigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 33)];
        C2491fm a = C4105zY.m41624a(SpaceZone.class, "86fa17422c596e445cc31c9465a63127", i10);
        f8570bN = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(SpaceZone.class, "efc9574e40b0b56be09caa8e55ff1b17", i11);
        f8571bO = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(SpaceZone.class, "9cfdfe0fea312f0f3ff0bf18e8196d11", i12);
        awo = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(SpaceZone.class, "d87d5d21542f9a0d3c9fee5af63f2c9b", i13);
        f8575vi = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(SpaceZone.class, "bb1a63fae14bfe3666c94509a1545448", i14);
        f8572bP = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(SpaceZone.class, "8401fd68e4861cb65d9f7327bafca43e", i15);
        f8573bQ = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(SpaceZone.class, "7ef33c4944170c559ed64063306a2a07", i16);
        awp = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(SpaceZone.class, "34aeebc90a6e8eb3af7aeedda3b8a647", i17);
        awq = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(SpaceZone.class, "6eca96cee1e6cbc8cd2f97e6699f2657", i18);
        awr = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(SpaceZone.class, "8bef5e6da7968de4ea1f18eaf02505ca", i19);
        aws = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(SpaceZone.class, "db94b84563eeb82d477703c5450a92ec", i20);
        awt = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(SpaceZone.class, "d57e790e47be7163d9cc38587e783419", i21);
        awu = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(SpaceZone.class, "7167d10551a6c3b5907a43505424abce", i22);
        awv = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(SpaceZone.class, "c0ff55ae482ca1f7edf58738433d7700", i23);
        aww = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(SpaceZone.class, "7a8aeb7e9af600d7fd5996af1deb6b37", i24);
        awx = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(SpaceZone.class, "9fa1f812dc26fb80f26c8814c9bf8205", i25);
        awy = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(SpaceZone.class, "0efa9846d7aa27c6dde7f4dec99cf10f", i26);
        f8565Mq = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(SpaceZone.class, "081c35ef939e8a3ddfe80d71c2273614", i27);
        f8566Wp = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(SpaceZone.class, "6eba8b278181fa91836cb20f77ba8143", i28);
        awz = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(SpaceZone.class, "d6a818313709f764093137d863734045", i29);
        f8576vs = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(SpaceZone.class, "ad527c18495f4a2146345ee38507db75", i30);
        _f_start_0020_0028_0029V = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        C2491fm a22 = C4105zY.m41624a(SpaceZone.class, "eef41fb1d8a7c6f17683a2ae4e7a86d7", i31);
        _f_stop_0020_0028_0029V = a22;
        fmVarArr[i31] = a22;
        int i32 = i31 + 1;
        C2491fm a23 = C4105zY.m41624a(SpaceZone.class, "dcaa4ae942fa411ac1468759caa25307", i32);
        f8562Lc = a23;
        fmVarArr[i32] = a23;
        int i33 = i32 + 1;
        C2491fm a24 = C4105zY.m41624a(SpaceZone.class, "19fc687d7b498a93d2fcbc35fb9ef794", i33);
        f8563Ld = a24;
        fmVarArr[i33] = a24;
        int i34 = i33 + 1;
        C2491fm a25 = C4105zY.m41624a(SpaceZone.class, "e946bfef6b70dfb0f544c62c1217bf29", i34);
        f8564Le = a25;
        fmVarArr[i34] = a25;
        int i35 = i34 + 1;
        C2491fm a26 = C4105zY.m41624a(SpaceZone.class, "fcfe2baafa9f4ee6f256b2c60613dfd5", i35);
        _f_onResurrect_0020_0028_0029V = a26;
        fmVarArr[i35] = a26;
        int i36 = i35 + 1;
        C2491fm a27 = C4105zY.m41624a(SpaceZone.class, "7d2738dd66f5184d753ce6f724dc0634", i36);
        _f_tick_0020_0028F_0029V = a27;
        fmVarArr[i36] = a27;
        int i37 = i36 + 1;
        C2491fm a28 = C4105zY.m41624a(SpaceZone.class, "d47c2c9fbec5d6f55699c83b686aa748", i37);
        awA = a28;
        fmVarArr[i37] = a28;
        int i38 = i37 + 1;
        C2491fm a29 = C4105zY.m41624a(SpaceZone.class, "85413e37033f69762ee35c44a9af6ea9", i38);
        awB = a29;
        fmVarArr[i38] = a29;
        int i39 = i38 + 1;
        C2491fm a30 = C4105zY.m41624a(SpaceZone.class, "aa15a719dce9e010ed6b617243565e97", i39);
        awC = a30;
        fmVarArr[i39] = a30;
        int i40 = i39 + 1;
        C2491fm a31 = C4105zY.m41624a(SpaceZone.class, "bb81474549256f90974a2a7879bc0d71", i40);
        awD = a31;
        fmVarArr[i40] = a31;
        int i41 = i40 + 1;
        C2491fm a32 = C4105zY.m41624a(SpaceZone.class, "7daebd850495a0b747e3bbe71665654a", i41);
        awE = a32;
        fmVarArr[i41] = a32;
        int i42 = i41 + 1;
        C2491fm a33 = C4105zY.m41624a(SpaceZone.class, "699eb916408df675bd4f682338684314", i42);
        f8561Do = a33;
        fmVarArr[i42] = a33;
        int i43 = i42 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Trigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpaceZone.class, C6979axs.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "bb81474549256f90974a2a7879bc0d71", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: A */
    private void m35135A(Actor cr) {
        throw new aWi(new aCE(this, awD, new Object[]{cr}));
    }

    /* renamed from: Ll */
    private C3438ra m35138Ll() {
        return ((SpaceZoneType) getType()).bgF();
    }

    /* renamed from: Lm */
    private C3438ra m35139Lm() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awg);
    }

    /* renamed from: Ln */
    private float m35140Ln() {
        return bFf().mo5608dq().mo3211m(awh);
    }

    /* renamed from: Lo */
    private boolean m35141Lo() {
        return bFf().mo5608dq().mo3201h(awj);
    }

    /* renamed from: Lp */
    private I18NString m35142Lp() {
        return (I18NString) bFf().mo5608dq().mo3214p(awl);
    }

    /* renamed from: Lq */
    private I18NString m35143Lq() {
        return (I18NString) bFf().mo5608dq().mo3214p(awn);
    }

    /* renamed from: a */
    private void m35156a(String str) {
        bFf().mo5608dq().mo3197f(f8569bM, str);
    }

    /* renamed from: a */
    private void m35157a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8568bL, uuid);
    }

    /* renamed from: aU */
    private void m35160aU(float f) {
        bFf().mo5608dq().mo3150a(awh, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Radius")
    @C0064Am(aul = "34aeebc90a6e8eb3af7aeedda3b8a647", aum = 0)
    @C5566aOg
    /* renamed from: aV */
    private void m35161aV(float f) {
        throw new aWi(new aCE(this, awq, new Object[]{new Float(f)}));
    }

    /* renamed from: an */
    private UUID m35162an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8568bL);
    }

    /* renamed from: ao */
    private String m35163ao() {
        return (String) bFf().mo5608dq().mo3214p(f8569bM);
    }

    /* renamed from: ar */
    private void m35166ar(boolean z) {
        bFf().mo5608dq().mo3153a(awj, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show Enter and Exit Message")
    @C0064Am(aul = "8bef5e6da7968de4ea1f18eaf02505ca", aum = 0)
    @C5566aOg
    /* renamed from: as */
    private void m35167as(boolean z) {
        throw new aWi(new aCE(this, aws, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Active")
    @C0064Am(aul = "9fa1f812dc26fb80f26c8814c9bf8205", aum = 0)
    @C5566aOg
    /* renamed from: au */
    private void m35168au(boolean z) {
        throw new aWi(new aCE(this, awy, new Object[]{new Boolean(z)}));
    }

    /* renamed from: b */
    private void m35169b(Actor cr, I18NString i18NString) {
        switch (bFf().mo6893i(awE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awE, new Object[]{cr, i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awE, new Object[]{cr, i18NString}));
                break;
        }
        m35152a(cr, i18NString);
    }

    /* renamed from: c */
    private void m35172c(UUID uuid) {
        switch (bFf().mo6893i(f8571bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8571bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8571bO, new Object[]{uuid}));
                break;
        }
        m35171b(uuid);
    }

    /* renamed from: ee */
    private void m35173ee(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(awl, i18NString);
    }

    /* renamed from: ef */
    private void m35174ef(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(awn, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enter Message")
    @C0064Am(aul = "d57e790e47be7163d9cc38587e783419", aum = 0)
    @C5566aOg
    /* renamed from: eg */
    private void m35175eg(I18NString i18NString) {
        throw new aWi(new aCE(this, awu, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Exit Message")
    @C0064Am(aul = "c0ff55ae482ca1f7edf58738433d7700", aum = 0)
    @C5566aOg
    /* renamed from: ei */
    private void m35176ei(I18NString i18NString) {
        throw new aWi(new aCE(this, aww, new Object[]{i18NString}));
    }

    @C0064Am(aul = "19fc687d7b498a93d2fcbc35fb9ef794", aum = 0)
    /* renamed from: n */
    private void m35178n(Actor cr) {
        throw new aWi(new aCE(this, f8563Ld, new Object[]{cr}));
    }

    @C0064Am(aul = "e946bfef6b70dfb0f544c62c1217bf29", aum = 0)
    /* renamed from: p */
    private void m35179p(Actor cr) {
        throw new aWi(new aCE(this, f8564Le, new Object[]{cr}));
    }

    @C0064Am(aul = "ad527c18495f4a2146345ee38507db75", aum = 0)
    /* renamed from: qN */
    private void m35180qN() {
        throw new aWi(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "eef41fb1d8a7c6f17683a2ae4e7a86d7", aum = 0)
    /* renamed from: qO */
    private void m35181qO() {
        throw new aWi(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "dcaa4ae942fa411ac1468759caa25307", aum = 0)
    /* renamed from: qP */
    private boolean m35182qP() {
        throw new aWi(new aCE(this, f8562Lc, new Object[0]));
    }

    /* renamed from: x */
    private void m35183x(C3438ra raVar) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "aa15a719dce9e010ed6b617243565e97", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: y */
    private void m35184y(Actor cr) {
        throw new aWi(new aCE(this, awC, new Object[]{cr}));
    }

    /* renamed from: y */
    private void m35185y(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awg, raVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: B */
    public void mo7552B(Actor cr) {
        switch (bFf().mo6893i(awD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                break;
        }
        m35135A(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Exit Message")
    /* renamed from: LA */
    public I18NString mo20313LA() {
        switch (bFf().mo6893i(awv)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, awv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, awv, new Object[0]));
                break;
        }
        return m35148Lz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Active")
    /* renamed from: LC */
    public boolean mo20314LC() {
        switch (bFf().mo6893i(awx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, awx, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, awx, new Object[0]));
                break;
        }
        return m35136LB();
    }

    /* access modifiers changed from: protected */
    /* renamed from: LE */
    public Random mo20315LE() {
        switch (bFf().mo6893i(awz)) {
            case 0:
                return null;
            case 2:
                return (Random) bFf().mo5606d(new aCE(this, awz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, awz, new Object[0]));
                break;
        }
        return m35137LD();
    }

    /* renamed from: Ls */
    public boolean mo959Ls() {
        switch (bFf().mo6893i(awo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, awo, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, awo, new Object[0]));
                break;
        }
        return m35144Lr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Radius")
    /* renamed from: Lu */
    public float mo20316Lu() {
        switch (bFf().mo6893i(awp)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, awp, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, awp, new Object[0]));
                break;
        }
        return m35145Lt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show Enter and Exit Message")
    /* renamed from: Lw */
    public boolean mo20317Lw() {
        switch (bFf().mo6893i(awr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, awr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, awr, new Object[0]));
                break;
        }
        return m35146Lv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enter Message")
    /* renamed from: Ly */
    public I18NString mo20318Ly() {
        switch (bFf().mo6893i(awt)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, awt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, awt, new Object[0]));
                break;
        }
        return m35147Lx();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m35149U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6979axs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Trigger._m_methodCount) {
            case 0:
                return m35164ap();
            case 1:
                m35171b((UUID) args[0]);
                return null;
            case 2:
                return new Boolean(m35144Lr());
            case 3:
                return m35177it();
            case 4:
                return m35165ar();
            case 5:
                m35170b((String) args[0]);
                return null;
            case 6:
                return new Float(m35145Lt());
            case 7:
                m35161aV(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Boolean(m35146Lv());
            case 9:
                m35167as(((Boolean) args[0]).booleanValue());
                return null;
            case 10:
                return m35147Lx();
            case 11:
                m35175eg((I18NString) args[0]);
                return null;
            case 12:
                return m35148Lz();
            case 13:
                m35176ei((I18NString) args[0]);
                return null;
            case 14:
                return new Boolean(m35136LB());
            case 15:
                m35168au(((Boolean) args[0]).booleanValue());
                return null;
            case 16:
                m35154a((StellarSystem) args[0]);
                return null;
            case 17:
                m35186zw();
                return null;
            case 18:
                return m35137LD();
            case 19:
                m35151a((Actor.C0200h) args[0]);
                return null;
            case 20:
                m35180qN();
                return null;
            case 21:
                m35181qO();
                return null;
            case 22:
                return new Boolean(m35182qP());
            case 23:
                m35178n((Actor) args[0]);
                return null;
            case 24:
                m35179p((Actor) args[0]);
                return null;
            case 25:
                m35158aG();
                return null;
            case 26:
                m35149U(((Float) args[0]).floatValue());
                return null;
            case 27:
                m35155a((SpaceZoneEvent) args[0]);
                return null;
            case 28:
                m35159aJ((String) args[0]);
                return null;
            case 29:
                m35184y((Actor) args[0]);
                return null;
            case 30:
                m35135A((Actor) args[0]);
                return null;
            case 31:
                m35152a((Actor) args[0], (I18NString) args[1]);
                return null;
            case 32:
                m35153a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m35158aG();
    }

    /* renamed from: aK */
    public void mo20319aK(String str) {
        switch (bFf().mo6893i(awB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awB, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awB, new Object[]{str}));
                break;
        }
        m35159aJ(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Radius")
    @C5566aOg
    /* renamed from: aW */
    public void mo20320aW(float f) {
        switch (bFf().mo6893i(awq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awq, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awq, new Object[]{new Float(f)}));
                break;
        }
        m35161aV(f);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8570bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8570bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8570bN, new Object[0]));
                break;
        }
        return m35164ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show Enter and Exit Message")
    @C5566aOg
    /* renamed from: at */
    public void mo20321at(boolean z) {
        switch (bFf().mo6893i(aws)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aws, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aws, new Object[]{new Boolean(z)}));
                break;
        }
        m35167as(z);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f8576vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8576vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8576vs, new Object[]{hVar}));
                break;
        }
        m35151a(hVar);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8561Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8561Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8561Do, new Object[]{jt}));
                break;
        }
        m35153a(jt);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f8565Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8565Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8565Mq, new Object[]{jj}));
                break;
        }
        m35154a(jj);
    }

    /* renamed from: b */
    public void mo20322b(SpaceZoneEvent afb) {
        switch (bFf().mo6893i(awA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awA, new Object[]{afb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awA, new Object[]{afb}));
                break;
        }
        m35155a(afb);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enter Message")
    @C5566aOg
    /* renamed from: eh */
    public void mo20323eh(I18NString i18NString) {
        switch (bFf().mo6893i(awu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awu, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awu, new Object[]{i18NString}));
                break;
        }
        m35175eg(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Exit Message")
    @C5566aOg
    /* renamed from: ej */
    public void mo20324ej(I18NString i18NString) {
        switch (bFf().mo6893i(aww)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aww, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aww, new Object[]{i18NString}));
                break;
        }
        m35176ei(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8572bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8572bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8572bP, new Object[0]));
                break;
        }
        return m35165ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8573bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8573bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8573bQ, new Object[]{str}));
                break;
        }
        m35170b(str);
    }

    public boolean isActive() {
        switch (bFf().mo6893i(f8562Lc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8562Lc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8562Lc, new Object[0]));
                break;
        }
        return m35182qP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Active")
    @C5566aOg
    public void setActive(boolean z) {
        switch (bFf().mo6893i(awy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awy, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awy, new Object[]{new Boolean(z)}));
                break;
        }
        m35168au(z);
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f8575vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8575vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8575vi, new Object[0]));
                break;
        }
        return m35177it();
    }

    /* renamed from: o */
    public void mo17476o(Actor cr) {
        switch (bFf().mo6893i(f8563Ld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8563Ld, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8563Ld, new Object[]{cr}));
                break;
        }
        m35178n(cr);
    }

    /* renamed from: q */
    public void mo17477q(Actor cr) {
        switch (bFf().mo6893i(f8564Le)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8564Le, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8564Le, new Object[]{cr}));
                break;
        }
        m35179p(cr);
    }

    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m35180qN();
    }

    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m35181qO();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m35184y(cr);
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f8566Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8566Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8566Wp, new Object[0]));
                break;
        }
        m35186zw();
    }

    @C0064Am(aul = "86fa17422c596e445cc31c9465a63127", aum = 0)
    /* renamed from: ap */
    private UUID m35164ap() {
        return m35162an();
    }

    @C0064Am(aul = "efc9574e40b0b56be09caa8e55ff1b17", aum = 0)
    /* renamed from: b */
    private void m35171b(UUID uuid) {
        m35157a(uuid);
    }

    @C0064Am(aul = "9cfdfe0fea312f0f3ff0bf18e8196d11", aum = 0)
    /* renamed from: Lr */
    private boolean m35144Lr() {
        return false;
    }

    @C0064Am(aul = "d87d5d21542f9a0d3c9fee5af63f2c9b", aum = 0)
    /* renamed from: it */
    private String m35177it() {
        return null;
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
        setHandle(String.valueOf(((C0468GU) getType()).getHandle()) + "_" + cWm());
        setStatic(true);
        m35157a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "bb1a63fae14bfe3666c94509a1545448", aum = 0)
    /* renamed from: ar */
    private String m35165ar() {
        return m35163ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "8401fd68e4861cb65d9f7327bafca43e", aum = 0)
    /* renamed from: b */
    private void m35170b(String str) {
        m35156a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Radius")
    @C0064Am(aul = "7ef33c4944170c559ed64063306a2a07", aum = 0)
    /* renamed from: Lt */
    private float m35145Lt() {
        return m35140Ln();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show Enter and Exit Message")
    @C0064Am(aul = "6eca96cee1e6cbc8cd2f97e6699f2657", aum = 0)
    /* renamed from: Lv */
    private boolean m35146Lv() {
        return m35141Lo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enter Message")
    @C0064Am(aul = "db94b84563eeb82d477703c5450a92ec", aum = 0)
    /* renamed from: Lx */
    private I18NString m35147Lx() {
        return m35142Lp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Exit Message")
    @C0064Am(aul = "7167d10551a6c3b5907a43505424abce", aum = 0)
    /* renamed from: Lz */
    private I18NString m35148Lz() {
        return m35143Lq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Active")
    @C0064Am(aul = "7a8aeb7e9af600d7fd5996af1deb6b37", aum = 0)
    /* renamed from: LB */
    private boolean m35136LB() {
        return isActive();
    }

    @C0064Am(aul = "0efa9846d7aa27c6dde7f4dec99cf10f", aum = 0)
    /* renamed from: a */
    private void m35154a(StellarSystem jj) {
        start();
        super.mo993b(jj);
    }

    @C0064Am(aul = "081c35ef939e8a3ddfe80d71c2273614", aum = 0)
    /* renamed from: zw */
    private void m35186zw() {
        super.mo1099zx();
        stop();
    }

    @C0064Am(aul = "6eba8b278181fa91836cb20f77ba8143", aum = 0)
    /* renamed from: LD */
    private Random m35137LD() {
        if (this.awe == null) {
            this.awe = new Random();
        }
        return this.awe;
    }

    @C0064Am(aul = "d6a818313709f764093137d863734045", aum = 0)
    /* renamed from: a */
    private void m35151a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(mo20316Lu()));
    }

    @C0064Am(aul = "fcfe2baafa9f4ee6f256b2c60613dfd5", aum = 0)
    /* renamed from: aG */
    private void m35158aG() {
        super.mo70aH();
        if (isActive()) {
            mo20322b((SpaceZoneEvent) null);
        }
        if (m35163ao() == null) {
            setHandle(String.valueOf(((C0468GU) getType()).getHandle()) + "_" + cWm());
        }
    }

    @C0064Am(aul = "7d2738dd66f5184d753ce6f724dc0634", aum = 0)
    /* renamed from: U */
    private void m35149U(float f) {
        super.mo4709V(f);
        ArrayList<SpaceZoneEvent> arrayList = new ArrayList<>();
        long cVr = cVr();
        long j = Long.MAX_VALUE;
        for (SpaceZoneEvent afb : m35139Lm()) {
            if (cVr >= afb.getTime()) {
                arrayList.add(afb);
            }
            if (afb.getTime() > cVr && afb.getTime() < j) {
                j = afb.getTime();
            }
        }
        for (SpaceZoneEvent afb2 : arrayList) {
            if (afb2 == null) {
                mo8358lY("Space zone '" + getHandle() + "' is trying to trigger a null event");
            } else {
                m35139Lm().remove(afb2);
                C3763uj cYj = afb2.cYj();
                if (cYj == null) {
                    mo8358lY("Space zone '" + getHandle() + "' is trying to trigger a event with null event (listener handle: '" + afb2.getHandle() + "')");
                } else {
                    cYj.mo14332cj(afb2.getHandle());
                }
            }
        }
        if (j != Long.MAX_VALUE) {
            mo8361ms(((float) (j - cVr)) / 1000.0f);
        }
    }

    @C0064Am(aul = "d47c2c9fbec5d6f55699c83b686aa748", aum = 0)
    /* renamed from: a */
    private void m35155a(SpaceZoneEvent afb) {
        if (afb != null) {
            m35139Lm().add(afb);
        }
        long j = Long.MAX_VALUE;
        for (SpaceZoneEvent afb2 : m35139Lm()) {
            if (afb2.getTime() < j) {
                j = afb2.getTime();
            }
        }
        long cVr = cVr();
        if (j >= Long.MAX_VALUE || ((float) j) + 0.01f <= ((float) cVr)) {
            mo8361ms(0.01f);
        } else {
            mo8361ms(((float) (j - cVr)) / 1000.0f);
        }
    }

    @C0064Am(aul = "85413e37033f69762ee35c44a9af6ea9", aum = 0)
    /* renamed from: aJ */
    private void m35159aJ(String str) {
        if (str != null) {
            Iterator it = m35139Lm().iterator();
            while (it.hasNext()) {
                if (str.equals(((SpaceZoneEvent) it.next()).getHandle())) {
                    it.remove();
                }
            }
        }
    }

    @C0064Am(aul = "7daebd850495a0b747e3bbe71665654a", aum = 0)
    /* renamed from: a */
    private void m35152a(Actor cr, I18NString i18NString) {
        if (i18NString != null && (cr instanceof Pawn)) {
            Controller hb = ((Pawn) cr).mo2998hb();
            if (hb instanceof PlayerController) {
                ((PlayerController) hb).mo22135dL().mo14419f((C1506WA) new C0284Df(i18NString, false, new Object[0]));
            }
        }
    }

    @C0064Am(aul = "699eb916408df675bd4f682338684314", aum = 0)
    /* renamed from: a */
    private void m35153a(C0665JT jt) {
        if (jt.mo3117j(1, 2, 0)) {
            setRadius(((Float) jt.get("zoneRadius")).floatValue());
        }
        if (jt.mo3117j(1, 2, 1)) {
            boolean bae = bae();
            mo1099zx();
            setStatic(true);
            if (bae) {
                cMC();
            }
        }
    }
}
