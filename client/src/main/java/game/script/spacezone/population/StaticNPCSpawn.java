package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C0557Hk;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.BG */
/* compiled from: a */
public class StaticNPCSpawn extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aGq = null;
    public static final C2491fm aGt = null;
    public static final C2491fm aGw = null;
    /* renamed from: bL */
    public static final C5663aRz f178bL = null;
    /* renamed from: bM */
    public static final C5663aRz f179bM = null;
    /* renamed from: bN */
    public static final C2491fm f180bN = null;
    /* renamed from: bO */
    public static final C2491fm f181bO = null;
    /* renamed from: bP */
    public static final C2491fm f182bP = null;
    /* renamed from: bQ */
    public static final C2491fm f183bQ = null;
    public static final C5663aRz bfK = null;
    public static final C2491fm bfQ = null;
    public static final C2491fm bfR = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "702574f96ff590c33c5f3c0a9a7f6092", aum = 2)

    /* renamed from: bK */
    private static UUID f177bK;
    @C0064Am(aul = "193399ec8b7bc0ac6c29343ebad88f27", aum = 1)

    /* renamed from: cZ */
    private static int f184cZ;
    @C0064Am(aul = "07d901f8a863b53b43ad802eaa97b9e7", aum = 3)
    private static String handle;
    @C0064Am(aul = "eb67480cbe357b337f227d563abdb8f4", aum = 0)

    /* renamed from: nC */
    private static NPCType f185nC;

    static {
        m1052V();
    }

    public StaticNPCSpawn() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StaticNPCSpawn(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m1052V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(StaticNPCSpawn.class, "eb67480cbe357b337f227d563abdb8f4", i);
        bfK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StaticNPCSpawn.class, "193399ec8b7bc0ac6c29343ebad88f27", i2);
        aGq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(StaticNPCSpawn.class, "702574f96ff590c33c5f3c0a9a7f6092", i3);
        f178bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(StaticNPCSpawn.class, "07d901f8a863b53b43ad802eaa97b9e7", i4);
        f179bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(StaticNPCSpawn.class, "b02a34a6bc2ae0d43c9b95d65260de97", i6);
        f180bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(StaticNPCSpawn.class, "e5c518ce84598fd6789c71060df92d98", i7);
        f181bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(StaticNPCSpawn.class, "2d5be9a204641dcfa81cd086b29bd4c5", i8);
        f182bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(StaticNPCSpawn.class, "8a8ed154be7a7b87fb64fe540334de4c", i9);
        f183bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(StaticNPCSpawn.class, "7fa9695b9f29497dfbd4e97c18fa1ff0", i10);
        bfQ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(StaticNPCSpawn.class, "e9e02c1875f171ce1acbc9912ceaf907", i11);
        bfR = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(StaticNPCSpawn.class, "6ffd26a117bb08f670cd7346d308dc70", i12);
        aGt = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(StaticNPCSpawn.class, "681850ab6312a7c7ef3c96121b2ae988", i13);
        aGw = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StaticNPCSpawn.class, C0557Hk.class, _m_fields, _m_methods);
    }

    /* renamed from: PF */
    private int m1050PF() {
        return bFf().mo5608dq().mo3212n(aGq);
    }

    /* renamed from: a */
    private void m1053a(String str) {
        bFf().mo5608dq().mo3197f(f179bM, str);
    }

    /* renamed from: a */
    private void m1054a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f178bL, uuid);
    }

    private NPCType abn() {
        return (NPCType) bFf().mo5608dq().mo3214p(bfK);
    }

    /* renamed from: an */
    private UUID m1055an() {
        return (UUID) bFf().mo5608dq().mo3214p(f178bL);
    }

    /* renamed from: ao */
    private String m1056ao() {
        return (String) bFf().mo5608dq().mo3214p(f179bM);
    }

    /* renamed from: b */
    private void m1059b(NPCType aed) {
        bFf().mo5608dq().mo3197f(bfK, aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "8a8ed154be7a7b87fb64fe540334de4c", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m1060b(String str) {
        throw new aWi(new aCE(this, f183bQ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC type")
    @C0064Am(aul = "e9e02c1875f171ce1acbc9912ceaf907", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m1062c(NPCType aed) {
        throw new aWi(new aCE(this, bfR, new Object[]{aed}));
    }

    /* renamed from: c */
    private void m1063c(UUID uuid) {
        switch (bFf().mo6893i(f181bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f181bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f181bO, new Object[]{uuid}));
                break;
        }
        m1061b(uuid);
    }

    /* renamed from: cZ */
    private void m1064cZ(int i) {
        bFf().mo5608dq().mo3183b(aGq, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Quantity")
    @C0064Am(aul = "681850ab6312a7c7ef3c96121b2ae988", aum = 0)
    @C5566aOg
    /* renamed from: da */
    private void m1065da(int i) {
        throw new aWi(new aCE(this, aGw, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Quantity")
    /* renamed from: Iq */
    public int mo668Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m1051PK();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0557Hk(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m1057ap();
            case 1:
                m1061b((UUID) args[0]);
                return null;
            case 2:
                return m1058ar();
            case 3:
                m1060b((String) args[0]);
                return null;
            case 4:
                return abr();
            case 5:
                m1062c((NPCType) args[0]);
                return null;
            case 6:
                return new Integer(m1051PK());
            case 7:
                m1065da(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC type")
    public NPCType abs() {
        switch (bFf().mo6893i(bfQ)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, bfQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfQ, new Object[0]));
                break;
        }
        return abr();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f180bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f180bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f180bN, new Object[0]));
                break;
        }
        return m1057ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC type")
    @C5566aOg
    /* renamed from: d */
    public void mo670d(NPCType aed) {
        switch (bFf().mo6893i(bfR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfR, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfR, new Object[]{aed}));
                break;
        }
        m1062c(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Quantity")
    @C5566aOg
    /* renamed from: db */
    public void mo671db(int i) {
        switch (bFf().mo6893i(aGw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                break;
        }
        m1065da(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f182bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f182bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f182bP, new Object[0]));
                break;
        }
        return m1058ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f183bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f183bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f183bQ, new Object[]{str}));
                break;
        }
        m1060b(str);
    }

    @C0064Am(aul = "b02a34a6bc2ae0d43c9b95d65260de97", aum = 0)
    /* renamed from: ap */
    private UUID m1057ap() {
        return m1055an();
    }

    @C0064Am(aul = "e5c518ce84598fd6789c71060df92d98", aum = 0)
    /* renamed from: b */
    private void m1061b(UUID uuid) {
        m1054a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m1054a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "2d5be9a204641dcfa81cd086b29bd4c5", aum = 0)
    /* renamed from: ar */
    private String m1058ar() {
        return m1056ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC type")
    @C0064Am(aul = "7fa9695b9f29497dfbd4e97c18fa1ff0", aum = 0)
    private NPCType abr() {
        return abn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Quantity")
    @C0064Am(aul = "6ffd26a117bb08f670cd7346d308dc70", aum = 0)
    /* renamed from: PK */
    private int m1051PK() {
        return m1050PF();
    }
}
