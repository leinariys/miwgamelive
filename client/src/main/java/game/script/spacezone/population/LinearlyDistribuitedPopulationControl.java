package game.script.spacezone.population;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.faction.Faction;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.spacezone.PopulationControl;
import logic.baa.*;
import logic.bbb.C2820ka;
import logic.data.link.C0471GX;
import logic.data.link.C3981xi;
import logic.data.mbean.C0877Mi;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.JF */
/* compiled from: a */
public class LinearlyDistribuitedPopulationControl extends PopulationControl implements C1616Xf {

    /* renamed from: Lg */
    public static final C2491fm f745Lg = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dkH = null;
    public static final C5663aRz dkJ = null;
    public static final C5663aRz dkL = null;
    public static final C5663aRz dkN = null;
    public static final C5663aRz dkP = null;
    public static final C5663aRz dkR = null;
    public static final C2491fm dkS = null;
    public static final C2491fm dkT = null;
    public static final C2491fm dkU = null;
    public static final C2491fm dkV = null;
    public static final C2491fm dkW = null;
    public static final C2491fm dkX = null;
    public static final C2491fm dkY = null;
    public static final C2491fm dkZ = null;
    public static final C2491fm dla = null;
    public static final C2491fm dlb = null;
    public static final C2491fm dlc = null;
    public static final C2491fm dld = null;
    public static final C2491fm dle = null;
    public static final C2491fm dlf = null;
    public static final C2491fm dlg = null;
    public static final C2491fm dlh = null;
    public static final C2491fm dli = null;
    public static final C2491fm dlj = null;
    public static final C2491fm dlk = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ebda4f9d95aedd663e3fd037d4704740", aum = 0)
    @C5454aJy("Spawn Table")
    private static C3438ra<NPCSpawn> dkG;
    @C0064Am(aul = "e7e6b1b5038656dd50a0314c9b636615", aum = 1)
    private static int dkI;
    @C0064Am(aul = "f0f5fb305d0385989e84ee38d3598af2", aum = 2)
    private static int dkK;
    @C0064Am(aul = "06fba6f41eddd766ed02bf3f54de369f", aum = 3)
    private static int dkM;
    @C0064Am(aul = "31b6851038ffdad57692cdaac8a951b8", aum = 4)
    private static int dkO;
    @C0064Am(aul = "4bce5d2741ab4ab93d70a506968a40ca", aum = 5)
    private static C3438ra<NPC> dkQ;

    static {
        m5538V();
    }

    public LinearlyDistribuitedPopulationControl() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LinearlyDistribuitedPopulationControl(LinearlyDistribuitedPopulationControlType kx) {
        super((C5540aNg) null);
        super._m_script_init(kx);
    }

    public LinearlyDistribuitedPopulationControl(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5538V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PopulationControl._m_fieldCount + 6;
        _m_methodCount = PopulationControl._m_methodCount + 21;
        int i = PopulationControl._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(LinearlyDistribuitedPopulationControl.class, "ebda4f9d95aedd663e3fd037d4704740", i);
        dkH = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(LinearlyDistribuitedPopulationControl.class, "e7e6b1b5038656dd50a0314c9b636615", i2);
        dkJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(LinearlyDistribuitedPopulationControl.class, "f0f5fb305d0385989e84ee38d3598af2", i3);
        dkL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(LinearlyDistribuitedPopulationControl.class, "06fba6f41eddd766ed02bf3f54de369f", i4);
        dkN = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(LinearlyDistribuitedPopulationControl.class, "31b6851038ffdad57692cdaac8a951b8", i5);
        dkP = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(LinearlyDistribuitedPopulationControl.class, "4bce5d2741ab4ab93d70a506968a40ca", i6);
        dkR = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) PopulationControl._m_fields, (Object[]) _m_fields);
        int i8 = PopulationControl._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 21)];
        C2491fm a = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "2b3db4bbe9c46b44388808ffebccf83d", i8);
        dkS = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "8d84f0701f1eda8cc0dfd9bf9cb44b54", i9);
        dkT = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "df168dd67c20478ba70917e1debb668d", i10);
        dkU = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "87c54048c8e88b707af9d2bbdee1a57d", i11);
        dkV = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "c0ceb10c920959f63e228d8c383bebe1", i12);
        dkW = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "b0a1c4423985b64114def6b24b6f526a", i13);
        dkX = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "e61e1a8596f821707a842069ff22c8db", i14);
        dkY = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "d4552d0cdd3512b52c5cb405354e18b7", i15);
        dkZ = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "303b424811bb02139da7599b1376e028", i16);
        dla = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "3d12c5cabeac74fd7e3a907fbd79cdd4", i17);
        dlb = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "ad6cbb3a064552326196f16bf79fe00c", i18);
        dlc = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "b4023e221c272e69689fdd352fbcdb18", i19);
        dld = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "a021d56c1ee7fac30a1396380c15a741", i20);
        dle = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "bae9c9225b2228275363b48f1ea156e6", i21);
        dlf = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "8953b92f034b00e1eb4ad0968c977432", i22);
        dlg = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "2cab37c352372d3c15bc3b1efbc81924", i23);
        dlh = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "2875cac8619f3ce2009f3ce060eb44f7", i24);
        dli = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        C2491fm a18 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "6c657d0ea3b8588735fd5beb66961bd1", i25);
        f745Lg = a18;
        fmVarArr[i25] = a18;
        int i26 = i25 + 1;
        C2491fm a19 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "8934103af61e97466b2d02341a7fc06c", i26);
        dlj = a19;
        fmVarArr[i26] = a19;
        int i27 = i26 + 1;
        C2491fm a20 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "ba0c0b36b5376609e7b557d25269a6bd", i27);
        dlk = a20;
        fmVarArr[i27] = a20;
        int i28 = i27 + 1;
        C2491fm a21 = C4105zY.m41624a(LinearlyDistribuitedPopulationControl.class, "5346b50160cdd377e9532d87c404955b", i28);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a21;
        fmVarArr[i28] = a21;
        int i29 = i28 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PopulationControl._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LinearlyDistribuitedPopulationControl.class, C0877Mi.class, _m_fields, _m_methods);
    }

    private C3438ra aYU() {
        return ((LinearlyDistribuitedPopulationControlType) getType()).bek();
    }

    private int aYV() {
        return bFf().mo5608dq().mo3212n(dkJ);
    }

    private int aYW() {
        return bFf().mo5608dq().mo3212n(dkL);
    }

    private int aYX() {
        return bFf().mo5608dq().mo3212n(dkN);
    }

    private int aYY() {
        return bFf().mo5608dq().mo3212n(dkP);
    }

    private C3438ra aYZ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dkR);
    }

    private int aZh() {
        switch (bFf().mo6893i(dkY)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dkY, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dkY, new Object[0]));
                break;
        }
        return aZg();
    }

    private int aZj() {
        switch (bFf().mo6893i(dkZ)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dkZ, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dkZ, new Object[0]));
                break;
        }
        return aZi();
    }

    private void aZs() {
        switch (bFf().mo6893i(dli)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dli, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dli, new Object[0]));
                break;
        }
        aZr();
    }

    /* renamed from: bd */
    private void m5540bd(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: be */
    private void m5541be(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dkR, raVar);
    }

    /* renamed from: iJ */
    private void m5543iJ() {
        switch (bFf().mo6893i(dlb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlb, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlb, new Object[0]));
                break;
        }
        aZm();
    }

    /* renamed from: lf */
    private void m5544lf(int i) {
        bFf().mo5608dq().mo3183b(dkJ, i);
    }

    /* renamed from: lg */
    private void m5545lg(int i) {
        bFf().mo5608dq().mo3183b(dkL, i);
    }

    /* renamed from: lh */
    private void m5546lh(int i) {
        bFf().mo5608dq().mo3183b(dkN, i);
    }

    /* renamed from: li */
    private void m5547li(int i) {
        bFf().mo5608dq().mo3183b(dkP, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Population")
    @C0064Am(aul = "8d84f0701f1eda8cc0dfd9bf9cb44b54", aum = 0)
    @C5566aOg
    /* renamed from: lj */
    private void m5548lj(int i) {
        throw new aWi(new aCE(this, dkT, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Population")
    @C0064Am(aul = "87c54048c8e88b707af9d2bbdee1a57d", aum = 0)
    @C5566aOg
    /* renamed from: ll */
    private void m5549ll(int i) {
        throw new aWi(new aCE(this, dkV, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creeps Spawned per Invader")
    @C0064Am(aul = "b0a1c4423985b64114def6b24b6f526a", aum = 0)
    @C5566aOg
    /* renamed from: ln */
    private void m5550ln(int i) {
        throw new aWi(new aCE(this, dkX, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "5346b50160cdd377e9532d87c404955b", aum = 0)
    /* renamed from: qW */
    private Object m5552qW() {
        return aZu();
    }

    /* renamed from: D */
    public void mo2941D(Character acx) {
        switch (bFf().mo6893i(dld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                break;
        }
        m5535C(acx);
    }

    /* renamed from: F */
    public void mo2942F(Character acx) {
        switch (bFf().mo6893i(dle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                break;
        }
        m5536E(acx);
    }

    /* renamed from: H */
    public void mo2943H(Character acx) {
        switch (bFf().mo6893i(dlf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                break;
        }
        m5537G(acx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0877Mi(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PopulationControl._m_methodCount) {
            case 0:
                return new Integer(aZa());
            case 1:
                m5548lj(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Integer(aZc());
            case 3:
                m5549ll(((Integer) args[0]).intValue());
                return null;
            case 4:
                return new Integer(aZe());
            case 5:
                m5550ln(((Integer) args[0]).intValue());
                return null;
            case 6:
                return new Integer(aZg());
            case 7:
                return new Integer(aZi());
            case 8:
                aZk();
                return null;
            case 9:
                aZm();
                return null;
            case 10:
                m5542e((NPC) args[0]);
                return null;
            case 11:
                m5535C((Character) args[0]);
                return null;
            case 12:
                m5536E((Character) args[0]);
                return null;
            case 13:
                m5537G((Character) args[0]);
                return null;
            case 14:
                aZn();
                return null;
            case 15:
                aZp();
                return null;
            case 16:
                aZr();
                return null;
            case 17:
                return m5551qQ();
            case 18:
                return aZt();
            case 19:
                m5539a((Character) args[0], (Character) args[1]);
                return null;
            case 20:
                return m5552qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Population")
    public int aZb() {
        switch (bFf().mo6893i(dkS)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dkS, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dkS, new Object[0]));
                break;
        }
        return aZa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Population")
    public int aZd() {
        switch (bFf().mo6893i(dkU)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dkU, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dkU, new Object[0]));
                break;
        }
        return aZc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creeps Spawned per Invader")
    public int aZf() {
        switch (bFf().mo6893i(dkW)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dkW, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dkW, new Object[0]));
                break;
        }
        return aZe();
    }

    /* access modifiers changed from: protected */
    public void aZl() {
        switch (bFf().mo6893i(dla)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                break;
        }
        aZk();
    }

    public void aZo() {
        switch (bFf().mo6893i(dlg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                break;
        }
        aZn();
    }

    public void aZq() {
        switch (bFf().mo6893i(dlh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                break;
        }
        aZp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    public LinearlyDistribuitedPopulationControlType aZu() {
        switch (bFf().mo6893i(dlj)) {
            case 0:
                return null;
            case 2:
                return (LinearlyDistribuitedPopulationControlType) bFf().mo5606d(new aCE(this, dlj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dlj, new Object[0]));
                break;
        }
        return aZt();
    }

    /* renamed from: b */
    public void mo2952b(Character acx, Character acx2) {
        switch (bFf().mo6893i(dlk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                break;
        }
        m5539a(acx, acx2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: f */
    public void mo2953f(NPC auf) {
        switch (bFf().mo6893i(dlc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                break;
        }
        m5542e(auf);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m5552qW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Population")
    @C5566aOg
    /* renamed from: lk */
    public void mo2954lk(int i) {
        switch (bFf().mo6893i(dkT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dkT, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dkT, new Object[]{new Integer(i)}));
                break;
        }
        m5548lj(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Population")
    @C5566aOg
    /* renamed from: lm */
    public void mo2955lm(int i) {
        switch (bFf().mo6893i(dkV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dkV, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dkV, new Object[]{new Integer(i)}));
                break;
        }
        m5549ll(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creeps Spawned per Invader")
    @C5566aOg
    /* renamed from: lo */
    public void mo2956lo(int i) {
        switch (bFf().mo6893i(dkX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dkX, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dkX, new Object[]{new Integer(i)}));
                break;
        }
        m5550ln(i);
    }

    /* renamed from: qR */
    public List<NPC> mo2957qR() {
        switch (bFf().mo6893i(f745Lg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f745Lg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f745Lg, new Object[0]));
                break;
        }
        return m5551qQ();
    }

    /* renamed from: a */
    public void mo2944a(LinearlyDistribuitedPopulationControlType kx) {
        super.mo14333h(kx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Population")
    @C0064Am(aul = "2b3db4bbe9c46b44388808ffebccf83d", aum = 0)
    private int aZa() {
        return aYW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Population")
    @C0064Am(aul = "df168dd67c20478ba70917e1debb668d", aum = 0)
    private int aZc() {
        return aYV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creeps Spawned per Invader")
    @C0064Am(aul = "c0ceb10c920959f63e228d8c383bebe1", aum = 0)
    private int aZe() {
        return aYX();
    }

    @C0064Am(aul = "e61e1a8596f821707a842069ff22c8db", aum = 0)
    private int aZg() {
        int i = 0;
        for (NPC bQx : aYZ()) {
            Ship bQx2 = bQx.bQx();
            if (bQx2 != null && bQx2.bae()) {
                i++;
            }
        }
        return i;
    }

    @C0064Am(aul = "d4552d0cdd3512b52c5cb405354e18b7", aum = 0)
    private int aZi() {
        int aYY = aYY() * aYX();
        if (aYW() >= 0 && aYY > aYW()) {
            aYY = aYW();
        }
        if (aYV() < 0 || aYY >= aYV()) {
            return aYY;
        }
        return aYV();
    }

    @C0064Am(aul = "303b424811bb02139da7599b1376e028", aum = 0)
    private void aZk() {
        if (aZh() < aZj()) {
            aZs();
        }
    }

    @C0064Am(aul = "3d12c5cabeac74fd7e3a907fbd79cdd4", aum = 0)
    private void aZm() {
        int aZj = aZj() - aZh();
        while (true) {
            aZj--;
            if (aZj >= 0) {
                aZs();
            } else {
                return;
            }
        }
    }

    @C0064Am(aul = "ad6cbb3a064552326196f16bf79fe00c", aum = 0)
    /* renamed from: e */
    private void m5542e(NPC auf) {
        aYZ().remove(auf);
        auf.dispose();
        mo14334hO(cVr() + ((long) (chu() * 1000.0f)));
    }

    @C0064Am(aul = "b4023e221c272e69689fdd352fbcdb18", aum = 0)
    /* renamed from: C */
    private void m5535C(Character acx) {
        m5547li(aYY() - 1);
    }

    @C0064Am(aul = "a021d56c1ee7fac30a1396380c15a741", aum = 0)
    /* renamed from: E */
    private void m5536E(Character acx) {
        Faction azN;
        m5547li(aYY() + 1);
        if (mo8669Uy().isActive()) {
            m5543iJ();
        }
        if ((acx instanceof Player) && (azN = ((NPCSpawn) aYU().get(0)).abs().azN()) != null) {
            ((Player) acx).dyl().mo11981e((C4068yr) azN);
        }
    }

    @C0064Am(aul = "bae9c9225b2228275363b48f1ea156e6", aum = 0)
    /* renamed from: G */
    private void m5537G(Character acx) {
        m5547li(aYY() - 1);
    }

    @C0064Am(aul = "8953b92f034b00e1eb4ad0968c977432", aum = 0)
    private void aZn() {
        int i;
        super.aZo();
        if (aYV() > aYW()) {
            //Минимальная и максимальная численность населения в зоне "+ mo8669Uy (). GetName () +" недопустимы.
            throw new RuntimeException("Min and max population in zone " + mo8669Uy().getName() + " are invalid");
        }
        int i2 = 0;
        Iterator it = aYU().iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            i2 = ((NPCSpawn) it.next()).dfh() + i;
        }
        if (i != 100) {
            //Таблица появления в зоне «+ mo8669Uy (). GetName () +» не дает в сумме 100
            throw new RuntimeException("Spawn table in zone " + mo8669Uy().getName() + " doesn't add up to 100");
        }
        Iterator it2 = aYZ().iterator();
        while (it2.hasNext()) {
            NPC auf = (NPC) it2.next();
            Ship bQx = auf.bQx();
            if (bQx != null && !bQx.bae()) {
                it2.remove();
                auf.dispose();
            }
        }
        m5543iJ();
    }

    @C0064Am(aul = "2cab37c352372d3c15bc3b1efbc81924", aum = 0)
    private void aZp() {
        super.aZq();
        m5547li(0);
        for (NPC auf : aYZ()) {
            Ship bQx = auf.bQx();
            if (bQx != null) {
                bQx.mo8355h(C0471GX.C0472a.class, mo8669Uy());
                bQx.mo8355h(C3981xi.C3982a.class, mo8669Uy());
            }
            auf.dispose();
        }
        aYZ().clear();
    }

    @C0064Am(aul = "2875cac8619f3ce2009f3ce060eb44f7", aum = 0)
    private void aZr() {
        NPCType aed;
        float nextFloat = mo14328LE().nextFloat();
        List<C2782jv> a = C2945lv.m35367a(aYZ(), aYU(), aZj());
        if (a != null) {
            Iterator<C2782jv> it = a.iterator();
            while (true) {
                float f = nextFloat;
                if (!it.hasNext()) {
                    aed = null;
                    break;
                }
                C2782jv next = it.next();
                if (f < next.mo19998Ft()) {
                    aed = next.mo19997Fs();
                    break;
                }
                nextFloat = f - next.mo19998Ft();
            }
            if (aed == null) {
                mo8358lY("Zone '" + mo8669Uy().getName() + "' has a non-complete spawn table");
                return;
            }
            new C2820ka(aed.bTC().mo19050uX().bkx().getFile());
            Vec3f chy = chy();
            if (chy != null) {
                NPC bUa = aed.bUa();
                Quat4fWrap chw = chw();
                bUa.bQx().setPosition(mo8669Uy().getPosition().mo9531q(chy));
                bUa.bQx().setOrientation(chw);
                aYZ().add(bUa);
                bUa.bQx().mo993b(mo8669Uy().mo960Nc());
                mo8669Uy().mo18865b(bUa);
                return;
            }
            mo8358lY("Unable to find an uncolliding place to spawn the NPC of type '" + aed.mo11470ke() + "' in the NPC Zone '" + mo8669Uy().getName() + "'");
        }
    }

    @C0064Am(aul = "6c657d0ea3b8588735fd5beb66961bd1", aum = 0)
    /* renamed from: qQ */
    private List<NPC> m5551qQ() {
        return aYZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "8934103af61e97466b2d02341a7fc06c", aum = 0)
    private LinearlyDistribuitedPopulationControlType aZt() {
        return (LinearlyDistribuitedPopulationControlType) super.getType();
    }

    @C0064Am(aul = "ba0c0b36b5376609e7b557d25269a6bd", aum = 0)
    /* renamed from: a */
    private void m5539a(Character acx, Character acx2) {
    }
}
