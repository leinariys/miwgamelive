package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.spacezone.PopulationControlType;
import logic.baa.*;
import logic.data.mbean.C1526WS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.KX */
/* compiled from: a */
public class LinearlyDistribuitedPopulationControlType extends PopulationControlType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f975dN = null;
    public static final C5663aRz dkH = null;
    public static final C2491fm dsr = null;
    public static final C2491fm dss = null;
    public static final C2491fm dst = null;
    public static final C2491fm dsu = null;
    public static final C2491fm dsv = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "56519477275e16bff13c07dd1e7c6b0a", aum = 0)
    private static C3438ra<NPCSpawn> dkG;

    static {
        m6429V();
    }

    public LinearlyDistribuitedPopulationControlType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LinearlyDistribuitedPopulationControlType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6429V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PopulationControlType._m_fieldCount + 1;
        _m_methodCount = PopulationControlType._m_methodCount + 6;
        int i = PopulationControlType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(LinearlyDistribuitedPopulationControlType.class, "56519477275e16bff13c07dd1e7c6b0a", i);
        dkH = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) PopulationControlType._m_fields, (Object[]) _m_fields);
        int i3 = PopulationControlType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
        C2491fm a = C4105zY.m41624a(LinearlyDistribuitedPopulationControlType.class, "14f81d430778e83c912405e741821a9d", i3);
        dsr = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(LinearlyDistribuitedPopulationControlType.class, "ded2a89b7cc95b45286b40cf4f749f8a", i4);
        dss = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(LinearlyDistribuitedPopulationControlType.class, "a00c21dd88bc1a69ffbec3641b208ece", i5);
        dst = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(LinearlyDistribuitedPopulationControlType.class, "f614eb178c0321795a03be492af59f9b", i6);
        dsu = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(LinearlyDistribuitedPopulationControlType.class, "cefd2f6dd3dbb4ecaa1f4f4b405455fc", i7);
        dsv = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(LinearlyDistribuitedPopulationControlType.class, "76b141eb7cbcef6176cbd14fc1b00495", i8);
        f975dN = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PopulationControlType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LinearlyDistribuitedPopulationControlType.class, C1526WS.class, _m_fields, _m_methods);
    }

    private C3438ra aYU() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dkH);
    }

    /* renamed from: bd */
    private void m6432bd(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dkH, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1526WS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PopulationControlType._m_methodCount) {
            case 0:
                m6430a((NPCSpawn) args[0]);
                return null;
            case 1:
                m6433c((NPCSpawn) args[0]);
                return null;
            case 2:
                return bej();
            case 3:
                return bel();
            case 4:
                return ben();
            case 5:
                return m6431aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f975dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f975dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f975dN, new Object[0]));
                break;
        }
        return m6431aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Spawn Table")
    /* renamed from: b */
    public void mo3560b(NPCSpawn aij) {
        switch (bFf().mo6893i(dsr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsr, new Object[]{aij}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsr, new Object[]{aij}));
                break;
        }
        m6430a(aij);
    }

    public C3438ra<NPCSpawn> bek() {
        switch (bFf().mo6893i(dst)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dst, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dst, new Object[0]));
                break;
        }
        return bej();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Spawn Table")
    public List<NPCSpawn> bem() {
        switch (bFf().mo6893i(dsu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dsu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsu, new Object[0]));
                break;
        }
        return bel();
    }

    public LinearlyDistribuitedPopulationControl beo() {
        switch (bFf().mo6893i(dsv)) {
            case 0:
                return null;
            case 2:
                return (LinearlyDistribuitedPopulationControl) bFf().mo5606d(new aCE(this, dsv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsv, new Object[0]));
                break;
        }
        return ben();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Spawn Table")
    /* renamed from: d */
    public void mo3564d(NPCSpawn aij) {
        switch (bFf().mo6893i(dss)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dss, new Object[]{aij}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dss, new Object[]{aij}));
                break;
        }
        m6433c(aij);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Spawn Table")
    @C0064Am(aul = "14f81d430778e83c912405e741821a9d", aum = 0)
    /* renamed from: a */
    private void m6430a(NPCSpawn aij) {
        aYU().add(aij);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Spawn Table")
    @C0064Am(aul = "ded2a89b7cc95b45286b40cf4f749f8a", aum = 0)
    /* renamed from: c */
    private void m6433c(NPCSpawn aij) {
        aYU().remove(aij);
    }

    @C0064Am(aul = "a00c21dd88bc1a69ffbec3641b208ece", aum = 0)
    private C3438ra<NPCSpawn> bej() {
        return aYU();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Spawn Table")
    @C0064Am(aul = "f614eb178c0321795a03be492af59f9b", aum = 0)
    private List<NPCSpawn> bel() {
        return Collections.unmodifiableList(aYU());
    }

    @C0064Am(aul = "cefd2f6dd3dbb4ecaa1f4f4b405455fc", aum = 0)
    private LinearlyDistribuitedPopulationControl ben() {
        return (LinearlyDistribuitedPopulationControl) mo745aU();
    }

    @C0064Am(aul = "76b141eb7cbcef6176cbd14fc1b00495", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6431aT() {
        T t = (LinearlyDistribuitedPopulationControl) bFf().mo6865M(LinearlyDistribuitedPopulationControl.class);
        t.mo2944a(this);
        return t;
    }
}
