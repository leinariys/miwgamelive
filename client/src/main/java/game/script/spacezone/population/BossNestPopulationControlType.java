package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.spacezone.PopulationControlType;
import logic.baa.*;
import logic.data.mbean.C6255ajT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aQX */
/* compiled from: a */
public class BossNestPopulationControlType extends PopulationControlType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f3623dN = null;
    public static final C5663aRz ggb = null;
    public static final C5663aRz ggf = null;
    public static final C2491fm iGg = null;
    public static final C2491fm iGh = null;
    public static final C2491fm iGi = null;
    public static final C2491fm iGj = null;
    public static final C2491fm iGk = null;
    public static final C2491fm iGl = null;
    public static final C2491fm iGm = null;
    public static final C2491fm iGn = null;
    public static final C2491fm iGo = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "35ec43e51e740ff648917127c5f13059", aum = 0)
    private static C3438ra<NPCSpawn> ftm;
    @C0064Am(aul = "b6cc6b93a8cd27503cc59f362c138497", aum = 1)
    private static C3438ra<StaticNPCSpawn> ftq;

    static {
        m17518V();
    }

    public BossNestPopulationControlType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BossNestPopulationControlType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17518V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PopulationControlType._m_fieldCount + 2;
        _m_methodCount = PopulationControlType._m_methodCount + 10;
        int i = PopulationControlType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BossNestPopulationControlType.class, "35ec43e51e740ff648917127c5f13059", i);
        ggb = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BossNestPopulationControlType.class, "b6cc6b93a8cd27503cc59f362c138497", i2);
        ggf = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) PopulationControlType._m_fields, (Object[]) _m_fields);
        int i4 = PopulationControlType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 10)];
        C2491fm a = C4105zY.m41624a(BossNestPopulationControlType.class, "cf7e8c8af07e44c8b0a5b20ea3494089", i4);
        iGg = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BossNestPopulationControlType.class, "5c0faae4522e401f932ae686b71ff398", i5);
        iGh = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BossNestPopulationControlType.class, "aa1fca9ab04ebd6c5e21ffda1342bad9", i6);
        iGi = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BossNestPopulationControlType.class, "75fa88cbb536677fb90e797a8e6a8553", i7);
        iGj = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BossNestPopulationControlType.class, "8016feb4b3f7e3734adfa94f20f0da0d", i8);
        iGk = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BossNestPopulationControlType.class, "e8426526e002a02ba1d616f9a6f996b6", i9);
        iGl = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(BossNestPopulationControlType.class, "6d75997cede8235b231f3947e10dd07c", i10);
        iGm = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(BossNestPopulationControlType.class, "2fc23dd344f1f16bbfb73159620144f6", i11);
        iGn = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(BossNestPopulationControlType.class, "638af0354560e0c15bad09c4711173d6", i12);
        iGo = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(BossNestPopulationControlType.class, "7edc1f808a76da2b42688312260c4c51", i13);
        f3623dN = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PopulationControlType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BossNestPopulationControlType.class, C6255ajT.class, _m_fields, _m_methods);
    }

    /* renamed from: bZ */
    private void m17521bZ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(ggb, raVar);
    }

    /* renamed from: ca */
    private void m17523ca(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(ggf, raVar);
    }

    private C3438ra cmh() {
        return (C3438ra) bFf().mo5608dq().mo3214p(ggb);
    }

    private C3438ra cml() {
        return (C3438ra) bFf().mo5608dq().mo3214p(ggf);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6255ajT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PopulationControlType._m_methodCount) {
            case 0:
                m17524e((NPCSpawn) args[0]);
                return null;
            case 1:
                m17525g((NPCSpawn) args[0]);
                return null;
            case 2:
                return dqS();
            case 3:
                return dqU();
            case 4:
                m17519a((StaticNPCSpawn) args[0]);
                return null;
            case 5:
                m17522c((StaticNPCSpawn) args[0]);
                return null;
            case 6:
                return dqW();
            case 7:
                return dqY();
            case 8:
                return dra();
            case 9:
                return m17520aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3623dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3623dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3623dN, new Object[0]));
                break;
        }
        return m17520aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Boss Spawn Table")
    /* renamed from: b */
    public void mo11011b(StaticNPCSpawn bg) {
        switch (bFf().mo6893i(iGk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGk, new Object[]{bg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGk, new Object[]{bg}));
                break;
        }
        m17519a(bg);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Boss Spawn Table")
    /* renamed from: d */
    public void mo11012d(StaticNPCSpawn bg) {
        switch (bFf().mo6893i(iGl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGl, new Object[]{bg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGl, new Object[]{bg}));
                break;
        }
        m17522c(bg);
    }

    public C3438ra<NPCSpawn> dqT() {
        switch (bFf().mo6893i(iGi)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, iGi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGi, new Object[0]));
                break;
        }
        return dqS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Creep Spawn Table")
    public List<NPCSpawn> dqV() {
        switch (bFf().mo6893i(iGj)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iGj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGj, new Object[0]));
                break;
        }
        return dqU();
    }

    public C3438ra<StaticNPCSpawn> dqX() {
        switch (bFf().mo6893i(iGm)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, iGm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGm, new Object[0]));
                break;
        }
        return dqW();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Boss Spawn Table")
    public List<StaticNPCSpawn> dqZ() {
        switch (bFf().mo6893i(iGn)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iGn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGn, new Object[0]));
                break;
        }
        return dqY();
    }

    public BossNestPopulationControl drb() {
        switch (bFf().mo6893i(iGo)) {
            case 0:
                return null;
            case 2:
                return (BossNestPopulationControl) bFf().mo5606d(new aCE(this, iGo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGo, new Object[0]));
                break;
        }
        return dra();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Creep Spawn Table")
    /* renamed from: f */
    public void mo11018f(NPCSpawn aij) {
        switch (bFf().mo6893i(iGg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGg, new Object[]{aij}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGg, new Object[]{aij}));
                break;
        }
        m17524e(aij);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Creep Spawn Table")
    /* renamed from: h */
    public void mo11019h(NPCSpawn aij) {
        switch (bFf().mo6893i(iGh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGh, new Object[]{aij}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGh, new Object[]{aij}));
                break;
        }
        m17525g(aij);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Creep Spawn Table")
    @C0064Am(aul = "cf7e8c8af07e44c8b0a5b20ea3494089", aum = 0)
    /* renamed from: e */
    private void m17524e(NPCSpawn aij) {
        cmh().add(aij);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Creep Spawn Table")
    @C0064Am(aul = "5c0faae4522e401f932ae686b71ff398", aum = 0)
    /* renamed from: g */
    private void m17525g(NPCSpawn aij) {
        cmh().remove(aij);
    }

    @C0064Am(aul = "aa1fca9ab04ebd6c5e21ffda1342bad9", aum = 0)
    private C3438ra<NPCSpawn> dqS() {
        return cmh();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Creep Spawn Table")
    @C0064Am(aul = "75fa88cbb536677fb90e797a8e6a8553", aum = 0)
    private List<NPCSpawn> dqU() {
        return Collections.unmodifiableList(cmh());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Boss Spawn Table")
    @C0064Am(aul = "8016feb4b3f7e3734adfa94f20f0da0d", aum = 0)
    /* renamed from: a */
    private void m17519a(StaticNPCSpawn bg) {
        cml().add(bg);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Boss Spawn Table")
    @C0064Am(aul = "e8426526e002a02ba1d616f9a6f996b6", aum = 0)
    /* renamed from: c */
    private void m17522c(StaticNPCSpawn bg) {
        cml().remove(bg);
    }

    @C0064Am(aul = "6d75997cede8235b231f3947e10dd07c", aum = 0)
    private C3438ra<StaticNPCSpawn> dqW() {
        return cml();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Boss Spawn Table")
    @C0064Am(aul = "2fc23dd344f1f16bbfb73159620144f6", aum = 0)
    private List<StaticNPCSpawn> dqY() {
        return Collections.unmodifiableList(cml());
    }

    @C0064Am(aul = "638af0354560e0c15bad09c4711173d6", aum = 0)
    private BossNestPopulationControl dra() {
        return (BossNestPopulationControl) mo745aU();
    }

    @C0064Am(aul = "7edc1f808a76da2b42688312260c4c51", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m17520aT() {
        T t = (BossNestPopulationControl) bFf().mo6865M(BossNestPopulationControl.class);
        t.mo15276a(this);
        return t;
    }
}
