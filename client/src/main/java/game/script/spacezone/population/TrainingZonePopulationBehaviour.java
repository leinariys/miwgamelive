package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.TaikodomObject;
import game.script.npc.NPC;
import game.script.ship.Ship;
import game.script.spacezone.PopulationBehaviour;
import logic.baa.*;
import logic.data.link.C0471GX;
import logic.data.link.C3161oY;
import logic.data.mbean.C3001mp;
import logic.data.mbean.C4085zG;
import logic.data.mbean.C5421aIr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5829abJ("1.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.WU */
/* compiled from: a */
public class TrainingZonePopulationBehaviour extends PopulationBehaviour implements C1616Xf, aOW, C3161oY.C3162a {

    /* renamed from: Do */
    public static final C2491fm f2031Do = null;

    /* renamed from: Lf */
    public static final C2491fm f2032Lf = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f2033x13860637 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cvT = null;
    public static final C2491fm dFT = null;
    public static final C2491fm dlc = null;
    public static final C2491fm dld = null;
    public static final C2491fm dle = null;
    public static final C2491fm dlf = null;
    public static final C2491fm dlg = null;
    public static final C2491fm dlh = null;
    public static final C2491fm dlk = null;
    public static final C5663aRz iDV = null;
    public static final C5663aRz iDW = null;
    public static final C5663aRz iDX = null;
    public static final C5663aRz iDY = null;
    public static final C2491fm iDZ = null;
    public static final C2491fm iEa = null;
    public static final C2491fm iEb = null;
    public static final C2491fm iEc = null;
    public static final C2491fm iEd = null;
    public static final C2491fm iEe = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "67504d0baa5bb32ab9db220d5d29edc1", aum = 0)
    private static boolean icc;
    @C0064Am(aul = "c65a803d3bd43741f724b4224e777bc5", aum = 1)
    private static int icd;
    @C0064Am(aul = "0412a41e7073e56da2a4476e174556f9", aum = 2)
    private static C1556Wo<NPC, CharacterList> ice;
    @C0064Am(aul = "2be56080bb5fb4ebd312d326120a1d33", aum = 3)
    private static C1556Wo<Character, NPCList> icf;

    static {
        m11216V();
    }

    public TrainingZonePopulationBehaviour() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TrainingZonePopulationBehaviour(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11216V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PopulationBehaviour._m_fieldCount + 4;
        _m_methodCount = PopulationBehaviour._m_methodCount + 18;
        int i = PopulationBehaviour._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(TrainingZonePopulationBehaviour.class, "67504d0baa5bb32ab9db220d5d29edc1", i);
        iDV = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TrainingZonePopulationBehaviour.class, "c65a803d3bd43741f724b4224e777bc5", i2);
        iDW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TrainingZonePopulationBehaviour.class, "0412a41e7073e56da2a4476e174556f9", i3);
        iDX = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TrainingZonePopulationBehaviour.class, "2be56080bb5fb4ebd312d326120a1d33", i4);
        iDY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) PopulationBehaviour._m_fields, (Object[]) _m_fields);
        int i6 = PopulationBehaviour._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 18)];
        C2491fm a = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "55bfbf7b3d9da456a9fa028566aced77", i6);
        iDZ = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "d6e1591cf816419fe263e810b3c5ddf8", i7);
        iEa = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "5665d6dfe67fdc5daacce5d540db7716", i8);
        iEb = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "a554e5b979c0c6203f4ba268a1777aea", i9);
        iEc = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "4f3d165933527a12b4a9d314209ed10c", i10);
        iEd = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "e5aec672a3e59609ff7eed2bcd65157f", i11);
        f2032Lf = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "b846861370c0b6869f8adf6a25c57cdf", i12);
        dlc = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "63f5745af7dcf47deedc0b2351e2a6b1", i13);
        iEe = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "46c8c899ac2b980af6b83d825d7337d5", i14);
        dld = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "c611f60a06fc6db0c75cffde9702ac8d", i15);
        dle = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "3b12eba17668220a58f84ce24b8edca2", i16);
        dlf = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "1824d3419ad7c33097750d70170f6f6d", i17);
        dlg = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "9f4c40a657e648b77a08bbf50474fc72", i18);
        dlh = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "8b032e67d13bddc591282e6cb5f09075", i19);
        dFT = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "fde46863704decc21081f0e9563c163e", i20);
        cvT = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "e163bdcdd38602066cddbdc3246066c6", i21);
        dlk = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "691bd1e62c738e3068430ba5dd1084af", i22);
        f2031Do = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(TrainingZonePopulationBehaviour.class, "1779e9794edc887afd75167fc8b35493", i23);
        f2033x13860637 = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PopulationBehaviour._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TrainingZonePopulationBehaviour.class, C5421aIr.class, _m_fields, _m_methods);
    }

    /* renamed from: Ag */
    private void m11211Ag(int i) {
        bFf().mo5608dq().mo3183b(iDW, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attacking NPCs per Invader")
    @C0064Am(aul = "d6e1591cf816419fe263e810b3c5ddf8", aum = 0)
    @C5566aOg
    /* renamed from: Ah */
    private void m11212Ah(int i) {
        throw new aWi(new aCE(this, iEa, new Object[]{new Integer(i)}));
    }

    /* renamed from: aa */
    private void m11222aa(Character acx) {
        switch (bFf().mo6893i(iEe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEe, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEe, new Object[]{acx}));
                break;
        }
        m11217Z(acx);
    }

    /* renamed from: ao */
    private void m11223ao(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iDX, wo);
    }

    /* renamed from: ap */
    private void m11224ap(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iDY, wo);
    }

    private boolean dpg() {
        return bFf().mo5608dq().mo3201h(iDV);
    }

    private int dph() {
        return bFf().mo5608dq().mo3212n(iDW);
    }

    private C1556Wo dpi() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iDX);
    }

    private C1556Wo dpj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iDY);
    }

    /* renamed from: jZ */
    private void m11227jZ(boolean z) {
        bFf().mo5608dq().mo3153a(iDV, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC reacts when attacked")
    @C0064Am(aul = "a554e5b979c0c6203f4ba268a1777aea", aum = 0)
    @C5566aOg
    /* renamed from: ka */
    private void m11228ka(boolean z) {
        throw new aWi(new aCE(this, iEc, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Attacking NPCs per Invader")
    @C5566aOg
    /* renamed from: Ai */
    public void mo6579Ai(int i) {
        switch (bFf().mo6893i(iEa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEa, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEa, new Object[]{new Integer(i)}));
                break;
        }
        m11212Ah(i);
    }

    /* renamed from: D */
    public void mo2941D(Character acx) {
        switch (bFf().mo6893i(dld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                break;
        }
        m11213C(acx);
    }

    /* renamed from: F */
    public void mo2942F(Character acx) {
        switch (bFf().mo6893i(dle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                break;
        }
        m11214E(acx);
    }

    /* renamed from: H */
    public void mo2943H(Character acx) {
        switch (bFf().mo6893i(dlf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                break;
        }
        m11215G(acx);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5421aIr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PopulationBehaviour._m_methodCount) {
            case 0:
                return new Integer(dpk());
            case 1:
                m11212Ah(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Boolean(dpm());
            case 3:
                m11228ka(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return dpo();
            case 5:
                m11219a((NPC) args[0]);
                return null;
            case 6:
                m11226e((NPC) args[0]);
                return null;
            case 7:
                m11217Z((Character) args[0]);
                return null;
            case 8:
                m11213C((Character) args[0]);
                return null;
            case 9:
                m11214E((Character) args[0]);
                return null;
            case 10:
                m11215G((Character) args[0]);
                return null;
            case 11:
                aZn();
                return null;
            case 12:
                aZp();
                return null;
            case 13:
                m11220a((NPC) args[0], (Character) args[1]);
                return null;
            case 14:
                aBx();
                return null;
            case 15:
                m11221a((Character) args[0], (Character) args[1]);
                return null;
            case 16:
                m11218a((C0665JT) args[0]);
                return null;
            case 17:
                m11225b((aDJ) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public void aBy() {
        switch (bFf().mo6893i(cvT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cvT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cvT, new Object[0]));
                break;
        }
        aBx();
    }

    public void aZo() {
        switch (bFf().mo6893i(dlg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                break;
        }
        aZn();
    }

    public void aZq() {
        switch (bFf().mo6893i(dlh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                break;
        }
        aZp();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2031Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2031Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2031Do, new Object[]{jt}));
                break;
        }
        m11218a(jt);
    }

    /* renamed from: b */
    public void mo6580b(NPC auf) {
        switch (bFf().mo6893i(f2032Lf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2032Lf, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2032Lf, new Object[]{auf}));
                break;
        }
        m11219a(auf);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo6581b(NPC auf, Character acx) {
        switch (bFf().mo6893i(dFT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFT, new Object[]{auf, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFT, new Object[]{auf, acx}));
                break;
        }
        m11220a(auf, acx);
    }

    /* renamed from: b */
    public void mo2952b(Character acx, Character acx2) {
        switch (bFf().mo6893i(dlk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                break;
        }
        m11221a(acx, acx2);
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f2033x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2033x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2033x13860637, new Object[]{adj}));
                break;
        }
        m11225b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attacking NPCs per Invader")
    public int dpl() {
        switch (bFf().mo6893i(iDZ)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, iDZ, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, iDZ, new Object[0]));
                break;
        }
        return dpk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC reacts when attacked")
    public boolean dpn() {
        switch (bFf().mo6893i(iEb)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iEb, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iEb, new Object[0]));
                break;
        }
        return dpm();
    }

    /* access modifiers changed from: protected */
    public Map<Character, NPCList> dpp() {
        switch (bFf().mo6893i(iEd)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, iEd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iEd, new Object[0]));
                break;
        }
        return dpo();
    }

    /* renamed from: f */
    public void mo2953f(NPC auf) {
        switch (bFf().mo6893i(dlc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                break;
        }
        m11226e(auf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC reacts when attacked")
    @C5566aOg
    /* renamed from: kb */
    public void mo6585kb(boolean z) {
        switch (bFf().mo6893i(iEc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEc, new Object[]{new Boolean(z)}));
                break;
        }
        m11228ka(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Attacking NPCs per Invader")
    @C0064Am(aul = "55bfbf7b3d9da456a9fa028566aced77", aum = 0)
    private int dpk() {
        return dph();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC reacts when attacked")
    @C0064Am(aul = "5665d6dfe67fdc5daacce5d540db7716", aum = 0)
    private boolean dpm() {
        return dpg();
    }

    @C0064Am(aul = "4f3d165933527a12b4a9d314209ed10c", aum = 0)
    private Map<Character, NPCList> dpo() {
        return dpj();
    }

    @C0064Am(aul = "e5aec672a3e59609ff7eed2bcd65157f", aum = 0)
    /* renamed from: a */
    private void m11219a(NPC auf) {
        auf.mo8348d(C3161oY.C3162a.class, this);
        Controller hb = auf.mo12657hb();
        C6491anv.m24341a(hb, mo8669Uy().getPosition(), mo8669Uy().mo20316Lu());
        C6491anv.m24344f(hb);
        if (mo8669Uy().isActive()) {
            aBy();
        }
    }

    @C0064Am(aul = "b846861370c0b6869f8adf6a25c57cdf", aum = 0)
    /* renamed from: e */
    private void m11226e(NPC auf) {
        Iterator it = dpj().keySet().iterator();
        while (true) {
            if (it.hasNext()) {
                NPCList aVar = (NPCList) dpj().get((Character) it.next());
                if (aVar != null && aVar.bEX().contains(auf)) {
                    mo6581b(auf, (Character) null);
                    aVar.bEX().remove(auf);
                    break;
                }
            } else {
                break;
            }
        }
        CharacterList bVar = (CharacterList) dpi().remove(auf);
        if (bVar != null) {
            for (Character P : bVar.diH()) {
                P.mo12633P((Character) auf);
            }
            bVar.diH().clear();
        }
        if (mo8669Uy().isActive()) {
            aBy();
        }
    }

    @C0064Am(aul = "63f5745af7dcf47deedc0b2351e2a6b1", aum = 0)
    /* renamed from: Z */
    private void m11217Z(Character acx) {
        NPCList aVar = (NPCList) dpj().remove(acx);
        if (aVar != null) {
            for (NPC b : aVar.bEX()) {
                mo6581b(b, (Character) null);
            }
            if (mo8669Uy().isActive()) {
                aBy();
                return;
            }
            return;
        }
        mo6317hy("Character " + acx.getName() + " called onDeath() from NPCZone '" + mo8669Uy().getName() + "' but his assignment list is null");
        Ship bQx = acx.bQx();
        if (bQx != null) {
            bQx.mo8355h(C0471GX.C0472a.class, mo8669Uy());
        }
    }

    @C0064Am(aul = "46c8c899ac2b980af6b83d825d7337d5", aum = 0)
    /* renamed from: C */
    private void m11213C(Character acx) {
        m11222aa(acx);
    }

    @C0064Am(aul = "c611f60a06fc6db0c75cffde9702ac8d", aum = 0)
    /* renamed from: E */
    private void m11214E(Character acx) {
        acx.mo8348d(C3161oY.C3162a.class, this);
        if (!dpj().containsKey(acx)) {
            C1556Wo dpj = dpj();
            NPCList aVar = (NPCList) bFf().mo6865M(NPCList.class);
            aVar.mo10S();
            dpj.put(acx, aVar);
        }
        if (mo8669Uy().isActive()) {
            aBy();
        }
    }

    @C0064Am(aul = "3b12eba17668220a58f84ce24b8edca2", aum = 0)
    /* renamed from: G */
    private void m11215G(Character acx) {
        m11222aa(acx);
    }

    @C0064Am(aul = "1824d3419ad7c33097750d70170f6f6d", aum = 0)
    private void aZn() {
        aBy();
    }

    @C0064Am(aul = "9f4c40a657e648b77a08bbf50474fc72", aum = 0)
    private void aZp() {
        for (Character acx : dpj().keySet()) {
            NPCList aVar = (NPCList) dpj().get(acx);
            if (aVar != null) {
                for (NPC b : aVar.bEX()) {
                    mo6581b(b, (Character) null);
                }
                aVar.bEX().clear();
            }
        }
        dpj().clear();
        for (NPC auf : dpi().keySet()) {
            CharacterList bVar = (CharacterList) dpi().get(auf);
            if (bVar != null) {
                for (Character P : bVar.diH()) {
                    P.mo12633P((Character) auf);
                }
                bVar.diH().clear();
            }
        }
        dpi().clear();
    }

    @C0064Am(aul = "8b032e67d13bddc591282e6cb5f09075", aum = 0)
    /* renamed from: a */
    private void m11220a(NPC auf, Character acx) {
        C6534aom aom;
        if (auf.isDisposed()) {
            mo8358lY("NPC " + auf.bFY() + " is disposed and being notified by zone's AI");
            return;
        }
        Controller hb = auf.mo12657hb();
        if ((hb instanceof C6534aom) && (aom = (C6534aom) hb) != null) {
            if (acx == null) {
                Ship bUT = aom.bUT();
                if (bUT != null) {
                    Character agj = bUT.agj();
                    agj.mo12633P((Character) auf);
                    CharacterList bVar = (CharacterList) dpi().get(auf);
                    if (bVar != null) {
                        bVar.diH().remove(agj);
                    }
                    aom.mo4588J((Ship) null);
                    return;
                }
                return;
            }
            acx.mo12632N((Character) auf);
            CharacterList bVar2 = (CharacterList) dpi().get(auf);
            if (bVar2 == null) {
                bVar2 = (CharacterList) bFf().mo6865M(CharacterList.class);
                bVar2.mo10S();
                dpi().put(auf, bVar2);
            }
            bVar2.diH().add(acx);
            aom.mo4588J(acx.bQx());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:?, code lost:
        return;
     */
    @p001a.C0064Am(aul = "fde46863704decc21081f0e9563c163e", aum = 0)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void aBx() {
        /*
            r12 = this;
            java.util.ArrayList r7 = new java.util.ArrayList
            a.fw r0 = r12.mo8669Uy()
            java.util.List r0 = r0.mo18875qR()
            r7.<init>(r0)
            a.Wo r0 = r12.dpj()
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
        L_0x0019:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x003c
            java.util.Iterator r1 = r7.iterator()
        L_0x0023:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x004c
            a.Wo r0 = r12.dpj()
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r8 = r0.iterator()
        L_0x0035:
            boolean r0 = r8.hasNext()
            if (r0 != 0) goto L_0x005e
        L_0x003b:
            return
        L_0x003c:
            java.lang.Object r0 = r1.next()
            a.WU$a r0 = (p001a.C1528WU.C1529a) r0
            if (r0 == 0) goto L_0x0019
            a.ra r0 = r0.bEX()
            r7.removeAll(r0)
            goto L_0x0019
        L_0x004c:
            java.lang.Object r0 = r1.next()
            a.aUf r0 = (p001a.C5721aUf) r0
            a.Jx r0 = r0.mo12657hb()
            boolean r0 = r0 instanceof p001a.C6534aom
            if (r0 != 0) goto L_0x0023
            r1.remove()
            goto L_0x0023
        L_0x005e:
            java.lang.Object r0 = r8.next()
            a.acX r0 = (p001a.C5895acX) r0
            if (r0 == 0) goto L_0x0035
            a.Wo r1 = r12.dpj()
            java.lang.Object r1 = r1.get(r0)
            a.WU$a r1 = (p001a.C1528WU.C1529a) r1
            if (r1 == 0) goto L_0x0035
            a.fA r9 = r0.bQx()
            if (r9 == 0) goto L_0x0035
            int r2 = r12.dph()
            a.ra r3 = r1.bEX()
            int r3 = r3.size()
            int r2 = r2 - r3
        L_0x0085:
            int r6 = r2 + -1
            if (r2 <= 0) goto L_0x0035
            int r2 = r7.size()
            if (r2 <= 0) goto L_0x0035
            r4 = 0
            r2 = 2139095039(0x7f7fffff, float:3.4028235E38)
            java.util.Iterator r10 = r7.iterator()
            r3 = r2
            r5 = r4
        L_0x0099:
            boolean r2 = r10.hasNext()
            if (r2 != 0) goto L_0x00b0
            if (r5 == 0) goto L_0x00ae
            r7.remove(r5)
            a.ra r2 = r1.bEX()
            r2.add(r5)
            r12.mo6581b((p001a.C5721aUf) r5, (p001a.C5895acX) r0)
        L_0x00ae:
            r2 = r6
            goto L_0x0085
        L_0x00b0:
            java.lang.Object r2 = r10.next()
            a.aUf r2 = (p001a.C5721aUf) r2
            a.fA r4 = r2.bQx()
            if (r4 == 0) goto L_0x0099
            float r4 = r4.mo1011bv((p001a.C0192CR) r9)
            boolean r11 = java.lang.Float.isInfinite(r4)
            if (r11 != 0) goto L_0x00cc
            boolean r11 = java.lang.Float.isNaN(r4)
            if (r11 == 0) goto L_0x00e0
        L_0x00cc:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "distance is is NaN or Infinite: "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            r12.mo6317hy(r0)
            goto L_0x003b
        L_0x00e0:
            int r11 = (r4 > r3 ? 1 : (r4 == r3 ? 0 : -1))
            if (r11 >= 0) goto L_0x0099
            r3 = r4
            r5 = r2
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1528WU.aBx():void");
    }

    @C0064Am(aul = "e163bdcdd38602066cddbdc3246066c6", aum = 0)
    /* renamed from: a */
    private void m11221a(Character acx, Character acx2) {
        if (dpg() && (acx instanceof NPC)) {
            for (Character acx3 : dpj().keySet()) {
                if (((NPCList) dpj().get(acx3)).bEX().contains(acx)) {
                    return;
                }
            }
            NPCList aVar = (NPCList) dpj().get(acx2);
            if (aVar == null) {
                aVar = (NPCList) bFf().mo6865M(NPCList.class);
                aVar.mo10S();
                dpj().put(acx2, aVar);
            }
            aVar.bEX().add((NPC) acx);
            mo6581b((NPC) acx, acx2);
        }
    }

    @C0064Am(aul = "691bd1e62c738e3068430ba5dd1084af", aum = 0)
    /* renamed from: a */
    private void m11218a(C0665JT jt) {
        if ("".equals(jt.getVersion())) {
            dpj().clear();
            HashMap hashMap = (HashMap) jt.get("assignments");
            if (hashMap != null) {
                for (C0665JT jt2 : hashMap.keySet()) {
                    NPCList aVar = (NPCList) bFf().mo6865M(NPCList.class);
                    aVar.mo10S();
                    if (!(hashMap.get(jt2) instanceof C0665JT)) {
                        ArrayList arrayList = (ArrayList) hashMap.get(jt2);
                        if (arrayList != null) {
                            Iterator it = arrayList.iterator();
                            while (it.hasNext()) {
                                NPC auf = (NPC) ((C0665JT) it.next()).baw();
                                if (auf != null) {
                                    aVar.bEX().add(auf);
                                }
                            }
                            dpj().put((Character) jt2.baw(), aVar);
                            arrayList.clear();
                        }
                    } else {
                        return;
                    }
                }
                hashMap.clear();
            }
        }
    }

    @C0064Am(aul = "1779e9794edc887afd75167fc8b35493", aum = 0)
    /* renamed from: b */
    private void m11225b(aDJ adj) {
        dpi().remove(adj);
        dpj().remove(adj);
        for (CharacterList diH : dpi().values()) {
            diH.diH().remove(adj);
        }
        for (NPCList bEX : dpj().values()) {
            bEX.bEX().remove(adj);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.WU$b */
    /* compiled from: a */
    public static class CharacterList extends TaikodomObject implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C5663aRz inI = null;
        public static final C2491fm inJ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "3396cc5b617c458ca6af778b5392beb2", aum = 0)
        private static C3438ra<Character> caw;

        static {
            m11256V();
        }

        public CharacterList() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CharacterList(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m11256V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaikodomObject._m_fieldCount + 1;
            _m_methodCount = TaikodomObject._m_methodCount + 1;
            int i = TaikodomObject._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CharacterList.class, "3396cc5b617c458ca6af778b5392beb2", i);
            inI = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
            int i3 = TaikodomObject._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(CharacterList.class, "0217ec8f93450b72550818203f7f5e62", i3);
            inJ = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CharacterList.class, C4085zG.class, _m_fields, _m_methods);
        }

        /* renamed from: cw */
        private void m11257cw(C3438ra raVar) {
            bFf().mo5608dq().mo3197f(inI, raVar);
        }

        private C3438ra diF() {
            return (C3438ra) bFf().mo5608dq().mo3214p(inI);
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C4085zG(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
                case 0:
                    return diG();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public C3438ra<Character> diH() {
            switch (bFf().mo6893i(inJ)) {
                case 0:
                    return null;
                case 2:
                    return (C3438ra) bFf().mo5606d(new aCE(this, inJ, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, inJ, new Object[0]));
                    break;
            }
            return diG();
        }

        /* access modifiers changed from: protected */
        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        @C0064Am(aul = "0217ec8f93450b72550818203f7f5e62", aum = 0)
        private C3438ra<Character> diG() {
            return diF();
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.WU$a */
    public static class NPCList extends TaikodomObject implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C5663aRz eAe = null;
        public static final C2491fm eAf = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "45b1eda594836b596d9b3e401b7dc67e", aum = 0)
        private static C3438ra<NPC> aCe;

        static {
            m11247V();
        }

        public NPCList() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public NPCList(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m11247V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaikodomObject._m_fieldCount + 1;
            _m_methodCount = TaikodomObject._m_methodCount + 1;
            int i = TaikodomObject._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(NPCList.class, "45b1eda594836b596d9b3e401b7dc67e", i);
            eAe = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
            int i3 = TaikodomObject._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(NPCList.class, "151ecc07a3270c5a63566b2cb32034d3", i3);
            eAf = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(NPCList.class, C3001mp.class, _m_fields, _m_methods);
        }

        private C3438ra bEV() {
            return (C3438ra) bFf().mo5608dq().mo3214p(eAe);
        }

        /* renamed from: bx */
        private void m11248bx(C3438ra raVar) {
            bFf().mo5608dq().mo3197f(eAe, raVar);
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3001mp(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
                case 0:
                    return bEW();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public C3438ra<NPC> bEX() {
            switch (bFf().mo6893i(eAf)) {
                case 0:
                    return null;
                case 2:
                    return (C3438ra) bFf().mo5606d(new aCE(this, eAf, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, eAf, new Object[0]));
                    break;
            }
            return bEW();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        @C0064Am(aul = "151ecc07a3270c5a63566b2cb32034d3", aum = 0)
        private C3438ra<NPC> bEW() {
            return bEV();
        }
    }
}
