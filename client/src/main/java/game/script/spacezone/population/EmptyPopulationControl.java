package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.npc.NPC;
import game.script.spacezone.PopulationControl;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6668arQ;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.Yg */
/* compiled from: a */
public class EmptyPopulationControl extends PopulationControl implements C1616Xf {

    /* renamed from: Lg */
    public static final C2491fm f2185Lg = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm dla = null;
    public static final C2491fm dlc = null;
    public static final C2491fm dld = null;
    public static final C2491fm dle = null;
    public static final C2491fm dlf = null;
    public static final C2491fm dlg = null;
    public static final C2491fm dlh = null;
    public static final C2491fm dlk = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m11896V();
    }

    public EmptyPopulationControl() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public EmptyPopulationControl(EmptyPopulationControlType anp) {
        super((C5540aNg) null);
        super._m_script_init(anp);
    }

    public EmptyPopulationControl(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11896V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PopulationControl._m_fieldCount + 0;
        _m_methodCount = PopulationControl._m_methodCount + 9;
        _m_fields = new C5663aRz[(PopulationControl._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) PopulationControl._m_fields, (Object[]) _m_fields);
        int i = PopulationControl._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 9)];
        C2491fm a = C4105zY.m41624a(EmptyPopulationControl.class, "19dbd63075af13102558b029ea1e24f2", i);
        f2185Lg = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(EmptyPopulationControl.class, "548cb5e797dfdcf0ac5470687680c633", i2);
        dld = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(EmptyPopulationControl.class, "5599cce94762a7712b8b8219dffaf2c5", i3);
        dle = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(EmptyPopulationControl.class, "c584a2eceadb0ab0652c924ca22123da", i4);
        dlf = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(EmptyPopulationControl.class, "570a47aab19f2cb9b4d07c550ed84f4a", i5);
        dlc = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(EmptyPopulationControl.class, "827320bebed057379794bb0965b68e98", i6);
        dlg = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(EmptyPopulationControl.class, "73ad159822f23b424a984b15f076f3cc", i7);
        dlh = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(EmptyPopulationControl.class, "4bba133d41eaa3cbc3a2e22c7cbdb5e9", i8);
        dlk = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(EmptyPopulationControl.class, "5c78874205326be426c6415a34c56420", i9);
        dla = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PopulationControl._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(EmptyPopulationControl.class, C6668arQ.class, _m_fields, _m_methods);
    }

    /* renamed from: D */
    public void mo2941D(Character acx) {
        switch (bFf().mo6893i(dld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                break;
        }
        m11893C(acx);
    }

    /* renamed from: F */
    public void mo2942F(Character acx) {
        switch (bFf().mo6893i(dle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                break;
        }
        m11894E(acx);
    }

    /* renamed from: H */
    public void mo2943H(Character acx) {
        switch (bFf().mo6893i(dlf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                break;
        }
        m11895G(acx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6668arQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PopulationControl._m_methodCount) {
            case 0:
                return m11899qQ();
            case 1:
                m11893C((Character) args[0]);
                return null;
            case 2:
                m11894E((Character) args[0]);
                return null;
            case 3:
                m11895G((Character) args[0]);
                return null;
            case 4:
                m11898e((NPC) args[0]);
                return null;
            case 5:
                aZn();
                return null;
            case 6:
                aZp();
                return null;
            case 7:
                m11897a((Character) args[0], (Character) args[1]);
                return null;
            case 8:
                aZk();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public void aZl() {
        switch (bFf().mo6893i(dla)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                break;
        }
        aZk();
    }

    public void aZo() {
        switch (bFf().mo6893i(dlg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                break;
        }
        aZn();
    }

    public void aZq() {
        switch (bFf().mo6893i(dlh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                break;
        }
        aZp();
    }

    /* renamed from: b */
    public void mo2952b(Character acx, Character acx2) {
        switch (bFf().mo6893i(dlk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                break;
        }
        m11897a(acx, acx2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: f */
    public void mo2953f(NPC auf) {
        switch (bFf().mo6893i(dlc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                break;
        }
        m11898e(auf);
    }

    /* renamed from: qR */
    public List<NPC> mo2957qR() {
        switch (bFf().mo6893i(f2185Lg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f2185Lg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2185Lg, new Object[0]));
                break;
        }
        return m11899qQ();
    }

    /* renamed from: a */
    public void mo7247a(EmptyPopulationControlType anp) {
        super.mo14333h(anp);
    }

    @C0064Am(aul = "19dbd63075af13102558b029ea1e24f2", aum = 0)
    /* renamed from: qQ */
    private List<NPC> m11899qQ() {
        return new ArrayList();
    }

    @C0064Am(aul = "548cb5e797dfdcf0ac5470687680c633", aum = 0)
    /* renamed from: C */
    private void m11893C(Character acx) {
    }

    @C0064Am(aul = "5599cce94762a7712b8b8219dffaf2c5", aum = 0)
    /* renamed from: E */
    private void m11894E(Character acx) {
    }

    @C0064Am(aul = "c584a2eceadb0ab0652c924ca22123da", aum = 0)
    /* renamed from: G */
    private void m11895G(Character acx) {
    }

    @C0064Am(aul = "570a47aab19f2cb9b4d07c550ed84f4a", aum = 0)
    /* renamed from: e */
    private void m11898e(NPC auf) {
    }

    @C0064Am(aul = "827320bebed057379794bb0965b68e98", aum = 0)
    private void aZn() {
    }

    @C0064Am(aul = "73ad159822f23b424a984b15f076f3cc", aum = 0)
    private void aZp() {
    }

    @C0064Am(aul = "4bba133d41eaa3cbc3a2e22c7cbdb5e9", aum = 0)
    /* renamed from: a */
    private void m11897a(Character acx, Character acx2) {
    }

    @C0064Am(aul = "5c78874205326be426c6415a34c56420", aum = 0)
    private void aZk() {
    }
}
