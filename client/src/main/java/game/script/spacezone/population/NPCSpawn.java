package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C1118QR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aIJ */
/* compiled from: a */
public class NPCSpawn extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f3042bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3043bM = null;
    /* renamed from: bN */
    public static final C2491fm f3044bN = null;
    /* renamed from: bO */
    public static final C2491fm f3045bO = null;
    /* renamed from: bP */
    public static final C2491fm f3046bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3047bQ = null;
    public static final C5663aRz bfK = null;
    public static final C2491fm bfQ = null;
    public static final C2491fm bfR = null;
    public static final C5663aRz idb = null;
    public static final C2491fm idc = null;
    public static final C2491fm idd = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5aa6c3e109b68413669cf3785facf699", aum = 2)

    /* renamed from: bK */
    private static UUID f3041bK;
    @C0064Am(aul = "900ac7392aaacff26e0f6729cb033339", aum = 1)
    private static int dXm;
    @C0064Am(aul = "47f7a456f6537a56cc5f3ddff6e5f4ad", aum = 3)
    private static String handle;
    @C0064Am(aul = "55e40dce4d3d92b48256005651b38513", aum = 0)

    /* renamed from: nC */
    private static NPCType f3048nC;

    static {
        m15371V();
    }

    public NPCSpawn() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCSpawn(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15371V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(NPCSpawn.class, "55e40dce4d3d92b48256005651b38513", i);
        bfK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCSpawn.class, "900ac7392aaacff26e0f6729cb033339", i2);
        idb = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPCSpawn.class, "5aa6c3e109b68413669cf3785facf699", i3);
        f3042bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NPCSpawn.class, "47f7a456f6537a56cc5f3ddff6e5f4ad", i4);
        f3043bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(NPCSpawn.class, "9a3bb6c7f15a35ae0b805ec27c79ebc8", i6);
        f3044bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCSpawn.class, "9c7aa33fd60fa63e11c6010d979e73a0", i7);
        f3045bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCSpawn.class, "be4b72bcdda04fef7de87160a2496b3a", i8);
        f3046bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCSpawn.class, "e49da5bc38839a235491f1cf37ebf437", i9);
        f3047bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCSpawn.class, "b873b21a65dec03e3f3bada0ec60303f", i10);
        bfQ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCSpawn.class, "5d0ccaf2be3bb1bf7f7af0fccee3cc85", i11);
        bfR = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCSpawn.class, "fae063b6275986aea33c8469ad7bda93", i12);
        idc = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCSpawn.class, "d2d5a416e2bcfafa394c8ea69e83497a", i13);
        idd = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCSpawn.class, C1118QR.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m15372a(String str) {
        bFf().mo5608dq().mo3197f(f3043bM, str);
    }

    /* renamed from: a */
    private void m15373a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3042bL, uuid);
    }

    private NPCType abn() {
        return (NPCType) bFf().mo5608dq().mo3214p(bfK);
    }

    /* renamed from: an */
    private UUID m15374an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3042bL);
    }

    /* renamed from: ao */
    private String m15375ao() {
        return (String) bFf().mo5608dq().mo3214p(f3043bM);
    }

    /* renamed from: b */
    private void m15378b(NPCType aed) {
        bFf().mo5608dq().mo3197f(bfK, aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "e49da5bc38839a235491f1cf37ebf437", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m15379b(String str) {
        throw new aWi(new aCE(this, f3047bQ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC type")
    @C0064Am(aul = "5d0ccaf2be3bb1bf7f7af0fccee3cc85", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m15381c(NPCType aed) {
        throw new aWi(new aCE(this, bfR, new Object[]{aed}));
    }

    /* renamed from: c */
    private void m15382c(UUID uuid) {
        switch (bFf().mo6893i(f3045bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3045bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3045bO, new Object[]{uuid}));
                break;
        }
        m15380b(uuid);
    }

    private int dff() {
        return bFf().mo5608dq().mo3212n(idb);
    }

    /* renamed from: yp */
    private void m15383yp(int i) {
        bFf().mo5608dq().mo3183b(idb, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Probability")
    @C0064Am(aul = "d2d5a416e2bcfafa394c8ea69e83497a", aum = 0)
    @C5566aOg
    /* renamed from: yq */
    private void m15384yq(int i) {
        throw new aWi(new aCE(this, idd, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1118QR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m15376ap();
            case 1:
                m15380b((UUID) args[0]);
                return null;
            case 2:
                return m15377ar();
            case 3:
                m15379b((String) args[0]);
                return null;
            case 4:
                return abr();
            case 5:
                m15381c((NPCType) args[0]);
                return null;
            case 6:
                return new Integer(dfg());
            case 7:
                m15384yq(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC type")
    public NPCType abs() {
        switch (bFf().mo6893i(bfQ)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, bfQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfQ, new Object[0]));
                break;
        }
        return abr();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3044bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3044bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3044bN, new Object[0]));
                break;
        }
        return m15376ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC type")
    @C5566aOg
    /* renamed from: d */
    public void mo9339d(NPCType aed) {
        switch (bFf().mo6893i(bfR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfR, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfR, new Object[]{aed}));
                break;
        }
        m15381c(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Probability")
    public int dfh() {
        switch (bFf().mo6893i(idc)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, idc, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, idc, new Object[0]));
                break;
        }
        return dfg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3046bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3046bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3046bP, new Object[0]));
                break;
        }
        return m15377ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3047bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3047bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3047bQ, new Object[]{str}));
                break;
        }
        m15379b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Probability")
    @C5566aOg
    /* renamed from: yr */
    public void mo9342yr(int i) {
        switch (bFf().mo6893i(idd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, idd, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, idd, new Object[]{new Integer(i)}));
                break;
        }
        m15384yq(i);
    }

    @C0064Am(aul = "9a3bb6c7f15a35ae0b805ec27c79ebc8", aum = 0)
    /* renamed from: ap */
    private UUID m15376ap() {
        return m15374an();
    }

    @C0064Am(aul = "9c7aa33fd60fa63e11c6010d979e73a0", aum = 0)
    /* renamed from: b */
    private void m15380b(UUID uuid) {
        m15373a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m15373a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "be4b72bcdda04fef7de87160a2496b3a", aum = 0)
    /* renamed from: ar */
    private String m15377ar() {
        return m15375ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC type")
    @C0064Am(aul = "b873b21a65dec03e3f3bada0ec60303f", aum = 0)
    private NPCType abr() {
        return abn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Probability")
    @C0064Am(aul = "fae063b6275986aea33c8469ad7bda93", aum = 0)
    private int dfg() {
        return dff();
    }
}
