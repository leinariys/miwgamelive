package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Character;
import game.script.npc.NPC;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1739Zh;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

@C5829abJ("1.1.0")
@C5511aMd
@C6485anp
/* renamed from: a.Cy */
/* compiled from: a */
public class FactionedZonePopulationBehaviour extends TrainingZonePopulationBehaviour implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cvT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m1880V();
    }

    public FactionedZonePopulationBehaviour() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FactionedZonePopulationBehaviour(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m1880V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TrainingZonePopulationBehaviour._m_fieldCount + 0;
        _m_methodCount = TrainingZonePopulationBehaviour._m_methodCount + 1;
        _m_fields = new C5663aRz[(TrainingZonePopulationBehaviour._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TrainingZonePopulationBehaviour._m_fields, (Object[]) _m_fields);
        int i = TrainingZonePopulationBehaviour._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(FactionedZonePopulationBehaviour.class, "255513fa0dba5f7ee0882ad9cb597508", i);
        cvT = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TrainingZonePopulationBehaviour._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FactionedZonePopulationBehaviour.class, C1739Zh.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1739Zh(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - TrainingZonePopulationBehaviour._m_methodCount) {
            case 0:
                aBx();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public void aBy() {
        switch (bFf().mo6893i(cvT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cvT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cvT, new Object[0]));
                break;
        }
        aBx();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "255513fa0dba5f7ee0882ad9cb597508", aum = 0)
    private void aBx() {
        Map<Character, NPCList> dpp = dpp();
        ArrayList<NPC> arrayList = new ArrayList<>(mo8669Uy().mo18875qR());
        for (NPCList next : dpp.values()) {
            if (next != null) {
                arrayList.removeAll(next.bEX());
            }
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            if (!(((NPC) it.next()).mo12657hb() instanceof C6534aom)) {
                it.remove();
            }
        }
        for (Character next2 : dpp.keySet()) {
            NPCList aVar = dpp.get(next2);
            if (aVar == null) {
                aVar = (NPCList) bFf().mo6865M(NPCList.class);
                aVar.mo10S();
                dpp.put(next2, aVar);
            }
            NPCList aVar2 = aVar;
            if (aVar2.bEX().size() < dpl()) {
                ArrayList<NPC> arrayList2 = new ArrayList<>();
                for (NPC auf : arrayList) {
                    if (auf.azN().mo6826x(next2.azN())) {
                        arrayList2.add(auf);
                    }
                }
                arrayList.removeAll(arrayList2);
                Ship bQx = next2.bQx();
                if (bQx != null) {
                    int dpl = dpl() - aVar2.bEX().size();
                    while (true) {
                        int i = dpl - 1;
                        if (dpl <= 0 || arrayList2.size() <= 0) {
                            break;
                        }
                        float f = Float.MAX_VALUE;
                        NPC auf2 = null;
                        for (NPC auf3 : arrayList2) {
                            Ship bQx2 = auf3.bQx();
                            if (bQx2 != null) {
                                float bv = bQx2.mo1011bv((Actor) bQx);
                                if (bv < f) {
                                    f = bv;
                                    auf2 = auf3;
                                }
                            }
                        }
                        if (auf2 != null) {
                            arrayList2.remove(auf2);
                            aVar2.bEX().add(auf2);
                            mo6581b(auf2, next2);
                        }
                        dpl = i;
                    }
                }
            }
        }
    }
}
