package game.script.spacezone.population;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.npc.NPC;
import game.script.spacezone.PopulationBehaviour;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1812aN;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.asM  reason: case insensitive filesystem */
/* compiled from: a */
public class ShootingZonePopulationBehaviour extends PopulationBehaviour implements C1616Xf {

    /* renamed from: Lf */
    public static final C2491fm f5279Lf = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm dlc = null;
    public static final C2491fm dld = null;
    public static final C2491fm dle = null;
    public static final C2491fm dlf = null;
    public static final C2491fm dlg = null;
    public static final C2491fm dlh = null;
    public static final C2491fm dlk = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m25657V();
    }

    public ShootingZonePopulationBehaviour() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShootingZonePopulationBehaviour(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25657V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PopulationBehaviour._m_fieldCount + 0;
        _m_methodCount = PopulationBehaviour._m_methodCount + 8;
        _m_fields = new C5663aRz[(PopulationBehaviour._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) PopulationBehaviour._m_fields, (Object[]) _m_fields);
        int i = PopulationBehaviour._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 8)];
        C2491fm a = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "a25325075f4c45a3cd4cefbb071c947e", i);
        f5279Lf = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "0f08a6ddbf993630b989a6ff2fc5f822", i2);
        dlc = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "3be1893fa97c4e7288b5eaa0952c38df", i3);
        dld = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "ea579dffeea0eabe6d5c60af3043bd03", i4);
        dle = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "ab93df1ea3858d870694108a987a63c7", i5);
        dlf = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "70d551e95ab7c1e607d2b30580f4bbf8", i6);
        dlg = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "3c41cff1009eb248299fb4e196845cbe", i7);
        dlh = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(ShootingZonePopulationBehaviour.class, "ecac43989cd986efa85195911eb00133", i8);
        dlk = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PopulationBehaviour._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShootingZonePopulationBehaviour.class, C1812aN.class, _m_fields, _m_methods);
    }

    /* renamed from: D */
    public void mo2941D(Character acx) {
        switch (bFf().mo6893i(dld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                break;
        }
        m25654C(acx);
    }

    /* renamed from: F */
    public void mo2942F(Character acx) {
        switch (bFf().mo6893i(dle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                break;
        }
        m25655E(acx);
    }

    /* renamed from: H */
    public void mo2943H(Character acx) {
        switch (bFf().mo6893i(dlf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                break;
        }
        m25656G(acx);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1812aN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PopulationBehaviour._m_methodCount) {
            case 0:
                m25658a((NPC) args[0]);
                return null;
            case 1:
                m25660e((NPC) args[0]);
                return null;
            case 2:
                m25654C((Character) args[0]);
                return null;
            case 3:
                m25655E((Character) args[0]);
                return null;
            case 4:
                m25656G((Character) args[0]);
                return null;
            case 5:
                aZn();
                return null;
            case 6:
                aZp();
                return null;
            case 7:
                m25659a((Character) args[0], (Character) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void aZo() {
        switch (bFf().mo6893i(dlg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                break;
        }
        aZn();
    }

    public void aZq() {
        switch (bFf().mo6893i(dlh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                break;
        }
        aZp();
    }

    /* renamed from: b */
    public void mo6580b(NPC auf) {
        switch (bFf().mo6893i(f5279Lf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5279Lf, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5279Lf, new Object[]{auf}));
                break;
        }
        m25658a(auf);
    }

    /* renamed from: b */
    public void mo2952b(Character acx, Character acx2) {
        switch (bFf().mo6893i(dlk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                break;
        }
        m25659a(acx, acx2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: f */
    public void mo2953f(NPC auf) {
        switch (bFf().mo6893i(dlc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                break;
        }
        m25660e(auf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a25325075f4c45a3cd4cefbb071c947e", aum = 0)
    /* renamed from: a */
    private void m25658a(NPC auf) {
    }

    @C0064Am(aul = "0f08a6ddbf993630b989a6ff2fc5f822", aum = 0)
    /* renamed from: e */
    private void m25660e(NPC auf) {
    }

    @C0064Am(aul = "3be1893fa97c4e7288b5eaa0952c38df", aum = 0)
    /* renamed from: C */
    private void m25654C(Character acx) {
    }

    @C0064Am(aul = "ea579dffeea0eabe6d5c60af3043bd03", aum = 0)
    /* renamed from: E */
    private void m25655E(Character acx) {
    }

    @C0064Am(aul = "ab93df1ea3858d870694108a987a63c7", aum = 0)
    /* renamed from: G */
    private void m25656G(Character acx) {
    }

    @C0064Am(aul = "70d551e95ab7c1e607d2b30580f4bbf8", aum = 0)
    private void aZn() {
    }

    @C0064Am(aul = "3c41cff1009eb248299fb4e196845cbe", aum = 0)
    private void aZp() {
    }

    @C0064Am(aul = "ecac43989cd986efa85195911eb00133", aum = 0)
    /* renamed from: a */
    private void m25659a(Character acx, Character acx2) {
    }
}
