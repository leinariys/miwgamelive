package game.script.spacezone.population;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.network.message.externalizable.C0352Eg;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.faction.Faction;
import game.script.login.UserConnection;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.player.Player;
import game.script.player.User;
import game.script.ship.Ship;
import game.script.spacezone.NPCZone;
import game.script.spacezone.PopulationControl;
import game.script.spacezone.SpaceZoneEvent;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.bbb.C2820ka;
import logic.data.mbean.C6061afh;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.aod  reason: case insensitive filesystem */
/* compiled from: a */
public class BossNestPopulationControl extends PopulationControl implements C1616Xf, C3763uj {

    /* renamed from: Lg */
    public static final C2491fm f5066Lg = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cnY = null;
    public static final C5663aRz dkP = null;
    public static final C2491fm dla = null;
    public static final C2491fm dlc = null;
    public static final C2491fm dld = null;
    public static final C2491fm dle = null;
    public static final C2491fm dlf = null;
    public static final C2491fm dlg = null;
    public static final C2491fm dlh = null;
    public static final C2491fm dlk = null;
    public static final C2491fm esf = null;
    public static final C2491fm ggA = null;
    public static final C2491fm ggB = null;
    public static final C2491fm ggC = null;
    public static final C2491fm ggD = null;
    public static final C2491fm ggE = null;
    public static final C2491fm ggF = null;
    public static final C2491fm ggG = null;
    public static final C2491fm ggH = null;
    public static final C2491fm ggI = null;
    public static final C2491fm ggJ = null;
    public static final C2491fm ggK = null;
    public static final C2491fm ggL = null;
    public static final C2491fm ggM = null;
    public static final C2491fm ggN = null;
    public static final C2491fm ggO = null;
    public static final C2491fm ggP = null;
    public static final C2491fm ggQ = null;
    public static final C5663aRz ggb = null;
    public static final C5663aRz ggc = null;
    public static final C5663aRz ggd = null;
    public static final C5663aRz gge = null;
    public static final C5663aRz ggf = null;
    public static final C5663aRz ggg = null;
    public static final C5663aRz ggh = null;
    public static final C5663aRz ggi = null;
    public static final C5663aRz ggj = null;
    public static final C5663aRz ggk = null;
    public static final C5663aRz ggl = null;
    public static final C5663aRz ggm = null;
    public static final C5663aRz ggn = null;
    public static final C2491fm ggo = null;
    public static final C2491fm ggp = null;
    public static final C2491fm ggq = null;
    public static final C2491fm ggr = null;
    public static final C2491fm ggs = null;
    public static final C2491fm ggt = null;
    public static final C2491fm ggu = null;
    public static final C2491fm ggv = null;
    public static final C2491fm ggw = null;
    public static final C2491fm ggx = null;
    public static final C2491fm ggy = null;
    public static final C2491fm ggz = null;
    public static final long serialVersionUID = 0;
    private static final long gfX = 60000;
    private static final long gfY = 1000;
    private static final String gfZ = "bossTimeHandle";
    private static final String gga = "bossMessageTimeHandle";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "82c50538e431e97b97d74825b67a5fb3", aum = 1)
    private static C3438ra<NPC> axY = null;
    @C0064Am(aul = "189eb82481b5c7c08e44c8f3830412c7", aum = 14)
    private static int dkO = 0;
    @C0064Am(aul = "b893a1e1741f076efadec107c6995b77", aum = 0)
    @C5454aJy("Creep Spawn Table")
    private static C3438ra<NPCSpawn> ftm = null;
    @C0064Am(aul = "17001b4dc7a60a92d7a4d9b92df13a68", aum = 2)
    private static int ftn = 0;
    @C0064Am(aul = "6aa92e3f570631aea8cc416e510b007d", aum = 3)
    private static int fto = 0;
    @C0064Am(aul = "1c6c040c2cce425d2b2e1530f6eb3929", aum = 4)
    private static int ftp = 0;
    @C0064Am(aul = "323c43624ca604d09c43ff80c4f14037", aum = 5)
    @C5454aJy("Boss Spawn Table")
    private static C3438ra<StaticNPCSpawn> ftq = null;
    @C0064Am(aul = "76aa76e087cb6c60099ab8c89edfe91d", aum = 6)
    private static C3438ra<NPC> ftr = null;
    @C0064Am(aul = "d8797396c02c57546ca7a784bca38f2b", aum = 7)
    private static long fts = 0;
    @C0064Am(aul = "00b3faa810c49a355bec883e89e72640", aum = 8)
    private static long ftt = 0;
    @C0064Am(aul = "681aaf83bbf604264ef82aabb1f7fb34", aum = 9)
    private static I18NString ftu = null;
    @C0064Am(aul = "e5474363c4e6142c6dd85f3886e61cec", aum = 10)
    private static I18NString ftv = null;
    @C0064Am(aul = "62cd1e169d45b2a6971d81d19f886014", aum = 11)
    private static boolean ftw = false;
    @C0064Am(aul = "acafca7c86bc99c785b953068577cab9", aum = 12)
    private static long ftx = 0;
    @C0064Am(aul = "9ff1ac21bf6cf8b803683eb22ba3b923", aum = 13)
    private static long fty = 0;

    static {
        m24581V();
    }

    public BossNestPopulationControl() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BossNestPopulationControl(C5540aNg ang) {
        super(ang);
    }

    public BossNestPopulationControl(BossNestPopulationControlType aqx) {
        super((C5540aNg) null);
        super._m_script_init(aqx);
    }

    /* renamed from: V */
    static void m24581V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PopulationControl._m_fieldCount + 15;
        _m_methodCount = PopulationControl._m_methodCount + 41;
        int i = PopulationControl._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 15)];
        C5663aRz b = C5640aRc.m17844b(BossNestPopulationControl.class, "b893a1e1741f076efadec107c6995b77", i);
        ggb = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BossNestPopulationControl.class, "82c50538e431e97b97d74825b67a5fb3", i2);
        cnY = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BossNestPopulationControl.class, "17001b4dc7a60a92d7a4d9b92df13a68", i3);
        ggc = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(BossNestPopulationControl.class, "6aa92e3f570631aea8cc416e510b007d", i4);
        ggd = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(BossNestPopulationControl.class, "1c6c040c2cce425d2b2e1530f6eb3929", i5);
        gge = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(BossNestPopulationControl.class, "323c43624ca604d09c43ff80c4f14037", i6);
        ggf = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(BossNestPopulationControl.class, "76aa76e087cb6c60099ab8c89edfe91d", i7);
        ggg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(BossNestPopulationControl.class, "d8797396c02c57546ca7a784bca38f2b", i8);
        ggh = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(BossNestPopulationControl.class, "00b3faa810c49a355bec883e89e72640", i9);
        ggi = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(BossNestPopulationControl.class, "681aaf83bbf604264ef82aabb1f7fb34", i10);
        ggj = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(BossNestPopulationControl.class, "e5474363c4e6142c6dd85f3886e61cec", i11);
        ggk = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(BossNestPopulationControl.class, "62cd1e169d45b2a6971d81d19f886014", i12);
        ggl = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(BossNestPopulationControl.class, "acafca7c86bc99c785b953068577cab9", i13);
        ggm = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(BossNestPopulationControl.class, "9ff1ac21bf6cf8b803683eb22ba3b923", i14);
        ggn = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(BossNestPopulationControl.class, "189eb82481b5c7c08e44c8f3830412c7", i15);
        dkP = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) PopulationControl._m_fields, (Object[]) _m_fields);
        int i17 = PopulationControl._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i17 + 41)];
        C2491fm a = C4105zY.m41624a(BossNestPopulationControl.class, "e709e7703945e73d2c18c27eaa4f23a8", i17);
        ggo = a;
        fmVarArr[i17] = a;
        int i18 = i17 + 1;
        C2491fm a2 = C4105zY.m41624a(BossNestPopulationControl.class, "8e720e50f6e813da7c66dc3507b2012d", i18);
        ggp = a2;
        fmVarArr[i18] = a2;
        int i19 = i18 + 1;
        C2491fm a3 = C4105zY.m41624a(BossNestPopulationControl.class, "cd3119d6346bc336756fc8f48da9820f", i19);
        ggq = a3;
        fmVarArr[i19] = a3;
        int i20 = i19 + 1;
        C2491fm a4 = C4105zY.m41624a(BossNestPopulationControl.class, "ef31136434073c09b9473ca763bac7f8", i20);
        ggr = a4;
        fmVarArr[i20] = a4;
        int i21 = i20 + 1;
        C2491fm a5 = C4105zY.m41624a(BossNestPopulationControl.class, "86e687f9b35053b86bdf76fbd9bd4c6e", i21);
        ggs = a5;
        fmVarArr[i21] = a5;
        int i22 = i21 + 1;
        C2491fm a6 = C4105zY.m41624a(BossNestPopulationControl.class, "2b42c96ac93ef00148d92e64e53db8b4", i22);
        ggt = a6;
        fmVarArr[i22] = a6;
        int i23 = i22 + 1;
        C2491fm a7 = C4105zY.m41624a(BossNestPopulationControl.class, "cfbd58f3cdf7adab077f0af6d101a39f", i23);
        ggu = a7;
        fmVarArr[i23] = a7;
        int i24 = i23 + 1;
        C2491fm a8 = C4105zY.m41624a(BossNestPopulationControl.class, "69ba6d12c11a036f5751d83829304368", i24);
        ggv = a8;
        fmVarArr[i24] = a8;
        int i25 = i24 + 1;
        C2491fm a9 = C4105zY.m41624a(BossNestPopulationControl.class, "3102ae1bc01f91d4c3551d309ba2724f", i25);
        ggw = a9;
        fmVarArr[i25] = a9;
        int i26 = i25 + 1;
        C2491fm a10 = C4105zY.m41624a(BossNestPopulationControl.class, "97cb875646a41ffd86fe7b8150393d44", i26);
        ggx = a10;
        fmVarArr[i26] = a10;
        int i27 = i26 + 1;
        C2491fm a11 = C4105zY.m41624a(BossNestPopulationControl.class, "2b8f8b2aeed6f22ad040915f8e31614b", i27);
        ggy = a11;
        fmVarArr[i27] = a11;
        int i28 = i27 + 1;
        C2491fm a12 = C4105zY.m41624a(BossNestPopulationControl.class, "0d10dad2eb282e29548dd91636f6148f", i28);
        ggz = a12;
        fmVarArr[i28] = a12;
        int i29 = i28 + 1;
        C2491fm a13 = C4105zY.m41624a(BossNestPopulationControl.class, "9fffbfaba4e6c1b6584e8875bfb3186a", i29);
        ggA = a13;
        fmVarArr[i29] = a13;
        int i30 = i29 + 1;
        C2491fm a14 = C4105zY.m41624a(BossNestPopulationControl.class, "a8ece08d6251cadf99558f8f7a03ca30", i30);
        ggB = a14;
        fmVarArr[i30] = a14;
        int i31 = i30 + 1;
        C2491fm a15 = C4105zY.m41624a(BossNestPopulationControl.class, "fb019228689e108ee54b8dc50f744c1d", i31);
        ggC = a15;
        fmVarArr[i31] = a15;
        int i32 = i31 + 1;
        C2491fm a16 = C4105zY.m41624a(BossNestPopulationControl.class, "ec454fee8819f16cc5821ed513ab5e73", i32);
        ggD = a16;
        fmVarArr[i32] = a16;
        int i33 = i32 + 1;
        C2491fm a17 = C4105zY.m41624a(BossNestPopulationControl.class, "ed2d3356b84bc44c7419bf181044a287", i33);
        f5066Lg = a17;
        fmVarArr[i33] = a17;
        int i34 = i33 + 1;
        C2491fm a18 = C4105zY.m41624a(BossNestPopulationControl.class, "64ae0f63a1c84768d31643afa8d88505", i34);
        dld = a18;
        fmVarArr[i34] = a18;
        int i35 = i34 + 1;
        C2491fm a19 = C4105zY.m41624a(BossNestPopulationControl.class, "068f87f3e79ae31a562130d8e3aa627e", i35);
        ggE = a19;
        fmVarArr[i35] = a19;
        int i36 = i35 + 1;
        C2491fm a20 = C4105zY.m41624a(BossNestPopulationControl.class, "5e549a2b51adfc0e773d6a7921b2179c", i36);
        dle = a20;
        fmVarArr[i36] = a20;
        int i37 = i36 + 1;
        C2491fm a21 = C4105zY.m41624a(BossNestPopulationControl.class, "bcda436ddd2d65409e14310e0b2b0388", i37);
        dlf = a21;
        fmVarArr[i37] = a21;
        int i38 = i37 + 1;
        C2491fm a22 = C4105zY.m41624a(BossNestPopulationControl.class, "54a2cc5de0d7408c2044a78af5f58e56", i38);
        dlc = a22;
        fmVarArr[i38] = a22;
        int i39 = i38 + 1;
        C2491fm a23 = C4105zY.m41624a(BossNestPopulationControl.class, "c3cb57b43a110acc9a3cf9b2f6d5483b", i39);
        ggF = a23;
        fmVarArr[i39] = a23;
        int i40 = i39 + 1;
        C2491fm a24 = C4105zY.m41624a(BossNestPopulationControl.class, "617d385b86e7cbd10f4123d05fa2b44a", i40);
        dlg = a24;
        fmVarArr[i40] = a24;
        int i41 = i40 + 1;
        C2491fm a25 = C4105zY.m41624a(BossNestPopulationControl.class, "04372b8f63d4b8dec8aa312675298332", i41);
        dlh = a25;
        fmVarArr[i41] = a25;
        int i42 = i41 + 1;
        C2491fm a26 = C4105zY.m41624a(BossNestPopulationControl.class, "e884814a59ebd5b785de6de1131697a9", i42);
        ggG = a26;
        fmVarArr[i42] = a26;
        int i43 = i42 + 1;
        C2491fm a27 = C4105zY.m41624a(BossNestPopulationControl.class, "ce5419bcc02a456441a9be87941745b6", i43);
        ggH = a27;
        fmVarArr[i43] = a27;
        int i44 = i43 + 1;
        C2491fm a28 = C4105zY.m41624a(BossNestPopulationControl.class, "5d3cb0797bb3ae1b48f6faeff2cef0be", i44);
        ggI = a28;
        fmVarArr[i44] = a28;
        int i45 = i44 + 1;
        C2491fm a29 = C4105zY.m41624a(BossNestPopulationControl.class, "f6e87cf40948590170241b35e6d3d94a", i45);
        ggJ = a29;
        fmVarArr[i45] = a29;
        int i46 = i45 + 1;
        C2491fm a30 = C4105zY.m41624a(BossNestPopulationControl.class, "e31e5f798a463e5790d367e521f1f58f", i46);
        ggK = a30;
        fmVarArr[i46] = a30;
        int i47 = i46 + 1;
        C2491fm a31 = C4105zY.m41624a(BossNestPopulationControl.class, "012a4286ca1a6c74e3676ddaf07239a7", i47);
        ggL = a31;
        fmVarArr[i47] = a31;
        int i48 = i47 + 1;
        C2491fm a32 = C4105zY.m41624a(BossNestPopulationControl.class, "53817d51ccaa6987a2df107d7351193d", i48);
        ggM = a32;
        fmVarArr[i48] = a32;
        int i49 = i48 + 1;
        C2491fm a33 = C4105zY.m41624a(BossNestPopulationControl.class, "422101a4fb1699e904472e3afb85aa69", i49);
        ggN = a33;
        fmVarArr[i49] = a33;
        int i50 = i49 + 1;
        C2491fm a34 = C4105zY.m41624a(BossNestPopulationControl.class, "b085b74cd19e1483108e8aac05bd8ca3", i50);
        ggO = a34;
        fmVarArr[i50] = a34;
        int i51 = i50 + 1;
        C2491fm a35 = C4105zY.m41624a(BossNestPopulationControl.class, "9c98d3ef5e18d609815f1341012e5dc0", i51);
        ggP = a35;
        fmVarArr[i51] = a35;
        int i52 = i51 + 1;
        C2491fm a36 = C4105zY.m41624a(BossNestPopulationControl.class, "c0ee3990d00c323403db6806cadbdaeb", i52);
        _f_onResurrect_0020_0028_0029V = a36;
        fmVarArr[i52] = a36;
        int i53 = i52 + 1;
        C2491fm a37 = C4105zY.m41624a(BossNestPopulationControl.class, "dffefeeb1f1d0d9e4ec6f05122a8697a", i53);
        ggQ = a37;
        fmVarArr[i53] = a37;
        int i54 = i53 + 1;
        C2491fm a38 = C4105zY.m41624a(BossNestPopulationControl.class, "954c54b921b890962d4d51421bb8c220", i54);
        dlk = a38;
        fmVarArr[i54] = a38;
        int i55 = i54 + 1;
        C2491fm a39 = C4105zY.m41624a(BossNestPopulationControl.class, "74c8fddbb6a1707c6d6506c1da52d822", i55);
        dla = a39;
        fmVarArr[i55] = a39;
        int i56 = i55 + 1;
        C2491fm a40 = C4105zY.m41624a(BossNestPopulationControl.class, "260b0053fef163f7d2690e1c89d20c5f", i56);
        esf = a40;
        fmVarArr[i56] = a40;
        int i57 = i56 + 1;
        C2491fm a41 = C4105zY.m41624a(BossNestPopulationControl.class, "1f6a6270ad3f80c147ee0e558fbe82da", i57);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a41;
        fmVarArr[i57] = a41;
        int i58 = i57 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PopulationControl._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BossNestPopulationControl.class, C6061afh.class, _m_fields, _m_methods);
    }

    /* renamed from: Y */
    private void m24583Y(Ship fAVar) {
        switch (bFf().mo6893i(ggE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggE, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggE, new Object[]{fAVar}));
                break;
        }
        m24582X(fAVar);
    }

    private int aYY() {
        return bFf().mo5608dq().mo3212n(dkP);
    }

    /* renamed from: ae */
    private void m24586ae(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnY, raVar);
    }

    private C3438ra azu() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnY);
    }

    /* renamed from: bZ */
    private void m24587bZ(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: ca */
    private void m24588ca(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: cb */
    private void m24589cb(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(ggg, raVar);
    }

    /* renamed from: cd */
    private int m24591cd(C3438ra<NPC> raVar) {
        switch (bFf().mo6893i(ggG)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggG, new Object[]{raVar}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggG, new Object[]{raVar}));
                break;
        }
        return m24590cc(raVar);
    }

    private void cmL() {
        switch (bFf().mo6893i(ggF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggF, new Object[0]));
                break;
        }
        cmK();
    }

    private int cmN() {
        switch (bFf().mo6893i(ggH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggH, new Object[0]));
                break;
        }
        return cmM();
    }

    private int cmP() {
        switch (bFf().mo6893i(ggI)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggI, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggI, new Object[0]));
                break;
        }
        return cmO();
    }

    private void cmR() {
        switch (bFf().mo6893i(ggL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggL, new Object[0]));
                break;
        }
        cmQ();
    }

    private int cmT() {
        switch (bFf().mo6893i(ggM)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggM, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggM, new Object[0]));
                break;
        }
        return cmS();
    }

    private void cmV() {
        switch (bFf().mo6893i(ggN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggN, new Object[0]));
                break;
        }
        cmU();
    }

    private void cmX() {
        switch (bFf().mo6893i(ggO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggO, new Object[0]));
                break;
        }
        cmW();
    }

    private C3438ra cmh() {
        return ((BossNestPopulationControlType) getType()).dqT();
    }

    private int cmi() {
        return bFf().mo5608dq().mo3212n(ggc);
    }

    private int cmj() {
        return bFf().mo5608dq().mo3212n(ggd);
    }

    private int cmk() {
        return bFf().mo5608dq().mo3212n(gge);
    }

    private C3438ra cml() {
        return ((BossNestPopulationControlType) getType()).dqX();
    }

    private C3438ra cmm() {
        return (C3438ra) bFf().mo5608dq().mo3214p(ggg);
    }

    private long cmn() {
        return bFf().mo5608dq().mo3213o(ggh);
    }

    private long cmo() {
        return bFf().mo5608dq().mo3213o(ggi);
    }

    private I18NString cmp() {
        return (I18NString) bFf().mo5608dq().mo3214p(ggj);
    }

    private I18NString cmq() {
        return (I18NString) bFf().mo5608dq().mo3214p(ggk);
    }

    private boolean cmr() {
        return bFf().mo5608dq().mo3201h(ggl);
    }

    private long cms() {
        return bFf().mo5608dq().mo3213o(ggm);
    }

    private long cmt() {
        return bFf().mo5608dq().mo3213o(ggn);
    }

    /* renamed from: fk */
    private void m24593fk(boolean z) {
        bFf().mo5608dq().mo3153a(ggl, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show Boss Spawn Message")
    @C0064Am(aul = "a8ece08d6251cadf99558f8f7a03ca30", aum = 0)
    @C5566aOg
    /* renamed from: fl */
    private void m24594fl(boolean z) {
        throw new aWi(new aCE(this, ggB, new Object[]{new Boolean(z)}));
    }

    /* renamed from: hZ */
    private void m24596hZ(long j) {
        bFf().mo5608dq().mo3184b(ggh, j);
    }

    /* renamed from: ia */
    private void m24597ia(long j) {
        bFf().mo5608dq().mo3184b(ggi, j);
    }

    /* renamed from: ib */
    private void m24598ib(long j) {
        bFf().mo5608dq().mo3184b(ggm, j);
    }

    /* renamed from: ic */
    private void m24599ic(long j) {
        bFf().mo5608dq().mo3184b(ggn, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Boss reposition period (minutes)")
    @C0064Am(aul = "69ba6d12c11a036f5751d83829304368", aum = 0)
    @C5566aOg
    /* renamed from: id */
    private void m24600id(long j) {
        throw new aWi(new aCE(this, ggv, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message repetition period (seconds)")
    @C0064Am(aul = "ec454fee8819f16cc5821ed513ab5e73", aum = 0)
    @C5566aOg
    /* renamed from: if */
    private void m24601if(long j) {
        throw new aWi(new aCE(this, ggD, new Object[]{new Long(j)}));
    }

    /* renamed from: li */
    private void m24602li(int i) {
        bFf().mo5608dq().mo3183b(dkP, i);
    }

    /* renamed from: mN */
    private void m24603mN(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ggj, i18NString);
    }

    /* renamed from: mO */
    private void m24604mO(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ggk, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Boss Spawn Message")
    @C0064Am(aul = "97cb875646a41ffd86fe7b8150393d44", aum = 0)
    @C5566aOg
    /* renamed from: mP */
    private void m24605mP(I18NString i18NString) {
        throw new aWi(new aCE(this, ggx, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "All Bosses Killed Message")
    @C0064Am(aul = "2b8f8b2aeed6f22ad040915f8e31614b", aum = 0)
    @C5566aOg
    /* renamed from: mR */
    private void m24606mR(I18NString i18NString) {
        throw new aWi(new aCE(this, ggy, new Object[]{i18NString}));
    }

    /* renamed from: mU */
    private void m24608mU(I18NString i18NString) {
        switch (bFf().mo6893i(ggK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggK, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggK, new Object[]{i18NString}));
                break;
        }
        m24607mT(i18NString);
    }

    @C0064Am(aul = "1f6a6270ad3f80c147ee0e558fbe82da", aum = 0)
    /* renamed from: qW */
    private Object m24610qW() {
        return cmZ();
    }

    /* renamed from: sY */
    private void m24611sY(int i) {
        bFf().mo5608dq().mo3183b(ggc, i);
    }

    /* renamed from: sZ */
    private void m24612sZ(int i) {
        bFf().mo5608dq().mo3183b(ggd, i);
    }

    /* renamed from: ta */
    private void m24613ta(int i) {
        bFf().mo5608dq().mo3183b(gge, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Creeps Population")
    @C0064Am(aul = "8e720e50f6e813da7c66dc3507b2012d", aum = 0)
    @C5566aOg
    /* renamed from: tb */
    private void m24614tb(int i) {
        throw new aWi(new aCE(this, ggp, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Creeps Population")
    @C0064Am(aul = "ef31136434073c09b9473ca763bac7f8", aum = 0)
    @C5566aOg
    /* renamed from: td */
    private void m24615td(int i) {
        throw new aWi(new aCE(this, ggr, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creeps Spawned per Invader")
    @C0064Am(aul = "2b42c96ac93ef00148d92e64e53db8b4", aum = 0)
    @C5566aOg
    /* renamed from: tf */
    private void m24616tf(int i) {
        throw new aWi(new aCE(this, ggt, new Object[]{new Integer(i)}));
    }

    /* renamed from: v */
    private int m24618v(NPCType aed) {
        switch (bFf().mo6893i(ggJ)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggJ, new Object[]{aed}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggJ, new Object[]{aed}));
                break;
        }
        return m24617u(aed);
    }

    /* renamed from: x */
    private NPC m24620x(NPCType aed) {
        switch (bFf().mo6893i(ggP)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, ggP, new Object[]{aed}));
            case 3:
                bFf().mo5606d(new aCE(this, ggP, new Object[]{aed}));
                break;
        }
        return m24619w(aed);
    }

    /* renamed from: D */
    public void mo2941D(Character acx) {
        switch (bFf().mo6893i(dld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                break;
        }
        m24578C(acx);
    }

    /* renamed from: F */
    public void mo2942F(Character acx) {
        switch (bFf().mo6893i(dle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                break;
        }
        m24579E(acx);
    }

    /* renamed from: H */
    public void mo2943H(Character acx) {
        switch (bFf().mo6893i(dlf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                break;
        }
        m24580G(acx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6061afh(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PopulationControl._m_methodCount) {
            case 0:
                return new Integer(cmu());
            case 1:
                m24614tb(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Integer(cmw());
            case 3:
                m24615td(((Integer) args[0]).intValue());
                return null;
            case 4:
                return new Integer(cmy());
            case 5:
                m24616tf(((Integer) args[0]).intValue());
                return null;
            case 6:
                return new Long(cmA());
            case 7:
                m24600id(((Long) args[0]).longValue());
                return null;
            case 8:
                return cmC();
            case 9:
                m24605mP((I18NString) args[0]);
                return null;
            case 10:
                m24606mR((I18NString) args[0]);
                return null;
            case 11:
                return cmE();
            case 12:
                return new Boolean(cmG());
            case 13:
                m24594fl(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                return new Long(cmI());
            case 15:
                m24601if(((Long) args[0]).longValue());
                return null;
            case 16:
                return m24609qQ();
            case 17:
                m24578C((Character) args[0]);
                return null;
            case 18:
                m24582X((Ship) args[0]);
                return null;
            case 19:
                m24579E((Character) args[0]);
                return null;
            case 20:
                m24580G((Character) args[0]);
                return null;
            case 21:
                m24592e((NPC) args[0]);
                return null;
            case 22:
                cmK();
                return null;
            case 23:
                aZn();
                return null;
            case 24:
                aZp();
                return null;
            case 25:
                return new Integer(m24590cc((C3438ra) args[0]));
            case 26:
                return new Integer(cmM());
            case 27:
                return new Integer(cmO());
            case 28:
                return new Integer(m24617u((NPCType) args[0]));
            case 29:
                m24607mT((I18NString) args[0]);
                return null;
            case 30:
                cmQ();
                return null;
            case 31:
                return new Integer(cmS());
            case 32:
                cmU();
                return null;
            case 33:
                cmW();
                return null;
            case 34:
                return m24619w((NPCType) args[0]);
            case 35:
                m24585aG();
                return null;
            case 36:
                return cmY();
            case 37:
                m24584a((Character) args[0], (Character) args[1]);
                return null;
            case 38:
                aZk();
                return null;
            case 39:
                m24595gQ((String) args[0]);
                return null;
            case 40:
                return m24610qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m24585aG();
    }

    /* access modifiers changed from: protected */
    public void aZl() {
        switch (bFf().mo6893i(dla)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                break;
        }
        aZk();
    }

    public void aZo() {
        switch (bFf().mo6893i(dlg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                break;
        }
        aZn();
    }

    public void aZq() {
        switch (bFf().mo6893i(dlh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                break;
        }
        aZp();
    }

    /* renamed from: b */
    public void mo2952b(Character acx, Character acx2) {
        switch (bFf().mo6893i(dlk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                break;
        }
        m24584a(acx, acx2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cj */
    public void mo14332cj(String str) {
        switch (bFf().mo6893i(esf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esf, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esf, new Object[]{str}));
                break;
        }
        m24595gQ(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Boss reposition period (minutes)")
    public long cmB() {
        switch (bFf().mo6893i(ggu)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ggu, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggu, new Object[0]));
                break;
        }
        return cmA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Boss Spawn Message")
    public I18NString cmD() {
        switch (bFf().mo6893i(ggw)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ggw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ggw, new Object[0]));
                break;
        }
        return cmC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "All Bosses Killed Message")
    public I18NString cmF() {
        switch (bFf().mo6893i(ggz)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ggz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ggz, new Object[0]));
                break;
        }
        return cmE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show Boss Spawn Message")
    public boolean cmH() {
        switch (bFf().mo6893i(ggA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ggA, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggA, new Object[0]));
                break;
        }
        return cmG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message repetition period (seconds)")
    public long cmJ() {
        switch (bFf().mo6893i(ggC)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ggC, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggC, new Object[0]));
                break;
        }
        return cmI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    public BossNestPopulationControlType cmZ() {
        switch (bFf().mo6893i(ggQ)) {
            case 0:
                return null;
            case 2:
                return (BossNestPopulationControlType) bFf().mo5606d(new aCE(this, ggQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ggQ, new Object[0]));
                break;
        }
        return cmY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Creeps Population")
    public int cmv() {
        switch (bFf().mo6893i(ggo)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggo, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggo, new Object[0]));
                break;
        }
        return cmu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Creeps Population")
    public int cmx() {
        switch (bFf().mo6893i(ggq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggq, new Object[0]));
                break;
        }
        return cmw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creeps Spawned per Invader")
    public int cmz() {
        switch (bFf().mo6893i(ggs)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ggs, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ggs, new Object[0]));
                break;
        }
        return cmy();
    }

    /* renamed from: f */
    public void mo2953f(NPC auf) {
        switch (bFf().mo6893i(dlc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                break;
        }
        m24592e(auf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Show Boss Spawn Message")
    @C5566aOg
    /* renamed from: fm */
    public void mo15286fm(boolean z) {
        switch (bFf().mo6893i(ggB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggB, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggB, new Object[]{new Boolean(z)}));
                break;
        }
        m24594fl(z);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m24610qW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Boss reposition period (minutes)")
    @C5566aOg
    /* renamed from: ie */
    public void mo15287ie(long j) {
        switch (bFf().mo6893i(ggv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggv, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggv, new Object[]{new Long(j)}));
                break;
        }
        m24600id(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message repetition period (seconds)")
    @C5566aOg
    /* renamed from: ig */
    public void mo15288ig(long j) {
        switch (bFf().mo6893i(ggD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggD, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggD, new Object[]{new Long(j)}));
                break;
        }
        m24601if(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Boss Spawn Message")
    @C5566aOg
    /* renamed from: mQ */
    public void mo15289mQ(I18NString i18NString) {
        switch (bFf().mo6893i(ggx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggx, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggx, new Object[]{i18NString}));
                break;
        }
        m24605mP(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "All Bosses Killed Message")
    @C5566aOg
    /* renamed from: mS */
    public void mo15290mS(I18NString i18NString) {
        switch (bFf().mo6893i(ggy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggy, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggy, new Object[]{i18NString}));
                break;
        }
        m24606mR(i18NString);
    }

    /* renamed from: qR */
    public List<NPC> mo2957qR() {
        switch (bFf().mo6893i(f5066Lg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f5066Lg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5066Lg, new Object[0]));
                break;
        }
        return m24609qQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Creeps Population")
    @C5566aOg
    /* renamed from: tc */
    public void mo15291tc(int i) {
        switch (bFf().mo6893i(ggp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggp, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggp, new Object[]{new Integer(i)}));
                break;
        }
        m24614tb(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Creeps Population")
    @C5566aOg
    /* renamed from: te */
    public void mo15292te(int i) {
        switch (bFf().mo6893i(ggr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggr, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggr, new Object[]{new Integer(i)}));
                break;
        }
        m24615td(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creeps Spawned per Invader")
    @C5566aOg
    /* renamed from: tg */
    public void mo15293tg(int i) {
        switch (bFf().mo6893i(ggt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ggt, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ggt, new Object[]{new Integer(i)}));
                break;
        }
        m24616tf(i);
    }

    /* renamed from: a */
    public void mo15276a(BossNestPopulationControlType aqx) {
        super.mo14333h(aqx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Creeps Population")
    @C0064Am(aul = "e709e7703945e73d2c18c27eaa4f23a8", aum = 0)
    private int cmu() {
        return cmj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Creeps Population")
    @C0064Am(aul = "cd3119d6346bc336756fc8f48da9820f", aum = 0)
    private int cmw() {
        return cmi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creeps Spawned per Invader")
    @C0064Am(aul = "86e687f9b35053b86bdf76fbd9bd4c6e", aum = 0)
    private int cmy() {
        return cmk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Boss reposition period (minutes)")
    @C0064Am(aul = "cfbd58f3cdf7adab077f0af6d101a39f", aum = 0)
    private long cmA() {
        return cmn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Boss Spawn Message")
    @C0064Am(aul = "3102ae1bc01f91d4c3551d309ba2724f", aum = 0)
    private I18NString cmC() {
        return cmp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "All Bosses Killed Message")
    @C0064Am(aul = "0d10dad2eb282e29548dd91636f6148f", aum = 0)
    private I18NString cmE() {
        return cmq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Show Boss Spawn Message")
    @C0064Am(aul = "9fffbfaba4e6c1b6584e8875bfb3186a", aum = 0)
    private boolean cmG() {
        return cmr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message repetition period (seconds)")
    @C0064Am(aul = "fb019228689e108ee54b8dc50f744c1d", aum = 0)
    private long cmI() {
        return cms();
    }

    @C0064Am(aul = "ed2d3356b84bc44c7419bf181044a287", aum = 0)
    /* renamed from: qQ */
    private List<NPC> m24609qQ() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(cmm());
        arrayList.addAll(azu());
        return arrayList;
    }

    @C0064Am(aul = "64ae0f63a1c84768d31643afa8d88505", aum = 0)
    /* renamed from: C */
    private void m24578C(Character acx) {
        m24602li(aYY() - 1);
    }

    @C0064Am(aul = "068f87f3e79ae31a562130d8e3aa627e", aum = 0)
    /* renamed from: X */
    private void m24582X(Ship fAVar) {
        if (fAVar != null) {
            for (NPC bQx : cmm()) {
                Ship bQx2 = bQx.bQx();
                if (bQx2 != null) {
                    bQx2.cyP().mo17926i(fAVar);
                }
            }
        }
    }

    @C0064Am(aul = "5e549a2b51adfc0e773d6a7921b2179c", aum = 0)
    /* renamed from: E */
    private void m24579E(Character acx) {
        Faction azN;
        m24602li(aYY() + 1);
        cmV();
        m24583Y(acx.bQx());
        if ((acx instanceof Player) && (azN = ((StaticNPCSpawn) cml().get(0)).abs().azN()) != null) {
            ((Player) acx).dyl().mo11981e((C4068yr) azN);
        }
    }

    @C0064Am(aul = "bcda436ddd2d65409e14310e0b2b0388", aum = 0)
    /* renamed from: G */
    private void m24580G(Character acx) {
        m24602li(aYY() - 1);
    }

    @C0064Am(aul = "54a2cc5de0d7408c2044a78af5f58e56", aum = 0)
    /* renamed from: e */
    private void m24592e(NPC auf) {
        boolean z = true;
        boolean contains = cmm().contains(auf);
        if (!cmm().contains(auf) || cmm().size() != 1) {
            z = false;
        }
        if (contains) {
            cmm().remove(auf);
        } else {
            azu().remove(auf);
        }
        auf.dispose();
        if (z && cmr()) {
            mo8669Uy().mo20319aK(gga);
            m24608mU(cmq());
        }
        if (cmP() > 0 && !contains) {
            mo14334hO(cVr() + ((long) (chu() * 1000.0f)));
        }
    }

    @C0064Am(aul = "c3cb57b43a110acc9a3cf9b2f6d5483b", aum = 0)
    private void cmK() {
        int i;
        if (cmh().size() != 0) {
            int i2 = 0;
            Iterator it = cmh().iterator();
            while (true) {
                i = i2;
                if (!it.hasNext()) {
                    break;
                }
                i2 = ((NPCSpawn) it.next()).dfh() + i;
            }
            if (i != 100) {
                throw new RuntimeException("Creeps Spawn table in zone " + mo8669Uy().getName() + " doesn't add up to 100.");
            }
        }
    }

    @C0064Am(aul = "617d385b86e7cbd10f4123d05fa2b44a", aum = 0)
    private void aZn() {
        super.aZo();
        if (cmi() > cmj()) {
            //Мин. И макс. Численность крипов в зоне "+ mo8669Uy (). GetName () +" недопустимы.
            throw new RuntimeException("Min and max creeps population in zone " + mo8669Uy().getName() + " are invalid.");
        }
        cmL();
        Iterator it = cmm().iterator();
        while (it.hasNext()) {
            if (!((NPC) it.next()).bQx().bae()) {
                it.remove();
            }
        }
        Iterator it2 = azu().iterator();
        while (it2.hasNext()) {
            if (!((NPC) it2.next()).bQx().bae()) {
                it2.remove();
            }
        }
        m24597ia(cVr());
        m24599ic(cVr());
        NPCZone Uy = mo8669Uy();
        SpaceZoneEvent afb = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
        afb.mo8792a(this, cmo(), gfZ);
        Uy.mo20322b(afb);
    }

    @C0064Am(aul = "04372b8f63d4b8dec8aa312675298332", aum = 0)
    private void aZp() {
        super.aZq();
        mo8669Uy().mo20319aK(gfZ);
        mo8669Uy().mo20319aK(gga);
        if (cmm().size() > 0 && cmr()) {
            m24608mU(cmq());
        }
        for (NPC dispose : mo2957qR()) {
            dispose.dispose();
        }
        cmm().clear();
        azu().clear();
        m24602li(0);
    }

    @C0064Am(aul = "e884814a59ebd5b785de6de1131697a9", aum = 0)
    /* renamed from: cc */
    private int m24590cc(C3438ra<NPC> raVar) {
        int i = 0;
        for (NPC bQx : raVar) {
            Ship bQx2 = bQx.bQx();
            if (bQx2 != null && bQx2.bae()) {
                i++;
            }
        }
        return i;
    }

    @C0064Am(aul = "ce5419bcc02a456441a9be87941745b6", aum = 0)
    private int cmM() {
        return m24591cd(azu());
    }

    @C0064Am(aul = "5d3cb0797bb3ae1b48f6faeff2cef0be", aum = 0)
    private int cmO() {
        return m24591cd(cmm());
    }

    @C0064Am(aul = "f6e87cf40948590170241b35e6d3d94a", aum = 0)
    /* renamed from: u */
    private int m24617u(NPCType aed) {
        Iterator it = cmm().iterator();
        while (it.hasNext()) {
            NPC auf = (NPC) it.next();
            if (auf == null) {
                it.remove();
            } else if (auf.bQx() == null) {
                mo6317hy("NPC " + auf.bFY() + " is being kept in bosses list without active ship." + "Is it disposed? " + auf.isDisposed() + ".");
                it.remove();
            }
        }
        int i = 0;
        for (NPC auf2 : cmm()) {
            if (auf2.mo11654Fs() == aed && auf2.bQx().bae()) {
                i++;
            }
        }
        return i;
    }

    @C0064Am(aul = "e31e5f798a463e5790d367e521f1f58f", aum = 0)
    /* renamed from: mT */
    private void m24607mT(I18NString i18NString) {
        Player dL;
        for (User cSV : ala().aJQ()) {
            UserConnection cSV2 = cSV.cSV();
            if (!(cSV2 == null || (dL = cSV2.mo15388dL()) == null)) {
                dL.mo14419f((C1506WA) new C0352Eg(dL, i18NString, new Object[0]));
            }
        }
    }

    @C0064Am(aul = "012a4286ca1a6c74e3676ddaf07239a7", aum = 0)
    private void cmQ() {
        for (StaticNPCSpawn bg : cml()) {
            int Iq = bg.mo668Iq() - m24618v(bg.abs());
            for (int i = 0; i < Iq; i++) {
                NPCType abs = bg.abs();
                NPC x = m24620x(abs);
                if (x != null) {
                    cmm().add(x);
                } else {
                    mo8358lY("Unable to find an uncolliding place to spawn the NPC of type '" + abs.mo11470ke() + "' in the NPC Zone '" + mo8669Uy().getName() + "'");
                }
            }
        }
    }

    @C0064Am(aul = "53817d51ccaa6987a2df107d7351193d", aum = 0)
    private int cmS() {
        int aYY = aYY() * cmk();
        if (cmj() >= 0 && aYY > cmj()) {
            aYY = cmj();
        }
        if (cmi() < 0 || aYY >= cmi()) {
            return aYY;
        }
        return cmi();
    }

    @C0064Am(aul = "422101a4fb1699e904472e3afb85aa69", aum = 0)
    private void cmU() {
        if (cmh().size() != 0 && cmP() != 0) {
            int cmT = cmT() - azu().size();
            while (true) {
                cmT--;
                if (cmT >= 0) {
                    cmX();
                } else {
                    return;
                }
            }
        }
    }

    @C0064Am(aul = "b085b74cd19e1483108e8aac05bd8ca3", aum = 0)
    private void cmW() {
        NPCType aed;
        if (cmh().size() != 0) {
            cmL();
            float nextFloat = mo14328LE().nextFloat();
            List<C2782jv> a = C2945lv.m35367a(azu(), cmh(), cmT());
            if (a != null) {
                Iterator<C2782jv> it = a.iterator();
                while (true) {
                    float f = nextFloat;
                    if (!it.hasNext()) {
                        aed = null;
                        break;
                    }
                    C2782jv next = it.next();
                    if (f < next.mo19998Ft()) {
                        aed = next.mo19997Fs();
                        break;
                    }
                    nextFloat = f - next.mo19998Ft();
                }
                if (aed == null) {
                    mo8358lY("Couldnt find a suitable type.. bail");
                    return;
                }
                NPC x = m24620x(aed);
                if (x != null) {
                    azu().add(x);
                } else {
                    mo8358lY("Unable to find an uncolliding place to spawn the NPC of type '" + aed.mo11470ke() + "' in the NPC Zone '" + mo8669Uy().getName() + "'");
                }
            }
        }
    }

    @C0064Am(aul = "9c98d3ef5e18d609815f1341012e5dc0", aum = 0)
    /* renamed from: w */
    private NPC m24619w(NPCType aed) {
        new C2820ka(aed.bTC().mo19050uX().bkx().getFile());
        Vec3f chy = chy();
        if (chy == null) {
            return null;
        }
        NPC bUa = aed.bUa();
        Quat4fWrap chw = chw();
        bUa.bQx().setPosition(mo8669Uy().getPosition().mo9531q(chy));
        bUa.bQx().setOrientation(chw);
        bUa.bQx().mo993b(mo8669Uy().mo960Nc());
        mo8669Uy().mo18865b(bUa);
        return bUa;
    }

    @C0064Am(aul = "c0ee3990d00c323403db6806cadbdaeb", aum = 0)
    /* renamed from: aG */
    private void m24585aG() {
        super.mo70aH();
        if (mo8669Uy() != null && mo8669Uy().isActive()) {
            m24597ia(cVr());
            NPCZone Uy = mo8669Uy();
            SpaceZoneEvent afb = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
            afb.mo8792a(this, cmo(), gfZ);
            Uy.mo20322b(afb);
            if (cmr()) {
                m24599ic(cVr());
                NPCZone Uy2 = mo8669Uy();
                SpaceZoneEvent afb2 = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
                afb2.mo8792a(this, cmt(), gga);
                Uy2.mo20322b(afb2);
            }
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "dffefeeb1f1d0d9e4ec6f05122a8697a", aum = 0)
    private BossNestPopulationControlType cmY() {
        return (BossNestPopulationControlType) super.getType();
    }

    @C0064Am(aul = "954c54b921b890962d4d51421bb8c220", aum = 0)
    /* renamed from: a */
    private void m24584a(Character acx, Character acx2) {
    }

    @C0064Am(aul = "74c8fddbb6a1707c6d6506c1da52d822", aum = 0)
    private void aZk() {
        if (cmN() < cmT()) {
            cmX();
        }
    }

    @C0064Am(aul = "260b0053fef163f7d2690e1c89d20c5f", aum = 0)
    /* renamed from: gQ */
    private void m24595gQ(String str) {
        if (str.equals(gfZ)) {
            cmR();
            cmV();
            m24597ia(cmo() + (cmn() * gfX));
            NPCZone Uy = mo8669Uy();
            SpaceZoneEvent afb = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
            afb.mo8792a(this, cmo(), gfZ);
            Uy.mo20322b(afb);
            if (cmr()) {
                m24599ic(cVr());
                NPCZone Uy2 = mo8669Uy();
                SpaceZoneEvent afb2 = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
                afb2.mo8792a(this, cmt(), gga);
                Uy2.mo20322b(afb2);
            }
        } else if (str.equals(gga) && cmr() && cmP() > 0) {
            m24608mU(cmp());
            if (cms() > 0) {
                m24599ic(cmt() + (cms() * gfY));
                NPCZone Uy3 = mo8669Uy();
                SpaceZoneEvent afb3 = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
                afb3.mo8792a(this, cmt(), gga);
                Uy3.mo20322b(afb3);
                return;
            }
            m24599ic(Long.MAX_VALUE);
        }
    }
}
