package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.npc.NPC;
import logic.baa.*;
import logic.data.mbean.C1542Wf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.oO */
/* compiled from: a */
public abstract class PopulationBehaviour extends TaikodomObject implements C0468GU, C1616Xf, C5313aEn {

    /* renamed from: Lf */
    public static final C2491fm f8760Lf = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aQr = null;
    public static final C2491fm aQs = null;
    public static final C2491fm aQt = null;
    /* renamed from: bL */
    public static final C5663aRz f8763bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8764bM = null;
    /* renamed from: bN */
    public static final C2491fm f8765bN = null;
    /* renamed from: bO */
    public static final C2491fm f8766bO = null;
    /* renamed from: bP */
    public static final C2491fm f8767bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8768bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e559b560c22c174b165f1998ff542400", aum = 0)

    /* renamed from: Xx */
    private static NPCZone f8761Xx;
    @C0064Am(aul = "c17500aba195feb31a585acb2a38f1a1", aum = 1)

    /* renamed from: bK */
    private static UUID f8762bK;
    @C0064Am(aul = "589209dc843247fc30bc79163d22f4e1", aum = 2)
    private static String handle;

    static {
        m36684V();
    }

    public PopulationBehaviour() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PopulationBehaviour(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36684V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 7;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(PopulationBehaviour.class, "e559b560c22c174b165f1998ff542400", i);
        aQr = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PopulationBehaviour.class, "c17500aba195feb31a585acb2a38f1a1", i2);
        f8763bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PopulationBehaviour.class, "589209dc843247fc30bc79163d22f4e1", i3);
        f8764bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(PopulationBehaviour.class, "130d2ba72b3ef4ea6778adf2812fee5d", i5);
        f8765bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(PopulationBehaviour.class, "fb7d312653b50d1d71530f72168d7314", i6);
        f8766bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(PopulationBehaviour.class, "417109e31c47a96e7c5ec0d6a384579f", i7);
        f8767bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(PopulationBehaviour.class, "697d00d093debb32f16576cf4bcddc70", i8);
        f8768bQ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(PopulationBehaviour.class, "05abe97033d8417278f0cb6339e906ac", i9);
        f8760Lf = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(PopulationBehaviour.class, "ea32984a16a6c40a9d67efbeef2b117a", i10);
        aQs = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(PopulationBehaviour.class, "bbc0b8911327dc55f4a8e32572050924", i11);
        aQt = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PopulationBehaviour.class, C1542Wf.class, _m_fields, _m_methods);
    }

    /* renamed from: Uw */
    private NPCZone m36682Uw() {
        return (NPCZone) bFf().mo5608dq().mo3214p(aQr);
    }

    @C0064Am(aul = "05abe97033d8417278f0cb6339e906ac", aum = 0)
    /* renamed from: a */
    private void m36685a(NPC auf) {
        throw new aWi(new aCE(this, f8760Lf, new Object[]{auf}));
    }

    /* renamed from: a */
    private void m36686a(NPCZone fwVar) {
        bFf().mo5608dq().mo3197f(aQr, fwVar);
    }

    /* renamed from: a */
    private void m36687a(String str) {
        bFf().mo5608dq().mo3197f(f8764bM, str);
    }

    /* renamed from: a */
    private void m36688a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8763bL, uuid);
    }

    /* renamed from: an */
    private UUID m36689an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8763bL);
    }

    /* renamed from: ao */
    private String m36690ao() {
        return (String) bFf().mo5608dq().mo3214p(f8764bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "697d00d093debb32f16576cf4bcddc70", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m36694b(String str) {
        throw new aWi(new aCE(this, f8768bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m36696c(UUID uuid) {
        switch (bFf().mo6893i(f8766bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8766bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8766bO, new Object[]{uuid}));
                break;
        }
        m36695b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: Uy */
    public NPCZone mo8669Uy() {
        switch (bFf().mo6893i(aQt)) {
            case 0:
                return null;
            case 2:
                return (NPCZone) bFf().mo5606d(new aCE(this, aQt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQt, new Object[0]));
                break;
        }
        return m36683Ux();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1542Wf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m36691ap();
            case 1:
                m36695b((UUID) args[0]);
                return null;
            case 2:
                return m36692ar();
            case 3:
                m36694b((String) args[0]);
                return null;
            case 4:
                m36685a((NPC) args[0]);
                return null;
            case 5:
                m36693b((NPCZone) args[0]);
                return null;
            case 6:
                return m36683Ux();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8765bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8765bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8765bN, new Object[0]));
                break;
        }
        return m36691ap();
    }

    /* renamed from: b */
    public void mo6580b(NPC auf) {
        switch (bFf().mo6893i(f8760Lf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8760Lf, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8760Lf, new Object[]{auf}));
                break;
        }
        m36685a(auf);
    }

    /* renamed from: c */
    public void mo8670c(NPCZone fwVar) {
        switch (bFf().mo6893i(aQs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aQs, new Object[]{fwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aQs, new Object[]{fwVar}));
                break;
        }
        m36693b(fwVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8767bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8767bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8767bP, new Object[0]));
                break;
        }
        return m36692ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8768bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8768bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8768bQ, new Object[]{str}));
                break;
        }
        m36694b(str);
    }

    @C0064Am(aul = "130d2ba72b3ef4ea6778adf2812fee5d", aum = 0)
    /* renamed from: ap */
    private UUID m36691ap() {
        return m36689an();
    }

    @C0064Am(aul = "fb7d312653b50d1d71530f72168d7314", aum = 0)
    /* renamed from: b */
    private void m36695b(UUID uuid) {
        m36688a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m36688a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "417109e31c47a96e7c5ec0d6a384579f", aum = 0)
    /* renamed from: ar */
    private String m36692ar() {
        return m36690ao();
    }

    @C0064Am(aul = "ea32984a16a6c40a9d67efbeef2b117a", aum = 0)
    /* renamed from: b */
    private void m36693b(NPCZone fwVar) {
        m36686a(fwVar);
    }

    @C0064Am(aul = "bbc0b8911327dc55f4a8e32572050924", aum = 0)
    /* renamed from: Ux */
    private NPCZone m36683Ux() {
        return m36682Uw();
    }
}
