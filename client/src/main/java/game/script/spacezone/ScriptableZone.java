package game.script.spacezone;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C5311aEl;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.Actor;
import game.script.Character;
import game.script.TaskletImpl;
import game.script.Trigger;
import game.script.ai.npc.ArrivalController;
import game.script.ai.npc.RoundAIController;
import game.script.ai.npc.TrackShipAIController;
import game.script.mission.scripting.I18NStringTable;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.AsteroidType;
import game.script.space.Gate;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.bbb.C2820ka;
import logic.bbb.C6348alI;
import logic.data.link.C0471GX;
import logic.data.link.C3694ts;
import logic.data.link.C3981xi;
import logic.data.mbean.C3425rQ;
import logic.data.mbean.C6019aer;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.Context;
import org.mozilla1.javascript.Function;
import org.mozilla1.javascript.NativeArray;
import org.mozilla1.javascript.Scriptable;
import p001a.*;
import taikodom.game.script.p003ai.npc.DroneAIController;
import taikodom.infra.script.I18NString;

import javax.vecmath.Quat4f;
import javax.vecmath.Tuple3d;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Random;

@C5511aMd
@C6485anp
/* renamed from: a.Zz */
/* compiled from: a */
public class ScriptableZone extends Trigger implements C0471GX.C0472a, C1616Xf, C3694ts.C3695a, C3981xi.C3982a {

    /* renamed from: KF */
    public static final C5663aRz f2258KF = null;

    /* renamed from: Lc */
    public static final C2491fm f2259Lc = null;

    /* renamed from: Lj */
    public static final C2491fm f2260Lj = null;

    /* renamed from: Ll */
    public static final C2491fm f2261Ll = null;

    /* renamed from: Mq */
    public static final C2491fm f2262Mq = null;
    /* renamed from: Wp */
    public static final C2491fm f2264Wp = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aSi = null;
    public static final C2491fm aSj = null;
    public static final C2491fm awC = null;
    public static final C2491fm awD = null;
    public static final C5663aRz awh = null;
    public static final C2491fm awp = null;
    public static final C2491fm awq = null;
    public static final C2491fm awy = null;
    public static final C2491fm awz = null;
    /* renamed from: bM */
    public static final C5663aRz f2265bM = null;
    /* renamed from: bP */
    public static final C2491fm f2266bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2267bQ = null;
    public static final C5663aRz dEK = null;
    public static final C2491fm dES = null;
    public static final C2491fm dET = null;
    public static final C2491fm dFb = null;
    public static final C2491fm dFd = null;
    public static final C2491fm dFe = null;
    public static final C2491fm dFq = null;
    public static final C5663aRz dRg = null;
    public static final C5663aRz dkR = null;
    public static final C5663aRz eOQ = null;
    public static final C5663aRz eOV = null;
    public static final C5663aRz eOW = null;
    public static final C2491fm eOX = null;
    public static final C2491fm eOY = null;
    public static final C2491fm eOZ = null;
    public static final C2491fm ePA = null;
    public static final C2491fm ePB = null;
    public static final C2491fm ePC = null;
    public static final C2491fm ePa = null;
    public static final C2491fm ePb = null;
    public static final C2491fm ePc = null;
    public static final C2491fm ePd = null;
    public static final C2491fm ePe = null;
    public static final C2491fm ePf = null;
    public static final C2491fm ePg = null;
    public static final C2491fm ePh = null;
    public static final C2491fm ePi = null;
    public static final C2491fm ePj = null;
    public static final C2491fm ePk = null;
    public static final C2491fm ePl = null;
    public static final C2491fm ePm = null;
    public static final C2491fm ePn = null;
    public static final C2491fm ePo = null;
    public static final C2491fm ePp = null;
    public static final C2491fm ePq = null;
    public static final C2491fm ePr = null;
    public static final C2491fm ePs = null;
    public static final C2491fm ePt = null;
    public static final C2491fm ePu = null;
    public static final C2491fm ePv = null;
    public static final C2491fm ePw = null;
    public static final C2491fm ePx = null;
    public static final C2491fm ePy = null;
    public static final C2491fm ePz = null;
    /* renamed from: ui */
    public static final C5663aRz f2268ui = null;
    /* renamed from: vs */
    public static final C2491fm f2269vs = null;
    /* renamed from: zu */
    public static final C2491fm f2270zu = null;
    private static final float eOO = 3.0f;
    private static final int eOP = 10;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "3741b1a3dc24da7970cb51e49f1e7550", aum = 6)

    /* renamed from: UA */
    private static String f2263UA = null;
    @C0064Am(aul = "a40741e3d5ba85d9bf2af41c6c230851", aum = 8)
    private static Node aSh = null;
    @C0064Am(aul = "efc707110c91a94e6ee190aa59eb0114", aum = 7)
    private static boolean active = false;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "1a4e7f3b8fcc2b4d0fd1dc07ca5ac0c7", aum = 0)
    @C5566aOg
    private static Heartbeat bdT = null;
    @C0064Am(aul = "5c6f1e3d01ffb763aa8472df3b6ad863", aum = 1)
    @C5566aOg
    private static C1556Wo<String, NPC> bdU = null;
    @C0064Am(aul = "19ee65a2cebca1ab0dfbf738dac909ee", aum = 2)
    @C5566aOg
    private static C1556Wo<Ship, String> bdV = null;
    @C0064Am(aul = "e0683a94a27ec759e00faa28e2f0d2a3", aum = 3)
    @C5566aOg
    private static C1556Wo<String, AsteroidZone> bdW = null;
    @C0064Am(aul = "7d12a54bdc13f19a8024f6a37215dc3d", aum = 9)
    private static I18NStringTable bdX = null;
    @C0064Am(aul = "259019b0ab6e276a530566687f6ce387", aum = 5)
    private static String handle = null;
    @C0064Am(aul = "27bf9e5b6b10b8cb498056cf51326b18", aum = 4)
    private static float radius = 0.0f;

    static {
        m12289V();
    }

    @C5566aOg

    /* renamed from: TR */
    private transient Scriptable f2271TR;
    @C5566aOg
    private transient Random awe;
    @C5566aOg
    private transient ThreadLocal<Context> daB;
    @C5566aOg
    private transient Map<String, Player> eOR;
    @C5566aOg
    private transient Map<NPC, String> eOS;
    @C5566aOg
    private transient Map<String, Object> eOT;
    @C5566aOg
    private transient int eOU;

    public ScriptableZone() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScriptableZone(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12289V() {
        _m_fieldCount = Trigger._m_fieldCount + 10;
        _m_methodCount = Trigger._m_methodCount + 58;
        int i = Trigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(ScriptableZone.class, "1a4e7f3b8fcc2b4d0fd1dc07ca5ac0c7", i);
        eOQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ScriptableZone.class, "5c6f1e3d01ffb763aa8472df3b6ad863", i2);
        dkR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ScriptableZone.class, "19ee65a2cebca1ab0dfbf738dac909ee", i3);
        dEK = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ScriptableZone.class, "e0683a94a27ec759e00faa28e2f0d2a3", i4);
        eOV = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ScriptableZone.class, "27bf9e5b6b10b8cb498056cf51326b18", i5);
        awh = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ScriptableZone.class, "259019b0ab6e276a530566687f6ce387", i6);
        f2265bM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ScriptableZone.class, "3741b1a3dc24da7970cb51e49f1e7550", i7);
        dRg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ScriptableZone.class, "efc707110c91a94e6ee190aa59eb0114", i8);
        f2258KF = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ScriptableZone.class, "a40741e3d5ba85d9bf2af41c6c230851", i9);
        f2268ui = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(ScriptableZone.class, "7d12a54bdc13f19a8024f6a37215dc3d", i10);
        eOW = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Trigger._m_fields, (Object[]) _m_fields);
        int i12 = Trigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 58)];
        C2491fm a = C4105zY.m41624a(ScriptableZone.class, "550ba6c42a6359572d86e1e743a03bde", i12);
        f2266bP = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(ScriptableZone.class, "e670afb56d66162eda35484ef01f22ba", i13);
        f2267bQ = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(ScriptableZone.class, "949ec85372107f7ff0aceb1cf5733c90", i14);
        eOX = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(ScriptableZone.class, "de79d134432147e4912d813bc311f45b", i15);
        eOY = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(ScriptableZone.class, "28f27a54f7b6f9588c4e3b8cd1bdcc6a", i16);
        f2259Lc = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(ScriptableZone.class, "dbde7ca7dd80b546bf2c5f76e3bd7fc1", i17);
        awy = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(ScriptableZone.class, "5a4060ed60ed9be416965c72a84c4014", i18);
        f2262Mq = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(ScriptableZone.class, "aff37266141efb293c034aa14a179dd9", i19);
        f2264Wp = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(ScriptableZone.class, "681b020de767c990bba1f342e2fa448a", i20);
        aSi = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(ScriptableZone.class, "40ab8641d797c324b788f0a6ee6618d6", i21);
        aSj = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(ScriptableZone.class, "9c2d6393180d2e7dd073a2670072be36", i22);
        awp = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(ScriptableZone.class, "0f4de65e03b2d4e7aef48064470f3346", i23);
        awq = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(ScriptableZone.class, "12e0424a13253d31c42f9d6e742b8f9a", i24);
        eOZ = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(ScriptableZone.class, "f4c7412c81a63fd6f2d7ef87ee589ceb", i25);
        ePa = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(ScriptableZone.class, "4488b4113f9919c5f84224db3fbda9c8", i26);
        ePb = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(ScriptableZone.class, "57d1f4cf1765a96802a339b2ad97e9ae", i27);
        ePc = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(ScriptableZone.class, "e5648902d76c823cdc705823151bec64", i28);
        dFd = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(ScriptableZone.class, "d83fd75e860939eaa470b1350c36d419", i29);
        dES = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(ScriptableZone.class, "6372d4fa1c5d6c2327925c941066d628", i30);
        dET = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(ScriptableZone.class, "759cafef803394ffae6c81592d20c383", i31);
        ePd = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(ScriptableZone.class, "2779187d7af9aebaf9888e05cbd2ee00", i32);
        f2269vs = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(ScriptableZone.class, "9bbe77df4dde6c40d44db2ab6ec245b7", i33);
        awC = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(ScriptableZone.class, "70a777089fb270a056d5872212dd909b", i34);
        awD = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        C2491fm a24 = C4105zY.m41624a(ScriptableZone.class, "cfecb87b4f4140e6463aa6fe459aaed0", i35);
        _f_start_0020_0028_0029V = a24;
        fmVarArr[i35] = a24;
        int i36 = i35 + 1;
        C2491fm a25 = C4105zY.m41624a(ScriptableZone.class, "59c40347b7a0fc9aedbc7b7fdebe0ea9", i36);
        _f_stop_0020_0028_0029V = a25;
        fmVarArr[i36] = a25;
        int i37 = i36 + 1;
        C2491fm a26 = C4105zY.m41624a(ScriptableZone.class, "b4743cb0e0a1c2bd9e94ad75f33c11e8", i37);
        f2270zu = a26;
        fmVarArr[i37] = a26;
        int i38 = i37 + 1;
        C2491fm a27 = C4105zY.m41624a(ScriptableZone.class, "4fae9b9c7a881f02261887457f67d6ea", i38);
        ePe = a27;
        fmVarArr[i38] = a27;
        int i39 = i38 + 1;
        C2491fm a28 = C4105zY.m41624a(ScriptableZone.class, "77af63d9c9b5e9d0f05f3416c3284eb1", i39);
        ePf = a28;
        fmVarArr[i39] = a28;
        int i40 = i39 + 1;
        C2491fm a29 = C4105zY.m41624a(ScriptableZone.class, "1144a6eeb69f355f34e62edaf20d2dc1", i40);
        ePg = a29;
        fmVarArr[i40] = a29;
        int i41 = i40 + 1;
        C2491fm a30 = C4105zY.m41624a(ScriptableZone.class, "a74fb9b8fbc7b8c26bd5e2c6217f542d", i41);
        ePh = a30;
        fmVarArr[i41] = a30;
        int i42 = i41 + 1;
        C2491fm a31 = C4105zY.m41624a(ScriptableZone.class, "348119407f739b141846b0c7270f9c00", i42);
        ePi = a31;
        fmVarArr[i42] = a31;
        int i43 = i42 + 1;
        C2491fm a32 = C4105zY.m41624a(ScriptableZone.class, "34a6809849b0ef4c704e47c857721601", i43);
        ePj = a32;
        fmVarArr[i43] = a32;
        int i44 = i43 + 1;
        C2491fm a33 = C4105zY.m41624a(ScriptableZone.class, "b5f34a4480f7d841cdfa5cbd4ee4fb17", i44);
        ePk = a33;
        fmVarArr[i44] = a33;
        int i45 = i44 + 1;
        C2491fm a34 = C4105zY.m41624a(ScriptableZone.class, "733bbeb75735e7cfeb813e7fff2f5614", i45);
        dFb = a34;
        fmVarArr[i45] = a34;
        int i46 = i45 + 1;
        C2491fm a35 = C4105zY.m41624a(ScriptableZone.class, "bb535528c877bc13162acdc673337c01", i46);
        dFq = a35;
        fmVarArr[i46] = a35;
        int i47 = i46 + 1;
        C2491fm a36 = C4105zY.m41624a(ScriptableZone.class, "2b21591b71641ef8a053c109cd1ae9a4", i47);
        dFe = a36;
        fmVarArr[i47] = a36;
        int i48 = i47 + 1;
        C2491fm a37 = C4105zY.m41624a(ScriptableZone.class, "e869e258cc89b21ad8d41378d2bca878", i48);
        ePl = a37;
        fmVarArr[i48] = a37;
        int i49 = i48 + 1;
        C2491fm a38 = C4105zY.m41624a(ScriptableZone.class, "ccb54d3f01dc0f5dd2b1101824c576e4", i49);
        ePm = a38;
        fmVarArr[i49] = a38;
        int i50 = i49 + 1;
        C2491fm a39 = C4105zY.m41624a(ScriptableZone.class, "4fda16663b5539918e9c4d61f4cd2a86", i50);
        ePn = a39;
        fmVarArr[i50] = a39;
        int i51 = i50 + 1;
        C2491fm a40 = C4105zY.m41624a(ScriptableZone.class, "2cd1413a49e1d9ba8302d79ed571ffa2", i51);
        ePo = a40;
        fmVarArr[i51] = a40;
        int i52 = i51 + 1;
        C2491fm a41 = C4105zY.m41624a(ScriptableZone.class, "e3f6b650d261c6b3564f353d533dcd0b", i52);
        ePp = a41;
        fmVarArr[i52] = a41;
        int i53 = i52 + 1;
        C2491fm a42 = C4105zY.m41624a(ScriptableZone.class, "9db58c887a00f6fdb242daad6094e56d", i53);
        ePq = a42;
        fmVarArr[i53] = a42;
        int i54 = i53 + 1;
        C2491fm a43 = C4105zY.m41624a(ScriptableZone.class, "25625eace308c79bb8a76111075eecc4", i54);
        ePr = a43;
        fmVarArr[i54] = a43;
        int i55 = i54 + 1;
        C2491fm a44 = C4105zY.m41624a(ScriptableZone.class, "6aaa8e1f711240b6447451c857cc27e2", i55);
        awz = a44;
        fmVarArr[i55] = a44;
        int i56 = i55 + 1;
        C2491fm a45 = C4105zY.m41624a(ScriptableZone.class, "41d90ced97725601a254e85ea8a0d423", i56);
        ePs = a45;
        fmVarArr[i56] = a45;
        int i57 = i56 + 1;
        C2491fm a46 = C4105zY.m41624a(ScriptableZone.class, "3d8e9ec97d3e65c6ead1b5b84c11932c", i57);
        ePt = a46;
        fmVarArr[i57] = a46;
        int i58 = i57 + 1;
        C2491fm a47 = C4105zY.m41624a(ScriptableZone.class, "c73f97f4fa07a732ce83cd839203a666", i58);
        ePu = a47;
        fmVarArr[i58] = a47;
        int i59 = i58 + 1;
        C2491fm a48 = C4105zY.m41624a(ScriptableZone.class, "c53742bdf41015c2682bbfa05b99d3bf", i59);
        ePv = a48;
        fmVarArr[i59] = a48;
        int i60 = i59 + 1;
        C2491fm a49 = C4105zY.m41624a(ScriptableZone.class, "de215411e0f45317f83963e9a3346523", i60);
        ePw = a49;
        fmVarArr[i60] = a49;
        int i61 = i60 + 1;
        C2491fm a50 = C4105zY.m41624a(ScriptableZone.class, "da82657bec4c857fe77ba916054cb3cd", i61);
        ePx = a50;
        fmVarArr[i61] = a50;
        int i62 = i61 + 1;
        C2491fm a51 = C4105zY.m41624a(ScriptableZone.class, "e21b6021a5c90eb91186bb61fa3b1987", i62);
        ePy = a51;
        fmVarArr[i62] = a51;
        int i63 = i62 + 1;
        C2491fm a52 = C4105zY.m41624a(ScriptableZone.class, "a12d9af8a6546946bf274c8af25ddf39", i63);
        ePz = a52;
        fmVarArr[i63] = a52;
        int i64 = i63 + 1;
        C2491fm a53 = C4105zY.m41624a(ScriptableZone.class, "1e43cca91f4ba1278cc0e8b29b3a7498", i64);
        ePA = a53;
        fmVarArr[i64] = a53;
        int i65 = i64 + 1;
        C2491fm a54 = C4105zY.m41624a(ScriptableZone.class, "b5234eb7d2cf57ebacd8990bc56fb9b9", i65);
        ePB = a54;
        fmVarArr[i65] = a54;
        int i66 = i65 + 1;
        C2491fm a55 = C4105zY.m41624a(ScriptableZone.class, "da8ccca3fafcfba0aa4ab4c57dfb5d3c", i66);
        f2261Ll = a55;
        fmVarArr[i66] = a55;
        int i67 = i66 + 1;
        C2491fm a56 = C4105zY.m41624a(ScriptableZone.class, "debcca3fd4203ad4dbc488fa20713736", i67);
        f2260Lj = a56;
        fmVarArr[i67] = a56;
        int i68 = i67 + 1;
        C2491fm a57 = C4105zY.m41624a(ScriptableZone.class, "89bd863e668bbdce36f3694c0799d0df", i68);
        ePC = a57;
        fmVarArr[i68] = a57;
        int i69 = i68 + 1;
        C2491fm a58 = C4105zY.m41624a(ScriptableZone.class, "b754b72256d5a16a894008d0b6e1c1fd", i69);
        _f_onResurrect_0020_0028_0029V = a58;
        fmVarArr[i69] = a58;
        int i70 = i69 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Trigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScriptableZone.class, C3425rQ.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m12281F(boolean z) {
        bFf().mo5608dq().mo3153a(f2258KF, z);
    }

    /* renamed from: J */
    private void m12282J(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dEK, wo);
    }

    /* renamed from: LE */
    private Random m12284LE() {
        switch (bFf().mo6893i(awz)) {
            case 0:
                return null;
            case 2:
                return (Random) bFf().mo5606d(new aCE(this, awz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, awz, new Object[0]));
                break;
        }
        return m12283LD();
    }

    /* renamed from: Ln */
    private float m12285Ln() {
        return bFf().mo5608dq().mo3211m(awh);
    }

    /* renamed from: T */
    private void m12287T(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dkR, wo);
    }

    /* renamed from: U */
    private void m12288U(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(eOV, wo);
    }

    /* renamed from: Vb */
    private Node m12290Vb() {
        return (Node) bFf().mo5608dq().mo3214p(f2268ui);
    }

    /* renamed from: a */
    private void m12297a(I18NStringTable wz) {
        bFf().mo5608dq().mo3197f(eOW, wz);
    }

    /* renamed from: a */
    private void m12298a(Heartbeat aVar) {
        bFf().mo5608dq().mo3197f(eOQ, aVar);
    }

    @C0064Am(aul = "1144a6eeb69f355f34e62edaf20d2dc1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m12299a(NPC auf, String str) {
        throw new aWi(new aCE(this, ePg, new Object[]{auf, str}));
    }

    /* renamed from: a */
    private void m12302a(Node rPVar) {
        bFf().mo5608dq().mo3197f(f2268ui, rPVar);
    }

    /* renamed from: a */
    private void m12303a(String str) {
        bFf().mo5608dq().mo3197f(f2265bM, str);
    }

    /* renamed from: aH */
    private Vec3d m12309aH(Actor cr) {
        switch (bFf().mo6893i(ePl)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, ePl, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, ePl, new Object[]{cr}));
                break;
        }
        return m12307aG(cr);
    }

    /* renamed from: aJ */
    private Quat4fWrap m12311aJ(Actor cr) {
        switch (bFf().mo6893i(ePm)) {
            case 0:
                return null;
            case 2:
                return (Quat4fWrap) bFf().mo5606d(new aCE(this, ePm, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, ePm, new Object[]{cr}));
                break;
        }
        return m12310aI(cr);
    }

    /* access modifiers changed from: private */
    public Context aRv() {
        switch (bFf().mo6893i(dES)) {
            case 0:
                return null;
            case 2:
                return (Context) bFf().mo5606d(new aCE(this, dES, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dES, new Object[0]));
                break;
        }
        return bjv();
    }

    /* renamed from: aU */
    private void m12312aU(float f) {
        bFf().mo5608dq().mo3150a(awh, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Radius")
    @C0064Am(aul = "0f4de65e03b2d4e7aef48064470f3346", aum = 0)
    @C5566aOg
    /* renamed from: aV */
    private void m12313aV(float f) {
        throw new aWi(new aCE(this, awq, new Object[]{new Float(f)}));
    }

    /* renamed from: ao */
    private String m12318ao() {
        return (String) bFf().mo5608dq().mo3214p(f2265bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Active")
    @C0064Am(aul = "dbde7ca7dd80b546bf2c5f76e3bd7fc1", aum = 0)
    @C5566aOg
    /* renamed from: au */
    private void m12320au(boolean z) {
        throw new aWi(new aCE(this, awy, new Object[]{new Boolean(z)}));
    }

    /* renamed from: b */
    private Vec3d m12321b(C3279pu puVar, NPCType aed) {
        switch (bFf().mo6893i(ePs)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, ePs, new Object[]{puVar, aed}));
            case 3:
                bFf().mo5606d(new aCE(this, ePs, new Object[]{puVar, aed}));
                break;
        }
        return m12293a(puVar, aed);
    }

    /* renamed from: b */
    private String m12322b(NPCType aed, Vec3d ajr, Quat4fWrap aoy) {
        switch (bFf().mo6893i(ePo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ePo, new Object[]{aed, ajr, aoy}));
            case 3:
                bFf().mo5606d(new aCE(this, ePo, new Object[]{aed, ajr, aoy}));
                break;
        }
        return m12294a(aed, ajr, aoy);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "String Table")
    @C0064Am(aul = "f4c7412c81a63fd6f2d7ef87ee589ceb", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m12323b(I18NStringTable wz) {
        throw new aWi(new aCE(this, ePa, new Object[]{wz}));
    }

    @C0064Am(aul = "89bd863e668bbdce36f3694c0799d0df", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m12324b(AsteroidZone btVar, AsteroidType aqn, Actor cr) {
        throw new aWi(new aCE(this, ePC, new Object[]{btVar, aqn, cr}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Node")
    @C0064Am(aul = "40ab8641d797c324b788f0a6ee6618d6", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m12325b(Node rPVar) {
        throw new aWi(new aCE(this, aSj, new Object[]{rPVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "e670afb56d66162eda35484ef01f22ba", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m12326b(String str) {
        throw new aWi(new aCE(this, f2267bQ, new Object[]{str}));
    }

    private Heartbeat bIN() {
        return (Heartbeat) bFf().mo5608dq().mo3214p(eOQ);
    }

    private C1556Wo bIO() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dkR);
    }

    private C1556Wo bIP() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(eOV);
    }

    private I18NStringTable bIQ() {
        return (I18NStringTable) bFf().mo5608dq().mo3214p(eOW);
    }

    /* access modifiers changed from: private */
    public void bIW() {
        switch (bFf().mo6893i(ePd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePd, new Object[0]));
                break;
        }
        bIV();
    }

    private String bIY() {
        switch (bFf().mo6893i(ePn)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ePn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ePn, new Object[0]));
                break;
        }
        return bIX();
    }

    @C0064Am(aul = "e21b6021a5c90eb91186bb61fa3b1987", aum = 0)
    @C5566aOg
    private float bIZ() {
        throw new aWi(new aCE(this, ePy, new Object[0]));
    }

    private C1556Wo bjr() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dEK);
    }

    private void bjx() {
        switch (bFf().mo6893i(dET)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dET, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dET, new Object[0]));
                break;
        }
        bjw();
    }

    private String bnz() {
        return (String) bFf().mo5608dq().mo3214p(dRg);
    }

    @C0064Am(aul = "4fae9b9c7a881f02261887457f67d6ea", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: br */
    private void m12327br(Player aku) {
        throw new aWi(new aCE(this, ePe, new Object[]{aku}));
    }

    private void cleanup() {
        switch (bFf().mo6893i(f2270zu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2270zu, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2270zu, new Object[0]));
                break;
        }
        m12348jV();
    }

    /* renamed from: fb */
    private Actor m12334fb(String str) {
        switch (bFf().mo6893i(dFe)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, dFe, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFe, new Object[]{str}));
                break;
        }
        return m12333fa(str);
    }

    /* renamed from: fl */
    private NPCType m12336fl(String str) {
        switch (bFf().mo6893i(dFq)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, dFq, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFq, new Object[]{str}));
                break;
        }
        return m12335fk(str);
    }

    /* renamed from: gk */
    private void m12337gk(String str) {
        bFf().mo5608dq().mo3197f(dRg, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Script")
    @C0064Am(aul = "de79d134432147e4912d813bc311f45b", aum = 0)
    @C5566aOg
    /* renamed from: hL */
    private void m12338hL(String str) {
        throw new aWi(new aCE(this, eOY, new Object[]{str}));
    }

    /* renamed from: hO */
    private void m12340hO(String str) {
        switch (bFf().mo6893i(ePb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePb, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePb, new Object[]{str}));
                break;
        }
        m12339hN(str);
    }

    /* renamed from: hQ */
    private void m12342hQ(String str) {
        switch (bFf().mo6893i(ePc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePc, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePc, new Object[]{str}));
                break;
        }
        m12341hP(str);
    }

    @C0064Am(aul = "77af63d9c9b5e9d0f05f3416c3284eb1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: hR */
    private Player m12343hR(String str) {
        throw new aWi(new aCE(this, ePf, new Object[]{str}));
    }

    @C0064Am(aul = "34a6809849b0ef4c704e47c857721601", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: hT */
    private Object m12344hT(String str) {
        throw new aWi(new aCE(this, ePj, new Object[]{str}));
    }

    @C0064Am(aul = "a74fb9b8fbc7b8c26bd5e2c6217f542d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: j */
    private String m12347j(NPC auf) {
        throw new aWi(new aCE(this, ePh, new Object[]{auf}));
    }

    @C0064Am(aul = "348119407f739b141846b0c7270f9c00", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: l */
    private String m12349l(NPC auf) {
        throw new aWi(new aCE(this, ePi, new Object[]{auf}));
    }

    /* renamed from: qv */
    private boolean m12353qv() {
        return bFf().mo5608dq().mo3201h(f2258KF);
    }

    @C0064Am(aul = "b5f34a4480f7d841cdfa5cbd4ee4fb17", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: t */
    private void m12354t(String str, Object obj) {
        throw new aWi(new aCE(this, ePk, new Object[]{str, obj}));
    }

    /* renamed from: B */
    public void mo7552B(Actor cr) {
        switch (bFf().mo6893i(awD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                break;
        }
        m12280A(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Radius")
    /* renamed from: Lu */
    public float mo7553Lu() {
        switch (bFf().mo6893i(awp)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, awp, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, awp, new Object[0]));
                break;
        }
        return m12286Lt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Node")
    /* renamed from: Ve */
    public Node mo7554Ve() {
        switch (bFf().mo6893i(aSi)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, aSi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aSi, new Object[0]));
                break;
        }
        return m12291Vd();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3425rQ(this);
    }

    /* renamed from: Z */
    public String mo7555Z(String str, String str2) {
        switch (bFf().mo6893i(ePp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ePp, new Object[]{str, str2}));
            case 3:
                bFf().mo5606d(new aCE(this, ePp, new Object[]{str, str2}));
                break;
        }
        return m12292Y(str, str2);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Trigger._m_methodCount) {
            case 0:
                return m12319ar();
            case 1:
                m12326b((String) args[0]);
                return null;
            case 2:
                return bIR();
            case 3:
                m12338hL((String) args[0]);
                return null;
            case 4:
                return new Boolean(m12352qP());
            case 5:
                m12320au(((Boolean) args[0]).booleanValue());
                return null;
            case 6:
                m12296a((StellarSystem) args[0]);
                return null;
            case 7:
                m12356zw();
                return null;
            case 8:
                return m12291Vd();
            case 9:
                m12325b((Node) args[0]);
                return null;
            case 10:
                return new Float(m12286Lt());
            case 11:
                m12313aV(((Float) args[0]).floatValue());
                return null;
            case 12:
                return bIT();
            case 13:
                m12323b((I18NStringTable) args[0]);
                return null;
            case 14:
                m12339hN((String) args[0]);
                return null;
            case 15:
                m12341hP((String) args[0]);
                return null;
            case 16:
                m12331eY((String) args[0]);
                return null;
            case 17:
                return bjv();
            case 18:
                bjw();
                return null;
            case 19:
                bIV();
                return null;
            case 20:
                m12295a((Actor.C0200h) args[0]);
                return null;
            case 21:
                m12355y((Actor) args[0]);
                return null;
            case 22:
                m12280A((Actor) args[0]);
                return null;
            case 23:
                m12350qN();
                return null;
            case 24:
                m12351qO();
                return null;
            case 25:
                m12348jV();
                return null;
            case 26:
                m12327br((Player) args[0]);
                return null;
            case 27:
                return m12343hR((String) args[0]);
            case 28:
                m12299a((NPC) args[0], (String) args[1]);
                return null;
            case 29:
                return m12347j((NPC) args[0]);
            case 30:
                return m12349l((NPC) args[0]);
            case 31:
                return m12344hT((String) args[0]);
            case 32:
                m12354t((String) args[0], args[1]);
                return null;
            case 33:
                return m12329e((String) args[0], (Object[]) args[1]);
            case 34:
                return m12335fk((String) args[0]);
            case 35:
                return m12333fa((String) args[0]);
            case 36:
                return m12307aG((Actor) args[0]);
            case 37:
                return m12310aI((Actor) args[0]);
            case 38:
                return bIX();
            case 39:
                return m12294a((NPCType) args[0], (Vec3d) args[1], (Quat4fWrap) args[2]);
            case 40:
                return m12292Y((String) args[0], (String) args[1]);
            case 41:
                m12328c((String) args[0], (NativeArray) args[1]);
                return null;
            case 42:
                m12345hV((String) args[0]);
                return null;
            case 43:
                return m12283LD();
            case 44:
                return m12293a((C3279pu) args[0], (NPCType) args[1]);
            case 45:
                return m12314aa((String) args[0], (String) args[1]);
            case 46:
                m12315ac((String) args[0], (String) args[1]);
                return null;
            case 47:
                m12304a((String) args[0], ((Double) args[1]).doubleValue(), ((Double) args[2]).doubleValue(), ((Double) args[3]).doubleValue(), ((Long) args[4]).longValue());
                return null;
            case 48:
                m12316ae((String) args[0], (String) args[1]);
                return null;
            case 49:
                return new Float(m12317ag((String) args[0], (String) args[1]));
            case 50:
                return new Float(bIZ());
            case 51:
                m12346hX((String) args[0]);
                return null;
            case 52:
                m12305a((String) args[0], (String) args[1], (String) args[2], ((Float) args[3]).floatValue());
                return null;
            case 53:
                m12306a((String) args[0], (String) args[1], (String) args[2], ((Float) args[3]).floatValue(), (Asset) args[4]);
                return null;
            case 54:
                m12301a((Ship) args[0], (Actor) args[1]);
                return null;
            case 55:
                m12300a((Pawn) args[0], (Actor) args[1]);
                return null;
            case 56:
                m12324b((AsteroidZone) args[0], (AsteroidType) args[1], (Actor) args[2]);
                return null;
            case 57:
                m12308aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    @C5566aOg
    @C2499fr
    /* renamed from: a */
    public void mo7556a(AsteroidZone btVar, AsteroidType aqn, Actor cr) {
        switch (bFf().mo6893i(ePC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePC, new Object[]{btVar, aqn, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePC, new Object[]{btVar, aqn, cr}));
                break;
        }
        m12324b(btVar, aqn, cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m12308aG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Radius")
    @C5566aOg
    /* renamed from: aW */
    public void mo7557aW(float f) {
        switch (bFf().mo6893i(awq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awq, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awq, new Object[]{new Float(f)}));
                break;
        }
        m12313aV(f);
    }

    /* renamed from: ab */
    public String mo7558ab(String str, String str2) {
        switch (bFf().mo6893i(ePt)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ePt, new Object[]{str, str2}));
            case 3:
                bFf().mo5606d(new aCE(this, ePt, new Object[]{str, str2}));
                break;
        }
        return m12314aa(str, str2);
    }

    /* renamed from: ad */
    public void mo7559ad(String str, String str2) {
        switch (bFf().mo6893i(ePu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePu, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePu, new Object[]{str, str2}));
                break;
        }
        m12315ac(str, str2);
    }

    /* renamed from: af */
    public void mo7560af(String str, String str2) {
        switch (bFf().mo6893i(ePw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePw, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePw, new Object[]{str, str2}));
                break;
        }
        m12316ae(str, str2);
    }

    /* renamed from: ah */
    public float mo7561ah(String str, String str2) {
        switch (bFf().mo6893i(ePx)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ePx, new Object[]{str, str2}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ePx, new Object[]{str, str2}));
                break;
        }
        return m12317ag(str, str2);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f2269vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2269vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2269vs, new Object[]{hVar}));
                break;
        }
        m12295a(hVar);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f2262Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2262Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2262Mq, new Object[]{jj}));
                break;
        }
        m12296a(jj);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo7562b(NPC auf, String str) {
        switch (bFf().mo6893i(ePg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePg, new Object[]{auf, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePg, new Object[]{auf, str}));
                break;
        }
        m12299a(auf, str);
    }

    /* renamed from: b */
    public void mo2353b(Pawn avi, Actor cr) {
        switch (bFf().mo6893i(f2260Lj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2260Lj, new Object[]{avi, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2260Lj, new Object[]{avi, cr}));
                break;
        }
        m12300a(avi, cr);
    }

    /* renamed from: b */
    public void mo7563b(Ship fAVar, Actor cr) {
        switch (bFf().mo6893i(f2261Ll)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2261Ll, new Object[]{fAVar, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2261Ll, new Object[]{fAVar, cr}));
                break;
        }
        m12301a(fAVar, cr);
    }

    /* renamed from: b */
    public void mo7564b(String str, double d, double d2, double d3, long j) {
        switch (bFf().mo6893i(ePv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePv, new Object[]{str, new Double(d), new Double(d2), new Double(d3), new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePv, new Object[]{str, new Double(d), new Double(d2), new Double(d3), new Long(j)}));
                break;
        }
        m12304a(str, d, d2, d3, j);
    }

    /* renamed from: b */
    public void mo7565b(String str, String str2, String str3, float f) {
        switch (bFf().mo6893i(ePA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePA, new Object[]{str, str2, str3, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePA, new Object[]{str, str2, str3, new Float(f)}));
                break;
        }
        m12305a(str, str2, str3, f);
    }

    /* renamed from: b */
    public void mo7566b(String str, String str2, String str3, float f, Asset tCVar) {
        switch (bFf().mo6893i(ePB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePB, new Object[]{str, str2, str3, new Float(f), tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePB, new Object[]{str, str2, str3, new Float(f), tCVar}));
                break;
        }
        m12306a(str, str2, str3, f, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Script")
    public String bIS() {
        switch (bFf().mo6893i(eOX)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, eOX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eOX, new Object[0]));
                break;
        }
        return bIR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "String Table")
    public I18NStringTable bIU() {
        switch (bFf().mo6893i(eOZ)) {
            case 0:
                return null;
            case 2:
                return (I18NStringTable) bFf().mo5606d(new aCE(this, eOZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eOZ, new Object[0]));
                break;
        }
        return bIT();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: bs */
    public void mo7569bs(Player aku) {
        switch (bFf().mo6893i(ePe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePe, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePe, new Object[]{aku}));
                break;
        }
        m12327br(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "String Table")
    @C5566aOg
    /* renamed from: c */
    public void mo7570c(I18NStringTable wz) {
        switch (bFf().mo6893i(ePa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePa, new Object[]{wz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePa, new Object[]{wz}));
                break;
        }
        m12323b(wz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Node")
    @C5566aOg
    /* renamed from: c */
    public void mo7571c(Node rPVar) {
        switch (bFf().mo6893i(aSj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                break;
        }
        m12325b(rPVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo7572d(String str, NativeArray ayt) {
        switch (bFf().mo6893i(ePq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePq, new Object[]{str, ayt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePq, new Object[]{str, ayt}));
                break;
        }
        m12328c(str, ayt);
    }

    /* renamed from: eZ */
    public void mo7573eZ(String str) {
        switch (bFf().mo6893i(dFd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFd, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFd, new Object[]{str}));
                break;
        }
        m12331eY(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public Object mo7574f(String str, Object... objArr) {
        switch (bFf().mo6893i(dFb)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, dFb, new Object[]{str, objArr}));
            case 3:
                bFf().mo5606d(new aCE(this, dFb, new Object[]{str, objArr}));
                break;
        }
        return m12329e(str, objArr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2266bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2266bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2266bP, new Object[0]));
                break;
        }
        return m12319ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2267bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2267bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2267bQ, new Object[]{str}));
                break;
        }
        m12326b(str);
    }

    @C5566aOg
    public float getTime() {
        switch (bFf().mo6893i(ePy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ePy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ePy, new Object[0]));
                break;
        }
        return bIZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Script")
    @C5566aOg
    /* renamed from: hM */
    public void mo7577hM(String str) {
        switch (bFf().mo6893i(eOY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eOY, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eOY, new Object[]{str}));
                break;
        }
        m12338hL(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: hS */
    public Player mo7578hS(String str) {
        switch (bFf().mo6893i(ePf)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, ePf, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, ePf, new Object[]{str}));
                break;
        }
        return m12343hR(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: hU */
    public Object mo7579hU(String str) {
        switch (bFf().mo6893i(ePj)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, ePj, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, ePj, new Object[]{str}));
                break;
        }
        return m12344hT(str);
    }

    /* renamed from: hW */
    public void mo7580hW(String str) {
        switch (bFf().mo6893i(ePr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePr, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePr, new Object[]{str}));
                break;
        }
        m12345hV(str);
    }

    /* renamed from: hY */
    public void mo7581hY(String str) {
        switch (bFf().mo6893i(ePz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePz, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePz, new Object[]{str}));
                break;
        }
        m12346hX(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Active")
    public boolean isActive() {
        switch (bFf().mo6893i(f2259Lc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2259Lc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2259Lc, new Object[0]));
                break;
        }
        return m12352qP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Active")
    @C5566aOg
    public void setActive(boolean z) {
        switch (bFf().mo6893i(awy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awy, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awy, new Object[]{new Boolean(z)}));
                break;
        }
        m12320au(z);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: k */
    public String mo7583k(NPC auf) {
        switch (bFf().mo6893i(ePh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ePh, new Object[]{auf}));
            case 3:
                bFf().mo5606d(new aCE(this, ePh, new Object[]{auf}));
                break;
        }
        return m12347j(auf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: m */
    public String mo7584m(NPC auf) {
        switch (bFf().mo6893i(ePi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ePi, new Object[]{auf}));
            case 3:
                bFf().mo5606d(new aCE(this, ePi, new Object[]{auf}));
                break;
        }
        return m12349l(auf);
    }

    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m12350qN();
    }

    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m12351qO();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: u */
    public void mo7589u(String str, Object obj) {
        switch (bFf().mo6893i(ePk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePk, new Object[]{str, obj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePk, new Object[]{str, obj}));
                break;
        }
        m12354t(str, obj);
    }

    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m12355y(cr);
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f2264Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2264Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2264Wp, new Object[0]));
                break;
        }
        m12356zw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "550ba6c42a6359572d86e1e743a03bde", aum = 0)
    /* renamed from: ar */
    private String m12319ar() {
        return m12318ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Script")
    @C0064Am(aul = "949ec85372107f7ff0aceb1cf5733c90", aum = 0)
    private String bIR() {
        return bnz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Active")
    @C0064Am(aul = "28f27a54f7b6f9588c4e3b8cd1bdcc6a", aum = 0)
    /* renamed from: qP */
    private boolean m12352qP() {
        return m12353qv();
    }

    @C0064Am(aul = "5a4060ed60ed9be416965c72a84c4014", aum = 0)
    /* renamed from: a */
    private void m12296a(StellarSystem jj) {
        super.mo993b(jj);
        start();
    }

    @C0064Am(aul = "aff37266141efb293c034aa14a179dd9", aum = 0)
    /* renamed from: zw */
    private void m12356zw() {
        super.mo1099zx();
        stop();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Node")
    @C0064Am(aul = "681b020de767c990bba1f342e2fa448a", aum = 0)
    /* renamed from: Vd */
    private Node m12291Vd() {
        return m12290Vb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Radius")
    @C0064Am(aul = "9c2d6393180d2e7dd073a2670072be36", aum = 0)
    /* renamed from: Lt */
    private float m12286Lt() {
        return m12285Ln();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "String Table")
    @C0064Am(aul = "12e0424a13253d31c42f9d6e742b8f9a", aum = 0)
    private I18NStringTable bIT() {
        return bIQ();
    }

    @C0064Am(aul = "4488b4113f9919c5f84224db3fbda9c8", aum = 0)
    /* renamed from: hN */
    private void m12339hN(String str) {
        mo6317hy("Scriptable zone '" + m12318ao() + "': " + str);
    }

    @C0064Am(aul = "57d1f4cf1765a96802a339b2ad97e9ae", aum = 0)
    /* renamed from: hP */
    private void m12341hP(String str) {
        mo8358lY("Scriptable zone '" + m12318ao() + "': " + str);
    }

    @C0064Am(aul = "e5648902d76c823cdc705823151bec64", aum = 0)
    /* renamed from: eY */
    private void m12331eY(String str) {
        System.out.println("ScriptableZone '" + m12318ao() + "': " + str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        Heartbeat aVar = (Heartbeat) bFf().mo6865M(Heartbeat.class);
        aVar.mo7591a(this, (aDJ) this);
        m12298a(aVar);
    }

    @C0064Am(aul = "d83fd75e860939eaa470b1350c36d419", aum = 0)
    private Context bjv() {
        Context bkP = Context.call();
        bkP.setOptimizationLevel(0);
        return bkP;
    }

    @C0064Am(aul = "6372d4fa1c5d6c2327925c941066d628", aum = 0)
    private void bjw() {
        try {
            this.daB = new C1761b();
            Context lhVar = this.daB.get();
            this.f2271TR = lhVar.initStandardObjects();
            this.f2271TR.put("controller", this.f2271TR, (Object) this);
            this.f2271TR.put("game/script", this.f2271TR, (Object) bnz());
            lhVar.evaluateReader(this.f2271TR, (Reader) new InputStreamReader(ClassLoader.getSystemResourceAsStream("taikodom/game/script/spacezone/ScriptableZoneInclude.js")), "<ZoneScript>", 1, (Object) null);
        } catch (Exception e) {
            Context.exit();
            e.printStackTrace();
        }
    }

    @C0064Am(aul = "759cafef803394ffae6c81592d20c383", aum = 0)
    private void bIV() {
        mo7574f("heartbeat", new Object[0]);
    }

    @C0064Am(aul = "2779187d7af9aebaf9888e05cbd2ee00", aum = 0)
    /* renamed from: a */
    private void m12295a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(mo7553Lu()));
    }

    @C0064Am(aul = "9bbe77df4dde6c40d44db2ab6ec245b7", aum = 0)
    /* renamed from: y */
    private void m12355y(Actor cr) {
        if (cr instanceof Ship) {
            Character agj = ((Ship) cr).agj();
            if (agj instanceof Player) {
                mo7569bs((Player) agj);
                mo7574f("onPlayerEnter", ((Player) agj).getName());
            } else if ((agj instanceof NPC) && !bIO().values().contains(agj)) {
                String k = mo7583k((NPC) agj);
                if (k == null) {
                    k = bIY();
                    mo7562b((NPC) agj, k);
                }
                mo7574f("onNPCEnter", k);
            }
        }
    }

    @C0064Am(aul = "70a777089fb270a056d5872212dd909b", aum = 0)
    /* renamed from: A */
    private void m12280A(Actor cr) {
        String m;
        if (cr instanceof Ship) {
            Character agj = ((Ship) cr).agj();
            if (agj instanceof Player) {
                mo7574f("onPlayerLeave", ((Player) agj).getName());
            } else if ((agj instanceof NPC) && !bIO().values().contains(agj) && (m = mo7584m((NPC) agj)) != null) {
                mo7574f("onNPCLeave", m);
            }
        }
    }

    @C0064Am(aul = "cfecb87b4f4140e6463aa6fe459aaed0", aum = 0)
    /* renamed from: qN */
    private void m12350qN() {
        bjx();
        m12281F(true);
        bIN().mo4704hk(eOO);
        mo7574f("onZoneStart", new Object[0]);
    }

    @C0064Am(aul = "59c40347b7a0fc9aedbc7b7fdebe0ea9", aum = 0)
    /* renamed from: qO */
    private void m12351qO() {
        mo7574f("onZoneStop", new Object[0]);
        bIN().stop();
        cleanup();
        m12281F(false);
    }

    @C0064Am(aul = "b4743cb0e0a1c2bd9e94ad75f33c11e8", aum = 0)
    /* renamed from: jV */
    private void m12348jV() {
        for (NPC dispose : bIO().values()) {
            dispose.dispose();
        }
        bIO().clear();
        for (Ship fAVar : bjr().keySet()) {
            fAVar.mo8355h(C3981xi.C3982a.class, this);
            fAVar.mo8355h(C0471GX.C0472a.class, this);
        }
        bjr().clear();
        for (AsteroidZone h : bIP().values()) {
            h.mo8355h(C3694ts.C3695a.class, this);
        }
        bIP().clear();
    }

    @C0064Am(aul = "733bbeb75735e7cfeb813e7fff2f5614", aum = 0)
    /* renamed from: e */
    private Object m12329e(String str, Object[] objArr) {
        int i;
        if (this.f2271TR == null) {
            return null;
        }
        if (objArr != null) {
            i = objArr.length;
        } else {
            i = 0;
        }
        String str2 = "invokeTrigger" + String.valueOf(i);
        Object[] objArr2 = new Object[(i + 1)];
        objArr2[0] = str;
        for (int i2 = 0; i2 < i; i2++) {
            objArr2[i2 + 1] = objArr[i2];
        }
        Object obj = this.f2271TR.get(str2, this.f2271TR);
        if (obj != Scriptable.NOT_FOUND) {
            try {
                return ((Function) obj).call(this.daB.get(), this.f2271TR, this.f2271TR, objArr2);
            } catch (Exception e) {
                mo8354g("Error calling trigger '" + str + "' for scriptable zone '" + m12318ao() + "'", (Throwable) e);
            }
        } else {
            m12342hQ("has no relay function '" + str2 + "'!!!");
            return null;
        }
    }

    @C0064Am(aul = "bb535528c877bc13162acdc673337c01", aum = 0)
    /* renamed from: fk */
    private NPCType m12335fk(String str) {
        if (str == null) {
            return null;
        }
        for (NPCType next : ala().aJY()) {
            if (str.equals(next.getHandle())) {
                return next;
            }
        }
        return null;
    }

    @C0064Am(aul = "2b21591b71641ef8a053c109cd1ae9a4", aum = 0)
    /* renamed from: fa */
    private Actor m12333fa(String str) {
        if (str == null) {
            return null;
        }
        if (m12290Vb() == null) {
            return null;
        }
        for (Actor next : m12290Vb().mo21624Zw()) {
            if (next instanceof Station) {
                if (str.equals(((Station) next).getHandle())) {
                    return next;
                }
            } else if ((next instanceof Gate) && str.equals(((Gate) next).getHandle())) {
                return next;
            }
        }
        return null;
    }

    @C0064Am(aul = "e869e258cc89b21ad8d41378d2bca878", aum = 0)
    /* renamed from: aG */
    private Vec3d m12307aG(Actor cr) {
        if (cr == null) {
            return null;
        }
        if (cr instanceof Station) {
            Station bf = (Station) cr;
            return bf.getPosition().mo9531q(bf.azP().mo4109rJ());
        } else if (!(cr instanceof Gate)) {
            return null;
        } else {
            Gate fFVar = (Gate) cr;
            return fFVar.getPosition().mo9531q(fFVar.mo18388rJ());
        }
    }

    @C0064Am(aul = "ccb54d3f01dc0f5dd2b1101824c576e4", aum = 0)
    /* renamed from: aI */
    private Quat4fWrap m12310aI(Actor cr) {
        if (cr == null) {
            return null;
        }
        if (cr instanceof Station) {
            Station bf = (Station) cr;
            return bf.getOrientation().mo15233d((Quat4f) bf.azP().mo4108rH());
        } else if (!(cr instanceof Gate)) {
            return null;
        } else {
            Gate fFVar = (Gate) cr;
            return fFVar.getOrientation().mo15233d((Quat4f) fFVar.mo18387rH());
        }
    }

    @C0064Am(aul = "4fda16663b5539918e9c4d61f4cd2a86", aum = 0)
    private String bIX() {
        StringBuilder sb = new StringBuilder(String.valueOf(m12318ao()));
        int i = this.eOU;
        this.eOU = i + 1;
        return sb.append(String.valueOf(i)).toString();
    }

    @C0064Am(aul = "2cd1413a49e1d9ba8302d79ed571ffa2", aum = 0)
    /* renamed from: a */
    private String m12294a(NPCType aed, Vec3d ajr, Quat4fWrap aoy) {
        NPC bUa = aed.bUa();
        String bIY = bIY();
        bIO().put(bIY, bUa);
        bUa.mo11677nP(bIY);
        Ship bQx = bUa.bQx();
        if (!(ajr == null || aoy == null)) {
            bQx.setPosition(ajr);
            bQx.setOrientation(aoy);
            bQx.mo993b(m12290Vb().mo21606Nc());
        }
        bQx.mo8348d(C3981xi.C3982a.class, this);
        bQx.mo8348d(C0471GX.C0472a.class, this);
        bjr().put(bQx, m12318ao());
        return bIY;
    }

    @C0064Am(aul = "e3f6b650d261c6b3564f353d533dcd0b", aum = 0)
    /* renamed from: Y */
    private String m12292Y(String str, String str2) {
        NPCType fl = m12336fl(str);
        if (fl == null) {
            m12342hQ("trying to spawn an NPC at an actor, but the NPC type is invalid (" + str + ")");
            return null;
        }
        Actor fb = m12334fb(str2);
        if (fb == null) {
            m12342hQ("trying to spawn an NPC at an actor, but the actor is invalid (" + str + ")");
            return null;
        } else if (!(fb instanceof Station)) {
            return m12322b(fl, m12309aH(fb), m12311aJ(fb));
        } else {
            String b = m12322b(fl, (Vec3d) null, (Quat4fWrap) null);
            try {
                ((Station) fb).mo618b((Character) (NPC) bIO().get(b), true);
                return b;
            } catch (C6046afS e) {
                return b;
            }
        }
    }

    @C0064Am(aul = "9db58c887a00f6fdb242daad6094e56d", aum = 0)
    /* renamed from: c */
    private void m12328c(String str, NativeArray ayt) {
        NPCType fl = m12336fl(str);
        if (fl == null) {
            m12340hO("trying to create a routing NPC with handle '" + str + "', but there's no such NPC type");
            return;
        }
        NPC auf = (NPC) bIO().get(m12322b(fl, (Vec3d) null, (Quat4fWrap) null));
        Controller hb = auf.mo12657hb();
        if (!(hb instanceof RoundAIController)) {
            m12340hO("trying to create a routing NPC, but NPCType '" + str + "' has not the correct AI Controller type");
            return;
        }
        RoundAIController im = (RoundAIController) hb;
        long length = ayt.getLength();
        boolean z = false;
        for (int i = 0; ((long) i) < length; i++) {
            Object obj = ayt.get(i, this.f2271TR);
            if (obj instanceof String) {
                Actor fb = m12334fb((String) obj);
                if (fb instanceof Station) {
                    if (!z) {
                        Vec3d aH = m12309aH(fb);
                        Quat4fWrap aJ = m12311aJ(fb);
                        Ship bQx = auf.bQx();
                        bQx.setPosition(aH);
                        bQx.setOrientation(aJ);
                        bQx.mo993b(mo960Nc());
                        z = true;
                    }
                    im.mo2796B((Station) fb);
                }
            }
        }
        im.start();
    }

    @C0064Am(aul = "25625eace308c79bb8a76111075eecc4", aum = 0)
    /* renamed from: hV */
    private void m12345hV(String str) {
        if (str != null) {
            NPC auf = (NPC) bIO().get(str);
            if (auf.bQB()) {
                Actor bhE = auf.bhE();
                if (bhE instanceof Station) {
                    ((Station) bhE).mo658s((Character) auf);
                }
            }
        }
    }

    @C0064Am(aul = "6aaa8e1f711240b6447451c857cc27e2", aum = 0)
    /* renamed from: LD */
    private Random m12283LD() {
        if (this.awe == null) {
            this.awe = new Random();
        }
        return this.awe;
    }

    @C0064Am(aul = "41d90ced97725601a254e85ea8a0d423", aum = 0)
    /* renamed from: a */
    private Vec3d m12293a(C3279pu puVar, NPCType aed) {
        float outerSphereRadius = new C2820ka(aed.bTC().mo19050uX().bkx().getFile()).getOuterSphereRadius();
        Random LE = m12284LE();
        for (int i = 0; i < 10; i++) {
            Vec3f vec3f = new Vec3f((LE.nextFloat() * 2.0f) - 1.0f, (LE.nextFloat() * 2.0f) - 1.0f, (LE.nextFloat() * 2.0f) - 1.0f);
            vec3f.normalize();
            vec3f.scale((float) puVar.mo21237Vg());
            Vec3d q = puVar.mo21241zO().mo9531q(vec3f);
            List<Actor> c = mo965Zc().mo1900c(q, outerSphereRadius);
            if (!(c != null && c.size() > 0)) {
                return q;
            }
        }
        return null;
    }

    @C0064Am(aul = "3d8e9ec97d3e65c6ead1b5b84c11932c", aum = 0)
    /* renamed from: aa */
    private String m12314aa(String str, String str2) {
        if (str == null || str2 == null) {
            return null;
        }
        NPCType fl = m12336fl(str);
        if (fl == null) {
            m12342hQ("trying to spawn a NPC in an area, but the NPC type is invalid (" + str + ")");
            return null;
        }
        C3279pu puVar = (C3279pu) mo7579hU(str2);
        if (puVar == null) {
            m12342hQ("trying to spawn a NPC int an area, but the area is invalid (" + str2 + ")");
            return null;
        }
        Vec3d b = m12321b(puVar, fl);
        if (b == null) {
            m12342hQ("trying to spawn a NPC in an area, but couldn't find an empty position");
            return null;
        }
        String b2 = m12322b(fl, b, getOrientation());
        NPC auf = (NPC) bIO().get(b2);
        if (auf != null) {
            Controller hb = auf.mo12657hb();
            if (hb instanceof DroneAIController) {
                DroneAIController droneAIController = (DroneAIController) hb;
                droneAIController.mo4610h(puVar.mo21241zO());
                droneAIController.setRadius((float) puVar.mo21237Vg());
                droneAIController.start();
            }
        }
        return b2;
    }

    @C0064Am(aul = "c73f97f4fa07a732ce83cd839203a666", aum = 0)
    /* renamed from: ac */
    private void m12315ac(String str, String str2) {
        NPC auf = (NPC) bIO().get(str);
        if (auf == null || auf.isDisposed() || !auf.bQx().bae()) {
            m12340hO("trying to send an NPC to attack a player, but the NPC can't be found (" + str + ")");
            return;
        }
        Player hS = mo7578hS(str2);
        if (hS == null) {
            m12340hO("trying to send an NPC to attack a player, but the player can't be found (" + str2 + ")");
            return;
        }
        Controller hb = auf.mo12657hb();
        if (hb instanceof TrackShipAIController) {
            ((TrackShipAIController) hb).mo4589L(hS);
        } else {
            m12340hO("trying to send an NPC to attack a player, but the NPC has not this capability (" + str2 + ")");
        }
    }

    @C0064Am(aul = "c53742bdf41015c2682bbfa05b99d3bf", aum = 0)
    /* renamed from: a */
    private void m12304a(String str, double d, double d2, double d3, long j) {
        if (str != null) {
            C3279pu puVar = (C3279pu) bFf().mo6865M(C3279pu.class);
            puVar.mo10S();
            puVar.mo21238c(m12290Vb());
            puVar.mo21240h(new Vec3d(d, d2, d3).mo9504d((Tuple3d) m12290Vb().getPosition()));
            puVar.mo21239cJ(j);
            mo7589u(str, puVar);
        }
    }

    @C0064Am(aul = "de215411e0f45317f83963e9a3346523", aum = 0)
    /* renamed from: ae */
    private void m12316ae(String str, String str2) {
        NPC auf = (NPC) bIO().get(str);
        if (auf == null || auf.isDisposed() || !auf.bQx().bae()) {
            m12340hO("trying to send an NPC at an actor, but the NPC can't be found (" + str + ")");
            return;
        }
        Actor fb = m12334fb(str2);
        if (fb == null) {
            m12340hO("trying to send an NPC at an actor, but the actor can't be found (" + str + ")");
            return;
        }
        Controller hb = auf.mo12657hb();
        if (!(hb instanceof ArrivalController)) {
            m12340hO("trying to send an NPC at an actor, but the NPC controller doesn't have such capability (" + str + ")");
            return;
        }
        ArrivalController aby = (ArrivalController) hb;
        aby.mo12546T(m12309aH(fb));
        aby.start();
    }

    @C0064Am(aul = "da82657bec4c857fe77ba916054cb3cd", aum = 0)
    /* renamed from: ag */
    private float m12317ag(String str, String str2) {
        NPC auf = (NPC) bIO().get(str);
        if (auf == null || auf.isDisposed() || !auf.bQx().bae()) {
            m12340hO("trying to get the distance between a NPC and an actor, but the NPC can't be found (" + str + ")");
            return -1.0f;
        }
        Actor fb = m12334fb(str2);
        if (fb != null) {
            return auf.bQx().mo1011bv(fb);
        }
        m12340hO("trying to get the distance between a NPC and an actor, but the actor can't be found (" + str + ")");
        return -1.0f;
    }

    @C0064Am(aul = "a12d9af8a6546946bf274c8af25ddf39", aum = 0)
    /* renamed from: hX */
    private void m12346hX(String str) {
        if (str != null) {
            for (AsteroidZone next : m12290Vb().mo21615ZQ()) {
                if (str.equals(next.getHandle())) {
                    bIP().put(str, next);
                    next.mo8348d(C3694ts.C3695a.class, this);
                    return;
                }
            }
        }
    }

    @C0064Am(aul = "1e43cca91f4ba1278cc0e8b29b3a7498", aum = 0)
    /* renamed from: a */
    private void m12305a(String str, String str2, String str3, float f) {
        mo7566b(str, str2, str3, f, (Asset) null);
    }

    @C0064Am(aul = "b5234eb7d2cf57ebacd8990bc56fb9b9", aum = 0)
    /* renamed from: a */
    private void m12306a(String str, String str2, String str3, float f, Asset tCVar) {
        if (str != null && str3 != null && str2 != null) {
            I18NString hs = bIQ().mo6668hs(str3);
            if (hs == null) {
                m12340hO("is trying to show a NPC message with an invalid string handle: " + str3);
                return;
            }
            NPC auf = (NPC) bIO().get(str2);
            if (auf == null) {
                m12340hO("is trying to show a NPC message with an invalid NPC: " + str2);
                return;
            }
            Player hS = mo7578hS(str);
            if (hS == null) {
                m12340hO("is trying to show a NPC message with an invalid player: " + str);
                return;
            }
            hS.mo14419f((C1506WA) new C5311aEl(auf.mo11654Fs(), hs, (long) (1000.0f * f), tCVar));
        }
    }

    @C0064Am(aul = "da8ccca3fafcfba0aa4ab4c57dfb5d3c", aum = 0)
    /* renamed from: a */
    private void m12301a(Ship fAVar, Actor cr) {
        String str;
        String str2 = (String) bjr().get(fAVar);
        if (cr instanceof Ship) {
            Character agj = ((Ship) cr).agj();
            if (agj instanceof Player) {
                str = ((Player) agj).getName();
                if (str2 != null && str != null) {
                    mo7574f("onNPCDamage", str2, str);
                    return;
                }
            }
        }
        str = null;
        if (str2 != null) {
        }
    }

    @C0064Am(aul = "debcca3fd4203ad4dbc488fa20713736", aum = 0)
    /* renamed from: a */
    private void m12300a(Pawn avi, Actor cr) {
        String str;
        String str2 = (String) bjr().get(avi);
        if (str2 != null) {
            if (cr instanceof Ship) {
                Character agj = ((Ship) cr).agj();
                if (agj instanceof Player) {
                    str = ((Player) agj).getName();
                    mo7574f("onNPCKilled", str2, str);
                }
            }
            str = "unknown";
            mo7574f("onNPCKilled", str2, str);
        }
    }

    @C0064Am(aul = "b754b72256d5a16a894008d0b6e1c1fd", aum = 0)
    /* renamed from: aG */
    private void m12308aG() {
        super.mo70aH();
        if (m12353qv()) {
            stop();
            start();
        }
    }

    @C6485anp
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C5511aMd
    /* renamed from: a.Zz$a */
    public class Heartbeat extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f2272JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f2273aT = null;

        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "a27312afdda86c8167c7522231b3de03", aum = 0)
        static /* synthetic */ ScriptableZone fnC = null;

        static {
            m12399V();
        }

        public Heartbeat() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public Heartbeat(ScriptableZone zz, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(zz, adj);
        }

        public Heartbeat(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m12399V() {
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(Heartbeat.class, "a27312afdda86c8167c7522231b3de03", i);
            f2273aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(Heartbeat.class, "aca6e0904a50554bdd889f4f467c84e8", i3);
            f2272JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(Heartbeat.class, C6019aer.class, _m_fields, _m_methods);
        }

        private ScriptableZone djG() {
            return (ScriptableZone) bFf().mo5608dq().mo3214p(f2273aT);
        }

        /* renamed from: g */
        private void m12400g(ScriptableZone zz) {
            bFf().mo5608dq().mo3197f(f2273aT, zz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6019aer(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m12401pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f2272JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f2272JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f2272JD, new Object[0]));
                    break;
            }
            m12401pi();
        }

        /* renamed from: a */
        public void mo7591a(ScriptableZone zz, aDJ adj) {
            m12400g(zz);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "aca6e0904a50554bdd889f4f467c84e8", aum = 0)
        /* renamed from: pi */
        private void m12401pi() {
            djG().bIW();
        }
    }

    /* renamed from: a.Zz$b */
    /* compiled from: a */
    class C1761b extends ThreadLocal<Context> {
        C1761b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: bin */
        public Context initialValue() {
            return ScriptableZone.this.aRv();
        }
    }
}
