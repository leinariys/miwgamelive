package game.script.spacezone;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.space.Asteroid;
import game.script.space.AsteroidType;
import logic.baa.*;
import logic.bbb.C6348alI;
import logic.data.link.C2911li;
import logic.data.link.C3694ts;
import logic.data.mbean.C6977axq;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5829abJ("1.2.2")
@C6485anp
@C2712iu(mo19786Bt = SpaceZoneType.class)
@C5511aMd
/* renamed from: a.bt */
/* compiled from: a */
public class AsteroidZone extends SpaceZone implements C1616Xf, aOW, C2911li.C2912a, C3763uj {

    /* renamed from: Do */
    public static final C2491fm f5918Do = null;

    /* renamed from: KF */
    public static final C5663aRz f5919KF = null;

    /* renamed from: Lc */
    public static final C2491fm f5920Lc = null;

    /* renamed from: Ld */
    public static final C2491fm f5921Ld = null;

    /* renamed from: Le */
    public static final C2491fm f5922Le = null;

    /* renamed from: Lm */
    public static final C2491fm f5923Lm = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm awC = null;
    public static final C5663aRz dkH = null;
    public static final float erH = 5.0f;
    public static final C5663aRz erI = null;
    public static final C5663aRz erJ = null;
    public static final C5663aRz erK = null;
    public static final C5663aRz erL = null;
    public static final C5663aRz erM = null;
    public static final C5663aRz erN = null;
    public static final C5663aRz erO = null;
    public static final C5663aRz erP = null;
    public static final C5663aRz erR = null;
    public static final C5663aRz erT = null;
    public static final C5663aRz erV = null;
    public static final C2491fm erW = null;
    public static final C2491fm erX = null;
    public static final C2491fm erY = null;
    public static final C2491fm erZ = null;
    public static final C2491fm esa = null;
    public static final C2491fm esb = null;
    public static final C2491fm esc = null;
    public static final C2491fm esd = null;
    public static final C2491fm ese = null;
    public static final C2491fm esf = null;
    public static final C2491fm esg = null;
    public static final C2491fm esh = null;
    private static final String erG = "asteroid_reposition";
    private static final long serialVersionUID = 1961874086450107147L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "613eb2db7709cf4c0cdab0ed14a4ed85", aum = 11)
    private static boolean active = false;
    @C0064Am(aul = "02c8cc35c7665dd1600977036900f09f", aum = 0)
    private static C2089b bmk = null;
    @C0064Am(aul = "4c05862818630611d0cb5d10662673c8", aum = 1)
    @C5454aJy("Distribution Function (arch)")
    private static C2088a bml = null;
    @C0064Am(aul = "dedbacca160ac2c44edd90f4a038c0e0", aum = 2)
    @C5454aJy("Distribution Angle (arch)")
    private static float bmm = 0.0f;
    @C0064Am(aul = "e6518a72d1b279e1ff2bd9eb777c5693", aum = 3)
    @C5454aJy("Number of Turns (spiral)")
    private static float bmn = 0.0f;
    @C0064Am(aul = "86c3fe7c0d31e3184da453b2e98f00c7", aum = 4)
    @C5454aJy("Distribution Width (0-100, relative to zone radius)")
    private static float bmo = 0.0f;
    @C0064Am(aul = "08de3feb5b122085ed39aba514da3997", aum = 5)
    private static int bmp = 0;
    @C0064Am(aul = "c9233f0c5b7a0e181b8694800ddaab97", aum = 6)
    private static int bmq = 0;
    @C0064Am(aul = "64975d0f2e4875d382a25be83dfe6588", aum = 7)
    @C5454aJy("Dispersion Factor (arch, spiral)")
    private static float bmr = 0.0f;
    @C0064Am(aul = "76ee57ebaf9ed83dce5fc99ef37d182a", aum = 8)
    private static AsteroidSpawnTable bms = null;
    @C0064Am(aul = "5336e2be20e3ce37efd0f6b39b5f563a", aum = 9)
    private static AsteroidZoneCategory erQ = null;
    @C0064Am(aul = "7a2c38a4d12cb05bdaf13760b89ec4c1", aum = 10)
    private static boolean erS = false;
    @C0064Am(aul = "9cee1d408ef08b4a98ea486a97744e49", aum = 12)
    private static C3438ra<Asteroid> erU = null;

    static {
        m28049V();
    }

    public AsteroidZone() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidZone(C5540aNg ang) {
        super(ang);
    }

    public AsteroidZone(AsteroidZoneType asq) {
        super((C5540aNg) null);
        super._m_script_init(asq);
    }

    /* renamed from: V */
    static void m28049V() {
        _m_fieldCount = SpaceZone._m_fieldCount + 13;
        _m_methodCount = SpaceZone._m_methodCount + 24;
        int i = SpaceZone._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 13)];
        C5663aRz b = C5640aRc.m17844b(AsteroidZone.class, "02c8cc35c7665dd1600977036900f09f", i);
        erI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AsteroidZone.class, "4c05862818630611d0cb5d10662673c8", i2);
        erJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AsteroidZone.class, "dedbacca160ac2c44edd90f4a038c0e0", i3);
        erK = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AsteroidZone.class, "e6518a72d1b279e1ff2bd9eb777c5693", i4);
        erL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AsteroidZone.class, "86c3fe7c0d31e3184da453b2e98f00c7", i5);
        erM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AsteroidZone.class, "08de3feb5b122085ed39aba514da3997", i6);
        erN = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AsteroidZone.class, "c9233f0c5b7a0e181b8694800ddaab97", i7);
        erO = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(AsteroidZone.class, "64975d0f2e4875d382a25be83dfe6588", i8);
        erP = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(AsteroidZone.class, "76ee57ebaf9ed83dce5fc99ef37d182a", i9);
        dkH = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(AsteroidZone.class, "5336e2be20e3ce37efd0f6b39b5f563a", i10);
        erR = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(AsteroidZone.class, "7a2c38a4d12cb05bdaf13760b89ec4c1", i11);
        erT = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(AsteroidZone.class, "613eb2db7709cf4c0cdab0ed14a4ed85", i12);
        f5919KF = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(AsteroidZone.class, "9cee1d408ef08b4a98ea486a97744e49", i13);
        erV = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpaceZone._m_fields, (Object[]) _m_fields);
        int i15 = SpaceZone._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i15 + 24)];
        C2491fm a = C4105zY.m41624a(AsteroidZone.class, "3d9b5c9fbfee671366a872a73d6e474e", i15);
        erW = a;
        fmVarArr[i15] = a;
        int i16 = i15 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidZone.class, "c464574fdb141fbff2509d38a32a36ef", i16);
        erX = a2;
        fmVarArr[i16] = a2;
        int i17 = i16 + 1;
        C2491fm a3 = C4105zY.m41624a(AsteroidZone.class, "7137be90beaab4e1d8db8a6bf09e5ec0", i17);
        erY = a3;
        fmVarArr[i17] = a3;
        int i18 = i17 + 1;
        C2491fm a4 = C4105zY.m41624a(AsteroidZone.class, "73f85e9d4a96501bec3dcd5f5d3961f4", i18);
        erZ = a4;
        fmVarArr[i18] = a4;
        int i19 = i18 + 1;
        C2491fm a5 = C4105zY.m41624a(AsteroidZone.class, "db90b28e2e905aa4d2e1ab8a271e8efd", i19);
        esa = a5;
        fmVarArr[i19] = a5;
        int i20 = i19 + 1;
        C2491fm a6 = C4105zY.m41624a(AsteroidZone.class, "5e558a170478b995e6e52857b0a84491", i20);
        esb = a6;
        fmVarArr[i20] = a6;
        int i21 = i20 + 1;
        C2491fm a7 = C4105zY.m41624a(AsteroidZone.class, "8c7282cb169aad5aecde3bb668984bb2", i21);
        _f_start_0020_0028_0029V = a7;
        fmVarArr[i21] = a7;
        int i22 = i21 + 1;
        C2491fm a8 = C4105zY.m41624a(AsteroidZone.class, "ae68706c4b1b7d89cc56d91de5d94874", i22);
        _f_stop_0020_0028_0029V = a8;
        fmVarArr[i22] = a8;
        int i23 = i22 + 1;
        C2491fm a9 = C4105zY.m41624a(AsteroidZone.class, "8c218185988a9ca1e4ec8e59197fb372", i23);
        f5920Lc = a9;
        fmVarArr[i23] = a9;
        int i24 = i23 + 1;
        C2491fm a10 = C4105zY.m41624a(AsteroidZone.class, "a28d641f139f6496d291a992916828de", i24);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a10;
        fmVarArr[i24] = a10;
        int i25 = i24 + 1;
        C2491fm a11 = C4105zY.m41624a(AsteroidZone.class, "eb006ef3d6b3ead10c5d4d447473e5a0", i25);
        _f_onResurrect_0020_0028_0029V = a11;
        fmVarArr[i25] = a11;
        int i26 = i25 + 1;
        C2491fm a12 = C4105zY.m41624a(AsteroidZone.class, "057a09a90a7836f9c3debe006f3dee91", i26);
        esc = a12;
        fmVarArr[i26] = a12;
        int i27 = i26 + 1;
        C2491fm a13 = C4105zY.m41624a(AsteroidZone.class, "d2eaee628ba25a985106174257a82a46", i27);
        esd = a13;
        fmVarArr[i27] = a13;
        int i28 = i27 + 1;
        C2491fm a14 = C4105zY.m41624a(AsteroidZone.class, "f7cc5b6b8c987189bc98151eff99859c", i28);
        f5921Ld = a14;
        fmVarArr[i28] = a14;
        int i29 = i28 + 1;
        C2491fm a15 = C4105zY.m41624a(AsteroidZone.class, "7ef259af8a15cd20660296187a74a853", i29);
        ese = a15;
        fmVarArr[i29] = a15;
        int i30 = i29 + 1;
        C2491fm a16 = C4105zY.m41624a(AsteroidZone.class, "279a21a533e6f970fdadff751e9c0fae", i30);
        esf = a16;
        fmVarArr[i30] = a16;
        int i31 = i30 + 1;
        C2491fm a17 = C4105zY.m41624a(AsteroidZone.class, "272048b72dad70687e7234847fed46c0", i31);
        _f_tick_0020_0028F_0029V = a17;
        fmVarArr[i31] = a17;
        int i32 = i31 + 1;
        C2491fm a18 = C4105zY.m41624a(AsteroidZone.class, "6d9e69b0ef40852396f20dec23acc575", i32);
        f5922Le = a18;
        fmVarArr[i32] = a18;
        int i33 = i32 + 1;
        C2491fm a19 = C4105zY.m41624a(AsteroidZone.class, "04cc7293bcc3dd058d2a0e537b21f1b3", i33);
        f5918Do = a19;
        fmVarArr[i33] = a19;
        int i34 = i33 + 1;
        C2491fm a20 = C4105zY.m41624a(AsteroidZone.class, "04838cfbd693af6c76306cf8f71af189", i34);
        f5923Lm = a20;
        fmVarArr[i34] = a20;
        int i35 = i34 + 1;
        C2491fm a21 = C4105zY.m41624a(AsteroidZone.class, "97e8bf9d1a7f3345ec7ee856b2084246", i35);
        awC = a21;
        fmVarArr[i35] = a21;
        int i36 = i35 + 1;
        C2491fm a22 = C4105zY.m41624a(AsteroidZone.class, "15d3bcad7dee524f1a5cb3492209f596", i36);
        esg = a22;
        fmVarArr[i36] = a22;
        int i37 = i36 + 1;
        C2491fm a23 = C4105zY.m41624a(AsteroidZone.class, "f20f0cf501a0a099bfeebf770e0240a5", i37);
        esh = a23;
        fmVarArr[i37] = a23;
        int i38 = i37 + 1;
        C2491fm a24 = C4105zY.m41624a(AsteroidZone.class, "e6f132201317b5fc3662783c5ab8b548", i38);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a24;
        fmVarArr[i38] = a24;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpaceZone._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidZone.class, C6977axq.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m28047F(boolean z) {
        bFf().mo5608dq().mo3153a(f5919KF, z);
    }

    /* renamed from: a */
    private void m28051a(AsteroidSpawnTable oi) {
        throw new C6039afL();
    }

    @C0064Am(aul = "3d9b5c9fbfee671366a872a73d6e474e", aum = 0)
    @C6580apg
    /* renamed from: a */
    private void m28052a(AsteroidType aqn, Actor cr) {
        bFf().mo5600a((Class<? extends C4062yl>) C3694ts.C3695a.class, (C0495Gr) new aCE(this, erW, new Object[]{aqn, cr}));
    }

    /* renamed from: a */
    private void m28053a(C2088a aVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m28054a(C2089b bVar) {
        throw new C6039afL();
    }

    @C6580apg
    /* renamed from: b */
    private void m28058b(AsteroidType aqn, Actor cr) {
        switch (bFf().mo6893i(erW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, erW, new Object[]{aqn, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, erW, new Object[]{aqn, cr}));
                break;
        }
        m28052a(aqn, cr);
    }

    private void bAa() {
        switch (bFf().mo6893i(esa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esa, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esa, new Object[0]));
                break;
        }
        bzZ();
    }

    @C0064Am(aul = "5e558a170478b995e6e52857b0a84491", aum = 0)
    @C5566aOg
    private void bAb() {
        throw new aWi(new aCE(this, esb, new Object[0]));
    }

    @C5566aOg
    private void bAc() {
        switch (bFf().mo6893i(esb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esb, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esb, new Object[0]));
                break;
        }
        bAb();
    }

    /* renamed from: bs */
    private void m28059bs(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(erV, raVar);
    }

    private C2089b bzJ() {
        return ((AsteroidZoneType) getType()).duS();
    }

    private C2088a bzK() {
        return ((AsteroidZoneType) getType()).duU();
    }

    private float bzL() {
        return ((AsteroidZoneType) getType()).duW();
    }

    private float bzM() {
        return ((AsteroidZoneType) getType()).duY();
    }

    private float bzN() {
        return ((AsteroidZoneType) getType()).dva();
    }

    private int bzO() {
        return ((AsteroidZoneType) getType()).dvc();
    }

    private int bzP() {
        return ((AsteroidZoneType) getType()).dve();
    }

    private float bzQ() {
        return ((AsteroidZoneType) getType()).dvg();
    }

    private AsteroidSpawnTable bzR() {
        return ((AsteroidZoneType) getType()).dvi();
    }

    private AsteroidZoneCategory bzS() {
        return (AsteroidZoneCategory) bFf().mo5608dq().mo3214p(erR);
    }

    private boolean bzT() {
        return bFf().mo5608dq().mo3201h(erT);
    }

    private C3438ra bzU() {
        return (C3438ra) bFf().mo5608dq().mo3214p(erV);
    }

    private float bzY() {
        switch (bFf().mo6893i(erZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, erZ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, erZ, new Object[0]));
                break;
        }
        return bzX();
    }

    /* renamed from: dy */
    private void m28060dy(boolean z) {
        bFf().mo5608dq().mo3153a(erT, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Aligned Asteroids")
    @C0064Am(aul = "7137be90beaab4e1d8db8a6bf09e5ec0", aum = 0)
    @C5566aOg
    /* renamed from: dz */
    private void m28061dz(boolean z) {
        throw new aWi(new aCE(this, erY, new Object[]{new Boolean(z)}));
    }

    /* renamed from: e */
    private void m28062e(AsteroidZoneCategory nWVar) {
        bFf().mo5608dq().mo3197f(erR, nWVar);
    }

    /* renamed from: iv */
    private void m28065iv(float f) {
        throw new C6039afL();
    }

    /* renamed from: iw */
    private void m28066iw(float f) {
        throw new C6039afL();
    }

    /* renamed from: ix */
    private void m28067ix(float f) {
        throw new C6039afL();
    }

    /* renamed from: iy */
    private void m28068iy(float f) {
        throw new C6039afL();
    }

    /* renamed from: k */
    private void m28069k(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Object[] args = gr.getArgs();
            ((C3694ts.C3695a) it.next()).mo7556a(this, (AsteroidType) args[0], (Actor) args[1]);
        }
    }

    /* renamed from: oF */
    private void m28071oF(int i) {
        throw new C6039afL();
    }

    /* renamed from: oG */
    private void m28072oG(int i) {
        throw new C6039afL();
    }

    @C0064Am(aul = "8c7282cb169aad5aecde3bb668984bb2", aum = 0)
    @C5566aOg
    /* renamed from: qN */
    private void m28074qN() {
        throw new aWi(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "ae68706c4b1b7d89cc56d91de5d94874", aum = 0)
    @C5566aOg
    /* renamed from: qO */
    private void m28075qO() {
        throw new aWi(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "e6f132201317b5fc3662783c5ab8b548", aum = 0)
    /* renamed from: qW */
    private Object m28078qW() {
        return bAe();
    }

    /* renamed from: qv */
    private boolean m28079qv() {
        return bFf().mo5608dq().mo3201h(f5919KF);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m28048U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6977axq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpaceZone._m_methodCount) {
            case 0:
                m28052a((AsteroidType) args[0], (Actor) args[1]);
                return null;
            case 1:
                return new Boolean(bzV());
            case 2:
                m28061dz(((Boolean) args[0]).booleanValue());
                return null;
            case 3:
                return new Float(bzX());
            case 4:
                bzZ();
                return null;
            case 5:
                bAb();
                return null;
            case 6:
                m28074qN();
                return null;
            case 7:
                m28075qO();
                return null;
            case 8:
                return new Boolean(m28076qP());
            case 9:
                return m28056au();
            case 10:
                m28055aG();
                return null;
            case 11:
                return bAd();
            case 12:
                return bAf();
            case 13:
                m28070n((Actor) args[0]);
                return null;
            case 14:
                m28057b((Asteroid) args[0], (Actor) args[1]);
                return null;
            case 15:
                m28064gQ((String) args[0]);
                return null;
            case 16:
                m28048U(((Float) args[0]).floatValue());
                return null;
            case 17:
                m28073p((Actor) args[0]);
                return null;
            case 18:
                m28050a((C0665JT) args[0]);
                return null;
            case 19:
                m28077qU();
                return null;
            case 20:
                m28080y((Actor) args[0]);
                return null;
            case 21:
                return bAh();
            case 22:
                m28063f((AsteroidZoneCategory) args[0]);
                return null;
            case 23:
                return m28078qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo17467a(Asteroid aeg, Actor cr) {
        switch (bFf().mo6893i(ese)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ese, new Object[]{aeg, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ese, new Object[]{aeg, cr}));
                break;
        }
        m28057b(aeg, cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - SpaceZone._m_methodCount) {
            case 0:
                m28069k(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m28055aG();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f5918Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5918Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5918Do, new Object[]{jt}));
                break;
        }
        m28050a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    public AsteroidZoneType bAe() {
        switch (bFf().mo6893i(esc)) {
            case 0:
                return null;
            case 2:
                return (AsteroidZoneType) bFf().mo5606d(new aCE(this, esc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esc, new Object[0]));
                break;
        }
        return bAd();
    }

    public List<Asteroid> bAg() {
        switch (bFf().mo6893i(esd)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, esd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esd, new Object[0]));
                break;
        }
        return bAf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Category")
    public AsteroidZoneCategory bAi() {
        switch (bFf().mo6893i(esg)) {
            case 0:
                return null;
            case 2:
                return (AsteroidZoneCategory) bFf().mo5606d(new aCE(this, esg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esg, new Object[0]));
                break;
        }
        return bAh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Aligned Asteroids")
    public boolean bzW() {
        switch (bFf().mo6893i(erX)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, erX, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, erX, new Object[0]));
                break;
        }
        return bzV();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cj */
    public void mo14332cj(String str) {
        switch (bFf().mo6893i(esf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esf, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esf, new Object[]{str}));
                break;
        }
        m28064gQ(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Aligned Asteroids")
    @C5566aOg
    /* renamed from: dA */
    public void mo17473dA(boolean z) {
        switch (bFf().mo6893i(erY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, erY, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, erY, new Object[]{new Boolean(z)}));
                break;
        }
        m28061dz(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Category")
    /* renamed from: g */
    public void mo17474g(AsteroidZoneCategory nWVar) {
        switch (bFf().mo6893i(esh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esh, new Object[]{nWVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esh, new Object[]{nWVar}));
                break;
        }
        m28063f(nWVar);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m28078qW();
    }

    public boolean isActive() {
        switch (bFf().mo6893i(f5920Lc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5920Lc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5920Lc, new Object[0]));
                break;
        }
        return m28076qP();
    }

    /* renamed from: o */
    public void mo17476o(Actor cr) {
        switch (bFf().mo6893i(f5921Ld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5921Ld, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5921Ld, new Object[]{cr}));
                break;
        }
        m28070n(cr);
    }

    /* renamed from: q */
    public void mo17477q(Actor cr) {
        switch (bFf().mo6893i(f5922Le)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5922Le, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5922Le, new Object[]{cr}));
                break;
        }
        m28073p(cr);
    }

    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f5923Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5923Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5923Lm, new Object[0]));
                break;
        }
        m28077qU();
    }

    @C5566aOg
    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m28074qN();
    }

    @C5566aOg
    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m28075qO();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m28056au();
    }

    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m28080y(cr);
    }

    /* renamed from: c */
    public void mo17472c(AsteroidZoneType asq) {
        super.mo967a((C2961mJ) asq);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Aligned Asteroids")
    @C0064Am(aul = "c464574fdb141fbff2509d38a32a36ef", aum = 0)
    private boolean bzV() {
        return bzT();
    }

    @C0064Am(aul = "73f85e9d4a96501bec3dcd5f5d3961f4", aum = 0)
    private float bzX() {
        float bzP = (float) bzP();
        if (bzP < 5.0f) {
            return 5.0f;
        }
        return bzP;
    }

    @C0064Am(aul = "db90b28e2e905aa4d2e1ab8a271e8efd", aum = 0)
    private void bzZ() {
        int nextFloat;
        Vec3f vec3f;
        Quat4fWrap orientation;
        Random LE = mo20315LE();
        float Lu = mo20316Lu() * (bzN() / 100.0f);
        if (bzJ() == C2089b.ARCH) {
            float f = 0.0f;
            if (bzK() == C2088a.LINEAR) {
                f = (LE.nextFloat() - 0.5f) * bzL();
            } else if (bzK() == C2088a.GAUSSIAN) {
                f = (((float) LE.nextGaussian()) / 3.0f) * (bzL() / 2.0f);
            }
            float cos = (float) Math.cos(Math.toRadians((double) f));
            float f2 = Lu / 2.0f;
            float Lu2 = mo20316Lu() - f2;
            float cos2 = 2.0f * (((float) Math.cos(Math.toRadians((double) ((f / bzL()) * 180.0f)))) + 0.1f);
            Vec3f aT = getOrientation().mo15209aT(new Vec3f((((float) Math.sin(Math.toRadians((double) f))) * Lu2) + (((LE.nextFloat() * 2.0f) - 1.0f) * f2 * cos2), (LE.nextFloat() - 0.5f) * cos2 * f2 * 2.0f, (f2 * ((LE.nextFloat() * 2.0f) - 1.0f) * cos2) + (cos * (-Lu2))));
            nextFloat = ((int) (((float) Math.cos(Math.toRadians((double) ((f / bzL()) * 180.0f)))) * 100.0f)) - ((int) ((((double) LE.nextFloat()) - 0.25d) * ((double) bzQ())));
            vec3f = aT;
        } else if (bzJ() == C2089b.SPHERE) {
            Vec3f mS = new Vec3f((LE.nextFloat() * 2.0f) - 1.0f, (LE.nextFloat() * 2.0f) - 1.0f, (LE.nextFloat() * 2.0f) - 1.0f).dfO().mo23510mS((mo20316Lu() - Lu) + (LE.nextFloat() * Lu));
            nextFloat = (int) (LE.nextFloat() * 100.0f);
            vec3f = mS;
        } else if (bzJ() == C2089b.SPIRAL) {
            float nextFloat2 = LE.nextFloat();
            float bzM = 360.0f * nextFloat2 * bzM();
            float Lu3 = mo20316Lu() * nextFloat2;
            float f3 = Lu * (1.0f - nextFloat2);
            Vec3f aT2 = getOrientation().mo15209aT(new Vec3f(((((LE.nextFloat() * 2.0f) - 1.0f) * f3 * (1.0f - nextFloat2)) + Lu3) * ((float) Math.cos(Math.toRadians((double) bzM))), ((LE.nextFloat() * 2.0f) - 1.0f) * f3 * (1.0f - nextFloat2), ((f3 * ((LE.nextFloat() * 2.0f) - 1.0f) * (1.0f - nextFloat2)) + Lu3) * ((float) Math.sin(Math.toRadians((double) bzM)))));
            nextFloat = ((int) ((1.0f - nextFloat2) * 100.0f)) - ((int) ((((double) LE.nextFloat()) - 0.25d) * ((double) bzQ())));
            vec3f = aT2;
        } else {
            throw new IllegalArgumentException("Asteroid zone '" + getName() + "' has not a valid distribution shape");
        }
        if (!bzT()) {
            orientation = new Quat4fWrap((((double) (360.0f * LE.nextFloat())) * 3.141592653589793d) / 180.0d, (((double) (360.0f * LE.nextFloat())) * 3.141592653589793d) / 180.0d, (((double) (LE.nextFloat() * 360.0f)) * 3.141592653589793d) / 180.0d);
        } else {
            orientation = getOrientation();
        }
        Asteroid dpN = bzR().mo4478md(nextFloat).dpN();
        dpN.mo8652j(bzS());
        dpN.setPosition(getPosition().mo9531q(vec3f));
        dpN.setOrientation(orientation);
        dpN.setStatic(true);
        dpN.mo993b(mo960Nc());
        dpN.mo8348d(C2911li.C2912a.class, this);
        bzU().add(dpN);
    }

    @C0064Am(aul = "8c218185988a9ca1e4ec8e59197fb372", aum = 0)
    /* renamed from: qP */
    private boolean m28076qP() {
        return m28079qv();
    }

    @C0064Am(aul = "a28d641f139f6496d291a992916828de", aum = 0)
    /* renamed from: au */
    private String m28056au() {
        return "AsteroidZone: [" + getName() + "]";
    }

    @C0064Am(aul = "eb006ef3d6b3ead10c5d4d447473e5a0", aum = 0)
    /* renamed from: aG */
    private void m28055aG() {
        super.mo70aH();
        if (m28079qv()) {
            start();
        }
        if (C5877acF.RUNNING_CLIENT_BOT && !isActive()) {
            System.out.println("[BOTS] Will start asteroid zone " + getName());
            mo8361ms((float) ((Math.random() * 10.0d) + 10.0d));
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "057a09a90a7836f9c3debe006f3dee91", aum = 0)
    private AsteroidZoneType bAd() {
        return (AsteroidZoneType) super.getType();
    }

    @C0064Am(aul = "d2eaee628ba25a985106174257a82a46", aum = 0)
    private List<Asteroid> bAf() {
        return Collections.unmodifiableList(bzU());
    }

    @C0064Am(aul = "f7cc5b6b8c987189bc98151eff99859c", aum = 0)
    /* renamed from: n */
    private void m28070n(Actor cr) {
        if (bHa() && !cr.isStatic() && (cr instanceof Ship)) {
            ((Ship) cr).agj();
        }
    }

    @C0064Am(aul = "7ef259af8a15cd20660296187a74a853", aum = 0)
    /* renamed from: b */
    private void m28057b(Asteroid aeg, Actor cr) {
        aeg.mo8355h(C2911li.C2912a.class, this);
        if (bzU().contains(aeg)) {
            bzU().remove(aeg);
            SpaceZoneEvent afb = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
            afb.mo8792a(this, cVr() + ((long) (bzY() * 1000.0f)), erG);
            mo20322b(afb);
            m28058b(aeg.ddg(), cr);
        }
    }

    @C0064Am(aul = "279a21a533e6f970fdadff751e9c0fae", aum = 0)
    /* renamed from: gQ */
    private void m28064gQ(String str) {
        if (erG.equals(str)) {
            bAa();
        }
    }

    @C0064Am(aul = "272048b72dad70687e7234847fed46c0", aum = 0)
    /* renamed from: U */
    private void m28048U(float f) {
        super.mo4709V(f);
        if (!m28079qv() && C5877acF.RUNNING_CLIENT_BOT) {
            setActive(true);
        }
    }

    @C0064Am(aul = "6d9e69b0ef40852396f20dec23acc575", aum = 0)
    /* renamed from: p */
    private void m28073p(Actor cr) {
    }

    @C0064Am(aul = "04cc7293bcc3dd058d2a0e537b21f1b3", aum = 0)
    /* renamed from: a */
    private void m28050a(C0665JT jt) {
        Object obj;
        if ("".equals(jt.getVersion())) {
            mo8360mc("stopping Asteroid Zone " + getName());
            stop();
        }
        if (jt.mo3117j(1, 2, 1)) {
            super.mo24b(jt);
        }
        if (jt.mo3117j(1, 2, 2) && (obj = jt.get("shape")) != null && (obj instanceof C6348alI)) {
            C6348alI ali = (C6348alI) obj;
            mo8359ma("Porting Zone. Radius: " + ali.getRadius());
            mo20320aW(ali.getRadius());
        }
    }

    @C0064Am(aul = "04838cfbd693af6c76306cf8f71af189", aum = 0)
    /* renamed from: qU */
    private void m28077qU() {
        push();
    }

    @C0064Am(aul = "97e8bf9d1a7f3345ec7ee856b2084246", aum = 0)
    /* renamed from: y */
    private void m28080y(Actor cr) {
        super.mo7590z(cr);
        if (bGY() && (cr instanceof Ship) && (((Ship) cr).agj() instanceof Player)) {
            Player aku = (Player) ((Ship) cr).agj();
            if (bzS() != null) {
                aku.dyl().mo11981e((C4068yr) bzS());
            }
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Category")
    @C0064Am(aul = "15d3bcad7dee524f1a5cb3492209f596", aum = 0)
    private AsteroidZoneCategory bAh() {
        return bzS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Category")
    @C0064Am(aul = "f20f0cf501a0a099bfeebf770e0240a5", aum = 0)
    /* renamed from: f */
    private void m28063f(AsteroidZoneCategory nWVar) {
        m28062e(nWVar);
    }

    /* renamed from: a.bt$a */
    public enum C2088a {
        LINEAR,
        GAUSSIAN
    }

    /* renamed from: a.bt$b */
    /* compiled from: a */
    public enum C2089b {
        SPHERE,
        ARCH,
        SPIRAL
    }
}
