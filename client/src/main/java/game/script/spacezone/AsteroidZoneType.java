package game.script.spacezone;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C3693tr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aSQ */
/* compiled from: a */
public class AsteroidZoneType extends SpaceZoneType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f3750dN = null;
    public static final C5663aRz dkH = null;
    public static final C5663aRz erI = null;
    public static final C5663aRz erJ = null;
    public static final C5663aRz erK = null;
    public static final C5663aRz erL = null;
    public static final C5663aRz erM = null;
    public static final C5663aRz erN = null;
    public static final C5663aRz erO = null;
    public static final C5663aRz erP = null;
    public static final C2491fm iNA = null;
    public static final C2491fm iNB = null;
    public static final C2491fm iNC = null;
    public static final C2491fm iND = null;
    public static final C2491fm iNE = null;
    public static final C2491fm iNF = null;
    public static final C2491fm iNG = null;
    public static final C2491fm iNH = null;
    public static final C2491fm iNI = null;
    public static final C2491fm iNJ = null;
    public static final C2491fm iNK = null;
    public static final C2491fm iNL = null;
    public static final C2491fm iNM = null;
    public static final C2491fm iNu = null;
    public static final C2491fm iNv = null;
    public static final C2491fm iNw = null;
    public static final C2491fm iNx = null;
    public static final C2491fm iNy = null;
    public static final C2491fm iNz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8a8ee4d7b08e51622b23881fa61de4df", aum = 0)
    private static AsteroidZone.C2089b bmk;
    @C0064Am(aul = "a01f94046f592b49f79e54aaed514e03", aum = 1)
    private static AsteroidZone.C2088a bml;
    @C0064Am(aul = "c4e1e251c8b7b0fb0dad50a119b21cf3", aum = 2)
    private static float bmm;
    @C0064Am(aul = "b0bf771034b583740303577e96359052", aum = 3)
    private static float bmn;
    @C0064Am(aul = "3d79f0d3d555551d41b597bf7ce5964e", aum = 4)
    private static float bmo;
    @C0064Am(aul = "0f361b499661f13de83d455a59519340", aum = 5)
    private static int bmp;
    @C0064Am(aul = "8b491e5ecc45e592ce0b943266b131f3", aum = 6)
    private static int bmq;
    @C0064Am(aul = "2af3cbf155f52ad1358d7967b32431b9", aum = 7)
    private static float bmr;
    @C0064Am(aul = "37ef1669c8b354e171c0e60cf979e740", aum = 8)
    private static AsteroidSpawnTable bms;

    static {
        m18180V();
    }

    public AsteroidZoneType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidZoneType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18180V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpaceZoneType._m_fieldCount + 9;
        _m_methodCount = SpaceZoneType._m_methodCount + 20;
        int i = SpaceZoneType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(AsteroidZoneType.class, "8a8ee4d7b08e51622b23881fa61de4df", i);
        erI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AsteroidZoneType.class, "a01f94046f592b49f79e54aaed514e03", i2);
        erJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AsteroidZoneType.class, "c4e1e251c8b7b0fb0dad50a119b21cf3", i3);
        erK = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AsteroidZoneType.class, "b0bf771034b583740303577e96359052", i4);
        erL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AsteroidZoneType.class, "3d79f0d3d555551d41b597bf7ce5964e", i5);
        erM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AsteroidZoneType.class, "0f361b499661f13de83d455a59519340", i6);
        erN = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AsteroidZoneType.class, "8b491e5ecc45e592ce0b943266b131f3", i7);
        erO = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(AsteroidZoneType.class, "2af3cbf155f52ad1358d7967b32431b9", i8);
        erP = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(AsteroidZoneType.class, "37ef1669c8b354e171c0e60cf979e740", i9);
        dkH = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpaceZoneType._m_fields, (Object[]) _m_fields);
        int i11 = SpaceZoneType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 20)];
        C2491fm a = C4105zY.m41624a(AsteroidZoneType.class, "8d0a0a62934cb7ad5dc5af42ca4b780c", i11);
        iNu = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidZoneType.class, "8231b3a196f83687b52953d1d062bae9", i12);
        iNv = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(AsteroidZoneType.class, "f1ba071a43241ef12346767b8b08360e", i13);
        iNw = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(AsteroidZoneType.class, "028e73454a062745d2b5088fde47f71f", i14);
        iNx = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(AsteroidZoneType.class, "d3d1e6e2c7546a73c265e0e6cacce906", i15);
        iNy = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(AsteroidZoneType.class, "daebcac309b8ef9d333ff31b8d205ad7", i16);
        iNz = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(AsteroidZoneType.class, "bb1ae3c59aaa77d8d260561fea30c1fc", i17);
        iNA = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(AsteroidZoneType.class, "9d1c165f6c2f005922e0c6a2af91af43", i18);
        iNB = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(AsteroidZoneType.class, "2da9eb3959be9332438771b38c6efa02", i19);
        iNC = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(AsteroidZoneType.class, "f6838c06f22cd2513df963aabcf8af2b", i20);
        iND = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(AsteroidZoneType.class, "9facfcdaf0087330c3a3a4e17fa87c62", i21);
        iNE = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(AsteroidZoneType.class, "1e8488ff791b0ffbeb93c461a84c88a4", i22);
        iNF = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        C2491fm a13 = C4105zY.m41624a(AsteroidZoneType.class, "6c230b7ca3b4dbac5d0a9d797a9f3b6f", i23);
        iNG = a13;
        fmVarArr[i23] = a13;
        int i24 = i23 + 1;
        C2491fm a14 = C4105zY.m41624a(AsteroidZoneType.class, "e8d4e68f430481d192c064f3b4de8ec2", i24);
        iNH = a14;
        fmVarArr[i24] = a14;
        int i25 = i24 + 1;
        C2491fm a15 = C4105zY.m41624a(AsteroidZoneType.class, "24580703f6e816d106ed6a7ee1948a18", i25);
        iNI = a15;
        fmVarArr[i25] = a15;
        int i26 = i25 + 1;
        C2491fm a16 = C4105zY.m41624a(AsteroidZoneType.class, "4aaa869134cbc647bfa3951abee4f69e", i26);
        iNJ = a16;
        fmVarArr[i26] = a16;
        int i27 = i26 + 1;
        C2491fm a17 = C4105zY.m41624a(AsteroidZoneType.class, "cc487c4dcd822537cd17978d2834d0e1", i27);
        iNK = a17;
        fmVarArr[i27] = a17;
        int i28 = i27 + 1;
        C2491fm a18 = C4105zY.m41624a(AsteroidZoneType.class, "bccb51050bbeb7a8082025c1ff81d3ff", i28);
        iNL = a18;
        fmVarArr[i28] = a18;
        int i29 = i28 + 1;
        C2491fm a19 = C4105zY.m41624a(AsteroidZoneType.class, "cf44d5f004a760d32a7b682f9abd6daf", i29);
        iNM = a19;
        fmVarArr[i29] = a19;
        int i30 = i29 + 1;
        C2491fm a20 = C4105zY.m41624a(AsteroidZoneType.class, "d1cdae3f96f913d2603119427cc72850", i30);
        f3750dN = a20;
        fmVarArr[i30] = a20;
        int i31 = i30 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpaceZoneType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidZoneType.class, C3693tr.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m18181a(AsteroidSpawnTable oi) {
        bFf().mo5608dq().mo3197f(dkH, oi);
    }

    /* renamed from: a */
    private void m18182a(AsteroidZone.C2088a aVar) {
        bFf().mo5608dq().mo3197f(erJ, aVar);
    }

    /* renamed from: a */
    private void m18183a(AsteroidZone.C2089b bVar) {
        bFf().mo5608dq().mo3197f(erI, bVar);
    }

    private AsteroidZone.C2089b bzJ() {
        return (AsteroidZone.C2089b) bFf().mo5608dq().mo3214p(erI);
    }

    private AsteroidZone.C2088a bzK() {
        return (AsteroidZone.C2088a) bFf().mo5608dq().mo3214p(erJ);
    }

    private float bzL() {
        return bFf().mo5608dq().mo3211m(erK);
    }

    private float bzM() {
        return bFf().mo5608dq().mo3211m(erL);
    }

    private float bzN() {
        return bFf().mo5608dq().mo3211m(erM);
    }

    private int bzO() {
        return bFf().mo5608dq().mo3212n(erN);
    }

    private int bzP() {
        return bFf().mo5608dq().mo3212n(erO);
    }

    private float bzQ() {
        return bFf().mo5608dq().mo3211m(erP);
    }

    private AsteroidSpawnTable bzR() {
        return (AsteroidSpawnTable) bFf().mo5608dq().mo3214p(dkH);
    }

    /* renamed from: iv */
    private void m18188iv(float f) {
        bFf().mo5608dq().mo3150a(erK, f);
    }

    /* renamed from: iw */
    private void m18189iw(float f) {
        bFf().mo5608dq().mo3150a(erL, f);
    }

    /* renamed from: ix */
    private void m18190ix(float f) {
        bFf().mo5608dq().mo3150a(erM, f);
    }

    /* renamed from: iy */
    private void m18191iy(float f) {
        bFf().mo5608dq().mo3150a(erP, f);
    }

    /* renamed from: oF */
    private void m18192oF(int i) {
        bFf().mo5608dq().mo3183b(erN, i);
    }

    /* renamed from: oG */
    private void m18193oG(int i) {
        bFf().mo5608dq().mo3183b(erO, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Num Of Asteroids")
    /* renamed from: AL */
    public void mo11367AL(int i) {
        switch (bFf().mo6893i(iNF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNF, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNF, new Object[]{new Integer(i)}));
                break;
        }
        m18178AK(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reposition Period")
    /* renamed from: AN */
    public void mo11368AN(int i) {
        switch (bFf().mo6893i(iNH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNH, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNH, new Object[]{new Integer(i)}));
                break;
        }
        m18179AM(i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3693tr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpaceZoneType._m_methodCount) {
            case 0:
                return duR();
            case 1:
                m18187b((AsteroidZone.C2089b) args[0]);
                return null;
            case 2:
                return duT();
            case 3:
                m18186b((AsteroidZone.C2088a) args[0]);
                return null;
            case 4:
                return new Float(duV());
            case 5:
                m18194os(((Float) args[0]).floatValue());
                return null;
            case 6:
                return new Float(duX());
            case 7:
                m18195ou(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Float(duZ());
            case 9:
                m18196ow(((Float) args[0]).floatValue());
                return null;
            case 10:
                return new Integer(dvb());
            case 11:
                m18178AK(((Integer) args[0]).intValue());
                return null;
            case 12:
                return new Integer(dvd());
            case 13:
                m18179AM(((Integer) args[0]).intValue());
                return null;
            case 14:
                return new Float(dvf());
            case 15:
                m18197oy(((Float) args[0]).floatValue());
                return null;
            case 16:
                return dvh();
            case 17:
                m18185b((AsteroidSpawnTable) args[0]);
                return null;
            case 18:
                return dvj();
            case 19:
                return m18184aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3750dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3750dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3750dN, new Object[0]));
                break;
        }
        return m18184aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Table")
    /* renamed from: c */
    public void mo11369c(AsteroidSpawnTable oi) {
        switch (bFf().mo6893i(iNL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNL, new Object[]{oi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNL, new Object[]{oi}));
                break;
        }
        m18185b(oi);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Function (arch)")
    /* renamed from: c */
    public void mo11370c(AsteroidZone.C2088a aVar) {
        switch (bFf().mo6893i(iNx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNx, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNx, new Object[]{aVar}));
                break;
        }
        m18186b(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Shape")
    /* renamed from: c */
    public void mo11371c(AsteroidZone.C2089b bVar) {
        switch (bFf().mo6893i(iNv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNv, new Object[]{bVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNv, new Object[]{bVar}));
                break;
        }
        m18187b(bVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Shape")
    public AsteroidZone.C2089b duS() {
        switch (bFf().mo6893i(iNu)) {
            case 0:
                return null;
            case 2:
                return (AsteroidZone.C2089b) bFf().mo5606d(new aCE(this, iNu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iNu, new Object[0]));
                break;
        }
        return duR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Function (arch)")
    public AsteroidZone.C2088a duU() {
        switch (bFf().mo6893i(iNw)) {
            case 0:
                return null;
            case 2:
                return (AsteroidZone.C2088a) bFf().mo5606d(new aCE(this, iNw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iNw, new Object[0]));
                break;
        }
        return duT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Angle (arch)")
    public float duW() {
        switch (bFf().mo6893i(iNy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iNy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iNy, new Object[0]));
                break;
        }
        return duV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Number of Turns (spiral)")
    public float duY() {
        switch (bFf().mo6893i(iNA)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iNA, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iNA, new Object[0]));
                break;
        }
        return duX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Width (0-100, relative to zone radius)")
    public float dva() {
        switch (bFf().mo6893i(iNC)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iNC, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iNC, new Object[0]));
                break;
        }
        return duZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Num Of Asteroids")
    public int dvc() {
        switch (bFf().mo6893i(iNE)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, iNE, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, iNE, new Object[0]));
                break;
        }
        return dvb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reposition Period")
    public int dve() {
        switch (bFf().mo6893i(iNG)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, iNG, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, iNG, new Object[0]));
                break;
        }
        return dvd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dispersion Factor (arch, spiral)")
    public float dvg() {
        switch (bFf().mo6893i(iNI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iNI, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iNI, new Object[0]));
                break;
        }
        return dvf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Table")
    public AsteroidSpawnTable dvi() {
        switch (bFf().mo6893i(iNK)) {
            case 0:
                return null;
            case 2:
                return (AsteroidSpawnTable) bFf().mo5606d(new aCE(this, iNK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iNK, new Object[0]));
                break;
        }
        return dvh();
    }

    public AsteroidZone dvk() {
        switch (bFf().mo6893i(iNM)) {
            case 0:
                return null;
            case 2:
                return (AsteroidZone) bFf().mo5606d(new aCE(this, iNM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iNM, new Object[0]));
                break;
        }
        return dvj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Angle (arch)")
    /* renamed from: ot */
    public void mo11382ot(float f) {
        switch (bFf().mo6893i(iNz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNz, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNz, new Object[]{new Float(f)}));
                break;
        }
        m18194os(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Number of Turns (spiral)")
    /* renamed from: ov */
    public void mo11383ov(float f) {
        switch (bFf().mo6893i(iNB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNB, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNB, new Object[]{new Float(f)}));
                break;
        }
        m18195ou(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Width (0-100, relative to zone radius)")
    /* renamed from: ox */
    public void mo11384ox(float f) {
        switch (bFf().mo6893i(iND)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iND, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iND, new Object[]{new Float(f)}));
                break;
        }
        m18196ow(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dispersion Factor (arch, spiral)")
    /* renamed from: oz */
    public void mo11385oz(float f) {
        switch (bFf().mo6893i(iNJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iNJ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iNJ, new Object[]{new Float(f)}));
                break;
        }
        m18197oy(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Shape")
    @C0064Am(aul = "8d0a0a62934cb7ad5dc5af42ca4b780c", aum = 0)
    private AsteroidZone.C2089b duR() {
        return bzJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Shape")
    @C0064Am(aul = "8231b3a196f83687b52953d1d062bae9", aum = 0)
    /* renamed from: b */
    private void m18187b(AsteroidZone.C2089b bVar) {
        m18183a(bVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Function (arch)")
    @C0064Am(aul = "f1ba071a43241ef12346767b8b08360e", aum = 0)
    private AsteroidZone.C2088a duT() {
        return bzK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Function (arch)")
    @C0064Am(aul = "028e73454a062745d2b5088fde47f71f", aum = 0)
    /* renamed from: b */
    private void m18186b(AsteroidZone.C2088a aVar) {
        m18182a(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Angle (arch)")
    @C0064Am(aul = "d3d1e6e2c7546a73c265e0e6cacce906", aum = 0)
    private float duV() {
        return bzL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Angle (arch)")
    @C0064Am(aul = "daebcac309b8ef9d333ff31b8d205ad7", aum = 0)
    /* renamed from: os */
    private void m18194os(float f) {
        m18188iv(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Number of Turns (spiral)")
    @C0064Am(aul = "bb1ae3c59aaa77d8d260561fea30c1fc", aum = 0)
    private float duX() {
        return bzM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Number of Turns (spiral)")
    @C0064Am(aul = "9d1c165f6c2f005922e0c6a2af91af43", aum = 0)
    /* renamed from: ou */
    private void m18195ou(float f) {
        m18189iw(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Distribution Width (0-100, relative to zone radius)")
    @C0064Am(aul = "2da9eb3959be9332438771b38c6efa02", aum = 0)
    private float duZ() {
        return bzN();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Distribution Width (0-100, relative to zone radius)")
    @C0064Am(aul = "f6838c06f22cd2513df963aabcf8af2b", aum = 0)
    /* renamed from: ow */
    private void m18196ow(float f) {
        m18190ix(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Num Of Asteroids")
    @C0064Am(aul = "9facfcdaf0087330c3a3a4e17fa87c62", aum = 0)
    private int dvb() {
        return bzO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Num Of Asteroids")
    @C0064Am(aul = "1e8488ff791b0ffbeb93c461a84c88a4", aum = 0)
    /* renamed from: AK */
    private void m18178AK(int i) {
        m18192oF(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reposition Period")
    @C0064Am(aul = "6c230b7ca3b4dbac5d0a9d797a9f3b6f", aum = 0)
    private int dvd() {
        return bzP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reposition Period")
    @C0064Am(aul = "e8d4e68f430481d192c064f3b4de8ec2", aum = 0)
    /* renamed from: AM */
    private void m18179AM(int i) {
        m18193oG(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dispersion Factor (arch, spiral)")
    @C0064Am(aul = "24580703f6e816d106ed6a7ee1948a18", aum = 0)
    private float dvf() {
        return bzQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dispersion Factor (arch, spiral)")
    @C0064Am(aul = "4aaa869134cbc647bfa3951abee4f69e", aum = 0)
    /* renamed from: oy */
    private void m18197oy(float f) {
        m18191iy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Table")
    @C0064Am(aul = "cc487c4dcd822537cd17978d2834d0e1", aum = 0)
    private AsteroidSpawnTable dvh() {
        return bzR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Table")
    @C0064Am(aul = "bccb51050bbeb7a8082025c1ff81d3ff", aum = 0)
    /* renamed from: b */
    private void m18185b(AsteroidSpawnTable oi) {
        m18181a(oi);
    }

    @C0064Am(aul = "cf44d5f004a760d32a7b682f9abd6daf", aum = 0)
    private AsteroidZone dvj() {
        return (AsteroidZone) mo745aU();
    }

    @C0064Am(aul = "d1cdae3f96f913d2603119427cc72850", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m18184aT() {
        T t = (AsteroidZone) bFf().mo6865M(AsteroidZone.class);
        t.mo17472c(this);
        return t;
    }
}
