package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C2565gs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.Vt */
/* compiled from: a */
public class SpaceZoneTypesManager extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1923bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1924bM = null;
    /* renamed from: bN */
    public static final C2491fm f1925bN = null;
    /* renamed from: bO */
    public static final C2491fm f1926bO = null;
    /* renamed from: bP */
    public static final C2491fm f1927bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1928bQ = null;
    public static final C5663aRz esJ = null;
    public static final C5663aRz esK = null;
    public static final C5663aRz esL = null;
    public static final C2491fm esM = null;
    public static final C2491fm esN = null;
    public static final C2491fm esO = null;
    public static final C2491fm esP = null;
    public static final C2491fm esQ = null;
    public static final C2491fm esR = null;
    public static final C2491fm esS = null;
    public static final C2491fm esT = null;
    public static final C2491fm esU = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c838ba4e498b64eddcdf9b6cb0271cba", aum = 0)

    /* renamed from: Qb */
    private static C3438ra<NPCZoneType> f1919Qb;
    @C0064Am(aul = "8776fe1b666200b8970a6b33f7d72d39", aum = 1)

    /* renamed from: Qc */
    private static C3438ra<PopulationControlType> f1920Qc;
    @C0064Am(aul = "a10cd40c179a9daf94468223ed3692f6", aum = 2)

    /* renamed from: Qd */
    private static C3438ra<AsteroidZoneType> f1921Qd;
    @C0064Am(aul = "e24308886401a5e3cf09bb94e3e6f44b", aum = 3)

    /* renamed from: bK */
    private static UUID f1922bK;
    @C0064Am(aul = "25505f127572146fe07b99a1e9c647bf", aum = 4)
    private static String handle;

    static {
        m10901V();
    }

    public SpaceZoneTypesManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpaceZoneTypesManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10901V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(SpaceZoneTypesManager.class, "c838ba4e498b64eddcdf9b6cb0271cba", i);
        esJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpaceZoneTypesManager.class, "8776fe1b666200b8970a6b33f7d72d39", i2);
        esK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpaceZoneTypesManager.class, "a10cd40c179a9daf94468223ed3692f6", i3);
        esL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpaceZoneTypesManager.class, "e24308886401a5e3cf09bb94e3e6f44b", i4);
        f1923bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpaceZoneTypesManager.class, "25505f127572146fe07b99a1e9c647bf", i5);
        f1924bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 13)];
        C2491fm a = C4105zY.m41624a(SpaceZoneTypesManager.class, "8ea6acd361cf594e17f8aff458fa267d", i7);
        f1925bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(SpaceZoneTypesManager.class, "4a8ca1a4dc83c083208433c941f05430", i8);
        f1926bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(SpaceZoneTypesManager.class, "db86cea594ad23a7e6abe7d3ed973ff7", i9);
        f1927bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(SpaceZoneTypesManager.class, "a29b5ae46ee02c8b5a5d499a892fcb06", i10);
        f1928bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(SpaceZoneTypesManager.class, "7d743378e0427cb01da9cd714f239495", i11);
        esM = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(SpaceZoneTypesManager.class, "eb2e1527a22eaac01752e12081dbb2e4", i12);
        esN = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(SpaceZoneTypesManager.class, "64b6a28b259f8f3e2de86c1b34094344", i13);
        esO = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(SpaceZoneTypesManager.class, "d742327006ed1fcd369ecfc4925c2536", i14);
        esP = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(SpaceZoneTypesManager.class, "0735727bf1c7cf048c547610e657917d", i15);
        esQ = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(SpaceZoneTypesManager.class, "7b26279f9426443887ad4ac325223d68", i16);
        esR = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(SpaceZoneTypesManager.class, "4f8c2a0589b97ae7fc0e2c167d2072f0", i17);
        esS = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(SpaceZoneTypesManager.class, "3d541c413ad88eb615bb53cda12bd470", i18);
        esT = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(SpaceZoneTypesManager.class, "e03ed62337f08385f746436525872091", i19);
        esU = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpaceZoneTypesManager.class, C2565gs.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m10902a(String str) {
        bFf().mo5608dq().mo3197f(f1924bM, str);
    }

    /* renamed from: a */
    private void m10903a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1923bL, uuid);
    }

    /* renamed from: an */
    private UUID m10904an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1923bL);
    }

    /* renamed from: ao */
    private String m10905ao() {
        return (String) bFf().mo5608dq().mo3214p(f1924bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "a29b5ae46ee02c8b5a5d499a892fcb06", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m10908b(String str) {
        throw new aWi(new aCE(this, f1928bQ, new Object[]{str}));
    }

    private C3438ra bAQ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(esJ);
    }

    private C3438ra bAR() {
        return (C3438ra) bFf().mo5608dq().mo3214p(esK);
    }

    private C3438ra bAS() {
        return (C3438ra) bFf().mo5608dq().mo3214p(esL);
    }

    /* renamed from: bt */
    private void m10910bt(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(esJ, raVar);
    }

    /* renamed from: bu */
    private void m10911bu(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(esK, raVar);
    }

    /* renamed from: bv */
    private void m10912bv(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(esL, raVar);
    }

    /* renamed from: c */
    private void m10913c(UUID uuid) {
        switch (bFf().mo6893i(f1926bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1926bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1926bO, new Object[]{uuid}));
                break;
        }
        m10909b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCZoneTypes")
    @C0064Am(aul = "7d743378e0427cb01da9cd714f239495", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m10914d(NPCZoneType hu) {
        throw new aWi(new aCE(this, esM, new Object[]{hu}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AsteroidZoneTypes")
    @C0064Am(aul = "4f8c2a0589b97ae7fc0e2c167d2072f0", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m10915d(AsteroidZoneType asq) {
        throw new aWi(new aCE(this, esS, new Object[]{asq}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "PopulationControlTypes")
    @C0064Am(aul = "d742327006ed1fcd369ecfc4925c2536", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m10916d(PopulationControlType rsVar) {
        throw new aWi(new aCE(this, esP, new Object[]{rsVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCZoneTypes")
    @C0064Am(aul = "eb2e1527a22eaac01752e12081dbb2e4", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m10917f(NPCZoneType hu) {
        throw new aWi(new aCE(this, esN, new Object[]{hu}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AsteroidZoneTypes")
    @C0064Am(aul = "3d541c413ad88eb615bb53cda12bd470", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m10918f(AsteroidZoneType asq) {
        throw new aWi(new aCE(this, esT, new Object[]{asq}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "PopulationControlTypes")
    @C0064Am(aul = "0735727bf1c7cf048c547610e657917d", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m10919f(PopulationControlType rsVar) {
        throw new aWi(new aCE(this, esQ, new Object[]{rsVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2565gs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m10906ap();
            case 1:
                m10909b((UUID) args[0]);
                return null;
            case 2:
                return m10907ar();
            case 3:
                m10908b((String) args[0]);
                return null;
            case 4:
                m10914d((NPCZoneType) args[0]);
                return null;
            case 5:
                m10917f((NPCZoneType) args[0]);
                return null;
            case 6:
                return bAT();
            case 7:
                m10916d((PopulationControlType) args[0]);
                return null;
            case 8:
                m10919f((PopulationControlType) args[0]);
                return null;
            case 9:
                return bAV();
            case 10:
                m10915d((AsteroidZoneType) args[0]);
                return null;
            case 11:
                m10918f((AsteroidZoneType) args[0]);
                return null;
            case 12:
                return bAX();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1925bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1925bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1925bN, new Object[0]));
                break;
        }
        return m10906ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCZoneTypes")
    public List<NPCZoneType> bAU() {
        switch (bFf().mo6893i(esO)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, esO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esO, new Object[0]));
                break;
        }
        return bAT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "PopulationControlTypes")
    public List<PopulationControlType> bAW() {
        switch (bFf().mo6893i(esR)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, esR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esR, new Object[0]));
                break;
        }
        return bAV();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AsteroidZoneTypes")
    public List<AsteroidZoneType> bAY() {
        switch (bFf().mo6893i(esU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, esU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esU, new Object[0]));
                break;
        }
        return bAX();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCZoneTypes")
    @C5566aOg
    /* renamed from: e */
    public void mo6429e(NPCZoneType hu) {
        switch (bFf().mo6893i(esM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esM, new Object[]{hu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esM, new Object[]{hu}));
                break;
        }
        m10914d(hu);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AsteroidZoneTypes")
    @C5566aOg
    /* renamed from: e */
    public void mo6430e(AsteroidZoneType asq) {
        switch (bFf().mo6893i(esS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esS, new Object[]{asq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esS, new Object[]{asq}));
                break;
        }
        m10915d(asq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "PopulationControlTypes")
    @C5566aOg
    /* renamed from: e */
    public void mo6431e(PopulationControlType rsVar) {
        switch (bFf().mo6893i(esP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esP, new Object[]{rsVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esP, new Object[]{rsVar}));
                break;
        }
        m10916d(rsVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCZoneTypes")
    @C5566aOg
    /* renamed from: g */
    public void mo6432g(NPCZoneType hu) {
        switch (bFf().mo6893i(esN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esN, new Object[]{hu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esN, new Object[]{hu}));
                break;
        }
        m10917f(hu);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AsteroidZoneTypes")
    @C5566aOg
    /* renamed from: g */
    public void mo6433g(AsteroidZoneType asq) {
        switch (bFf().mo6893i(esT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esT, new Object[]{asq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esT, new Object[]{asq}));
                break;
        }
        m10918f(asq);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "PopulationControlTypes")
    @C5566aOg
    /* renamed from: g */
    public void mo6434g(PopulationControlType rsVar) {
        switch (bFf().mo6893i(esQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esQ, new Object[]{rsVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esQ, new Object[]{rsVar}));
                break;
        }
        m10919f(rsVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1927bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1927bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1927bP, new Object[0]));
                break;
        }
        return m10907ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1928bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1928bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1928bQ, new Object[]{str}));
                break;
        }
        m10908b(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "8ea6acd361cf594e17f8aff458fa267d", aum = 0)
    /* renamed from: ap */
    private UUID m10906ap() {
        return m10904an();
    }

    @C0064Am(aul = "4a8ca1a4dc83c083208433c941f05430", aum = 0)
    /* renamed from: b */
    private void m10909b(UUID uuid) {
        m10903a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "db86cea594ad23a7e6abe7d3ed973ff7", aum = 0)
    /* renamed from: ar */
    private String m10907ar() {
        return m10905ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCZoneTypes")
    @C0064Am(aul = "64b6a28b259f8f3e2de86c1b34094344", aum = 0)
    private List<NPCZoneType> bAT() {
        return Collections.unmodifiableList(bAQ());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "PopulationControlTypes")
    @C0064Am(aul = "7b26279f9426443887ad4ac325223d68", aum = 0)
    private List<PopulationControlType> bAV() {
        return Collections.unmodifiableList(bAR());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AsteroidZoneTypes")
    @C0064Am(aul = "e03ed62337f08385f746436525872091", aum = 0)
    private List<AsteroidZoneType> bAX() {
        return Collections.unmodifiableList(bAS());
    }
}
