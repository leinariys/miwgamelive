package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Trigger;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5470aKo;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aGz  reason: case insensitive filesystem */
/* compiled from: a */
public class SpaceZoneTrigger extends Trigger implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm awC = null;
    public static final C2491fm awD = null;
    public static final C2491fm awo = null;
    public static final C5663aRz dSF = null;
    public static final C2491fm hPY = null;
    /* renamed from: vi */
    public static final C2491fm f2900vi = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "199574f91a11b25ff4918dd3da4c9733", aum = 0)
    private static SpaceZone hPX = null;

    static {
        m15072V();
    }

    public SpaceZoneTrigger() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpaceZoneTrigger(C5540aNg ang) {
        super(ang);
    }

    public SpaceZoneTrigger(SpaceZone ldVar) {
        super((C5540aNg) null);
        super._m_script_init(ldVar);
    }

    /* renamed from: V */
    static void m15072V() {
        _m_fieldCount = Trigger._m_fieldCount + 1;
        _m_methodCount = Trigger._m_methodCount + 6;
        int i = Trigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(SpaceZoneTrigger.class, "199574f91a11b25ff4918dd3da4c9733", i);
        dSF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Trigger._m_fields, (Object[]) _m_fields);
        int i3 = Trigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
        C2491fm a = C4105zY.m41624a(SpaceZoneTrigger.class, "7a5bdc7ff589f62b2de4968c5cfb48b4", i3);
        awo = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(SpaceZoneTrigger.class, "64eecb86a08abdc00f3d94170c95f846", i4);
        f2900vi = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(SpaceZoneTrigger.class, "48cb4edb56dff19ea3e7b3cc59451224", i5);
        awC = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(SpaceZoneTrigger.class, "a81f4ac8d515f7608521f1defd5131ce", i6);
        awD = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(SpaceZoneTrigger.class, "275ad1946a0dc4d724a7c30d37a16b91", i7);
        hPY = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(SpaceZoneTrigger.class, "1e38529e9818359613d7a8acbf228894", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Trigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpaceZoneTrigger.class, C5470aKo.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "a81f4ac8d515f7608521f1defd5131ce", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: A */
    private void m15070A(Actor cr) {
        throw new aWi(new aCE(this, awD, new Object[]{cr}));
    }

    /* renamed from: a */
    private void m15073a(SpaceZone ldVar) {
        bFf().mo5608dq().mo3197f(dSF, ldVar);
    }

    private SpaceZone daq() {
        return (SpaceZone) bFf().mo5608dq().mo3214p(dSF);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "48cb4edb56dff19ea3e7b3cc59451224", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: y */
    private void m15076y(Actor cr) {
        throw new aWi(new aCE(this, awC, new Object[]{cr}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: B */
    public void mo7552B(Actor cr) {
        switch (bFf().mo6893i(awD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                break;
        }
        m15070A(cr);
    }

    /* renamed from: Ls */
    public boolean mo959Ls() {
        switch (bFf().mo6893i(awo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, awo, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, awo, new Object[0]));
                break;
        }
        return m15071Lr();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5470aKo(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Trigger._m_methodCount) {
            case 0:
                return new Boolean(m15071Lr());
            case 1:
                return m15075it();
            case 2:
                m15076y((Actor) args[0]);
                return null;
            case 3:
                m15070A((Actor) args[0]);
                return null;
            case 4:
                return dar();
            case 5:
                return m15074au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public SpaceZone das() {
        switch (bFf().mo6893i(hPY)) {
            case 0:
                return null;
            case 2:
                return (SpaceZone) bFf().mo5606d(new aCE(this, hPY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hPY, new Object[0]));
                break;
        }
        return dar();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f2900vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2900vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2900vi, new Object[0]));
                break;
        }
        return m15075it();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m15074au();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m15076y(cr);
    }

    /* renamed from: b */
    public void mo9148b(SpaceZone ldVar) {
        super.mo10S();
        m15073a(ldVar);
    }

    @C0064Am(aul = "7a5bdc7ff589f62b2de4968c5cfb48b4", aum = 0)
    /* renamed from: Lr */
    private boolean m15071Lr() {
        return false;
    }

    @C0064Am(aul = "64eecb86a08abdc00f3d94170c95f846", aum = 0)
    /* renamed from: it */
    private String m15075it() {
        return null;
    }

    @C0064Am(aul = "275ad1946a0dc4d724a7c30d37a16b91", aum = 0)
    private SpaceZone dar() {
        return daq();
    }

    @C0064Am(aul = "1e38529e9818359613d7a8acbf228894", aum = 0)
    /* renamed from: au */
    private String m15074au() {
        return "SpaceZoneTrigger: [" + daq().getName() + "]";
    }
}
