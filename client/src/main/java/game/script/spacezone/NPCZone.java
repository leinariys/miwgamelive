package game.script.spacezone;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.Actor;
import game.script.Character;
import game.script.npc.NPC;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.spacezone.population.BossNestPopulationControl;
import logic.baa.*;
import logic.bbb.C6348alI;
import logic.data.link.C0471GX;
import logic.data.link.C3161oY;
import logic.data.link.C3981xi;
import logic.data.mbean.C3726uF;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;

@C5829abJ("1.2.3")
@C6485anp
@C2712iu(mo19786Bt = SpaceZoneType.class)
@C5511aMd
/* renamed from: a.fw */
/* compiled from: a */
public class NPCZone extends SpaceZone implements C0471GX.C0472a, C1616Xf, aOW, C3161oY.C3162a, C3981xi.C3982a {

    /* renamed from: Do */
    public static final C2491fm f7496Do = null;

    /* renamed from: KA */
    public static final C5663aRz f7497KA = null;
    /* renamed from: KC */
    public static final C5663aRz f7499KC = null;
    /* renamed from: KE */
    public static final C5663aRz f7501KE = null;
    /* renamed from: KF */
    public static final C5663aRz f7502KF = null;
    /* renamed from: KH */
    public static final C5663aRz f7504KH = null;
    /* renamed from: KJ */
    public static final C5663aRz f7506KJ = null;
    /* renamed from: KL */
    public static final C5663aRz f7508KL = null;
    /* renamed from: KN */
    public static final C5663aRz f7510KN = null;
    /* renamed from: KP */
    public static final C5663aRz f7512KP = null;
    /* renamed from: KQ */
    public static final C2491fm f7513KQ = null;
    /* renamed from: KR */
    public static final C2491fm f7514KR = null;
    /* renamed from: KS */
    public static final C2491fm f7515KS = null;
    /* renamed from: KT */
    public static final C2491fm f7516KT = null;
    /* renamed from: KU */
    public static final C2491fm f7517KU = null;
    /* renamed from: KV */
    public static final C2491fm f7518KV = null;
    /* renamed from: KW */
    public static final C2491fm f7519KW = null;
    /* renamed from: KX */
    public static final C2491fm f7520KX = null;
    /* renamed from: KY */
    public static final C2491fm f7521KY = null;
    /* renamed from: KZ */
    public static final C2491fm f7522KZ = null;
    /* renamed from: La */
    public static final C2491fm f7524La = null;
    /* renamed from: Lb */
    public static final C2491fm f7525Lb = null;
    /* renamed from: Lc */
    public static final C2491fm f7526Lc = null;
    /* renamed from: Ld */
    public static final C2491fm f7527Ld = null;
    /* renamed from: Le */
    public static final C2491fm f7528Le = null;
    /* renamed from: Lf */
    public static final C2491fm f7529Lf = null;
    /* renamed from: Lg */
    public static final C2491fm f7530Lg = null;
    /* renamed from: Lh */
    public static final C2491fm f7531Lh = null;
    /* renamed from: Li */
    public static final C2491fm f7532Li = null;
    /* renamed from: Lj */
    public static final C2491fm f7533Lj = null;
    /* renamed from: Lk */
    public static final C2491fm f7534Lk = null;
    /* renamed from: Ll */
    public static final C2491fm f7535Ll = null;
    /* renamed from: Lm */
    public static final C2491fm f7536Lm = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f7537x13860637 = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    private static final long serialVersionUID = -6547359859994017325L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "2a4e42c0e5b76b8df7eeba02d2f41e09", aum = 1)

    /* renamed from: KB */
    private static PopulationBehaviour f7498KB = null;
    @C0064Am(aul = "1aef66102293474c414cba1f9fcb9c16", aum = 2)

    /* renamed from: KD */
    private static C2686iZ<Character> f7500KD = null;
    @C0064Am(aul = "ec26ac83187601c22df43ef96066e9de", aum = 4)

    /* renamed from: KG */
    private static boolean f7503KG = false;
    @C0064Am(aul = "e97793f3973b073865c56b01b4d9365f", aum = 5)
    @C5454aJy("Zone Music SFX")

    /* renamed from: KI */
    private static Asset f7505KI = null;
    @C0064Am(aul = "fc02d9d68d135b44c03cf21db5d3f9e9", aum = 6)

    /* renamed from: KK */
    private static Vec3f f7507KK = null;
    @C0064Am(aul = "d3bb811e8efee6a93bb9090577a29d72", aum = 7)

    /* renamed from: KM */
    private static float f7509KM = 0.0f;
    @C0064Am(aul = "f37434ffa8615cc856209fab31675d8e", aum = 8)

    /* renamed from: KO */
    private static C2505a f7511KO = null;
    @C0064Am(aul = "55c0dce5f85c860224d11995113be5c7", aum = 0)
    @C0803Ld

    /* renamed from: Kz */
    private static PopulationControl f7523Kz = null;
    @C0064Am(aul = "b797416a3b7ec9e7c92e21c124c002bc", aum = 3)
    private static boolean active = false;

    static {
        m31545V();
    }

    public NPCZone() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCZone(NPCZoneType hu) {
        super((C5540aNg) null);
        super._m_script_init(hu);
    }

    public NPCZone(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m31545V() {
        _m_fieldCount = SpaceZone._m_fieldCount + 9;
        _m_methodCount = SpaceZone._m_methodCount + 31;
        int i = SpaceZone._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(NPCZone.class, "55c0dce5f85c860224d11995113be5c7", i);
        f7497KA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCZone.class, "2a4e42c0e5b76b8df7eeba02d2f41e09", i2);
        f7499KC = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPCZone.class, "1aef66102293474c414cba1f9fcb9c16", i3);
        f7501KE = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NPCZone.class, "b797416a3b7ec9e7c92e21c124c002bc", i4);
        f7502KF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NPCZone.class, "ec26ac83187601c22df43ef96066e9de", i5);
        f7504KH = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NPCZone.class, "e97793f3973b073865c56b01b4d9365f", i6);
        f7506KJ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NPCZone.class, "fc02d9d68d135b44c03cf21db5d3f9e9", i7);
        f7508KL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NPCZone.class, "d3bb811e8efee6a93bb9090577a29d72", i8);
        f7510KN = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NPCZone.class, "f37434ffa8615cc856209fab31675d8e", i9);
        f7512KP = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpaceZone._m_fields, (Object[]) _m_fields);
        int i11 = SpaceZone._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 31)];
        C2491fm a = C4105zY.m41624a(NPCZone.class, "62cb003fd2cc32ea33bc475f13122bc8", i11);
        f7513KQ = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCZone.class, "39580b5339ba8db1ea70a34396bd82cf", i12);
        f7514KR = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCZone.class, "6aa398b2a9af3c0eef81ddc33c5e6b49", i13);
        f7515KS = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCZone.class, "5a8cb7c543ea4f92067713e7a8759a58", i14);
        f7516KT = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCZone.class, "e1bb2fa61b68300f5a9b8b7d5d5acb2a", i15);
        f7517KU = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCZone.class, "0a67ce6e3186edf0a1eebfbbd2d35d82", i16);
        f7518KV = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCZone.class, "aac7abbdb87cee8fa10433d4f2924420", i17);
        f7519KW = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCZone.class, "79a4210ed3f80e95e49e09a58ea4c25e", i18);
        f7520KX = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(NPCZone.class, "415492ecbb8a9bfc2ce8a2123657b42c", i19);
        f7521KY = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(NPCZone.class, "0d48db76818daea9666d999d751f023f", i20);
        f7522KZ = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(NPCZone.class, "b41f7446edebcb8044df2a01b7079cdc", i21);
        f7524La = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(NPCZone.class, "fd1a915a650a6c56e42d362302cbabc3", i22);
        f7525Lb = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        C2491fm a13 = C4105zY.m41624a(NPCZone.class, "f306bb57f289fd1b71dab19bb8847e50", i23);
        _f_start_0020_0028_0029V = a13;
        fmVarArr[i23] = a13;
        int i24 = i23 + 1;
        C2491fm a14 = C4105zY.m41624a(NPCZone.class, "0c8fce40b675fa32239d247ed148a3ea", i24);
        _f_stop_0020_0028_0029V = a14;
        fmVarArr[i24] = a14;
        int i25 = i24 + 1;
        C2491fm a15 = C4105zY.m41624a(NPCZone.class, "863277e393e2234ad52d70923c28f12e", i25);
        f7526Lc = a15;
        fmVarArr[i25] = a15;
        int i26 = i25 + 1;
        C2491fm a16 = C4105zY.m41624a(NPCZone.class, "ddc641d1dd94015ad6751d57e4ba8826", i26);
        f7527Ld = a16;
        fmVarArr[i26] = a16;
        int i27 = i26 + 1;
        C2491fm a17 = C4105zY.m41624a(NPCZone.class, "017ff39d125301b4abe2346940384ecb", i27);
        f7528Le = a17;
        fmVarArr[i27] = a17;
        int i28 = i27 + 1;
        C2491fm a18 = C4105zY.m41624a(NPCZone.class, "010917f7481a611ae1087701e2adf90b", i28);
        f7529Lf = a18;
        fmVarArr[i28] = a18;
        int i29 = i28 + 1;
        C2491fm a19 = C4105zY.m41624a(NPCZone.class, "b52520e57fcb16c5a93be31758a253fe", i29);
        f7530Lg = a19;
        fmVarArr[i29] = a19;
        int i30 = i29 + 1;
        C2491fm a20 = C4105zY.m41624a(NPCZone.class, "e198a9d47fb2feb402affaa6382a11b3", i30);
        f7531Lh = a20;
        fmVarArr[i30] = a20;
        int i31 = i30 + 1;
        C2491fm a21 = C4105zY.m41624a(NPCZone.class, "db22078276f717ebd19ace7d3b3cda62", i31);
        f7532Li = a21;
        fmVarArr[i31] = a21;
        int i32 = i31 + 1;
        C2491fm a22 = C4105zY.m41624a(NPCZone.class, "28735c8ade91ca6b0f034ac7c4fb22d1", i32);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a22;
        fmVarArr[i32] = a22;
        int i33 = i32 + 1;
        C2491fm a23 = C4105zY.m41624a(NPCZone.class, "2217eea1ad218dd7c3250e0ea4802453", i33);
        f7533Lj = a23;
        fmVarArr[i33] = a23;
        int i34 = i33 + 1;
        C2491fm a24 = C4105zY.m41624a(NPCZone.class, "e1bbe497c525919290e620dc202c9a96", i34);
        f7534Lk = a24;
        fmVarArr[i34] = a24;
        int i35 = i34 + 1;
        C2491fm a25 = C4105zY.m41624a(NPCZone.class, "2823de5a31441be7b2a775363cfa430c", i35);
        _f_onResurrect_0020_0028_0029V = a25;
        fmVarArr[i35] = a25;
        int i36 = i35 + 1;
        C2491fm a26 = C4105zY.m41624a(NPCZone.class, "12db3621aa27dfb603b90cf377fb6362", i36);
        f7535Ll = a26;
        fmVarArr[i36] = a26;
        int i37 = i36 + 1;
        C2491fm a27 = C4105zY.m41624a(NPCZone.class, "cec901d119610c2dee3414b8231991c6", i37);
        _f_tick_0020_0028F_0029V = a27;
        fmVarArr[i37] = a27;
        int i38 = i37 + 1;
        C2491fm a28 = C4105zY.m41624a(NPCZone.class, "d2d41a517438669d60ccb2dfac24d4f7", i38);
        f7496Do = a28;
        fmVarArr[i38] = a28;
        int i39 = i38 + 1;
        C2491fm a29 = C4105zY.m41624a(NPCZone.class, "9907f3154df856df473431637c13a672", i39);
        f7536Lm = a29;
        fmVarArr[i39] = a29;
        int i40 = i39 + 1;
        C2491fm a30 = C4105zY.m41624a(NPCZone.class, "af20d7605cd75eacd15896e5d4d2d01d", i40);
        f7537x13860637 = a30;
        fmVarArr[i40] = a30;
        int i41 = i40 + 1;
        C2491fm a31 = C4105zY.m41624a(NPCZone.class, "adc3c09a3c8ab50b76d3118ea7c3b553", i41);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a31;
        fmVarArr[i41] = a31;
        int i42 = i41 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpaceZone._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCZone.class, C3726uF.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m31539F(boolean z) {
        bFf().mo5608dq().mo3153a(f7502KF, z);
    }

    /* renamed from: G */
    private void m31540G(boolean z) {
        bFf().mo5608dq().mo3153a(f7504KH, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reacts Only to Players")
    @C0064Am(aul = "79a4210ed3f80e95e49e09a58ea4c25e", aum = 0)
    @C5566aOg
    /* renamed from: H */
    private void m31541H(boolean z) {
        throw new aWi(new aCE(this, f7520KX, new Object[]{new Boolean(z)}));
    }

    /* renamed from: R */
    private void m31542R(float f) {
        bFf().mo5608dq().mo3150a(f7510KN, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creation Radius")
    @C0064Am(aul = "5a8cb7c543ea4f92067713e7a8759a58", aum = 0)
    @C5566aOg
    /* renamed from: S */
    private void m31543S(float f) {
        throw new aWi(new aCE(this, f7516KT, new Object[]{new Float(f)}));
    }

    @C0064Am(aul = "010917f7481a611ae1087701e2adf90b", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31547a(NPC auf) {
        throw new aWi(new aCE(this, f7529Lf, new Object[]{auf}));
    }

    /* renamed from: a */
    private void m31548a(PopulationControl aks) {
        bFf().mo5608dq().mo3197f(f7497KA, aks);
    }

    @C0064Am(aul = "2217eea1ad218dd7c3250e0ea4802453", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31549a(Pawn avi, Actor cr) {
        throw new aWi(new aCE(this, f7533Lj, new Object[]{avi, cr}));
    }

    /* renamed from: a */
    private void m31551a(C2505a aVar) {
        bFf().mo5608dq().mo3197f(f7512KP, aVar);
    }

    /* renamed from: a */
    private void m31552a(PopulationBehaviour oOVar) {
        bFf().mo5608dq().mo3197f(f7499KC, oOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone category")
    @C0064Am(aul = "0a67ce6e3186edf0a1eebfbbd2d35d82", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m31557b(C2505a aVar) {
        throw new aWi(new aCE(this, f7518KV, new Object[]{aVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Population Behaviour")
    @C0064Am(aul = "fd1a915a650a6c56e42d362302cbabc3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m31558b(PopulationBehaviour oOVar) {
        throw new aWi(new aCE(this, f7525Lb, new Object[]{oOVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Population Control")
    /* renamed from: c */
    private void m31559c(PopulationControl aks) {
        switch (bFf().mo6893i(f7522KZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7522KZ, new Object[]{aks}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7522KZ, new Object[]{aks}));
                break;
        }
        m31556b(aks);
    }

    /* renamed from: f */
    private void m31560f(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(f7508KL, vec3f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creation Offset")
    @C0064Am(aul = "39580b5339ba8db1ea70a34396bd82cf", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m31561g(Vec3f vec3f) {
        throw new aWi(new aCE(this, f7514KR, new Object[]{vec3f}));
    }

    /* renamed from: j */
    private void m31562j(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(f7501KE, iZVar);
    }

    @C0064Am(aul = "ddc641d1dd94015ad6751d57e4ba8826", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m31563n(Actor cr) {
        throw new aWi(new aCE(this, f7527Ld, new Object[]{cr}));
    }

    @C0064Am(aul = "017ff39d125301b4abe2346940384ecb", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m31564p(Actor cr) {
        throw new aWi(new aCE(this, f7528Le, new Object[]{cr}));
    }

    /* renamed from: q */
    private void m31565q(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: qA */
    private C2505a m31566qA() {
        return (C2505a) bFf().mo5608dq().mo3214p(f7512KP);
    }

    @C0064Am(aul = "f306bb57f289fd1b71dab19bb8847e50", aum = 0)
    @C5566aOg
    /* renamed from: qN */
    private void m31573qN() {
        throw new aWi(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "0c8fce40b675fa32239d247ed148a3ea", aum = 0)
    @C5566aOg
    /* renamed from: qO */
    private void m31574qO() {
        throw new aWi(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "b52520e57fcb16c5a93be31758a253fe", aum = 0)
    @C5566aOg
    /* renamed from: qQ */
    private List<NPC> m31576qQ() {
        throw new aWi(new aCE(this, f7530Lg, new Object[0]));
    }

    @C0064Am(aul = "9907f3154df856df473431637c13a672", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m31578qU() {
        throw new aWi(new aCE(this, f7536Lm, new Object[0]));
    }

    @C0064Am(aul = "adc3c09a3c8ab50b76d3118ea7c3b553", aum = 0)
    /* renamed from: qW */
    private Object m31579qW() {
        return mo18876qT();
    }

    /* renamed from: qs */
    private PopulationControl m31580qs() {
        return (PopulationControl) bFf().mo5608dq().mo3214p(f7497KA);
    }

    /* renamed from: qt */
    private PopulationBehaviour m31581qt() {
        return (PopulationBehaviour) bFf().mo5608dq().mo3214p(f7499KC);
    }

    /* renamed from: qu */
    private C2686iZ m31582qu() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(f7501KE);
    }

    /* renamed from: qv */
    private boolean m31583qv() {
        return bFf().mo5608dq().mo3201h(f7502KF);
    }

    /* renamed from: qw */
    private boolean m31584qw() {
        return bFf().mo5608dq().mo3201h(f7504KH);
    }

    /* renamed from: qx */
    private Asset m31585qx() {
        return ((NPCZoneType) getType()).aVF();
    }

    /* renamed from: qy */
    private Vec3f m31586qy() {
        return (Vec3f) bFf().mo5608dq().mo3214p(f7508KL);
    }

    /* renamed from: qz */
    private float m31587qz() {
        return bFf().mo5608dq().mo3211m(f7510KN);
    }

    /* renamed from: s */
    private void m31589s(Actor cr) {
        switch (bFf().mo6893i(f7531Lh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7531Lh, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7531Lh, new Object[]{cr}));
                break;
        }
        m31588r(cr);
    }

    /* renamed from: u */
    private void m31591u(Actor cr) {
        switch (bFf().mo6893i(f7532Li)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7532Li, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7532Li, new Object[]{cr}));
                break;
        }
        m31590t(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reacts Only to Players")
    @C5566aOg
    /* renamed from: I */
    public void mo18862I(boolean z) {
        switch (bFf().mo6893i(f7520KX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7520KX, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7520KX, new Object[]{new Boolean(z)}));
                break;
        }
        m31541H(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creation Radius")
    @C5566aOg
    /* renamed from: T */
    public void mo18863T(float f) {
        switch (bFf().mo6893i(f7516KT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7516KT, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7516KT, new Object[]{new Float(f)}));
                break;
        }
        m31543S(f);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m31544U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3726uF(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpaceZone._m_methodCount) {
            case 0:
                return m31567qB();
            case 1:
                m31561g((Vec3f) args[0]);
                return null;
            case 2:
                return new Float(m31568qD());
            case 3:
                m31543S(((Float) args[0]).floatValue());
                return null;
            case 4:
                return m31569qF();
            case 5:
                m31557b((C2505a) args[0]);
                return null;
            case 6:
                return new Boolean(m31570qH());
            case 7:
                m31541H(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                return m31571qJ();
            case 9:
                m31556b((PopulationControl) args[0]);
                return null;
            case 10:
                return m31572qL();
            case 11:
                m31558b((PopulationBehaviour) args[0]);
                return null;
            case 12:
                m31573qN();
                return null;
            case 13:
                m31574qO();
                return null;
            case 14:
                return new Boolean(m31575qP());
            case 15:
                m31563n((Actor) args[0]);
                return null;
            case 16:
                m31564p((Actor) args[0]);
                return null;
            case 17:
                m31547a((NPC) args[0]);
                return null;
            case 18:
                return m31576qQ();
            case 19:
                m31588r((Actor) args[0]);
                return null;
            case 20:
                m31590t((Actor) args[0]);
                return null;
            case 21:
                return m31554au();
            case 22:
                m31549a((Pawn) args[0], (Actor) args[1]);
                return null;
            case 23:
                return m31577qS();
            case 24:
                m31553aG();
                return null;
            case 25:
                m31550a((Ship) args[0], (Actor) args[1]);
                return null;
            case 26:
                m31544U(((Float) args[0]).floatValue());
                return null;
            case 27:
                m31546a((C0665JT) args[0]);
                return null;
            case 28:
                m31578qU();
                return null;
            case 29:
                m31555b((aDJ) args[0]);
                return null;
            case 30:
                return m31579qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m31553aG();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f7496Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7496Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7496Do, new Object[]{jt}));
                break;
        }
        m31546a(jt);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18865b(NPC auf) {
        switch (bFf().mo6893i(f7529Lf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7529Lf, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7529Lf, new Object[]{auf}));
                break;
        }
        m31547a(auf);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo2353b(Pawn avi, Actor cr) {
        switch (bFf().mo6893i(f7533Lj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7533Lj, new Object[]{avi, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7533Lj, new Object[]{avi, cr}));
                break;
        }
        m31549a(avi, cr);
    }

    /* renamed from: b */
    public void mo7563b(Ship fAVar, Actor cr) {
        switch (bFf().mo6893i(f7535Ll)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7535Ll, new Object[]{fAVar, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7535Ll, new Object[]{fAVar, cr}));
                break;
        }
        m31550a(fAVar, cr);
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f7537x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7537x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7537x13860637, new Object[]{adj}));
                break;
        }
        m31555b(adj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone category")
    @C5566aOg
    /* renamed from: c */
    public void mo18866c(C2505a aVar) {
        switch (bFf().mo6893i(f7518KV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7518KV, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7518KV, new Object[]{aVar}));
                break;
        }
        m31557b(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Population Behaviour")
    @C5566aOg
    /* renamed from: c */
    public void mo18867c(PopulationBehaviour oOVar) {
        switch (bFf().mo6893i(f7525Lb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7525Lb, new Object[]{oOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7525Lb, new Object[]{oOVar}));
                break;
        }
        m31558b(oOVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m31579qW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Creation Offset")
    @C5566aOg
    /* renamed from: h */
    public void mo18868h(Vec3f vec3f) {
        switch (bFf().mo6893i(f7514KR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7514KR, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7514KR, new Object[]{vec3f}));
                break;
        }
        m31561g(vec3f);
    }

    public boolean isActive() {
        switch (bFf().mo6893i(f7526Lc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7526Lc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7526Lc, new Object[0]));
                break;
        }
        return m31575qP();
    }

    @C5566aOg
    /* renamed from: o */
    public void mo17476o(Actor cr) {
        switch (bFf().mo6893i(f7527Ld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7527Ld, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7527Ld, new Object[]{cr}));
                break;
        }
        m31563n(cr);
    }

    @C5566aOg
    /* renamed from: q */
    public void mo17477q(Actor cr) {
        switch (bFf().mo6893i(f7528Le)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7528Le, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7528Le, new Object[]{cr}));
                break;
        }
        m31564p(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creation Offset")
    /* renamed from: qC */
    public Vec3f mo18869qC() {
        switch (bFf().mo6893i(f7513KQ)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, f7513KQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7513KQ, new Object[0]));
                break;
        }
        return m31567qB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creation Radius")
    /* renamed from: qE */
    public float mo18870qE() {
        switch (bFf().mo6893i(f7515KS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7515KS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7515KS, new Object[0]));
                break;
        }
        return m31568qD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone category")
    /* renamed from: qG */
    public C2505a mo18871qG() {
        switch (bFf().mo6893i(f7517KU)) {
            case 0:
                return null;
            case 2:
                return (C2505a) bFf().mo5606d(new aCE(this, f7517KU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7517KU, new Object[0]));
                break;
        }
        return m31569qF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reacts Only to Players")
    /* renamed from: qI */
    public boolean mo18872qI() {
        switch (bFf().mo6893i(f7519KW)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7519KW, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7519KW, new Object[0]));
                break;
        }
        return m31570qH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Population Control")
    /* renamed from: qK */
    public PopulationControl mo18873qK() {
        switch (bFf().mo6893i(f7521KY)) {
            case 0:
                return null;
            case 2:
                return (PopulationControl) bFf().mo5606d(new aCE(this, f7521KY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7521KY, new Object[0]));
                break;
        }
        return m31571qJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Population Behaviour")
    /* renamed from: qM */
    public PopulationBehaviour mo18874qM() {
        switch (bFf().mo6893i(f7524La)) {
            case 0:
                return null;
            case 2:
                return (PopulationBehaviour) bFf().mo5606d(new aCE(this, f7524La, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7524La, new Object[0]));
                break;
        }
        return m31572qL();
    }

    @C5566aOg
    /* renamed from: qR */
    public List<NPC> mo18875qR() {
        switch (bFf().mo6893i(f7530Lg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f7530Lg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7530Lg, new Object[0]));
                break;
        }
        return m31576qQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    /* renamed from: qT */
    public NPCZoneType mo18876qT() {
        switch (bFf().mo6893i(f7534Lk)) {
            case 0:
                return null;
            case 2:
                return (NPCZoneType) bFf().mo5606d(new aCE(this, f7534Lk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7534Lk, new Object[0]));
                break;
        }
        return m31577qS();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f7536Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7536Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7536Lm, new Object[0]));
                break;
        }
        m31578qU();
    }

    @C5566aOg
    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m31573qN();
    }

    @C5566aOg
    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m31574qO();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m31554au();
    }

    /* renamed from: a */
    public void mo18864a(NPCZoneType hu) {
        super.mo967a((C2961mJ) hu);
        if (m31580qs() != null) {
            m31580qs().mo8670c(this);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creation Offset")
    @C0064Am(aul = "62cb003fd2cc32ea33bc475f13122bc8", aum = 0)
    /* renamed from: qB */
    private Vec3f m31567qB() {
        return m31586qy();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Creation Radius")
    @C0064Am(aul = "6aa398b2a9af3c0eef81ddc33c5e6b49", aum = 0)
    /* renamed from: qD */
    private float m31568qD() {
        return m31587qz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone category")
    @C0064Am(aul = "e1bb2fa61b68300f5a9b8b7d5d5acb2a", aum = 0)
    /* renamed from: qF */
    private C2505a m31569qF() {
        return m31566qA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reacts Only to Players")
    @C0064Am(aul = "aac7abbdb87cee8fa10433d4f2924420", aum = 0)
    /* renamed from: qH */
    private boolean m31570qH() {
        return m31584qw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Population Control")
    @C0064Am(aul = "415492ecbb8a9bfc2ce8a2123657b42c", aum = 0)
    /* renamed from: qJ */
    private PopulationControl m31571qJ() {
        return m31580qs();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Population Control")
    @C0064Am(aul = "0d48db76818daea9666d999d751f023f", aum = 0)
    /* renamed from: b */
    private void m31556b(PopulationControl aks) {
        m31548a(aks);
        if (m31580qs() != null) {
            m31580qs().mo8670c(this);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Population Behaviour")
    @C0064Am(aul = "b41f7446edebcb8044df2a01b7079cdc", aum = 0)
    /* renamed from: qL */
    private PopulationBehaviour m31572qL() {
        return m31581qt();
    }

    @C0064Am(aul = "863277e393e2234ad52d70923c28f12e", aum = 0)
    /* renamed from: qP */
    private boolean m31575qP() {
        return m31583qv();
    }

    @C0064Am(aul = "e198a9d47fb2feb402affaa6382a11b3", aum = 0)
    /* renamed from: r */
    private void m31588r(Actor cr) {
        Controller hb = ((Ship) cr).mo2998hb();
        if ((hb instanceof PlayerController) && m31585qx().getHandle() != null && m31585qx().getFile() != null) {
            ((PlayerController) hb).mo22058aJ(m31585qx().getFile(), m31585qx().getHandle());
        }
    }

    @C0064Am(aul = "db22078276f717ebd19ace7d3b3cda62", aum = 0)
    /* renamed from: t */
    private void m31590t(Actor cr) {
        boolean z = ((Ship) cr).mo2998hb() instanceof PlayerController;
    }

    @C0064Am(aul = "28735c8ade91ca6b0f034ac7c4fb22d1", aum = 0)
    /* renamed from: au */
    private String m31554au() {
        return "NPCZone: [" + getName() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "e1bbe497c525919290e620dc202c9a96", aum = 0)
    /* renamed from: qS */
    private NPCZoneType m31577qS() {
        return (NPCZoneType) super.getType();
    }

    @C0064Am(aul = "2823de5a31441be7b2a775363cfa430c", aum = 0)
    /* renamed from: aG */
    private void m31553aG() {
        super.mo70aH();
        if (m31580qs() != null) {
            m31580qs().mo8670c(this);
        }
        if (m31581qt() != null) {
            m31581qt().mo8670c(this);
        }
        if (!C5877acF.RUNNING_CLIENT_BOT) {
            return;
        }
        if (!(mo18873qK() instanceof BossNestPopulationControl)) {
            if (!isActive()) {
                System.out.println("[BOTS] Will start event " + getName());
                mo8361ms((float) ((Math.random() * 5.0d) + 5.0d));
            }
        } else if (isActive()) {
            System.out.println("[BOTS] Will stop event " + getName());
            mo8361ms((float) ((Math.random() * 5.0d) + 5.0d));
        }
    }

    @C0064Am(aul = "12db3621aa27dfb603b90cf377fb6362", aum = 0)
    /* renamed from: a */
    private void m31550a(Ship fAVar, Actor cr) {
        if (cr instanceof Ship) {
            Character agj = fAVar.agj();
            Character agj2 = ((Ship) cr).agj();
            if (agj != null && agj2 != null) {
                if (m31580qs() != null) {
                    m31580qs().mo2952b(agj, agj2);
                }
                if (m31581qt() != null) {
                    m31581qt().mo2952b(agj, agj2);
                }
            }
        }
    }

    @C0064Am(aul = "cec901d119610c2dee3414b8231991c6", aum = 0)
    /* renamed from: U */
    private void m31544U(float f) {
        super.mo4709V(f);
        if (!C5877acF.RUNNING_CLIENT_BOT) {
            return;
        }
        if (!(mo18873qK() instanceof BossNestPopulationControl)) {
            setActive(true);
        } else {
            setActive(false);
        }
    }

    @C0064Am(aul = "d2d41a517438669d60ccb2dfac24d4f7", aum = 0)
    /* renamed from: a */
    private void m31546a(C0665JT jt) {
        Object obj;
        if ("".equals(jt.getVersion())) {
            mo8360mc("stopping NPCZone " + getName());
            stop();
        }
        if (jt.mo3117j(1, 2, 1)) {
            super.mo24b(jt);
        }
        if (jt.mo3117j(1, 2, 2)) {
            if (((Boolean) jt.get("isEvent")).booleanValue()) {
                m31551a(C2505a.EVENT);
            } else {
                m31551a(C2505a.CONFLICT);
            }
        }
        if (jt.mo3117j(1, 2, 3) && (obj = jt.get("shape")) != null && (obj instanceof C6348alI)) {
            C6348alI ali = (C6348alI) obj;
            mo8359ma("Porting Zone. Radius: " + ali.getRadius());
            mo20320aW(ali.getRadius());
        }
    }

    @C0064Am(aul = "af20d7605cd75eacd15896e5d4d2d01d", aum = 0)
    /* renamed from: b */
    private void m31555b(aDJ adj) {
        if (m31582qu().contains(adj)) {
            Character acx = (Character) adj;
            Ship bQx = acx.bQx();
            if (bQx != null) {
                bQx.mo8355h(C0471GX.C0472a.class, this);
            }
            acx.mo8355h(C3161oY.C3162a.class, this);
            m31582qu().remove(acx);
            if (m31580qs() != null) {
                m31580qs().mo2943H(acx);
            }
            if (m31581qt() != null) {
                m31581qt().mo2943H(acx);
            }
        }
    }

    /* renamed from: a.fw$a */
    public enum C2505a {
        CONFLICT,
        BOSS,
        EVENT,
        MISSION,
        MESSAGE,
        DEFENCE,
        CONSTRUCTION
    }
}
