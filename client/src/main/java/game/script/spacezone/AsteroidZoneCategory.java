package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C1413Ui;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.nW */
/* compiled from: a */
public class AsteroidZoneCategory extends TaikodomObject implements C0468GU, C1616Xf, C4068yr {

    /* renamed from: MN */
    public static final C2491fm f8718MN = null;

    /* renamed from: NY */
    public static final C5663aRz f8719NY = null;

    /* renamed from: Ob */
    public static final C2491fm f8720Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f8721Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f8722QA = null;

    /* renamed from: QD */
    public static final C2491fm f8723QD = null;

    /* renamed from: QE */
    public static final C2491fm f8724QE = null;

    /* renamed from: QF */
    public static final C2491fm f8725QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f8726Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMt = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    /* renamed from: bL */
    public static final C5663aRz f8728bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8729bM = null;
    /* renamed from: bN */
    public static final C2491fm f8730bN = null;
    /* renamed from: bO */
    public static final C2491fm f8731bO = null;
    /* renamed from: bP */
    public static final C2491fm f8732bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8733bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ffa173a37c2692733272adbec436b48f", aum = 8)

    /* renamed from: bK */
    private static UUID f8727bK;
    @C0064Am(aul = "2438b99e9e79960d342305c7949e4794", aum = 0)
    private static String handle;
    @C0064Am(aul = "85cdd11e392eb8e7d31b3d6fd9edbb71", aum = 5)

    /* renamed from: jS */
    private static DatabaseCategory f8734jS;
    @C0064Am(aul = "bc04854cb7808cac5b1c94c8164aa1c4", aum = 6)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f8735jT;
    @C0064Am(aul = "129275444d262df156492ca5b22838b4", aum = 2)

    /* renamed from: jU */
    private static Asset f8736jU;
    @C0064Am(aul = "065b7bbac39e271d42c4a1879072d44e", aum = 7)

    /* renamed from: jV */
    private static float f8737jV;
    @C0064Am(aul = "8f33df8f37e35a06009e2841192842d6", aum = 1)

    /* renamed from: jn */
    private static Asset f8738jn;
    @C0064Am(aul = "b3267866ac9b36a17a46cee98828fc3c", aum = 4)

    /* renamed from: nh */
    private static I18NString f8739nh;
    @C0064Am(aul = "296688f3bfb30cc1f295f6dc5fd223a8", aum = 3)

    /* renamed from: ni */
    private static I18NString f8740ni;

    static {
        m36154V();
    }

    public AsteroidZoneCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidZoneCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36154V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 9;
        _m_methodCount = TaikodomObject._m_methodCount + 23;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(AsteroidZoneCategory.class, "2438b99e9e79960d342305c7949e4794", i);
        f8729bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AsteroidZoneCategory.class, "8f33df8f37e35a06009e2841192842d6", i2);
        f8719NY = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AsteroidZoneCategory.class, "129275444d262df156492ca5b22838b4", i3);
        aMg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AsteroidZoneCategory.class, "296688f3bfb30cc1f295f6dc5fd223a8", i4);
        f8726Qz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AsteroidZoneCategory.class, "b3267866ac9b36a17a46cee98828fc3c", i5);
        f8722QA = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AsteroidZoneCategory.class, "85cdd11e392eb8e7d31b3d6fd9edbb71", i6);
        aMh = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AsteroidZoneCategory.class, "bc04854cb7808cac5b1c94c8164aa1c4", i7);
        awd = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(AsteroidZoneCategory.class, "065b7bbac39e271d42c4a1879072d44e", i8);
        aMi = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(AsteroidZoneCategory.class, "ffa173a37c2692733272adbec436b48f", i9);
        f8728bL = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i11 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 23)];
        C2491fm a = C4105zY.m41624a(AsteroidZoneCategory.class, "3bd501e62e6e86a81bf8aee73bcd507a", i11);
        f8730bN = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidZoneCategory.class, "3bba12d4f381f1832ee72ad74c6a1622", i12);
        f8731bO = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(AsteroidZoneCategory.class, "4efb8439bac4c5c50e8e7081f7766783", i13);
        f8732bP = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(AsteroidZoneCategory.class, "2b5108e379afe2b5e998284b843b8d1a", i14);
        f8733bQ = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(AsteroidZoneCategory.class, "4cfcb3842cb0a95832683bc4abe42a7f", i15);
        f8718MN = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(AsteroidZoneCategory.class, "00644aefe56044d9297601a984c3b9dd", i16);
        f8723QD = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(AsteroidZoneCategory.class, "09a35e75abb3bb82521f30d5b8af9fce", i17);
        f8724QE = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(AsteroidZoneCategory.class, "17bd53a080bdedc84664c93c7b3430b9", i18);
        f8725QF = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(AsteroidZoneCategory.class, "1ad522b3744d286d4bcce4651b202e56", i19);
        aMj = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(AsteroidZoneCategory.class, "dbbef5b852f4ab0228a0e586afa4ce10", i20);
        aMk = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(AsteroidZoneCategory.class, "d424fdbfd0def919149a43487d6b9247", i21);
        aMl = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(AsteroidZoneCategory.class, "166e4fdd15462c408e16ec4969d06d63", i22);
        aMm = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        C2491fm a13 = C4105zY.m41624a(AsteroidZoneCategory.class, "438a1ad720b02b5a2db2cfe242079479", i23);
        aMn = a13;
        fmVarArr[i23] = a13;
        int i24 = i23 + 1;
        C2491fm a14 = C4105zY.m41624a(AsteroidZoneCategory.class, "07baa4c4b9179a8186c0b62602703ec2", i24);
        aMo = a14;
        fmVarArr[i24] = a14;
        int i25 = i24 + 1;
        C2491fm a15 = C4105zY.m41624a(AsteroidZoneCategory.class, "e59b07e70c890957d1956890ce0fc2b1", i25);
        aMp = a15;
        fmVarArr[i25] = a15;
        int i26 = i25 + 1;
        C2491fm a16 = C4105zY.m41624a(AsteroidZoneCategory.class, "3ddc70dda92293eb0ea0175060bab4f4", i26);
        f8721Oc = a16;
        fmVarArr[i26] = a16;
        int i27 = i26 + 1;
        C2491fm a17 = C4105zY.m41624a(AsteroidZoneCategory.class, "e83f86e4952cda0e32d083b1fa871358", i27);
        f8720Ob = a17;
        fmVarArr[i27] = a17;
        int i28 = i27 + 1;
        C2491fm a18 = C4105zY.m41624a(AsteroidZoneCategory.class, "ee64fda7c11b253bf852a14a142f5c0f", i28);
        aMq = a18;
        fmVarArr[i28] = a18;
        int i29 = i28 + 1;
        C2491fm a19 = C4105zY.m41624a(AsteroidZoneCategory.class, "15dce6febf06f7b644d79035b2492ac5", i29);
        aMr = a19;
        fmVarArr[i29] = a19;
        int i30 = i29 + 1;
        C2491fm a20 = C4105zY.m41624a(AsteroidZoneCategory.class, "a79c1df9f6dc7a54077cdb7626b2797d", i30);
        aMs = a20;
        fmVarArr[i30] = a20;
        int i31 = i30 + 1;
        C2491fm a21 = C4105zY.m41624a(AsteroidZoneCategory.class, "eb9affa5ca6427b770bab5bbc5283911", i31);
        aMt = a21;
        fmVarArr[i31] = a21;
        int i32 = i31 + 1;
        C2491fm a22 = C4105zY.m41624a(AsteroidZoneCategory.class, "63f3a489952ac84a24bb9eb743e25059", i32);
        aMu = a22;
        fmVarArr[i32] = a22;
        int i33 = i32 + 1;
        C2491fm a23 = C4105zY.m41624a(AsteroidZoneCategory.class, "7c950c37fa090b99a2edd616bbee26f6", i33);
        aMv = a23;
        fmVarArr[i33] = a23;
        int i34 = i33 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidZoneCategory.class, C1413Ui.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m36140L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m36141Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "a79c1df9f6dc7a54077cdb7626b2797d", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m36143M(Asset tCVar) {
        throw new aWi(new aCE(this, aMs, new Object[]{tCVar}));
    }

    /* renamed from: RQ */
    private Asset m36144RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m36145RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m36146RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    /* renamed from: a */
    private void m36155a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    /* renamed from: a */
    private void m36157a(String str) {
        bFf().mo5608dq().mo3197f(f8729bM, str);
    }

    /* renamed from: a */
    private void m36158a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8728bL, uuid);
    }

    /* renamed from: an */
    private UUID m36159an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8728bL);
    }

    /* renamed from: ao */
    private String m36160ao() {
        return (String) bFf().mo5608dq().mo3214p(f8729bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "07baa4c4b9179a8186c0b62602703ec2", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m36163b(DatabaseCategory aik) {
        throw new aWi(new aCE(this, aMo, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "2b5108e379afe2b5e998284b843b8d1a", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m36164b(String str) {
        throw new aWi(new aCE(this, f8733bQ, new Object[]{str}));
    }

    /* renamed from: bq */
    private void m36166bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f8726Qz, i18NString);
    }

    /* renamed from: br */
    private void m36167br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f8722QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "00644aefe56044d9297601a984c3b9dd", aum = 0)
    @C5566aOg
    /* renamed from: bs */
    private void m36168bs(I18NString i18NString) {
        throw new aWi(new aCE(this, f8723QD, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "17bd53a080bdedc84664c93c7b3430b9", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m36169bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f8725QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m36171c(UUID uuid) {
        switch (bFf().mo6893i(f8731bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8731bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8731bO, new Object[]{uuid}));
                break;
        }
        m36165b(uuid);
    }

    /* renamed from: cx */
    private void m36172cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: sG */
    private Asset m36175sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f8719NY);
    }

    /* renamed from: t */
    private void m36177t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f8719NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "3ddc70dda92293eb0ea0175060bab4f4", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m36178u(Asset tCVar) {
        throw new aWi(new aCE(this, f8721Oc, new Object[]{tCVar}));
    }

    /* renamed from: vS */
    private I18NString m36179vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f8726Qz);
    }

    /* renamed from: vT */
    private I18NString m36180vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f8722QA);
    }

    /* renamed from: x */
    private void m36182x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    @aFW(cZD = true, value = "Extraction itens")
    /* renamed from: N */
    public Set<aDJ> mo20756N(Player aku) {
        switch (bFf().mo6893i(aMt)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, aMt, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, aMt, new Object[]{aku}));
                break;
        }
        return m36142M(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C5566aOg
    /* renamed from: N */
    public void mo20757N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m36143M(tCVar);
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m36147RT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m36148RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m36149RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m36150RZ();
    }

    /* renamed from: Sc */
    public String mo20758Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m36151Sb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m36152Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m36153Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1413Ui(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m36161ap();
            case 1:
                m36165b((UUID) args[0]);
                return null;
            case 2:
                return m36162ar();
            case 3:
                m36164b((String) args[0]);
                return null;
            case 4:
                return m36174rO();
            case 5:
                m36168bs((I18NString) args[0]);
                return null;
            case 6:
                return m36181vV();
            case 7:
                m36169bu((I18NString) args[0]);
                return null;
            case 8:
                return m36147RT();
            case 9:
                return m36148RV();
            case 10:
                m36156a((TaikopediaEntry) args[0]);
                return null;
            case 11:
                m36170c((TaikopediaEntry) args[0]);
                return null;
            case 12:
                return m36149RX();
            case 13:
                m36163b((DatabaseCategory) args[0]);
                return null;
            case 14:
                return m36150RZ();
            case 15:
                m36178u((Asset) args[0]);
                return null;
            case 16:
                return m36176sJ();
            case 17:
                return m36151Sb();
            case 18:
                return m36152Sd();
            case 19:
                m36143M((Asset) args[0]);
                return null;
            case 20:
                return m36142M((Player) args[0]);
            case 21:
                return new Float(m36153Sf());
            case 22:
                m36173cy(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8730bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8730bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8730bN, new Object[0]));
                break;
        }
        return m36161ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    /* renamed from: b */
    public void mo20759b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m36156a(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C5566aOg
    /* renamed from: bt */
    public void mo20760bt(I18NString i18NString) {
        switch (bFf().mo6893i(f8723QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8723QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8723QD, new Object[]{i18NString}));
                break;
        }
        m36168bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo20761bv(I18NString i18NString) {
        switch (bFf().mo6893i(f8725QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8725QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8725QF, new Object[]{i18NString}));
                break;
        }
        m36169bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C5566aOg
    /* renamed from: c */
    public void mo20762c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m36163b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    /* renamed from: cz */
    public void mo20763cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m36173cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    /* renamed from: d */
    public void mo20764d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m36170c(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8732bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8732bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8732bP, new Object[0]));
                break;
        }
        return m36162ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8733bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8733bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8733bQ, new Object[]{str}));
                break;
        }
        m36164b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f8718MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8718MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8718MN, new Object[0]));
                break;
        }
        return m36174rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo20765sK() {
        switch (bFf().mo6893i(f8720Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f8720Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8720Ob, new Object[0]));
                break;
        }
        return m36176sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo20767v(Asset tCVar) {
        switch (bFf().mo6893i(f8721Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8721Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8721Oc, new Object[]{tCVar}));
                break;
        }
        m36178u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo20768vW() {
        switch (bFf().mo6893i(f8724QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8724QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8724QE, new Object[0]));
                break;
        }
        return m36181vV();
    }

    @C0064Am(aul = "3bd501e62e6e86a81bf8aee73bcd507a", aum = 0)
    /* renamed from: ap */
    private UUID m36161ap() {
        return m36159an();
    }

    @C0064Am(aul = "3bba12d4f381f1832ee72ad74c6a1622", aum = 0)
    /* renamed from: b */
    private void m36165b(UUID uuid) {
        m36158a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m36158a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "4efb8439bac4c5c50e8e7081f7766783", aum = 0)
    /* renamed from: ar */
    private String m36162ar() {
        return m36160ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "4cfcb3842cb0a95832683bc4abe42a7f", aum = 0)
    /* renamed from: rO */
    private I18NString m36174rO() {
        return m36179vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "09a35e75abb3bb82521f30d5b8af9fce", aum = 0)
    /* renamed from: vV */
    private I18NString m36181vV() {
        return m36180vT();
    }

    @C0064Am(aul = "1ad522b3744d286d4bcce4651b202e56", aum = 0)
    /* renamed from: RT */
    private I18NString m36147RT() {
        return mo20768vW();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "dbbef5b852f4ab0228a0e586afa4ce10", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m36148RV() {
        return Collections.unmodifiableList(m36141Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "d424fdbfd0def919149a43487d6b9247", aum = 0)
    /* renamed from: a */
    private void m36156a(TaikopediaEntry aiz) {
        m36141Ll().add(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "166e4fdd15462c408e16ec4969d06d63", aum = 0)
    /* renamed from: c */
    private void m36170c(TaikopediaEntry aiz) {
        m36141Ll().remove(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "438a1ad720b02b5a2db2cfe242079479", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m36149RX() {
        return m36145RR();
    }

    @C0064Am(aul = "e59b07e70c890957d1956890ce0fc2b1", aum = 0)
    /* renamed from: RZ */
    private String m36150RZ() {
        if (m36175sG() != null) {
            return m36175sG().getHandle();
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "e83f86e4952cda0e32d083b1fa871358", aum = 0)
    /* renamed from: sJ */
    private Asset m36176sJ() {
        return m36175sG();
    }

    @C0064Am(aul = "ee64fda7c11b253bf852a14a142f5c0f", aum = 0)
    /* renamed from: Sb */
    private String m36151Sb() {
        return m36144RQ().getHandle();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "15dce6febf06f7b644d79035b2492ac5", aum = 0)
    /* renamed from: Sd */
    private Asset m36152Sd() {
        return m36144RQ();
    }

    @aFW(cZD = true, value = "Extraction itens")
    @C0064Am(aul = "eb9affa5ca6427b770bab5bbc5283911", aum = 0)
    /* renamed from: M */
    private Set<aDJ> m36142M(Player aku) {
        return aku.dyl().mo11984g(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "63f3a489952ac84a24bb9eb743e25059", aum = 0)
    /* renamed from: Sf */
    private float m36153Sf() {
        return m36146RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "7c950c37fa090b99a2edd616bbee26f6", aum = 0)
    /* renamed from: cy */
    private void m36173cy(float f) {
        m36172cx(f);
    }
}
