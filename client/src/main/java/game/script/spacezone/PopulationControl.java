package game.script.spacezone;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.TaikodomObject;
import game.script.npc.NPC;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C2693ig;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@C2712iu(mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
@C6485anp
/* renamed from: a.akS  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class PopulationControl extends TaikodomObject implements C0468GU, C1616Xf, C5313aEn, C3763uj {

    /* renamed from: Lg */
    public static final C2491fm f4767Lg = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aQr = null;
    public static final C2491fm aQs = null;
    public static final C2491fm aQt = null;
    public static final C2491fm awz = null;
    /* renamed from: bL */
    public static final C5663aRz f4771bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4772bM = null;
    /* renamed from: bN */
    public static final C2491fm f4773bN = null;
    /* renamed from: bO */
    public static final C2491fm f4774bO = null;
    /* renamed from: bP */
    public static final C2491fm f4775bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4776bQ = null;
    public static final C2491fm dla = null;
    public static final C2491fm dlc = null;
    public static final C2491fm dld = null;
    public static final C2491fm dle = null;
    public static final C2491fm dlf = null;
    public static final C2491fm dlg = null;
    public static final C2491fm dlh = null;
    public static final C2491fm dlk = null;
    public static final C2491fm esf = null;
    public static final C5663aRz fUH = null;
    public static final C2491fm fUI = null;
    public static final C2491fm fUJ = null;
    public static final C2491fm fUK = null;
    public static final C2491fm fUL = null;
    public static final C2491fm fUM = null;
    public static final long serialVersionUID = 0;
    private static final String fUG = "respawnTime";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "70aef6509f6b7746bc438be6b2793ef8", aum = 0)

    /* renamed from: Xx */
    private static NPCZone f4768Xx = null;
    @C0064Am(aul = "bbbf7fe3a8a29bb493c36238952d79f1", aum = 1)

    /* renamed from: Xy */
    private static float f4769Xy = 0.0f;
    @C0064Am(aul = "58396f8d58b4166b7243dae52d54a1e2", aum = 2)

    /* renamed from: bK */
    private static UUID f4770bK = null;
    @C0064Am(aul = "d2fddb763263c03e962b10d3a1a078f1", aum = 3)
    private static String handle;

    static {
        m23191V();
    }

    private transient Random awe;

    public PopulationControl() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PopulationControl(C5540aNg ang) {
        super(ang);
    }

    public PopulationControl(PopulationControlType rsVar) {
        super((C5540aNg) null);
        super._m_script_init(rsVar);
    }

    /* renamed from: V */
    static void m23191V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 22;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(PopulationControl.class, "70aef6509f6b7746bc438be6b2793ef8", i);
        aQr = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PopulationControl.class, "bbbf7fe3a8a29bb493c36238952d79f1", i2);
        fUH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PopulationControl.class, "58396f8d58b4166b7243dae52d54a1e2", i3);
        f4771bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PopulationControl.class, "d2fddb763263c03e962b10d3a1a078f1", i4);
        f4772bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 22)];
        C2491fm a = C4105zY.m41624a(PopulationControl.class, "1ad19abc50d4b7e3b9fec65d5299a122", i6);
        f4773bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(PopulationControl.class, "4d90600d15245e9356067a138d6c5081", i7);
        f4774bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(PopulationControl.class, "706a15762ffcf9ccd9a2461465b5f775", i8);
        f4775bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(PopulationControl.class, "56ea65690d655675f23ff3e8573bac9d", i9);
        f4776bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(PopulationControl.class, "821c9e15b2d60c0071c7512617440f9e", i10);
        fUI = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(PopulationControl.class, "03013c9cf45f730290ee60a0133f55d4", i11);
        fUJ = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(PopulationControl.class, "b062ea35c6e2638541d1ab0838f59329", i12);
        f4767Lg = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(PopulationControl.class, "f9acfb17b5c1eae5b285b34668345cf0", i13);
        dla = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(PopulationControl.class, "b7eaff148b9dc5743453bdd2ae48c53f", i14);
        fUK = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(PopulationControl.class, "d4b83bc2b9b263b5ecf6dddddec07198", i15);
        dld = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(PopulationControl.class, "7eae00edfa9ba36dc90c6225b5afb55c", i16);
        dle = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(PopulationControl.class, "5b8b0cac871156282f984232ad4258c5", i17);
        dlf = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(PopulationControl.class, "fd93821136e1350392508517cd3becd9", i18);
        dlk = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(PopulationControl.class, "eb1df50afd5ed856f4fea00ce1edcd9a", i19);
        dlc = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(PopulationControl.class, "97410bb154a3914bf3b20b436a81180d", i20);
        dlg = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(PopulationControl.class, "610fde54f84f9c1ad21ed2624f9fb50a", i21);
        dlh = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(PopulationControl.class, "536f9b7ee2e26c5819e8081f79b12bbf", i22);
        awz = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(PopulationControl.class, "9b8869e5b871c047c409076f0a2471bd", i23);
        fUL = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        C2491fm a19 = C4105zY.m41624a(PopulationControl.class, "39d57e8dfeb7ea65865c8d731233c4de", i24);
        fUM = a19;
        fmVarArr[i24] = a19;
        int i25 = i24 + 1;
        C2491fm a20 = C4105zY.m41624a(PopulationControl.class, "2de4d8cf6d74333ec453ff397d52efd8", i25);
        aQs = a20;
        fmVarArr[i25] = a20;
        int i26 = i25 + 1;
        C2491fm a21 = C4105zY.m41624a(PopulationControl.class, "8d43f2d598f6165272707a260a8410da", i26);
        aQt = a21;
        fmVarArr[i26] = a21;
        int i27 = i26 + 1;
        C2491fm a22 = C4105zY.m41624a(PopulationControl.class, "7e976e7f669880489eb395a17d510f6c", i27);
        esf = a22;
        fmVarArr[i27] = a22;
        int i28 = i27 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PopulationControl.class, C2693ig.class, _m_fields, _m_methods);
    }

    /* renamed from: Uw */
    private NPCZone m23189Uw() {
        return (NPCZone) bFf().mo5608dq().mo3214p(aQr);
    }

    /* renamed from: a */
    private void m23193a(NPCZone fwVar) {
        bFf().mo5608dq().mo3197f(aQr, fwVar);
    }

    /* renamed from: a */
    private void m23194a(String str) {
        bFf().mo5608dq().mo3197f(f4772bM, str);
    }

    /* renamed from: a */
    private void m23195a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4771bL, uuid);
    }

    @C0064Am(aul = "f9acfb17b5c1eae5b285b34668345cf0", aum = 0)
    private void aZk() {
        throw new aWi(new aCE(this, dla, new Object[0]));
    }

    /* renamed from: an */
    private UUID m23196an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4771bL);
    }

    /* renamed from: ao */
    private String m23197ao() {
        return (String) bFf().mo5608dq().mo3214p(f4772bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "56ea65690d655675f23ff3e8573bac9d", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m23201b(String str) {
        throw new aWi(new aCE(this, f4776bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m23203c(UUID uuid) {
        switch (bFf().mo6893i(f4774bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4774bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4774bO, new Object[]{uuid}));
                break;
        }
        m23202b(uuid);
    }

    private float chs() {
        return bFf().mo5608dq().mo3211m(fUH);
    }

    /* renamed from: jO */
    private void m23207jO(float f) {
        bFf().mo5608dq().mo3150a(fUH, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Respawn Time (sec)")
    @C0064Am(aul = "03013c9cf45f730290ee60a0133f55d4", aum = 0)
    @C5566aOg
    /* renamed from: jP */
    private void m23208jP(float f) {
        throw new aWi(new aCE(this, fUJ, new Object[]{new Float(f)}));
    }

    @C0064Am(aul = "b062ea35c6e2638541d1ab0838f59329", aum = 0)
    /* renamed from: qQ */
    private List<NPC> m23209qQ() {
        throw new aWi(new aCE(this, f4767Lg, new Object[0]));
    }

    /* renamed from: D */
    public void mo2941D(Character acx) {
        switch (bFf().mo6893i(dld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dld, new Object[]{acx}));
                break;
        }
        m23185C(acx);
    }

    /* renamed from: F */
    public void mo2942F(Character acx) {
        switch (bFf().mo6893i(dle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dle, new Object[]{acx}));
                break;
        }
        m23186E(acx);
    }

    /* renamed from: H */
    public void mo2943H(Character acx) {
        switch (bFf().mo6893i(dlf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlf, new Object[]{acx}));
                break;
        }
        m23187G(acx);
    }

    /* access modifiers changed from: protected */
    /* renamed from: LE */
    public Random mo14328LE() {
        switch (bFf().mo6893i(awz)) {
            case 0:
                return null;
            case 2:
                return (Random) bFf().mo5606d(new aCE(this, awz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, awz, new Object[0]));
                break;
        }
        return m23188LD();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: Uy */
    public NPCZone mo8669Uy() {
        switch (bFf().mo6893i(aQt)) {
            case 0:
                return null;
            case 2:
                return (NPCZone) bFf().mo5606d(new aCE(this, aQt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQt, new Object[0]));
                break;
        }
        return m23190Ux();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2693ig(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m23198ap();
            case 1:
                m23202b((UUID) args[0]);
                return null;
            case 2:
                return m23199ar();
            case 3:
                m23201b((String) args[0]);
                return null;
            case 4:
                return new Float(cht());
            case 5:
                m23208jP(((Float) args[0]).floatValue());
                return null;
            case 6:
                return m23209qQ();
            case 7:
                aZk();
                return null;
            case 8:
                m23206hN(((Long) args[0]).longValue());
                return null;
            case 9:
                m23185C((Character) args[0]);
                return null;
            case 10:
                m23186E((Character) args[0]);
                return null;
            case 11:
                m23187G((Character) args[0]);
                return null;
            case 12:
                m23192a((Character) args[0], (Character) args[1]);
                return null;
            case 13:
                m23204e((NPC) args[0]);
                return null;
            case 14:
                aZn();
                return null;
            case 15:
                aZp();
                return null;
            case 16:
                return m23188LD();
            case 17:
                return chv();
            case 18:
                return chx();
            case 19:
                m23200b((NPCZone) args[0]);
                return null;
            case 20:
                return m23190Ux();
            case 21:
                m23205gQ((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public void aZl() {
        switch (bFf().mo6893i(dla)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dla, new Object[0]));
                break;
        }
        aZk();
    }

    public void aZo() {
        switch (bFf().mo6893i(dlg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlg, new Object[0]));
                break;
        }
        aZn();
    }

    public void aZq() {
        switch (bFf().mo6893i(dlh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlh, new Object[0]));
                break;
        }
        aZp();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4773bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4773bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4773bN, new Object[0]));
                break;
        }
        return m23198ap();
    }

    /* renamed from: b */
    public void mo2952b(Character acx, Character acx2) {
        switch (bFf().mo6893i(dlk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlk, new Object[]{acx, acx2}));
                break;
        }
        m23192a(acx, acx2);
    }

    /* renamed from: c */
    public void mo8670c(NPCZone fwVar) {
        switch (bFf().mo6893i(aQs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aQs, new Object[]{fwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aQs, new Object[]{fwVar}));
                break;
        }
        m23200b(fwVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Respawn Time (sec)")
    public float chu() {
        switch (bFf().mo6893i(fUI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, fUI, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, fUI, new Object[0]));
                break;
        }
        return cht();
    }

    /* access modifiers changed from: protected */
    public Quat4fWrap chw() {
        switch (bFf().mo6893i(fUL)) {
            case 0:
                return null;
            case 2:
                return (Quat4fWrap) bFf().mo5606d(new aCE(this, fUL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fUL, new Object[0]));
                break;
        }
        return chv();
    }

    /* access modifiers changed from: protected */
    public Vec3f chy() {
        switch (bFf().mo6893i(fUM)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, fUM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fUM, new Object[0]));
                break;
        }
        return chx();
    }

    /* renamed from: cj */
    public void mo14332cj(String str) {
        switch (bFf().mo6893i(esf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, esf, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, esf, new Object[]{str}));
                break;
        }
        m23205gQ(str);
    }

    /* renamed from: f */
    public void mo2953f(NPC auf) {
        switch (bFf().mo6893i(dlc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dlc, new Object[]{auf}));
                break;
        }
        m23204e(auf);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4775bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4775bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4775bP, new Object[0]));
                break;
        }
        return m23199ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4776bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4776bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4776bQ, new Object[]{str}));
                break;
        }
        m23201b(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: hO */
    public void mo14334hO(long j) {
        switch (bFf().mo6893i(fUK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fUK, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fUK, new Object[]{new Long(j)}));
                break;
        }
        m23206hN(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Respawn Time (sec)")
    @C5566aOg
    /* renamed from: jQ */
    public void mo14335jQ(float f) {
        switch (bFf().mo6893i(fUJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fUJ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fUJ, new Object[]{new Float(f)}));
                break;
        }
        m23208jP(f);
    }

    /* renamed from: qR */
    public List<NPC> mo2957qR() {
        switch (bFf().mo6893i(f4767Lg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f4767Lg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4767Lg, new Object[0]));
                break;
        }
        return m23209qQ();
    }

    @C0064Am(aul = "1ad19abc50d4b7e3b9fec65d5299a122", aum = 0)
    /* renamed from: ap */
    private UUID m23198ap() {
        return m23196an();
    }

    @C0064Am(aul = "4d90600d15245e9356067a138d6c5081", aum = 0)
    /* renamed from: b */
    private void m23202b(UUID uuid) {
        m23195a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "706a15762ffcf9ccd9a2461465b5f775", aum = 0)
    /* renamed from: ar */
    private String m23199ar() {
        return m23197ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Respawn Time (sec)")
    @C0064Am(aul = "821c9e15b2d60c0071c7512617440f9e", aum = 0)
    private float cht() {
        return chs();
    }

    /* renamed from: h */
    public void mo14333h(PopulationControlType rsVar) {
        super.mo967a((C2961mJ) rsVar);
        m23195a(UUID.randomUUID());
    }

    @C0064Am(aul = "b7eaff148b9dc5743453bdd2ae48c53f", aum = 0)
    /* renamed from: hN */
    private void m23206hN(long j) {
        NPCZone Uy = mo8669Uy();
        SpaceZoneEvent afb = (SpaceZoneEvent) bFf().mo6865M(SpaceZoneEvent.class);
        afb.mo8792a(this, j, fUG);
        Uy.mo20322b(afb);
    }

    @C0064Am(aul = "d4b83bc2b9b263b5ecf6dddddec07198", aum = 0)
    /* renamed from: C */
    private void m23185C(Character acx) {
    }

    @C0064Am(aul = "7eae00edfa9ba36dc90c6225b5afb55c", aum = 0)
    /* renamed from: E */
    private void m23186E(Character acx) {
    }

    @C0064Am(aul = "5b8b0cac871156282f984232ad4258c5", aum = 0)
    /* renamed from: G */
    private void m23187G(Character acx) {
    }

    @C0064Am(aul = "fd93821136e1350392508517cd3becd9", aum = 0)
    /* renamed from: a */
    private void m23192a(Character acx, Character acx2) {
    }

    @C0064Am(aul = "eb1df50afd5ed856f4fea00ce1edcd9a", aum = 0)
    /* renamed from: e */
    private void m23204e(NPC auf) {
    }

    @C0064Am(aul = "97410bb154a3914bf3b20b436a81180d", aum = 0)
    private void aZn() {
    }

    @C0064Am(aul = "610fde54f84f9c1ad21ed2624f9fb50a", aum = 0)
    private void aZp() {
        m23189Uw().mo20319aK(fUG);
    }

    @C0064Am(aul = "536f9b7ee2e26c5819e8081f79b12bbf", aum = 0)
    /* renamed from: LD */
    private Random m23188LD() {
        if (this.awe == null) {
            this.awe = new Random();
        }
        return this.awe;
    }

    @C0064Am(aul = "9b8869e5b871c047c409076f0a2471bd", aum = 0)
    private Quat4fWrap chv() {
        Random LE = mo14328LE();
        return new Quat4fWrap(Quat4fWrap.m24427c((double) (LE.nextFloat() * 360.0f), (double) (LE.nextFloat() * 360.0f), (double) (LE.nextFloat() * 360.0f)));
    }

    @C0064Am(aul = "39d57e8dfeb7ea65865c8d731233c4de", aum = 0)
    private Vec3f chx() {
        Random LE = mo14328LE();
        Vec3f dfO = new Vec3f((LE.nextFloat() * 2.0f) - 1.0f, (LE.nextFloat() * 2.0f) - 1.0f, (LE.nextFloat() * 2.0f) - 1.0f).dfO();
        float qE = mo8669Uy().mo18870qE();
        if (qE == 0.0f) {
            qE = mo8669Uy().mo20316Lu();
        }
        Vec3f mS = dfO.mo23510mS(qE * LE.nextFloat());
        Vec3f qC = mo8669Uy().mo18869qC();
        if (qC != null) {
            return mS.mo23503i(qC);
        }
        return mS;
    }

    @C0064Am(aul = "2de4d8cf6d74333ec453ff397d52efd8", aum = 0)
    /* renamed from: b */
    private void m23200b(NPCZone fwVar) {
        m23193a(fwVar);
    }

    @C0064Am(aul = "8d43f2d598f6165272707a260a8410da", aum = 0)
    /* renamed from: Ux */
    private NPCZone m23190Ux() {
        return m23189Uw();
    }

    @C0064Am(aul = "7e976e7f669880489eb395a17d510f6c", aum = 0)
    /* renamed from: gQ */
    private void m23205gQ(String str) {
        if (fUG.equals(str)) {
            aZl();
        }
    }
}
