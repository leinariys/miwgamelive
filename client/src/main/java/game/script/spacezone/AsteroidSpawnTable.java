package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.space.AsteroidType;
import logic.baa.*;
import logic.data.mbean.C0459GL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.Oi */
/* compiled from: a */
public class AsteroidSpawnTable extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: Pf */
    public static final C2491fm f1326Pf = null;

    /* renamed from: Pg */
    public static final C2491fm f1327Pg = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1329bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1330bM = null;
    /* renamed from: bN */
    public static final C2491fm f1331bN = null;
    /* renamed from: bO */
    public static final C2491fm f1332bO = null;
    /* renamed from: bP */
    public static final C2491fm f1333bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1334bQ = null;
    public static final C5663aRz cJt = null;
    public static final C2491fm cOC = null;
    public static final C2491fm dJo = null;
    public static final C2491fm dJp = null;
    public static final C2491fm dJq = null;
    public static final C2491fm dJr = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f1335zQ = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0cafdd62ea30586e0a07aaab1885a2a2", aum = 2)

    /* renamed from: bK */
    private static UUID f1328bK;
    @C0064Am(aul = "78afcd5797b8566827a6976440112990", aum = 0)
    private static C3438ra<AsteroidType> cJs;
    @C0064Am(aul = "19110ce7c864fd06ddb204e7c68df340", aum = 3)
    private static String handle;
    @C0064Am(aul = "7086b6af24d3f61bac0d99db9b9a96f1", aum = 1)
    private static String name;

    static {
        m8111V();
    }

    private transient List<C6349alJ> dJn;

    public AsteroidSpawnTable() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidSpawnTable(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8111V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(AsteroidSpawnTable.class, "78afcd5797b8566827a6976440112990", i);
        cJt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AsteroidSpawnTable.class, "7086b6af24d3f61bac0d99db9b9a96f1", i2);
        f1335zQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AsteroidSpawnTable.class, "0cafdd62ea30586e0a07aaab1885a2a2", i3);
        f1329bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AsteroidSpawnTable.class, "19110ce7c864fd06ddb204e7c68df340", i4);
        f1330bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 12)];
        C2491fm a = C4105zY.m41624a(AsteroidSpawnTable.class, "ba43053d78b28bf164421a214d19187c", i6);
        f1331bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidSpawnTable.class, "7a1d6254d8c6281980a2d9d33f57e350", i7);
        f1332bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(AsteroidSpawnTable.class, "cec94661f6d19fc4ff35c60e032c4c72", i8);
        f1333bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(AsteroidSpawnTable.class, "b1e863c38bf44740243cb5ae1eec026b", i9);
        f1334bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(AsteroidSpawnTable.class, "30742f0ac98df3d830528086fb0b30dc", i10);
        f1326Pf = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(AsteroidSpawnTable.class, "0c302690e1497acbe0b75cf6e5200ebb", i11);
        f1327Pg = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(AsteroidSpawnTable.class, "bc3102230cef6cc106c9a892f1209448", i12);
        dJo = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(AsteroidSpawnTable.class, "504149019277e5995dad5aa00e2138ca", i13);
        dJp = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(AsteroidSpawnTable.class, "03b586b424ddaab9a355247dd53b85a2", i14);
        cOC = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(AsteroidSpawnTable.class, "1a19c5f12b4881b1a84476605ba29d5f", i15);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(AsteroidSpawnTable.class, "8f03d4e3cbdbd3953e35befefaf9b9d1", i16);
        dJq = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(AsteroidSpawnTable.class, "4dbfaba73d51e2d7b58abdc991e540fb", i17);
        dJr = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidSpawnTable.class, C0459GL.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m8112a(String str) {
        bFf().mo5608dq().mo3197f(f1330bM, str);
    }

    /* renamed from: a */
    private void m8113a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1329bL, uuid);
    }

    private C3438ra aHu() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJt);
    }

    /* renamed from: an */
    private UUID m8114an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1329bL);
    }

    /* renamed from: ao */
    private String m8115ao() {
        return (String) bFf().mo5608dq().mo3214p(f1330bM);
    }

    /* renamed from: ap */
    private void m8117ap(String str) {
        bFf().mo5608dq().mo3197f(f1335zQ, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "0c302690e1497acbe0b75cf6e5200ebb", aum = 0)
    @C5566aOg
    /* renamed from: aq */
    private void m8118aq(String str) {
        throw new aWi(new aCE(this, f1327Pg, new Object[]{str}));
    }

    /* renamed from: ar */
    private void m8120ar(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJt, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "b1e863c38bf44740243cb5ae1eec026b", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m8122b(String str) {
        throw new aWi(new aCE(this, f1334bQ, new Object[]{str}));
    }

    @C0064Am(aul = "8f03d4e3cbdbd3953e35befefaf9b9d1", aum = 0)
    @C5566aOg
    private void bld() {
        throw new aWi(new aCE(this, dJq, new Object[0]));
    }

    @C5566aOg
    private void ble() {
        switch (bFf().mo6893i(dJq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dJq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dJq, new Object[0]));
                break;
        }
        bld();
    }

    /* renamed from: c */
    private void m8124c(UUID uuid) {
        switch (bFf().mo6893i(f1332bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1332bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1332bO, new Object[]{uuid}));
                break;
        }
        m8123b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Asteroid types")
    @C0064Am(aul = "bc3102230cef6cc106c9a892f1209448", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m8125h(AsteroidType aqn) {
        throw new aWi(new aCE(this, dJo, new Object[]{aqn}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Asteroid types")
    @C0064Am(aul = "504149019277e5995dad5aa00e2138ca", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m8126j(AsteroidType aqn) {
        throw new aWi(new aCE(this, dJp, new Object[]{aqn}));
    }

    @C0064Am(aul = "4dbfaba73d51e2d7b58abdc991e540fb", aum = 0)
    @C5566aOg
    /* renamed from: mc */
    private AsteroidType m8127mc(int i) {
        throw new aWi(new aCE(this, dJr, new Object[]{new Integer(i)}));
    }

    /* renamed from: uU */
    private String m8128uU() {
        return (String) bFf().mo5608dq().mo3214p(f1335zQ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0459GL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m8116ap();
            case 1:
                m8123b((UUID) args[0]);
                return null;
            case 2:
                return m8119ar();
            case 3:
                m8122b((String) args[0]);
                return null;
            case 4:
                return m8129uV();
            case 5:
                m8118aq((String) args[0]);
                return null;
            case 6:
                m8125h((AsteroidType) args[0]);
                return null;
            case 7:
                m8126j((AsteroidType) args[0]);
                return null;
            case 8:
                return aKV();
            case 9:
                return m8121au();
            case 10:
                bld();
                return null;
            case 11:
                return m8127mc(((Integer) args[0]).intValue());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Asteroid types")
    public List<AsteroidType> aKW() {
        switch (bFf().mo6893i(cOC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOC, new Object[0]));
                break;
        }
        return aKV();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1331bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1331bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1331bN, new Object[0]));
                break;
        }
        return m8116ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1333bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1333bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1333bP, new Object[0]));
                break;
        }
        return m8119ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1334bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1334bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1334bQ, new Object[]{str}));
                break;
        }
        m8122b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    public String getName() {
        switch (bFf().mo6893i(f1326Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1326Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1326Pf, new Object[0]));
                break;
        }
        return m8129uV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    public void setName(String str) {
        switch (bFf().mo6893i(f1327Pg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1327Pg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1327Pg, new Object[]{str}));
                break;
        }
        m8118aq(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Asteroid types")
    @C5566aOg
    /* renamed from: i */
    public void mo4476i(AsteroidType aqn) {
        switch (bFf().mo6893i(dJo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dJo, new Object[]{aqn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dJo, new Object[]{aqn}));
                break;
        }
        m8125h(aqn);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Asteroid types")
    @C5566aOg
    /* renamed from: k */
    public void mo4477k(AsteroidType aqn) {
        switch (bFf().mo6893i(dJp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dJp, new Object[]{aqn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dJp, new Object[]{aqn}));
                break;
        }
        m8126j(aqn);
    }

    @C5566aOg
    /* renamed from: md */
    public AsteroidType mo4478md(int i) {
        switch (bFf().mo6893i(dJr)) {
            case 0:
                return null;
            case 2:
                return (AsteroidType) bFf().mo5606d(new aCE(this, dJr, new Object[]{new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, dJr, new Object[]{new Integer(i)}));
                break;
        }
        return m8127mc(i);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m8121au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ba43053d78b28bf164421a214d19187c", aum = 0)
    /* renamed from: ap */
    private UUID m8116ap() {
        return m8114an();
    }

    @C0064Am(aul = "7a1d6254d8c6281980a2d9d33f57e350", aum = 0)
    /* renamed from: b */
    private void m8123b(UUID uuid) {
        m8113a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "cec94661f6d19fc4ff35c60e032c4c72", aum = 0)
    /* renamed from: ar */
    private String m8119ar() {
        return m8115ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "30742f0ac98df3d830528086fb0b30dc", aum = 0)
    /* renamed from: uV */
    private String m8129uV() {
        return m8128uU();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Asteroid types")
    @C0064Am(aul = "03b586b424ddaab9a355247dd53b85a2", aum = 0)
    private List<AsteroidType> aKV() {
        return Collections.unmodifiableList(aHu());
    }

    @C0064Am(aul = "1a19c5f12b4881b1a84476605ba29d5f", aum = 0)
    /* renamed from: au */
    private String m8121au() {
        if (m8128uU() != null) {
            return m8128uU();
        }
        return "[null]";
    }
}
