package game.script.spacezone;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5696aTg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.io.Serializable;
import java.util.Collection;

@C5511aMd
@C6485anp
public
        /* renamed from: a.pu */
        /* compiled from: a */
class ZoneArea extends C1634Xv implements C1616Xf, Serializable {
    public static final C5663aRz _f_center = null;
    /* renamed from: _f_getCenter_0020_0028_0029Lcom_002fhoplon_002fgeometry_002fVec3d_003b */
    public static final C2491fm f8856xdc7de0cc = null;
    /* renamed from: _f_setCenter_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003b_0029V */
    public static final C2491fm f8857x6107980 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aSi = null;
    public static final C2491fm aSj = null;
    public static final C2491fm aSk = null;
    public static final C2491fm aSl = null;
    public static final C5663aRz awh = null;
    /* renamed from: ui */
    public static final C5663aRz f8858ui = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "d5ab8f2b7356926acd5698324b45456c", aum = 2)

    /* renamed from: Dw */
    private static long f8855Dw = 0;
    @C0064Am(aul = "bb8e39cf5816a99d7e56259bde8b5230", aum = 0)
    private static Node aSh = null;
    @C0064Am(aul = "5d00cfdc5f528a9acc6ed399e32db9fd", aum = 1)
    private static Vec3d center = null;

    static {
        m37326V();
    }

    ZoneArea() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ZoneArea(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37326V() {
        _m_fieldCount = 3;
        _m_methodCount = 6;
        C5663aRz b = C5640aRc.m17844b(ZoneArea.class, "bb8e39cf5816a99d7e56259bde8b5230", 0);
        f8858ui = b;
        C5663aRz b2 = C5640aRc.m17844b(ZoneArea.class, "5d00cfdc5f528a9acc6ed399e32db9fd", 1);
        _f_center = b2;
        C5663aRz b3 = C5640aRc.m17844b(ZoneArea.class, "d5ab8f2b7356926acd5698324b45456c", 2);
        awh = b3;
        _m_fields = new C5663aRz[]{b, b2, b3};
        C2491fm a = C4105zY.m41624a(ZoneArea.class, "d48e42fac5711cf508d4c5c32d0d9fbe", 0);
        aSi = a;
        C2491fm a2 = C4105zY.m41624a(ZoneArea.class, "394b19f815a71d9ecf2e8545572e698a", 1);
        aSj = a2;
        C2491fm a3 = C4105zY.m41624a(ZoneArea.class, "5d29dff820d0332ab7e97ac42589e0d4", 2);
        f8856xdc7de0cc = a3;
        C2491fm a4 = C4105zY.m41624a(ZoneArea.class, "cb728383c1ca6526fda53442c43cf3d4", 3);
        f8857x6107980 = a4;
        C2491fm a5 = C4105zY.m41624a(ZoneArea.class, "851b8f38fd1d3fd4d329bb2db4cb0741", 4);
        aSk = a5;
        C2491fm a6 = C4105zY.m41624a(ZoneArea.class, "fdca38403cd759f8e84d39e3f53ceb29", 5);
        aSl = a6;
        _m_methods = new C2491fm[]{a, a2, a3, a4, a5, a6};
        ___iScriptClass = aUO.m18566a(ZoneArea.class, C5696aTg.class, _m_fields, _m_methods);
    }

    /* renamed from: Vb */
    private Node m37327Vb() {
        return (Node) bFf().mo5608dq().mo3214p(f8858ui);
    }

    /* renamed from: Vc */
    private long m37328Vc() {
        return bFf().mo5608dq().mo3213o(awh);
    }

    /* renamed from: a */
    private void m37331a(Node rPVar) {
        bFf().mo5608dq().mo3197f(f8858ui, rPVar);
    }

    /* renamed from: cH */
    private void m37333cH(long j) {
        bFf().mo5608dq().mo3184b(awh, j);
    }

    /* renamed from: f */
    private void m37335f(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(_f_center, ajr);
    }

    /* renamed from: zL */
    private Vec3d m37337zL() {
        return (Vec3d) bFf().mo5608dq().mo3214p(_f_center);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: Ve */
    public Node mo21236Ve() {
        switch (bFf().mo6893i(aSi)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, aSi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aSi, new Object[0]));
                break;
        }
        return m37329Vd();
    }

    /* renamed from: Vg */
    public long mo21237Vg() {
        switch (bFf().mo6893i(aSk)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, aSk, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, aSk, new Object[0]));
                break;
        }
        return m37330Vf();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5696aTg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq()) {
            case 0:
                return m37329Vd();
            case 1:
                m37332b((Node) args[0]);
                return null;
            case 2:
                return m37338zN();
            case 3:
                m37336g((Vec3d) args[0]);
                return null;
            case 4:
                return new Long(m37330Vf());
            case 5:
                m37334cI(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        throw new aWi(gr);
    }

    /* renamed from: c */
    public void mo21238c(Node rPVar) {
        switch (bFf().mo6893i(aSj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                break;
        }
        m37332b(rPVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cJ */
    public void mo21239cJ(long j) {
        switch (bFf().mo6893i(aSl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                break;
        }
        m37334cI(j);
    }

    /* renamed from: h */
    public void mo21240h(Vec3d ajr) {
        switch (bFf().mo6893i(f8857x6107980)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8857x6107980, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8857x6107980, new Object[]{ajr}));
                break;
        }
        m37336g(ajr);
    }

    /* renamed from: zO */
    public Vec3d mo21241zO() {
        switch (bFf().mo6893i(f8856xdc7de0cc)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, f8856xdc7de0cc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8856xdc7de0cc, new Object[0]));
                break;
        }
        return m37338zN();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: S */
    public void mo10S() {
    }

    @C0064Am(aul = "d48e42fac5711cf508d4c5c32d0d9fbe", aum = 0)
    /* renamed from: Vd */
    private Node m37329Vd() {
        return m37327Vb();
    }

    @C0064Am(aul = "394b19f815a71d9ecf2e8545572e698a", aum = 0)
    /* renamed from: b */
    private void m37332b(Node rPVar) {
        m37331a(rPVar);
    }

    @C0064Am(aul = "5d29dff820d0332ab7e97ac42589e0d4", aum = 0)
    /* renamed from: zN */
    private Vec3d m37338zN() {
        return m37337zL();
    }

    @C0064Am(aul = "cb728383c1ca6526fda53442c43cf3d4", aum = 0)
    /* renamed from: g */
    private void m37336g(Vec3d ajr) {
        m37335f(ajr);
    }

    @C0064Am(aul = "851b8f38fd1d3fd4d329bb2db4cb0741", aum = 0)
    /* renamed from: Vf */
    private long m37330Vf() {
        return m37328Vc();
    }

    @C0064Am(aul = "fdca38403cd759f8e84d39e3f53ceb29", aum = 0)
    /* renamed from: cI */
    private void m37334cI(long j) {
        m37333cH(j);
    }
}
