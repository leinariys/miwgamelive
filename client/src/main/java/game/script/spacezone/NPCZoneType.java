package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.link.C1674Ye;
import logic.data.mbean.C2596hL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.HU */
/* compiled from: a */
public class NPCZoneType extends SpaceZoneType implements C1616Xf {

    /* renamed from: KA */
    public static final C5663aRz f666KA = null;
    /* renamed from: KJ */
    public static final C5663aRz f668KJ = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    /* renamed from: dN */
    public static final C2491fm f670dN = null;
    public static final C2491fm ddY = null;
    public static final C2491fm ddZ = null;
    public static final C2491fm dea = null;
    public static final C2491fm deb = null;
    public static final C2491fm dec = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5adf1af3f09205dc0827b3cec5896333", aum = 1)

    /* renamed from: KI */
    private static Asset f667KI;
    @C0064Am(aul = "79232f60aabb8630baec9bdeac7fdf34", aum = 0)

    /* renamed from: Vk */
    private static PopulationControlType f669Vk;

    static {
        m5183V();
    }

    public NPCZoneType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCZoneType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5183V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = SpaceZoneType._m_fieldCount + 2;
        _m_methodCount = SpaceZoneType._m_methodCount + 7;
        int i = SpaceZoneType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(NPCZoneType.class, "79232f60aabb8630baec9bdeac7fdf34", i);
        f666KA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCZoneType.class, "5adf1af3f09205dc0827b3cec5896333", i2);
        f668KJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) SpaceZoneType._m_fields, (Object[]) _m_fields);
        int i4 = SpaceZoneType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(NPCZoneType.class, "7d72d7d52dd81923679b7f94a0cf04fe", i4);
        ddY = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCZoneType.class, "8d3a8cf6bcae20388148e6ddd9cde74e", i5);
        ddZ = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCZoneType.class, "7051a0f34c0146a2c46a9f31fee2739c", i6);
        dea = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCZoneType.class, "3ed0d8c7c7bc2574ea8d490fa13dc589", i7);
        deb = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCZoneType.class, "22d2a522b513d1569b4fcd4a52dabcfd", i8);
        aDR = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCZoneType.class, "41cdb5b59140b27a56758e16a19f32d9", i9);
        dec = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCZoneType.class, "26bac4f5518095f64165d30e094510df", i10);
        f670dN = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) SpaceZoneType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCZoneType.class, C2596hL.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m5184a(PopulationControlType rsVar) {
        bFf().mo5608dq().mo3197f(f666KA, rsVar);
    }

    private PopulationControlType aVB() {
        return (PopulationControlType) bFf().mo5608dq().mo3214p(f666KA);
    }

    /* renamed from: q */
    private void m5189q(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f668KJ, tCVar);
    }

    /* renamed from: qx */
    private Asset m5190qx() {
        return (Asset) bFf().mo5608dq().mo3214p(f668KJ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2596hL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - SpaceZoneType._m_methodCount) {
            case 0:
                return aVC();
            case 1:
                m5187b((PopulationControlType) args[0]);
                return null;
            case 2:
                return aVE();
            case 3:
                m5186aq((Asset) args[0]);
                return null;
            case 4:
                m5188e((aDJ) args[0]);
                return null;
            case 5:
                return aVG();
            case 6:
                return m5185aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f670dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f670dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f670dN, new Object[0]));
                break;
        }
        return m5185aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Population Control")
    public PopulationControlType aVD() {
        switch (bFf().mo6893i(ddY)) {
            case 0:
                return null;
            case 2:
                return (PopulationControlType) bFf().mo5606d(new aCE(this, ddY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ddY, new Object[0]));
                break;
        }
        return aVC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Music SFX")
    public Asset aVF() {
        switch (bFf().mo6893i(dea)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dea, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dea, new Object[0]));
                break;
        }
        return aVE();
    }

    public NPCZone aVH() {
        switch (bFf().mo6893i(dec)) {
            case 0:
                return null;
            case 2:
                return (NPCZone) bFf().mo5606d(new aCE(this, dec, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dec, new Object[0]));
                break;
        }
        return aVG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Music SFX")
    /* renamed from: ar */
    public void mo2629ar(Asset tCVar) {
        switch (bFf().mo6893i(deb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, deb, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, deb, new Object[]{tCVar}));
                break;
        }
        m5186aq(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Population Control")
    /* renamed from: c */
    public void mo2630c(PopulationControlType rsVar) {
        switch (bFf().mo6893i(ddZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ddZ, new Object[]{rsVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ddZ, new Object[]{rsVar}));
                break;
        }
        m5187b(rsVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m5188e(adj);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Population Control")
    @C0064Am(aul = "7d72d7d52dd81923679b7f94a0cf04fe", aum = 0)
    private PopulationControlType aVC() {
        return aVB();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Population Control")
    @C0064Am(aul = "8d3a8cf6bcae20388148e6ddd9cde74e", aum = 0)
    /* renamed from: b */
    private void m5187b(PopulationControlType rsVar) {
        m5184a(rsVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Music SFX")
    @C0064Am(aul = "7051a0f34c0146a2c46a9f31fee2739c", aum = 0)
    private Asset aVE() {
        return m5190qx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Music SFX")
    @C0064Am(aul = "3ed0d8c7c7bc2574ea8d490fa13dc589", aum = 0)
    /* renamed from: aq */
    private void m5186aq(Asset tCVar) {
        m5189q(tCVar);
    }

    @C0064Am(aul = "22d2a522b513d1569b4fcd4a52dabcfd", aum = 0)
    /* renamed from: e */
    private void m5188e(aDJ adj) {
        super.mo2631f(adj);
        if (adj instanceof NPCZone) {
            C1616Xf xf = adj;
            if (aVB() != null) {
                xf.mo6765g(C1674Ye.eKZ, aVB().mo7459NK());
            }
        }
    }

    @C0064Am(aul = "41cdb5b59140b27a56758e16a19f32d9", aum = 0)
    private NPCZone aVG() {
        return (NPCZone) mo745aU();
    }

    @C0064Am(aul = "26bac4f5518095f64165d30e094510df", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m5185aT() {
        T t = (NPCZone) bFf().mo6865M(NPCZone.class);
        t.mo18864a(this);
        return t;
    }
}
