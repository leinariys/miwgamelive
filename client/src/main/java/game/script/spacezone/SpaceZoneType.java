package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.pda.TaikopediaEntry;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C0218Cj;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.Qm */
/* compiled from: a */
public abstract class SpaceZoneType extends BaseTaikodomContent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C5663aRz awd = null;
    public static final C2491fm dyZ = null;
    public static final C2491fm dza = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3c776bc3ed77bd1c631383d54091ae20", aum = 0)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f1467jT;

    static {
        m8980V();
    }

    public SpaceZoneType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpaceZoneType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8980V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 1;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 4;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(SpaceZoneType.class, "3c776bc3ed77bd1c631383d54091ae20", i);
        awd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i3 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(SpaceZoneType.class, "d5e5a55747823c4cf341fe376162d24e", i3);
        aMl = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(SpaceZoneType.class, "82e461ca41c36c8ff3bc19d285d6469f", i4);
        aMm = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(SpaceZoneType.class, "5f8b88329d0c88f028539c274e1c0cfc", i5);
        dyZ = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(SpaceZoneType.class, "c124e1abe13195ac5d04bf30a3882146", i6);
        dza = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpaceZoneType.class, C0218Cj.class, _m_fields, _m_methods);
    }

    /* renamed from: Ll */
    private C3438ra m8979Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    /* renamed from: x */
    private void m8983x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0218Cj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                m8981a((TaikopediaEntry) args[0]);
                return null;
            case 1:
                m8982c((TaikopediaEntry) args[0]);
                return null;
            case 2:
                return bgE();
            case 3:
                return bgG();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    /* renamed from: b */
    public void mo5080b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m8981a(aiz);
    }

    public C3438ra<TaikopediaEntry> bgF() {
        switch (bFf().mo6893i(dyZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
                break;
        }
        return bgE();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    public List<TaikopediaEntry> bgH() {
        switch (bFf().mo6893i(dza)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dza, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dza, new Object[0]));
                break;
        }
        return bgG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    /* renamed from: d */
    public void mo5083d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m8982c(aiz);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "d5e5a55747823c4cf341fe376162d24e", aum = 0)
    /* renamed from: a */
    private void m8981a(TaikopediaEntry aiz) {
        m8979Ll().add(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "82e461ca41c36c8ff3bc19d285d6469f", aum = 0)
    /* renamed from: c */
    private void m8982c(TaikopediaEntry aiz) {
        m8979Ll().remove(aiz);
    }

    @C0064Am(aul = "5f8b88329d0c88f028539c274e1c0cfc", aum = 0)
    private C3438ra<TaikopediaEntry> bgE() {
        return m8979Ll();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "c124e1abe13195ac5d04bf30a3882146", aum = 0)
    private List<TaikopediaEntry> bgG() {
        return Collections.unmodifiableList(m8979Ll());
    }
}
