package game.script.spacezone;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aCR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aFb  reason: case insensitive filesystem */
/* compiled from: a */
public class SpaceZoneEvent extends TaikodomObject implements C1616Xf {

    /* renamed from: De */
    public static final C5663aRz f2830De = null;

    /* renamed from: Dj */
    public static final C2491fm f2831Dj = null;

    /* renamed from: Dk */
    public static final C2491fm f2832Dk = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bM */
    public static final C5663aRz f2833bM = null;
    /* renamed from: bP */
    public static final C2491fm f2834bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2835bQ = null;
    public static final C5663aRz cqr = null;
    public static final C2491fm hIE = null;
    public static final C2491fm hIF = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4c62af046e113f544f7cf7b9bf2b9c8c", aum = 2)
    private static String handle;
    @C0064Am(aul = "83653c16f7a5f48b4770fdd3bde1c27e", aum = 0)
    private static C3763uj hxt;
    @C0064Am(aul = "c402a7f23430145259f0d060756343ea", aum = 1)
    private static long time;

    static {
        m14595V();
    }

    public SpaceZoneEvent() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpaceZoneEvent(C5540aNg ang) {
        super(ang);
    }

    public SpaceZoneEvent(C3763uj ujVar, long j, String str) {
        super((C5540aNg) null);
        super._m_script_init(ujVar, j, str);
    }

    /* renamed from: V */
    static void m14595V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(SpaceZoneEvent.class, "83653c16f7a5f48b4770fdd3bde1c27e", i);
        cqr = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpaceZoneEvent.class, "c402a7f23430145259f0d060756343ea", i2);
        f2830De = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpaceZoneEvent.class, "4c62af046e113f544f7cf7b9bf2b9c8c", i3);
        f2833bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(SpaceZoneEvent.class, "cab7467edc061840502ab42bea322d47", i5);
        hIE = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(SpaceZoneEvent.class, "52049c22a3d68f4423897c33f2e89880", i6);
        hIF = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(SpaceZoneEvent.class, "548a35ca9d0b50fc6c91a7b6a22dc2c9", i7);
        f2831Dj = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(SpaceZoneEvent.class, "af685c29d9b6995d552f8d65d88e018a", i8);
        f2832Dk = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(SpaceZoneEvent.class, "419f72582d19b4d6501811137c253f87", i9);
        f2834bP = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(SpaceZoneEvent.class, "f031ecf74c357f197ebbe619dee625ca", i10);
        f2835bQ = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpaceZoneEvent.class, aCR.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m14596a(C3763uj ujVar) {
        bFf().mo5608dq().mo3197f(cqr, ujVar);
    }

    /* renamed from: a */
    private void m14597a(String str) {
        bFf().mo5608dq().mo3197f(f2833bM, str);
    }

    /* renamed from: ao */
    private String m14598ao() {
        return (String) bFf().mo5608dq().mo3214p(f2833bM);
    }

    private C3763uj cYh() {
        return (C3763uj) bFf().mo5608dq().mo3214p(cqr);
    }

    /* renamed from: lT */
    private long m14602lT() {
        return bFf().mo5608dq().mo3213o(f2830De);
    }

    /* renamed from: s */
    private void m14604s(long j) {
        bFf().mo5608dq().mo3184b(f2830De, j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aCR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cYi();
            case 1:
                m14600b((C3763uj) args[0]);
                return null;
            case 2:
                return new Long(m14603lX());
            case 3:
                m14605t(((Long) args[0]).longValue());
                return null;
            case 4:
                return m14599ar();
            case 5:
                m14601b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public void mo8793c(C3763uj ujVar) {
        switch (bFf().mo6893i(hIF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hIF, new Object[]{ujVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hIF, new Object[]{ujVar}));
                break;
        }
        m14600b(ujVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C3763uj cYj() {
        switch (bFf().mo6893i(hIE)) {
            case 0:
                return null;
            case 2:
                return (C3763uj) bFf().mo5606d(new aCE(this, hIE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hIE, new Object[0]));
                break;
        }
        return cYi();
    }

    public String getHandle() {
        switch (bFf().mo6893i(f2834bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2834bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2834bP, new Object[0]));
                break;
        }
        return m14599ar();
    }

    public void setHandle(String str) {
        switch (bFf().mo6893i(f2835bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2835bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2835bQ, new Object[]{str}));
                break;
        }
        m14601b(str);
    }

    public long getTime() {
        switch (bFf().mo6893i(f2831Dj)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f2831Dj, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2831Dj, new Object[0]));
                break;
        }
        return m14603lX();
    }

    public void setTime(long j) {
        switch (bFf().mo6893i(f2832Dk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2832Dk, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2832Dk, new Object[]{new Long(j)}));
                break;
        }
        m14605t(j);
    }

    /* renamed from: a */
    public void mo8792a(C3763uj ujVar, long j, String str) {
        super.mo10S();
        m14596a(ujVar);
        m14604s(j);
        m14597a(str);
    }

    @C0064Am(aul = "cab7467edc061840502ab42bea322d47", aum = 0)
    private C3763uj cYi() {
        return cYh();
    }

    @C0064Am(aul = "52049c22a3d68f4423897c33f2e89880", aum = 0)
    /* renamed from: b */
    private void m14600b(C3763uj ujVar) {
        m14596a(ujVar);
    }

    @C0064Am(aul = "548a35ca9d0b50fc6c91a7b6a22dc2c9", aum = 0)
    /* renamed from: lX */
    private long m14603lX() {
        return m14602lT();
    }

    @C0064Am(aul = "af685c29d9b6995d552f8d65d88e018a", aum = 0)
    /* renamed from: t */
    private void m14605t(long j) {
        m14604s(j);
    }

    @C0064Am(aul = "419f72582d19b4d6501811137c253f87", aum = 0)
    /* renamed from: ar */
    private String m14599ar() {
        return m14598ao();
    }

    @C0064Am(aul = "f031ecf74c357f197ebbe619dee625ca", aum = 0)
    /* renamed from: b */
    private void m14601b(String str) {
        m14597a(str);
    }
}
