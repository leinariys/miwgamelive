package game.script;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.mbean.C6757atB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.PW */
/* compiled from: a */
public abstract class TaskletImpl extends TaikodomObject implements C1616Xf, C6920awI, C3161oY.C3162a {

    /* renamed from: JD */
    public static final C2491fm f1375JD = null;

    /* renamed from: KF */
    public static final C5663aRz f1376KF = null;

    /* renamed from: Lc */
    public static final C2491fm f1377Lc = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f1378x13860637 = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dSF = null;
    public static final C5663aRz dSH = null;
    public static final C5663aRz dSJ = null;
    public static final C5663aRz dSL = null;
    public static final C5663aRz dSN = null;
    public static final C5663aRz dSP = null;
    public static final C5663aRz dSR = null;
    public static final C5663aRz dST = null;
    public static final C5663aRz dSV = null;
    public static final C2491fm dSW = null;
    public static final C2491fm dSX = null;
    public static final C2491fm dSY = null;
    public static final C2491fm dSZ = null;
    public static final C2491fm dTa = null;
    public static final C2491fm dTb = null;
    public static final C2491fm dTc = null;
    public static final C2491fm dTd = null;
    public static final C2491fm dTe = null;
    public static final C2491fm dTf = null;
    public static final C2491fm dTg = null;
    public static final C2491fm dTh = null;
    public static final C2491fm dTi = null;
    public static final C2491fm dTj = null;
    public static final C2491fm dTk = null;
    public static final C2491fm dTl = null;
    public static final long serialVersionUID = 0;
    private static final Integer dSD = new Integer(-164723);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "261a708877317283e2f81298f9a8b4c0", aum = 1)
    private static boolean active;
    @C0064Am(aul = "bea96816a58a3d7ef481723c95db6804", aum = 0)
    private static aDJ dSE;
    @C0064Am(aul = "144240ef70873508fe6415f619e50e49", aum = 2)
    private static long dSG;
    @C0064Am(aul = "7ab331132c9b764f94d369d8f3350f29", aum = 3)
    private static long dSI;
    @C0064Am(aul = "ad922a339606c3eaf17c8c9b26634fc1", aum = 4)
    private static int dSK;
    @C0064Am(aul = "fc7be6723eb1f960bc42008375e78fe8", aum = 5)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static int dSM;
    @C0064Am(aul = "e94896763f29c1398567c0f02ebda582", aum = 6)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static long dSO;
    @C0064Am(aul = "f970997d60a54e4d3b95d1f8af4cbeab", aum = 7)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static long dSQ;
    @C0064Am(aul = "9dee5454171e5f7d9876cf59f265f98a", aum = 8)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static int dSS;
    @C0064Am(aul = "39eb41c40a716f8587e2024f672537d5", aum = 9)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static long dSU;

    static {
        m8450V();
    }

    public TaskletImpl() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TaskletImpl(aDJ adj) {
        super((C5540aNg) null);
        super._m_script_init(adj);
    }

    public TaskletImpl(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8450V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 10;
        _m_methodCount = TaikodomObject._m_methodCount + 22;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(TaskletImpl.class, "bea96816a58a3d7ef481723c95db6804", i);
        dSF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TaskletImpl.class, "261a708877317283e2f81298f9a8b4c0", i2);
        f1376KF = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TaskletImpl.class, "144240ef70873508fe6415f619e50e49", i3);
        dSH = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TaskletImpl.class, "7ab331132c9b764f94d369d8f3350f29", i4);
        dSJ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(TaskletImpl.class, "ad922a339606c3eaf17c8c9b26634fc1", i5);
        dSL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(TaskletImpl.class, "fc7be6723eb1f960bc42008375e78fe8", i6);
        dSN = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(TaskletImpl.class, "e94896763f29c1398567c0f02ebda582", i7);
        dSP = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(TaskletImpl.class, "f970997d60a54e4d3b95d1f8af4cbeab", i8);
        dSR = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(TaskletImpl.class, "9dee5454171e5f7d9876cf59f265f98a", i9);
        dST = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(TaskletImpl.class, "39eb41c40a716f8587e2024f672537d5", i10);
        dSV = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i12 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 22)];
        C2491fm a = C4105zY.m41624a(TaskletImpl.class, "8a1a6a179b4e1de03f21565ef70ed3f8", i12);
        f1375JD = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(TaskletImpl.class, "53bbe35de6a03461e61245682fce16c3", i13);
        f1377Lc = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(TaskletImpl.class, "83ff477fa4b7d361d5e72bcf61b0bc1f", i14);
        dSW = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(TaskletImpl.class, "6a82f70bfcb972e66a60a3dc939f3438", i15);
        dSX = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(TaskletImpl.class, "1eb5b2249843c188283c64b66eed0742", i16);
        dSY = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(TaskletImpl.class, "d815bc26eb547163fdea3082911b1bd2", i17);
        dSZ = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(TaskletImpl.class, "946ea7e68c2a341f268d6c9c72e24e40", i18);
        dTa = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(TaskletImpl.class, "a53f51efe6ad142c1c3b79adad43db81", i19);
        dTb = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(TaskletImpl.class, "cec24e94b00e2d7f23bc338617338052", i20);
        dTc = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(TaskletImpl.class, "5f5061d8a5599f6b37ca5ee023d20b86", i21);
        dTd = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(TaskletImpl.class, "9111a4c6fb9f1867fbd7c74f573fa346", i22);
        _f_stop_0020_0028_0029V = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(TaskletImpl.class, "8e1797ff8cdc57b02c52e00721f029d9", i23);
        _f_dispose_0020_0028_0029V = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(TaskletImpl.class, "589159a84672e21ba7aa930029df8a0a", i24);
        dTe = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(TaskletImpl.class, "5af21344450caca04e2353f8cda5e3b9", i25);
        dTf = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(TaskletImpl.class, "defa5ecd7a6c3748045db3ebe0f9dc69", i26);
        dTg = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(TaskletImpl.class, "25deb8cd0306de1fb374f0b72f151b56", i27);
        dTh = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(TaskletImpl.class, "d45dfe574cfb8b688dfd2ee0be574b53", i28);
        dTi = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(TaskletImpl.class, "008033dfbc3f28ad2d1a46bdb779ed3b", i29);
        dTj = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(TaskletImpl.class, "256852a312ba6bdad6e447e7ebc5e181", i30);
        dTk = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(TaskletImpl.class, "9d0b2dcf9f30dc695c4502d300de9ba1", i31);
        f1378x13860637 = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(TaskletImpl.class, "3a3174fa1a39ceace821d271a722da66", i32);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(TaskletImpl.class, "e22b51ca885dbdacdd328d48a78b716d", i33);
        dTl = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TaskletImpl.class, C6757atB.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m8449F(boolean z) {
        bFf().mo5608dq().mo3153a(f1376KF, z);
    }

    @C0064Am(aul = "e22b51ca885dbdacdd328d48a78b716d", aum = 0)
    /* renamed from: W */
    private int m8451W(Object obj) {
        return mo4701d((C6920awI) obj);
    }

    @C0064Am(aul = "a53f51efe6ad142c1c3b79adad43db81", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m8452a(float f, Integer num) {
        throw new aWi(new aCE(this, dTb, new Object[]{new Float(f), num}));
    }

    @C5566aOg
    /* renamed from: b */
    private void m8454b(float f, Integer num) {
        switch (bFf().mo6893i(dTb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTb, new Object[]{new Float(f), num}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTb, new Object[]{new Float(f), num}));
                break;
        }
        m8452a(f, num);
    }

    private aDJ bok() {
        return (aDJ) bFf().mo5608dq().mo3214p(dSF);
    }

    private long bol() {
        return bFf().mo5608dq().mo3213o(dSH);
    }

    private long bom() {
        return bFf().mo5608dq().mo3213o(dSJ);
    }

    private int bon() {
        return bFf().mo5608dq().mo3212n(dSL);
    }

    private int boo() {
        return bFf().mo5608dq().mo3212n(dSN);
    }

    private long bop() {
        return bFf().mo5608dq().mo3213o(dSP);
    }

    private long boq() {
        return bFf().mo5608dq().mo3213o(dSR);
    }

    private int bor() {
        return bFf().mo5608dq().mo3212n(dST);
    }

    private long bos() {
        return bFf().mo5608dq().mo3213o(dSV);
    }

    private boolean bou() {
        switch (bFf().mo6893i(dSW)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dSW, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dSW, new Object[0]));
                break;
        }
        return bot();
    }

    private void bow() {
        switch (bFf().mo6893i(dSX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dSX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dSX, new Object[0]));
                break;
        }
        bov();
    }

    @C0064Am(aul = "cec24e94b00e2d7f23bc338617338052", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m8457c(float f, int i) {
        throw new aWi(new aCE(this, dTc, new Object[]{new Float(f), new Integer(i)}));
    }

    /* renamed from: fr */
    private void m8459fr(long j) {
        bFf().mo5608dq().mo3184b(dSH, j);
    }

    /* renamed from: fs */
    private void m8460fs(long j) {
        bFf().mo5608dq().mo3184b(dSJ, j);
    }

    /* renamed from: ft */
    private void m8461ft(long j) {
        bFf().mo5608dq().mo3184b(dSP, j);
    }

    /* renamed from: fu */
    private void m8462fu(long j) {
        bFf().mo5608dq().mo3184b(dSR, j);
    }

    /* renamed from: fv */
    private void m8463fv(long j) {
        bFf().mo5608dq().mo3184b(dSV, j);
    }

    @C0064Am(aul = "589159a84672e21ba7aa930029df8a0a", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m8464g(long j, int i) {
        throw new aWi(new aCE(this, dTe, new Object[]{new Long(j), new Integer(i)}));
    }

    @C0064Am(aul = "5f5061d8a5599f6b37ca5ee023d20b86", aum = 0)
    @C5566aOg
    /* renamed from: hj */
    private void m8465hj(float f) {
        throw new aWi(new aCE(this, dTd, new Object[]{new Float(f)}));
    }

    /* renamed from: k */
    private void m8466k(aDJ adj) {
        bFf().mo5608dq().mo3197f(dSF, adj);
    }

    /* renamed from: mJ */
    private void m8467mJ(int i) {
        bFf().mo5608dq().mo3183b(dSL, i);
    }

    /* renamed from: mK */
    private void m8468mK(int i) {
        bFf().mo5608dq().mo3183b(dSN, i);
    }

    /* renamed from: mL */
    private void m8469mL(int i) {
        bFf().mo5608dq().mo3183b(dST, i);
    }

    @C0064Am(aul = "8a1a6a179b4e1de03f21565ef70ed3f8", aum = 0)
    /* renamed from: pi */
    private void m8471pi() {
        throw new aWi(new aCE(this, f1375JD, new Object[0]));
    }

    @C0064Am(aul = "9111a4c6fb9f1867fbd7c74f573fa346", aum = 0)
    @C5566aOg
    /* renamed from: qO */
    private void m8472qO() {
        throw new aWi(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
    }

    /* renamed from: qv */
    private boolean m8474qv() {
        return bFf().mo5608dq().mo3201h(f1376KF);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6757atB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m8471pi();
                return null;
            case 1:
                return new Boolean(m8473qP());
            case 2:
                return new Boolean(bot());
            case 3:
                bov();
                return null;
            case 4:
                m8470mM(((Integer) args[0]).intValue());
                return null;
            case 5:
                return new Long(box());
            case 6:
                return new Long(boz());
            case 7:
                m8452a(((Float) args[0]).floatValue(), (Integer) args[1]);
                return null;
            case 8:
                m8457c(((Float) args[0]).floatValue(), ((Integer) args[1]).intValue());
                return null;
            case 9:
                m8465hj(((Float) args[0]).floatValue());
                return null;
            case 10:
                m8472qO();
                return null;
            case 11:
                m8458fg();
                return null;
            case 12:
                m8464g(((Long) args[0]).longValue(), ((Integer) args[1]).intValue());
                return null;
            case 13:
                return new Boolean(boB());
            case 14:
                return new Integer(m8456c((C6920awI) args[0]));
            case 15:
                return new Float(boD());
            case 16:
                return new Float(boF());
            case 17:
                return new Integer(boH());
            case 18:
                return new Long(boJ());
            case 19:
                m8455b((aDJ) args[0]);
                return null;
            case 20:
                return m8453au();
            case 21:
                return new Integer(m8451W(args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public long boA() {
        switch (bFf().mo6893i(dTa)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dTa, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTa, new Object[0]));
                break;
        }
        return boz();
    }

    /* access modifiers changed from: protected */
    public boolean boC() {
        switch (bFf().mo6893i(dTf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dTf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTf, new Object[0]));
                break;
        }
        return boB();
    }

    public final float boE() {
        switch (bFf().mo6893i(dTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTh, new Object[0]));
                break;
        }
        return boD();
    }

    public final float boG() {
        switch (bFf().mo6893i(dTi)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dTi, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTi, new Object[0]));
                break;
        }
        return boF();
    }

    public final int boI() {
        switch (bFf().mo6893i(dTj)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dTj, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTj, new Object[0]));
                break;
        }
        return boH();
    }

    public long boK() {
        switch (bFf().mo6893i(dTk)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dTk, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTk, new Object[0]));
                break;
        }
        return boJ();
    }

    public long boy() {
        switch (bFf().mo6893i(dSZ)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dSZ, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dSZ, new Object[0]));
                break;
        }
        return box();
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f1378x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1378x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1378x13860637, new Object[]{adj}));
                break;
        }
        m8455b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        switch (bFf().mo6893i(dTl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dTl, new Object[]{obj}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTl, new Object[]{obj}));
                break;
        }
        return m8451W(obj);
    }

    /* renamed from: d */
    public final int mo4701d(C6920awI awi) {
        switch (bFf().mo6893i(dTg)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dTg, new Object[]{awi}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTg, new Object[]{awi}));
                break;
        }
        return m8456c(awi);
    }

    @C5566aOg
    /* renamed from: d */
    public final void mo4702d(float f, int i) {
        switch (bFf().mo6893i(dTc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTc, new Object[]{new Float(f), new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTc, new Object[]{new Float(f), new Integer(i)}));
                break;
        }
        m8457c(f, i);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m8458fg();
    }

    @C5566aOg
    /* renamed from: h */
    public final void mo4703h(long j, int i) {
        switch (bFf().mo6893i(dTe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTe, new Object[]{new Long(j), new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTe, new Object[]{new Long(j), new Integer(i)}));
                break;
        }
        m8464g(j, i);
    }

    @C5566aOg
    /* renamed from: hk */
    public final void mo4704hk(float f) {
        switch (bFf().mo6893i(dTd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTd, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTd, new Object[]{new Float(f)}));
                break;
        }
        m8465hj(f);
    }

    public boolean isActive() {
        switch (bFf().mo6893i(f1377Lc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1377Lc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1377Lc, new Object[0]));
                break;
        }
        return m8473qP();
    }

    /* renamed from: mN */
    public void mo4707mN(int i) {
        switch (bFf().mo6893i(dSY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dSY, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dSY, new Object[]{new Integer(i)}));
                break;
        }
        m8470mM(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: pj */
    public void mo667pj() {
        switch (bFf().mo6893i(f1375JD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1375JD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1375JD, new Object[0]));
                break;
        }
        m8471pi();
    }

    @C5566aOg
    public final void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m8472qO();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m8453au();
    }

    /* renamed from: l */
    public void mo4706l(aDJ adj) {
        super.mo10S();
        m8460fs(-1);
        adj.mo8348d(C3161oY.C3162a.class, this);
        m8466k(adj);
    }

    @C0064Am(aul = "53bbe35de6a03461e61245682fce16c3", aum = 0)
    /* renamed from: qP */
    private boolean m8473qP() {
        return m8474qv();
    }

    @C0064Am(aul = "83ff477fa4b7d361d5e72bcf61b0bc1f", aum = 0)
    private boolean bot() {
        return m8474qv() && (bon() == dSD.intValue() || bon() > 0);
    }

    @C0064Am(aul = "6a82f70bfcb972e66a60a3dc939f3438", aum = 0)
    private void bov() {
        if (bon() != dSD.intValue()) {
            m8467mJ(bon() - 1);
        }
    }

    @C0064Am(aul = "1eb5b2249843c188283c64b66eed0742", aum = 0)
    /* renamed from: mM */
    private void m8470mM(int i) {
        m8468mK(i);
    }

    @C0064Am(aul = "d815bc26eb547163fdea3082911b1bd2", aum = 0)
    private long box() {
        return bol();
    }

    @C0064Am(aul = "946ea7e68c2a341f268d6c9c72e24e40", aum = 0)
    private long boz() {
        return bom() + bol();
    }

    @C0064Am(aul = "8e1797ff8cdc57b02c52e00721f029d9", aum = 0)
    /* renamed from: fg */
    private void m8458fg() {
        stop();
        m8466k((aDJ) null);
        super.dispose();
    }

    @C0064Am(aul = "5af21344450caca04e2353f8cda5e3b9", aum = 0)
    private boolean boB() {
        return false;
    }

    @C0064Am(aul = "defa5ecd7a6c3748045db3ebe0f9dc69", aum = 0)
    /* renamed from: c */
    private int m8456c(C6920awI awi) {
        if (awi == this) {
            return 0;
        }
        if (boA() == awi.boA()) {
            return mo8317Ej() > awi.mo16678Ej() ? 1 : -1;
        }
        return Long.valueOf(boA()).compareTo(Long.valueOf(awi.boA()));
    }

    @C0064Am(aul = "25deb8cd0306de1fb374f0b72f151b56", aum = 0)
    private float boD() {
        if (bop() == 0) {
            return 0.0f;
        }
        return ((float) boq()) / ((float) bop());
    }

    @C0064Am(aul = "d45dfe574cfb8b688dfd2ee0be574b53", aum = 0)
    private float boF() {
        if (bor() == 0) {
            return 0.0f;
        }
        return ((float) bos()) / ((float) bor());
    }

    @C0064Am(aul = "008033dfbc3f28ad2d1a46bdb779ed3b", aum = 0)
    private int boH() {
        return bor();
    }

    @C0064Am(aul = "256852a312ba6bdad6e447e7ebc5e181", aum = 0)
    private long boJ() {
        return bop();
    }

    @C0064Am(aul = "9d0b2dcf9f30dc695c4502d300de9ba1", aum = 0)
    /* renamed from: b */
    private void m8455b(aDJ adj) {
        if (adj == bok()) {
            dispose();
        }
    }

    @C0064Am(aul = "3a3174fa1a39ceace821d271a722da66", aum = 0)
    /* renamed from: au */
    private String m8453au() {
        return super.bFY();
    }
}
