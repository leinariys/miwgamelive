package game.script.nls;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.view.TipString;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0121BX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.aRi  reason: case insensitive filesystem */
/* compiled from: a */
public class NLSResourceLoader extends NLSType implements C1616Xf {
    public static final C2491fm _f_push_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eSo = null;
    public static final C2491fm eSp = null;
    public static final C2491fm eSq = null;
    public static final C5663aRz iGU = null;
    public static final C2491fm iGV = null;
    public static final C2491fm iGW = null;
    public static final C2491fm iGX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4ed7212e371268200ec76c74e6d0d8c4", aum = 0)

    /* renamed from: Rs */
    private static I18NString f3716Rs;
    @C0064Am(aul = "5862e953bf243bcddfd7008035976e5a", aum = 1)
    private static C3438ra<TipString> cuE;

    static {
        m17903V();
    }

    public NLSResourceLoader() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSResourceLoader(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17903V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 2;
        _m_methodCount = NLSType._m_methodCount + 7;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(NLSResourceLoader.class, "4ed7212e371268200ec76c74e6d0d8c4", i);
        eSo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSResourceLoader.class, "5862e953bf243bcddfd7008035976e5a", i2);
        iGU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i4 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(NLSResourceLoader.class, "f7f541f971b4c319672a0f1c9aa504f6", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSResourceLoader.class, "85dd22aa388a633da194ed5cfa6a9a9a", i5);
        eSp = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSResourceLoader.class, "09734873edd4a0ac149ce69ba46d2a9c", i6);
        eSq = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSResourceLoader.class, "e410671dd4be5e0cf621464357fd0dba", i7);
        iGV = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSResourceLoader.class, "b140a582b560ba263bd3391e69c7c02a", i8);
        iGW = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSResourceLoader.class, "c4badddcc1136bb5c8dc5281d45dc767", i9);
        iGX = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSResourceLoader.class, "5495b4b5653c9a544d8c8954a032586e", i10);
        _f_push_0020_0028_0029V = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSResourceLoader.class, C0121BX.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Tips")
    @C0064Am(aul = "e410671dd4be5e0cf621464357fd0dba", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m17904a(TipString aae) {
        throw new aWi(new aCE(this, iGV, new Object[]{aae}));
    }

    private I18NString bJJ() {
        return (I18NString) bFf().mo5608dq().mo3214p(eSo);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Tips")
    @C0064Am(aul = "b140a582b560ba263bd3391e69c7c02a", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m17906c(TipString aae) {
        throw new aWi(new aCE(this, iGW, new Object[]{aae}));
    }

    /* renamed from: cz */
    private void m17907cz(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iGU, raVar);
    }

    private C3438ra drn() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iGU);
    }

    /* renamed from: kn */
    private void m17908kn(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(eSo, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tip label")
    @C0064Am(aul = "09734873edd4a0ac149ce69ba46d2a9c", aum = 0)
    @C5566aOg
    /* renamed from: ko */
    private void m17909ko(I18NString i18NString) {
        throw new aWi(new aCE(this, eSq, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0121BX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m17905au();
            case 1:
                return bJK();
            case 2:
                m17909ko((I18NString) args[0]);
                return null;
            case 3:
                m17904a((TipString) args[0]);
                return null;
            case 4:
                m17906c((TipString) args[0]);
                return null;
            case 5:
                return dro();
            case 6:
                m17902Gd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Tips")
    @C5566aOg
    /* renamed from: b */
    public void mo11183b(TipString aae) {
        switch (bFf().mo6893i(iGV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGV, new Object[]{aae}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGV, new Object[]{aae}));
                break;
        }
        m17904a(aae);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tip label")
    public I18NString bJL() {
        switch (bFf().mo6893i(eSp)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, eSp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eSp, new Object[0]));
                break;
        }
        return bJK();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Tips")
    @C5566aOg
    /* renamed from: d */
    public void mo11185d(TipString aae) {
        switch (bFf().mo6893i(iGW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iGW, new Object[]{aae}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iGW, new Object[]{aae}));
                break;
        }
        m17906c(aae);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Tips")
    public List<TipString> drp() {
        switch (bFf().mo6893i(iGX)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iGX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGX, new Object[0]));
                break;
        }
        return dro();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tip label")
    @C5566aOg
    /* renamed from: kp */
    public void mo11187kp(I18NString i18NString) {
        switch (bFf().mo6893i(eSq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eSq, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eSq, new Object[]{i18NString}));
                break;
        }
        m17909ko(i18NString);
    }

    public void push() {
        switch (bFf().mo6893i(_f_push_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                break;
        }
        m17902Gd();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m17905au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f7f541f971b4c319672a0f1c9aa504f6", aum = 0)
    /* renamed from: au */
    private String m17905au() {
        return "Resource Loader View";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tip label")
    @C0064Am(aul = "85dd22aa388a633da194ed5cfa6a9a9a", aum = 0)
    private I18NString bJK() {
        return bJJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Tips")
    @C0064Am(aul = "c4badddcc1136bb5c8dc5281d45dc767", aum = 0)
    private List<TipString> dro() {
        return Collections.unmodifiableList(drn());
    }

    @C0064Am(aul = "5495b4b5653c9a544d8c8954a032586e", aum = 0)
    /* renamed from: Gd */
    private void m17902Gd() {
        super.push();
        for (TipString push : drn()) {
            push.push();
        }
    }
}
