package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2775jp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aLe  reason: case insensitive filesystem */
/* compiled from: a */
public class NLSInteraction extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bnH = null;
    public static final C5663aRz ijY = null;
    public static final C5663aRz ijZ = null;
    public static final C2491fm ikA = null;
    public static final C2491fm ikB = null;
    public static final C2491fm ikC = null;
    public static final C2491fm ikD = null;
    public static final C2491fm ikE = null;
    public static final C2491fm ikF = null;
    public static final C2491fm ikG = null;
    public static final C5663aRz ika = null;
    public static final C5663aRz ikb = null;
    public static final C5663aRz ikc = null;
    public static final C5663aRz ikd = null;
    public static final C5663aRz ike = null;
    public static final C5663aRz ikf = null;
    public static final C5663aRz ikg = null;
    public static final C5663aRz ikh = null;
    public static final C5663aRz iki = null;
    public static final C2491fm ikj = null;
    public static final C2491fm ikk = null;
    public static final C2491fm ikl = null;
    public static final C2491fm ikm = null;
    public static final C2491fm ikn = null;
    public static final C2491fm iko = null;
    public static final C2491fm ikp = null;
    public static final C2491fm ikq = null;
    public static final C2491fm ikr = null;
    public static final C2491fm iks = null;
    public static final C2491fm ikt = null;
    public static final C2491fm iku = null;
    public static final C2491fm ikv = null;
    public static final C2491fm ikw = null;
    public static final C2491fm ikx = null;
    public static final C2491fm iky = null;
    public static final C2491fm ikz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d87baadc6a7331faf8821c50722bdd81", aum = 1)
    private static I18NString ajA;
    @C0064Am(aul = "7f91ebc1b9336bc5467767c35f2807c8", aum = 2)
    private static I18NString ajB;
    @C0064Am(aul = "20cee661201ca29734d5dcf8730edd18", aum = 3)
    private static I18NString ajC;
    @C0064Am(aul = "60e2edd623e385bd92eefea1a021d0bc", aum = 4)
    private static I18NString ajD;
    @C0064Am(aul = "1a3f15081c15832559aa477711980184", aum = 5)
    private static I18NString ajE;
    @C0064Am(aul = "e2e349b60778e19d2dc671e1b95efb9f", aum = 6)
    private static I18NString ajF;
    @C0064Am(aul = "6b2006ea287d15fa291a36e0a91c59fc", aum = 7)
    private static I18NString ajG;
    @C0064Am(aul = "515d81bfc40d529862fe14586ed1358b", aum = 8)
    private static I18NString ajH;
    @C0064Am(aul = "7f3a68f19e7cace0c2088dbfd79909cb", aum = 9)
    private static I18NString ajI;
    @C0064Am(aul = "44701e7353e74545810b96f922f13b6c", aum = 10)
    private static I18NString ajJ;
    @C0064Am(aul = "068a89108be3a95696191c923d81b630", aum = 11)
    private static I18NString ajK;
    @C0064Am(aul = "becdefed20bf976e1e792c2d90d62a5a", aum = 0)
    private static I18NString ajz;

    static {
        m16218V();
    }

    public NLSInteraction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSInteraction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16218V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 12;
        _m_methodCount = NLSType._m_methodCount + 25;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(NLSInteraction.class, "becdefed20bf976e1e792c2d90d62a5a", i);
        ijY = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSInteraction.class, "d87baadc6a7331faf8821c50722bdd81", i2);
        bnH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSInteraction.class, "7f91ebc1b9336bc5467767c35f2807c8", i3);
        ijZ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSInteraction.class, "20cee661201ca29734d5dcf8730edd18", i4);
        ika = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NLSInteraction.class, "60e2edd623e385bd92eefea1a021d0bc", i5);
        ikb = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NLSInteraction.class, "1a3f15081c15832559aa477711980184", i6);
        ikc = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NLSInteraction.class, "e2e349b60778e19d2dc671e1b95efb9f", i7);
        ikd = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NLSInteraction.class, "6b2006ea287d15fa291a36e0a91c59fc", i8);
        ike = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NLSInteraction.class, "515d81bfc40d529862fe14586ed1358b", i9);
        ikf = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(NLSInteraction.class, "7f3a68f19e7cace0c2088dbfd79909cb", i10);
        ikg = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(NLSInteraction.class, "44701e7353e74545810b96f922f13b6c", i11);
        ikh = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(NLSInteraction.class, "068a89108be3a95696191c923d81b630", i12);
        iki = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i14 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 25)];
        C2491fm a = C4105zY.m41624a(NLSInteraction.class, "9e0e15efd36b7259c18857a8e8a3cff5", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSInteraction.class, "16377a0c653b1215e2b0e6253d034821", i15);
        ikj = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSInteraction.class, "2118633e30c244662c226b09712e7b38", i16);
        ikk = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSInteraction.class, "a278f739927e1bb49310efecf8ba27a1", i17);
        ikl = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSInteraction.class, "d75feef2e689d721879090ca2cac68d1", i18);
        ikm = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSInteraction.class, "4e97174b9c360cbd2753c0827c26fb07", i19);
        ikn = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSInteraction.class, "0fb11a86574b84cf40c44e7541860d32", i20);
        iko = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSInteraction.class, "653423ef9a72a5eacd986e86580d329c", i21);
        ikp = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSInteraction.class, "ba787bd1777a99b359c0ad4e920524c9", i22);
        ikq = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSInteraction.class, "763411bcc2a65ea1362a4b4279d4ab95", i23);
        ikr = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSInteraction.class, "bcd3f36eb7b4393cc7b200925b610303", i24);
        iks = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(NLSInteraction.class, "2e25969bfc18af6213c4daab914fd12b", i25);
        ikt = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(NLSInteraction.class, "2dc0c7ba65026aef153fcfaec64bda4d", i26);
        iku = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(NLSInteraction.class, "36fe7d121425aca4f7cbd97e60c1d29c", i27);
        ikv = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(NLSInteraction.class, "70590fae6b20faaba19e327e69d9d303", i28);
        ikw = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(NLSInteraction.class, "e97e7c456a7046886d7b86230cfae165", i29);
        ikx = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(NLSInteraction.class, "b6a7d08813b67039fe1daaffc2afe0c4", i30);
        iky = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(NLSInteraction.class, "6e95b2b3bd89ba8fcce34d8fc842f807", i31);
        ikz = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(NLSInteraction.class, "57595e6094feb996ecd27d2c297eeeb1", i32);
        ikA = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(NLSInteraction.class, "94c1a799b13be08c0005015bfcec7bba", i33);
        ikB = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(NLSInteraction.class, "d1a4dbc296606d578f8b04da1460e2c6", i34);
        ikC = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(NLSInteraction.class, "358fbea708c404f7bf499d6023d97606", i35);
        ikD = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(NLSInteraction.class, "e5647cf16a3114d808c6ac40d5d32c79", i36);
        ikE = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        C2491fm a24 = C4105zY.m41624a(NLSInteraction.class, "67671b39d55a4369dc408ce0997531c7", i37);
        ikF = a24;
        fmVarArr[i37] = a24;
        int i38 = i37 + 1;
        C2491fm a25 = C4105zY.m41624a(NLSInteraction.class, "666a8351170d2c1e33df5f6af129ae80", i38);
        ikG = a25;
        fmVarArr[i38] = a25;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSInteraction.class, C2775jp.class, _m_fields, _m_methods);
    }

    private I18NString dgS() {
        return (I18NString) bFf().mo5608dq().mo3214p(ijY);
    }

    private I18NString dgT() {
        return (I18NString) bFf().mo5608dq().mo3214p(bnH);
    }

    private I18NString dgU() {
        return (I18NString) bFf().mo5608dq().mo3214p(ijZ);
    }

    private I18NString dgV() {
        return (I18NString) bFf().mo5608dq().mo3214p(ika);
    }

    private I18NString dgW() {
        return (I18NString) bFf().mo5608dq().mo3214p(ikb);
    }

    private I18NString dgX() {
        return (I18NString) bFf().mo5608dq().mo3214p(ikc);
    }

    private I18NString dgY() {
        return (I18NString) bFf().mo5608dq().mo3214p(ikd);
    }

    private I18NString dgZ() {
        return (I18NString) bFf().mo5608dq().mo3214p(ike);
    }

    private I18NString dha() {
        return (I18NString) bFf().mo5608dq().mo3214p(ikf);
    }

    private I18NString dhb() {
        return (I18NString) bFf().mo5608dq().mo3214p(ikg);
    }

    private I18NString dhc() {
        return (I18NString) bFf().mo5608dq().mo3214p(ikh);
    }

    private I18NString dhd() {
        return (I18NString) bFf().mo5608dq().mo3214p(iki);
    }

    /* renamed from: pL */
    private void m16220pL(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ijY, i18NString);
    }

    /* renamed from: pM */
    private void m16221pM(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(bnH, i18NString);
    }

    /* renamed from: pN */
    private void m16222pN(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ijZ, i18NString);
    }

    /* renamed from: pO */
    private void m16223pO(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ika, i18NString);
    }

    /* renamed from: pP */
    private void m16224pP(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ikb, i18NString);
    }

    /* renamed from: pQ */
    private void m16225pQ(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ikc, i18NString);
    }

    /* renamed from: pR */
    private void m16226pR(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ikd, i18NString);
    }

    /* renamed from: pS */
    private void m16227pS(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ike, i18NString);
    }

    /* renamed from: pT */
    private void m16228pT(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ikf, i18NString);
    }

    /* renamed from: pU */
    private void m16229pU(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ikg, i18NString);
    }

    /* renamed from: pV */
    private void m16230pV(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ikh, i18NString);
    }

    /* renamed from: pW */
    private void m16231pW(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iki, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Corporation Exclusive")
    @C0064Am(aul = "2118633e30c244662c226b09712e7b38", aum = 0)
    @C5566aOg
    /* renamed from: pX */
    private void m16232pX(I18NString i18NString) {
        throw new aWi(new aCE(this, ikk, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Banned Faction")
    @C0064Am(aul = "d75feef2e689d721879090ca2cac68d1", aum = 0)
    @C5566aOg
    /* renamed from: pZ */
    private void m16233pZ(I18NString i18NString) {
        throw new aWi(new aCE(this, ikm, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Can't get loot")
    @C0064Am(aul = "0fb11a86574b84cf40c44e7541860d32", aum = 0)
    @C5566aOg
    /* renamed from: qb */
    private void m16234qb(I18NString i18NString) {
        throw new aWi(new aCE(this, iko, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cruise speed")
    @C0064Am(aul = "ba787bd1777a99b359c0ad4e920524c9", aum = 0)
    @C5566aOg
    /* renamed from: qd */
    private void m16235qd(I18NString i18NString) {
        throw new aWi(new aCE(this, ikq, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Deactivated")
    @C0064Am(aul = "bcd3f36eb7b4393cc7b200925b610303", aum = 0)
    @C5566aOg
    /* renamed from: qf */
    private void m16236qf(I18NString i18NString) {
        throw new aWi(new aCE(this, iks, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Object in use")
    @C0064Am(aul = "2dc0c7ba65026aef153fcfaec64bda4d", aum = 0)
    @C5566aOg
    /* renamed from: qh */
    private void m16237qh(I18NString i18NString) {
        throw new aWi(new aCE(this, iku, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Out of range")
    @C0064Am(aul = "70590fae6b20faaba19e327e69d9d303", aum = 0)
    @C5566aOg
    /* renamed from: qj */
    private void m16238qj(I18NString i18NString) {
        throw new aWi(new aCE(this, ikw, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Not enough space")
    @C0064Am(aul = "b6a7d08813b67039fe1daaffc2afe0c4", aum = 0)
    @C5566aOg
    /* renamed from: ql */
    private void m16239ql(I18NString i18NString) {
        throw new aWi(new aCE(this, iky, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Not enough money for toll")
    @C0064Am(aul = "57595e6094feb996ecd27d2c297eeeb1", aum = 0)
    @C5566aOg
    /* renamed from: qn */
    private void m16240qn(I18NString i18NString) {
        throw new aWi(new aCE(this, ikA, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Restricted Access")
    @C0064Am(aul = "d1a4dbc296606d578f8b04da1460e2c6", aum = 0)
    @C5566aOg
    /* renamed from: qp */
    private void m16241qp(I18NString i18NString) {
        throw new aWi(new aCE(this, ikC, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Citizenship Only")
    @C0064Am(aul = "e5647cf16a3114d808c6ac40d5d32c79", aum = 0)
    @C5566aOg
    /* renamed from: qr */
    private void m16242qr(I18NString i18NString) {
        throw new aWi(new aCE(this, ikE, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Pass Needed")
    @C0064Am(aul = "666a8351170d2c1e33df5f6af129ae80", aum = 0)
    @C5566aOg
    /* renamed from: qt */
    private void m16243qt(I18NString i18NString) {
        throw new aWi(new aCE(this, ikG, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2775jp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m16219au();
            case 1:
                return dhe();
            case 2:
                m16232pX((I18NString) args[0]);
                return null;
            case 3:
                return dhg();
            case 4:
                m16233pZ((I18NString) args[0]);
                return null;
            case 5:
                return dhi();
            case 6:
                m16234qb((I18NString) args[0]);
                return null;
            case 7:
                return dhk();
            case 8:
                m16235qd((I18NString) args[0]);
                return null;
            case 9:
                return dhm();
            case 10:
                m16236qf((I18NString) args[0]);
                return null;
            case 11:
                return dho();
            case 12:
                m16237qh((I18NString) args[0]);
                return null;
            case 13:
                return dhq();
            case 14:
                m16238qj((I18NString) args[0]);
                return null;
            case 15:
                return dhs();
            case 16:
                m16239ql((I18NString) args[0]);
                return null;
            case 17:
                return dhu();
            case 18:
                m16240qn((I18NString) args[0]);
                return null;
            case 19:
                return dhw();
            case 20:
                m16241qp((I18NString) args[0]);
                return null;
            case 21:
                return dhy();
            case 22:
                m16242qr((I18NString) args[0]);
                return null;
            case 23:
                return dhA();
            case 24:
                m16243qt((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Pass Needed")
    public I18NString dhB() {
        switch (bFf().mo6893i(ikF)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikF, new Object[0]));
                break;
        }
        return dhA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corporation Exclusive")
    public I18NString dhf() {
        switch (bFf().mo6893i(ikj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikj, new Object[0]));
                break;
        }
        return dhe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Banned Faction")
    public I18NString dhh() {
        switch (bFf().mo6893i(ikl)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikl, new Object[0]));
                break;
        }
        return dhg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Can't get loot")
    public I18NString dhj() {
        switch (bFf().mo6893i(ikn)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikn, new Object[0]));
                break;
        }
        return dhi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cruise speed")
    public I18NString dhl() {
        switch (bFf().mo6893i(ikp)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikp, new Object[0]));
                break;
        }
        return dhk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Deactivated")
    public I18NString dhn() {
        switch (bFf().mo6893i(ikr)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikr, new Object[0]));
                break;
        }
        return dhm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Object in use")
    public I18NString dhp() {
        switch (bFf().mo6893i(ikt)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikt, new Object[0]));
                break;
        }
        return dho();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Out of range")
    public I18NString dhr() {
        switch (bFf().mo6893i(ikv)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikv, new Object[0]));
                break;
        }
        return dhq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Not enough space")
    public I18NString dht() {
        switch (bFf().mo6893i(ikx)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikx, new Object[0]));
                break;
        }
        return dhs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Not enough money for toll")
    public I18NString dhv() {
        switch (bFf().mo6893i(ikz)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikz, new Object[0]));
                break;
        }
        return dhu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Restricted Access")
    public I18NString dhx() {
        switch (bFf().mo6893i(ikB)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikB, new Object[0]));
                break;
        }
        return dhw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Citizenship Only")
    public I18NString dhz() {
        switch (bFf().mo6893i(ikD)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ikD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ikD, new Object[0]));
                break;
        }
        return dhy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Corporation Exclusive")
    @C5566aOg
    /* renamed from: pY */
    public void mo9922pY(I18NString i18NString) {
        switch (bFf().mo6893i(ikk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikk, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikk, new Object[]{i18NString}));
                break;
        }
        m16232pX(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Banned Faction")
    @C5566aOg
    /* renamed from: qa */
    public void mo9923qa(I18NString i18NString) {
        switch (bFf().mo6893i(ikm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikm, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikm, new Object[]{i18NString}));
                break;
        }
        m16233pZ(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Can't get loot")
    @C5566aOg
    /* renamed from: qc */
    public void mo9924qc(I18NString i18NString) {
        switch (bFf().mo6893i(iko)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iko, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iko, new Object[]{i18NString}));
                break;
        }
        m16234qb(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cruise speed")
    @C5566aOg
    /* renamed from: qe */
    public void mo9925qe(I18NString i18NString) {
        switch (bFf().mo6893i(ikq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikq, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikq, new Object[]{i18NString}));
                break;
        }
        m16235qd(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Deactivated")
    @C5566aOg
    /* renamed from: qg */
    public void mo9926qg(I18NString i18NString) {
        switch (bFf().mo6893i(iks)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iks, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iks, new Object[]{i18NString}));
                break;
        }
        m16236qf(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Object in use")
    @C5566aOg
    /* renamed from: qi */
    public void mo9927qi(I18NString i18NString) {
        switch (bFf().mo6893i(iku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iku, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iku, new Object[]{i18NString}));
                break;
        }
        m16237qh(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Out of range")
    @C5566aOg
    /* renamed from: qk */
    public void mo9928qk(I18NString i18NString) {
        switch (bFf().mo6893i(ikw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikw, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikw, new Object[]{i18NString}));
                break;
        }
        m16238qj(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Not enough space")
    @C5566aOg
    /* renamed from: qm */
    public void mo9929qm(I18NString i18NString) {
        switch (bFf().mo6893i(iky)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iky, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iky, new Object[]{i18NString}));
                break;
        }
        m16239ql(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Not enough money for toll")
    @C5566aOg
    /* renamed from: qo */
    public void mo9930qo(I18NString i18NString) {
        switch (bFf().mo6893i(ikA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikA, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikA, new Object[]{i18NString}));
                break;
        }
        m16240qn(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Restricted Access")
    @C5566aOg
    /* renamed from: qq */
    public void mo9931qq(I18NString i18NString) {
        switch (bFf().mo6893i(ikC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikC, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikC, new Object[]{i18NString}));
                break;
        }
        m16241qp(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Citizenship Only")
    @C5566aOg
    /* renamed from: qs */
    public void mo9932qs(I18NString i18NString) {
        switch (bFf().mo6893i(ikE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikE, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikE, new Object[]{i18NString}));
                break;
        }
        m16242qr(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Pass Needed")
    @C5566aOg
    /* renamed from: qu */
    public void mo9933qu(I18NString i18NString) {
        switch (bFf().mo6893i(ikG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ikG, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ikG, new Object[]{i18NString}));
                break;
        }
        m16243qt(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m16219au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "9e0e15efd36b7259c18857a8e8a3cff5", aum = 0)
    /* renamed from: au */
    private String m16219au() {
        return "Interaction";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corporation Exclusive")
    @C0064Am(aul = "16377a0c653b1215e2b0e6253d034821", aum = 0)
    private I18NString dhe() {
        return dgV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Banned Faction")
    @C0064Am(aul = "a278f739927e1bb49310efecf8ba27a1", aum = 0)
    private I18NString dhg() {
        return dgU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Can't get loot")
    @C0064Am(aul = "4e97174b9c360cbd2753c0827c26fb07", aum = 0)
    private I18NString dhi() {
        return dgW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cruise speed")
    @C0064Am(aul = "653423ef9a72a5eacd986e86580d329c", aum = 0)
    private I18NString dhk() {
        return dgT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Deactivated")
    @C0064Am(aul = "763411bcc2a65ea1362a4b4279d4ab95", aum = 0)
    private I18NString dhm() {
        return dgS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Object in use")
    @C0064Am(aul = "2e25969bfc18af6213c4daab914fd12b", aum = 0)
    private I18NString dho() {
        return dgY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Out of range")
    @C0064Am(aul = "36fe7d121425aca4f7cbd97e60c1d29c", aum = 0)
    private I18NString dhq() {
        return dgX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Not enough space")
    @C0064Am(aul = "e97e7c456a7046886d7b86230cfae165", aum = 0)
    private I18NString dhs() {
        return dgZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Not enough money for toll")
    @C0064Am(aul = "6e95b2b3bd89ba8fcce34d8fc842f807", aum = 0)
    private I18NString dhu() {
        return dha();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Restricted Access")
    @C0064Am(aul = "94c1a799b13be08c0005015bfcec7bba", aum = 0)
    private I18NString dhw() {
        return dhb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Citizenship Only")
    @C0064Am(aul = "358fbea708c404f7bf499d6023d97606", aum = 0)
    private I18NString dhy() {
        return dhc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Pass Needed")
    @C0064Am(aul = "67671b39d55a4369dc408ce0997531c7", aum = 0)
    private I18NString dhA() {
        return dhd();
    }
}
