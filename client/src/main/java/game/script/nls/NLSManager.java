package game.script.nls;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.corporation.NLSCorporation;
import gnu.trove.THashSet;
import logic.baa.*;
import logic.data.mbean.axD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.addon.TaikodomAddon;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5829abJ("1.0.2")
@C6485anp
@C5511aMd
/* renamed from: a.Vf */
/* compiled from: a */
public class NLSManager extends aDJ implements C0468GU, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f1896Do = null;
    public static final C2491fm _f_loggerWarning_0020_0028Ljava_002flang_002fString_003b_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1898bL = null;
    /* renamed from: bN */
    public static final C2491fm f1899bN = null;
    /* renamed from: bO */
    public static final C2491fm f1900bO = null;
    /* renamed from: bP */
    public static final C2491fm f1901bP = null;
    public static final C2491fm gNA = null;
    public static final C2491fm gNB = null;
    public static final C2491fm gNC = null;
    public static final C2491fm gND = null;
    public static final C2491fm gNE = null;
    public static final C2491fm gNF = null;
    public static final C2491fm gNG = null;
    public static final C2491fm gNH = null;
    public static final C2491fm gNI = null;
    public static final C2491fm gNJ = null;
    public static final C2491fm gNK = null;
    public static final C2491fm gNL = null;
    public static final C2491fm gNM = null;
    public static final I18NString[] gNs;
    public static final C5663aRz gNu = null;
    public static final C5663aRz gNw = null;
    public static final C5663aRz gNy = null;
    public static final long serialVersionUID = 0;
    private static final I18NString gNo;
    private static final I18NString gNp;
    private static final I18NString gNq;
    private static final I18NString gNr;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2d523df96815d9185ed2973947967c36", aum = 0)

    /* renamed from: bK */
    private static UUID f1897bK;
    @C0064Am(aul = "d99cabc409cbb5fa7ab09dce7121c991", aum = 1)
    private static C1556Wo<C1472a, aDJ> gNt;
    @C0064Am(aul = "5cedf5758f55f2f9d97aaeff8fb2e540", aum = 2)
    private static C1556Wo<String, TranslationNode> gNv;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "4d23047d71c5f9a0c0f050bd078a11fd", aum = 3)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C5256aCi
    private static C2686iZ<String> gNx;

    static {
        m10762V();
        I18NString i18NString = new I18NString();
        gNo = i18NString;
        I18NString i18NString2 = new I18NString();
        gNp = i18NString2;
        I18NString i18NString3 = new I18NString();
        gNq = i18NString3;
        I18NString i18NString4 = new I18NString();
        gNr = i18NString4;
        gNs = new I18NString[]{i18NString, i18NString2, i18NString3, i18NString4};
        m10767a(gNo, "Yes", "Sim");
        m10767a(gNp, "No", "Não");
        m10767a(gNq, "OK", "OK");
        m10767a(gNr, "Cancel", "Cancelar");
    }

    private transient Set<String> gNz;

    public NLSManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10762V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 20;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(NLSManager.class, "2d523df96815d9185ed2973947967c36", i);
        f1898bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSManager.class, "d99cabc409cbb5fa7ab09dce7121c991", i2);
        gNu = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSManager.class, "5cedf5758f55f2f9d97aaeff8fb2e540", i3);
        gNw = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSManager.class, "4d23047d71c5f9a0c0f050bd078a11fd", i4);
        gNy = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 20)];
        C2491fm a = C4105zY.m41624a(NLSManager.class, "317f6e041ce154a65c2967c2eb82e12c", i6);
        f1899bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSManager.class, "b41cfdc91dce4e9efe018275684512af", i7);
        f1900bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSManager.class, "932e587bcc7589a551a63fae37a21e7c", i8);
        f1901bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSManager.class, "5dd68e967be1a0b9f26bb35a3c12b34c", i9);
        gNA = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSManager.class, "d8538217358437ed84d20c806d8a53d3", i10);
        gNB = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSManager.class, "8dc3d306bd28cc2ef86eb2592779f635", i11);
        gNC = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSManager.class, "d8fa33f21ccbbeede0d712c8974c8d5f", i12);
        gND = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSManager.class, "e393b1682bc116790a2faf94f9680e3d", i13);
        gNE = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSManager.class, "3fd0eb612d80c00c6f2862728c735a69", i14);
        gNF = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSManager.class, "09a12d6db507f81355b5a3b5e7ea041e", i15);
        gNG = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSManager.class, "0e1aa2dd3bf1af8e8fe840bbb7d7c1b6", i16);
        gNH = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(NLSManager.class, "330456b53bf8e93f085db46008f0d9ca", i17);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(NLSManager.class, "895779f44f29c1dc4538bac3d97e51df", i18);
        gNI = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(NLSManager.class, "049c1bf8b0f923293f156365bcc8c3b8", i19);
        _f_onResurrect_0020_0028_0029V = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(NLSManager.class, "db77a4bdef4fc611e23a12e5c58020f0", i20);
        _f_loggerWarning_0020_0028Ljava_002flang_002fString_003b_0029V = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(NLSManager.class, "b295798619e4a1c8b33f2b331f1a22a7", i21);
        gNJ = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(NLSManager.class, "321eff827ab4c80c00235426dc803de8", i22);
        gNK = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(NLSManager.class, "5a2071b5a4dbef27ac51714595a08ab3", i23);
        gNL = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        C2491fm a19 = C4105zY.m41624a(NLSManager.class, "9bc26fc99e16475e02b16e611a7b44dc", i24);
        gNM = a19;
        fmVarArr[i24] = a19;
        int i25 = i24 + 1;
        C2491fm a20 = C4105zY.m41624a(NLSManager.class, "5d0eb30c6b9cc16d455fa3063803640b", i25);
        f1896Do = a20;
        fmVarArr[i25] = a20;
        int i26 = i25 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSManager.class, axD.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private static void m10767a(I18NString i18NString, String... strArr) {
        C1584XI[] values = C1584XI.values();
        if (strArr.length != values.length) {
            throw new IllegalArgumentException("Number of informed contents is not the same as the valid locales");
        }
        for (int i = 0; i < strArr.length; i++) {
            i18NString.set(values[i].getCode(), strArr[i]);
        }
    }

    /* renamed from: X */
    private void m10763X(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(gNy, iZVar);
    }

    @C0064Am(aul = "e393b1682bc116790a2faf94f9680e3d", aum = 0)
    @C5566aOg
    @C5794aaa
    /* renamed from: a */
    private TranslationNode m10764a(TranslationNode yjVar) {
        throw new aWi(new aCE(this, gNE, new Object[]{yjVar}));
    }

    /* renamed from: a */
    private void m10766a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1898bL, uuid);
    }

    /* renamed from: aa */
    private void m10769aa(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(gNu, wo);
    }

    /* renamed from: ab */
    private void m10770ab(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(gNw, wo);
    }

    /* renamed from: an */
    private UUID m10771an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1898bL);
    }

    /* renamed from: c */
    private void m10780c(UUID uuid) {
        switch (bFf().mo6893i(f1900bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1900bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1900bO, new Object[]{uuid}));
                break;
        }
        m10779b(uuid);
    }

    @C0064Am(aul = "09a12d6db507f81355b5a3b5e7ea041e", aum = 0)
    @C5566aOg
    @C5794aaa
    /* renamed from: c */
    private boolean m10781c(TranslationNode yjVar) {
        throw new aWi(new aCE(this, gNG, new Object[]{yjVar}));
    }

    private C1556Wo cAZ() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(gNu);
    }

    private C1556Wo cBa() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(gNw);
    }

    private C2686iZ cBb() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(gNy);
    }

    @C0064Am(aul = "3fd0eb612d80c00c6f2862728c735a69", aum = 0)
    @C5566aOg
    @C5794aaa
    /* renamed from: kt */
    private void m10784kt(String str) {
        throw new aWi(new aCE(this, gNF, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new axD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m10772ap();
            case 1:
                m10779b((UUID) args[0]);
                return null;
            case 2:
                return m10773ar();
            case 3:
                return cBc();
            case 4:
                return cBe();
            case 5:
                return cBg();
            case 6:
                return m10783kr((String) args[0]);
            case 7:
                return m10764a((TranslationNode) args[0]);
            case 8:
                m10784kt((String) args[0]);
                return null;
            case 9:
                return new Boolean(m10781c((TranslationNode) args[0]));
            case 10:
                return m10775b((C1472a) args[0]);
            case 11:
                return m10774au();
            case 12:
                cBi();
                return null;
            case 13:
                m10768aG();
                return null;
            case 14:
                m10785kv((String) args[0]);
                return null;
            case 15:
                return m10777b((Class<?>) (Class) args[0], (String) args[1], (Object[]) args[2]);
            case 16:
                return m10776b((Class) args[0], (String) args[1], (String) args[2], (Object[]) args[3]);
            case 17:
                return m10782j((Class) args[0], (String) args[1]);
            case 18:
                return m10778b((Class<?>) (Class) args[0], (String) args[1], (String) args[2]);
            case 19:
                m10765a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public String mo6307a(Class<?> cls, String str, String str2, Object... objArr) {
        switch (bFf().mo6893i(gNK)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gNK, new Object[]{cls, str, str2, objArr}));
            case 3:
                bFf().mo5606d(new aCE(this, gNK, new Object[]{cls, str, str2, objArr}));
                break;
        }
        return m10776b(cls, str, str2, objArr);
    }

    /* renamed from: a */
    public String mo6308a(Class<?> cls, String str, Object... objArr) {
        switch (bFf().mo6893i(gNJ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gNJ, new Object[]{cls, str, objArr}));
            case 3:
                bFf().mo5606d(new aCE(this, gNJ, new Object[]{cls, str, objArr}));
                break;
        }
        return m10777b(cls, str, objArr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m10768aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1899bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1899bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1899bN, new Object[0]));
                break;
        }
        return m10772ap();
    }

    @C5566aOg
    @C5794aaa
    /* renamed from: b */
    public TranslationNode mo6309b(TranslationNode yjVar) {
        switch (bFf().mo6893i(gNE)) {
            case 0:
                return null;
            case 2:
                return (TranslationNode) bFf().mo5606d(new aCE(this, gNE, new Object[]{yjVar}));
            case 3:
                bFf().mo5606d(new aCE(this, gNE, new Object[]{yjVar}));
                break;
        }
        return m10764a(yjVar);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f1896Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1896Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1896Do, new Object[]{jt}));
                break;
        }
        m10765a(jt);
    }

    /* renamed from: c */
    public <T extends aDJ> T mo6310c(C1472a aVar) {
        switch (bFf().mo6893i(gNH)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, gNH, new Object[]{aVar}));
            case 3:
                bFf().mo5606d(new aCE(this, gNH, new Object[]{aVar}));
                break;
        }
        return m10775b(aVar);
    }

    /* renamed from: c */
    public I18NString mo6311c(Class<?> cls, String str, String str2) {
        switch (bFf().mo6893i(gNM)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gNM, new Object[]{cls, str, str2}));
            case 3:
                bFf().mo5606d(new aCE(this, gNM, new Object[]{cls, str, str2}));
                break;
        }
        return m10778b(cls, str, str2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NLSScripts")
    public Collection<aDJ> cBd() {
        switch (bFf().mo6893i(gNA)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, gNA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gNA, new Object[0]));
                break;
        }
        return cBc();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Translations Nodes")
    @C5794aaa
    public List<TranslationNode> cBf() {
        switch (bFf().mo6893i(gNB)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gNB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gNB, new Object[0]));
                break;
        }
        return cBe();
    }

    public List<TranslationNode> cBh() {
        switch (bFf().mo6893i(gNC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gNC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gNC, new Object[0]));
                break;
        }
        return cBg();
    }

    public void cBj() {
        switch (bFf().mo6893i(gNI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gNI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gNI, new Object[0]));
                break;
        }
        cBi();
    }

    @C5566aOg
    @C5794aaa
    /* renamed from: d */
    public boolean mo6316d(TranslationNode yjVar) {
        switch (bFf().mo6893i(gNG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gNG, new Object[]{yjVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gNG, new Object[]{yjVar}));
                break;
        }
        return m10781c(yjVar);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f1901bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1901bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1901bP, new Object[0]));
                break;
        }
        return m10773ar();
    }

    /* access modifiers changed from: protected */
    /* renamed from: hy */
    public void mo6317hy(String str) {
        switch (bFf().mo6893i(_f_loggerWarning_0020_0028Ljava_002flang_002fString_003b_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_loggerWarning_0020_0028Ljava_002flang_002fString_003b_0029V, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_loggerWarning_0020_0028Ljava_002flang_002fString_003b_0029V, new Object[]{str}));
                break;
        }
        m10785kv(str);
    }

    /* renamed from: k */
    public I18NString mo6318k(Class<?> cls, String str) {
        switch (bFf().mo6893i(gNL)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gNL, new Object[]{cls, str}));
            case 3:
                bFf().mo5606d(new aCE(this, gNL, new Object[]{cls, str}));
                break;
        }
        return m10782j(cls, str);
    }

    @C5794aaa
    /* renamed from: ks */
    public TranslationNode mo6319ks(String str) {
        switch (bFf().mo6893i(gND)) {
            case 0:
                return null;
            case 2:
                return (TranslationNode) bFf().mo5606d(new aCE(this, gND, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, gND, new Object[]{str}));
                break;
        }
        return m10783kr(str);
    }

    @C5566aOg
    @C5794aaa
    /* renamed from: ku */
    public void mo6320ku(String str) {
        switch (bFf().mo6893i(gNF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gNF, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gNF, new Object[]{str}));
                break;
        }
        m10784kt(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m10774au();
    }

    @C0064Am(aul = "317f6e041ce154a65c2967c2eb82e12c", aum = 0)
    /* renamed from: ap */
    private UUID m10772ap() {
        return m10771an();
    }

    @C0064Am(aul = "b41cfdc91dce4e9efe018275684512af", aum = 0)
    /* renamed from: b */
    private void m10779b(UUID uuid) {
        m10766a(uuid);
    }

    @C0064Am(aul = "932e587bcc7589a551a63fae37a21e7c", aum = 0)
    /* renamed from: ar */
    private String m10773ar() {
        return "nls_manager";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NLSScripts")
    @C0064Am(aul = "5dd68e967be1a0b9f26bb35a3c12b34c", aum = 0)
    private Collection<aDJ> cBc() {
        return Collections.unmodifiableList(new ArrayList(cAZ().values()));
    }

    @C5794aaa
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Translations Nodes")
    @C0064Am(aul = "d8538217358437ed84d20c806d8a53d3", aum = 0)
    private List<TranslationNode> cBe() {
        return Collections.unmodifiableList(new ArrayList(cBa().values()));
    }

    @C0064Am(aul = "8dc3d306bd28cc2ef86eb2592779f635", aum = 0)
    private List<TranslationNode> cBg() {
        boolean z;
        ArrayList arrayList = new ArrayList();
        String[] strArr = {"taikodom.game.input.categories", "taikodom.addon.template", "taikodom.addon.template.helper", "taikodom.addon.flashplayer", "taikodom.addon.profileview"};
        for (TranslationNode yjVar : cBa().values()) {
            int length = strArr.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    z = false;
                    break;
                } else if (strArr[i].equals(yjVar.getScope())) {
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                arrayList.add(yjVar);
            }
        }
        return Collections.unmodifiableList(arrayList);
    }

    @C0064Am(aul = "d8fa33f21ccbbeede0d712c8974c8d5f", aum = 0)
    @C5794aaa
    /* renamed from: kr */
    private TranslationNode m10783kr(String str) {
        return (TranslationNode) cBa().get(str);
    }

    @C0064Am(aul = "0e1aa2dd3bf1af8e8fe840bbb7d7c1b6", aum = 0)
    /* renamed from: b */
    private <T extends aDJ> T m10775b(C1472a aVar) {
        T t = (aDJ) cAZ().get(aVar);
        if (t != null) {
            return t;
        }
        T n = mo8363n(aVar.getClazz());
        cAZ().put(aVar, n);
        return n;
    }

    @C0064Am(aul = "330456b53bf8e93f085db46008f0d9ca", aum = 0)
    /* renamed from: au */
    private String m10774au() {
        return "NLSManager";
    }

    @C0064Am(aul = "895779f44f29c1dc4538bac3d97e51df", aum = 0)
    private void cBi() {
        push();
        for (aDJ push : cAZ().values()) {
            push.push();
        }
    }

    @C0064Am(aul = "049c1bf8b0f923293f156365bcc8c3b8", aum = 0)
    /* renamed from: aG */
    private void m10768aG() {
        super.mo70aH();
        for (C1472a aVar : C1472a.values()) {
            if (!aVar.erE && !cAZ().containsKey(aVar)) {
                cAZ().put(aVar, mo8363n(aVar.clazz));
            }
        }
    }

    @C0064Am(aul = "db77a4bdef4fc611e23a12e5c58020f0", aum = 0)
    /* renamed from: kv */
    private void m10785kv(String str) {
        if (!bHa()) {
            if (this.gNz == null) {
                this.gNz = new THashSet();
            }
            if (!this.gNz.contains(str)) {
                this.gNz.add(str);
                super.mo6317hy(str);
            }
        } else if (!cBb().contains(str)) {
            cBb().add(str);
            super.mo6317hy(str);
        }
    }

    @C0064Am(aul = "b295798619e4a1c8b33f2b331f1a22a7", aum = 0)
    /* renamed from: b */
    private String m10777b(Class<?> cls, String str, Object[] objArr) {
        return mo6307a(cls, (String) null, str, objArr);
    }

    @C0064Am(aul = "321eff827ab4c80c00235426dc803de8", aum = 0)
    /* renamed from: b */
    private String m10776b(Class<?> cls, String str, String str2, Object[] objArr) {
        String value;
        String str3;
        String str4 = str != null ? String.valueOf(str) + '.' + str2 : str2;
        for (I18NString i18NString : gNs) {
            if (i18NString.get(I18NString.DEFAULT_LOCATION).equalsIgnoreCase(str4)) {
                return i18NString.get();
            }
        }
        TaikodomAddon awk = (TaikodomAddon) cls.getAnnotation(TaikodomAddon.class);
        if (awk == null) {
            mo6317hy("No @TranslationScope annotation for " + cls.getName());
            value = cls.getPackage().getName();
        } else {
            value = awk.value();
        }
        TranslationNode ks = mo6319ks(value);
        if (ks == null) {
            mo6317hy("No TranslationNode at game content for " + cls.getName() + " scope: " + value);
            str3 = "$?" + str2;
        } else {
            String str5 = ks.get(str4);
            if (str5 == null) {
                if (str4.startsWith("$")) {
                    str5 = ks.get(str4.substring(1));
                } else {
                    str5 = ks.get("$" + str4);
                }
            }
            if (str5 == null) {
                mo6317hy("No translation entry at game content for " + cls.getName() + " key: " + str4 + " scope: " + value);
                str3 = "$?" + str2;
            } else {
                str3 = str5;
            }
        }
        if (objArr == null || objArr.length <= 0) {
            return str3;
        }
        return String.format(str3, objArr);
    }

    @C0064Am(aul = "5a2071b5a4dbef27ac51714595a08ab3", aum = 0)
    /* renamed from: j */
    private I18NString m10782j(Class<?> cls, String str) {
        return mo6311c(cls, (String) null, str);
    }

    @C0064Am(aul = "9bc26fc99e16475e02b16e611a7b44dc", aum = 0)
    /* renamed from: b */
    private I18NString m10778b(Class<?> cls, String str, String str2) {
        String value;
        String str3 = str != null ? String.valueOf(str) + '.' + str2 : str2;
        for (I18NString i18NString : gNs) {
            if (i18NString.get(I18NString.DEFAULT_LOCATION).equalsIgnoreCase(str3)) {
                return i18NString;
            }
        }
        TaikodomAddon awk = (TaikodomAddon) cls.getAnnotation(TaikodomAddon.class);
        if (awk == null) {
            mo6317hy("No @TranslationScope annotation for " + cls.getName());
            value = cls.getPackage().getName();
        } else {
            value = awk.value();
        }
        TranslationNode ks = mo6319ks(value);
        if (ks == null) {
            mo6317hy("No TranslationNode at game content for " + cls.getName() + " scope: " + value);
            return new I18NString("$?" + str2);
        }
        I18NString cN = ks.mo23170cN(str3);
        if (cN == null) {
            if (str3.startsWith("$")) {
                cN = ks.mo23170cN(str3.substring(1));
            } else {
                cN = ks.mo23170cN("$" + str3);
            }
        }
        if (cN != null) {
            return cN;
        }
        mo6317hy("No translation entry at game content for " + cls.getName() + " key: " + str3 + " scope: " + value);
        return new I18NString("$?" + str2);
    }

    @C0064Am(aul = "5d0eb30c6b9cc16d455fa3063803640b", aum = 0)
    /* renamed from: a */
    private void m10765a(C0665JT jt) {
        Iterator it = new HashSet(cAZ().keySet()).iterator();
        while (it.hasNext()) {
            C1472a aVar = (C1472a) it.next();
            if (aVar.erE) {
                cAZ().remove(aVar);
            }
        }
    }

    /* renamed from: a.Vf$a */
    public enum C1472a {
        CHAT(NLSChat.class),
        WINDOWALERT(NLSWindowAlert.class),
        CONSOLEALERT(NLSConsoleAlert.class),
        LOOT(NLSLoot.class),
        ITEM(NLSItem.class),
        MISSION(NLSMission.class),
        PARTY(NLSParty.class),
        ASSOCIATES(NLSAssociates.class),
        CRUISESPEED(NLSCruiseSpeed.class),
        SPEECHACTIONS(NLSSpeechActions.class),
        HUDSELECTION(NLSHUDSelection.class),
        AMPLIFIER(NLSAmplifier.class),
        INTERACTION(NLSInteraction.class),
        CORPORATION(NLSCorporation.class),
        RESOURCELOADER(NLSResourceLoader.class),
        OUTPOST(NLSOutpost.class),
        CLONING(NLSCloning.class),
        PROGRESSION(NLSProgression.class),
        PDAKEYMANAGER(NLSPdaKeyManager.class);

        /* access modifiers changed from: private */
        public Class<? extends aDJ> clazz;
        boolean erE;

        private C1472a(Class<? extends aDJ> cls) {
            this(r2, r3, cls, false);
        }

        private C1472a(Class<? extends aDJ> cls, boolean z) {
            this.clazz = cls;
            this.erE = z;
        }

        public Class<? extends aDJ> getClazz() {
            return this.clazz;
        }

        public boolean isDeprecated() {
            return this.erE;
        }
    }
}
