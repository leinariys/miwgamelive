package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6684arg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.VA */
/* compiled from: a */
public class NLSHUDSelection extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz etG = null;
    public static final C5663aRz etI = null;
    public static final C5663aRz etK = null;
    public static final C5663aRz etM = null;
    public static final C2491fm etN = null;
    public static final C2491fm etO = null;
    public static final C2491fm etP = null;
    public static final C2491fm etQ = null;
    public static final C2491fm etR = null;
    public static final C2491fm etS = null;
    public static final C2491fm etT = null;
    public static final C2491fm etU = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f1845zQ = null;
    /* renamed from: zT */
    public static final C2491fm f1846zT = null;
    /* renamed from: zU */
    public static final C2491fm f1847zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2474566baa8259f16e3e7b51754fe630", aum = 0)
    private static I18NString etF;
    @C0064Am(aul = "3539f4ac275bbd1a7dfcb9ea02aed96e", aum = 1)
    private static I18NString etH;
    @C0064Am(aul = "b18c9b0641fd2c7dea4bca22ec3d2410", aum = 3)
    private static I18NString etJ;
    @C0064Am(aul = "75a8cccda944501a0d1a076adf3e76df", aum = 4)
    private static I18NString etL;
    @C0064Am(aul = "b431c10f460e80f3b0fb2e3ed0ac9e7a", aum = 2)

    /* renamed from: zP */
    private static I18NString f1844zP;

    static {
        m10419V();
    }

    public NLSHUDSelection() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSHUDSelection(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10419V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 5;
        _m_methodCount = NLSType._m_methodCount + 11;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(NLSHUDSelection.class, "2474566baa8259f16e3e7b51754fe630", i);
        etG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSHUDSelection.class, "3539f4ac275bbd1a7dfcb9ea02aed96e", i2);
        etI = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSHUDSelection.class, "b431c10f460e80f3b0fb2e3ed0ac9e7a", i3);
        f1845zQ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSHUDSelection.class, "b18c9b0641fd2c7dea4bca22ec3d2410", i4);
        etK = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NLSHUDSelection.class, "75a8cccda944501a0d1a076adf3e76df", i5);
        etM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i7 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 11)];
        C2491fm a = C4105zY.m41624a(NLSHUDSelection.class, "0c5a39e10696988fa5b37f78b7fc22d9", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSHUDSelection.class, "7b2f3cdc12510ba2205de1b3fcfa9f6b", i8);
        etN = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSHUDSelection.class, "72f124657ac2c9c1cc8cb2878bee6c86", i9);
        etO = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSHUDSelection.class, "32acb2056762eb29e8ea7188ff5c1254", i10);
        etP = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSHUDSelection.class, "f4a0d50f89faf917161093417731a0e6", i11);
        etQ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSHUDSelection.class, "a6b87ecbd99d04bd0ba6f517dfaabb69", i12);
        f1846zT = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSHUDSelection.class, "b9fecc842d4f88bd4f77911cd0def682", i13);
        f1847zU = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSHUDSelection.class, "f31f8e2afdb991bee6bfebb6ac30576b", i14);
        etR = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSHUDSelection.class, "4c222b845b9c71d457d557ec25da41ea", i15);
        etS = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSHUDSelection.class, "90143202f9cf236f4a784eb853ae223a", i16);
        etT = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSHUDSelection.class, "3c345d6b38013c0f4aa8b18f41d34389", i17);
        etU = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSHUDSelection.class, C6684arg.class, _m_fields, _m_methods);
    }

    private I18NString bBk() {
        return (I18NString) bFf().mo5608dq().mo3214p(etG);
    }

    private I18NString bBl() {
        return (I18NString) bFf().mo5608dq().mo3214p(etI);
    }

    private I18NString bBm() {
        return (I18NString) bFf().mo5608dq().mo3214p(etK);
    }

    private I18NString bBn() {
        return (I18NString) bFf().mo5608dq().mo3214p(etM);
    }

    /* renamed from: d */
    private void m10421d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f1845zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Object Name")
    @C0064Am(aul = "b9fecc842d4f88bd4f77911cd0def682", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m10422e(I18NString i18NString) {
        throw new aWi(new aCE(this, f1847zU, new Object[]{i18NString}));
    }

    /* renamed from: jp */
    private void m10423jp(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(etG, i18NString);
    }

    /* renamed from: jq */
    private void m10424jq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(etI, i18NString);
    }

    /* renamed from: jr */
    private void m10425jr(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(etK, i18NString);
    }

    /* renamed from: js */
    private void m10426js(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(etM, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ships")
    @C0064Am(aul = "72f124657ac2c9c1cc8cb2878bee6c86", aum = 0)
    @C5566aOg
    /* renamed from: jt */
    private void m10427jt(I18NString i18NString) {
        throw new aWi(new aCE(this, etO, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Objects Of Interest")
    @C0064Am(aul = "f4a0d50f89faf917161093417731a0e6", aum = 0)
    @C5566aOg
    /* renamed from: jv */
    private void m10428jv(I18NString i18NString) {
        throw new aWi(new aCE(this, etQ, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Object Distance")
    @C0064Am(aul = "4c222b845b9c71d457d557ec25da41ea", aum = 0)
    @C5566aOg
    /* renamed from: jx */
    private void m10429jx(I18NString i18NString) {
        throw new aWi(new aCE(this, etS, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unknown Ship")
    @C0064Am(aul = "3c345d6b38013c0f4aa8b18f41d34389", aum = 0)
    @C5566aOg
    /* renamed from: jz */
    private void m10430jz(I18NString i18NString) {
        throw new aWi(new aCE(this, etU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m10431kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f1845zQ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6684arg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m10420au();
            case 1:
                return bBo();
            case 2:
                m10427jt((I18NString) args[0]);
                return null;
            case 3:
                return bBq();
            case 4:
                m10428jv((I18NString) args[0]);
                return null;
            case 5:
                return m10432kd();
            case 6:
                m10422e((I18NString) args[0]);
                return null;
            case 7:
                return bBs();
            case 8:
                m10429jx((I18NString) args[0]);
                return null;
            case 9:
                return bBu();
            case 10:
                m10430jz((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ships")
    public I18NString bBp() {
        switch (bFf().mo6893i(etN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, etN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, etN, new Object[0]));
                break;
        }
        return bBo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Objects Of Interest")
    public I18NString bBr() {
        switch (bFf().mo6893i(etP)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, etP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, etP, new Object[0]));
                break;
        }
        return bBq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Object Distance")
    public I18NString bBt() {
        switch (bFf().mo6893i(etR)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, etR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, etR, new Object[0]));
                break;
        }
        return bBs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unknown Ship")
    public I18NString bBv() {
        switch (bFf().mo6893i(etT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, etT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, etT, new Object[0]));
                break;
        }
        return bBu();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Object Name")
    @C5566aOg
    /* renamed from: f */
    public void mo5948f(I18NString i18NString) {
        switch (bFf().mo6893i(f1847zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1847zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1847zU, new Object[]{i18NString}));
                break;
        }
        m10422e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unknown Ship")
    @C5566aOg
    /* renamed from: jA */
    public void mo5949jA(I18NString i18NString) {
        switch (bFf().mo6893i(etU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, etU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, etU, new Object[]{i18NString}));
                break;
        }
        m10430jz(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ships")
    @C5566aOg
    /* renamed from: ju */
    public void mo5950ju(I18NString i18NString) {
        switch (bFf().mo6893i(etO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, etO, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, etO, new Object[]{i18NString}));
                break;
        }
        m10427jt(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Objects Of Interest")
    @C5566aOg
    /* renamed from: jw */
    public void mo5951jw(I18NString i18NString) {
        switch (bFf().mo6893i(etQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, etQ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, etQ, new Object[]{i18NString}));
                break;
        }
        m10428jv(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Object Distance")
    @C5566aOg
    /* renamed from: jy */
    public void mo5952jy(I18NString i18NString) {
        switch (bFf().mo6893i(etS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, etS, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, etS, new Object[]{i18NString}));
                break;
        }
        m10429jx(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Object Name")
    /* renamed from: ke */
    public I18NString mo5953ke() {
        switch (bFf().mo6893i(f1846zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1846zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1846zT, new Object[0]));
                break;
        }
        return m10432kd();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m10420au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "0c5a39e10696988fa5b37f78b7fc22d9", aum = 0)
    /* renamed from: au */
    private String m10420au() {
        return "HUD Selection View";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ships")
    @C0064Am(aul = "7b2f3cdc12510ba2205de1b3fcfa9f6b", aum = 0)
    private I18NString bBo() {
        return bBk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Objects Of Interest")
    @C0064Am(aul = "32acb2056762eb29e8ea7188ff5c1254", aum = 0)
    private I18NString bBq() {
        return bBl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Object Name")
    @C0064Am(aul = "a6b87ecbd99d04bd0ba6f517dfaabb69", aum = 0)
    /* renamed from: kd */
    private I18NString m10432kd() {
        return m10431kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Object Distance")
    @C0064Am(aul = "f31f8e2afdb991bee6bfebb6ac30576b", aum = 0)
    private I18NString bBs() {
        return bBm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unknown Ship")
    @C0064Am(aul = "90143202f9cf236f4a784eb853ae223a", aum = 0)
    private I18NString bBu() {
        return bBn();
    }
}
