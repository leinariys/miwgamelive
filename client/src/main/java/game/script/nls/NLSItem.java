package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6490anu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Sh */
/* compiled from: a */
public class NLSItem extends NLSType implements C1616Xf {

    /* renamed from: QA */
    public static final C5663aRz f1550QA = null;

    /* renamed from: QE */
    public static final C2491fm f1551QE = null;

    /* renamed from: QF */
    public static final C2491fm f1552QF = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dyv = null;
    public static final C5663aRz ecV = null;
    public static final C5663aRz ecX = null;
    public static final C5663aRz ecZ = null;
    public static final C2491fm edA = null;
    public static final C2491fm edB = null;
    public static final C2491fm edC = null;
    public static final C2491fm edD = null;
    public static final C2491fm edE = null;
    public static final C2491fm edF = null;
    public static final C2491fm edG = null;
    public static final C2491fm edH = null;
    public static final C2491fm edI = null;
    public static final C2491fm edJ = null;
    public static final C5663aRz edb = null;
    public static final C5663aRz edd = null;
    public static final C5663aRz edf = null;
    public static final C5663aRz edh = null;
    public static final C5663aRz edj = null;
    public static final C5663aRz edl = null;
    public static final C5663aRz edn = null;
    public static final C2491fm edo = null;
    public static final C2491fm edp = null;
    public static final C2491fm edq = null;
    public static final C2491fm edr = null;
    public static final C2491fm eds = null;
    public static final C2491fm edt = null;
    public static final C2491fm edu = null;
    public static final C2491fm edv = null;
    public static final C2491fm edw = null;
    public static final C2491fm edx = null;
    public static final C2491fm edy = null;
    public static final C2491fm edz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "12fae0095baed6a9a4fc3740d2675c84", aum = 0)
    private static I18NString ecT;
    @C0064Am(aul = "7656cc18243c8329de3ba1a7b6a1e6a2", aum = 1)
    private static I18NString ecU;
    @C0064Am(aul = "7df4b967fae368333f2d5a52a619e593", aum = 2)
    private static I18NString ecW;
    @C0064Am(aul = "b46558bb07c147f4692a157f035d6d3d", aum = 3)
    private static I18NString ecY;
    @C0064Am(aul = "6be2383a990ef32ea69448c5175bee6f", aum = 4)
    private static I18NString eda;
    @C0064Am(aul = "de8ebc05a08966b5b9ab8f36d7f7cd9e", aum = 5)
    private static I18NString edc;
    @C0064Am(aul = "c7eec7a98cdb8afa1e7e2361b980b87d", aum = 6)
    private static I18NString ede;
    @C0064Am(aul = "9f1dbcbf3e9b88dffa3507f94119a74d", aum = 8)
    private static String edg;
    @C0064Am(aul = "a90b4c69f3b46826e4c8d0d4609b8f93", aum = 9)
    private static I18NString edi;
    @C0064Am(aul = "1aa6bbd7def89a418fe4bdb27b81fdea", aum = 10)
    private static I18NString edk;
    @C0064Am(aul = "802352099fec1986813296050b1d03a7", aum = 11)
    private static I18NString edm;
    @C0064Am(aul = "0d6838036e668ed3fb314052da51f1ef", aum = 7)

    /* renamed from: nh */
    private static I18NString f1553nh;

    static {
        m9501V();
    }

    public NLSItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSItem(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9501V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 12;
        _m_methodCount = NLSType._m_methodCount + 25;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(NLSItem.class, "12fae0095baed6a9a4fc3740d2675c84", i);
        dyv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSItem.class, "7656cc18243c8329de3ba1a7b6a1e6a2", i2);
        ecV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSItem.class, "7df4b967fae368333f2d5a52a619e593", i3);
        ecX = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSItem.class, "b46558bb07c147f4692a157f035d6d3d", i4);
        ecZ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NLSItem.class, "6be2383a990ef32ea69448c5175bee6f", i5);
        edb = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NLSItem.class, "de8ebc05a08966b5b9ab8f36d7f7cd9e", i6);
        edd = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NLSItem.class, "c7eec7a98cdb8afa1e7e2361b980b87d", i7);
        edf = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NLSItem.class, "0d6838036e668ed3fb314052da51f1ef", i8);
        f1550QA = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NLSItem.class, "9f1dbcbf3e9b88dffa3507f94119a74d", i9);
        edh = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(NLSItem.class, "a90b4c69f3b46826e4c8d0d4609b8f93", i10);
        edj = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(NLSItem.class, "1aa6bbd7def89a418fe4bdb27b81fdea", i11);
        edl = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(NLSItem.class, "802352099fec1986813296050b1d03a7", i12);
        edn = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i14 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 25)];
        C2491fm a = C4105zY.m41624a(NLSItem.class, "a7d1dfdcd871da87d1cb93f8d644dd07", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSItem.class, "354e1244502a67f4369a8797f6a302e6", i15);
        edo = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSItem.class, "bf5cf1bd54a51f539f355a3806018ba4", i16);
        edp = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSItem.class, "9c7c8ff2dbf53ff20401b27ad3986aeb", i17);
        edq = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSItem.class, "40e5428d96d31aa523abd7048313c27e", i18);
        edr = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSItem.class, "fb7a0f8214c168b4d8d2e07d42061800", i19);
        eds = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSItem.class, "cba431466524b06e82ce3a0a0c571bc5", i20);
        edt = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSItem.class, "0fcb16d98ac86dd53c03657f1a64604e", i21);
        edu = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSItem.class, "63e79f3806038928ad4845c712b50ff3", i22);
        edv = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSItem.class, "a03c6262ee5cfb6e0eecc1ceac91a53c", i23);
        edw = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSItem.class, "b08b21d401ed6c07d020ce99aa5c8d8e", i24);
        edx = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(NLSItem.class, "c8ea4a6232b5d65c7b02529a2ccfbdc4", i25);
        edy = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(NLSItem.class, "fe4ed559f227a41695deb44f24d6981c", i26);
        edz = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(NLSItem.class, "0d91c54337f1e066b36a8a2de3a6ba2d", i27);
        edA = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(NLSItem.class, "b3ea46a661156c54316ad8c960f0b437", i28);
        edB = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(NLSItem.class, "5293e33501927b1ee59461fb69dc1767", i29);
        f1551QE = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(NLSItem.class, "7891b13a48bf77d148d2737ba4e4d921", i30);
        f1552QF = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(NLSItem.class, "8cefe089c7076149bdb9c216190efb10", i31);
        edC = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(NLSItem.class, "dd93c2c139fda9275ea0d68e2de925ef", i32);
        edD = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(NLSItem.class, "00a420db4b34a9f90e7e72dfddeba10f", i33);
        edE = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(NLSItem.class, "53147ea97be27df434eb779beb7e3221", i34);
        edF = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(NLSItem.class, "3bcd296d0aad89b5484fdb5845033f6a", i35);
        edG = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(NLSItem.class, "fdb43eaf95e75c56e4ea7f5b466124e3", i36);
        edH = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        C2491fm a24 = C4105zY.m41624a(NLSItem.class, "7c3bd57a975ee66d0077e73bf70ee7ab", i37);
        edI = a24;
        fmVarArr[i37] = a24;
        int i38 = i37 + 1;
        C2491fm a25 = C4105zY.m41624a(NLSItem.class, "caee54fd354768416453c5ce08bc221f", i38);
        edJ = a25;
        fmVarArr[i38] = a25;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSItem.class, C6490anu.class, _m_fields, _m_methods);
    }

    /* renamed from: br */
    private void m9503br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f1550QA, i18NString);
    }

    private I18NString bsK() {
        return (I18NString) bFf().mo5608dq().mo3214p(dyv);
    }

    private I18NString bsL() {
        return (I18NString) bFf().mo5608dq().mo3214p(ecV);
    }

    private I18NString bsM() {
        return (I18NString) bFf().mo5608dq().mo3214p(ecX);
    }

    private I18NString bsN() {
        return (I18NString) bFf().mo5608dq().mo3214p(ecZ);
    }

    private I18NString bsO() {
        return (I18NString) bFf().mo5608dq().mo3214p(edb);
    }

    private I18NString bsP() {
        return (I18NString) bFf().mo5608dq().mo3214p(edd);
    }

    private I18NString bsQ() {
        return (I18NString) bFf().mo5608dq().mo3214p(edf);
    }

    private String bsR() {
        return (String) bFf().mo5608dq().mo3214p(edh);
    }

    private I18NString bsS() {
        return (I18NString) bFf().mo5608dq().mo3214p(edj);
    }

    private I18NString bsT() {
        return (I18NString) bFf().mo5608dq().mo3214p(edl);
    }

    private I18NString bsU() {
        return (I18NString) bFf().mo5608dq().mo3214p(edn);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "7891b13a48bf77d148d2737ba4e4d921", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m9504bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f1552QF, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NeededCertificate")
    @C0064Am(aul = "dd93c2c139fda9275ea0d68e2de925ef", aum = 0)
    @C5566aOg
    /* renamed from: gA */
    private void m9505gA(String str) {
        throw new aWi(new aCE(this, edD, new Object[]{str}));
    }

    /* renamed from: gz */
    private void m9506gz(String str) {
        bFf().mo5608dq().mo3197f(edh, str);
    }

    /* renamed from: iI */
    private void m9507iI(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(dyv, i18NString);
    }

    /* renamed from: iJ */
    private void m9508iJ(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ecV, i18NString);
    }

    /* renamed from: iK */
    private void m9509iK(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ecX, i18NString);
    }

    /* renamed from: iL */
    private void m9510iL(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ecZ, i18NString);
    }

    /* renamed from: iM */
    private void m9511iM(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(edb, i18NString);
    }

    /* renamed from: iN */
    private void m9512iN(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(edd, i18NString);
    }

    /* renamed from: iO */
    private void m9513iO(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(edf, i18NString);
    }

    /* renamed from: iP */
    private void m9514iP(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(edj, i18NString);
    }

    /* renamed from: iQ */
    private void m9515iQ(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(edl, i18NString);
    }

    /* renamed from: iR */
    private void m9516iR(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(edn, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destroy Item")
    @C0064Am(aul = "bf5cf1bd54a51f539f355a3806018ba4", aum = 0)
    @C5566aOg
    /* renamed from: iS */
    private void m9517iS(I18NString i18NString) {
        throw new aWi(new aCE(this, edp, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destroy Item Title")
    @C0064Am(aul = "40e5428d96d31aa523abd7048313c27e", aum = 0)
    @C5566aOg
    /* renamed from: iU */
    private void m9518iU(I18NString i18NString) {
        throw new aWi(new aCE(this, edr, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] BasePrice")
    @C0064Am(aul = "cba431466524b06e82ce3a0a0c571bc5", aum = 0)
    @C5566aOg
    /* renamed from: iW */
    private void m9519iW(I18NString i18NString) {
        throw new aWi(new aCE(this, edt, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] Unit Volume")
    @C0064Am(aul = "63e79f3806038928ad4845c712b50ff3", aum = 0)
    @C5566aOg
    /* renamed from: iY */
    private void m9520iY(I18NString i18NString) {
        throw new aWi(new aCE(this, edv, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] Total Volume")
    @C0064Am(aul = "b08b21d401ed6c07d020ce99aa5c8d8e", aum = 0)
    @C5566aOg
    /* renamed from: ja */
    private void m9521ja(I18NString i18NString) {
        throw new aWi(new aCE(this, edx, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] Cargo Volume")
    @C0064Am(aul = "fe4ed559f227a41695deb44f24d6981c", aum = 0)
    @C5566aOg
    /* renamed from: jc */
    private void m9522jc(I18NString i18NString) {
        throw new aWi(new aCE(this, edz, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Destination Window Message (double-click)")
    @C0064Am(aul = "b3ea46a661156c54316ad8c960f0b437", aum = 0)
    @C5566aOg
    /* renamed from: je */
    private void m9523je(I18NString i18NString) {
        throw new aWi(new aCE(this, edB, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destroy multiple items")
    @C0064Am(aul = "53147ea97be27df434eb779beb7e3221", aum = 0)
    @C5566aOg
    /* renamed from: jg */
    private void m9524jg(I18NString i18NString) {
        throw new aWi(new aCE(this, edF, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item could not be moved to destination")
    @C0064Am(aul = "fdb43eaf95e75c56e4ea7f5b466124e3", aum = 0)
    @C5566aOg
    /* renamed from: ji */
    private void m9525ji(I18NString i18NString) {
        throw new aWi(new aCE(this, edH, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "You cannot destroy items at this node")
    @C0064Am(aul = "caee54fd354768416453c5ce08bc221f", aum = 0)
    @C5566aOg
    /* renamed from: jk */
    private void m9526jk(I18NString i18NString) {
        throw new aWi(new aCE(this, edJ, new Object[]{i18NString}));
    }

    /* renamed from: vT */
    private I18NString m9527vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f1550QA);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6490anu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m9502au();
            case 1:
                return bsV();
            case 2:
                m9517iS((I18NString) args[0]);
                return null;
            case 3:
                return bsX();
            case 4:
                m9518iU((I18NString) args[0]);
                return null;
            case 5:
                return bsZ();
            case 6:
                m9519iW((I18NString) args[0]);
                return null;
            case 7:
                return btb();
            case 8:
                m9520iY((I18NString) args[0]);
                return null;
            case 9:
                return btd();
            case 10:
                m9521ja((I18NString) args[0]);
                return null;
            case 11:
                return btf();
            case 12:
                m9522jc((I18NString) args[0]);
                return null;
            case 13:
                return bth();
            case 14:
                m9523je((I18NString) args[0]);
                return null;
            case 15:
                return m9528vV();
            case 16:
                m9504bu((I18NString) args[0]);
                return null;
            case 17:
                return btj();
            case 18:
                m9505gA((String) args[0]);
                return null;
            case 19:
                return btl();
            case 20:
                m9524jg((I18NString) args[0]);
                return null;
            case 21:
                return btn();
            case 22:
                m9525ji((I18NString) args[0]);
                return null;
            case 23:
                return btp();
            case 24:
                m9526jk((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destroy Item")
    public I18NString bsW() {
        switch (bFf().mo6893i(edo)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edo, new Object[0]));
                break;
        }
        return bsV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destroy Item Title")
    public I18NString bsY() {
        switch (bFf().mo6893i(edq)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edq, new Object[0]));
                break;
        }
        return bsX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] BasePrice")
    public I18NString bta() {
        switch (bFf().mo6893i(eds)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, eds, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eds, new Object[0]));
                break;
        }
        return bsZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] Unit Volume")
    public I18NString btc() {
        switch (bFf().mo6893i(edu)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edu, new Object[0]));
                break;
        }
        return btb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] Total Volume")
    public I18NString bte() {
        switch (bFf().mo6893i(edw)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edw, new Object[0]));
                break;
        }
        return btd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] Cargo Volume")
    public I18NString btg() {
        switch (bFf().mo6893i(edy)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edy, new Object[0]));
                break;
        }
        return btf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Destination Window Message (double-click)")
    public I18NString bti() {
        switch (bFf().mo6893i(edA)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edA, new Object[0]));
                break;
        }
        return bth();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NeededCertificate")
    public String btk() {
        switch (bFf().mo6893i(edC)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, edC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edC, new Object[0]));
                break;
        }
        return btj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destroy multiple items")
    public I18NString btm() {
        switch (bFf().mo6893i(edE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edE, new Object[0]));
                break;
        }
        return btl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item could not be moved to destination")
    public I18NString bto() {
        switch (bFf().mo6893i(edG)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edG, new Object[0]));
                break;
        }
        return btn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "You cannot destroy items at this node")
    public I18NString btq() {
        switch (bFf().mo6893i(edI)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, edI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, edI, new Object[0]));
                break;
        }
        return btp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo5443bv(I18NString i18NString) {
        switch (bFf().mo6893i(f1552QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1552QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1552QF, new Object[]{i18NString}));
                break;
        }
        m9504bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NeededCertificate")
    @C5566aOg
    /* renamed from: gB */
    public void mo5444gB(String str) {
        switch (bFf().mo6893i(edD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edD, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edD, new Object[]{str}));
                break;
        }
        m9505gA(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destroy Item")
    @C5566aOg
    /* renamed from: iT */
    public void mo5445iT(I18NString i18NString) {
        switch (bFf().mo6893i(edp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edp, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edp, new Object[]{i18NString}));
                break;
        }
        m9517iS(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destroy Item Title")
    @C5566aOg
    /* renamed from: iV */
    public void mo5446iV(I18NString i18NString) {
        switch (bFf().mo6893i(edr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edr, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edr, new Object[]{i18NString}));
                break;
        }
        m9518iU(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] BasePrice")
    @C5566aOg
    /* renamed from: iX */
    public void mo5447iX(I18NString i18NString) {
        switch (bFf().mo6893i(edt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edt, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edt, new Object[]{i18NString}));
                break;
        }
        m9519iW(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] Unit Volume")
    @C5566aOg
    /* renamed from: iZ */
    public void mo5448iZ(I18NString i18NString) {
        switch (bFf().mo6893i(edv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edv, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edv, new Object[]{i18NString}));
                break;
        }
        m9520iY(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] Total Volume")
    @C5566aOg
    /* renamed from: jb */
    public void mo5449jb(I18NString i18NString) {
        switch (bFf().mo6893i(edx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edx, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edx, new Object[]{i18NString}));
                break;
        }
        m9521ja(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Tooltip] Cargo Volume")
    @C5566aOg
    /* renamed from: jd */
    public void mo5450jd(I18NString i18NString) {
        switch (bFf().mo6893i(edz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edz, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edz, new Object[]{i18NString}));
                break;
        }
        m9522jc(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Destination Window Message (double-click)")
    @C5566aOg
    /* renamed from: jf */
    public void mo5451jf(I18NString i18NString) {
        switch (bFf().mo6893i(edB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edB, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edB, new Object[]{i18NString}));
                break;
        }
        m9523je(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destroy multiple items")
    @C5566aOg
    /* renamed from: jh */
    public void mo5452jh(I18NString i18NString) {
        switch (bFf().mo6893i(edF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edF, new Object[]{i18NString}));
                break;
        }
        m9524jg(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item could not be moved to destination")
    @C5566aOg
    /* renamed from: jj */
    public void mo5453jj(I18NString i18NString) {
        switch (bFf().mo6893i(edH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edH, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edH, new Object[]{i18NString}));
                break;
        }
        m9525ji(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "You cannot destroy items at this node")
    @C5566aOg
    /* renamed from: jl */
    public void mo5454jl(I18NString i18NString) {
        switch (bFf().mo6893i(edJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edJ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edJ, new Object[]{i18NString}));
                break;
        }
        m9526jk(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m9502au();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo5455vW() {
        switch (bFf().mo6893i(f1551QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1551QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1551QE, new Object[0]));
                break;
        }
        return m9528vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a7d1dfdcd871da87d1cb93f8d644dd07", aum = 0)
    /* renamed from: au */
    private String m9502au() {
        return "Item";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destroy Item")
    @C0064Am(aul = "354e1244502a67f4369a8797f6a302e6", aum = 0)
    private I18NString bsV() {
        return bsQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destroy Item Title")
    @C0064Am(aul = "9c7c8ff2dbf53ff20401b27ad3986aeb", aum = 0)
    private I18NString bsX() {
        return bsP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] BasePrice")
    @C0064Am(aul = "fb7a0f8214c168b4d8d2e07d42061800", aum = 0)
    private I18NString bsZ() {
        return bsK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] Unit Volume")
    @C0064Am(aul = "0fcb16d98ac86dd53c03657f1a64604e", aum = 0)
    private I18NString btb() {
        return bsL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] Total Volume")
    @C0064Am(aul = "a03c6262ee5cfb6e0eecc1ceac91a53c", aum = 0)
    private I18NString btd() {
        return bsM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Tooltip] Cargo Volume")
    @C0064Am(aul = "c8ea4a6232b5d65c7b02529a2ccfbdc4", aum = 0)
    private I18NString btf() {
        return bsN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Destination Window Message (double-click)")
    @C0064Am(aul = "0d91c54337f1e066b36a8a2de3a6ba2d", aum = 0)
    private I18NString bth() {
        return bsO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "5293e33501927b1ee59461fb69dc1767", aum = 0)
    /* renamed from: vV */
    private I18NString m9528vV() {
        return m9527vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NeededCertificate")
    @C0064Am(aul = "8cefe089c7076149bdb9c216190efb10", aum = 0)
    private String btj() {
        return bsR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destroy multiple items")
    @C0064Am(aul = "00a420db4b34a9f90e7e72dfddeba10f", aum = 0)
    private I18NString btl() {
        return bsS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item could not be moved to destination")
    @C0064Am(aul = "3bcd296d0aad89b5484fdb5845033f6a", aum = 0)
    private I18NString btn() {
        return bsT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "You cannot destroy items at this node")
    @C0064Am(aul = "7c3bd57a975ee66d0077e73bf70ee7ab", aum = 0)
    private I18NString btp() {
        return bsU();
    }
}
