package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2484fi;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.acf  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class NLSType extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4258bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4259bM = null;
    /* renamed from: bN */
    public static final C2491fm f4260bN = null;
    /* renamed from: bO */
    public static final C2491fm f4261bO = null;
    /* renamed from: bP */
    public static final C2491fm f4262bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4263bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fa25e7e8f57e864ba25a4f6e0e07bfa4", aum = 0)

    /* renamed from: bK */
    private static UUID f4257bK;
    @C0064Am(aul = "9cdeb43b662bbbdc295ac194a1348d00", aum = 1)
    private static String handle;

    static {
        m20545V();
    }

    public NLSType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20545V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 4;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(NLSType.class, "fa25e7e8f57e864ba25a4f6e0e07bfa4", i);
        f4258bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSType.class, "9cdeb43b662bbbdc295ac194a1348d00", i2);
        f4259bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(NLSType.class, "5966f317e1ec1d28242e9b743d0f0694", i4);
        f4263bQ = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSType.class, "c8a8338ab971d79b3136ccdc7961113e", i5);
        f4262bP = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSType.class, "897166c78f3d5ed8b29e6aabcb2cee7f", i6);
        f4260bN = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSType.class, "eebc8d69abe332341d8ea47e7bc031df", i7);
        f4261bO = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSType.class, C2484fi.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m20546a(String str) {
        bFf().mo5608dq().mo3197f(f4259bM, str);
    }

    /* renamed from: a */
    private void m20547a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4258bL, uuid);
    }

    /* renamed from: an */
    private UUID m20548an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4258bL);
    }

    /* renamed from: ao */
    private String m20549ao() {
        return (String) bFf().mo5608dq().mo3214p(f4259bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "5966f317e1ec1d28242e9b743d0f0694", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m20552b(String str) {
        throw new aWi(new aCE(this, f4263bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m20554c(UUID uuid) {
        switch (bFf().mo6893i(f4261bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4261bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4261bO, new Object[]{uuid}));
                break;
        }
        m20553b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2484fi(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m20552b((String) args[0]);
                return null;
            case 1:
                return m20551ar();
            case 2:
                return m20550ap();
            case 3:
                m20553b((UUID) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4260bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4260bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4260bN, new Object[0]));
                break;
        }
        return m20550ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4262bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4262bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4262bP, new Object[0]));
                break;
        }
        return m20551ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4263bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4263bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4263bQ, new Object[]{str}));
                break;
        }
        m20552b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "c8a8338ab971d79b3136ccdc7961113e", aum = 0)
    /* renamed from: ar */
    private String m20551ar() {
        return m20549ao();
    }

    @C0064Am(aul = "897166c78f3d5ed8b29e6aabcb2cee7f", aum = 0)
    /* renamed from: ap */
    private UUID m20550ap() {
        return m20548an();
    }

    @C0064Am(aul = "eebc8d69abe332341d8ea47e7bc031df", aum = 0)
    /* renamed from: b */
    private void m20553b(UUID uuid) {
        m20547a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m20547a(UUID.randomUUID());
    }
}
