package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2358eR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.WP */
/* compiled from: a */
public class NLSSpeechActions extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm eAa = null;
    public static final C2491fm eAb = null;
    public static final C2491fm eAc = null;
    public static final C2491fm eAd = null;
    public static final C5663aRz ezA = null;
    public static final C5663aRz ezB = null;
    public static final C5663aRz ezC = null;
    public static final C5663aRz ezD = null;
    public static final C5663aRz ezE = null;
    public static final C5663aRz ezF = null;
    public static final C2491fm ezG = null;
    public static final C2491fm ezH = null;
    public static final C2491fm ezI = null;
    public static final C2491fm ezJ = null;
    public static final C2491fm ezK = null;
    public static final C2491fm ezL = null;
    public static final C2491fm ezM = null;
    public static final C2491fm ezN = null;
    public static final C2491fm ezO = null;
    public static final C2491fm ezP = null;
    public static final C2491fm ezQ = null;
    public static final C2491fm ezR = null;
    public static final C2491fm ezS = null;
    public static final C2491fm ezT = null;
    public static final C2491fm ezU = null;
    public static final C2491fm ezV = null;
    public static final C2491fm ezW = null;
    public static final C2491fm ezX = null;
    public static final C2491fm ezY = null;
    public static final C2491fm ezZ = null;
    public static final C5663aRz ezu = null;
    public static final C5663aRz ezv = null;
    public static final C5663aRz ezw = null;
    public static final C5663aRz ezx = null;
    public static final C5663aRz ezy = null;
    public static final C5663aRz ezz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "18738f8ff9ca1ab430c2697d5810de98", aum = 0)

    /* renamed from: Ea */
    private static I18NString f2018Ea;
    @C0064Am(aul = "0e2a9f469f45978b09b4efcd5073b75e", aum = 1)

    /* renamed from: Eb */
    private static I18NString f2019Eb;
    @C0064Am(aul = "bbbf899b47130a30ae47ae4d6a122494", aum = 2)

    /* renamed from: Ec */
    private static I18NString f2020Ec;
    @C0064Am(aul = "23f38e3f7ddb24d4521ec69d48842a08", aum = 3)

    /* renamed from: Ed */
    private static I18NString f2021Ed;
    @C0064Am(aul = "c4ccfd14ac1cdc6ba1c05805e0bcc787", aum = 4)

    /* renamed from: Ee */
    private static I18NString f2022Ee;
    @C0064Am(aul = "499733e476d9365d8e0944c7cc52e4b3", aum = 5)

    /* renamed from: Ef */
    private static I18NString f2023Ef;
    @C0064Am(aul = "4cd1803d4b0f358424a741125ce6e9b5", aum = 6)

    /* renamed from: Eg */
    private static I18NString f2024Eg;
    @C0064Am(aul = "9f68d30d1aab29c7dbd349573521aa6c", aum = 7)

    /* renamed from: Eh */
    private static I18NString f2025Eh;
    @C0064Am(aul = "530a66f154cfed2f470f1efc4e2cca55", aum = 8)

    /* renamed from: Ei */
    private static I18NString f2026Ei;
    @C0064Am(aul = "c7091d22900151feb881fd1bacc65ccc", aum = 9)

    /* renamed from: Ej */
    private static I18NString f2027Ej;
    @C0064Am(aul = "1d802eaaff6d04cc6adf8115f7cd15a4", aum = 10)

    /* renamed from: Ek */
    private static I18NString f2028Ek;
    @C0064Am(aul = "261943cb3b23964581335be725072131", aum = 11)

    /* renamed from: El */
    private static I18NString f2029El;

    static {
        m11157V();
    }

    public NLSSpeechActions() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSSpeechActions(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11157V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 12;
        _m_methodCount = NLSType._m_methodCount + 25;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(NLSSpeechActions.class, "18738f8ff9ca1ab430c2697d5810de98", i);
        ezu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSSpeechActions.class, "0e2a9f469f45978b09b4efcd5073b75e", i2);
        ezv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSSpeechActions.class, "bbbf899b47130a30ae47ae4d6a122494", i3);
        ezw = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSSpeechActions.class, "23f38e3f7ddb24d4521ec69d48842a08", i4);
        ezx = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NLSSpeechActions.class, "c4ccfd14ac1cdc6ba1c05805e0bcc787", i5);
        ezy = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NLSSpeechActions.class, "499733e476d9365d8e0944c7cc52e4b3", i6);
        ezz = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NLSSpeechActions.class, "4cd1803d4b0f358424a741125ce6e9b5", i7);
        ezA = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NLSSpeechActions.class, "9f68d30d1aab29c7dbd349573521aa6c", i8);
        ezB = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NLSSpeechActions.class, "530a66f154cfed2f470f1efc4e2cca55", i9);
        ezC = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(NLSSpeechActions.class, "c7091d22900151feb881fd1bacc65ccc", i10);
        ezD = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(NLSSpeechActions.class, "1d802eaaff6d04cc6adf8115f7cd15a4", i11);
        ezE = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(NLSSpeechActions.class, "261943cb3b23964581335be725072131", i12);
        ezF = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i14 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 25)];
        C2491fm a = C4105zY.m41624a(NLSSpeechActions.class, "39934d3c3727d636a5a2f391a13b25ff", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSSpeechActions.class, "fd80005724df7bb8d1515d6fbc6492b7", i15);
        ezG = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSSpeechActions.class, "a16979eb1931551c3f73d8d036e514e5", i16);
        ezH = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSSpeechActions.class, "f8a1e0f3239e6457dabce8418046d9b9", i17);
        ezI = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSSpeechActions.class, "2e2ee1fe762b1a4d33238c13ecfe0854", i18);
        ezJ = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSSpeechActions.class, "cc497c3dc520be09b941c7c38e308b49", i19);
        ezK = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSSpeechActions.class, "1b8f085f1014d5a09779f057270f3e0d", i20);
        ezL = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSSpeechActions.class, "29dd8451c8ba6d8d925c4d340627461d", i21);
        ezM = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSSpeechActions.class, "55843dd7ccef0d86977de5e656391645", i22);
        ezN = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSSpeechActions.class, "3a62831e0fbc61e9fc578d0b89d413b3", i23);
        ezO = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSSpeechActions.class, "8c9aed96df2189074f62ed233d93d023", i24);
        ezP = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(NLSSpeechActions.class, "7ef2acde05478b83aab8496c799a97ee", i25);
        ezQ = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(NLSSpeechActions.class, "b21d0791a1d365ec4bf0a631e822cb14", i26);
        ezR = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(NLSSpeechActions.class, "75e9888f2fe9ffd2648f9a80aea52edd", i27);
        ezS = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(NLSSpeechActions.class, "5a39edf8f82101e85313929c31247d46", i28);
        ezT = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(NLSSpeechActions.class, "0f20e75ea0658fe6851323cd9aa57e7c", i29);
        ezU = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(NLSSpeechActions.class, "ed3784458875c54af7bdb8024c9c812c", i30);
        ezV = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(NLSSpeechActions.class, "061104b59d4a4802f5f2f0a67052ac04", i31);
        ezW = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(NLSSpeechActions.class, "aea21d7da0a010ae1218a0c11b076591", i32);
        ezX = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(NLSSpeechActions.class, "190f4703014a9fb11545d8101b37f0d1", i33);
        ezY = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(NLSSpeechActions.class, "2eb22f4c783c550b374cb3c97eebb8c0", i34);
        ezZ = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(NLSSpeechActions.class, "e133a55b932c080ea486d83cf1c9f16c", i35);
        eAa = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(NLSSpeechActions.class, "0d2d8f66bf72d1843b43afa542d1e61d", i36);
        eAb = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        C2491fm a24 = C4105zY.m41624a(NLSSpeechActions.class, "b8f65b0c6d113fbc9807cd9a74eedaca", i37);
        eAc = a24;
        fmVarArr[i37] = a24;
        int i38 = i37 + 1;
        C2491fm a25 = C4105zY.m41624a(NLSSpeechActions.class, "2c6822bc636f9dafe6da0c29bcfd37d2", i38);
        eAd = a25;
        fmVarArr[i38] = a25;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSSpeechActions.class, C2358eR.class, _m_fields, _m_methods);
    }

    private I18NString bEl() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezu);
    }

    private I18NString bEm() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezv);
    }

    private I18NString bEn() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezw);
    }

    private I18NString bEo() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezx);
    }

    private I18NString bEp() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezy);
    }

    private I18NString bEq() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezz);
    }

    private I18NString bEr() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezA);
    }

    private I18NString bEs() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezB);
    }

    private I18NString bEt() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezC);
    }

    private I18NString bEu() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezD);
    }

    private I18NString bEv() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezE);
    }

    private I18NString bEw() {
        return (I18NString) bFf().mo5608dq().mo3214p(ezF);
    }

    /* renamed from: jD */
    private void m11159jD(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezu, i18NString);
    }

    /* renamed from: jE */
    private void m11160jE(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezv, i18NString);
    }

    /* renamed from: jF */
    private void m11161jF(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezw, i18NString);
    }

    /* renamed from: jG */
    private void m11162jG(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezx, i18NString);
    }

    /* renamed from: jH */
    private void m11163jH(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezy, i18NString);
    }

    /* renamed from: jI */
    private void m11164jI(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezz, i18NString);
    }

    /* renamed from: jJ */
    private void m11165jJ(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezA, i18NString);
    }

    /* renamed from: jK */
    private void m11166jK(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezB, i18NString);
    }

    /* renamed from: jL */
    private void m11167jL(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezC, i18NString);
    }

    /* renamed from: jM */
    private void m11168jM(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezD, i18NString);
    }

    /* renamed from: jN */
    private void m11169jN(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezE, i18NString);
    }

    /* renamed from: jO */
    private void m11170jO(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ezF, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TakeItem")
    @C0064Am(aul = "a16979eb1931551c3f73d8d036e514e5", aum = 0)
    @C5566aOg
    /* renamed from: jP */
    private void m11171jP(I18NString i18NString) {
        throw new aWi(new aCE(this, ezH, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "GiveItem")
    @C0064Am(aul = "2e2ee1fe762b1a4d33238c13ecfe0854", aum = 0)
    @C5566aOg
    /* renamed from: jR */
    private void m11172jR(I18NString i18NString) {
        throw new aWi(new aCE(this, ezJ, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TakeMoney")
    @C0064Am(aul = "1b8f085f1014d5a09779f057270f3e0d", aum = 0)
    @C5566aOg
    /* renamed from: jT */
    private void m11173jT(I18NString i18NString) {
        throw new aWi(new aCE(this, ezL, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "GiveMoney")
    @C0064Am(aul = "55843dd7ccef0d86977de5e656391645", aum = 0)
    @C5566aOg
    /* renamed from: jV */
    private void m11174jV(I18NString i18NString) {
        throw new aWi(new aCE(this, ezN, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CraftItem")
    @C0064Am(aul = "8c9aed96df2189074f62ed233d93d023", aum = 0)
    @C5566aOg
    /* renamed from: jX */
    private void m11175jX(I18NString i18NString) {
        throw new aWi(new aCE(this, ezP, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "RepairShip")
    @C0064Am(aul = "b21d0791a1d365ec4bf0a631e822cb14", aum = 0)
    @C5566aOg
    /* renamed from: jZ */
    private void m11176jZ(I18NString i18NString) {
        throw new aWi(new aCE(this, ezR, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Money for 'TakeMoney'")
    @C0064Am(aul = "5a39edf8f82101e85313929c31247d46", aum = 0)
    @C5566aOg
    /* renamed from: kb */
    private void m11177kb(I18NString i18NString) {
        throw new aWi(new aCE(this, ezT, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Blueprint found for Crafting")
    @C0064Am(aul = "ed3784458875c54af7bdb8024c9c812c", aum = 0)
    @C5566aOg
    /* renamed from: kd */
    private void m11178kd(I18NString i18NString) {
        throw new aWi(new aCE(this, ezV, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No ingredient found for Crafting")
    @C0064Am(aul = "aea21d7da0a010ae1218a0c11b076591", aum = 0)
    @C5566aOg
    /* renamed from: kf */
    private void m11179kf(I18NString i18NString) {
        throw new aWi(new aCE(this, ezX, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No room for Crafting")
    @C0064Am(aul = "2eb22f4c783c550b374cb3c97eebb8c0", aum = 0)
    @C5566aOg
    /* renamed from: kh */
    private void m11180kh(I18NString i18NString) {
        throw new aWi(new aCE(this, ezZ, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Insuficient Itens in Container")
    @C0064Am(aul = "0d2d8f66bf72d1843b43afa542d1e61d", aum = 0)
    @C5566aOg
    /* renamed from: kj */
    private void m11181kj(I18NString i18NString) {
        throw new aWi(new aCE(this, eAb, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No room")
    @C0064Am(aul = "2c6822bc636f9dafe6da0c29bcfd37d2", aum = 0)
    @C5566aOg
    /* renamed from: kl */
    private void m11182kl(I18NString i18NString) {
        throw new aWi(new aCE(this, eAd, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2358eR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m11158au();
            case 1:
                return bEx();
            case 2:
                m11171jP((I18NString) args[0]);
                return null;
            case 3:
                return bEz();
            case 4:
                m11172jR((I18NString) args[0]);
                return null;
            case 5:
                return bEB();
            case 6:
                m11173jT((I18NString) args[0]);
                return null;
            case 7:
                return bED();
            case 8:
                m11174jV((I18NString) args[0]);
                return null;
            case 9:
                return bEF();
            case 10:
                m11175jX((I18NString) args[0]);
                return null;
            case 11:
                return bEH();
            case 12:
                m11176jZ((I18NString) args[0]);
                return null;
            case 13:
                return bEJ();
            case 14:
                m11177kb((I18NString) args[0]);
                return null;
            case 15:
                return bEL();
            case 16:
                m11178kd((I18NString) args[0]);
                return null;
            case 17:
                return bEN();
            case 18:
                m11179kf((I18NString) args[0]);
                return null;
            case 19:
                return bEP();
            case 20:
                m11180kh((I18NString) args[0]);
                return null;
            case 21:
                return bER();
            case 22:
                m11181kj((I18NString) args[0]);
                return null;
            case 23:
                return bET();
            case 24:
                m11182kl((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "GiveItem")
    public I18NString bEA() {
        switch (bFf().mo6893i(ezI)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezI, new Object[0]));
                break;
        }
        return bEz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TakeMoney")
    public I18NString bEC() {
        switch (bFf().mo6893i(ezK)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezK, new Object[0]));
                break;
        }
        return bEB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "GiveMoney")
    public I18NString bEE() {
        switch (bFf().mo6893i(ezM)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezM, new Object[0]));
                break;
        }
        return bED();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CraftItem")
    public I18NString bEG() {
        switch (bFf().mo6893i(ezO)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezO, new Object[0]));
                break;
        }
        return bEF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "RepairShip")
    public I18NString bEI() {
        switch (bFf().mo6893i(ezQ)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezQ, new Object[0]));
                break;
        }
        return bEH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Money for 'TakeMoney'")
    public I18NString bEK() {
        switch (bFf().mo6893i(ezS)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezS, new Object[0]));
                break;
        }
        return bEJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Blueprint found for Crafting")
    public I18NString bEM() {
        switch (bFf().mo6893i(ezU)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezU, new Object[0]));
                break;
        }
        return bEL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No ingredient found for Crafting")
    public I18NString bEO() {
        switch (bFf().mo6893i(ezW)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezW, new Object[0]));
                break;
        }
        return bEN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No room for Crafting")
    public I18NString bEQ() {
        switch (bFf().mo6893i(ezY)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezY, new Object[0]));
                break;
        }
        return bEP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Insuficient Itens in Container")
    public I18NString bES() {
        switch (bFf().mo6893i(eAa)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, eAa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eAa, new Object[0]));
                break;
        }
        return bER();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No room")
    public I18NString bEU() {
        switch (bFf().mo6893i(eAc)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, eAc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eAc, new Object[0]));
                break;
        }
        return bET();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TakeItem")
    public I18NString bEy() {
        switch (bFf().mo6893i(ezG)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ezG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezG, new Object[0]));
                break;
        }
        return bEx();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TakeItem")
    @C5566aOg
    /* renamed from: jQ */
    public void mo6560jQ(I18NString i18NString) {
        switch (bFf().mo6893i(ezH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezH, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezH, new Object[]{i18NString}));
                break;
        }
        m11171jP(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "GiveItem")
    @C5566aOg
    /* renamed from: jS */
    public void mo6561jS(I18NString i18NString) {
        switch (bFf().mo6893i(ezJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezJ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezJ, new Object[]{i18NString}));
                break;
        }
        m11172jR(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TakeMoney")
    @C5566aOg
    /* renamed from: jU */
    public void mo6562jU(I18NString i18NString) {
        switch (bFf().mo6893i(ezL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezL, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezL, new Object[]{i18NString}));
                break;
        }
        m11173jT(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "GiveMoney")
    @C5566aOg
    /* renamed from: jW */
    public void mo6563jW(I18NString i18NString) {
        switch (bFf().mo6893i(ezN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezN, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezN, new Object[]{i18NString}));
                break;
        }
        m11174jV(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CraftItem")
    @C5566aOg
    /* renamed from: jY */
    public void mo6564jY(I18NString i18NString) {
        switch (bFf().mo6893i(ezP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezP, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezP, new Object[]{i18NString}));
                break;
        }
        m11175jX(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "RepairShip")
    @C5566aOg
    /* renamed from: ka */
    public void mo6565ka(I18NString i18NString) {
        switch (bFf().mo6893i(ezR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezR, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezR, new Object[]{i18NString}));
                break;
        }
        m11176jZ(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Money for 'TakeMoney'")
    @C5566aOg
    /* renamed from: kc */
    public void mo6566kc(I18NString i18NString) {
        switch (bFf().mo6893i(ezT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezT, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezT, new Object[]{i18NString}));
                break;
        }
        m11177kb(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Blueprint found for Crafting")
    @C5566aOg
    /* renamed from: ke */
    public void mo6567ke(I18NString i18NString) {
        switch (bFf().mo6893i(ezV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezV, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezV, new Object[]{i18NString}));
                break;
        }
        m11178kd(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No ingredient found for Crafting")
    @C5566aOg
    /* renamed from: kg */
    public void mo6568kg(I18NString i18NString) {
        switch (bFf().mo6893i(ezX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezX, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezX, new Object[]{i18NString}));
                break;
        }
        m11179kf(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No room for Crafting")
    @C5566aOg
    /* renamed from: ki */
    public void mo6569ki(I18NString i18NString) {
        switch (bFf().mo6893i(ezZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezZ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezZ, new Object[]{i18NString}));
                break;
        }
        m11180kh(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Insuficient Itens in Container")
    @C5566aOg
    /* renamed from: kk */
    public void mo6570kk(I18NString i18NString) {
        switch (bFf().mo6893i(eAb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eAb, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eAb, new Object[]{i18NString}));
                break;
        }
        m11181kj(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No room")
    @C5566aOg
    /* renamed from: km */
    public void mo6571km(I18NString i18NString) {
        switch (bFf().mo6893i(eAd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eAd, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eAd, new Object[]{i18NString}));
                break;
        }
        m11182kl(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m11158au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "39934d3c3727d636a5a2f391a13b25ff", aum = 0)
    /* renamed from: au */
    private String m11158au() {
        return "Speech Actions";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TakeItem")
    @C0064Am(aul = "fd80005724df7bb8d1515d6fbc6492b7", aum = 0)
    private I18NString bEx() {
        return bEl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "GiveItem")
    @C0064Am(aul = "f8a1e0f3239e6457dabce8418046d9b9", aum = 0)
    private I18NString bEz() {
        return bEm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TakeMoney")
    @C0064Am(aul = "cc497c3dc520be09b941c7c38e308b49", aum = 0)
    private I18NString bEB() {
        return bEn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "GiveMoney")
    @C0064Am(aul = "29dd8451c8ba6d8d925c4d340627461d", aum = 0)
    private I18NString bED() {
        return bEo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CraftItem")
    @C0064Am(aul = "3a62831e0fbc61e9fc578d0b89d413b3", aum = 0)
    private I18NString bEF() {
        return bEq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "RepairShip")
    @C0064Am(aul = "7ef2acde05478b83aab8496c799a97ee", aum = 0)
    private I18NString bEH() {
        return bEp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Money for 'TakeMoney'")
    @C0064Am(aul = "75e9888f2fe9ffd2648f9a80aea52edd", aum = 0)
    private I18NString bEJ() {
        return bEr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Blueprint found for Crafting")
    @C0064Am(aul = "0f20e75ea0658fe6851323cd9aa57e7c", aum = 0)
    private I18NString bEL() {
        return bEs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No ingredient found for Crafting")
    @C0064Am(aul = "061104b59d4a4802f5f2f0a67052ac04", aum = 0)
    private I18NString bEN() {
        return bEt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No room for Crafting")
    @C0064Am(aul = "190f4703014a9fb11545d8101b37f0d1", aum = 0)
    private I18NString bEP() {
        return bEu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Insuficient Itens in Container")
    @C0064Am(aul = "e133a55b932c080ea486d83cf1c9f16c", aum = 0)
    private I18NString bER() {
        return bEv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No room")
    @C0064Am(aul = "b8f65b0c6d113fbc9807cd9a74eedaca", aum = 0)
    private I18NString bET() {
        return bEw();
    }
}
