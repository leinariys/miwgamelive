package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3844vh;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.UA */
/* compiled from: a */
public class NLSPdaKeyManager extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eoF = null;
    public static final C2491fm eoG = null;
    public static final C2491fm eoH = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2338738af4087b698478739406788fa0", aum = 0)
    private static I18NString byT;

    static {
        m10135V();
    }

    public NLSPdaKeyManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSPdaKeyManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10135V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 1;
        _m_methodCount = NLSType._m_methodCount + 3;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(NLSPdaKeyManager.class, "2338738af4087b698478739406788fa0", i);
        eoF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i3 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(NLSPdaKeyManager.class, "492d5b15ca48b31d0872a083c0b55199", i3);
        eoG = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSPdaKeyManager.class, "4f5639b24e372c8484a1d136fee01337", i4);
        eoH = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSPdaKeyManager.class, "8c5f707f6360de93708382897d2ee301", i5);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSPdaKeyManager.class, C3844vh.class, _m_fields, _m_methods);
    }

    private I18NString bxO() {
        return (I18NString) bFf().mo5608dq().mo3214p(eoF);
    }

    /* renamed from: jm */
    private void m10137jm(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(eoF, i18NString);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3844vh(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return bxP();
            case 1:
                m10138jn((I18NString) args[0]);
                return null;
            case 2:
                return m10136au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "New PDA Entry")
    public I18NString bxQ() {
        switch (bFf().mo6893i(eoG)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, eoG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eoG, new Object[0]));
                break;
        }
        return bxP();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "New PDA Entry")
    /* renamed from: jo */
    public void mo5772jo(I18NString i18NString) {
        switch (bFf().mo6893i(eoH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eoH, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eoH, new Object[]{i18NString}));
                break;
        }
        m10138jn(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m10136au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "New PDA Entry")
    @C0064Am(aul = "492d5b15ca48b31d0872a083c0b55199", aum = 0)
    private I18NString bxP() {
        return bxO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "New PDA Entry")
    @C0064Am(aul = "4f5639b24e372c8484a1d136fee01337", aum = 0)
    /* renamed from: jn */
    private void m10138jn(I18NString i18NString) {
        m10137jm(i18NString);
    }

    @C0064Am(aul = "8c5f707f6360de93708382897d2ee301", aum = 0)
    /* renamed from: au */
    private String m10136au() {
        return "PDA Key Manager";
    }
}
