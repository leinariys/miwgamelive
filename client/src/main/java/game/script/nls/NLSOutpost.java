package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aVD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.acI  reason: case insensitive filesystem */
/* compiled from: a */
public class NLSOutpost extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz feK = null;
    public static final C5663aRz feM = null;
    public static final C2491fm feN = null;
    public static final C2491fm feO = null;
    public static final C2491fm feP = null;
    public static final C2491fm feQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e1a8318f6658c2c72df56eb1e6d531ee", aum = 0)
    private static I18NString feJ;
    @C0064Am(aul = "f93ceb67030255c37f794bedec4b468d", aum = 1)
    private static I18NString feL;

    static {
        m20286V();
    }

    public NLSOutpost() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSOutpost(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20286V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 2;
        _m_methodCount = NLSType._m_methodCount + 5;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(NLSOutpost.class, "e1a8318f6658c2c72df56eb1e6d531ee", i);
        feK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSOutpost.class, "f93ceb67030255c37f794bedec4b468d", i2);
        feM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i4 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(NLSOutpost.class, "5b7308c0f1d0353dafd51d6b000b02c8", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSOutpost.class, "bd7866dc352d69dbda83700f55f65dfb", i5);
        feN = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSOutpost.class, "a43e104170c7b7fd384b37e58550c9af", i6);
        feO = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSOutpost.class, "777d6446a18340b0fd2cc549cecc84bd", i7);
        feP = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSOutpost.class, "9dc029441723d88cf179ddb3f2984e7d", i8);
        feQ = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSOutpost.class, aVD.class, _m_fields, _m_methods);
    }

    private I18NString bPJ() {
        return (I18NString) bFf().mo5608dq().mo3214p(feK);
    }

    private I18NString bPK() {
        return (I18NString) bFf().mo5608dq().mo3214p(feM);
    }

    /* renamed from: lQ */
    private void m20288lQ(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(feK, i18NString);
    }

    /* renamed from: lR */
    private void m20289lR(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(feM, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reserved Outpost Name")
    @C0064Am(aul = "a43e104170c7b7fd384b37e58550c9af", aum = 0)
    @C5566aOg
    /* renamed from: lS */
    private void m20290lS(I18NString i18NString) {
        throw new aWi(new aCE(this, feO, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Available Outpost Name")
    @C0064Am(aul = "9dc029441723d88cf179ddb3f2984e7d", aum = 0)
    @C5566aOg
    /* renamed from: lU */
    private void m20291lU(I18NString i18NString) {
        throw new aWi(new aCE(this, feQ, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aVD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m20287au();
            case 1:
                return bPL();
            case 2:
                m20290lS((I18NString) args[0]);
                return null;
            case 3:
                return bPN();
            case 4:
                m20291lU((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reserved Outpost Name")
    public I18NString bPM() {
        switch (bFf().mo6893i(feN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, feN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, feN, new Object[0]));
                break;
        }
        return bPL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Available Outpost Name")
    public I18NString bPO() {
        switch (bFf().mo6893i(feP)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, feP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, feP, new Object[0]));
                break;
        }
        return bPN();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reserved Outpost Name")
    @C5566aOg
    /* renamed from: lT */
    public void mo12591lT(I18NString i18NString) {
        switch (bFf().mo6893i(feO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, feO, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, feO, new Object[]{i18NString}));
                break;
        }
        m20290lS(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Available Outpost Name")
    @C5566aOg
    /* renamed from: lV */
    public void mo12592lV(I18NString i18NString) {
        switch (bFf().mo6893i(feQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, feQ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, feQ, new Object[]{i18NString}));
                break;
        }
        m20291lU(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m20287au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "5b7308c0f1d0353dafd51d6b000b02c8", aum = 0)
    /* renamed from: au */
    private String m20287au() {
        return "Outpost";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reserved Outpost Name")
    @C0064Am(aul = "bd7866dc352d69dbda83700f55f65dfb", aum = 0)
    private I18NString bPL() {
        return bPK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Available Outpost Name")
    @C0064Am(aul = "777d6446a18340b0fd2cc549cecc84bd", aum = 0)
    private I18NString bPN() {
        return bPJ();
    }
}
