package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0787LP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aVT */
/* compiled from: a */
public class NLSLoot extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aff = null;
    public static final C2491fm afs = null;
    public static final C2491fm aft = null;
    public static final C5663aRz dnS = null;
    public static final C2491fm dpa = null;
    public static final C2491fm dpb = null;
    public static final C5663aRz jcG = null;
    public static final C5663aRz jcH = null;
    public static final C5663aRz jcI = null;
    public static final C5663aRz jcJ = null;
    public static final C5663aRz jcK = null;
    public static final C5663aRz jcL = null;
    public static final C5663aRz jcM = null;
    public static final C2491fm jcN = null;
    public static final C2491fm jcO = null;
    public static final C2491fm jcP = null;
    public static final C2491fm jcQ = null;
    public static final C2491fm jcR = null;
    public static final C2491fm jcS = null;
    public static final C2491fm jcT = null;
    public static final C2491fm jcU = null;
    public static final C2491fm jcV = null;
    public static final C2491fm jcW = null;
    public static final C2491fm jcX = null;
    public static final C2491fm jcY = null;
    public static final C2491fm jcZ = null;
    public static final C2491fm jda = null;
    public static final C2491fm jdb = null;
    public static final C2491fm jdc = null;
    /* renamed from: kO */
    public static final C5663aRz f3917kO = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "dfa0757d4375e8fcca4d64c9a8b30b82", aum = 0)
    private static I18NString afe;
    @C0064Am(aul = "e362835fa375c5678970b69cffb8b0cf", aum = 2)
    private static I18NString dxN;
    @C0064Am(aul = "91c53c36de5bcff2bb2f382c8935ce6a", aum = 3)
    private static I18NString dxO;
    @C0064Am(aul = "41c69912f621d1daa94140d04a3f3142", aum = 4)
    private static I18NString dxP;
    @C0064Am(aul = "100585f40b52dd9d67e8d1d889aadc4c", aum = 5)
    private static I18NString dxQ;
    @C0064Am(aul = "c4cc657ae75197b79f8e5870aea04ddc", aum = 6)
    private static I18NString dxR;
    @C0064Am(aul = "22771797da641190a0a819e8fb32ccea", aum = 7)
    private static I18NString dxS;
    @C0064Am(aul = "3582c36e76dee006de13f6ec5439a4ec", aum = 8)
    private static I18NString dxT;
    @C0064Am(aul = "2fac1bd2f0f9716f290224b3395badd0", aum = 9)
    private static I18NString dxU;
    @C0064Am(aul = "4300798f8bbc3c67c2ec8e376da4d278", aum = 1)

    /* renamed from: sD */
    private static I18NString f3918sD;

    static {
        m18820V();
    }

    public NLSLoot() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSLoot(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18820V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 10;
        _m_methodCount = NLSType._m_methodCount + 21;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(NLSLoot.class, "dfa0757d4375e8fcca4d64c9a8b30b82", i);
        aff = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSLoot.class, "4300798f8bbc3c67c2ec8e376da4d278", i2);
        dnS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSLoot.class, "e362835fa375c5678970b69cffb8b0cf", i3);
        jcG = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSLoot.class, "91c53c36de5bcff2bb2f382c8935ce6a", i4);
        jcH = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NLSLoot.class, "41c69912f621d1daa94140d04a3f3142", i5);
        jcI = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NLSLoot.class, "100585f40b52dd9d67e8d1d889aadc4c", i6);
        jcJ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NLSLoot.class, "c4cc657ae75197b79f8e5870aea04ddc", i7);
        jcK = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NLSLoot.class, "22771797da641190a0a819e8fb32ccea", i8);
        f3917kO = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NLSLoot.class, "3582c36e76dee006de13f6ec5439a4ec", i9);
        jcL = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(NLSLoot.class, "2fac1bd2f0f9716f290224b3395badd0", i10);
        jcM = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i12 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 21)];
        C2491fm a = C4105zY.m41624a(NLSLoot.class, "ed871e45879109bef95f3dd192ce363c", i12);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSLoot.class, "6097e0d64a9c66d69d7446ccb67d552f", i13);
        jcN = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSLoot.class, "a1f7471ae92b036ca2eda1682628d14e", i14);
        jcO = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSLoot.class, "6f807934e41e2ab3a203c69b123fc4ad", i15);
        jcP = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSLoot.class, "093a3534745e7fb4619086f820e263a9", i16);
        jcQ = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSLoot.class, "203ca9a3a127f29b04ef18d04c4cb359", i17);
        jcR = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSLoot.class, "3c6255d8e27e02e90716639cb1b83d69", i18);
        jcS = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSLoot.class, "b6ef40119e5c94bdd7bd6e83b68bcce9", i19);
        jcT = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSLoot.class, "aac3b46d1b86da4c97dc77cc2a7fc02e", i20);
        jcU = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSLoot.class, "ade2d1242e41ee69defc1142965019d9", i21);
        jcV = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSLoot.class, "75a44ea69a4da2c1186be6d552f17042", i22);
        jcW = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(NLSLoot.class, "03b8d38142fca5740bda8a2a5cfd3539", i23);
        jcX = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(NLSLoot.class, "3bf977640dc4384146a1cd2c10480abc", i24);
        jcY = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(NLSLoot.class, "99a8e3caab39f7f8f2ef77da23edb680", i25);
        dpa = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(NLSLoot.class, "ca1262149f32aea93ca608e5742f9563", i26);
        dpb = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(NLSLoot.class, "cdbcc90951ffbbe877dc9189b8a94dc3", i27);
        afs = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(NLSLoot.class, "e6a4136668db9db3dc4716cfec1c5384", i28);
        aft = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(NLSLoot.class, "5056da96c77cd22e6c0b1466f38fc4a6", i29);
        jcZ = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(NLSLoot.class, "c9b114579f923606d45fc391a0787c31", i30);
        jda = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(NLSLoot.class, "b9d26c72c3cdc9cae7e41272ac1707e3", i31);
        jdb = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(NLSLoot.class, "b03fa93eb0e6d00c3d888655b720b3a2", i32);
        jdc = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSLoot.class, C0787LP.class, _m_fields, _m_methods);
    }

    /* renamed from: Cn */
    private I18NString m18818Cn() {
        return (I18NString) bFf().mo5608dq().mo3214p(aff);
    }

    /* renamed from: bV */
    private void m18822bV(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(aff, i18NString);
    }

    private I18NString baV() {
        return (I18NString) bFf().mo5608dq().mo3214p(dnS);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warning")
    @C0064Am(aul = "e6a4136668db9db3dc4716cfec1c5384", aum = 0)
    @C5566aOg
    /* renamed from: cf */
    private void m18823cf(I18NString i18NString) {
        throw new aWi(new aCE(this, aft, new Object[]{i18NString}));
    }

    private I18NString dCO() {
        return (I18NString) bFf().mo5608dq().mo3214p(jcG);
    }

    private I18NString dCP() {
        return (I18NString) bFf().mo5608dq().mo3214p(jcH);
    }

    private I18NString dCQ() {
        return (I18NString) bFf().mo5608dq().mo3214p(jcI);
    }

    private I18NString dCR() {
        return (I18NString) bFf().mo5608dq().mo3214p(jcJ);
    }

    private I18NString dCS() {
        return (I18NString) bFf().mo5608dq().mo3214p(jcK);
    }

    private I18NString dCT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3917kO);
    }

    private I18NString dCU() {
        return (I18NString) bFf().mo5608dq().mo3214p(jcL);
    }

    private I18NString dCV() {
        return (I18NString) bFf().mo5608dq().mo3214p(jcM);
    }

    /* renamed from: gY */
    private void m18824gY(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(dnS, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NoLootRoom")
    @C0064Am(aul = "ca1262149f32aea93ca608e5742f9563", aum = 0)
    @C5566aOg
    /* renamed from: ii */
    private void m18825ii(I18NString i18NString) {
        throw new aWi(new aCE(this, dpb, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "LootOwner")
    @C0064Am(aul = "093a3534745e7fb4619086f820e263a9", aum = 0)
    @C5566aOg
    /* renamed from: sA */
    private void m18826sA(I18NString i18NString) {
        throw new aWi(new aCE(this, jcQ, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Target Name: RemainsOf")
    @C0064Am(aul = "3c6255d8e27e02e90716639cb1b83d69", aum = 0)
    @C5566aOg
    /* renamed from: sC */
    private void m18827sC(I18NString i18NString) {
        throw new aWi(new aCE(this, jcS, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Target Name: Ejected")
    @C0064Am(aul = "aac3b46d1b86da4c97dc77cc2a7fc02e", aum = 0)
    @C5566aOg
    /* renamed from: sE */
    private void m18828sE(I18NString i18NString) {
        throw new aWi(new aCE(this, jcU, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TakeAll")
    @C0064Am(aul = "75a44ea69a4da2c1186be6d552f17042", aum = 0)
    @C5566aOg
    /* renamed from: sG */
    private void m18829sG(I18NString i18NString) {
        throw new aWi(new aCE(this, jcW, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Close")
    @C0064Am(aul = "3bf977640dc4384146a1cd2c10480abc", aum = 0)
    @C5566aOg
    /* renamed from: sI */
    private void m18830sI(I18NString i18NString) {
        throw new aWi(new aCE(this, jcY, new Object[]{i18NString}));
    }

    /* renamed from: sq */
    private void m18833sq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(jcG, i18NString);
    }

    /* renamed from: sr */
    private void m18834sr(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(jcH, i18NString);
    }

    /* renamed from: ss */
    private void m18835ss(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(jcI, i18NString);
    }

    /* renamed from: st */
    private void m18836st(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(jcJ, i18NString);
    }

    /* renamed from: su */
    private void m18837su(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(jcK, i18NString);
    }

    /* renamed from: sv */
    private void m18838sv(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3917kO, i18NString);
    }

    /* renamed from: sw */
    private void m18839sw(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(jcL, i18NString);
    }

    /* renamed from: sx */
    private void m18840sx(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(jcM, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item")
    @C0064Am(aul = "a1f7471ae92b036ca2eda1682628d14e", aum = 0)
    @C5566aOg
    /* renamed from: sy */
    private void m18841sy(I18NString i18NString) {
        throw new aWi(new aCE(this, jcO, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warning")
    /* renamed from: Cy */
    public I18NString mo11776Cy() {
        switch (bFf().mo6893i(afs)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, afs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, afs, new Object[0]));
                break;
        }
        return m18819Cx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0787LP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m18821au();
            case 1:
                return dCW();
            case 2:
                m18841sy((I18NString) args[0]);
                return null;
            case 3:
                return dCY();
            case 4:
                m18826sA((I18NString) args[0]);
                return null;
            case 5:
                return dDa();
            case 6:
                m18827sC((I18NString) args[0]);
                return null;
            case 7:
                return dDc();
            case 8:
                m18828sE((I18NString) args[0]);
                return null;
            case 9:
                return dDe();
            case 10:
                m18829sG((I18NString) args[0]);
                return null;
            case 11:
                return dDg();
            case 12:
                m18830sI((I18NString) args[0]);
                return null;
            case 13:
                return bcf();
            case 14:
                m18825ii((I18NString) args[0]);
                return null;
            case 15:
                return m18819Cx();
            case 16:
                m18823cf((I18NString) args[0]);
                return null;
            case 17:
                return dDi();
            case 18:
                m18831sK((I18NString) args[0]);
                return null;
            case 19:
                return dDk();
            case 20:
                m18832sM((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NoLootRoom")
    public I18NString bcg() {
        switch (bFf().mo6893i(dpa)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, dpa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dpa, new Object[0]));
                break;
        }
        return bcf();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warning")
    @C5566aOg
    /* renamed from: cg */
    public void mo11778cg(I18NString i18NString) {
        switch (bFf().mo6893i(aft)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aft, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aft, new Object[]{i18NString}));
                break;
        }
        m18823cf(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item")
    public I18NString dCX() {
        switch (bFf().mo6893i(jcN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jcN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcN, new Object[0]));
                break;
        }
        return dCW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "LootOwner")
    public I18NString dCZ() {
        switch (bFf().mo6893i(jcP)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jcP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcP, new Object[0]));
                break;
        }
        return dCY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Target Name: RemainsOf")
    public I18NString dDb() {
        switch (bFf().mo6893i(jcR)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jcR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcR, new Object[0]));
                break;
        }
        return dDa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Target Name: Ejected")
    public I18NString dDd() {
        switch (bFf().mo6893i(jcT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jcT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcT, new Object[0]));
                break;
        }
        return dDc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TakeAll")
    public I18NString dDf() {
        switch (bFf().mo6893i(jcV)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jcV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcV, new Object[0]));
                break;
        }
        return dDe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Close")
    public I18NString dDh() {
        switch (bFf().mo6893i(jcX)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jcX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcX, new Object[0]));
                break;
        }
        return dDg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asteroid")
    public I18NString dDj() {
        switch (bFf().mo6893i(jcZ)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jcZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcZ, new Object[0]));
                break;
        }
        return dDi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "You cannot drop items into loot")
    public I18NString dDl() {
        switch (bFf().mo6893i(jdb)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, jdb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jdb, new Object[0]));
                break;
        }
        return dDk();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NoLootRoom")
    @C5566aOg
    /* renamed from: ij */
    public void mo11787ij(I18NString i18NString) {
        switch (bFf().mo6893i(dpb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dpb, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dpb, new Object[]{i18NString}));
                break;
        }
        m18825ii(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "LootOwner")
    @C5566aOg
    /* renamed from: sB */
    public void mo11788sB(I18NString i18NString) {
        switch (bFf().mo6893i(jcQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcQ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcQ, new Object[]{i18NString}));
                break;
        }
        m18826sA(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Target Name: RemainsOf")
    @C5566aOg
    /* renamed from: sD */
    public void mo11789sD(I18NString i18NString) {
        switch (bFf().mo6893i(jcS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcS, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcS, new Object[]{i18NString}));
                break;
        }
        m18827sC(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Target Name: Ejected")
    @C5566aOg
    /* renamed from: sF */
    public void mo11790sF(I18NString i18NString) {
        switch (bFf().mo6893i(jcU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcU, new Object[]{i18NString}));
                break;
        }
        m18828sE(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TakeAll")
    @C5566aOg
    /* renamed from: sH */
    public void mo11791sH(I18NString i18NString) {
        switch (bFf().mo6893i(jcW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcW, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcW, new Object[]{i18NString}));
                break;
        }
        m18829sG(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Close")
    @C5566aOg
    /* renamed from: sJ */
    public void mo11792sJ(I18NString i18NString) {
        switch (bFf().mo6893i(jcY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcY, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcY, new Object[]{i18NString}));
                break;
        }
        m18830sI(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asteroid")
    /* renamed from: sL */
    public void mo11793sL(I18NString i18NString) {
        switch (bFf().mo6893i(jda)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jda, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jda, new Object[]{i18NString}));
                break;
        }
        m18831sK(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "You cannot drop items into loot")
    /* renamed from: sN */
    public void mo11794sN(I18NString i18NString) {
        switch (bFf().mo6893i(jdc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jdc, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jdc, new Object[]{i18NString}));
                break;
        }
        m18832sM(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item")
    @C5566aOg
    /* renamed from: sz */
    public void mo11795sz(I18NString i18NString) {
        switch (bFf().mo6893i(jcO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcO, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcO, new Object[]{i18NString}));
                break;
        }
        m18841sy(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m18821au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ed871e45879109bef95f3dd192ce363c", aum = 0)
    /* renamed from: au */
    private String m18821au() {
        return "Loot";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item")
    @C0064Am(aul = "6097e0d64a9c66d69d7446ccb67d552f", aum = 0)
    private I18NString dCW() {
        return dCT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "LootOwner")
    @C0064Am(aul = "6f807934e41e2ab3a203c69b123fc4ad", aum = 0)
    private I18NString dCY() {
        return dCS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Target Name: RemainsOf")
    @C0064Am(aul = "203ca9a3a127f29b04ef18d04c4cb359", aum = 0)
    private I18NString dDa() {
        return dCR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Target Name: Ejected")
    @C0064Am(aul = "b6ef40119e5c94bdd7bd6e83b68bcce9", aum = 0)
    private I18NString dDc() {
        return dCQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TakeAll")
    @C0064Am(aul = "ade2d1242e41ee69defc1142965019d9", aum = 0)
    private I18NString dDe() {
        return dCP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Close")
    @C0064Am(aul = "03b8d38142fca5740bda8a2a5cfd3539", aum = 0)
    private I18NString dDg() {
        return dCO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NoLootRoom")
    @C0064Am(aul = "99a8e3caab39f7f8f2ef77da23edb680", aum = 0)
    private I18NString bcf() {
        return baV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warning")
    @C0064Am(aul = "cdbcc90951ffbbe877dc9189b8a94dc3", aum = 0)
    /* renamed from: Cx */
    private I18NString m18819Cx() {
        return m18818Cn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asteroid")
    @C0064Am(aul = "5056da96c77cd22e6c0b1466f38fc4a6", aum = 0)
    private I18NString dDi() {
        return dCU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asteroid")
    @C0064Am(aul = "c9b114579f923606d45fc391a0787c31", aum = 0)
    /* renamed from: sK */
    private void m18831sK(I18NString i18NString) {
        m18839sw(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "You cannot drop items into loot")
    @C0064Am(aul = "b9d26c72c3cdc9cae7e41272ac1707e3", aum = 0)
    private I18NString dDk() {
        return dCV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "You cannot drop items into loot")
    @C0064Am(aul = "b03fa93eb0e6d00c3d888655b720b3a2", aum = 0)
    /* renamed from: sM */
    private void m18832sM(I18NString i18NString) {
        m18840sx(i18NString);
    }
}
