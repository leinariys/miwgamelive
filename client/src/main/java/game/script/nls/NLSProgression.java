package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0968OF;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.auJ  reason: case insensitive filesystem */
/* compiled from: a */
public class NLSProgression extends NLSType implements C1616Xf {

    /* renamed from: BP */
    public static final C5663aRz f5366BP = null;

    /* renamed from: BR */
    public static final C5663aRz f5367BR = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gEB = null;
    public static final C5663aRz gEC = null;
    public static final C5663aRz gED = null;
    public static final C5663aRz gEE = null;
    public static final C5663aRz gEF = null;
    public static final C2491fm gEG = null;
    public static final C2491fm gEH = null;
    public static final C2491fm gEI = null;
    public static final C2491fm gEJ = null;
    public static final C2491fm gEK = null;
    public static final C2491fm gEL = null;
    public static final C2491fm gEM = null;
    public static final C2491fm gEN = null;
    public static final C2491fm gEO = null;
    public static final C2491fm gEP = null;
    public static final C2491fm gEQ = null;
    public static final C2491fm gER = null;
    public static final C2491fm gES = null;
    public static final C2491fm gET = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3332227ece75e2913e71e243eb33fe47", aum = 0)
    private static I18NString dNg;
    @C0064Am(aul = "9cb090254c45266453e95bc6e3d5e933", aum = 1)
    private static I18NString dNh;
    @C0064Am(aul = "d32e93bb62253d70a5a81d5efe714263", aum = 2)
    private static I18NString dNi;
    @C0064Am(aul = "7b81445e26e5d88720018d72bc448bea", aum = 3)
    private static I18NString dNj;
    @C0064Am(aul = "ec551d4a472b16c64061929d29db1b08", aum = 4)
    private static I18NString dNk;
    @C0064Am(aul = "242d907f7acad7524d6ab90128ff3934", aum = 5)
    private static I18NString dNl;
    @C0064Am(aul = "9ea8a199b43efe27a6e7f9586581297a", aum = 6)
    private static I18NString dNm;

    static {
        m26193V();
    }

    public NLSProgression() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSProgression(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26193V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 7;
        _m_methodCount = NLSType._m_methodCount + 15;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(NLSProgression.class, "3332227ece75e2913e71e243eb33fe47", i);
        gEB = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSProgression.class, "9cb090254c45266453e95bc6e3d5e933", i2);
        gEC = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSProgression.class, "d32e93bb62253d70a5a81d5efe714263", i3);
        gED = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSProgression.class, "7b81445e26e5d88720018d72bc448bea", i4);
        gEE = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NLSProgression.class, "ec551d4a472b16c64061929d29db1b08", i5);
        gEF = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NLSProgression.class, "242d907f7acad7524d6ab90128ff3934", i6);
        f5367BR = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NLSProgression.class, "9ea8a199b43efe27a6e7f9586581297a", i7);
        f5366BP = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i9 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 15)];
        C2491fm a = C4105zY.m41624a(NLSProgression.class, "5faf2b645f3a314325ae31c0919521cc", i9);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSProgression.class, "13c58f6297666df735a390b50412e7cb", i10);
        gEG = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSProgression.class, "40dd744441004cd8f7bc823270ecfa54", i11);
        gEH = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSProgression.class, "56c12bfe7751d9b4224961b52973f0cc", i12);
        gEI = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSProgression.class, "f6fd1dcbd189b27e6beb19a85c6d71dd", i13);
        gEJ = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSProgression.class, "5971a496115bf120443a1f83b62c5732", i14);
        gEK = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSProgression.class, "2240d9971b72163c3dc4dd6e5c133e90", i15);
        gEL = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSProgression.class, "4ce1e8f1c716231b349808615f4a445b", i16);
        gEM = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSProgression.class, "ff6d046ab6258aa4126eb4db82845d92", i17);
        gEN = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSProgression.class, "07b70037c7387188c8b91202adead05b", i18);
        gEO = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSProgression.class, "154d39f1af6bd0d55ad5a888e1919130", i19);
        gEP = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(NLSProgression.class, "4be3e1b5e08a55752f70c94169047051", i20);
        gEQ = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(NLSProgression.class, "e0191ff86cfa47498c68128fd53d7a0c", i21);
        gER = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(NLSProgression.class, "85b72aa4ad0df7ac48aa8aba95f035e8", i22);
        gES = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(NLSProgression.class, "0b833b0d2b135847257ebbe48c65eeba", i23);
        gET = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSProgression.class, C0968OF.class, _m_fields, _m_methods);
    }

    private I18NString cxM() {
        return (I18NString) bFf().mo5608dq().mo3214p(gEB);
    }

    private I18NString cxN() {
        return (I18NString) bFf().mo5608dq().mo3214p(gEC);
    }

    private I18NString cxO() {
        return (I18NString) bFf().mo5608dq().mo3214p(gED);
    }

    private I18NString cxP() {
        return (I18NString) bFf().mo5608dq().mo3214p(gEE);
    }

    private I18NString cxQ() {
        return (I18NString) bFf().mo5608dq().mo3214p(gEF);
    }

    private I18NString cxR() {
        return (I18NString) bFf().mo5608dq().mo3214p(f5367BR);
    }

    private I18NString cxS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f5366BP);
    }

    /* renamed from: mV */
    private void m26195mV(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(gEB, i18NString);
    }

    /* renamed from: mW */
    private void m26196mW(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(gEC, i18NString);
    }

    /* renamed from: mX */
    private void m26197mX(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(gED, i18NString);
    }

    /* renamed from: mY */
    private void m26198mY(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(gEE, i18NString);
    }

    /* renamed from: mZ */
    private void m26199mZ(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(gEF, i18NString);
    }

    /* renamed from: na */
    private void m26200na(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f5367BR, i18NString);
    }

    /* renamed from: nb */
    private void m26201nb(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f5366BP, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enter Explorer Invulnerability")
    @C0064Am(aul = "154d39f1af6bd0d55ad5a888e1919130", aum = 0)
    @C5566aOg
    /* renamed from: nk */
    private void m26206nk(I18NString i18NString) {
        throw new aWi(new aCE(this, gEP, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Exit Explorer Invulnerability")
    @C0064Am(aul = "e0191ff86cfa47498c68128fd53d7a0c", aum = 0)
    @C5566aOg
    /* renamed from: nm */
    private void m26207nm(I18NString i18NString) {
        throw new aWi(new aCE(this, gER, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cant equip item")
    @C0064Am(aul = "0b833b0d2b135847257ebbe48c65eeba", aum = 0)
    @C5566aOg
    /* renamed from: no */
    private void m26208no(I18NString i18NString) {
        throw new aWi(new aCE(this, gET, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0968OF(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m26194au();
            case 1:
                return cxT();
            case 2:
                m26202nc((I18NString) args[0]);
                return null;
            case 3:
                return cxV();
            case 4:
                m26203ne((I18NString) args[0]);
                return null;
            case 5:
                return cxX();
            case 6:
                m26204ng((I18NString) args[0]);
                return null;
            case 7:
                return cxZ();
            case 8:
                m26205ni((I18NString) args[0]);
                return null;
            case 9:
                return cyb();
            case 10:
                m26206nk((I18NString) args[0]);
                return null;
            case 11:
                return cyd();
            case 12:
                m26207nm((I18NString) args[0]);
                return null;
            case 13:
                return cyf();
            case 14:
                m26208no((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Rating")
    public I18NString cxU() {
        switch (bFf().mo6893i(gEG)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gEG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gEG, new Object[0]));
                break;
        }
        return cxT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Merit Points")
    public I18NString cxW() {
        switch (bFf().mo6893i(gEI)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gEI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gEI, new Object[0]));
                break;
        }
        return cxV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enter Bomber Ability")
    public I18NString cxY() {
        switch (bFf().mo6893i(gEK)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gEK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gEK, new Object[0]));
                break;
        }
        return cxX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Exit Bomber Ability")
    public I18NString cya() {
        switch (bFf().mo6893i(gEM)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gEM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gEM, new Object[0]));
                break;
        }
        return cxZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enter Explorer Invulnerability")
    public I18NString cyc() {
        switch (bFf().mo6893i(gEO)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gEO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gEO, new Object[0]));
                break;
        }
        return cyb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Exit Explorer Invulnerability")
    public I18NString cye() {
        switch (bFf().mo6893i(gEQ)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gEQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gEQ, new Object[0]));
                break;
        }
        return cyd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cant equip item")
    public I18NString cyg() {
        switch (bFf().mo6893i(gES)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gES, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gES, new Object[0]));
                break;
        }
        return cyf();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Rating")
    /* renamed from: nd */
    public void mo16321nd(I18NString i18NString) {
        switch (bFf().mo6893i(gEH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gEH, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gEH, new Object[]{i18NString}));
                break;
        }
        m26202nc(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Merit Points")
    /* renamed from: nf */
    public void mo16322nf(I18NString i18NString) {
        switch (bFf().mo6893i(gEJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gEJ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gEJ, new Object[]{i18NString}));
                break;
        }
        m26203ne(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enter Bomber Ability")
    /* renamed from: nh */
    public void mo16323nh(I18NString i18NString) {
        switch (bFf().mo6893i(gEL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gEL, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gEL, new Object[]{i18NString}));
                break;
        }
        m26204ng(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Exit Bomber Ability")
    /* renamed from: nj */
    public void mo16324nj(I18NString i18NString) {
        switch (bFf().mo6893i(gEN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gEN, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gEN, new Object[]{i18NString}));
                break;
        }
        m26205ni(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enter Explorer Invulnerability")
    @C5566aOg
    /* renamed from: nl */
    public void mo16325nl(I18NString i18NString) {
        switch (bFf().mo6893i(gEP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gEP, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gEP, new Object[]{i18NString}));
                break;
        }
        m26206nk(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Exit Explorer Invulnerability")
    @C5566aOg
    /* renamed from: nn */
    public void mo16326nn(I18NString i18NString) {
        switch (bFf().mo6893i(gER)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gER, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gER, new Object[]{i18NString}));
                break;
        }
        m26207nm(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cant equip item")
    @C5566aOg
    /* renamed from: np */
    public void mo16327np(I18NString i18NString) {
        switch (bFf().mo6893i(gET)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gET, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gET, new Object[]{i18NString}));
                break;
        }
        m26208no(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m26194au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "5faf2b645f3a314325ae31c0919521cc", aum = 0)
    /* renamed from: au */
    private String m26194au() {
        return "Progression";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Rating")
    @C0064Am(aul = "13c58f6297666df735a390b50412e7cb", aum = 0)
    private I18NString cxT() {
        return cxS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Rating")
    @C0064Am(aul = "40dd744441004cd8f7bc823270ecfa54", aum = 0)
    /* renamed from: nc */
    private void m26202nc(I18NString i18NString) {
        m26201nb(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Merit Points")
    @C0064Am(aul = "56c12bfe7751d9b4224961b52973f0cc", aum = 0)
    private I18NString cxV() {
        return cxR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Merit Points")
    @C0064Am(aul = "f6fd1dcbd189b27e6beb19a85c6d71dd", aum = 0)
    /* renamed from: ne */
    private void m26203ne(I18NString i18NString) {
        m26200na(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enter Bomber Ability")
    @C0064Am(aul = "5971a496115bf120443a1f83b62c5732", aum = 0)
    private I18NString cxX() {
        return cxO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enter Bomber Ability")
    @C0064Am(aul = "2240d9971b72163c3dc4dd6e5c133e90", aum = 0)
    /* renamed from: ng */
    private void m26204ng(I18NString i18NString) {
        m26197mX(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Exit Bomber Ability")
    @C0064Am(aul = "4ce1e8f1c716231b349808615f4a445b", aum = 0)
    private I18NString cxZ() {
        return cxP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Exit Bomber Ability")
    @C0064Am(aul = "ff6d046ab6258aa4126eb4db82845d92", aum = 0)
    /* renamed from: ni */
    private void m26205ni(I18NString i18NString) {
        m26198mY(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enter Explorer Invulnerability")
    @C0064Am(aul = "07b70037c7387188c8b91202adead05b", aum = 0)
    private I18NString cyb() {
        return cxM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Exit Explorer Invulnerability")
    @C0064Am(aul = "4be3e1b5e08a55752f70c94169047051", aum = 0)
    private I18NString cyd() {
        return cxN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cant equip item")
    @C0064Am(aul = "85b72aa4ad0df7ac48aa8aba95f035e8", aum = 0)
    private I18NString cyf() {
        return cxQ();
    }
}
