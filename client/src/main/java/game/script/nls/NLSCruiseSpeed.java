package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3739uQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aUl  reason: case insensitive filesystem */
/* compiled from: a */
public class NLSCruiseSpeed extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz iWJ = null;
    public static final C5663aRz iWK = null;
    public static final C5663aRz iWL = null;
    public static final C5663aRz iWM = null;
    public static final C5663aRz iWN = null;
    public static final C5663aRz iWO = null;
    public static final C2491fm iWP = null;
    public static final C2491fm iWQ = null;
    public static final C2491fm iWR = null;
    public static final C2491fm iWS = null;
    public static final C2491fm iWT = null;
    public static final C2491fm iWU = null;
    public static final C2491fm iWV = null;
    public static final C2491fm iWW = null;
    public static final C2491fm iWX = null;
    public static final C2491fm iWY = null;
    public static final C2491fm iWZ = null;
    public static final C2491fm iXa = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1a16d1a654867385fc9c847219105e20", aum = 0)
    private static I18NString bxq;
    @C0064Am(aul = "0b8fc35901ee5fadf759ac8ced4d7429", aum = 1)
    private static I18NString bxr;
    @C0064Am(aul = "2f60fe604651c308e23a591e13340604", aum = 2)
    private static I18NString bxs;
    @C0064Am(aul = "96dfcb2dd4c771807e83bf12d24475f5", aum = 3)
    private static I18NString bxt;
    @C0064Am(aul = "06964d3b65083a020bdab27101f93d66", aum = 4)
    private static I18NString bxu;
    @C0064Am(aul = "8d2e937314ea8bb55feaab7bfdd3b5e6", aum = 5)
    private static I18NString bxv;

    static {
        m18701V();
    }

    public NLSCruiseSpeed() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSCruiseSpeed(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18701V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 6;
        _m_methodCount = NLSType._m_methodCount + 13;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(NLSCruiseSpeed.class, "1a16d1a654867385fc9c847219105e20", i);
        iWJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSCruiseSpeed.class, "0b8fc35901ee5fadf759ac8ced4d7429", i2);
        iWK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSCruiseSpeed.class, "2f60fe604651c308e23a591e13340604", i3);
        iWL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NLSCruiseSpeed.class, "96dfcb2dd4c771807e83bf12d24475f5", i4);
        iWM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NLSCruiseSpeed.class, "06964d3b65083a020bdab27101f93d66", i5);
        iWN = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NLSCruiseSpeed.class, "8d2e937314ea8bb55feaab7bfdd3b5e6", i6);
        iWO = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i8 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 13)];
        C2491fm a = C4105zY.m41624a(NLSCruiseSpeed.class, "026456c7445b758c366a9a0c7e1b9a67", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSCruiseSpeed.class, "0a5de0bde79aab7adda3d914d96d19c6", i9);
        iWP = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSCruiseSpeed.class, "21ca7c7d9eb7b2744382d469ee693f80", i10);
        iWQ = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSCruiseSpeed.class, "6c21748339753f9ce87f59b5012a094f", i11);
        iWR = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSCruiseSpeed.class, "6a8b449435aa574317a7d4bd102e8daa", i12);
        iWS = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSCruiseSpeed.class, "60cb0a68da0a4e0ab0d74fb87de3e893", i13);
        iWT = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSCruiseSpeed.class, "045df92b8c142f7d7d763fbd9742bb24", i14);
        iWU = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(NLSCruiseSpeed.class, "e46c44a5ba5b70e1364d36f0e56c0d28", i15);
        iWV = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(NLSCruiseSpeed.class, "3efa02da40765c91bf54879a45958541", i16);
        iWW = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(NLSCruiseSpeed.class, "397f14e04d88f22a3f4e3260921200a8", i17);
        iWX = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(NLSCruiseSpeed.class, "469b9f768dbabc1d6076f436d88b29d6", i18);
        iWY = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(NLSCruiseSpeed.class, "42517cbb52f435bc9a796b8e6f1a58ae", i19);
        iWZ = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(NLSCruiseSpeed.class, "7af19bc40721d109ddf81cf0e6f3c907", i20);
        iXa = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSCruiseSpeed.class, C3739uQ.class, _m_fields, _m_methods);
    }

    private I18NString dzJ() {
        return (I18NString) bFf().mo5608dq().mo3214p(iWJ);
    }

    private I18NString dzK() {
        return (I18NString) bFf().mo5608dq().mo3214p(iWK);
    }

    private I18NString dzL() {
        return (I18NString) bFf().mo5608dq().mo3214p(iWL);
    }

    private I18NString dzM() {
        return (I18NString) bFf().mo5608dq().mo3214p(iWM);
    }

    private I18NString dzN() {
        return (I18NString) bFf().mo5608dq().mo3214p(iWN);
    }

    private I18NString dzO() {
        return (I18NString) bFf().mo5608dq().mo3214p(iWO);
    }

    /* renamed from: rS */
    private void m18703rS(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iWJ, i18NString);
    }

    /* renamed from: rT */
    private void m18704rT(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iWK, i18NString);
    }

    /* renamed from: rU */
    private void m18705rU(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iWL, i18NString);
    }

    /* renamed from: rV */
    private void m18706rV(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iWM, i18NString);
    }

    /* renamed from: rW */
    private void m18707rW(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iWN, i18NString);
    }

    /* renamed from: rX */
    private void m18708rX(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iWO, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activating")
    @C0064Am(aul = "21ca7c7d9eb7b2744382d469ee693f80", aum = 0)
    @C5566aOg
    /* renamed from: rY */
    private void m18709rY(I18NString i18NString) {
        throw new aWi(new aCE(this, iWQ, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CoolingDown")
    @C0064Am(aul = "6a8b449435aa574317a7d4bd102e8daa", aum = 0)
    @C5566aOg
    /* renamed from: sa */
    private void m18710sa(I18NString i18NString) {
        throw new aWi(new aCE(this, iWS, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CruiseDeactivated")
    @C0064Am(aul = "045df92b8c142f7d7d763fbd9742bb24", aum = 0)
    @C5566aOg
    /* renamed from: sc */
    private void m18711sc(I18NString i18NString) {
        throw new aWi(new aCE(this, iWU, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CruiseActivated")
    @C0064Am(aul = "3efa02da40765c91bf54879a45958541", aum = 0)
    @C5566aOg
    /* renamed from: se */
    private void m18712se(I18NString i18NString) {
        throw new aWi(new aCE(this, iWW, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ActiveInTime")
    @C0064Am(aul = "469b9f768dbabc1d6076f436d88b29d6", aum = 0)
    @C5566aOg
    /* renamed from: sg */
    private void m18713sg(I18NString i18NString) {
        throw new aWi(new aCE(this, iWY, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CantForTime")
    @C0064Am(aul = "7af19bc40721d109ddf81cf0e6f3c907", aum = 0)
    @C5566aOg
    /* renamed from: si */
    private void m18714si(I18NString i18NString) {
        throw new aWi(new aCE(this, iXa, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3739uQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m18702au();
            case 1:
                return dzP();
            case 2:
                m18709rY((I18NString) args[0]);
                return null;
            case 3:
                return dzR();
            case 4:
                m18710sa((I18NString) args[0]);
                return null;
            case 5:
                return dzT();
            case 6:
                m18711sc((I18NString) args[0]);
                return null;
            case 7:
                return dzV();
            case 8:
                m18712se((I18NString) args[0]);
                return null;
            case 9:
                return dzX();
            case 10:
                m18713sg((I18NString) args[0]);
                return null;
            case 11:
                return dzZ();
            case 12:
                m18714si((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CantForTime")
    public I18NString dAa() {
        switch (bFf().mo6893i(iWZ)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iWZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWZ, new Object[0]));
                break;
        }
        return dzZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activating")
    public I18NString dzQ() {
        switch (bFf().mo6893i(iWP)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iWP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWP, new Object[0]));
                break;
        }
        return dzP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CoolingDown")
    public I18NString dzS() {
        switch (bFf().mo6893i(iWR)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iWR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWR, new Object[0]));
                break;
        }
        return dzR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CruiseDeactivated")
    public I18NString dzU() {
        switch (bFf().mo6893i(iWT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iWT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWT, new Object[0]));
                break;
        }
        return dzT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CruiseActivated")
    public I18NString dzW() {
        switch (bFf().mo6893i(iWV)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iWV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWV, new Object[0]));
                break;
        }
        return dzV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ActiveInTime")
    public I18NString dzY() {
        switch (bFf().mo6893i(iWX)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iWX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWX, new Object[0]));
                break;
        }
        return dzX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activating")
    @C5566aOg
    /* renamed from: rZ */
    public void mo11690rZ(I18NString i18NString) {
        switch (bFf().mo6893i(iWQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iWQ, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iWQ, new Object[]{i18NString}));
                break;
        }
        m18709rY(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CoolingDown")
    @C5566aOg
    /* renamed from: sb */
    public void mo11691sb(I18NString i18NString) {
        switch (bFf().mo6893i(iWS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iWS, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iWS, new Object[]{i18NString}));
                break;
        }
        m18710sa(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CruiseDeactivated")
    @C5566aOg
    /* renamed from: sd */
    public void mo11692sd(I18NString i18NString) {
        switch (bFf().mo6893i(iWU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iWU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iWU, new Object[]{i18NString}));
                break;
        }
        m18711sc(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CruiseActivated")
    @C5566aOg
    /* renamed from: sf */
    public void mo11693sf(I18NString i18NString) {
        switch (bFf().mo6893i(iWW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iWW, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iWW, new Object[]{i18NString}));
                break;
        }
        m18712se(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ActiveInTime")
    @C5566aOg
    /* renamed from: sh */
    public void mo11694sh(I18NString i18NString) {
        switch (bFf().mo6893i(iWY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iWY, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iWY, new Object[]{i18NString}));
                break;
        }
        m18713sg(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "CantForTime")
    @C5566aOg
    /* renamed from: sj */
    public void mo11695sj(I18NString i18NString) {
        switch (bFf().mo6893i(iXa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iXa, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iXa, new Object[]{i18NString}));
                break;
        }
        m18714si(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m18702au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "026456c7445b758c366a9a0c7e1b9a67", aum = 0)
    /* renamed from: au */
    private String m18702au() {
        return "CruiseSpeed";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activating")
    @C0064Am(aul = "0a5de0bde79aab7adda3d914d96d19c6", aum = 0)
    private I18NString dzP() {
        return dzO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CoolingDown")
    @C0064Am(aul = "6c21748339753f9ce87f59b5012a094f", aum = 0)
    private I18NString dzR() {
        return dzN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CruiseDeactivated")
    @C0064Am(aul = "60cb0a68da0a4e0ab0d74fb87de3e893", aum = 0)
    private I18NString dzT() {
        return dzM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CruiseActivated")
    @C0064Am(aul = "e46c44a5ba5b70e1364d36f0e56c0d28", aum = 0)
    private I18NString dzV() {
        return dzL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ActiveInTime")
    @C0064Am(aul = "397f14e04d88f22a3f4e3260921200a8", aum = 0)
    private I18NString dzX() {
        return dzK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CantForTime")
    @C0064Am(aul = "42517cbb52f435bc9a796b8e6f1a58ae", aum = 0)
    private I18NString dzZ() {
        return dzJ();
    }
}
