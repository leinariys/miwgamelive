package game.script.nls;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import logic.baa.*;
import logic.data.mbean.C6422ame;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.yj */
/* compiled from: a */
public class TranslationNode extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bJM = null;
    public static final C5663aRz bJO = null;
    public static final C5663aRz bJQ = null;
    public static final C2491fm bJR = null;
    public static final C2491fm bJS = null;
    public static final C2491fm bJT = null;
    public static final C2491fm bJU = null;
    public static final C2491fm bJV = null;
    public static final C2491fm bJW = null;
    public static final C2491fm bJX = null;
    public static final C2491fm bJY = null;
    public static final C2491fm bJZ = null;
    public static final C2491fm bKa = null;
    public static final C2491fm bKb = null;
    /* renamed from: bL */
    public static final C5663aRz f9599bL = null;
    /* renamed from: bN */
    public static final C2491fm f9600bN = null;
    /* renamed from: bO */
    public static final C2491fm f9601bO = null;
    /* renamed from: bP */
    public static final C2491fm f9602bP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "180ed3adf86117f3e475b294a9b6859a", aum = 0)
    private static C1556Wo<String, I18NString> bJL;
    @C0064Am(aul = "cef4f206ca3ed48615a80517c9ceee84", aum = 1)
    @C5566aOg
    private static C1556Wo<String, String> bJN;
    @C0064Am(aul = "35bd14636a6daf7df027a543948c82ab", aum = 2)
    private static String bJP;
    @C0064Am(aul = "beaf1c09251fe7a9bd90ffef5eb4f643", aum = 3)

    /* renamed from: bK */
    private static UUID f9598bK;

    static {
        m41376V();
    }

    public TranslationNode() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TranslationNode(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41376V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 15;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(TranslationNode.class, "180ed3adf86117f3e475b294a9b6859a", i);
        bJM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TranslationNode.class, "cef4f206ca3ed48615a80517c9ceee84", i2);
        bJO = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TranslationNode.class, "35bd14636a6daf7df027a543948c82ab", i3);
        bJQ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TranslationNode.class, "beaf1c09251fe7a9bd90ffef5eb4f643", i4);
        f9599bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 15)];
        C2491fm a = C4105zY.m41624a(TranslationNode.class, "1963c2bef5b5f3de6e55782c4e0ab394", i6);
        f9600bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(TranslationNode.class, "aaf8dbf3bb0222f9fad2d5c5681baa9c", i7);
        f9601bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(TranslationNode.class, "d467fae5e6fdd81b6b2affccf2ac6c34", i8);
        bJR = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(TranslationNode.class, "dbefce3d2d410aa2bd9156b401c016a3", i9);
        bJS = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(TranslationNode.class, "35bd636d70927c22eb5e5910d76a4e8a", i10);
        bJT = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(TranslationNode.class, "d0abbfab2eb9e418e06825bb11e81756", i11);
        bJU = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(TranslationNode.class, "95f67517914d4658794676c4824d268b", i12);
        bJV = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(TranslationNode.class, "8a3ca013fa561916610f91db1de44c87", i13);
        bJW = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(TranslationNode.class, "4cc5b918f7f20763c0d455ac6451df0b", i14);
        bJX = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(TranslationNode.class, "9d61e47ff8025dd58ff324b04ea28fb0", i15);
        bJY = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(TranslationNode.class, "af602f158d1645d9ea76c121632d5cf6", i16);
        bJZ = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(TranslationNode.class, "b80644b04b7fcabab04ffa15911532c0", i17);
        bKa = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(TranslationNode.class, "2c123390e84995324a2d879fe137a651", i18);
        bKb = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(TranslationNode.class, "a33a2bffadde220ef80abc1c950e29cb", i19);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(TranslationNode.class, "71346397b96218f2a6e996f6f5380221", i20);
        f9602bP = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TranslationNode.class, C6422ame.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "8a3ca013fa561916610f91db1de44c87", aum = 0)
    @C5566aOg
    @C5794aaa
    /* renamed from: a */
    private I18NString m41377a(String str, I18NString i18NString) {
        throw new aWi(new aCE(this, bJW, new Object[]{str, i18NString}));
    }

    /* renamed from: a */
    private void m41378a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9599bL, uuid);
    }

    /* renamed from: an */
    private UUID m41379an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9599bL);
    }

    private C1556Wo apA() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bJO);
    }

    private String apB() {
        return (String) bFf().mo5608dq().mo3214p(bJQ);
    }

    @C5566aOg
    @C5794aaa
    @C0064Am(aul = "d0abbfab2eb9e418e06825bb11e81756", aum = 0)
    private Map<String, String> apE() {
        throw new aWi(new aCE(this, bJU, new Object[0]));
    }

    private C1556Wo apz() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bJM);
    }

    /* renamed from: c */
    private void m41384c(UUID uuid) {
        switch (bFf().mo6893i(f9601bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9601bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9601bO, new Object[]{uuid}));
                break;
        }
        m41383b(uuid);
    }

    /* renamed from: cH */
    private void m41385cH(String str) {
        bFf().mo5608dq().mo3197f(bJQ, str);
    }

    @C0064Am(aul = "35bd636d70927c22eb5e5910d76a4e8a", aum = 0)
    @C5566aOg
    @C5794aaa
    /* renamed from: cI */
    private String m41386cI(String str) {
        throw new aWi(new aCE(this, bJT, new Object[]{str}));
    }

    @C0064Am(aul = "4cc5b918f7f20763c0d455ac6451df0b", aum = 0)
    @C5566aOg
    @C5794aaa
    /* renamed from: cK */
    private void m41387cK(String str) {
        throw new aWi(new aCE(this, bJX, new Object[]{str}));
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Translations")
    @C0064Am(aul = "dbefce3d2d410aa2bd9156b401c016a3", aum = 0)
    /* renamed from: e */
    private void m41391e(Map<String, I18NString> map) {
        throw new aWi(new aCE(this, bJS, new Object[]{map}));
    }

    /* renamed from: t */
    private void m41392t(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bJM, wo);
    }

    /* renamed from: u */
    private void m41393u(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bJO, wo);
    }

    @C0064Am(aul = "95f67517914d4658794676c4824d268b", aum = 0)
    @C5566aOg
    @C5794aaa
    /* renamed from: v */
    private void m41394v(String str, String str2) {
        throw new aWi(new aCE(this, bJV, new Object[]{str, str2}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6422ame(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m41380ap();
            case 1:
                m41383b((UUID) args[0]);
                return null;
            case 2:
                return apC();
            case 3:
                m41391e((Map) args[0]);
                return null;
            case 4:
                return m41386cI((String) args[0]);
            case 5:
                return apE();
            case 6:
                m41394v((String) args[0], (String) args[1]);
                return null;
            case 7:
                return m41377a((String) args[0], (I18NString) args[1]);
            case 8:
                m41387cK((String) args[0]);
                return null;
            case 9:
                return m41388cL((String) args[0]);
            case 10:
                return m41389cM((String) args[0]);
            case 11:
                return apG();
            case 12:
                m41390cO((String) args[0]);
                return null;
            case 13:
                return m41382au();
            case 14:
                return m41381ar();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Translations")
    @C5794aaa
    public Map<String, I18NString> apD() {
        switch (bFf().mo6893i(bJR)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, bJR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJR, new Object[0]));
                break;
        }
        return apC();
    }

    @C5566aOg
    @C5794aaa
    public Map<String, String> apF() {
        switch (bFf().mo6893i(bJU)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, bJU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJU, new Object[0]));
                break;
        }
        return apE();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9600bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9600bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9600bN, new Object[0]));
                break;
        }
        return m41380ap();
    }

    @C5566aOg
    @C5794aaa
    /* renamed from: b */
    public I18NString mo23168b(String str, I18NString i18NString) {
        switch (bFf().mo6893i(bJW)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, bJW, new Object[]{str, i18NString}));
            case 3:
                bFf().mo5606d(new aCE(this, bJW, new Object[]{str, i18NString}));
                break;
        }
        return m41377a(str, i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C5794aaa
    /* renamed from: cJ */
    public String mo23169cJ(String str) {
        switch (bFf().mo6893i(bJT)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bJT, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, bJT, new Object[]{str}));
                break;
        }
        return m41386cI(str);
    }

    /* renamed from: cN */
    public I18NString mo23170cN(String str) {
        switch (bFf().mo6893i(bJZ)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, bJZ, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, bJZ, new Object[]{str}));
                break;
        }
        return m41389cM(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Translations")
    @C5566aOg
    /* renamed from: f */
    public void mo23171f(Map<String, I18NString> map) {
        switch (bFf().mo6893i(bJS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJS, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJS, new Object[]{map}));
                break;
        }
        m41391e(map);
    }

    public String get(String str) {
        switch (bFf().mo6893i(bJY)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bJY, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, bJY, new Object[]{str}));
                break;
        }
        return m41388cL(str);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f9602bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9602bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9602bP, new Object[0]));
                break;
        }
        return m41381ar();
    }

    public String getScope() {
        switch (bFf().mo6893i(bKa)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bKa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bKa, new Object[0]));
                break;
        }
        return apG();
    }

    public void setScope(String str) {
        switch (bFf().mo6893i(bKb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bKb, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bKb, new Object[]{str}));
                break;
        }
        m41390cO(str);
    }

    @C5566aOg
    @C5794aaa
    public void remove(String str) {
        switch (bFf().mo6893i(bJX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJX, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJX, new Object[]{str}));
                break;
        }
        m41387cK(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m41382au();
    }

    @C5566aOg
    @C5794aaa
    /* renamed from: w */
    public void mo23176w(String str, String str2) {
        switch (bFf().mo6893i(bJV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJV, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJV, new Object[]{str, str2}));
                break;
        }
        m41394v(str, str2);
    }

    @C0064Am(aul = "1963c2bef5b5f3de6e55782c4e0ab394", aum = 0)
    /* renamed from: ap */
    private UUID m41380ap() {
        return m41379an();
    }

    @C0064Am(aul = "aaf8dbf3bb0222f9fad2d5c5681baa9c", aum = 0)
    /* renamed from: b */
    private void m41383b(UUID uuid) {
        m41378a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m41378a(UUID.randomUUID());
    }

    @C5794aaa
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Translations")
    @C0064Am(aul = "d467fae5e6fdd81b6b2affccf2ac6c34", aum = 0)
    private Map<String, I18NString> apC() {
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(apz());
        return Collections.unmodifiableMap(treeMap);
    }

    @C0064Am(aul = "9d61e47ff8025dd58ff324b04ea28fb0", aum = 0)
    /* renamed from: cL */
    private String m41388cL(String str) {
        I18NString i18NString = (I18NString) apz().get(str);
        if (i18NString != null) {
            return i18NString.get();
        }
        return null;
    }

    @C0064Am(aul = "af602f158d1645d9ea76c121632d5cf6", aum = 0)
    /* renamed from: cM */
    private I18NString m41389cM(String str) {
        return (I18NString) apz().get(str);
    }

    @C0064Am(aul = "b80644b04b7fcabab04ffa15911532c0", aum = 0)
    private String apG() {
        return apB();
    }

    @C0064Am(aul = "2c123390e84995324a2d879fe137a651", aum = 0)
    /* renamed from: cO */
    private void m41390cO(String str) {
        m41385cH(str);
    }

    @C0064Am(aul = "a33a2bffadde220ef80abc1c950e29cb", aum = 0)
    /* renamed from: au */
    private String m41382au() {
        return apB();
    }

    @C0064Am(aul = "71346397b96218f2a6e996f6f5380221", aum = 0)
    /* renamed from: ar */
    private String m41381ar() {
        return apB();
    }
}
