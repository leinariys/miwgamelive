package game.script.nls;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5940adQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aCH */
/* compiled from: a */
public class NLSCloning extends NLSType implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz hwZ = null;
    public static final C5663aRz hxa = null;
    public static final C5663aRz hxb = null;
    public static final C2491fm hxc = null;
    public static final C2491fm hxd = null;
    public static final C2491fm hxe = null;
    public static final C2491fm hxf = null;
    public static final C2491fm hxg = null;
    public static final C2491fm hxh = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6acfaf259d561570c28b9113799b338a", aum = 0)
    private static I18NString flw;
    @C0064Am(aul = "02885ef25959b974f7d041d3a692d13c", aum = 1)
    private static I18NString flx;
    @C0064Am(aul = "46c343911f9d2ed0bbc05d1b95abd203", aum = 2)
    private static I18NString fly;

    static {
        m13144V();
    }

    public NLSCloning() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NLSCloning(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13144V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NLSType._m_fieldCount + 3;
        _m_methodCount = NLSType._m_methodCount + 7;
        int i = NLSType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(NLSCloning.class, "6acfaf259d561570c28b9113799b338a", i);
        hwZ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NLSCloning.class, "02885ef25959b974f7d041d3a692d13c", i2);
        hxa = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NLSCloning.class, "46c343911f9d2ed0bbc05d1b95abd203", i3);
        hxb = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NLSType._m_fields, (Object[]) _m_fields);
        int i5 = NLSType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(NLSCloning.class, "e70663e7412b4878a5d113bc03515181", i5);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(NLSCloning.class, "a44c068e180effc149490f49a8506444", i6);
        hxc = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(NLSCloning.class, "5f7ac6844a2d59dc27f553f745fab49c", i7);
        hxd = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(NLSCloning.class, "b6513f6f9759e158d7f6f9e84e224b5f", i8);
        hxe = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(NLSCloning.class, "9ec6f74b3ce8951128a0ffe09fcd75f2", i9);
        hxf = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(NLSCloning.class, "842d78bcf674ab0fdb1f930a85e042a1", i10);
        hxg = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(NLSCloning.class, "2078338d3f7683effe3c5b05877bbbd4", i11);
        hxh = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NLSType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NLSCloning.class, C5940adQ.class, _m_fields, _m_methods);
    }

    private I18NString cQi() {
        return (I18NString) bFf().mo5608dq().mo3214p(hwZ);
    }

    private I18NString cQj() {
        return (I18NString) bFf().mo5608dq().mo3214p(hxa);
    }

    private I18NString cQk() {
        return (I18NString) bFf().mo5608dq().mo3214p(hxb);
    }

    /* renamed from: nB */
    private void m13146nB(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(hwZ, i18NString);
    }

    /* renamed from: nC */
    private void m13147nC(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(hxa, i18NString);
    }

    /* renamed from: nD */
    private void m13148nD(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(hxb, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Descr 1")
    @C0064Am(aul = "5f7ac6844a2d59dc27f553f745fab49c", aum = 0)
    @C5566aOg
    /* renamed from: nE */
    private void m13149nE(I18NString i18NString) {
        throw new aWi(new aCE(this, hxd, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Descr 2")
    @C0064Am(aul = "9ec6f74b3ce8951128a0ffe09fcd75f2", aum = 0)
    @C5566aOg
    /* renamed from: nG */
    private void m13150nG(I18NString i18NString) {
        throw new aWi(new aCE(this, hxf, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "You have been rebornt at {0}. Remaining clones: {1}")
    @C0064Am(aul = "2078338d3f7683effe3c5b05877bbbd4", aum = 0)
    @C5566aOg
    /* renamed from: nI */
    private void m13151nI(I18NString i18NString) {
        throw new aWi(new aCE(this, hxh, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5940adQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NLSType._m_methodCount) {
            case 0:
                return m13145au();
            case 1:
                return cQl();
            case 2:
                m13149nE((I18NString) args[0]);
                return null;
            case 3:
                return cQn();
            case 4:
                m13150nG((I18NString) args[0]);
                return null;
            case 5:
                return cQp();
            case 6:
                m13151nI((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Descr 1")
    public I18NString cQm() {
        switch (bFf().mo6893i(hxc)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hxc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hxc, new Object[0]));
                break;
        }
        return cQl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Descr 2")
    public I18NString cQo() {
        switch (bFf().mo6893i(hxe)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hxe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hxe, new Object[0]));
                break;
        }
        return cQn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "You have been rebornt at {0}. Remaining clones: {1}")
    public I18NString cQq() {
        switch (bFf().mo6893i(hxg)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hxg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hxg, new Object[0]));
                break;
        }
        return cQp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Descr 1")
    @C5566aOg
    /* renamed from: nF */
    public void mo8007nF(I18NString i18NString) {
        switch (bFf().mo6893i(hxd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxd, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxd, new Object[]{i18NString}));
                break;
        }
        m13149nE(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Descr 2")
    @C5566aOg
    /* renamed from: nH */
    public void mo8008nH(I18NString i18NString) {
        switch (bFf().mo6893i(hxf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxf, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxf, new Object[]{i18NString}));
                break;
        }
        m13150nG(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "You have been rebornt at {0}. Remaining clones: {1}")
    @C5566aOg
    /* renamed from: nJ */
    public void mo8009nJ(I18NString i18NString) {
        switch (bFf().mo6893i(hxh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxh, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxh, new Object[]{i18NString}));
                break;
        }
        m13151nI(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m13145au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "e70663e7412b4878a5d113bc03515181", aum = 0)
    /* renamed from: au */
    private String m13145au() {
        return "Cloning";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Descr 1")
    @C0064Am(aul = "a44c068e180effc149490f49a8506444", aum = 0)
    private I18NString cQl() {
        return cQj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Descr 2")
    @C0064Am(aul = "b6513f6f9759e158d7f6f9e84e224b5f", aum = 0)
    private I18NString cQn() {
        return cQk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "You have been rebornt at {0}. Remaining clones: {1}")
    @C0064Am(aul = "842d78bcf674ab0fdb1f930a85e042a1", aum = 0)
    private I18NString cQp() {
        return cQi();
    }
}
