package game.script.hazardarea.HazardArea;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.login.UserConnection;
import game.script.simulation.Space;
import logic.aaa.C2235dB;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.bbb.C6348alI;
import logic.data.link.C3594sh;
import logic.data.mbean.C0331EU;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.auA  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class HollowActor extends Actor implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aRm = null;
    public static final C2491fm gDT = null;
    public static final C2491fm gDU = null;
    public static final C2491fm gDV = null;
    public static final C2491fm gDW = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uY */
    public static final C2491fm f5352uY = null;
    /* renamed from: vc */
    public static final C2491fm f5353vc = null;
    /* renamed from: ve */
    public static final C2491fm f5354ve = null;
    /* renamed from: vi */
    public static final C2491fm f5355vi = null;
    /* renamed from: vm */
    public static final C2491fm f5356vm = null;
    /* renamed from: vn */
    public static final C2491fm f5357vn = null;
    /* renamed from: vo */
    public static final C2491fm f5358vo = null;
    /* renamed from: vs */
    public static final C2491fm f5359vs = null;
    public static C6494any ___iScriptClass;

    static {
        m26155V();
    }

    public HollowActor() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HollowActor(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26155V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 0;
        _m_methodCount = Actor._m_methodCount + 14;
        _m_fields = new C5663aRz[(Actor._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 14)];
        C2491fm a = C4105zY.m41624a(HollowActor.class, C3594sh.bgb, i);
        aRm = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(HollowActor.class, "f35d6c2ab8d53a6089f09bc377289fcc", i2);
        f5356vm = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(HollowActor.class, "67b32e73175ad83bc45e37f3a0a73779", i3);
        f5357vn = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(HollowActor.class, "6fe3c07535305487cb907c8631c7807c", i4);
        f5358vo = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(HollowActor.class, "47af12d1689c85b20e919544dd5e7247", i5);
        f5353vc = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(HollowActor.class, "ea11527ce4e4c04916ec32554bf4d183", i6);
        f5354ve = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(HollowActor.class, "c5e303401dcca26ccfd0ceeca7971bb6", i7);
        f5355vi = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(HollowActor.class, "fe28a8a17124585122701e5413463025", i8);
        gDT = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(HollowActor.class, "0ac477dbf54dc3fbf1bb040752e5e886", i9);
        gDU = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(HollowActor.class, "2aa3e1854f9ab578de3dd2001d5f0454", i10);
        f5359vs = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(HollowActor.class, "6572fe5647553690528d6d021c02f303", i11);
        _f_onResurrect_0020_0028_0029V = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(HollowActor.class, "703a0cafc3c31989dd1dc5fe022036a9", i12);
        gDV = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(HollowActor.class, "345c7ebb45ef3cbd9e6fcab313114022", i13);
        gDW = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        C2491fm a14 = C4105zY.m41624a(HollowActor.class, "736080d9365f8ab3e4c91eac3820f71e", i14);
        f5352uY = a14;
        fmVarArr[i14] = a14;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HollowActor.class, C0331EU.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "703a0cafc3c31989dd1dc5fe022036a9", aum = 0)
    private void cxH() {
        throw new aWi(new aCE(this, gDV, new Object[0]));
    }

    @C0064Am(aul = "345c7ebb45ef3cbd9e6fcab313114022", aum = 0)
    private void cxJ() {
        throw new aWi(new aCE(this, gDW, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0331EU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return new Boolean(m26161c((C1722ZT) args[0]));
            case 1:
                m26157a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 2:
                m26160c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 3:
                return m26165iz();
            case 4:
                return m26162io();
            case 5:
                return m26163iq();
            case 6:
                return m26164it();
            case 7:
                cxD();
                return null;
            case 8:
                cxF();
                return null;
            case 9:
                m26156a((Actor.C0200h) args[0]);
                return null;
            case 10:
                m26158aG();
                return null;
            case 11:
                cxH();
                return null;
            case 12:
                cxJ();
                return null;
            case 13:
                return m26159c((Space) args[0], ((Long) args[1]).longValue());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m26158aG();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f5359vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5359vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5359vs, new Object[]{hVar}));
                break;
        }
        m26156a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f5356vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5356vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5356vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m26157a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cxE() {
        switch (bFf().mo6893i(gDT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDT, new Object[0]));
                break;
        }
        cxD();
    }

    public void cxG() {
        switch (bFf().mo6893i(gDU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDU, new Object[0]));
                break;
        }
        cxF();
    }

    /* access modifiers changed from: protected */
    public void cxI() {
        switch (bFf().mo6893i(gDV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDV, new Object[0]));
                break;
        }
        cxH();
    }

    /* access modifiers changed from: protected */
    public void cxK() {
        switch (bFf().mo6893i(gDW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDW, new Object[0]));
                break;
        }
        cxJ();
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f5352uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f5352uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f5352uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m26159c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f5357vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5357vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5357vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m26160c(cr, acm, vec3f, vec3f2);
    }

    @C5472aKq
    /* renamed from: d */
    public boolean mo16293d(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(aRm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}));
                break;
        }
        return m26161c(zt);
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f5358vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5358vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5358vo, new Object[0]));
                break;
        }
        return m26165iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f5353vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5353vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5353vc, new Object[0]));
                break;
        }
        return m26162io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f5354ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5354ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5354ve, new Object[0]));
                break;
        }
        return m26163iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f5355vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5355vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5355vi, new Object[0]));
                break;
        }
        return m26164it();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        setStatic(true);
    }

    @C0064Am(aul = "a3c35945097ec72aa735288270c2cf0a", aum = 0)
    @C5472aKq
    /* renamed from: c */
    private boolean m26161c(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        } else if (bGZ()) {
            if (zt.bFq().mo15388dL() == aPA()) {
                return true;
            }
            return false;
        } else if (zt.bFq().mo15388dL() == getPlayer()) {
            return true;
        } else {
            return false;
        }
    }

    @C0064Am(aul = "f35d6c2ab8d53a6089f09bc377289fcc", aum = 0)
    /* renamed from: a */
    private void m26157a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "67b32e73175ad83bc45e37f3a0a73779", aum = 0)
    /* renamed from: c */
    private void m26160c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "6fe3c07535305487cb907c8631c7807c", aum = 0)
    /* renamed from: iz */
    private String m26165iz() {
        return null;
    }

    @C0064Am(aul = "47af12d1689c85b20e919544dd5e7247", aum = 0)
    /* renamed from: io */
    private String m26162io() {
        return null;
    }

    @C0064Am(aul = "ea11527ce4e4c04916ec32554bf4d183", aum = 0)
    /* renamed from: iq */
    private String m26163iq() {
        return null;
    }

    @C0064Am(aul = "c5e303401dcca26ccfd0ceeca7971bb6", aum = 0)
    /* renamed from: it */
    private String m26164it() {
        return null;
    }

    @C0064Am(aul = "fe28a8a17124585122701e5413463025", aum = 0)
    private void cxD() {
        cxI();
    }

    @C0064Am(aul = "0ac477dbf54dc3fbf1bb040752e5e886", aum = 0)
    private void cxF() {
        cxK();
    }

    @C0064Am(aul = "2aa3e1854f9ab578de3dd2001d5f0454", aum = 0)
    /* renamed from: a */
    private void m26156a(Actor.C0200h hVar) {
        C6348alI ali = new C6348alI(1.0f);
        ali.setTransparent(true);
        hVar.mo1103c(ali);
    }

    @C0064Am(aul = "6572fe5647553690528d6d021c02f303", aum = 0)
    /* renamed from: aG */
    private void m26158aG() {
        super.mo70aH();
    }

    @C0064Am(aul = "736080d9365f8ab3e4c91eac3820f71e", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m26159c(Space ea, long j) {
        aBA aba = new aBA(ea, this, mo958IL());
        aba.mo2449a(ea.cKn().mo5725a(j, (C2235dB) aba, 13));
        return aba;
    }
}
