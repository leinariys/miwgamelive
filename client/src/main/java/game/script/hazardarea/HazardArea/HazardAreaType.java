package game.script.hazardarea.HazardArea;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.buff.module.AttributeBuffType;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C1644YD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.ass  reason: case insensitive filesystem */
/* compiled from: a */
public class HazardAreaType extends BaseHazardAreaType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aQU = null;
    public static final C5663aRz aQW = null;
    public static final C5663aRz aQY = null;
    public static final C5663aRz aRa = null;
    public static final C2491fm awp = null;
    public static final C2491fm awq = null;
    /* renamed from: dN */
    public static final C2491fm f5291dN = null;
    public static final C2491fm guP = null;
    public static final C2491fm guQ = null;
    public static final C2491fm guR = null;
    public static final C2491fm guS = null;
    public static final C2491fm guT = null;
    public static final C2491fm guU = null;
    public static final C2491fm guV = null;
    public static final C2491fm guW = null;
    public static final C2491fm guX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3068c81b8ca08274f1b0000793f1da62", aum = 0)
    private static C3438ra<AttributeBuffType> aQT;
    @C0064Am(aul = "dee562f383964962dbd45170d6c76f67", aum = 1)
    private static int aQV;
    @C0064Am(aul = "bf40c2a8df015e5a0931ea0690b8e42f", aum = 2)
    private static float aQX;
    @C0064Am(aul = "28f9b0e36d266a9f52f3dcc80459fbd6", aum = 3)
    private static Asset aQZ;

    static {
        m25787V();
    }

    public HazardAreaType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HazardAreaType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25787V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseHazardAreaType._m_fieldCount + 4;
        _m_methodCount = BaseHazardAreaType._m_methodCount + 12;
        int i = BaseHazardAreaType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(HazardAreaType.class, "3068c81b8ca08274f1b0000793f1da62", i);
        aQU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HazardAreaType.class, "dee562f383964962dbd45170d6c76f67", i2);
        aQW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(HazardAreaType.class, "bf40c2a8df015e5a0931ea0690b8e42f", i3);
        aQY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(HazardAreaType.class, "28f9b0e36d266a9f52f3dcc80459fbd6", i4);
        aRa = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseHazardAreaType._m_fields, (Object[]) _m_fields);
        int i6 = BaseHazardAreaType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 12)];
        C2491fm a = C4105zY.m41624a(HazardAreaType.class, "7ef4fe094953a0b10b75ead85e7d6767", i6);
        guP = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(HazardAreaType.class, "03a9ff13063cecf0a9113123c9384e1b", i7);
        guQ = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(HazardAreaType.class, "09f3a9d3b17a9a3fd575b94419cb7ed4", i8);
        guR = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(HazardAreaType.class, "7b9c36ac21ce0a545b4eae77c4d86359", i9);
        guS = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(HazardAreaType.class, "95f84391434aa000078ea7b93b9bc15c", i10);
        guT = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(HazardAreaType.class, "41e9036e9f836e06d8aa0f758cd558e1", i11);
        guU = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(HazardAreaType.class, "3e7807d8c17e93527e7e461134620b4a", i12);
        awp = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(HazardAreaType.class, "a81f9fe8acdbd103c481f859a2812fd2", i13);
        awq = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(HazardAreaType.class, "5059a9c7eb82bd92714042b7f39931c7", i14);
        guV = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(HazardAreaType.class, "c89aed1af3f41f92b228ae1a6d0bf930", i15);
        guW = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(HazardAreaType.class, "30e0f87f1b38af1353a6264f19a77e07", i16);
        guX = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(HazardAreaType.class, "76eb5940b18e8be47e0b840f504f911d", i17);
        f5291dN = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseHazardAreaType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HazardAreaType.class, C1644YD.class, _m_fields, _m_methods);
    }

    /* renamed from: D */
    private void m25780D(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aQU, raVar);
    }

    /* renamed from: O */
    private void m25782O(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aRa, tCVar);
    }

    /* renamed from: UG */
    private C3438ra m25783UG() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aQU);
    }

    /* renamed from: UH */
    private int m25784UH() {
        return bFf().mo5608dq().mo3212n(aQW);
    }

    /* renamed from: UI */
    private float m25785UI() {
        return bFf().mo5608dq().mo3211m(aQY);
    }

    /* renamed from: UJ */
    private Asset m25786UJ() {
        return (Asset) bFf().mo5608dq().mo3214p(aRa);
    }

    /* renamed from: cM */
    private void m25791cM(float f) {
        bFf().mo5608dq().mo3150a(aQY, f);
    }

    /* renamed from: dJ */
    private void m25792dJ(int i) {
        bFf().mo5608dq().mo3183b(aQW, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Radius")
    /* renamed from: Lu */
    public float mo15984Lu() {
        switch (bFf().mo6893i(awp)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, awp, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, awp, new Object[0]));
                break;
        }
        return m25781Lt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1644YD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseHazardAreaType._m_methodCount) {
            case 0:
                m25793j((AttributeBuffType) args[0]);
                return null;
            case 1:
                m25794l((AttributeBuffType) args[0]);
                return null;
            case 2:
                return cud();
            case 3:
                return cuf();
            case 4:
                return new Integer(cuh());
            case 5:
                m25795uq(((Integer) args[0]).intValue());
                return null;
            case 6:
                return new Float(m25781Lt());
            case 7:
                m25789aV(((Float) args[0]).floatValue());
                return null;
            case 8:
                return cuj();
            case 9:
                m25790bE((Asset) args[0]);
                return null;
            case 10:
                return cul();
            case 11:
                return m25788aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f5291dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f5291dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5291dN, new Object[0]));
                break;
        }
        return m25788aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Radius")
    /* renamed from: aW */
    public void mo15985aW(float f) {
        switch (bFf().mo6893i(awq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awq, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awq, new Object[]{new Float(f)}));
                break;
        }
        m25789aV(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hazard Area G F X")
    /* renamed from: bF */
    public void mo15986bF(Asset tCVar) {
        switch (bFf().mo6893i(guW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, guW, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, guW, new Object[]{tCVar}));
                break;
        }
        m25790bE(tCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C3438ra<AttributeBuffType> cue() {
        switch (bFf().mo6893i(guR)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, guR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, guR, new Object[0]));
                break;
        }
        return cud();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Buff Types")
    public List<AttributeBuffType> cug() {
        switch (bFf().mo6893i(guS)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, guS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, guS, new Object[0]));
                break;
        }
        return cuf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tick In Sec")
    public int cui() {
        switch (bFf().mo6893i(guT)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, guT, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, guT, new Object[0]));
                break;
        }
        return cuh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hazard Area G F X")
    public Asset cuk() {
        switch (bFf().mo6893i(guV)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, guV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, guV, new Object[0]));
                break;
        }
        return cuj();
    }

    public HazardArea cum() {
        switch (bFf().mo6893i(guX)) {
            case 0:
                return null;
            case 2:
                return (HazardArea) bFf().mo5606d(new aCE(this, guX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, guX, new Object[0]));
                break;
        }
        return cul();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Buff Types")
    /* renamed from: k */
    public void mo15992k(AttributeBuffType api) {
        switch (bFf().mo6893i(guP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, guP, new Object[]{api}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, guP, new Object[]{api}));
                break;
        }
        m25793j(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Buff Types")
    /* renamed from: m */
    public void mo15993m(AttributeBuffType api) {
        switch (bFf().mo6893i(guQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, guQ, new Object[]{api}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, guQ, new Object[]{api}));
                break;
        }
        m25794l(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tick In Sec")
    /* renamed from: ur */
    public void mo15994ur(int i) {
        switch (bFf().mo6893i(guU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, guU, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, guU, new Object[]{new Integer(i)}));
                break;
        }
        m25795uq(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Buff Types")
    @C0064Am(aul = "7ef4fe094953a0b10b75ead85e7d6767", aum = 0)
    /* renamed from: j */
    private void m25793j(AttributeBuffType api) {
        m25783UG().add(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Buff Types")
    @C0064Am(aul = "03a9ff13063cecf0a9113123c9384e1b", aum = 0)
    /* renamed from: l */
    private void m25794l(AttributeBuffType api) {
        m25783UG().remove(api);
    }

    @C0064Am(aul = "09f3a9d3b17a9a3fd575b94419cb7ed4", aum = 0)
    private C3438ra<AttributeBuffType> cud() {
        return m25783UG();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Buff Types")
    @C0064Am(aul = "7b9c36ac21ce0a545b4eae77c4d86359", aum = 0)
    private List<AttributeBuffType> cuf() {
        return Collections.unmodifiableList(m25783UG());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tick In Sec")
    @C0064Am(aul = "95f84391434aa000078ea7b93b9bc15c", aum = 0)
    private int cuh() {
        return m25784UH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tick In Sec")
    @C0064Am(aul = "41e9036e9f836e06d8aa0f758cd558e1", aum = 0)
    /* renamed from: uq */
    private void m25795uq(int i) {
        m25792dJ(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zone Radius")
    @C0064Am(aul = "3e7807d8c17e93527e7e461134620b4a", aum = 0)
    /* renamed from: Lt */
    private float m25781Lt() {
        return m25785UI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zone Radius")
    @C0064Am(aul = "a81f9fe8acdbd103c481f859a2812fd2", aum = 0)
    /* renamed from: aV */
    private void m25789aV(float f) {
        m25791cM(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hazard Area G F X")
    @C0064Am(aul = "5059a9c7eb82bd92714042b7f39931c7", aum = 0)
    private Asset cuj() {
        return m25786UJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hazard Area G F X")
    @C0064Am(aul = "c89aed1af3f41f92b228ae1a6d0bf930", aum = 0)
    /* renamed from: bE */
    private void m25790bE(Asset tCVar) {
        m25782O(tCVar);
    }

    @C0064Am(aul = "30e0f87f1b38af1353a6264f19a77e07", aum = 0)
    private HazardArea cul() {
        return (HazardArea) mo745aU();
    }

    @C0064Am(aul = "76eb5940b18e8be47e0b840f504f911d", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m25788aT() {
        T t = (HazardArea) bFf().mo6865M(HazardArea.class);
        t.mo21195a(this);
        return t;
    }
}
