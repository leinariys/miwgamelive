package game.script.hazardarea.HazardArea;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.item.Module;
import game.script.item.buff.module.AttributeBuff;
import game.script.item.buff.module.AttributeBuffType;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.link.C6051afX;
import logic.data.mbean.C1681Yl;
import logic.data.mbean.C6689arl;
import logic.render.IEngineGraphics;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.sound.C0907NJ;
import p001a.*;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RBillboardsCloud;
import taikodom.render.scene.SceneObject;

import java.util.Collection;
import java.util.UUID;

@C5829abJ("1.0.0")
@C6485anp
@C2712iu(mo19786Bt = BaseHazardAreaType.class)
@C5511aMd
/* renamed from: a.pb */
/* compiled from: a */
public class HazardArea extends Trigger implements C0468GU, C1616Xf, C3161oY.C3162a {

    /* renamed from: KF */
    public static final C5663aRz f8830KF = null;

    /* renamed from: Mq */
    public static final C2491fm f8831Mq = null;

    /* renamed from: Wp */
    public static final C2491fm f8832Wp = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f8833x13860637 = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aQU = null;
    public static final C5663aRz aQW = null;
    public static final C5663aRz aQY = null;
    public static final C5663aRz aRa = null;
    public static final C5663aRz aRc = null;
    public static final C5663aRz aRe = null;
    public static final C5663aRz aRg = null;
    public static final C2491fm aRi = null;
    public static final C2491fm aRj = null;
    public static final C2491fm aRk = null;
    public static final C2491fm aRl = null;
    public static final C2491fm aRm = null;
    public static final C2491fm aRn = null;
    public static final C2491fm aRo = null;
    public static final C2491fm awC = null;
    public static final C2491fm awD = null;
    public static final C2491fm awx = null;
    public static final C2491fm awy = null;
    /* renamed from: bL */
    public static final C5663aRz f8835bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8836bM = null;
    /* renamed from: bN */
    public static final C2491fm f8837bN = null;
    /* renamed from: bO */
    public static final C2491fm f8838bO = null;
    /* renamed from: bP */
    public static final C2491fm f8839bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8840bQ = null;
    /* renamed from: vi */
    public static final C2491fm f8841vi = null;
    /* renamed from: vp */
    public static final C2491fm f8842vp = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "df4d4728f2767e02cb1f260dcc3e36c5", aum = 0)
    private static C3438ra<AttributeBuffType> aQT = null;
    @C0064Am(aul = "e5b2198e45ec605c23c9a4f83dc4764f", aum = 1)
    private static int aQV = 0;
    @C0064Am(aul = "a3ad329c446686366b1e8bbdb406df92", aum = 2)
    private static float aQX = 0.0f;
    @C0064Am(aul = "5c0deb1aa2adfa1149549deb09d4009b", aum = 3)
    private static Asset aQZ = null;
    @C0064Am(aul = "dbf99865d1817dfc89ec895f4d837713", aum = 5)
    private static C2686iZ<Ship> aRb = null;
    @C0064Am(aul = "6338404582f004b69a04326664e2f58c", aum = 7)
    private static HollowActor aRd = null;
    @C0064Am(aul = "66f40d5cb05b0b128a6e08a1478bd6b9", aum = 8)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C3438ra<AttributeBuff> aRf = null;
    @C0064Am(aul = "283b3ec4d1042f5edafc4253310ea623", aum = 6)
    private static boolean active = false;
    @C0064Am(aul = "c6fc975d0f6bd134f33ffbdd59b13385", aum = 9)

    /* renamed from: bK */
    private static UUID f8834bK = null;
    @C0064Am(aul = "dbab4d71a42e309f0acd2ebba877373d", aum = 4)
    private static String handle = null;

    static {
        m37203V();
    }

    /* access modifiers changed from: private */
    @ClientOnly
    public SceneObject aRh;

    public HazardArea() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HazardArea(C5540aNg ang) {
        super(ang);
    }

    public HazardArea(HazardAreaType ass) {
        super((C5540aNg) null);
        super._m_script_init(ass);
    }

    /* renamed from: V */
    static void m37203V() {
        _m_fieldCount = Trigger._m_fieldCount + 10;
        _m_methodCount = Trigger._m_methodCount + 23;
        int i = Trigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(HazardArea.class, "df4d4728f2767e02cb1f260dcc3e36c5", i);
        aQU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HazardArea.class, "e5b2198e45ec605c23c9a4f83dc4764f", i2);
        aQW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(HazardArea.class, "a3ad329c446686366b1e8bbdb406df92", i3);
        aQY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(HazardArea.class, "5c0deb1aa2adfa1149549deb09d4009b", i4);
        aRa = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(HazardArea.class, "dbab4d71a42e309f0acd2ebba877373d", i5);
        f8836bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(HazardArea.class, "dbf99865d1817dfc89ec895f4d837713", i6);
        aRc = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(HazardArea.class, "283b3ec4d1042f5edafc4253310ea623", i7);
        f8830KF = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(HazardArea.class, "6338404582f004b69a04326664e2f58c", i8);
        aRe = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(HazardArea.class, "66f40d5cb05b0b128a6e08a1478bd6b9", i9);
        aRg = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(HazardArea.class, "c6fc975d0f6bd134f33ffbdd59b13385", i10);
        f8835bL = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Trigger._m_fields, (Object[]) _m_fields);
        int i12 = Trigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 23)];
        C2491fm a = C4105zY.m41624a(HazardArea.class, "9c92ca2c9598e07e4008c4fc5f728fe0", i12);
        f8837bN = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(HazardArea.class, "75cbc12da1b25391d0fe00a220e5be61", i13);
        f8838bO = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(HazardArea.class, "34e773c9253335fb9388a73b46102c6f", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(HazardArea.class, "7ad2818c1c91dce6cfaf7eab61531210", i15);
        f8839bP = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(HazardArea.class, "b0f1ed8eb1b4449387e3ade019863a87", i16);
        f8840bQ = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(HazardArea.class, "d8a00d4613738f427487a42c3c990ae9", i17);
        aRi = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(HazardArea.class, "d4bd1efabc2a17f9be4fb77003a0c538", i18);
        f8841vi = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(HazardArea.class, "627ccd847ea8b8dea8d317356498c4ae", i19);
        f8831Mq = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(HazardArea.class, "440788886867caa68c0530ff14bc5a11", i20);
        f8842vp = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(HazardArea.class, "90c22f0622b9ed5396f8e42cc39f76fb", i21);
        f8832Wp = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(HazardArea.class, "06a89ad45f6077dc0b93353a9b35eaaf", i22);
        awC = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(HazardArea.class, "c25e74fd2c63e81b209903be5ca7474d", i23);
        awD = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(HazardArea.class, "bc1ede76aecc4265dde8e28132e5fe07", i24);
        aRj = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(HazardArea.class, "7346fd600ccf089a6426765a84f4aa64", i25);
        aRk = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(HazardArea.class, "3d17a96b6a52035a250aa039dddf538e", i26);
        _f_onResurrect_0020_0028_0029V = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(HazardArea.class, "8f20c21d4758ecf29fcf72e482fa142e", i27);
        aRl = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(HazardArea.class, "3d8967a4af11b77620893ada86b5de53", i28);
        awx = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(HazardArea.class, "13e72a2aa6e515811cf7910830471592", i29);
        awy = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(HazardArea.class, C6051afX.bgb, i30);
        aRm = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(HazardArea.class, "def6cdc742525f62fea87b7575894720", i31);
        aRn = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(HazardArea.class, "cf51a0f10fe65efbd30e56a9c1bdebc0", i32);
        aRo = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(HazardArea.class, "facd8e4fabbdbd631d36ec9e3a4c2061", i33);
        f8833x13860637 = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(HazardArea.class, "22b1694224c07935785a9dc9632752b3", i34);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Trigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HazardArea.class, C1681Yl.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "c25e74fd2c63e81b209903be5ca7474d", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: A */
    private void m37182A(Actor cr) {
        throw new aWi(new aCE(this, awD, new Object[]{cr}));
    }

    /* renamed from: D */
    private void m37183D(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: E */
    private void m37184E(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aRg, raVar);
    }

    /* renamed from: F */
    private void m37185F(boolean z) {
        bFf().mo5608dq().mo3153a(f8830KF, z);
    }

    /* renamed from: O */
    private void m37187O(Asset tCVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "bc1ede76aecc4265dde8e28132e5fe07", aum = 0)
    @C5566aOg
    /* renamed from: Q */
    private void m37188Q(Player aku) {
        throw new aWi(new aCE(this, aRj, new Object[]{aku}));
    }

    @C5566aOg
    /* renamed from: R */
    private void m37189R(Player aku) {
        switch (bFf().mo6893i(aRj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aRj, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aRj, new Object[]{aku}));
                break;
        }
        m37188Q(aku);
    }

    @C0064Am(aul = "7346fd600ccf089a6426765a84f4aa64", aum = 0)
    @C5566aOg
    /* renamed from: S */
    private void m37190S(Player aku) {
        throw new aWi(new aCE(this, aRk, new Object[]{aku}));
    }

    @C5566aOg
    /* renamed from: T */
    private void m37191T(Player aku) {
        switch (bFf().mo6893i(aRk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aRk, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aRk, new Object[]{aku}));
                break;
        }
        m37190S(aku);
    }

    /* renamed from: UG */
    private C3438ra m37192UG() {
        return ((HazardAreaType) getType()).cue();
    }

    /* renamed from: UH */
    private int m37193UH() {
        return ((HazardAreaType) getType()).cui();
    }

    /* renamed from: UI */
    private float m37194UI() {
        return ((HazardAreaType) getType()).mo15984Lu();
    }

    /* renamed from: UJ */
    private Asset m37195UJ() {
        return ((HazardAreaType) getType()).cuk();
    }

    /* renamed from: UK */
    private C2686iZ m37196UK() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(aRc);
    }

    /* renamed from: UL */
    private HollowActor m37197UL() {
        return (HollowActor) bFf().mo5608dq().mo3214p(aRe);
    }

    /* renamed from: UM */
    private C3438ra m37198UM() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aRg);
    }

    /* renamed from: a */
    private void m37206a(HollowActor aua) {
        bFf().mo5608dq().mo3197f(aRe, aua);
    }

    /* renamed from: a */
    private void m37208a(String str) {
        bFf().mo5608dq().mo3197f(f8836bM, str);
    }

    /* renamed from: a */
    private void m37209a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8835bL, uuid);
    }

    /* renamed from: an */
    private UUID m37211an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8835bL);
    }

    /* renamed from: ao */
    private String m37212ao() {
        return (String) bFf().mo5608dq().mo3214p(f8836bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Active")
    @C0064Am(aul = "13e72a2aa6e515811cf7910830471592", aum = 0)
    @C5566aOg
    /* renamed from: au */
    private void m37216au(boolean z) {
        throw new aWi(new aCE(this, awy, new Object[]{new Boolean(z)}));
    }

    /* renamed from: c */
    private void m37221c(UUID uuid) {
        switch (bFf().mo6893i(f8838bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8838bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8838bO, new Object[]{uuid}));
                break;
        }
        m37219b(uuid);
    }

    /* renamed from: cM */
    private void m37223cM(float f) {
        throw new C6039afL();
    }

    /* renamed from: dJ */
    private void m37224dJ(int i) {
        throw new C6039afL();
    }

    /* renamed from: l */
    private void m37226l(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(aRc, iZVar);
    }

    @C0064Am(aul = "22b1694224c07935785a9dc9632752b3", aum = 0)
    /* renamed from: qW */
    private Object m37227qW() {
        return mo21191UO();
    }

    /* renamed from: qv */
    private boolean m37228qv() {
        return bFf().mo5608dq().mo3201h(f8830KF);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "06a89ad45f6077dc0b93353a9b35eaaf", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: y */
    private void m37229y(Actor cr) {
        throw new aWi(new aCE(this, awC, new Object[]{cr}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: B */
    public void mo7552B(Actor cr) {
        switch (bFf().mo6893i(awD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                break;
        }
        m37182A(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Active")
    /* renamed from: LC */
    public boolean mo21190LC() {
        switch (bFf().mo6893i(awx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, awx, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, awx, new Object[0]));
                break;
        }
        return m37186LB();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    /* renamed from: UO */
    public HazardAreaType mo21191UO() {
        switch (bFf().mo6893i(aRi)) {
            case 0:
                return null;
            case 2:
                return (HazardAreaType) bFf().mo5606d(new aCE(this, aRi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aRi, new Object[0]));
                break;
        }
        return m37199UN();
    }

    /* access modifiers changed from: protected */
    /* renamed from: UQ */
    public int mo21192UQ() {
        switch (bFf().mo6893i(aRl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aRl, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aRl, new Object[0]));
                break;
        }
        return m37200UP();
    }

    /* renamed from: US */
    public Asset mo21193US() {
        switch (bFf().mo6893i(aRn)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aRn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aRn, new Object[0]));
                break;
        }
        return m37201UR();
    }

    /* renamed from: UU */
    public float mo21194UU() {
        switch (bFf().mo6893i(aRo)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aRo, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aRo, new Object[0]));
                break;
        }
        return m37202UT();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1681Yl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Trigger._m_methodCount) {
            case 0:
                return m37213ap();
            case 1:
                m37219b((UUID) args[0]);
                return null;
            case 2:
                return m37215au();
            case 3:
                return m37214ar();
            case 4:
                m37218b((String) args[0]);
                return null;
            case 5:
                return m37199UN();
            case 6:
                return m37225it();
            case 7:
                m37205a((StellarSystem) args[0]);
                return null;
            case 8:
                m37220c((Vec3d) args[0]);
                return null;
            case 9:
                m37230zw();
                return null;
            case 10:
                m37229y((Actor) args[0]);
                return null;
            case 11:
                m37182A((Actor) args[0]);
                return null;
            case 12:
                m37188Q((Player) args[0]);
                return null;
            case 13:
                m37190S((Player) args[0]);
                return null;
            case 14:
                m37210aG();
                return null;
            case 15:
                return new Integer(m37200UP());
            case 16:
                return new Boolean(m37186LB());
            case 17:
                m37216au(((Boolean) args[0]).booleanValue());
                return null;
            case 18:
                return new Boolean(m37222c((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 19:
                return m37201UR();
            case 20:
                return new Float(m37202UT());
            case 21:
                m37217b((aDJ) args[0]);
                return null;
            case 22:
                return m37227qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m37210aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8837bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8837bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8837bN, new Object[0]));
                break;
        }
        return m37213ap();
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f8831Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8831Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8831Mq, new Object[]{jj}));
                break;
        }
        m37205a(jj);
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f8833x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8833x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8833x13860637, new Object[]{adj}));
                break;
        }
        m37217b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5472aKq
    /* renamed from: d */
    public boolean mo21196d(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(aRm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}));
                break;
        }
        return m37222c(zt);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8839bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8839bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8839bP, new Object[0]));
                break;
        }
        return m37214ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8840bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8840bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8840bQ, new Object[]{str}));
                break;
        }
        m37218b(str);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m37227qW();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f8841vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8841vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8841vi, new Object[0]));
                break;
        }
        return m37225it();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Active")
    @C5566aOg
    public void setActive(boolean z) {
        switch (bFf().mo6893i(awy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awy, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awy, new Object[]{new Boolean(z)}));
                break;
        }
        m37216au(z);
    }

    public void setPosition(Vec3d ajr) {
        switch (bFf().mo6893i(f8842vp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8842vp, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8842vp, new Object[]{ajr}));
                break;
        }
        m37220c(ajr);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m37215au();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m37229y(cr);
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f8832Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8832Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8832Wp, new Object[0]));
                break;
        }
        m37230zw();
    }

    @C0064Am(aul = "9c92ca2c9598e07e4008c4fc5f728fe0", aum = 0)
    /* renamed from: ap */
    private UUID m37213ap() {
        return m37211an();
    }

    @C0064Am(aul = "75cbc12da1b25391d0fe00a220e5be61", aum = 0)
    /* renamed from: b */
    private void m37219b(UUID uuid) {
        m37209a(uuid);
    }

    /* renamed from: a */
    public void mo21195a(HazardAreaType ass) {
        super.mo967a((C2961mJ) ass);
        setRadius(m37194UI());
        setHandle(String.valueOf(mo21191UO().getHandle()) + "_" + cWm());
        m37209a(UUID.randomUUID());
    }

    @C0064Am(aul = "34e773c9253335fb9388a73b46102c6f", aum = 0)
    /* renamed from: au */
    private String m37215au() {
        return "HazardArea: [" + getName() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "7ad2818c1c91dce6cfaf7eab61531210", aum = 0)
    /* renamed from: ar */
    private String m37214ar() {
        return m37212ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "b0f1ed8eb1b4449387e3ade019863a87", aum = 0)
    /* renamed from: b */
    private void m37218b(String str) {
        m37208a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "d8a00d4613738f427487a42c3c990ae9", aum = 0)
    /* renamed from: UN */
    private HazardAreaType m37199UN() {
        return (HazardAreaType) super.getType();
    }

    @C0064Am(aul = "d4bd1efabc2a17f9be4fb77003a0c538", aum = 0)
    /* renamed from: it */
    private String m37225it() {
        return null;
    }

    @C0064Am(aul = "627ccd847ea8b8dea8d317356498c4ae", aum = 0)
    /* renamed from: a */
    private void m37205a(StellarSystem jj) {
        if (!m37228qv()) {
            m37185F(true);
            if (!bae()) {
                super.setRadius(m37194UI());
            }
            super.mo993b(jj);
            if (bHa()) {
                HazardAreaGFX aVar = (HazardAreaGFX) bFf().mo6865M(HazardAreaGFX.class);
                aVar.mo21199g(this);
                m37206a((HollowActor) aVar);
                m37197UL().mo993b(mo960Nc());
                m37197UL().setPosition(getPosition());
            }
            for (AttributeBuffType api : m37192UG()) {
                if (api.dod() != Module.C4010b.TARGET) {
                    mo6317hy("Hazard buff is not Targeting, forcing it");
                    api.mo10827c(Module.C4010b.TARGET);
                }
            }
        }
    }

    @C0064Am(aul = "440788886867caa68c0530ff14bc5a11", aum = 0)
    /* renamed from: c */
    private void m37220c(Vec3d ajr) {
        super.setPosition(ajr);
        if (m37197UL() != null) {
            m37197UL().setPosition(ajr);
        }
    }

    @C0064Am(aul = "90c22f0622b9ed5396f8e42cc39f76fb", aum = 0)
    /* renamed from: zw */
    private void m37230zw() {
        super.mo1099zx();
        m37185F(false);
        if (bHa()) {
            if (m37197UL() != null && m37197UL().bae()) {
                m37197UL().mo1099zx();
            }
            m37206a((HollowActor) null);
        }
        for (Ship h : m37196UK()) {
            h.mo8355h(C3161oY.C3162a.class, this);
        }
        m37196UK().clear();
        for (AttributeBuff Uh : m37198UM()) {
            Uh.mo3752Uh();
        }
    }

    @C0064Am(aul = "3d17a96b6a52035a250aa039dddf538e", aum = 0)
    /* renamed from: aG */
    private void m37210aG() {
        super.mo70aH();
        if (m37212ao() == null) {
            setHandle(String.valueOf(mo21191UO().getHandle()) + "_" + cWm());
        }
    }

    @C0064Am(aul = "8f20c21d4758ecf29fcf72e482fa142e", aum = 0)
    /* renamed from: UP */
    private int m37200UP() {
        return m37193UH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Active")
    @C0064Am(aul = "3d8967a4af11b77620893ada86b5de53", aum = 0)
    /* renamed from: LB */
    private boolean m37186LB() {
        return m37228qv();
    }

    @C0064Am(aul = "19d0d3979efa8f4f0ecc961fe52775c7", aum = 0)
    @C5472aKq
    /* renamed from: c */
    private boolean m37222c(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        } else if (zt.bFq() == null) {
            return false;
        } else {
            if (bGZ()) {
                if (zt.bFq().mo15388dL() == aPA()) {
                    return true;
                }
                return false;
            } else if (zt.bFq().mo15388dL() == aPC()) {
                return true;
            } else {
                return false;
            }
        }
    }

    @C0064Am(aul = "def6cdc742525f62fea87b7575894720", aum = 0)
    /* renamed from: UR */
    private Asset m37201UR() {
        return m37195UJ();
    }

    @C0064Am(aul = "cf51a0f10fe65efbd30e56a9c1bdebc0", aum = 0)
    /* renamed from: UT */
    private float m37202UT() {
        return m37194UI();
    }

    @C0064Am(aul = "facd8e4fabbdbd631d36ec9e3a4c2061", aum = 0)
    /* renamed from: b */
    private void m37217b(aDJ adj) {
        mo7552B((Actor) adj);
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.pb$a */
    public class HazardAreaGFX extends HollowActor implements C1616Xf {
        public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8843aT = null;
        public static final C2491fm gDV = null;
        public static final C2491fm gDW = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "cd24b973277230068f092e7ed61fa318", aum = 0)
        static /* synthetic */ HazardArea grG;

        static {
            m37253V();
        }

        public HazardAreaGFX() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public HazardAreaGFX(C5540aNg ang) {
            super(ang);
        }

        public HazardAreaGFX(HazardArea pbVar) {
            super((C5540aNg) null);
            super._m_script_init(pbVar);
        }

        /* renamed from: V */
        static void m37253V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HollowActor._m_fieldCount + 1;
            _m_methodCount = HollowActor._m_methodCount + 3;
            int i = HollowActor._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(HazardAreaGFX.class, "cd24b973277230068f092e7ed61fa318", i);
            f8843aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HollowActor._m_fields, (Object[]) _m_fields);
            int i3 = HollowActor._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
            C2491fm a = C4105zY.m41624a(HazardAreaGFX.class, "8afaafeda428045404b199480ebe3c96", i3);
            _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(HazardAreaGFX.class, "d426e3bb8322b8891789c8cab59280ee", i4);
            gDV = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(HazardAreaGFX.class, "89ff2a2324a0cd85ee60ed937959f0e0", i5);
            gDW = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HollowActor._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(HazardAreaGFX.class, C6689arl.class, _m_fields, _m_methods);
        }

        /* access modifiers changed from: private */
        public HazardArea cDF() {
            return (HazardArea) bFf().mo5608dq().mo3214p(f8843aT);
        }

        /* renamed from: f */
        private void m37256f(HazardArea pbVar) {
            bFf().mo5608dq().mo3197f(f8843aT, pbVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6689arl(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - HollowActor._m_methodCount) {
                case 0:
                    return m37255au();
                case 1:
                    cxH();
                    return null;
                case 2:
                    cxJ();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        public void cxI() {
            switch (bFf().mo6893i(gDV)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gDV, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gDV, new Object[0]));
                    break;
            }
            cxH();
        }

        /* access modifiers changed from: protected */
        public void cxK() {
            switch (bFf().mo6893i(gDW)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gDW, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gDW, new Object[0]));
                    break;
            }
            cxJ();
        }

        public String toString() {
            switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                    break;
            }
            return m37255au();
        }

        /* renamed from: g */
        public void mo21199g(HazardArea pbVar) {
            m37256f(pbVar);
            super.mo10S();
        }

        @C0064Am(aul = "8afaafeda428045404b199480ebe3c96", aum = 0)
        /* renamed from: au */
        private String m37255au() {
            return "HazardAreaGFX from [" + cDF().getHandle() + "]";
        }

        @C0064Am(aul = "d426e3bb8322b8891789c8cab59280ee", aum = 0)
        private void cxH() {
            if (cDF().mo21193US() != null) {
                C5916acs.getSingolton().getLoaderTrail().mo4995a(cDF().mo21193US().getFile(), cDF().mo21193US().getHandle(), C5916acs.getSingolton().getEngineGraphics().adZ(), new C3247a(), "HazardArea: spawn GFX");
            }
        }

        @C0064Am(aul = "89ff2a2324a0cd85ee60ed937959f0e0", aum = 0)
        private void cxJ() {
            IEngineGraphics ale = C5916acs.getSingolton().getEngineGraphics();
            if (ale != null && cDF().aRh != null) {
                ale.adZ().removeChild(cDF().aRh);
            }
        }

        /* renamed from: a.pb$a$a */
        class C3247a implements C0907NJ {
            C3247a() {
            }

            /* renamed from: a */
            public void mo968a(RenderAsset renderAsset) {
                if (renderAsset instanceof SceneObject) {
                    HazardAreaGFX.this.cDF().aRh = (SceneObject) renderAsset;
                    HazardAreaGFX.this.cDF().aRh.setPosition(HazardAreaGFX.this.getPosition());
                    if (renderAsset instanceof RBillboardsCloud) {
                        ((RBillboardsCloud) renderAsset).setRadius(HazardAreaGFX.this.cDF().mo21194UU());
                    }
                }
            }
        }
    }
}
