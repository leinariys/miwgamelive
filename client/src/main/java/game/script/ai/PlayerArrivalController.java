package game.script.ai;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.C4135zz;
import game.network.message.externalizable.C5667aSd;
import game.network.message.externalizable.ObjectId;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.item.Shot;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.space.Node;
import logic.aaa.C1506WA;
import logic.aaa.aIP;
import logic.abb.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1390UN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import javax.vecmath.Vector3f;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C5256aCi
@C6485anp
/* renamed from: a.ON */
/* compiled from: a */
public class PlayerArrivalController extends Controller implements C1616Xf, C2540gZ {
    /* renamed from: _f_applyUsingDesiredVelocity_0020_0028FLtaikodom_002fgame_002fscript_002fPawn_003bLcom_002fhoplon_002fgeometry_002fVec3f_003bFFIF_0029V */
    public static final C2491fm f1294x5a73435d = null;
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f1295xdce2e729 = null;
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f1296xebad5167 = null;
    /* renamed from: _f_convertForceToDesiredVelocity_0020_0028FLtaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003b_0029Lcom_002fhoplon_002fgeometry_002fVec3f_003b */
    public static final C2491fm f1297x1ef544f6 = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_fillShipCommand_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bF_0029Z */
    public static final C2491fm f1298xbc82b75d = null;
    public static final C2491fm _f_forgetNearbyObjects_0020_0028_0029V = null;
    public static final C2491fm _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f1299x851d656b = null;
    /* renamed from: _f_isEnemy_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f1300x72b3cf67 = null;
    /* renamed from: _f_isInvalid_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003b_0029Z */
    public static final C2491fm f1301x30bf686c = null;
    public static final C2491fm _f_isRunning_0020_0028_0029Z = null;
    /* renamed from: _f_noCollision_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f1302xe5145ada = null;
    public static final C2491fm _f_onEnterCruiseSpeed_0020_0028_0029V = null;
    public static final C2491fm _f_onExitCruiseSpeed_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm azU = null;
    public static final C5663aRz boP = null;
    public static final C2491fm bqg = null;
    public static final C2491fm eZx = null;
    public static final C2491fm gAD = null;
    public static final C2491fm gAE = null;
    public static final C2491fm gAF = null;
    public static final C2491fm gAG = null;
    public static final C2491fm gAH = null;
    public static final C5663aRz gAx = null;
    public static final C5663aRz gAy = null;
    public static final C5663aRz gAz = null;
    public static final C5663aRz gOs = null;
    public static final C2491fm gOw = null;
    public static final C2491fm gOx = null;
    public static final C2491fm gOy = null;
    public static final long serialVersionUID = 0;
    private static final int FIRST_STEP_STARTING_CRUISE_SPEED = 2;
    private static final int IN_CRUISE_SPEED = 1;
    private static final int IN_NORMAL_SPEED = 0;
    private static final int STARTING_CRUISE_SPEED = 3;
    private static final float gAB = 1000000.0f;
    private static final float gOu = 100.0f;
    private static final float gOv = 1000000.0f;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "f9dc8ba1a884fdc8fb0d18f2fe06badf", aum = 3)
    @C5256aCi
    private static Actor ams = null;
    @C0064Am(aul = "791645b9ced7992708a78e72827bfb75", aum = 0)
    @C5256aCi
    private static C0573Hw[] aue = null;
    @C0064Am(aul = "41be9403b66fcb690c0fb7e6d9a50f8a", aum = 2)
    @C5256aCi
    private static C1218Ry auf = null;
    @C0064Am(aul = "a2963aaf58dbe5f1bb72469455d8b92f", aum = 1)
    @C5256aCi
    private static C6822auO avoidObstaclesBehaviour = null;
    @C0064Am(aul = "2ba47ce0762117d39ac9eed5aaec70d8", aum = 4)
    @C5256aCi
    private static boolean equ = false;

    static {
        m7979V();
    }

    @ClientOnly
    private boolean gOt;
    @C5256aCi
    private transient List<Actor> nearbyObjects;

    public PlayerArrivalController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerArrivalController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7979V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Controller._m_fieldCount + 5;
        _m_methodCount = Controller._m_methodCount + 28;
        int i = Controller._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(PlayerArrivalController.class, "791645b9ced7992708a78e72827bfb75", i);
        gAx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerArrivalController.class, "a2963aaf58dbe5f1bb72469455d8b92f", i2);
        gAy = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerArrivalController.class, "41be9403b66fcb690c0fb7e6d9a50f8a", i3);
        gAz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PlayerArrivalController.class, "f9dc8ba1a884fdc8fb0d18f2fe06badf", i4);
        boP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PlayerArrivalController.class, "2ba47ce0762117d39ac9eed5aaec70d8", i5);
        gOs = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Controller._m_fields, (Object[]) _m_fields);
        int i7 = Controller._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 28)];
        C2491fm a = C4105zY.m41624a(PlayerArrivalController.class, "8b31f8bdb32aacd553361c5d923e81a8", i7);
        gOw = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerArrivalController.class, "c51c05d0bf678221d6aabdb2bec5d1d5", i8);
        bqg = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerArrivalController.class, "af01d394240406d3f2dba4698f89b5ba", i9);
        gOx = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerArrivalController.class, "2fa57a7cd7700884dba748d0d16408d6", i10);
        azU = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerArrivalController.class, "364ff6ce92544add7c5f56575048a695", i11);
        eZx = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerArrivalController.class, "dfbcf708317ed43456dd05f9cedd5f17", i12);
        _f_dispose_0020_0028_0029V = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerArrivalController.class, "b96fc2e7dc7509bcbdd4ce07fea31188", i13);
        _f_stop_0020_0028_0029V = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerArrivalController.class, "3c92f0104c1f4d5c02dac79c8a8ed98c", i14);
        _f_step_0020_0028F_0029V = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerArrivalController.class, "c2f8497cd8639fb0ce5628fe8b16df54", i15);
        f1295xdce2e729 = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerArrivalController.class, "038e5d8ead60d128ca9ca5bc6854e5aa", i16);
        f1296xebad5167 = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerArrivalController.class, "6e5ceee69e4938c92e09eb8ea42feb96", i17);
        f1301x30bf686c = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerArrivalController.class, "bd3af35193a206bffb1d61bb1d3dd9b7", i18);
        f1298xbc82b75d = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerArrivalController.class, "d0cdcf753d01f37957230b06abd041b0", i19);
        _f_forgetNearbyObjects_0020_0028_0029V = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerArrivalController.class, "80795dfdba6c986823ee8b40486d7a78", i20);
        _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(PlayerArrivalController.class, "fe10f1367de39164d6fc990d05b3547c", i21);
        f1302xe5145ada = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(PlayerArrivalController.class, "aeaccdf688ae76e746170d153932d1bd", i22);
        f1294x5a73435d = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        C2491fm a17 = C4105zY.m41624a(PlayerArrivalController.class, "1467c8360d98152b9e98ebc1c7d5a18f", i23);
        gAD = a17;
        fmVarArr[i23] = a17;
        int i24 = i23 + 1;
        C2491fm a18 = C4105zY.m41624a(PlayerArrivalController.class, "e9c772473237e25dce90c921abe7abc5", i24);
        gOy = a18;
        fmVarArr[i24] = a18;
        int i25 = i24 + 1;
        C2491fm a19 = C4105zY.m41624a(PlayerArrivalController.class, "81e7a4026a3676d250cbdaec7268cced", i25);
        f1299x851d656b = a19;
        fmVarArr[i25] = a19;
        int i26 = i25 + 1;
        C2491fm a20 = C4105zY.m41624a(PlayerArrivalController.class, "0041ef41660c4f02a8de831c6b873c18", i26);
        f1297x1ef544f6 = a20;
        fmVarArr[i26] = a20;
        int i27 = i26 + 1;
        C2491fm a21 = C4105zY.m41624a(PlayerArrivalController.class, "86abf9f7f691d2238e071c14e95e7934", i27);
        _f_isRunning_0020_0028_0029Z = a21;
        fmVarArr[i27] = a21;
        int i28 = i27 + 1;
        C2491fm a22 = C4105zY.m41624a(PlayerArrivalController.class, "553eeb940fa506d696beadacdfcea8ac", i28);
        gAE = a22;
        fmVarArr[i28] = a22;
        int i29 = i28 + 1;
        C2491fm a23 = C4105zY.m41624a(PlayerArrivalController.class, "12a038bcfa3e970cfe07604049ea2374", i29);
        gAF = a23;
        fmVarArr[i29] = a23;
        int i30 = i29 + 1;
        C2491fm a24 = C4105zY.m41624a(PlayerArrivalController.class, "e47faf3159958ff177ad3be6b4f9f38e", i30);
        _f_onEnterCruiseSpeed_0020_0028_0029V = a24;
        fmVarArr[i30] = a24;
        int i31 = i30 + 1;
        C2491fm a25 = C4105zY.m41624a(PlayerArrivalController.class, "ea60c4e88c1b3b34f2872c014fc7ed7c", i31);
        _f_onExitCruiseSpeed_0020_0028_0029V = a25;
        fmVarArr[i31] = a25;
        int i32 = i31 + 1;
        C2491fm a26 = C4105zY.m41624a(PlayerArrivalController.class, "e1ecf185341e0c958a284c62a19279d3", i32);
        gAG = a26;
        fmVarArr[i32] = a26;
        int i33 = i32 + 1;
        C2491fm a27 = C4105zY.m41624a(PlayerArrivalController.class, "c59ecec5f97e2ccd2e784d6e5c9b04e4", i33);
        gAH = a27;
        fmVarArr[i33] = a27;
        int i34 = i33 + 1;
        C2491fm a28 = C4105zY.m41624a(PlayerArrivalController.class, "8105ac4e9d1bb566b1e88e4e2d33d5c1", i34);
        f1300x72b3cf67 = a28;
        fmVarArr[i34] = a28;
        int i35 = i34 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Controller._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerArrivalController.class, C1390UN.class, _m_fields, _m_methods);
    }

    /* renamed from: Mj */
    private void m7972Mj() {
        switch (bFf().mo6893i(azU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                break;
        }
        m7971Mi();
    }

    /* renamed from: Ml */
    private void m7974Ml() {
        switch (bFf().mo6893i(_f_forgetNearbyObjects_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                break;
        }
        m7973Mk();
    }

    /* renamed from: R */
    private void m7978R(Actor cr) {
        bFf().mo5608dq().mo3197f(boP, cr);
    }

    /* renamed from: a */
    private void m7984a(Actor cr) {
        switch (bFf().mo6893i(eZx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eZx, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eZx, new Object[]{cr}));
                break;
        }
        m7989aM(cr);
    }

    /* renamed from: a */
    private void m7985a(C6822auO auo) {
        bFf().mo5608dq().mo3197f(gAy, auo);
    }

    /* renamed from: a */
    private boolean m7986a(C1216Rw rw, float f) {
        switch (bFf().mo6893i(f1298xbc82b75d)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1298xbc82b75d, new Object[]{rw, new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1298xbc82b75d, new Object[]{rw, new Float(f)}));
                break;
        }
        return m7999b(rw, f);
    }

    /* access modifiers changed from: private */
    public Actor afq() {
        return (Actor) bFf().mo5608dq().mo3214p(boP);
    }

    /* renamed from: ax */
    private boolean m7991ax(Actor cr) {
        switch (bFf().mo6893i(f1302xe5145ada)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1302xe5145ada, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1302xe5145ada, new Object[]{cr}));
                break;
        }
        return m7990aw(cr);
    }

    /* renamed from: b */
    private int m7993b(C1216Rw rw) {
        switch (bFf().mo6893i(gAD)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gAD, new Object[]{rw}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAD, new Object[]{rw}));
                break;
        }
        return m7980a(rw);
    }

    /* renamed from: b */
    private Vec3f m7994b(float f, C1216Rw rw) {
        switch (bFf().mo6893i(f1297x1ef544f6)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, f1297x1ef544f6, new Object[]{new Float(f), rw}));
            case 3:
                bFf().mo5606d(new aCE(this, f1297x1ef544f6, new Object[]{new Float(f), rw}));
                break;
        }
        return m7982a(f, rw);
    }

    /* renamed from: b */
    private void m7995b(float f, Pawn avi, Vec3f vec3f, float f2, float f3, int i, float f4) {
        switch (bFf().mo6893i(f1294x5a73435d)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1294x5a73435d, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Integer(i), new Float(f4)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1294x5a73435d, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Integer(i), new Float(f4)}));
                break;
        }
        m7983a(f, avi, vec3f, f2, f3, i, f4);
    }

    /* renamed from: b */
    private void m7997b(C1218Ry ry) {
        bFf().mo5608dq().mo3197f(gAz, ry);
    }

    /* renamed from: b */
    private boolean m8000b(Vec3d ajr, String str) {
        switch (bFf().mo6893i(f1295xdce2e729)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1295xdce2e729, new Object[]{ajr, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1295xdce2e729, new Object[]{ajr, str}));
                break;
        }
        return m7987a(ajr, str);
    }

    /* renamed from: b */
    private boolean m8001b(Vec3f vec3f, String str) {
        switch (bFf().mo6893i(f1296xebad5167)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1296xebad5167, new Object[]{vec3f, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1296xebad5167, new Object[]{vec3f, str}));
                break;
        }
        return m7988a(vec3f, str);
    }

    @ClientOnly
    private void cBB() {
        switch (bFf().mo6893i(gOy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gOy, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gOy, new Object[0]));
                break;
        }
        cBA();
    }

    /* access modifiers changed from: private */
    public boolean cBx() {
        return bFf().mo5608dq().mo3201h(gOs);
    }

    private C0573Hw[] cwb() {
        return (C0573Hw[]) bFf().mo5608dq().mo3214p(gAx);
    }

    private C6822auO cwc() {
        return (C6822auO) bFf().mo5608dq().mo3214p(gAy);
    }

    private C1218Ry cwd() {
        return (C1218Ry) bFf().mo5608dq().mo3214p(gAz);
    }

    private float cwi() {
        switch (bFf().mo6893i(gAF)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gAF, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAF, new Object[0]));
                break;
        }
        return cwh();
    }

    /* renamed from: e */
    private void m8003e(C0573Hw[] hwArr) {
        bFf().mo5608dq().mo3197f(gAx, hwArr);
    }

    /* renamed from: hc */
    private void m8005hc(boolean z) {
        bFf().mo5608dq().mo3153a(gOs, z);
    }

    /* renamed from: s */
    private boolean m8008s(Vec3f vec3f) {
        switch (bFf().mo6893i(f1301x30bf686c)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1301x30bf686c, new Object[]{vec3f}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1301x30bf686c, new Object[]{vec3f}));
                break;
        }
        return m8007r(vec3f);
    }

    /* renamed from: D */
    public boolean mo3298D(Actor cr) {
        switch (bFf().mo6893i(f1300x72b3cf67)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1300x72b3cf67, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1300x72b3cf67, new Object[]{cr}));
                break;
        }
        return m7970C(cr);
    }

    /* renamed from: Mn */
    public List<Actor> mo3301Mn() {
        switch (bFf().mo6893i(_f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
                break;
        }
        return m7975Mm();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1390UN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Controller._m_methodCount) {
            case 0:
                return new Boolean(cBy());
            case 1:
                return agA();
            case 2:
                m7996b((Actor) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 3:
                m7971Mi();
                return null;
            case 4:
                m7989aM((Actor) args[0]);
                return null;
            case 5:
                m8004fg();
                return null;
            case 6:
                m8006qO();
                return null;
            case 7:
                m7992ay(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Boolean(m7987a((Vec3d) args[0], (String) args[1]));
            case 9:
                return new Boolean(m7988a((Vec3f) args[0], (String) args[1]));
            case 10:
                return new Boolean(m8007r((Vec3f) args[0]));
            case 11:
                return new Boolean(m7999b((C1216Rw) args[0], ((Float) args[1]).floatValue()));
            case 12:
                m7973Mk();
                return null;
            case 13:
                return m7975Mm();
            case 14:
                return new Boolean(m7990aw((Actor) args[0]));
            case 15:
                m7983a(((Float) args[0]).floatValue(), (Pawn) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), ((Float) args[4]).floatValue(), ((Integer) args[5]).intValue(), ((Float) args[6]).floatValue());
                return null;
            case 16:
                return new Integer(m7980a((C1216Rw) args[0]));
            case 17:
                cBA();
                return null;
            case 18:
                return m7976Mo();
            case 19:
                return m7982a(((Float) args[0]).floatValue(), (C1216Rw) args[1]);
            case 20:
                return new Boolean(m7977Mp());
            case 21:
                return new Boolean(cwf());
            case 22:
                return new Float(cwh());
            case 23:
                aYj();
                return null;
            case 24:
                aYk();
                return null;
            case 25:
                m8002e((Actor) args[0], (Vec3f) args[1]);
                return null;
            case 26:
                cwj();
                return null;
            case 27:
                return new Boolean(m7970C((Actor) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Actor agB() {
        switch (bFf().mo6893i(bqg)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, bqg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqg, new Object[0]));
                break;
        }
        return agA();
    }

    /* renamed from: al */
    public Ship mo4399al() {
        switch (bFf().mo6893i(f1299x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f1299x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1299x851d656b, new Object[0]));
                break;
        }
        return m7976Mo();
    }

    /* renamed from: c */
    public void mo4400c(Actor cr, boolean z) {
        switch (bFf().mo6893i(gOx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gOx, new Object[]{cr, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gOx, new Object[]{cr, new Boolean(z)}));
                break;
        }
        m7996b(cr, z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public boolean cBz() {
        switch (bFf().mo6893i(gOw)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gOw, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gOw, new Object[0]));
                break;
        }
        return cBy();
    }

    public boolean cwg() {
        switch (bFf().mo6893i(gAE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gAE, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAE, new Object[0]));
                break;
        }
        return cwf();
    }

    public void cwk() {
        switch (bFf().mo6893i(gAH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAH, new Object[0]));
                break;
        }
        cwj();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m8004fg();
    }

    /* renamed from: f */
    public void mo4404f(Actor cr, Vec3f vec3f) {
        switch (bFf().mo6893i(gAG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAG, new Object[]{cr, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAG, new Object[]{cr, vec3f}));
                break;
        }
        m8002e(cr, vec3f);
    }

    public boolean isRunning() {
        switch (bFf().mo6893i(_f_isRunning_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m7977Mp();
    }

    @C1253SX
    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m7992ay(f);
    }

    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m8006qO();
    }

    /* renamed from: xO */
    public void mo3296xO() {
        switch (bFf().mo6893i(_f_onEnterCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYj();
    }

    /* renamed from: xP */
    public void mo3297xP() {
        switch (bFf().mo6893i(_f_onExitCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYk();
    }

    @C0064Am(aul = "8b31f8bdb32aacd553361c5d923e81a8", aum = 0)
    private boolean cBy() {
        return cBx() && (afq() instanceof C0286Dh);
    }

    @C0064Am(aul = "c51c05d0bf678221d6aabdb2bec5d1d5", aum = 0)
    private Actor agA() {
        return afq();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        this.gOt = false;
    }

    @C0064Am(aul = "af01d394240406d3f2dba4698f89b5ba", aum = 0)
    /* renamed from: b */
    private void m7996b(Actor cr, boolean z) {
        stop();
        m8005hc(z);
        m7978R(cr);
        m7972Mj();
        m7984a(cr);
    }

    @C0064Am(aul = "2fa57a7cd7700884dba748d0d16408d6", aum = 0)
    /* renamed from: Mi */
    private void m7971Mi() {
        m7985a(new C6822auO());
        cwc().mo16334a(afq());
        m8003e(new C0573Hw[2]);
        C1829aV aVVar = new C1829aV();
        cwb()[0] = aVVar;
        aVVar.mo11762a(afq());
        if (afq() instanceof Ship) {
            m7997b((C1218Ry) new C5697aTh((Ship) afq(), false));
        } else {
            m7997b((C1218Ry) new C0977a(afq().getPosition()));
            ((C3284pw) cwd()).mo21251cO((float) Math.sqrt((double) cwi()));
        }
        cwb()[1] = new C0870Mc(cwc(), cwd());
        for (C0573Hw a : cwb()) {
            a.mo2697a(this);
        }
    }

    @C0064Am(aul = "364ff6ce92544add7c5f56575048a695", aum = 0)
    /* renamed from: aM */
    private void m7989aM(Actor cr) {
        ((C1829aV) cwb()[0]).mo11762a(cr);
        cwc().mo16334a(cr);
    }

    @C0064Am(aul = "dfbcf708317ed43456dd05f9cedd5f17", aum = 0)
    /* renamed from: fg */
    private void m8004fg() {
        stop();
        super.dispose();
    }

    @C0064Am(aul = "b96fc2e7dc7509bcbdd4ce07fea31188", aum = 0)
    /* renamed from: qO */
    private void m8006qO() {
        m7978R((Actor) null);
        m8003e((C0573Hw[]) null);
        if (cwc() != null) {
            cwc().mo16334a((Actor) null);
        }
        m7985a((C6822auO) null);
    }

    @C0064Am(aul = "3c92f0104c1f4d5c02dac79c8a8ed98c", aum = 0)
    @C1253SX
    /* renamed from: ay */
    private void m7992ay(float f) {
        float f2;
        cwk();
        if (!C5877acF.fdP) {
            if (!isRunning()) {
                stop();
                return;
            }
            Pawn aYa = aYa();
            if (m8001b(aYa.mo1090qZ(), "pawn local linear velocity") && m8001b(aYa.aSf(), "pawn local angular velocity") && m8000b(aYa.getPosition(), "pawn relative position")) {
                C1216Rw rw = new C1216Rw();
                if (m7986a(rw, f)) {
                    mo4404f(aYa, rw.brw());
                    int i = 0;
                    if (aYa instanceof Ship) {
                        i = m7993b(rw);
                    }
                    if (!(rw.brw().x == 0.0f && rw.brw().y == 0.0f && rw.brw().z == 0.0f) && m8001b(rw.brw(), "Force")) {
                        Vec3f b = m7994b(f, rw);
                        C6294akG.m23128a("desired-speed", (Actor) aYa(), b);
                        if (i == 0) {
                            float length = b.length();
                            float ra = aYa.mo1091ra();
                            if (length <= ra) {
                                if (((double) length) < 1.01d) {
                                    ra = 0.0f;
                                } else {
                                    ra = length;
                                }
                            }
                            f2 = !isRunning() ? 0.0f : ra;
                        } else {
                            f2 = 0.0f;
                        }
                        m7995b(f, aYa, b, f2, rw.getRoll(), i, rw.brA());
                    }
                }
            }
        }
    }

    @C0064Am(aul = "c2f8497cd8639fb0ce5628fe8b16df54", aum = 0)
    /* renamed from: a */
    private boolean m7987a(Vec3d ajr, String str) {
        float f = (float) (ajr.x + ajr.y + ajr.z);
        if (!Float.isNaN(f) && !Float.isInfinite(f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        mo8358lY("PlayerArrivalController: " + str + " is NaN or Infinite: " + ajr + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        return false;
    }

    @C0064Am(aul = "038e5d8ead60d128ca9ca5bc6854e5aa", aum = 0)
    /* renamed from: a */
    private boolean m7988a(Vec3f vec3f, String str) {
        if (!m8008s(vec3f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        mo8358lY("PlayerArrivalController: " + str + " is NaN or Infinite: " + vec3f + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        return false;
    }

    @C0064Am(aul = "6e5ceee69e4938c92e09eb8ea42feb96", aum = 0)
    /* renamed from: r */
    private boolean m8007r(Vec3f vec3f) {
        return Float.isNaN(vec3f.x) || Float.isInfinite(vec3f.x) || Float.isNaN(vec3f.y) || Float.isInfinite(vec3f.y) || Float.isNaN(vec3f.z) || Float.isInfinite(vec3f.z);
    }

    @C0064Am(aul = "bd3af35193a206bffb1d61bb1d3dd9b7", aum = 0)
    /* renamed from: b */
    private boolean m7999b(C1216Rw rw, float f) {
        m7974Ml();
        rw.clear();
        if (cwb() == null) {
            return false;
        }
        for (C0573Hw a : cwb()) {
            if (a.mo31a(rw, f)) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "d0cdcf753d01f37957230b06abd041b0", aum = 0)
    /* renamed from: Mk */
    private void m7973Mk() {
        this.nearbyObjects = null;
    }

    @C0064Am(aul = "80795dfdba6c986823ee8b40486d7a78", aum = 0)
    /* renamed from: Mm */
    private List<Actor> m7975Mm() {
        if (this.nearbyObjects == null) {
            this.nearbyObjects = aYa().mo965Zc().mo1900c(aYa().getPosition(), 4000.0f);
            Iterator<Actor> it = this.nearbyObjects.iterator();
            while (it.hasNext()) {
                Actor next = it.next();
                if (next == aYa() || m7991ax(next) || (next instanceof Shot)) {
                    it.remove();
                }
            }
        }
        return this.nearbyObjects;
    }

    @C0064Am(aul = "fe10f1367de39164d6fc990d05b3547c", aum = 0)
    /* renamed from: aw */
    private boolean m7990aw(Actor cr) {
        if (cr.aiD() == null || !aYa().mo965Zc().cKn().bvg().mo1932a(aYa().aiD(), cr.aiD()) || aYa().mo965Zc().cKn().mo5736c(aYa().aiD(), cr.aiD()) == 0) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "aeaccdf688ae76e746170d153932d1bd", aum = 0)
    /* renamed from: a */
    private void m7983a(float f, Pawn avi, Vec3f vec3f, float f2, float f3, int i, float f4) {
        float f5;
        float f6;
        float VH = avi.mo963VH();
        float e = C3241pX.m37169e(-vec3f.z, vec3f.x);
        float e2 = C3241pX.m37169e(-vec3f.z, vec3f.y);
        if (Math.abs(e2) <= VH && Math.abs(e) <= VH) {
            f6 = e2;
            f5 = e;
        } else if (Math.abs(e2) > Math.abs(e)) {
            f6 = Math.min(Math.abs(e2), VH) * Math.signum(e2);
            f5 = (e / e2) * f6;
        } else {
            f5 = Math.signum(e) * Math.min(Math.abs(e), VH);
            f6 = f5 * (e2 / e);
        }
        if (i == 0) {
            if (Math.abs(e2) > 45.0f || Math.abs(e) > 45.0f) {
                if (f4 > 0.75f) {
                    f2 = 0.0f;
                } else if (f4 < 0.25f) {
                    f2 = Math.max(f2, (aYa().mo1091ra() / 10.0f) + 1.0f);
                }
            }
            avi.mo964W(f2);
            ((PlayerController) avi.mo2998hb()).mo22152lJ(f2);
        }
        avi.mo1063f(f3, f5, f6);
    }

    @C0064Am(aul = "1467c8360d98152b9e98ebc1c7d5a18f", aum = 0)
    /* renamed from: a */
    private int m7980a(C1216Rw rw) {
        if (!rw.bry() || cwg()) {
            if (!mo4399al().agZ() && !mo4399al().ahh()) {
                return 0;
            }
            if (bGX()) {
                cBB();
            }
            mo4399al().mo7599de(cVr());
            if (!mo4399al().agZ()) {
                return 3;
            }
            return 1;
        } else if (mo4399al().ahf()) {
            if (bGX()) {
                cBB();
            }
            mo4399al().mo7599de(cVr());
            return 2;
        } else if (mo4399al().ahh()) {
            return 3;
        } else {
            return 1;
        }
    }

    @C0064Am(aul = "e9c772473237e25dce90c921abe7abc5", aum = 0)
    @ClientOnly
    private void cBA() {
        Ship al = mo4399al();
        if (!this.gOt && al.afL().acZ().equals(C6809auB.C1996a.DEACTIVE)) {
            ala().ald().aVU().mo13975h(new aIP(C6809auB.C1996a.WARMUP));
            this.gOt = true;
        } else if (this.gOt && al.afL().acZ().equals(C6809auB.C1996a.ACTIVE)) {
            ala().ald().aVU().mo13975h(new aIP(C6809auB.C1996a.COOLDOWN));
            this.gOt = false;
        }
    }

    @C0064Am(aul = "81e7a4026a3676d250cbdaec7268cced", aum = 0)
    /* renamed from: Mo */
    private Ship m7976Mo() {
        return (Ship) aYa();
    }

    @C0064Am(aul = "0041ef41660c4f02a8de831c6b873c18", aum = 0)
    /* renamed from: a */
    private Vec3f m7982a(float f, C1216Rw rw) {
        Vec3f vec3f = new Vec3f((Vector3f) rw.brw());
        vec3f.scale(f);
        Vec3f qZ = aYa().mo1090qZ();
        vec3f.x += qZ.x;
        vec3f.y += qZ.y;
        vec3f.z = qZ.z + vec3f.z;
        return vec3f;
    }

    @C0064Am(aul = "86abf9f7f691d2238e071c14e95e7934", aum = 0)
    /* renamed from: Mp */
    private boolean m7977Mp() {
        if (aYa() == null || aYa().isDead() || aYa().isDisposed() || !aYa().bae() || aYa().getPosition() == null || afq() == null || afq().aiD() == null || afq().aiD().getPosition() == null) {
            if (!(cwd() instanceof C5697aTh)) {
                return false;
            }
            aPC().mo14419f((C1506WA) new C4135zz(C4135zz.C4136a.LOST_TARGET, true));
            return false;
        } else if (!cwg()) {
            return true;
        } else {
            return false;
        }
    }

    @C0064Am(aul = "553eeb940fa506d696beadacdfcea8ac", aum = 0)
    private boolean cwf() {
        boolean z = false;
        if (cwd() instanceof C5697aTh) {
            Ship bUT = ((C5697aTh) cwd()).bUT();
            if (bUT != null && bUT.mo1011bv((Actor) aYa()) <= 1000000.0f && bUT.bae() && !bUT.isDead() && bUT.cLZ() && !bUT.isDisposed()) {
                return false;
            }
            aPC().mo14419f((C1506WA) new C4135zz(C4135zz.C4136a.LOST_TARGET, true));
            return true;
        }
        if (aYa().mo1013bx(afq()) < cwi()) {
            z = true;
        }
        if (z && (mo4399al().agj() instanceof Player) && ((Player) mo4399al().agj()) == aPC() && !cBx()) {
            aPC().mo14419f((C1506WA) new C5667aSd("SHUTDOWN", true));
        }
        return z;
    }

    @C0064Am(aul = "12a038bcfa3e970cfe07604049ea2374", aum = 0)
    private float cwh() {
        if (afq() instanceof C0286Dh) {
            return PlayerController.m39266c((C0286Dh) afq()) * 0.9f;
        }
        if (afq() instanceof Trigger) {
            return gOu;
        }
        if (cwc() == null || cwc().cyj() == null || cwc().cyj().aiD() == null) {
            return 1000000.0f;
        }
        float outerSphereRadius = afq().aiD().mo2437IL().getOuterSphereRadius() + aYa().aiD().mo2437IL().getOuterSphereRadius();
        return outerSphereRadius * outerSphereRadius;
    }

    @C0064Am(aul = "e47faf3159958ff177ad3be6b4f9f38e", aum = 0)
    private void aYj() {
        super.mo3296xO();
        if (isRunning()) {
            aYa().mo964W(aYa().mo1091ra());
        }
    }

    @C0064Am(aul = "ea60c4e88c1b3b34f2872c014fc7ed7c", aum = 0)
    private void aYk() {
        super.mo3297xP();
        if (isRunning()) {
            aYa().mo964W(aYa().mo1091ra());
        }
    }

    @C0064Am(aul = "e1ecf185341e0c958a284c62a19279d3", aum = 0)
    /* renamed from: e */
    private void m8002e(Actor cr, Vec3f vec3f) {
    }

    @C0064Am(aul = "c59ecec5f97e2ccd2e784d6e5c9b04e4", aum = 0)
    private void cwj() {
    }

    @C0064Am(aul = "8105ac4e9d1bb566b1e88e4e2d33d5c1", aum = 0)
    /* renamed from: C */
    private boolean m7970C(Actor cr) {
        return false;
    }

    /* renamed from: a.ON$a */
    class C0977a extends C3284pw {
        C0977a(Vec3d ajr) {
            super(ajr);
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public boolean mo4408e(Vec3f vec3f, float f) {
            boolean e;
            boolean z;
            if (PlayerArrivalController.this.afq() == null) {
                e = super.mo4408e(vec3f, f);
            } else {
                f = aYa().mo1013bx(PlayerArrivalController.this.afq());
                e = super.mo4408e(vec3f, f);
            }
            if (!e) {
                return false;
            }
            if (!PlayerArrivalController.this.cBx() || !(PlayerArrivalController.this.afq() instanceof C0286Dh)) {
                return true;
            }
            if (f > PlayerController.m39266c((C0286Dh) PlayerArrivalController.this.afq()) * 2.0f) {
                z = true;
            } else {
                z = false;
            }
            return z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: cN */
        public float mo3565cN(float f) {
            if (PlayerArrivalController.this.cBx() && (PlayerArrivalController.this.afq() instanceof C0286Dh)) {
                return aYa().mo1091ra();
            }
            if (PlayerArrivalController.this.afq() != null) {
                return (float) Math.max((double) super.mo3565cN(aYa().mo1013bx(PlayerArrivalController.this.afq())), ((double) aYa().mo1091ra()) * 0.1d);
            }
            return (float) Math.max((double) super.mo3565cN(f), ((double) aYa().mo1091ra()) * 0.1d);
        }
    }
}
