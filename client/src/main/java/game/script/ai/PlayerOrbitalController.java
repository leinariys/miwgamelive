package game.script.ai;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.ObjectId;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.item.Shot;
import game.script.ship.Ship;
import game.script.space.Node;
import logic.abb.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2845kx;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import javax.vecmath.Vector3f;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C5256aCi
@C6485anp
/* renamed from: a.vN */
/* compiled from: a */
public class PlayerOrbitalController extends Controller implements C1616Xf, C2540gZ {
    /* renamed from: _f_applyUsingDesiredVelocity_0020_0028FLtaikodom_002fgame_002fscript_002fPawn_003bLcom_002fhoplon_002fgeometry_002fVec3f_003bFFIF_0029V */
    public static final C2491fm f9405x5a73435d = null;
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f9406xdce2e729 = null;
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f9407xebad5167 = null;
    /* renamed from: _f_convertForceToDesiredVelocity_0020_0028FLtaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003b_0029Lcom_002fhoplon_002fgeometry_002fVec3f_003b */
    public static final C2491fm f9408x1ef544f6 = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_fillShipCommand_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bF_0029Z */
    public static final C2491fm f9409xbc82b75d = null;
    public static final C2491fm _f_forgetNearbyObjects_0020_0028_0029V = null;
    public static final C2491fm _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f9410x851d656b = null;
    /* renamed from: _f_isEnemy_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f9411x72b3cf67 = null;
    /* renamed from: _f_isInvalid_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003b_0029Z */
    public static final C2491fm f9412x30bf686c = null;
    public static final C2491fm _f_isRunning_0020_0028_0029Z = null;
    /* renamed from: _f_noCollision_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f9413xe5145ada = null;
    public static final C2491fm _f_onEnterCruiseSpeed_0020_0028_0029V = null;
    public static final C2491fm _f_onExitCruiseSpeed_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm azU = null;
    public static final C5663aRz boP = null;
    public static final C2491fm eZw = null;
    public static final C2491fm eZx = null;
    public static final C5663aRz gAA = null;
    public static final C2491fm gAC = null;
    public static final C2491fm gAD = null;
    public static final C2491fm gAE = null;
    public static final C2491fm gAF = null;
    public static final C2491fm gAG = null;
    public static final C2491fm gAH = null;
    public static final C5663aRz gAv = null;
    public static final C5663aRz gAw = null;
    public static final C5663aRz gAx = null;
    public static final C5663aRz gAy = null;
    public static final C5663aRz gAz = null;
    public static final long serialVersionUID = 0;
    private static final int FIRST_STEP_STARTING_CRUISE_SPEED = 2;
    private static final int IN_CRUISE_SPEED = 1;
    private static final int IN_NORMAL_SPEED = 0;
    private static final int STARTING_CRUISE_SPEED = 3;
    private static final float gAB = 1000.0f;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "cf8e8969f576997302bfaf3afccf7c37", aum = 6)
    @C5256aCi
    private static Actor ams = null;
    @C0064Am(aul = "186e77b6d002a6d639c4444925181d18", aum = 0)
    @C5256aCi
    private static C0573Hw[] auc = null;
    @C0064Am(aul = "fc8182e36efed984920722969c37c359", aum = 1)
    @C5256aCi
    private static C0573Hw[] aud = null;
    @C0064Am(aul = "298f544cdd4be517e51bd484cb57337c", aum = 2)
    @C5256aCi
    private static C0573Hw[] aue = null;
    @C0064Am(aul = "c2447492c798c0839c37b87bc2846519", aum = 4)
    @C5256aCi
    private static C1218Ry auf = null;
    @C0064Am(aul = "dbd7d44c464b1e126f44abcef1f1810d", aum = 5)
    @C5256aCi
    private static C0070Ar aug = null;
    @C0064Am(aul = "b57e7b749d8302e008ec30dfe8b012e3", aum = 3)
    @C5256aCi
    private static C6822auO avoidObstaclesBehaviour = null;

    static {
        m40205V();
    }

    @C5256aCi
    private transient List<Actor> nearbyObjects;

    public PlayerOrbitalController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerOrbitalController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40205V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Controller._m_fieldCount + 7;
        _m_methodCount = Controller._m_methodCount + 26;
        int i = Controller._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(PlayerOrbitalController.class, "186e77b6d002a6d639c4444925181d18", i);
        gAv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerOrbitalController.class, "fc8182e36efed984920722969c37c359", i2);
        gAw = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerOrbitalController.class, "298f544cdd4be517e51bd484cb57337c", i3);
        gAx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PlayerOrbitalController.class, "b57e7b749d8302e008ec30dfe8b012e3", i4);
        gAy = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PlayerOrbitalController.class, "c2447492c798c0839c37b87bc2846519", i5);
        gAz = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(PlayerOrbitalController.class, "dbd7d44c464b1e126f44abcef1f1810d", i6);
        gAA = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(PlayerOrbitalController.class, "cf8e8969f576997302bfaf3afccf7c37", i7);
        boP = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Controller._m_fields, (Object[]) _m_fields);
        int i9 = Controller._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 26)];
        C2491fm a = C4105zY.m41624a(PlayerOrbitalController.class, "9e75354b31dba97a3634b649735fd365", i9);
        gAC = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerOrbitalController.class, "43716cb9f3c8c2a4fb88ffe323eb3dd6", i10);
        azU = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerOrbitalController.class, "5f394dd4e1cf886535b1251351b3bc6a", i11);
        eZx = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerOrbitalController.class, "36892b9b7c3a5693319866efb015748e", i12);
        eZw = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerOrbitalController.class, "fe65536f04d73861905591694ac6737b", i13);
        _f_dispose_0020_0028_0029V = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerOrbitalController.class, "9aea42501feff514f7a5e6746aee181c", i14);
        _f_stop_0020_0028_0029V = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerOrbitalController.class, "ea657e86fb98a9db1854fd84a55133c3", i15);
        _f_step_0020_0028F_0029V = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerOrbitalController.class, "722e99d60f2c586e7274015edd99fac6", i16);
        f9406xdce2e729 = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerOrbitalController.class, "891c430c6791459d83318d2366150a38", i17);
        f9407xebad5167 = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerOrbitalController.class, "a4c48f06d7ff6c20ab5ce94812d17296", i18);
        f9412x30bf686c = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerOrbitalController.class, "77e412a2a0a364c5fb50a860dd4b0094", i19);
        f9409xbc82b75d = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerOrbitalController.class, "2ac6b109a86e89d136deacb3b7aa6bbf", i20);
        _f_forgetNearbyObjects_0020_0028_0029V = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerOrbitalController.class, "9819f0bb4d95aff0ffcbc2d08515e739", i21);
        _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerOrbitalController.class, "5fd88aaaee0c35aa59bf468e17aa1cd7", i22);
        f9413xe5145ada = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(PlayerOrbitalController.class, "4440af3295558617f3c409d326138299", i23);
        f9411x72b3cf67 = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(PlayerOrbitalController.class, "9c53673c0c06ca90c9d0892047b458c8", i24);
        f9405x5a73435d = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(PlayerOrbitalController.class, "80cd54c7aeee4b6ede98b87f33f7c795", i25);
        gAD = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(PlayerOrbitalController.class, "c116fcf7aea6e1e6f6074d4615080c25", i26);
        f9410x851d656b = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        C2491fm a19 = C4105zY.m41624a(PlayerOrbitalController.class, "14a9c2f218d1e1e9821f7d2fa61375a6", i27);
        f9408x1ef544f6 = a19;
        fmVarArr[i27] = a19;
        int i28 = i27 + 1;
        C2491fm a20 = C4105zY.m41624a(PlayerOrbitalController.class, "0ecf5f06994e7178ad8cde79954fef91", i28);
        _f_isRunning_0020_0028_0029Z = a20;
        fmVarArr[i28] = a20;
        int i29 = i28 + 1;
        C2491fm a21 = C4105zY.m41624a(PlayerOrbitalController.class, "7d1be3ba058946cdf9dbfd7adbff0622", i29);
        gAE = a21;
        fmVarArr[i29] = a21;
        int i30 = i29 + 1;
        C2491fm a22 = C4105zY.m41624a(PlayerOrbitalController.class, "267ccddc16d6ac7655b8d76ba2105b3f", i30);
        gAF = a22;
        fmVarArr[i30] = a22;
        int i31 = i30 + 1;
        C2491fm a23 = C4105zY.m41624a(PlayerOrbitalController.class, "b5cd9d4e3d3d4b85c5dad129af027ba1", i31);
        _f_onEnterCruiseSpeed_0020_0028_0029V = a23;
        fmVarArr[i31] = a23;
        int i32 = i31 + 1;
        C2491fm a24 = C4105zY.m41624a(PlayerOrbitalController.class, "52c2080d96b4b86c6bb989c39b160a20", i32);
        _f_onExitCruiseSpeed_0020_0028_0029V = a24;
        fmVarArr[i32] = a24;
        int i33 = i32 + 1;
        C2491fm a25 = C4105zY.m41624a(PlayerOrbitalController.class, "61e5f97ec468b6bd063663787b9f6bdf", i33);
        gAG = a25;
        fmVarArr[i33] = a25;
        int i34 = i33 + 1;
        C2491fm a26 = C4105zY.m41624a(PlayerOrbitalController.class, "ca8f1d7ecc9cc858460472ec802c9a30", i34);
        gAH = a26;
        fmVarArr[i34] = a26;
        int i35 = i34 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Controller._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerOrbitalController.class, C2845kx.class, _m_fields, _m_methods);
    }

    /* renamed from: Mj */
    private void m40196Mj() {
        switch (bFf().mo6893i(azU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                break;
        }
        m40195Mi();
    }

    /* renamed from: Ml */
    private void m40198Ml() {
        switch (bFf().mo6893i(_f_forgetNearbyObjects_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                break;
        }
        m40197Mk();
    }

    /* renamed from: R */
    private void m40202R(Actor cr) {
        bFf().mo5608dq().mo3197f(boP, cr);
    }

    /* renamed from: T */
    private void m40204T(Vec3d ajr) {
        switch (bFf().mo6893i(eZw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eZw, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eZw, new Object[]{ajr}));
                break;
        }
        m40203S(ajr);
    }

    /* renamed from: a */
    private void m40209a(C0070Ar ar) {
        bFf().mo5608dq().mo3197f(gAA, ar);
    }

    /* renamed from: a */
    private void m40210a(Actor cr) {
        switch (bFf().mo6893i(eZx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eZx, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eZx, new Object[]{cr}));
                break;
        }
        m40215aM(cr);
    }

    /* renamed from: a */
    private void m40211a(C6822auO auo) {
        bFf().mo5608dq().mo3197f(gAy, auo);
    }

    /* renamed from: a */
    private boolean m40212a(C1216Rw rw, float f) {
        switch (bFf().mo6893i(f9409xbc82b75d)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9409xbc82b75d, new Object[]{rw, new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9409xbc82b75d, new Object[]{rw, new Float(f)}));
                break;
        }
        return m40224b(rw, f);
    }

    private Actor afq() {
        return (Actor) bFf().mo5608dq().mo3214p(boP);
    }

    /* renamed from: ax */
    private boolean m40218ax(Actor cr) {
        switch (bFf().mo6893i(f9413xe5145ada)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9413xe5145ada, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9413xe5145ada, new Object[]{cr}));
                break;
        }
        return m40217aw(cr);
    }

    /* renamed from: b */
    private int m40220b(C1216Rw rw) {
        switch (bFf().mo6893i(gAD)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gAD, new Object[]{rw}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAD, new Object[]{rw}));
                break;
        }
        return m40206a(rw);
    }

    /* renamed from: b */
    private Vec3f m40221b(float f, C1216Rw rw) {
        switch (bFf().mo6893i(f9408x1ef544f6)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, f9408x1ef544f6, new Object[]{new Float(f), rw}));
            case 3:
                bFf().mo5606d(new aCE(this, f9408x1ef544f6, new Object[]{new Float(f), rw}));
                break;
        }
        return m40207a(f, rw);
    }

    /* renamed from: b */
    private void m40222b(float f, Pawn avi, Vec3f vec3f, float f2, float f3, int i, float f4) {
        switch (bFf().mo6893i(f9405x5a73435d)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9405x5a73435d, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Integer(i), new Float(f4)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9405x5a73435d, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Integer(i), new Float(f4)}));
                break;
        }
        m40208a(f, avi, vec3f, f2, f3, i, f4);
    }

    /* renamed from: b */
    private void m40223b(C1218Ry ry) {
        bFf().mo5608dq().mo3197f(gAz, ry);
    }

    /* renamed from: b */
    private boolean m40225b(Vec3d ajr, String str) {
        switch (bFf().mo6893i(f9406xdce2e729)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9406xdce2e729, new Object[]{ajr, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9406xdce2e729, new Object[]{ajr, str}));
                break;
        }
        return m40213a(ajr, str);
    }

    /* renamed from: b */
    private boolean m40226b(Vec3f vec3f, String str) {
        switch (bFf().mo6893i(f9407xebad5167)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9407xebad5167, new Object[]{vec3f, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9407xebad5167, new Object[]{vec3f, str}));
                break;
        }
        return m40214a(vec3f, str);
    }

    /* renamed from: c */
    private void m40227c(C0573Hw[] hwArr) {
        bFf().mo5608dq().mo3197f(gAv, hwArr);
    }

    private C0573Hw[] cvZ() {
        return (C0573Hw[]) bFf().mo5608dq().mo3214p(gAv);
    }

    private C0573Hw[] cwa() {
        return (C0573Hw[]) bFf().mo5608dq().mo3214p(gAw);
    }

    private C0573Hw[] cwb() {
        return (C0573Hw[]) bFf().mo5608dq().mo3214p(gAx);
    }

    private C6822auO cwc() {
        return (C6822auO) bFf().mo5608dq().mo3214p(gAy);
    }

    private C1218Ry cwd() {
        return (C1218Ry) bFf().mo5608dq().mo3214p(gAz);
    }

    private C0070Ar cwe() {
        return (C0070Ar) bFf().mo5608dq().mo3214p(gAA);
    }

    private float cwi() {
        switch (bFf().mo6893i(gAF)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gAF, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAF, new Object[0]));
                break;
        }
        return cwh();
    }

    /* renamed from: d */
    private void m40228d(C0573Hw[] hwArr) {
        bFf().mo5608dq().mo3197f(gAw, hwArr);
    }

    /* renamed from: e */
    private void m40230e(C0573Hw[] hwArr) {
        bFf().mo5608dq().mo3197f(gAx, hwArr);
    }

    /* renamed from: s */
    private boolean m40234s(Vec3f vec3f) {
        switch (bFf().mo6893i(f9412x30bf686c)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9412x30bf686c, new Object[]{vec3f}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9412x30bf686c, new Object[]{vec3f}));
                break;
        }
        return m40233r(vec3f);
    }

    /* renamed from: D */
    public boolean mo3298D(Actor cr) {
        switch (bFf().mo6893i(f9411x72b3cf67)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9411x72b3cf67, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9411x72b3cf67, new Object[]{cr}));
                break;
        }
        return m40194C(cr);
    }

    /* renamed from: Mn */
    public List<Actor> mo3301Mn() {
        switch (bFf().mo6893i(_f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
                break;
        }
        return m40199Mm();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2845kx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Controller._m_methodCount) {
            case 0:
                m40216aZ((Actor) args[0]);
                return null;
            case 1:
                m40195Mi();
                return null;
            case 2:
                m40215aM((Actor) args[0]);
                return null;
            case 3:
                m40203S((Vec3d) args[0]);
                return null;
            case 4:
                m40231fg();
                return null;
            case 5:
                m40232qO();
                return null;
            case 6:
                m40219ay(((Float) args[0]).floatValue());
                return null;
            case 7:
                return new Boolean(m40213a((Vec3d) args[0], (String) args[1]));
            case 8:
                return new Boolean(m40214a((Vec3f) args[0], (String) args[1]));
            case 9:
                return new Boolean(m40233r((Vec3f) args[0]));
            case 10:
                return new Boolean(m40224b((C1216Rw) args[0], ((Float) args[1]).floatValue()));
            case 11:
                m40197Mk();
                return null;
            case 12:
                return m40199Mm();
            case 13:
                return new Boolean(m40217aw((Actor) args[0]));
            case 14:
                return new Boolean(m40194C((Actor) args[0]));
            case 15:
                m40208a(((Float) args[0]).floatValue(), (Pawn) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), ((Float) args[4]).floatValue(), ((Integer) args[5]).intValue(), ((Float) args[6]).floatValue());
                return null;
            case 16:
                return new Integer(m40206a((C1216Rw) args[0]));
            case 17:
                return m40200Mo();
            case 18:
                return m40207a(((Float) args[0]).floatValue(), (C1216Rw) args[1]);
            case 19:
                return new Boolean(m40201Mp());
            case 20:
                return new Boolean(cwf());
            case 21:
                return new Float(cwh());
            case 22:
                aYj();
                return null;
            case 23:
                aYk();
                return null;
            case 24:
                m40229e((Actor) args[0], (Vec3f) args[1]);
                return null;
            case 25:
                cwj();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: al */
    public Ship mo22573al() {
        switch (bFf().mo6893i(f9410x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f9410x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9410x851d656b, new Object[0]));
                break;
        }
        return m40200Mo();
    }

    /* renamed from: ba */
    public void mo22574ba(Actor cr) {
        switch (bFf().mo6893i(gAC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAC, new Object[]{cr}));
                break;
        }
        m40216aZ(cr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public boolean cwg() {
        switch (bFf().mo6893i(gAE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gAE, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAE, new Object[0]));
                break;
        }
        return cwf();
    }

    public void cwk() {
        switch (bFf().mo6893i(gAH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAH, new Object[0]));
                break;
        }
        cwj();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m40231fg();
    }

    /* renamed from: f */
    public void mo22577f(Actor cr, Vec3f vec3f) {
        switch (bFf().mo6893i(gAG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAG, new Object[]{cr, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAG, new Object[]{cr, vec3f}));
                break;
        }
        m40229e(cr, vec3f);
    }

    public boolean isRunning() {
        switch (bFf().mo6893i(_f_isRunning_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m40201Mp();
    }

    @C1253SX
    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m40219ay(f);
    }

    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m40232qO();
    }

    /* renamed from: xO */
    public void mo3296xO() {
        switch (bFf().mo6893i(_f_onEnterCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYj();
    }

    /* renamed from: xP */
    public void mo3297xP() {
        switch (bFf().mo6893i(_f_onExitCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYk();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "9e75354b31dba97a3634b649735fd365", aum = 0)
    /* renamed from: aZ */
    private void m40216aZ(Actor cr) {
        stop();
        m40202R(cr);
        m40196Mj();
        m40210a(cr);
    }

    @C0064Am(aul = "43716cb9f3c8c2a4fb88ffe323eb3dd6", aum = 0)
    /* renamed from: Mi */
    private void m40195Mi() {
        m40211a(new C6822auO());
        cwc().mo16334a(afq());
        m40227c(new C0573Hw[2]);
        C1829aV aVVar = new C1829aV();
        cvZ()[0] = aVVar;
        aVVar.mo11762a(afq());
        if (afq() instanceof Ship) {
            m40223b((C1218Ry) new C5697aTh((Ship) afq(), false));
        } else {
            m40223b((C1218Ry) new C3816a(afq().getPosition()));
            ((C3284pw) cwd()).mo21251cO((float) Math.sqrt((double) cwi()));
        }
        cvZ()[1] = new C0870Mc(cwc(), cwd());
        for (C0573Hw a : cvZ()) {
            a.mo2697a(this);
        }
        m40209a(new C0070Ar());
        m40228d(new C0573Hw[2]);
        C6822auO auo = new C6822auO();
        auo.mo16341kP(1.0f);
        auo.mo16342kQ(10.0f);
        auo.cym();
        auo.mo16340gQ(true);
        cwa()[0] = auo;
        cwa()[1] = new C0870Mc(cwc(), cwe());
        for (C0573Hw a2 : cwa()) {
            a2.mo2697a(this);
        }
        m40230e(cvZ());
    }

    @C0064Am(aul = "5f394dd4e1cf886535b1251351b3bc6a", aum = 0)
    /* renamed from: aM */
    private void m40215aM(Actor cr) {
        ((C1829aV) cvZ()[0]).mo11762a(cr);
        ((C6822auO) cwa()[0]).mo16334a(cr);
        cwc().mo16334a(cr);
        cwe().mo476am(cr);
    }

    @C0064Am(aul = "36892b9b7c3a5693319866efb015748e", aum = 0)
    /* renamed from: S */
    private void m40203S(Vec3d ajr) {
    }

    @C0064Am(aul = "fe65536f04d73861905591694ac6737b", aum = 0)
    /* renamed from: fg */
    private void m40231fg() {
        stop();
        super.dispose();
    }

    @C0064Am(aul = "9aea42501feff514f7a5e6746aee181c", aum = 0)
    /* renamed from: qO */
    private void m40232qO() {
        m40202R((Actor) null);
        m40227c((C0573Hw[]) null);
        m40228d((C0573Hw[]) null);
        if (cwc() != null) {
            cwc().mo16334a((Actor) null);
        }
        m40211a((C6822auO) null);
        if (cwe() != null) {
            cwe().mo476am((Actor) null);
        }
        m40209a((C0070Ar) null);
    }

    @C0064Am(aul = "ea657e86fb98a9db1854fd84a55133c3", aum = 0)
    @C1253SX
    /* renamed from: ay */
    private void m40219ay(float f) {
        float f2;
        cwk();
        if (!C5877acF.fdP) {
            if (!isRunning()) {
                stop();
                return;
            }
            if (cwg()) {
                m40230e(cwa());
            } else {
                m40230e(cvZ());
            }
            Pawn aYa = aYa();
            if (m40226b(aYa.mo1090qZ(), "pawn local linear velocity") && m40226b(aYa.aSf(), "pawn local angular velocity") && m40225b(aYa.getPosition(), "pawn relative position")) {
                C1216Rw rw = new C1216Rw();
                if (m40212a(rw, f)) {
                    mo22577f(aYa, rw.brw());
                    int i = 0;
                    if (aYa instanceof Ship) {
                        i = m40220b(rw);
                    }
                    if (!(rw.brw().x == 0.0f && rw.brw().y == 0.0f && rw.brw().z == 0.0f) && m40226b(rw.brw(), "Force")) {
                        Vec3f b = m40221b(f, rw);
                        C6294akG.m23128a("desired-speed", (Actor) aYa(), b);
                        if (i == 0) {
                            float length = b.length();
                            float ra = aYa.mo1091ra();
                            if (length <= ra) {
                                if (((double) length) < 1.01d) {
                                    ra = 0.0f;
                                } else {
                                    ra = length;
                                }
                            }
                            f2 = !isRunning() ? 0.0f : ra;
                        } else {
                            f2 = 0.0f;
                        }
                        m40222b(f, aYa, b, f2, rw.getRoll(), i, rw.brA());
                    }
                }
            }
        }
    }

    @C0064Am(aul = "722e99d60f2c586e7274015edd99fac6", aum = 0)
    /* renamed from: a */
    private boolean m40213a(Vec3d ajr, String str) {
        float f = (float) (ajr.x + ajr.y + ajr.z);
        if (!Float.isNaN(f) && !Float.isInfinite(f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        mo8358lY("PlayerArrivalController: " + str + " is NaN or Infinite: " + ajr + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        return false;
    }

    @C0064Am(aul = "891c430c6791459d83318d2366150a38", aum = 0)
    /* renamed from: a */
    private boolean m40214a(Vec3f vec3f, String str) {
        if (!m40234s(vec3f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        mo8358lY("PlayerArrivalController: " + str + " is NaN or Infinite: " + vec3f + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        return false;
    }

    @C0064Am(aul = "a4c48f06d7ff6c20ab5ce94812d17296", aum = 0)
    /* renamed from: r */
    private boolean m40233r(Vec3f vec3f) {
        return Float.isNaN(vec3f.x) || Float.isInfinite(vec3f.x) || Float.isNaN(vec3f.y) || Float.isInfinite(vec3f.y) || Float.isNaN(vec3f.z) || Float.isInfinite(vec3f.z);
    }

    @C0064Am(aul = "77e412a2a0a364c5fb50a860dd4b0094", aum = 0)
    /* renamed from: b */
    private boolean m40224b(C1216Rw rw, float f) {
        m40198Ml();
        rw.clear();
        if (cwb() == null) {
            return false;
        }
        for (C0573Hw a : cwb()) {
            if (a.mo31a(rw, f)) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "2ac6b109a86e89d136deacb3b7aa6bbf", aum = 0)
    /* renamed from: Mk */
    private void m40197Mk() {
        this.nearbyObjects = null;
    }

    @C0064Am(aul = "9819f0bb4d95aff0ffcbc2d08515e739", aum = 0)
    /* renamed from: Mm */
    private List<Actor> m40199Mm() {
        if (this.nearbyObjects == null) {
            this.nearbyObjects = aYa().mo965Zc().mo1900c(aYa().getPosition(), 4000.0f);
            Iterator<Actor> it = this.nearbyObjects.iterator();
            while (it.hasNext()) {
                Actor next = it.next();
                if (next == aYa() || m40218ax(next) || (next instanceof Shot)) {
                    it.remove();
                }
            }
        }
        return this.nearbyObjects;
    }

    @C0064Am(aul = "5fd88aaaee0c35aa59bf468e17aa1cd7", aum = 0)
    /* renamed from: aw */
    private boolean m40217aw(Actor cr) {
        if (cr.aiD() == null || !aYa().mo965Zc().cKn().bvg().mo1932a(aYa().aiD(), cr.aiD()) || aYa().mo965Zc().cKn().mo5736c(aYa().aiD(), cr.aiD()) == 0) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "4440af3295558617f3c409d326138299", aum = 0)
    /* renamed from: C */
    private boolean m40194C(Actor cr) {
        return false;
    }

    @C0064Am(aul = "9c53673c0c06ca90c9d0892047b458c8", aum = 0)
    /* renamed from: a */
    private void m40208a(float f, Pawn avi, Vec3f vec3f, float f2, float f3, int i, float f4) {
        float f5;
        float f6;
        float VH = avi.mo963VH();
        float e = C3241pX.m37169e(-vec3f.z, vec3f.x);
        float e2 = C3241pX.m37169e(-vec3f.z, vec3f.y);
        if (Math.abs(e2) <= VH && Math.abs(e) <= VH) {
            f6 = e2;
            f5 = e;
        } else if (Math.abs(e2) > Math.abs(e)) {
            f6 = Math.min(Math.abs(e2), VH) * Math.signum(e2);
            f5 = (e / e2) * f6;
        } else {
            f5 = Math.signum(e) * Math.min(Math.abs(e), VH);
            f6 = f5 * (e2 / e);
        }
        if (i == 0) {
            if (Math.abs(e2) > 45.0f || Math.abs(e) > 45.0f) {
                if (f4 > 0.75f) {
                    f2 = 0.0f;
                } else if (f4 < 0.25f) {
                    f2 = Math.max(f2, (aYa().mo1091ra() / 10.0f) + 1.0f);
                }
            }
            avi.mo964W(f2);
            ((PlayerController) avi.mo2998hb()).mo22152lJ(f2);
        }
        avi.mo1063f(f3, f5, f6);
    }

    @C0064Am(aul = "80cd54c7aeee4b6ede98b87f33f7c795", aum = 0)
    /* renamed from: a */
    private int m40206a(C1216Rw rw) {
        if (rw.bry()) {
            if (mo22573al().ahf()) {
                mo22573al().mo7599de(cVr());
                return 2;
            } else if (mo22573al().ahh()) {
                return 3;
            } else {
                return 1;
            }
        } else if (!mo22573al().agZ() && !mo22573al().ahh()) {
            return 0;
        } else {
            mo22573al().mo7599de(cVr());
            if (!mo22573al().agZ()) {
                return 3;
            }
            return 1;
        }
    }

    @C0064Am(aul = "c116fcf7aea6e1e6f6074d4615080c25", aum = 0)
    /* renamed from: Mo */
    private Ship m40200Mo() {
        return (Ship) aYa();
    }

    @C0064Am(aul = "14a9c2f218d1e1e9821f7d2fa61375a6", aum = 0)
    /* renamed from: a */
    private Vec3f m40207a(float f, C1216Rw rw) {
        Vec3f vec3f = new Vec3f((Vector3f) rw.brw());
        vec3f.scale(f);
        Vec3f qZ = aYa().mo1090qZ();
        vec3f.x += qZ.x;
        vec3f.y += qZ.y;
        vec3f.z = qZ.z + vec3f.z;
        return vec3f;
    }

    @C0064Am(aul = "0ecf5f06994e7178ad8cde79954fef91", aum = 0)
    /* renamed from: Mp */
    private boolean m40201Mp() {
        if ((aYa() != null && aYa().isDead()) || aYa().isDisposed() || !aYa().bae() || aYa().getPosition() == null || afq() == null || afq().aiD() == null || afq().aiD().getPosition() == null) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "7d1be3ba058946cdf9dbfd7adbff0622", aum = 0)
    private boolean cwf() {
        return aYa().mo1013bx(afq()) < cwi();
    }

    @C0064Am(aul = "267ccddc16d6ac7655b8d76ba2105b3f", aum = 0)
    private float cwh() {
        if (cwc() == null || cwc().cyj() == null) {
            return 1000000.0f;
        }
        float outerSphereRadius = cwc().cyj().aiD().mo2437IL().getOuterSphereRadius() + aYa().aiD().mo2437IL().getOuterSphereRadius() + gAB;
        return outerSphereRadius * outerSphereRadius;
    }

    @C0064Am(aul = "b5cd9d4e3d3d4b85c5dad129af027ba1", aum = 0)
    private void aYj() {
        super.mo3296xO();
        if (isRunning()) {
            aYa().mo964W(aYa().mo1091ra());
        }
    }

    @C0064Am(aul = "52c2080d96b4b86c6bb989c39b160a20", aum = 0)
    private void aYk() {
        super.mo3297xP();
        if (isRunning()) {
            aYa().mo964W(aYa().mo1091ra());
        }
    }

    @C0064Am(aul = "61e5f97ec468b6bd063663787b9f6bdf", aum = 0)
    /* renamed from: e */
    private void m40229e(Actor cr, Vec3f vec3f) {
    }

    @C0064Am(aul = "ca8f1d7ecc9cc858460472ec802c9a30", aum = 0)
    private void cwj() {
    }

    /* renamed from: a.vN$a */
    class C3816a extends C3284pw {
        C3816a(Vec3d ajr) {
            super(ajr);
        }

        /* access modifiers changed from: protected */
        /* renamed from: cN */
        public float mo3565cN(float f) {
            return aYa().mo1091ra();
        }
    }
}
