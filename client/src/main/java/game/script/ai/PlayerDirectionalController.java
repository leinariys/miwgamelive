package game.script.ai;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.ObjectId;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.ship.Ship;
import game.script.space.Node;
import logic.abb.C1218Ry;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3993xq;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import javax.vecmath.Vector3f;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C5256aCi
@C6485anp
/* renamed from: a.lE */
/* compiled from: a */
public class PlayerDirectionalController extends Controller implements C1616Xf, C2540gZ {
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f8497xdce2e729 = null;
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f8498xebad5167 = null;
    /* renamed from: _f_convertForceToDesiredVelocity_0020_0028FLtaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003b_0029Lcom_002fhoplon_002fgeometry_002fVec3f_003b */
    public static final C2491fm f8499x1ef544f6 = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_fillShipCommand_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bF_0029Z */
    public static final C2491fm f8500xbc82b75d = null;
    public static final C2491fm _f_forgetNearbyObjects_0020_0028_0029V = null;
    public static final C2491fm _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f8501x851d656b = null;
    /* renamed from: _f_isEnemy_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f8502x72b3cf67 = null;
    /* renamed from: _f_isInvalid_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003b_0029Z */
    public static final C2491fm f8503x30bf686c = null;
    public static final C2491fm _f_isRunning_0020_0028_0029Z = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz azP = null;
    public static final C5663aRz azR = null;
    public static final C2491fm azS = null;
    public static final C2491fm azT = null;
    public static final C2491fm azU = null;
    public static final C2491fm azV = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uS */
    public static final C5663aRz f8504uS = null;
    private static final float azO = 0.004363323f;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "ebf0a87a2394a17ff4f2086f7dd7fd01", aum = 2)
    @C5256aCi
    private static C1218Ry azQ;
    @C0064Am(aul = "ff82bc7c67b2c3d931ce5187c3425afc", aum = 0)
    @C5256aCi
    private static Vec3d point;
    @C0064Am(aul = "b225cc5651b954b99627a76fd4144670", aum = 1)
    @C5256aCi
    private static float speed;

    static {
        m34725V();
    }

    @C5256aCi
    private transient List<Actor> nearbyObjects;

    public PlayerDirectionalController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerDirectionalController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34725V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Controller._m_fieldCount + 3;
        _m_methodCount = Controller._m_methodCount + 17;
        int i = Controller._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(PlayerDirectionalController.class, "ff82bc7c67b2c3d931ce5187c3425afc", i);
        azP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerDirectionalController.class, "b225cc5651b954b99627a76fd4144670", i2);
        f8504uS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerDirectionalController.class, "ebf0a87a2394a17ff4f2086f7dd7fd01", i3);
        azR = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Controller._m_fields, (Object[]) _m_fields);
        int i5 = Controller._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 17)];
        C2491fm a = C4105zY.m41624a(PlayerDirectionalController.class, "328706926f0d98412d51bdc5299042b4", i5);
        azS = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerDirectionalController.class, "de7e6f16c5cbb83eeacdfb2c0c28cecf", i6);
        azT = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerDirectionalController.class, "aa1f84b74249f2afd3ae7ed7e3e0cf14", i7);
        azU = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerDirectionalController.class, "c85df35a6bdc4cf4916c23d887d57257", i8);
        _f_dispose_0020_0028_0029V = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerDirectionalController.class, "89bc4fc06b9d15640db4de18a299286e", i9);
        _f_stop_0020_0028_0029V = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerDirectionalController.class, "7312524f8768abe66f2ff3053be4c171", i10);
        _f_step_0020_0028F_0029V = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerDirectionalController.class, "6f6a8f323e55b7e2df7af7e9122f83dd", i11);
        f8497xdce2e729 = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerDirectionalController.class, "7e88bda91eec7ffdf41b6dc36a685de5", i12);
        f8498xebad5167 = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerDirectionalController.class, "5d998ca6f0e6dfa29dd73de58131feb1", i13);
        f8503x30bf686c = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerDirectionalController.class, "25f28fa4f904a7249c5e6f92f72a8a3f", i14);
        f8500xbc82b75d = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerDirectionalController.class, "8ccc8fae4be3ae3be4d9c855242a0144", i15);
        _f_forgetNearbyObjects_0020_0028_0029V = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerDirectionalController.class, "d4f876edcfab035f069284291e0affe4", i16);
        _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerDirectionalController.class, "6013ce7601bfb600c81323ca219e1495", i17);
        azV = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerDirectionalController.class, "d775c484e7395d7fa3d57224e77dd205", i18);
        f8501x851d656b = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(PlayerDirectionalController.class, "cf94ae6e4f9f6bbc4a4f1fecb805a127", i19);
        f8499x1ef544f6 = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(PlayerDirectionalController.class, "c5d88f2903c8d5f0119553fe6e7d7ca3", i20);
        _f_isRunning_0020_0028_0029Z = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(PlayerDirectionalController.class, "dfe28e233c66f45b3003882379ffa7f5", i21);
        f8502x72b3cf67 = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Controller._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerDirectionalController.class, C3993xq.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m34714H(float f) {
        bFf().mo5608dq().mo3150a(f8504uS, f);
    }

    /* access modifiers changed from: private */
    /* renamed from: Me */
    public Vec3d m34715Me() {
        return (Vec3d) bFf().mo5608dq().mo3214p(azP);
    }

    /* renamed from: Mf */
    private C1218Ry m34716Mf() {
        return (C1218Ry) bFf().mo5608dq().mo3214p(azR);
    }

    /* renamed from: Mj */
    private void m34719Mj() {
        switch (bFf().mo6893i(azU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                break;
        }
        m34718Mi();
    }

    /* renamed from: Ml */
    private void m34721Ml() {
        switch (bFf().mo6893i(_f_forgetNearbyObjects_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                break;
        }
        m34720Mk();
    }

    /* renamed from: a */
    private void m34729a(C1218Ry ry) {
        bFf().mo5608dq().mo3197f(azR, ry);
    }

    /* renamed from: a */
    private boolean m34731a(C1216Rw rw, float f) {
        switch (bFf().mo6893i(f8500xbc82b75d)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8500xbc82b75d, new Object[]{rw, new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8500xbc82b75d, new Object[]{rw, new Float(f)}));
                break;
        }
        return m34738b(rw, f);
    }

    /* renamed from: b */
    private Vec3f m34736b(float f, C1216Rw rw) {
        switch (bFf().mo6893i(f8499x1ef544f6)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, f8499x1ef544f6, new Object[]{new Float(f), rw}));
            case 3:
                bFf().mo5606d(new aCE(this, f8499x1ef544f6, new Object[]{new Float(f), rw}));
                break;
        }
        return m34727a(f, rw);
    }

    /* renamed from: b */
    private void m34737b(float f, Pawn avi, Vec3f vec3f, float f2, float f3, float f4) {
        switch (bFf().mo6893i(azV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, azV, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Float(f4)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, azV, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Float(f4)}));
                break;
        }
        m34728a(f, avi, vec3f, f2, f3, f4);
    }

    /* renamed from: b */
    private boolean m34739b(Vec3d ajr, String str) {
        switch (bFf().mo6893i(f8497xdce2e729)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8497xdce2e729, new Object[]{ajr, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8497xdce2e729, new Object[]{ajr, str}));
                break;
        }
        return m34732a(ajr, str);
    }

    /* renamed from: b */
    private boolean m34740b(Vec3f vec3f, String str) {
        switch (bFf().mo6893i(f8498xebad5167)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8498xebad5167, new Object[]{vec3f, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8498xebad5167, new Object[]{vec3f, str}));
                break;
        }
        return m34733a(vec3f, str);
    }

    /* access modifiers changed from: private */
    /* renamed from: ii */
    public float m34742ii() {
        return bFf().mo5608dq().mo3211m(f8504uS);
    }

    /* renamed from: o */
    private void m34743o(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(azP, ajr);
    }

    /* renamed from: s */
    private boolean m34746s(Vec3f vec3f) {
        switch (bFf().mo6893i(f8503x30bf686c)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8503x30bf686c, new Object[]{vec3f}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8503x30bf686c, new Object[]{vec3f}));
                break;
        }
        return m34745r(vec3f);
    }

    /* renamed from: D */
    public boolean mo3298D(Actor cr) {
        switch (bFf().mo6893i(f8502x72b3cf67)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f8502x72b3cf67, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8502x72b3cf67, new Object[]{cr}));
                break;
        }
        return m34713C(cr);
    }

    /* renamed from: Mh */
    public Vec3d mo20179Mh() {
        switch (bFf().mo6893i(azS)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, azS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, azS, new Object[0]));
                break;
        }
        return m34717Mg();
    }

    /* renamed from: Mn */
    public List<Actor> mo3301Mn() {
        switch (bFf().mo6893i(_f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
                break;
        }
        return m34722Mm();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3993xq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Controller._m_methodCount) {
            case 0:
                return m34717Mg();
            case 1:
                m34730a((Ship) args[0], (Vec3f) args[1]);
                return null;
            case 2:
                m34718Mi();
                return null;
            case 3:
                m34741fg();
                return null;
            case 4:
                m34744qO();
                return null;
            case 5:
                m34734ay(((Float) args[0]).floatValue());
                return null;
            case 6:
                return new Boolean(m34732a((Vec3d) args[0], (String) args[1]));
            case 7:
                return new Boolean(m34733a((Vec3f) args[0], (String) args[1]));
            case 8:
                return new Boolean(m34745r((Vec3f) args[0]));
            case 9:
                return new Boolean(m34738b((C1216Rw) args[0], ((Float) args[1]).floatValue()));
            case 10:
                m34720Mk();
                return null;
            case 11:
                return m34722Mm();
            case 12:
                m34728a(((Float) args[0]).floatValue(), (Pawn) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), ((Float) args[4]).floatValue(), ((Float) args[5]).floatValue());
                return null;
            case 13:
                return m34723Mo();
            case 14:
                return m34727a(((Float) args[0]).floatValue(), (C1216Rw) args[1]);
            case 15:
                return new Boolean(m34724Mp());
            case 16:
                return new Boolean(m34713C((Actor) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: al */
    public Ship mo20180al() {
        switch (bFf().mo6893i(f8501x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f8501x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8501x851d656b, new Object[0]));
                break;
        }
        return m34723Mo();
    }

    /* renamed from: b */
    public void mo20181b(Ship fAVar, Vec3f vec3f) {
        switch (bFf().mo6893i(azT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, azT, new Object[]{fAVar, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, azT, new Object[]{fAVar, vec3f}));
                break;
        }
        m34730a(fAVar, vec3f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m34741fg();
    }

    public boolean isRunning() {
        switch (bFf().mo6893i(_f_isRunning_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m34724Mp();
    }

    @C1253SX
    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m34734ay(f);
    }

    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m34744qO();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "328706926f0d98412d51bdc5299042b4", aum = 0)
    /* renamed from: Mg */
    private Vec3d m34717Mg() {
        return m34715Me();
    }

    @C0064Am(aul = "de7e6f16c5cbb83eeacdfb2c0c28cecf", aum = 0)
    /* renamed from: a */
    private void m34730a(Ship fAVar, Vec3f vec3f) {
        stop();
        mo3293e(fAVar);
        m34743o(fAVar.mo1002bB(vec3f));
        if (aYa().mo1090qZ().lengthSquared() <= 0.0f) {
            m34714H(aYa().mo1091ra() / 2.0f);
        } else {
            m34714H(aYa().aSj());
        }
        m34719Mj();
    }

    @C0064Am(aul = "aa1f84b74249f2afd3ae7ed7e3e0cf14", aum = 0)
    /* renamed from: Mi */
    private void m34718Mi() {
        m34729a((C1218Ry) new C2866a());
        m34716Mf().mo2697a((C6078afy) this);
    }

    @C0064Am(aul = "c85df35a6bdc4cf4916c23d887d57257", aum = 0)
    /* renamed from: fg */
    private void m34741fg() {
        stop();
        super.dispose();
    }

    @C0064Am(aul = "89bc4fc06b9d15640db4de18a299286e", aum = 0)
    /* renamed from: qO */
    private void m34744qO() {
        m34743o((Vec3d) null);
        m34729a((C1218Ry) null);
    }

    @C0064Am(aul = "7312524f8768abe66f2ff3053be4c171", aum = 0)
    @C1253SX
    /* renamed from: ay */
    private void m34734ay(float f) {
        float f2;
        if (!C5877acF.fdP) {
            if (!isRunning()) {
                stop();
                return;
            }
            Pawn aYa = aYa();
            if (m34740b(aYa.mo1090qZ(), "pawn local linear velocity") && m34740b(aYa.aSf(), "pawn local angular velocity") && m34739b(aYa.getPosition(), "pawn relative position")) {
                C1216Rw rw = new C1216Rw();
                if (!m34731a(rw, f)) {
                    return;
                }
                if (!(rw.brw().x == 0.0f && rw.brw().y == 0.0f && rw.brw().z == 0.0f) && m34740b(rw.brw(), "Force")) {
                    Vec3f b = m34736b(f, rw);
                    float length = b.length();
                    float ra = aYa.mo1091ra();
                    if (length <= ra) {
                        if (((double) length) < 1.01d) {
                            ra = 0.0f;
                        } else {
                            ra = length;
                        }
                    }
                    if (!isRunning()) {
                        f2 = 0.0f;
                    } else {
                        f2 = ra;
                    }
                    m34737b(f, aYa, b, f2, rw.getRoll(), rw.brA());
                }
            }
        }
    }

    @C0064Am(aul = "6f6a8f323e55b7e2df7af7e9122f83dd", aum = 0)
    /* renamed from: a */
    private boolean m34732a(Vec3d ajr, String str) {
        float f = (float) (ajr.x + ajr.y + ajr.z);
        if (!Float.isNaN(f) && !Float.isInfinite(f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        mo8358lY("PlayerArrivalController: " + str + " is NaN or Infinite: " + ajr + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        return false;
    }

    @C0064Am(aul = "7e88bda91eec7ffdf41b6dc36a685de5", aum = 0)
    /* renamed from: a */
    private boolean m34733a(Vec3f vec3f, String str) {
        if (!m34746s(vec3f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        mo8358lY("PlayerArrivalController: " + str + " is NaN or Infinite: " + vec3f + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        return false;
    }

    @C0064Am(aul = "5d998ca6f0e6dfa29dd73de58131feb1", aum = 0)
    /* renamed from: r */
    private boolean m34745r(Vec3f vec3f) {
        return Float.isNaN(vec3f.x) || Float.isInfinite(vec3f.x) || Float.isNaN(vec3f.y) || Float.isInfinite(vec3f.y) || Float.isNaN(vec3f.z) || Float.isInfinite(vec3f.z);
    }

    @C0064Am(aul = "25f28fa4f904a7249c5e6f92f72a8a3f", aum = 0)
    /* renamed from: b */
    private boolean m34738b(C1216Rw rw, float f) {
        m34721Ml();
        rw.clear();
        if (m34716Mf() != null) {
            return m34716Mf().mo31a(rw, f);
        }
        return false;
    }

    @C0064Am(aul = "8ccc8fae4be3ae3be4d9c855242a0144", aum = 0)
    /* renamed from: Mk */
    private void m34720Mk() {
        this.nearbyObjects = null;
    }

    @C0064Am(aul = "d4f876edcfab035f069284291e0affe4", aum = 0)
    /* renamed from: Mm */
    private List<Actor> m34722Mm() {
        if (this.nearbyObjects == null) {
            this.nearbyObjects = aYa().mo965Zc().mo1900c(aYa().getPosition(), 4000.0f);
            Iterator<Actor> it = this.nearbyObjects.iterator();
            while (it.hasNext()) {
                if (it.next() == aYa()) {
                    it.remove();
                }
            }
        }
        return this.nearbyObjects;
    }

    @C0064Am(aul = "6013ce7601bfb600c81323ca219e1495", aum = 0)
    /* renamed from: a */
    private void m34728a(float f, Pawn avi, Vec3f vec3f, float f2, float f3, float f4) {
        float f5;
        float f6;
        float VH = avi.mo963VH();
        float e = C3241pX.m37169e(-vec3f.z, vec3f.x);
        float e2 = C3241pX.m37169e(-vec3f.z, vec3f.y);
        if (Math.abs(e2) <= VH && Math.abs(e) <= VH) {
            f6 = e2;
            f5 = e;
        } else if (Math.abs(e2) > Math.abs(e)) {
            f6 = Math.min(Math.abs(e2), VH) * Math.signum(e2);
            f5 = (e / e2) * f6;
        } else {
            f5 = Math.signum(e) * Math.min(Math.abs(e), VH);
            f6 = f5 * (e2 / e);
        }
        if (Math.abs(e2) > 45.0f || Math.abs(e) > 45.0f) {
            if (f4 > 0.75f) {
                f2 = 0.0f;
            } else if (f4 < 0.25f) {
                f2 = Math.max(f2, (aYa().mo1091ra() / 10.0f) + 1.0f);
            }
        }
        avi.mo964W(f2);
        avi.mo1063f(f3, f5, f6);
    }

    @C0064Am(aul = "d775c484e7395d7fa3d57224e77dd205", aum = 0)
    /* renamed from: Mo */
    private Ship m34723Mo() {
        return (Ship) aYa();
    }

    @C0064Am(aul = "cf94ae6e4f9f6bbc4a4f1fecb805a127", aum = 0)
    /* renamed from: a */
    private Vec3f m34727a(float f, C1216Rw rw) {
        Vec3f vec3f = new Vec3f((Vector3f) rw.brw());
        vec3f.scale(f);
        Vec3f qZ = aYa().mo1090qZ();
        vec3f.x += qZ.x;
        vec3f.y += qZ.y;
        vec3f.z = qZ.z + vec3f.z;
        return vec3f;
    }

    @C0064Am(aul = "c5d88f2903c8d5f0119553fe6e7d7ca3", aum = 0)
    /* renamed from: Mp */
    private boolean m34724Mp() {
        Vec3f vec3f = new Vec3f(0.0f, 0.0f, -1.0f);
        if (m34715Me() == null) {
            return false;
        }
        if (aYa().mo989an(m34715Me()).angle(vec3f) >= azO) {
            return true;
        }
        aYa().mo1063f(0.0f, 0.0f, 0.0f);
        return false;
    }

    @C0064Am(aul = "dfe28e233c66f45b3003882379ffa7f5", aum = 0)
    /* renamed from: C */
    private boolean m34713C(Actor cr) {
        return false;
    }

    /* renamed from: a.lE$a */
    class C2866a extends C1218Ry {
        private final Vec3d grI;
        private final float speed;

        C2866a() {
            this.grI = PlayerDirectionalController.this.m34715Me();
            this.speed = PlayerDirectionalController.this.m34742ii();
        }

        /* access modifiers changed from: protected */
        /* renamed from: cN */
        public float mo3565cN(float f) {
            return Math.max(0.0f, this.speed);
        }

        /* access modifiers changed from: protected */
        /* renamed from: Vv */
        public Vec3d mo5315Vv() {
            return this.grI;
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public boolean mo4408e(Vec3f vec3f, float f) {
            return false;
        }

        /* access modifiers changed from: protected */
        /* renamed from: Vw */
        public float mo5316Vw() {
            return Float.MAX_VALUE;
        }
    }
}
