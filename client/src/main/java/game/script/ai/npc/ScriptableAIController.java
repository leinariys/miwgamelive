package game.script.ai.npc;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1216Rw;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C1684Yo;
import game.script.Character;
import game.script.ship.Ship;
import logic.abb.C0573Hw;
import logic.abb.C3254pg;
import logic.baa.*;
import logic.data.mbean.C6989ayc;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.TaikodomAddon;

import javax.vecmath.Tuple3d;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

@C6485anp
@TaikodomAddon("taikodom.npcs.game.script")
@C2712iu
@C5511aMd
/* renamed from: a.PC */
/* compiled from: a */
public class ScriptableAIController extends TrackShipAIController implements C1616Xf, aJO {
    public static final C5663aRz _f_center = null;
    public static final C2491fm _f_changeAction_0020_0028_0029V = null;
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    /* renamed from: _f_fillShipCommand_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bF_0029Z */
    public static final C2491fm f1347xbc82b75d = null;
    /* renamed from: _f_getCenter_0020_0028_0029Lcom_002fhoplon_002fgeometry_002fVec3d_003b */
    public static final C2491fm f1348xdc7de0cc = null;
    public static final C2491fm _f_isValidTrackedShip_0020_0028_0029Z = null;
    public static final C5663aRz _f_radius2 = null;
    /* renamed from: _f_setCenter_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003b_0029V */
    public static final C2491fm f1349x6107980 = null;
    public static final C2491fm _f_setRadius_0020_0028F_0029V = null;
    /* renamed from: _f_setTrackedPilot_0020_0028Ltaikodom_002fgame_002fscript_002fCharacter_003b_0029V */
    public static final C2491fm f1350xf8357b61 = null;
    /* renamed from: _f_setTrackedShip_0020_0028Ltaikodom_002fgame_002fscript_002fship_002fShip_003b_0029V */
    public static final C2491fm f1351x30fdc3fb = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bcC = null;
    public static final C2491fm dEU = null;
    public static final C2491fm dRA = null;
    public static final C2491fm dRB = null;
    public static final C2491fm dRC = null;
    public static final C2491fm dRD = null;
    public static final C2491fm dRE = null;
    public static final C2491fm dRF = null;
    public static final C2491fm dRG = null;
    public static final C2491fm dRH = null;
    public static final C2491fm dRI = null;
    public static final C5663aRz dRg = null;
    public static final C5663aRz dRi = null;
    public static final C5663aRz dRk = null;
    public static final C5663aRz dRm = null;
    public static final C5663aRz dRo = null;
    public static final C2491fm dRr = null;
    public static final C2491fm dRs = null;
    public static final C2491fm dRt = null;
    public static final C2491fm dRu = null;
    public static final C2491fm dRv = null;
    public static final C2491fm dRw = null;
    public static final C2491fm dRx = null;
    public static final C2491fm dRy = null;
    public static final C2491fm dRz = null;
    private static final long serialVersionUID = 6148656021439756998L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "b2b621b437db55b16b91ced056544c95", aum = 0)
    @C5454aJy("Script")

    /* renamed from: UA */
    private static String f1346UA = "";
    @C0064Am(aul = "ab16ed9119b100700214b59b9fb4acc0", aum = 5)
    private static Vec3d center = null;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "2f121a786570d97e2a7a0daf01ad250b", aum = 1)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C1253SX
    private static C1556Wo<C1684Yo, LinkedList<C0573Hw>> dRh = null;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "af88a92a65b6a614daa5d4ba84fb37e2", aum = 2)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C1253SX
    private static C1556Wo<C1684Yo, Float> dRj = null;
    @C0064Am(aul = "0fc0d6a9816cd0f018bd02ebb76a32ba", aum = 3)
    @C1253SX
    private static C1556Wo<C1684Yo, C3254pg> dRl = null;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "a99a66ad0b2c3880d95bfeeae1e64f91", aum = 4)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C1253SX
    private static C1556Wo<C1684Yo, LinkedList<String>> dRn = null;
    @C0064Am(aul = "25523864bdbec0d1782d0f726cc85682", aum = 6)
    private static float radius2 = 0.0f;

    static {
        m8265V();
    }

    @ClientOnly

    /* renamed from: Aa */
    private org.mozilla1.javascript.Scriptable f1352Aa;
    @ClientOnly
    private C1684Yo dRp;
    @ClientOnly
    private boolean dRq;
    @ClientOnly

    /* renamed from: zZ */
    private org.mozilla1.javascript.Context f1353zZ;

    public ScriptableAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScriptableAIController(ScriptableAIControllerType abl) {
        super((C5540aNg) null);
        super._m_script_init(abl);
    }

    public ScriptableAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8265V() {
        _m_fieldCount = TrackShipAIController._m_fieldCount + 7;
        _m_methodCount = TrackShipAIController._m_methodCount + 30;
        int i = TrackShipAIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(ScriptableAIController.class, "b2b621b437db55b16b91ced056544c95", i);
        dRg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ScriptableAIController.class, "2f121a786570d97e2a7a0daf01ad250b", i2);
        dRi = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ScriptableAIController.class, "af88a92a65b6a614daa5d4ba84fb37e2", i3);
        dRk = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ScriptableAIController.class, "0fc0d6a9816cd0f018bd02ebb76a32ba", i4);
        dRm = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ScriptableAIController.class, "a99a66ad0b2c3880d95bfeeae1e64f91", i5);
        dRo = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ScriptableAIController.class, "ab16ed9119b100700214b59b9fb4acc0", i6);
        _f_center = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ScriptableAIController.class, "25523864bdbec0d1782d0f726cc85682", i7);
        _f_radius2 = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_fields, (Object[]) _m_fields);
        int i9 = TrackShipAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 30)];
        C2491fm a = C4105zY.m41624a(ScriptableAIController.class, "a4523ca871d786ea1b29d4640653a3c9", i9);
        dRr = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(ScriptableAIController.class, "2d0bb3a622715bedbec6f0acb236f62c", i10);
        _f_changeAction_0020_0028_0029V = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(ScriptableAIController.class, "578197e7f2c8dac32ea8a43112b1e617", i11);
        dRs = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(ScriptableAIController.class, "b67aa3cfdfc72ef13b4a5a4b031c8d48", i12);
        dRt = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(ScriptableAIController.class, "eb26828bed84f2e628a396a31f32e5a0", i13);
        dEU = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(ScriptableAIController.class, "b33d82a16553ef9b741c5e94be44cbcb", i14);
        dRu = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(ScriptableAIController.class, "4bb16573c7e128b6f8214b40d6c4332f", i15);
        _f_changeCurrentAction_0020_0028_0029I = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(ScriptableAIController.class, "456d3a4ae1f69b4b9456713ab627b043", i16);
        _f_setupBehaviours_0020_0028_0029V = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(ScriptableAIController.class, "179c3376991f3cf55decb3d2c286cac6", i17);
        f1347xbc82b75d = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(ScriptableAIController.class, "465349825bbacd3325e68e7b5681d38e", i18);
        f1351x30fdc3fb = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(ScriptableAIController.class, "58401d448096e161bd254a29ee7ae4b7", i19);
        f1350xf8357b61 = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(ScriptableAIController.class, "280a4d579d34b64eb1bf8153689c2a69", i20);
        _f_isValidTrackedShip_0020_0028_0029Z = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(ScriptableAIController.class, "8d93a901245fea34195ed51c2fa0f80d", i21);
        dRv = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(ScriptableAIController.class, "9d64a3816ae8b6ebf656310afdc36323", i22);
        dRw = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(ScriptableAIController.class, "5f773a3bc223ad78607f92e873fcdbaa", i23);
        dRx = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(ScriptableAIController.class, "f05014b3e20652ce7eefbc017c922f5e", i24);
        dRy = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(ScriptableAIController.class, "b01ea9514c8eb5ed0a3b2e859c501ded", i25);
        dRz = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(ScriptableAIController.class, "56ee972db25f6e1b6df46c3000370242", i26);
        dRA = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        C2491fm a19 = C4105zY.m41624a(ScriptableAIController.class, "034d693a87c49721136df072f9add5e0", i27);
        dRB = a19;
        fmVarArr[i27] = a19;
        int i28 = i27 + 1;
        C2491fm a20 = C4105zY.m41624a(ScriptableAIController.class, "5e849b33a34cd859010c6fad70b14109", i28);
        dRC = a20;
        fmVarArr[i28] = a20;
        int i29 = i28 + 1;
        C2491fm a21 = C4105zY.m41624a(ScriptableAIController.class, "bc437b810a70487fb4bd3a7c3f9fbd71", i29);
        dRD = a21;
        fmVarArr[i29] = a21;
        int i30 = i29 + 1;
        C2491fm a22 = C4105zY.m41624a(ScriptableAIController.class, "1beb8526619a4b856d26bfecf42bd2fa", i30);
        dRE = a22;
        fmVarArr[i30] = a22;
        int i31 = i30 + 1;
        C2491fm a23 = C4105zY.m41624a(ScriptableAIController.class, "c12a3e19163875ea77d286a98cff2441", i31);
        dRF = a23;
        fmVarArr[i31] = a23;
        int i32 = i31 + 1;
        C2491fm a24 = C4105zY.m41624a(ScriptableAIController.class, "e5d67ba028a0d280396a4a1a32eba3f4", i32);
        dRG = a24;
        fmVarArr[i32] = a24;
        int i33 = i32 + 1;
        C2491fm a25 = C4105zY.m41624a(ScriptableAIController.class, "e30c554aac223c78b464b5ba4e3b9067", i33);
        f1349x6107980 = a25;
        fmVarArr[i33] = a25;
        int i34 = i33 + 1;
        C2491fm a26 = C4105zY.m41624a(ScriptableAIController.class, "9471416a483990fd3c1a2e9edb64e42c", i34);
        _f_setRadius_0020_0028F_0029V = a26;
        fmVarArr[i34] = a26;
        int i35 = i34 + 1;
        C2491fm a27 = C4105zY.m41624a(ScriptableAIController.class, "84248d7e3f5c86a879071aa56f29f965", i35);
        f1348xdc7de0cc = a27;
        fmVarArr[i35] = a27;
        int i36 = i35 + 1;
        C2491fm a28 = C4105zY.m41624a(ScriptableAIController.class, "5da6b3d50b2f4cef6c8b91fc01fac93d", i36);
        dRH = a28;
        fmVarArr[i36] = a28;
        int i37 = i36 + 1;
        C2491fm a29 = C4105zY.m41624a(ScriptableAIController.class, "cbc0dd27e1eb87f8e1dbb50b5dcdb970", i37);
        dRI = a29;
        fmVarArr[i37] = a29;
        int i38 = i37 + 1;
        C2491fm a30 = C4105zY.m41624a(ScriptableAIController.class, "d6cca306edf996c9af7da365a13828b1", i38);
        bcC = a30;
        fmVarArr[i38] = a30;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScriptableAIController.class, C6989ayc.class, _m_fields, _m_methods);
    }

    /* renamed from: M */
    private void m8261M(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dRi, wo);
    }

    /* renamed from: N */
    private void m8262N(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dRk, wo);
    }

    /* renamed from: O */
    private void m8263O(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dRm, wo);
    }

    /* renamed from: P */
    private void m8264P(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dRo, wo);
    }

    /* renamed from: az */
    private void m8274az(float f) {
        bFf().mo5608dq().mo3150a(_f_radius2, f);
    }

    private C1556Wo bnA() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dRi);
    }

    private C1556Wo bnB() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dRk);
    }

    private C1556Wo bnC() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dRm);
    }

    private C1556Wo bnD() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dRo);
    }

    @C1253SX
    private void bnF() {
        switch (bFf().mo6893i(dRr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRr, new Object[0]));
                break;
        }
        bnE();
    }

    private String bnz() {
        return ((ScriptableAIControllerType) getType()).bIS();
    }

    /* renamed from: f */
    private void m8282f(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(_f_center, ajr);
    }

    /* renamed from: gk */
    private void m8285gk(String str) {
        throw new C6039afL();
    }

    /* renamed from: zL */
    private Vec3d m8288zL() {
        return (Vec3d) bFf().mo5608dq().mo3214p(_f_center);
    }

    /* renamed from: zM */
    private float m8289zM() {
        return bFf().mo5608dq().mo3211m(_f_radius2);
    }

    /* renamed from: J */
    public void mo4588J(Ship fAVar) {
        switch (bFf().mo6893i(f1351x30fdc3fb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1351x30fdc3fb, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1351x30fdc3fb, new Object[]{fAVar}));
                break;
        }
        m8259I(fAVar);
    }

    /* renamed from: L */
    public void mo4589L(Character acx) {
        switch (bFf().mo6893i(f1350xf8357b61)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1350xf8357b61, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1350xf8357b61, new Object[]{acx}));
                break;
        }
        m8260K(acx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6989ayc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TrackShipAIController._m_methodCount) {
            case 0:
                bnE();
                return null;
            case 1:
                aYp();
                return null;
            case 2:
                bnG();
                return null;
            case 3:
                m8287hg(((Float) args[0]).floatValue());
                return null;
            case 4:
                return m8278c((String) args[0], (Object[]) args[1]);
            case 5:
                m8275b((org.mozilla1.javascript.JavaScriptException) args[0]);
                return null;
            case 6:
                return new Integer(m8292zT());
            case 7:
                m8291zP();
                return null;
            case 8:
                return new Boolean(m8276b((C1216Rw) args[0], ((Float) args[1]).floatValue()));
            case 9:
                m8259I((Ship) args[0]);
                return null;
            case 10:
                m8260K((Character) args[0]);
                return null;
            case 11:
                return new Boolean(bnI());
            case 12:
                return bnK();
            case 13:
                return m8286gl((String) args[0]);
            case 14:
                return m8268a((C1684Yo) args[0]);
            case 15:
                m8279c((C1684Yo) args[0]);
                return null;
            case 16:
                m8270a((C1684Yo) args[0], (C0573Hw) args[1]);
                return null;
            case 17:
                m8272a((C1684Yo) args[0], (LinkedList<C0573Hw>) (LinkedList) args[1]);
                return null;
            case 18:
                m8269a((C1684Yo) args[0], ((Integer) args[1]).intValue(), (C0573Hw) args[2]);
                return null;
            case 19:
                return new Boolean(m8280c((C1684Yo) args[0], (C0573Hw) args[1]));
            case 20:
                return m8267a((C1684Yo) args[0], ((Integer) args[1]).intValue());
            case 21:
                return m8277c((C1684Yo) args[0], ((Integer) args[1]).intValue(), (C0573Hw) args[2]);
            case 22:
                m8281e((C1684Yo) args[0]);
                return null;
            case 23:
                m8271a((C1684Yo) args[0], (String) args[1]);
                return null;
            case 24:
                m8284g((Vec3d) args[0]);
                return null;
            case 25:
                m8273aA(((Float) args[0]).floatValue());
                return null;
            case 26:
                return m8290zN();
            case 27:
                return bnM();
            case 28:
                m8283g((C1684Yo) args[0]);
                return null;
            case 29:
                return m8266Zd();
            default:
                return super.mo14a(gr);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo4591a(org.mozilla1.javascript.JavaScriptException dxVar) {
        switch (bFf().mo6893i(dRu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRu, new Object[]{dxVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRu, new Object[]{dxVar}));
                break;
        }
        m8275b(dxVar);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo3305a(C1216Rw rw, float f) {
        switch (bFf().mo6893i(f1347xbc82b75d)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f1347xbc82b75d, new Object[]{rw, new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1347xbc82b75d, new Object[]{rw, new Float(f)}));
                break;
        }
        return m8276b(rw, f);
    }

    /* access modifiers changed from: protected */
    public void aYq() {
        switch (bFf().mo6893i(_f_changeAction_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_changeAction_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeAction_0020_0028_0029V, new Object[0]));
                break;
        }
        aYp();
    }

    /* renamed from: b */
    public C0573Hw mo4592b(C1684Yo yo, int i) {
        switch (bFf().mo6893i(dRD)) {
            case 0:
                return null;
            case 2:
                return (C0573Hw) bFf().mo5606d(new aCE(this, dRD, new Object[]{yo, new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, dRD, new Object[]{yo, new Integer(i)}));
                break;
        }
        return m8267a(yo, i);
    }

    /* renamed from: b */
    public LinkedList<C0573Hw> mo4593b(C1684Yo yo) {
        switch (bFf().mo6893i(dRx)) {
            case 0:
                return null;
            case 2:
                return (LinkedList) bFf().mo5606d(new aCE(this, dRx, new Object[]{yo}));
            case 3:
                bFf().mo5606d(new aCE(this, dRx, new Object[]{yo}));
                break;
        }
        return m8268a(yo);
    }

    /* renamed from: b */
    public void mo4594b(C1684Yo yo, int i, C0573Hw hw) {
        switch (bFf().mo6893i(dRB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRB, new Object[]{yo, new Integer(i), hw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRB, new Object[]{yo, new Integer(i), hw}));
                break;
        }
        m8269a(yo, i, hw);
    }

    /* renamed from: b */
    public void mo4595b(C1684Yo yo, C0573Hw hw) {
        switch (bFf().mo6893i(dRz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRz, new Object[]{yo, hw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRz, new Object[]{yo, hw}));
                break;
        }
        m8270a(yo, hw);
    }

    /* renamed from: b */
    public void mo4596b(C1684Yo yo, String str) {
        switch (bFf().mo6893i(dRG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRG, new Object[]{yo, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRG, new Object[]{yo, str}));
                break;
        }
        m8271a(yo, str);
    }

    /* renamed from: b */
    public void mo4597b(C1684Yo yo, LinkedList<C0573Hw> linkedList) {
        switch (bFf().mo6893i(dRA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRA, new Object[]{yo, linkedList}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRA, new Object[]{yo, linkedList}));
                break;
        }
        m8272a(yo, linkedList);
    }

    /* access modifiers changed from: protected */
    public void bnH() {
        switch (bFf().mo6893i(dRs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRs, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRs, new Object[0]));
                break;
        }
        bnG();
    }

    /* access modifiers changed from: protected */
    public boolean bnJ() {
        switch (bFf().mo6893i(_f_isValidTrackedShip_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]));
                break;
        }
        return bnI();
    }

    public Set<C1684Yo> bnL() {
        switch (bFf().mo6893i(dRv)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, dRv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dRv, new Object[0]));
                break;
        }
        return bnK();
    }

    public C1684Yo bnN() {
        switch (bFf().mo6893i(dRH)) {
            case 0:
                return null;
            case 2:
                return (C1684Yo) bFf().mo5606d(new aCE(this, dRH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dRH, new Object[0]));
                break;
        }
        return bnM();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public C0573Hw mo4602d(C1684Yo yo, int i, C0573Hw hw) {
        switch (bFf().mo6893i(dRE)) {
            case 0:
                return null;
            case 2:
                return (C0573Hw) bFf().mo5606d(new aCE(this, dRE, new Object[]{yo, new Integer(i), hw}));
            case 3:
                bFf().mo5606d(new aCE(this, dRE, new Object[]{yo, new Integer(i), hw}));
                break;
        }
        return m8277c(yo, i, hw);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public Object mo4603d(String str, Object... objArr) {
        switch (bFf().mo6893i(dEU)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, dEU, new Object[]{str, objArr}));
            case 3:
                bFf().mo5606d(new aCE(this, dEU, new Object[]{str, objArr}));
                break;
        }
        return m8278c(str, objArr);
    }

    /* renamed from: d */
    public void mo4604d(C1684Yo yo) {
        switch (bFf().mo6893i(dRy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRy, new Object[]{yo}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRy, new Object[]{yo}));
                break;
        }
        m8279c(yo);
    }

    /* renamed from: d */
    public boolean mo4605d(C1684Yo yo, C0573Hw hw) {
        switch (bFf().mo6893i(dRC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dRC, new Object[]{yo, hw}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dRC, new Object[]{yo, hw}));
                break;
        }
        return m8280c(yo, hw);
    }

    /* renamed from: f */
    public void mo4606f(C1684Yo yo) {
        switch (bFf().mo6893i(dRF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRF, new Object[]{yo}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRF, new Object[]{yo}));
                break;
        }
        m8281e(yo);
    }

    public Vec3d getPosition() {
        switch (bFf().mo6893i(bcC)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bcC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcC, new Object[0]));
                break;
        }
        return m8266Zd();
    }

    /* renamed from: gm */
    public C1684Yo mo4608gm(String str) {
        switch (bFf().mo6893i(dRw)) {
            case 0:
                return null;
            case 2:
                return (C1684Yo) bFf().mo5606d(new aCE(this, dRw, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dRw, new Object[]{str}));
                break;
        }
        return m8286gl(str);
    }

    /* renamed from: h */
    public void mo4609h(C1684Yo yo) {
        switch (bFf().mo6893i(dRI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRI, new Object[]{yo}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRI, new Object[]{yo}));
                break;
        }
        m8283g(yo);
    }

    /* renamed from: h */
    public void mo4610h(Vec3d ajr) {
        switch (bFf().mo6893i(f1349x6107980)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1349x6107980, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1349x6107980, new Object[]{ajr}));
                break;
        }
        m8284g(ajr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: hh */
    public void mo4611hh(float f) {
        switch (bFf().mo6893i(dRt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dRt, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dRt, new Object[]{new Float(f)}));
                break;
        }
        m8287hg(f);
    }

    public void setRadius(float f) {
        switch (bFf().mo6893i(_f_setRadius_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m8273aA(f);
    }

    /* renamed from: zO */
    public Vec3d mo4613zO() {
        switch (bFf().mo6893i(f1348xdc7de0cc)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, f1348xdc7de0cc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1348xdc7de0cc, new Object[0]));
                break;
        }
        return m8290zN();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m8291zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m8292zT();
    }

    /* renamed from: a */
    public void mo4590a(ScriptableAIControllerType abl) {
        super.mo967a((C2961mJ) abl);
        m8274az(-1.0f);
        this.dRq = false;
    }

    @C0064Am(aul = "a4523ca871d786ea1b29d4640653a3c9", aum = 0)
    @C1253SX
    private void bnE() {
        this.f1353zZ = org.mozilla1.javascript.Context.call();
        this.f1353zZ.setOptimizationLevel(-1);
        this.f1352Aa = this.f1353zZ.initStandardObjects();
        this.f1352Aa.put("controller", this.f1352Aa, (Object) this);
        this.f1352Aa.put("game/script", this.f1352Aa, (Object) bnz());
        this.f1352Aa.put("out", this.f1352Aa, (Object) System.out);
        try {
            this.f1353zZ.evaluateReader(this.f1352Aa, (Reader) new InputStreamReader(ScriptableAIController.class.getClassLoader().getResourceAsStream("taikodom/game/script/ai/npc/AIScript.js")), "<AIScript>", 1, (Object) null);
        } catch (org.mozilla1.javascript.JavaScriptException e) {
            mo4591a(e);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    @C0064Am(aul = "2d0bb3a622715bedbec6f0acb236f62c", aum = 0)
    private void aYp() {
        if (this.f1353zZ == null) {
            bnF();
        }
        if (mo4593b(this.dRp) != null) {
            bnH();
        }
    }

    @C0064Am(aul = "578197e7f2c8dac32ea8a43112b1e617", aum = 0)
    private void bnG() {
        double d;
        LinkedList linkedList = (LinkedList) bnD().get(this.dRp);
        if (linkedList == null) {
            linkedList = new LinkedList();
        }
        boolean bnJ = bnJ();
        if (linkedList.contains("validTarget") && bnJ) {
            mo4603d("invokeTrigger", "validTarget");
        }
        if (linkedList.contains("invalidTarget") && !bnJ) {
            mo4603d("invokeTrigger", "invalidTarget");
        }
        if (mo3314al() == null || mo3314al().getPosition() == null || m8288zL() == null) {
            d = ScriptRuntime.NaN;
        } else {
            d = Vec3d.m15654o(m8288zL(), mo3314al().getPosition());
        }
        if (linkedList.contains("outsideSphere") && d > ((double) (m8289zM() + 10000.0f))) {
            mo4603d("invokeTrigger", "outsideSphere");
        }
        if (linkedList.contains("insideSphere") && d < ((double) (m8289zM() + 10000.0f))) {
            mo4603d("invokeTrigger", "insideSphere");
        }
    }

    @C0064Am(aul = "b67aa3cfdfc72ef13b4a5a4b031c8d48", aum = 0)
    /* renamed from: hg */
    private void m8287hg(float f) {
        Float f2 = (Float) bnB().get(this.dRp);
        if (f2 != null) {
            float floatValue = f2.floatValue() - f;
            if (floatValue <= 0.0f) {
                mo4603d("continuate", new Object[0]);
                bnB().remove(this.dRp);
            } else {
                bnB().put(this.dRp, Float.valueOf(floatValue));
            }
        }
        C3254pg pgVar = (C3254pg) bnC().get(this.dRp);
        if (pgVar != null && pgVar.isEnded()) {
            mo4603d("continuate", new Object[0]);
            bnC().containsValue(pgVar);
            bnC().remove(this.dRp);
            bnC().containsValue(pgVar);
        }
    }

    @C0064Am(aul = "eb26828bed84f2e628a396a31f32e5a0", aum = 0)
    /* renamed from: c */
    private Object m8278c(String str, Object[] objArr) {
        Object obj = this.f1352Aa.get(str, this.f1352Aa);
        if (obj != org.mozilla1.javascript.Scriptable.NOT_FOUND) {
            try {
                return ((org.mozilla1.javascript.Function) obj).call(this.f1353zZ, this.f1352Aa, this.f1352Aa, objArr);
            } catch (org.mozilla1.javascript.JavaScriptException e) {
                mo4591a(e);
            }
        }
        return null;
    }

    @C0064Am(aul = "b33d82a16553ef9b741c5e94be44cbcb", aum = 0)
    /* renamed from: b */
    private void m8275b(org.mozilla1.javascript.JavaScriptException dxVar) {
        if ("waiting".equals(dxVar.getValue())) {
            Object d = mo4603d("getEnd", new Object[0]);
            if (d instanceof Double) {
                bnB().put(this.dRp, Float.valueOf(((Double) d).floatValue()));
            } else if (d instanceof org.mozilla1.javascript.NativeJavaObject) {
                bnC().put(this.dRp, (C3254pg) ((org.mozilla1.javascript.NativeJavaObject) d).unwrap());
            }
        } else {
            dxVar.printStackTrace();
        }
    }

    @C0064Am(aul = "4bb16573c7e128b6f8214b40d6c4332f", aum = 0)
    /* renamed from: zT */
    private int m8292zT() {
        return 0;
    }

    @C0064Am(aul = "456d3a4ae1f69b4b9456713ab627b043", aum = 0)
    /* renamed from: zP */
    private void m8291zP() {
        for (C1684Yo yo : bnA().keySet()) {
            LinkedList linkedList = (LinkedList) bnA().get(yo);
            if (linkedList != null && linkedList.size() > 0) {
                Iterator it = linkedList.iterator();
                while (it.hasNext()) {
                    ((C0573Hw) it.next()).mo2697a(this);
                }
                this.dRq = true;
            }
        }
    }

    @C0064Am(aul = "179c3376991f3cf55decb3d2c286cac6", aum = 0)
    /* renamed from: b */
    private boolean m8276b(C1216Rw rw, float f) {
        mo4611hh(f);
        if (!this.dRq) {
            mo2800zQ();
        }
        if (this.dRp == null || ((LinkedList) bnA().get(this.dRp)).size() > 0) {
            LinkedList<C0573Hw> b = mo4593b(this.dRp);
            mo3300Ml();
            rw.clear();
            if (b != null) {
                Iterator it = b.iterator();
                while (it.hasNext()) {
                    if (((C0573Hw) it.next()).mo31a(rw, f)) {
                        return true;
                    }
                }
            }
            return false;
        }
        mo8358lY("Issue #3351 - Stepping before " + this + " is started.");
        mo8358lY(" ** " + isEnabled() + " ** " + aYE() + " ** " + isRunning());
        rw.clear();
        return false;
    }

    @C0064Am(aul = "465349825bbacd3325e68e7b5681d38e", aum = 0)
    /* renamed from: I */
    private void m8259I(Ship fAVar) {
        if (bUT() != null && (aYa() instanceof Ship)) {
            ((Ship) aYa()).agj().mo12633P(bUT().agj());
        }
        super.mo4588J(fAVar);
        if (bUT() != null && (aYa() instanceof Ship)) {
            ((Ship) aYa()).agj().mo12632N(bUT().agj());
        }
    }

    @C0064Am(aul = "58401d448096e161bd254a29ee7ae4b7", aum = 0)
    /* renamed from: K */
    private void m8260K(Character acx) {
        if (bUT() != null && (aYa() instanceof Ship)) {
            ((Ship) aYa()).agj().mo12633P(bUT().agj());
        }
        super.mo4589L(acx);
        if (bUT() != null && (aYa() instanceof Ship)) {
            ((Ship) aYa()).agj().mo12632N(bUT().agj());
        }
    }

    @C0064Am(aul = "280a4d579d34b64eb1bf8153689c2a69", aum = 0)
    private boolean bnI() {
        return super.bnJ() && m8288zL().mo9517f((Tuple3d) bUT().getPosition()).lengthSquared() < ((double) m8289zM());
    }

    @C0064Am(aul = "8d93a901245fea34195ed51c2fa0f80d", aum = 0)
    private Set<C1684Yo> bnK() {
        return bnA().keySet();
    }

    @C0064Am(aul = "9d64a3816ae8b6ebf656310afdc36323", aum = 0)
    /* renamed from: gl */
    private C1684Yo m8286gl(String str) {
        for (C1684Yo yo : bnA().keySet()) {
            if (yo.getName().equals(str)) {
                return yo;
            }
        }
        return null;
    }

    @C0064Am(aul = "5f773a3bc223ad78607f92e873fcdbaa", aum = 0)
    /* renamed from: a */
    private LinkedList<C0573Hw> m8268a(C1684Yo yo) {
        return (LinkedList) bnA().get(yo);
    }

    @C0064Am(aul = "f05014b3e20652ce7eefbc017c922f5e", aum = 0)
    /* renamed from: c */
    private void m8279c(C1684Yo yo) {
        bnA().put(yo, new LinkedList());
    }

    @C0064Am(aul = "b01ea9514c8eb5ed0a3b2e859c501ded", aum = 0)
    /* renamed from: a */
    private void m8270a(C1684Yo yo, C0573Hw hw) {
        ((LinkedList) bnA().get(yo)).add(hw);
    }

    @C0064Am(aul = "56ee972db25f6e1b6df46c3000370242", aum = 0)
    /* renamed from: a */
    private void m8272a(C1684Yo yo, LinkedList<C0573Hw> linkedList) {
        bnA().put(yo, linkedList);
        this.dRq = false;
    }

    @C0064Am(aul = "034d693a87c49721136df072f9add5e0", aum = 0)
    /* renamed from: a */
    private void m8269a(C1684Yo yo, int i, C0573Hw hw) {
        ((LinkedList) bnA().get(yo)).add(i, hw);
    }

    @C0064Am(aul = "5e849b33a34cd859010c6fad70b14109", aum = 0)
    /* renamed from: c */
    private boolean m8280c(C1684Yo yo, C0573Hw hw) {
        return ((LinkedList) bnA().get(yo)).remove(hw);
    }

    @C0064Am(aul = "bc437b810a70487fb4bd3a7c3f9fbd71", aum = 0)
    /* renamed from: a */
    private C0573Hw m8267a(C1684Yo yo, int i) {
        return (C0573Hw) ((LinkedList) bnA().get(yo)).remove(i);
    }

    @C0064Am(aul = "1beb8526619a4b856d26bfecf42bd2fa", aum = 0)
    /* renamed from: c */
    private C0573Hw m8277c(C1684Yo yo, int i, C0573Hw hw) {
        return (C0573Hw) ((LinkedList) bnA().get(yo)).set(i, hw);
    }

    @C0064Am(aul = "c12a3e19163875ea77d286a98cff2441", aum = 0)
    /* renamed from: e */
    private void m8281e(C1684Yo yo) {
        bnA().put(yo, new LinkedList());
        this.dRq = false;
    }

    @C0064Am(aul = "e5d67ba028a0d280396a4a1a32eba3f4", aum = 0)
    /* renamed from: a */
    private void m8271a(C1684Yo yo, String str) {
        LinkedList linkedList = (LinkedList) bnD().get(yo);
        if (linkedList == null) {
            linkedList = new LinkedList();
        }
        linkedList.add(str);
        bnD().put(yo, linkedList);
    }

    @C0064Am(aul = "e30c554aac223c78b464b5ba4e3b9067", aum = 0)
    /* renamed from: g */
    private void m8284g(Vec3d ajr) {
        m8282f(ajr);
    }

    @C0064Am(aul = "9471416a483990fd3c1a2e9edb64e42c", aum = 0)
    /* renamed from: aA */
    private void m8273aA(float f) {
        m8274az(f * f);
    }

    @C0064Am(aul = "84248d7e3f5c86a879071aa56f29f965", aum = 0)
    /* renamed from: zN */
    private Vec3d m8290zN() {
        return m8288zL();
    }

    @C0064Am(aul = "5da6b3d50b2f4cef6c8b91fc01fac93d", aum = 0)
    private C1684Yo bnM() {
        return this.dRp;
    }

    @C0064Am(aul = "cbc0dd27e1eb87f8e1dbb50b5dcdb970", aum = 0)
    /* renamed from: g */
    private void m8283g(C1684Yo yo) {
        this.dRp = yo;
    }

    @C0064Am(aul = "d6cca306edf996c9af7da365a13828b1", aum = 0)
    /* renamed from: Zd */
    private Vec3d m8266Zd() {
        return aYa().getPosition();
    }
}
