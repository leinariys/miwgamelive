package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2641hu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aBl  reason: case insensitive filesystem */
/* compiled from: a */
public class ScriptableAIControllerType extends AIControllerType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f2440dN = null;
    public static final C5663aRz dRg = null;
    public static final C2491fm eOX = null;
    public static final C2491fm eOY = null;
    public static final C2491fm hhe = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5fa3a998929922a81517111b0112010e", aum = 0)

    /* renamed from: UA */
    private static String f2439UA;

    static {
        m13056V();
    }

    public ScriptableAIControllerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScriptableAIControllerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13056V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIControllerType._m_fieldCount + 1;
        _m_methodCount = AIControllerType._m_methodCount + 4;
        int i = AIControllerType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ScriptableAIControllerType.class, "5fa3a998929922a81517111b0112010e", i);
        dRg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_fields, (Object[]) _m_fields);
        int i3 = AIControllerType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(ScriptableAIControllerType.class, "a744202f44885c457932e73bc012be23", i3);
        eOX = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ScriptableAIControllerType.class, "43a0a877c1e1fc61eb0556f6f9ff9472", i4);
        eOY = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ScriptableAIControllerType.class, "58364416b9576a5b775d54aa641845be", i5);
        hhe = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ScriptableAIControllerType.class, "e475343ad00e80416b25a67489922f64", i6);
        f2440dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScriptableAIControllerType.class, C2641hu.class, _m_fields, _m_methods);
    }

    private String bnz() {
        return (String) bFf().mo5608dq().mo3214p(dRg);
    }

    /* renamed from: gk */
    private void m13058gk(String str) {
        bFf().mo5608dq().mo3197f(dRg, str);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2641hu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIControllerType._m_methodCount) {
            case 0:
                return bIR();
            case 1:
                m13059hL((String) args[0]);
                return null;
            case 2:
                return cIX();
            case 3:
                return m13057aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2440dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2440dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2440dN, new Object[0]));
                break;
        }
        return m13057aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Script")
    public String bIS() {
        switch (bFf().mo6893i(eOX)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, eOX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eOX, new Object[0]));
                break;
        }
        return bIR();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public ScriptableAIController cIY() {
        switch (bFf().mo6893i(hhe)) {
            case 0:
                return null;
            case 2:
                return (ScriptableAIController) bFf().mo5606d(new aCE(this, hhe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hhe, new Object[0]));
                break;
        }
        return cIX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Script")
    /* renamed from: hM */
    public void mo7969hM(String str) {
        switch (bFf().mo6893i(eOY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eOY, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eOY, new Object[]{str}));
                break;
        }
        m13059hL(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m13058gk("");
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Script")
    @C0064Am(aul = "a744202f44885c457932e73bc012be23", aum = 0)
    private String bIR() {
        return bnz();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Script")
    @C0064Am(aul = "43a0a877c1e1fc61eb0556f6f9ff9472", aum = 0)
    /* renamed from: hL */
    private void m13059hL(String str) {
        m13058gk(str);
    }

    @C0064Am(aul = "58364416b9576a5b775d54aa641845be", aum = 0)
    private ScriptableAIController cIX() {
        return (ScriptableAIController) mo745aU();
    }

    @C0064Am(aul = "e475343ad00e80416b25a67489922f64", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m13057aT() {
        T t = (ScriptableAIController) bFf().mo6865M(ScriptableAIController.class);
        t.mo4590a(this);
        return t;
    }
}
