package game.script.ai.npc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.ObjectId;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.Character;
import game.script.Controller;
import game.script.faction.Faction;
import game.script.item.Weapon;
import game.script.missile.MissileWeapon;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.simulation.Space;
import game.script.space.Node;
import game.script.template.BaseTaikodomContent;
import logic.abb.C0573Hw;
import logic.abb.C0870Mc;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.mbean.C6937awc;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import javax.vecmath.Vector3f;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C2712iu(mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
@C6485anp
/* renamed from: a.Jy */
/* compiled from: a */
public abstract class AIController extends Controller implements C1616Xf, C6078afy, C6222aim {
    public static final int AI_RADAR_RADIUS = 4000;
    /* renamed from: _f_addToSimulation_0020_0028Ltaikodom_002fgame_002fscript_002fsimulation_002fSpace_003bJ_0029V */
    public static final C2491fm f895x430f8e5 = null;
    /* renamed from: _f_applyUsingDesiredVelocity_0020_0028FLtaikodom_002fgame_002fscript_002fPawn_003bLcom_002fhoplon_002fgeometry_002fVec3f_003bFFIF_0029V */
    public static final C2491fm f896x5a73435d = null;
    /* renamed from: _f_applyUsingSetState_0020_0028FLtaikodom_002fgame_002fscript_002fPawn_003bLcom_002fhoplon_002fgeometry_002fVec3f_003bF_0029V */
    public static final C2491fm f897xe26df43a = null;
    /* renamed from: _f_bGroup_0020_0028_005bLtaikodom_002finfra_002fscript_002fai_002fbehaviours_002fBehaviour_003b_0029Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fBehaviour_003b */
    public static final C2491fm f898xc4967e71 = null;
    public static final C2491fm _f_changeAction_0020_0028_0029V = null;
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f899xdce2e729 = null;
    /* renamed from: _f_checkVec3f_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003bLjava_002flang_002fString_003b_0029Z */
    public static final C2491fm f900xebad5167 = null;
    public static final C2491fm _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V = null;
    public static final C2491fm _f_configureBehaviours_0020_0028_0029V = null;
    /* renamed from: _f_convertForceToDesiredVelocity_0020_0028FLtaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003b_0029Lcom_002fhoplon_002fgeometry_002fVec3f_003b */
    public static final C2491fm f901x1ef544f6 = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C5663aRz _f_enabled = null;
    /* renamed from: _f_fillShipCommand_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bF_0029Z */
    public static final C2491fm f902xbc82b75d = null;
    public static final C2491fm _f_forgetNearbyObjects_0020_0028_0029V = null;
    /* renamed from: _f_getBehaviour_0020_0028III_0029Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fBehaviour_003b */
    public static final C2491fm f903x2081525f = null;
    /* renamed from: _f_getBehaviour_0020_0028II_0029Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fBehaviour_003b */
    public static final C2491fm f904x1a89aa04 = null;
    public static final C2491fm _f_getCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_getInitialAction_0020_0028_0029I = null;
    public static final C2491fm _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = null;
    /* renamed from: _f_getOffloaders_0020_0028_0029Ljava_002futil_002fCollection_003b */
    public static final C2491fm f905x99c43db3 = null;
    /* renamed from: _f_getPilot_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Ltaikodom_002fgame_002fscript_002fCharacter_003b */
    public static final C2491fm f906x4e8affd0 = null;
    public static final C2491fm _f_getPitchGain_0020_0028_0029F = null;
    /* renamed from: _f_getShipType_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShipType_003b */
    public static final C2491fm f907xe514b437 = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f908x851d656b = null;
    public static final C2491fm _f_getSpeedGain_0020_0028_0029F = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    /* renamed from: _f_getType_0020_0028_0029Ltaikodom_002fgame_002fscript_002fai_002fnpc_002fAIControllerType_003b */
    public static final C2491fm f909xed6e425 = null;
    /* renamed from: _f_getWeaponWithAmmo_0020_0028_0029Ltaikodom_002fgame_002fscript_002fitem_002fWeapon_003b */
    public static final C2491fm f910x9a783f6e = null;
    public static final C2491fm _f_getYawGain_0020_0028_0029F = null;
    public static final C2491fm _f_hasFinished_0020_0028_0029Z = null;
    public static final C2491fm _f_isEnabled_0020_0028_0029Z = null;
    /* renamed from: _f_isEnemy_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f911x72b3cf67 = null;
    /* renamed from: _f_isInvalid_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3f_003b_0029Z */
    public static final C2491fm f912x30bf686c = null;
    public static final C2491fm _f_isOffloadInited_0020_0028_0029Z = null;
    public static final C2491fm _f_isRunning_0020_0028_0029Z = null;
    /* renamed from: _f_noCollision_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f913xe5145ada = null;
    public static final C2491fm _f_offloadInit_0020_0028_0029V = null;
    public static final C2491fm _f_offloadedResetInitFlag_0020_0028_0029V = null;
    public static final C2491fm _f_offloadedStart_0020_0028_0029V = null;
    public static final C2491fm _f_onPawnExplode_0020_0028_0029V = null;
    public static final C2491fm _f_removeFromSimulation_0020_0028_0029V = null;
    /* renamed from: _f_setAction_0020_0028I_005bLtaikodom_002finfra_002fscript_002fai_002fbehaviours_002fBehaviour_003b_0029V */
    public static final C2491fm f914x82340bc8 = null;
    public static final C2491fm _f_setCurrentAction_0020_0028I_0029V = null;
    public static final C2491fm _f_setInitialAction_0020_0028I_0029V = null;
    public static final C2491fm _f_setNumberOfActions_0020_0028I_0029V = null;
    public static final C2491fm _f_setPitchGain_0020_0028F_0029V = null;
    public static final C2491fm _f_setSpeedGain_0020_0028F_0029V = null;
    public static final C2491fm _f_setYawGain_0020_0028F_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final C2491fm _f_startStepping_0020_0028_0029V = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_stopStepping_0020_0028_0029V = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    /* renamed from: _f_updateCruiserCommands_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bLtaikodom_002fgame_002fscript_002fship_002fShip_003b_0029I */
    public static final C2491fm f915xbc23e737 = null;
    /* renamed from: _f_updateFireCommands_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bLtaikodom_002fgame_002fscript_002fship_002fShip_003b_0029V */
    public static final C2491fm f916x9bad3dc9 = null;
    /* renamed from: _f_updateFireCommands_0020_0028Ltaikodom_002finfra_002fscript_002fai_002fbehaviours_002fAIShipCommand_003bLtaikodom_002fgame_002fscript_002fturret_002fTurret_003b_0029V */
    public static final C2491fm f917x7436c109 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    private static final int FIRST_STEP_STARTING_CRUISE_SPEED = 2;
    private static final int IN_CRUISE_SPEED = 1;
    private static final int IN_NORMAL_SPEED = 0;
    private static final int STARTING_CRUISE_SPEED = 3;
    private static final Log logger = LogPrinter.setClass(AIController.class);
    public static C6494any ___iScriptClass;
    public static transient int nanFloodControl = 0;
    @C0064Am(aul = "a3ea55cccc8e169f0f68073350ccda41", aum = 0)
    @C5256aCi
    private static boolean enabled;

    static {
        m6084V();
    }

    public transient aNU pitchController;
    public transient aNU speedController;
    public transient aNU yawController;
    private transient float PITCH_GAIN;
    private transient float SPEED_GAIN;
    private transient float YAW_GAIN;
    private transient C0573Hw[][] actions;
    private transient int currentAction;
    private transient StringBuffer debugStr;
    private transient int initialAction;
    private transient List<Actor> nearbyObjects;
    @ClientOnly
    private transient boolean offloadInitied;

    public AIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AIController(C5540aNg ang) {
        super(ang);
    }

    public AIController(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m6084V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Controller._m_fieldCount + 1;
        _m_methodCount = Controller._m_methodCount + 57;
        int i = Controller._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(AIController.class, "a3ea55cccc8e169f0f68073350ccda41", i);
        _f_enabled = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Controller._m_fields, (Object[]) _m_fields);
        int i3 = Controller._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 57)];
        C2491fm a = C4105zY.m41624a(AIController.class, "12a598868836566f1fd09f95e46ad3d3", i3);
        _f_step_0020_0028F_0029V = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(AIController.class, "1392add7a648fb22d2877c56318ca9c0", i4);
        f899xdce2e729 = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(AIController.class, "95a2ae9b377178c7e3160e39150cb1cc", i5);
        f900xebad5167 = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(AIController.class, "68b3d1203355c22130e46dbb2ae2ed4a", i6);
        f912x30bf686c = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(AIController.class, "e6ccbb9d6893a0e6cc85235e51de9aee", i7);
        _f_configureBehaviours_0020_0028_0029V = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(AIController.class, "9c9b0ecc600b822392fd3b2a5b8812d3", i8);
        f901x1ef544f6 = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(AIController.class, "ed198bb7f100f88e62698e421b53dc2e", i9);
        f897xe26df43a = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(AIController.class, "8b7d665c692940128888499addbfc220", i10);
        f896x5a73435d = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(AIController.class, "ad564219a450622e8a5fd232f38aad9e", i11);
        f915xbc23e737 = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(AIController.class, "4afc2650567d3477030e9a52b2d0250a", i12);
        f916x9bad3dc9 = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(AIController.class, "ae74be47231f40841c1151f8776302df", i13);
        f917x7436c109 = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(AIController.class, "d5b3fc4f7bc2e0fe52533eb1df29549f", i14);
        _f_changeAction_0020_0028_0029V = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(AIController.class, "d7caee63067f956d1e595c2354e2d277", i15);
        _f_changeCurrentAction_0020_0028_0029I = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        C2491fm a14 = C4105zY.m41624a(AIController.class, "8e448e6a27cb9a6609bb0315c71db240", i16);
        f902xbc82b75d = a14;
        fmVarArr[i16] = a14;
        int i17 = i16 + 1;
        C2491fm a15 = C4105zY.m41624a(AIController.class, "913723e2b40a5bc4cf6f9c16ff8d3ed3", i17);
        _f_forgetNearbyObjects_0020_0028_0029V = a15;
        fmVarArr[i17] = a15;
        int i18 = i17 + 1;
        C2491fm a16 = C4105zY.m41624a(AIController.class, "c58c15f254eb5ec96765dc1b65a001ba", i18);
        _f_getCurrentAction_0020_0028_0029I = a16;
        fmVarArr[i18] = a16;
        int i19 = i18 + 1;
        C2491fm a17 = C4105zY.m41624a(AIController.class, "5bacad016db11d611dd928aa58cbebb6", i19);
        _f_setCurrentAction_0020_0028I_0029V = a17;
        fmVarArr[i19] = a17;
        int i20 = i19 + 1;
        C2491fm a18 = C4105zY.m41624a(AIController.class, "3e0b461e49e8450b7e45a36f5d4f204e", i20);
        _f_getInitialAction_0020_0028_0029I = a18;
        fmVarArr[i20] = a18;
        int i21 = i20 + 1;
        C2491fm a19 = C4105zY.m41624a(AIController.class, "7ca2c1c38964c41b9fefd76ae70a0c8d", i21);
        _f_setInitialAction_0020_0028I_0029V = a19;
        fmVarArr[i21] = a19;
        int i22 = i21 + 1;
        C2491fm a20 = C4105zY.m41624a(AIController.class, "a1dfc562eafb55f946d2ee68ea2c5a85", i22);
        _f_isEnabled_0020_0028_0029Z = a20;
        fmVarArr[i22] = a20;
        int i23 = i22 + 1;
        C2491fm a21 = C4105zY.m41624a(AIController.class, "0ba1040c6bc1ede14fce2a1e7488aeea", i23);
        _f_setNumberOfActions_0020_0028I_0029V = a21;
        fmVarArr[i23] = a21;
        int i24 = i23 + 1;
        C2491fm a22 = C4105zY.m41624a(AIController.class, "7eb591d15745523e0abd55f1f162c6e6", i24);
        f914x82340bc8 = a22;
        fmVarArr[i24] = a22;
        int i25 = i24 + 1;
        C2491fm a23 = C4105zY.m41624a(AIController.class, "4007ca3a804d3048bfa567bdff72577f", i25);
        f898xc4967e71 = a23;
        fmVarArr[i25] = a23;
        int i26 = i25 + 1;
        C2491fm a24 = C4105zY.m41624a(AIController.class, "3354966384e88adec6cdc92b6d7fb289", i26);
        f904x1a89aa04 = a24;
        fmVarArr[i26] = a24;
        int i27 = i26 + 1;
        C2491fm a25 = C4105zY.m41624a(AIController.class, "372c96032bf8e821f72deb29703b4ac2", i27);
        f903x2081525f = a25;
        fmVarArr[i27] = a25;
        int i28 = i27 + 1;
        C2491fm a26 = C4105zY.m41624a(AIController.class, "b3db05ae23fa098b0076b7f45732773d", i28);
        _f_start_0020_0028_0029V = a26;
        fmVarArr[i28] = a26;
        int i29 = i28 + 1;
        C2491fm a27 = C4105zY.m41624a(AIController.class, "2f3f5e10e6be5454554bce3da82c3ade", i29);
        _f_offloadedResetInitFlag_0020_0028_0029V = a27;
        fmVarArr[i29] = a27;
        int i30 = i29 + 1;
        C2491fm a28 = C4105zY.m41624a(AIController.class, "27dd9c6e1046dfc7d3322efb37c814fa", i30);
        _f_offloadedStart_0020_0028_0029V = a28;
        fmVarArr[i30] = a28;
        int i31 = i30 + 1;
        C2491fm a29 = C4105zY.m41624a(AIController.class, "aa0ad1a61751b6aeb0c96dcc0e0a8a70", i31);
        _f_startStepping_0020_0028_0029V = a29;
        fmVarArr[i31] = a29;
        int i32 = i31 + 1;
        C2491fm a30 = C4105zY.m41624a(AIController.class, "b5f6749fa0d8d1761b34d3e99331a84f", i32);
        _f_stopStepping_0020_0028_0029V = a30;
        fmVarArr[i32] = a30;
        int i33 = i32 + 1;
        C2491fm a31 = C4105zY.m41624a(AIController.class, "5cdf6bc88483a12f24f32bf2cad72bf2", i33);
        _f_stop_0020_0028_0029V = a31;
        fmVarArr[i33] = a31;
        int i34 = i33 + 1;
        C2491fm a32 = C4105zY.m41624a(AIController.class, "a7bd72ee8a933bd698b87e2df602cc45", i34);
        _f_setupBehaviours_0020_0028_0029V = a32;
        fmVarArr[i34] = a32;
        int i35 = i34 + 1;
        C2491fm a33 = C4105zY.m41624a(AIController.class, "f7d7cd3aac86616911028b095b9170c6", i35);
        _f_isOffloadInited_0020_0028_0029Z = a33;
        fmVarArr[i35] = a33;
        int i36 = i35 + 1;
        C2491fm a34 = C4105zY.m41624a(AIController.class, "a9b5305c5878b142f46020301fbf4b78", i36);
        _f_offloadInit_0020_0028_0029V = a34;
        fmVarArr[i36] = a34;
        int i37 = i36 + 1;
        C2491fm a35 = C4105zY.m41624a(AIController.class, "7303ff72315fb37470445cb45ba7943c", i37);
        _f_onPawnExplode_0020_0028_0029V = a35;
        fmVarArr[i37] = a35;
        int i38 = i37 + 1;
        C2491fm a36 = C4105zY.m41624a(AIController.class, "133deede7aed00d96a33ed6c9fc7565c", i38);
        _f_isRunning_0020_0028_0029Z = a36;
        fmVarArr[i38] = a36;
        int i39 = i38 + 1;
        C2491fm a37 = C4105zY.m41624a(AIController.class, "b355044a511db66cff148d5ddc45f3eb", i39);
        _f_hasFinished_0020_0028_0029Z = a37;
        fmVarArr[i39] = a37;
        int i40 = i39 + 1;
        C2491fm a38 = C4105zY.m41624a(AIController.class, "60309ea2dccbb220df5b89cc14894bce", i40);
        f908x851d656b = a38;
        fmVarArr[i40] = a38;
        int i41 = i40 + 1;
        C2491fm a39 = C4105zY.m41624a(AIController.class, "f35b1b104da947f4c48005d073c7e46d", i41);
        f907xe514b437 = a39;
        fmVarArr[i41] = a39;
        int i42 = i41 + 1;
        C2491fm a40 = C4105zY.m41624a(AIController.class, "5272c6133eabde5b17305d018bea9cf1", i42);
        _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b = a40;
        fmVarArr[i42] = a40;
        int i43 = i42 + 1;
        C2491fm a41 = C4105zY.m41624a(AIController.class, "1d5d004a947be7c09ae78e5ec509b3a4", i43);
        f913xe5145ada = a41;
        fmVarArr[i43] = a41;
        int i44 = i43 + 1;
        C2491fm a42 = C4105zY.m41624a(AIController.class, "d22fa8df6b96962858f2cd2d02ae2da4", i44);
        f910x9a783f6e = a42;
        fmVarArr[i44] = a42;
        int i45 = i44 + 1;
        C2491fm a43 = C4105zY.m41624a(AIController.class, "dea7e7dd7ff390900c640aff8254c64d", i45);
        f906x4e8affd0 = a43;
        fmVarArr[i45] = a43;
        int i46 = i45 + 1;
        C2491fm a44 = C4105zY.m41624a(AIController.class, "c76bc647a5b9552d7e1417ab20b784a0", i46);
        f911x72b3cf67 = a44;
        fmVarArr[i46] = a44;
        int i47 = i46 + 1;
        C2491fm a45 = C4105zY.m41624a(AIController.class, "15729dc6678f88b095100b702337e82c", i47);
        _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V = a45;
        fmVarArr[i47] = a45;
        int i48 = i47 + 1;
        C2491fm a46 = C4105zY.m41624a(AIController.class, "920425fc2c068966fe1e4f1f1f744a32", i48);
        _f_dispose_0020_0028_0029V = a46;
        fmVarArr[i48] = a46;
        int i49 = i48 + 1;
        C2491fm a47 = C4105zY.m41624a(AIController.class, "9cddd78b076a9d88384e3e23f1b7b414", i49);
        _f_getPitchGain_0020_0028_0029F = a47;
        fmVarArr[i49] = a47;
        int i50 = i49 + 1;
        C2491fm a48 = C4105zY.m41624a(AIController.class, "5f5e2600f59f58a61d04cfca7c761f4a", i50);
        _f_setPitchGain_0020_0028F_0029V = a48;
        fmVarArr[i50] = a48;
        int i51 = i50 + 1;
        C2491fm a49 = C4105zY.m41624a(AIController.class, "e0cbd8427863fea1fcf78b9e35a4f08b", i51);
        _f_getSpeedGain_0020_0028_0029F = a49;
        fmVarArr[i51] = a49;
        int i52 = i51 + 1;
        C2491fm a50 = C4105zY.m41624a(AIController.class, "fe60ba69a8e6ea07c8318be9d1383a7c", i52);
        _f_setSpeedGain_0020_0028F_0029V = a50;
        fmVarArr[i52] = a50;
        int i53 = i52 + 1;
        C2491fm a51 = C4105zY.m41624a(AIController.class, "e100302e14993e7678a417b7f0e0057a", i53);
        _f_getYawGain_0020_0028_0029F = a51;
        fmVarArr[i53] = a51;
        int i54 = i53 + 1;
        C2491fm a52 = C4105zY.m41624a(AIController.class, "c0d58fb717ef9d35543c4f0ad9c2b873", i54);
        _f_setYawGain_0020_0028F_0029V = a52;
        fmVarArr[i54] = a52;
        int i55 = i54 + 1;
        C2491fm a53 = C4105zY.m41624a(AIController.class, "45b9b0385f65ec6560c89ad5ed331568", i55);
        f909xed6e425 = a53;
        fmVarArr[i55] = a53;
        int i56 = i55 + 1;
        C2491fm a54 = C4105zY.m41624a(AIController.class, "150f8bc736cef060ea539b5ec0f62796", i56);
        f895x430f8e5 = a54;
        fmVarArr[i56] = a54;
        int i57 = i56 + 1;
        C2491fm a55 = C4105zY.m41624a(AIController.class, "db3ebc343f69473dbff06f030f61bec8", i57);
        _f_removeFromSimulation_0020_0028_0029V = a55;
        fmVarArr[i57] = a55;
        int i58 = i57 + 1;
        C2491fm a56 = C4105zY.m41624a(AIController.class, "f5219a11e9820355a17c12edbe7b7734", i58);
        f905x99c43db3 = a56;
        fmVarArr[i58] = a56;
        int i59 = i58 + 1;
        C2491fm a57 = C4105zY.m41624a(AIController.class, "563329c9476563af702c3ff74eaba399", i59);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a57;
        fmVarArr[i59] = a57;
        int i60 = i59 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Controller._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AIController.class, C6937awc.class, _m_fields, _m_methods);
    }

    /* renamed from: J */
    private void m6079J(boolean z) {
        bFf().mo5608dq().mo3153a(_f_enabled, z);
    }

    @Deprecated
    private void aYA() {
        switch (bFf().mo6893i(_f_startStepping_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_startStepping_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_startStepping_0020_0028_0029V, new Object[0]));
                break;
        }
        aYz();
    }

    @C4034yP
    @C2499fr
    @C1253SX
    @Deprecated
    private void aYC() {
        switch (bFf().mo6893i(_f_stopStepping_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stopStepping_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stopStepping_0020_0028_0029V, new Object[0]));
                break;
        }
        aYB();
    }

    @C0064Am(aul = "d22fa8df6b96962858f2cd2d02ae2da4", aum = 0)
    @C5566aOg
    private Weapon aYI() {
        throw new aWi(new aCE(this, f910x9a783f6e, new Object[0]));
    }

    @C4034yP
    @C0064Am(aul = "7303ff72315fb37470445cb45ba7943c", aum = 0)
    @C5566aOg
    private void aYf() {
        throw new aWi(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
    }

    @C4034yP
    @C2499fr
    @C1253SX
    private void aYw() {
        switch (bFf().mo6893i(_f_offloadedResetInitFlag_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_offloadedResetInitFlag_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_offloadedResetInitFlag_0020_0028_0029V, new Object[0]));
                break;
        }
        aYv();
    }

    @C1253SX
    private void aYy() {
        switch (bFf().mo6893i(_f_offloadedStart_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_offloadedStart_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_offloadedStart_0020_0028_0029V, new Object[0]));
                break;
        }
        aYx();
    }

    /* renamed from: ax */
    private boolean m6096ax(Actor cr) {
        switch (bFf().mo6893i(f913xe5145ada)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f913xe5145ada, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f913xe5145ada, new Object[]{cr}));
                break;
        }
        return m6095aw(cr);
    }

    /* renamed from: b */
    private int m6099b(C1216Rw rw, Ship fAVar) {
        switch (bFf().mo6893i(f915xbc23e737)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f915xbc23e737, new Object[]{rw, fAVar}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f915xbc23e737, new Object[]{rw, fAVar}));
                break;
        }
        return m6085a(rw, fAVar);
    }

    /* renamed from: b */
    private Vec3f m6100b(float f, C1216Rw rw) {
        switch (bFf().mo6893i(f901x1ef544f6)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, f901x1ef544f6, new Object[]{new Float(f), rw}));
            case 3:
                bFf().mo5606d(new aCE(this, f901x1ef544f6, new Object[]{new Float(f), rw}));
                break;
        }
        return m6087a(f, rw);
    }

    /* renamed from: b */
    private void m6101b(float f, Pawn avi, Vec3f vec3f, float f2) {
        switch (bFf().mo6893i(f897xe26df43a)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f897xe26df43a, new Object[]{new Float(f), avi, vec3f, new Float(f2)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f897xe26df43a, new Object[]{new Float(f), avi, vec3f, new Float(f2)}));
                break;
        }
        m6088a(f, avi, vec3f, f2);
    }

    /* renamed from: b */
    private void m6102b(float f, Pawn avi, Vec3f vec3f, float f2, float f3, int i, float f4) {
        switch (bFf().mo6893i(f896x5a73435d)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f896x5a73435d, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Integer(i), new Float(f4)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f896x5a73435d, new Object[]{new Float(f), avi, vec3f, new Float(f2), new Float(f3), new Integer(i), new Float(f4)}));
                break;
        }
        m6089a(f, avi, vec3f, f2, f3, i, f4);
    }

    /* renamed from: b */
    private void m6103b(C1216Rw rw, Turret jq) {
        switch (bFf().mo6893i(f917x7436c109)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f917x7436c109, new Object[]{rw, jq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f917x7436c109, new Object[]{rw, jq}));
                break;
        }
        m6091a(rw, jq);
    }

    /* renamed from: b */
    private boolean m6105b(Vec3d ajr, String str) {
        switch (bFf().mo6893i(f899xdce2e729)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f899xdce2e729, new Object[]{ajr, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f899xdce2e729, new Object[]{ajr, str}));
                break;
        }
        return m6093a(ajr, str);
    }

    /* renamed from: b */
    private boolean m6106b(Vec3f vec3f, String str) {
        switch (bFf().mo6893i(f900xebad5167)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f900xebad5167, new Object[]{vec3f, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f900xebad5167, new Object[]{vec3f, str}));
                break;
        }
        return m6094a(vec3f, str);
    }

    /* renamed from: d */
    private void m6108d(C1216Rw rw, Ship fAVar) {
        switch (bFf().mo6893i(f916x9bad3dc9)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f916x9bad3dc9, new Object[]{rw, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f916x9bad3dc9, new Object[]{rw, fAVar}));
                break;
        }
        m6107c(rw, fAVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "5cdf6bc88483a12f24f32bf2cad72bf2", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: qO */
    private void m6119qO() {
        throw new aWi(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "563329c9476563af702c3ff74eaba399", aum = 0)
    /* renamed from: qW */
    private Object m6120qW() {
        return mo3334zY();
    }

    /* renamed from: rm */
    private boolean m6122rm() {
        return bFf().mo5608dq().mo3201h(_f_enabled);
    }

    /* renamed from: s */
    private boolean m6124s(Vec3f vec3f) {
        switch (bFf().mo6893i(f912x30bf686c)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f912x30bf686c, new Object[]{vec3f}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f912x30bf686c, new Object[]{vec3f}));
                break;
        }
        return m6121r(vec3f);
    }

    @C0064Am(aul = "a7bd72ee8a933bd698b87e2df602cc45", aum = 0)
    /* renamed from: zP */
    private void m6126zP() {
        throw new aWi(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "d7caee63067f956d1e595c2354e2d277", aum = 0)
    /* renamed from: zT */
    private int m6128zT() {
        throw new aWi(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
    }

    /* renamed from: D */
    public boolean mo3298D(Actor cr) {
        switch (bFf().mo6893i(f911x72b3cf67)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f911x72b3cf67, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f911x72b3cf67, new Object[]{cr}));
                break;
        }
        return m6077C(cr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: J */
    public C0573Hw mo3299J(int i, int i2) {
        switch (bFf().mo6893i(f904x1a89aa04)) {
            case 0:
                return null;
            case 2:
                return (C0573Hw) bFf().mo5606d(new aCE(this, f904x1a89aa04, new Object[]{new Integer(i), new Integer(i2)}));
            case 3:
                bFf().mo5606d(new aCE(this, f904x1a89aa04, new Object[]{new Integer(i), new Integer(i2)}));
                break;
        }
        return m6078I(i, i2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ml */
    public void mo3300Ml() {
        switch (bFf().mo6893i(_f_forgetNearbyObjects_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_forgetNearbyObjects_0020_0028_0029V, new Object[0]));
                break;
        }
        m6080Mk();
    }

    /* renamed from: Mn */
    public List<Actor> mo3301Mn() {
        switch (bFf().mo6893i(_f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getNearbyObjects_0020_0028_0029Ljava_002futil_002fList_003b, new Object[0]));
                break;
        }
        return m6081Mm();
    }

    /* renamed from: Pk */
    public float mo3302Pk() {
        switch (bFf().mo6893i(_f_getSpeedGain_0020_0028_0029F)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, _f_getSpeedGain_0020_0028_0029F, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_getSpeedGain_0020_0028_0029F, new Object[0]));
                break;
        }
        return aYL();
    }

    /* renamed from: Pl */
    public float mo3303Pl() {
        switch (bFf().mo6893i(_f_getYawGain_0020_0028_0029F)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, _f_getYawGain_0020_0028_0029F, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_getYawGain_0020_0028_0029F, new Object[0]));
                break;
        }
        return aYM();
    }

    /* renamed from: Pm */
    public float mo3304Pm() {
        switch (bFf().mo6893i(_f_getPitchGain_0020_0028_0029F)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, _f_getPitchGain_0020_0028_0029F, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_getPitchGain_0020_0028_0029F, new Object[0]));
                break;
        }
        return aYK();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6937awc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Controller._m_methodCount) {
            case 0:
                m6098ay(((Float) args[0]).floatValue());
                return null;
            case 1:
                return new Boolean(m6093a((Vec3d) args[0], (String) args[1]));
            case 2:
                return new Boolean(m6094a((Vec3f) args[0], (String) args[1]));
            case 3:
                return new Boolean(m6121r((Vec3f) args[0]));
            case 4:
                m6127zR();
                return null;
            case 5:
                return m6087a(((Float) args[0]).floatValue(), (C1216Rw) args[1]);
            case 6:
                m6088a(((Float) args[0]).floatValue(), (Pawn) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue());
                return null;
            case 7:
                m6089a(((Float) args[0]).floatValue(), (Pawn) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), ((Float) args[4]).floatValue(), ((Integer) args[5]).intValue(), ((Float) args[6]).floatValue());
                return null;
            case 8:
                return new Integer(m6085a((C1216Rw) args[0], (Ship) args[1]));
            case 9:
                m6107c((C1216Rw) args[0], (Ship) args[1]);
                return null;
            case 10:
                m6091a((C1216Rw) args[0], (Turret) args[1]);
                return null;
            case 11:
                aYp();
                return null;
            case 12:
                return new Integer(m6128zT());
            case 13:
                return new Boolean(m6104b((C1216Rw) args[0], ((Float) args[1]).floatValue()));
            case 14:
                m6080Mk();
                return null;
            case 15:
                return new Integer(aYr());
            case 16:
                m6115kY(((Integer) args[0]).intValue());
                return null;
            case 17:
                return new Integer(aYt());
            case 18:
                m6116la(((Integer) args[0]).intValue());
                return null;
            case 19:
                return new Boolean(m6123rx());
            case 20:
                m6117lc(((Integer) args[0]).intValue());
                return null;
            case 21:
                m6090a(((Integer) args[0]).intValue(), (C0573Hw[]) args[1]);
                return null;
            case 22:
                return m6086a((C0573Hw[]) args[0]);
            case 23:
                return m6078I(((Integer) args[0]).intValue(), ((Integer) args[1]).intValue());
            case 24:
                return m6113g(((Integer) args[0]).intValue(), ((Integer) args[1]).intValue(), ((Integer) args[2]).intValue());
            case 25:
                m6118qN();
                return null;
            case 26:
                aYv();
                return null;
            case 27:
                aYx();
                return null;
            case 28:
                aYz();
                return null;
            case 29:
                aYB();
                return null;
            case 30:
                m6119qO();
                return null;
            case 31:
                m6126zP();
                return null;
            case 32:
                return new Boolean(aYD());
            case 33:
                aYF();
                return null;
            case 34:
                aYf();
                return null;
            case 35:
                return new Boolean(m6083Mp());
            case 36:
                return new Boolean(aYH());
            case 37:
                return m6082Mo();
            case 38:
                return m6125uW();
            case 39:
                return m6081Mm();
            case 40:
                return new Boolean(m6095aw((Actor) args[0]));
            case 41:
                return aYI();
            case 42:
                return m6097ay((Actor) args[0]);
            case 43:
                return new Boolean(m6077C((Actor) args[0]));
            case 44:
                m6092a(((Boolean) args[0]).booleanValue(), (String) args[1]);
                return null;
            case 45:
                m6112fg();
                return null;
            case 46:
                return new Float(aYK());
            case 47:
                m6109fK(((Float) args[0]).floatValue());
                return null;
            case 48:
                return new Float(aYL());
            case 49:
                m6110fL(((Float) args[0]).floatValue());
                return null;
            case 50:
                return new Float(aYM());
            case 51:
                m6111fM(((Float) args[0]).floatValue());
                return null;
            case 52:
                return m6129zX();
            case 53:
                m6114g((Space) args[0], ((Long) args[1]).longValue());
                return null;
            case 54:
                aYN();
                return null;
            case 55:
                return aad();
            case 56:
                return m6120qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo3305a(C1216Rw rw, float f) {
        switch (bFf().mo6893i(f902xbc82b75d)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f902xbc82b75d, new Object[]{rw, new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f902xbc82b75d, new Object[]{rw, new Float(f)}));
                break;
        }
        return m6104b(rw, f);
    }

    /* access modifiers changed from: protected */
    @C1253SX
    public boolean aYE() {
        switch (bFf().mo6893i(_f_isOffloadInited_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isOffloadInited_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isOffloadInited_0020_0028_0029Z, new Object[0]));
                break;
        }
        return aYD();
    }

    @C1253SX
    public void aYG() {
        switch (bFf().mo6893i(_f_offloadInit_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                break;
        }
        aYF();
    }

    @C5566aOg
    public Weapon aYJ() {
        switch (bFf().mo6893i(f910x9a783f6e)) {
            case 0:
                return null;
            case 2:
                return (Weapon) bFf().mo5606d(new aCE(this, f910x9a783f6e, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f910x9a783f6e, new Object[0]));
                break;
        }
        return aYI();
    }

    @C1253SX
    public void aYO() {
        switch (bFf().mo6893i(_f_removeFromSimulation_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_removeFromSimulation_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_removeFromSimulation_0020_0028_0029V, new Object[0]));
                break;
        }
        aYN();
    }

    @C4034yP
    @C5566aOg
    public void aYg() {
        switch (bFf().mo6893i(_f_onPawnExplode_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                break;
        }
        aYf();
    }

    /* access modifiers changed from: protected */
    public void aYq() {
        switch (bFf().mo6893i(_f_changeAction_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_changeAction_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeAction_0020_0028_0029V, new Object[0]));
                break;
        }
        aYp();
    }

    /* access modifiers changed from: protected */
    public int aYs() {
        switch (bFf().mo6893i(_f_getCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_getCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_getCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return aYr();
    }

    /* access modifiers changed from: protected */
    public int aYu() {
        switch (bFf().mo6893i(_f_getInitialAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_getInitialAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_getInitialAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return aYt();
    }

    public Collection<aDR> aae() {
        switch (bFf().mo6893i(f905x99c43db3)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, f905x99c43db3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f905x99c43db3, new Object[0]));
                break;
        }
        return aad();
    }

    public boolean akd() {
        switch (bFf().mo6893i(_f_hasFinished_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasFinished_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasFinished_0020_0028_0029Z, new Object[0]));
                break;
        }
        return aYH();
    }

    /* renamed from: al */
    public Ship mo3314al() {
        switch (bFf().mo6893i(f908x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f908x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f908x851d656b, new Object[0]));
                break;
        }
        return m6082Mo();
    }

    /* access modifiers changed from: protected */
    /* renamed from: az */
    public Character mo3315az(Actor cr) {
        switch (bFf().mo6893i(f906x4e8affd0)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, f906x4e8affd0, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, f906x4e8affd0, new Object[]{cr}));
                break;
        }
        return m6097ay(cr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public C0573Hw mo3316b(C0573Hw... hwArr) {
        switch (bFf().mo6893i(f898xc4967e71)) {
            case 0:
                return null;
            case 2:
                return (C0573Hw) bFf().mo5606d(new aCE(this, f898xc4967e71, new Object[]{hwArr}));
            case 3:
                bFf().mo5606d(new aCE(this, f898xc4967e71, new Object[]{hwArr}));
                break;
        }
        return m6086a(hwArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo3317b(int i, C0573Hw... hwArr) {
        switch (bFf().mo6893i(f914x82340bc8)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f914x82340bc8, new Object[]{new Integer(i), hwArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f914x82340bc8, new Object[]{new Integer(i), hwArr}));
                break;
        }
        m6090a(i, hwArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo3318b(boolean z, String str) {
        switch (bFf().mo6893i(_f_check_0020_0028ZLjava_002flang_002fString_003b_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V, new Object[]{new Boolean(z), str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V, new Object[]{new Boolean(z), str}));
                break;
        }
        m6092a(z, str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cp */
    public void mo3319cp(float f) {
        switch (bFf().mo6893i(_f_setSpeedGain_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setSpeedGain_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setSpeedGain_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m6110fL(f);
    }

    /* renamed from: cq */
    public void mo3320cq(float f) {
        switch (bFf().mo6893i(_f_setYawGain_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setYawGain_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setYawGain_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m6111fM(f);
    }

    /* renamed from: cr */
    public void mo3321cr(float f) {
        switch (bFf().mo6893i(_f_setPitchGain_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setPitchGain_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setPitchGain_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m6109fK(f);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m6112fg();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m6120qW();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public C0573Hw mo3322h(int i, int i2, int i3) {
        switch (bFf().mo6893i(f903x2081525f)) {
            case 0:
                return null;
            case 2:
                return (C0573Hw) bFf().mo5606d(new aCE(this, f903x2081525f, new Object[]{new Integer(i), new Integer(i2), new Integer(i3)}));
            case 3:
                bFf().mo5606d(new aCE(this, f903x2081525f, new Object[]{new Integer(i), new Integer(i2), new Integer(i3)}));
                break;
        }
        return m6113g(i, i2, i3);
    }

    @C1253SX
    /* renamed from: h */
    public void mo3323h(Space ea, long j) {
        switch (bFf().mo6893i(f895x430f8e5)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f895x430f8e5, new Object[]{ea, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f895x430f8e5, new Object[]{ea, new Long(j)}));
                break;
        }
        m6114g(ea, j);
    }

    /* access modifiers changed from: protected */
    public boolean isEnabled() {
        switch (bFf().mo6893i(_f_isEnabled_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m6123rx();
    }

    public boolean isRunning() {
        switch (bFf().mo6893i(_f_isRunning_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m6083Mp();
    }

    /* access modifiers changed from: protected */
    /* renamed from: kZ */
    public void mo3326kZ(int i) {
        switch (bFf().mo6893i(_f_setCurrentAction_0020_0028I_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setCurrentAction_0020_0028I_0029V, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setCurrentAction_0020_0028I_0029V, new Object[]{new Integer(i)}));
                break;
        }
        m6115kY(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: lb */
    public void mo3327lb(int i) {
        switch (bFf().mo6893i(_f_setInitialAction_0020_0028I_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setInitialAction_0020_0028I_0029V, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setInitialAction_0020_0028I_0029V, new Object[]{new Integer(i)}));
                break;
        }
        m6116la(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ld */
    public void mo3328ld(int i) {
        switch (bFf().mo6893i(_f_setNumberOfActions_0020_0028I_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setNumberOfActions_0020_0028I_0029V, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setNumberOfActions_0020_0028I_0029V, new Object[]{new Integer(i)}));
                break;
        }
        m6117lc(i);
    }

    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m6118qN();
    }

    @C1253SX
    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m6098ay(f);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m6119qO();
    }

    /* renamed from: uX */
    public ShipType mo3332uX() {
        switch (bFf().mo6893i(f907xe514b437)) {
            case 0:
                return null;
            case 2:
                return (ShipType) bFf().mo5606d(new aCE(this, f907xe514b437, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f907xe514b437, new Object[0]));
                break;
        }
        return m6125uW();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m6126zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zS */
    public void mo3333zS() {
        switch (bFf().mo6893i(_f_configureBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_configureBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_configureBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m6127zR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m6128zT();
    }

    /* renamed from: zY */
    public AIControllerType mo3334zY() {
        switch (bFf().mo6893i(f909xed6e425)) {
            case 0:
                return null;
            case 2:
                return (AIControllerType) bFf().mo5606d(new aCE(this, f909xed6e425, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f909xed6e425, new Object[0]));
                break;
        }
        return m6129zX();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m6079J(false);
        this.currentAction = -1;
        this.offloadInitied = false;
        this.initialAction = -1;
        this.SPEED_GAIN = 1.0f;
        this.YAW_GAIN = 1.0f;
        this.PITCH_GAIN = 1.0f;
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
        m6079J(false);
        this.currentAction = -1;
        this.offloadInitied = false;
        this.initialAction = -1;
        this.SPEED_GAIN = 1.0f;
        this.YAW_GAIN = 1.0f;
        this.PITCH_GAIN = 1.0f;
    }

    @C0064Am(aul = "12a598868836566f1fd09f95e46ad3d3", aum = 0)
    @C1253SX
    /* renamed from: ay */
    private void m6098ay(float f) {
        float A;
        int i = 0;
        float f2 = 0.0f;
        if (!C5877acF.fdP && m6122rm()) {
            if (!this.offloadInitied) {
                aYG();
            }
            if (isRunning()) {
                Pawn aYa = aYa();
                boolean b = m6106b(aYa.mo1090qZ(), "pawn local linear velocity");
                boolean b2 = m6106b(aYa.aSf(), "pawn local angular velocity");
                boolean b3 = m6105b(aYa.getPosition(), "pawn relative position");
                if (!b) {
                    aYa.mo1016bz(new Vec3f(0.0f, 0.0f, 0.0f));
                }
                if (!b2) {
                    aYa.mo1014bx(new Vec3f(0.0f, 0.0f, 0.0f));
                }
                if (b3) {
                    aYq();
                    if (this.debugStr != null) {
                        this.debugStr.setLength(0);
                        this.debugStr.append("Name: " + aYa().getName() + "\n");
                        this.debugStr.append(String.valueOf(getClass().getSimpleName()) + ": " + this.currentAction + "\n");
                    }
                    C1216Rw rw = new C1216Rw();
                    if (mo3305a(rw, f)) {
                        if (aYa instanceof Ship) {
                            Ship fAVar = (Ship) aYa;
                            m6108d(rw, fAVar);
                            i = m6099b(rw, fAVar);
                        } else if (aYa instanceof Turret) {
                            m6103b(rw, (Turret) aYa);
                        }
                        if (!(rw.brw().x == 0.0f && rw.brw().y == 0.0f && rw.brw().z == 0.0f) && m6106b(rw.brw(), "Force")) {
                            Vec3f b4 = m6100b(f, rw);
                            if (i == 0) {
                                float length = b4.length();
                                if (this.speedController == null) {
                                    A = length * this.SPEED_GAIN;
                                } else {
                                    A = this.speedController.mo10275A(length, f);
                                }
                                float ra = aYa.mo1091ra();
                                boolean bru = rw.bru();
                                if (A <= ra) {
                                    if (((double) A) < 1.01d) {
                                        ra = 0.0f;
                                    } else {
                                        ra = A;
                                    }
                                }
                                if (aYa instanceof Ship) {
                                    ((Ship) aYa).mo18307bv(bru);
                                }
                                f2 = ra;
                            }
                            m6102b(f, aYa, b4, f2, rw.getRoll(), i, rw.brA());
                        }
                    }
                }
            }
        }
    }

    @C0064Am(aul = "1392add7a648fb22d2877c56318ca9c0", aum = 0)
    /* renamed from: a */
    private boolean m6093a(Vec3d ajr, String str) {
        float f = (float) (ajr.x + ajr.y + ajr.z);
        if (!Float.isNaN(f) && !Float.isInfinite(f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        if (nanFloodControl < 10) {
            nanFloodControl++;
            logger.error(String.valueOf(getClass().getSimpleName()) + " [" + aYs() + "] : " + str + " is NaN or Infinite: " + ajr + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        }
        return false;
    }

    @C0064Am(aul = "95a2ae9b377178c7e3160e39150cb1cc", aum = 0)
    /* renamed from: a */
    private boolean m6094a(Vec3f vec3f, String str) {
        if (!m6124s(vec3f)) {
            return true;
        }
        Pawn aYa = aYa();
        ObjectId hC = aYa.bFf().mo6891hC();
        Node rPVar = null;
        if (aYa.mo960Nc() != null) {
            rPVar = aYa.azW();
        }
        if (nanFloodControl < 10) {
            nanFloodControl++;
            logger.error(String.valueOf(getClass().getSimpleName()) + " [" + aYs() + "] : " + str + " is NaN or Infinite: " + vec3f + " for Pawn " + aYa.getName() + ", " + hC + ", in Node " + rPVar);
        }
        return false;
    }

    @C0064Am(aul = "68b3d1203355c22130e46dbb2ae2ed4a", aum = 0)
    /* renamed from: r */
    private boolean m6121r(Vec3f vec3f) {
        return Float.isNaN(vec3f.x) || Float.isInfinite(vec3f.x) || Float.isNaN(vec3f.y) || Float.isInfinite(vec3f.y) || Float.isNaN(vec3f.z) || Float.isInfinite(vec3f.z);
    }

    @C0064Am(aul = "e6ccbb9d6893a0e6cc85235e51de9aee", aum = 0)
    /* renamed from: zR */
    private void m6127zR() {
    }

    @C0064Am(aul = "9c9b0ecc600b822392fd3b2a5b8812d3", aum = 0)
    /* renamed from: a */
    private Vec3f m6087a(float f, C1216Rw rw) {
        Vec3f vec3f = new Vec3f((Vector3f) rw.brw());
        vec3f.scale(f);
        Vec3f qZ = aYa().mo1090qZ();
        vec3f.x += qZ.x;
        vec3f.y += qZ.y;
        vec3f.z = qZ.z + vec3f.z;
        return vec3f;
    }

    @C0064Am(aul = "ed198bb7f100f88e62698e421b53dc2e", aum = 0)
    /* renamed from: a */
    private void m6088a(float f, Pawn avi, Vec3f vec3f, float f2) {
        float VH = avi.mo963VH() * f;
        Vec3f vec3f2 = new Vec3f(0.0f, 0.0f, -1.0f);
        float angle = (vec3f2.angle(vec3f) * 180.0f) / 3.1415927f;
        if (angle > VH) {
            vec3f = Quat4fWrap.m24430d(vec3f2.mo23476b(vec3f), (double) VH).mo15209aT(vec3f2);
        } else {
            VH = angle;
        }
        Vec3f br = avi.mo1009br(vec3f);
        br.negate();
        br.normalize();
        Vec3f b = new Vec3f(0.0f, 1.0f, 0.0f).mo23476b(br);
        b.normalize();
        Vec3f b2 = br.mo23476b(b);
        b2.normalize();
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.mo13988aD(b);
        ajk.mo13989aE(b2);
        ajk.mo13990aF(br);
        Quat4fWrap aoy = new Quat4fWrap(ajk);
        if (this.debugStr != null) {
            this.debugStr.append("ang: " + VH + " X: " + b + " Y:" + b2 + " Z:" + br + "\n");
        }
        avi.mo998b(avi.getPosition(), aoy, new Vec3f(0.0f, 0.0f, -f2), new Vec3f(0.0f, 0.0f, 0.0f), f2, 0.0f, 0.0f, 0.0f, -1, true);
    }

    @C0064Am(aul = "8b7d665c692940128888499addbfc220", aum = 0)
    /* renamed from: a */
    private void m6089a(float f, Pawn avi, Vec3f vec3f, float f2, float f3, int i, float f4) {
        float A;
        float A2;
        float f5;
        float f6;
        float f7;
        float f8;
        float VH = avi.mo963VH();
        float e = C3241pX.m37169e(-vec3f.z, vec3f.x);
        float e2 = C3241pX.m37169e(-vec3f.z, vec3f.y);
        if (this.yawController == null) {
            A = e * this.YAW_GAIN;
        } else {
            A = this.yawController.mo10275A(e, f);
        }
        if (this.pitchController == null) {
            A2 = e2 * this.PITCH_GAIN;
        } else {
            A2 = this.pitchController.mo10275A(e2, f);
        }
        if (this.debugStr != null) {
            this.debugStr.append("Maxes: " + avi.mo1091ra() + " " + VH + "\n");
            this.debugStr.append("desired: linVel " + f2 + " yaw " + A + " pitch " + A2 + "\n");
        }
        if (Math.abs(A2) <= VH && Math.abs(A) <= VH) {
            f6 = A2;
            f5 = A;
        } else if (Math.abs(A2) > Math.abs(A)) {
            f6 = Math.min(Math.abs(A2), VH) * Math.signum(A2);
            f5 = (A / A2) * f6;
        } else {
            f5 = Math.signum(A) * Math.min(Math.abs(A), VH);
            f6 = f5 * (A2 / A);
        }
        if (i == 0 && (aYa() instanceof Ship)) {
            if (Math.abs(A2) > 45.0f || Math.abs(A) > 45.0f) {
                if (f4 > 0.75f) {
                    f2 = 0.0f;
                } else if (f4 < 0.25f) {
                    f2 = Math.max(f2, (aYa().mo1091ra() / 10.0f) + 1.0f);
                }
                f8 = f6 * f;
                f7 = f5 * f;
            } else {
                f8 = f6;
                f7 = f5;
            }
            avi.mo964W(f2);
            f6 = f8;
            f5 = f7;
        } else if (i == 2) {
            avi.mo964W(0.0f);
        }
        if (i != 2 && i != 3) {
            avi.mo1063f(f3, f5, f6);
        } else if (i == 2) {
            avi.mo1063f(f3, 0.0f, 0.0f);
        }
        if (this.debugStr != null) {
            this.debugStr.append("actual: linVel " + f2 + " yaw " + f5 + " pitch " + f6 + "\n");
            this.debugStr.append("DesiredLinVel: " + avi.aSj() + "\n");
            this.debugStr.append("DesiredRollYawPitch: " + avi.aSh() + " " + avi.aSi() + " " + avi.aSg() + "\n");
        }
    }

    @C0064Am(aul = "ad564219a450622e8a5fd232f38aad9e", aum = 0)
    /* renamed from: a */
    private int m6085a(C1216Rw rw, Ship fAVar) {
        if (rw.bry()) {
            if (mo3314al().ahf()) {
                fAVar.mo7599de(cVr());
                return 2;
            } else if (mo3314al().ahh()) {
                return 3;
            } else {
                return 1;
            }
        } else if (!mo3314al().agZ() && !mo3314al().ahh()) {
            return 0;
        } else {
            fAVar.mo7599de(cVr());
            if (!mo3314al().agZ()) {
                return 3;
            }
            return 1;
        }
    }

    @C0064Am(aul = "4afc2650567d3477030e9a52b2d0250a", aum = 0)
    /* renamed from: c */
    private void m6107c(C1216Rw rw, Ship fAVar) {
        if (rw.brx() != null) {
            fAVar.mo9105k(rw.brx());
        }
        if (fAVar.afR() != null) {
            if (rw.brt()) {
                if (fAVar.agB() != rw.brz()) {
                    fAVar.mo2967X(rw.brz());
                    if (bGY()) {
                        fAVar.mo18263ad(rw.brz());
                    }
                }
                if (fAVar.afR() instanceof MissileWeapon) {
                    MissileWeapon bp = (MissileWeapon) fAVar.afR();
                    if (!bp.ayF()) {
                        bp.mo830bR(true);
                    } else if (bp.isLocked()) {
                        fAVar.mo18331y(bp.mo7860sM());
                    }
                } else if (!fAVar.afR().cUB()) {
                    fAVar.mo18331y(C3904wY.CANNON);
                }
            } else if (rw.brv() && fAVar.afR().cUB()) {
                fAVar.mo18250A(C3904wY.CANNON);
            }
        }
    }

    @C0064Am(aul = "ae74be47231f40841c1151f8776302df", aum = 0)
    /* renamed from: a */
    private void m6091a(C1216Rw rw, Turret jq) {
        if (jq.afR() != null) {
            if (rw.brt()) {
                jq.mo2967X(rw.brz());
                jq.ahE();
            } else if (rw.brv()) {
                jq.bai();
            }
        }
    }

    @C0064Am(aul = "d5b3fc4f7bc2e0fe52533eb1df29549f", aum = 0)
    private void aYp() {
        if (this.actions != null) {
            int zU = mo2801zU();
            while (zU != -1) {
                mo3326kZ(zU);
                C0573Hw[] hwArr = this.actions[zU];
                if (hwArr != null) {
                    for (C0573Hw start : hwArr) {
                        start.start();
                    }
                    zU = mo2801zU();
                }
            }
        }
    }

    @C0064Am(aul = "8e448e6a27cb9a6609bb0315c71db240", aum = 0)
    /* renamed from: b */
    private boolean m6104b(C1216Rw rw, float f) {
        if (this.actions == null || this.actions.length > 0) {
            C0573Hw[] hwArr = this.actions[aYs()];
            if (this.debugStr != null) {
                this.debugStr.append("Behaviours:");
            }
            mo3300Ml();
            rw.clear();
            if (hwArr != null) {
                for (C0573Hw hw : hwArr) {
                    if (this.debugStr != null) {
                        this.debugStr.append(" - " + hw.getClass().getName() + ":\n");
                    }
                    hw.mo2698a(this.debugStr, "   ");
                    if (hw.mo31a(rw, f)) {
                        if (this.debugStr != null) {
                            this.debugStr.append("   CMD: ");
                            this.debugStr.append(rw);
                            this.debugStr.append("\n");
                        }
                        return true;
                    }
                }
            }
            if (this.debugStr == null) {
                return false;
            }
            this.debugStr.append(" -> nothing done.\n");
            return false;
        }
        mo8358lY("Issue #3351 - Stepping before " + this + " is started.");
        mo8358lY(" ** " + isEnabled() + " ** " + aYE() + " ** " + isRunning());
        rw.clear();
        return false;
    }

    @C0064Am(aul = "913723e2b40a5bc4cf6f9c16ff8d3ed3", aum = 0)
    /* renamed from: Mk */
    private void m6080Mk() {
        this.nearbyObjects = null;
    }

    @C0064Am(aul = "c58c15f254eb5ec96765dc1b65a001ba", aum = 0)
    private int aYr() {
        return this.currentAction;
    }

    @C0064Am(aul = "5bacad016db11d611dd928aa58cbebb6", aum = 0)
    /* renamed from: kY */
    private void m6115kY(int i) {
        this.currentAction = i;
    }

    @C0064Am(aul = "3e0b461e49e8450b7e45a36f5d4f204e", aum = 0)
    private int aYt() {
        return this.initialAction;
    }

    @C0064Am(aul = "7ca2c1c38964c41b9fefd76ae70a0c8d", aum = 0)
    /* renamed from: la */
    private void m6116la(int i) {
        this.initialAction = i;
    }

    @C0064Am(aul = "a1dfc562eafb55f946d2ee68ea2c5a85", aum = 0)
    /* renamed from: rx */
    private boolean m6123rx() {
        return m6122rm();
    }

    @C0064Am(aul = "0ba1040c6bc1ede14fce2a1e7488aeea", aum = 0)
    /* renamed from: lc */
    private void m6117lc(int i) {
        this.actions = new C0573Hw[i][];
    }

    @C0064Am(aul = "7eb591d15745523e0abd55f1f162c6e6", aum = 0)
    /* renamed from: a */
    private void m6090a(int i, C0573Hw[] hwArr) {
        if (hwArr == null) {
            hwArr = new C0573Hw[0];
        } else {
            for (C0573Hw a : hwArr) {
                a.mo2697a(this);
            }
        }
        this.actions[i] = hwArr;
    }

    @C0064Am(aul = "4007ca3a804d3048bfa567bdff72577f", aum = 0)
    /* renamed from: a */
    private C0573Hw m6086a(C0573Hw[] hwArr) {
        return new C0870Mc(hwArr);
    }

    @C0064Am(aul = "3354966384e88adec6cdc92b6d7fb289", aum = 0)
    /* renamed from: I */
    private C0573Hw m6078I(int i, int i2) {
        return this.actions[i][i2];
    }

    @C0064Am(aul = "372c96032bf8e821f72deb29703b4ac2", aum = 0)
    /* renamed from: g */
    private C0573Hw m6113g(int i, int i2, int i3) {
        return ((C0870Mc) this.actions[i][i2]).bgB()[i3];
    }

    @C0064Am(aul = "b3db05ae23fa098b0076b7f45732773d", aum = 0)
    /* renamed from: qN */
    private void m6118qN() {
        if (!m6122rm()) {
            aYw();
            aYy();
            m6079J(true);
            aYA();
        }
    }

    @C4034yP
    @C0064Am(aul = "2f3f5e10e6be5454554bce3da82c3ade", aum = 0)
    @C2499fr
    @C1253SX
    private void aYv() {
        this.offloadInitied = false;
    }

    @C0064Am(aul = "27dd9c6e1046dfc7d3322efb37c814fa", aum = 0)
    @C1253SX
    private void aYx() {
        boolean z;
        boolean z2 = true;
        mo3318b(aYa() != null, "No pawn set in AI Controller");
        mo2800zQ();
        mo3333zS();
        if (this.actions != null) {
            z = true;
        } else {
            z = false;
        }
        mo3318b(z, "Invalid behaviours configuration");
        if (this.initialAction < 0 || this.initialAction >= this.actions.length) {
            z2 = false;
        }
        mo3318b(z2, "Invalid behaviours configuration");
        mo3326kZ(this.initialAction);
    }

    @C0064Am(aul = "aa0ad1a61751b6aeb0c96dcc0e0a8a70", aum = 0)
    @Deprecated
    private void aYz() {
    }

    @C4034yP
    @C0064Am(aul = "b5f6749fa0d8d1761b34d3e99331a84f", aum = 0)
    @C2499fr
    @C1253SX
    @Deprecated
    private void aYB() {
        aYO();
    }

    @C0064Am(aul = "f7d7cd3aac86616911028b095b9170c6", aum = 0)
    @C1253SX
    private boolean aYD() {
        return this.offloadInitied;
    }

    @C0064Am(aul = "a9b5305c5878b142f46020301fbf4b78", aum = 0)
    @C1253SX
    private void aYF() {
        boolean z = true;
        if (this.actions == null) {
            mo3328ld(1);
        }
        if (isDisposed()) {
            m6079J(false);
            return;
        }
        this.offloadInitied = true;
        this.currentAction = 0;
        this.SPEED_GAIN = 1.0f;
        this.YAW_GAIN = 1.0f;
        this.PITCH_GAIN = 1.0f;
        if (m6122rm()) {
            try {
                if (aYa() == null) {
                    z = false;
                }
                mo3318b(z, "No pawn set in AI Controller");
                mo2800zQ();
                mo3333zS();
            } catch (RuntimeException e) {
                logger.error("Stopping AI because of XML inconsistency", e);
                stop();
            }
        }
    }

    @C0064Am(aul = "133deede7aed00d96a33ed6c9fc7565c", aum = 0)
    /* renamed from: Mp */
    private boolean m6083Mp() {
        return aYs() >= 0 && aYa() != null && aYa().bae() && aYa().cLZ();
    }

    @C0064Am(aul = "b355044a511db66cff148d5ddc45f3eb", aum = 0)
    private boolean aYH() {
        return false;
    }

    @C0064Am(aul = "60309ea2dccbb220df5b89cc14894bce", aum = 0)
    /* renamed from: Mo */
    private Ship m6082Mo() {
        return (Ship) aYa();
    }

    @C0064Am(aul = "f35b1b104da947f4c48005d073c7e46d", aum = 0)
    /* renamed from: uW */
    private ShipType m6125uW() {
        return mo3314al().agH();
    }

    @C0064Am(aul = "5272c6133eabde5b17305d018bea9cf1", aum = 0)
    /* renamed from: Mm */
    private List<Actor> m6081Mm() {
        if (this.nearbyObjects == null) {
            this.nearbyObjects = aYa().mo965Zc().mo1900c(aYa().getPosition(), 4000.0f);
            Iterator<Actor> it = this.nearbyObjects.iterator();
            while (it.hasNext()) {
                Actor next = it.next();
                if (next == aYa() || m6096ax(next)) {
                    it.remove();
                }
            }
        }
        return this.nearbyObjects;
    }

    @C0064Am(aul = "1d5d004a947be7c09ae78e5ec509b3a4", aum = 0)
    /* renamed from: aw */
    private boolean m6095aw(Actor cr) {
        if (cr.aiD() == null || !aYa().mo965Zc().cKn().bvg().mo1932a(aYa().aiD(), cr.aiD()) || aYa().mo965Zc().cKn().mo5736c(aYa().aiD(), cr.aiD()) == 0) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "dea7e7dd7ff390900c640aff8254c64d", aum = 0)
    /* renamed from: ay */
    private Character m6097ay(Actor cr) {
        if (cr instanceof Ship) {
            return ((Ship) cr).agj();
        }
        if (cr instanceof Turret) {
            return mo3315az(((Turret) cr).aZS());
        }
        return null;
    }

    @C0064Am(aul = "c76bc647a5b9552d7e1417ab20b784a0", aum = 0)
    /* renamed from: C */
    private boolean m6077C(Actor cr) {
        Character az;
        Character az2 = mo3315az(aYa());
        if (az2 == null || (az = mo3315az(cr)) == null || az2.mo12639T(az) != Faction.C1624a.ENEMY) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "15729dc6678f88b095100b702337e82c", aum = 0)
    /* renamed from: a */
    private void m6092a(boolean z, String str) {
        if (!z) {
            throw new RuntimeException(str);
        }
    }

    @C0064Am(aul = "920425fc2c068966fe1e4f1f1f744a32", aum = 0)
    /* renamed from: fg */
    private void m6112fg() {
        stop();
        super.dispose();
    }

    @C0064Am(aul = "9cddd78b076a9d88384e3e23f1b7b414", aum = 0)
    private float aYK() {
        return this.PITCH_GAIN;
    }

    @C0064Am(aul = "5f5e2600f59f58a61d04cfca7c761f4a", aum = 0)
    /* renamed from: fK */
    private void m6109fK(float f) {
        this.PITCH_GAIN = f;
    }

    @C0064Am(aul = "e0cbd8427863fea1fcf78b9e35a4f08b", aum = 0)
    private float aYL() {
        return this.SPEED_GAIN;
    }

    @C0064Am(aul = "fe60ba69a8e6ea07c8318be9d1383a7c", aum = 0)
    /* renamed from: fL */
    private void m6110fL(float f) {
        this.SPEED_GAIN = f;
    }

    @C0064Am(aul = "e100302e14993e7678a417b7f0e0057a", aum = 0)
    private float aYM() {
        return this.YAW_GAIN;
    }

    @C0064Am(aul = "c0d58fb717ef9d35543c4f0ad9c2b873", aum = 0)
    /* renamed from: fM */
    private void m6111fM(float f) {
        this.YAW_GAIN = f;
    }

    @C0064Am(aul = "45b9b0385f65ec6560c89ad5ed331568", aum = 0)
    /* renamed from: zX */
    private AIControllerType m6129zX() {
        return (AIControllerType) super.getType();
    }

    @C0064Am(aul = "150f8bc736cef060ea539b5ec0f62796", aum = 0)
    @C1253SX
    /* renamed from: g */
    private void m6114g(Space ea, long j) {
    }

    @C0064Am(aul = "db3ebc343f69473dbff06f030f61bec8", aum = 0)
    @C1253SX
    private void aYN() {
    }

    @C0064Am(aul = "f5219a11e9820355a17c12edbe7b7734", aum = 0)
    private Collection<aDR> aad() {
        return aYa().aae();
    }
}
