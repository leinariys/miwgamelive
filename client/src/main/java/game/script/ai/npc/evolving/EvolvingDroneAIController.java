package game.script.ai.npc.evolving;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.ai.npc.DroneAIControllerType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6406amO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.game.script.p003ai.npc.DroneAIController;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.NS */
/* compiled from: a */
public class EvolvingDroneAIController extends DroneAIController implements C1616Xf {
    /* renamed from: _f_onDamageDealt_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003bLtaikodom_002fgame_002fDamage_003bLcom_002fhoplon_002fgeometry_002fVec3f_003b_0029V */
    public static final C2491fm f1230x402d3b93 = null;
    /* renamed from: _f_onDamageTaken_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003bLtaikodom_002fgame_002fDamage_003bLcom_002fhoplon_002fgeometry_002fVec3f_003b_0029V */
    public static final C2491fm f1231xbe599734 = null;
    /* renamed from: _f_onKill_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029V */
    public static final C2491fm f1232xb55723f2 = null;
    public static final C2491fm _f_onPawnExplode_0020_0028_0029V = null;
    public static final C2491fm _f_onPawnSpawn_0020_0028_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dIv = null;
    public static final C5663aRz dIx = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    public static HashMap<C2971mQ, C6712asI> dIt;
    private static boolean dIs = true;
    @C0064Am(aul = "dab2463d551cf8ac6f302b23c46abadc", aum = 0)
    private static C2971mQ dIu;
    @C0064Am(aul = "bf42b21deaf206143233714595d50130", aum = 1)
    private static C6712asI dIw;

    static {
        m7627V();
    }

    public EvolvingDroneAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public EvolvingDroneAIController(DroneAIControllerType aco) {
        super((C5540aNg) null);
        super.mo4238a(aco);
    }

    public EvolvingDroneAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7627V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = DroneAIController._m_fieldCount + 2;
        _m_methodCount = DroneAIController._m_methodCount + 6;
        int i = DroneAIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(EvolvingDroneAIController.class, "dab2463d551cf8ac6f302b23c46abadc", i);
        dIv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(EvolvingDroneAIController.class, "bf42b21deaf206143233714595d50130", i2);
        dIx = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) DroneAIController._m_fields, (Object[]) _m_fields);
        int i4 = DroneAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(EvolvingDroneAIController.class, "a19561ac918ddc10475f508bac0894b2", i4);
        _f_setupBehaviours_0020_0028_0029V = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(EvolvingDroneAIController.class, "c5fcfb517faaa09213384140a44fdc68", i5);
        f1231xbe599734 = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(EvolvingDroneAIController.class, "8dd1ac6ab9e45ae86369e00dd0ac2721", i6);
        f1230x402d3b93 = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(EvolvingDroneAIController.class, "ba8b599c43bb0fbe359fd92b91f5daba", i7);
        _f_onPawnSpawn_0020_0028_0029V = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(EvolvingDroneAIController.class, "f6e301deeef09a84f138508b1c4e980b", i8);
        _f_onPawnExplode_0020_0028_0029V = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(EvolvingDroneAIController.class, "d6902229e4dc8308a256baac8c8fc638", i9);
        f1232xb55723f2 = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) DroneAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(EvolvingDroneAIController.class, C6406amO.class, _m_fields, _m_methods);
    }

    private static C2971mQ bkS() {
        if (dIt == null) {
            bkT();
        }
        if (dIt.containsValue((Object) null)) {
            for (C2971mQ next : dIt.keySet()) {
                if (dIt.get(next) == null) {
                    return next;
                }
            }
        }
        List<T> H = C3255ph.m37270H(dIt);
        dIt.clear();
        for (T put : H) {
            dIt.put(put, (Object) null);
        }
        if (dIs) {
            System.out.println("New Generation created:");
            System.out.println();
            for (C2971mQ mQVar : dIt.keySet()) {
                System.out.println(mQVar.toString());
            }
            System.out.println();
        }
        return bkS();
    }

    public static void bkT() {
        dIt = new HashMap<>();
        C2971mQ mQVar = new C2971mQ();
        for (int i = 0; i < 10; i++) {
            dIt.put(new C2971mQ(mQVar), (Object) null);
            mQVar.mo18006mu();
            mQVar.mo18006mu();
            mQVar.mo18006mu();
            mQVar.mo18006mu();
            mQVar.mo18006mu();
        }
        if (dIs) {
            System.out.println("Generation created:");
            System.out.println();
            for (C2971mQ mQVar2 : dIt.keySet()) {
                System.out.println(mQVar2.toString());
            }
        }
    }

    /* renamed from: a */
    private void m7629a(C6712asI asi) {
        bFf().mo5608dq().mo3197f(dIx, asi);
    }

    /* renamed from: a */
    private void m7630a(C2971mQ mQVar) {
        bFf().mo5608dq().mo3197f(dIv, mQVar);
    }

    private C2971mQ bkQ() {
        return (C2971mQ) bFf().mo5608dq().mo3214p(dIv);
    }

    private C6712asI bkR() {
        return (C6712asI) bFf().mo5608dq().mo3214p(dIx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6406amO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - DroneAIController._m_methodCount) {
            case 0:
                m7633zP();
                return null;
            case 1:
                m7628a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2]);
                return null;
            case 2:
                m7632c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2]);
                return null;
            case 3:
                aYb();
                return null;
            case 4:
                aYf();
                return null;
            case 5:
                m7631au((Actor) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void aYc() {
        switch (bFf().mo6893i(_f_onPawnSpawn_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnSpawn_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnSpawn_0020_0028_0029V, new Object[0]));
                break;
        }
        aYb();
    }

    public void aYg() {
        switch (bFf().mo6893i(_f_onPawnExplode_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                break;
        }
        aYf();
    }

    /* renamed from: av */
    public void mo3289av(Actor cr) {
        switch (bFf().mo6893i(f1232xb55723f2)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1232xb55723f2, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1232xb55723f2, new Object[]{cr}));
                break;
        }
        m7631au(cr);
    }

    /* renamed from: b */
    public void mo3290b(Actor cr, C5260aCm acm, Vec3f vec3f) {
        switch (bFf().mo6893i(f1231xbe599734)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1231xbe599734, new Object[]{cr, acm, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1231xbe599734, new Object[]{cr, acm, vec3f}));
                break;
        }
        m7628a(cr, acm, vec3f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo3292d(Actor cr, C5260aCm acm, Vec3f vec3f) {
        switch (bFf().mo6893i(f1230x402d3b93)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1230x402d3b93, new Object[]{cr, acm, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1230x402d3b93, new Object[]{cr, acm, vec3f}));
                break;
        }
        m7632c(cr, acm, vec3f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m7633zP();
    }

    /* renamed from: a */
    public void mo4238a(DroneAIControllerType aco) {
        super.mo4238a(aco);
        m7630a((C2971mQ) null);
        m7629a((C6712asI) null);
    }

    @C0064Am(aul = "a19561ac918ddc10475f508bac0894b2", aum = 0)
    /* renamed from: zP */
    private void m7633zP() {
        if (bkQ() == null) {
            m7630a(bkS());
        }
        if (bkR() == null) {
            m7629a(new C6712asI());
            dIt.put(bkQ(), bkR());
        }
        super.mo3319cp(bkQ().mo20532Pk());
        super.mo3320cq(bkQ().mo20533Pl());
        super.mo3321cr(bkQ().mo20534Pm());
        super.mo2800zQ();
    }

    @C0064Am(aul = "c5fcfb517faaa09213384140a44fdc68", aum = 0)
    /* renamed from: a */
    private void m7628a(Actor cr, C5260aCm acm, Vec3f vec3f) {
        super.mo3290b(cr, acm, vec3f);
        bkR().mo15903d(acm);
    }

    @C0064Am(aul = "8dd1ac6ab9e45ae86369e00dd0ac2721", aum = 0)
    /* renamed from: c */
    private void m7632c(Actor cr, C5260aCm acm, Vec3f vec3f) {
        super.mo3292d(cr, acm, vec3f);
        bkR().mo15904e(acm);
    }

    @C0064Am(aul = "ba8b599c43bb0fbe359fd92b91f5daba", aum = 0)
    private void aYb() {
        super.aYc();
        mo8359ma("Spawning evoDrone\n" + bkQ());
        bkR().mo15905iI(ala().currentTimeMillis());
    }

    @C0064Am(aul = "f6e301deeef09a84f138508b1c4e980b", aum = 0)
    private void aYf() {
        super.aYg();
        bkR().mo15906iJ(ala().currentTimeMillis());
    }

    @C0064Am(aul = "d6902229e4dc8308a256baac8c8fc638", aum = 0)
    /* renamed from: au */
    private void m7631au(Actor cr) {
        super.mo3289av(cr);
        bkR().cuL();
    }
}
