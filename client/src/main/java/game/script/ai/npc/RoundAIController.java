package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Character;
import game.script.ship.Ship;
import game.script.ship.Station;
import logic.abb.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6851aur;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;

@C5511aMd
@C6485anp
/* renamed from: a.IM */
/* compiled from: a */
public class RoundAIController extends AIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bpW = null;
    public static final C5663aRz dgB = null;
    public static final C5663aRz dgD = null;
    public static final C5663aRz dgF = null;
    public static final C5663aRz dgH = null;
    public static final C5663aRz dgI = null;
    public static final C2491fm dgJ = null;
    public static final C2491fm dgK = null;
    public static final C2491fm dgL = null;
    public static final C2491fm dgM = null;
    public static final C2491fm dgN = null;
    public static final long serialVersionUID = 0;
    private static final int WAITING = 1;
    private static final float dgx = 60.0f;
    private static final float dgy = 5.0f;
    private static final int dgz = 0;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "97ebba05cefbb9d19aa299e1e18c0087", aum = 4)
    private static boolean ajo = false;
    @C0064Am(aul = "3176a4f30258349bdc1d9c41a7435ab5", aum = 0)
    private static C3438ra<Station> dgA = null;
    @C0064Am(aul = "21c21d2eed47e8a1824abb951ec42cd2", aum = 1)
    private static float dgC = 0.0f;
    @C0064Am(aul = "ba44933c9b628093b78e3f5fb7b41f56", aum = 2)
    private static float dgE = 0.0f;
    @C0064Am(aul = "648b663d161c6c20a954fecaa1a71ef0", aum = 3)
    private static Station dgG = null;

    static {
        m5348V();
    }

    private transient C6822auO avoidObstaclesBehaviour;

    public RoundAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RoundAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5348V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 5;
        _m_methodCount = AIController._m_methodCount + 8;
        int i = AIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(RoundAIController.class, "3176a4f30258349bdc1d9c41a7435ab5", i);
        dgB = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(RoundAIController.class, "21c21d2eed47e8a1824abb951ec42cd2", i2);
        dgD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(RoundAIController.class, "ba44933c9b628093b78e3f5fb7b41f56", i3);
        dgF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(RoundAIController.class, "648b663d161c6c20a954fecaa1a71ef0", i4);
        dgH = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(RoundAIController.class, "97ebba05cefbb9d19aa299e1e18c0087", i5);
        dgI = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i7 = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 8)];
        C2491fm a = C4105zY.m41624a(RoundAIController.class, "4b8b6aae90291c4b32012e7bb0822d6b", i7);
        dgJ = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(RoundAIController.class, "cff70bea9ecff2274b234d73af21e371", i8);
        dgK = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(RoundAIController.class, "d37954fd587da69f4faebe60f070868e", i9);
        dgL = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(RoundAIController.class, "5924a2886113b7d3959308ef85dfe375", i10);
        dgM = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(RoundAIController.class, "77d1ab285e5f632144027d25145c1e8d", i11);
        bpW = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(RoundAIController.class, "d734b524bfff1b67cccd961c7c54fd4e", i12);
        _f_changeCurrentAction_0020_0028_0029I = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(RoundAIController.class, "aaecb00a1fff9e0f3ac7a4db4ea2e96e", i13);
        dgN = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(RoundAIController.class, "0d43e5f46d5ea46b59f858c2a80bcc38", i14);
        _f_setupBehaviours_0020_0028_0029V = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RoundAIController.class, C6851aur.class, _m_fields, _m_methods);
    }

    private C3438ra aWX() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dgB);
    }

    private float aWY() {
        return bFf().mo5608dq().mo3211m(dgD);
    }

    private float aWZ() {
        return bFf().mo5608dq().mo3211m(dgF);
    }

    private Station aXa() {
        return (Station) bFf().mo5608dq().mo3214p(dgH);
    }

    private boolean aXb() {
        return bFf().mo5608dq().mo3201h(dgI);
    }

    @C0064Am(aul = "aaecb00a1fff9e0f3ac7a4db4ea2e96e", aum = 0)
    @C5566aOg
    @C2499fr
    private void aXe() {
        throw new aWi(new aCE(this, dgN, new Object[0]));
    }

    @C5566aOg
    @C2499fr
    private void aXf() {
        switch (bFf().mo6893i(dgN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dgN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dgN, new Object[0]));
                break;
        }
        aXe();
    }

    private Character agj() {
        switch (bFf().mo6893i(bpW)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, bpW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpW, new Object[0]));
                break;
        }
        return agi();
    }

    /* renamed from: bb */
    private void m5349bb(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dgB, raVar);
    }

    /* renamed from: cQ */
    private void m5350cQ(boolean z) {
        bFf().mo5608dq().mo3153a(dgI, z);
    }

    /* renamed from: fB */
    private void m5351fB(float f) {
        bFf().mo5608dq().mo3150a(dgD, f);
    }

    /* renamed from: fC */
    private void m5352fC(float f) {
        bFf().mo5608dq().mo3150a(dgF, f);
    }

    /* renamed from: z */
    private void m5355z(Station bf) {
        bFf().mo5608dq().mo3197f(dgH, bf);
    }

    /* renamed from: B */
    public void mo2796B(Station bf) {
        switch (bFf().mo6893i(dgK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dgK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dgK, new Object[]{bf}));
                break;
        }
        m5347A(bf);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6851aur(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                aXc();
                return null;
            case 1:
                m5347A((Station) args[0]);
                return null;
            case 2:
                m5353fD(((Float) args[0]).floatValue());
                return null;
            case 3:
                m5354fF(((Float) args[0]).floatValue());
                return null;
            case 4:
                return agi();
            case 5:
                return new Integer(m5357zT());
            case 6:
                aXe();
                return null;
            case 7:
                m5356zP();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void aXd() {
        switch (bFf().mo6893i(dgJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dgJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dgJ, new Object[0]));
                break;
        }
        aXc();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: fE */
    public void mo2798fE(float f) {
        switch (bFf().mo6893i(dgL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dgL, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dgL, new Object[]{new Float(f)}));
                break;
        }
        m5353fD(f);
    }

    /* renamed from: fG */
    public void mo2799fG(float f) {
        switch (bFf().mo6893i(dgM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dgM, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dgM, new Object[]{new Float(f)}));
                break;
        }
        m5354fF(f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m5356zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m5357zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m5351fB(5.0f);
        m5352fC(dgx);
        m5355z((Station) null);
    }

    @C0064Am(aul = "4b8b6aae90291c4b32012e7bb0822d6b", aum = 0)
    private void aXc() {
        aWX().clear();
        m5355z((Station) null);
    }

    @C0064Am(aul = "cff70bea9ecff2274b234d73af21e371", aum = 0)
    /* renamed from: A */
    private void m5347A(Station bf) {
        boolean z;
        boolean z2;
        Iterator it = aWX().iterator();
        while (true) {
            if (it.hasNext()) {
                if (((Station) it.next()).cLb() != bf.cLb()) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (z) {
            z2 = false;
        } else {
            z2 = true;
        }
        mo3318b(z2, "RoundController must have all stations in the same node");
        aWX().add(bf);
        if (aXa() == null) {
            m5355z(bf);
        }
    }

    @C0064Am(aul = "d37954fd587da69f4faebe60f070868e", aum = 0)
    /* renamed from: fD */
    private void m5353fD(float f) {
        if (f >= 5.0f && f <= dgx && f <= aWZ()) {
            m5351fB(f);
        }
    }

    @C0064Am(aul = "5924a2886113b7d3959308ef85dfe375", aum = 0)
    /* renamed from: fF */
    private void m5354fF(float f) {
        if (f >= 5.0f && f <= dgx && f >= aWY()) {
            m5352fC(f);
        }
    }

    @C0064Am(aul = "77d1ab285e5f632144027d25145c1e8d", aum = 0)
    private Character agi() {
        Pawn aYa = aYa();
        if (aYa instanceof Ship) {
            return ((Ship) aYa).agj();
        }
        return null;
    }

    @C0064Am(aul = "d734b524bfff1b67cccd961c7c54fd4e", aum = 0)
    /* renamed from: zT */
    private int m5357zT() {
        int aYs = aYs();
        Character agj = agj();
        if (agj == null) {
            return -1;
        }
        if (aYs == 0) {
            if (aYa().mo1001bB((Actor) aXa()) >= PlayerController.m39266c((C0286Dh) aXa())) {
                return -1;
            }
            try {
                aXa().mo654o(agj);
                aXa().mo623b(agj, (((float) Math.random()) * (aWZ() - aWY())) + aWY());
                return 1;
            } catch (C6046afS e) {
                e.printStackTrace();
                return -1;
            }
        } else if (aYs != 1) {
            return -1;
        } else {
            aXf();
            mo3317b(0, new C1829aV(), mo3316b(this.avoidObstaclesBehaviour, new C3284pw(aXa().cLl()), new C6434amq()));
            return 0;
        }
    }

    @C0064Am(aul = "0d43e5f46d5ea46b59f858c2a80bcc38", aum = 0)
    /* renamed from: zP */
    private void m5356zP() {
        boolean z;
        mo3318b(aWX().size() >= 2, "RoundController must have at least 2 stations to round on");
        mo3328ld(2);
        Character agj = agj();
        if (agj != null) {
            z = true;
        } else {
            z = false;
        }
        mo3318b(z, "Ship doesn't have a pilot!");
        if (aXa() == null) {
            m5355z((Station) aWX().get(0));
        }
        if (agj.bQB()) {
            mo3327lb(1);
            ((RoundAIController) mo8362mt(5.0f)).mo2801zU();
        } else {
            mo3327lb(0);
        }
        this.avoidObstaclesBehaviour = new C6822auO();
        mo3317b(1, new C3103nq());
        mo3317b(0, new C1829aV(), mo3316b(this.avoidObstaclesBehaviour, new C3284pw(), new C6434amq()));
    }
}
