package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1656YO;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.jl */
/* compiled from: a */
public class TestAIController extends AIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static final int aiF = 0;
    public static C6494any ___iScriptClass;

    static {
        m34117V();
    }

    public TestAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TestAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34117V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 0;
        _m_methodCount = AIController._m_methodCount + 2;
        _m_fields = new C5663aRz[(AIController._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(TestAIController.class, "1e8fa0f276b4e2f735d5414653b7a0ec", i);
        _f_setupBehaviours_0020_0028_0029V = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(TestAIController.class, "793c2f9d21690844a283e967077d98a1", i2);
        _f_changeCurrentAction_0020_0028_0029I = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TestAIController.class, C1656YO.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "1e8fa0f276b4e2f735d5414653b7a0ec", aum = 0)
    @C5566aOg
    /* renamed from: zP */
    private void m34118zP() {
        throw new aWi(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "793c2f9d21690844a283e967077d98a1", aum = 0)
    @C5566aOg
    /* renamed from: zT */
    private int m34119zT() {
        throw new aWi(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1656YO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                m34118zP();
                return null;
            case 1:
                return new Integer(m34119zT());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m34118zP();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m34119zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
