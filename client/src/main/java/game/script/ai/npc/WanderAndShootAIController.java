package game.script.ai.npc;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.WanderAndShootAIControllerType;
import logic.abb.*;
import logic.baa.*;
import logic.data.mbean.C1426Ut;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import javax.vecmath.Tuple3d;
import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.hV */
/* compiled from: a */
public class WanderAndShootAIController extends AIController implements C1616Xf, aJO {
    /* renamed from: WC */
    public static final C2491fm f7949WC = null;
    public static final C5663aRz _f_center = null;
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_configureBehaviours_0020_0028_0029V = null;
    /* renamed from: _f_getCenter_0020_0028_0029Lcom_002fhoplon_002fgeometry_002fVec3d_003b */
    public static final C2491fm f7950xdc7de0cc = null;
    /* renamed from: _f_getType_0020_0028_0029Ltaikodom_002fgame_002fscript_002fai_002fnpc_002fAIControllerType_003b */
    public static final C2491fm f7951xed6e425 = null;
    public static final C5663aRz _f_radius2 = null;
    /* renamed from: _f_setCenter_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003b_0029V */
    public static final C2491fm f7952x6107980 = null;
    public static final C2491fm _f_setRadius2_0020_0028F_0029V = null;
    public static final C2491fm _f_setRadius_0020_0028F_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final C5663aRz _f_shootAngle = null;
    public static final C5663aRz _f_shootPredictMovimet = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dF */
    public static final C5663aRz f7954dF = null;
    public static final long serialVersionUID = 0;
    public static final int RETURNING_TO_SPHERE = 1;
    public static final int WANDERING = 0;
    private static final int RADIUS_ALLOWED_OUTSIDE_SPHERE_SQUARED = 10000;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "56c52ddeac3098754cc1f21bc228ff9e", aum = 3)
    private static Vec3d center;
    @C0064Am(aul = "d7a222275900cb1efe4ff9ac64547f37", aum = 2)
    @C5454aJy("Wander - Factor beetween going ahead and turning\\nMust be beetween 1 (full ahead) and 0 (lots of turns)")

    /* renamed from: dE */
    private static float f7953dE = 0.5f;
    @C0064Am(aul = "73b8927c76962633165f0a1568cae592", aum = 4)
    private static float radius2;
    @C0064Am(aul = "8e5221fdc60c4b91b0766d27f7835874", aum = 0)
    @C5454aJy("Shoot - Angle to start shooting (º)")
    private static float shootAngle = 10.0f;
    @C0064Am(aul = "b97399df89e11f4bbc4c62438ecfdabb", aum = 1)
    @C5454aJy("Shoot - Predict target moviment")
    private static boolean shootPredictMovimet = false;

    static {
        m32706V();
    }

    /* renamed from: WB */
    public transient C5280aDg f7955WB;
    public transient C6305akR shootEnemyBehaviour;

    public WanderAndShootAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WanderAndShootAIController(WanderAndShootAIControllerType k) {
        super((C5540aNg) null);
        super._m_script_init(k);
    }

    public WanderAndShootAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32706V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 5;
        _m_methodCount = AIController._m_methodCount + 10;
        int i = AIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(WanderAndShootAIController.class, "8e5221fdc60c4b91b0766d27f7835874", i);
        _f_shootAngle = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WanderAndShootAIController.class, "b97399df89e11f4bbc4c62438ecfdabb", i2);
        _f_shootPredictMovimet = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WanderAndShootAIController.class, "d7a222275900cb1efe4ff9ac64547f37", i3);
        f7954dF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WanderAndShootAIController.class, "56c52ddeac3098754cc1f21bc228ff9e", i4);
        _f_center = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WanderAndShootAIController.class, "73b8927c76962633165f0a1568cae592", i5);
        _f_radius2 = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i7 = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(WanderAndShootAIController.class, "6b5845f4f057b6629020a50ee2b87267", i7);
        _f_step_0020_0028F_0029V = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(WanderAndShootAIController.class, "5042eaa2bc510cde820d64f46a8f92d4", i8);
        _f_setRadius_0020_0028F_0029V = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(WanderAndShootAIController.class, "6614ee715541ab547848865ca96a7bc3", i9);
        _f_setRadius2_0020_0028F_0029V = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(WanderAndShootAIController.class, "2c75293cd9a42627db085ecee1f70318", i10);
        f7950xdc7de0cc = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(WanderAndShootAIController.class, "ef8d28dbb70a4fb6d85717593326ddac", i11);
        f7952x6107980 = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(WanderAndShootAIController.class, "dc111424090d9d28dd478ec1dbf3ea4d", i12);
        _f_setupBehaviours_0020_0028_0029V = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(WanderAndShootAIController.class, "6dc4f3cb4f0521ef314bce876bdbb288", i13);
        _f_configureBehaviours_0020_0028_0029V = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(WanderAndShootAIController.class, "2dae389b825553d1a36241b9abcb9769", i14);
        _f_changeCurrentAction_0020_0028_0029I = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(WanderAndShootAIController.class, "27752ed15736add399db890e5b799869", i15);
        f7949WC = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(WanderAndShootAIController.class, "27ed20608a351b4763f30905591af353", i16);
        f7951xed6e425 = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WanderAndShootAIController.class, C1426Ut.class, _m_fields, _m_methods);
    }

    /* renamed from: aI */
    private float m32709aI() {
        return ((WanderAndShootAIControllerType) getType()).mo3458aM();
    }

    /* renamed from: aJ */
    private boolean m32710aJ() {
        return ((WanderAndShootAIControllerType) getType()).mo3459aO();
    }

    /* renamed from: aK */
    private float m32711aK() {
        return ((WanderAndShootAIControllerType) getType()).mo3460aQ();
    }

    /* renamed from: az */
    private void m32713az(float f) {
        bFf().mo5608dq().mo3150a(_f_radius2, f);
    }

    /* renamed from: b */
    private void m32714b(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m32715f(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(_f_center, ajr);
    }

    /* renamed from: i */
    private void m32717i(float f) {
        throw new C6039afL();
    }

    /* renamed from: j */
    private void m32718j(float f) {
        throw new C6039afL();
    }

    /* renamed from: zL */
    private Vec3d m32719zL() {
        return (Vec3d) bFf().mo5608dq().mo3214p(_f_center);
    }

    /* renamed from: zM */
    private float m32720zM() {
        return bFf().mo5608dq().mo3211m(_f_radius2);
    }

    @C0064Am(aul = "27ed20608a351b4763f30905591af353", aum = 0)
    /* renamed from: zX */
    private AIControllerType m32726zX() {
        return mo19275zW();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1426Ut(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                m32712ay(((Float) args[0]).floatValue());
                return null;
            case 1:
                m32707aA(((Float) args[0]).floatValue());
                return null;
            case 2:
                m32708aB(((Float) args[0]).floatValue());
                return null;
            case 3:
                return m32721zN();
            case 4:
                m32716g((Vec3d) args[0]);
                return null;
            case 5:
                m32722zP();
                return null;
            case 6:
                m32723zR();
                return null;
            case 7:
                return new Integer(m32724zT());
            case 8:
                return m32725zV();
            case 9:
                return m32726zX();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aC */
    public void mo19273aC(float f) {
        switch (bFf().mo6893i(_f_setRadius2_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius2_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius2_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m32708aB(f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: h */
    public void mo4610h(Vec3d ajr) {
        switch (bFf().mo6893i(f7952x6107980)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7952x6107980, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7952x6107980, new Object[]{ajr}));
                break;
        }
        m32716g(ajr);
    }

    public void setRadius(float f) {
        switch (bFf().mo6893i(_f_setRadius_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m32707aA(f);
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m32712ay(f);
    }

    /* renamed from: zO */
    public Vec3d mo19274zO() {
        switch (bFf().mo6893i(f7950xdc7de0cc)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, f7950xdc7de0cc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7950xdc7de0cc, new Object[0]));
                break;
        }
        return m32721zN();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m32722zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zS */
    public void mo3333zS() {
        switch (bFf().mo6893i(_f_configureBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_configureBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_configureBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m32723zR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m32724zT();
    }

    /* renamed from: zW */
    public WanderAndShootAIControllerType mo19275zW() {
        switch (bFf().mo6893i(f7949WC)) {
            case 0:
                return null;
            case 2:
                return (WanderAndShootAIControllerType) bFf().mo5606d(new aCE(this, f7949WC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7949WC, new Object[0]));
                break;
        }
        return m32725zV();
    }

    /* renamed from: zY */
    public /* bridge */ /* synthetic */ AIControllerType mo3334zY() {
        switch (bFf().mo6893i(f7951xed6e425)) {
            case 0:
                return null;
            case 2:
                return (AIControllerType) bFf().mo5606d(new aCE(this, f7951xed6e425, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7951xed6e425, new Object[0]));
                break;
        }
        return m32726zX();
    }

    /* renamed from: S */
    public void mo10S() {
        WanderAndShootAIControllerType k = (WanderAndShootAIControllerType) bFf().mo6865M(WanderAndShootAIControllerType.class);
        k.mo10S();
        mo19272a(k);
    }

    /* renamed from: a */
    public void mo19272a(WanderAndShootAIControllerType k) {
        super.mo967a((C2961mJ) k);
        m32713az(-1.0f);
        if (k == null) {
            mo8358lY("WanderAndShootAIController created without a type: " + cWm());
        }
    }

    @C0064Am(aul = "6b5845f4f057b6629020a50ee2b87267", aum = 0)
    /* renamed from: ay */
    private void m32712ay(float f) {
        if (mo19275zW() != null) {
            super.step(f);
        }
    }

    @C0064Am(aul = "5042eaa2bc510cde820d64f46a8f92d4", aum = 0)
    /* renamed from: aA */
    private void m32707aA(float f) {
        m32713az(f * f);
    }

    @C0064Am(aul = "6614ee715541ab547848865ca96a7bc3", aum = 0)
    /* renamed from: aB */
    private void m32708aB(float f) {
        m32713az(f);
    }

    @C0064Am(aul = "2c75293cd9a42627db085ecee1f70318", aum = 0)
    /* renamed from: zN */
    private Vec3d m32721zN() {
        return m32719zL();
    }

    @C0064Am(aul = "ef8d28dbb70a4fb6d85717593326ddac", aum = 0)
    /* renamed from: g */
    private void m32716g(Vec3d ajr) {
        m32715f(ajr);
    }

    @C0064Am(aul = "dc111424090d9d28dd478ec1dbf3ea4d", aum = 0)
    /* renamed from: zP */
    private void m32722zP() {
        boolean z;
        mo3318b(m32719zL() != null, "You must set a center for WanderAndShootAIController");
        if (m32720zM() > 0.0f) {
            z = true;
        } else {
            z = false;
        }
        mo3318b(z, "You must set the radius for WanderAndShootAIController");
        this.shootEnemyBehaviour = new C6305akR();
        this.f7955WB = new C5280aDg();
        mo3328ld(2);
        mo3327lb(0);
        mo3317b(1, new C1829aV(), mo3316b(new C6822auO(), new C5706aTq(m32719zL()), this.shootEnemyBehaviour));
        mo3317b(0, new C1829aV(), mo3316b(new C6822auO(), this.f7955WB, this.shootEnemyBehaviour));
    }

    @C0064Am(aul = "6dc4f3cb4f0521ef314bce876bdbb288", aum = 0)
    /* renamed from: zR */
    private void m32723zR() {
        this.shootEnemyBehaviour.mo14327jN(m32709aI());
        this.shootEnemyBehaviour.mo14326fe(m32710aJ());
        this.f7955WB.mo8426mm(m32711aK());
    }

    @C0064Am(aul = "2dae389b825553d1a36241b9abcb9769", aum = 0)
    /* renamed from: zT */
    private int m32724zT() {
        switch (aYs()) {
            case 0:
                if (m32719zL().mo9517f((Tuple3d) mo3314al().getPosition()).lengthSquared() > ((double) (m32720zM() + 10000.0f))) {
                    return 1;
                }
                break;
            case 1:
                if (m32719zL().mo9517f((Tuple3d) mo3314al().getPosition()).lengthSquared() < ((double) m32720zM())) {
                    return 0;
                }
                break;
        }
        return -1;
    }

    @C0064Am(aul = "27752ed15736add399db890e5b799869", aum = 0)
    /* renamed from: zV */
    private WanderAndShootAIControllerType m32725zV() {
        return (WanderAndShootAIControllerType) super.mo3334zY();
    }
}
