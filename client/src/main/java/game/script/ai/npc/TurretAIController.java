package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Character;
import game.script.aggro.AggroTable;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.abb.C0332EV;
import logic.abb.C6434amq;
import logic.abb.aNA;
import logic.baa.*;
import logic.data.mbean.C0969OG;
import logic.data.mbean.C1073Ph;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Random;

@C6485anp
@C2712iu
@C5511aMd
/* renamed from: a.aqs  reason: case insensitive filesystem */
/* compiled from: a */
public class TurretAIController extends AIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    /* renamed from: _f_getType_0020_0028_0029Ltaikodom_002fgame_002fscript_002fai_002fnpc_002fAIControllerType_003b */
    public static final C2491fm f5202xed6e425 = null;
    /* renamed from: _f_isEnemy_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029Z */
    public static final C2491fm f5203x72b3cf67 = null;
    public static final C2491fm _f_isRunning_0020_0028_0029Z = null;
    public static final C2491fm _f_offloadInit_0020_0028_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm azU = null;
    public static final C5663aRz dqA = null;
    public static final C5663aRz dqy = null;
    public static final C2491fm hRc = null;
    public static final C2491fm hRd = null;
    public static final C2491fm hRe = null;
    public static final C2491fm hRf = null;
    public static final C2491fm hRg = null;
    public static final C2491fm hRh = null;
    public static final C2491fm hRi = null;
    public static final C2491fm hRj = null;
    public static final long serialVersionUID = 0;
    public static final int hQJ = 0;
    public static final int hQK = 1;
    private static final float hQL = 2.0f;
    private static final float hQM = 2.5f;
    private static final float hQN = 1.0f;
    private static final float hQO = 10.0f;
    private static final float hQP = 0.025f;
    private static final float hQQ = 0.005f;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "76a5c44b292aa01c165d435c1687eb5d", aum = 0)
    @C5454aJy("Time to remove an target from target list (s)\\nUse 0 to never remove")
    private static float dqx = 0.0f;
    @C0064Am(aul = "72d9a7c7634891e68aa76261bfd5bdd6", aum = 1)
    @C5454aJy("How long a target will remain locked (s)\\n(if there is more than one target in list)")
    private static float dqz = 20.0f;

    static {
        m25266V();
    }

    private transient float currentTime;
    private transient aNA hQR;
    private transient float hQS;
    private transient aNU hQT;
    private transient aNU hQU;
    private transient aNU hQV;
    private transient aNU hQW;
    private transient Actor hQX;
    private transient float hQY;
    private transient float hQZ;
    private transient float hRa;
    private transient float hRb;
    private transient Random random;

    public TurretAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TurretAIController(C0712KI ki) {
        super((C5540aNg) null);
        super._m_script_init(ki);
    }

    public TurretAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25266V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 2;
        _m_methodCount = AIController._m_methodCount + 16;
        int i = AIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(TurretAIController.class, "76a5c44b292aa01c165d435c1687eb5d", i);
        dqy = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TurretAIController.class, "72d9a7c7634891e68aa76261bfd5bdd6", i2);
        dqA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i4 = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 16)];
        C2491fm a = C4105zY.m41624a(TurretAIController.class, "60188b12f5ed9a09db542d1f5634bb69", i4);
        _f_setupBehaviours_0020_0028_0029V = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(TurretAIController.class, "7caf16ba92fabe7215815822bcedf903", i5);
        hRc = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(TurretAIController.class, "260c0e55db16d69b5328adc3546b635a", i6);
        _f_step_0020_0028F_0029V = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(TurretAIController.class, "465aa055f45e6506f3c6316b679bf005", i7);
        hRd = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(TurretAIController.class, "c6f9b44ebe38103af854960ea04cefc9", i8);
        _f_changeCurrentAction_0020_0028_0029I = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(TurretAIController.class, "97715fa9ffacfbbd4ccf51ead72301d6", i9);
        hRe = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(TurretAIController.class, "c5cd13c3b436acd63f2109c60119578c", i10);
        hRf = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(TurretAIController.class, "38ba990f4e7f8e0f513b0c16b679ba38", i11);
        hRg = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(TurretAIController.class, "2766e6fd6d7fe69c189d8e7b5f5be5e4", i12);
        azU = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(TurretAIController.class, "1c5781806dec342068e4d44c9c9576ea", i13);
        hRh = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        C2491fm a11 = C4105zY.m41624a(TurretAIController.class, "2b0a6838692c41b55cf38f879e89ea45", i14);
        _f_isRunning_0020_0028_0029Z = a11;
        fmVarArr[i14] = a11;
        int i15 = i14 + 1;
        C2491fm a12 = C4105zY.m41624a(TurretAIController.class, "942f4e8adf8b0d29f4ac099b46f789f7", i15);
        hRi = a12;
        fmVarArr[i15] = a12;
        int i16 = i15 + 1;
        C2491fm a13 = C4105zY.m41624a(TurretAIController.class, "0bd77b728381c5d4d813ad087631696e", i16);
        f5203x72b3cf67 = a13;
        fmVarArr[i16] = a13;
        int i17 = i16 + 1;
        C2491fm a14 = C4105zY.m41624a(TurretAIController.class, "f21d9f1c648a2d6cb96d64dbfa24fcc6", i17);
        hRj = a14;
        fmVarArr[i17] = a14;
        int i18 = i17 + 1;
        C2491fm a15 = C4105zY.m41624a(TurretAIController.class, "c63c39e2814622193e65be6707296d6b", i18);
        _f_offloadInit_0020_0028_0029V = a15;
        fmVarArr[i18] = a15;
        int i19 = i18 + 1;
        C2491fm a16 = C4105zY.m41624a(TurretAIController.class, "a09d2e755dc04ae3ee6ddf05f58f22ef", i19);
        f5202xed6e425 = a16;
        fmVarArr[i19] = a16;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TurretAIController.class, C1073Ph.class, _m_fields, _m_methods);
    }

    /* renamed from: Mj */
    private void m25264Mj() {
        switch (bFf().mo6893i(azU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, azU, new Object[0]));
                break;
        }
        m25263Mi();
    }

    private Turret aPd() {
        switch (bFf().mo6893i(hRh)) {
            case 0:
                return null;
            case 2:
                return (Turret) bFf().mo5606d(new aCE(this, hRh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hRh, new Object[0]));
                break;
        }
        return daz();
    }

    private float bcV() {
        return ((C0712KI) getType()).bcY();
    }

    private float bcW() {
        return ((C0712KI) getType()).bda();
    }

    /* renamed from: c */
    private boolean m25269c(Target aVar) {
        switch (bFf().mo6893i(hRg)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hRg, new Object[]{aVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hRg, new Object[]{aVar}));
                break;
        }
        return m25268b(aVar);
    }

    /* renamed from: cg */
    private void m25271cg(Actor cr) {
        switch (bFf().mo6893i(hRe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hRe, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hRe, new Object[]{cr}));
                break;
        }
        m25270cf(cr);
    }

    /* renamed from: fR */
    private void m25273fR(float f) {
        throw new C6039afL();
    }

    /* renamed from: fS */
    private void m25274fS(float f) {
        throw new C6039afL();
    }

    /* renamed from: mG */
    private void m25277mG(float f) {
        switch (bFf().mo6893i(hRd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hRd, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hRd, new Object[]{new Float(f)}));
                break;
        }
        m25276mF(f);
    }

    @C0064Am(aul = "a09d2e755dc04ae3ee6ddf05f58f22ef", aum = 0)
    /* renamed from: zX */
    private AIControllerType m25280zX() {
        return daB();
    }

    /* renamed from: D */
    public boolean mo3298D(Actor cr) {
        switch (bFf().mo6893i(f5203x72b3cf67)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5203x72b3cf67, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5203x72b3cf67, new Object[]{cr}));
                break;
        }
        return m25262C(cr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1073Ph(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                m25278zP();
                return null;
            case 1:
                m25275jw(((Boolean) args[0]).booleanValue());
                return null;
            case 2:
                m25267ay(((Float) args[0]).floatValue());
                return null;
            case 3:
                m25276mF(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Integer(m25279zT());
            case 5:
                m25270cf((Actor) args[0]);
                return null;
            case 6:
                return new Float(dax());
            case 7:
                return new Boolean(m25268b((Target) args[0]));
            case 8:
                m25263Mi();
                return null;
            case 9:
                return daz();
            case 10:
                return new Boolean(m25265Mp());
            case 11:
                return new Boolean(m25272ch((Actor) args[0]));
            case 12:
                return new Boolean(m25262C((Actor) args[0]));
            case 13:
                return daA();
            case 14:
                aYF();
                return null;
            case 15:
                return m25280zX();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C1253SX
    public void aYG() {
        switch (bFf().mo6893i(_f_offloadInit_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                break;
        }
        aYF();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: ci */
    public boolean mo15673ci(Actor cr) {
        switch (bFf().mo6893i(hRi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hRi, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hRi, new Object[]{cr}));
                break;
        }
        return m25272ch(cr);
    }

    public C0712KI daB() {
        switch (bFf().mo6893i(hRj)) {
            case 0:
                return null;
            case 2:
                return (C0712KI) bFf().mo5606d(new aCE(this, hRj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hRj, new Object[0]));
                break;
        }
        return daA();
    }

    public float day() {
        switch (bFf().mo6893i(hRf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hRf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hRf, new Object[0]));
                break;
        }
        return dax();
    }

    public boolean isRunning() {
        switch (bFf().mo6893i(_f_isRunning_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isRunning_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m25265Mp();
    }

    /* renamed from: jx */
    public void mo15677jx(boolean z) {
        switch (bFf().mo6893i(hRc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hRc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hRc, new Object[]{new Boolean(z)}));
                break;
        }
        m25275jw(z);
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m25267ay(f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m25278zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m25279zT();
    }

    /* renamed from: zY */
    public /* bridge */ /* synthetic */ AIControllerType mo3334zY() {
        switch (bFf().mo6893i(f5202xed6e425)) {
            case 0:
                return null;
            case 2:
                return (AIControllerType) bFf().mo5606d(new aCE(this, f5202xed6e425, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5202xed6e425, new Object[0]));
                break;
        }
        return m25280zX();
    }

    /* renamed from: d */
    public void mo15674d(C0712KI ki) {
        super.mo967a((C2961mJ) ki);
        this.hQS = 0.0f;
        if (ki == null) {
            mo8358lY("TurretAIController created without a type: " + cWm());
        }
    }

    @C0064Am(aul = "60188b12f5ed9a09db542d1f5634bb69", aum = 0)
    /* renamed from: zP */
    private void m25278zP() {
        boolean z;
        mo3318b(aPd().aZS() != null, "The turret is attached to no one");
        if (aPd().bac() != null) {
            z = true;
        } else {
            z = false;
        }
        mo3318b(z, "The turret has no type");
        mo3319cp(0.0f);
        mo3320cq(hQM);
        mo3321cr(hQM);
        if (this.hQV == null) {
            this.hQV = new aNU();
        }
        this.hQV.mo10278D(hQO, 1.0f);
        this.hQV.mo10289nz(360.0f);
        this.hQV.mo10292yZ(4);
        if (this.hQW == null) {
            this.hQW = new aNU();
        }
        this.hQW.mo10278D(hQO, 1.0f);
        this.hQW.mo10289nz(360.0f);
        this.hQW.mo10292yZ(4);
        if (this.hQT == null) {
            this.hQT = new aNU();
        }
        this.hQT.mo10277C(hQQ, hQP);
        this.hQT.mo10289nz(360.0f);
        this.hQT.mo10292yZ(6);
        if (this.hQU == null) {
            this.hQU = new aNU();
        }
        this.hQU.mo10277C(hQQ, hQP);
        this.hQU.mo10289nz(360.0f);
        this.hQU.mo10292yZ(6);
        this.currentTime = 0.0f;
        this.yawController = this.hQV;
        this.pitchController = this.hQW;
        if (this.hQR == null) {
            this.hQR = new aNA();
        }
        mo3328ld(2);
        mo3327lb(0);
        mo3317b(0, mo3316b(new C6434amq(), new C0332EV()));
        mo3317b(1, this.hQR);
    }

    @C0064Am(aul = "7caf16ba92fabe7215815822bcedf903", aum = 0)
    /* renamed from: jw */
    private void m25275jw(boolean z) {
        if (z) {
            if (this.yawController != this.hQT || this.pitchController != this.hQU) {
                this.yawController = this.hQT;
                this.pitchController = this.hQU;
                this.hQT.mo10287nx(aPd().bas());
                this.hQU.mo10287nx(aPd().baq());
            }
        } else if (this.yawController != this.hQV || this.pitchController != this.hQW) {
            this.yawController = this.hQV;
            this.pitchController = this.hQW;
            this.hQV.mo10287nx(aPd().bas());
            this.hQW.mo10287nx(aPd().baq());
        }
    }

    @C0064Am(aul = "260c0e55db16d69b5328adc3546b635a", aum = 0)
    /* renamed from: ay */
    private void m25267ay(float f) {
        if (!aYE()) {
            aYG();
        }
        if (isRunning() && daB() != null) {
            m25277mG(f);
            super.step(f);
        }
    }

    @C0064Am(aul = "465aa055f45e6506f3c6316b679bf005", aum = 0)
    /* renamed from: mF */
    private void m25276mF(float f) {
        if (this.hQS < 1.0f) {
            this.hQS += f;
            return;
        }
        this.hQS = 0.0f;
        AggroTable cyP = aPd().aZS().cyP();
        if (cyP != null) {
            float in = aPd().afR().mo12940in();
            Pawn b = cyP.mo17922b(in, aPd().getPosition());
            if (b != null && !b.isDisposed() && b.isAlive() && b.bae()) {
                m25271cg(b);
            }
            if (this.hQX == null || this.hQX.isDisposed() || !this.hQX.bae() || aYa().mo1013bx(this.hQX) > in * in) {
                m25271cg((Actor) null);
            }
        }
    }

    @C0064Am(aul = "c6f9b44ebe38103af854960ea04cefc9", aum = 0)
    /* renamed from: zT */
    private int m25279zT() {
        switch (aYs()) {
            case 0:
                this.yawController = null;
                this.pitchController = null;
                if (this.hQX != null) {
                    this.yawController = this.hQV;
                    this.hQV.mo10287nx(aPd().bas());
                    this.pitchController = this.hQW;
                    this.hQW.mo10287nx(aPd().baq());
                    return 1;
                }
                break;
            case 1:
                if (this.hQX == null || this.hQX.isDisposed() || !this.hQX.bae()) {
                    m25271cg((Actor) null);
                    this.yawController = null;
                    this.pitchController = null;
                    return 0;
                }
        }
        return -1;
    }

    @C0064Am(aul = "97715fa9ffacfbbd4ccf51ead72301d6", aum = 0)
    /* renamed from: cf */
    private void m25270cf(Actor cr) {
        if (cr != this.hQX) {
            this.hQX = cr;
            this.hQR.mo10203X(cr);
        }
    }

    @C0064Am(aul = "c5cd13c3b436acd63f2109c60119578c", aum = 0)
    private float dax() {
        return bcW();
    }

    @C0064Am(aul = "38ba990f4e7f8e0f513b0c16b679ba38", aum = 0)
    /* renamed from: b */
    private boolean m25268b(Target aVar) {
        return bcV() > 0.0f && aVar.crq() + bcV() < this.currentTime;
    }

    @C0064Am(aul = "2766e6fd6d7fe69c189d8e7b5f5be5e4", aum = 0)
    /* renamed from: Mi */
    private void m25263Mi() {
        if (this.random == null) {
            this.random = new Random(currentTimeMillis());
        }
        if (this.hQY == 0.0f && this.hRa == 0.0f && this.hQZ == 0.0f && this.hRb == 0.0f) {
            this.hQY = -2.0f;
            this.hRa = -2.0f;
            this.hQZ = 2.0f;
            this.hRb = 2.0f;
            float maxPitch = (float) ((((double) aPd().bac().getMaxPitch()) * 3.141592653589793d) / 180.0d);
            float clO = (float) ((((double) aPd().bac().clO()) * 3.141592653589793d) / 180.0d);
            float cos = (float) Math.cos((double) clO);
            float cos2 = (float) Math.cos((double) maxPitch);
            if (maxPitch > 0.0f) {
                this.hQZ = cos2;
                if (clO <= 0.0f) {
                    this.hQY = 1.0f;
                }
            } else {
                this.hRa = cos2;
            }
            if (clO > 0.0f) {
                this.hQY = cos;
                return;
            }
            this.hRb = cos;
            if (maxPitch >= 0.0f) {
                this.hRa = 1.0f;
            }
        }
    }

    @C0064Am(aul = "1c5781806dec342068e4d44c9c9576ea", aum = 0)
    private Turret daz() {
        return (Turret) aYa();
    }

    @C0064Am(aul = "2b0a6838692c41b55cf38f879e89ea45", aum = 0)
    /* renamed from: Mp */
    private boolean m25265Mp() {
        return aYs() >= 0 && aYa() != null && aPd().aZS() != null && aPd().aZS().bae();
    }

    @C0064Am(aul = "942f4e8adf8b0d29f4ac099b46f789f7", aum = 0)
    /* renamed from: ch */
    private boolean m25272ch(Actor cr) {
        if (cr == null || !cr.bae()) {
            return false;
        }
        if (!(cr instanceof Ship) || !((Ship) cr).isDead()) {
            return mo3298D(cr);
        }
        return false;
    }

    @C0064Am(aul = "0bd77b728381c5d4d813ad087631696e", aum = 0)
    /* renamed from: C */
    private boolean m25262C(Actor cr) {
        Character az = mo3315az(aYa());
        if (az == null) {
            return false;
        }
        Character az2 = mo3315az(cr);
        if (az2 == null) {
            return false;
        }
        if (!(az instanceof Player) || !(az2 instanceof Player)) {
            return super.mo3298D(cr);
        }
        if (!((Player) az).dxk()) {
            return true;
        }
        if (!((Player) az2).dxk()) {
            return true;
        }
        return ((Player) az).dxi() != ((Player) az2).dxi();
    }

    @C0064Am(aul = "f21d9f1c648a2d6cb96d64dbfa24fcc6", aum = 0)
    private C0712KI daA() {
        return (C0712KI) super.mo3334zY();
    }

    @C0064Am(aul = "c63c39e2814622193e65be6707296d6b", aum = 0)
    @C1253SX
    private void aYF() {
        this.hQS = 0.0f;
        super.aYG();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aqs$a */
    public static class Target extends TaikodomObject implements C1616Xf {

        /* renamed from: Kl */
        public static final C2491fm f5204Kl = null;

        /* renamed from: Km */
        public static final C2491fm f5205Km = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C5663aRz gqw = null;
        public static final C5663aRz gqx = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "8e40402bc2426950c26087a09985b69f", aum = 1)
        private static float dNx;
        @C0064Am(aul = "83b07c3ee75f3acbc7e141f5cb945cfd", aum = 0)

        /* renamed from: pI */
        private static Actor f5206pI;

        static {
            m25295V();
        }

        public Target() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public Target(Actor cr, float f) {
            super((C5540aNg) null);
            super._m_script_init(cr, f);
        }

        public Target(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m25295V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaikodomObject._m_fieldCount + 2;
            _m_methodCount = TaikodomObject._m_methodCount + 2;
            int i = TaikodomObject._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 2)];
            C5663aRz b = C5640aRc.m17844b(Target.class, "83b07c3ee75f3acbc7e141f5cb945cfd", i);
            gqw = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(Target.class, "8e40402bc2426950c26087a09985b69f", i2);
            gqx = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
            int i4 = TaikodomObject._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i4 + 2)];
            C2491fm a = C4105zY.m41624a(Target.class, "323d3623571af943c9230005b3bc3a2b", i4);
            f5205Km = a;
            fmVarArr[i4] = a;
            int i5 = i4 + 1;
            C2491fm a2 = C4105zY.m41624a(Target.class, "7839739fd46dd1bab38c0e16fba208b2", i5);
            f5204Kl = a2;
            fmVarArr[i5] = a2;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(Target.class, C0969OG.class, _m_fields, _m_methods);
        }

        /* renamed from: aW */
        private void m25297aW(Actor cr) {
            bFf().mo5608dq().mo3197f(gqw, cr);
        }

        private Actor crp() {
            return (Actor) bFf().mo5608dq().mo3214p(gqw);
        }

        /* access modifiers changed from: private */
        public float crq() {
            return bFf().mo5608dq().mo3211m(gqx);
        }

        /* renamed from: kv */
        private void m25298kv(float f) {
            bFf().mo5608dq().mo3150a(gqx, f);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0969OG(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
                case 0:
                    return new Boolean(m25300v(args[0]));
                case 1:
                    return new Integer(m25299qk());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public boolean equals(Object obj) {
            switch (bFf().mo6893i(f5205Km)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f5205Km, new Object[]{obj}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f5205Km, new Object[]{obj}));
                    break;
            }
            return m25300v(obj);
        }

        public int hashCode() {
            switch (bFf().mo6893i(f5204Kl)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, f5204Kl, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f5204Kl, new Object[0]));
                    break;
            }
            return m25299qk();
        }

        /* renamed from: d */
        public void mo15678d(Actor cr, float f) {
            super.mo10S();
            m25297aW(cr);
            m25298kv(f);
        }

        @C0064Am(aul = "323d3623571af943c9230005b3bc3a2b", aum = 0)
        /* renamed from: v */
        private boolean m25300v(Object obj) {
            if (!(obj instanceof Target)) {
                return false;
            }
            if (crp() != null) {
                return crp().equals(((Target) obj).crp());
            }
            if (((Target) obj).crp() == null) {
                return true;
            }
            return false;
        }

        @C0064Am(aul = "7839739fd46dd1bab38c0e16fba208b2", aum = 0)
        /* renamed from: qk */
        private int m25299qk() {
            if (crp() == null) {
                return 0;
            }
            return crp().hashCode();
        }
    }
}
