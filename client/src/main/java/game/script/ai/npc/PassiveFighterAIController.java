package game.script.ai.npc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import logic.abb.C5706aTq;
import logic.abb.C6822auO;
import logic.abb.aER;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3195oy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aOz  reason: case insensitive filesystem */
/* compiled from: a */
public class PassiveFighterAIController extends TrackShipAIController implements C1616Xf, aJO {
    public static final C5663aRz _f_center = null;
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    /* renamed from: _f_getCenter_0020_0028_0029Lcom_002fhoplon_002fgeometry_002fVec3d_003b */
    public static final C2491fm f3491xdc7de0cc = null;
    public static final C2491fm _f_isValidTrackedShip_0020_0028_0029Z = null;
    /* renamed from: _f_onDamageTaken_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003bLtaikodom_002fgame_002fDamage_003bLcom_002fhoplon_002fgeometry_002fVec3f_003b_0029V */
    public static final C2491fm f3492xbe599734 = null;
    public static final C5663aRz _f_radius2 = null;
    /* renamed from: _f_setCenter_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003b_0029V */
    public static final C2491fm f3493x6107980 = null;
    public static final C2491fm _f_setRadius_0020_0028F_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bcF = null;
    public static final C2491fm gAb = null;
    public static final C2491fm izF = null;
    public static final C2491fm izG = null;
    public static final C2491fm izH = null;
    public static final long serialVersionUID = 0;
    public static final int FIGHTING = 2;
    public static final int RETURNING_TO_SPHERE = 1;
    public static final int WANDERING = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "72b95085ed8e8aa8b204d6667666ef82", aum = 0)
    private static Vec3d center;
    @C0064Am(aul = "8313b3693098a7a22f1024b18c591766", aum = 1)
    private static float radius2;

    static {
        m16987V();
    }

    private transient C5706aTq fra;
    private transient C6822auO izE;
    private transient aER pursuitBehaviour;

    public PassiveFighterAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PassiveFighterAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16987V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TrackShipAIController._m_fieldCount + 2;
        _m_methodCount = TrackShipAIController._m_methodCount + 12;
        int i = TrackShipAIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(PassiveFighterAIController.class, "72b95085ed8e8aa8b204d6667666ef82", i);
        _f_center = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PassiveFighterAIController.class, "8313b3693098a7a22f1024b18c591766", i2);
        _f_radius2 = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_fields, (Object[]) _m_fields);
        int i4 = TrackShipAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 12)];
        C2491fm a = C4105zY.m41624a(PassiveFighterAIController.class, "036979e5d43c38ebee0e7d4477a5d54b", i4);
        _f_setupBehaviours_0020_0028_0029V = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(PassiveFighterAIController.class, "ff99823ac5c6ac886b4b26684d6eaa95", i5);
        _f_changeCurrentAction_0020_0028_0029I = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(PassiveFighterAIController.class, "d2dc6ad92ba145f11b40f2f93f59a647", i6);
        _f_isValidTrackedShip_0020_0028_0029Z = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(PassiveFighterAIController.class, "c0f08571dfea0d1f34face7315708b0c", i7);
        f3492xbe599734 = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(PassiveFighterAIController.class, "68b452017987fedbdf45e20912e7a70a", i8);
        izF = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(PassiveFighterAIController.class, "7a05470cc311f1cb0edbe29ed205b788", i9);
        izG = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(PassiveFighterAIController.class, "b423b69598f5e75103bce6c67cb43780", i10);
        f3491xdc7de0cc = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(PassiveFighterAIController.class, "023100a7c3e2682744930c63930ed131", i11);
        f3493x6107980 = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(PassiveFighterAIController.class, "8df4457620b9324e04060451afad48a4", i12);
        gAb = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(PassiveFighterAIController.class, "503ea5f38eee62124de8a352ecf6f874", i13);
        _f_setRadius_0020_0028F_0029V = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        C2491fm a11 = C4105zY.m41624a(PassiveFighterAIController.class, "b218e5ff397df859653549a224fbc5f0", i14);
        izH = a11;
        fmVarArr[i14] = a11;
        int i15 = i14 + 1;
        C2491fm a12 = C4105zY.m41624a(PassiveFighterAIController.class, "b7047923eb3441969fc334b0c01eef3b", i15);
        bcF = a12;
        fmVarArr[i15] = a12;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PassiveFighterAIController.class, C3195oy.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "c0f08571dfea0d1f34face7315708b0c", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m16989a(Actor cr, C5260aCm acm, Vec3f vec3f) {
        throw new aWi(new aCE(this, f3492xbe599734, new Object[]{cr, acm, vec3f}));
    }

    @C0064Am(aul = "503ea5f38eee62124de8a352ecf6f874", aum = 0)
    @C5566aOg
    /* renamed from: aA */
    private void m16990aA(float f) {
        throw new aWi(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
    }

    /* renamed from: az */
    private void m16991az(float f) {
        bFf().mo5608dq().mo3150a(_f_radius2, f);
    }

    @C0064Am(aul = "d2dc6ad92ba145f11b40f2f93f59a647", aum = 0)
    @C5566aOg
    private boolean bnI() {
        throw new aWi(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]));
    }

    @C0064Am(aul = "8df4457620b9324e04060451afad48a4", aum = 0)
    @C5566aOg
    private float cvR() {
        throw new aWi(new aCE(this, gAb, new Object[0]));
    }

    @C0064Am(aul = "68b452017987fedbdf45e20912e7a70a", aum = 0)
    @C5566aOg
    private C6822auO dnE() {
        throw new aWi(new aCE(this, izF, new Object[0]));
    }

    @C0064Am(aul = "7a05470cc311f1cb0edbe29ed205b788", aum = 0)
    @C5566aOg
    private C5706aTq dnG() {
        throw new aWi(new aCE(this, izG, new Object[0]));
    }

    @C0064Am(aul = "b218e5ff397df859653549a224fbc5f0", aum = 0)
    @C5566aOg
    private aER dnI() {
        throw new aWi(new aCE(this, izH, new Object[0]));
    }

    /* renamed from: f */
    private void m16992f(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(_f_center, ajr);
    }

    @C0064Am(aul = "023100a7c3e2682744930c63930ed131", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m16993g(Vec3d ajr) {
        throw new aWi(new aCE(this, f3493x6107980, new Object[]{ajr}));
    }

    /* renamed from: zL */
    private Vec3d m16994zL() {
        return (Vec3d) bFf().mo5608dq().mo3214p(_f_center);
    }

    /* renamed from: zM */
    private float m16995zM() {
        return bFf().mo5608dq().mo3211m(_f_radius2);
    }

    @C0064Am(aul = "b423b69598f5e75103bce6c67cb43780", aum = 0)
    @C5566aOg
    /* renamed from: zN */
    private Vec3d m16996zN() {
        throw new aWi(new aCE(this, f3491xdc7de0cc, new Object[0]));
    }

    @C0064Am(aul = "036979e5d43c38ebee0e7d4477a5d54b", aum = 0)
    @C5566aOg
    /* renamed from: zP */
    private void m16997zP() {
        throw new aWi(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "ff99823ac5c6ac886b4b26684d6eaa95", aum = 0)
    @C5566aOg
    /* renamed from: zT */
    private int m16998zT() {
        throw new aWi(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3195oy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TrackShipAIController._m_methodCount) {
            case 0:
                m16997zP();
                return null;
            case 1:
                return new Integer(m16998zT());
            case 2:
                return new Boolean(bnI());
            case 3:
                m16989a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2]);
                return null;
            case 4:
                return dnE();
            case 5:
                return dnG();
            case 6:
                return m16996zN();
            case 7:
                m16993g((Vec3d) args[0]);
                return null;
            case 8:
                return new Float(cvR());
            case 9:
                m16990aA(((Float) args[0]).floatValue());
                return null;
            case 10:
                return dnI();
            case 11:
                return new Float(m16988Zg());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo3290b(Actor cr, C5260aCm acm, Vec3f vec3f) {
        switch (bFf().mo6893i(f3492xbe599734)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3492xbe599734, new Object[]{cr, acm, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3492xbe599734, new Object[]{cr, acm, vec3f}));
                break;
        }
        m16989a(cr, acm, vec3f);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public float bcR() {
        switch (bFf().mo6893i(gAb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gAb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAb, new Object[0]));
                break;
        }
        return cvR();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public boolean bnJ() {
        switch (bFf().mo6893i(_f_isValidTrackedShip_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]));
                break;
        }
        return bnI();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public C6822auO dnF() {
        switch (bFf().mo6893i(izF)) {
            case 0:
                return null;
            case 2:
                return (C6822auO) bFf().mo5606d(new aCE(this, izF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, izF, new Object[0]));
                break;
        }
        return dnE();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public C5706aTq dnH() {
        switch (bFf().mo6893i(izG)) {
            case 0:
                return null;
            case 2:
                return (C5706aTq) bFf().mo5606d(new aCE(this, izG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, izG, new Object[0]));
                break;
        }
        return dnG();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public aER dnJ() {
        switch (bFf().mo6893i(izH)) {
            case 0:
                return null;
            case 2:
                return (aER) bFf().mo5606d(new aCE(this, izH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, izH, new Object[0]));
                break;
        }
        return dnI();
    }

    public float getRadius() {
        switch (bFf().mo6893i(bcF)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bcF, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcF, new Object[0]));
                break;
        }
        return m16988Zg();
    }

    @C5566aOg
    public void setRadius(float f) {
        switch (bFf().mo6893i(_f_setRadius_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m16990aA(f);
    }

    @C5566aOg
    /* renamed from: h */
    public void mo4610h(Vec3d ajr) {
        switch (bFf().mo6893i(f3493x6107980)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3493x6107980, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3493x6107980, new Object[]{ajr}));
                break;
        }
        m16993g(ajr);
    }

    @C5566aOg
    /* renamed from: zO */
    public Vec3d mo10690zO() {
        switch (bFf().mo6893i(f3491xdc7de0cc)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, f3491xdc7de0cc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3491xdc7de0cc, new Object[0]));
                break;
        }
        return m16996zN();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m16997zP();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m16998zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m16991az(-1.0f);
    }

    @C0064Am(aul = "b7047923eb3441969fc334b0c01eef3b", aum = 0)
    /* renamed from: Zg */
    private float m16988Zg() {
        return (float) Math.sqrt((double) m16995zM());
    }
}
