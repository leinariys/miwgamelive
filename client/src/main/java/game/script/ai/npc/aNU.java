package game.script.ai.npc;

/* renamed from: a.aNU */
/* compiled from: a */
public class aNU {
    private float ixo;
    private float ixp;
    private float ixq;
    private float ixr;
    private float ixs;
    private float ixt;
    private float ixu;

    public aNU(float f, float f2, float f3) {
        this.ixu = -1.0f;
        this.ixo = f;
        this.ixp = f2;
        this.ixq = f3;
        this.ixr = 0.0f;
        this.ixs = 0.0f;
        this.ixt = -1.0f;
    }

    public aNU() {
        this.ixu = -1.0f;
        this.ixo = 1.0f;
        this.ixp = 0.0f;
        this.ixq = 0.0f;
        this.ixr = 0.0f;
        this.ixs = 0.0f;
        this.ixt = -1.0f;
    }

    public float dkR() {
        return this.ixq;
    }

    /* renamed from: nt */
    public void mo10284nt(float f) {
        this.ixq = f;
        reset();
    }

    public float dkS() {
        return this.ixp;
    }

    /* renamed from: nu */
    public void mo10285nu(float f) {
        this.ixp = f;
        reset();
    }

    public float dkT() {
        return this.ixo;
    }

    /* renamed from: nv */
    public void mo10286nv(float f) {
        this.ixo = f;
        reset();
    }

    /* renamed from: A */
    public float mo10275A(float f, float f2) {
        float f3 = (((f - this.ixs) / f2) * this.ixq) + (this.ixo * f) + (this.ixr * this.ixp);
        if (!m16589nw(f3)) {
            this.ixr = f * f2;
            this.ixs = f;
            return f;
        }
        this.ixr += f * f2;
        this.ixs = f3;
        dkU();
        return f3;
    }

    /* renamed from: nw */
    private boolean m16589nw(float f) {
        if (this.ixu < 0.0f) {
            return true;
        }
        if (f > this.ixu || f < (-this.ixu)) {
            return false;
        }
        return true;
    }

    private void dkU() {
        if (this.ixt != -1.0f) {
            while (this.ixr > this.ixt) {
                this.ixr -= this.ixt;
            }
            while (this.ixr < (-this.ixt)) {
                this.ixr += this.ixt;
            }
        }
    }

    /* renamed from: nx */
    public void mo10287nx(float f) {
        this.ixr = 0.0f;
        this.ixs = f;
    }

    public void reset() {
        this.ixr = 0.0f;
        this.ixs = 0.0f;
    }

    /* renamed from: ny */
    public void mo10288ny(float f) {
        this.ixs = f;
    }

    public float dkV() {
        return this.ixt;
    }

    /* renamed from: nz */
    public void mo10289nz(float f) {
        this.ixt = f;
    }

    /* renamed from: yZ */
    public void mo10292yZ(int i) {
        this.ixu = (float) Math.pow(10.0d, (double) i);
    }

    /* renamed from: B */
    public void mo10276B(float f, float f2) {
        this.ixo = 2.0f * f * f2;
        this.ixp = f2 * f2;
        this.ixq = f;
        reset();
    }

    /* renamed from: C */
    public void mo10277C(float f, float f2) {
        this.ixo = f2 / f;
        this.ixp = 0.0f;
        this.ixq = 0.0f;
        reset();
    }

    /* renamed from: D */
    public void mo10278D(float f, float f2) {
        this.ixo = (0.9f * f2) / f;
        this.ixp = this.ixo / (f / 0.3f);
        this.ixq = this.ixo * 0.0f;
        reset();
    }

    /* renamed from: E */
    public void mo10279E(float f, float f2) {
        this.ixo = (1.2f * f2) / f;
        this.ixp = this.ixo / (2.0f * f);
        this.ixq = this.ixo * 0.5f * f;
        reset();
    }

    public String toString() {
        return "PID Controller: [P " + this.ixo + " I " + this.ixp + " D " + this.ixq + " Accumulator: " + this.ixr + " ]";
    }
}
