package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C0757Kp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.TZ */
/* compiled from: a */
public abstract class AIControllerType extends BaseTaikodomContent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz emA = null;
    public static final C2491fm emB = null;
    public static final C2491fm emC = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b0c6833dd77c94aa128b95e2f510a82f", aum = 0)
    private static int nanFloodControl;

    static {
        m10029V();
    }

    public AIControllerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AIControllerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10029V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 1;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 2;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(AIControllerType.class, "b0c6833dd77c94aa128b95e2f510a82f", i);
        emA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i3 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(AIControllerType.class, "aac11facdbe1fb449f4bb921dbbab0ea", i3);
        emB = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(AIControllerType.class, "336d33c74fe2cba2494227f6af803938", i4);
        emC = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AIControllerType.class, C0757Kp.class, _m_fields, _m_methods);
    }

    private int bxf() {
        return bFf().mo5608dq().mo3212n(emA);
    }

    /* renamed from: nw */
    private void m10030nw(int i) {
        bFf().mo5608dq().mo3183b(emA, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0757Kp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return new Integer(bxg());
            case 1:
                m10031nx(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Nan Flood Control")
    public int bxh() {
        switch (bFf().mo6893i(emB)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, emB, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, emB, new Object[0]));
                break;
        }
        return bxg();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Nan Flood Control")
    /* renamed from: ny */
    public void mo5694ny(int i) {
        switch (bFf().mo6893i(emC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emC, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emC, new Object[]{new Integer(i)}));
                break;
        }
        m10031nx(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m10030nw(0);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Nan Flood Control")
    @C0064Am(aul = "aac11facdbe1fb449f4bb921dbbab0ea", aum = 0)
    private int bxg() {
        return bxf();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Nan Flood Control")
    @C0064Am(aul = "336d33c74fe2cba2494227f6af803938", aum = 0)
    /* renamed from: nx */
    private void m10031nx(int i) {
        m10030nw(i);
    }
}
