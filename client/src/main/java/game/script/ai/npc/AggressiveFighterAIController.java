package game.script.ai.npc;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3215pJ;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import javax.vecmath.Tuple3d;
import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.uh */
/* compiled from: a */
public class AggressiveFighterAIController extends PassiveFighterAIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    /* renamed from: _f_onDamageTaken_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003bLtaikodom_002fgame_002fDamage_003bLcom_002fhoplon_002fgeometry_002fVec3f_003b_0029V */
    public static final C2491fm f9367xbe599734 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bvc = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m40091V();
    }

    public AggressiveFighterAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AggressiveFighterAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40091V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = PassiveFighterAIController._m_fieldCount + 0;
        _m_methodCount = PassiveFighterAIController._m_methodCount + 3;
        _m_fields = new C5663aRz[(PassiveFighterAIController._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) PassiveFighterAIController._m_fields, (Object[]) _m_fields);
        int i = PassiveFighterAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 3)];
        C2491fm a = C4105zY.m41624a(AggressiveFighterAIController.class, "524fe6d1444c71254eef6ca67ba751d6", i);
        _f_changeCurrentAction_0020_0028_0029I = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(AggressiveFighterAIController.class, "e66d587441261f678dbcb7e55665f424", i2);
        bvc = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(AggressiveFighterAIController.class, "28d4dda6f3166029465208f21be27ac6", i3);
        f9367xbe599734 = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) PassiveFighterAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AggressiveFighterAIController.class, C3215pJ.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "28d4dda6f3166029465208f21be27ac6", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m40092a(Actor cr, C5260aCm acm, Vec3f vec3f) {
        throw new aWi(new aCE(this, f9367xbe599734, new Object[]{cr, acm, vec3f}));
    }

    private Ship ajd() {
        switch (bFf().mo6893i(bvc)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, bvc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bvc, new Object[0]));
                break;
        }
        return ajc();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3215pJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - PassiveFighterAIController._m_methodCount) {
            case 0:
                return new Integer(m40093zT());
            case 1:
                return ajc();
            case 2:
                m40092a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo3290b(Actor cr, C5260aCm acm, Vec3f vec3f) {
        switch (bFf().mo6893i(f9367xbe599734)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9367xbe599734, new Object[]{cr, acm, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9367xbe599734, new Object[]{cr, acm, vec3f}));
                break;
        }
        m40092a(cr, acm, vec3f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m40093zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "524fe6d1444c71254eef6ca67ba751d6", aum = 0)
    /* renamed from: zT */
    private int m40093zT() {
        Ship ajd;
        if (aYs() != 0) {
            return super.mo2801zU();
        }
        if (mo10690zO().mo9517f((Tuple3d) mo3314al().getPosition()).lengthSquared() >= ((double) bcR())) {
            dnH().mo11544q(mo10690zO());
            return 1;
        } else if (aYJ() == null || (ajd = ajd()) == null) {
            return -1;
        } else {
            mo4588J(ajd);
            dnJ().mo8609J(ajd);
            dnF().mo16334a(ajd);
            return 2;
        }
    }

    @C0064Am(aul = "e66d587441261f678dbcb7e55665f424", aum = 0)
    private Ship ajc() {
        float f = Float.MAX_VALUE;
        Ship fAVar = null;
        for (Actor next : mo3301Mn()) {
            if (next instanceof Ship) {
                Ship fAVar2 = (Ship) next;
                if (mo3298D(fAVar2)) {
                    float bB = mo3314al().mo1001bB((Actor) fAVar2);
                    if (bB < f) {
                        f = bB;
                        fAVar = fAVar2;
                    }
                }
            }
        }
        return fAVar;
    }
}
