package game.script.ai.npc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.ship.Ship;
import logic.abb.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6815auH;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import javax.vecmath.Tuple3d;
import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aeM  reason: case insensitive filesystem */
/* compiled from: a */
public class FollowerController extends TrackShipAIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    /* renamed from: _f_getTrackedShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f4386x798f6a5d = null;
    public static final C2491fm _f_onPawnExplode_0020_0028_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bvc = null;
    public static final C5663aRz cfD = null;
    public static final C5663aRz ffU = null;
    public static final C5663aRz fqO = null;
    public static final C5663aRz fqQ = null;
    public static final C5663aRz fqS = null;
    public static final C5663aRz fqU = null;
    public static final C5663aRz fqW = null;
    public static final C2491fm frb = null;
    public static final C2491fm frc = null;
    public static final C2491fm frd = null;
    public static final C2491fm fre = null;
    public static final long serialVersionUID = 0;
    public static final int fqL = 0;
    public static final int fqM = 1;
    private static final float fqJ = 300.0f;
    private static final int fqK = 5000;
    public static C6494any ___iScriptClass = null;
    static String[] names = {"Adriano", "Afonso", "Akio", "Albrecht", "Alexandre", "Alves", "Alysson", "Anderson", "Andrade", "Andre", "Andreas", "Arantes", "Arthur", "Atila", "Azeredo", "Balzer", "Baran", "Benedet", "Bernardes", "Bernz", "Berti", "Bertoncini", "Bittencourt", "Boesel", "Bombasar", "Boneti", "Borem", "Borges", "Bruno", "Carlo", "Carolina", "Carrijo", "Cechinel", "Cesar", "Conte", "Cordeiro", "Correa", "Coutinh", "Cuareli", "Daniel", "Deitos", "Diego", "Doneda", "Dotto", "Eduardo", "Erico", "Fabio", "Felipe", "Felippe", "Feranti", "Fernandes", "Fernando", "Ferreira", "Flavio", "Fonseca", "Francisco", "Freitas", "Garcia", "Gibran", "Giorgio", "Giovani", "Giudice", "Giuliano", "Gomes", "Gustavo", "Heniique", "Henrique", "Hirata", "Irving", "Issao", "Ivan", "Joao", "Johnny", "Jose", "Junior", "Karine", "Kilian", "Kleinmayer", "Laura", "Leal", "Leonardo", "Lima", "Losso", "Lucas", "Luiz", "Machado", "Marcel", "Marcella", "Marcelo", "Martins", "Mateus", "Mattos", "Mauro", "Mazoni", "Meer", "Merli", "Mitsunaga", "Mongruel", "Moritz", "Muller", "Muraro", "Navarro", "Orsini", "Osnei", "Padilha", "Paniago", "Patrick", "Paz", "Pedro", "Penteado", "Pereira", "Petrazzini", "Piekarski", "Pontara", "Priscila", "Rafael", "Ramon", "Ramos", "Regis", "Renato", "Rodrigo", "Rodrigues", "Rui", "Ruwer", "Saad", "Salgado", "Sangoi", "Santos", "Schneider", "Schurt", "Serafim", "Sfredo", "Sherfis", "Shiomi", "Silva", "Smaniotto", "Soares", "Souza", "Stradiotto", "Tacla", "Teixeira", "Tiago", "Timboni", "Tobias", "Tumelero", "Valim", "Van", "Vegini", "Weege", "Yamamoto", "Zacchello", "Zerbinatti"};
    @C0064Am(aul = "b0bfbbba44af41c41aedee8790700284", aum = 5)
    private static NPCParty azb = null;
    @C0064Am(aul = "8cb0f5e4d45fcd74dde2655e9502ee9c", aum = 6)
    private static Controller cCG = null;
    @C0064Am(aul = "57907b60d0141b2ca56f5c4c701f3a67", aum = 0)
    private static float fqN;
    @C0064Am(aul = "2d23e2b5cd8362a8dd41b1159adde8b1", aum = 1)
    private static float fqP;
    @C0064Am(aul = "d00a6819f31f87accc091e5a954bffc3", aum = 2)
    private static float fqR;
    @C0064Am(aul = "5d0e2f3185b9a9800886a9021daac041", aum = 3)
    private static long fqT;
    @C0064Am(aul = "0fb85f07fb0107b3e0891a08eb28b751", aum = 4)
    private static Vec3d fqV;

    static {
        m21189V();
    }

    private transient C6822auO avoidObstaclesBehaviour;
    private transient aER fqX;
    private transient aER fqY;
    private transient C1829aV fqZ;
    private transient C5706aTq fra;
    private transient C6305akR shootEnemyBehaviour;

    public FollowerController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FollowerController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m21189V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TrackShipAIController._m_fieldCount + 7;
        _m_methodCount = TrackShipAIController._m_methodCount + 10;
        int i = TrackShipAIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(FollowerController.class, "57907b60d0141b2ca56f5c4c701f3a67", i);
        fqO = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(FollowerController.class, "2d23e2b5cd8362a8dd41b1159adde8b1", i2);
        fqQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(FollowerController.class, "d00a6819f31f87accc091e5a954bffc3", i3);
        fqS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(FollowerController.class, "5d0e2f3185b9a9800886a9021daac041", i4);
        fqU = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(FollowerController.class, "0fb85f07fb0107b3e0891a08eb28b751", i5);
        fqW = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(FollowerController.class, "b0bfbbba44af41c41aedee8790700284", i6);
        cfD = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(FollowerController.class, "8cb0f5e4d45fcd74dde2655e9502ee9c", i7);
        ffU = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_fields, (Object[]) _m_fields);
        int i9 = TrackShipAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 10)];
        C2491fm a = C4105zY.m41624a(FollowerController.class, "f03f177a5d3781345fcf688836f771be", i9);
        frb = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(FollowerController.class, "8dd860df16d0011c55de0315623a5a59", i10);
        _f_changeCurrentAction_0020_0028_0029I = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(FollowerController.class, "7c2b1fff2493a4692baea048f449500d", i11);
        _f_setupBehaviours_0020_0028_0029V = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(FollowerController.class, "15f941e3704de50d44c22ca3a7523c4d", i12);
        bvc = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(FollowerController.class, "0211778897806f5564bf0c0f462683e3", i13);
        _f_step_0020_0028F_0029V = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(FollowerController.class, "34450e5f5444b6bf54585f92ae9d831c", i14);
        frc = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(FollowerController.class, "54dc8c802b8b6be246c0336617d8c7b9", i15);
        f4386x798f6a5d = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(FollowerController.class, "2dbf1c071fb6eb73fa2b8ed4732c15dd", i16);
        _f_onPawnExplode_0020_0028_0029V = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(FollowerController.class, "d24406f3cc05ba57d0412a0ecea3e2f5", i17);
        frd = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(FollowerController.class, "a997acb922c5ae61c77c3d6a479d4c0d", i18);
        fre = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FollowerController.class, C6815auH.class, _m_fields, _m_methods);
    }

    public static String bUR() {
        return String.valueOf(names[(int) (Math.random() * ((double) names.length))]) + " " + names[(int) (Math.random() * ((double) names.length))];
    }

    /* renamed from: U */
    private void m21188U(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(fqW, ajr);
    }

    /* renamed from: a */
    private void m21190a(NPCParty dv) {
        bFf().mo5608dq().mo3197f(cfD, dv);
    }

    /* renamed from: a */
    private void m21191a(Controller jx) {
        bFf().mo5608dq().mo3197f(ffU, jx);
    }

    @C0064Am(aul = "15f941e3704de50d44c22ca3a7523c4d", aum = 0)
    @C5566aOg
    private Ship ajc() {
        throw new aWi(new aCE(this, bvc, new Object[0]));
    }

    @C5566aOg
    private Ship ajd() {
        switch (bFf().mo6893i(bvc)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, bvc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bvc, new Object[0]));
                break;
        }
        return ajc();
    }

    private Controller bQp() {
        return (Controller) bFf().mo5608dq().mo3214p(ffU);
    }

    private float bUJ() {
        return bFf().mo5608dq().mo3211m(fqO);
    }

    private float bUK() {
        return bFf().mo5608dq().mo3211m(fqQ);
    }

    private float bUL() {
        return bFf().mo5608dq().mo3211m(fqS);
    }

    private long bUM() {
        return bFf().mo5608dq().mo3213o(fqU);
    }

    private Vec3d bUN() {
        return (Vec3d) bFf().mo5608dq().mo3214p(fqW);
    }

    private NPCParty bUO() {
        return (NPCParty) bFf().mo5608dq().mo3214p(cfD);
    }

    @C0064Am(aul = "f03f177a5d3781345fcf688836f771be", aum = 0)
    @C5566aOg
    private boolean bUP() {
        throw new aWi(new aCE(this, frb, new Object[0]));
    }

    @C5566aOg
    private boolean bUQ() {
        switch (bFf().mo6893i(frb)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, frb, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, frb, new Object[0]));
                break;
        }
        return bUP();
    }

    /* renamed from: gZ */
    private void m21195gZ(long j) {
        bFf().mo5608dq().mo3184b(fqU, j);
    }

    /* renamed from: jk */
    private void m21196jk(float f) {
        bFf().mo5608dq().mo3150a(fqO, f);
    }

    /* renamed from: jl */
    private void m21197jl(float f) {
        bFf().mo5608dq().mo3150a(fqQ, f);
    }

    /* renamed from: jm */
    private void m21198jm(float f) {
        bFf().mo5608dq().mo3150a(fqS, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6815auH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TrackShipAIController._m_methodCount) {
            case 0:
                return new Boolean(bUP());
            case 1:
                return new Integer(m21200zT());
            case 2:
                m21199zP();
                return null;
            case 3:
                return ajc();
            case 4:
                m21192ay(((Float) args[0]).floatValue());
                return null;
            case 5:
                m21194d((Controller) args[0]);
                return null;
            case 6:
                return bUS();
            case 7:
                aYf();
                return null;
            case 8:
                return bUU();
            case 9:
                m21193b((NPCParty) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void aYg() {
        switch (bFf().mo6893i(_f_onPawnExplode_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                break;
        }
        aYf();
    }

    public Ship bUT() {
        switch (bFf().mo6893i(f4386x798f6a5d)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f4386x798f6a5d, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4386x798f6a5d, new Object[0]));
                break;
        }
        return bUS();
    }

    public NPCParty bUV() {
        switch (bFf().mo6893i(frd)) {
            case 0:
                return null;
            case 2:
                return (NPCParty) bFf().mo5606d(new aCE(this, frd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, frd, new Object[0]));
                break;
        }
        return bUU();
    }

    /* renamed from: c */
    public void mo13069c(NPCParty dv) {
        switch (bFf().mo6893i(fre)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fre, new Object[]{dv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fre, new Object[]{dv}));
                break;
        }
        m21193b(dv);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo13070e(Controller jx) {
        switch (bFf().mo6893i(frc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, frc, new Object[]{jx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, frc, new Object[]{jx}));
                break;
        }
        m21194d(jx);
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m21192ay(f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m21199zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m21200zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m21196jk(60.0f);
        m21197jl(200.0f);
        m21198jm(500.0f);
    }

    @C0064Am(aul = "8dd860df16d0011c55de0315623a5a59", aum = 0)
    /* renamed from: zT */
    private int m21200zT() {
        Ship ajd;
        Vec3f mS = bUT().getOrientation().mo15209aT(new Vec3f(0.0f, 0.0f, -1.0f)).mo23510mS(fqJ);
        m21188U(bUT().getPosition().mo9504d((Tuple3d) new Vec3d((double) mS.x, (double) mS.y, (double) mS.z)));
        this.fra.mo11544q(bUN());
        if ((this.fqY.bUT() == null || this.fqY.bUT().isDead()) && bUO() != null) {
            Character aGt = bUO().aGt();
            if (aGt == null || aGt.bQx() == null || !aGt.bQx().isAlive() || !aGt.bQx().bae()) {
                bUO().mo1779B(mo3314al().agj());
                this.fqY.mo8609J(mo3314al());
            } else {
                this.fqY.mo8609J(aGt.bQx());
            }
        }
        switch (aYs()) {
            case 0:
                if (bUQ()) {
                    return 1;
                }
                break;
            case 1:
                long time = bxM().getTime();
                if ((time - bUM() > 5000 || this.shootEnemyBehaviour.bUT() == null || this.shootEnemyBehaviour.bUT().isDead() || !this.shootEnemyBehaviour.bUT().bae()) && (ajd = ajd()) != null) {
                    this.shootEnemyBehaviour.mo14322J(ajd);
                    this.fqX.mo8609J(ajd);
                    m21195gZ(time);
                    this.avoidObstaclesBehaviour.mo16334a(ajd);
                }
                if (!bUQ()) {
                    this.avoidObstaclesBehaviour.mo16334a(bUT());
                    return 0;
                }
                break;
        }
        return -1;
    }

    @C0064Am(aul = "7c2b1fff2493a4692baea048f449500d", aum = 0)
    /* renamed from: zP */
    private void m21199zP() {
        mo3318b(super.bnJ(), "A near enough ship must be set to be followed");
        this.fra = new C5706aTq();
        this.fra.mo11544q(bUT().getPosition());
        this.fqX = new aER();
        this.fqX.mo11520oC(bUJ());
        this.fqY = new aER();
        this.fqY.mo8612fe(false);
        this.fqY.mo11520oC(bUK());
        this.fqZ = new C1829aV();
        this.fqZ.mo11762a(bUT());
        this.avoidObstaclesBehaviour = new C6822auO(bUT());
        this.shootEnemyBehaviour = new C6305akR(30.0f, false);
        mo3328ld(2);
        mo3327lb(0);
        mo3317b(0, this.fra, mo3316b(this.fqZ, this.avoidObstaclesBehaviour, this.shootEnemyBehaviour));
        mo3317b(1, this.fqZ, this.shootEnemyBehaviour, mo3316b(this.fqY, this.avoidObstaclesBehaviour, this.shootEnemyBehaviour, this.fqX));
    }

    @C0064Am(aul = "0211778897806f5564bf0c0f462683e3", aum = 0)
    /* renamed from: ay */
    private void m21192ay(float f) {
        if (mo3314al() == null || mo3314al().isDead()) {
            stop();
            return;
        }
        super.step(f);
        if (aYa().aSj() < 150.0f) {
            aYa().mo964W(150.0f);
        }
    }

    @C0064Am(aul = "34450e5f5444b6bf54585f92ae9d831c", aum = 0)
    /* renamed from: d */
    private void m21194d(Controller jx) {
        m21191a(jx);
        if (jx instanceof PlayerController) {
            mo4588J(((PlayerController) jx).mo22061al());
        }
    }

    @C0064Am(aul = "54dc8c802b8b6be246c0336617d8c7b9", aum = 0)
    private Ship bUS() {
        return (Ship) bQp().aYa();
    }

    @C0064Am(aul = "2dbf1c071fb6eb73fa2b8ed4732c15dd", aum = 0)
    private void aYf() {
        super.aYg();
    }

    @C0064Am(aul = "d24406f3cc05ba57d0412a0ecea3e2f5", aum = 0)
    private NPCParty bUU() {
        return bUO();
    }

    @C0064Am(aul = "a997acb922c5ae61c77c3d6a479d4c0d", aum = 0)
    /* renamed from: b */
    private void m21193b(NPCParty dv) {
        m21190a(dv);
    }
}
