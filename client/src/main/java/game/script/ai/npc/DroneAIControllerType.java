package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6769atN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.game.script.p003ai.npc.DroneAIController;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aCO */
/* compiled from: a */
public class DroneAIControllerType extends AIControllerType implements C1616Xf {
    public static final C5663aRz _f_dodgeAngle = null;
    public static final C5663aRz _f_dodgeDisabled = null;
    public static final C5663aRz _f_dodgeDistance = null;
    public static final C5663aRz _f_dodgeReactionTime = null;
    public static final C5663aRz _f_initialReactionTime = null;
    public static final C5663aRz _f_pursuitDistance = null;
    public static final C5663aRz _f_pursuitPredictMovimet = null;
    public static final C5663aRz _f_shootAngle = null;
    public static final C5663aRz _f_shootPredictMovimet = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dG */
    public static final C2491fm f2470dG = null;
    /* renamed from: dH */
    public static final C2491fm f2471dH = null;
    /* renamed from: dI */
    public static final C2491fm f2472dI = null;
    /* renamed from: dJ */
    public static final C2491fm f2473dJ = null;
    /* renamed from: dN */
    public static final C2491fm f2474dN = null;
    public static final C2491fm ekP = null;
    public static final C2491fm ekQ = null;
    public static final C2491fm ekR = null;
    public static final C2491fm ekS = null;
    public static final C2491fm ekT = null;
    public static final C2491fm ekU = null;
    public static final C2491fm ekV = null;
    public static final C2491fm ekW = null;
    public static final C2491fm hxm = null;
    public static final C2491fm hxn = null;
    public static final C2491fm hxo = null;
    public static final C2491fm hxp = null;
    public static final C2491fm hxq = null;
    public static final C2491fm hxr = null;
    public static final C2491fm hxs = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "812bd8b496d59371cba2978b433fcce3", aum = 4)
    private static float dodgeAngle;
    @C0064Am(aul = "0f084d28688bc61da7763314af3114aa", aum = 5)
    private static boolean dodgeDisabled;
    @C0064Am(aul = "c1d1c21b835a2ebb712822038ecb70a1", aum = 3)
    private static float dodgeDistance;
    @C0064Am(aul = "ba126431149b0598bbc8371efdab6bba", aum = 2)
    private static float dodgeReactionTime;
    @C0064Am(aul = "f8a1ae4ad7e66c7fcf562db52fb5eb8b", aum = 8)
    private static float initialReactionTime;
    @C0064Am(aul = "1a1fffe0ef4ea23b262bf0f5a7ca9d58", aum = 0)
    private static float pursuitDistance;
    @C0064Am(aul = "824f7dad097148fbf4f854e4e66a88b4", aum = 1)
    private static boolean pursuitPredictMovimet;
    @C0064Am(aul = "d88ddce6045645c91a50dda25f1c4d40", aum = 6)
    private static float shootAngle;
    @C0064Am(aul = "241348072fd9fe13845f67a0f5c4825f", aum = 7)
    private static boolean shootPredictMovimet;

    static {
        m13168V();
    }

    public DroneAIControllerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public DroneAIControllerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13168V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIControllerType._m_fieldCount + 9;
        _m_methodCount = AIControllerType._m_methodCount + 20;
        int i = AIControllerType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(DroneAIControllerType.class, "1a1fffe0ef4ea23b262bf0f5a7ca9d58", i);
        _f_pursuitDistance = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(DroneAIControllerType.class, "824f7dad097148fbf4f854e4e66a88b4", i2);
        _f_pursuitPredictMovimet = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(DroneAIControllerType.class, "ba126431149b0598bbc8371efdab6bba", i3);
        _f_dodgeReactionTime = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(DroneAIControllerType.class, "c1d1c21b835a2ebb712822038ecb70a1", i4);
        _f_dodgeDistance = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(DroneAIControllerType.class, "812bd8b496d59371cba2978b433fcce3", i5);
        _f_dodgeAngle = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(DroneAIControllerType.class, "0f084d28688bc61da7763314af3114aa", i6);
        _f_dodgeDisabled = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(DroneAIControllerType.class, "d88ddce6045645c91a50dda25f1c4d40", i7);
        _f_shootAngle = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(DroneAIControllerType.class, "241348072fd9fe13845f67a0f5c4825f", i8);
        _f_shootPredictMovimet = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(DroneAIControllerType.class, "f8a1ae4ad7e66c7fcf562db52fb5eb8b", i9);
        _f_initialReactionTime = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_fields, (Object[]) _m_fields);
        int i11 = AIControllerType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 20)];
        C2491fm a = C4105zY.m41624a(DroneAIControllerType.class, "415d3da0923b23c865eb6ea010803bcf", i11);
        ekV = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(DroneAIControllerType.class, "d471750da8b3f2e3225b9fb27d19c693", i12);
        ekW = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(DroneAIControllerType.class, "fe2dc6ffe61ae854d7e543206e25fc18", i13);
        hxm = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(DroneAIControllerType.class, "3873776dda85f14c7632f62f14181dde", i14);
        hxn = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(DroneAIControllerType.class, "7012199104fdc8de68f60d3419bee29c", i15);
        ekP = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(DroneAIControllerType.class, "bbc069d5c7919efaac1537c017d3b8cb", i16);
        ekQ = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(DroneAIControllerType.class, "0673edc135f78c09299797249e72dd1c", i17);
        ekR = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(DroneAIControllerType.class, "e3a1066dfdf854d29c58b0eced87518d", i18);
        ekS = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(DroneAIControllerType.class, "cc575b3ed30a1dadc8e5dbe0c41315aa", i19);
        ekT = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(DroneAIControllerType.class, "ef5916463131d9acbfeeaa86c39cf856", i20);
        ekU = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(DroneAIControllerType.class, "790bc294be939252ea74db3fed3ebc4f", i21);
        hxo = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(DroneAIControllerType.class, "6d9f7c3858987a5198360508331ee501", i22);
        hxp = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        C2491fm a13 = C4105zY.m41624a(DroneAIControllerType.class, "d78b82d69871658ea8aa5f4449f75f82", i23);
        f2470dG = a13;
        fmVarArr[i23] = a13;
        int i24 = i23 + 1;
        C2491fm a14 = C4105zY.m41624a(DroneAIControllerType.class, "c5f7a6eb5f22c593ad80f72b2840969a", i24);
        f2471dH = a14;
        fmVarArr[i24] = a14;
        int i25 = i24 + 1;
        C2491fm a15 = C4105zY.m41624a(DroneAIControllerType.class, "e5bf90f1cc768bba792f016fd3c8766e", i25);
        f2472dI = a15;
        fmVarArr[i25] = a15;
        int i26 = i25 + 1;
        C2491fm a16 = C4105zY.m41624a(DroneAIControllerType.class, "42b94b584877db4a3175b0caaf1300de", i26);
        f2473dJ = a16;
        fmVarArr[i26] = a16;
        int i27 = i26 + 1;
        C2491fm a17 = C4105zY.m41624a(DroneAIControllerType.class, "255290cf20bb5891298a3cd23f2f8d3d", i27);
        hxq = a17;
        fmVarArr[i27] = a17;
        int i28 = i27 + 1;
        C2491fm a18 = C4105zY.m41624a(DroneAIControllerType.class, "d77b4a46128f23c7d662d55f1f3e7ecd", i28);
        hxr = a18;
        fmVarArr[i28] = a18;
        int i29 = i28 + 1;
        C2491fm a19 = C4105zY.m41624a(DroneAIControllerType.class, "75a24c46849919fb4df9741c642237ed", i29);
        hxs = a19;
        fmVarArr[i29] = a19;
        int i30 = i29 + 1;
        C2491fm a20 = C4105zY.m41624a(DroneAIControllerType.class, "4e056d4f111ffcf7651ba7cd4a3b239d", i30);
        f2474dN = a20;
        fmVarArr[i30] = a20;
        int i31 = i30 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(DroneAIControllerType.class, C6769atN.class, _m_fields, _m_methods);
    }

    /* renamed from: aI */
    private float m13169aI() {
        return bFf().mo5608dq().mo3211m(_f_shootAngle);
    }

    /* renamed from: aJ */
    private boolean m13170aJ() {
        return bFf().mo5608dq().mo3201h(_f_shootPredictMovimet);
    }

    /* renamed from: b */
    private void m13174b(boolean z) {
        bFf().mo5608dq().mo3153a(_f_shootPredictMovimet, z);
    }

    private float bwe() {
        return bFf().mo5608dq().mo3211m(_f_dodgeReactionTime);
    }

    private float bwf() {
        return bFf().mo5608dq().mo3211m(_f_dodgeDistance);
    }

    private float bwg() {
        return bFf().mo5608dq().mo3211m(_f_dodgeAngle);
    }

    private float bwh() {
        return bFf().mo5608dq().mo3211m(_f_pursuitDistance);
    }

    private boolean cbK() {
        return bFf().mo5608dq().mo3201h(_f_pursuitPredictMovimet);
    }

    private boolean cbL() {
        return bFf().mo5608dq().mo3201h(_f_dodgeDisabled);
    }

    private float cbM() {
        return bFf().mo5608dq().mo3211m(_f_initialReactionTime);
    }

    /* renamed from: eU */
    private void m13176eU(boolean z) {
        bFf().mo5608dq().mo3153a(_f_pursuitPredictMovimet, z);
    }

    /* renamed from: eV */
    private void m13177eV(boolean z) {
        bFf().mo5608dq().mo3153a(_f_dodgeDisabled, z);
    }

    /* renamed from: ht */
    private void m13180ht(float f) {
        bFf().mo5608dq().mo3150a(_f_dodgeReactionTime, f);
    }

    /* renamed from: hu */
    private void m13181hu(float f) {
        bFf().mo5608dq().mo3150a(_f_dodgeDistance, f);
    }

    /* renamed from: hv */
    private void m13182hv(float f) {
        bFf().mo5608dq().mo3150a(_f_dodgeAngle, f);
    }

    /* renamed from: hw */
    private void m13183hw(float f) {
        bFf().mo5608dq().mo3150a(_f_pursuitDistance, f);
    }

    /* renamed from: i */
    private void m13186i(float f) {
        bFf().mo5608dq().mo3150a(_f_shootAngle, f);
    }

    /* renamed from: jy */
    private void m13189jy(float f) {
        bFf().mo5608dq().mo3150a(_f_initialReactionTime, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6769atN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIControllerType._m_methodCount) {
            case 0:
                return new Float(bwo());
            case 1:
                m13179hD(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Boolean(cQr());
            case 3:
                m13187iB(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return new Float(bwi());
            case 5:
                m13184hx(((Float) args[0]).floatValue());
                return null;
            case 6:
                return new Float(bwk());
            case 7:
                m13185hz(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Float(bwm());
            case 9:
                m13178hB(((Float) args[0]).floatValue());
                return null;
            case 10:
                return new Boolean(cQt());
            case 11:
                m13188iD(((Boolean) args[0]).booleanValue());
                return null;
            case 12:
                return new Float(m13171aL());
            case 13:
                m13190k(((Float) args[0]).floatValue());
                return null;
            case 14:
                return new Boolean(m13172aN());
            case 15:
                m13175c(((Boolean) args[0]).booleanValue());
                return null;
            case 16:
                return new Float(cQv());
            case 17:
                m13191lM(((Float) args[0]).floatValue());
                return null;
            case 18:
                return cQx();
            case 19:
                return m13173aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Angle to start shooting (º)")
    /* renamed from: aM */
    public float mo8010aM() {
        switch (bFf().mo6893i(f2470dG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f2470dG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2470dG, new Object[0]));
                break;
        }
        return m13171aL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Predict target moviment")
    /* renamed from: aO */
    public boolean mo8011aO() {
        switch (bFf().mo6893i(f2472dI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2472dI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2472dI, new Object[0]));
                break;
        }
        return m13172aN();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2474dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2474dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2474dN, new Object[0]));
                break;
        }
        return m13173aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Reaction time (s)")
    public float bwj() {
        switch (bFf().mo6893i(ekP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekP, new Object[0]));
                break;
        }
        return bwi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Distance to start dodgin")
    public float bwl() {
        switch (bFf().mo6893i(ekR)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekR, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekR, new Object[0]));
                break;
        }
        return bwk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (º)")
    public float bwn() {
        switch (bFf().mo6893i(ekT)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekT, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekT, new Object[0]));
                break;
        }
        return bwm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pursuit - Desired distance to target")
    public float bwp() {
        switch (bFf().mo6893i(ekV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ekV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekV, new Object[0]));
                break;
        }
        return bwo();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pursuit - Predict moviment")
    public boolean cQs() {
        switch (bFf().mo6893i(hxm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hxm, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hxm, new Object[0]));
                break;
        }
        return cQr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Disabled")
    public boolean cQu() {
        switch (bFf().mo6893i(hxo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hxo, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hxo, new Object[0]));
                break;
        }
        return cQt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Initial reaction time before going to the fight")
    public float cQw() {
        switch (bFf().mo6893i(hxq)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hxq, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hxq, new Object[0]));
                break;
        }
        return cQv();
    }

    public DroneAIController cQy() {
        switch (bFf().mo6893i(hxs)) {
            case 0:
                return null;
            case 2:
                return (DroneAIController) bFf().mo5606d(new aCE(this, hxs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hxs, new Object[0]));
                break;
        }
        return cQx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Predict target moviment")
    /* renamed from: d */
    public void mo8020d(boolean z) {
        switch (bFf().mo6893i(f2473dJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2473dJ, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2473dJ, new Object[]{new Boolean(z)}));
                break;
        }
        m13175c(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Distance to start dodgin")
    /* renamed from: hA */
    public void mo8021hA(float f) {
        switch (bFf().mo6893i(ekS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekS, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekS, new Object[]{new Float(f)}));
                break;
        }
        m13185hz(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (º)")
    /* renamed from: hC */
    public void mo8022hC(float f) {
        switch (bFf().mo6893i(ekU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekU, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekU, new Object[]{new Float(f)}));
                break;
        }
        m13178hB(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pursuit - Desired distance to target")
    /* renamed from: hE */
    public void mo8023hE(float f) {
        switch (bFf().mo6893i(ekW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekW, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekW, new Object[]{new Float(f)}));
                break;
        }
        m13179hD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Reaction time (s)")
    /* renamed from: hy */
    public void mo8024hy(float f) {
        switch (bFf().mo6893i(ekQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekQ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekQ, new Object[]{new Float(f)}));
                break;
        }
        m13184hx(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pursuit - Predict moviment")
    /* renamed from: iC */
    public void mo8025iC(boolean z) {
        switch (bFf().mo6893i(hxn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxn, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxn, new Object[]{new Boolean(z)}));
                break;
        }
        m13187iB(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Disabled")
    /* renamed from: iE */
    public void mo8026iE(boolean z) {
        switch (bFf().mo6893i(hxp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxp, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxp, new Object[]{new Boolean(z)}));
                break;
        }
        m13188iD(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Angle to start shooting (º)")
    /* renamed from: l */
    public void mo8027l(float f) {
        switch (bFf().mo6893i(f2471dH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2471dH, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2471dH, new Object[]{new Float(f)}));
                break;
        }
        m13190k(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Initial reaction time before going to the fight")
    /* renamed from: lN */
    public void mo8028lN(float f) {
        switch (bFf().mo6893i(hxr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxr, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxr, new Object[]{new Float(f)}));
                break;
        }
        m13191lM(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m13183hw(300.0f);
        m13176eU(true);
        m13180ht(0.5f);
        m13181hu(2000.0f);
        m13182hv(12.0f);
        m13177eV(false);
        m13186i(15.0f);
        m13174b(true);
        m13189jy(2.0f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pursuit - Desired distance to target")
    @C0064Am(aul = "415d3da0923b23c865eb6ea010803bcf", aum = 0)
    private float bwo() {
        return bwh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pursuit - Desired distance to target")
    @C0064Am(aul = "d471750da8b3f2e3225b9fb27d19c693", aum = 0)
    /* renamed from: hD */
    private void m13179hD(float f) {
        m13183hw(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pursuit - Predict moviment")
    @C0064Am(aul = "fe2dc6ffe61ae854d7e543206e25fc18", aum = 0)
    private boolean cQr() {
        return cbK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pursuit - Predict moviment")
    @C0064Am(aul = "3873776dda85f14c7632f62f14181dde", aum = 0)
    /* renamed from: iB */
    private void m13187iB(boolean z) {
        m13176eU(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Reaction time (s)")
    @C0064Am(aul = "7012199104fdc8de68f60d3419bee29c", aum = 0)
    private float bwi() {
        return bwe();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Reaction time (s)")
    @C0064Am(aul = "bbc069d5c7919efaac1537c017d3b8cb", aum = 0)
    /* renamed from: hx */
    private void m13184hx(float f) {
        m13180ht(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Distance to start dodgin")
    @C0064Am(aul = "0673edc135f78c09299797249e72dd1c", aum = 0)
    private float bwk() {
        return bwf();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Distance to start dodgin")
    @C0064Am(aul = "e3a1066dfdf854d29c58b0eced87518d", aum = 0)
    /* renamed from: hz */
    private void m13185hz(float f) {
        m13181hu(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (º)")
    @C0064Am(aul = "cc575b3ed30a1dadc8e5dbe0c41315aa", aum = 0)
    private float bwm() {
        return bwg();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Max angle beetween enemy direction and AI ship (º)")
    @C0064Am(aul = "ef5916463131d9acbfeeaa86c39cf856", aum = 0)
    /* renamed from: hB */
    private void m13178hB(float f) {
        m13182hv(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Dodge Bullets - Disabled")
    @C0064Am(aul = "790bc294be939252ea74db3fed3ebc4f", aum = 0)
    private boolean cQt() {
        return cbL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Dodge Bullets - Disabled")
    @C0064Am(aul = "6d9f7c3858987a5198360508331ee501", aum = 0)
    /* renamed from: iD */
    private void m13188iD(boolean z) {
        m13177eV(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Angle to start shooting (º)")
    @C0064Am(aul = "d78b82d69871658ea8aa5f4449f75f82", aum = 0)
    /* renamed from: aL */
    private float m13171aL() {
        return m13169aI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Angle to start shooting (º)")
    @C0064Am(aul = "c5f7a6eb5f22c593ad80f72b2840969a", aum = 0)
    /* renamed from: k */
    private void m13190k(float f) {
        m13186i(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shoot - Predict target moviment")
    @C0064Am(aul = "e5bf90f1cc768bba792f016fd3c8766e", aum = 0)
    /* renamed from: aN */
    private boolean m13172aN() {
        return m13170aJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shoot - Predict target moviment")
    @C0064Am(aul = "42b94b584877db4a3175b0caaf1300de", aum = 0)
    /* renamed from: c */
    private void m13175c(boolean z) {
        m13174b(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Initial reaction time before going to the fight")
    @C0064Am(aul = "255290cf20bb5891298a3cd23f2f8d3d", aum = 0)
    private float cQv() {
        return cbM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Initial reaction time before going to the fight")
    @C0064Am(aul = "d77b4a46128f23c7d662d55f1f3e7ecd", aum = 0)
    /* renamed from: lM */
    private void m13191lM(float f) {
        m13189jy(f);
    }

    @C0064Am(aul = "75a24c46849919fb4df9741c642237ed", aum = 0)
    private DroneAIController cQx() {
        return (DroneAIController) mo745aU();
    }

    @C0064Am(aul = "4e056d4f111ffcf7651ba7cd4a3b239d", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m13173aT() {
        T t = (DroneAIController) bFf().mo6865M(DroneAIController.class);
        t.mo4238a(this);
        return t;
    }
}
