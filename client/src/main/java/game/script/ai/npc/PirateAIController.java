package game.script.ai.npc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.ship.Ship;
import logic.abb.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1135Qf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C5511aMd
@C6485anp
/* renamed from: a.atv  reason: case insensitive filesystem */
/* compiled from: a */
public class PirateAIController extends AIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    /* renamed from: _f_onDamageTaken_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003bLtaikodom_002fgame_002fDamage_003bLcom_002fhoplon_002fgeometry_002fVec3f_003b_0029V */
    public static final C2491fm f5340xbe599734 = null;
    public static final C5663aRz _f_radius2 = null;
    public static final C2491fm _f_setRadius2_0020_0028F_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm gAa = null;
    public static final C2491fm gAb = null;
    public static final C2491fm gAc = null;
    public static final C2491fm gAd = null;
    public static final C2491fm gAe = null;
    public static final C5663aRz gzF = null;
    public static final C5663aRz gzG = null;
    public static final C5663aRz gzH = null;
    public static final C5663aRz gzI = null;
    public static final C5663aRz gzJ = null;
    public static final C5663aRz gzK = null;
    public static final C5663aRz gzL = null;
    public static final C5663aRz gzM = null;
    public static final C5663aRz gzN = null;
    public static final C5663aRz gzO = null;
    public static final C5663aRz gzP = null;
    public static final C5663aRz gzQ = null;
    public static final C5663aRz gzT = null;
    public static final C2491fm gzZ = null;
    public static final long serialVersionUID = 0;
    public static final int FIGHTING = 2;
    public static final int RETURNING_TO_SPHERE = 1;
    public static final int WANDERING = 0;
    public static final int gzC = 3;
    public static final int gzD = 4;
    public static final int gzE = 5;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "2ae87d321d7b7f904c8eb09058ec60da", aum = 0)
    private static float dUn = 0.0f;
    @C0064Am(aul = "2aece51e581509f20f087aa956e244bb", aum = 1)
    private static float dUo = 0.0f;
    @C0064Am(aul = "fe5c18954979de7f01a4e1a3ef7e8b6c", aum = 2)
    private static float dUp = 0.0f;
    @C0064Am(aul = "65681454f831028bba7585aedd5572c8", aum = 3)
    private static float dUq = 0.0f;
    @C0064Am(aul = "dd03277b72a3ae16e56b6d9f96d8f833", aum = 4)
    private static float dUr = 0.0f;
    @C0064Am(aul = "1e7125b38bbddb12ddab927c5057d4f7", aum = 5)
    private static long dUs = 0;
    @C0064Am(aul = "b1b4db826a3549e8ca040f978b5bf495", aum = 6)
    private static long dUt = 0;
    @C0064Am(aul = "7017f723033ad16ded186e11ba6ec8c8", aum = 7)
    private static long dUu = 0;
    @C0064Am(aul = "1b68126aee265191fad3e6a791fd795c", aum = 8)
    private static float dUv = 0.0f;
    @C0064Am(aul = "8bc1ccda5443662d8ba19b1c72b72f04", aum = 9)
    private static Ship dUw = null;
    @C0064Am(aul = "e545d9b798cf18a4a3a700a159c68427", aum = 10)
    private static float dUx = 0.0f;
    @C0064Am(aul = "02369f273a708a55ceffaf0735e6e9a0", aum = 11)
    private static long dUy = 0;
    @C0064Am(aul = "d0d40ca35757cbbea2f8eb02e49bcfe0", aum = 12)
    private static Vec3d dUz = null;
    @C0064Am(aul = "f35487a4275ba5525c17e23902c968cb", aum = 13)
    private static float radius2;

    static {
        m26088V();
    }

    private transient C6822auO avoidObstaclesBehaviour;
    private transient long gzR;
    private transient long gzS;
    private transient aER gzU;
    private transient aER gzV;
    private transient C6305akR gzW;
    private transient C6822auO gzX;
    private transient Map<Ship, Float> gzY;
    private transient C6305akR shootEnemyBehaviour;

    public PirateAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PirateAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26088V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 14;
        _m_methodCount = AIController._m_methodCount + 10;
        int i = AIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(PirateAIController.class, "2ae87d321d7b7f904c8eb09058ec60da", i);
        gzF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PirateAIController.class, "2aece51e581509f20f087aa956e244bb", i2);
        gzG = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PirateAIController.class, "fe5c18954979de7f01a4e1a3ef7e8b6c", i3);
        gzH = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PirateAIController.class, "65681454f831028bba7585aedd5572c8", i4);
        gzI = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PirateAIController.class, "dd03277b72a3ae16e56b6d9f96d8f833", i5);
        gzJ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(PirateAIController.class, "1e7125b38bbddb12ddab927c5057d4f7", i6);
        gzK = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(PirateAIController.class, "b1b4db826a3549e8ca040f978b5bf495", i7);
        gzL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(PirateAIController.class, "7017f723033ad16ded186e11ba6ec8c8", i8);
        gzM = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(PirateAIController.class, "1b68126aee265191fad3e6a791fd795c", i9);
        gzN = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(PirateAIController.class, "8bc1ccda5443662d8ba19b1c72b72f04", i10);
        gzO = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(PirateAIController.class, "e545d9b798cf18a4a3a700a159c68427", i11);
        gzP = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(PirateAIController.class, "02369f273a708a55ceffaf0735e6e9a0", i12);
        gzQ = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(PirateAIController.class, "d0d40ca35757cbbea2f8eb02e49bcfe0", i13);
        gzT = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(PirateAIController.class, "f35487a4275ba5525c17e23902c968cb", i14);
        _f_radius2 = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i16 = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 10)];
        C2491fm a = C4105zY.m41624a(PirateAIController.class, "8a9c0684c2856e0a9fe39c1d96dbb7bc", i16);
        gzZ = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(PirateAIController.class, "b6e05ff7825556c9e443cec6c06c1d75", i17);
        gAa = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(PirateAIController.class, "4af08d6503f455abedc8ad1dcbf7a117", i18);
        gAb = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(PirateAIController.class, "8eb64f01953c7e45981d9fc3e826caa4", i19);
        _f_setRadius2_0020_0028F_0029V = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(PirateAIController.class, "47e10a10fa1e6be321a6c963498e3687", i20);
        _f_changeCurrentAction_0020_0028_0029I = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(PirateAIController.class, "ac0c993c9284ac280f47b38593953c64", i21);
        gAc = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(PirateAIController.class, "137d88747ad054aa045b9b96046b1d52", i22);
        gAd = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(PirateAIController.class, "2a7a8b70a722620abe8a31a59793a7ef", i23);
        _f_setupBehaviours_0020_0028_0029V = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(PirateAIController.class, "af32f1e83fa167548a5a1968e0666605", i24);
        gAe = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(PirateAIController.class, "53338ca20a90c739e6fff1ca1e42df1e", i25);
        f5340xbe599734 = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PirateAIController.class, C1135Qf.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "53338ca20a90c739e6fff1ca1e42df1e", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m26089a(Actor cr, C5260aCm acm, Vec3f vec3f) {
        throw new aWi(new aCE(this, f5340xbe599734, new Object[]{cr, acm, vec3f}));
    }

    @C0064Am(aul = "8eb64f01953c7e45981d9fc3e826caa4", aum = 0)
    @C5566aOg
    /* renamed from: aB */
    private void m26090aB(float f) {
        throw new aWi(new aCE(this, _f_setRadius2_0020_0028F_0029V, new Object[]{new Float(f)}));
    }

    /* renamed from: aa */
    private void m26091aa(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(gzT, ajr);
    }

    @C0064Am(aul = "b6e05ff7825556c9e443cec6c06c1d75", aum = 0)
    @C5566aOg
    /* renamed from: ab */
    private void m26092ab(Vec3d ajr) {
        throw new aWi(new aCE(this, gAa, new Object[]{ajr}));
    }

    /* renamed from: af */
    private void m26093af(Ship fAVar) {
        bFf().mo5608dq().mo3197f(gzO, fAVar);
    }

    /* renamed from: ah */
    private void m26095ah(Ship fAVar) {
        switch (bFf().mo6893i(gAe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAe, new Object[]{fAVar}));
                break;
        }
        m26094ag(fAVar);
    }

    /* renamed from: az */
    private void m26096az(float f) {
        bFf().mo5608dq().mo3150a(_f_radius2, f);
    }

    private float cvC() {
        return bFf().mo5608dq().mo3211m(gzF);
    }

    private float cvD() {
        return bFf().mo5608dq().mo3211m(gzG);
    }

    private float cvE() {
        return bFf().mo5608dq().mo3211m(gzH);
    }

    private float cvF() {
        return bFf().mo5608dq().mo3211m(gzI);
    }

    private float cvG() {
        return bFf().mo5608dq().mo3211m(gzJ);
    }

    private long cvH() {
        return bFf().mo5608dq().mo3213o(gzK);
    }

    private long cvI() {
        return bFf().mo5608dq().mo3213o(gzL);
    }

    private long cvJ() {
        return bFf().mo5608dq().mo3213o(gzM);
    }

    private float cvK() {
        return bFf().mo5608dq().mo3211m(gzN);
    }

    private Ship cvL() {
        return (Ship) bFf().mo5608dq().mo3214p(gzO);
    }

    private float cvM() {
        return bFf().mo5608dq().mo3211m(gzP);
    }

    private long cvN() {
        return bFf().mo5608dq().mo3213o(gzQ);
    }

    private Vec3d cvO() {
        return (Vec3d) bFf().mo5608dq().mo3214p(gzT);
    }

    private void cvT() {
        switch (bFf().mo6893i(gAc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAc, new Object[0]));
                break;
        }
        cvS();
    }

    private boolean cvV() {
        switch (bFf().mo6893i(gAd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gAd, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAd, new Object[0]));
                break;
        }
        return cvU();
    }

    /* renamed from: iM */
    private void m26097iM(long j) {
        bFf().mo5608dq().mo3184b(gzK, j);
    }

    /* renamed from: iN */
    private void m26098iN(long j) {
        bFf().mo5608dq().mo3184b(gzL, j);
    }

    /* renamed from: iO */
    private void m26099iO(long j) {
        bFf().mo5608dq().mo3184b(gzM, j);
    }

    /* renamed from: iP */
    private void m26100iP(long j) {
        bFf().mo5608dq().mo3184b(gzQ, j);
    }

    /* renamed from: kC */
    private void m26101kC(float f) {
        bFf().mo5608dq().mo3150a(gzF, f);
    }

    /* renamed from: kD */
    private void m26102kD(float f) {
        bFf().mo5608dq().mo3150a(gzG, f);
    }

    /* renamed from: kE */
    private void m26103kE(float f) {
        bFf().mo5608dq().mo3150a(gzH, f);
    }

    /* renamed from: kF */
    private void m26104kF(float f) {
        bFf().mo5608dq().mo3150a(gzI, f);
    }

    /* renamed from: kG */
    private void m26105kG(float f) {
        bFf().mo5608dq().mo3150a(gzJ, f);
    }

    /* renamed from: kH */
    private void m26106kH(float f) {
        bFf().mo5608dq().mo3150a(gzN, f);
    }

    /* renamed from: kI */
    private void m26107kI(float f) {
        bFf().mo5608dq().mo3150a(gzP, f);
    }

    /* renamed from: zM */
    private float m26108zM() {
        return bFf().mo5608dq().mo3211m(_f_radius2);
    }

    @C0064Am(aul = "47e10a10fa1e6be321a6c963498e3687", aum = 0)
    @C5566aOg
    /* renamed from: zT */
    private int m26110zT() {
        throw new aWi(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1135Qf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                return cvP();
            case 1:
                m26092ab((Vec3d) args[0]);
                return null;
            case 2:
                return new Float(cvR());
            case 3:
                m26090aB(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Integer(m26110zT());
            case 5:
                cvS();
                return null;
            case 6:
                return new Boolean(cvU());
            case 7:
                m26109zP();
                return null;
            case 8:
                m26094ag((Ship) args[0]);
                return null;
            case 9:
                m26089a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: aC */
    public void mo16278aC(float f) {
        switch (bFf().mo6893i(_f_setRadius2_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius2_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius2_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m26090aB(f);
    }

    @C5566aOg
    /* renamed from: ac */
    public void mo16279ac(Vec3d ajr) {
        switch (bFf().mo6893i(gAa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gAa, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gAa, new Object[]{ajr}));
                break;
        }
        m26092ab(ajr);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo3290b(Actor cr, C5260aCm acm, Vec3f vec3f) {
        switch (bFf().mo6893i(f5340xbe599734)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5340xbe599734, new Object[]{cr, acm, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5340xbe599734, new Object[]{cr, acm, vec3f}));
                break;
        }
        m26089a(cr, acm, vec3f);
    }

    public float bcR() {
        switch (bFf().mo6893i(gAb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gAb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gAb, new Object[0]));
                break;
        }
        return cvR();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Vec3d cvQ() {
        switch (bFf().mo6893i(gzZ)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, gzZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gzZ, new Object[0]));
                break;
        }
        return cvP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m26109zP();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m26110zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m26101kC(0.5f);
        m26102kD(0.15f);
        m26103kE(250.0f);
        m26104kF(500.0f);
        m26105kG(3000.0f);
        m26097iM(500);
        m26098iN(0);
        m26099iO(7000);
        m26106kH(0.25f);
        m26093af((Ship) null);
        this.gzR = -1;
        this.gzS = -1;
        m26096az(-1.0f);
        this.gzY = null;
    }

    @C0064Am(aul = "8a9c0684c2856e0a9fe39c1d96dbb7bc", aum = 0)
    private Vec3d cvP() {
        return cvO();
    }

    @C0064Am(aul = "4af08d6503f455abedc8ad1dcbf7a117", aum = 0)
    private float cvR() {
        return m26108zM();
    }

    @C0064Am(aul = "ac0c993c9284ac280f47b38593953c64", aum = 0)
    private void cvS() {
        if (this.gzY != null) {
            float f = 0.0f;
            Ship fAVar = null;
            for (Ship next : this.gzY.keySet()) {
                if (next.isDead() || !next.bae()) {
                    this.gzY.remove(next);
                    cvT();
                    return;
                }
                float floatValue = this.gzY.get(next).floatValue();
                if (floatValue > f) {
                    f = floatValue;
                    fAVar = next;
                }
            }
            if (fAVar != null) {
                m26095ah(fAVar);
            }
        }
    }

    @C0064Am(aul = "137d88747ad054aa045b9b96046b1d52", aum = 0)
    private boolean cvU() {
        for (Actor next : mo3301Mn()) {
            if (next instanceof Ship) {
                Ship fAVar = (Ship) next;
                if (mo3298D(fAVar) && mo3314al().mo1013bx((Actor) fAVar) < cvG() * cvG()) {
                    m26095ah(fAVar);
                    return true;
                }
            }
        }
        return false;
    }

    @C0064Am(aul = "2a7a8b70a722620abe8a31a59793a7ef", aum = 0)
    /* renamed from: zP */
    private void m26109zP() {
        boolean z;
        mo3318b(cvO() != null, "You must set a center for the Pirate");
        if (m26108zM() > 0.0f) {
            z = true;
        } else {
            z = false;
        }
        mo3318b(z, "You must set a radius for the Pirate");
        this.gzU = new aER(cvL());
        this.gzU.mo11520oC(cvF());
        this.gzV = new aER(cvL());
        this.gzV.mo11520oC(cvE());
        this.shootEnemyBehaviour = new C6305akR();
        this.shootEnemyBehaviour.mo14322J(cvL());
        this.gzW = new C6305akR();
        this.gzW.mo14322J(cvL());
        this.avoidObstaclesBehaviour = new C6822auO(cvL());
        this.gzX = new C6822auO(cvL());
        mo3328ld(6);
        mo3327lb(0);
        mo3317b(0, new C1829aV(), mo3316b(new C6822auO(), new C5280aDg(), new C6434amq()));
        mo3317b(1, new C1829aV(), mo3316b(new C6822auO(), new C5706aTq(cvO()), new C6434amq()));
        mo3317b(2, new C1829aV(), mo3316b(this.avoidObstaclesBehaviour, this.gzU, this.shootEnemyBehaviour));
        mo3317b(3, new C1829aV(), mo3316b(this.gzX, this.gzV, this.gzW));
        mo3317b(4, new C1829aV(), mo3316b(new C6822auO(), new C2653iB(), new C6434amq()));
        mo3317b(5, new C1829aV(), new C6822auO());
    }

    @C0064Am(aul = "af32f1e83fa167548a5a1968e0666605", aum = 0)
    /* renamed from: ag */
    private void m26094ag(Ship fAVar) {
        String str;
        String str2;
        if (fAVar != cvL()) {
            if (fAVar != null) {
                str = String.valueOf("") + "new target: " + fAVar.getName();
            } else {
                str = String.valueOf("") + "new target null";
            }
            if (cvL() != null) {
                str2 = String.valueOf(str) + " target was " + cvL().getName();
            } else {
                str2 = String.valueOf(str) + " target was null";
            }
            mo8359ma(str2);
            this.gzU.mo8609J(fAVar);
            this.gzV.mo8609J(fAVar);
            this.avoidObstaclesBehaviour.mo16334a(fAVar);
            this.gzX.mo16334a(fAVar);
            this.shootEnemyBehaviour.mo14322J(fAVar);
            this.gzW.mo14322J(fAVar);
            m26093af(fAVar);
        }
    }
}
