package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.ship.Ship;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.mbean.C5574aOo;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aSt  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class TrackShipAIController extends AIController implements C1616Xf, C6188aiE, C6534aom, C3161oY.C3162a {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getTrackedPilot_0020_0028_0029Ltaikodom_002fgame_002fscript_002fCharacter_003b */
    public static final C2491fm f3777x84fc63e3 = null;
    /* renamed from: _f_getTrackedShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f3778x798f6a5d = null;
    /* renamed from: _f_internalSetTracked_0020_0028Ltaikodom_002fgame_002fscript_002fship_002fShip_003bLtaikodom_002fgame_002fscript_002fCharacter_003b_0029V */
    public static final C2491fm f3779x161b142d = null;
    public static final C2491fm _f_isValidTrackedShip_0020_0028_0029Z = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f3780x13860637 = null;
    /* renamed from: _f_setTrackedPilot_0020_0028Ltaikodom_002fgame_002fscript_002fCharacter_003b_0029V */
    public static final C2491fm f3781xf8357b61 = null;
    /* renamed from: _f_setTrackedShip_0020_0028Ltaikodom_002fgame_002fscript_002fship_002fShip_003b_0029V */
    public static final C2491fm f3782x30fdc3fb = null;
    public static final C5663aRz _f_trackedPilot = null;
    public static final C5663aRz _f_trackedShip = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "229a6f579084f76e5f33bac4c6439582", aum = 1)
    private static Character trackedPilot;
    @C0064Am(aul = "860d72f63518be54661cb8f52b8bdc17", aum = 0)
    private static Ship trackedShip;

    static {
        m18256V();
    }

    public TrackShipAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TrackShipAIController(C5540aNg ang) {
        super(ang);
    }

    public TrackShipAIController(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m18256V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 2;
        _m_methodCount = AIController._m_methodCount + 8;
        int i = AIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(TrackShipAIController.class, "860d72f63518be54661cb8f52b8bdc17", i);
        _f_trackedShip = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TrackShipAIController.class, "229a6f579084f76e5f33bac4c6439582", i2);
        _f_trackedPilot = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i4 = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 8)];
        C2491fm a = C4105zY.m41624a(TrackShipAIController.class, "78fd47cca60890891bfc3c8c79e06397", i4);
        f3782x30fdc3fb = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(TrackShipAIController.class, "233385bd30a08e17cc5b3b087e7dbef1", i5);
        f3778x798f6a5d = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(TrackShipAIController.class, "4fc0a9a9f35e1d33145a908cb6424331", i6);
        f3781xf8357b61 = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(TrackShipAIController.class, "46390ccc764157200e419f3051774854", i7);
        f3777x84fc63e3 = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(TrackShipAIController.class, "31b8fd6bd8d40dab5aa273ccabc0e2cb", i8);
        f3779x161b142d = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(TrackShipAIController.class, "db3d3e0ecebf40aaa0ad42c70584b932", i9);
        _f_isValidTrackedShip_0020_0028_0029Z = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(TrackShipAIController.class, "4ec137978d83f9e32c0946f7fe01ec28", i10);
        f3780x13860637 = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(TrackShipAIController.class, "578b6228e68a19d3d2a03713d7d0ea1a", i11);
        _f_dispose_0020_0028_0029V = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TrackShipAIController.class, C5574aOo.class, _m_fields, _m_methods);
    }

    /* renamed from: ap */
    private void m18258ap(Ship fAVar) {
        bFf().mo5608dq().mo3197f(_f_trackedShip, fAVar);
    }

    /* renamed from: as */
    private void m18259as(Character acx) {
        bFf().mo5608dq().mo3197f(_f_trackedPilot, acx);
    }

    /* renamed from: b */
    private void m18261b(Ship fAVar, Character acx) {
        switch (bFf().mo6893i(f3779x161b142d)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3779x161b142d, new Object[]{fAVar, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3779x161b142d, new Object[]{fAVar, acx}));
                break;
        }
        m18257a(fAVar, acx);
    }

    private Ship cXN() {
        return (Ship) bFf().mo5608dq().mo3214p(_f_trackedShip);
    }

    private Character dum() {
        return (Character) bFf().mo5608dq().mo3214p(_f_trackedPilot);
    }

    /* renamed from: J */
    public void mo4588J(Ship fAVar) {
        switch (bFf().mo6893i(f3782x30fdc3fb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3782x30fdc3fb, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3782x30fdc3fb, new Object[]{fAVar}));
                break;
        }
        m18254I(fAVar);
    }

    /* renamed from: L */
    public void mo4589L(Character acx) {
        switch (bFf().mo6893i(f3781xf8357b61)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3781xf8357b61, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3781xf8357b61, new Object[]{acx}));
                break;
        }
        m18255K(acx);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5574aOo(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                m18254I((Ship) args[0]);
                return null;
            case 1:
                return bUS();
            case 2:
                m18255K((Character) args[0]);
                return null;
            case 3:
                return dun();
            case 4:
                m18257a((Ship) args[0], (Character) args[1]);
                return null;
            case 5:
                return new Boolean(bnI());
            case 6:
                m18260b((aDJ) args[0]);
                return null;
            case 7:
                m18262fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Ship bUT() {
        switch (bFf().mo6893i(f3778x798f6a5d)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f3778x798f6a5d, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3778x798f6a5d, new Object[0]));
                break;
        }
        return bUS();
    }

    /* access modifiers changed from: protected */
    public boolean bnJ() {
        switch (bFf().mo6893i(_f_isValidTrackedShip_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isValidTrackedShip_0020_0028_0029Z, new Object[0]));
                break;
        }
        return bnI();
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f3780x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3780x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3780x13860637, new Object[]{adj}));
                break;
        }
        m18260b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Character ccu() {
        switch (bFf().mo6893i(f3777x84fc63e3)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, f3777x84fc63e3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3777x84fc63e3, new Object[0]));
                break;
        }
        return dun();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m18262fg();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    @C0064Am(aul = "78fd47cca60890891bfc3c8c79e06397", aum = 0)
    /* renamed from: I */
    private void m18254I(Ship fAVar) {
        m18261b(fAVar, (Character) null);
    }

    @C0064Am(aul = "233385bd30a08e17cc5b3b087e7dbef1", aum = 0)
    private Ship bUS() {
        if (cXN() != null) {
            return cXN();
        }
        if (dum() != null) {
            return dum().bQx();
        }
        return null;
    }

    @C0064Am(aul = "4fc0a9a9f35e1d33145a908cb6424331", aum = 0)
    /* renamed from: K */
    private void m18255K(Character acx) {
        m18261b((Ship) null, acx);
    }

    @C0064Am(aul = "46390ccc764157200e419f3051774854", aum = 0)
    private Character dun() {
        return dum();
    }

    @C0064Am(aul = "31b8fd6bd8d40dab5aa273ccabc0e2cb", aum = 0)
    /* renamed from: a */
    private void m18257a(Ship fAVar, Character acx) {
        if (cXN() != null) {
            cXN().mo8355h(C3161oY.C3162a.class, this);
        }
        m18258ap(fAVar);
        m18259as(acx);
        if (cXN() != null) {
            cXN().mo8348d(C3161oY.C3162a.class, this);
        }
    }

    @C0064Am(aul = "db3d3e0ecebf40aaa0ad42c70584b932", aum = 0)
    private boolean bnI() {
        return bUT() != null && !bUT().isDead() && bUT().bae();
    }

    @C0064Am(aul = "4ec137978d83f9e32c0946f7fe01ec28", aum = 0)
    /* renamed from: b */
    private void m18260b(aDJ adj) {
        if (adj == cXN()) {
            m18261b((Ship) null, (Character) null);
        }
    }

    @C0064Am(aul = "578b6228e68a19d3d2a03713d7d0ea1a", aum = 0)
    /* renamed from: fg */
    private void m18262fg() {
        m18261b((Ship) null, (Character) null);
        super.dispose();
    }
}
