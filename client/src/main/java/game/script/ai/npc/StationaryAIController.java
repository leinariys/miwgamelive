package game.script.ai.npc;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3771up;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ahc  reason: case insensitive filesystem */
/* compiled from: a */
public class StationaryAIController extends AIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_offloadInit_0020_0028_0029V = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm fGz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m22261V();
    }

    public StationaryAIController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StationaryAIController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22261V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 0;
        _m_methodCount = AIController._m_methodCount + 6;
        _m_fields = new C5663aRz[(AIController._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 6)];
        C2491fm a = C4105zY.m41624a(StationaryAIController.class, "2ff9cba2c5382e9719811afac5615b3b", i);
        _f_changeCurrentAction_0020_0028_0029I = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(StationaryAIController.class, "5d4d4dd89b5f906550405a3de721258e", i2);
        _f_setupBehaviours_0020_0028_0029V = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(StationaryAIController.class, "3c9b4f89f0bcc94141705c60ca80e6ae", i3);
        _f_start_0020_0028_0029V = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(StationaryAIController.class, "ce8fad3d842593db3b62c8ec7bae01d2", i4);
        _f_offloadInit_0020_0028_0029V = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(StationaryAIController.class, "5d08ed5967aa8cb0541cae1286340859", i5);
        fGz = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(StationaryAIController.class, "e8cdbcdf0aa2764e245991f9651981f7", i6);
        _f_step_0020_0028F_0029V = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StationaryAIController.class, C3771up.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "5d08ed5967aa8cb0541cae1286340859", aum = 0)
    @C5566aOg
    private void bXI() {
        throw new aWi(new aCE(this, fGz, new Object[0]));
    }

    @C5566aOg
    private void bXJ() {
        switch (bFf().mo6893i(fGz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fGz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fGz, new Object[0]));
                break;
        }
        bXI();
    }

    @C0064Am(aul = "3c9b4f89f0bcc94141705c60ca80e6ae", aum = 0)
    @C5566aOg
    /* renamed from: qN */
    private void m22263qN() {
        throw new aWi(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "5d4d4dd89b5f906550405a3de721258e", aum = 0)
    @C5566aOg
    /* renamed from: zP */
    private void m22264zP() {
        throw new aWi(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "2ff9cba2c5382e9719811afac5615b3b", aum = 0)
    @C5566aOg
    /* renamed from: zT */
    private int m22265zT() {
        throw new aWi(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3771up(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                return new Integer(m22265zT());
            case 1:
                m22264zP();
                return null;
            case 2:
                m22263qN();
                return null;
            case 3:
                aYF();
                return null;
            case 4:
                bXI();
                return null;
            case 5:
                m22262ay(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C1253SX
    public void aYG() {
        switch (bFf().mo6893i(_f_offloadInit_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_offloadInit_0020_0028_0029V, new Object[0]));
                break;
        }
        aYF();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m22263qN();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m22262ay(f);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m22264zP();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m22265zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ce8fad3d842593db3b62c8ec7bae01d2", aum = 0)
    @C1253SX
    private void aYF() {
        super.aYG();
    }

    @C0064Am(aul = "e8cdbcdf0aa2764e245991f9651981f7", aum = 0)
    /* renamed from: ay */
    private void m22262ay(float f) {
        if (((double) aYa().mo1090qZ().length()) > 0.05d) {
            bXJ();
        }
    }
}
