package game.script.ai.npc;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import logic.abb.C0009AC;
import logic.abb.C5778aaK;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0228Cs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.yT */
/* compiled from: a */
public class FormationController extends TrackShipAIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bML = null;
    public static final C5663aRz bMN = null;
    public static final C2491fm bMQ = null;
    public static final C2491fm bMR = null;
    public static final C2491fm bMS = null;
    public static final C2491fm bMT = null;
    public static final C2491fm bMU = null;
    public static final C2491fm bMV = null;
    public static final long serialVersionUID = 0;
    public static final int bMJ = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8ffd8289615d06ee376d339f164ccb7a", aum = 0)
    private static Vec3f bMK;
    @C0064Am(aul = "8f470ac21c2068aeaf67ccbe1fa505ac", aum = 1)
    private static float bMM;

    static {
        m41280V();
    }

    private transient C0009AC bMO;
    private transient C5778aaK bMP;

    public FormationController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FormationController(C5540aNg ang) {
        super(ang);
    }

    public FormationController(Vec3f vec3f, float f) {
        super((C5540aNg) null);
        super._m_script_init(vec3f, f);
    }

    /* renamed from: V */
    static void m41280V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TrackShipAIController._m_fieldCount + 2;
        _m_methodCount = TrackShipAIController._m_methodCount + 8;
        int i = TrackShipAIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(FormationController.class, "8ffd8289615d06ee376d339f164ccb7a", i);
        bML = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(FormationController.class, "8f470ac21c2068aeaf67ccbe1fa505ac", i2);
        bMN = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_fields, (Object[]) _m_fields);
        int i4 = TrackShipAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 8)];
        C2491fm a = C4105zY.m41624a(FormationController.class, "ead10d87dc3e517157bf4320402d3148", i4);
        _f_changeCurrentAction_0020_0028_0029I = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(FormationController.class, "558c30db9f5945920b3e9100455e79be", i5);
        _f_setupBehaviours_0020_0028_0029V = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(FormationController.class, "9b48c807a00b25f46486973599b03eb5", i6);
        bMQ = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(FormationController.class, "6880517576d7afd6b33b76c7dad5040b", i7);
        bMR = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(FormationController.class, "57e278e901cf769d5b1363ca064b5dde", i8);
        bMS = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(FormationController.class, "099ba690bd703af9f697a3a6d80a3553", i9);
        bMT = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(FormationController.class, "6a3792f4353e23a8f2e18274b6c19510", i10);
        bMU = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(FormationController.class, "91ada951dea8a09912f5d67ace8db683", i11);
        bMV = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FormationController.class, C0228Cs.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m41278H(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(bML, vec3f);
    }

    private Vec3f aqA() {
        return (Vec3f) bFf().mo5608dq().mo3214p(bML);
    }

    private float aqB() {
        return bFf().mo5608dq().mo3211m(bMN);
    }

    /* renamed from: ec */
    private void m41283ec(float f) {
        bFf().mo5608dq().mo3150a(bMN, f);
    }

    /* renamed from: J */
    public void mo23126J(Vec3f vec3f) {
        switch (bFf().mo6893i(bMR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMR, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMR, new Object[]{vec3f}));
                break;
        }
        m41279I(vec3f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0228Cs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TrackShipAIController._m_methodCount) {
            case 0:
                return new Integer(m41286zT());
            case 1:
                m41285zP();
                return null;
            case 2:
                return aqC();
            case 3:
                m41279I((Vec3f) args[0]);
                return null;
            case 4:
                m41284ed(((Float) args[0]).floatValue());
                return null;
            case 5:
                m41281ai((Actor) args[0]);
                return null;
            case 6:
                return m41282ak((Actor) args[0]);
            case 7:
                aqE();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aj */
    public void mo23127aj(Actor cr) {
        switch (bFf().mo6893i(bMT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMT, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMT, new Object[]{cr}));
                break;
        }
        m41281ai(cr);
    }

    /* renamed from: al */
    public Float mo23128al(Actor cr) {
        switch (bFf().mo6893i(bMU)) {
            case 0:
                return null;
            case 2:
                return (Float) bFf().mo5606d(new aCE(this, bMU, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, bMU, new Object[]{cr}));
                break;
        }
        return m41282ak(cr);
    }

    public Vec3f aqD() {
        switch (bFf().mo6893i(bMQ)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bMQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMQ, new Object[0]));
                break;
        }
        return aqC();
    }

    public void aqF() {
        switch (bFf().mo6893i(bMV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMV, new Object[0]));
                break;
        }
        aqE();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: ee */
    public void mo23132ee(float f) {
        switch (bFf().mo6893i(bMS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMS, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMS, new Object[]{new Float(f)}));
                break;
        }
        m41284ed(f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m41285zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m41286zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: c */
    public void mo23131c(Vec3f vec3f, float f) {
        super.mo10S();
        m41278H(vec3f);
        m41283ec(f);
    }

    @C0064Am(aul = "ead10d87dc3e517157bf4320402d3148", aum = 0)
    /* renamed from: zT */
    private int m41286zT() {
        return -1;
    }

    @C0064Am(aul = "558c30db9f5945920b3e9100455e79be", aum = 0)
    /* renamed from: zP */
    private void m41285zP() {
        boolean z;
        mo3318b(bUT() != null, "You must set a tracked ship (leader) for FormationController");
        if (aqA() != null) {
            z = true;
        } else {
            z = false;
        }
        mo3318b(z, "You must set the formation distance for FormationController");
        mo3328ld(1);
        mo3327lb(0);
        this.bMO = new C0009AC(bUT(), aqA(), aqB());
        this.bMP = new C5778aaK(bUT());
        mo3317b(0, mo3316b(this.bMO, this.bMP));
    }

    @C0064Am(aul = "9b48c807a00b25f46486973599b03eb5", aum = 0)
    private Vec3f aqC() {
        return aqA();
    }

    @C0064Am(aul = "6880517576d7afd6b33b76c7dad5040b", aum = 0)
    /* renamed from: I */
    private void m41279I(Vec3f vec3f) {
        m41278H(vec3f);
        if (this.bMO != null) {
            this.bMO.mo30J(vec3f);
        }
    }

    @C0064Am(aul = "57e278e901cf769d5b1363ca064b5dde", aum = 0)
    /* renamed from: ed */
    private void m41284ed(float f) {
        m41283ec(f);
        if (this.bMO != null) {
            this.bMO.mo35ee(aqB());
        }
    }

    @C0064Am(aul = "099ba690bd703af9f697a3a6d80a3553", aum = 0)
    /* renamed from: ai */
    private void m41281ai(Actor cr) {
        if (this.bMP != null) {
            this.bMP.mo12169a(cr);
        }
    }

    @C0064Am(aul = "6a3792f4353e23a8f2e18274b6c19510", aum = 0)
    /* renamed from: ak */
    private Float m41282ak(Actor cr) {
        if (this.bMP != null) {
            return this.bMP.mo12171aL(cr);
        }
        return null;
    }

    @C0064Am(aul = "91ada951dea8a09912f5d67ace8db683", aum = 0)
    private void aqE() {
        if (this.bMP != null) {
            this.bMP.bMJ().clear();
        }
    }
}
