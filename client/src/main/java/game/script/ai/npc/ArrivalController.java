package game.script.ai.npc;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import logic.abb.C1829aV;
import logic.abb.C3284pw;
import logic.abb.C6434amq;
import logic.abb.C6822auO;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3931wu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aby  reason: case insensitive filesystem */
/* compiled from: a */
public class ArrivalController extends AIController implements C1616Xf {
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz azP = null;
    public static final C2491fm eZw = null;
    public static final C2491fm eZx = null;
    public static final long serialVersionUID = 0;
    private static final int dgz = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bc47a3f23f8b69f0809417f676a6d5c7", aum = 0)
    private static Vec3d point;

    static {
        m20122V();
    }

    private transient C6822auO avoidObstaclesBehaviour;

    public ArrivalController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ArrivalController(Vec3d ajr) {
        super((C5540aNg) null);
        super._m_script_init(ajr);
    }

    public ArrivalController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20122V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIController._m_fieldCount + 1;
        _m_methodCount = AIController._m_methodCount + 4;
        int i = AIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ArrivalController.class, "bc47a3f23f8b69f0809417f676a6d5c7", i);
        azP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIController._m_fields, (Object[]) _m_fields);
        int i3 = AIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(ArrivalController.class, "f03726b97d8d7800148bfb72eda13484", i3);
        eZw = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ArrivalController.class, "d8ba6e100cacea03dd19e50238a83e6c", i4);
        _f_changeCurrentAction_0020_0028_0029I = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ArrivalController.class, "aa1697fe5d7d5ee30c4e24472c86d4d4", i5);
        _f_setupBehaviours_0020_0028_0029V = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ArrivalController.class, "5a80bc540df37f522ff0f0c6f0aa8924", i6);
        eZx = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ArrivalController.class, C3931wu.class, _m_fields, _m_methods);
    }

    /* renamed from: Me */
    private Vec3d m20120Me() {
        return (Vec3d) bFf().mo5608dq().mo3214p(azP);
    }

    /* renamed from: o */
    private void m20124o(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(azP, ajr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: T */
    public void mo12546T(Vec3d ajr) {
        switch (bFf().mo6893i(eZw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eZw, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eZw, new Object[]{ajr}));
                break;
        }
        m20121S(ajr);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3931wu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIController._m_methodCount) {
            case 0:
                m20121S((Vec3d) args[0]);
                return null;
            case 1:
                return new Integer(m20126zT());
            case 2:
                m20125zP();
                return null;
            case 3:
                m20123aM((Actor) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo12547a(Actor cr) {
        switch (bFf().mo6893i(eZx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eZx, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eZx, new Object[]{cr}));
                break;
        }
        m20123aM(cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m20125zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m20126zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: R */
    public void mo12545R(Vec3d ajr) {
        super.mo10S();
        m20124o(ajr);
    }

    @C0064Am(aul = "f03726b97d8d7800148bfb72eda13484", aum = 0)
    /* renamed from: S */
    private void m20121S(Vec3d ajr) {
        m20124o(ajr);
    }

    @C0064Am(aul = "d8ba6e100cacea03dd19e50238a83e6c", aum = 0)
    /* renamed from: zT */
    private int m20126zT() {
        return -1;
    }

    @C0064Am(aul = "aa1697fe5d7d5ee30c4e24472c86d4d4", aum = 0)
    /* renamed from: zP */
    private void m20125zP() {
        mo3318b(m20120Me() != null, "ArrivalController must have a point of destiny");
        mo3328ld(1);
        mo3327lb(0);
        this.avoidObstaclesBehaviour = new C6822auO();
        mo3317b(0, new C1829aV(), mo3316b(this.avoidObstaclesBehaviour, new C3284pw(m20120Me()), new C6434amq()));
    }

    @C0064Am(aul = "5a80bc540df37f522ff0f0c6f0aa8924", aum = 0)
    /* renamed from: aM */
    private void m20123aM(Actor cr) {
        this.avoidObstaclesBehaviour.mo16334a(cr);
    }
}
