package game.script.tutorial;

import game.script.TaikodomObject;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@Deprecated
@C6485anp
/* renamed from: a.agr  reason: case insensitive filesystem */
/* compiled from: a */
public class Rule extends TaikodomObject implements C0468GU, C1616Xf {
    public static final String DEFAULT = "!!non-initialized!!";
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4537bL = null;
    /* renamed from: bN */
    public static final C2491fm f4538bN = null;
    /* renamed from: bO */
    public static final C2491fm f4539bO = null;
    /* renamed from: bP */
    public static final C2491fm f4540bP = null;
    public static final C5663aRz cDe = null;
    public static final C5663aRz fxV = null;
    public static final C2491fm fxX = null;
    public static final C2491fm fxY = null;
    public static final C2491fm fxZ = null;
    public static final C2491fm fya = null;
    public static final C2491fm fyb = null;
    public static final C2491fm fyc = null;
    public static final C2491fm fyd = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2e1113f7227c13446873bd038f1af238", aum = 2)

    /* renamed from: bK */
    private static UUID f4536bK;
    @C0064Am(aul = "36953a302e7f814bfe15027b11308db3", aum = 0)
    private static String event;
    @C0064Am(aul = "d006c821580ab6cb6b6abe9c0d88caf7", aum = 1)
    private static String param;

    static {
        m22131V();
    }

    @C3974xb
    private boolean fxW;

    public Rule() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Rule(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22131V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Rule.class, "36953a302e7f814bfe15027b11308db3", i);
        cDe = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Rule.class, "d006c821580ab6cb6b6abe9c0d88caf7", i2);
        fxV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Rule.class, "2e1113f7227c13446873bd038f1af238", i3);
        f4537bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 10)];
        C2491fm a = C4105zY.m41624a(Rule.class, "63c1ea9a1f7d00d0e88a174b32ac5727", i5);
        f4538bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Rule.class, "018a627c38d15f4bf533779227626399", i6);
        f4539bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Rule.class, "3b19059f9143f15794a870b16d49e606", i7);
        f4540bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Rule.class, "73876004e3d9c720d57650521c5cdfc9", i8);
        fxX = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Rule.class, "621bde93157823d4635eeaec1e4a8c0c", i9);
        fxY = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Rule.class, "e0d508f42d25d33176151561508fe2ba", i10);
        fxZ = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Rule.class, "b5a13fe12fd6ac65bd7f92039542d3c2", i11);
        fya = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(Rule.class, "8199a6af180462d86f7641be59267e89", i12);
        fyb = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(Rule.class, "2257b41ff403b8723291d4238376a1f7", i13);
        fyc = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(Rule.class, "d76548b07217da954beb898792a0fe55", i14);
        fyd = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Rule.class, C5946adW.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m22132a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4537bL, uuid);
    }

    /* renamed from: an */
    private UUID m22133an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4537bL);
    }

    private String bWF() {
        return (String) bFf().mo5608dq().mo3214p(cDe);
    }

    private String bWG() {
        return (String) bFf().mo5608dq().mo3214p(fxV);
    }

    /* renamed from: c */
    private void m22138c(UUID uuid) {
        switch (bFf().mo6893i(f4539bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4539bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4539bO, new Object[]{uuid}));
                break;
        }
        m22137b(uuid);
    }

    /* renamed from: iH */
    private void m22140iH(String str) {
        bFf().mo5608dq().mo3197f(cDe, str);
    }

    /* renamed from: iI */
    private void m22141iI(String str) {
        bFf().mo5608dq().mo3197f(fxV, str);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5946adW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m22134ap();
            case 1:
                m22137b((UUID) args[0]);
                return null;
            case 2:
                return m22136ar();
            case 3:
                return bWH();
            case 4:
                m22142iJ((String) args[0]);
                return null;
            case 5:
                return bWI();
            case 6:
                m22143iK((String) args[0]);
                return null;
            case 7:
                return new Boolean(bWJ());
            case 8:
                m22139ej(((Boolean) args[0]).booleanValue());
                return null;
            case 9:
                return new Boolean(m22135ap((String) args[0], (String) args[1]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4538bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4538bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4538bN, new Object[0]));
                break;
        }
        return m22134ap();
    }

    /* renamed from: aq */
    public boolean mo13465aq(String str, String str2) {
        switch (bFf().mo6893i(fyd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fyd, new Object[]{str, str2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fyd, new Object[]{str, str2}));
                break;
        }
        return m22135ap(str, str2);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Param")
    public String bUu() {
        switch (bFf().mo6893i(fxZ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fxZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fxZ, new Object[0]));
                break;
        }
        return bWI();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3974xb
    /* renamed from: ek */
    public void mo13467ek(boolean z) {
        switch (bFf().mo6893i(fyc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fyc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fyc, new Object[]{new Boolean(z)}));
                break;
        }
        m22139ej(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Event")
    public String getEvent() {
        switch (bFf().mo6893i(fxX)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fxX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fxX, new Object[0]));
                break;
        }
        return bWH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Event")
    public void setEvent(String str) {
        switch (bFf().mo6893i(fxY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fxY, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fxY, new Object[]{str}));
                break;
        }
        m22142iJ(str);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4540bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4540bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4540bP, new Object[0]));
                break;
        }
        return m22136ar();
    }

    public boolean isBlocked() {
        switch (bFf().mo6893i(fyb)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fyb, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fyb, new Object[0]));
                break;
        }
        return bWJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Param")
    public void setParam(String str) {
        switch (bFf().mo6893i(fya)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fya, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fya, new Object[]{str}));
                break;
        }
        m22143iK(str);
    }

    @C0064Am(aul = "63c1ea9a1f7d00d0e88a174b32ac5727", aum = 0)
    /* renamed from: ap */
    private UUID m22134ap() {
        return m22133an();
    }

    @C0064Am(aul = "018a627c38d15f4bf533779227626399", aum = 0)
    /* renamed from: b */
    private void m22137b(UUID uuid) {
        m22132a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m22140iH(DEFAULT);
        m22141iI(DEFAULT);
        m22132a(UUID.randomUUID());
    }

    @C0064Am(aul = "3b19059f9143f15794a870b16d49e606", aum = 0)
    /* renamed from: ar */
    private String m22136ar() {
        return bFY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Event")
    @C0064Am(aul = "73876004e3d9c720d57650521c5cdfc9", aum = 0)
    private String bWH() {
        return bWF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Event")
    @C0064Am(aul = "621bde93157823d4635eeaec1e4a8c0c", aum = 0)
    /* renamed from: iJ */
    private void m22142iJ(String str) {
        m22140iH(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Param")
    @C0064Am(aul = "e0d508f42d25d33176151561508fe2ba", aum = 0)
    private String bWI() {
        return bWG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Param")
    @C0064Am(aul = "b5a13fe12fd6ac65bd7f92039542d3c2", aum = 0)
    /* renamed from: iK */
    private void m22143iK(String str) {
        m22141iI(str);
    }

    @C0064Am(aul = "8199a6af180462d86f7641be59267e89", aum = 0)
    private boolean bWJ() {
        return this.fxW;
    }

    @C0064Am(aul = "2257b41ff403b8723291d4238376a1f7", aum = 0)
    @C3974xb
    /* renamed from: ej */
    private void m22139ej(boolean z) {
        this.fxW = z;
    }

    @C0064Am(aul = "d76548b07217da954beb898792a0fe55", aum = 0)
    /* renamed from: ap */
    private boolean m22135ap(String str, String str2) {
        return bWF().equalsIgnoreCase(str) && (bWG().equals("") || bWG().equalsIgnoreCase(str2));
    }
}
