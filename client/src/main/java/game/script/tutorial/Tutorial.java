package game.script.tutorial;

import game.script.template.BaseTaikodomContent;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.List;

@C6485anp
@C5511aMd
@Deprecated
/* renamed from: a.ajV  reason: case insensitive filesystem */
/* compiled from: a */
public class Tutorial extends BaseTaikodomContent implements C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f4701MN = null;

    /* renamed from: QD */
    public static final C2491fm f4702QD = null;

    /* renamed from: QE */
    public static final C2491fm f4703QE = null;

    /* renamed from: QF */
    public static final C2491fm f4704QF = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bih = null;
    public static final C2491fm bin = null;
    public static final C2491fm bio = null;
    public static final C2491fm bkF = null;
    public static final C2491fm cQi = null;
    /* renamed from: dN */
    public static final C2491fm f4705dN = null;
    public static final C5663aRz fEq = null;
    public static final C2491fm fEw = null;
    public static final C2491fm fEx = null;
    public static final C2491fm fQC = null;
    public static final C2491fm fQD = null;
    public static final C5663aRz fQz = null;
    public static final C2491fm gaA = null;
    public static final C2491fm gaB = null;
    public static final C2491fm gaC = null;
    public static final C2491fm gaD = null;
    public static final C2491fm gaE = null;
    public static final C2491fm gaF = null;
    public static final C2491fm gaG = null;
    public static final C2491fm gaH = null;
    public static final C2491fm gaI = null;
    public static final C5663aRz gak = null;
    public static final C5663aRz gal = null;
    public static final C5663aRz gam = null;
    public static final C2491fm gas = null;
    public static final C2491fm gat = null;
    public static final C2491fm gau = null;
    public static final C2491fm gav = null;
    public static final C2491fm gaw = null;
    public static final C2491fm gax = null;
    public static final C2491fm gay = null;
    public static final C2491fm gaz = null;
    public static final long serialVersionUID = 0;
    static final int gag = 1;
    static final int gah = 2;
    static final int gai = 4;
    static final int gaj = 8;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "dbc8f8c4475dc2a3c2b8e66efd4a05d1", aum = 0)
    private static boolean big = false;
    @C0064Am(aul = "807ae4df47ef3636a1e1982524da3536", aum = 1)
    private static boolean dIR = false;
    @C0064Am(aul = "d321f98357c9ca6f43122a5094cf1ab4", aum = 2)
    private static C3438ra<C6278ajq> dIS = null;
    @C0064Am(aul = "7abb26e95668a5cce74eba80dc58fe57", aum = 3)
    private static C3438ra<C6123agr> dIT = null;
    @C0064Am(aul = "91d27c9f50f46dc1f904886e4d586832", aum = 4)
    private static C6278ajq dIU = null;
    @C0064Am(aul = "761f9e92fc94eb1dfc4a414528b6c70d", aum = 5)
    private static C6278ajq dIV = null;

    static {
        m22931V();
    }

    @C3974xb
    private int gan;
    @C3974xb
    private int gao;
    @C3974xb
    private int gap;
    @C3974xb
    private C1923b gaq;
    @C3974xb
    private C6278ajq gar;

    public Tutorial() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Tutorial(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22931V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 6;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 30;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(Tutorial.class, "dbc8f8c4475dc2a3c2b8e66efd4a05d1", i);
        bih = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Tutorial.class, "807ae4df47ef3636a1e1982524da3536", i2);
        fEq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Tutorial.class, "d321f98357c9ca6f43122a5094cf1ab4", i3);
        gak = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Tutorial.class, "7abb26e95668a5cce74eba80dc58fe57", i4);
        fQz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Tutorial.class, "91d27c9f50f46dc1f904886e4d586832", i5);
        gal = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Tutorial.class, "761f9e92fc94eb1dfc4a414528b6c70d", i6);
        gam = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i8 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 30)];
        C2491fm a = C4105zY.m41624a(Tutorial.class, "67a0a1cda3165303d5b8fa948c8bdbc4", i8);
        cQi = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(Tutorial.class, "19ecf85fa42d3da93a1eaef34a5c8baf", i9);
        bkF = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(Tutorial.class, "5ec381dae193af4f5329ba93c69e6985", i10);
        bin = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(Tutorial.class, "9a393997893f18190aa01c125f66aedb", i11);
        bio = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(Tutorial.class, "582dea5da17a3dccf3314c2cf7f82232", i12);
        fEw = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(Tutorial.class, "4221b338de993e206039a3f48ad937c7", i13);
        fEx = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(Tutorial.class, "92a021911d14ac0a2489af2498b49a90", i14);
        f4701MN = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(Tutorial.class, "598d72d64a79e182093cc795deeb454d", i15);
        f4702QD = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(Tutorial.class, "6804f857955efab0cf6b916b8e293ad8", i16);
        f4703QE = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(Tutorial.class, "daac89446a5f37dc224d8e3378137c19", i17);
        gas = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(Tutorial.class, "2dae5ad5709105cfaf1b9c7f8e7cf5a2", i18);
        gat = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(Tutorial.class, "ccedb2f789d3badd3ed3db732f23f301", i19);
        f4704QF = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(Tutorial.class, "4112179b3be667a55294536edb9cd640", i20);
        gau = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(Tutorial.class, "22ca6a84945eb391764b18fac9cf6a2d", i21);
        gav = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(Tutorial.class, "18cfda4efad025d5de2183d2fc8cfbf4", i22);
        gaw = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(Tutorial.class, "a018cc25d72f6bcebb546472b0caa225", i23);
        fQC = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(Tutorial.class, "0ccf05ceb1c11811d0b8cfe369672962", i24);
        fQD = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        C2491fm a18 = C4105zY.m41624a(Tutorial.class, "1e2c2e5f9c7dca856a6613fc86a298d3", i25);
        gax = a18;
        fmVarArr[i25] = a18;
        int i26 = i25 + 1;
        C2491fm a19 = C4105zY.m41624a(Tutorial.class, "82d4eaa26a0cfc2a524cd2248a2ecfc1", i26);
        gay = a19;
        fmVarArr[i26] = a19;
        int i27 = i26 + 1;
        C2491fm a20 = C4105zY.m41624a(Tutorial.class, "1ff0e574b63b9a48ec7df8232fbef088", i27);
        gaz = a20;
        fmVarArr[i27] = a20;
        int i28 = i27 + 1;
        C2491fm a21 = C4105zY.m41624a(Tutorial.class, "e6262e62b9dd5d43adb82baf480659ce", i28);
        gaA = a21;
        fmVarArr[i28] = a21;
        int i29 = i28 + 1;
        C2491fm a22 = C4105zY.m41624a(Tutorial.class, "93119fde94d56a8964113df5ebe922e4", i29);
        gaB = a22;
        fmVarArr[i29] = a22;
        int i30 = i29 + 1;
        C2491fm a23 = C4105zY.m41624a(Tutorial.class, "8d0cfbf5e4856f976a33cee3f51fbc07", i30);
        gaC = a23;
        fmVarArr[i30] = a23;
        int i31 = i30 + 1;
        C2491fm a24 = C4105zY.m41624a(Tutorial.class, "19aa936cc5a35bad9494937f2f1c5d21", i31);
        gaD = a24;
        fmVarArr[i31] = a24;
        int i32 = i31 + 1;
        C2491fm a25 = C4105zY.m41624a(Tutorial.class, "dddd01bdc6d8fbd57347cb1dbf216198", i32);
        gaE = a25;
        fmVarArr[i32] = a25;
        int i33 = i32 + 1;
        C2491fm a26 = C4105zY.m41624a(Tutorial.class, "20a0312b08a4902b3fb906632ad9f594", i33);
        f4705dN = a26;
        fmVarArr[i33] = a26;
        int i34 = i33 + 1;
        C2491fm a27 = C4105zY.m41624a(Tutorial.class, "7ba751fc883f8a403b71f5fee6ccebee", i34);
        gaF = a27;
        fmVarArr[i34] = a27;
        int i35 = i34 + 1;
        C2491fm a28 = C4105zY.m41624a(Tutorial.class, "bba1f51013adb06e12001c50f9521b2b", i35);
        gaG = a28;
        fmVarArr[i35] = a28;
        int i36 = i35 + 1;
        C2491fm a29 = C4105zY.m41624a(Tutorial.class, "576c5407a855dbcdab3e279031aa708a", i36);
        gaH = a29;
        fmVarArr[i36] = a29;
        int i37 = i36 + 1;
        C2491fm a30 = C4105zY.m41624a(Tutorial.class, "d78a988d57d7ec254ecc8c621be77b7f", i37);
        gaI = a30;
        fmVarArr[i37] = a30;
        int i38 = i37 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Tutorial.class, C0993Od.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m22934a(C6278ajq ajq) {
        bFf().mo5608dq().mo3197f(gal, ajq);
    }

    /* renamed from: aY */
    private void m22936aY(boolean z) {
        bFf().mo5608dq().mo3153a(bih, z);
    }

    private boolean abL() {
        return bFf().mo5608dq().mo3201h(bih);
    }

    /* renamed from: b */
    private void m22939b(C6278ajq ajq) {
        bFf().mo5608dq().mo3197f(gam, ajq);
    }

    /* renamed from: bU */
    private void m22940bU(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gak, raVar);
    }

    /* renamed from: bV */
    private void m22941bV(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fQz, raVar);
    }

    private boolean bXi() {
        return bFf().mo5608dq().mo3201h(fEq);
    }

    private C3438ra ciU() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gak);
    }

    private C3438ra ciV() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fQz);
    }

    private C6278ajq ciW() {
        return (C6278ajq) bFf().mo5608dq().mo3214p(gal);
    }

    private C6278ajq ciX() {
        return (C6278ajq) bFf().mo5608dq().mo3214p(gam);
    }

    /* renamed from: el */
    private void m22947el(boolean z) {
        bFf().mo5608dq().mo3153a(fEq, z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0993Od(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                aMm();
                return null;
            case 1:
                adk();
                return null;
            case 2:
                return new Boolean(abS());
            case 3:
                m22937aZ(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return new Boolean(bXm());
            case 5:
                m22948en(((Boolean) args[0]).booleanValue());
                return null;
            case 6:
                return m22950rO();
            case 7:
                m22942bs((I18NString) args[0]);
                return null;
            case 8:
                return m22951vV();
            case 9:
                m22949mJ((I18NString) args[0]);
                return null;
            case 10:
                return ciY();
            case 11:
                m22943bu((I18NString) args[0]);
                return null;
            case 12:
                m22945c((C6278ajq) args[0]);
                return null;
            case 13:
                m22946e((C6278ajq) args[0]);
                return null;
            case 14:
                return cja();
            case 15:
                m22932a((C6123agr) args[0]);
                return null;
            case 16:
                m22944c((C6123agr) args[0]);
                return null;
            case 17:
                return cjc();
            case 18:
                return cje();
            case 19:
                return cjg();
            case 20:
                return cji();
            case 21:
                return new Boolean(cjk());
            case 22:
                return cjm();
            case 23:
                m22933a((C1923b) args[0]);
                return null;
            case 24:
                return new Boolean(m22938as((String) args[0], (String) args[1]));
            case 25:
                return m22935aT();
            case 26:
                return cjo();
            case 27:
                return cjq();
            case 28:
                return new Integer(cjs());
            case 29:
                return new Integer(cju());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3974xb
    public void aMn() {
        switch (bFf().mo6893i(cQi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQi, new Object[0]));
                break;
        }
        aMm();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4705dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4705dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4705dN, new Object[0]));
                break;
        }
        return m22935aT();
    }

    /* renamed from: at */
    public boolean mo14100at(String str, String str2) {
        switch (bFf().mo6893i(gaE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gaE, new Object[]{str, str2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gaE, new Object[]{str, str2}));
                break;
        }
        return m22938as(str, str2);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Rules to open")
    /* renamed from: b */
    public void mo14101b(C6123agr agr) {
        switch (bFf().mo6893i(fQC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQC, new Object[]{agr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQC, new Object[]{agr}));
                break;
        }
        m22932a(agr);
    }

    @C3974xb
    /* renamed from: b */
    public void mo14102b(C1923b bVar) {
        switch (bFf().mo6893i(gaD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gaD, new Object[]{bVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gaD, new Object[]{bVar}));
                break;
        }
        m22933a(bVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Popup")
    public boolean bXn() {
        switch (bFf().mo6893i(fEw)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fEw, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fEw, new Object[0]));
                break;
        }
        return bXm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title (max. 36 char.)")
    /* renamed from: bt */
    public void mo14104bt(I18NString i18NString) {
        switch (bFf().mo6893i(f4702QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4702QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4702QD, new Object[]{i18NString}));
                break;
        }
        m22942bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Success Message")
    /* renamed from: bv */
    public void mo14105bv(I18NString i18NString) {
        switch (bFf().mo6893i(f4704QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4704QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4704QF, new Object[]{i18NString}));
                break;
        }
        m22943bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Success Message")
    public I18NString ciZ() {
        switch (bFf().mo6893i(gat)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gat, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gat, new Object[0]));
                break;
        }
        return ciY();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Pages")
    public List<C6278ajq> cjb() {
        switch (bFf().mo6893i(gaw)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gaw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gaw, new Object[0]));
                break;
        }
        return cja();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Rules to open")
    public List<C6123agr> cjd() {
        switch (bFf().mo6893i(gax)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gax, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gax, new Object[0]));
                break;
        }
        return cjc();
    }

    @C3974xb
    public C1922a cjf() {
        switch (bFf().mo6893i(gay)) {
            case 0:
                return null;
            case 2:
                return (C1922a) bFf().mo5606d(new aCE(this, gay, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gay, new Object[0]));
                break;
        }
        return cje();
    }

    @C3974xb
    public C1922a cjh() {
        switch (bFf().mo6893i(gaz)) {
            case 0:
                return null;
            case 2:
                return (C1922a) bFf().mo5606d(new aCE(this, gaz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gaz, new Object[0]));
                break;
        }
        return cjg();
    }

    public C6278ajq cjj() {
        switch (bFf().mo6893i(gaA)) {
            case 0:
                return null;
            case 2:
                return (C6278ajq) bFf().mo5606d(new aCE(this, gaA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gaA, new Object[0]));
                break;
        }
        return cji();
    }

    public boolean cjl() {
        switch (bFf().mo6893i(gaB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gaB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gaB, new Object[0]));
                break;
        }
        return cjk();
    }

    @C3974xb
    public C1923b cjn() {
        switch (bFf().mo6893i(gaC)) {
            case 0:
                return null;
            case 2:
                return (C1923b) bFf().mo5606d(new aCE(this, gaC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gaC, new Object[0]));
                break;
        }
        return cjm();
    }

    public C6278ajq cjp() {
        switch (bFf().mo6893i(gaF)) {
            case 0:
                return null;
            case 2:
                return (C6278ajq) bFf().mo5606d(new aCE(this, gaF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gaF, new Object[0]));
                break;
        }
        return cjo();
    }

    public C6278ajq cjr() {
        switch (bFf().mo6893i(gaG)) {
            case 0:
                return null;
            case 2:
                return (C6278ajq) bFf().mo5606d(new aCE(this, gaG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gaG, new Object[0]));
                break;
        }
        return cjq();
    }

    @C3974xb
    public int cjt() {
        switch (bFf().mo6893i(gaH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gaH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gaH, new Object[0]));
                break;
        }
        return cjs();
    }

    @C3974xb
    public int cjv() {
        switch (bFf().mo6893i(gaI)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gaI, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gaI, new Object[0]));
                break;
        }
        return cju();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Rules to open")
    /* renamed from: d */
    public void mo14118d(C6123agr agr) {
        switch (bFf().mo6893i(fQD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQD, new Object[]{agr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQD, new Object[]{agr}));
                break;
        }
        m22944c(agr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Pages")
    /* renamed from: d */
    public void mo14119d(C6278ajq ajq) {
        switch (bFf().mo6893i(gau)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gau, new Object[]{ajq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gau, new Object[]{ajq}));
                break;
        }
        m22945c(ajq);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Popup")
    /* renamed from: eo */
    public void mo14120eo(boolean z) {
        switch (bFf().mo6893i(fEx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fEx, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fEx, new Object[]{new Boolean(z)}));
                break;
        }
        m22948en(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Pages")
    /* renamed from: f */
    public void mo14121f(C6278ajq ajq) {
        switch (bFf().mo6893i(gav)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gav, new Object[]{ajq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gav, new Object[]{ajq}));
                break;
        }
        m22946e(ajq);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hidden")
    public boolean isHidden() {
        switch (bFf().mo6893i(bin)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bin, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bin, new Object[0]));
                break;
        }
        return abS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hidden")
    public void setHidden(boolean z) {
        switch (bFf().mo6893i(bio)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                break;
        }
        m22937aZ(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Initial Message")
    /* renamed from: mK */
    public void mo14123mK(I18NString i18NString) {
        switch (bFf().mo6893i(gas)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gas, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gas, new Object[]{i18NString}));
                break;
        }
        m22949mJ(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title (max. 36 char.)")
    /* renamed from: rP */
    public I18NString mo14124rP() {
        switch (bFf().mo6893i(f4701MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4701MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4701MN, new Object[0]));
                break;
        }
        return m22950rO();
    }

    @C3974xb
    public void reset() {
        switch (bFf().mo6893i(bkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                break;
        }
        adk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Initial Message")
    /* renamed from: vW */
    public I18NString mo14127vW() {
        switch (bFf().mo6893i(f4703QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4703QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4703QE, new Object[0]));
                break;
        }
        return m22951vV();
    }

    @C0064Am(aul = "67a0a1cda3165303d5b8fa948c8bdbc4", aum = 0)
    @C3974xb
    private void aMm() {
        C1922a aVar;
        reset();
        for (C6278ajq b : ciU()) {
            b.mo14200b(C1922a.NORMAL_PAGE);
        }
        if (!ciW().cea()) {
            if (ciU().size() > 0) {
                if (ciX().cea() || ciU().size() != 1) {
                    ((C6278ajq) ciU().get(0)).mo14200b(C1922a.FIRST_PAGE);
                } else {
                    ((C6278ajq) ciU().get(0)).mo14200b(C1922a.ONE_PAGE);
                }
            } else if (ciX().cea()) {
                ciX().mo14200b(C1922a.ONE_PAGE);
            } else {
                System.out.println("Error: Empty tutorial!");
                return;
            }
            this.gan = -1;
        } else {
            ciW().mo14200b(C1922a.FIRST_PAGE);
            this.gap++;
        }
        if (ciX().cea()) {
            C6278ajq ciX = ciX();
            if (ciU().size() == 0) {
                aVar = C1922a.ONE_PAGE;
            } else {
                aVar = C1922a.LAST_PAGE;
            }
            ciX.mo14200b(aVar);
            this.gap++;
        } else if (ciU().size() == 1 && !ciW().cea()) {
            ((C6278ajq) ciU().get(ciU().size() - 1)).mo14200b(C1922a.ONE_PAGE);
        } else if (ciU().size() > 0) {
            ((C6278ajq) ciU().get(ciU().size() - 1)).mo14200b(C1922a.LAST_PAGE);
        } else {
            ciW().mo14200b(C1922a.ONE_PAGE);
        }
        this.gar = null;
        cjf();
    }

    @C0064Am(aul = "19ecf85fa42d3da93a1eaef34a5c8baf", aum = 0)
    @C3974xb
    private void adk() {
        this.gan = -2;
        this.gaq = C1923b.INVISIBLE;
        this.gao = 0;
        this.gap = ciU().size();
        for (C6278ajq aMn : ciU()) {
            aMn.aMn();
        }
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        this.gaq = C1923b.INVISIBLE;
        C6278ajq ajq = (C6278ajq) bFf().mo6865M(C6278ajq.class);
        ajq.mo10S();
        m22934a(ajq);
        C6278ajq ajq2 = (C6278ajq) bFf().mo6865M(C6278ajq.class);
        ajq2.mo10S();
        m22939b(ajq2);
        ciW().mo14211ek(false);
        ciX().mo14211ek(false);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hidden")
    @C0064Am(aul = "5ec381dae193af4f5329ba93c69e6985", aum = 0)
    private boolean abS() {
        return abL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hidden")
    @C0064Am(aul = "9a393997893f18190aa01c125f66aedb", aum = 0)
    /* renamed from: aZ */
    private void m22937aZ(boolean z) {
        m22936aY(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Popup")
    @C0064Am(aul = "582dea5da17a3dccf3314c2cf7f82232", aum = 0)
    private boolean bXm() {
        return bXi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Popup")
    @C0064Am(aul = "4221b338de993e206039a3f48ad937c7", aum = 0)
    /* renamed from: en */
    private void m22948en(boolean z) {
        m22947el(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title (max. 36 char.)")
    @C0064Am(aul = "92a021911d14ac0a2489af2498b49a90", aum = 0)
    /* renamed from: rO */
    private I18NString m22950rO() {
        return ciW().mo14215rP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title (max. 36 char.)")
    @C0064Am(aul = "598d72d64a79e182093cc795deeb454d", aum = 0)
    /* renamed from: bs */
    private void m22942bs(I18NString i18NString) {
        ciX().mo14201bt(i18NString);
        ciW().mo14201bt(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Initial Message")
    @C0064Am(aul = "6804f857955efab0cf6b916b8e293ad8", aum = 0)
    /* renamed from: vV */
    private I18NString m22951vV() {
        return ciW().cdT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Initial Message")
    @C0064Am(aul = "daac89446a5f37dc224d8e3378137c19", aum = 0)
    /* renamed from: mJ */
    private void m22949mJ(I18NString i18NString) {
        ciW().mo14213mG(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Success Message")
    @C0064Am(aul = "2dae5ad5709105cfaf1b9c7f8e7cf5a2", aum = 0)
    private I18NString ciY() {
        return ciX().cdT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Success Message")
    @C0064Am(aul = "ccedb2f789d3badd3ed3db732f23f301", aum = 0)
    /* renamed from: bu */
    private void m22943bu(I18NString i18NString) {
        ciX().mo14213mG(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Pages")
    @C0064Am(aul = "4112179b3be667a55294536edb9cd640", aum = 0)
    /* renamed from: c */
    private void m22945c(C6278ajq ajq) {
        ciU().add(ajq);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Pages")
    @C0064Am(aul = "22ca6a84945eb391764b18fac9cf6a2d", aum = 0)
    /* renamed from: e */
    private void m22946e(C6278ajq ajq) {
        ciU().remove(ajq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Pages")
    @C0064Am(aul = "18cfda4efad025d5de2183d2fc8cfbf4", aum = 0)
    private List<C6278ajq> cja() {
        return ciU();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Rules to open")
    @C0064Am(aul = "a018cc25d72f6bcebb546472b0caa225", aum = 0)
    /* renamed from: a */
    private void m22932a(C6123agr agr) {
        ciV().add(agr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Rules to open")
    @C0064Am(aul = "0ccf05ceb1c11811d0b8cfe369672962", aum = 0)
    /* renamed from: c */
    private void m22944c(C6123agr agr) {
        ciV().remove(agr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Rules to open")
    @C0064Am(aul = "1e2c2e5f9c7dca856a6613fc86a298d3", aum = 0)
    private List<C6123agr> cjc() {
        return ciV();
    }

    @C0064Am(aul = "82d4eaa26a0cfc2a524cd2248a2ecfc1", aum = 0)
    @C3974xb
    private C1922a cje() {
        C6278ajq ciX;
        C6278ajq ajq = null;
        if (this.gar != null && this.gar.ceg().equals(C1922a.ONE_PAGE)) {
            return C1922a.END_OF_TUTORIAL;
        }
        this.gan++;
        this.gao++;
        if (this.gan < 0) {
            if (ciW() == null || !ciW().cea()) {
                ciX = ciU().size() > 0 ? (C6278ajq) ciU().get(0) : ciX();
            } else {
                ciX = ciW();
            }
            this.gar = ciX;
        } else if (this.gan == ciU().size()) {
            if (ciX().cea()) {
                ajq = ciX();
            }
            this.gar = ajq;
        } else if (this.gan > ciU().size()) {
            this.gar = null;
        } else {
            this.gar = (C6278ajq) ciU().get(this.gan);
        }
        if (this.gar == null) {
            return C1922a.END_OF_TUTORIAL;
        }
        this.gar.cei();
        return this.gar.ceg();
    }

    @C0064Am(aul = "1ff0e574b63b9a48ec7df8232fbef088", aum = 0)
    @C3974xb
    private C1922a cjg() {
        C6278ajq ciX;
        this.gan--;
        this.gao--;
        if (this.gan < 0) {
            if (ciW() == null || !ciW().cea()) {
                ciX = ciU().size() > 0 ? (C6278ajq) ciU().get(0) : ciX();
            } else {
                ciX = ciW();
            }
            this.gar = ciX;
        } else {
            this.gar = (C6278ajq) ciU().get(this.gan);
        }
        this.gar.cei();
        return this.gar.ceg();
    }

    @C0064Am(aul = "e6262e62b9dd5d43adb82baf480659ce", aum = 0)
    private C6278ajq cji() {
        return this.gar;
    }

    @C0064Am(aul = "93119fde94d56a8964113df5ebe922e4", aum = 0)
    private boolean cjk() {
        for (C6123agr isBlocked : ciV()) {
            if (isBlocked.isBlocked()) {
                return false;
            }
        }
        return true;
    }

    @C0064Am(aul = "8d0cfbf5e4856f976a33cee3f51fbc07", aum = 0)
    @C3974xb
    private C1923b cjm() {
        return this.gaq;
    }

    @C0064Am(aul = "19aa936cc5a35bad9494937f2f1c5d21", aum = 0)
    @C3974xb
    /* renamed from: a */
    private void m22933a(C1923b bVar) {
        this.gaq = bVar;
    }

    @C0064Am(aul = "dddd01bdc6d8fbd57347cb1dbf216198", aum = 0)
    /* renamed from: as */
    private boolean m22938as(String str, String str2) {
        for (C6123agr aq : ciV()) {
            if (aq.mo13465aq(str, str2)) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "20a0312b08a4902b3fb906632ad9f594", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m22935aT() {
        return null;
    }

    @C0064Am(aul = "7ba751fc883f8a403b71f5fee6ccebee", aum = 0)
    private C6278ajq cjo() {
        return ciW();
    }

    @C0064Am(aul = "bba1f51013adb06e12001c50f9521b2b", aum = 0)
    private C6278ajq cjq() {
        return ciX();
    }

    @C0064Am(aul = "576c5407a855dbcdab3e279031aa708a", aum = 0)
    @C3974xb
    private int cjs() {
        return this.gao;
    }

    @C0064Am(aul = "d78a988d57d7ec254ecc8c621be77b7f", aum = 0)
    @C3974xb
    private int cju() {
        return this.gap;
    }

    /* renamed from: a.ajV$a */
    public enum C1922a {
        ONE_PAGE,
        FIRST_PAGE,
        NORMAL_PAGE,
        LAST_PAGE,
        LAST_PAGE_WITH_CONFIRM,
        END_OF_TUTORIAL
    }

    /* renamed from: a.ajV$b */
    /* compiled from: a */
    public enum C1923b {
        INVISIBLE,
        VISIBLE,
        MINIMIZED,
        DONE
    }
}
