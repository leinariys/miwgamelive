package game.script.tutorial;

import game.script.TaikodomObject;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

@C5511aMd
@Deprecated
@C6485anp
/* renamed from: a.ajq  reason: case insensitive filesystem */
/* compiled from: a */
public class TutorialPage extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f4720MN = null;

    /* renamed from: Nl */
    public static final C5663aRz f4721Nl = null;

    /* renamed from: QD */
    public static final C2491fm f4722QD = null;

    /* renamed from: Qz */
    public static final C5663aRz f4723Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4725bL = null;
    /* renamed from: bN */
    public static final C2491fm f4726bN = null;
    /* renamed from: bO */
    public static final C2491fm f4727bO = null;
    /* renamed from: bP */
    public static final C2491fm f4728bP = null;
    public static final C2491fm cQi = null;
    public static final C2491fm fQA = null;
    public static final C2491fm fQB = null;
    public static final C2491fm fQC = null;
    public static final C2491fm fQD = null;
    public static final C2491fm fQE = null;
    public static final C2491fm fQF = null;
    public static final C2491fm fQG = null;
    public static final C2491fm fQH = null;
    public static final C2491fm fQI = null;
    public static final C2491fm fQJ = null;
    public static final C2491fm fQK = null;
    public static final C2491fm fQL = null;
    public static final C2491fm fQM = null;
    public static final C2491fm fQN = null;
    public static final C5663aRz fQx = null;
    public static final C5663aRz fQz = null;
    public static final C2491fm fyc = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "eb1e047715296cf1bde1b3fcb2211dea", aum = 4)

    /* renamed from: bK */
    private static UUID f4724bK;
    @C0064Am(aul = "ab1b976f810af85b760cc89798082475", aum = 1)
    private static I18NString fQv;
    @C0064Am(aul = "749e073a8593d03f91934172dff5685c", aum = 2)
    private static I18NString fQw;
    @C0064Am(aul = "068e797efb5e61e1eacc0396981f9cbb", aum = 3)
    private static C2686iZ<C6123agr> fQy;
    @C0064Am(aul = "890958eccab9f32782d5cf19b009ee1d", aum = 0)

    /* renamed from: ni */
    private static I18NString f4729ni;

    static {
        m23060V();
    }

    @C3974xb
    private C6257ajV.C1922a fQu;

    public TutorialPage() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TutorialPage(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m23060V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 21;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(TutorialPage.class, "890958eccab9f32782d5cf19b009ee1d", i);
        f4723Qz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TutorialPage.class, "ab1b976f810af85b760cc89798082475", i2);
        f4721Nl = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TutorialPage.class, "749e073a8593d03f91934172dff5685c", i3);
        fQx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TutorialPage.class, "068e797efb5e61e1eacc0396981f9cbb", i4);
        fQz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(TutorialPage.class, "eb1e047715296cf1bde1b3fcb2211dea", i5);
        f4725bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 21)];
        C2491fm a = C4105zY.m41624a(TutorialPage.class, "7a1263aaeb5f30b16424ccd317710b99", i7);
        f4726bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(TutorialPage.class, "8f8d8246235e9b7922de51b3819b05dc", i8);
        f4727bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(TutorialPage.class, "bb370e29955d4f9b3256b492d8dc27a1", i9);
        f4728bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(TutorialPage.class, "fe316b8d7ac62f364807d62db8f4e7dd", i10);
        cQi = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(TutorialPage.class, "3c6ecf80e80c7c4227d4e6e3fde9d07e", i11);
        f4720MN = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(TutorialPage.class, "1dbc7786a2c398ef737b9fd3460f7e0b", i12);
        f4722QD = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(TutorialPage.class, "63251af8fa9f294a2ea06d24861ae2fe", i13);
        fQA = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(TutorialPage.class, "762651a9518d387ef8624e395c754406", i14);
        fQB = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(TutorialPage.class, "a6ef171c5fe3b8990df25ef6329e06de", i15);
        fQC = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(TutorialPage.class, "0602164273db1f281460108dc8040f93", i16);
        fQD = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(TutorialPage.class, "cc942bcf55f8532884dd0804eb96d425", i17);
        fQE = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(TutorialPage.class, "4d259860c4becaf2de863ca0fafb673c", i18);
        fQF = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(TutorialPage.class, "8fb9af79f5af67a864c568e425ffc0a5", i19);
        fQG = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(TutorialPage.class, "0bdead703b97898a77332957413880a1", i20);
        fyc = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(TutorialPage.class, "d95aeeb64fb89fdeef39f1da9db6fdee", i21);
        fQH = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(TutorialPage.class, "e50d666a0ea7abf6d6e0611c830c076a", i22);
        fQI = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        C2491fm a17 = C4105zY.m41624a(TutorialPage.class, "3315a89f3dd90336648e8b66940639c2", i23);
        fQJ = a17;
        fmVarArr[i23] = a17;
        int i24 = i23 + 1;
        C2491fm a18 = C4105zY.m41624a(TutorialPage.class, "e21fca06ba12bdfd1ea07e8e525ed9b1", i24);
        fQK = a18;
        fmVarArr[i24] = a18;
        int i25 = i24 + 1;
        C2491fm a19 = C4105zY.m41624a(TutorialPage.class, "9ce94a04892cf629a0dddee199470f2d", i25);
        fQL = a19;
        fmVarArr[i25] = a19;
        int i26 = i25 + 1;
        C2491fm a20 = C4105zY.m41624a(TutorialPage.class, "f646f0516168f6368a64d21cb20383b7", i26);
        fQM = a20;
        fmVarArr[i26] = a20;
        int i27 = i26 + 1;
        C2491fm a21 = C4105zY.m41624a(TutorialPage.class, "6e003a7fc572c00c90bfca9071d15d68", i27);
        fQN = a21;
        fmVarArr[i27] = a21;
        int i28 = i27 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TutorialPage.class, C5246aBy.class, _m_fields, _m_methods);
    }

    /* renamed from: V */
    private void m23061V(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(fQz, iZVar);
    }

    /* renamed from: a */
    private void m23064a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4725bL, uuid);
    }

    /* renamed from: an */
    private UUID m23065an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4725bL);
    }

    /* renamed from: bq */
    private void m23069bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4723Qz, i18NString);
    }

    /* renamed from: c */
    private void m23072c(UUID uuid) {
        switch (bFf().mo6893i(f4727bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4727bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4727bO, new Object[]{uuid}));
                break;
        }
        m23068b(uuid);
    }

    private I18NString cdP() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4721Nl);
    }

    private I18NString cdQ() {
        return (I18NString) bFf().mo5608dq().mo3214p(fQx);
    }

    private C2686iZ cdR() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(fQz);
    }

    /* renamed from: mD */
    private void m23074mD(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4721Nl, i18NString);
    }

    /* renamed from: mE */
    private void m23075mE(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(fQx, i18NString);
    }

    /* renamed from: vS */
    private I18NString m23079vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4723Qz);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5246aBy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m23066ap();
            case 1:
                m23068b((UUID) args[0]);
                return null;
            case 2:
                return m23067ar();
            case 3:
                aMm();
                return null;
            case 4:
                return m23078rO();
            case 5:
                m23070bs((I18NString) args[0]);
                return null;
            case 6:
                return cdS();
            case 7:
                m23076mF((I18NString) args[0]);
                return null;
            case 8:
                m23062a((C6123agr) args[0]);
                return null;
            case 9:
                m23071c((C6123agr) args[0]);
                return null;
            case 10:
                return cdU();
            case 11:
                return cdW();
            case 12:
                m23077mH((I18NString) args[0]);
                return null;
            case 13:
                m23073ej(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                return new Boolean(cdY());
            case 15:
                return new Boolean(cdZ());
            case 16:
                return new Boolean(ceb());
            case 17:
                return new Boolean(ced());
            case 18:
                return cef();
            case 19:
                m23063a((C6257ajV.C1922a) args[0]);
                return null;
            case 20:
                ceh();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: package-private */
    @C3974xb
    public void aMn() {
        switch (bFf().mo6893i(cQi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQi, new Object[0]));
                break;
        }
        aMm();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4726bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4726bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4726bN, new Object[0]));
                break;
        }
        return m23066ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Rules to complete")
    /* renamed from: b */
    public void mo14199b(C6123agr agr) {
        switch (bFf().mo6893i(fQC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQC, new Object[]{agr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQC, new Object[]{agr}));
                break;
        }
        m23062a(agr);
    }

    @C3974xb
    /* renamed from: b */
    public void mo14200b(C6257ajV.C1922a aVar) {
        switch (bFf().mo6893i(fQM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQM, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQM, new Object[]{aVar}));
                break;
        }
        m23063a(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title (max. 36 char.)")
    /* renamed from: bt */
    public void mo14201bt(I18NString i18NString) {
        switch (bFf().mo6893i(f4722QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4722QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4722QD, new Object[]{i18NString}));
                break;
        }
        m23070bs(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Content")
    public I18NString cdT() {
        switch (bFf().mo6893i(fQA)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fQA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fQA, new Object[0]));
                break;
        }
        return cdS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Rules to complete")
    public Collection<C6123agr> cdV() {
        switch (bFf().mo6893i(fQE)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, fQE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fQE, new Object[0]));
                break;
        }
        return cdU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Success Message")
    public I18NString cdX() {
        switch (bFf().mo6893i(fQF)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fQF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fQF, new Object[0]));
                break;
        }
        return cdW();
    }

    @C3974xb
    public boolean cea() {
        switch (bFf().mo6893i(fQI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fQI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fQI, new Object[0]));
                break;
        }
        return cdZ();
    }

    @C3974xb
    public boolean cec() {
        switch (bFf().mo6893i(fQJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fQJ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fQJ, new Object[0]));
                break;
        }
        return ceb();
    }

    @C3974xb
    public boolean cee() {
        switch (bFf().mo6893i(fQK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fQK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fQK, new Object[0]));
                break;
        }
        return ced();
    }

    @C3974xb
    public C6257ajV.C1922a ceg() {
        switch (bFf().mo6893i(fQL)) {
            case 0:
                return null;
            case 2:
                return (C6257ajV.C1922a) bFf().mo5606d(new aCE(this, fQL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fQL, new Object[0]));
                break;
        }
        return cef();
    }

    public void cei() {
        switch (bFf().mo6893i(fQN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQN, new Object[0]));
                break;
        }
        ceh();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Rules to complete")
    /* renamed from: d */
    public void mo14210d(C6123agr agr) {
        switch (bFf().mo6893i(fQD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQD, new Object[]{agr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQD, new Object[]{agr}));
                break;
        }
        m23071c(agr);
    }

    @C3974xb
    /* renamed from: ek */
    public void mo14211ek(boolean z) {
        switch (bFf().mo6893i(fyc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fyc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fyc, new Object[]{new Boolean(z)}));
                break;
        }
        m23073ej(z);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4728bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4728bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4728bP, new Object[0]));
                break;
        }
        return m23067ar();
    }

    @C3974xb
    public boolean isComplete() {
        switch (bFf().mo6893i(fQH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fQH, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fQH, new Object[0]));
                break;
        }
        return cdY();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Content")
    /* renamed from: mG */
    public void mo14213mG(I18NString i18NString) {
        switch (bFf().mo6893i(fQB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQB, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQB, new Object[]{i18NString}));
                break;
        }
        m23076mF(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Success Message")
    /* renamed from: mI */
    public void mo14214mI(I18NString i18NString) {
        switch (bFf().mo6893i(fQG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQG, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQG, new Object[]{i18NString}));
                break;
        }
        m23077mH(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title (max. 36 char.)")
    /* renamed from: rP */
    public I18NString mo14215rP() {
        switch (bFf().mo6893i(f4720MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4720MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4720MN, new Object[0]));
                break;
        }
        return m23078rO();
    }

    @C0064Am(aul = "7a1263aaeb5f30b16424ccd317710b99", aum = 0)
    /* renamed from: ap */
    private UUID m23066ap() {
        return m23065an();
    }

    @C0064Am(aul = "8f8d8246235e9b7922de51b3819b05dc", aum = 0)
    /* renamed from: b */
    private void m23068b(UUID uuid) {
        m23064a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m23064a(UUID.randomUUID());
    }

    @C0064Am(aul = "bb370e29955d4f9b3256b492d8dc27a1", aum = 0)
    /* renamed from: ar */
    private String m23067ar() {
        return bFY();
    }

    @C0064Am(aul = "fe316b8d7ac62f364807d62db8f4e7dd", aum = 0)
    @C3974xb
    private void aMm() {
        mo14211ek(true);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title (max. 36 char.)")
    @C0064Am(aul = "3c6ecf80e80c7c4227d4e6e3fde9d07e", aum = 0)
    /* renamed from: rO */
    private I18NString m23078rO() {
        return m23079vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title (max. 36 char.)")
    @C0064Am(aul = "1dbc7786a2c398ef737b9fd3460f7e0b", aum = 0)
    /* renamed from: bs */
    private void m23070bs(I18NString i18NString) {
        m23069bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Content")
    @C0064Am(aul = "63251af8fa9f294a2ea06d24861ae2fe", aum = 0)
    private I18NString cdS() {
        return cdP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Content")
    @C0064Am(aul = "762651a9518d387ef8624e395c754406", aum = 0)
    /* renamed from: mF */
    private void m23076mF(I18NString i18NString) {
        m23074mD(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Rules to complete")
    @C0064Am(aul = "a6ef171c5fe3b8990df25ef6329e06de", aum = 0)
    /* renamed from: a */
    private void m23062a(C6123agr agr) {
        cdR().add(agr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Rules to complete")
    @C0064Am(aul = "0602164273db1f281460108dc8040f93", aum = 0)
    /* renamed from: c */
    private void m23071c(C6123agr agr) {
        cdR().remove(agr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Rules to complete")
    @C0064Am(aul = "cc942bcf55f8532884dd0804eb96d425", aum = 0)
    private Collection<C6123agr> cdU() {
        return Collections.unmodifiableCollection(cdR());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Success Message")
    @C0064Am(aul = "4d259860c4becaf2de863ca0fafb673c", aum = 0)
    private I18NString cdW() {
        return cdQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Success Message")
    @C0064Am(aul = "8fb9af79f5af67a864c568e425ffc0a5", aum = 0)
    /* renamed from: mH */
    private void m23077mH(I18NString i18NString) {
        m23075mE(i18NString);
    }

    @C0064Am(aul = "0bdead703b97898a77332957413880a1", aum = 0)
    @C3974xb
    /* renamed from: ej */
    private void m23073ej(boolean z) {
        for (C6123agr ek : cdR()) {
            ek.mo13467ek(z);
        }
    }

    @C0064Am(aul = "d95aeeb64fb89fdeef39f1da9db6fdee", aum = 0)
    @C3974xb
    private boolean cdY() {
        for (C6123agr isBlocked : cdR()) {
            if (isBlocked.isBlocked()) {
                return false;
            }
        }
        return true;
    }

    @C0064Am(aul = "e50d666a0ea7abf6d6e0611c830c076a", aum = 0)
    @C3974xb
    private boolean cdZ() {
        return !cdP().get().equals("") && !cdP().get().endsWith(".content");
    }

    @C0064Am(aul = "3315a89f3dd90336648e8b66940639c2", aum = 0)
    @C3974xb
    private boolean ceb() {
        return !cdQ().get().equals("") && !cdQ().get().endsWith(".successContent");
    }

    @C0064Am(aul = "e21fca06ba12bdfd1ea07e8e525ed9b1", aum = 0)
    @C3974xb
    private boolean ced() {
        return cdR().size() > 0;
    }

    @C0064Am(aul = "9ce94a04892cf629a0dddee199470f2d", aum = 0)
    @C3974xb
    private C6257ajV.C1922a cef() {
        return this.fQu;
    }

    @C0064Am(aul = "f646f0516168f6368a64d21cb20383b7", aum = 0)
    @C3974xb
    /* renamed from: a */
    private void m23063a(C6257ajV.C1922a aVar) {
        if (!aVar.equals(C6257ajV.C1922a.LAST_PAGE) || !cec()) {
            this.fQu = aVar;
        } else {
            this.fQu = C6257ajV.C1922a.LAST_PAGE_WITH_CONFIRM;
        }
    }

    @C0064Am(aul = "6e003a7fc572c00c90bfca9071d15d68", aum = 0)
    private void ceh() {
        for (C6123agr ek : cdR()) {
            ek.mo13467ek(true);
        }
    }
}
