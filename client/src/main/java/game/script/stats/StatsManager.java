package game.script.stats;

import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.TaikodomObject;
import game.script.item.BlueprintType;
import game.script.item.ItemType;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.npc.NPC;
import game.script.player.Player;
import game.script.player.User;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6526aoe;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C5878acG;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@C5511aMd
@C6485anp
/* renamed from: a.aBK */
/* compiled from: a */
public class StatsManager extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz hhM = null;
    public static final C5663aRz hhN = null;
    public static final C2491fm hhZ = null;
    public static final C2491fm hia = null;
    public static final C2491fm hib = null;
    public static final C2491fm hic = null;
    public static final C2491fm hid = null;
    public static final C2491fm hie = null;
    public static final C2491fm hif = null;
    public static final C2491fm hig = null;
    public static final C2491fm hih = null;
    public static final C2491fm hii = null;
    public static final C2491fm hij = null;
    public static final C2491fm hik = null;
    public static final C2491fm hil = null;
    public static final C2491fm him = null;
    public static final C2491fm hin = null;
    public static final C2491fm hio = null;
    public static final C2491fm hip = null;
    public static final C2491fm hiq = null;
    public static final C2491fm hir = null;
    public static final C2491fm his = null;
    public static final C2491fm hit = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    static Log logger = LogPrinter.setClass(StatsManager.class);
    private static String SEPARATOR = "||";
    @C0064Am(aul = "f6e04e94cfbca698c1a0c691b41e36cf", aum = 0)
    private static C3438ra<String> ggR;
    @C0064Am(aul = "034907c5153fceac77f48b0a24b068bb", aum = 1)
    private static C3438ra<String> ggS;
    private static SimpleDateFormat hhO;
    private static String hhP = "kill";
    private static String hhQ = "advertisement";
    private static String hhR = "mission";
    private static String hhS = "craft";
    private static String hhT = "extract";
    private static String hhU = "login";
    private static String hhV = "logout";
    private static String hhW = "ping";
    private static String hhX = "restart";
    private static int hhY = 60;

    static {
        m12806V();
    }

    public StatsManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StatsManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12806V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 23;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(StatsManager.class, "f6e04e94cfbca698c1a0c691b41e36cf", i);
        hhM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StatsManager.class, "034907c5153fceac77f48b0a24b068bb", i2);
        hhN = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 23)];
        C2491fm a = C4105zY.m41624a(StatsManager.class, "2f0713db67268a22468868ec53ff649d", i4);
        hhZ = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(StatsManager.class, "eaf2604cc95498c84d82442d46eec80d", i5);
        hia = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(StatsManager.class, "9734fdc4a105a639776822e31b360ae6", i6);
        hib = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(StatsManager.class, "2b744efd607bd30ba2f8d2cf95d2cd0e", i7);
        hic = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(StatsManager.class, "0962333fd3b5228f2d9371b2c4941f7e", i8);
        hid = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(StatsManager.class, "5f11350d9a6c655477ad921d5a50d39e", i9);
        hie = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(StatsManager.class, "d2e352587bba499bdbe28ae9f61d7a67", i10);
        hif = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(StatsManager.class, "a42a8c2ba640ef988fab126ab9a96acc", i11);
        hig = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(StatsManager.class, "41127bf50519fc0cb2f97b5b0a99005a", i12);
        hih = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(StatsManager.class, "d658a6a40b5fcf8ffebdf65a78860636", i13);
        hii = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        C2491fm a11 = C4105zY.m41624a(StatsManager.class, "091ad717371df06ecb9abacc17b07457", i14);
        hij = a11;
        fmVarArr[i14] = a11;
        int i15 = i14 + 1;
        C2491fm a12 = C4105zY.m41624a(StatsManager.class, "86c1a592e4ee932c77c683d64bb7e014", i15);
        hik = a12;
        fmVarArr[i15] = a12;
        int i16 = i15 + 1;
        C2491fm a13 = C4105zY.m41624a(StatsManager.class, "3deadb5fac3708e57d7521473ef71076", i16);
        hil = a13;
        fmVarArr[i16] = a13;
        int i17 = i16 + 1;
        C2491fm a14 = C4105zY.m41624a(StatsManager.class, "026333a54c9a8e3a97c75ce60f82acf0", i17);
        him = a14;
        fmVarArr[i17] = a14;
        int i18 = i17 + 1;
        C2491fm a15 = C4105zY.m41624a(StatsManager.class, "81b907a667203573d0a6c9026ecc9d5f", i18);
        hin = a15;
        fmVarArr[i18] = a15;
        int i19 = i18 + 1;
        C2491fm a16 = C4105zY.m41624a(StatsManager.class, "3215dc7caea733fb8ddf27e4e954439d", i19);
        _f_onResurrect_0020_0028_0029V = a16;
        fmVarArr[i19] = a16;
        int i20 = i19 + 1;
        C2491fm a17 = C4105zY.m41624a(StatsManager.class, "e7145a72fc1be1aee0df83fdb4a670b1", i20);
        _f_tick_0020_0028F_0029V = a17;
        fmVarArr[i20] = a17;
        int i21 = i20 + 1;
        C2491fm a18 = C4105zY.m41624a(StatsManager.class, "663544aecc5394aa1efd078b72c88c7a", i21);
        hio = a18;
        fmVarArr[i21] = a18;
        int i22 = i21 + 1;
        C2491fm a19 = C4105zY.m41624a(StatsManager.class, "13d3770fe63d422594d2c39f35bff877", i22);
        hip = a19;
        fmVarArr[i22] = a19;
        int i23 = i22 + 1;
        C2491fm a20 = C4105zY.m41624a(StatsManager.class, "c78a50145f880ec294d33d909cff57a8", i23);
        hiq = a20;
        fmVarArr[i23] = a20;
        int i24 = i23 + 1;
        C2491fm a21 = C4105zY.m41624a(StatsManager.class, "72fedcd6d5a74015991f0dbf714bc25f", i24);
        hir = a21;
        fmVarArr[i24] = a21;
        int i25 = i24 + 1;
        C2491fm a22 = C4105zY.m41624a(StatsManager.class, "c898ed19ae61d611c58b5c0c9811848e", i25);
        his = a22;
        fmVarArr[i25] = a22;
        int i26 = i25 + 1;
        C2491fm a23 = C4105zY.m41624a(StatsManager.class, "411d5f7d9ac3b1ddfb004ea3f24e4d09", i26);
        hit = a23;
        fmVarArr[i26] = a23;
        int i27 = i26 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StatsManager.class, C6526aoe.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private int m12804B(User adk) {
        switch (bFf().mo6893i(hik)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hik, new Object[]{adk}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hik, new Object[]{adk}));
                break;
        }
        return m12803A(adk);
    }

    private C3438ra cJB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hhM);
    }

    private C3438ra cJC() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hhN);
    }

    private SimpleDateFormat cJM() {
        switch (bFf().mo6893i(hig)) {
            case 0:
                return null;
            case 2:
                return (SimpleDateFormat) bFf().mo5606d(new aCE(this, hig, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hig, new Object[0]));
                break;
        }
        return cJL();
    }

    /* renamed from: co */
    private void m12814co(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hhM, raVar);
    }

    /* renamed from: cp */
    private void m12815cp(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hhN, raVar);
    }

    @C0401Fa
    /* renamed from: lf */
    private void m12820lf(String str) {
        switch (bFf().mo6893i(hif)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hif, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hif, new Object[]{str}));
                break;
        }
        m12819le(str);
    }

    /* renamed from: lh */
    private String m12822lh(String str) {
        switch (bFf().mo6893i(hih)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hih, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, hih, new Object[]{str}));
                break;
        }
        return m12821lg(str);
    }

    /* renamed from: lj */
    private void m12824lj(String str) {
        switch (bFf().mo6893i(hin)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hin, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hin, new Object[]{str}));
                break;
        }
        m12823li(str);
    }

    @C0064Am(aul = "663544aecc5394aa1efd078b72c88c7a", aum = 0)
    @C5566aOg
    /* renamed from: lk */
    private void m12825lk(String str) {
        throw new aWi(new aCE(this, hio, new Object[]{str}));
    }

    @C5566aOg
    /* renamed from: ll */
    private void m12826ll(String str) {
        switch (bFf().mo6893i(hio)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hio, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hio, new Object[]{str}));
                break;
        }
        m12825lk(str);
    }

    /* renamed from: m */
    private String m12828m(String[] strArr) {
        switch (bFf().mo6893i(hii)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hii, new Object[]{strArr}));
            case 3:
                bFf().mo5606d(new aCE(this, hii, new Object[]{strArr}));
                break;
        }
        return m12816l(strArr);
    }

    /* renamed from: n */
    private int m12829n(ClientConnect bVar) {
        switch (bFf().mo6893i(hil)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hil, new Object[]{bVar}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hil, new Object[]{bVar}));
                break;
        }
        return m12827m(bVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m12805U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6526aoe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cJD();
            case 1:
                return cJF();
            case 2:
                m12817la((String) args[0]);
                return null;
            case 3:
                return cJH();
            case 4:
                return cJJ();
            case 5:
                m12818lc((String) args[0]);
                return null;
            case 6:
                m12819le((String) args[0]);
                return null;
            case 7:
                return cJL();
            case 8:
                return m12821lg((String) args[0]);
            case 9:
                return m12816l((String[]) args[0]);
            case 10:
                m12830y((User) args[0]);
                return null;
            case 11:
                return new Integer(m12803A((User) args[0]));
            case 12:
                return new Integer(m12827m((ClientConnect) args[0]));
            case 13:
                m12808a((User) args[0], (ClientConnect) args[1]);
                return null;
            case 14:
                m12823li((String) args[0]);
                return null;
            case 15:
                m12813aG();
                return null;
            case 16:
                m12805U(((Float) args[0]).floatValue());
                return null;
            case 17:
                m12825lk((String) args[0]);
                return null;
            case 18:
                m12810a((Character) args[0], (Pawn) args[1], (WeaponType) args[2]);
                return null;
            case 19:
                m12811a((Player) args[0], (String) args[1], ((Long) args[2]).longValue());
                return null;
            case 20:
                m12809a((MissionTemplate) args[0], (Player) args[1]);
                return null;
            case 21:
                m12807a((BlueprintType) args[0], (Player) args[1]);
                return null;
            case 22:
                m12812a((ItemType) args[0], ((Float) args[1]).floatValue(), (Player) args[2]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m12813aG();
    }

    /* renamed from: b */
    public void mo7841b(BlueprintType mr, Player aku) {
        switch (bFf().mo6893i(his)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, his, new Object[]{mr, aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, his, new Object[]{mr, aku}));
                break;
        }
        m12807a(mr, aku);
    }

    /* renamed from: b */
    public void mo7842b(User adk, ClientConnect bVar) {
        switch (bFf().mo6893i(him)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, him, new Object[]{adk, bVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, him, new Object[]{adk, bVar}));
                break;
        }
        m12808a(adk, bVar);
    }

    /* renamed from: b */
    public void mo7843b(MissionTemplate avh, Player aku) {
        switch (bFf().mo6893i(hir)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hir, new Object[]{avh, aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hir, new Object[]{avh, aku}));
                break;
        }
        m12809a(avh, aku);
    }

    /* renamed from: b */
    public void mo7844b(Character acx, Pawn avi, WeaponType apt) {
        switch (bFf().mo6893i(hip)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hip, new Object[]{acx, avi, apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hip, new Object[]{acx, avi, apt}));
                break;
        }
        m12810a(acx, avi, apt);
    }

    /* renamed from: b */
    public void mo7845b(Player aku, String str, long j) {
        switch (bFf().mo6893i(hiq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hiq, new Object[]{aku, str, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hiq, new Object[]{aku, str, new Long(j)}));
                break;
        }
        m12811a(aku, str, j);
    }

    /* renamed from: b */
    public void mo7846b(ItemType jCVar, float f, Player aku) {
        switch (bFf().mo6893i(hit)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hit, new Object[]{jCVar, new Float(f), aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hit, new Object[]{jCVar, new Float(f), aku}));
                break;
        }
        m12812a(jCVar, f, aku);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C3438ra<String> cJE() {
        switch (bFf().mo6893i(hhZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, hhZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hhZ, new Object[0]));
                break;
        }
        return cJD();
    }

    public ArrayList<String> cJG() {
        switch (bFf().mo6893i(hia)) {
            case 0:
                return null;
            case 2:
                return (ArrayList) bFf().mo5606d(new aCE(this, hia, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hia, new Object[0]));
                break;
        }
        return cJF();
    }

    public C3438ra<String> cJI() {
        switch (bFf().mo6893i(hic)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, hic, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hic, new Object[0]));
                break;
        }
        return cJH();
    }

    public ArrayList<String> cJK() {
        switch (bFf().mo6893i(hid)) {
            case 0:
                return null;
            case 2:
                return (ArrayList) bFf().mo5606d(new aCE(this, hid, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hid, new Object[0]));
                break;
        }
        return cJJ();
    }

    /* renamed from: lb */
    public void mo7851lb(String str) {
        switch (bFf().mo6893i(hib)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hib, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hib, new Object[]{str}));
                break;
        }
        m12817la(str);
    }

    /* renamed from: ld */
    public void mo7852ld(String str) {
        switch (bFf().mo6893i(hie)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hie, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hie, new Object[]{str}));
                break;
        }
        m12818lc(str);
    }

    /* renamed from: z */
    public void mo7853z(User adk) {
        switch (bFf().mo6893i(hij)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hij, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hij, new Object[]{adk}));
                break;
        }
        m12830y(adk);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m12824lj(m12822lh(hhX));
        mo8361ms((float) hhY);
    }

    @C0064Am(aul = "2f0713db67268a22468868ec53ff649d", aum = 0)
    private C3438ra<String> cJD() {
        return cJB();
    }

    @C0064Am(aul = "eaf2604cc95498c84d82442d46eec80d", aum = 0)
    private ArrayList<String> cJF() {
        ArrayList<String> arrayList = new ArrayList<>(cJE());
        cJB().clear();
        return arrayList;
    }

    @C0064Am(aul = "9734fdc4a105a639776822e31b360ae6", aum = 0)
    /* renamed from: la */
    private void m12817la(String str) {
        cJE().add(str);
    }

    @C0064Am(aul = "2b744efd607bd30ba2f8d2cf95d2cd0e", aum = 0)
    private C3438ra<String> cJH() {
        return cJC();
    }

    @C0064Am(aul = "0962333fd3b5228f2d9371b2c4941f7e", aum = 0)
    private ArrayList<String> cJJ() {
        ArrayList<String> arrayList = new ArrayList<>(cJI());
        cJC().clear();
        return arrayList;
    }

    @C0064Am(aul = "5f11350d9a6c655477ad921d5a50d39e", aum = 0)
    /* renamed from: lc */
    private void m12818lc(String str) {
        cJI().add(str);
        m12820lf(str);
    }

    @C0401Fa
    @C0064Am(aul = "d2e352587bba499bdbe28ae9f61d7a67", aum = 0)
    /* renamed from: le */
    private void m12819le(String str) {
        C0811Li.log(str);
    }

    @C0064Am(aul = "a42a8c2ba640ef988fab126ab9a96acc", aum = 0)
    private SimpleDateFormat cJL() {
        if (hhO == null) {
            hhO = new SimpleDateFormat(C5878acG.feC);
        }
        return hhO;
    }

    @C0064Am(aul = "41127bf50519fc0cb2f97b5b0a99005a", aum = 0)
    /* renamed from: lg */
    private String m12821lg(String str) {
        return String.valueOf(cJM().format(new Date())) + SEPARATOR + str;
    }

    @C0064Am(aul = "d658a6a40b5fcf8ffebdf65a78860636", aum = 0)
    /* renamed from: l */
    private String m12816l(String[] strArr) {
        String format = cJM().format(new Date());
        for (int i = 0; i < strArr.length; i++) {
            format = String.valueOf(format) + SEPARATOR + strArr[i];
        }
        return format;
    }

    @C0064Am(aul = "091ad717371df06ecb9abacc17b07457", aum = 0)
    /* renamed from: y */
    private void m12830y(User adk) {
        m12824lj(m12828m(new String[]{hhU, new StringBuilder(String.valueOf(adk.cTe())).toString(), adk.getUsername(), adk.cTk(), new StringBuilder(String.valueOf(m12804B(adk))).toString()}));
    }

    @C0064Am(aul = "86c1a592e4ee932c77c683d64bb7e014", aum = 0)
    /* renamed from: A */
    private int m12803A(User adk) {
        if (adk.cSV() != null) {
            return m12829n(adk.cSV().bgt());
        }
        return 0;
    }

    @C0064Am(aul = "3deadb5fac3708e57d7521473ef71076", aum = 0)
    /* renamed from: m */
    private int m12827m(ClientConnect bVar) {
        if (bVar != null) {
            return bVar.getId();
        }
        return 0;
    }

    @C0064Am(aul = "026333a54c9a8e3a97c75ce60f82acf0", aum = 0)
    /* renamed from: a */
    private void m12808a(User adk, ClientConnect bVar) {
        m12824lj(m12828m(new String[]{hhV, new StringBuilder(String.valueOf(adk.cTe())).toString(), adk.getUsername(), new StringBuilder(String.valueOf(m12829n(bVar))).toString()}));
    }

    @C0064Am(aul = "81b907a667203573d0a6c9026ecc9d5f", aum = 0)
    /* renamed from: li */
    private void m12823li(String str) {
        mo7852ld(str);
    }

    @C0064Am(aul = "3215dc7caea733fb8ddf27e4e954439d", aum = 0)
    /* renamed from: aG */
    private void m12813aG() {
        super.mo70aH();
        m12824lj(m12822lh(hhX));
        mo8361ms((float) hhY);
    }

    @C0064Am(aul = "e7145a72fc1be1aee0df83fdb4a670b1", aum = 0)
    /* renamed from: U */
    private void m12805U(float f) {
        m12824lj(m12822lh(hhW));
        mo8361ms((float) hhY);
    }

    @C0064Am(aul = "13d3770fe63d422594d2c39f35bff877", aum = 0)
    /* renamed from: a */
    private void m12810a(Character acx, Pawn avi, WeaponType apt) {
        Character agj;
        boolean z;
        if (apt != null) {
            try {
                if ((acx instanceof Player) && (avi instanceof Ship)) {
                    Player aku = (Player) acx;
                    Ship fAVar = (Ship) avi;
                    if (!fAVar.isStatic() && (agj = fAVar.agj()) != null) {
                        Ship bQx = aku.bQx();
                        long cqo = bQx.agH().bFf().getObjectId().getId();
                        long cqo2 = apt.bFf().getObjectId().getId();
                        long j = 0;
                        String str = "";
                        int i = 0;
                        String str2 = "";
                        if (agj instanceof Player) {
                            Player aku2 = (Player) agj;
                            j = (long) aku2.bOx().cTe();
                            str = aku2.bOx().getUsername();
                            i = aku2.cTe();
                            z = true;
                            str2 = aku2.getName();
                        } else if (agj instanceof NPC) {
                            j = ((NPC) agj).mo11654Fs().bFf().getObjectId().getId();
                            z = false;
                        } else {
                            z = false;
                        }
                        m12826ll(String.valueOf(hhP) + SEPARATOR + aku.bOx().cTe() + SEPARATOR + aku.bOx().getUsername() + SEPARATOR + aku.cTe() + SEPARATOR + aku.getName() + SEPARATOR + cqo + SEPARATOR + cqo2 + SEPARATOR + j + SEPARATOR + str + SEPARATOR + i + SEPARATOR + str2 + SEPARATOR + fAVar.agH().bFf().getObjectId().getId() + SEPARATOR + z + SEPARATOR + bQx.mo960Nc().bFf().getObjectId().getId() + SEPARATOR + bQx.azW().bFf().getObjectId().getId());
                    }
                }
            } catch (Exception e) {
                logger.error(e);
            }
        }
    }

    @C0064Am(aul = "c78a50145f880ec294d33d909cff57a8", aum = 0)
    /* renamed from: a */
    private void m12811a(Player aku, String str, long j) {
        m12826ll(String.valueOf(hhQ) + SEPARATOR + aku.bOx().getUsername() + SEPARATOR + aku.bOx().cTe() + SEPARATOR + aku.getName() + SEPARATOR + aku.cTe() + SEPARATOR + str + SEPARATOR + j);
    }

    @C0064Am(aul = "72fedcd6d5a74015991f0dbf714bc25f", aum = 0)
    /* renamed from: a */
    private void m12809a(MissionTemplate avh, Player aku) {
        m12826ll(String.valueOf(hhR) + SEPARATOR + aku.bOx().cTe() + SEPARATOR + aku.bOx().getUsername() + SEPARATOR + aku.cTe() + SEPARATOR + aku.getName() + SEPARATOR + avh.bFf().getObjectId().getId());
    }

    @C0064Am(aul = "c898ed19ae61d611c58b5c0c9811848e", aum = 0)
    /* renamed from: a */
    private void m12807a(BlueprintType mr, Player aku) {
        m12826ll(String.valueOf(hhS) + SEPARATOR + aku.bOx().cTe() + SEPARATOR + aku.bOx().getUsername() + SEPARATOR + aku.cTe() + SEPARATOR + aku.getName() + SEPARATOR + mr.bFf().getObjectId().getId());
    }

    @C0064Am(aul = "411d5f7d9ac3b1ddfb004ea3f24e4d09", aum = 0)
    /* renamed from: a */
    private void m12812a(ItemType jCVar, float f, Player aku) {
        m12826ll(String.valueOf(hhT) + SEPARATOR + aku.bOx().cTe() + SEPARATOR + aku.bOx().getUsername() + SEPARATOR + aku.cTe() + SEPARATOR + aku.getName() + SEPARATOR + jCVar.bFf().getObjectId().getId() + SEPARATOR + f);
    }
}
