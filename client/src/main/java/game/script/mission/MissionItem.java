package game.script.mission;

import game.network.message.externalizable.aCE;
import game.script.item.Item;
import logic.baa.*;
import logic.data.mbean.C0673JZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.qP */
/* compiled from: a */
public class MissionItem extends aDJ implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aWS = null;
    /* renamed from: kO */
    public static final C5663aRz f8906kO = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0a1a4a04f173df1d6dcf40d274e02e84", aum = 0)
    private static Item aoR;

    static {
        m37505V();
    }

    public MissionItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionItem(C5540aNg ang) {
        super(ang);
    }

    public MissionItem(Item auq) {
        super((C5540aNg) null);
        super._m_script_init(auq);
    }

    /* renamed from: V */
    static void m37505V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 2;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(MissionItem.class, "0a1a4a04f173df1d6dcf40d274e02e84", i);
        f8906kO = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(MissionItem.class, "5cd0ffae331393e97e4f89986985556f", i3);
        aWS = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionItem.class, "914fca8f15e3c9ae3c855d75a341fa5e", i4);
        _f_dispose_0020_0028_0029V = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionItem.class, C0673JZ.class, _m_fields, _m_methods);
    }

    /* renamed from: XF */
    private Item m37506XF() {
        return (Item) bFf().mo5608dq().mo3214p(f8906kO);
    }

    /* renamed from: p */
    private void m37509p(Item auq) {
        bFf().mo5608dq().mo3197f(f8906kO, auq);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0673JZ(this);
    }

    /* renamed from: XH */
    public Item mo21346XH() {
        switch (bFf().mo6893i(aWS)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, aWS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aWS, new Object[0]));
                break;
        }
        return m37507XG();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m37507XG();
            case 1:
                m37508fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m37508fg();
    }

    /* renamed from: q */
    public void mo21347q(Item auq) {
        super.mo10S();
        m37509p(auq);
    }

    @C0064Am(aul = "5cd0ffae331393e97e4f89986985556f", aum = 0)
    /* renamed from: XG */
    private Item m37507XG() {
        return m37506XF();
    }

    @C0064Am(aul = "914fca8f15e3c9ae3c855d75a341fa5e", aum = 0)
    /* renamed from: fg */
    private void m37508fg() {
        m37506XF().dispose();
        super.dispose();
    }
}
