package game.script.mission;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C5560aOa;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C3437rZ(aak = "", aal = "taikodom.game.script.conflictZone.NPCSpawn")
@C5511aMd
@C6485anp
/* renamed from: a.JU */
/* compiled from: a */
public class NPCSpawn extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f782bL = null;
    /* renamed from: bM */
    public static final C5663aRz f783bM = null;
    /* renamed from: bN */
    public static final C2491fm f784bN = null;
    /* renamed from: bO */
    public static final C2491fm f785bO = null;
    /* renamed from: bP */
    public static final C2491fm f786bP = null;
    /* renamed from: bQ */
    public static final C2491fm f787bQ = null;
    public static final C5663aRz bfK = null;
    public static final C5663aRz dmT = null;
    public static final C2491fm dmU = null;
    public static final C2491fm dmV = null;
    public static final C2491fm dmW = null;
    public static final C2491fm dmX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3256f5278a22d3094081fdf18cf94578", aum = 2)

    /* renamed from: bK */
    private static UUID f781bK;
    @C0064Am(aul = "b3c301999bab56336612260b283afc09", aum = 1)
    private static int dmS;
    @C0064Am(aul = "1d5388b38f22ed1cfd176a49c030eac5", aum = 3)
    private static String handle;
    @C0064Am(aul = "0da2fd31342368d85c3a558e9a40c035", aum = 0)

    /* renamed from: nC */
    private static NPCType f788nC;

    static {
        m5770V();
    }

    public NPCSpawn() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCSpawn(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5770V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 9;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(NPCSpawn.class, "0da2fd31342368d85c3a558e9a40c035", i);
        bfK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCSpawn.class, "b3c301999bab56336612260b283afc09", i2);
        dmT = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPCSpawn.class, "3256f5278a22d3094081fdf18cf94578", i3);
        f782bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NPCSpawn.class, "1d5388b38f22ed1cfd176a49c030eac5", i4);
        f783bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(NPCSpawn.class, "e05ec29e4e2aacd53e07f52a25d5c65b", i6);
        f784bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCSpawn.class, "e0d44e91ff27b36a368f8a5481003b30", i7);
        f785bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCSpawn.class, "a1d4c3d7a9b09c2530e58c3748331f56", i8);
        f786bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCSpawn.class, "43dea497848f71eef2e5eae05229d28d", i9);
        f787bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCSpawn.class, "75de10f10f6516004e5fdd2a1b6d3a69", i10);
        dmU = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCSpawn.class, "1c2760cb478a34f599c822c0d2559768", i11);
        dmV = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCSpawn.class, "47b036c7721b82271e70c70ae5bae422", i12);
        dmW = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCSpawn.class, "e54710f69246060cc635be3cde70130b", i13);
        dmX = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(NPCSpawn.class, "76c87007bde1475f9c4448ef6c8bc4e4", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCSpawn.class, C5560aOa.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m5771a(String str) {
        bFf().mo5608dq().mo3197f(f783bM, str);
    }

    /* renamed from: a */
    private void m5772a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f782bL, uuid);
    }

    private NPCType abn() {
        return (NPCType) bFf().mo5608dq().mo3214p(bfK);
    }

    /* renamed from: an */
    private UUID m5773an() {
        return (UUID) bFf().mo5608dq().mo3214p(f782bL);
    }

    /* renamed from: ao */
    private String m5774ao() {
        return (String) bFf().mo5608dq().mo3214p(f783bM);
    }

    /* renamed from: b */
    private void m5778b(NPCType aed) {
        bFf().mo5608dq().mo3197f(bfK, aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "43dea497848f71eef2e5eae05229d28d", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m5779b(String str) {
        throw new aWi(new aCE(this, f787bQ, new Object[]{str}));
    }

    private int bax() {
        return bFf().mo5608dq().mo3212n(dmT);
    }

    /* renamed from: c */
    private void m5781c(UUID uuid) {
        switch (bFf().mo6893i(f785bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f785bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f785bO, new Object[]{uuid}));
                break;
        }
        m5780b(uuid);
    }

    /* renamed from: lp */
    private void m5782lp(int i) {
        bFf().mo5608dq().mo3183b(dmT, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Percent")
    @C0064Am(aul = "e54710f69246060cc635be3cde70130b", aum = 0)
    @C5566aOg
    /* renamed from: lq */
    private void m5783lq(int i) {
        throw new aWi(new aCE(this, dmX, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPCType")
    @C0064Am(aul = "1c2760cb478a34f599c822c0d2559768", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m5784p(NPCType aed) {
        throw new aWi(new aCE(this, dmV, new Object[]{aed}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5560aOa(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m5775ap();
            case 1:
                m5780b((UUID) args[0]);
                return null;
            case 2:
                return m5776ar();
            case 3:
                m5779b((String) args[0]);
                return null;
            case 4:
                return bay();
            case 5:
                m5784p((NPCType) args[0]);
                return null;
            case 6:
                return new Integer(baA());
            case 7:
                m5783lq(((Integer) args[0]).intValue());
                return null;
            case 8:
                return m5777au();
            default:
                return super.mo14a(gr);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPCType")
    @C5566aOg
    /* renamed from: a */
    public void mo3118a(NPCType aed) {
        switch (bFf().mo6893i(dmV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmV, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmV, new Object[]{aed}));
                break;
        }
        m5784p(aed);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f784bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f784bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f784bN, new Object[0]));
                break;
        }
        return m5775ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Percent")
    public int baB() {
        switch (bFf().mo6893i(dmW)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dmW, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmW, new Object[0]));
                break;
        }
        return baA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPCType")
    public NPCType baz() {
        switch (bFf().mo6893i(dmU)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, dmU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmU, new Object[0]));
                break;
        }
        return bay();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f786bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f786bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f786bP, new Object[0]));
                break;
        }
        return m5776ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f787bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f787bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f787bQ, new Object[]{str}));
                break;
        }
        m5779b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Percent")
    @C5566aOg
    /* renamed from: lr */
    public void mo3121lr(int i) {
        switch (bFf().mo6893i(dmX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmX, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmX, new Object[]{new Integer(i)}));
                break;
        }
        m5783lq(i);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m5777au();
    }

    @C0064Am(aul = "e05ec29e4e2aacd53e07f52a25d5c65b", aum = 0)
    /* renamed from: ap */
    private UUID m5775ap() {
        return m5773an();
    }

    @C0064Am(aul = "e0d44e91ff27b36a368f8a5481003b30", aum = 0)
    /* renamed from: b */
    private void m5780b(UUID uuid) {
        m5772a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m5772a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "a1d4c3d7a9b09c2530e58c3748331f56", aum = 0)
    /* renamed from: ar */
    private String m5776ar() {
        return m5774ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPCType")
    @C0064Am(aul = "75de10f10f6516004e5fdd2a1b6d3a69", aum = 0)
    private NPCType bay() {
        return abn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Percent")
    @C0064Am(aul = "47b036c7721b82271e70c70ae5bae422", aum = 0)
    private int baA() {
        return bax();
    }

    @C0064Am(aul = "76c87007bde1475f9c4448ef6c8bc4e4", aum = 0)
    /* renamed from: au */
    private String m5777au() {
        if (abn() == null) {
            return "null";
        }
        return abn().mo11470ke() + " " + bax() + "%";
    }
}
