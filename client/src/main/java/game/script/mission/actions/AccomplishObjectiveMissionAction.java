package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.mission.scripting.MissionScript;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6440amw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.asR  reason: case insensitive filesystem */
/* compiled from: a */
public class AccomplishObjectiveMissionAction extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f5281Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gxh = null;
    public static final C2491fm gxi = null;
    public static final C2491fm gxj = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "388f8c892a8acb8df3e3c25e9cb81943", aum = 0)
    private static String gaW;

    static {
        m25679V();
    }

    public AccomplishObjectiveMissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AccomplishObjectiveMissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25679V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 1;
        _m_methodCount = MissionAction._m_methodCount + 3;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(AccomplishObjectiveMissionAction.class, "388f8c892a8acb8df3e3c25e9cb81943", i);
        gxh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i3 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(AccomplishObjectiveMissionAction.class, "1131957fae3f2be7e643327128ad8e47", i3);
        gxi = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(AccomplishObjectiveMissionAction.class, "bc04e6cde61c8f5ea0d41974c7c258fd", i4);
        gxj = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(AccomplishObjectiveMissionAction.class, "592c908bb32ee5916d5bdc79d1ee755f", i5);
        f5281Dn = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AccomplishObjectiveMissionAction.class, C6440amw.class, _m_fields, _m_methods);
    }

    private String cuP() {
        return (String) bFf().mo5608dq().mo3214p(gxh);
    }

    /* renamed from: jR */
    private void m25681jR(String str) {
        bFf().mo5608dq().mo3197f(gxh, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Objective Handle")
    @C0064Am(aul = "bc04e6cde61c8f5ea0d41974c7c258fd", aum = 0)
    @C5566aOg
    /* renamed from: jS */
    private void m25682jS(String str) {
        throw new aWi(new aCE(this, gxj, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6440amw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return cuQ();
            case 1:
                m25682jS((String) args[0]);
                return null;
            case 2:
                m25680a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f5281Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5281Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5281Dn, new Object[]{aiw, af}));
                break;
        }
        m25680a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Objective Handle")
    public String cuR() {
        switch (bFf().mo6893i(gxi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gxi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gxi, new Object[0]));
                break;
        }
        return cuQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Objective Handle")
    @C5566aOg
    /* renamed from: jT */
    public void mo15930jT(String str) {
        switch (bFf().mo6893i(gxj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gxj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gxj, new Object[]{str}));
                break;
        }
        m25682jS(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Objective Handle")
    @C0064Am(aul = "1131957fae3f2be7e643327128ad8e47", aum = 0)
    private String cuQ() {
        return cuP();
    }

    @C0064Am(aul = "592c908bb32ee5916d5bdc79d1ee755f", aum = 0)
    /* renamed from: a */
    private void m25680a(C5426aIw aiw, Mission af) {
        if (af instanceof MissionScript) {
            ((MissionScript) af).mo566b(cuP(), Mission.C0015a.ACCOMPLISHED);
        }
    }
}
