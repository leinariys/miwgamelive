package game.script.mission.actions;

import game.network.message.externalizable.C5311aEl;
import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.npc.NPCType;
import game.script.resource.Asset;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1174RO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.sg */
/* compiled from: a */
public class ShowNPCMessage extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f9147Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bfK = null;
    public static final C5663aRz bfL = null;
    public static final C5663aRz bfN = null;
    public static final C5663aRz bfP = null;
    public static final C2491fm bfQ = null;
    public static final C2491fm bfR = null;
    public static final C2491fm bfS = null;
    public static final C2491fm bfT = null;
    public static final C2491fm bfU = null;
    public static final C2491fm bfV = null;
    public static final C2491fm bfW = null;
    public static final C2491fm bfX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bef3fd672c656166a311f935f5f0093d", aum = 1)
    private static I18NString auP;
    @C0064Am(aul = "972eecc9b5f5594ff1de0c8b21fb9775", aum = 2)
    private static float bfM;
    @C0064Am(aul = "c9fed26ddc367dd177a69fa48f04d21b", aum = 3)
    private static Asset bfO;
    @C0064Am(aul = "28e662287caafff0c51e108351fbd6d3", aum = 0)

    /* renamed from: nC */
    private static NPCType f9148nC;

    static {
        m39074V();
    }

    public ShowNPCMessage() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShowNPCMessage(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39074V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 4;
        _m_methodCount = MissionAction._m_methodCount + 9;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ShowNPCMessage.class, "28e662287caafff0c51e108351fbd6d3", i);
        bfK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShowNPCMessage.class, "bef3fd672c656166a311f935f5f0093d", i2);
        bfL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShowNPCMessage.class, "972eecc9b5f5594ff1de0c8b21fb9775", i3);
        bfN = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShowNPCMessage.class, "c9fed26ddc367dd177a69fa48f04d21b", i4);
        bfP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i6 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(ShowNPCMessage.class, "a0d76b1085a9ca6742c48a9897de61ee", i6);
        bfQ = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ShowNPCMessage.class, "62918d2a562695a245e05197fc083770", i7);
        bfR = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ShowNPCMessage.class, "689f0887ca9a46db1b3699bb53498768", i8);
        bfS = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ShowNPCMessage.class, "1095496400b18e91a5f0c78a49a0223e", i9);
        bfT = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ShowNPCMessage.class, "7ffd4ec7d8842b562420f22917ce0c78", i10);
        bfU = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ShowNPCMessage.class, "f4483f7794e2da118ef39b1d257f9e67", i11);
        bfV = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ShowNPCMessage.class, "8b022fa6e3c066a720609f4f41f1ccf5", i12);
        bfW = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ShowNPCMessage.class, "381ee79573b92ba0d50150210df7747d", i13);
        bfX = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ShowNPCMessage.class, "a5b690c81528d8d48e837568530d2a6c", i14);
        f9147Dn = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShowNPCMessage.class, C1174RO.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m39072S(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bfP, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C0064Am(aul = "381ee79573b92ba0d50150210df7747d", aum = 0)
    @C5566aOg
    /* renamed from: T */
    private void m39073T(Asset tCVar) {
        throw new aWi(new aCE(this, bfX, new Object[]{tCVar}));
    }

    private NPCType abn() {
        return (NPCType) bFf().mo5608dq().mo3214p(bfK);
    }

    private I18NString abo() {
        return (I18NString) bFf().mo5608dq().mo3214p(bfL);
    }

    private float abp() {
        return bFf().mo5608dq().mo3211m(bfN);
    }

    private Asset abq() {
        return (Asset) bFf().mo5608dq().mo3214p(bfP);
    }

    /* renamed from: b */
    private void m39076b(NPCType aed) {
        bFf().mo5608dq().mo3197f(bfK, aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Type")
    @C0064Am(aul = "62918d2a562695a245e05197fc083770", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m39077c(NPCType aed) {
        throw new aWi(new aCE(this, bfR, new Object[]{aed}));
    }

    /* renamed from: df */
    private void m39078df(float f) {
        bFf().mo5608dq().mo3150a(bfN, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timeout (seconds)")
    @C0064Am(aul = "8b022fa6e3c066a720609f4f41f1ccf5", aum = 0)
    @C5566aOg
    /* renamed from: dg */
    private void m39079dg(float f) {
        throw new aWi(new aCE(this, bfW, new Object[]{new Float(f)}));
    }

    /* renamed from: eC */
    private void m39080eC(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(bfL, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message")
    @C0064Am(aul = "1095496400b18e91a5f0c78a49a0223e", aum = 0)
    @C5566aOg
    /* renamed from: eD */
    private void m39081eD(I18NString i18NString) {
        throw new aWi(new aCE(this, bfT, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message")
    /* renamed from: KI */
    public I18NString mo22014KI() {
        switch (bFf().mo6893i(bfS)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, bfS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfS, new Object[0]));
                break;
        }
        return abt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C5566aOg
    /* renamed from: U */
    public void mo22015U(Asset tCVar) {
        switch (bFf().mo6893i(bfX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfX, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfX, new Object[]{tCVar}));
                break;
        }
        m39073T(tCVar);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1174RO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return abr();
            case 1:
                m39077c((NPCType) args[0]);
                return null;
            case 2:
                return abt();
            case 3:
                m39081eD((I18NString) args[0]);
                return null;
            case 4:
                return new Float(abu());
            case 5:
                return abw();
            case 6:
                m39079dg(((Float) args[0]).floatValue());
                return null;
            case 7:
                m39073T((Asset) args[0]);
                return null;
            case 8:
                m39075a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Type")
    public NPCType abs() {
        switch (bFf().mo6893i(bfQ)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, bfQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfQ, new Object[0]));
                break;
        }
        return abr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timeout (seconds)")
    public float abv() {
        switch (bFf().mo6893i(bfU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bfU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bfU, new Object[0]));
                break;
        }
        return abu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    public Asset abx() {
        switch (bFf().mo6893i(bfV)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, bfV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfV, new Object[0]));
                break;
        }
        return abw();
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f9147Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9147Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9147Dn, new Object[]{aiw, af}));
                break;
        }
        m39075a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NPC Type")
    @C5566aOg
    /* renamed from: d */
    public void mo22019d(NPCType aed) {
        switch (bFf().mo6893i(bfR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfR, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfR, new Object[]{aed}));
                break;
        }
        m39077c(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timeout (seconds)")
    @C5566aOg
    /* renamed from: dh */
    public void mo22020dh(float f) {
        switch (bFf().mo6893i(bfW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfW, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfW, new Object[]{new Float(f)}));
                break;
        }
        m39079dg(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message")
    @C5566aOg
    /* renamed from: eE */
    public void mo22021eE(I18NString i18NString) {
        switch (bFf().mo6893i(bfT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfT, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfT, new Object[]{i18NString}));
                break;
        }
        m39081eD(i18NString);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NPC Type")
    @C0064Am(aul = "a0d76b1085a9ca6742c48a9897de61ee", aum = 0)
    private NPCType abr() {
        return abn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message")
    @C0064Am(aul = "689f0887ca9a46db1b3699bb53498768", aum = 0)
    private I18NString abt() {
        return abo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timeout (seconds)")
    @C0064Am(aul = "7ffd4ec7d8842b562420f22917ce0c78", aum = 0)
    private float abu() {
        return abp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    @C0064Am(aul = "f4483f7794e2da118ef39b1d257f9e67", aum = 0)
    private Asset abw() {
        return abq();
    }

    @C0064Am(aul = "a5b690c81528d8d48e837568530d2a6c", aum = 0)
    /* renamed from: a */
    private void m39075a(C5426aIw aiw, Mission af) {
        af.mo64KY().mo14419f((C1506WA) new C5311aEl(abn(), abo(), (long) (abp() * 1000.0f), abq()));
    }
}
