package game.script.mission.actions;

import game.network.message.externalizable.C5543aNj;
import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.resource.Asset;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5732aUq;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.auL  reason: case insensitive filesystem */
/* compiled from: a */
public class RecurrentMessageRequestMissionAction extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f5371Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bca = null;
    public static final C5663aRz bcc = null;
    public static final C5663aRz bce = null;
    public static final C2491fm bcf = null;
    public static final C2491fm bcg = null;
    public static final C2491fm bch = null;
    public static final C2491fm bci = null;
    public static final C2491fm bcj = null;
    public static final C2491fm bck = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6362664fefc881edba8b2109a0c1f6a5", aum = 0)
    private static String bbZ;
    @C0064Am(aul = "8901df5093e5ec73af4830dc2d632d1c", aum = 1)
    private static I18NString bcb;
    @C0064Am(aul = "dfc214a6c5910883d65ac76014ffecb3", aum = 2)
    private static Asset bcd;

    static {
        m26241V();
    }

    public RecurrentMessageRequestMissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RecurrentMessageRequestMissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26241V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 3;
        _m_methodCount = MissionAction._m_methodCount + 7;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(RecurrentMessageRequestMissionAction.class, "6362664fefc881edba8b2109a0c1f6a5", i);
        bca = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(RecurrentMessageRequestMissionAction.class, "8901df5093e5ec73af4830dc2d632d1c", i2);
        bcc = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(RecurrentMessageRequestMissionAction.class, "dfc214a6c5910883d65ac76014ffecb3", i3);
        bce = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i5 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(RecurrentMessageRequestMissionAction.class, "21327d6574894630d1e56b10ab092f52", i5);
        bcf = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(RecurrentMessageRequestMissionAction.class, "f8c1f51eb41a04e52bef65eb8daa7b4c", i6);
        bcg = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(RecurrentMessageRequestMissionAction.class, "785060fdf6605e45702faac65b43cf83", i7);
        bch = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(RecurrentMessageRequestMissionAction.class, "61f271045da2ecf61886ab69b1ee355b", i8);
        bci = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(RecurrentMessageRequestMissionAction.class, "de916051cbf47ce82a764838537db27c", i9);
        bcj = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(RecurrentMessageRequestMissionAction.class, "4a2eab00929669a1c4c617595beacb96", i10);
        bck = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(RecurrentMessageRequestMissionAction.class, "90031578526bca1b7137356943146832", i11);
        f5371Dn = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RecurrentMessageRequestMissionAction.class, C5732aUq.class, _m_fields, _m_methods);
    }

    /* renamed from: P */
    private void m26239P(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bce, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Background Image")
    @C0064Am(aul = "4a2eab00929669a1c4c617595beacb96", aum = 0)
    @C5566aOg
    /* renamed from: Q */
    private void m26240Q(Asset tCVar) {
        throw new aWi(new aCE(this, bck, new Object[]{tCVar}));
    }

    /* renamed from: YL */
    private String m26242YL() {
        return (String) bFf().mo5608dq().mo3214p(bca);
    }

    /* renamed from: YM */
    private I18NString m26243YM() {
        return (I18NString) bFf().mo5608dq().mo3214p(bcc);
    }

    /* renamed from: YN */
    private Asset m26244YN() {
        return (Asset) bFf().mo5608dq().mo3214p(bce);
    }

    /* renamed from: bh */
    private void m26249bh(String str) {
        bFf().mo5608dq().mo3197f(bca, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C0064Am(aul = "f8c1f51eb41a04e52bef65eb8daa7b4c", aum = 0)
    @C5566aOg
    /* renamed from: bi */
    private void m26250bi(String str) {
        throw new aWi(new aCE(this, bcg, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Header text")
    @C0064Am(aul = "61f271045da2ecf61886ab69b1ee355b", aum = 0)
    @C5566aOg
    /* renamed from: eA */
    private void m26251eA(I18NString i18NString) {
        throw new aWi(new aCE(this, bci, new Object[]{i18NString}));
    }

    /* renamed from: ez */
    private void m26252ez(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(bcc, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Background Image")
    @C5566aOg
    /* renamed from: R */
    public void mo16328R(Asset tCVar) {
        switch (bFf().mo6893i(bck)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bck, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bck, new Object[]{tCVar}));
                break;
        }
        m26240Q(tCVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5732aUq(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    /* renamed from: YP */
    public String mo16329YP() {
        switch (bFf().mo6893i(bcf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bcf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcf, new Object[0]));
                break;
        }
        return m26245YO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Header text")
    /* renamed from: YR */
    public I18NString mo16330YR() {
        switch (bFf().mo6893i(bch)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, bch, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bch, new Object[0]));
                break;
        }
        return m26246YQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Background Image")
    /* renamed from: YT */
    public Asset mo16331YT() {
        switch (bFf().mo6893i(bcj)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, bcj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcj, new Object[0]));
                break;
        }
        return m26247YS();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return m26245YO();
            case 1:
                m26250bi((String) args[0]);
                return null;
            case 2:
                return m26246YQ();
            case 3:
                m26251eA((I18NString) args[0]);
                return null;
            case 4:
                return m26247YS();
            case 5:
                m26240Q((Asset) args[0]);
                return null;
            case 6:
                m26248a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f5371Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5371Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5371Dn, new Object[]{aiw, af}));
                break;
        }
        m26248a(aiw, af);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C5566aOg
    /* renamed from: bj */
    public void mo16332bj(String str) {
        switch (bFf().mo6893i(bcg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                break;
        }
        m26250bi(str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Header text")
    @C5566aOg
    /* renamed from: eB */
    public void mo16333eB(I18NString i18NString) {
        switch (bFf().mo6893i(bci)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bci, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bci, new Object[]{i18NString}));
                break;
        }
        m26251eA(i18NString);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    @C0064Am(aul = "21327d6574894630d1e56b10ab092f52", aum = 0)
    /* renamed from: YO */
    private String m26245YO() {
        return m26242YL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Header text")
    @C0064Am(aul = "785060fdf6605e45702faac65b43cf83", aum = 0)
    /* renamed from: YQ */
    private I18NString m26246YQ() {
        return m26243YM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Background Image")
    @C0064Am(aul = "de916051cbf47ce82a764838537db27c", aum = 0)
    /* renamed from: YS */
    private Asset m26247YS() {
        return m26244YN();
    }

    @C0064Am(aul = "90031578526bca1b7137356943146832", aum = 0)
    /* renamed from: a */
    private void m26248a(C5426aIw aiw, Mission af) {
        af.mo64KY().mo14419f((C1506WA) new C5543aNj(m26242YL(), m26243YM(), m26244YN()));
    }
}
