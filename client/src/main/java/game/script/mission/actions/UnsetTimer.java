package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3170og;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aoa  reason: case insensitive filesystem */
/* compiled from: a */
public class UnsetTimer extends MissionAction implements C1616Xf {
    /* renamed from: Dd */
    public static final C5663aRz f5062Dd = null;
    /* renamed from: Dh */
    public static final C2491fm f5063Dh = null;
    /* renamed from: Di */
    public static final C2491fm f5064Di = null;
    /* renamed from: Dn */
    public static final C2491fm f5065Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b290238313b634f57461a970b832814a", aum = 0)

    /* renamed from: Dc */
    private static String f5061Dc;

    static {
        m24561V();
    }

    public UnsetTimer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public UnsetTimer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24561V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 1;
        _m_methodCount = MissionAction._m_methodCount + 3;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(UnsetTimer.class, "b290238313b634f57461a970b832814a", i);
        f5062Dd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i3 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(UnsetTimer.class, "463cfcadaa324e648bbf02aa75d98b0f", i3);
        f5063Dh = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(UnsetTimer.class, "6f4b361aef8d92c6584cfc4797078706", i4);
        f5064Di = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(UnsetTimer.class, "1a1b940f7a5e86f2be63e3d80adeef77", i5);
        f5065Dn = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(UnsetTimer.class, C3170og.class, _m_fields, _m_methods);
    }

    /* renamed from: Y */
    private void m24562Y(String str) {
        bFf().mo5608dq().mo3197f(f5062Dd, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timer handle")
    @C0064Am(aul = "6f4b361aef8d92c6584cfc4797078706", aum = 0)
    @C5566aOg
    /* renamed from: Z */
    private void m24563Z(String str) {
        throw new aWi(new aCE(this, f5064Di, new Object[]{str}));
    }

    /* renamed from: lS */
    private String m24565lS() {
        return (String) bFf().mo5608dq().mo3214p(f5062Dd);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3170og(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return m24566lV();
            case 1:
                m24563Z((String) args[0]);
                return null;
            case 2:
                m24564a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timer handle")
    @C5566aOg
    /* renamed from: aa */
    public void mo15271aa(String str) {
        switch (bFf().mo6893i(f5064Di)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5064Di, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5064Di, new Object[]{str}));
                break;
        }
        m24563Z(str);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f5065Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5065Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5065Dn, new Object[]{aiw, af}));
                break;
        }
        m24564a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timer handle")
    /* renamed from: lW */
    public String mo15272lW() {
        switch (bFf().mo6893i(f5063Dh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5063Dh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5063Dh, new Object[0]));
                break;
        }
        return m24566lV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timer handle")
    @C0064Am(aul = "463cfcadaa324e648bbf02aa75d98b0f", aum = 0)
    /* renamed from: lV */
    private String m24566lV() {
        return m24565lS();
    }

    @C0064Am(aul = "1a1b940f7a5e86f2be63e3d80adeef77", aum = 0)
    /* renamed from: a */
    private void m24564a(C5426aIw aiw, Mission af) {
        af.mo127iX(m24565lS());
    }
}
