package game.script.mission.actions;

import game.network.message.externalizable.C3131oI;
import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6221ail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.SQ */
/* compiled from: a */
public class NurseryTutorialMissionAction extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f1539Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ega = null;
    public static final C2491fm egb = null;
    public static final C2491fm egc = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8ecf25430eb8218e4c4765c2810e8850", aum = 0)

    /* renamed from: dB */
    private static C3131oI.C3132a f1540dB;

    static {
        m9446V();
    }

    public NurseryTutorialMissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NurseryTutorialMissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9446V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 1;
        _m_methodCount = MissionAction._m_methodCount + 3;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(NurseryTutorialMissionAction.class, "8ecf25430eb8218e4c4765c2810e8850", i);
        ega = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i3 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(NurseryTutorialMissionAction.class, "b8fec584bf351f62a47886f36c9b084c", i3);
        egb = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(NurseryTutorialMissionAction.class, "3a7ba3e3eb1fccc2b14b037f34b94a31", i4);
        egc = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(NurseryTutorialMissionAction.class, "72b7ed681a429bd49ef5d5ebbc7a98f5", i5);
        f1539Dn = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NurseryTutorialMissionAction.class, C6221ail.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9448a(C3131oI.C3132a aVar) {
        bFf().mo5608dq().mo3197f(ega, aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutorial transition")
    @C0064Am(aul = "3a7ba3e3eb1fccc2b14b037f34b94a31", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m9449b(C3131oI.C3132a aVar) {
        throw new aWi(new aCE(this, egc, new Object[]{aVar}));
    }

    private C3131oI.C3132a buC() {
        return (C3131oI.C3132a) bFf().mo5608dq().mo3214p(ega);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutorial transition")
    /* renamed from: Us */
    public C3131oI.C3132a mo5392Us() {
        switch (bFf().mo6893i(egb)) {
            case 0:
                return null;
            case 2:
                return (C3131oI.C3132a) bFf().mo5606d(new aCE(this, egb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, egb, new Object[0]));
                break;
        }
        return buD();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6221ail(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return buD();
            case 1:
                m9449b((C3131oI.C3132a) args[0]);
                return null;
            case 2:
                m9447a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f1539Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1539Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1539Dn, new Object[]{aiw, af}));
                break;
        }
        m9447a(aiw, af);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutorial transition")
    @C5566aOg
    /* renamed from: c */
    public void mo5394c(C3131oI.C3132a aVar) {
        switch (bFf().mo6893i(egc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, egc, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, egc, new Object[]{aVar}));
                break;
        }
        m9449b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutorial transition")
    @C0064Am(aul = "b8fec584bf351f62a47886f36c9b084c", aum = 0)
    private C3131oI.C3132a buD() {
        return buC();
    }

    @C0064Am(aul = "72b7ed681a429bd49ef5d5ebbc7a98f5", aum = 0)
    /* renamed from: a */
    private void m9447a(C5426aIw aiw, Mission af) {
        af.mo64KY().mo14419f((C1506WA) new C3131oI(buC()));
    }
}
