package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import logic.baa.*;
import logic.data.mbean.C1320TK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.ey */
/* compiled from: a */
public class SetTimer extends MissionAction implements C1616Xf, aOW {
    /* renamed from: Dd */
    public static final C5663aRz f7104Dd = null;
    /* renamed from: De */
    public static final C5663aRz f7105De = null;
    /* renamed from: Dg */
    public static final C5663aRz f7107Dg = null;
    /* renamed from: Dh */
    public static final C2491fm f7108Dh = null;
    /* renamed from: Di */
    public static final C2491fm f7109Di = null;
    /* renamed from: Dj */
    public static final C2491fm f7110Dj = null;
    /* renamed from: Dk */
    public static final C2491fm f7111Dk = null;
    /* renamed from: Dl */
    public static final C2491fm f7112Dl = null;
    /* renamed from: Dm */
    public static final C2491fm f7113Dm = null;
    /* renamed from: Dn */
    public static final C2491fm f7114Dn = null;
    /* renamed from: Do */
    public static final C2491fm f7115Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d90ab2085568e2bb37d7b5dc27b9a753", aum = 0)

    /* renamed from: Dc */
    private static String f7103Dc;
    @C0064Am(aul = "b9ac7a3d62520e019ec8100bd3799ffc", aum = 2)

    /* renamed from: Df */
    private static boolean f7106Df;
    @C0064Am(aul = "763c1230ec9cbf26bb45aed42bfdaf30", aum = 1)
    private static long time;

    static {
        m30153V();
    }

    public SetTimer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SetTimer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m30153V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 3;
        _m_methodCount = MissionAction._m_methodCount + 8;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(SetTimer.class, "d90ab2085568e2bb37d7b5dc27b9a753", i);
        f7104Dd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SetTimer.class, "763c1230ec9cbf26bb45aed42bfdaf30", i2);
        f7105De = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SetTimer.class, "b9ac7a3d62520e019ec8100bd3799ffc", i3);
        f7107Dg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i5 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(SetTimer.class, "3840e3e68e09b26f09648c8a57ddbdb7", i5);
        f7108Dh = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(SetTimer.class, "3b9d392460c56f9f85e5398a13e65a89", i6);
        f7109Di = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(SetTimer.class, "d48e406a4a71e65d8933e1c00874190a", i7);
        f7110Dj = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(SetTimer.class, "854584a340dda62a7ff66ba6aaa1d806", i8);
        f7111Dk = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(SetTimer.class, "f3bf8143cabc61396cfc9d29fa424bb5", i9);
        f7112Dl = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(SetTimer.class, "08a7371283a65120f3b9c1fa91bd3b5c", i10);
        f7113Dm = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(SetTimer.class, "d030b5ab957f377ff1dd56d848a37b3d", i11);
        f7114Dn = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(SetTimer.class, "5e7548d4a9a1b1b77f80a79d3b6869ab", i12);
        f7115Do = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SetTimer.class, C1320TK.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m30151B(boolean z) {
        bFf().mo5608dq().mo3153a(f7107Dg, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent")
    @C0064Am(aul = "08a7371283a65120f3b9c1fa91bd3b5c", aum = 0)
    @C5566aOg
    /* renamed from: C */
    private void m30152C(boolean z) {
        throw new aWi(new aCE(this, f7113Dm, new Object[]{new Boolean(z)}));
    }

    /* renamed from: Y */
    private void m30154Y(String str) {
        bFf().mo5608dq().mo3197f(f7104Dd, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timer handle")
    @C0064Am(aul = "3b9d392460c56f9f85e5398a13e65a89", aum = 0)
    @C5566aOg
    /* renamed from: Z */
    private void m30155Z(String str) {
        throw new aWi(new aCE(this, f7109Di, new Object[]{str}));
    }

    /* renamed from: lS */
    private String m30158lS() {
        return (String) bFf().mo5608dq().mo3214p(f7104Dd);
    }

    /* renamed from: lT */
    private long m30159lT() {
        return bFf().mo5608dq().mo3213o(f7105De);
    }

    /* renamed from: lU */
    private boolean m30160lU() {
        return bFf().mo5608dq().mo3201h(f7107Dg);
    }

    /* renamed from: s */
    private void m30164s(long j) {
        bFf().mo5608dq().mo3184b(f7105De, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timeout (seconds)")
    @C0064Am(aul = "854584a340dda62a7ff66ba6aaa1d806", aum = 0)
    @C5566aOg
    /* renamed from: t */
    private void m30165t(long j) {
        throw new aWi(new aCE(this, f7111Dk, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent")
    @C5566aOg
    /* renamed from: D */
    public void mo18244D(boolean z) {
        switch (bFf().mo6893i(f7113Dm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7113Dm, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7113Dm, new Object[]{new Boolean(z)}));
                break;
        }
        m30152C(z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1320TK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return m30161lV();
            case 1:
                m30155Z((String) args[0]);
                return null;
            case 2:
                return new Long(m30162lX());
            case 3:
                m30165t(((Long) args[0]).longValue());
                return null;
            case 4:
                return new Boolean(m30163lY());
            case 5:
                m30152C(((Boolean) args[0]).booleanValue());
                return null;
            case 6:
                m30157a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            case 7:
                m30156a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timer handle")
    @C5566aOg
    /* renamed from: aa */
    public void mo18245aa(String str) {
        switch (bFf().mo6893i(f7109Di)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7109Di, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7109Di, new Object[]{str}));
                break;
        }
        m30155Z(str);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f7115Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7115Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7115Do, new Object[]{jt}));
                break;
        }
        m30156a(jt);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f7114Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7114Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7114Dn, new Object[]{aiw, af}));
                break;
        }
        m30157a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timeout (seconds)")
    public long getTime() {
        switch (bFf().mo6893i(f7110Dj)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7110Dj, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7110Dj, new Object[0]));
                break;
        }
        return m30162lX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Timeout (seconds)")
    @C5566aOg
    public void setTime(long j) {
        switch (bFf().mo6893i(f7111Dk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7111Dk, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7111Dk, new Object[]{new Long(j)}));
                break;
        }
        m30165t(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timer handle")
    /* renamed from: lW */
    public String mo18247lW() {
        switch (bFf().mo6893i(f7108Dh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7108Dh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7108Dh, new Object[0]));
                break;
        }
        return m30161lV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent")
    /* renamed from: lZ */
    public boolean mo18248lZ() {
        switch (bFf().mo6893i(f7112Dl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7112Dl, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7112Dl, new Object[0]));
                break;
        }
        return m30163lY();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timer handle")
    @C0064Am(aul = "3840e3e68e09b26f09648c8a57ddbdb7", aum = 0)
    /* renamed from: lV */
    private String m30161lV() {
        return m30158lS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Timeout (seconds)")
    @C0064Am(aul = "d48e406a4a71e65d8933e1c00874190a", aum = 0)
    /* renamed from: lX */
    private long m30162lX() {
        return m30159lT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent")
    @C0064Am(aul = "f3bf8143cabc61396cfc9d29fa424bb5", aum = 0)
    /* renamed from: lY */
    private boolean m30163lY() {
        return m30160lU();
    }

    @C0064Am(aul = "d030b5ab957f377ff1dd56d848a37b3d", aum = 0)
    /* renamed from: a */
    private void m30157a(C5426aIw aiw, Mission af) {
        af.mo101c(m30158lS(), m30159lT(), 1);
    }

    @C0064Am(aul = "5e7548d4a9a1b1b77f80a79d3b6869ab", aum = 0)
    /* renamed from: a */
    private void m30156a(C0665JT jt) {
        super.mo24b(jt);
        if (jt.mo3117j(1, 0, 0)) {
            m30151B(true);
        }
    }
}
