package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.mission.scripting.MissionScript;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.awZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aJv  reason: case insensitive filesystem */
/* compiled from: a */
public class ChangeObjectiveVisibilityMissionAction extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f3239Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bih = null;
    public static final C2491fm bin = null;
    public static final C2491fm bio = null;
    public static final C5663aRz gxh = null;
    public static final C2491fm gxi = null;
    public static final C2491fm gxj = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9982983947d9404731c82a84db036ad4", aum = 1)
    private static boolean big;
    @C0064Am(aul = "75477dd9792a1567c7ae4c92d377f839", aum = 0)
    private static String gaW;

    static {
        m15875V();
    }

    public ChangeObjectiveVisibilityMissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ChangeObjectiveVisibilityMissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15875V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 2;
        _m_methodCount = MissionAction._m_methodCount + 5;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ChangeObjectiveVisibilityMissionAction.class, "75477dd9792a1567c7ae4c92d377f839", i);
        gxh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ChangeObjectiveVisibilityMissionAction.class, "9982983947d9404731c82a84db036ad4", i2);
        bih = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i4 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(ChangeObjectiveVisibilityMissionAction.class, "5fd97db3fa34c077430d9e89a84678b8", i4);
        gxi = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ChangeObjectiveVisibilityMissionAction.class, "520ac98835671c7a0c0e34b6b5e0f98b", i5);
        gxj = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ChangeObjectiveVisibilityMissionAction.class, "63b400e0d02c830d98382a7d2f1f1a91", i6);
        bin = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ChangeObjectiveVisibilityMissionAction.class, "c35fab30ffccf51e769ec3091d9f23d5", i7);
        bio = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ChangeObjectiveVisibilityMissionAction.class, "da0da08bde12bb36fdd6ac22ca87921f", i8);
        f3239Dn = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ChangeObjectiveVisibilityMissionAction.class, awZ.class, _m_fields, _m_methods);
    }

    /* renamed from: aY */
    private void m15877aY(boolean z) {
        bFf().mo5608dq().mo3153a(bih, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hidden")
    @C0064Am(aul = "c35fab30ffccf51e769ec3091d9f23d5", aum = 0)
    @C5566aOg
    /* renamed from: aZ */
    private void m15878aZ(boolean z) {
        throw new aWi(new aCE(this, bio, new Object[]{new Boolean(z)}));
    }

    private boolean abL() {
        return bFf().mo5608dq().mo3201h(bih);
    }

    private String cuP() {
        return (String) bFf().mo5608dq().mo3214p(gxh);
    }

    /* renamed from: jR */
    private void m15879jR(String str) {
        bFf().mo5608dq().mo3197f(gxh, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Objective Handle")
    @C0064Am(aul = "520ac98835671c7a0c0e34b6b5e0f98b", aum = 0)
    @C5566aOg
    /* renamed from: jS */
    private void m15880jS(String str) {
        throw new aWi(new aCE(this, gxj, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new awZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return cuQ();
            case 1:
                m15880jS((String) args[0]);
                return null;
            case 2:
                return new Boolean(abS());
            case 3:
                m15878aZ(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                m15876a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f3239Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3239Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3239Dn, new Object[]{aiw, af}));
                break;
        }
        m15876a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Objective Handle")
    public String cuR() {
        switch (bFf().mo6893i(gxi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gxi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gxi, new Object[0]));
                break;
        }
        return cuQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hidden")
    public boolean isHidden() {
        switch (bFf().mo6893i(bin)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bin, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bin, new Object[0]));
                break;
        }
        return abS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hidden")
    @C5566aOg
    public void setHidden(boolean z) {
        switch (bFf().mo6893i(bio)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                break;
        }
        m15878aZ(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Objective Handle")
    @C5566aOg
    /* renamed from: jT */
    public void mo9634jT(String str) {
        switch (bFf().mo6893i(gxj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gxj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gxj, new Object[]{str}));
                break;
        }
        m15880jS(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Objective Handle")
    @C0064Am(aul = "5fd97db3fa34c077430d9e89a84678b8", aum = 0)
    private String cuQ() {
        return cuP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hidden")
    @C0064Am(aul = "63b400e0d02c830d98382a7d2f1f1a91", aum = 0)
    private boolean abS() {
        return abL();
    }

    @C0064Am(aul = "da0da08bde12bb36fdd6ac22ca87921f", aum = 0)
    /* renamed from: a */
    private void m15876a(C5426aIw aiw, Mission af) {
        if (af instanceof MissionScript) {
            MissionScript bc = (MissionScript) af;
            if (abL()) {
                bc.mo569dL(cuP());
            } else {
                bc.mo568dJ(cuP());
            }
        }
    }
}
