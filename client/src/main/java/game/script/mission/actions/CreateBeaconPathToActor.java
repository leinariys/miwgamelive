package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.mission.Mission;
import logic.baa.*;
import logic.data.mbean.awY;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.awh  reason: case insensitive filesystem */
/* compiled from: a */
public class CreateBeaconPathToActor extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f5522Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gLF = null;
    public static final C2491fm gLG = null;
    public static final C2491fm gLH = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b93b6c2601ca570202123ad7ba022dc7", aum = 0)
    private static C0468GU gLE;

    static {
        m26903V();
    }

    public CreateBeaconPathToActor() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CreateBeaconPathToActor(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26903V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 1;
        _m_methodCount = MissionAction._m_methodCount + 3;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(CreateBeaconPathToActor.class, "b93b6c2601ca570202123ad7ba022dc7", i);
        gLF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i3 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(CreateBeaconPathToActor.class, "fcda8239c0ba8a4aadab8661c3abd3e2", i3);
        gLG = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(CreateBeaconPathToActor.class, "7aba249fa699fed4ca023e19419e7581", i4);
        gLH = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(CreateBeaconPathToActor.class, "20f6a5ba2755be54219d36d5e408dcd7", i5);
        f5522Dn = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CreateBeaconPathToActor.class, awY.class, _m_fields, _m_methods);
    }

    /* renamed from: c */
    private void m26905c(C0468GU gu) {
        bFf().mo5608dq().mo3197f(gLF, gu);
    }

    private C0468GU cAN() {
        return (C0468GU) bFf().mo5608dq().mo3214p(gLF);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Actor")
    @C0064Am(aul = "7aba249fa699fed4ca023e19419e7581", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m26906d(C0468GU gu) {
        throw new aWi(new aCE(this, gLH, new Object[]{gu}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new awY(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return cAO();
            case 1:
                m26906d((C0468GU) args[0]);
                return null;
            case 2:
                m26904a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f5522Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5522Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5522Dn, new Object[]{aiw, af}));
                break;
        }
        m26904a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Actor")
    public C0468GU cAP() {
        switch (bFf().mo6893i(gLG)) {
            case 0:
                return null;
            case 2:
                return (C0468GU) bFf().mo5606d(new aCE(this, gLG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gLG, new Object[0]));
                break;
        }
        return cAO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destination Actor")
    @C5566aOg
    /* renamed from: e */
    public void mo16711e(C0468GU gu) {
        switch (bFf().mo6893i(gLH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gLH, new Object[]{gu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gLH, new Object[]{gu}));
                break;
        }
        m26906d(gu);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destination Actor")
    @C0064Am(aul = "fcda8239c0ba8a4aadab8661c3abd3e2", aum = 0)
    private C0468GU cAO() {
        return cAN();
    }

    @C0064Am(aul = "20f6a5ba2755be54219d36d5e408dcd7", aum = 0)
    /* renamed from: a */
    private void m26904a(C5426aIw aiw, Mission af) {
        if (cAN() != null) {
            af.mo71aT((Actor) cAN());
            af.cdg();
        }
    }
}
