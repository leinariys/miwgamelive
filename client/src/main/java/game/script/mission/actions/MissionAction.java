package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.mission.Mission;
import logic.baa.*;
import logic.data.mbean.C2503fv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.cn */
/* compiled from: a */
public abstract class MissionAction extends TaikodomObject implements C0468GU, C1616Xf, aOW {

    /* renamed from: Dn */
    public static final C2491fm f6220Dn = null;

    /* renamed from: Do */
    public static final C2491fm f6221Do = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f6225bL = null;
    /* renamed from: bM */
    public static final C5663aRz f6226bM = null;
    /* renamed from: bN */
    public static final C2491fm f6227bN = null;
    /* renamed from: bO */
    public static final C2491fm f6228bO = null;
    /* renamed from: bP */
    public static final C2491fm f6229bP = null;
    /* renamed from: bQ */
    public static final C2491fm f6230bQ = null;
    public static final C2491fm bYW = null;
    public static final C5663aRz cDe = null;
    public static final C5663aRz cDf = null;
    public static final C2491fm cDg = null;
    public static final C2491fm cDh = null;
    public static final C2491fm cDi = null;
    public static final C2491fm cDj = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "98f6a727fc8e429cb519219692e436f7", aum = 0)

    /* renamed from: Kt */
    private static C2210a f6222Kt;
    @C0064Am(aul = "2ae96bac2035c4b17e0066f40c0b3b9d", aum = 2)

    /* renamed from: Ku */
    private static boolean f6223Ku;
    @C0064Am(aul = "f7bb964d95fdee7f4b7f0c6f4aff4656", aum = 3)

    /* renamed from: bK */
    private static UUID f6224bK;
    @C0064Am(aul = "7f838fe1cde12da4ce5f11416cb09761", aum = 1)
    private static String handle;

    static {
        m28600V();
    }

    public MissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28600V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(MissionAction.class, "98f6a727fc8e429cb519219692e436f7", i);
        cDe = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionAction.class, "7f838fe1cde12da4ce5f11416cb09761", i2);
        f6226bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionAction.class, "2ae96bac2035c4b17e0066f40c0b3b9d", i3);
        cDf = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionAction.class, "f7bb964d95fdee7f4b7f0c6f4aff4656", i4);
        f6225bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 12)];
        C2491fm a = C4105zY.m41624a(MissionAction.class, "82707caa1456a607af614d7ee7b68505", i6);
        f6227bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionAction.class, "2ca6ec075a57532b504bbc60a7341143", i7);
        f6228bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionAction.class, "747971f6a52b9ab6960a95050b982492", i8);
        cDg = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionAction.class, "9f2256a62910c9b711a52fadfb0b76d4", i9);
        cDh = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionAction.class, "4bb08dc28ff4595e6964fa31536a298f", i10);
        f6229bP = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionAction.class, "98cf29f5400d1d8245ff49e371451261", i11);
        f6230bQ = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionAction.class, "5b7e33644c125632bad3cbdebd7119d6", i12);
        cDi = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionAction.class, "25c34477462ff3c1c8d2e0689b236083", i13);
        cDj = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionAction.class, "4e56d3f39feb9620b3658678e26dc791", i14);
        bYW = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionAction.class, "65ae7517a7c60544cd4d3bebc7abae4e", i15);
        f6220Dn = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionAction.class, "17a72c948e2f2fa90fa0af9667c7a653", i16);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(MissionAction.class, "ac730a41486bb1f034676859a3266cae", i17);
        f6221Do = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionAction.class, C2503fv.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "65ae7517a7c60544cd4d3bebc7abae4e", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m28602a(C5426aIw aiw, Mission af) {
        throw new aWi(new aCE(this, f6220Dn, new Object[]{aiw, af}));
    }

    /* renamed from: a */
    private void m28603a(C2210a aVar) {
        bFf().mo5608dq().mo3197f(cDe, aVar);
    }

    /* renamed from: a */
    private void m28604a(String str) {
        bFf().mo5608dq().mo3197f(f6226bM, str);
    }

    /* renamed from: a */
    private void m28605a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f6225bL, uuid);
    }

    @C0064Am(aul = "4e56d3f39feb9620b3658678e26dc791", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m28606a(I18NString i18NString, Object[] objArr) {
        throw new aWi(new aCE(this, bYW, new Object[]{i18NString, objArr}));
    }

    private C2210a aDL() {
        return (C2210a) bFf().mo5608dq().mo3214p(cDe);
    }

    private boolean aDM() {
        return bFf().mo5608dq().mo3201h(cDf);
    }

    /* renamed from: an */
    private UUID m28607an() {
        return (UUID) bFf().mo5608dq().mo3214p(f6225bL);
    }

    /* renamed from: ao */
    private String m28608ao() {
        return (String) bFf().mo5608dq().mo3214p(f6226bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Event")
    @C0064Am(aul = "9f2256a62910c9b711a52fadfb0b76d4", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m28612b(C2210a aVar) {
        throw new aWi(new aCE(this, cDh, new Object[]{aVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Event Handle")
    @C0064Am(aul = "98cf29f5400d1d8245ff49e371451261", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m28613b(String str) {
        throw new aWi(new aCE(this, f6230bQ, new Object[]{str}));
    }

    /* renamed from: bT */
    private void m28615bT(boolean z) {
        bFf().mo5608dq().mo3153a(cDf, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "One Time Only")
    @C0064Am(aul = "25c34477462ff3c1c8d2e0689b236083", aum = 0)
    @C5566aOg
    /* renamed from: bU */
    private void m28616bU(boolean z) {
        throw new aWi(new aCE(this, cDj, new Object[]{new Boolean(z)}));
    }

    /* renamed from: c */
    private void m28617c(UUID uuid) {
        switch (bFf().mo6893i(f6228bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6228bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6228bO, new Object[]{uuid}));
                break;
        }
        m28614b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2503fv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m28609ap();
            case 1:
                m28614b((UUID) args[0]);
                return null;
            case 2:
                return aDN();
            case 3:
                m28612b((C2210a) args[0]);
                return null;
            case 4:
                return m28610ar();
            case 5:
                m28613b((String) args[0]);
                return null;
            case 6:
                return new Boolean(aDP());
            case 7:
                m28616bU(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                m28606a((I18NString) args[0], (Object[]) args[1]);
                return null;
            case 9:
                m28602a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            case 10:
                return m28611au();
            case 11:
                m28601a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Event")
    public C2210a aDO() {
        switch (bFf().mo6893i(cDg)) {
            case 0:
                return null;
            case 2:
                return (C2210a) bFf().mo5606d(new aCE(this, cDg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cDg, new Object[0]));
                break;
        }
        return aDN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "One Time Only")
    public boolean aDQ() {
        switch (bFf().mo6893i(cDi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cDi, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cDi, new Object[0]));
                break;
        }
        return aDP();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f6227bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f6227bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6227bN, new Object[0]));
                break;
        }
        return m28609ap();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f6221Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6221Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6221Do, new Object[]{jt}));
                break;
        }
        m28601a(jt);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f6220Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6220Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6220Dn, new Object[]{aiw, af}));
                break;
        }
        m28602a(aiw, af);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final void mo17682b(I18NString i18NString, Object... objArr) {
        switch (bFf().mo6893i(bYW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYW, new Object[]{i18NString, objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYW, new Object[]{i18NString, objArr}));
                break;
        }
        m28606a(i18NString, objArr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "One Time Only")
    @C5566aOg
    /* renamed from: bV */
    public void mo17683bV(boolean z) {
        switch (bFf().mo6893i(cDj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cDj, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cDj, new Object[]{new Boolean(z)}));
                break;
        }
        m28616bU(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Event")
    @C5566aOg
    /* renamed from: c */
    public void mo17684c(C2210a aVar) {
        switch (bFf().mo6893i(cDh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cDh, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cDh, new Object[]{aVar}));
                break;
        }
        m28612b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Event Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f6229bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f6229bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6229bP, new Object[0]));
                break;
        }
        return m28610ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Event Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f6230bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6230bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6230bQ, new Object[]{str}));
                break;
        }
        m28613b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m28611au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "82707caa1456a607af614d7ee7b68505", aum = 0)
    /* renamed from: ap */
    private UUID m28609ap() {
        return m28607an();
    }

    @C0064Am(aul = "2ca6ec075a57532b504bbc60a7341143", aum = 0)
    /* renamed from: b */
    private void m28614b(UUID uuid) {
        m28605a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Event")
    @C0064Am(aul = "747971f6a52b9ab6960a95050b982492", aum = 0)
    private C2210a aDN() {
        return aDL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Event Handle")
    @C0064Am(aul = "4bb08dc28ff4595e6964fa31536a298f", aum = 0)
    /* renamed from: ar */
    private String m28610ar() {
        return m28608ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "One Time Only")
    @C0064Am(aul = "5b7e33644c125632bad3cbdebd7119d6", aum = 0)
    private boolean aDP() {
        return aDM();
    }

    @C0064Am(aul = "17a72c948e2f2fa90fa0af9667c7a653", aum = 0)
    /* renamed from: au */
    private String m28611au() {
        String str = null;
        if (aDL() != null) {
            str = aDL().toString();
        }
        return "MissionAction: [" + str + "]";
    }

    @C0064Am(aul = "ac730a41486bb1f034676859a3266cae", aum = 0)
    /* renamed from: a */
    private void m28601a(C0665JT jt) {
        if (jt.mo3117j(1, 0, 0) && aDL() == C2210a.TIMER) {
            m28615bT(false);
        }
    }

    /* renamed from: a.cn$a */
    public enum C2210a {
        ACCEPT,
        ABORT,
        ACCOMPLISH,
        NPC_CHAT,
        MINE,
        LOOT,
        WAYPOINT_REACHED,
        UNDOCK,
        NPC_KILLED,
        FAIL,
        PLAYER_DIED,
        TIMER,
        STATE_CHANGE,
        WAYPOINT_SELECTED
    }
}
