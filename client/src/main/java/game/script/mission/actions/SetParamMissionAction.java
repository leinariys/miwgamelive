package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aBP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aPc  reason: case insensitive filesystem */
/* compiled from: a */
public class SetParamMissionAction extends MissionAction implements C1616Xf {

    /* renamed from: CR */
    public static final C5663aRz f3534CR = null;

    /* renamed from: CS */
    public static final C5663aRz f3535CS = null;

    /* renamed from: CU */
    public static final C2491fm f3536CU = null;

    /* renamed from: CV */
    public static final C2491fm f3537CV = null;

    /* renamed from: CW */
    public static final C2491fm f3538CW = null;

    /* renamed from: CX */
    public static final C2491fm f3539CX = null;

    /* renamed from: Dn */
    public static final C2491fm f3540Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ba00033597e6b36de8a4f3e1bef48a45", aum = 0)
    private static String key;
    @C0064Am(aul = "c4e1a4f0a7158f7af683583d691a02ec", aum = 1)
    private static String value;

    static {
        m17275V();
    }

    public SetParamMissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SetParamMissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17275V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 2;
        _m_methodCount = MissionAction._m_methodCount + 5;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(SetParamMissionAction.class, "ba00033597e6b36de8a4f3e1bef48a45", i);
        f3534CR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SetParamMissionAction.class, "c4e1a4f0a7158f7af683583d691a02ec", i2);
        f3535CS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i4 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(SetParamMissionAction.class, "aa8fb384fb13202f3ff1f34f3e0a7151", i4);
        f3540Dn = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(SetParamMissionAction.class, "4141d369299ca7c28a391c0756acf763", i5);
        f3536CU = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(SetParamMissionAction.class, "0717ead9cd17d4af875f528afdb87a9e", i6);
        f3537CV = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(SetParamMissionAction.class, "7af9d301b3469cf71d086f62435dedee", i7);
        f3538CW = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(SetParamMissionAction.class, "2926ff998b82d8bd78bd99111d0995cb", i8);
        f3539CX = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SetParamMissionAction.class, aBP.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m17274U(String str) {
        bFf().mo5608dq().mo3197f(f3534CR, str);
    }

    /* renamed from: V */
    private void m17276V(String str) {
        bFf().mo5608dq().mo3197f(f3535CS, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C0064Am(aul = "0717ead9cd17d4af875f528afdb87a9e", aum = 0)
    @C5566aOg
    /* renamed from: W */
    private void m17277W(String str) {
        throw new aWi(new aCE(this, f3537CV, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C0064Am(aul = "2926ff998b82d8bd78bd99111d0995cb", aum = 0)
    @C5566aOg
    /* renamed from: X */
    private void m17278X(String str) {
        throw new aWi(new aCE(this, f3539CX, new Object[]{str}));
    }

    /* renamed from: lM */
    private String m17280lM() {
        return (String) bFf().mo5608dq().mo3214p(f3534CR);
    }

    /* renamed from: lN */
    private String m17281lN() {
        return (String) bFf().mo5608dq().mo3214p(f3535CS);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aBP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                m17279a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            case 1:
                return m17282lO();
            case 2:
                m17277W((String) args[0]);
                return null;
            case 3:
                return m17283lP();
            case 4:
                m17278X((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f3540Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3540Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3540Dn, new Object[]{aiw, af}));
                break;
        }
        m17279a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    public String getKey() {
        switch (bFf().mo6893i(f3536CU)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3536CU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3536CU, new Object[0]));
                break;
        }
        return m17282lO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Key")
    @C5566aOg
    public void setKey(String str) {
        switch (bFf().mo6893i(f3537CV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3537CV, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3537CV, new Object[]{str}));
                break;
        }
        m17277W(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    public String getValue() {
        switch (bFf().mo6893i(f3538CW)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3538CW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3538CW, new Object[0]));
                break;
        }
        return m17283lP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C5566aOg
    public void setValue(String str) {
        switch (bFf().mo6893i(f3539CX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3539CX, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3539CX, new Object[]{str}));
                break;
        }
        m17278X(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "aa8fb384fb13202f3ff1f34f3e0a7151", aum = 0)
    /* renamed from: a */
    private void m17279a(C5426aIw aiw, Mission af) {
        aiw.setParameter(m17280lM(), m17281lN());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Key")
    @C0064Am(aul = "4141d369299ca7c28a391c0756acf763", aum = 0)
    /* renamed from: lO */
    private String m17282lO() {
        return m17280lM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    @C0064Am(aul = "7af9d301b3469cf71d086f62435dedee", aum = 0)
    /* renamed from: lP */
    private String m17283lP() {
        return m17281lN();
    }
}
