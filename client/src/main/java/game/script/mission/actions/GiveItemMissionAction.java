package game.script.mission.actions;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0040AX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.oi */
/* compiled from: a */
public class GiveItemMissionAction extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f8781Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMX = null;
    public static final C5663aRz aMZ = null;
    public static final C5663aRz aNb = null;
    public static final C2491fm aNc = null;
    public static final C2491fm aNd = null;
    public static final C2491fm aNe = null;
    public static final C2491fm aNf = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "296cf3495062c80b8f94c2efdc6bd805", aum = 0)
    private static ItemType aMW;
    @C0064Am(aul = "494708f3c6e4242596d3aa0e05e3fd5e", aum = 1)
    private static int aMY;
    @C0064Am(aul = "2433fdf45b28b759b97841246799637f", aum = 2)
    private static boolean aNa;

    static {
        m36792V();
    }

    public GiveItemMissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GiveItemMissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36792V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 3;
        _m_methodCount = MissionAction._m_methodCount + 5;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(GiveItemMissionAction.class, "296cf3495062c80b8f94c2efdc6bd805", i);
        aMX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(GiveItemMissionAction.class, "494708f3c6e4242596d3aa0e05e3fd5e", i2);
        aMZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(GiveItemMissionAction.class, "2433fdf45b28b759b97841246799637f", i3);
        aNb = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i5 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 5)];
        C2491fm a = C4105zY.m41624a(GiveItemMissionAction.class, "95a70a031a356e6f531adacdfe9db760", i5);
        aNc = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(GiveItemMissionAction.class, "56fa1ffce12e92f3787a244e48fa6a35", i6);
        aNd = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(GiveItemMissionAction.class, "f42977552cf6f83d3eccfc6a4192ca80", i7);
        aNe = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(GiveItemMissionAction.class, "fd9ff1735cbcfd5b6cb574ccdf03d41b", i8);
        aNf = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(GiveItemMissionAction.class, "0d37b55d35b72f092a861d0e9c2dbf2d", i9);
        f8781Dn = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GiveItemMissionAction.class, C0040AX.class, _m_fields, _m_methods);
    }

    /* renamed from: Sr */
    private ItemType m36787Sr() {
        return (ItemType) bFf().mo5608dq().mo3214p(aMX);
    }

    /* renamed from: Ss */
    private int m36788Ss() {
        return bFf().mo5608dq().mo3212n(aMZ);
    }

    /* renamed from: St */
    private boolean m36789St() {
        return bFf().mo5608dq().mo3201h(aNb);
    }

    /* renamed from: aK */
    private void m36794aK(boolean z) {
        bFf().mo5608dq().mo3153a(aNb, z);
    }

    /* renamed from: ds */
    private void m36795ds(int i) {
        bFf().mo5608dq().mo3183b(aMZ, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Quantity")
    @C0064Am(aul = "fd9ff1735cbcfd5b6cb574ccdf03d41b", aum = 0)
    @C5566aOg
    /* renamed from: dt */
    private void m36796dt(int i) {
        throw new aWi(new aCE(this, aNf, new Object[]{new Integer(i)}));
    }

    /* renamed from: k */
    private void m36797k(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(aMX, jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Item Type")
    @C0064Am(aul = "56fa1ffce12e92f3787a244e48fa6a35", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m36798l(ItemType jCVar) {
        throw new aWi(new aCE(this, aNd, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Item Type")
    /* renamed from: Sv */
    public ItemType mo21027Sv() {
        switch (bFf().mo6893i(aNc)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, aNc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aNc, new Object[0]));
                break;
        }
        return m36790Su();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Quantity")
    /* renamed from: Sx */
    public int mo21028Sx() {
        switch (bFf().mo6893i(aNe)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aNe, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aNe, new Object[0]));
                break;
        }
        return m36791Sw();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0040AX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return m36790Su();
            case 1:
                m36798l((ItemType) args[0]);
                return null;
            case 2:
                return new Integer(m36791Sw());
            case 3:
                m36796dt(((Integer) args[0]).intValue());
                return null;
            case 4:
                m36793a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f8781Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8781Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8781Dn, new Object[]{aiw, af}));
                break;
        }
        m36793a(aiw, af);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Quantity")
    @C5566aOg
    /* renamed from: du */
    public void mo21029du(int i) {
        switch (bFf().mo6893i(aNf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aNf, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aNf, new Object[]{new Integer(i)}));
                break;
        }
        m36796dt(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Receive Item Type")
    @C5566aOg
    /* renamed from: m */
    public void mo21030m(ItemType jCVar) {
        switch (bFf().mo6893i(aNd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aNd, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aNd, new Object[]{jCVar}));
                break;
        }
        m36798l(jCVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m36795ds(1);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Item Type")
    @C0064Am(aul = "95a70a031a356e6f531adacdfe9db760", aum = 0)
    /* renamed from: Su */
    private ItemType m36790Su() {
        return m36787Sr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Receive Quantity")
    @C0064Am(aul = "f42977552cf6f83d3eccfc6a4192ca80", aum = 0)
    /* renamed from: Sw */
    private int m36791Sw() {
        return m36788Ss();
    }

    @C0064Am(aul = "0d37b55d35b72f092a861d0e9c2dbf2d", aum = 0)
    /* renamed from: a */
    private void m36793a(C5426aIw aiw, Mission af) {
        if (!aiw.mo9406b(m36787Sr(), m36788Ss(), af)) {
            throw new C3944xC("Error giving item", C3944xC.C3945a.GIVE_NO_ROOM);
        }
        mo17682b(((NLSSpeechActions) ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEA(), m36787Sr().mo19891ke());
    }
}
