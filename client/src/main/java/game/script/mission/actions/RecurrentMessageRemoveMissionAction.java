package game.script.mission.actions;

import game.network.message.externalizable.C6559apL;
import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aOP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.sL */
/* compiled from: a */
public class RecurrentMessageRemoveMissionAction extends MissionAction implements C1616Xf {

    /* renamed from: Dn */
    public static final C2491fm f9099Dn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bca = null;
    public static final C2491fm bcf = null;
    public static final C2491fm bcg = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "879edad908e5e5593057df7b816305b5", aum = 0)
    private static String bbZ;

    static {
        m38712V();
    }

    public RecurrentMessageRemoveMissionAction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RecurrentMessageRemoveMissionAction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38712V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionAction._m_fieldCount + 1;
        _m_methodCount = MissionAction._m_methodCount + 3;
        int i = MissionAction._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(RecurrentMessageRemoveMissionAction.class, "879edad908e5e5593057df7b816305b5", i);
        bca = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_fields, (Object[]) _m_fields);
        int i3 = MissionAction._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(RecurrentMessageRemoveMissionAction.class, "64e21166d86d9cebe0e77dcd6f53ca89", i3);
        bcf = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(RecurrentMessageRemoveMissionAction.class, "24264e48e00558c08d60e1cde0212b64", i4);
        bcg = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(RecurrentMessageRemoveMissionAction.class, "8fe3156fcb187e0f5ab39bf25df868ef", i5);
        f9099Dn = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionAction._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RecurrentMessageRemoveMissionAction.class, aOP.class, _m_fields, _m_methods);
    }

    /* renamed from: YL */
    private String m38713YL() {
        return (String) bFf().mo5608dq().mo3214p(bca);
    }

    /* renamed from: bh */
    private void m38716bh(String str) {
        bFf().mo5608dq().mo3197f(bca, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C0064Am(aul = "24264e48e00558c08d60e1cde0212b64", aum = 0)
    @C5566aOg
    /* renamed from: bi */
    private void m38717bi(String str) {
        throw new aWi(new aCE(this, bcg, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aOP(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    /* renamed from: YP */
    public String mo21875YP() {
        switch (bFf().mo6893i(bcf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bcf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcf, new Object[0]));
                break;
        }
        return m38714YO();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionAction._m_methodCount) {
            case 0:
                return m38714YO();
            case 1:
                m38717bi((String) args[0]);
                return null;
            case 2:
                m38715a((C5426aIw) args[0], (Mission) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo5393b(C5426aIw aiw, Mission af) {
        switch (bFf().mo6893i(f9099Dn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9099Dn, new Object[]{aiw, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9099Dn, new Object[]{aiw, af}));
                break;
        }
        m38715a(aiw, af);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Message Handle")
    @C5566aOg
    /* renamed from: bj */
    public void mo21876bj(String str) {
        switch (bFf().mo6893i(bcg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcg, new Object[]{str}));
                break;
        }
        m38717bi(str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Message Handle")
    @C0064Am(aul = "64e21166d86d9cebe0e77dcd6f53ca89", aum = 0)
    /* renamed from: YO */
    private String m38714YO() {
        return m38713YL();
    }

    @C0064Am(aul = "8fe3156fcb187e0f5ab39bf25df868ef", aum = 0)
    /* renamed from: a */
    private void m38715a(C5426aIw aiw, Mission af) {
        af.mo64KY().mo14419f((C1506WA) new C6559apL(m38713YL()));
    }
}
