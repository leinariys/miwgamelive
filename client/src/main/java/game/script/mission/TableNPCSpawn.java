package game.script.mission;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C0087BA;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C3437rZ(aak = "", aal = "taikodom.game.script.conflictZone.TableNPCSpawn")
@C5511aMd
@C6485anp
/* renamed from: a.aoJ  reason: case insensitive filesystem */
/* compiled from: a */
public class TableNPCSpawn extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4992bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4993bM = null;
    /* renamed from: bN */
    public static final C2491fm f4994bN = null;
    /* renamed from: bO */
    public static final C2491fm f4995bO = null;
    /* renamed from: bP */
    public static final C2491fm f4996bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4997bQ = null;
    public static final C5663aRz dEJ = null;
    public static final C2491fm ejT = null;
    public static final C2491fm ejU = null;
    public static final C2491fm gjg = null;
    public static final C2491fm gjh = null;
    public static final C2491fm gji = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "982d9fe48a329decfdd168520aecbfb2", aum = 1)

    /* renamed from: bK */
    private static UUID f4991bK;
    @C0064Am(aul = "6bdafa26e69344e81c34866fc0db25f2", aum = 0)
    private static C3438ra<NPCSpawn> cng;
    @C0064Am(aul = "81045c8d1ec4a7ee1ada3ba4981eb10e", aum = 2)
    private static String handle;

    static {
        m24380V();
    }

    public TableNPCSpawn() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TableNPCSpawn(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24380V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(TableNPCSpawn.class, "6bdafa26e69344e81c34866fc0db25f2", i);
        dEJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TableNPCSpawn.class, "982d9fe48a329decfdd168520aecbfb2", i2);
        f4992bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TableNPCSpawn.class, "81045c8d1ec4a7ee1ada3ba4981eb10e", i3);
        f4993bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 10)];
        C2491fm a = C4105zY.m41624a(TableNPCSpawn.class, "0c9e0c3c8e494cd2a805f736270c99ab", i5);
        f4994bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(TableNPCSpawn.class, "06ce8c385b1e6bbe18266080b033a60e", i6);
        f4995bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(TableNPCSpawn.class, "76065517589b9d750095a19e627d180b", i7);
        f4996bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(TableNPCSpawn.class, "972f6910b5be29f9be48778ced56b213", i8);
        f4997bQ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(TableNPCSpawn.class, "56a5e1f219fc7c24ba79633add152a3b", i9);
        gjg = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(TableNPCSpawn.class, "1718dd010a5c06f54d7ba5e5cb6f426a", i10);
        gjh = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(TableNPCSpawn.class, "38bbd9a95a2fe7e4663c876c2cb52f0b", i11);
        gji = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(TableNPCSpawn.class, "85e0c107df52a307058219cbebe22bc3", i12);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(TableNPCSpawn.class, "8867cd8913a8dc39d923458eef3d4ebb", i13);
        ejT = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(TableNPCSpawn.class, "2219c925aaadc438075300dc39e4cb33", i14);
        ejU = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TableNPCSpawn.class, C0087BA.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCs")
    @C0064Am(aul = "56a5e1f219fc7c24ba79633add152a3b", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m24381a(NPCSpawn ju) {
        throw new aWi(new aCE(this, gjg, new Object[]{ju}));
    }

    /* renamed from: a */
    private void m24382a(String str) {
        bFf().mo5608dq().mo3197f(f4993bM, str);
    }

    /* renamed from: a */
    private void m24383a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4992bL, uuid);
    }

    /* renamed from: an */
    private UUID m24384an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4992bL);
    }

    /* renamed from: ao */
    private String m24385ao() {
        return (String) bFf().mo5608dq().mo3214p(f4993bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "972f6910b5be29f9be48778ced56b213", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m24389b(String str) {
        throw new aWi(new aCE(this, f4997bQ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCs")
    @C0064Am(aul = "1718dd010a5c06f54d7ba5e5cb6f426a", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m24391c(NPCSpawn ju) {
        throw new aWi(new aCE(this, gjh, new Object[]{ju}));
    }

    /* renamed from: c */
    private void m24392c(UUID uuid) {
        switch (bFf().mo6893i(f4995bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4995bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4995bO, new Object[]{uuid}));
                break;
        }
        m24390b(uuid);
    }

    /* renamed from: ce */
    private void m24393ce(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dEJ, raVar);
    }

    private C3438ra cog() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dEJ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0087BA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m24386ap();
            case 1:
                m24390b((UUID) args[0]);
                return null;
            case 2:
                return m24387ar();
            case 3:
                m24389b((String) args[0]);
                return null;
            case 4:
                m24381a((NPCSpawn) args[0]);
                return null;
            case 5:
                m24391c((NPCSpawn) args[0]);
                return null;
            case 6:
                return coh();
            case 7:
                return m24388au();
            case 8:
                return bvz();
            case 9:
                return bvB();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4994bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4994bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4994bN, new Object[0]));
                break;
        }
        return m24386ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCs")
    @C5566aOg
    /* renamed from: b */
    public void mo15168b(NPCSpawn ju) {
        switch (bFf().mo6893i(gjg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gjg, new Object[]{ju}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gjg, new Object[]{ju}));
                break;
        }
        m24381a(ju);
    }

    public NPCType bvA() {
        switch (bFf().mo6893i(ejT)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, ejT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejT, new Object[0]));
                break;
        }
        return bvz();
    }

    public List<NPCType> bvC() {
        switch (bFf().mo6893i(ejU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, ejU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ejU, new Object[0]));
                break;
        }
        return bvB();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCs")
    public List<NPCSpawn> coi() {
        switch (bFf().mo6893i(gji)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gji, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gji, new Object[0]));
                break;
        }
        return coh();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCs")
    @C5566aOg
    /* renamed from: d */
    public void mo15172d(NPCSpawn ju) {
        switch (bFf().mo6893i(gjh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gjh, new Object[]{ju}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gjh, new Object[]{ju}));
                break;
        }
        m24391c(ju);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4996bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4996bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4996bP, new Object[0]));
                break;
        }
        return m24387ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4997bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4997bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4997bQ, new Object[]{str}));
                break;
        }
        m24389b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m24388au();
    }

    @C0064Am(aul = "0c9e0c3c8e494cd2a805f736270c99ab", aum = 0)
    /* renamed from: ap */
    private UUID m24386ap() {
        return m24384an();
    }

    @C0064Am(aul = "06ce8c385b1e6bbe18266080b033a60e", aum = 0)
    /* renamed from: b */
    private void m24390b(UUID uuid) {
        m24383a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m24383a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "76065517589b9d750095a19e627d180b", aum = 0)
    /* renamed from: ar */
    private String m24387ar() {
        return m24385ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCs")
    @C0064Am(aul = "38bbd9a95a2fe7e4663c876c2cb52f0b", aum = 0)
    private List<NPCSpawn> coh() {
        return Collections.unmodifiableList(cog());
    }

    @C0064Am(aul = "85e0c107df52a307058219cbebe22bc3", aum = 0)
    /* renamed from: au */
    private String m24388au() {
        return "TableNPCSpawn: " + cog().size();
    }

    @C0064Am(aul = "8867cd8913a8dc39d923458eef3d4ebb", aum = 0)
    private NPCType bvz() {
        int nextInt = new Random().nextInt(100);
        int i = 0;
        Iterator it = cog().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return null;
            }
            NPCSpawn ju = (NPCSpawn) it.next();
            if (nextInt >= i2 && nextInt < ju.baB() + i2) {
                return ju.baz();
            }
            i = ju.baB() + i2;
        }
    }

    @C0064Am(aul = "2219c925aaadc438075300dc39e4cb33", aum = 0)
    private List<NPCType> bvB() {
        ArrayList arrayList = new ArrayList();
        Random random = new Random();
        for (NPCSpawn ju : cog()) {
            if (random.nextInt(100) < ju.baB()) {
                arrayList.add(ju.baz());
            }
        }
        return arrayList;
    }
}
