package game.script.mission;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.Actor;
import game.script.Character;
import game.script.TaikodomObject;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.*;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.link.C3161oY;
import logic.data.mbean.C0505HA;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import javax.vecmath.Tuple3d;
import java.util.*;

@C6485anp
@C5511aMd
/* renamed from: a.AF */
/* compiled from: a */
public abstract class Mission<T extends C4045yZ> extends TaikodomObject implements C3161oY.C3162a, C6222aim, C3161oY.C3162a {

    /* renamed from: Lc */
    public static final C2491fm f11Lc = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getOffloaders_0020_0028_0029Ljava_002futil_002fCollection_003b */
    public static final C2491fm f12x99c43db3 = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f13x13860637 = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLS = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLX = null;
    public static final C2491fm aLY = null;
    public static final C2491fm aMe = null;
    public static final C2491fm avN = null;
    public static final C2491fm bYI = null;
    public static final C2491fm beF = null;
    public static final C2491fm beG = null;
    public static final C2491fm beH = null;
    public static final C5663aRz bid = null;
    public static final C2491fm byB = null;
    public static final C2491fm byC = null;
    public static final C2491fm byD = null;
    public static final C2491fm byE = null;
    public static final C2491fm byF = null;
    public static final C2491fm byG = null;
    public static final C2491fm byH = null;
    public static final C2491fm byI = null;
    public static final C2491fm byJ = null;
    public static final C2491fm byK = null;
    public static final C2491fm byL = null;
    public static final C2491fm byM = null;
    public static final C2491fm byN = null;
    public static final C2491fm byO = null;
    public static final C2491fm byP = null;
    public static final C2491fm byQ = null;
    public static final C2491fm byR = null;
    public static final C2491fm byS = null;
    public static final C2491fm cpT = null;
    public static final C2491fm dFZ = null;
    public static final C2491fm dGa = null;
    public static final C2491fm dGc = null;
    public static final C5663aRz dRo = null;
    public static final C5663aRz eRV = null;
    public static final C2491fm fPA = null;
    public static final C2491fm fPB = null;
    public static final C2491fm fPC = null;
    public static final C2491fm fPD = null;
    public static final C2491fm fPE = null;
    public static final C2491fm fPF = null;
    public static final C2491fm fPG = null;
    public static final C2491fm fPH = null;
    public static final C2491fm fPI = null;
    public static final C2491fm fPJ = null;
    public static final C2491fm fPK = null;
    public static final C2491fm fPL = null;
    public static final C2491fm fPM = null;
    public static final C2491fm fPN = null;
    public static final C2491fm fPO = null;
    public static final C2491fm fPP = null;
    public static final C2491fm fPQ = null;
    public static final C2491fm fPR = null;
    public static final C2491fm fPS = null;
    public static final C2491fm fPT = null;
    public static final C2491fm fPU = null;
    public static final C2491fm fPV = null;
    public static final C2491fm fPW = null;
    public static final C2491fm fPX = null;
    public static final C2491fm fPY = null;
    public static final C2491fm fPZ = null;
    public static final C5663aRz fPq = null;
    public static final C5663aRz fPr = null;
    public static final C5663aRz fPs = null;
    public static final C5663aRz fPt = null;
    public static final C5663aRz fPu = null;
    public static final C5663aRz fPv = null;
    public static final C5663aRz fPw = null;
    public static final C5663aRz fPx = null;
    public static final C5663aRz fPy = null;
    public static final C2491fm fPz = null;
    public static final C2491fm fQa = null;
    public static final C2491fm fQb = null;
    public static final C2491fm fQc = null;
    public static final C2491fm fQd = null;
    public static final C2491fm fQe = null;
    public static final C2491fm fQf = null;
    public static final C2491fm fQg = null;
    public static final C2491fm fQh = null;
    public static final C2491fm fQi = null;
    public static final C2491fm fQj = null;
    public static final C2491fm fQk = null;
    public static final C2491fm fQl = null;
    public static final C2491fm fQm = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "62b4ce0ab39edb5a52e95752a92ce832", aum = 0)
    private static LDScriptingController dbY;
    @C0064Am(aul = "4af313ac3bd302374e3a6d864f25fbca", aum = 2)
    private static C4045yZ dbZ;
    @C0064Am(aul = "f8f7aff71b31bde5fa140260f0588e12", aum = 3)
    private static C2686iZ<MissionTimer> dca;
    @C0064Am(aul = "b1fa53dea9d5f9a77d41d9ea00b9ec7c", aum = 4)
    private static C2686iZ<MissionTrigger> dcb;
    @C0064Am(aul = "9e2ea73ce448fc7f35f6c85369530556", aum = 5)
    private static Actor dcc;
    @C0064Am(aul = "35f5505ef92412f1e128fc997292ad63", aum = 6)
    private static Actor dcd;
    @C0064Am(aul = "39b1bcd73eed57626d96860fa43df2a3", aum = 7)
    private static C2686iZ<NPC> dce;
    @C0064Am(aul = "1e92689c6677ef81ede1960810ac3394", aum = 8)
    private static C2686iZ<NPC> dcf;
    @C0064Am(aul = "aa0c717889a7c1634c38e9fd0c102337", aum = 9)
    private static C2686iZ<MissionItem> dcg;
    @C0064Am(aul = "e7d5a1e16d5a648daf6e780b3fa5cef7", aum = 10)
    private static boolean dch;
    @C0064Am(aul = "4d9b2bdc4b276ca6692969f9be624966", aum = 11)
    private static boolean dci;
    @C0064Am(aul = "cbcdbb9ca3e3484eb24b6920064586ff", aum = 1)

    /* renamed from: oi */
    private static C0015a f14oi;

    static {
        m63V();
    }

    public Mission() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Mission(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m63V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 12;
        _m_methodCount = TaikodomObject._m_methodCount + 81;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(Mission.class, "62b4ce0ab39edb5a52e95752a92ce832", i);
        fPq = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Mission.class, "cbcdbb9ca3e3484eb24b6920064586ff", i2);
        bid = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Mission.class, "4af313ac3bd302374e3a6d864f25fbca", i3);
        eRV = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Mission.class, "f8f7aff71b31bde5fa140260f0588e12", i4);
        fPr = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Mission.class, "b1fa53dea9d5f9a77d41d9ea00b9ec7c", i5);
        dRo = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Mission.class, "9e2ea73ce448fc7f35f6c85369530556", i6);
        fPs = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Mission.class, "35f5505ef92412f1e128fc997292ad63", i7);
        fPt = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Mission.class, "39b1bcd73eed57626d96860fa43df2a3", i8);
        fPu = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Mission.class, "1e92689c6677ef81ede1960810ac3394", i9);
        fPv = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Mission.class, "aa0c717889a7c1634c38e9fd0c102337", i10);
        fPw = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Mission.class, "e7d5a1e16d5a648daf6e780b3fa5cef7", i11);
        fPx = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Mission.class, "4d9b2bdc4b276ca6692969f9be624966", i12);
        fPy = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i14 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 81)];
        C2491fm a = C4105zY.m41624a(Mission.class, "ef994e20049567b81ea6ff43f9eafd52", i14);
        aLT = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(Mission.class, "4dc497c949f9f58762f787ef50172bd9", i15);
        aLU = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(Mission.class, "faa10d2e287d0d8c2601f8189ef26b75", i16);
        byJ = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(Mission.class, "4bbf083430414238a8bff61db98ac7d7", i17);
        byK = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(Mission.class, "7814e698cf58e13c01064b22ca5d00e8", i18);
        byL = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(Mission.class, "bbbe658edce6c9ab82773be410e2ce8c", i19);
        aLS = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(Mission.class, "8048b3c461e2810fc3b7cd62d98dd2c3", i20);
        aMe = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(Mission.class, "bad651b2f405ddd1a302363f86c9a368", i21);
        byI = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(Mission.class, "978f04ad8cd1bf607f6004ef4bf35ecc", i22);
        aLY = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(Mission.class, "4a193aa64c51a437fc76bf7146f3ff95", i23);
        aLR = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(Mission.class, "7ea172df90744fbbcd9bd18c754ff369", i24);
        beF = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(Mission.class, "dc2894a40c77510b19fb254d57661971", i25);
        beH = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(Mission.class, "7bfa224e1e13e78955652f288ba96f4a", i26);
        byN = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(Mission.class, "1f4c66475f88cdf783a94eda1397d519", i27);
        byM = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(Mission.class, "163b3384abb8fdf5954f5da183ed5600", i28);
        byO = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(Mission.class, "ef0b89d0535ab4362afb2b145efefb75", i29);
        byP = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(Mission.class, "4f822beec8bfd8a5bc8866d10e134efe", i30);
        byQ = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(Mission.class, "eac86e72a5c800b62e3946f263b16e44", i31);
        byS = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(Mission.class, "2219d8bda79c89689985f0909206fd0b", i32);
        aLX = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(Mission.class, "da89c867e6537f14f0e079263eb6f9f2", i33);
        byB = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(Mission.class, "6e270f7c5c8ea51090bbd07f3f295bbe", i34);
        byG = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(Mission.class, "37e2609ef6e722fe09b22a742f2319dc", i35);
        byF = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(Mission.class, "71afa6d852fac2953c48cf7a18648727", i36);
        beG = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        C2491fm a24 = C4105zY.m41624a(Mission.class, "8179b8eca91cdd431417b1014e56a941", i37);
        byH = a24;
        fmVarArr[i37] = a24;
        int i38 = i37 + 1;
        C2491fm a25 = C4105zY.m41624a(Mission.class, "85850e70b4aae6ae7f497ef90e6f13b9", i38);
        byC = a25;
        fmVarArr[i38] = a25;
        int i39 = i38 + 1;
        C2491fm a26 = C4105zY.m41624a(Mission.class, "e9828b7b757f3d881bce0ee1e978efbe", i39);
        byR = a26;
        fmVarArr[i39] = a26;
        int i40 = i39 + 1;
        C2491fm a27 = C4105zY.m41624a(Mission.class, "e4abf6282dccfbffddf67a5932a90960", i40);
        byD = a27;
        fmVarArr[i40] = a27;
        int i41 = i40 + 1;
        C2491fm a28 = C4105zY.m41624a(Mission.class, "7faa4502226435a05c85919966c28827", i41);
        byE = a28;
        fmVarArr[i41] = a28;
        int i42 = i41 + 1;
        C2491fm a29 = C4105zY.m41624a(Mission.class, "9a8f60fee3cfba4bff7aac438f919f66", i42);
        aLV = a29;
        fmVarArr[i42] = a29;
        int i43 = i42 + 1;
        C2491fm a30 = C4105zY.m41624a(Mission.class, "0f9f01aa29310682387f83f088b96ad9", i43);
        aLW = a30;
        fmVarArr[i43] = a30;
        int i44 = i43 + 1;
        C2491fm a31 = C4105zY.m41624a(Mission.class, "310a2be2fd18176a1827994f889c0315", i44);
        fPz = a31;
        fmVarArr[i44] = a31;
        int i45 = i44 + 1;
        C2491fm a32 = C4105zY.m41624a(Mission.class, "eb2774f551989af88b54bc6aa38746f9", i45);
        fPA = a32;
        fmVarArr[i45] = a32;
        int i46 = i45 + 1;
        C2491fm a33 = C4105zY.m41624a(Mission.class, "c22f80195f84cccec49c96eb7fed16df", i46);
        fPB = a33;
        fmVarArr[i46] = a33;
        int i47 = i46 + 1;
        C2491fm a34 = C4105zY.m41624a(Mission.class, "5dd9bed95a0c6fa7f77ecd9a153d4398", i47);
        fPC = a34;
        fmVarArr[i47] = a34;
        int i48 = i47 + 1;
        C2491fm a35 = C4105zY.m41624a(Mission.class, "dd82e1764a2e1676da13fa36a37deea3", i48);
        fPD = a35;
        fmVarArr[i48] = a35;
        int i49 = i48 + 1;
        C2491fm a36 = C4105zY.m41624a(Mission.class, "de968ec55f130e72b96f9d15def5f960", i49);
        f13x13860637 = a36;
        fmVarArr[i49] = a36;
        int i50 = i49 + 1;
        C2491fm a37 = C4105zY.m41624a(Mission.class, "09719510712df50c00c624342b27d1b5", i50);
        fPE = a37;
        fmVarArr[i50] = a37;
        int i51 = i50 + 1;
        C2491fm a38 = C4105zY.m41624a(Mission.class, "25bb3599062881fe242cda990b0a7a31", i51);
        fPF = a38;
        fmVarArr[i51] = a38;
        int i52 = i51 + 1;
        C2491fm a39 = C4105zY.m41624a(Mission.class, "28cc90113392622f59f9ad7de769be26", i52);
        fPG = a39;
        fmVarArr[i52] = a39;
        int i53 = i52 + 1;
        C2491fm a40 = C4105zY.m41624a(Mission.class, "daed8bd7bd3a153d6061632b6d89a403", i53);
        fPH = a40;
        fmVarArr[i53] = a40;
        int i54 = i53 + 1;
        C2491fm a41 = C4105zY.m41624a(Mission.class, "fa3cfbc1c9322257948c52864d8b3ee3", i54);
        fPI = a41;
        fmVarArr[i54] = a41;
        int i55 = i54 + 1;
        C2491fm a42 = C4105zY.m41624a(Mission.class, "57e8043c98d9f6c438fc921272774368", i55);
        fPJ = a42;
        fmVarArr[i55] = a42;
        int i56 = i55 + 1;
        C2491fm a43 = C4105zY.m41624a(Mission.class, "252ad628720d92e4edd301f521c58a19", i56);
        fPK = a43;
        fmVarArr[i56] = a43;
        int i57 = i56 + 1;
        C2491fm a44 = C4105zY.m41624a(Mission.class, "d4aa0e40b3aec0901bca9663a34c84c6", i57);
        fPL = a44;
        fmVarArr[i57] = a44;
        int i58 = i57 + 1;
        C2491fm a45 = C4105zY.m41624a(Mission.class, "abde149411375985d67658bc820a6183", i58);
        fPM = a45;
        fmVarArr[i58] = a45;
        int i59 = i58 + 1;
        C2491fm a46 = C4105zY.m41624a(Mission.class, "0ff641d0e2e4c77af7b735163eaed500", i59);
        fPN = a46;
        fmVarArr[i59] = a46;
        int i60 = i59 + 1;
        C2491fm a47 = C4105zY.m41624a(Mission.class, "a80fb22818e37293bdd9621af84de729", i60);
        fPO = a47;
        fmVarArr[i60] = a47;
        int i61 = i60 + 1;
        C2491fm a48 = C4105zY.m41624a(Mission.class, "e6435f6cadccd6ecc29dee922a9788c0", i61);
        fPP = a48;
        fmVarArr[i61] = a48;
        int i62 = i61 + 1;
        C2491fm a49 = C4105zY.m41624a(Mission.class, "9a6e272ee2c5fa79c398b323cca6f4f6", i62);
        bYI = a49;
        fmVarArr[i62] = a49;
        int i63 = i62 + 1;
        C2491fm a50 = C4105zY.m41624a(Mission.class, "f03b3344f583b160721d9f36a244a8e9", i63);
        f11Lc = a50;
        fmVarArr[i63] = a50;
        int i64 = i63 + 1;
        C2491fm a51 = C4105zY.m41624a(Mission.class, "f64d02c5a1bad5736ec237586f514f33", i64);
        fPQ = a51;
        fmVarArr[i64] = a51;
        int i65 = i64 + 1;
        C2491fm a52 = C4105zY.m41624a(Mission.class, "ba157a12c7bdf0a2736c81533fba42dd", i65);
        fPR = a52;
        fmVarArr[i65] = a52;
        int i66 = i65 + 1;
        C2491fm a53 = C4105zY.m41624a(Mission.class, "0f1497b66fc9ad8dcf012c00e026098e", i66);
        fPS = a53;
        fmVarArr[i66] = a53;
        int i67 = i66 + 1;
        C2491fm a54 = C4105zY.m41624a(Mission.class, "8abe81bfb78d83ef0953eeb0c076fbbd", i67);
        fPT = a54;
        fmVarArr[i67] = a54;
        int i68 = i67 + 1;
        C2491fm a55 = C4105zY.m41624a(Mission.class, "c444b0a2991e272629d157e77c8bb5fd", i68);
        fPU = a55;
        fmVarArr[i68] = a55;
        int i69 = i68 + 1;
        C2491fm a56 = C4105zY.m41624a(Mission.class, "cd3e8a1d91894d9b130c6d7019907e4b", i69);
        fPV = a56;
        fmVarArr[i69] = a56;
        int i70 = i69 + 1;
        C2491fm a57 = C4105zY.m41624a(Mission.class, "d76b25a54e186f806faa281bf6016190", i70);
        fPW = a57;
        fmVarArr[i70] = a57;
        int i71 = i70 + 1;
        C2491fm a58 = C4105zY.m41624a(Mission.class, "77f9b737dfccf7f5a4238a517a93815f", i71);
        fPX = a58;
        fmVarArr[i71] = a58;
        int i72 = i71 + 1;
        C2491fm a59 = C4105zY.m41624a(Mission.class, "bdc918b8f1d7a17dd14fa4e7fc3c5a2e", i72);
        fPY = a59;
        fmVarArr[i72] = a59;
        int i73 = i72 + 1;
        C2491fm a60 = C4105zY.m41624a(Mission.class, "86e0666d30efeb74044d6f31244bd486", i73);
        fPZ = a60;
        fmVarArr[i73] = a60;
        int i74 = i73 + 1;
        C2491fm a61 = C4105zY.m41624a(Mission.class, "0bbcca1906d7db6f66ba8f1589e95c04", i74);
        fQa = a61;
        fmVarArr[i74] = a61;
        int i75 = i74 + 1;
        C2491fm a62 = C4105zY.m41624a(Mission.class, "406b68b752f86993a6576ce04381295c", i75);
        _f_dispose_0020_0028_0029V = a62;
        fmVarArr[i75] = a62;
        int i76 = i75 + 1;
        C2491fm a63 = C4105zY.m41624a(Mission.class, "30ea7be683aa5778d10a557a4a60864c", i76);
        fQb = a63;
        fmVarArr[i76] = a63;
        int i77 = i76 + 1;
        C2491fm a64 = C4105zY.m41624a(Mission.class, "52e6c010628ec29bb39f0ee644b565e2", i77);
        fQc = a64;
        fmVarArr[i77] = a64;
        int i78 = i77 + 1;
        C2491fm a65 = C4105zY.m41624a(Mission.class, "6e365ab7c3dd067826496bf815d971f5", i78);
        fQd = a65;
        fmVarArr[i78] = a65;
        int i79 = i78 + 1;
        C2491fm a66 = C4105zY.m41624a(Mission.class, "1d71cc8920ddbde79fca1ad5b8a5e4d5", i79);
        _f_onResurrect_0020_0028_0029V = a66;
        fmVarArr[i79] = a66;
        int i80 = i79 + 1;
        C2491fm a67 = C4105zY.m41624a(Mission.class, "d7bc0377f2ca21d029f944dad45b44eb", i80);
        fQe = a67;
        fmVarArr[i80] = a67;
        int i81 = i80 + 1;
        C2491fm a68 = C4105zY.m41624a(Mission.class, "bab72780cc5532b83d2d7be103c0e11b", i81);
        fQf = a68;
        fmVarArr[i81] = a68;
        int i82 = i81 + 1;
        C2491fm a69 = C4105zY.m41624a(Mission.class, "78aa4b676f55dd74c19c8cb4d6147fcf", i82);
        fQg = a69;
        fmVarArr[i82] = a69;
        int i83 = i82 + 1;
        C2491fm a70 = C4105zY.m41624a(Mission.class, "0820f77d9f5ed67c3b0b36da8e9237c6", i83);
        fQh = a70;
        fmVarArr[i83] = a70;
        int i84 = i83 + 1;
        C2491fm a71 = C4105zY.m41624a(Mission.class, "92f401093da8bfba7c43aa3db2578aea", i84);
        cpT = a71;
        fmVarArr[i84] = a71;
        int i85 = i84 + 1;
        C2491fm a72 = C4105zY.m41624a(Mission.class, "9bbbd4593d985500ad9a71e8d3784b3c", i85);
        fQi = a72;
        fmVarArr[i85] = a72;
        int i86 = i85 + 1;
        C2491fm a73 = C4105zY.m41624a(Mission.class, "b63dda6967885c7daea3366e31043c18", i86);
        fQj = a73;
        fmVarArr[i86] = a73;
        int i87 = i86 + 1;
        C2491fm a74 = C4105zY.m41624a(Mission.class, "846b548542c2f619df4f2568753119fe", i87);
        fQk = a74;
        fmVarArr[i87] = a74;
        int i88 = i87 + 1;
        C2491fm a75 = C4105zY.m41624a(Mission.class, "ba987a822545cabf9095a5095832b276", i88);
        avN = a75;
        fmVarArr[i88] = a75;
        int i89 = i88 + 1;
        C2491fm a76 = C4105zY.m41624a(Mission.class, "019179d5e81f997edee826507eb38fe2", i89);
        f12x99c43db3 = a76;
        fmVarArr[i89] = a76;
        int i90 = i89 + 1;
        C2491fm a77 = C4105zY.m41624a(Mission.class, "0a54f67638df6c3a8ed65fe7bd90359d", i90);
        fQl = a77;
        fmVarArr[i90] = a77;
        int i91 = i90 + 1;
        C2491fm a78 = C4105zY.m41624a(Mission.class, "b82ee3ad43e89b5abaf4185e21f7a88f", i91);
        fQm = a78;
        fmVarArr[i91] = a78;
        int i92 = i91 + 1;
        C2491fm a79 = C4105zY.m41624a(Mission.class, "17eb3bce5ad2886f4bf1ef479600b705", i92);
        dGc = a79;
        fmVarArr[i92] = a79;
        int i93 = i92 + 1;
        C2491fm a80 = C4105zY.m41624a(Mission.class, "ca60fb035ad416120dd5d3dd91743a88", i93);
        dFZ = a80;
        fmVarArr[i93] = a80;
        int i94 = i93 + 1;
        C2491fm a81 = C4105zY.m41624a(Mission.class, "658de7392e3a425de53857dbabd0b844", i94);
        dGa = a81;
        fmVarArr[i94] = a81;
        int i95 = i94 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Mission.class, C0505HA.class, _m_fields, _m_methods);
    }

    /* renamed from: Q */
    private void m53Q(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(fPr, iZVar);
    }

    /* renamed from: R */
    private void m54R(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(dRo, iZVar);
    }

    @C0064Am(aul = "ef994e20049567b81ea6ff43f9eafd52", aum = 0)
    @C5566aOg
    /* renamed from: RJ */
    private void m55RJ() {
        throw new aWi(new aCE(this, aLT, new Object[0]));
    }

    @C0064Am(aul = "4dc497c949f9f58762f787ef50172bd9", aum = 0)
    @C5566aOg
    /* renamed from: RL */
    private void m56RL() {
        throw new aWi(new aCE(this, aLU, new Object[0]));
    }

    @C0064Am(aul = "2219d8bda79c89689985f0909206fd0b", aum = 0)
    @C5566aOg
    /* renamed from: RN */
    private void m57RN() {
        throw new aWi(new aCE(this, aLX, new Object[0]));
    }

    /* renamed from: S */
    private void m58S(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(fPu, iZVar);
    }

    /* renamed from: T */
    private void m60T(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(fPv, iZVar);
    }

    /* renamed from: U */
    private void m61U(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(fPw, iZVar);
    }

    @C0064Am(aul = "5dd9bed95a0c6fa7f77ecd9a153d4398", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m65a(NPCType aed, StellarSystem jj, Vec3d ajr) {
        throw new aWi(new aCE(this, fPC, new Object[]{aed, jj, ajr}));
    }

    @C0064Am(aul = "dd82e1764a2e1676da13fa36a37deea3", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m66a(NPCType aed, StellarSystem jj, Vec3d ajr, boolean z, Collection<NPC> collection) {
        throw new aWi(new aCE(this, fPD, new Object[]{aed, jj, ajr, new Boolean(z), collection}));
    }

    @C0064Am(aul = "09719510712df50c00c624342b27d1b5", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m67a(String str, NPCType aed, StellarSystem jj, Vec3d ajr) {
        throw new aWi(new aCE(this, fPE, new Object[]{str, aed, jj, ajr}));
    }

    @C0064Am(aul = "28cc90113392622f59f9ad7de769be26", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m68a(String str, NPCType aed, StellarSystem jj, Vec3d ajr, float f) {
        throw new aWi(new aCE(this, fPG, new Object[]{str, aed, jj, ajr, new Float(f)}));
    }

    @C0064Am(aul = "daed8bd7bd3a153d6061632b6d89a403", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m69a(String str, NPCType aed, StellarSystem jj, Vec3d ajr, boolean z) {
        throw new aWi(new aCE(this, fPH, new Object[]{str, aed, jj, ajr, new Boolean(z)}));
    }

    @C0064Am(aul = "fa3cfbc1c9322257948c52864d8b3ee3", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m70a(String str, NPCType aed, Vec3d ajr, String str2, Vec3d ajr2, long j, boolean z, StellarSystem jj, boolean z2) {
        throw new aWi(new aCE(this, fPI, new Object[]{str, aed, ajr, str2, ajr2, new Long(j), new Boolean(z), jj, new Boolean(z2)}));
    }

    /* renamed from: a */
    private void m72a(C0015a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    @C0064Am(aul = "978f04ad8cd1bf607f6004ef4bf35ecc", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m73a(Actor cr, C5260aCm acm) {
        throw new aWi(new aCE(this, aLY, new Object[]{cr, acm}));
    }

    @C0064Am(aul = "e9828b7b757f3d881bce0ee1e978efbe", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m74a(LootType ahc, ItemType jCVar, int i) {
        throw new aWi(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
    }

    @C0064Am(aul = "163b3384abb8fdf5954f5da183ed5600", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m75a(MissionTrigger aus) {
        throw new aWi(new aCE(this, byO, new Object[]{aus}));
    }

    @C0064Am(aul = "7faa4502226435a05c85919966c28827", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m76a(Character acx, Loot ael) {
        throw new aWi(new aCE(this, byE, new Object[]{acx, ael}));
    }

    @C0064Am(aul = "bdc918b8f1d7a17dd14fa4e7fc3c5a2e", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m77a(MissionTimer apy) {
        throw new aWi(new aCE(this, fPY, new Object[]{apy}));
    }

    @C0064Am(aul = "9a8f60fee3cfba4bff7aac438f919f66", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m78a(Item auq, ItemLocation aag, ItemLocation aag2) {
        throw new aWi(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
    }

    /* renamed from: a */
    private void m79a(C4045yZ yZVar) {
        bFf().mo5608dq().mo3197f(eRV, yZVar);
    }

    @C0064Am(aul = "da89c867e6537f14f0e079263eb6f9f2", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m80a(String str, Station bf) {
        throw new aWi(new aCE(this, byB, new Object[]{str, bf}));
    }

    @C0064Am(aul = "252ad628720d92e4edd301f521c58a19", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m81a(String str, Vec3d ajr, StellarSystem jj, long j, boolean z, boolean z2) {
        throw new aWi(new aCE(this, fPK, new Object[]{str, ajr, jj, new Long(j), new Boolean(z), new Boolean(z2)}));
    }

    @C0064Am(aul = "85850e70b4aae6ae7f497ef90e6f13b9", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m82a(String str, Character acx) {
        throw new aWi(new aCE(this, byC, new Object[]{str, acx}));
    }

    @C0064Am(aul = "37e2609ef6e722fe09b22a742f2319dc", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m83a(String str, Ship fAVar) {
        throw new aWi(new aCE(this, byF, new Object[]{str, fAVar}));
    }

    @C0064Am(aul = "71afa6d852fac2953c48cf7a18648727", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m84a(String str, Ship fAVar, Ship fAVar2) {
        throw new aWi(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
    }

    /* renamed from: aQ */
    private void m86aQ(Actor cr) {
        bFf().mo5608dq().mo3197f(fPs, cr);
    }

    /* renamed from: aR */
    private void m87aR(Actor cr) {
        bFf().mo5608dq().mo3197f(fPt, cr);
    }

    @C0064Am(aul = "a80fb22818e37293bdd9621af84de729", aum = 0)
    @C5566aOg
    /* renamed from: aU */
    private void m89aU(Actor cr) {
        throw new aWi(new aCE(this, fPO, new Object[]{cr}));
    }

    @C0064Am(aul = "4a193aa64c51a437fc76bf7146f3ff95", aum = 0)
    @C5566aOg
    /* renamed from: aV */
    private void m90aV(String str) {
        throw new aWi(new aCE(this, aLR, new Object[]{str}));
    }

    private C0015a abJ() {
        return (C0015a) bFf().mo5608dq().mo3214p(bid);
    }

    @C0064Am(aul = "ef0b89d0535ab4362afb2b145efefb75", aum = 0)
    @C5566aOg
    private void akJ() {
        throw new aWi(new aCE(this, byP, new Object[0]));
    }

    @C0064Am(aul = "eac86e72a5c800b62e3946f263b16e44", aum = 0)
    @C5566aOg
    private void akL() {
        throw new aWi(new aCE(this, byS, new Object[0]));
    }

    @C5566aOg
    /* renamed from: b */
    private NPC m91b(NPCType aed, StellarSystem jj, Vec3d ajr, boolean z, Collection<NPC> collection) {
        switch (bFf().mo6893i(fPD)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fPD, new Object[]{aed, jj, ajr, new Boolean(z), collection}));
            case 3:
                bFf().mo5606d(new aCE(this, fPD, new Object[]{aed, jj, ajr, new Boolean(z), collection}));
                break;
        }
        return m66a(aed, jj, ajr, z, collection);
    }

    /* renamed from: b */
    private List<Gate> m92b(Node rPVar, Node rPVar2, List<Node> list) {
        switch (bFf().mo6893i(fPL)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fPL, new Object[]{rPVar, rPVar2, list}));
            case 3:
                bFf().mo5606d(new aCE(this, fPL, new Object[]{rPVar, rPVar2, list}));
                break;
        }
        return m71a(rPVar, rPVar2, list);
    }

    @C0064Am(aul = "e4abf6282dccfbffddf67a5932a90960", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m94b(Loot ael) {
        throw new aWi(new aCE(this, byD, new Object[]{ael}));
    }

    @C0064Am(aul = "cd3e8a1d91894d9b130c6d7019907e4b", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m95b(T t) {
        throw new aWi(new aCE(this, fPV, new Object[]{t}));
    }

    @C0064Am(aul = "eb2774f551989af88b54bc6aa38746f9", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m96b(String str, long j, int i) {
        throw new aWi(new aCE(this, fPA, new Object[]{str, new Long(j), new Integer(i)}));
    }

    @C0064Am(aul = "7ea172df90744fbbcd9bd18c754ff369", aum = 0)
    @C5566aOg
    /* renamed from: bk */
    private boolean m97bk(String str) {
        throw new aWi(new aCE(this, beF, new Object[]{str}));
    }

    @C0064Am(aul = "57e8043c98d9f6c438fc921272774368", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private NPCEventDispatcher<T> m98c(String str, NPCType aed, StellarSystem jj, Vec3d ajr, boolean z) {
        throw new aWi(new aCE(this, fPJ, new Object[]{str, aed, jj, ajr, new Boolean(z)}));
    }

    @C0064Am(aul = "86e0666d30efeb74044d6f31244bd486", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m99c(MissionTrigger aus) {
        throw new aWi(new aCE(this, fPZ, new Object[]{aus}));
    }

    @C0064Am(aul = "0f9f01aa29310682387f83f088b96ad9", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m100c(Item auq, ItemLocation aag) {
        throw new aWi(new aCE(this, aLW, new Object[]{auq, aag}));
    }

    @C0064Am(aul = "6e270f7c5c8ea51090bbd07f3f295bbe", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m102c(String str, Ship fAVar) {
        throw new aWi(new aCE(this, byG, new Object[]{str, fAVar}));
    }

    private LDScriptingController ccU() {
        return (LDScriptingController) bFf().mo5608dq().mo3214p(fPq);
    }

    private C4045yZ ccV() {
        return (C4045yZ) bFf().mo5608dq().mo3214p(eRV);
    }

    private C2686iZ ccW() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(fPr);
    }

    private C2686iZ ccX() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(dRo);
    }

    private Actor ccY() {
        return (Actor) bFf().mo5608dq().mo3214p(fPs);
    }

    private Actor ccZ() {
        return (Actor) bFf().mo5608dq().mo3214p(fPt);
    }

    @C0064Am(aul = "d7bc0377f2ca21d029f944dad45b44eb", aum = 0)
    @C5566aOg
    private void cdA() {
        throw new aWi(new aCE(this, fQe, new Object[0]));
    }

    @C0064Am(aul = "bab72780cc5532b83d2d7be103c0e11b", aum = 0)
    @C5566aOg
    private void cdC() {
        throw new aWi(new aCE(this, fQf, new Object[0]));
    }

    @C0064Am(aul = "78aa4b676f55dd74c19c8cb4d6147fcf", aum = 0)
    @C5566aOg
    private void cdE() {
        throw new aWi(new aCE(this, fQg, new Object[0]));
    }

    @C0064Am(aul = "0820f77d9f5ed67c3b0b36da8e9237c6", aum = 0)
    @C5566aOg
    private void cdG() {
        throw new aWi(new aCE(this, fQh, new Object[0]));
    }

    private C2686iZ cda() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(fPu);
    }

    private C2686iZ cdb() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(fPv);
    }

    private C2686iZ cdc() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(fPw);
    }

    private boolean cdd() {
        return bFf().mo5608dq().mo3201h(fPx);
    }

    private boolean cde() {
        return bFf().mo5608dq().mo3201h(fPy);
    }

    @C0064Am(aul = "ba157a12c7bdf0a2736c81533fba42dd", aum = 0)
    @C5566aOg
    @C6143ahL
    private void cdj() {
        throw new aWi(new aCE(this, fPR, new Object[0]));
    }

    @C0064Am(aul = "8abe81bfb78d83ef0953eeb0c076fbbd", aum = 0)
    @C5566aOg
    @C6143ahL
    private void cdn() {
        throw new aWi(new aCE(this, fPT, new Object[0]));
    }

    @C0064Am(aul = "0bbcca1906d7db6f66ba8f1589e95c04", aum = 0)
    @C5566aOg
    private Mission cds() {
        throw new aWi(new aCE(this, fQa, new Object[0]));
    }

    @C0064Am(aul = "30ea7be683aa5778d10a557a4a60864c", aum = 0)
    @C5566aOg
    private void cdu() {
        throw new aWi(new aCE(this, fQb, new Object[0]));
    }

    @C0064Am(aul = "52e6c010628ec29bb39f0ee644b565e2", aum = 0)
    @C5566aOg
    private void cdw() {
        throw new aWi(new aCE(this, fQc, new Object[0]));
    }

    @C5566aOg
    private void cdx() {
        switch (bFf().mo6893i(fQc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQc, new Object[0]));
                break;
        }
        cdw();
    }

    @C0064Am(aul = "6e365ab7c3dd067826496bf815d971f5", aum = 0)
    @C5566aOg
    private void cdy() {
        throw new aWi(new aCE(this, fQd, new Object[0]));
    }

    @C5566aOg
    private void cdz() {
        switch (bFf().mo6893i(fQd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQd, new Object[0]));
                break;
        }
        cdy();
    }

    @C0064Am(aul = "4f822beec8bfd8a5bc8866d10e134efe", aum = 0)
    @C5566aOg
    /* renamed from: cq */
    private void m103cq(String str) {
        throw new aWi(new aCE(this, byQ, new Object[]{str}));
    }

    @C5566aOg
    /* renamed from: d */
    private final NPCEventDispatcher<T> m104d(String str, NPCType aed, StellarSystem jj, Vec3d ajr, boolean z) {
        switch (bFf().mo6893i(fPJ)) {
            case 0:
                return null;
            case 2:
                return (NPCEventDispatcher) bFf().mo5606d(new aCE(this, fPJ, new Object[]{str, aed, jj, ajr, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, fPJ, new Object[]{str, aed, jj, ajr, new Boolean(z)}));
                break;
        }
        return m98c(str, aed, jj, ajr, z);
    }

    @C0064Am(aul = "7814e698cf58e13c01064b22ca5d00e8", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m105d(Node rPVar) {
        throw new aWi(new aCE(this, byL, new Object[]{rPVar}));
    }

    @C0064Am(aul = "310a2be2fd18176a1827994f889c0315", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m106e(I18NString i18NString, Object[] objArr) {
        throw new aWi(new aCE(this, fPz, new Object[]{i18NString, objArr}));
    }

    /* renamed from: eZ */
    private void m107eZ(boolean z) {
        bFf().mo5608dq().mo3153a(fPx, z);
    }

    /* renamed from: f */
    private void m108f(LDScriptingController lYVar) {
        bFf().mo5608dq().mo3197f(fPq, lYVar);
    }

    /* renamed from: fa */
    private void m110fa(boolean z) {
        bFf().mo5608dq().mo3153a(fPy, z);
    }

    @C0064Am(aul = "bbbe658edce6c9ab82773be410e2ce8c", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m112g(Ship fAVar) {
        throw new aWi(new aCE(this, aLS, new Object[]{fAVar}));
    }

    @C0064Am(aul = "77f9b737dfccf7f5a4238a517a93815f", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m113g(LDScriptingController lYVar) {
        throw new aWi(new aCE(this, fPX, new Object[]{lYVar}));
    }

    @C0064Am(aul = "8048b3c461e2810fc3b7cd62d98dd2c3", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m114i(Ship fAVar) {
        throw new aWi(new aCE(this, aMe, new Object[]{fAVar}));
    }

    @C0064Am(aul = "c22f80195f84cccec49c96eb7fed16df", aum = 0)
    @C5566aOg
    /* renamed from: iW */
    private void m115iW(String str) {
        throw new aWi(new aCE(this, fPB, new Object[]{str}));
    }

    @C0064Am(aul = "e6435f6cadccd6ecc29dee922a9788c0", aum = 0)
    @C5566aOg
    /* renamed from: iY */
    private void m116iY(String str) {
        throw new aWi(new aCE(this, fPP, new Object[]{str}));
    }

    @C0064Am(aul = "1f4c66475f88cdf783a94eda1397d519", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m117k(Gate fFVar) {
        throw new aWi(new aCE(this, byM, new Object[]{fFVar}));
    }

    @C0064Am(aul = "dc2894a40c77510b19fb254d57661971", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m118n(Ship fAVar) {
        throw new aWi(new aCE(this, beH, new Object[]{fAVar}));
    }

    @C0064Am(aul = "faa10d2e287d0d8c2601f8189ef26b75", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m119p(Station bf) {
        throw new aWi(new aCE(this, byJ, new Object[]{bf}));
    }

    @C0064Am(aul = "4bbf083430414238a8bff61db98ac7d7", aum = 0)
    @C5566aOg
    /* renamed from: r */
    private void m121r(Station bf) {
        throw new aWi(new aCE(this, byK, new Object[]{bf}));
    }

    @C0064Am(aul = "8179b8eca91cdd431417b1014e56a941", aum = 0)
    @C5566aOg
    /* renamed from: s */
    private void m122s(String str, String str2) {
        throw new aWi(new aCE(this, byH, new Object[]{str, str2}));
    }

    @C0064Am(aul = "7bfa224e1e13e78955652f288ba96f4a", aum = 0)
    @C5566aOg
    /* renamed from: t */
    private void m123t(Station bf) {
        throw new aWi(new aCE(this, byN, new Object[]{bf}));
    }

    @C0064Am(aul = "bad651b2f405ddd1a302363f86c9a368", aum = 0)
    @C5566aOg
    /* renamed from: w */
    private void m125w(Ship fAVar) {
        throw new aWi(new aCE(this, byI, new Object[]{fAVar}));
    }

    /* renamed from: G */
    public void mo63G(Item auq) {
        switch (bFf().mo6893i(fQl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQl, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQl, new Object[]{auq}));
                break;
        }
        m51F(auq);
    }

    /* renamed from: KY */
    public Player mo64KY() {
        switch (bFf().mo6893i(avN)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, avN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, avN, new Object[0]));
                break;
        }
        return m52KX();
    }

    @C5566aOg
    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m55RJ();
    }

    @C5566aOg
    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m56RL();
    }

    @C5566aOg
    /* renamed from: RO */
    public void mo67RO() {
        switch (bFf().mo6893i(aLX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                break;
        }
        m57RN();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: T */
    public final boolean mo68T(Ship fAVar) {
        switch (bFf().mo6893i(fQj)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fQj, new Object[]{fAVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fQj, new Object[]{fAVar}));
                break;
        }
        return m59S(fAVar);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public final boolean mo69V(Character acx) {
        switch (bFf().mo6893i(fQi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fQi, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fQi, new Object[]{acx}));
                break;
        }
        return m62U(acx);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0505HA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m55RJ();
                return null;
            case 1:
                m56RL();
                return null;
            case 2:
                m119p((Station) args[0]);
                return null;
            case 3:
                m121r((Station) args[0]);
                return null;
            case 4:
                m105d((Node) args[0]);
                return null;
            case 5:
                m112g((Ship) args[0]);
                return null;
            case 6:
                m114i((Ship) args[0]);
                return null;
            case 7:
                m125w((Ship) args[0]);
                return null;
            case 8:
                m73a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            case 9:
                m90aV((String) args[0]);
                return null;
            case 10:
                return new Boolean(m97bk((String) args[0]));
            case 11:
                m118n((Ship) args[0]);
                return null;
            case 12:
                m123t((Station) args[0]);
                return null;
            case 13:
                m117k((Gate) args[0]);
                return null;
            case 14:
                m75a((MissionTrigger) args[0]);
                return null;
            case 15:
                akJ();
                return null;
            case 16:
                m103cq((String) args[0]);
                return null;
            case 17:
                akL();
                return null;
            case 18:
                m57RN();
                return null;
            case 19:
                m80a((String) args[0], (Station) args[1]);
                return null;
            case 20:
                m102c((String) args[0], (Ship) args[1]);
                return null;
            case 21:
                m83a((String) args[0], (Ship) args[1]);
                return null;
            case 22:
                m84a((String) args[0], (Ship) args[1], (Ship) args[2]);
                return null;
            case 23:
                m122s((String) args[0], (String) args[1]);
                return null;
            case 24:
                m82a((String) args[0], (Character) args[1]);
                return null;
            case 25:
                m74a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 26:
                m94b((Loot) args[0]);
                return null;
            case 27:
                m76a((Character) args[0], (Loot) args[1]);
                return null;
            case 28:
                m78a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 29:
                m100c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 30:
                m106e((I18NString) args[0], (Object[]) args[1]);
                return null;
            case 31:
                m96b((String) args[0], ((Long) args[1]).longValue(), ((Integer) args[2]).intValue());
                return null;
            case 32:
                m115iW((String) args[0]);
                return null;
            case 33:
                return m65a((NPCType) args[0], (StellarSystem) args[1], (Vec3d) args[2]);
            case 34:
                return m66a((NPCType) args[0], (StellarSystem) args[1], (Vec3d) args[2], ((Boolean) args[3]).booleanValue(), (Collection<NPC>) (Collection) args[4]);
            case 35:
                m93b((aDJ) args[0]);
                return null;
            case 36:
                return m67a((String) args[0], (NPCType) args[1], (StellarSystem) args[2], (Vec3d) args[3]);
            case 37:
                return m64a((StellarSystem) args[0], (Vec3d) args[1], ((Float) args[2]).floatValue(), ((Float) args[3]).floatValue());
            case 38:
                return m68a((String) args[0], (NPCType) args[1], (StellarSystem) args[2], (Vec3d) args[3], ((Float) args[4]).floatValue());
            case 39:
                return m69a((String) args[0], (NPCType) args[1], (StellarSystem) args[2], (Vec3d) args[3], ((Boolean) args[4]).booleanValue());
            case 40:
                return m70a((String) args[0], (NPCType) args[1], (Vec3d) args[2], (String) args[3], (Vec3d) args[4], ((Long) args[5]).longValue(), ((Boolean) args[6]).booleanValue(), (StellarSystem) args[7], ((Boolean) args[8]).booleanValue());
            case 41:
                return m98c((String) args[0], (NPCType) args[1], (StellarSystem) args[2], (Vec3d) args[3], ((Boolean) args[4]).booleanValue());
            case 42:
                m81a((String) args[0], (Vec3d) args[1], (StellarSystem) args[2], ((Long) args[3]).longValue(), ((Boolean) args[4]).booleanValue(), ((Boolean) args[5]).booleanValue());
                return null;
            case 43:
                return m71a((Node) args[0], (Node) args[1], (List<Node>) (List) args[2]);
            case 44:
                m88aS((Actor) args[0]);
                return null;
            case 45:
                cdf();
                return null;
            case 46:
                m89aU((Actor) args[0]);
                return null;
            case 47:
                m116iY((String) args[0]);
                return null;
            case 48:
                return new Boolean(arx());
            case 49:
                return new Boolean(m120qP());
            case 50:
                return new Boolean(cdh());
            case 51:
                cdj();
                return null;
            case 52:
                return new Boolean(cdl());
            case 53:
                cdn();
                return null;
            case 54:
                return cdo();
            case 55:
                m95b((C4045yZ) args[0]);
                return null;
            case 56:
                return cdq();
            case 57:
                m113g((LDScriptingController) args[0]);
                return null;
            case 58:
                m77a((MissionTimer) args[0]);
                return null;
            case 59:
                m99c((MissionTrigger) args[0]);
                return null;
            case 60:
                return cds();
            case 61:
                m111fg();
                return null;
            case 62:
                cdu();
                return null;
            case 63:
                cdw();
                return null;
            case 64:
                cdy();
                return null;
            case 65:
                m85aG();
                return null;
            case 66:
                cdA();
                return null;
            case 67:
                cdC();
                return null;
            case 68:
                cdE();
                return null;
            case 69:
                cdG();
                return null;
            case 70:
                return aAH();
            case 71:
                return new Boolean(m62U((Character) args[0]));
            case 72:
                return new Boolean(m59S((Ship) args[0]));
            case 73:
                return cdI();
            case 74:
                return m52KX();
            case 75:
                return aad();
            case 76:
                m51F((Item) args[0]);
                return null;
            case 77:
                return new Boolean(m124t((NPC) args[0]));
            case 78:
                return bjE();
            case 79:
                return new Boolean(m109fS((String) args[0]));
            case 80:
                m101c((ItemType) args[0], ((Integer) args[1]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m85aG();
    }

    /* renamed from: aT */
    public void mo71aT(Actor cr) {
        switch (bFf().mo6893i(fPM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPM, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPM, new Object[]{cr}));
                break;
        }
        m88aS(cr);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: aV */
    public final void mo72aV(Actor cr) {
        switch (bFf().mo6893i(fPO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPO, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPO, new Object[]{cr}));
                break;
        }
        m89aU(cr);
    }

    @C5566aOg
    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m90aV(str);
    }

    public Collection<aDR> aae() {
        switch (bFf().mo6893i(f12x99c43db3)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, f12x99c43db3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f12x99c43db3, new Object[0]));
                break;
        }
        return aad();
    }

    @C5566aOg
    public void akK() {
        switch (bFf().mo6893i(byP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                break;
        }
        akJ();
    }

    @C5566aOg
    public void akM() {
        switch (bFf().mo6893i(byS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                break;
        }
        akL();
    }

    public String aqO() {
        switch (bFf().mo6893i(cpT)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cpT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpT, new Object[0]));
                break;
        }
        return aAH();
    }

    @ClientOnly
    public boolean ary() {
        switch (bFf().mo6893i(bYI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bYI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bYI, new Object[0]));
                break;
        }
        return arx();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Vec3d mo79b(StellarSystem jj, Vec3d ajr, float f, float f2) {
        switch (bFf().mo6893i(fPF)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, fPF, new Object[]{jj, ajr, new Float(f), new Float(f2)}));
            case 3:
                bFf().mo5606d(new aCE(this, fPF, new Object[]{jj, ajr, new Float(f), new Float(f2)}));
                break;
        }
        return m64a(jj, ajr, f, f2);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final NPC mo80b(NPCType aed, StellarSystem jj, Vec3d ajr) {
        switch (bFf().mo6893i(fPC)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fPC, new Object[]{aed, jj, ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, fPC, new Object[]{aed, jj, ajr}));
                break;
        }
        return m65a(aed, jj, ajr);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final NPC mo81b(String str, NPCType aed, StellarSystem jj, Vec3d ajr) {
        switch (bFf().mo6893i(fPE)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fPE, new Object[]{str, aed, jj, ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, fPE, new Object[]{str, aed, jj, ajr}));
                break;
        }
        return m67a(str, aed, jj, ajr);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final NPC mo82b(String str, NPCType aed, StellarSystem jj, Vec3d ajr, float f) {
        switch (bFf().mo6893i(fPG)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fPG, new Object[]{str, aed, jj, ajr, new Float(f)}));
            case 3:
                bFf().mo5606d(new aCE(this, fPG, new Object[]{str, aed, jj, ajr, new Float(f)}));
                break;
        }
        return m68a(str, aed, jj, ajr, f);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final NPC mo83b(String str, NPCType aed, StellarSystem jj, Vec3d ajr, boolean z) {
        switch (bFf().mo6893i(fPH)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fPH, new Object[]{str, aed, jj, ajr, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, fPH, new Object[]{str, aed, jj, ajr, new Boolean(z)}));
                break;
        }
        return m69a(str, aed, jj, ajr, z);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final NPC mo84b(String str, NPCType aed, Vec3d ajr, String str2, Vec3d ajr2, long j, boolean z, StellarSystem jj, boolean z2) {
        switch (bFf().mo6893i(fPI)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fPI, new Object[]{str, aed, ajr, str2, ajr2, new Long(j), new Boolean(z), jj, new Boolean(z2)}));
            case 3:
                bFf().mo5606d(new aCE(this, fPI, new Object[]{str, aed, ajr, str2, ajr2, new Long(j), new Boolean(z), jj, new Boolean(z2)}));
                break;
        }
        return m70a(str, aed, ajr, str2, ajr2, j, z, jj, z2);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo85b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m73a(cr, acm);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo86b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m74a(ahc, jCVar, i);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo87b(MissionTrigger aus) {
        switch (bFf().mo6893i(byO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                break;
        }
        m75a(aus);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo88b(Character acx, Loot ael) {
        switch (bFf().mo6893i(byE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                break;
        }
        m76a(acx, ael);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: b */
    public final void mo89b(MissionTimer apy) {
        switch (bFf().mo6893i(fPY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPY, new Object[]{apy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPY, new Object[]{apy}));
                break;
        }
        m77a(apy);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo90b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m78a(auq, aag, aag2);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo91b(String str, Station bf) {
        switch (bFf().mo6893i(byB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                break;
        }
        m80a(str, bf);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public final void mo92b(String str, Vec3d ajr, StellarSystem jj, long j, boolean z, boolean z2) {
        switch (bFf().mo6893i(fPK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPK, new Object[]{str, ajr, jj, new Long(j), new Boolean(z), new Boolean(z2)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPK, new Object[]{str, ajr, jj, new Long(j), new Boolean(z), new Boolean(z2)}));
                break;
        }
        m81a(str, ajr, jj, j, z, z2);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo93b(String str, Character acx) {
        switch (bFf().mo6893i(byC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                break;
        }
        m82a(str, acx);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo94b(String str, Ship fAVar) {
        switch (bFf().mo6893i(byF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                break;
        }
        m83a(str, fAVar);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo95b(String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(beG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                break;
        }
        m84a(str, fAVar, fAVar2);
    }

    public List<GatePass> bjF() {
        switch (bFf().mo6893i(dGc)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dGc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGc, new Object[0]));
                break;
        }
        return bjE();
    }

    @C5566aOg
    /* renamed from: bl */
    public boolean mo97bl(String str) {
        switch (bFf().mo6893i(beF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                break;
        }
        return m97bk(str);
    }

    /* renamed from: c */
    public final void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f13x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f13x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f13x13860637, new Object[]{adj}));
                break;
        }
        m93b(adj);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo99c(Loot ael) {
        switch (bFf().mo6893i(byD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                break;
        }
        m94b(ael);
    }

    @C5566aOg
    /* renamed from: c */
    public final void mo100c(T t) {
        switch (bFf().mo6893i(fPV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPV, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPV, new Object[]{t}));
                break;
        }
        m95b(t);
    }

    @C5566aOg
    /* renamed from: c */
    public final void mo101c(String str, long j, int i) {
        switch (bFf().mo6893i(fPA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPA, new Object[]{str, new Long(j), new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPA, new Object[]{str, new Long(j), new Integer(i)}));
                break;
        }
        m96b(str, j, i);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public final void cdB() {
        switch (bFf().mo6893i(fQe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQe, new Object[0]));
                break;
        }
        cdA();
    }

    @C5566aOg
    public void cdD() {
        switch (bFf().mo6893i(fQf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQf, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQf, new Object[0]));
                break;
        }
        cdC();
    }

    @C5566aOg
    public final void cdF() {
        switch (bFf().mo6893i(fQg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQg, new Object[0]));
                break;
        }
        cdE();
    }

    @C5566aOg
    public void cdH() {
        switch (bFf().mo6893i(fQh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQh, new Object[0]));
                break;
        }
        cdG();
    }

    /* access modifiers changed from: protected */
    public Collection<NPC> cdJ() {
        switch (bFf().mo6893i(fQk)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, fQk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fQk, new Object[0]));
                break;
        }
        return cdI();
    }

    public void cdg() {
        switch (bFf().mo6893i(fPN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPN, new Object[0]));
                break;
        }
        cdf();
    }

    public final boolean cdi() {
        switch (bFf().mo6893i(fPQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fPQ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fPQ, new Object[0]));
                break;
        }
        return cdh();
    }

    @C5566aOg
    @C6143ahL
    public final void cdk() {
        switch (bFf().mo6893i(fPR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPR, new Object[0]));
                break;
        }
        cdj();
    }

    public final boolean cdm() {
        switch (bFf().mo6893i(fPS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fPS, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fPS, new Object[0]));
                break;
        }
        return cdl();
    }

    public final T cdp() {
        switch (bFf().mo6893i(fPU)) {
            case 0:
                return null;
            case 2:
                return (C4045yZ) bFf().mo5606d(new aCE(this, fPU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fPU, new Object[0]));
                break;
        }
        return cdo();
    }

    public final C5426aIw cdr() {
        switch (bFf().mo6893i(fPW)) {
            case 0:
                return null;
            case 2:
                return (C5426aIw) bFf().mo5606d(new aCE(this, fPW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fPW, new Object[0]));
                break;
        }
        return cdq();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public Mission cdt() {
        switch (bFf().mo6893i(fQa)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, fQa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fQa, new Object[0]));
                break;
        }
        return cds();
    }

    @C5566aOg
    public void cdv() {
        switch (bFf().mo6893i(fQb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fQb, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fQb, new Object[0]));
                break;
        }
        cdu();
    }

    @C5566aOg
    /* renamed from: cr */
    public void mo115cr(String str) {
        switch (bFf().mo6893i(byQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                break;
        }
        m103cq(str);
    }

    @C5566aOg
    /* renamed from: d */
    public final void mo116d(MissionTrigger aus) {
        switch (bFf().mo6893i(fPZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPZ, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPZ, new Object[]{aus}));
                break;
        }
        m99c(aus);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo117d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m100c(auq, aag);
    }

    /* renamed from: d */
    public void mo118d(ItemType jCVar, int i) {
        switch (bFf().mo6893i(dGa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGa, new Object[]{jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGa, new Object[]{jCVar, new Integer(i)}));
                break;
        }
        m101c(jCVar, i);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo119d(String str, Ship fAVar) {
        switch (bFf().mo6893i(byG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                break;
        }
        m102c(str, fAVar);
    }

    public final void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m111fg();
    }

    @C5566aOg
    /* renamed from: e */
    public void mo121e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m105d(rPVar);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: f */
    public final void mo122f(I18NString i18NString, Object... objArr) {
        switch (bFf().mo6893i(fPz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPz, new Object[]{i18NString, objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPz, new Object[]{i18NString, objArr}));
                break;
        }
        m106e(i18NString, objArr);
    }

    /* renamed from: fT */
    public boolean mo123fT(String str) {
        switch (bFf().mo6893i(dFZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dFZ, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFZ, new Object[]{str}));
                break;
        }
        return m109fS(str);
    }

    @C5566aOg
    @C6143ahL
    public final void fail() {
        switch (bFf().mo6893i(fPT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPT, new Object[0]));
                break;
        }
        cdn();
    }

    @C5566aOg
    /* renamed from: h */
    public void mo125h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m112g(fAVar);
    }

    @C5566aOg
    /* renamed from: h */
    public final void mo126h(LDScriptingController lYVar) {
        switch (bFf().mo6893i(fPX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPX, new Object[]{lYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPX, new Object[]{lYVar}));
                break;
        }
        m113g(lYVar);
    }

    @C5566aOg
    /* renamed from: iX */
    public final void mo127iX(String str) {
        switch (bFf().mo6893i(fPB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPB, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPB, new Object[]{str}));
                break;
        }
        m115iW(str);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: iZ */
    public final void mo128iZ(String str) {
        switch (bFf().mo6893i(fPP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fPP, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fPP, new Object[]{str}));
                break;
        }
        m116iY(str);
    }

    public final boolean isActive() {
        switch (bFf().mo6893i(f11Lc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f11Lc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f11Lc, new Object[0]));
                break;
        }
        return m120qP();
    }

    @C5566aOg
    /* renamed from: j */
    public void mo130j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m114i(fAVar);
    }

    @C5566aOg
    /* renamed from: l */
    public void mo131l(Gate fFVar) {
        switch (bFf().mo6893i(byM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                break;
        }
        m117k(fFVar);
    }

    @C5566aOg
    /* renamed from: o */
    public void mo132o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m118n(fAVar);
    }

    @C5566aOg
    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m119p(bf);
    }

    @C5566aOg
    /* renamed from: s */
    public void mo134s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m121r(bf);
    }

    @C5566aOg
    /* renamed from: t */
    public void mo135t(String str, String str2) {
        switch (bFf().mo6893i(byH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                break;
        }
        m122s(str, str2);
    }

    @C5566aOg
    /* renamed from: u */
    public void mo136u(Station bf) {
        switch (bFf().mo6893i(byN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                break;
        }
        m123t(bf);
    }

    /* renamed from: u */
    public boolean mo137u(NPC auf) {
        switch (bFf().mo6893i(fQm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fQm, new Object[]{auf}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fQm, new Object[]{auf}));
                break;
        }
        return m124t(auf);
    }

    @C5566aOg
    /* renamed from: x */
    public void mo138x(Ship fAVar) {
        switch (bFf().mo6893i(byI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                break;
        }
        m125w(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m72a(C0015a.ACTIVE);
    }

    @C0064Am(aul = "de968ec55f130e72b96f9d15def5f960", aum = 0)
    /* renamed from: b */
    private void m93b(aDJ adj) {
        if (adj instanceof NPC) {
            cda().remove(adj);
            cdb().remove(adj);
        }
    }

    @C0064Am(aul = "25bb3599062881fe242cda990b0a7a31", aum = 0)
    /* renamed from: a */
    private Vec3d m64a(StellarSystem jj, Vec3d ajr, float f, float f2) {
        return ajr.mo9504d((Tuple3d) new Vec3d((Math.random() * 2.0d) - 1.0d, (Math.random() * 2.0d) - 1.0d, (Math.random() * 2.0d) - 1.0d).dfW().mo9476Z(Math.random() * ((double) f) * 0.9d));
    }

    @C0064Am(aul = "d4aa0e40b3aec0901bca9663a34c84c6", aum = 0)
    /* renamed from: a */
    private List<Gate> m71a(Node rPVar, Node rPVar2, List<Node> list) {
        List<Gate> b;
        List<Gate> list2 = null;
        if (rPVar == rPVar2) {
            return new ArrayList();
        }
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(rPVar);
        Set<Actor> Zw = rPVar.mo21624Zw();
        HashMap hashMap = new HashMap();
        for (Actor next : Zw) {
            if (next instanceof Gate) {
                Gate fFVar = (Gate) next;
                Gate rF = fFVar.mo18386rF();
                if (fFVar.isEnabled() && fFVar.bae() && fFVar.mo18393rw() == 0 && rF != null && !list.contains(rF.cLb()) && (b = m92b(rF.cLb(), rPVar2, (List<Node>) new ArrayList(list))) != null) {
                    b.add(0, fFVar);
                    hashMap.put(fFVar, b);
                }
            }
        }
        for (Gate fFVar2 : hashMap.keySet()) {
            List<Gate> list3 = (List) hashMap.get(fFVar2);
            if (list2 == null || list3.size() < list2.size()) {
                list2 = list3;
            }
        }
        return list2;
    }

    @C0064Am(aul = "abde149411375985d67658bc820a6183", aum = 0)
    /* renamed from: aS */
    private void m88aS(Actor cr) {
        m87aR(cr);
    }

    @C0064Am(aul = "0ff641d0e2e4c77af7b735163eaed500", aum = 0)
    private void cdf() {
        Actor cWW;
        if (ccZ() != null && (cWW = cdr().cWW()) != null) {
            Node cLb = cWW.cLb();
            Node cLb2 = ccZ().cLb();
            if (cLb != cLb2) {
                List<Gate> b = m92b(cLb, cLb2, (List<Node>) null);
                if (b != null && !b.isEmpty()) {
                    mo72aV((Actor) b.get(0));
                }
            } else if (!(ccZ() instanceof MissionTrigger)) {
                mo72aV(ccZ());
            } else {
                mo72aV((Actor) null);
            }
        }
    }

    @C0064Am(aul = "9a6e272ee2c5fa79c398b323cca6f4f6", aum = 0)
    @ClientOnly
    private boolean arx() {
        return true;
    }

    @C0064Am(aul = "f03b3344f583b160721d9f36a244a8e9", aum = 0)
    /* renamed from: qP */
    private boolean m120qP() {
        return abJ() == C0015a.ACTIVE;
    }

    @C0064Am(aul = "f64d02c5a1bad5736ec237586f514f33", aum = 0)
    private boolean cdh() {
        return abJ() == C0015a.ACCOMPLISHED;
    }

    @C0064Am(aul = "0f1497b66fc9ad8dcf012c00e026098e", aum = 0)
    private boolean cdl() {
        return abJ() == C0015a.FAILED;
    }

    @C0064Am(aul = "c444b0a2991e272629d157e77c8bb5fd", aum = 0)
    private T cdo() {
        if (ccV() == null) {
            mo8358lY("Mission with no MissionTemplate! Class = " + getClass().getName() + ", mission owner = " + cdr().getName());
        }
        return ccV();
    }

    @C0064Am(aul = "d76b25a54e186f806faa281bf6016190", aum = 0)
    private C5426aIw cdq() {
        return ccU();
    }

    @C0064Am(aul = "406b68b752f86993a6576ce04381295c", aum = 0)
    /* renamed from: fg */
    private void m111fg() {
        cdx();
    }

    @C0064Am(aul = "1d71cc8920ddbde79fca1ad5b8a5e4d5", aum = 0)
    /* renamed from: aG */
    private void m85aG() {
        super.mo70aH();
        LDScriptingController lYVar = (LDScriptingController) cdr();
        if (!cdd() && !lYVar.cWH().contains(this)) {
            mo8358lY("FOUND AN MISSION UNKNOWN TO ITS OWNER: " + cdp().getHandle() + " (" + bFY() + "). Owner is " + lYVar.cWF().getName() + ". Is this mission the active one ? " + (this == lYVar.cWL()));
            cdv();
            dispose();
        }
        if (abJ() == C0015a.ACTIVE) {
            cdH();
            if (cdd()) {
                cdz();
            }
        }
    }

    @C0064Am(aul = "92f401093da8bfba7c43aa3db2578aea", aum = 0)
    private String aAH() {
        return null;
    }

    @C0064Am(aul = "9bbbd4593d985500ad9a71e8d3784b3c", aum = 0)
    /* renamed from: U */
    private boolean m62U(Character acx) {
        return ccU().cWF() == acx;
    }

    @C0064Am(aul = "b63dda6967885c7daea3366e31043c18", aum = 0)
    /* renamed from: S */
    private boolean m59S(Ship fAVar) {
        for (NPC bQx : cda()) {
            if (bQx.bQx() == fAVar) {
                return true;
            }
        }
        for (NPC bQx2 : cdb()) {
            if (bQx2.bQx() == fAVar) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "846b548542c2f619df4f2568753119fe", aum = 0)
    private Collection<NPC> cdI() {
        return cda();
    }

    @C0064Am(aul = "ba987a822545cabf9095a5095832b276", aum = 0)
    /* renamed from: KX */
    private Player m52KX() {
        if (ccU() != null && (ccU().cWF() instanceof Player)) {
            return (Player) ccU().cWF();
        }
        if (ccU() == null) {
            mo6317hy("getOwner() is returning null because the field 'missionOwner' is null");
        } else if (!(ccU().cWF() instanceof Player)) {
            mo6317hy("getOwner() is returning null because the owner isn't a Player (actually it is '" + ccU().cWF().getClass().getName() + "')");
        }
        return null;
    }

    @C0064Am(aul = "019179d5e81f997edee826507eb38fe2", aum = 0)
    private Collection<aDR> aad() {
        if (cdr().bQx() == null) {
            return null;
        }
        return cdr().bQx().aae();
    }

    @C0064Am(aul = "0a54f67638df6c3a8ed65fe7bd90359d", aum = 0)
    /* renamed from: F */
    private void m51F(Item auq) {
        C2686iZ cdc = cdc();
        MissionItem qPVar = (MissionItem) bFf().mo6865M(MissionItem.class);
        qPVar.mo21347q(auq);
        cdc.add(qPVar);
    }

    @C0064Am(aul = "b82ee3ad43e89b5abaf4185e21f7a88f", aum = 0)
    /* renamed from: t */
    private boolean m124t(NPC auf) {
        return cda().contains(auf) || cdb().contains(auf);
    }

    @C0064Am(aul = "17eb3bce5ad2886f4bf1ef479600b705", aum = 0)
    private List<GatePass> bjE() {
        return null;
    }

    @C0064Am(aul = "ca60fb035ad416120dd5d3dd91743a88", aum = 0)
    /* renamed from: fS */
    private boolean m109fS(String str) {
        return true;
    }

    @C0064Am(aul = "658de7392e3a425de53857dbabd0b844", aum = 0)
    /* renamed from: c */
    private void m101c(ItemType jCVar, int i) {
    }

    /* renamed from: a.AF$a */
    public enum C0015a {
        ACTIVE,
        ACCOMPLISHED,
        FAILED
    }
}
