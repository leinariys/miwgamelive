package game.script.mission;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.aFM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.sG */
/* compiled from: a */
public class MissionObjective extends aDJ implements C1616Xf, C2780jt {

    /* renamed from: QA */
    public static final C5663aRz f9084QA = null;

    /* renamed from: QE */
    public static final C2491fm f9085QE = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bM */
    public static final C5663aRz f9086bM = null;
    /* renamed from: bP */
    public static final C2491fm f9087bP = null;
    public static final C5663aRz bic = null;
    public static final C5663aRz bid = null;
    public static final C5663aRz bif = null;
    public static final C5663aRz bih = null;
    public static final C2491fm bii = null;
    public static final C2491fm bij = null;
    public static final C2491fm bik = null;
    public static final C2491fm bil = null;
    public static final C2491fm bim = null;
    public static final C2491fm bin = null;
    public static final C2491fm bio = null;
    public static final C2491fm bip = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0cb5b1c207ffc72941a7ddd092454070", aum = 2)
    private static int bib;
    @C0064Am(aul = "9115da31458e82a71fb33abc06a40681", aum = 4)
    private static int bie;
    @C0064Am(aul = "34b2a844c230d7810f2ca211c9addf19", aum = 5)
    private static boolean big;
    @C0064Am(aul = "10419ca5e02bd57d7b6d58bb4ee9dd0b", aum = 0)
    private static String handle;
    @C0064Am(aul = "137dd39425db32f0b17947b81c33a826", aum = 1)

    /* renamed from: nh */
    private static I18NString f9088nh;
    @C0064Am(aul = "ac94a1405bbea8373f72d5d5bba76f30", aum = 3)

    /* renamed from: oi */
    private static Mission.C0015a f9089oi;

    static {
        m38684V();
    }

    public MissionObjective() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionObjective(C5540aNg ang) {
        super(ang);
    }

    public MissionObjective(String str, I18NString i18NString, int i, boolean z) {
        super((C5540aNg) null);
        super._m_script_init(str, i18NString, i, z);
    }

    /* renamed from: V */
    static void m38684V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 6;
        _m_methodCount = aDJ._m_methodCount + 10;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(MissionObjective.class, "10419ca5e02bd57d7b6d58bb4ee9dd0b", i);
        f9086bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionObjective.class, "137dd39425db32f0b17947b81c33a826", i2);
        f9084QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionObjective.class, "0cb5b1c207ffc72941a7ddd092454070", i3);
        bic = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionObjective.class, "ac94a1405bbea8373f72d5d5bba76f30", i4);
        bid = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MissionObjective.class, "9115da31458e82a71fb33abc06a40681", i5);
        bif = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(MissionObjective.class, "34b2a844c230d7810f2ca211c9addf19", i6);
        bih = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i8 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 10)];
        C2491fm a = C4105zY.m41624a(MissionObjective.class, "e00ad8033e2c464ad1ecbd43047269f2", i8);
        bii = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionObjective.class, "81e65619c1801b0d3faf7ccce1eb80d4", i9);
        f9087bP = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionObjective.class, "d3e0830b6823eefe4bb823035aca73da", i10);
        bij = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionObjective.class, "f2e16ced2b4eeeb63d67715c7a5e19b0", i11);
        bik = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionObjective.class, "5b5bda3273dcb0f773d7f5bd409dea6b", i12);
        bil = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionObjective.class, "88abfebac4d4ab58c9eef55d5587edfb", i13);
        bim = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionObjective.class, "d55867d148686da086bdbf1dc68bf720", i14);
        f9085QE = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionObjective.class, "c6e92fc2ffbd5f0ca01e2bfc9e9b3d2f", i15);
        bin = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionObjective.class, "ce44f3a4237a6b2b4fe6566e375e22f6", i16);
        bio = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionObjective.class, "9a580e761a58d11acc2c65497275358e", i17);
        bip = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionObjective.class, aFM.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m38685a(Mission.C0015a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    /* renamed from: a */
    private void m38686a(String str) {
        bFf().mo5608dq().mo3197f(f9086bM, str);
    }

    /* renamed from: aY */
    private void m38687aY(boolean z) {
        bFf().mo5608dq().mo3153a(bih, z);
    }

    private int abI() {
        return bFf().mo5608dq().mo3212n(bic);
    }

    private Mission.C0015a abJ() {
        return (Mission.C0015a) bFf().mo5608dq().mo3214p(bid);
    }

    private int abK() {
        return bFf().mo5608dq().mo3212n(bif);
    }

    private boolean abL() {
        return bFf().mo5608dq().mo3201h(bih);
    }

    /* renamed from: ao */
    private String m38689ao() {
        return (String) bFf().mo5608dq().mo3214p(f9086bM);
    }

    /* renamed from: br */
    private void m38692br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9084QA, i18NString);
    }

    /* renamed from: ex */
    private void m38693ex(int i) {
        bFf().mo5608dq().mo3183b(bic, i);
    }

    /* renamed from: ey */
    private void m38694ey(int i) {
        bFf().mo5608dq().mo3183b(bif, i);
    }

    /* renamed from: vT */
    private I18NString m38696vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9084QA);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aFM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m38691b((Mission.C0015a) args[0]);
                return null;
            case 1:
                return m38690ar();
            case 2:
                return abM();
            case 3:
                return new Integer(abO());
            case 4:
                m38695ez(((Integer) args[0]).intValue());
                return null;
            case 5:
                return new Integer(abQ());
            case 6:
                return m38697vV();
            case 7:
                return new Boolean(abS());
            case 8:
                m38688aZ(((Boolean) args[0]).booleanValue());
                return null;
            case 9:
                return abT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Mission.C0015a abN() {
        switch (bFf().mo6893i(bij)) {
            case 0:
                return null;
            case 2:
                return (Mission.C0015a) bFf().mo5606d(new aCE(this, bij, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bij, new Object[0]));
                break;
        }
        return abM();
    }

    public int abP() {
        switch (bFf().mo6893i(bik)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bik, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bik, new Object[0]));
                break;
        }
        return abO();
    }

    public int abR() {
        switch (bFf().mo6893i(bim)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bim, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bim, new Object[0]));
                break;
        }
        return abQ();
    }

    /* renamed from: c */
    public void mo21870c(Mission.C0015a aVar) {
        switch (bFf().mo6893i(bii)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bii, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bii, new Object[]{aVar}));
                break;
        }
        m38691b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: eA */
    public void mo21871eA(int i) {
        switch (bFf().mo6893i(bil)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bil, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bil, new Object[]{new Integer(i)}));
                break;
        }
        m38695ez(i);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f9087bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9087bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9087bP, new Object[0]));
                break;
        }
        return m38690ar();
    }

    public String getInfo() {
        switch (bFf().mo6893i(bip)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bip, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bip, new Object[0]));
                break;
        }
        return abT();
    }

    public boolean isHidden() {
        switch (bFf().mo6893i(bin)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bin, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bin, new Object[0]));
                break;
        }
        return abS();
    }

    public void setHidden(boolean z) {
        switch (bFf().mo6893i(bio)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                break;
        }
        m38688aZ(z);
    }

    /* renamed from: vW */
    public I18NString mo4247vW() {
        switch (bFf().mo6893i(f9085QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9085QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9085QE, new Object[0]));
                break;
        }
        return m38697vV();
    }

    /* renamed from: a */
    public void mo21866a(String str, I18NString i18NString, int i, boolean z) {
        super.mo10S();
        m38686a(str);
        m38692br(i18NString);
        m38693ex(i);
        m38687aY(z);
    }

    @C0064Am(aul = "e00ad8033e2c464ad1ecbd43047269f2", aum = 0)
    /* renamed from: b */
    private void m38691b(Mission.C0015a aVar) {
        m38685a(aVar);
    }

    @C0064Am(aul = "81e65619c1801b0d3faf7ccce1eb80d4", aum = 0)
    /* renamed from: ar */
    private String m38690ar() {
        return m38689ao();
    }

    @C0064Am(aul = "d3e0830b6823eefe4bb823035aca73da", aum = 0)
    private Mission.C0015a abM() {
        return abJ();
    }

    @C0064Am(aul = "f2e16ced2b4eeeb63d67715c7a5e19b0", aum = 0)
    private int abO() {
        return abK();
    }

    @C0064Am(aul = "5b5bda3273dcb0f773d7f5bd409dea6b", aum = 0)
    /* renamed from: ez */
    private void m38695ez(int i) {
        m38694ey(i);
    }

    @C0064Am(aul = "88abfebac4d4ab58c9eef55d5587edfb", aum = 0)
    private int abQ() {
        return abI();
    }

    @C0064Am(aul = "d55867d148686da086bdbf1dc68bf720", aum = 0)
    /* renamed from: vV */
    private I18NString m38697vV() {
        return m38696vT();
    }

    @C0064Am(aul = "c6e92fc2ffbd5f0ca01e2bfc9e9b3d2f", aum = 0)
    private boolean abS() {
        return abL();
    }

    @C0064Am(aul = "ce44f3a4237a6b2b4fe6566e375e22f6", aum = 0)
    /* renamed from: aZ */
    private void m38688aZ(boolean z) {
        m38687aY(z);
    }

    @C0064Am(aul = "9a580e761a58d11acc2c65497275358e", aum = 0)
    private String abT() {
        return String.valueOf(String.valueOf(abK())) + C0147Bi.SEPARATOR + String.valueOf(abI());
    }
}
