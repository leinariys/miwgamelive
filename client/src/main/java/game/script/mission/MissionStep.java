package game.script.mission;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2086bs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aaR  reason: case insensitive filesystem */
/* compiled from: a */
public class MissionStep extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bM */
    public static final C5663aRz f4065bM = null;
    /* renamed from: bP */
    public static final C2491fm f4066bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4067bQ = null;
    public static final C5663aRz bid = null;
    public static final C2491fm bii = null;
    public static final C2491fm bij = null;
    public static final C5663aRz eXh = null;
    public static final C2491fm eXi = null;
    public static final C2491fm eXj = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0fc3adc13d38013aef690f39a7d19e0f", aum = 0)
    private static String handle;
    @C0064Am(aul = "e6aaf20d811c49dbb635da0ff7a67dde", aum = 1)

    /* renamed from: oi */
    private static Mission.C0015a f4068oi;
    @C0064Am(aul = "c7d42a041f20b11297999fd79456f449", aum = 2)

    /* renamed from: oj */
    private static Object[] f4069oj;

    static {
        m19471V();
    }

    public MissionStep() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionStep(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19471V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 6;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(MissionStep.class, "0fc3adc13d38013aef690f39a7d19e0f", i);
        f4065bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionStep.class, "e6aaf20d811c49dbb635da0ff7a67dde", i2);
        bid = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionStep.class, "c7d42a041f20b11297999fd79456f449", i3);
        eXh = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(MissionStep.class, "a8c065e3acb1f8b3ad033fb542a607be", i5);
        f4067bQ = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionStep.class, "a2b1ef99f223d5e7f378527db259a00b", i6);
        f4066bP = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionStep.class, "0bd15c135a919b3d77d4b6d3f1901303", i7);
        bij = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionStep.class, "ccda24efefd7f3984ba87c8dec81d57d", i8);
        bii = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionStep.class, "5466d3fc1988791384c33a476e227ac3", i9);
        eXi = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionStep.class, "8cc2d2be516c8b1250c1c0d3d846e8ae", i10);
        eXj = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionStep.class, C2086bs.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m19472a(Mission.C0015a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    /* renamed from: a */
    private void m19473a(String str) {
        bFf().mo5608dq().mo3197f(f4065bM, str);
    }

    private Mission.C0015a abJ() {
        return (Mission.C0015a) bFf().mo5608dq().mo3214p(bid);
    }

    /* renamed from: ao */
    private String m19474ao() {
        return (String) bFf().mo5608dq().mo3214p(f4065bM);
    }

    private Object[] bMP() {
        return (Object[]) bFf().mo5608dq().mo3214p(eXh);
    }

    /* renamed from: r */
    private void m19478r(Object[] objArr) {
        bFf().mo5608dq().mo3197f(eXh, objArr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2086bs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m19477b((String) args[0]);
                return null;
            case 1:
                return m19475ar();
            case 2:
                return abM();
            case 3:
                m19476b((Mission.C0015a) args[0]);
                return null;
            case 4:
                return bMQ();
            case 5:
                m19479s((Object[]) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Mission.C0015a abN() {
        switch (bFf().mo6893i(bij)) {
            case 0:
                return null;
            case 2:
                return (Mission.C0015a) bFf().mo5606d(new aCE(this, bij, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bij, new Object[0]));
                break;
        }
        return abM();
    }

    /* renamed from: c */
    public void mo12205c(Mission.C0015a aVar) {
        switch (bFf().mo6893i(bii)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bii, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bii, new Object[]{aVar}));
                break;
        }
        m19476b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Object[] getData() {
        switch (bFf().mo6893i(eXi)) {
            case 0:
                return null;
            case 2:
                return (Object[]) bFf().mo5606d(new aCE(this, eXi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eXi, new Object[0]));
                break;
        }
        return bMQ();
    }

    public void setData(Object[] objArr) {
        switch (bFf().mo6893i(eXj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eXj, new Object[]{objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eXj, new Object[]{objArr}));
                break;
        }
        m19479s(objArr);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4066bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4066bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4066bP, new Object[0]));
                break;
        }
        return m19475ar();
    }

    public void setHandle(String str) {
        switch (bFf().mo6893i(f4067bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4067bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4067bQ, new Object[]{str}));
                break;
        }
        m19477b(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a8c065e3acb1f8b3ad033fb542a607be", aum = 0)
    /* renamed from: b */
    private void m19477b(String str) {
        m19473a(str);
    }

    @C0064Am(aul = "a2b1ef99f223d5e7f378527db259a00b", aum = 0)
    /* renamed from: ar */
    private String m19475ar() {
        return m19474ao();
    }

    @C0064Am(aul = "0bd15c135a919b3d77d4b6d3f1901303", aum = 0)
    private Mission.C0015a abM() {
        return abJ();
    }

    @C0064Am(aul = "ccda24efefd7f3984ba87c8dec81d57d", aum = 0)
    /* renamed from: b */
    private void m19476b(Mission.C0015a aVar) {
        m19472a(aVar);
    }

    @C0064Am(aul = "5466d3fc1988791384c33a476e227ac3", aum = 0)
    private Object[] bMQ() {
        return bMP();
    }

    @C0064Am(aul = "8cc2d2be516c8b1250c1c0d3d846e8ae", aum = 0)
    /* renamed from: s */
    private void m19479s(Object[] objArr) {
        m19478r(objArr);
    }
}
