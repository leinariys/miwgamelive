package game.script.mission;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.agent.AgentType;
import game.script.npc.NPCType;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C1756Zw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aVh  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class MissionTemplate extends TaikodomObject implements C0468GU, C1616Xf, C4045yZ {
    /* renamed from: BP */
    public static final C5663aRz f3950BP = null;
    /* renamed from: Cq */
    public static final C2491fm f3951Cq = null;
    /* renamed from: Cr */
    public static final C2491fm f3952Cr = null;
    /* renamed from: Dg */
    public static final C5663aRz f3954Dg = null;
    /* renamed from: Dl */
    public static final C2491fm f3955Dl = null;
    /* renamed from: Dm */
    public static final C2491fm f3956Dm = null;
    /* renamed from: MN */
    public static final C2491fm f3958MN = null;
    /* renamed from: QA */
    public static final C5663aRz f3959QA = null;
    /* renamed from: QD */
    public static final C2491fm f3960QD = null;
    /* renamed from: QE */
    public static final C2491fm f3961QE = null;
    /* renamed from: QF */
    public static final C2491fm f3962QF = null;
    /* renamed from: Qz */
    public static final C5663aRz f3963Qz = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f3965bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3966bM = null;
    /* renamed from: bN */
    public static final C2491fm f3967bN = null;
    /* renamed from: bO */
    public static final C2491fm f3968bO = null;
    /* renamed from: bP */
    public static final C2491fm f3969bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3970bQ = null;
    public static final C5663aRz bfP = null;
    public static final C2491fm cpR = null;
    public static final C2491fm cpS = null;
    public static final C2491fm cpT = null;
    public static final C2491fm cpU = null;
    public static final C2491fm cpV = null;
    public static final C2491fm cpW = null;
    public static final C2491fm cpX = null;
    public static final C2491fm cpY = null;
    public static final C2491fm cpy = null;
    public static final C2491fm cqa = null;
    public static final C2491fm cqb = null;
    public static final C2491fm cqc = null;
    public static final C5663aRz dPh = null;
    public static final C2491fm dPi = null;
    public static final C5663aRz iYA = null;
    public static final C5663aRz iYB = null;
    public static final C5663aRz iYC = null;
    public static final C5663aRz iYD = null;
    public static final C5663aRz iYE = null;
    public static final C5663aRz iYF = null;
    public static final C5663aRz iYG = null;
    public static final C5663aRz iYH = null;
    public static final C5663aRz iYI = null;
    public static final C5663aRz iYJ = null;
    public static final C2491fm iYK = null;
    public static final C2491fm iYL = null;
    public static final C2491fm iYM = null;
    public static final C2491fm iYN = null;
    public static final C2491fm iYO = null;
    public static final C2491fm iYP = null;
    public static final C2491fm iYQ = null;
    public static final C2491fm iYR = null;
    public static final C2491fm iYS = null;
    public static final C2491fm iYT = null;
    public static final C2491fm iYU = null;
    public static final C2491fm iYV = null;
    public static final C2491fm iYW = null;
    public static final C2491fm iYX = null;
    public static final C2491fm iYY = null;
    public static final C2491fm iYZ = null;
    public static final C5663aRz iYz = null;
    public static final C2491fm iZa = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e8bb52dcbe49f51b58c4110be1d08267", aum = 15)

    /* renamed from: BO */
    private static int f3949BO;
    @C0064Am(aul = "635f871eb8f677762484a185dd6c8e6f", aum = 11)

    /* renamed from: Df */
    private static boolean f3953Df;
    @C0064Am(aul = "16bfed488d94be91915de8cd37ca1a18", aum = 8)

    /* renamed from: E */
    private static long f3957E;
    @C0064Am(aul = "07d6f56499cb8ec9287c795e47c8051c", aum = 18)

    /* renamed from: bK */
    private static UUID f3964bK;
    @C0064Am(aul = "e95eabb42293c2f61e67ba06e48ba0b4", aum = 17)
    private static Asset bfO;
    @C0064Am(aul = "07229a7365ab7eab75a00de9cf42fe35", aum = 0)
    @Deprecated
    private static NPCType eOh;
    @C0064Am(aul = "af6fe1b05998f9551b99f633302fd75a", aum = 1)
    private static AgentType eOi;
    @C0064Am(aul = "e1c851e2b1c3f1e74553b1a1b50b875a", aum = 5)
    private static I18NString eOj;
    @C0064Am(aul = "b28fbdeb613584b709a0bdb2ef2ace34", aum = 6)
    private static boolean eOk;
    @C0064Am(aul = "2c9b032b586e63862af93939dc8c78d7", aum = 7)
    private static I18NString eOl;
    @C0064Am(aul = "93d9b483a38cdf232454a48d498ae4b3", aum = 9)
    private static C3438ra<ItemTypeTableItem> eOm;
    @C0064Am(aul = "54db8f2fa01eea0857e3bb77497402a1", aum = 10)
    private static C3438ra<ItemTypeTableItem> eOn;
    @C0064Am(aul = "e3a46ee3f04e1aaaf2994964834e4ef0", aum = 12)
    private static float eOo;
    @C0064Am(aul = "40a1070b4cdab4c39e36d02fbb854307", aum = 13)
    private static C4045yZ.C4046a eOp;
    @C0064Am(aul = "4dc6c2401bfd536e4ba8fa188f78ba78", aum = 14)
    private static int eOq;
    @C0064Am(aul = "506d50fdcd306ff559c3a215a2fbdfb5", aum = 16)
    private static boolean eOr;
    @C0064Am(aul = "5c863d31d77047b824bfa91c51b93b82", aum = 2)
    private static String handle;
    @C0064Am(aul = "ccaefa7be9b5e7a29b4f7e4c70c8783f", aum = 4)

    /* renamed from: nh */
    private static I18NString f3971nh;
    @C0064Am(aul = "bd5d2d7b177f325d9f7fbd95b43cdcda", aum = 3)

    /* renamed from: ni */
    private static I18NString f3972ni;

    static {
        m18962V();
    }

    public MissionTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18962V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 19;
        _m_methodCount = TaikodomObject._m_methodCount + 44;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 19)];
        C5663aRz b = C5640aRc.m17844b(MissionTemplate.class, "07229a7365ab7eab75a00de9cf42fe35", i);
        iYz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionTemplate.class, "af6fe1b05998f9551b99f633302fd75a", i2);
        iYA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionTemplate.class, "5c863d31d77047b824bfa91c51b93b82", i3);
        f3966bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionTemplate.class, "bd5d2d7b177f325d9f7fbd95b43cdcda", i4);
        f3963Qz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MissionTemplate.class, "ccaefa7be9b5e7a29b4f7e4c70c8783f", i5);
        f3959QA = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(MissionTemplate.class, "e1c851e2b1c3f1e74553b1a1b50b875a", i6);
        iYB = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(MissionTemplate.class, "b28fbdeb613584b709a0bdb2ef2ace34", i7);
        iYC = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(MissionTemplate.class, "2c9b032b586e63862af93939dc8c78d7", i8);
        iYD = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(MissionTemplate.class, "16bfed488d94be91915de8cd37ca1a18", i9);
        dPh = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(MissionTemplate.class, "93d9b483a38cdf232454a48d498ae4b3", i10);
        iYE = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(MissionTemplate.class, "54db8f2fa01eea0857e3bb77497402a1", i11);
        iYF = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(MissionTemplate.class, "635f871eb8f677762484a185dd6c8e6f", i12);
        f3954Dg = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(MissionTemplate.class, "e3a46ee3f04e1aaaf2994964834e4ef0", i13);
        iYG = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(MissionTemplate.class, "40a1070b4cdab4c39e36d02fbb854307", i14);
        iYH = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(MissionTemplate.class, "4dc6c2401bfd536e4ba8fa188f78ba78", i15);
        iYI = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(MissionTemplate.class, "e8bb52dcbe49f51b58c4110be1d08267", i16);
        f3950BP = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(MissionTemplate.class, "506d50fdcd306ff559c3a215a2fbdfb5", i17);
        iYJ = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(MissionTemplate.class, "e95eabb42293c2f61e67ba06e48ba0b4", i18);
        bfP = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(MissionTemplate.class, "07d6f56499cb8ec9287c795e47c8051c", i19);
        f3965bL = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i21 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i21 + 44)];
        C2491fm a = C4105zY.m41624a(MissionTemplate.class, "4fc3971abb1b544339e9c51005f9bf3c", i21);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i21] = a;
        int i22 = i21 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionTemplate.class, "14391230c8edfa9668a988fef674b8e6", i22);
        f3967bN = a2;
        fmVarArr[i22] = a2;
        int i23 = i22 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionTemplate.class, "770dfff047fa78c0ecec4f123f4564d3", i23);
        f3968bO = a3;
        fmVarArr[i23] = a3;
        int i24 = i23 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionTemplate.class, "ea95bdb1ea1e3dbf57c4e8f09a6193bd", i24);
        f3961QE = a4;
        fmVarArr[i24] = a4;
        int i25 = i24 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionTemplate.class, "083f14b872be6a0b0719d2f055797152", i25);
        f3962QF = a5;
        fmVarArr[i25] = a5;
        int i26 = i25 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionTemplate.class, "8abca9dd7f7bcffed7609b5747cfee14", i26);
        iYK = a6;
        fmVarArr[i26] = a6;
        int i27 = i26 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionTemplate.class, "8e291c02f5bf37f81ed692bfda4fcd11", i27);
        cpY = a7;
        fmVarArr[i27] = a7;
        int i28 = i27 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionTemplate.class, "ed7ae862213355d10954fc1d36092445", i28);
        cpy = a8;
        fmVarArr[i28] = a8;
        int i29 = i28 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionTemplate.class, "1783b37fba1cb25bad7c2c8bf2dfce04", i29);
        iYL = a9;
        fmVarArr[i29] = a9;
        int i30 = i29 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionTemplate.class, "d6f002cc34642b1609fb76eb644f8b4b", i30);
        iYM = a10;
        fmVarArr[i30] = a10;
        int i31 = i30 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionTemplate.class, "098d84a69184947e1f43f95eb33bb4af", i31);
        cpX = a11;
        fmVarArr[i31] = a11;
        int i32 = i31 + 1;
        C2491fm a12 = C4105zY.m41624a(MissionTemplate.class, "91d651405d8524d650d1c87b563d5671", i32);
        f3958MN = a12;
        fmVarArr[i32] = a12;
        int i33 = i32 + 1;
        C2491fm a13 = C4105zY.m41624a(MissionTemplate.class, "81c093fdee150bf23ada55764c7b4318", i33);
        f3960QD = a13;
        fmVarArr[i33] = a13;
        int i34 = i33 + 1;
        C2491fm a14 = C4105zY.m41624a(MissionTemplate.class, "9976e2e53db7ebc166b81383dd545102", i34);
        f3969bP = a14;
        fmVarArr[i34] = a14;
        int i35 = i34 + 1;
        C2491fm a15 = C4105zY.m41624a(MissionTemplate.class, "fd53f2b44946934c501b7c17955a6d62", i35);
        f3970bQ = a15;
        fmVarArr[i35] = a15;
        int i36 = i35 + 1;
        C2491fm a16 = C4105zY.m41624a(MissionTemplate.class, "93e4a66deeb5ee2bedb1a949337e8c78", i36);
        cpW = a16;
        fmVarArr[i36] = a16;
        int i37 = i36 + 1;
        C2491fm a17 = C4105zY.m41624a(MissionTemplate.class, "b2a44c695bfc982a3b81945dbaa739c2", i37);
        dPi = a17;
        fmVarArr[i37] = a17;
        int i38 = i37 + 1;
        C2491fm a18 = C4105zY.m41624a(MissionTemplate.class, "e93555343c3bb5a79f3766e705ee2322", i38);
        iYN = a18;
        fmVarArr[i38] = a18;
        int i39 = i38 + 1;
        C2491fm a19 = C4105zY.m41624a(MissionTemplate.class, "92f8b76ae406f61473b15f36cc14524e", i39);
        iYO = a19;
        fmVarArr[i39] = a19;
        int i40 = i39 + 1;
        C2491fm a20 = C4105zY.m41624a(MissionTemplate.class, "3a0bb02207f5d9fa65ca5c19f5cef92d", i40);
        cpS = a20;
        fmVarArr[i40] = a20;
        int i41 = i40 + 1;
        C2491fm a21 = C4105zY.m41624a(MissionTemplate.class, "825ed975138e7835b28bd499bf65aba7", i41);
        cpT = a21;
        fmVarArr[i41] = a21;
        int i42 = i41 + 1;
        C2491fm a22 = C4105zY.m41624a(MissionTemplate.class, "c9a3589a3da1ee6252aad276d187e0d9", i42);
        iYP = a22;
        fmVarArr[i42] = a22;
        int i43 = i42 + 1;
        C2491fm a23 = C4105zY.m41624a(MissionTemplate.class, "5b727e74d803e07309b9c5882db5068e", i43);
        iYQ = a23;
        fmVarArr[i43] = a23;
        int i44 = i43 + 1;
        C2491fm a24 = C4105zY.m41624a(MissionTemplate.class, "eb7c681092d62454d45a7900e6e9b717", i44);
        f3955Dl = a24;
        fmVarArr[i44] = a24;
        int i45 = i44 + 1;
        C2491fm a25 = C4105zY.m41624a(MissionTemplate.class, "e32cc0e193a40d2914d028ed417d5b49", i45);
        f3956Dm = a25;
        fmVarArr[i45] = a25;
        int i46 = i45 + 1;
        C2491fm a26 = C4105zY.m41624a(MissionTemplate.class, "62ec3eb3c072997d96e146dfd33619ee", i46);
        cpR = a26;
        fmVarArr[i46] = a26;
        int i47 = i46 + 1;
        C2491fm a27 = C4105zY.m41624a(MissionTemplate.class, "c14de0a3210757db8c10903e65cd6e47", i47);
        iYR = a27;
        fmVarArr[i47] = a27;
        int i48 = i47 + 1;
        C2491fm a28 = C4105zY.m41624a(MissionTemplate.class, "5611835cb2ba01a754e0fe6263d067f2", i48);
        iYS = a28;
        fmVarArr[i48] = a28;
        int i49 = i48 + 1;
        C2491fm a29 = C4105zY.m41624a(MissionTemplate.class, "6cc459a05dbb25af05b0318a9800dadf", i49);
        iYT = a29;
        fmVarArr[i49] = a29;
        int i50 = i49 + 1;
        C2491fm a30 = C4105zY.m41624a(MissionTemplate.class, "8f99b64584fbc3712036cd88d1c4dbb7", i50);
        cpU = a30;
        fmVarArr[i50] = a30;
        int i51 = i50 + 1;
        C2491fm a31 = C4105zY.m41624a(MissionTemplate.class, "17f7df88814e4d31f7652359a3954c1d", i51);
        iYU = a31;
        fmVarArr[i51] = a31;
        int i52 = i51 + 1;
        C2491fm a32 = C4105zY.m41624a(MissionTemplate.class, "819ee753cdfd8340d0096bbb2cfbba9e", i52);
        iYV = a32;
        fmVarArr[i52] = a32;
        int i53 = i52 + 1;
        C2491fm a33 = C4105zY.m41624a(MissionTemplate.class, "0ebd6d36f44af011c414814fca49bf4e", i53);
        cpV = a33;
        fmVarArr[i53] = a33;
        int i54 = i53 + 1;
        C2491fm a34 = C4105zY.m41624a(MissionTemplate.class, "9fee3ec71de7e5ef98cfbc1153beec63", i54);
        cqb = a34;
        fmVarArr[i54] = a34;
        int i55 = i54 + 1;
        C2491fm a35 = C4105zY.m41624a(MissionTemplate.class, "850f5f5baa5a1cb41a5044b24187a705", i55);
        iYW = a35;
        fmVarArr[i55] = a35;
        int i56 = i55 + 1;
        C2491fm a36 = C4105zY.m41624a(MissionTemplate.class, "aa6d19585cb24d1a13dbd05779c9d50f", i56);
        cqa = a36;
        fmVarArr[i56] = a36;
        int i57 = i56 + 1;
        C2491fm a37 = C4105zY.m41624a(MissionTemplate.class, "0bca1f239c553ac6a9ebab22e88d5746", i57);
        iYX = a37;
        fmVarArr[i57] = a37;
        int i58 = i57 + 1;
        C2491fm a38 = C4105zY.m41624a(MissionTemplate.class, "913e44bbba5a04ed7dc31e0da2acae95", i58);
        f3951Cq = a38;
        fmVarArr[i58] = a38;
        int i59 = i58 + 1;
        C2491fm a39 = C4105zY.m41624a(MissionTemplate.class, "a655a24e6d57be0229f59c5a4a38a60e", i59);
        f3952Cr = a39;
        fmVarArr[i59] = a39;
        int i60 = i59 + 1;
        C2491fm a40 = C4105zY.m41624a(MissionTemplate.class, "9ee5f932d9d93c7b6e2449886c09f127", i60);
        cqc = a40;
        fmVarArr[i60] = a40;
        int i61 = i60 + 1;
        C2491fm a41 = C4105zY.m41624a(MissionTemplate.class, "7bd0a7c7bc87a25c48aa738a41c52aa2", i61);
        iYY = a41;
        fmVarArr[i61] = a41;
        int i62 = i61 + 1;
        C2491fm a42 = C4105zY.m41624a(MissionTemplate.class, "326d1bd4b232bcdc09961bf03900b59b", i62);
        _f_onResurrect_0020_0028_0029V = a42;
        fmVarArr[i62] = a42;
        int i63 = i62 + 1;
        C2491fm a43 = C4105zY.m41624a(MissionTemplate.class, "f6dca1aa11b9f4d7cb9e9ebacfda4d0d", i63);
        iYZ = a43;
        fmVarArr[i63] = a43;
        int i64 = i63 + 1;
        C2491fm a44 = C4105zY.m41624a(MissionTemplate.class, "4a0f09abda7e440679d174b68aa21e82", i64);
        iZa = a44;
        fmVarArr[i64] = a44;
        int i65 = i64 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionTemplate.class, C1756Zw.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Giver NPC (old)")
    @C0064Am(aul = "92f8b76ae406f61473b15f36cc14524e", aum = 0)
    @C5566aOg
    /* renamed from: A */
    private void m18956A(NPCType aed) {
        throw new aWi(new aCE(this, iYO, new Object[]{aed}));
    }

    /* renamed from: B */
    private void m18957B(boolean z) {
        bFf().mo5608dq().mo3153a(f3954Dg, z);
    }

    /* renamed from: Bc */
    private void m18958Bc(int i) {
        bFf().mo5608dq().mo3183b(iYI, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Merit Points")
    @C0064Am(aul = "850f5f5baa5a1cb41a5044b24187a705", aum = 0)
    @C5566aOg
    /* renamed from: Bd */
    private void m18959Bd(int i) {
        throw new aWi(new aCE(this, iYW, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent")
    @C0064Am(aul = "e32cc0e193a40d2914d028ed417d5b49", aum = 0)
    @C5566aOg
    /* renamed from: C */
    private void m18960C(boolean z) {
        throw new aWi(new aCE(this, f3956Dm, new Object[]{new Boolean(z)}));
    }

    /* renamed from: S */
    private void m18961S(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bfP, tCVar);
    }

    /* renamed from: a */
    private void m18963a(C4045yZ.C4046a aVar) {
        bFf().mo5608dq().mo3197f(iYH, aVar);
    }

    /* renamed from: a */
    private void m18964a(String str) {
        bFf().mo5608dq().mo3197f(f3966bM, str);
    }

    /* renamed from: a */
    private void m18965a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3965bL, uuid);
    }

    private Asset abq() {
        return (Asset) bFf().mo5608dq().mo3214p(bfP);
    }

    /* renamed from: an */
    private UUID m18967an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3965bL);
    }

    /* renamed from: ao */
    private String m18968ao() {
        return (String) bFf().mo5608dq().mo3214p(f3966bM);
    }

    /* renamed from: ar */
    private void m18971ar(int i) {
        bFf().mo5608dq().mo3183b(f3950BP, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Challenge Rating")
    @C0064Am(aul = "a655a24e6d57be0229f59c5a4a38a60e", aum = 0)
    @C5566aOg
    /* renamed from: ax */
    private void m18973ax(int i) {
        throw new aWi(new aCE(this, f3952Cr, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MissionCategory")
    @C0064Am(aul = "0bca1f239c553ac6a9ebab22e88d5746", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m18974b(C4045yZ.C4046a aVar) {
        throw new aWi(new aCE(this, iYX, new Object[]{aVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "fd53f2b44946934c501b7c17955a6d62", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m18975b(String str) {
        throw new aWi(new aCE(this, f3970bQ, new Object[]{str}));
    }

    private long bnb() {
        return bFf().mo5608dq().mo3213o(dPh);
    }

    /* renamed from: bq */
    private void m18977bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3963Qz, i18NString);
    }

    /* renamed from: br */
    private void m18978br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3959QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "81c093fdee150bf23ada55764c7b4318", aum = 0)
    @C5566aOg
    /* renamed from: bs */
    private void m18979bs(I18NString i18NString) {
        throw new aWi(new aCE(this, f3960QD, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "083f14b872be6a0b0719d2f055797152", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m18980bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f3962QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m18981c(UUID uuid) {
        switch (bFf().mo6893i(f3968bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3968bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3968bO, new Object[]{uuid}));
                break;
        }
        m18976b(uuid);
    }

    /* renamed from: cH */
    private void m18982cH(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iYE, raVar);
    }

    /* renamed from: cI */
    private void m18983cI(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iYF, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C0064Am(aul = "4a0f09abda7e440679d174b68aa21e82", aum = 0)
    @C5566aOg
    /* renamed from: cr */
    private void m18984cr(Asset tCVar) {
        throw new aWi(new aCE(this, iZa, new Object[]{tCVar}));
    }

    /* renamed from: d */
    private void m18985d(AgentType goVar) {
        bFf().mo5608dq().mo3197f(iYA, goVar);
    }

    private NPCType dAU() {
        return (NPCType) bFf().mo5608dq().mo3214p(iYz);
    }

    private AgentType dAV() {
        return (AgentType) bFf().mo5608dq().mo3214p(iYA);
    }

    private I18NString dAW() {
        return (I18NString) bFf().mo5608dq().mo3214p(iYB);
    }

    private boolean dAX() {
        return bFf().mo5608dq().mo3201h(iYC);
    }

    private I18NString dAY() {
        return (I18NString) bFf().mo5608dq().mo3214p(iYD);
    }

    private C3438ra dAZ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iYE);
    }

    private C3438ra dBa() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iYF);
    }

    private float dBb() {
        return bFf().mo5608dq().mo3211m(iYG);
    }

    private C4045yZ.C4046a dBc() {
        return (C4045yZ.C4046a) bFf().mo5608dq().mo3214p(iYH);
    }

    private int dBd() {
        return bFf().mo5608dq().mo3212n(iYI);
    }

    private boolean dBe() {
        return bFf().mo5608dq().mo3201h(iYJ);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Giver Agent")
    @C0064Am(aul = "5b727e74d803e07309b9c5882db5068e", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m18986e(AgentType goVar) {
        throw new aWi(new aCE(this, iYQ, new Object[]{goVar}));
    }

    /* renamed from: fo */
    private void m18987fo(long j) {
        bFf().mo5608dq().mo3184b(dPh, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Money Reward")
    @C0064Am(aul = "b2a44c695bfc982a3b81945dbaa739c2", aum = 0)
    @C5566aOg
    /* renamed from: fp */
    private void m18988fp(long j) {
        throw new aWi(new aCE(this, dPi, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Text Reward Check")
    @C0064Am(aul = "1783b37fba1cb25bad7c2c8bf2dfce04", aum = 0)
    @C5566aOg
    /* renamed from: kA */
    private void m18989kA(boolean z) {
        throw new aWi(new aCE(this, iYL, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mandatory")
    @C0064Am(aul = "7bd0a7c7bc87a25c48aa738a41c52aa2", aum = 0)
    @C5566aOg
    /* renamed from: kC */
    private void m18990kC(boolean z) {
        throw new aWi(new aCE(this, iYY, new Object[]{new Boolean(z)}));
    }

    /* renamed from: ky */
    private void m18991ky(boolean z) {
        bFf().mo5608dq().mo3153a(iYC, z);
    }

    /* renamed from: kz */
    private void m18992kz(boolean z) {
        bFf().mo5608dq().mo3153a(iYJ, z);
    }

    /* renamed from: lU */
    private boolean m18993lU() {
        return bFf().mo5608dq().mo3201h(f3954Dg);
    }

    /* renamed from: le */
    private int m18995le() {
        return bFf().mo5608dq().mo3212n(f3950BP);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Rewards")
    @C0064Am(aul = "5611835cb2ba01a754e0fe6263d067f2", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m18997m(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, iYS, new Object[]{zlVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Rewards")
    @C0064Am(aul = "6cc459a05dbb25af05b0318a9800dadf", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m18998o(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, iYT, new Object[]{zlVar}));
    }

    /* renamed from: oF */
    private void m18999oF(float f) {
        bFf().mo5608dq().mo3150a(iYG, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unspawn Delay (in seconds)")
    @C0064Am(aul = "c14de0a3210757db8c10903e65cd6e47", aum = 0)
    @C5566aOg
    /* renamed from: oG */
    private void m19000oG(float f) {
        throw new aWi(new aCE(this, iYR, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Choice Rewards")
    @C0064Am(aul = "17f7df88814e4d31f7652359a3954c1d", aum = 0)
    @C5566aOg
    /* renamed from: q */
    private void m19001q(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, iYU, new Object[]{zlVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Choice Rewards")
    @C0064Am(aul = "819ee753cdfd8340d0096bbb2cfbba9e", aum = 0)
    @C5566aOg
    /* renamed from: s */
    private void m19003s(ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, iYV, new Object[]{zlVar}));
    }

    /* renamed from: sk */
    private void m19004sk(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iYB, i18NString);
    }

    /* renamed from: sl */
    private void m19005sl(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(iYD, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Summary")
    @C0064Am(aul = "8abca9dd7f7bcffed7609b5747cfee14", aum = 0)
    @C5566aOg
    /* renamed from: sm */
    private void m19006sm(I18NString i18NString) {
        throw new aWi(new aCE(this, iYK, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Text Reward")
    @C0064Am(aul = "d6f002cc34642b1609fb76eb644f8b4b", aum = 0)
    @C5566aOg
    /* renamed from: so */
    private void m19007so(I18NString i18NString) {
        throw new aWi(new aCE(this, iYM, new Object[]{i18NString}));
    }

    /* renamed from: vS */
    private I18NString m19008vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3963Qz);
    }

    /* renamed from: vT */
    private I18NString m19009vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3959QA);
    }

    /* renamed from: z */
    private void m19011z(NPCType aed) {
        bFf().mo5608dq().mo3197f(iYz, aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Giver NPC (old)")
    @C5566aOg
    /* renamed from: B */
    public void mo11872B(NPCType aed) {
        switch (bFf().mo6893i(iYO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYO, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYO, new Object[]{aed}));
                break;
        }
        m18956A(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Merit Points")
    @C5566aOg
    /* renamed from: Be */
    public void mo11873Be(int i) {
        switch (bFf().mo6893i(iYW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYW, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYW, new Object[]{new Integer(i)}));
                break;
        }
        m18959Bd(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent")
    @C5566aOg
    /* renamed from: D */
    public void mo11874D(boolean z) {
        switch (bFf().mo6893i(f3956Dm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3956Dm, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3956Dm, new Object[]{new Boolean(z)}));
                break;
        }
        m18960C(z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1756Zw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m18972au();
            case 1:
                return m18969ap();
            case 2:
                m18976b((UUID) args[0]);
                return null;
            case 3:
                return m19010vV();
            case 4:
                m18980bu((I18NString) args[0]);
                return null;
            case 5:
                m19006sm((I18NString) args[0]);
                return null;
            case 6:
                return aAM();
            case 7:
                return new Boolean(aAk());
            case 8:
                m18989kA(((Boolean) args[0]).booleanValue());
                return null;
            case 9:
                m19007so((I18NString) args[0]);
                return null;
            case 10:
                return aAL();
            case 11:
                return m19002rO();
            case 12:
                m18979bs((I18NString) args[0]);
                return null;
            case 13:
                return m18970ar();
            case 14:
                m18975b((String) args[0]);
                return null;
            case 15:
                return new Long(aAK());
            case 16:
                m18988fp(((Long) args[0]).longValue());
                return null;
            case 17:
                return dBf();
            case 18:
                m18956A((NPCType) args[0]);
                return null;
            case 19:
                return aAG();
            case 20:
                return aAH();
            case 21:
                return dBh();
            case 22:
                m18986e((AgentType) args[0]);
                return null;
            case 23:
                return new Boolean(m18994lY());
            case 24:
                m18960C(((Boolean) args[0]).booleanValue());
                return null;
            case 25:
                return new Float(aAF());
            case 26:
                m19000oG(((Float) args[0]).floatValue());
                return null;
            case 27:
                m18997m((ItemTypeTableItem) args[0]);
                return null;
            case 28:
                m18998o((ItemTypeTableItem) args[0]);
                return null;
            case 29:
                return aAI();
            case 30:
                m19001q((ItemTypeTableItem) args[0]);
                return null;
            case 31:
                m19003s((ItemTypeTableItem) args[0]);
                return null;
            case 32:
                return aAJ();
            case 33:
                return new Integer(aAQ());
            case 34:
                m18959Bd(((Integer) args[0]).intValue());
                return null;
            case 35:
                return aAP();
            case 36:
                m18974b((C4045yZ.C4046a) args[0]);
                return null;
            case 37:
                return new Integer(m18996ls());
            case 38:
                m18973ax(((Integer) args[0]).intValue());
                return null;
            case 39:
                return new Boolean(aAR());
            case 40:
                m18990kC(((Boolean) args[0]).booleanValue());
                return null;
            case 41:
                m18966aG();
                return null;
            case 42:
                return dBj();
            case 43:
                m18984cr((Asset) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m18966aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3967bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3967bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3967bN, new Object[0]));
                break;
        }
        return m18969ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Summary")
    public I18NString aqH() {
        switch (bFf().mo6893i(cpY)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cpY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpY, new Object[0]));
                break;
        }
        return aAM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Text Reward")
    public I18NString aqI() {
        switch (bFf().mo6893i(cpX)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cpX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpX, new Object[0]));
                break;
        }
        return aAL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Text Reward Check")
    public boolean aqJ() {
        switch (bFf().mo6893i(cpy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cpy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpy, new Object[0]));
                break;
        }
        return aAk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unspawn Delay (in seconds)")
    public float aqK() {
        switch (bFf().mo6893i(cpR)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cpR, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpR, new Object[0]));
                break;
        }
        return aAF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Money Reward")
    public long aqL() {
        switch (bFf().mo6893i(cpW)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cpW, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpW, new Object[0]));
                break;
        }
        return aAK();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Rewards")
    public List<ItemTypeTableItem> aqM() {
        switch (bFf().mo6893i(cpU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cpU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpU, new Object[0]));
                break;
        }
        return aAI();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Choice Rewards")
    public List<ItemTypeTableItem> aqN() {
        switch (bFf().mo6893i(cpV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cpV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpV, new Object[0]));
                break;
        }
        return aAJ();
    }

    public final String aqO() {
        switch (bFf().mo6893i(cpT)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cpT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpT, new Object[0]));
                break;
        }
        return aAH();
    }

    public final String aqP() {
        switch (bFf().mo6893i(cpS)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cpS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpS, new Object[0]));
                break;
        }
        return aAG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MissionCategory")
    public C4045yZ.C4046a aqQ() {
        switch (bFf().mo6893i(cqa)) {
            case 0:
                return null;
            case 2:
                return (C4045yZ.C4046a) bFf().mo5606d(new aCE(this, cqa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqa, new Object[0]));
                break;
        }
        return aAP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Merit Points")
    public int aqR() {
        switch (bFf().mo6893i(cqb)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cqb, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cqb, new Object[0]));
                break;
        }
        return aAQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mandatory")
    public boolean aqS() {
        switch (bFf().mo6893i(cqc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cqc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cqc, new Object[0]));
                break;
        }
        return aAR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Challenge Rating")
    @C5566aOg
    /* renamed from: ay */
    public void mo11875ay(int i) {
        switch (bFf().mo6893i(f3952Cr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3952Cr, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3952Cr, new Object[]{new Integer(i)}));
                break;
        }
        m18973ax(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C5566aOg
    /* renamed from: bt */
    public void mo11876bt(I18NString i18NString) {
        switch (bFf().mo6893i(f3960QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3960QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3960QD, new Object[]{i18NString}));
                break;
        }
        m18979bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo11877bv(I18NString i18NString) {
        switch (bFf().mo6893i(f3962QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3962QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3962QF, new Object[]{i18NString}));
                break;
        }
        m18980bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MissionCategory")
    @C5566aOg
    /* renamed from: c */
    public void mo11878c(C4045yZ.C4046a aVar) {
        switch (bFf().mo6893i(iYX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYX, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYX, new Object[]{aVar}));
                break;
        }
        m18974b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speech sound")
    @C5566aOg
    /* renamed from: cs */
    public void mo11879cs(Asset tCVar) {
        switch (bFf().mo6893i(iZa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZa, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZa, new Object[]{tCVar}));
                break;
        }
        m18984cr(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Giver NPC (old)")
    public NPCType dBg() {
        switch (bFf().mo6893i(iYN)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, iYN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iYN, new Object[0]));
                break;
        }
        return dBf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Giver Agent")
    public AgentType dBi() {
        switch (bFf().mo6893i(iYP)) {
            case 0:
                return null;
            case 2:
                return (AgentType) bFf().mo5606d(new aCE(this, iYP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iYP, new Object[0]));
                break;
        }
        return dBh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    public Asset dBk() {
        switch (bFf().mo6893i(iYZ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iYZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iYZ, new Object[0]));
                break;
        }
        return dBj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Giver Agent")
    @C5566aOg
    /* renamed from: f */
    public void mo11883f(AgentType goVar) {
        switch (bFf().mo6893i(iYQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYQ, new Object[]{goVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYQ, new Object[]{goVar}));
                break;
        }
        m18986e(goVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Money Reward")
    @C5566aOg
    /* renamed from: fq */
    public void mo11884fq(long j) {
        switch (bFf().mo6893i(dPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dPi, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dPi, new Object[]{new Long(j)}));
                break;
        }
        m18988fp(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3969bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3969bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3969bP, new Object[0]));
                break;
        }
        return m18970ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3970bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3970bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3970bQ, new Object[]{str}));
                break;
        }
        m18975b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Text Reward Check")
    @C5566aOg
    /* renamed from: kB */
    public void mo11885kB(boolean z) {
        switch (bFf().mo6893i(iYL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYL, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYL, new Object[]{new Boolean(z)}));
                break;
        }
        m18989kA(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mandatory")
    @C5566aOg
    /* renamed from: kD */
    public void mo11886kD(boolean z) {
        switch (bFf().mo6893i(iYY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYY, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYY, new Object[]{new Boolean(z)}));
                break;
        }
        m18990kC(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent")
    /* renamed from: lZ */
    public boolean mo709lZ() {
        switch (bFf().mo6893i(f3955Dl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f3955Dl, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3955Dl, new Object[0]));
                break;
        }
        return m18994lY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Challenge Rating")
    /* renamed from: lt */
    public int mo710lt() {
        switch (bFf().mo6893i(f3951Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f3951Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3951Cq, new Object[0]));
                break;
        }
        return m18996ls();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Rewards")
    @C5566aOg
    /* renamed from: n */
    public void mo11887n(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(iYS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYS, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYS, new Object[]{zlVar}));
                break;
        }
        m18997m(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unspawn Delay (in seconds)")
    @C5566aOg
    /* renamed from: oH */
    public void mo11888oH(float f) {
        switch (bFf().mo6893i(iYR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYR, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYR, new Object[]{new Float(f)}));
                break;
        }
        m19000oG(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Rewards")
    @C5566aOg
    /* renamed from: p */
    public void mo11889p(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(iYT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYT, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYT, new Object[]{zlVar}));
                break;
        }
        m18998o(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Choice Rewards")
    @C5566aOg
    /* renamed from: r */
    public void mo11890r(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(iYU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYU, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYU, new Object[]{zlVar}));
                break;
        }
        m19001q(zlVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo711rP() {
        switch (bFf().mo6893i(f3958MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3958MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3958MN, new Object[0]));
                break;
        }
        return m19002rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Summary")
    @C5566aOg
    /* renamed from: sn */
    public void mo11892sn(I18NString i18NString) {
        switch (bFf().mo6893i(iYK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYK, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYK, new Object[]{i18NString}));
                break;
        }
        m19006sm(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Text Reward")
    @C5566aOg
    /* renamed from: sp */
    public void mo11893sp(I18NString i18NString) {
        switch (bFf().mo6893i(iYM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYM, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYM, new Object[]{i18NString}));
                break;
        }
        m19007so(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Choice Rewards")
    @C5566aOg
    /* renamed from: t */
    public void mo11894t(ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(iYV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYV, new Object[]{zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYV, new Object[]{zlVar}));
                break;
        }
        m19003s(zlVar);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m18972au();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo713vW() {
        switch (bFf().mo6893i(f3961QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3961QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3961QE, new Object[0]));
                break;
        }
        return m19010vV();
    }

    @C0064Am(aul = "4fc3971abb1b544339e9c51005f9bf3c", aum = 0)
    /* renamed from: au */
    private String m18972au() {
        return "MissionTemplate: [" + m18968ao() + "]";
    }

    @C0064Am(aul = "14391230c8edfa9668a988fef674b8e6", aum = 0)
    /* renamed from: ap */
    private UUID m18969ap() {
        return m18967an();
    }

    @C0064Am(aul = "770dfff047fa78c0ecec4f123f4564d3", aum = 0)
    /* renamed from: b */
    private void m18976b(UUID uuid) {
        m18965a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        setHandle(C0468GU.dac);
        m18965a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "ea95bdb1ea1e3dbf57c4e8f09a6193bd", aum = 0)
    /* renamed from: vV */
    private I18NString m19010vV() {
        return m19009vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Summary")
    @C0064Am(aul = "8e291c02f5bf37f81ed692bfda4fcd11", aum = 0)
    private I18NString aAM() {
        return dAW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Text Reward Check")
    @C0064Am(aul = "ed7ae862213355d10954fc1d36092445", aum = 0)
    private boolean aAk() {
        return dAX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Text Reward")
    @C0064Am(aul = "098d84a69184947e1f43f95eb33bb4af", aum = 0)
    private I18NString aAL() {
        return dAY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "91d651405d8524d650d1c87b563d5671", aum = 0)
    /* renamed from: rO */
    private I18NString m19002rO() {
        return m19008vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "9976e2e53db7ebc166b81383dd545102", aum = 0)
    /* renamed from: ar */
    private String m18970ar() {
        return m18968ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Money Reward")
    @C0064Am(aul = "93e4a66deeb5ee2bedb1a949337e8c78", aum = 0)
    private long aAK() {
        return bnb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Giver NPC (old)")
    @C0064Am(aul = "e93555343c3bb5a79f3766e705ee2322", aum = 0)
    private NPCType dBf() {
        return dAU();
    }

    @C0064Am(aul = "3a0bb02207f5d9fa65ca5c19f5cef92d", aum = 0)
    private String aAG() {
        return dBi().bTI();
    }

    @C0064Am(aul = "825ed975138e7835b28bd499bf65aba7", aum = 0)
    private String aAH() {
        return dBi().mo11470ke().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Giver Agent")
    @C0064Am(aul = "c9a3589a3da1ee6252aad276d187e0d9", aum = 0)
    private AgentType dBh() {
        return dAV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent")
    @C0064Am(aul = "eb7c681092d62454d45a7900e6e9b717", aum = 0)
    /* renamed from: lY */
    private boolean m18994lY() {
        return m18993lU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unspawn Delay (in seconds)")
    @C0064Am(aul = "62ec3eb3c072997d96e146dfd33619ee", aum = 0)
    private float aAF() {
        return dBb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Rewards")
    @C0064Am(aul = "8f99b64584fbc3712036cd88d1c4dbb7", aum = 0)
    private List<ItemTypeTableItem> aAI() {
        return dAZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Choice Rewards")
    @C0064Am(aul = "0ebd6d36f44af011c414814fca49bf4e", aum = 0)
    private List<ItemTypeTableItem> aAJ() {
        return dBa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Merit Points")
    @C0064Am(aul = "9fee3ec71de7e5ef98cfbc1153beec63", aum = 0)
    private int aAQ() {
        return dBd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MissionCategory")
    @C0064Am(aul = "aa6d19585cb24d1a13dbd05779c9d50f", aum = 0)
    private C4045yZ.C4046a aAP() {
        return dBc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Challenge Rating")
    @C0064Am(aul = "913e44bbba5a04ed7dc31e0da2acae95", aum = 0)
    /* renamed from: ls */
    private int m18996ls() {
        return m18995le();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mandatory")
    @C0064Am(aul = "9ee5f932d9d93c7b6e2449886c09f127", aum = 0)
    private boolean aAR() {
        return dBe();
    }

    @C0064Am(aul = "326d1bd4b232bcdc09961bf03900b59b", aum = 0)
    /* renamed from: aG */
    private void m18966aG() {
        String handle2;
        super.mo70aH();
        if (dAV() == null && dAU() != null && (handle2 = dAU().getHandle()) != null) {
            for (NPCType next : ala().aJY()) {
                if ((next instanceof AgentType) && handle2.equals(next.getHandle())) {
                    m18985d((AgentType) next);
                    return;
                }
            }
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speech sound")
    @C0064Am(aul = "f6dca1aa11b9f4d7cb9e9ebacfda4d0d", aum = 0)
    private Asset dBj() {
        return abq();
    }
}
