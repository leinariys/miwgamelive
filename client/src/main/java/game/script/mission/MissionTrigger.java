package game.script.mission;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.C6950awp;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Trigger;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1235SL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Arrays;
import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aUS */
/* compiled from: a */
public class MissionTrigger extends Trigger implements C1616Xf {

    /* renamed from: Pf */
    public static final C2491fm f3857Pf = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aeP = null;
    public static final C2491fm awC = null;
    public static final C2491fm awo = null;
    public static final C2491fm brY = null;
    public static final C5663aRz iXL = null;
    public static final C5663aRz iXM = null;
    public static final C2491fm iXN = null;
    public static final C2491fm iXO = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vc */
    public static final C2491fm f3859vc = null;
    /* renamed from: ve */
    public static final C2491fm f3860ve = null;
    /* renamed from: vi */
    public static final C2491fm f3861vi = null;
    /* renamed from: vm */
    public static final C2491fm f3862vm = null;
    /* renamed from: vo */
    public static final C2491fm f3863vo = null;
    /* renamed from: vq */
    public static final C2491fm f3864vq = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7c62e4a0656302866cf175c487770610", aum = 0)
    private static String efM;
    @C0064Am(aul = "93f68848f9777fae6ade89644a5a918a", aum = 2)

    /* renamed from: mb */
    private static boolean f3858mb;
    @C0064Am(aul = "81135db7d5daf7ecd65a6afd95d588b7", aum = 1)

    /* renamed from: wx */
    private static Mission<? extends MissionTemplate> f3865wx;

    static {
        m18571V();
    }

    public MissionTrigger() {
        super((C5540aNg) null);
        super.mo10S();
    }

    MissionTrigger(Mission<? extends MissionTemplate> af, String str, boolean z) {
        super((C5540aNg) null);
        super._m_script_init(af, str, z);
    }

    public MissionTrigger(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18571V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Trigger._m_fieldCount + 3;
        _m_methodCount = Trigger._m_methodCount + 13;
        int i = Trigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(MissionTrigger.class, "7c62e4a0656302866cf175c487770610", i);
        iXL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionTrigger.class, "81135db7d5daf7ecd65a6afd95d588b7", i2);
        aeP = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionTrigger.class, "93f68848f9777fae6ade89644a5a918a", i3);
        iXM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Trigger._m_fields, (Object[]) _m_fields);
        int i5 = Trigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 13)];
        C2491fm a = C4105zY.m41624a(MissionTrigger.class, "7e00f963641dd905017135b5b494a9f5", i5);
        _f_dispose_0020_0028_0029V = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionTrigger.class, "028ec74329b21bef24190b050e270757", i6);
        f3859vc = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionTrigger.class, "248a0e03430153fbccce8c368d3b0d59", i7);
        f3860ve = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionTrigger.class, "e6f1e9bdac0ce053c65b583beb671a29", i8);
        f3863vo = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionTrigger.class, "1c8021684d9a403c61c27620cf2c1aba", i9);
        f3857Pf = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionTrigger.class, "89446b84e3d3b87aeef14cc94dd18780", i10);
        awC = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionTrigger.class, "c8f73af399fb1eb3015128f67548eaaa", i11);
        iXN = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionTrigger.class, "a814fc38655cc0105697e55e787ff7f3", i12);
        awo = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionTrigger.class, "d16acbcc9e80e7f40262ab0c2aea9217", i13);
        f3862vm = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionTrigger.class, "05592fc470489c043f3cbcec56c6a870", i14);
        f3861vi = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionTrigger.class, "e84543f3cdfd5b3255216ddd80728a07", i15);
        iXO = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(MissionTrigger.class, "84bf46e8ba5b5aebadc2ac6451e5d20d", i16);
        f3864vq = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(MissionTrigger.class, "5b259739b2ec3121495625be91e54090", i17);
        brY = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Trigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionTrigger.class, C1235SL.class, _m_fields, _m_methods);
    }

    private Mission cqz() {
        return (Mission) bFf().mo5608dq().mo3214p(aeP);
    }

    /* renamed from: d */
    private void m18574d(Mission af) {
        bFf().mo5608dq().mo3197f(aeP, af);
    }

    private String dAy() {
        return (String) bFf().mo5608dq().mo3214p(iXL);
    }

    private boolean dAz() {
        return bFf().mo5608dq().mo3201h(iXM);
    }

    /* renamed from: kv */
    private void m18580kv(boolean z) {
        bFf().mo5608dq().mo3153a(iXM, z);
    }

    /* renamed from: nS */
    private void m18581nS(String str) {
        bFf().mo5608dq().mo3197f(iXL, str);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "89446b84e3d3b87aeef14cc94dd18780", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: y */
    private void m18583y(Actor cr) {
        throw new aWi(new aCE(this, awC, new Object[]{cr}));
    }

    /* renamed from: Ls */
    public boolean mo959Ls() {
        switch (bFf().mo6893i(awo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, awo, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, awo, new Object[0]));
                break;
        }
        return m18570Lr();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1235SL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Trigger._m_methodCount) {
            case 0:
                m18575fg();
                return null;
            case 1:
                return m18576io();
            case 2:
                return m18577iq();
            case 3:
                return m18579iz();
            case 4:
                return m18582uV();
            case 5:
                m18583y((Actor) args[0]);
                return null;
            case 6:
                return dAA();
            case 7:
                return new Boolean(m18570Lr());
            case 8:
                m18573a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 9:
                return m18578it();
            case 10:
                return dAC();
            case 11:
                m18572a(((Integer) args[0]).intValue(), ((Long) args[1]).longValue(), (Collection<C6625aqZ>) (Collection) args[2], (C6705asB) args[3]);
                return null;
            case 12:
                return ail();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo990b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        switch (bFf().mo6893i(f3864vq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3864vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3864vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                break;
        }
        m18572a(i, j, collection, asb);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f3862vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3862vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3862vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m18573a(cr, acm, vec3f, vec3f2);
    }

    public Mission<? extends MissionTemplate> btC() {
        switch (bFf().mo6893i(iXO)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, iXO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iXO, new Object[0]));
                break;
        }
        return dAC();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String dAB() {
        switch (bFf().mo6893i(iXN)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, iXN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iXN, new Object[0]));
                break;
        }
        return dAA();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m18575fg();
    }

    @ClientOnly
    public Color getColor() {
        switch (bFf().mo6893i(brY)) {
            case 0:
                return null;
            case 2:
                return (Color) bFf().mo5606d(new aCE(this, brY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brY, new Object[0]));
                break;
        }
        return ail();
    }

    public String getName() {
        switch (bFf().mo6893i(f3857Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3857Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3857Pf, new Object[0]));
                break;
        }
        return m18582uV();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f3863vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3863vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3863vo, new Object[0]));
                break;
        }
        return m18579iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f3859vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3859vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3859vc, new Object[0]));
                break;
        }
        return m18576io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f3860ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3860ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3860ve, new Object[0]));
                break;
        }
        return m18577iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f3861vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3861vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3861vi, new Object[0]));
                break;
        }
        return m18578it();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m18583y(cr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo11632a(Mission<? extends MissionTemplate> af, String str, boolean z) {
        super.mo10S();
        m18574d(af);
        m18581nS(str);
        m18580kv(z);
        setStatic(true);
    }

    @C0064Am(aul = "7e00f963641dd905017135b5b494a9f5", aum = 0)
    /* renamed from: fg */
    private void m18575fg() {
        if (mo960Nc() != null) {
            mo1099zx();
        }
        super.dispose();
    }

    @C0064Am(aul = "028ec74329b21bef24190b050e270757", aum = 0)
    /* renamed from: io */
    private String m18576io() {
        return ala().aJe().mo19013xj().mo3521Nu().getHandle();
    }

    @C0064Am(aul = "248a0e03430153fbccce8c368d3b0d59", aum = 0)
    /* renamed from: iq */
    private String m18577iq() {
        return ala().aJe().mo19013xj().mo3521Nu().getFile();
    }

    @C0064Am(aul = "e6f1e9bdac0ce053c65b583beb671a29", aum = 0)
    /* renamed from: iz */
    private String m18579iz() {
        return ala().aIW().avE();
    }

    @C0064Am(aul = "1c8021684d9a403c61c27620cf2c1aba", aum = 0)
    /* renamed from: uV */
    private String m18582uV() {
        if (cqz().mo69V(getPlayer())) {
            return ala().aJe().mo19013xj().cZe().get();
        }
        return cqz().mo64KY().getName();
    }

    @C0064Am(aul = "c8f73af399fb1eb3015128f67548eaaa", aum = 0)
    private String dAA() {
        return dAy();
    }

    @C0064Am(aul = "a814fc38655cc0105697e55e787ff7f3", aum = 0)
    /* renamed from: Lr */
    private boolean m18570Lr() {
        return dAz();
    }

    @C0064Am(aul = "d16acbcc9e80e7f40262ab0c2aea9217", aum = 0)
    /* renamed from: a */
    private void m18573a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "05592fc470489c043f3cbcec56c6a870", aum = 0)
    /* renamed from: it */
    private String m18578it() {
        return null;
    }

    @C0064Am(aul = "e84543f3cdfd5b3255216ddd80728a07", aum = 0)
    private Mission<? extends MissionTemplate> dAC() {
        return cqz();
    }

    @C0064Am(aul = "84bf46e8ba5b5aebadc2ac6451e5d20d", aum = 0)
    /* renamed from: a */
    private void m18572a(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        Player KY;
        Party dxi;
        if (cLd() != null && (KY = cqz().mo64KY()) != null) {
            asb.mo13024a((Collection<C6625aqZ>) Arrays.asList(new C6625aqZ[]{KY}), (C6950awp) new C4031yM(i, this, cLd().mo2472kQ(), j));
            if ((KY instanceof Player) && (dxi = KY.dxi()) != null) {
                for (Player next : dxi.aQS()) {
                    if (next != dxi.aQU() && collection.contains(next)) {
                        asb.mo13024a((Collection<C6625aqZ>) Arrays.asList(new C6625aqZ[]{next}), (C6950awp) new C4031yM(i, this, cLd().mo2472kQ(), j));
                    }
                }
            }
        }
    }

    @C0064Am(aul = "5b259739b2ec3121495625be91e54090", aum = 0)
    @ClientOnly
    private Color ail() {
        if (btC().mo69V(getPlayer())) {
            return ala().aIW().ave();
        }
        return ala().aIW().avg();
    }
}
