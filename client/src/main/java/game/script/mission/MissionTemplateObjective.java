package game.script.mission;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5690aTa;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aQY */
/* compiled from: a */
public class MissionTemplateObjective extends aDJ implements C0468GU, C1616Xf, C2780jt {

    /* renamed from: QA */
    public static final C5663aRz f3624QA = null;

    /* renamed from: QE */
    public static final C2491fm f3625QE = null;

    /* renamed from: QF */
    public static final C2491fm f3626QF = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aGq = null;
    public static final C2491fm aGt = null;
    public static final C2491fm aGw = null;
    /* renamed from: bL */
    public static final C5663aRz f3628bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3629bM = null;
    /* renamed from: bN */
    public static final C2491fm f3630bN = null;
    /* renamed from: bO */
    public static final C2491fm f3631bO = null;
    /* renamed from: bP */
    public static final C2491fm f3632bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3633bQ = null;
    public static final C5663aRz bih = null;
    public static final C2491fm bin = null;
    public static final C2491fm bio = null;
    public static final C2491fm bip = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ad01a73120a3c5a1d84f03c12cab7f7d", aum = 4)

    /* renamed from: bK */
    private static UUID f3627bK;
    @C0064Am(aul = "2a184974264302b93b647b38dfb77196", aum = 3)
    private static boolean big;
    @C0064Am(aul = "eb21a6a91565c7c71c723a64f3c848e4", aum = 2)

    /* renamed from: cZ */
    private static int f3634cZ;
    @C0064Am(aul = "363cfe5cccc7a90afbd125747fbe1f5d", aum = 0)
    private static String handle;
    @C0064Am(aul = "84ca92645366e4b47d496b16b33937f4", aum = 1)

    /* renamed from: nh */
    private static I18NString f3635nh;

    static {
        m17540V();
    }

    public MissionTemplateObjective() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionTemplateObjective(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17540V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 11;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(MissionTemplateObjective.class, "363cfe5cccc7a90afbd125747fbe1f5d", i);
        f3629bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionTemplateObjective.class, "84ca92645366e4b47d496b16b33937f4", i2);
        f3624QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionTemplateObjective.class, "eb21a6a91565c7c71c723a64f3c848e4", i3);
        aGq = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionTemplateObjective.class, "2a184974264302b93b647b38dfb77196", i4);
        bih = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MissionTemplateObjective.class, "ad01a73120a3c5a1d84f03c12cab7f7d", i5);
        f3628bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 11)];
        C2491fm a = C4105zY.m41624a(MissionTemplateObjective.class, "00bc11d01003cbb0b927995df94329ec", i7);
        f3630bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionTemplateObjective.class, "be800cf0e16092dfadf26d4e8cc740c1", i8);
        f3631bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionTemplateObjective.class, "de94349dc50c3df5dc3a2c91e4ae161e", i9);
        f3633bQ = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionTemplateObjective.class, "3e498dc5e8768028cc3dcc9725dba3f5", i10);
        f3632bP = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionTemplateObjective.class, "5d535ef8bda223837134ce0e7040f108", i11);
        f3625QE = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionTemplateObjective.class, "7e0e4a69712a202421eef8915d362000", i12);
        f3626QF = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionTemplateObjective.class, "b7ab2754c8111c22c6c43b8c92555cc4", i13);
        aGt = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionTemplateObjective.class, "7b40758f36a4c87c3e913bcf535744fc", i14);
        aGw = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionTemplateObjective.class, "36ae3ac6a5edd81eaaac77fc2ede9bd1", i15);
        bin = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionTemplateObjective.class, "6f4b4592ac115fa49beb07da4eacc6de", i16);
        bio = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionTemplateObjective.class, "016edb7ceb0641cc5b585b291f259431", i17);
        bip = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionTemplateObjective.class, C5690aTa.class, _m_fields, _m_methods);
    }

    /* renamed from: PF */
    private int m17538PF() {
        return bFf().mo5608dq().mo3212n(aGq);
    }

    /* renamed from: a */
    private void m17541a(String str) {
        bFf().mo5608dq().mo3197f(f3629bM, str);
    }

    /* renamed from: a */
    private void m17542a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3628bL, uuid);
    }

    /* renamed from: aY */
    private void m17543aY(boolean z) {
        bFf().mo5608dq().mo3153a(bih, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hidden")
    @C0064Am(aul = "6f4b4592ac115fa49beb07da4eacc6de", aum = 0)
    @C5566aOg
    /* renamed from: aZ */
    private void m17544aZ(boolean z) {
        throw new aWi(new aCE(this, bio, new Object[]{new Boolean(z)}));
    }

    private boolean abL() {
        return bFf().mo5608dq().mo3201h(bih);
    }

    /* renamed from: an */
    private UUID m17545an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3628bL);
    }

    /* renamed from: ao */
    private String m17546ao() {
        return (String) bFf().mo5608dq().mo3214p(f3629bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "de94349dc50c3df5dc3a2c91e4ae161e", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m17549b(String str) {
        throw new aWi(new aCE(this, f3633bQ, new Object[]{str}));
    }

    /* renamed from: br */
    private void m17551br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3624QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "7e0e4a69712a202421eef8915d362000", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m17552bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f3626QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m17553c(UUID uuid) {
        switch (bFf().mo6893i(f3631bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3631bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3631bO, new Object[]{uuid}));
                break;
        }
        m17550b(uuid);
    }

    /* renamed from: cZ */
    private void m17554cZ(int i) {
        bFf().mo5608dq().mo3183b(aGq, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Quantity")
    @C0064Am(aul = "7b40758f36a4c87c3e913bcf535744fc", aum = 0)
    @C5566aOg
    /* renamed from: da */
    private void m17555da(int i) {
        throw new aWi(new aCE(this, aGw, new Object[]{new Integer(i)}));
    }

    /* renamed from: vT */
    private I18NString m17556vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3624QA);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Quantity")
    /* renamed from: Iq */
    public int mo11020Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m17539PK();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5690aTa(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m17547ap();
            case 1:
                m17550b((UUID) args[0]);
                return null;
            case 2:
                m17549b((String) args[0]);
                return null;
            case 3:
                return m17548ar();
            case 4:
                return m17557vV();
            case 5:
                m17552bu((I18NString) args[0]);
                return null;
            case 6:
                return new Integer(m17539PK());
            case 7:
                m17555da(((Integer) args[0]).intValue());
                return null;
            case 8:
                return new Boolean(abS());
            case 9:
                m17544aZ(((Boolean) args[0]).booleanValue());
                return null;
            case 10:
                return abT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3630bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3630bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3630bN, new Object[0]));
                break;
        }
        return m17547ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo11021bv(I18NString i18NString) {
        switch (bFf().mo6893i(f3626QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3626QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3626QF, new Object[]{i18NString}));
                break;
        }
        m17552bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Quantity")
    @C5566aOg
    /* renamed from: db */
    public void mo11022db(int i) {
        switch (bFf().mo6893i(aGw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                break;
        }
        m17555da(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3632bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3632bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3632bP, new Object[0]));
                break;
        }
        return m17548ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3633bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3633bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3633bQ, new Object[]{str}));
                break;
        }
        m17549b(str);
    }

    public String getInfo() {
        switch (bFf().mo6893i(bip)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bip, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bip, new Object[0]));
                break;
        }
        return abT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hidden")
    public boolean isHidden() {
        switch (bFf().mo6893i(bin)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bin, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bin, new Object[0]));
                break;
        }
        return abS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hidden")
    @C5566aOg
    public void setHidden(boolean z) {
        switch (bFf().mo6893i(bio)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bio, new Object[]{new Boolean(z)}));
                break;
        }
        m17544aZ(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo4247vW() {
        switch (bFf().mo6893i(f3625QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3625QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3625QE, new Object[0]));
                break;
        }
        return m17557vV();
    }

    @C0064Am(aul = "00bc11d01003cbb0b927995df94329ec", aum = 0)
    /* renamed from: ap */
    private UUID m17547ap() {
        return m17545an();
    }

    @C0064Am(aul = "be800cf0e16092dfadf26d4e8cc740c1", aum = 0)
    /* renamed from: b */
    private void m17550b(UUID uuid) {
        m17542a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m17542a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "3e498dc5e8768028cc3dcc9725dba3f5", aum = 0)
    /* renamed from: ar */
    private String m17548ar() {
        return m17546ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "5d535ef8bda223837134ce0e7040f108", aum = 0)
    /* renamed from: vV */
    private I18NString m17557vV() {
        return m17556vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Quantity")
    @C0064Am(aul = "b7ab2754c8111c22c6c43b8c92555cc4", aum = 0)
    /* renamed from: PK */
    private int m17539PK() {
        return m17538PF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hidden")
    @C0064Am(aul = "36ae3ac6a5edd81eaaac77fc2ede9bd1", aum = 0)
    private boolean abS() {
        return abL();
    }

    @C0064Am(aul = "016edb7ceb0641cc5b585b291f259431", aum = 0)
    private String abT() {
        return String.valueOf(String.valueOf(0)) + C0147Bi.SEPARATOR + String.valueOf(m17538PF());
    }
}
