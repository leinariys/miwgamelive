package game.script.mission;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C6686ari;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aLB */
/* compiled from: a */
public class MissionTriggerType extends aDJ implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f3274LR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    /* renamed from: bL */
    public static final C5663aRz f3276bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3277bM = null;
    /* renamed from: bN */
    public static final C2491fm f3278bN = null;
    /* renamed from: bO */
    public static final C2491fm f3279bO = null;
    /* renamed from: bP */
    public static final C2491fm f3280bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3281bQ = null;
    public static final C5663aRz hJJ = null;
    public static final C2491fm hJK = null;
    public static final C2491fm hJL = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8fe4348811bac83c66e7befeb63cc972", aum = 1)

    /* renamed from: LQ */
    private static Asset f3273LQ;
    @C0064Am(aul = "d8b94225ec5cd98146d7f984f173c378", aum = 3)

    /* renamed from: bK */
    private static UUID f3275bK;
    @C0064Am(aul = "9df326470159548896f68937c27ff297", aum = 2)
    private static I18NString grv;
    @C0064Am(aul = "0f4cdfcfa769c65b76c7d02981f660fb", aum = 0)
    private static String handle;

    static {
        m16076V();
    }

    public MissionTriggerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionTriggerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16076V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 8;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(MissionTriggerType.class, "0f4cdfcfa769c65b76c7d02981f660fb", i);
        f3277bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionTriggerType.class, "8fe4348811bac83c66e7befeb63cc972", i2);
        f3274LR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionTriggerType.class, "9df326470159548896f68937c27ff297", i3);
        hJJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionTriggerType.class, "d8b94225ec5cd98146d7f984f173c378", i4);
        f3276bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(MissionTriggerType.class, "d4dea7c2d5bc5db632123b5c9e8f45d5", i6);
        f3278bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionTriggerType.class, "81f0e04d5e75411d704571b8983a413a", i7);
        f3279bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionTriggerType.class, "7db16e10eef40dc6a0fbc6f939f0654f", i8);
        aDk = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionTriggerType.class, "f312c18f2596ecb093ca8f5dcde121c9", i9);
        aDl = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionTriggerType.class, "58cc10afcc1bdee6e6e2f0d825f379a4", i10);
        hJK = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionTriggerType.class, "30e1860c15a99d3a6983ebd653badf89", i11);
        hJL = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionTriggerType.class, "3fcfe89c235b0b120b94ef020bca894d", i12);
        f3280bP = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionTriggerType.class, "0c1f0511e3089793cf537a245c8961ce", i13);
        f3281bQ = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionTriggerType.class, C6686ari.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Render")
    @C0064Am(aul = "f312c18f2596ecb093ca8f5dcde121c9", aum = 0)
    @C5566aOg
    /* renamed from: H */
    private void m16074H(Asset tCVar) {
        throw new aWi(new aCE(this, aDl, new Object[]{tCVar}));
    }

    /* renamed from: a */
    private void m16077a(String str) {
        bFf().mo5608dq().mo3197f(f3277bM, str);
    }

    /* renamed from: a */
    private void m16078a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3276bL, uuid);
    }

    /* renamed from: an */
    private UUID m16079an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3276bL);
    }

    /* renamed from: ao */
    private String m16080ao() {
        return (String) bFf().mo5608dq().mo3214p(f3277bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "0c1f0511e3089793cf537a245c8961ce", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m16083b(String str) {
        throw new aWi(new aCE(this, f3281bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m16085c(UUID uuid) {
        switch (bFf().mo6893i(f3279bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3279bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3279bO, new Object[]{uuid}));
                break;
        }
        m16084b(uuid);
    }

    private I18NString cZc() {
        return (I18NString) bFf().mo5608dq().mo3214p(hJJ);
    }

    /* renamed from: nK */
    private void m16086nK(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(hJJ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Name")
    @C0064Am(aul = "58cc10afcc1bdee6e6e2f0d825f379a4", aum = 0)
    @C5566aOg
    /* renamed from: nL */
    private void m16087nL(I18NString i18NString) {
        throw new aWi(new aCE(this, hJK, new Object[]{i18NString}));
    }

    /* renamed from: r */
    private void m16088r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3274LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m16089rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f3274LR);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Render")
    @C5566aOg
    /* renamed from: I */
    public void mo9817I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m16074H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Render")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m16075Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6686ari(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m16081ap();
            case 1:
                m16084b((UUID) args[0]);
                return null;
            case 2:
                return m16075Nt();
            case 3:
                m16074H((Asset) args[0]);
                return null;
            case 4:
                m16087nL((I18NString) args[0]);
                return null;
            case 5:
                return cZd();
            case 6:
                return m16082ar();
            case 7:
                m16083b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3278bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3278bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3278bN, new Object[0]));
                break;
        }
        return m16081ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Name")
    public I18NString cZe() {
        switch (bFf().mo6893i(hJL)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hJL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hJL, new Object[0]));
                break;
        }
        return cZd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3280bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3280bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3280bP, new Object[0]));
                break;
        }
        return m16082ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3281bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3281bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3281bQ, new Object[]{str}));
                break;
        }
        m16083b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Name")
    @C5566aOg
    /* renamed from: nM */
    public void mo9819nM(I18NString i18NString) {
        switch (bFf().mo6893i(hJK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hJK, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hJK, new Object[]{i18NString}));
                break;
        }
        m16087nL(i18NString);
    }

    @C0064Am(aul = "d4dea7c2d5bc5db632123b5c9e8f45d5", aum = 0)
    /* renamed from: ap */
    private UUID m16081ap() {
        return m16079an();
    }

    @C0064Am(aul = "81f0e04d5e75411d704571b8983a413a", aum = 0)
    /* renamed from: b */
    private void m16084b(UUID uuid) {
        m16078a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m16078a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Render")
    @C0064Am(aul = "7db16e10eef40dc6a0fbc6f939f0654f", aum = 0)
    /* renamed from: Nt */
    private Asset m16075Nt() {
        return m16089rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Name")
    @C0064Am(aul = "30e1860c15a99d3a6983ebd653badf89", aum = 0)
    private I18NString cZd() {
        return cZc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "3fcfe89c235b0b120b94ef020bca894d", aum = 0)
    /* renamed from: ar */
    private String m16082ar() {
        return m16080ao();
    }
}
