package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C0922NW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.NX */
/* compiled from: a */
public class ScriptableMissionTemplateObjective extends aDJ implements C0468GU, C1616Xf, C2780jt {

    /* renamed from: QE */
    public static final C2491fm f1241QE = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aGq = null;
    public static final C2491fm aGt = null;
    public static final C2491fm aGw = null;
    /* renamed from: bL */
    public static final C5663aRz f1243bL = null;
    /* renamed from: bN */
    public static final C2491fm f1244bN = null;
    /* renamed from: bO */
    public static final C2491fm f1245bO = null;
    /* renamed from: bP */
    public static final C2491fm f1246bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1247bQ = null;
    public static final C2491fm bin = null;
    public static final C2491fm bip = null;
    public static final C5663aRz dII = null;
    public static final C5663aRz dIJ = null;
    public static final C2491fm dIK = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f6de47af42ffa7ce5ee8341ecc3358a0", aum = 3)

    /* renamed from: bK */
    private static UUID f1242bK;
    @C0064Am(aul = "c16fadb4690e6387ac721bf6cee661bd", aum = 1)

    /* renamed from: cZ */
    private static int f1248cZ;
    @C0064Am(aul = "789e211f2fdf42f289cebbf9b0a3d2be", aum = 0)
    private static String dIA;
    @C0064Am(aul = "679adc9269140abb5dcd12c3398061da", aum = 2)
    private static ScriptableMissionTemplate dIB;

    static {
        m7664V();
    }

    public ScriptableMissionTemplateObjective() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScriptableMissionTemplateObjective(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7664V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 10;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ScriptableMissionTemplateObjective.class, "789e211f2fdf42f289cebbf9b0a3d2be", i);
        dII = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ScriptableMissionTemplateObjective.class, "c16fadb4690e6387ac721bf6cee661bd", i2);
        aGq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ScriptableMissionTemplateObjective.class, "679adc9269140abb5dcd12c3398061da", i3);
        dIJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ScriptableMissionTemplateObjective.class, "f6de47af42ffa7ce5ee8341ecc3358a0", i4);
        f1243bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "9566a20a972c8c96cb7ae6dccee9cb12", i6);
        f1244bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "921a002dae72bcdd375522d358e818df", i7);
        f1245bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "fa8be04d695c5588d024e8f85a5cfb92", i8);
        f1246bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "1b46080dd094d16152b7bd2b3053e283", i9);
        f1247bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "42463fa98351cce90b4b1809efd18f2a", i10);
        aGt = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "1255375f92089808e050e7befbcf124f", i11);
        aGw = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "0aca317a9bb1ab07cecc6355622509c8", i12);
        f1241QE = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "d229aa393e9360bfbdb70c14d316e95e", i13);
        bip = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "65f5dcddf511fc8b01255edaddf61b99", i14);
        bin = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ScriptableMissionTemplateObjective.class, "f8047020fce15997e032ef2f15e04e4f", i15);
        dIK = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScriptableMissionTemplateObjective.class, C0922NW.class, _m_fields, _m_methods);
    }

    /* renamed from: PF */
    private int m7662PF() {
        return bFf().mo5608dq().mo3212n(aGq);
    }

    /* renamed from: a */
    private void m7665a(ScriptableMissionTemplate awi) {
        bFf().mo5608dq().mo3197f(dIJ, awi);
    }

    /* renamed from: a */
    private void m7666a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1243bL, uuid);
    }

    /* renamed from: an */
    private UUID m7667an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1243bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "String Handle")
    @C0064Am(aul = "1b46080dd094d16152b7bd2b3053e283", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m7671b(String str) {
        throw new aWi(new aCE(this, f1247bQ, new Object[]{str}));
    }

    private String bkV() {
        return (String) bFf().mo5608dq().mo3214p(dII);
    }

    private ScriptableMissionTemplate bkW() {
        return (ScriptableMissionTemplate) bFf().mo5608dq().mo3214p(dIJ);
    }

    /* renamed from: c */
    private void m7673c(UUID uuid) {
        switch (bFf().mo6893i(f1245bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1245bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1245bO, new Object[]{uuid}));
                break;
        }
        m7672b(uuid);
    }

    /* renamed from: cZ */
    private void m7674cZ(int i) {
        bFf().mo5608dq().mo3183b(aGq, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Quantity")
    @C0064Am(aul = "1255375f92089808e050e7befbcf124f", aum = 0)
    @C5566aOg
    /* renamed from: da */
    private void m7675da(int i) {
        throw new aWi(new aCE(this, aGw, new Object[]{new Integer(i)}));
    }

    /* renamed from: gc */
    private void m7676gc(String str) {
        bFf().mo5608dq().mo3197f(dII, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Quantity")
    /* renamed from: Iq */
    public int mo4241Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m7663PK();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0922NW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m7668ap();
            case 1:
                m7672b((UUID) args[0]);
                return null;
            case 2:
                return m7669ar();
            case 3:
                m7671b((String) args[0]);
                return null;
            case 4:
                return new Integer(m7663PK());
            case 5:
                m7675da(((Integer) args[0]).intValue());
                return null;
            case 6:
                return m7677vV();
            case 7:
                return abT();
            case 8:
                return new Boolean(abS());
            case 9:
                m7670b((ScriptableMissionTemplate) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1244bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1244bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1244bN, new Object[0]));
                break;
        }
        return m7668ap();
    }

    /* renamed from: c */
    public void mo4242c(ScriptableMissionTemplate awi) {
        switch (bFf().mo6893i(dIK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dIK, new Object[]{awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dIK, new Object[]{awi}));
                break;
        }
        m7670b(awi);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Quantity")
    @C5566aOg
    /* renamed from: db */
    public void mo4243db(int i) {
        switch (bFf().mo6893i(aGw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                break;
        }
        m7675da(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "String Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1246bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1246bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1246bP, new Object[0]));
                break;
        }
        return m7669ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "String Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1247bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1247bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1247bQ, new Object[]{str}));
                break;
        }
        m7671b(str);
    }

    public String getInfo() {
        switch (bFf().mo6893i(bip)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bip, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bip, new Object[0]));
                break;
        }
        return abT();
    }

    public boolean isHidden() {
        switch (bFf().mo6893i(bin)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bin, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bin, new Object[0]));
                break;
        }
        return abS();
    }

    /* renamed from: vW */
    public I18NString mo4247vW() {
        switch (bFf().mo6893i(f1241QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1241QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1241QE, new Object[0]));
                break;
        }
        return m7677vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "9566a20a972c8c96cb7ae6dccee9cb12", aum = 0)
    /* renamed from: ap */
    private UUID m7668ap() {
        return m7667an();
    }

    @C0064Am(aul = "921a002dae72bcdd375522d358e818df", aum = 0)
    /* renamed from: b */
    private void m7672b(UUID uuid) {
        m7666a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "String Handle")
    @C0064Am(aul = "fa8be04d695c5588d024e8f85a5cfb92", aum = 0)
    /* renamed from: ar */
    private String m7669ar() {
        return bkV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Quantity")
    @C0064Am(aul = "42463fa98351cce90b4b1809efd18f2a", aum = 0)
    /* renamed from: PK */
    private int m7663PK() {
        return m7662PF();
    }

    @C0064Am(aul = "0aca317a9bb1ab07cecc6355622509c8", aum = 0)
    /* renamed from: vV */
    private I18NString m7677vV() {
        I18NStringTable bIU;
        if (bkW() == null || (bIU = bkW().bIU()) == null) {
            return null;
        }
        return bIU.mo6668hs(bkV());
    }

    @C0064Am(aul = "d229aa393e9360bfbdb70c14d316e95e", aum = 0)
    private String abT() {
        return String.valueOf(String.valueOf(0)) + C0147Bi.SEPARATOR + String.valueOf(m7662PF());
    }

    @C0064Am(aul = "65f5dcddf511fc8b01255edaddf61b99", aum = 0)
    private boolean abS() {
        return false;
    }

    @C0064Am(aul = "f8047020fce15997e032ef2f15e04e4f", aum = 0)
    /* renamed from: b */
    private void m7670b(ScriptableMissionTemplate awi) {
        m7665a(awi);
    }
}
