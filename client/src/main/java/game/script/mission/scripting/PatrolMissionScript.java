package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.mission.TableNPCSpawn;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.WaypointHelper;
import game.script.missiontemplate.scripting.PatrolMissionScriptTemplate;
import game.script.npc.NPCType;
import game.script.ship.Ship;
import logic.baa.*;
import logic.data.mbean.C3133oJ;
import logic.data.mbean.C3923wp;
import logic.data.mbean.C5428aIy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.mission.PatrolMission")
@C6485anp
@C5511aMd
/* renamed from: a.axo  reason: case insensitive filesystem */
/* compiled from: a */
public class PatrolMissionScript<T extends PatrolMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f5585Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gPC = null;
    public static final C5663aRz gPD = null;
    public static final C5663aRz gPE = null;
    public static final C5663aRz gPF = null;
    public static final C5663aRz gPG = null;
    public static final C5663aRz gPH = null;
    public static final C5663aRz gPI = null;
    public static final String gPy = "patrolMissionWaypoint";
    public static final String gPz = "patrolMissionKilling";
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f5586yH = null;
    public static final String gPA = "primarPatrolMissionCreepNpc";
    public static final String gPB = "primaryPatrolMissionCreepNpc";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "8f88b431ecaa02f01a31eb02609601ba", aum = 0)
    private static int bCN = 0;
    @C0064Am(aul = "98f621366dbf71fdfefcf2df0469a67c", aum = 1)
    private static boolean bCO = false;
    @C0064Am(aul = "9860115c0764fd786552791167435824", aum = 2)
    private static int bCP = 0;
    @C0064Am(aul = "a34f1131bdb211f41eae48314f9f847d", aum = 3)
    private static int bCQ = 0;
    @C0064Am(aul = "84297cc03c9342a88936944cbc171b68", aum = 4)
    private static int bCR = 0;
    @C0064Am(aul = "33e220cf25cde25f272c580b68d6cb66", aum = 5)
    private static int bCS = 0;
    @C0064Am(aul = "51188bb3308b57ea61098b8feeef2b6a", aum = 6)
    private static Vec3d bCT = null;

    static {
        m27179V();
    }

    public PatrolMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PatrolMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27179V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 7;
        _m_methodCount = MissionScript._m_methodCount + 2;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(PatrolMissionScript.class, "8f88b431ecaa02f01a31eb02609601ba", i);
        gPC = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PatrolMissionScript.class, "98f621366dbf71fdfefcf2df0469a67c", i2);
        gPD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PatrolMissionScript.class, "9860115c0764fd786552791167435824", i3);
        gPE = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PatrolMissionScript.class, "a34f1131bdb211f41eae48314f9f847d", i4);
        gPF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PatrolMissionScript.class, "84297cc03c9342a88936944cbc171b68", i5);
        gPG = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(PatrolMissionScript.class, "33e220cf25cde25f272c580b68d6cb66", i6);
        gPH = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(PatrolMissionScript.class, "51188bb3308b57ea61098b8feeef2b6a", i7);
        gPI = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i9 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 2)];
        C2491fm a = C4105zY.m41624a(PatrolMissionScript.class, "1d447a55fcdc9f41ff8e84ce30bf7e85", i9);
        f5586yH = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(PatrolMissionScript.class, "187eca8e5e3e99237a5eb3de96382482", i10);
        f5585Do = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PatrolMissionScript.class, C3923wp.class, _m_fields, _m_methods);
    }

    /* access modifiers changed from: private */
    /* renamed from: ae */
    public void m27187ae(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(gPI, ajr);
    }

    /* access modifiers changed from: private */
    public int cCA() {
        return bFf().mo5608dq().mo3212n(gPC);
    }

    /* access modifiers changed from: private */
    public boolean cCB() {
        return bFf().mo5608dq().mo3201h(gPD);
    }

    /* access modifiers changed from: private */
    public int cCC() {
        return bFf().mo5608dq().mo3212n(gPE);
    }

    /* access modifiers changed from: private */
    public int cCD() {
        return bFf().mo5608dq().mo3212n(gPF);
    }

    /* access modifiers changed from: private */
    public int cCE() {
        return bFf().mo5608dq().mo3212n(gPG);
    }

    /* access modifiers changed from: private */
    public int cCF() {
        return bFf().mo5608dq().mo3212n(gPH);
    }

    /* access modifiers changed from: private */
    public Vec3d cCG() {
        return (Vec3d) bFf().mo5608dq().mo3214p(gPI);
    }

    /* access modifiers changed from: private */
    /* renamed from: hs */
    public void m27198hs(boolean z) {
        bFf().mo5608dq().mo3153a(gPD, z);
    }

    /* access modifiers changed from: private */
    /* renamed from: vO */
    public void m27200vO(int i) {
        bFf().mo5608dq().mo3183b(gPC, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: vP */
    public void m27201vP(int i) {
        bFf().mo5608dq().mo3183b(gPE, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: vQ */
    public void m27202vQ(int i) {
        bFf().mo5608dq().mo3183b(gPF, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: vR */
    public void m27203vR(int i) {
        bFf().mo5608dq().mo3183b(gPG, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: vS */
    public void m27204vS(int i) {
        bFf().mo5608dq().mo3183b(gPH, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3923wp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m27199jF();
                return null;
            case 1:
                m27181a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f5585Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5585Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5585Do, new Object[]{jt}));
                break;
        }
        m27181a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f5586yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5586yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5586yH, new Object[0]));
                break;
        }
        m27199jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "1d447a55fcdc9f41ff8e84ce30bf7e85", aum = 0)
    /* renamed from: jF */
    private void m27199jF() {
        m27200vO(-1);
        m27202vQ(0);
        m27204vS(0);
        mo575p(StateA.class);
    }

    @C0064Am(aul = "187eca8e5e3e99237a5eb3de96382482", aum = 0)
    /* renamed from: a */
    private void m27181a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && jt.get("killCount") != null) {
            m27201vP(((Integer) jt.get("killCount")).intValue());
            m27203vR(((Integer) jt.get("npcSpawnedAmmount")).intValue());
            m27200vO(((Integer) jt.get("waypointCounter")).intValue());
            if (azm() != null) {
                return;
            }
            if (cCA() == -1) {
                mo576r(StateA.class);
            } else {
                mo576r(StateB.class);
            }
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.axo$b */
    /* compiled from: a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f5588aT = null;
        public static final C2491fm beE = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "64d0d83ef43160d06c28d7a20882621c", aum = 0)
        static /* synthetic */ PatrolMissionScript aPU;

        static {
            m27231V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        StateA(PatrolMissionScript axo) {
            super((C5540aNg) null);
            super._m_script_init(axo);
        }

        /* renamed from: V */
        static void m27231V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(PatrolMissionScript.StateA.class, "64d0d83ef43160d06c28d7a20882621c", i);
            f5588aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(PatrolMissionScript.StateA.class, "36d3b7db9d707df6303f598a0b8549eb", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(PatrolMissionScript.StateA.class, C5428aIy.class, _m_fields, _m_methods);
        }

        private PatrolMissionScript cZT() {
            return (PatrolMissionScript) bFf().mo5608dq().mo3214p(f5588aT);
        }

        /* renamed from: h */
        private void m27232h(PatrolMissionScript axo) {
            bFf().mo5608dq().mo3197f(f5588aT, axo);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5428aIy(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: i */
        public void mo16836i(PatrolMissionScript axo) {
            m27232h(axo);
            super.mo10S();
        }

        @C0064Am(aul = "36d3b7db9d707df6303f598a0b8549eb", aum = 0)
        private void aai() {
            if (cZT().cCA() >= 0) {
                cZT().mo128iZ("patrolMissionWaypoint" + cZT().cCA());
            }
            PatrolMissionScript cZT = cZT();
            cZT.m27200vO(cZT.cCA() + 1);
            if (cZT().cCA() == ((PatrolMissionScriptTemplate) cZT().cdp()).mo20544Ne().size()) {
                cZT().cdk();
                return;
            }
            cZT().m27201vP(0);
            cZT().m27203vR(0);
            WaypointDat cDVar = ((PatrolMissionScriptTemplate) cZT().cdp()).mo20544Ne().get(cZT().cCA());
            if (cZT().cCG() == null) {
                cZT().m27187ae(cDVar.getPosition());
            } else {
                cZT().m27187ae(WaypointHelper.m2400a(cDVar, cZT().cCG()));
            }
            cZT().mo92b("patrolMissionWaypoint" + cZT().cCA(), cZT().cCG(), ((PatrolMissionScriptTemplate) cZT().cdp()).mo20543Nc(), cDVar.aMJ(), true, cDVar.alW());
            cZT().m27198hs(false);
            cZT().mo575p(StateB.class);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.axo$a */
    public class StateB extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C2491fm aMe = null;
        /* renamed from: aT */
        public static final C5663aRz f5587aT = null;
        public static final C2491fm beF = null;
        public static final C2491fm beG = null;
        public static final C2491fm byI = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "f5d1e9cd1ea55f9f73ab503c36d6db6d", aum = 0)
        static /* synthetic */ PatrolMissionScript aPU;

        static {
            m27213V();
        }

        public StateB() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateB(C5540aNg ang) {
            super(ang);
        }

        StateB(PatrolMissionScript axo) {
            super((C5540aNg) null);
            super._m_script_init(axo);
        }

        /* renamed from: V */
        static void m27213V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 4;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(PatrolMissionScript.StateB.class, "f5d1e9cd1ea55f9f73ab503c36d6db6d", i);
            f5587aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
            C2491fm a = C4105zY.m41624a(PatrolMissionScript.StateB.class, "26d5b7ee7ef5d3d5bc49b3dfdcd7c80d", i3);
            beF = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(PatrolMissionScript.StateB.class, "e279536dec0c8759daa4d7e2232d5ca2", i4);
            aMe = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(PatrolMissionScript.StateB.class, "501e02121acb14a40e60553e8cce0431", i5);
            byI = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(PatrolMissionScript.StateB.class, "48579e3b86e630937c58de8beb2a068a", i6);
            beG = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(PatrolMissionScript.StateB.class, C3133oJ.class, _m_fields, _m_methods);
        }

        private PatrolMissionScript cZT() {
            return (PatrolMissionScript) bFf().mo5608dq().mo3214p(f5587aT);
        }

        /* renamed from: h */
        private void m27216h(PatrolMissionScript axo) {
            bFf().mo5608dq().mo3197f(f5587aT, axo);
        }

        @C0064Am(aul = "501e02121acb14a40e60553e8cce0431", aum = 0)
        @C5566aOg
        /* renamed from: w */
        private void m27218w(Ship fAVar) {
            throw new aWi(new aCE(this, byI, new Object[]{fAVar}));
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3133oJ(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    return new Boolean(m27215bk((String) args[0]));
                case 1:
                    m27217i((Ship) args[0]);
                    return null;
                case 2:
                    m27218w((Ship) args[0]);
                    return null;
                case 3:
                    m27214a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m27214a(str, fAVar, fAVar2);
        }

        /* renamed from: bl */
        public boolean mo4962bl(String str) {
            switch (bFf().mo6893i(beF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                    break;
            }
            return m27215bk(str);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: j */
        public void mo12264j(Ship fAVar) {
            switch (bFf().mo6893i(aMe)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                    break;
            }
            m27217i(fAVar);
        }

        @C5566aOg
        /* renamed from: x */
        public void mo12270x(Ship fAVar) {
            switch (bFf().mo6893i(byI)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                    break;
            }
            m27218w(fAVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: i */
        public void mo16835i(PatrolMissionScript axo) {
            m27216h(axo);
            super.mo10S();
        }

        @C0064Am(aul = "26d5b7ee7ef5d3d5bc49b3dfdcd7c80d", aum = 0)
        /* renamed from: bk */
        private boolean m27215bk(String str) {
            if (!cZT().cCB() && ("patrolMissionWaypoint" + cZT().cCA()).equals(str)) {
                cZT().m27198hs(true);
                cZT().mo574k("patrolMissionWaypoint", cZT().cCA() + 1);
                if (cZT().cCA() + 1 >= ((PatrolMissionScriptTemplate) cZT().cdp()).mo20544Ne().size()) {
                    cZT().mo566b("patrolMissionWaypoint", Mission.C0015a.ACCOMPLISHED);
                    cZT().mo568dJ("patrolMissionKilling");
                }
                WaypointDat cDVar = ((PatrolMissionScriptTemplate) cZT().cdp()).mo20544Ne().get(cZT().cCA());
                TableNPCSpawn bvw = cDVar.bvw();
                if (bvw != null) {
                    for (int i = 0; i < cDVar.getAmount(); i++) {
                        for (NPCType b : bvw.bvC()) {
                            PatrolMissionScript cZT = cZT();
                            StringBuilder sb = new StringBuilder(PatrolMissionScript.gPA);
                            PatrolMissionScript cZT2 = cZT();
                            int d = cZT2.cCD();
                            cZT2.m27202vQ(d + 1);
                            cZT.mo563b(sb.append(d).toString(), b, cZT().cCG(), (float) cDVar.aMJ(), cZT().cCG(), (float) cDVar.aMJ(), ((PatrolMissionScriptTemplate) cZT().cdp()).mo20543Nc(), cZT().cdr().bQx().agj());
                            PatrolMissionScript cZT3 = cZT();
                            cZT3.m27203vR(cZT3.cCE() + 1);
                        }
                    }
                }
                TableNPCSpawn bvI = cDVar.bvI();
                if (bvI != null) {
                    for (int i2 = 0; i2 < cDVar.getAmount(); i2++) {
                        for (NPCType b2 : bvI.bvC()) {
                            PatrolMissionScript cZT4 = cZT();
                            StringBuilder sb2 = new StringBuilder(PatrolMissionScript.gPB);
                            PatrolMissionScript cZT5 = cZT();
                            int d2 = cZT5.cCD();
                            cZT5.m27202vQ(d2 + 1);
                            cZT4.mo563b(sb2.append(d2).toString(), b2, cZT().cCG(), (float) cDVar.aMJ(), cZT().cCG(), (float) cDVar.aMJ(), ((PatrolMissionScriptTemplate) cZT().cdp()).mo20543Nc(), cZT().cdr().bQx().agj());
                        }
                    }
                }
                if (cZT().cCE() == 0) {
                    cZT().mo575p(StateA.class);
                }
            }
            return false;
        }

        @C0064Am(aul = "e279536dec0c8759daa4d7e2232d5ca2", aum = 0)
        /* renamed from: i */
        private void m27217i(Ship fAVar) {
            if (!fAVar.getName().startsWith(PatrolMissionScript.gPB) && fAVar.getName().startsWith(PatrolMissionScript.gPA)) {
                PatrolMissionScript cZT = cZT();
                cZT.m27201vP(cZT.cCC() + 1);
                PatrolMissionScript cZT2 = cZT();
                cZT2.m27204vS(cZT2.cCF() + 1);
                cZT().mo574k("patrolMissionKilling", cZT().cCF());
                if (cZT().cCF() >= ((PatrolMissionScriptTemplate) cZT().cdp()).mo20545Pr()) {
                    cZT().mo566b("patrolMissionKilling", Mission.C0015a.ACCOMPLISHED);
                }
                if (cZT().cCC() == cZT().cCE()) {
                    cZT().mo575p(StateA.class);
                }
            }
        }

        @C0064Am(aul = "48579e3b86e630937c58de8beb2a068a", aum = 0)
        /* renamed from: a */
        private void m27214a(String str, Ship fAVar, Ship fAVar2) {
            if (!str.startsWith(PatrolMissionScript.gPB) && str.startsWith(PatrolMissionScript.gPA)) {
                WaypointDat cDVar = ((PatrolMissionScriptTemplate) cZT().cdp()).mo20544Ne().get(cZT().cCA());
                if (!((PatrolMissionScriptTemplate) cZT().cdp()).mo20547Pv()) {
                    PatrolMissionScript cZT = cZT();
                    cZT.m27201vP(cZT.cCC() + 1);
                    PatrolMissionScript cZT2 = cZT();
                    cZT2.m27204vS(cZT2.cCF() + 1);
                    cZT().mo574k("patrolMissionKilling", cZT().cCF());
                    if (cZT().cCA() + 1 >= ((PatrolMissionScriptTemplate) cZT().cdp()).mo20544Ne().size()) {
                        cZT().mo566b("patrolMissionWaypoint", Mission.C0015a.ACCOMPLISHED);
                    }
                } else if (fAVar == cZT().cdr().bQx()) {
                    PatrolMissionScript cZT3 = cZT();
                    cZT3.m27201vP(cZT3.cCC() + 1);
                    PatrolMissionScript cZT4 = cZT();
                    cZT4.m27204vS(cZT4.cCF() + 1);
                    cZT().mo574k("patrolMissionKilling", cZT().cCF());
                    if (cZT().cCA() + 1 >= ((PatrolMissionScriptTemplate) cZT().cdp()).mo20544Ne().size()) {
                        cZT().mo566b("patrolMissionWaypoint", Mission.C0015a.ACCOMPLISHED);
                    }
                } else {
                    PatrolMissionScript cZT5 = cZT();
                    StringBuilder sb = new StringBuilder(PatrolMissionScript.gPA);
                    PatrolMissionScript cZT6 = cZT();
                    int d = cZT6.cCD();
                    cZT6.m27202vQ(d + 1);
                    cZT5.mo563b(sb.append(d).toString(), cDVar.bvw().bvA(), cZT().cCG(), (float) cDVar.aMJ(), cZT().cCG(), (float) cDVar.aMJ(), ((PatrolMissionScriptTemplate) cZT().cdp()).mo20543Nc(), cZT().cdr().bQx().agj());
                }
                if (cZT().cCC() == cZT().cCE()) {
                    cZT().mo575p(StateA.class);
                }
            }
        }
    }
}
