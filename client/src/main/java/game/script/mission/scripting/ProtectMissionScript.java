package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.ProtectMissionScriptTemplate;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1924ak;
import logic.data.mbean.C5478aKw;
import logic.data.mbean.C6773atR;
import logic.data.mbean.aQG;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.game.script.p003ai.npc.DroneAIController;
import taikodom.infra.script.I18NString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@C6485anp
@C5511aMd
/* renamed from: a.dl */
/* compiled from: a */
public class ProtectMissionScript<T extends ProtectMissionScriptTemplate> extends MissionScript<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yC */
    public static final C5663aRz f6593yC = null;
    /* renamed from: yD */
    public static final C5663aRz f6594yD = null;
    /* renamed from: yE */
    public static final C5663aRz f6595yE = null;
    /* renamed from: yF */
    public static final C5663aRz f6596yF = null;
    /* renamed from: yG */
    public static final C5663aRz f6597yG = null;
    /* renamed from: yH */
    public static final C2491fm f6598yH = null;
    /* renamed from: yA */
    private static final String f6591yA = "escort_npc";

    /* renamed from: yB */
    private static final String f6592yB = "attacker_npc";
    /* renamed from: yy */
    private static final String f6599yy = "protect_waypoint";
    /* renamed from: yz */
    private static final String f6600yz = "protected_npc";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "5ad3574595545cd772b4ca108740c25e", aum = 0)

    /* renamed from: hU */
    private static C1556Wo<String, NPC> f6586hU = null;
    @C0064Am(aul = "718cae164cd84abc64cc926b74cbdfb6", aum = 1)

    /* renamed from: hV */
    private static C1556Wo<String, NPC> f6587hV = null;
    @C0064Am(aul = "44379cad1b6986cbc073fcb1d7403034", aum = 2)

    /* renamed from: hW */
    private static C1556Wo<String, NPC> f6588hW = null;
    @C0064Am(aul = "afa2c18b485ee82344b344bb9bc16294", aum = 3)

    /* renamed from: hX */
    private static int f6589hX = 0;
    @C0064Am(aul = "5f3d723f90095ef278ea35e0e5a302db", aum = 4)

    /* renamed from: hY */
    private static int f6590hY = 0;

    static {
        m29186V();
    }

    public ProtectMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProtectMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m29186V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 5;
        _m_methodCount = MissionScript._m_methodCount + 1;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ProtectMissionScript.class, "5ad3574595545cd772b4ca108740c25e", i);
        f6593yC = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProtectMissionScript.class, "718cae164cd84abc64cc926b74cbdfb6", i2);
        f6594yD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProtectMissionScript.class, "44379cad1b6986cbc073fcb1d7403034", i3);
        f6595yE = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProtectMissionScript.class, "afa2c18b485ee82344b344bb9bc16294", i4);
        f6596yF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProtectMissionScript.class, "5f3d723f90095ef278ea35e0e5a302db", i5);
        f6597yG = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i7 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 1)];
        C2491fm a = C4105zY.m41624a(ProtectMissionScript.class, "d72c8669b8ed9f925aab3eabb5c58d81", i7);
        f6598yH = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProtectMissionScript.class, C1924ak.class, _m_fields, _m_methods);
    }

    /* access modifiers changed from: private */
    /* renamed from: ad */
    public void m29193ad(int i) {
        bFf().mo5608dq().mo3183b(f6596yF, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: ae */
    public void m29194ae(int i) {
        bFf().mo5608dq().mo3183b(f6597yG, i);
    }

    /* renamed from: c */
    private void m29198c(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f6593yC, wo);
    }

    /* renamed from: d */
    private void m29200d(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f6594yD, wo);
    }

    /* renamed from: e */
    private void m29202e(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f6595yE, wo);
    }

    /* access modifiers changed from: private */
    /* renamed from: jA */
    public C1556Wo m29203jA() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f6593yC);
    }

    /* access modifiers changed from: private */
    /* renamed from: jB */
    public C1556Wo m29204jB() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f6594yD);
    }

    /* access modifiers changed from: private */
    /* renamed from: jC */
    public C1556Wo m29205jC() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f6595yE);
    }

    /* access modifiers changed from: private */
    /* renamed from: jD */
    public int m29206jD() {
        return bFf().mo5608dq().mo3212n(f6596yF);
    }

    /* access modifiers changed from: private */
    /* renamed from: jE */
    public int m29207jE() {
        return bFf().mo5608dq().mo3212n(f6597yG);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1924ak(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m29208jF();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f6598yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6598yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6598yH, new Object[0]));
                break;
        }
        m29208jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "d72c8669b8ed9f925aab3eabb5c58d81", aum = 0)
    /* renamed from: jF */
    private void m29208jF() {
        mo575p(StateA.class);
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.dl$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final C2491fm _f_tick_0020_0028F_0029V = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6601aT = null;
        public static final C2491fm beE = null;
        public static final C5663aRz dbL = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "fcf366ce566faaf17b5c1ab0a8b252ae", aum = 1)
        static /* synthetic */ ProtectMissionScript dbM;
        @C0064Am(aul = "bd5aedd1a44b331e18bcf71d6c884706", aum = 0)
        private static float dbK;

        static {
            m29217V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        StateA(ProtectMissionScript dlVar) {
            super((C5540aNg) null);
            super._m_script_init(dlVar);
        }

        /* renamed from: V */
        static void m29217V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 2;
            _m_methodCount = MissionScriptState._m_methodCount + 2;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 2)];
            C5663aRz b = C5640aRc.m17844b(ProtectMissionScript.StateA.class, "bd5aedd1a44b331e18bcf71d6c884706", i);
            dbL = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(ProtectMissionScript.StateA.class, "fcf366ce566faaf17b5c1ab0a8b252ae", i2);
            f6601aT = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i4 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i4 + 2)];
            C2491fm a = C4105zY.m41624a(ProtectMissionScript.StateA.class, "25fd1c6a9da4ece1371b3e34de0cd193", i4);
            beE = a;
            fmVarArr[i4] = a;
            int i5 = i4 + 1;
            C2491fm a2 = C4105zY.m41624a(ProtectMissionScript.StateA.class, "c01b954a072238d90a7004254101acc8", i5);
            _f_tick_0020_0028F_0029V = a2;
            fmVarArr[i5] = a2;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ProtectMissionScript.StateA.class, C5478aKw.class, _m_fields, _m_methods);
        }

        private float aRD() {
            return bFf().mo5608dq().mo3211m(dbL);
        }

        private ProtectMissionScript aRE() {
            return (ProtectMissionScript) bFf().mo5608dq().mo3214p(f6601aT);
        }

        /* renamed from: f */
        private void m29218f(ProtectMissionScript dlVar) {
            bFf().mo5608dq().mo3197f(f6601aT, dlVar);
        }

        /* renamed from: fs */
        private void m29219fs(float f) {
            bFf().mo5608dq().mo3150a(dbL, f);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: V */
        public void mo4709V(float f) {
            switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                    break;
            }
            m29216U(f);
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5478aKw(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m29216U(((Float) args[0]).floatValue());
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: g */
        public void mo17903g(ProtectMissionScript dlVar) {
            m29218f(dlVar);
            super.mo10S();
        }

        @C0064Am(aul = "25fd1c6a9da4ece1371b3e34de0cd193", aum = 0)
        private void aai() {
            m29219fs(((ProtectMissionScriptTemplate) aRE().cdp()).aEU());
            WaypointDat aEQ = ((ProtectMissionScriptTemplate) aRE().cdp()).aEQ();
            aRE().mo92b(ProtectMissionScript.f6599yy, aEQ.getPosition(), ((ProtectMissionScriptTemplate) aRE().cdp()).mo1678Nc(), aEQ.aMJ(), true, aEQ.alW());
            if (aRD() == 0.0f) {
                aRE().mo575p(StateB.class);
            } else {
                mo8361ms(1.0f);
            }
        }

        @C0064Am(aul = "c01b954a072238d90a7004254101acc8", aum = 0)
        /* renamed from: U */
        private void m29216U(float f) {
            I18NString aEM;
            super.mo4709V(f);
            if (aRE().isActive()) {
                m29219fs(aRD() - f);
                if (aHI.m15140mH(aRD()) && ((ProtectMissionScriptTemplate) aRE().cdp()).aFe() && (aEM = ((ProtectMissionScriptTemplate) aRE().cdp()).aEM()) != null) {
                    int round = Math.round(aRD());
                    int i = round / 60;
                    int i2 = round % 60;
                    I18NString rP = ((ProtectMissionScriptTemplate) aRE().cdp()).mo711rP();
                    aRE().mo122f(aEM, rP, Integer.valueOf(i), Integer.valueOf(i2));
                }
                if (aRD() > 0.0f) {
                    mo8361ms(1.0f);
                } else {
                    aRE().mo575p(StateB.class);
                }
            }
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.dl$b */
            /* compiled from: a */
    class StateB extends MissionScriptState implements C1616Xf {
        public static final C2491fm _f_tick_0020_0028F_0029V = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6602aT = null;
        public static final C2491fm beE = null;
        public static final C2491fm beG = null;
        public static final C2491fm byI = null;
        public static final C5663aRz dbO = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "e7ef5585b7a4f1551ba6415f14885247", aum = 1)
        static /* synthetic */ ProtectMissionScript dbM;
        @C0064Am(aul = "cb9e671a911a800f1a9c6dbdc0c8c2ce", aum = 0)
        private static float dbN;

        static {
            m29230V();
        }

        public StateB() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateB(C5540aNg ang) {
            super(ang);
        }

        StateB(ProtectMissionScript dlVar) {
            super((C5540aNg) null);
            super._m_script_init(dlVar);
        }

        /* renamed from: V */
        static void m29230V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 2;
            _m_methodCount = MissionScriptState._m_methodCount + 4;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 2)];
            C5663aRz b = C5640aRc.m17844b(ProtectMissionScript.StateB.class, "cb9e671a911a800f1a9c6dbdc0c8c2ce", i);
            dbO = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(ProtectMissionScript.StateB.class, "e7ef5585b7a4f1551ba6415f14885247", i2);
            f6602aT = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i4 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
            C2491fm a = C4105zY.m41624a(ProtectMissionScript.StateB.class, "35cc9a594ed4f3b2b81c523795155105", i4);
            beE = a;
            fmVarArr[i4] = a;
            int i5 = i4 + 1;
            C2491fm a2 = C4105zY.m41624a(ProtectMissionScript.StateB.class, "25dd1d69d1e189ba6be7654eb2679daf", i5);
            _f_tick_0020_0028F_0029V = a2;
            fmVarArr[i5] = a2;
            int i6 = i5 + 1;
            C2491fm a3 = C4105zY.m41624a(ProtectMissionScript.StateB.class, "fb61e642b370f66457bb8c1288f0c146", i6);
            beG = a3;
            fmVarArr[i6] = a3;
            int i7 = i6 + 1;
            C2491fm a4 = C4105zY.m41624a(ProtectMissionScript.StateB.class, "97b3b150fe5b18b723339d0f083df2e8", i7);
            byI = a4;
            fmVarArr[i7] = a4;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ProtectMissionScript.StateB.class, C6773atR.class, _m_fields, _m_methods);
        }

        private ProtectMissionScript aRE() {
            return (ProtectMissionScript) bFf().mo5608dq().mo3214p(f6602aT);
        }

        private float aRH() {
            return bFf().mo5608dq().mo3211m(dbO);
        }

        /* renamed from: f */
        private void m29232f(ProtectMissionScript dlVar) {
            bFf().mo5608dq().mo3197f(f6602aT, dlVar);
        }

        /* renamed from: ft */
        private void m29233ft(float f) {
            bFf().mo5608dq().mo3150a(dbO, f);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: V */
        public void mo4709V(float f) {
            switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                    break;
            }
            m29229U(f);
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6773atR(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m29229U(((Float) args[0]).floatValue());
                    return null;
                case 2:
                    m29231a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                case 3:
                    m29234w((Ship) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m29231a(str, fAVar, fAVar2);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: x */
        public void mo12270x(Ship fAVar) {
            switch (bFf().mo6893i(byI)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                    break;
            }
            m29234w(fAVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: g */
        public void mo17904g(ProtectMissionScript dlVar) {
            m29232f(dlVar);
            super.mo10S();
        }

        @C0064Am(aul = "35cc9a594ed4f3b2b81c523795155105", aum = 0)
        private void aai() {
            I18NString aEO;
            m29233ft(((ProtectMissionScriptTemplate) aRE().cdp()).aES());
            WaypointDat aEQ = ((ProtectMissionScriptTemplate) aRE().cdp()).aEQ();
            aRE().m29193ad(0);
            int i = 0;
            for (int i2 = 0; i2 < aEQ.getAmount(); i2++) {
                List<NPCType> bvC = aEQ.bvC();
                if (bvC != null) {
                    for (NPCType a : bvC) {
                        int i3 = i + 1;
                        String str = ProtectMissionScript.f6600yz + String.valueOf(i);
                        NPC a2 = aRE().mo82b(str, a, ((ProtectMissionScriptTemplate) aRE().cdp()).mo1678Nc(), aEQ.getPosition(), (float) aEQ.aMJ());
                        if (a2 != null) {
                            aRE().m29203jA().put(str, a2);
                            ProtectMissionScript aRE = aRE();
                            aRE.m29193ad(aRE.m29206jD() + 1);
                        }
                        i = i3;
                    }
                }
            }
            aRE().m29194ae(0);
            int i4 = 0;
            for (int i5 = 0; i5 < aEQ.bvG(); i5++) {
                List<NPCType> bvE = aEQ.bvE();
                if (bvE != null) {
                    for (NPCType a3 : bvE) {
                        int i6 = i4 + 1;
                        String str2 = ProtectMissionScript.f6591yA + String.valueOf(i4);
                        NPC a4 = aRE().mo82b(str2, a3, ((ProtectMissionScriptTemplate) aRE().cdp()).mo1678Nc(), aEQ.getPosition(), (float) aEQ.aMJ());
                        if (a4 != null) {
                            aRE().m29204jB().put(str2, a4);
                            ProtectMissionScript aRE2 = aRE();
                            aRE2.m29194ae(aRE2.m29207jE() + 1);
                        }
                        i4 = i6;
                    }
                }
            }
            if (((ProtectMissionScriptTemplate) aRE().cdp()).aFg() && (aEO = ((ProtectMissionScriptTemplate) aRE().cdp()).aEO()) != null) {
                aRE().mo122f(aEO, new Object[0]);
            }
            if (aRH() == 0.0f) {
                aRE().mo575p(StateC.class);
            } else {
                mo8361ms(1.0f);
            }
        }

        @C0064Am(aul = "25dd1d69d1e189ba6be7654eb2679daf", aum = 0)
        /* renamed from: U */
        private void m29229U(float f) {
            super.mo4709V(f);
            if (aRE().isActive()) {
                m29233ft(aRH() - f);
                if (aHI.m15140mH(aRH()) && ((ProtectMissionScriptTemplate) aRE().cdp()).aFa()) {
                    int round = Math.round(aRH());
                    aRE().mo122f(((ProtectMissionScriptTemplate) aRE().cdp()).aEE(), ((ProtectMissionScriptTemplate) aRE().cdp()).mo711rP(), Integer.valueOf(round / 60), Integer.valueOf(round % 60));
                }
                if (aRH() > 0.0f) {
                    mo8361ms(1.0f);
                } else {
                    aRE().mo575p(StateC.class);
                }
            }
        }

        @C0064Am(aul = "fb61e642b370f66457bb8c1288f0c146", aum = 0)
        /* renamed from: a */
        private void m29231a(String str, Ship fAVar, Ship fAVar2) {
            if (str.startsWith(ProtectMissionScript.f6600yz)) {
                aRE().m29203jA().remove(str);
                if (aRE().m29203jA().size() < ((ProtectMissionScriptTemplate) aRE().cdp()).aEW()) {
                    aRE().fail();
                } else if (((ProtectMissionScriptTemplate) aRE().cdp()).aEC()) {
                    int b = aRE().m29206jD() - aRE().m29203jA().size();
                    aRE().mo122f(((ProtectMissionScriptTemplate) aRE().cdp()).aEA(), Integer.valueOf(b), Integer.valueOf(aRE().m29206jD()));
                }
            } else if (str.startsWith(ProtectMissionScript.f6591yA)) {
                aRE().m29204jB().remove(str);
                if (((ProtectMissionScriptTemplate) aRE().cdp()).aEy()) {
                    int d = aRE().m29207jE() - aRE().m29204jB().size();
                    aRE().mo122f(((ProtectMissionScriptTemplate) aRE().cdp()).aEw(), Integer.valueOf(d), Integer.valueOf(aRE().m29207jE()));
                }
            }
        }

        @C0064Am(aul = "97b3b150fe5b18b723339d0f083df2e8", aum = 0)
        /* renamed from: w */
        private void m29234w(Ship fAVar) {
            aRE().fail();
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.dl$c */
            /* compiled from: a */
    class StateC extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6603aT = null;
        public static final C5663aRz bHN = null;
        public static final C2491fm beE = null;
        public static final C2491fm beG = null;
        public static final C2491fm byI = null;
        public static final C2491fm cvT = null;
        public static final C5663aRz dbR = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "753066823a54b667f074b5045532b786", aum = 2)
        static /* synthetic */ ProtectMissionScript dbM;
        @C0064Am(aul = "d6e0a30e66e164d521bbd8319f70afb3", aum = 0)
        private static WaypointDat dbP;
        @C0064Am(aul = "4addf83c5a7ed0fdfadf1cf854668b6c", aum = 1)
        private static int dbQ;

        static {
            m29246V();
        }

        public StateC() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateC(C5540aNg ang) {
            super(ang);
        }

        StateC(ProtectMissionScript dlVar) {
            super((C5540aNg) null);
            super._m_script_init(dlVar);
        }

        /* renamed from: V */
        static void m29246V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 3;
            _m_methodCount = MissionScriptState._m_methodCount + 4;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 3)];
            C5663aRz b = C5640aRc.m17844b(ProtectMissionScript.StateC.class, "d6e0a30e66e164d521bbd8319f70afb3", i);
            bHN = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(ProtectMissionScript.StateC.class, "4addf83c5a7ed0fdfadf1cf854668b6c", i2);
            dbR = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(ProtectMissionScript.StateC.class, "753066823a54b667f074b5045532b786", i3);
            f6603aT = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i5 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i5 + 4)];
            C2491fm a = C4105zY.m41624a(ProtectMissionScript.StateC.class, "19cd0de9b56bee0df642589a7516ea03", i5);
            cvT = a;
            fmVarArr[i5] = a;
            int i6 = i5 + 1;
            C2491fm a2 = C4105zY.m41624a(ProtectMissionScript.StateC.class, "a986fbf452b6490f0fa0d1ba470a4d79", i6);
            beE = a2;
            fmVarArr[i6] = a2;
            int i7 = i6 + 1;
            C2491fm a3 = C4105zY.m41624a(ProtectMissionScript.StateC.class, "16483020fb8453011318fa26aef8160f", i7);
            beG = a3;
            fmVarArr[i7] = a3;
            int i8 = i7 + 1;
            C2491fm a4 = C4105zY.m41624a(ProtectMissionScript.StateC.class, "0718964ac99c2fe96f16630731a33b55", i8);
            byI = a4;
            fmVarArr[i8] = a4;
            int i9 = i8 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ProtectMissionScript.StateC.class, aQG.class, _m_fields, _m_methods);
        }

        private void aBy() {
            switch (bFf().mo6893i(cvT)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cvT, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cvT, new Object[0]));
                    break;
            }
            aBx();
        }

        private ProtectMissionScript aRE() {
            return (ProtectMissionScript) bFf().mo5608dq().mo3214p(f6603aT);
        }

        private WaypointDat aRI() {
            return (WaypointDat) bFf().mo5608dq().mo3214p(bHN);
        }

        private int aRJ() {
            return bFf().mo5608dq().mo3212n(dbR);
        }

        /* renamed from: f */
        private void m29248f(ProtectMissionScript dlVar) {
            bFf().mo5608dq().mo3197f(f6603aT, dlVar);
        }

        /* renamed from: hr */
        private void m29249hr(int i) {
            bFf().mo5608dq().mo3183b(dbR, i);
        }

        /* renamed from: n */
        private void m29250n(WaypointDat cDVar) {
            bFf().mo5608dq().mo3197f(bHN, cDVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new aQG(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aBx();
                    return null;
                case 1:
                    aai();
                    return null;
                case 2:
                    m29247a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                case 3:
                    m29251w((Ship) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m29247a(str, fAVar, fAVar2);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: x */
        public void mo12270x(Ship fAVar) {
            switch (bFf().mo6893i(byI)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                    break;
            }
            m29251w(fAVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: g */
        public void mo17905g(ProtectMissionScript dlVar) {
            m29248f(dlVar);
            super.mo10S();
            m29250n((WaypointDat) null);
            m29249hr(0);
        }

        @C0064Am(aul = "19cd0de9b56bee0df642589a7516ea03", aum = 0)
        private void aBx() {
            ArrayList arrayList = new ArrayList(aRE().m29205jC().values());
            ArrayList arrayList2 = new ArrayList(aRE().m29203jA().values());
            WaypointDat aEQ = ((ProtectMissionScriptTemplate) aRE().cdp()).aEQ();
            if (arrayList2.size() > 0) {
                for (int i = 0; i < arrayList.size(); i++) {
                    Controller hb = ((NPC) arrayList.get(i)).bQx().mo2998hb();
                    if (hb instanceof DroneAIController) {
                        DroneAIController droneAIController = (DroneAIController) hb;
                        droneAIController.mo4588J(((NPC) arrayList2.get(i % arrayList2.size())).bQx());
                        droneAIController.mo4610h(aEQ.getPosition());
                        droneAIController.setRadius((float) aEQ.aMJ());
                        droneAIController.start();
                    }
                }
            }
            arrayList2.addAll(aRE().m29204jB().values());
            if (arrayList.size() > 0) {
                for (int i2 = 0; i2 < arrayList2.size(); i2++) {
                    Controller hb2 = ((NPC) arrayList2.get(i2)).bQx().mo2998hb();
                    if (hb2 instanceof DroneAIController) {
                        DroneAIController droneAIController2 = (DroneAIController) hb2;
                        droneAIController2.mo4588J(((NPC) arrayList.get(i2 % arrayList.size())).bQx());
                        droneAIController2.mo4610h(aEQ.getPosition());
                        droneAIController2.setRadius((float) aEQ.aMJ());
                        droneAIController2.start();
                    }
                }
            }
        }

        @C0064Am(aul = "a986fbf452b6490f0fa0d1ba470a4d79", aum = 0)
        private void aai() {
            C3438ra<WaypointDat> aEI = ((ProtectMissionScriptTemplate) aRE().cdp()).aEI();
            if (aRI() == null) {
                m29250n((WaypointDat) aEI.get(0));
            } else {
                int indexOf = aEI.indexOf(aRI()) + 1;
                if (indexOf < aEI.size()) {
                    m29250n((WaypointDat) aEI.get(indexOf));
                    aRE().mo574k(ProtectMissionScriptTemplate.cEj, indexOf);
                } else if (((ProtectMissionScriptTemplate) aRE().cdp()).aEK()) {
                    aRE().cdk();
                    return;
                } else {
                    return;
                }
            }
            for (int i = 0; i < aRI().getAmount(); i++) {
                for (NPCType a : aRI().bvC()) {
                    StringBuilder sb = new StringBuilder(ProtectMissionScript.f6592yB);
                    int aRJ = aRJ();
                    m29249hr(aRJ + 1);
                    String sb2 = sb.append(String.valueOf(aRJ)).toString();
                    NPC a2 = aRE().mo81b(sb2, a, ((ProtectMissionScriptTemplate) aRE().cdp()).mo1678Nc(), aRI().bvy());
                    if (a2 != null) {
                        aRE().m29205jC().put(sb2, a2);
                    }
                }
            }
            if (!((ProtectMissionScriptTemplate) aRE().cdp()).aEK()) {
                aRE().mo575p(StateC.class);
            }
            aBy();
            if (((ProtectMissionScriptTemplate) aRE().cdp()).aFc()) {
                aRE().mo122f(((ProtectMissionScriptTemplate) aRE().cdp()).aEG(), new Object[0]);
            }
        }

        @C0064Am(aul = "16483020fb8453011318fa26aef8160f", aum = 0)
        /* renamed from: a */
        private void m29247a(String str, Ship fAVar, Ship fAVar2) {
            if (str.startsWith(ProtectMissionScript.f6600yz)) {
                aRE().m29203jA().remove(str);
                if (aRE().m29203jA().size() < ((ProtectMissionScriptTemplate) aRE().cdp()).aEW()) {
                    aRE().fail();
                    return;
                } else if (((ProtectMissionScriptTemplate) aRE().cdp()).aEC()) {
                    int amount = ((ProtectMissionScriptTemplate) aRE().cdp()).aEQ().getAmount();
                    int size = amount - aRE().m29203jA().size();
                    aRE().mo122f(((ProtectMissionScriptTemplate) aRE().cdp()).aEA(), Integer.valueOf(size), Integer.valueOf(amount));
                }
            } else if (str.startsWith(ProtectMissionScript.f6591yA)) {
                aRE().m29204jB().remove(str);
                if (((ProtectMissionScriptTemplate) aRE().cdp()).aEy()) {
                    int d = aRE().m29207jE() - aRE().m29204jB().size();
                    aRE().mo122f(((ProtectMissionScriptTemplate) aRE().cdp()).aEw(), Integer.valueOf(d), Integer.valueOf(aRE().m29207jE()));
                }
            } else if (str.startsWith(ProtectMissionScript.f6592yB)) {
                aRE().m29205jC().remove(str);
                if (aRE().m29205jC().size() == 0) {
                    if (((ProtectMissionScriptTemplate) aRE().cdp()).aEK()) {
                        aRE().mo575p(StateC.class);
                    } else {
                        aRE().cdk();
                        return;
                    }
                }
            }
            aBy();
        }

        @C0064Am(aul = "0718964ac99c2fe96f16630731a33b55", aum = 0)
        /* renamed from: w */
        private void m29251w(Ship fAVar) {
            aRE().fail();
        }
    }
}
