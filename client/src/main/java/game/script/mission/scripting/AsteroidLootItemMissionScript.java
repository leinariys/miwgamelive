package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.AsteroidLootItemMissionScriptTemplate;
import game.script.npc.NPCType;
import game.script.ship.Station;
import game.script.space.AsteroidType;
import logic.baa.*;
import logic.data.mbean.C0204CV;
import logic.data.mbean.C2541ga;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.missiontemplate.AsteroidLootItemMission")
@C6485anp
@C5511aMd
/* renamed from: a.QJ */
/* compiled from: a */
public class AsteroidLootItemMissionScript<T extends AsteroidLootItemMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f1419Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bHZ = null;
    public static final C5663aRz gFj = null;
    public static final C5663aRz gWS = null;
    public static final C5663aRz gWT = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f1420yH = null;
    public static final String hEQ = "lootMissionNpcListenLoot";
    public static final String jfB = "lootMissionNpc";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "f9dd54890feca18b14e5428f7f780f37", aum = 1)
    private static WaypointDat bHY = null;
    @C0064Am(aul = "fceb2e4ad4c34359f3ad39c6b856f4be", aum = 0)
    private static int cCx = 0;
    @C0064Am(aul = "9bfe8232dec09ff2e63266023383d580", aum = 2)
    private static WaypointDat cCy = null;
    @C0064Am(aul = "f3892782435fa34dbc6a32a62241656e", aum = 3)
    private static Vec3d cCz = null;

    static {
        m8756V();
    }

    public AsteroidLootItemMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidLootItemMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8756V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 4;
        _m_methodCount = MissionScript._m_methodCount + 2;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(AsteroidLootItemMissionScript.class, "fceb2e4ad4c34359f3ad39c6b856f4be", i);
        gWT = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AsteroidLootItemMissionScript.class, "f9dd54890feca18b14e5428f7f780f37", i2);
        bHZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AsteroidLootItemMissionScript.class, "9bfe8232dec09ff2e63266023383d580", i3);
        gWS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AsteroidLootItemMissionScript.class, "f3892782435fa34dbc6a32a62241656e", i4);
        gFj = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i6 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 2)];
        C2491fm a = C4105zY.m41624a(AsteroidLootItemMissionScript.class, "f4dfbde31e69ef558b41083972e4e5cc", i6);
        f1420yH = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidLootItemMissionScript.class, "0e7f919ea06b3de0fa4f169161601c0d", i7);
        f1419Do = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidLootItemMissionScript.class, C0204CV.class, _m_fields, _m_methods);
    }

    /* access modifiers changed from: private */
    /* renamed from: N */
    public void m8755N(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(gWS, cDVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: af */
    public void m8764af(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(gFj, ajr);
    }

    /* access modifiers changed from: private */
    public WaypointDat aoQ() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(bHZ);
    }

    /* access modifiers changed from: private */
    public int cEg() {
        return bFf().mo5608dq().mo3212n(gWT);
    }

    /* access modifiers changed from: private */
    public Vec3d cEi() {
        return (Vec3d) bFf().mo5608dq().mo3214p(gFj);
    }

    /* access modifiers changed from: private */
    public WaypointDat dDy() {
        return (WaypointDat) bFf().mo5608dq().mo3214p(gWS);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m8770f(WaypointDat cDVar) {
        bFf().mo5608dq().mo3197f(bHZ, cDVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: wk */
    public void m8772wk(int i) {
        bFf().mo5608dq().mo3183b(gWT, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0204CV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m8771jF();
                return null;
            case 1:
                m8759a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f1419Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1419Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1419Do, new Object[]{jt}));
                break;
        }
        m8759a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f1420yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1420yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1420yH, new Object[0]));
                break;
        }
        m8771jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f4dfbde31e69ef558b41083972e4e5cc", aum = 0)
    /* renamed from: jF */
    private void m8771jF() {
        mo575p(StateA.class);
    }

    @C0064Am(aul = "0e7f919ea06b3de0fa4f169161601c0d", aum = 0)
    /* renamed from: a */
    private void m8759a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && azm() == null) {
            mo576r(StateA.class);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.QJ$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f1422aT = null;
        public static final C2491fm bYK = null;
        public static final C2491fm beE = null;
        public static final C2491fm beF = null;
        public static final C2491fm byJ = null;
        public static final C2491fm dWA = null;
        public static final C2491fm dWB = null;
        public static final C2491fm dWC = null;
        public static final C2491fm dWD = null;
        public static final C2491fm dWE = null;
        public static final C2491fm dWF = null;
        public static final C2491fm dWv = null;
        public static final C2491fm dWw = null;
        public static final C2491fm dWx = null;
        public static final C2491fm dWy = null;
        public static final C2491fm dWz = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "db559f08bc31539ab12aa59c5768dca7", aum = 0)

        /* renamed from: OH */
        static /* synthetic */ AsteroidLootItemMissionScript f1421OH;

        static {
            m8781V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateA(AsteroidLootItemMissionScript qj) {
            super((C5540aNg) null);
            super._m_script_init(qj);
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m8781V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 15;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(AsteroidLootItemMissionScript.StateA.class, "db559f08bc31539ab12aa59c5768dca7", i);
            f1422aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 15)];
            C2491fm a = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "d3d82ab2987cd4aa6c49839470975af2", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "46c633874a82b0c88f45b03533442ec9", i4);
            dWv = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "29c6c1510f99acbd584e122e585de2c5", i5);
            dWw = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "28c41523e05f2251a9000161f286ad43", i6);
            dWx = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "bab2ab7d2a755b3ee8d50ba3f2fc7714", i7);
            dWy = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "087328f0ec0a770f7885fa8a6d37a2ae", i8);
            dWz = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "6e6d08598a6d6fe4d2a45312d1e45133", i9);
            byJ = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            C2491fm a8 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "b8e1d818042b3decabde69c51f5ae435", i10);
            beF = a8;
            fmVarArr[i10] = a8;
            int i11 = i10 + 1;
            C2491fm a9 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "0dca3f41681b011c0cb1b5103f4ce831", i11);
            dWA = a9;
            fmVarArr[i11] = a9;
            int i12 = i11 + 1;
            C2491fm a10 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "54f094287f12415efc4c7a1a280b18d2", i12);
            dWB = a10;
            fmVarArr[i12] = a10;
            int i13 = i12 + 1;
            C2491fm a11 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "e64093601c01c3ca2805628e9f4690c0", i13);
            bYK = a11;
            fmVarArr[i13] = a11;
            int i14 = i13 + 1;
            C2491fm a12 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "9613fa9fa0f4755068ed36d64bbd7e02", i14);
            dWC = a12;
            fmVarArr[i14] = a12;
            int i15 = i14 + 1;
            C2491fm a13 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "0a3995b1528f9cf0ceb9d46fec158097", i15);
            dWD = a13;
            fmVarArr[i15] = a13;
            int i16 = i15 + 1;
            C2491fm a14 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "4ae35edb5d47ec3aeff8ce841fd5be1b", i16);
            dWE = a14;
            fmVarArr[i16] = a14;
            int i17 = i16 + 1;
            C2491fm a15 = C4105zY.m41624a(AsteroidLootItemMissionScript.StateA.class, "e4645305844b724eee445295a01fd001", i17);
            dWF = a15;
            fmVarArr[i17] = a15;
            int i18 = i17 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(AsteroidLootItemMissionScript.StateA.class, C2541ga.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m8782a(AsteroidLootItemMissionScript qj) {
            bFf().mo5608dq().mo3197f(f1422aT, qj);
        }

        @C0064Am(aul = "e4645305844b724eee445295a01fd001", aum = 0)
        @C5566aOg
        /* renamed from: a */
        private void m8783a(String str, AsteroidType aqn, WaypointDat cDVar) {
            throw new aWi(new aCE(this, dWF, new Object[]{str, aqn, cDVar}));
        }

        @C0064Am(aul = "087328f0ec0a770f7885fa8a6d37a2ae", aum = 0)
        @C5566aOg
        /* renamed from: a */
        private void m8784a(String str, NPCType aed, WaypointDat cDVar) {
            throw new aWi(new aCE(this, dWz, new Object[]{str, aed, cDVar}));
        }

        @C0064Am(aul = "d3d82ab2987cd4aa6c49839470975af2", aum = 0)
        @C5566aOg
        private void aai() {
            throw new aWi(new aCE(this, beE, new Object[0]));
        }

        @C5566aOg
        /* renamed from: b */
        private void m8785b(String str, AsteroidType aqn, WaypointDat cDVar) {
            switch (bFf().mo6893i(dWF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dWF, new Object[]{str, aqn, cDVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dWF, new Object[]{str, aqn, cDVar}));
                    break;
            }
            m8783a(str, aqn, cDVar);
        }

        @C5566aOg
        /* renamed from: b */
        private void m8786b(String str, NPCType aed, WaypointDat cDVar) {
            switch (bFf().mo6893i(dWz)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dWz, new Object[]{str, aed, cDVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dWz, new Object[]{str, aed, cDVar}));
                    break;
            }
            m8784a(str, aed, cDVar);
        }

        @C0064Am(aul = "b8e1d818042b3decabde69c51f5ae435", aum = 0)
        @C5566aOg
        /* renamed from: bk */
        private boolean m8787bk(String str) {
            throw new aWi(new aCE(this, beF, new Object[]{str}));
        }

        private AsteroidLootItemMissionScript bqg() {
            return (AsteroidLootItemMissionScript) bFf().mo5608dq().mo3214p(f1422aT);
        }

        @C0064Am(aul = "46c633874a82b0c88f45b03533442ec9", aum = 0)
        @C5566aOg
        private void bqh() {
            throw new aWi(new aCE(this, dWv, new Object[0]));
        }

        @C0064Am(aul = "bab2ab7d2a755b3ee8d50ba3f2fc7714", aum = 0)
        @C5566aOg
        private void bqj() {
            throw new aWi(new aCE(this, dWy, new Object[0]));
        }

        @C0064Am(aul = "29c6c1510f99acbd584e122e585de2c5", aum = 0)
        @C5566aOg
        /* renamed from: mX */
        private String m8788mX(int i) {
            throw new aWi(new aCE(this, dWw, new Object[]{new Integer(i)}));
        }

        @C0064Am(aul = "28c41523e05f2251a9000161f286ad43", aum = 0)
        @C5566aOg
        /* renamed from: o */
        private String m8789o(WaypointDat cDVar) {
            throw new aWi(new aCE(this, dWx, new Object[]{cDVar}));
        }

        @C0064Am(aul = "6e6d08598a6d6fe4d2a45312d1e45133", aum = 0)
        @C5566aOg
        /* renamed from: p */
        private void m8790p(Station bf) {
            throw new aWi(new aCE(this, byJ, new Object[]{bf}));
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2541ga(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    bqh();
                    return null;
                case 2:
                    return m8788mX(((Integer) args[0]).intValue());
                case 3:
                    return m8789o((WaypointDat) args[0]);
                case 4:
                    bqj();
                    return null;
                case 5:
                    m8784a((String) args[0], (NPCType) args[1], (WaypointDat) args[2]);
                    return null;
                case 6:
                    m8790p((Station) args[0]);
                    return null;
                case 7:
                    return new Boolean(m8787bk((String) args[0]));
                case 8:
                    return bql();
                case 9:
                    m8791q((WaypointDat) args[0]);
                    return null;
                case 10:
                    return arz();
                case 11:
                    return bqn();
                case 12:
                    return new Integer(bqp());
                case 13:
                    bqr();
                    return null;
                case 14:
                    m8783a((String) args[0], (AsteroidType) args[1], (WaypointDat) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        @C5566aOg
        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* access modifiers changed from: protected */
        public Vec3d arA() {
            switch (bFf().mo6893i(bYK)) {
                case 0:
                    return null;
                case 2:
                    return (Vec3d) bFf().mo5606d(new aCE(this, bYK, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, bYK, new Object[0]));
                    break;
            }
            return arz();
        }

        @C5566aOg
        /* renamed from: bl */
        public boolean mo4962bl(String str) {
            switch (bFf().mo6893i(beF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                    break;
            }
            return m8787bk(str);
        }

        /* access modifiers changed from: protected */
        @C5566aOg
        public void bqi() {
            switch (bFf().mo6893i(dWv)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dWv, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dWv, new Object[0]));
                    break;
            }
            bqh();
        }

        /* access modifiers changed from: protected */
        @C5566aOg
        public void bqk() {
            switch (bFf().mo6893i(dWy)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dWy, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dWy, new Object[0]));
                    break;
            }
            bqj();
        }

        /* access modifiers changed from: protected */
        public WaypointDat bqm() {
            switch (bFf().mo6893i(dWA)) {
                case 0:
                    return null;
                case 2:
                    return (WaypointDat) bFf().mo5606d(new aCE(this, dWA, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, dWA, new Object[0]));
                    break;
            }
            return bql();
        }

        /* access modifiers changed from: protected */
        public WaypointDat bqo() {
            switch (bFf().mo6893i(dWC)) {
                case 0:
                    return null;
                case 2:
                    return (WaypointDat) bFf().mo5606d(new aCE(this, dWC, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, dWC, new Object[0]));
                    break;
            }
            return bqn();
        }

        /* access modifiers changed from: protected */
        public int bqq() {
            switch (bFf().mo6893i(dWD)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, dWD, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, dWD, new Object[0]));
                    break;
            }
            return bqp();
        }

        /* access modifiers changed from: protected */
        public void bqs() {
            switch (bFf().mo6893i(dWE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dWE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dWE, new Object[0]));
                    break;
            }
            bqr();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        @C5566aOg
        /* renamed from: mY */
        public String mo4969mY(int i) {
            switch (bFf().mo6893i(dWw)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, dWw, new Object[]{new Integer(i)}));
                case 3:
                    bFf().mo5606d(new aCE(this, dWw, new Object[]{new Integer(i)}));
                    break;
            }
            return m8788mX(i);
        }

        /* access modifiers changed from: protected */
        @C5566aOg
        /* renamed from: p */
        public String mo4970p(WaypointDat cDVar) {
            switch (bFf().mo6893i(dWx)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, dWx, new Object[]{cDVar}));
                case 3:
                    bFf().mo5606d(new aCE(this, dWx, new Object[]{cDVar}));
                    break;
            }
            return m8789o(cDVar);
        }

        @C5566aOg
        /* renamed from: q */
        public void mo4971q(Station bf) {
            switch (bFf().mo6893i(byJ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                    break;
            }
            m8790p(bf);
        }

        /* access modifiers changed from: protected */
        /* renamed from: r */
        public void mo4972r(WaypointDat cDVar) {
            switch (bFf().mo6893i(dWB)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dWB, new Object[]{cDVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dWB, new Object[]{cDVar}));
                    break;
            }
            m8791q(cDVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo4961b(AsteroidLootItemMissionScript qj) {
            m8782a(qj);
            super.mo10S();
        }

        @C0064Am(aul = "0dca3f41681b011c0cb1b5103f4ce831", aum = 0)
        private WaypointDat bql() {
            return bqg().aoQ();
        }

        @C0064Am(aul = "54f094287f12415efc4c7a1a280b18d2", aum = 0)
        /* renamed from: q */
        private void m8791q(WaypointDat cDVar) {
            bqg().m8755N(cDVar);
        }

        @C0064Am(aul = "e64093601c01c3ca2805628e9f4690c0", aum = 0)
        private Vec3d arz() {
            return bqg().cEi();
        }

        @C0064Am(aul = "9613fa9fa0f4755068ed36d64bbd7e02", aum = 0)
        private WaypointDat bqn() {
            return bqg().dDy();
        }

        @C0064Am(aul = "0a3995b1528f9cf0ceb9d46fec158097", aum = 0)
        private int bqp() {
            return bqg().cEg();
        }

        @C0064Am(aul = "4ae35edb5d47ec3aeff8ce841fd5be1b", aum = 0)
        private void bqr() {
            AsteroidLootItemMissionScript bqg = bqg();
            bqg.m8772wk(bqg.cEg() + 1);
        }
    }
}
