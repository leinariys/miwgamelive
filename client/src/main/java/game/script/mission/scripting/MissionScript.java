package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Character;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.MissionObjective;
import game.script.mission.MissionTemplateObjective;
import game.script.mission.MissionTrigger;
import game.script.mission.actions.MissionAction;
import game.script.missiontemplate.WaypointDat;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.*;
import logic.baa.*;
import logic.data.link.C3995xs;
import logic.data.mbean.aMY;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.BC */
/* compiled from: a */
public abstract class MissionScript<T extends MissionScriptTemplate> extends Mission<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLS = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLX = null;
    public static final C2491fm aLY = null;
    public static final C2491fm aMe = null;
    public static final C2491fm beF = null;
    public static final C2491fm beG = null;
    public static final C2491fm beH = null;
    public static final C2491fm byB = null;
    public static final C2491fm byC = null;
    public static final C2491fm byD = null;
    public static final C2491fm byE = null;
    public static final C2491fm byF = null;
    public static final C2491fm byG = null;
    public static final C2491fm byH = null;
    public static final C2491fm byI = null;
    public static final C2491fm byJ = null;
    public static final C2491fm byK = null;
    public static final C2491fm byL = null;
    public static final C2491fm byM = null;
    public static final C2491fm byN = null;
    public static final C2491fm byO = null;
    public static final C2491fm byP = null;
    public static final C2491fm byQ = null;
    public static final C2491fm byR = null;
    public static final C2491fm byS = null;
    public static final C2491fm cnA = null;
    public static final C2491fm cnB = null;
    public static final C2491fm cnC = null;
    public static final C2491fm cnD = null;
    public static final C2491fm cnE = null;
    public static final C2491fm cnF = null;
    public static final C2491fm cnG = null;
    public static final C2491fm cnH = null;
    public static final C2491fm cnI = null;
    public static final C2491fm cnJ = null;
    public static final C2491fm cnK = null;
    public static final C2491fm cnL = null;
    public static final C2491fm cnM = null;
    public static final C5663aRz cnl = null;
    public static final C5663aRz cnn = null;
    public static final C5663aRz cnp = null;
    public static final C5663aRz cnr = null;
    public static final C2491fm cns = null;
    public static final C2491fm cnt = null;
    public static final C2491fm cnu = null;
    public static final C2491fm cnv = null;
    public static final C2491fm cnw = null;
    public static final C2491fm cnx = null;
    public static final C2491fm cny = null;
    public static final C2491fm cnz = null;
    /* renamed from: yH */
    public static final C2491fm f126yH = null;
    public static final String cnj = "missionExpireTime";
    private static final long serialVersionUID = -8113841486569784735L;
    public static C6494any ___iScriptClass = null;
    @C5566aOg
    @C0064Am(aul = "39b1c4d08d05299c27d8812c452475e5", aum = 0)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C1556Wo<Class<?>, MissionScriptState> cnk = null;
    @C0064Am(aul = "f0f32b490410e2473bf27202fab4e2fa", aum = 1)
    private static MissionScriptState cnm = null;
    @C0064Am(aul = "46b5a292ec568ac185767ff77c46c9b9", aum = 2)
    private static C3438ra<MissionObjective> cno = null;
    @C5566aOg
    @C0064Am(aul = "0aba2fdafbc0a2e3b7bc709b687181b8", aum = 3)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C2686iZ<MissionAction> cnq = null;

    static {
        m740V();
    }

    public MissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m740V() {
        _m_fieldCount = Mission._m_fieldCount + 4;
        _m_methodCount = Mission._m_methodCount + 52;
        int i = Mission._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(MissionScript.class, "39b1c4d08d05299c27d8812c452475e5", i);
        cnl = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionScript.class, "f0f32b490410e2473bf27202fab4e2fa", i2);
        cnn = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionScript.class, "46b5a292ec568ac185767ff77c46c9b9", i3);
        cnp = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionScript.class, "0aba2fdafbc0a2e3b7bc709b687181b8", i4);
        cnr = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Mission._m_fields, (Object[]) _m_fields);
        int i6 = Mission._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 52)];
        C2491fm a = C4105zY.m41624a(MissionScript.class, "f9c9e23cadf206787f08b174e70ebb88", i6);
        cns = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionScript.class, "1b148450c86bc42499cdfaa1786b025d", i7);
        f126yH = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionScript.class, "eb183bf3456bf2c493f1c708741ed80c", i8);
        cnt = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionScript.class, "38cd493c46c4e59ca20490a5435a9642", i9);
        cnu = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionScript.class, "27796794fcbc4300446703bcf3388bf1", i10);
        cnv = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionScript.class, "c14a4dc7ce19ef8f522fe261cb96bc4f", i11);
        cnw = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionScript.class, "665b3d1edd6b8cf9a7ce551babeb13d5", i12);
        aLU = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionScript.class, "e0dd0401297c0cc84514a6d8504ef564", i13);
        aLT = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionScript.class, "3f9f29ad255593ab0853eabcee076f30", i14);
        byI = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionScript.class, "9bd06fa5dd4d6fadeeba7feaef852745", i15);
        aLY = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionScript.class, "eaf15185a0d630d871649dbbaeda3b64", i16);
        beG = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(MissionScript.class, "8884851e4b8aee3ac11f30f6e0621cc3", i17);
        byJ = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(MissionScript.class, "3caf05fe4aad7b4e0042c202f9c0896b", i18);
        byK = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(MissionScript.class, "e77a769a55819919e354eb5fcf3bbf8a", i19);
        byL = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(MissionScript.class, "62c0bf28301e08ee2a05ff96432f2bad", i20);
        byB = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(MissionScript.class, "a8227574f1330c65547d7f3efb632e45", i21);
        byM = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(MissionScript.class, "edf940ffbf8978f6bb9ffdacb1cf3d83", i22);
        beH = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(MissionScript.class, "230ecde0c5913bda284a5d1f8424182c", i23);
        byN = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        C2491fm a19 = C4105zY.m41624a(MissionScript.class, "181a718c0f43b2a96c13ec93970cc3a6", i24);
        byO = a19;
        fmVarArr[i24] = a19;
        int i25 = i24 + 1;
        C2491fm a20 = C4105zY.m41624a(MissionScript.class, "1101a99c73955ed1720f9c36d745d26a", i25);
        byP = a20;
        fmVarArr[i25] = a20;
        int i26 = i25 + 1;
        C2491fm a21 = C4105zY.m41624a(MissionScript.class, "dd63dce1aa572a0d1f46793b3e66b904", i26);
        byC = a21;
        fmVarArr[i26] = a21;
        int i27 = i26 + 1;
        C2491fm a22 = C4105zY.m41624a(MissionScript.class, "adc0d9fc345379e8b6fddd3afb2cf565", i27);
        byD = a22;
        fmVarArr[i27] = a22;
        int i28 = i27 + 1;
        C2491fm a23 = C4105zY.m41624a(MissionScript.class, "f2f10a10b03d9842351bd9df9781eaf2", i28);
        byE = a23;
        fmVarArr[i28] = a23;
        int i29 = i28 + 1;
        C2491fm a24 = C4105zY.m41624a(MissionScript.class, "cc566943ca2f8afbae1bb1da0c422250", i29);
        byQ = a24;
        fmVarArr[i29] = a24;
        int i30 = i29 + 1;
        C2491fm a25 = C4105zY.m41624a(MissionScript.class, "b33252aae43193bb27d1443428670944", i30);
        aMe = a25;
        fmVarArr[i30] = a25;
        int i31 = i30 + 1;
        C2491fm a26 = C4105zY.m41624a(MissionScript.class, "61b1e3d4c7dba24cec50d46d6b5b5cfa", i31);
        byF = a26;
        fmVarArr[i31] = a26;
        int i32 = i31 + 1;
        C2491fm a27 = C4105zY.m41624a(MissionScript.class, "8e97c2972e635557842692917c34243a", i32);
        aLS = a27;
        fmVarArr[i32] = a27;
        int i33 = i32 + 1;
        C2491fm a28 = C4105zY.m41624a(MissionScript.class, "27f54036d4f24deb401abec33f1f3265", i33);
        byG = a28;
        fmVarArr[i33] = a28;
        int i34 = i33 + 1;
        C2491fm a29 = C4105zY.m41624a(MissionScript.class, "770a3c9227dfcd86a9f67e9ceb1fd434", i34);
        aLR = a29;
        fmVarArr[i34] = a29;
        int i35 = i34 + 1;
        C2491fm a30 = C4105zY.m41624a(MissionScript.class, "4827ffd6bc0140a8bc458252a3e0d077", i35);
        beF = a30;
        fmVarArr[i35] = a30;
        int i36 = i35 + 1;
        C2491fm a31 = C4105zY.m41624a(MissionScript.class, "49adcb9323808237a62b5d7501df9795", i36);
        byH = a31;
        fmVarArr[i36] = a31;
        int i37 = i36 + 1;
        C2491fm a32 = C4105zY.m41624a(MissionScript.class, "766e7eb36803a0f9b64e210f14194db6", i37);
        byR = a32;
        fmVarArr[i37] = a32;
        int i38 = i37 + 1;
        C2491fm a33 = C4105zY.m41624a(MissionScript.class, "d36fe7ac1a1a3217c6a911c71210a41d", i38);
        byS = a33;
        fmVarArr[i38] = a33;
        int i39 = i38 + 1;
        C2491fm a34 = C4105zY.m41624a(MissionScript.class, "51e0607b72e0ecb430cf75d276ef3070", i39);
        aLX = a34;
        fmVarArr[i39] = a34;
        int i40 = i39 + 1;
        C2491fm a35 = C4105zY.m41624a(MissionScript.class, "10261ca56fe87317fa49189e6e3fcc4a", i40);
        aLV = a35;
        fmVarArr[i40] = a35;
        int i41 = i40 + 1;
        C2491fm a36 = C4105zY.m41624a(MissionScript.class, "b151645268c3a2de4c75a585e5beb454", i41);
        aLW = a36;
        fmVarArr[i41] = a36;
        int i42 = i41 + 1;
        C2491fm a37 = C4105zY.m41624a(MissionScript.class, "5516a0770d06685d110d78771b5380ba", i42);
        cnx = a37;
        fmVarArr[i42] = a37;
        int i43 = i42 + 1;
        C2491fm a38 = C4105zY.m41624a(MissionScript.class, "8a5f0913241d8b9b03e9d684b0c7ab34", i43);
        cny = a38;
        fmVarArr[i43] = a38;
        int i44 = i43 + 1;
        C2491fm a39 = C4105zY.m41624a(MissionScript.class, "518d0332d44ce315bb7579c22fbf6749", i44);
        cnz = a39;
        fmVarArr[i44] = a39;
        int i45 = i44 + 1;
        C2491fm a40 = C4105zY.m41624a(MissionScript.class, "3d22a4f37b882a4d2bfec19ca624efb1", i45);
        cnA = a40;
        fmVarArr[i45] = a40;
        int i46 = i45 + 1;
        C2491fm a41 = C4105zY.m41624a(MissionScript.class, "8afe076deb93b7e4f28ce95223a9ca9f", i46);
        cnB = a41;
        fmVarArr[i46] = a41;
        int i47 = i46 + 1;
        C2491fm a42 = C4105zY.m41624a(MissionScript.class, "0b5cb65dfaffdd10f8f957f6d902680d", i47);
        cnC = a42;
        fmVarArr[i47] = a42;
        int i48 = i47 + 1;
        C2491fm a43 = C4105zY.m41624a(MissionScript.class, "e4219171dda2aaa798f2ca1cffb4409b", i48);
        cnD = a43;
        fmVarArr[i48] = a43;
        int i49 = i48 + 1;
        C2491fm a44 = C4105zY.m41624a(MissionScript.class, "4a8a51a2a9f1fd2217136e2a7ba4ed7f", i49);
        cnE = a44;
        fmVarArr[i49] = a44;
        int i50 = i49 + 1;
        C2491fm a45 = C4105zY.m41624a(MissionScript.class, "84cf33b1296406bc653635c48f65ac29", i50);
        cnF = a45;
        fmVarArr[i50] = a45;
        int i51 = i50 + 1;
        C2491fm a46 = C4105zY.m41624a(MissionScript.class, "0fb823ff7b013a4c0a4456ad0dccdb6d", i51);
        cnG = a46;
        fmVarArr[i51] = a46;
        int i52 = i51 + 1;
        C2491fm a47 = C4105zY.m41624a(MissionScript.class, "7284457cb4c8361b6fe49826383e7a42", i52);
        cnH = a47;
        fmVarArr[i52] = a47;
        int i53 = i52 + 1;
        C2491fm a48 = C4105zY.m41624a(MissionScript.class, "837bf554ae34cd90d0f4fee6f4ccef82", i53);
        cnI = a48;
        fmVarArr[i53] = a48;
        int i54 = i53 + 1;
        C2491fm a49 = C4105zY.m41624a(MissionScript.class, "2d3e1a323dbf55ca1414c209c0067eb9", i54);
        cnJ = a49;
        fmVarArr[i54] = a49;
        int i55 = i54 + 1;
        C2491fm a50 = C4105zY.m41624a(MissionScript.class, "d998869dd7edba964fa8ae6a6422439c", i55);
        cnK = a50;
        fmVarArr[i55] = a50;
        int i56 = i55 + 1;
        C2491fm a51 = C4105zY.m41624a(MissionScript.class, "c2591aaf42a82b7d40965ee25e820f72", i56);
        cnL = a51;
        fmVarArr[i56] = a51;
        int i57 = i56 + 1;
        C2491fm a52 = C4105zY.m41624a(MissionScript.class, "04db10d2f2f8285c39bda2efb7c521d6", i57);
        cnM = a52;
        fmVarArr[i57] = a52;
        int i58 = i57 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Mission._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionScript.class, aMY.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "0b5cb65dfaffdd10f8f957f6d902680d", aum = 0)
    @C5566aOg
    /* renamed from: D */
    private boolean m736D(Ship fAVar) {
        throw new aWi(new aCE(this, cnC, new Object[]{fAVar}));
    }

    @C0064Am(aul = "518d0332d44ce315bb7579c22fbf6749", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m741a(String str, NPCType aed, Vec3d ajr, float f, Vec3d ajr2, float f2, StellarSystem jj, Character acx) {
        throw new aWi(new aCE(this, cnz, new Object[]{str, aed, ajr, new Float(f), ajr2, new Float(f2), jj, acx}));
    }

    @C0064Am(aul = "8a5f0913241d8b9b03e9d684b0c7ab34", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m742a(String str, NPCType aed, Vec3d ajr, float f, Vec3d ajr2, float f2, StellarSystem jj, Ship fAVar) {
        throw new aWi(new aCE(this, cny, new Object[]{str, aed, ajr, new Float(f), ajr2, new Float(f2), jj, fAVar}));
    }

    @C0064Am(aul = "5516a0770d06685d110d78771b5380ba", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m743a(String str, NPCType aed, WaypointDat cDVar, StellarSystem jj) {
        throw new aWi(new aCE(this, cnx, new Object[]{str, aed, cDVar, jj}));
    }

    /* renamed from: a */
    private void m747a(MissionScriptState aam) {
        bFf().mo5608dq().mo3197f(cnn, aam);
    }

    @C0064Am(aul = "eb183bf3456bf2c493f1c708741ed80c", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m750a(MissionAction.C2210a aVar, String str) {
        throw new aWi(new aCE(this, cnt, new Object[]{aVar, str}));
    }

    @C0064Am(aul = "8afe076deb93b7e4f28ce95223a9ca9f", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m756a(ItemType jCVar, int i) {
        throw new aWi(new aCE(this, cnB, new Object[]{jCVar, new Integer(i)}));
    }

    /* renamed from: ad */
    private void m758ad(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnp, raVar);
    }

    private C1556Wo aza() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cnl);
    }

    private MissionScriptState azb() {
        return (MissionScriptState) bFf().mo5608dq().mo3214p(cnn);
    }

    private C3438ra azc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnp);
    }

    private C2686iZ azd() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cnr);
    }

    @C0064Am(aul = "f9c9e23cadf206787f08b174e70ebb88", aum = 0)
    @C6580apg(cpn = true)
    private void aze() {
        bFf().mo5600a((Class<? extends C4062yl>) C3995xs.C3996a.class, (C0495Gr) new aCE(this, cns, new Object[0]));
    }

    @C0064Am(aul = "38cd493c46c4e59ca20490a5435a9642", aum = 0)
    @C5566aOg
    private void azg() {
        throw new aWi(new aCE(this, cnu, new Object[0]));
    }

    @C0064Am(aul = "27796794fcbc4300446703bcf3388bf1", aum = 0)
    @C5566aOg
    private void azh() {
        throw new aWi(new aCE(this, cnv, new Object[0]));
    }

    @C5566aOg
    private void azi() {
        switch (bFf().mo6893i(cnv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnv, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnv, new Object[0]));
                break;
        }
        azh();
    }

    @C0064Am(aul = "04db10d2f2f8285c39bda2efb7c521d6", aum = 0)
    @C5566aOg
    private MissionScriptState azl() {
        throw new aWi(new aCE(this, cnM, new Object[0]));
    }

    @C5566aOg
    /* renamed from: b */
    private void m760b(MissionAction.C2210a aVar, String str) {
        switch (bFf().mo6893i(cnt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnt, new Object[]{aVar, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnt, new Object[]{aVar, str}));
                break;
        }
        m750a(aVar, str);
    }

    @C5566aOg
    private void checkState() {
        switch (bFf().mo6893i(cnu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnu, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnu, new Object[0]));
                break;
        }
        azg();
    }

    @C0064Am(aul = "d998869dd7edba964fa8ae6a6422439c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: dM */
    private MissionObjective m768dM(String str) {
        throw new aWi(new aCE(this, cnK, new Object[]{str}));
    }

    @C0064Am(aul = "e4219171dda2aaa798f2ca1cffb4409b", aum = 0)
    @C5566aOg
    /* renamed from: ea */
    private boolean m769ea(long j) {
        throw new aWi(new aCE(this, cnD, new Object[]{new Long(j)}));
    }

    /* renamed from: f */
    private void m771f(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C3995xs.C3996a) it.next()).mo23003a(this);
        }
    }

    @C0064Am(aul = "1b148450c86bc42499cdfaa1786b025d", aum = 0)
    /* renamed from: jF */
    private void m775jF() {
        throw new aWi(new aCE(this, f126yH, new Object[0]));
    }

    @C0064Am(aul = "c14a4dc7ce19ef8f522fe261cb96bc4f", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m778o(Class cls) {
        throw new aWi(new aCE(this, cnw, new Object[]{cls}));
    }

    /* renamed from: p */
    private void m780p(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cnr, iZVar);
    }

    @C0064Am(aul = "c2591aaf42a82b7d40965ee25e820f72", aum = 0)
    @C5566aOg
    /* renamed from: q */
    private void m781q(Class cls) {
        throw new aWi(new aCE(this, cnL, new Object[]{cls}));
    }

    @C0064Am(aul = "3d22a4f37b882a4d2bfec19ca624efb1", aum = 0)
    @C5566aOg
    /* renamed from: q */
    private boolean m782q(ItemType jCVar) {
        throw new aWi(new aCE(this, cnA, new Object[]{jCVar}));
    }

    /* renamed from: y */
    private void m787y(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cnl, wo);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: E */
    public boolean mo559E(Ship fAVar) {
        switch (bFf().mo6893i(cnC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cnC, new Object[]{fAVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cnC, new Object[]{fAVar}));
                break;
        }
        return m736D(fAVar);
    }

    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m737RJ();
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m738RL();
    }

    /* renamed from: RO */
    public void mo67RO() {
        switch (bFf().mo6893i(aLX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                break;
        }
        m739RN();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aMY(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Mission._m_methodCount) {
            case 0:
                aze();
                return null;
            case 1:
                m775jF();
                return null;
            case 2:
                m750a((MissionAction.C2210a) args[0], (String) args[1]);
                return null;
            case 3:
                azg();
                return null;
            case 4:
                azh();
                return null;
            case 5:
                m778o((Class) args[0]);
                return null;
            case 6:
                m738RL();
                return null;
            case 7:
                m737RJ();
                return null;
            case 8:
                m786w((Ship) args[0]);
                return null;
            case 9:
                m744a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            case 10:
                m755a((String) args[0], (Ship) args[1], (Ship) args[2]);
                return null;
            case 11:
                m779p((Station) args[0]);
                return null;
            case 12:
                m783r((Station) args[0]);
                return null;
            case 13:
                m765d((Node) args[0]);
                return null;
            case 14:
                m752a((String) args[0], (Station) args[1]);
                return null;
            case 15:
                m776k((Gate) args[0]);
                return null;
            case 16:
                m777n((Ship) args[0]);
                return null;
            case 17:
                m785t((Station) args[0]);
                return null;
            case 18:
                m746a((MissionTrigger) args[0]);
                return null;
            case 19:
                akJ();
                return null;
            case 20:
                m753a((String) args[0], (Character) args[1]);
                return null;
            case 21:
                m759b((Loot) args[0]);
                return null;
            case 22:
                m748a((Character) args[0], (Loot) args[1]);
                return null;
            case 23:
                m764cq((String) args[0]);
                return null;
            case 24:
                m773i((Ship) args[0]);
                return null;
            case 25:
                m754a((String) args[0], (Ship) args[1]);
                return null;
            case 26:
                m772g((Ship) args[0]);
                return null;
            case 27:
                m763c((String) args[0], (Ship) args[1]);
                return null;
            case 28:
                m757aV((String) args[0]);
                return null;
            case 29:
                return new Boolean(m761bk((String) args[0]));
            case 30:
                m784s((String) args[0], (String) args[1]);
                return null;
            case 31:
                m745a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 32:
                akL();
                return null;
            case 33:
                m739RN();
                return null;
            case 34:
                m749a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 35:
                m762c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 36:
                return m743a((String) args[0], (NPCType) args[1], (WaypointDat) args[2], (StellarSystem) args[3]);
            case 37:
                return m742a((String) args[0], (NPCType) args[1], (Vec3d) args[2], ((Float) args[3]).floatValue(), (Vec3d) args[4], ((Float) args[5]).floatValue(), (StellarSystem) args[6], (Ship) args[7]);
            case 38:
                return m741a((String) args[0], (NPCType) args[1], (Vec3d) args[2], ((Float) args[3]).floatValue(), (Vec3d) args[4], ((Float) args[5]).floatValue(), (StellarSystem) args[6], (Character) args[7]);
            case 39:
                return new Boolean(m782q((ItemType) args[0]));
            case 40:
                return new Boolean(m756a((ItemType) args[0], ((Integer) args[1]).intValue()));
            case 41:
                return new Boolean(m736D((Ship) args[0]));
            case 42:
                return new Boolean(m769ea(((Long) args[0]).longValue()));
            case 43:
                m751a((String) args[0], (Mission.C0015a) args[1]);
                return null;
            case 44:
                m774j((String) args[0], ((Integer) args[1]).intValue());
                return null;
            case 45:
                m770f((String) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 46:
                m766dI((String) args[0]);
                return null;
            case 47:
                m767dK((String) args[0]);
                return null;
            case 48:
                return azj();
            case 49:
                return m768dM((String) args[0]);
            case 50:
                m781q((Class) args[0]);
                return null;
            case 51:
                return azl();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - Mission._m_methodCount) {
            case 0:
                m771f(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m757aV(str);
    }

    public void akK() {
        switch (bFf().mo6893i(byP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                break;
        }
        akJ();
    }

    public void akM() {
        switch (bFf().mo6893i(byS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                break;
        }
        akL();
    }

    @C6580apg(cpn = true)
    public void azf() {
        switch (bFf().mo6893i(cns)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cns, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cns, new Object[0]));
                break;
        }
        aze();
    }

    public List<MissionObjective> azk() {
        switch (bFf().mo6893i(cnJ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cnJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cnJ, new Object[0]));
                break;
        }
        return azj();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public MissionScriptState azm() {
        switch (bFf().mo6893i(cnM)) {
            case 0:
                return null;
            case 2:
                return (MissionScriptState) bFf().mo5606d(new aCE(this, cnM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cnM, new Object[0]));
                break;
        }
        return azl();
    }

    @C5566aOg
    /* renamed from: b */
    public NPC mo563b(String str, NPCType aed, Vec3d ajr, float f, Vec3d ajr2, float f2, StellarSystem jj, Character acx) {
        switch (bFf().mo6893i(cnz)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, cnz, new Object[]{str, aed, ajr, new Float(f), ajr2, new Float(f2), jj, acx}));
            case 3:
                bFf().mo5606d(new aCE(this, cnz, new Object[]{str, aed, ajr, new Float(f), ajr2, new Float(f2), jj, acx}));
                break;
        }
        return m741a(str, aed, ajr, f, ajr2, f2, jj, acx);
    }

    @C5566aOg
    /* renamed from: b */
    public NPC mo564b(String str, NPCType aed, Vec3d ajr, float f, Vec3d ajr2, float f2, StellarSystem jj, Ship fAVar) {
        switch (bFf().mo6893i(cny)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, cny, new Object[]{str, aed, ajr, new Float(f), ajr2, new Float(f2), jj, fAVar}));
            case 3:
                bFf().mo5606d(new aCE(this, cny, new Object[]{str, aed, ajr, new Float(f), ajr2, new Float(f2), jj, fAVar}));
                break;
        }
        return m742a(str, aed, ajr, f, ajr2, f2, jj, fAVar);
    }

    @C5566aOg
    /* renamed from: b */
    public NPC mo565b(String str, NPCType aed, WaypointDat cDVar, StellarSystem jj) {
        switch (bFf().mo6893i(cnx)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, cnx, new Object[]{str, aed, cDVar, jj}));
            case 3:
                bFf().mo5606d(new aCE(this, cnx, new Object[]{str, aed, cDVar, jj}));
                break;
        }
        return m743a(str, aed, cDVar, jj);
    }

    /* renamed from: b */
    public void mo85b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m744a(cr, acm);
    }

    /* renamed from: b */
    public void mo86b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m745a(ahc, jCVar, i);
    }

    /* renamed from: b */
    public void mo87b(MissionTrigger aus) {
        switch (bFf().mo6893i(byO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                break;
        }
        m746a(aus);
    }

    /* renamed from: b */
    public void mo88b(Character acx, Loot ael) {
        switch (bFf().mo6893i(byE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                break;
        }
        m748a(acx, ael);
    }

    /* renamed from: b */
    public void mo90b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m749a(auq, aag, aag2);
    }

    /* renamed from: b */
    public void mo566b(String str, Mission.C0015a aVar) {
        switch (bFf().mo6893i(cnE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnE, new Object[]{str, aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnE, new Object[]{str, aVar}));
                break;
        }
        m751a(str, aVar);
    }

    /* renamed from: b */
    public void mo91b(String str, Station bf) {
        switch (bFf().mo6893i(byB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                break;
        }
        m752a(str, bf);
    }

    /* renamed from: b */
    public void mo93b(String str, Character acx) {
        switch (bFf().mo6893i(byC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                break;
        }
        m753a(str, acx);
    }

    /* renamed from: b */
    public void mo94b(String str, Ship fAVar) {
        switch (bFf().mo6893i(byF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                break;
        }
        m754a(str, fAVar);
    }

    /* renamed from: b */
    public void mo95b(String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(beG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                break;
        }
        m755a(str, fAVar, fAVar2);
    }

    @C5566aOg
    /* renamed from: b */
    public boolean mo567b(ItemType jCVar, int i) {
        switch (bFf().mo6893i(cnB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cnB, new Object[]{jCVar, new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cnB, new Object[]{jCVar, new Integer(i)}));
                break;
        }
        return m756a(jCVar, i);
    }

    /* renamed from: bl */
    public boolean mo97bl(String str) {
        switch (bFf().mo6893i(beF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                break;
        }
        return m761bk(str);
    }

    /* renamed from: c */
    public void mo99c(Loot ael) {
        switch (bFf().mo6893i(byD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                break;
        }
        m759b(ael);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cr */
    public void mo115cr(String str) {
        switch (bFf().mo6893i(byQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                break;
        }
        m764cq(str);
    }

    /* renamed from: d */
    public void mo117d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m762c(auq, aag);
    }

    /* renamed from: d */
    public void mo119d(String str, Ship fAVar) {
        switch (bFf().mo6893i(byG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                break;
        }
        m763c(str, fAVar);
    }

    /* renamed from: dJ */
    public void mo568dJ(String str) {
        switch (bFf().mo6893i(cnH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnH, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnH, new Object[]{str}));
                break;
        }
        m766dI(str);
    }

    /* renamed from: dL */
    public void mo569dL(String str) {
        switch (bFf().mo6893i(cnI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnI, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnI, new Object[]{str}));
                break;
        }
        m767dK(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: dN */
    public MissionObjective mo570dN(String str) {
        switch (bFf().mo6893i(cnK)) {
            case 0:
                return null;
            case 2:
                return (MissionObjective) bFf().mo5606d(new aCE(this, cnK, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, cnK, new Object[]{str}));
                break;
        }
        return m768dM(str);
    }

    /* renamed from: e */
    public void mo121e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m765d(rPVar);
    }

    @C5566aOg
    /* renamed from: eb */
    public boolean mo571eb(long j) {
        switch (bFf().mo6893i(cnD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cnD, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cnD, new Object[]{new Long(j)}));
                break;
        }
        return m769ea(j);
    }

    /* renamed from: g */
    public void mo572g(String str, boolean z) {
        switch (bFf().mo6893i(cnG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnG, new Object[]{str, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnG, new Object[]{str, new Boolean(z)}));
                break;
        }
        m770f(str, z);
    }

    /* renamed from: h */
    public void mo125h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m772g(fAVar);
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f126yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f126yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f126yH, new Object[0]));
                break;
        }
        m775jF();
    }

    /* renamed from: j */
    public void mo130j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m773i(fAVar);
    }

    /* renamed from: k */
    public void mo574k(String str, int i) {
        switch (bFf().mo6893i(cnF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnF, new Object[]{str, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnF, new Object[]{str, new Integer(i)}));
                break;
        }
        m774j(str, i);
    }

    /* renamed from: l */
    public void mo131l(Gate fFVar) {
        switch (bFf().mo6893i(byM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                break;
        }
        m776k(fFVar);
    }

    /* renamed from: o */
    public void mo132o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m777n(fAVar);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: p */
    public void mo575p(Class cls) {
        switch (bFf().mo6893i(cnw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnw, new Object[]{cls}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnw, new Object[]{cls}));
                break;
        }
        m778o(cls);
    }

    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m779p(bf);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: r */
    public void mo576r(Class cls) {
        switch (bFf().mo6893i(cnL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cnL, new Object[]{cls}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cnL, new Object[]{cls}));
                break;
        }
        m781q(cls);
    }

    @C5566aOg
    /* renamed from: r */
    public boolean mo577r(ItemType jCVar) {
        switch (bFf().mo6893i(cnA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cnA, new Object[]{jCVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cnA, new Object[]{jCVar}));
                break;
        }
        return m782q(jCVar);
    }

    /* renamed from: s */
    public void mo134s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m783r(bf);
    }

    /* renamed from: t */
    public void mo135t(String str, String str2) {
        switch (bFf().mo6893i(byH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                break;
        }
        m784s(str, str2);
    }

    /* renamed from: u */
    public void mo136u(Station bf) {
        switch (bFf().mo6893i(byN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                break;
        }
        m785t(bf);
    }

    /* renamed from: x */
    public void mo138x(Ship fAVar) {
        switch (bFf().mo6893i(byI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                break;
        }
        m786w(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "665b3d1edd6b8cf9a7ce551babeb13d5", aum = 0)
    /* renamed from: RL */
    private void m738RL() {
        m760b(MissionAction.C2210a.ABORT, (String) null);
        checkState();
        cdr().setParameter(String.valueOf(((MissionScriptTemplate) cdp()).getHandle()) + "_state", (String) null);
        azb().mo6623RM();
    }

    @C0064Am(aul = "e0dd0401297c0cc84514a6d8504ef564", aum = 0)
    /* renamed from: RJ */
    private void m737RJ() {
        m760b(MissionAction.C2210a.ACCEPT, (String) null);
        azc().clear();
        for (MissionTemplateObjective next : ((MissionScriptTemplate) cdp()).cKb()) {
            MissionObjective sGVar = (MissionObjective) bFf().mo6865M(MissionObjective.class);
            sGVar.mo21866a(next.getHandle(), next.mo4247vW(), next.mo11020Iq(), next.isHidden());
            sGVar.mo21870c(Mission.C0015a.ACTIVE);
            sGVar.mo21871eA(0);
            azc().add(sGVar);
            sGVar.push();
        }
        if (((MissionScriptTemplate) cdp()).cKf() > 0) {
            mo101c(cnj, (long) ((MissionScriptTemplate) cdp()).cKf(), 10);
        }
        long cKh = ((MissionScriptTemplate) cdp()).cKh();
        if (cKh > 0 && !mo571eb(cKh)) {
            fail();
        }
        init();
    }

    @C0064Am(aul = "3f9f29ad255593ab0853eabcee076f30", aum = 0)
    /* renamed from: w */
    private void m786w(Ship fAVar) {
        m760b(MissionAction.C2210a.PLAYER_DIED, (String) null);
        checkState();
        azb().mo12270x(fAVar);
    }

    @C0064Am(aul = "9bd06fa5dd4d6fadeeba7feaef852745", aum = 0)
    /* renamed from: a */
    private void m744a(Actor cr, C5260aCm acm) {
        checkState();
        azb().mo12253b(cr, acm);
    }

    @C0064Am(aul = "eaf15185a0d630d871649dbbaeda3b64", aum = 0)
    /* renamed from: a */
    private void m755a(String str, Ship fAVar, Ship fAVar2) {
        checkState();
        azb().mo8419b(str, fAVar, fAVar2);
    }

    @C0064Am(aul = "8884851e4b8aee3ac11f30f6e0621cc3", aum = 0)
    /* renamed from: p */
    private void m779p(Station bf) {
        checkState();
        azb().mo4971q(bf);
    }

    @C0064Am(aul = "3caf05fe4aad7b4e0042c202f9c0896b", aum = 0)
    /* renamed from: r */
    private void m783r(Station bf) {
        m760b(MissionAction.C2210a.UNDOCK, bf.getHandle());
        C5426aIw cdr = cdr();
        if ((cdr instanceof LDScriptingController) && ((LDScriptingController) cdr).cWL() == this) {
            cdg();
        }
        checkState();
        azb().mo12267s(bf);
    }

    @C0064Am(aul = "e77a769a55819919e354eb5fcf3bbf8a", aum = 0)
    /* renamed from: d */
    private void m765d(Node rPVar) {
        C5426aIw cdr = cdr();
        if ((cdr instanceof LDScriptingController) && ((LDScriptingController) cdr).cWL() == this) {
            cdg();
        }
        checkState();
        azb().mo12262e(rPVar);
    }

    @C0064Am(aul = "62c0bf28301e08ee2a05ff96432f2bad", aum = 0)
    /* renamed from: a */
    private void m752a(String str, Station bf) {
        checkState();
        azb().mo12260d(str, bf);
    }

    @C0064Am(aul = "a8227574f1330c65547d7f3efb632e45", aum = 0)
    /* renamed from: k */
    private void m776k(Gate fFVar) {
        checkState();
        azb().mo12265l(fFVar);
    }

    @C0064Am(aul = "edf940ffbf8978f6bb9ffdacb1cf3d83", aum = 0)
    /* renamed from: n */
    private void m777n(Ship fAVar) {
        checkState();
        azb().mo12266o(fAVar);
    }

    @C0064Am(aul = "230ecde0c5913bda284a5d1f8424182c", aum = 0)
    /* renamed from: t */
    private void m785t(Station bf) {
        checkState();
        azb().mo12269u(bf);
    }

    @C0064Am(aul = "181a718c0f43b2a96c13ec93970cc3a6", aum = 0)
    /* renamed from: a */
    private void m746a(MissionTrigger aus) {
        m760b(MissionAction.C2210a.WAYPOINT_SELECTED, aus.dAB());
        checkState();
        azb().mo12254b(aus);
    }

    @C0064Am(aul = "1101a99c73955ed1720f9c36d745d26a", aum = 0)
    private void akJ() {
        checkState();
        azb().akK();
    }

    @C0064Am(aul = "dd63dce1aa572a0d1f46793b3e66b904", aum = 0)
    /* renamed from: a */
    private void m753a(String str, Character acx) {
        checkState();
        azb().mo8417b(str, acx);
    }

    @C0064Am(aul = "adc0d9fc345379e8b6fddd3afb2cf565", aum = 0)
    /* renamed from: b */
    private void m759b(Loot ael) {
        mo88b((Character) null, ael);
    }

    @C0064Am(aul = "f2f10a10b03d9842351bd9df9781eaf2", aum = 0)
    /* renamed from: a */
    private void m748a(Character acx, Loot ael) {
        checkState();
        azb().mo12255b(acx, ael);
    }

    @C0064Am(aul = "cc566943ca2f8afbae1bb1da0c422250", aum = 0)
    /* renamed from: cq */
    private void m764cq(String str) {
        m760b(MissionAction.C2210a.NPC_CHAT, str);
        checkState();
        azb().mo6626cr(str);
    }

    @C0064Am(aul = "b33252aae43193bb27d1443428670944", aum = 0)
    /* renamed from: i */
    private void m773i(Ship fAVar) {
        Character agj = fAVar.agj();
        if (agj instanceof NPC) {
            m760b(MissionAction.C2210a.NPC_KILLED, ((NPC) agj).mo11654Fs().getHandle());
        }
        checkState();
        azb().mo12264j(fAVar);
    }

    @C0064Am(aul = "61b1e3d4c7dba24cec50d46d6b5b5cfa", aum = 0)
    /* renamed from: a */
    private void m754a(String str, Ship fAVar) {
        checkState();
        azb().mo12257b(str, fAVar);
    }

    @C0064Am(aul = "8e97c2972e635557842692917c34243a", aum = 0)
    /* renamed from: g */
    private void m772g(Ship fAVar) {
        checkState();
        azb().mo12263h(fAVar);
    }

    @C0064Am(aul = "27f54036d4f24deb401abec33f1f3265", aum = 0)
    /* renamed from: c */
    private void m763c(String str, Ship fAVar) {
        checkState();
        azb().mo12261d(str, fAVar);
    }

    @C0064Am(aul = "770a3c9227dfcd86a9f67e9ceb1fd434", aum = 0)
    /* renamed from: aV */
    private void m757aV(String str) {
        m760b(MissionAction.C2210a.TIMER, str);
        if (str.equals(cnj)) {
            fail();
            return;
        }
        checkState();
        azb().mo6624aW(str);
    }

    @C0064Am(aul = "4827ffd6bc0140a8bc458252a3e0d077", aum = 0)
    /* renamed from: bk */
    private boolean m761bk(String str) {
        m760b(MissionAction.C2210a.WAYPOINT_REACHED, str);
        checkState();
        return azb().mo4962bl(str);
    }

    @C0064Am(aul = "49adcb9323808237a62b5d7501df9795", aum = 0)
    /* renamed from: s */
    private void m784s(String str, String str2) {
        checkState();
        azb().mo12268t(str, str2);
    }

    @C0064Am(aul = "766e7eb36803a0f9b64e210f14194db6", aum = 0)
    /* renamed from: a */
    private void m745a(LootType ahc, ItemType jCVar, int i) {
        if (ahc instanceof AsteroidLootType) {
            m760b(MissionAction.C2210a.MINE, jCVar.getHandle());
        } else {
            m760b(MissionAction.C2210a.LOOT, jCVar.getHandle());
        }
        checkState();
        azb().mo8416b(ahc, jCVar, i);
    }

    @C0064Am(aul = "d36fe7ac1a1a3217c6a911c71210a41d", aum = 0)
    private void akL() {
        m760b(MissionAction.C2210a.ACCOMPLISH, (String) null);
    }

    @C0064Am(aul = "51e0607b72e0ecb430cf75d276ef3070", aum = 0)
    /* renamed from: RN */
    private void m739RN() {
        m760b(MissionAction.C2210a.FAIL, (String) null);
    }

    @C0064Am(aul = "10261ca56fe87317fa49189e6e3fcc4a", aum = 0)
    /* renamed from: a */
    private void m749a(Item auq, ItemLocation aag, ItemLocation aag2) {
        checkState();
        azb().mo12256b(auq, aag, aag2);
    }

    @C0064Am(aul = "b151645268c3a2de4c75a585e5beb454", aum = 0)
    /* renamed from: c */
    private void m762c(Item auq, ItemLocation aag) {
        checkState();
        azb().mo12259d(auq, aag);
    }

    @C0064Am(aul = "4a8a51a2a9f1fd2217136e2a7ba4ed7f", aum = 0)
    /* renamed from: a */
    private void m751a(String str, Mission.C0015a aVar) {
        if (str != null) {
            boolean z = false;
            for (MissionObjective sGVar : azc()) {
                if (str.equals(sGVar.getHandle())) {
                    sGVar.mo21870c(aVar);
                    if (aVar == Mission.C0015a.ACCOMPLISHED) {
                        sGVar.mo21871eA(sGVar.abR());
                    }
                    z = true;
                }
            }
            if (z) {
                azf();
            }
        }
    }

    @C0064Am(aul = "84cf33b1296406bc653635c48f65ac29", aum = 0)
    /* renamed from: j */
    private void m774j(String str, int i) {
        if (str != null) {
            boolean z = false;
            for (MissionObjective sGVar : azc()) {
                if (str.equals(sGVar.getHandle())) {
                    sGVar.mo21871eA(i);
                    z = true;
                }
            }
            if (z) {
                azf();
            }
        }
    }

    @C0064Am(aul = "0fb823ff7b013a4c0a4456ad0dccdb6d", aum = 0)
    /* renamed from: f */
    private void m770f(String str, boolean z) {
        if (str != null) {
            boolean z2 = false;
            for (MissionObjective sGVar : azc()) {
                if (str.equals(sGVar.getHandle())) {
                    sGVar.setHidden(z);
                    z2 = true;
                }
            }
            if (z2) {
                azf();
            }
        }
    }

    @C0064Am(aul = "7284457cb4c8361b6fe49826383e7a42", aum = 0)
    /* renamed from: dI */
    private void m766dI(String str) {
        mo572g(str, false);
    }

    @C0064Am(aul = "837bf554ae34cd90d0f4fee6f4ccef82", aum = 0)
    /* renamed from: dK */
    private void m767dK(String str) {
        mo572g(str, true);
    }

    @C0064Am(aul = "2d3e1a323dbf55ca1414c209c0067eb9", aum = 0)
    private List<MissionObjective> azj() {
        return new ArrayList(azc());
    }
}
