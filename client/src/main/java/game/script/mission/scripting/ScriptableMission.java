package game.script.mission.scripting;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.*;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Character;
import game.script.hazardarea.HazardArea.HazardArea;
import game.script.item.BulkItemType;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.mission.*;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.progression.ProgressionCareer;
import game.script.resource.Asset;
import game.script.resource.AssetGroup;
import game.script.ship.*;
import game.script.space.*;
import game.script.spacezone.SpaceZone;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.data.link.C2043bU;
import logic.data.link.C3981xi;
import logic.data.mbean.C6248ajM;
import logic.data.mbean.C6695arr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import javax.vecmath.Tuple3d;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.*;

@C6485anp
@C5511aMd
/* renamed from: a.gE */
/* compiled from: a */
public class ScriptableMission<T extends ScriptableMissionTemplate> extends Mission<T> implements C3981xi.C3982a, C3981xi.C3982a {

    /* renamed from: Ll */
    public static final C2491fm f7545Ll = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLS = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLX = null;
    public static final C2491fm aLY = null;
    public static final C2491fm aMe = null;
    public static final C2491fm bYI = null;
    public static final C2491fm beF = null;
    public static final C2491fm beG = null;
    public static final C2491fm beH = null;
    public static final C2491fm byB = null;
    public static final C2491fm byC = null;
    public static final C2491fm byD = null;
    public static final C2491fm byE = null;
    public static final C2491fm byF = null;
    public static final C2491fm byG = null;
    public static final C2491fm byH = null;
    public static final C2491fm byI = null;
    public static final C2491fm byJ = null;
    public static final C2491fm byK = null;
    public static final C2491fm byL = null;
    public static final C2491fm byM = null;
    public static final C2491fm byN = null;
    public static final C2491fm byO = null;
    public static final C2491fm byP = null;
    public static final C2491fm byQ = null;
    public static final C2491fm byR = null;
    public static final C2491fm byS = null;
    public static final C2491fm cnJ = null;
    public static final C2491fm cnK = null;
    public static final C5663aRz cnp = null;
    public static final C2491fm coD = null;
    public static final C5663aRz dEF = null;
    public static final C5663aRz dEH = null;
    public static final C5663aRz dEJ = null;
    public static final C5663aRz dEK = null;
    public static final C5663aRz dEM = null;
    public static final C2491fm dEN = null;
    public static final C2491fm dEO = null;
    public static final C2491fm dEP = null;
    public static final C2491fm dEQ = null;
    public static final C2491fm dER = null;
    public static final C2491fm dES = null;
    public static final C2491fm dET = null;
    public static final C2491fm dEU = null;
    public static final C2491fm dEV = null;
    public static final C2491fm dEW = null;
    public static final C2491fm dEX = null;
    public static final C2491fm dEY = null;
    public static final C2491fm dEZ = null;
    public static final C2491fm dFA = null;
    public static final C2491fm dFB = null;
    public static final C2491fm dFC = null;
    public static final C2491fm dFD = null;
    public static final C2491fm dFE = null;
    public static final C2491fm dFF = null;
    public static final C2491fm dFG = null;
    public static final C2491fm dFH = null;
    public static final C2491fm dFI = null;
    public static final C2491fm dFJ = null;
    public static final C2491fm dFK = null;
    public static final C2491fm dFL = null;
    public static final C2491fm dFM = null;
    public static final C2491fm dFN = null;
    public static final C2491fm dFO = null;
    public static final C2491fm dFP = null;
    public static final C2491fm dFQ = null;
    public static final C2491fm dFR = null;
    public static final C2491fm dFS = null;
    public static final C2491fm dFT = null;
    public static final C2491fm dFU = null;
    public static final C2491fm dFV = null;
    public static final C2491fm dFW = null;
    public static final C2491fm dFX = null;
    public static final C2491fm dFY = null;
    public static final C2491fm dFZ = null;
    public static final C2491fm dFa = null;
    public static final C2491fm dFb = null;
    public static final C2491fm dFc = null;
    public static final C2491fm dFd = null;
    public static final C2491fm dFe = null;
    public static final C2491fm dFf = null;
    public static final C2491fm dFg = null;
    public static final C2491fm dFh = null;
    public static final C2491fm dFi = null;
    public static final C2491fm dFj = null;
    public static final C2491fm dFk = null;
    public static final C2491fm dFl = null;
    public static final C2491fm dFm = null;
    public static final C2491fm dFn = null;
    public static final C2491fm dFo = null;
    public static final C2491fm dFp = null;
    public static final C2491fm dFq = null;
    public static final C2491fm dFr = null;
    public static final C2491fm dFs = null;
    public static final C2491fm dFt = null;
    public static final C2491fm dFu = null;
    public static final C2491fm dFv = null;
    public static final C2491fm dFw = null;
    public static final C2491fm dFx = null;
    public static final C2491fm dFy = null;
    public static final C2491fm dFz = null;
    public static final C2491fm dGa = null;
    public static final C2491fm dGb = null;
    public static final C2491fm dGc = null;
    public static final C2491fm dGd = null;
    public static final C2491fm dGe = null;
    public static final C2491fm dGf = null;
    public static final C2491fm dGg = null;
    public static final C2491fm dGh = null;
    public static final C2491fm dGi = null;
    public static final C2491fm dGj = null;
    public static final C2491fm dGk = null;
    private static final long serialVersionUID = 5209865129077048789L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "90d7d01b2da9060c6ff48fdddb3d90dd", aum = 4)
    private static C1556Wo<Ship, String> bdV = null;
    @C0064Am(aul = "24b2bb569dd5fd90fcb23e8ad522a7c6", aum = 2)
    private static C3438ra<MissionObjective> cno = null;
    @C0064Am(aul = "c824d0bf3174abe446bbcc1f63f95249", aum = 0)
    @C5566aOg
    private static C1556Wo<String, Object> dEE = null;
    @C0064Am(aul = "862c9a53f226eca350a7b7343df0dae1", aum = 1)
    private static C3438ra<GatePass> dEG = null;
    @C0064Am(aul = "44c3374bdc95f73b89160343885a9024", aum = 3)
    private static C1556Wo<String, NPC> dEI = null;
    @C0064Am(aul = "5391084ae672014f8e0d139e5a878e61", aum = 5)
    @C5566aOg
    private static float dEL = 0.0f;

    static {
        m31670V();
    }

    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C5566aOg
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: TR */
    private transient org.mozilla1.javascript.Scriptable f7546TR;
    @C5566aOg
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private transient ThreadLocal<org.mozilla1.javascript.Context> daB;

    public ScriptableMission() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScriptableMission(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m31670V() {
        _m_fieldCount = Mission._m_fieldCount + 6;
        _m_methodCount = Mission._m_methodCount + 111;
        int i = Mission._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ScriptableMission.class, "c824d0bf3174abe446bbcc1f63f95249", i);
        dEF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ScriptableMission.class, "862c9a53f226eca350a7b7343df0dae1", i2);
        dEH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ScriptableMission.class, "24b2bb569dd5fd90fcb23e8ad522a7c6", i3);
        cnp = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ScriptableMission.class, "44c3374bdc95f73b89160343885a9024", i4);
        dEJ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ScriptableMission.class, "90d7d01b2da9060c6ff48fdddb3d90dd", i5);
        dEK = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ScriptableMission.class, "5391084ae672014f8e0d139e5a878e61", i6);
        dEM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Mission._m_fields, (Object[]) _m_fields);
        int i8 = Mission._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 111)];
        C2491fm a = C4105zY.m41624a(ScriptableMission.class, "82591d633f5fe02cda002e472fca2219", i8);
        dEN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ScriptableMission.class, "6fd7ce974fef7beb3d0fa9f46d4da41f", i9);
        dEO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ScriptableMission.class, "ca714447c341dc28d1544c8a81613a45", i10);
        dEP = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ScriptableMission.class, "0574970a327181cf11d9451d5a599fe9", i11);
        dEQ = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ScriptableMission.class, "a633fd5903ee18619df8745629eadba4", i12);
        dER = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ScriptableMission.class, "536d975a91521121207014d618146a24", i13);
        dES = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ScriptableMission.class, "fe3641a228bde0da2baaaa9d470044a5", i14);
        dET = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ScriptableMission.class, "b07e743db060154c610cbda51eb336c2", i15);
        cnJ = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ScriptableMission.class, "7c78c56912faba26ff28d46e04756d36", i16);
        dEU = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ScriptableMission.class, "e09d4e446ee7821c065fbe704e4cfbd8", i17);
        dEV = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ScriptableMission.class, "3b0a1b52a9d58bca9835d4a1e332dc10", i18);
        dEW = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ScriptableMission.class, "5b88fc3a2a169978b8c36ac6137b1953", i19);
        dEX = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ScriptableMission.class, "a8a99cbc3ea55d9e9b4ff77871358b21", i20);
        dEY = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(ScriptableMission.class, "0ffe02d41a6cbedacbba8f60979ae67c", i21);
        dEZ = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(ScriptableMission.class, "0c698580472f8aeb4b039b8242f4951c", i22);
        dFa = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(ScriptableMission.class, "5a32572d70eef184af7bd538ed4fdc0c", i23);
        dFb = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(ScriptableMission.class, "ce49c951a0077c5ae6a833bc4daecb21", i24);
        aLU = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        C2491fm a18 = C4105zY.m41624a(ScriptableMission.class, "87618186f7b200733de89f311eb1ccf4", i25);
        aLT = a18;
        fmVarArr[i25] = a18;
        int i26 = i25 + 1;
        C2491fm a19 = C4105zY.m41624a(ScriptableMission.class, "e0c96bc566f4fe327819cec9f1c34b97", i26);
        byS = a19;
        fmVarArr[i26] = a19;
        int i27 = i26 + 1;
        C2491fm a20 = C4105zY.m41624a(ScriptableMission.class, "ddc6157838d940dac25c38db67ac93d2", i27);
        byI = a20;
        fmVarArr[i27] = a20;
        int i28 = i27 + 1;
        C2491fm a21 = C4105zY.m41624a(ScriptableMission.class, "371ef7da0be46e256f84098a613f6037", i28);
        aLY = a21;
        fmVarArr[i28] = a21;
        int i29 = i28 + 1;
        C2491fm a22 = C4105zY.m41624a(ScriptableMission.class, "eb4057beb52b376c994edf4f91a18672", i29);
        beG = a22;
        fmVarArr[i29] = a22;
        int i30 = i29 + 1;
        C2491fm a23 = C4105zY.m41624a(ScriptableMission.class, "f37cc57c17dcd3aaccadb6d86ed0d512", i30);
        byJ = a23;
        fmVarArr[i30] = a23;
        int i31 = i30 + 1;
        C2491fm a24 = C4105zY.m41624a(ScriptableMission.class, "c9d760b24c4bd6608fe323c8a4cbde66", i31);
        byK = a24;
        fmVarArr[i31] = a24;
        int i32 = i31 + 1;
        C2491fm a25 = C4105zY.m41624a(ScriptableMission.class, "5b5b17bab3906383e09dc8ac8f0bac73", i32);
        byB = a25;
        fmVarArr[i32] = a25;
        int i33 = i32 + 1;
        C2491fm a26 = C4105zY.m41624a(ScriptableMission.class, "52a4bfc1cc25a04a1ff1e635462d8abf", i33);
        bYI = a26;
        fmVarArr[i33] = a26;
        int i34 = i33 + 1;
        C2491fm a27 = C4105zY.m41624a(ScriptableMission.class, "b3a28ab9c47817433fe4e0c57fc9d807", i34);
        byM = a27;
        fmVarArr[i34] = a27;
        int i35 = i34 + 1;
        C2491fm a28 = C4105zY.m41624a(ScriptableMission.class, "6993b06a12998cd5874b02d8942dc120", i35);
        beH = a28;
        fmVarArr[i35] = a28;
        int i36 = i35 + 1;
        C2491fm a29 = C4105zY.m41624a(ScriptableMission.class, "c9ec6a76b7ddd44c40f7b54ff88c9975", i36);
        byN = a29;
        fmVarArr[i36] = a29;
        int i37 = i36 + 1;
        C2491fm a30 = C4105zY.m41624a(ScriptableMission.class, "4d77ddce2260c0250ff190246ebecf6b", i37);
        byO = a30;
        fmVarArr[i37] = a30;
        int i38 = i37 + 1;
        C2491fm a31 = C4105zY.m41624a(ScriptableMission.class, "2adf6032cf79fed9c3767596378e8dd7", i38);
        byP = a31;
        fmVarArr[i38] = a31;
        int i39 = i38 + 1;
        C2491fm a32 = C4105zY.m41624a(ScriptableMission.class, "7db4243e5c1da5e2ad25d368d5d06fca", i39);
        byL = a32;
        fmVarArr[i39] = a32;
        int i40 = i39 + 1;
        C2491fm a33 = C4105zY.m41624a(ScriptableMission.class, "bba7812ae906083717305bd525cc223d", i40);
        byR = a33;
        fmVarArr[i40] = a33;
        int i41 = i40 + 1;
        C2491fm a34 = C4105zY.m41624a(ScriptableMission.class, "60720039f081cfe38552726011cb03d3", i41);
        byD = a34;
        fmVarArr[i41] = a34;
        int i42 = i41 + 1;
        C2491fm a35 = C4105zY.m41624a(ScriptableMission.class, "c769f7347842da22c259c53170382d93", i42);
        byE = a35;
        fmVarArr[i42] = a35;
        int i43 = i42 + 1;
        C2491fm a36 = C4105zY.m41624a(ScriptableMission.class, "082aac475d1058d8192bbd66d5129780", i43);
        dFc = a36;
        fmVarArr[i43] = a36;
        int i44 = i43 + 1;
        C2491fm a37 = C4105zY.m41624a(ScriptableMission.class, "ea32a58c8f0ef45db444c7c37f643951", i44);
        byC = a37;
        fmVarArr[i44] = a37;
        int i45 = i44 + 1;
        C2491fm a38 = C4105zY.m41624a(ScriptableMission.class, "8e0feb5a23cf9b514bd01a6492648f2d", i45);
        byQ = a38;
        fmVarArr[i45] = a38;
        int i46 = i45 + 1;
        C2491fm a39 = C4105zY.m41624a(ScriptableMission.class, "217875629c6929652f836610791c3ce2", i46);
        aMe = a39;
        fmVarArr[i46] = a39;
        int i47 = i46 + 1;
        C2491fm a40 = C4105zY.m41624a(ScriptableMission.class, "51b007ced9616d76aa75350e55186a3d", i47);
        byF = a40;
        fmVarArr[i47] = a40;
        int i48 = i47 + 1;
        C2491fm a41 = C4105zY.m41624a(ScriptableMission.class, "44e4b54a451a3e84cda225fe2aaa3a96", i48);
        aLS = a41;
        fmVarArr[i48] = a41;
        int i49 = i48 + 1;
        C2491fm a42 = C4105zY.m41624a(ScriptableMission.class, "a781885204799ebda131eb5872ab612e", i49);
        byG = a42;
        fmVarArr[i49] = a42;
        int i50 = i49 + 1;
        C2491fm a43 = C4105zY.m41624a(ScriptableMission.class, "860354822547163b2894d31b52a0490a", i50);
        aLR = a43;
        fmVarArr[i50] = a43;
        int i51 = i50 + 1;
        C2491fm a44 = C4105zY.m41624a(ScriptableMission.class, "4fd23a52d4d8604b2a6822590c34dcc7", i51);
        beF = a44;
        fmVarArr[i51] = a44;
        int i52 = i51 + 1;
        C2491fm a45 = C4105zY.m41624a(ScriptableMission.class, "178034c38da8c21300f43d4697a01a62", i52);
        byH = a45;
        fmVarArr[i52] = a45;
        int i53 = i52 + 1;
        C2491fm a46 = C4105zY.m41624a(ScriptableMission.class, "e9344d1376df80a346b404bdb92ea494", i53);
        aLV = a46;
        fmVarArr[i53] = a46;
        int i54 = i53 + 1;
        C2491fm a47 = C4105zY.m41624a(ScriptableMission.class, "52e75c7b2a70c3de88004fcf9a5b20ae", i54);
        aLW = a47;
        fmVarArr[i54] = a47;
        int i55 = i54 + 1;
        C2491fm a48 = C4105zY.m41624a(ScriptableMission.class, "09ac25f9268e1efc56ff40774c2eb5c8", i55);
        aLX = a48;
        fmVarArr[i55] = a48;
        int i56 = i55 + 1;
        C2491fm a49 = C4105zY.m41624a(ScriptableMission.class, "53328c7a267d7a63dbd07635b2373f2e", i56);
        dFd = a49;
        fmVarArr[i56] = a49;
        int i57 = i56 + 1;
        C2491fm a50 = C4105zY.m41624a(ScriptableMission.class, "62fce9d80aa720457cd68ae39e8db56c", i57);
        dFe = a50;
        fmVarArr[i57] = a50;
        int i58 = i57 + 1;
        C2491fm a51 = C4105zY.m41624a(ScriptableMission.class, "6f916665ac95053dd3ff831a45dff4cb", i58);
        dFf = a51;
        fmVarArr[i58] = a51;
        int i59 = i58 + 1;
        C2491fm a52 = C4105zY.m41624a(ScriptableMission.class, "ce7b2c63cf3c04bf050210ef2779cca6", i59);
        dFg = a52;
        fmVarArr[i59] = a52;
        int i60 = i59 + 1;
        C2491fm a53 = C4105zY.m41624a(ScriptableMission.class, "8ddc6e63e5c93e5cd2ec774982964732", i60);
        dFh = a53;
        fmVarArr[i60] = a53;
        int i61 = i60 + 1;
        C2491fm a54 = C4105zY.m41624a(ScriptableMission.class, "54e354337bd3fdd2f051af098100d4bc", i61);
        dFi = a54;
        fmVarArr[i61] = a54;
        int i62 = i61 + 1;
        C2491fm a55 = C4105zY.m41624a(ScriptableMission.class, "1ded28d61c9f1bc19df7a545546eed79", i62);
        dFj = a55;
        fmVarArr[i62] = a55;
        int i63 = i62 + 1;
        C2491fm a56 = C4105zY.m41624a(ScriptableMission.class, "644f784590a0b9e8aedbfc1c644893b1", i63);
        dFk = a56;
        fmVarArr[i63] = a56;
        int i64 = i63 + 1;
        C2491fm a57 = C4105zY.m41624a(ScriptableMission.class, "40b450c4699a70c02acd0d87d92a01ef", i64);
        dFl = a57;
        fmVarArr[i64] = a57;
        int i65 = i64 + 1;
        C2491fm a58 = C4105zY.m41624a(ScriptableMission.class, "5912f0b8eeffd89391a840fb12643746", i65);
        dFm = a58;
        fmVarArr[i65] = a58;
        int i66 = i65 + 1;
        C2491fm a59 = C4105zY.m41624a(ScriptableMission.class, "9673cb07c5483bdd3dbb15b72b0c7e64", i66);
        dFn = a59;
        fmVarArr[i66] = a59;
        int i67 = i66 + 1;
        C2491fm a60 = C4105zY.m41624a(ScriptableMission.class, "0ec96e974491b5547a820020886587c6", i67);
        dFo = a60;
        fmVarArr[i67] = a60;
        int i68 = i67 + 1;
        C2491fm a61 = C4105zY.m41624a(ScriptableMission.class, "f6f01c74b169dd1024a8f9f52cbcccc3", i68);
        dFp = a61;
        fmVarArr[i68] = a61;
        int i69 = i68 + 1;
        C2491fm a62 = C4105zY.m41624a(ScriptableMission.class, "3652a856f8670c931516adf73f1bd0cc", i69);
        dFq = a62;
        fmVarArr[i69] = a62;
        int i70 = i69 + 1;
        C2491fm a63 = C4105zY.m41624a(ScriptableMission.class, "e3c22b4bf5b5f50dfb98c1c8fd6eaa49", i70);
        dFr = a63;
        fmVarArr[i70] = a63;
        int i71 = i70 + 1;
        C2491fm a64 = C4105zY.m41624a(ScriptableMission.class, "8bd3afc7c6c185b33299e15c23e0d88d", i71);
        dFs = a64;
        fmVarArr[i71] = a64;
        int i72 = i71 + 1;
        C2491fm a65 = C4105zY.m41624a(ScriptableMission.class, "7105945654b382e23b03255167683db2", i72);
        dFt = a65;
        fmVarArr[i72] = a65;
        int i73 = i72 + 1;
        C2491fm a66 = C4105zY.m41624a(ScriptableMission.class, "187f4904fbd03eca63531bfca502fb80", i73);
        dFu = a66;
        fmVarArr[i73] = a66;
        int i74 = i73 + 1;
        C2491fm a67 = C4105zY.m41624a(ScriptableMission.class, "cea78836ec94059e0c04f4fea16ff2d3", i74);
        dFv = a67;
        fmVarArr[i74] = a67;
        int i75 = i74 + 1;
        C2491fm a68 = C4105zY.m41624a(ScriptableMission.class, "c8b8cb03281d6d84dbe5c0ba6d0eec73", i75);
        dFw = a68;
        fmVarArr[i75] = a68;
        int i76 = i75 + 1;
        C2491fm a69 = C4105zY.m41624a(ScriptableMission.class, "846f8276275a59dc027e2d1da362ad83", i76);
        cnK = a69;
        fmVarArr[i76] = a69;
        int i77 = i76 + 1;
        C2491fm a70 = C4105zY.m41624a(ScriptableMission.class, "1f4de417f22cf1f49f4ffbd115ae093b", i77);
        dFx = a70;
        fmVarArr[i77] = a70;
        int i78 = i77 + 1;
        C2491fm a71 = C4105zY.m41624a(ScriptableMission.class, "520d885bff545ebe1d9d882e32759992", i78);
        dFy = a71;
        fmVarArr[i78] = a71;
        int i79 = i78 + 1;
        C2491fm a72 = C4105zY.m41624a(ScriptableMission.class, "bf57a8bbbfbc8ac70d862bdcb333cb1f", i79);
        dFz = a72;
        fmVarArr[i79] = a72;
        int i80 = i79 + 1;
        C2491fm a73 = C4105zY.m41624a(ScriptableMission.class, "e551ad9d2342e4c5c0af81aca377446f", i80);
        dFA = a73;
        fmVarArr[i80] = a73;
        int i81 = i80 + 1;
        C2491fm a74 = C4105zY.m41624a(ScriptableMission.class, "a05550b7b636c661300c80adcb3949c2", i81);
        dFB = a74;
        fmVarArr[i81] = a74;
        int i82 = i81 + 1;
        C2491fm a75 = C4105zY.m41624a(ScriptableMission.class, "154c1de454d3ea6f3dc43ec907946916", i82);
        dFC = a75;
        fmVarArr[i82] = a75;
        int i83 = i82 + 1;
        C2491fm a76 = C4105zY.m41624a(ScriptableMission.class, "b2cd6b54b264f8013f7ab88c61f16b6f", i83);
        dFD = a76;
        fmVarArr[i83] = a76;
        int i84 = i83 + 1;
        C2491fm a77 = C4105zY.m41624a(ScriptableMission.class, "c9a11498321ae3b319b943defe6054b2", i84);
        dFE = a77;
        fmVarArr[i84] = a77;
        int i85 = i84 + 1;
        C2491fm a78 = C4105zY.m41624a(ScriptableMission.class, "f23184f58eb32be42d9c53cda3eb10f9", i85);
        dFF = a78;
        fmVarArr[i85] = a78;
        int i86 = i85 + 1;
        C2491fm a79 = C4105zY.m41624a(ScriptableMission.class, "3c6dfe76e64770bc57b6532a6c1e6ef5", i86);
        dFG = a79;
        fmVarArr[i86] = a79;
        int i87 = i86 + 1;
        C2491fm a80 = C4105zY.m41624a(ScriptableMission.class, "ba4bb9bae18c920f19f44e596562e557", i87);
        dFH = a80;
        fmVarArr[i87] = a80;
        int i88 = i87 + 1;
        C2491fm a81 = C4105zY.m41624a(ScriptableMission.class, "d8d66c747675ee1d516415ea9c06883d", i88);
        dFI = a81;
        fmVarArr[i88] = a81;
        int i89 = i88 + 1;
        C2491fm a82 = C4105zY.m41624a(ScriptableMission.class, "9efc0945bcf278c13f1792a1704fdbce", i89);
        dFJ = a82;
        fmVarArr[i89] = a82;
        int i90 = i89 + 1;
        C2491fm a83 = C4105zY.m41624a(ScriptableMission.class, "a6c31740948edec8be0c4dc9246df101", i90);
        dFK = a83;
        fmVarArr[i90] = a83;
        int i91 = i90 + 1;
        C2491fm a84 = C4105zY.m41624a(ScriptableMission.class, "3856ceddbb713358b16a30c51fe507b2", i91);
        dFL = a84;
        fmVarArr[i91] = a84;
        int i92 = i91 + 1;
        C2491fm a85 = C4105zY.m41624a(ScriptableMission.class, "ab07a0f9460d5f3ff513cddfbc59b7b7", i92);
        dFM = a85;
        fmVarArr[i92] = a85;
        int i93 = i92 + 1;
        C2491fm a86 = C4105zY.m41624a(ScriptableMission.class, "920c6a87d5633b28ed23b2078fe49ad6", i93);
        dFN = a86;
        fmVarArr[i93] = a86;
        int i94 = i93 + 1;
        C2491fm a87 = C4105zY.m41624a(ScriptableMission.class, "7d82bab5ea395fe3f3d6c4402c51066b", i94);
        dFO = a87;
        fmVarArr[i94] = a87;
        int i95 = i94 + 1;
        C2491fm a88 = C4105zY.m41624a(ScriptableMission.class, "5d6977b5d7d886f25432e804dffec4e0", i95);
        dFP = a88;
        fmVarArr[i95] = a88;
        int i96 = i95 + 1;
        C2491fm a89 = C4105zY.m41624a(ScriptableMission.class, "5be17df50d49b2811ca06d2fd0651ff4", i96);
        dFQ = a89;
        fmVarArr[i96] = a89;
        int i97 = i96 + 1;
        C2491fm a90 = C4105zY.m41624a(ScriptableMission.class, "afd4f766ceb4c7442107b06bfab5306e", i97);
        dFR = a90;
        fmVarArr[i97] = a90;
        int i98 = i97 + 1;
        C2491fm a91 = C4105zY.m41624a(ScriptableMission.class, "69d9b7d8bc1bd0bdccff65367e8ec51f", i98);
        dFS = a91;
        fmVarArr[i98] = a91;
        int i99 = i98 + 1;
        C2491fm a92 = C4105zY.m41624a(ScriptableMission.class, "1a12a8d5140ceceeac46fea2997ea59c", i99);
        f7545Ll = a92;
        fmVarArr[i99] = a92;
        int i100 = i99 + 1;
        C2491fm a93 = C4105zY.m41624a(ScriptableMission.class, "911e71a8143fced83359a3336a23c64a", i100);
        dFT = a93;
        fmVarArr[i100] = a93;
        int i101 = i100 + 1;
        C2491fm a94 = C4105zY.m41624a(ScriptableMission.class, "6372ccf9f270eb89520eee819ac82f74", i101);
        dFU = a94;
        fmVarArr[i101] = a94;
        int i102 = i101 + 1;
        C2491fm a95 = C4105zY.m41624a(ScriptableMission.class, "5b5ceda71a4c4775d8c31f4059f679d8", i102);
        dFV = a95;
        fmVarArr[i102] = a95;
        int i103 = i102 + 1;
        C2491fm a96 = C4105zY.m41624a(ScriptableMission.class, "0f78895bc0cffacf4f07f764a1415031", i103);
        dFW = a96;
        fmVarArr[i103] = a96;
        int i104 = i103 + 1;
        C2491fm a97 = C4105zY.m41624a(ScriptableMission.class, "5e3cbb61d019ab523fd7eebe5100d516", i104);
        dFX = a97;
        fmVarArr[i104] = a97;
        int i105 = i104 + 1;
        C2491fm a98 = C4105zY.m41624a(ScriptableMission.class, "93e2901d2c8f19c8d228859a56481e84", i105);
        dFY = a98;
        fmVarArr[i105] = a98;
        int i106 = i105 + 1;
        C2491fm a99 = C4105zY.m41624a(ScriptableMission.class, "e74a3b38f84138107511027e114d1814", i106);
        dFZ = a99;
        fmVarArr[i106] = a99;
        int i107 = i106 + 1;
        C2491fm a100 = C4105zY.m41624a(ScriptableMission.class, "8b5b128caae114ef476ebbab5eaf1770", i107);
        dGa = a100;
        fmVarArr[i107] = a100;
        int i108 = i107 + 1;
        C2491fm a101 = C4105zY.m41624a(ScriptableMission.class, "2f3f820d9efb1513d245277b70d87a50", i108);
        dGb = a101;
        fmVarArr[i108] = a101;
        int i109 = i108 + 1;
        C2491fm a102 = C4105zY.m41624a(ScriptableMission.class, "da1e38e9364ff63392718152cc093e5a", i109);
        dGc = a102;
        fmVarArr[i109] = a102;
        int i110 = i109 + 1;
        C2491fm a103 = C4105zY.m41624a(ScriptableMission.class, "0d2127df7543302c4e6a1b83457b17ba", i110);
        dGd = a103;
        fmVarArr[i110] = a103;
        int i111 = i110 + 1;
        C2491fm a104 = C4105zY.m41624a(ScriptableMission.class, "2662ae3abb1074a234ffdbe9c217d161", i111);
        dGe = a104;
        fmVarArr[i111] = a104;
        int i112 = i111 + 1;
        C2491fm a105 = C4105zY.m41624a(ScriptableMission.class, "36ba3b6330234a0d18bc65c43a720839", i112);
        dGf = a105;
        fmVarArr[i112] = a105;
        int i113 = i112 + 1;
        C2491fm a106 = C4105zY.m41624a(ScriptableMission.class, "5fc90f36857825ae62e666b0ede3fabc", i113);
        dGg = a106;
        fmVarArr[i113] = a106;
        int i114 = i113 + 1;
        C2491fm a107 = C4105zY.m41624a(ScriptableMission.class, "4f959eda27dbf7ca8f9e58c56fc08a97", i114);
        dGh = a107;
        fmVarArr[i114] = a107;
        int i115 = i114 + 1;
        C2491fm a108 = C4105zY.m41624a(ScriptableMission.class, "de7d37b39e2d8b36b2921f0dcc06a5e7", i115);
        dGi = a108;
        fmVarArr[i115] = a108;
        int i116 = i115 + 1;
        C2491fm a109 = C4105zY.m41624a(ScriptableMission.class, "21d3d95e02743d87c67efb2810dbb11a", i116);
        coD = a109;
        fmVarArr[i116] = a109;
        int i117 = i116 + 1;
        C2491fm a110 = C4105zY.m41624a(ScriptableMission.class, "5e0df387e6c8239eeef0e31ee868d9ad", i117);
        dGj = a110;
        fmVarArr[i117] = a110;
        int i118 = i117 + 1;
        C2491fm a111 = C4105zY.m41624a(ScriptableMission.class, "0e4c340c310b8924ad501e64aa588463", i118);
        dGk = a111;
        fmVarArr[i118] = a111;
        int i119 = i118 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Mission._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScriptableMission.class, C6695arr.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m31661H(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dEF, wo);
    }

    /* renamed from: I */
    private void m31663I(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dEJ, wo);
    }

    /* renamed from: J */
    private String m31665J(Character acx) {
        switch (bFf().mo6893i(dFc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dFc, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, dFc, new Object[]{acx}));
                break;
        }
        return m31662I(acx);
    }

    /* renamed from: J */
    private void m31666J(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dEK, wo);
    }

    @C0064Am(aul = "87618186f7b200733de89f311eb1ccf4", aum = 0)
    @C5566aOg
    /* renamed from: RJ */
    private void m31667RJ() {
        throw new aWi(new aCE(this, aLT, new Object[0]));
    }

    @C0064Am(aul = "ce49c951a0077c5ae6a833bc4daecb21", aum = 0)
    @C5566aOg
    /* renamed from: RL */
    private void m31668RL() {
        throw new aWi(new aCE(this, aLU, new Object[0]));
    }

    @C0064Am(aul = "09ac25f9268e1efc56ff40774c2eb5c8", aum = 0)
    @C5566aOg
    /* renamed from: RN */
    private void m31669RN() {
        throw new aWi(new aCE(this, aLX, new Object[0]));
    }

    @C0064Am(aul = "bba7812ae906083717305bd525cc223d", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31677a(LootType ahc, ItemType jCVar, int i) {
        throw new aWi(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
    }

    @C0064Am(aul = "4d77ddce2260c0250ff190246ebecf6b", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31678a(MissionTrigger aus) {
        throw new aWi(new aCE(this, byO, new Object[]{aus}));
    }

    @C0064Am(aul = "e9344d1376df80a346b404bdb92ea494", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31681a(Item auq, ItemLocation aag, ItemLocation aag2) {
        throw new aWi(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
    }

    @C0064Am(aul = "5fc90f36857825ae62e666b0ede3fabc", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31684a(MissionObjective sGVar) {
        throw new aWi(new aCE(this, dGg, new Object[]{sGVar}));
    }

    @C0064Am(aul = "5b5b17bab3906383e09dc8ac8f0bac73", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31685a(String str, Station bf) {
        throw new aWi(new aCE(this, byB, new Object[]{str, bf}));
    }

    @C0064Am(aul = "ea32a58c8f0ef45db444c7c37f643951", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31687a(String str, Character acx) {
        throw new aWi(new aCE(this, byC, new Object[]{str, acx}));
    }

    @C0064Am(aul = "51b007ced9616d76aa75350e55186a3d", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31689a(String str, Ship fAVar) {
        throw new aWi(new aCE(this, byF, new Object[]{str, fAVar}));
    }

    @C0064Am(aul = "eb4057beb52b376c994edf4f91a18672", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m31690a(String str, Ship fAVar, Ship fAVar2) {
        throw new aWi(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
    }

    @C0064Am(aul = "0e4c340c310b8924ad501e64aa588463", aum = 0)
    @C5566aOg
    /* renamed from: aD */
    private void m31698aD(Actor cr) {
        throw new aWi(new aCE(this, dGk, new Object[]{cr}));
    }

    @C5566aOg
    /* renamed from: aE */
    private void m31699aE(Actor cr) {
        switch (bFf().mo6893i(dGk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGk, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGk, new Object[]{cr}));
                break;
        }
        m31698aD(cr);
    }

    /* access modifiers changed from: private */
    public org.mozilla1.javascript.Context aRv() {
        switch (bFf().mo6893i(dES)) {
            case 0:
                return null;
            case 2:
                return (org.mozilla1.javascript.Context) bFf().mo5606d(new aCE(this, dES, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dES, new Object[0]));
                break;
        }
        return bjv();
    }

    @C0064Am(aul = "860354822547163b2894d31b52a0490a", aum = 0)
    @C5566aOg
    /* renamed from: aV */
    private void m31700aV(String str) {
        throw new aWi(new aCE(this, aLR, new Object[]{str}));
    }

    /* renamed from: ad */
    private void m31701ad(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnp, raVar);
    }

    @C0064Am(aul = "2adf6032cf79fed9c3767596378e8dd7", aum = 0)
    @C5566aOg
    private void akJ() {
        throw new aWi(new aCE(this, byP, new Object[0]));
    }

    @C0064Am(aul = "e0c96bc566f4fe327819cec9f1c34b97", aum = 0)
    @C5566aOg
    private void akL() {
        throw new aWi(new aCE(this, byS, new Object[0]));
    }

    private C3438ra azc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnp);
    }

    /* renamed from: b */
    private org.mozilla1.javascript.NativeObject m31702b(Map<String, Object> map, org.mozilla1.javascript.Scriptable avf) {
        switch (bFf().mo6893i(dEY)) {
            case 0:
                return null;
            case 2:
                return (org.mozilla1.javascript.NativeObject) bFf().mo5606d(new aCE(this, dEY, new Object[]{map, avf}));
            case 3:
                bFf().mo5606d(new aCE(this, dEY, new Object[]{map, avf}));
                break;
        }
        return m31671a(map, avf);
    }

    /* renamed from: b */
    private org.mozilla1.javascript.NativeArray m31703b(Object[] objArr, org.mozilla1.javascript.Scriptable avf) {
        switch (bFf().mo6893i(dEW)) {
            case 0:
                return null;
            case 2:
                return (org.mozilla1.javascript.NativeArray) bFf().mo5606d(new aCE(this, dEW, new Object[]{objArr, avf}));
            case 3:
                bFf().mo5606d(new aCE(this, dEW, new Object[]{objArr, avf}));
                break;
        }
        return m31672a(objArr, avf);
    }

    /* renamed from: b */
    private Asset m31705b(AssetGroup ne, String str) {
        switch (bFf().mo6893i(dFm)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dFm, new Object[]{ne, str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFm, new Object[]{ne, str}));
                break;
        }
        return m31673a(ne, str);
    }

    /* renamed from: b */
    private Asset m31706b(String str, AssetGroup ne) {
        switch (bFf().mo6893i(dFG)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dFG, new Object[]{str, ne}));
            case 3:
                bFf().mo5606d(new aCE(this, dFG, new Object[]{str, ne}));
                break;
        }
        return m31674a(str, ne);
    }

    /* renamed from: b */
    private Map<String, Object> m31707b(org.mozilla1.javascript.NativeObject pd, org.mozilla1.javascript.Scriptable avf) {
        switch (bFf().mo6893i(dEX)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, dEX, new Object[]{pd, avf}));
            case 3:
                bFf().mo5606d(new aCE(this, dEX, new Object[]{pd, avf}));
                break;
        }
        return m31675a(pd, avf);
    }

    @C0064Am(aul = "60720039f081cfe38552726011cb03d3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m31708b(Loot ael) {
        throw new aWi(new aCE(this, byD, new Object[]{ael}));
    }

    @C5566aOg
    /* renamed from: b */
    private void m31709b(MissionObjective sGVar) {
        switch (bFf().mo6893i(dGg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGg, new Object[]{sGVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGg, new Object[]{sGVar}));
                break;
        }
        m31684a(sGVar);
    }

    /* renamed from: b */
    private Object[] m31711b(org.mozilla1.javascript.NativeArray ayt, org.mozilla1.javascript.Scriptable avf) {
        switch (bFf().mo6893i(dEV)) {
            case 0:
                return null;
            case 2:
                return (Object[]) bFf().mo5606d(new aCE(this, dEV, new Object[]{ayt, avf}));
            case 3:
                bFf().mo5606d(new aCE(this, dEV, new Object[]{ayt, avf}));
                break;
        }
        return m31697a(ayt, avf);
    }

    /* renamed from: bj */
    private void m31712bj(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dEH, raVar);
    }

    private C1556Wo bjo() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dEF);
    }

    private C3438ra bjp() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dEH);
    }

    private C1556Wo bjq() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dEJ);
    }

    private C1556Wo bjr() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dEK);
    }

    private float bjs() {
        return bFf().mo5608dq().mo3211m(dEM);
    }

    @C0064Am(aul = "82591d633f5fe02cda002e472fca2219", aum = 0)
    @C6580apg(cpn = true)
    private void bjt() {
        bFf().mo5600a((Class<? extends C4062yl>) C2043bU.C2044a.class, (C0495Gr) new aCE(this, dEN, new Object[0]));
    }

    private void bjx() {
        switch (bFf().mo6893i(dET)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dET, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dET, new Object[0]));
                break;
        }
        bjw();
    }

    @C0064Am(aul = "4fd23a52d4d8604b2a6822590c34dcc7", aum = 0)
    @C5566aOg
    /* renamed from: bk */
    private boolean m31713bk(String str) {
        throw new aWi(new aCE(this, beF, new Object[]{str}));
    }

    @C0064Am(aul = "52e75c7b2a70c3de88004fcf9a5b20ae", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m31716c(Item auq, ItemLocation aag) {
        throw new aWi(new aCE(this, aLW, new Object[]{auq, aag}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "5e0df387e6c8239eeef0e31ee868d9ad", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: c */
    private void m31717c(Ship fAVar, Actor cr) {
        throw new aWi(new aCE(this, dGj, new Object[]{fAVar, cr}));
    }

    @C0064Am(aul = "a781885204799ebda131eb5872ab612e", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m31720c(String str, Ship fAVar) {
        throw new aWi(new aCE(this, byG, new Object[]{str, fAVar}));
    }

    @C0064Am(aul = "8e0feb5a23cf9b514bd01a6492648f2d", aum = 0)
    @C5566aOg
    /* renamed from: cq */
    private void m31721cq(String str) {
        throw new aWi(new aCE(this, byQ, new Object[]{str}));
    }

    /* renamed from: d */
    private void m31723d(org.mozilla1.javascript.NativeObject pd, org.mozilla1.javascript.Scriptable avf) {
        switch (bFf().mo6893i(dFa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFa, new Object[]{pd, avf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFa, new Object[]{pd, avf}));
                break;
        }
        m31715c(pd, avf);
    }

    /* access modifiers changed from: private */
    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: d */
    public void m31724d(Ship fAVar, Actor cr) {
        switch (bFf().mo6893i(dGj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGj, new Object[]{fAVar, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGj, new Object[]{fAVar, cr}));
                break;
        }
        m31717c(fAVar, cr);
    }

    @C0064Am(aul = "7db4243e5c1da5e2ad25d368d5d06fca", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m31725d(Node rPVar) {
        throw new aWi(new aCE(this, byL, new Object[]{rPVar}));
    }

    /* renamed from: dN */
    private MissionObjective m31727dN(String str) {
        switch (bFf().mo6893i(cnK)) {
            case 0:
                return null;
            case 2:
                return (MissionObjective) bFf().mo5606d(new aCE(this, cnK, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, cnK, new Object[]{str}));
                break;
        }
        return m31726dM(str);
    }

    /* renamed from: e */
    private org.mozilla1.javascript.NativeObject m31728e(org.mozilla1.javascript.Scriptable avf) {
        switch (bFf().mo6893i(dEZ)) {
            case 0:
                return null;
            case 2:
                return (org.mozilla1.javascript.NativeObject) bFf().mo5606d(new aCE(this, dEZ, new Object[]{avf}));
            case 3:
                bFf().mo5606d(new aCE(this, dEZ, new Object[]{avf}));
                break;
        }
        return m31722d(avf);
    }

    /* renamed from: f */
    private List<NPC> m31735f(Set<String> set) {
        switch (bFf().mo6893i(dFV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dFV, new Object[]{set}));
            case 3:
                bFf().mo5606d(new aCE(this, dFV, new Object[]{set}));
                break;
        }
        return m31730e(set);
    }

    /* renamed from: fb */
    private Actor m31750fb(String str) {
        switch (bFf().mo6893i(dFe)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, dFe, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFe, new Object[]{str}));
                break;
        }
        return m31749fa(str);
    }

    /* renamed from: fh */
    private ItemType m31755fh(String str) {
        switch (bFf().mo6893i(dFl)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, dFl, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFl, new Object[]{str}));
                break;
        }
        return m31754fg(str);
    }

    /* renamed from: fj */
    private Asset m31757fj(String str) {
        switch (bFf().mo6893i(dFn)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dFn, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFn, new Object[]{str}));
                break;
        }
        return m31756fi(str);
    }

    /* renamed from: fl */
    private NPCType m31759fl(String str) {
        switch (bFf().mo6893i(dFq)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, dFq, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFq, new Object[]{str}));
                break;
        }
        return m31758fk(str);
    }

    /* renamed from: fn */
    private Node m31761fn(String str) {
        switch (bFf().mo6893i(dFt)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, dFt, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFt, new Object[]{str}));
                break;
        }
        return m31760fm(str);
    }

    /* renamed from: fp */
    private I18NString m31763fp(String str) {
        switch (bFf().mo6893i(dFv)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, dFv, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFv, new Object[]{str}));
                break;
        }
        return m31762fo(str);
    }

    /* renamed from: fr */
    private ScriptableMissionTemplateObjective m31765fr(String str) {
        switch (bFf().mo6893i(dFw)) {
            case 0:
                return null;
            case 2:
                return (ScriptableMissionTemplateObjective) bFf().mo5606d(new aCE(this, dFw, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFw, new Object[]{str}));
                break;
        }
        return m31764fq(str);
    }

    @C0064Am(aul = "44e4b54a451a3e84cda225fe2aaa3a96", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m31770g(Ship fAVar) {
        throw new aWi(new aCE(this, aLS, new Object[]{fAVar}));
    }

    /* renamed from: gv */
    private void m31772gv(float f) {
        bFf().mo5608dq().mo3150a(dEM, f);
    }

    @C0064Am(aul = "217875629c6929652f836610791c3ce2", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m31774i(Ship fAVar) {
        throw new aWi(new aCE(this, aMe, new Object[]{fAVar}));
    }

    /* renamed from: j */
    private void m31775j(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C2043bU.C2044a) it.next()).mo17299c(this);
        }
    }

    @C0064Am(aul = "b3a28ab9c47817433fe4e0c57fc9d807", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m31776k(Gate fFVar) {
        throw new aWi(new aCE(this, byM, new Object[]{fFVar}));
    }

    @C0064Am(aul = "6993b06a12998cd5874b02d8942dc120", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m31779n(Ship fAVar) {
        throw new aWi(new aCE(this, beH, new Object[]{fAVar}));
    }

    @C0064Am(aul = "f37cc57c17dcd3aaccadb6d86ed0d512", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m31781p(Station bf) {
        throw new aWi(new aCE(this, byJ, new Object[]{bf}));
    }

    @C0064Am(aul = "c9d760b24c4bd6608fe323c8a4cbde66", aum = 0)
    @C5566aOg
    /* renamed from: r */
    private void m31784r(Station bf) {
        throw new aWi(new aCE(this, byK, new Object[]{bf}));
    }

    @C0064Am(aul = "178034c38da8c21300f43d4697a01a62", aum = 0)
    @C5566aOg
    /* renamed from: s */
    private void m31786s(String str, String str2) {
        throw new aWi(new aCE(this, byH, new Object[]{str, str2}));
    }

    /* renamed from: s */
    private void m31787s(List<NPC> list) {
        switch (bFf().mo6893i(dFW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFW, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFW, new Object[]{list}));
                break;
        }
        m31785r(list);
    }

    @C0064Am(aul = "c9ec6a76b7ddd44c40f7b54ff88c9975", aum = 0)
    @C5566aOg
    /* renamed from: t */
    private void m31788t(Station bf) {
        throw new aWi(new aCE(this, byN, new Object[]{bf}));
    }

    @C0064Am(aul = "ddc6157838d940dac25c38db67ac93d2", aum = 0)
    @C5566aOg
    /* renamed from: w */
    private void m31789w(Ship fAVar) {
        throw new aWi(new aCE(this, byI, new Object[]{fAVar}));
    }

    @C6143ahL
    /* renamed from: H */
    public int mo18888H(String str, String str2) {
        switch (bFf().mo6893i(dFE)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dFE, new Object[]{str, str2}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFE, new Object[]{str, str2}));
                break;
        }
        return m31660G(str, str2);
    }

    @C6143ahL
    /* renamed from: J */
    public void mo18889J(String str, String str2) {
        switch (bFf().mo6893i(dFX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFX, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFX, new Object[]{str, str2}));
                break;
        }
        m31664I(str, str2);
    }

    @C5566aOg
    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m31667RJ();
    }

    @C5566aOg
    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m31668RL();
    }

    @C5566aOg
    /* renamed from: RO */
    public void mo67RO() {
        switch (bFf().mo6893i(aLX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                break;
        }
        m31669RN();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6695arr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Mission._m_methodCount) {
            case 0:
                bjt();
                return null;
            case 1:
                m31731eS((String) args[0]);
                return null;
            case 2:
                m31732eU((String) args[0]);
                return null;
            case 3:
                m31695a((String) args[0], (Throwable) args[1]);
                return null;
            case 4:
                m31733eW((String) args[0]);
                return null;
            case 5:
                return bjv();
            case 6:
                bjw();
                return null;
            case 7:
                return azj();
            case 8:
                return m31714c((String) args[0], (Object[]) args[1]);
            case 9:
                return m31697a((org.mozilla1.javascript.NativeArray) args[0], (org.mozilla1.javascript.Scriptable) args[1]);
            case 10:
                return m31672a((Object[]) args[0], (org.mozilla1.javascript.Scriptable) args[1]);
            case 11:
                return m31675a((org.mozilla1.javascript.NativeObject) args[0], (org.mozilla1.javascript.Scriptable) args[1]);
            case 12:
                return m31671a((Map<String, Object>) (Map) args[0], (org.mozilla1.javascript.Scriptable) args[1]);
            case 13:
                return m31722d((org.mozilla1.javascript.Scriptable) args[0]);
            case 14:
                m31715c((org.mozilla1.javascript.NativeObject) args[0], (org.mozilla1.javascript.Scriptable) args[1]);
                return null;
            case 15:
                return m31729e((String) args[0], (Object[]) args[1]);
            case 16:
                m31668RL();
                return null;
            case 17:
                m31667RJ();
                return null;
            case 18:
                akL();
                return null;
            case 19:
                m31789w((Ship) args[0]);
                return null;
            case 20:
                m31676a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            case 21:
                m31690a((String) args[0], (Ship) args[1], (Ship) args[2]);
                return null;
            case 22:
                m31781p((Station) args[0]);
                return null;
            case 23:
                m31784r((Station) args[0]);
                return null;
            case 24:
                m31685a((String) args[0], (Station) args[1]);
                return null;
            case 25:
                return new Boolean(arx());
            case 26:
                m31776k((Gate) args[0]);
                return null;
            case 27:
                m31779n((Ship) args[0]);
                return null;
            case 28:
                m31788t((Station) args[0]);
                return null;
            case 29:
                m31678a((MissionTrigger) args[0]);
                return null;
            case 30:
                akJ();
                return null;
            case 31:
                m31725d((Node) args[0]);
                return null;
            case 32:
                m31677a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 33:
                m31708b((Loot) args[0]);
                return null;
            case 34:
                m31680a((Character) args[0], (Loot) args[1]);
                return null;
            case 35:
                return m31662I((Character) args[0]);
            case 36:
                m31687a((String) args[0], (Character) args[1]);
                return null;
            case 37:
                m31721cq((String) args[0]);
                return null;
            case 38:
                m31774i((Ship) args[0]);
                return null;
            case 39:
                m31689a((String) args[0], (Ship) args[1]);
                return null;
            case 40:
                m31770g((Ship) args[0]);
                return null;
            case 41:
                m31720c((String) args[0], (Ship) args[1]);
                return null;
            case 42:
                m31700aV((String) args[0]);
                return null;
            case 43:
                return new Boolean(m31713bk((String) args[0]));
            case 44:
                m31786s((String) args[0], (String) args[1]);
                return null;
            case 45:
                m31681a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 46:
                m31716c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 47:
                m31669RN();
                return null;
            case 48:
                m31734eY((String) args[0]);
                return null;
            case 49:
                return m31749fa((String) args[0]);
            case 50:
                m31751fc((String) args[0]);
                return null;
            case 51:
                m31783p((String) args[0], args[1]);
                return null;
            case 52:
                return m31752fe((String) args[0]);
            case 53:
                m31659F((String) args[0], (String) args[1]);
                return null;
            case 54:
                return m31753ff((String) args[0]);
            case 55:
                return bjy();
            case 56:
                return m31754fg((String) args[0]);
            case 57:
                return m31673a((AssetGroup) args[0], (String) args[1]);
            case 58:
                return m31756fi((String) args[0]);
            case 59:
                return new Integer(m31778l((String) args[0], ((Integer) args[1]).intValue()));
            case 60:
                return new Boolean(m31780n((String) args[0], ((Integer) args[1]).intValue()));
            case 61:
                return m31758fk((String) args[0]);
            case 62:
                m31686a((String) args[0], (org.mozilla1.javascript.NativeObject) args[1]);
                return null;
            case 63:
                return m31696a(((Integer) args[0]).intValue(), (String) args[1], (String) args[2], ((Boolean) args[3]).booleanValue());
            case 64:
                return m31760fm((String) args[0]);
            case 65:
                m31691a((String) args[0], (String) args[1], ((Double) args[2]).doubleValue(), ((Double) args[3]).doubleValue(), ((Double) args[4]).doubleValue(), ((Long) args[5]).longValue());
                return null;
            case 66:
                return m31762fo((String) args[0]);
            case 67:
                return m31764fq((String) args[0]);
            case 68:
                return m31726dM((String) args[0]);
            case 69:
                m31766fs((String) args[0]);
                return null;
            case 70:
                m31694a((String) args[0], (String) args[1], ((Integer) args[2]).intValue());
                return null;
            case 71:
                m31767fu((String) args[0]);
                return null;
            case 72:
                m31768fw((String) args[0]);
                return null;
            case 73:
                m31769fy((String) args[0]);
                return null;
            case 74:
                m31782p((String) args[0], ((Integer) args[1]).intValue());
                return null;
            case 75:
                return new Integer(m31736fA((String) args[0]));
            case 76:
                return new Integer(m31660G((String) args[0], (String) args[1]));
            case 77:
                m31692a((String) args[0], (String) args[1], ((Float) args[2]).floatValue());
                return null;
            case 78:
                return m31674a((String) args[0], (AssetGroup) args[1]);
            case 79:
                m31693a((String) args[0], (String) args[1], ((Float) args[2]).floatValue(), (String) args[3]);
                return null;
            case 80:
                m31771g((String) args[0], (String) args[1], (String) args[2]);
                return null;
            case 81:
                m31737fC((String) args[0]);
                return null;
            case 82:
                return new Boolean(m31738fE((String) args[0]));
            case 83:
                return new Boolean(m31710b((String) args[0], ((Long) args[1]).longValue()));
            case 84:
                m31739fG((String) args[0]);
                return null;
            case 85:
                return new Boolean(m31740fI((String) args[0]));
            case 86:
                return new Float(m31741fK((String) args[0]));
            case 87:
                return new Float(m31742fM((String) args[0]));
            case 88:
                return new Float(bjA());
            case 89:
                return new Float(bjC());
            case 90:
                m31743fO((String) args[0]);
                return null;
            case 91:
                m31682a((Ship) args[0], (Actor) args[1]);
                return null;
            case 92:
                m31679a((NPC) args[0], (Character) args[1]);
                return null;
            case 93:
                m31688a((String) args[0], (org.mozilla1.javascript.NativeArray) args[1]);
                return null;
            case 94:
                return m31730e((Set<String>) (Set) args[0]);
            case 95:
                m31785r((List<NPC>) (List) args[0]);
                return null;
            case 96:
                m31664I((String) args[0], (String) args[1]);
                return null;
            case 97:
                m31744fQ((String) args[0]);
                return null;
            case 98:
                return new Boolean(m31745fS((String) args[0]));
            case 99:
                m31718c((ItemType) args[0], ((Integer) args[1]).intValue());
                return null;
            case 100:
                m31746fU((String) args[0]);
                return null;
            case 101:
                return bjE();
            case 102:
                m31777k((String) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 103:
                m31747fW((String) args[0]);
                return null;
            case 104:
                return new Boolean(m31748fY((String) args[0]));
            case 105:
                m31684a((MissionObjective) args[0]);
                return null;
            case 106:
                return new Float(bjG());
            case 107:
                m31773gw(((Float) args[0]).floatValue());
                return null;
            case 108:
                m31719c((Runnable) args[0]);
                return null;
            case 109:
                m31717c((Ship) args[0], (Actor) args[1]);
                return null;
            case 110:
                m31698aD((Actor) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - Mission._m_methodCount) {
            case 0:
                m31775j(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    @C5566aOg
    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m31700aV(str);
    }

    @C5566aOg
    public void akK() {
        switch (bFf().mo6893i(byP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                break;
        }
        akJ();
    }

    @C5566aOg
    public void akM() {
        switch (bFf().mo6893i(byS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                break;
        }
        akL();
    }

    public boolean ary() {
        switch (bFf().mo6893i(bYI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bYI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bYI, new Object[0]));
                break;
        }
        return arx();
    }

    public List<MissionObjective> azk() {
        switch (bFf().mo6893i(cnJ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cnJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cnJ, new Object[0]));
                break;
        }
        return azj();
    }

    /* renamed from: b */
    public void mo85b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m31676a(cr, acm);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo86b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m31677a(ahc, jCVar, i);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo87b(MissionTrigger aus) {
        switch (bFf().mo6893i(byO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                break;
        }
        m31678a(aus);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo18891b(NPC auf, Character acx) {
        switch (bFf().mo6893i(dFT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFT, new Object[]{auf, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFT, new Object[]{auf, acx}));
                break;
        }
        m31679a(auf, acx);
    }

    /* renamed from: b */
    public void mo88b(Character acx, Loot ael) {
        switch (bFf().mo6893i(byE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                break;
        }
        m31680a(acx, ael);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo90b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m31681a(auq, aag, aag2);
    }

    /* renamed from: b */
    public void mo7563b(Ship fAVar, Actor cr) {
        switch (bFf().mo6893i(f7545Ll)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7545Ll, new Object[]{fAVar, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7545Ll, new Object[]{fAVar, cr}));
                break;
        }
        m31682a(fAVar, cr);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo91b(String str, Station bf) {
        switch (bFf().mo6893i(byB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                break;
        }
        m31685a(str, bf);
    }

    @C6143ahL
    /* renamed from: b */
    public void mo18892b(String str, org.mozilla1.javascript.NativeObject pd) {
        switch (bFf().mo6893i(dFr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFr, new Object[]{str, pd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFr, new Object[]{str, pd}));
                break;
        }
        m31686a(str, pd);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo93b(String str, Character acx) {
        switch (bFf().mo6893i(byC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                break;
        }
        m31687a(str, acx);
    }

    @C6143ahL
    /* renamed from: b */
    public void mo18893b(String str, org.mozilla1.javascript.NativeArray ayt) {
        switch (bFf().mo6893i(dFU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFU, new Object[]{str, ayt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFU, new Object[]{str, ayt}));
                break;
        }
        m31688a(str, ayt);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo94b(String str, Ship fAVar) {
        switch (bFf().mo6893i(byF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                break;
        }
        m31689a(str, fAVar);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo95b(String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(beG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                break;
        }
        m31690a(str, fAVar, fAVar2);
    }

    @C6143ahL
    /* renamed from: b */
    public void mo18894b(String str, String str2, double d, double d2, double d3, long j) {
        switch (bFf().mo6893i(dFu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFu, new Object[]{str, str2, new Double(d), new Double(d2), new Double(d3), new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFu, new Object[]{str, str2, new Double(d), new Double(d2), new Double(d3), new Long(j)}));
                break;
        }
        m31691a(str, str2, d, d2, d3, j);
    }

    @C6143ahL
    /* renamed from: b */
    public void mo18895b(String str, String str2, float f) {
        switch (bFf().mo6893i(dFF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFF, new Object[]{str, str2, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFF, new Object[]{str, str2, new Float(f)}));
                break;
        }
        m31692a(str, str2, f);
    }

    @C6143ahL
    /* renamed from: b */
    public void mo18896b(String str, String str2, float f, String str3) {
        switch (bFf().mo6893i(dFH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFH, new Object[]{str, str2, new Float(f), str3}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFH, new Object[]{str, str2, new Float(f), str3}));
                break;
        }
        m31693a(str, str2, f, str3);
    }

    @C6143ahL
    /* renamed from: b */
    public void mo18897b(String str, String str2, int i) {
        switch (bFf().mo6893i(dFy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFy, new Object[]{str, str2, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFy, new Object[]{str, str2, new Integer(i)}));
                break;
        }
        m31694a(str, str2, i);
    }

    /* renamed from: b */
    public void mo18898b(String str, Throwable th) {
        switch (bFf().mo6893i(dEQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEQ, new Object[]{str, th}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEQ, new Object[]{str, th}));
                break;
        }
        m31695a(str, th);
    }

    @C6143ahL
    /* renamed from: b */
    public Object[] mo18899b(int i, String str, String str2, boolean z) {
        switch (bFf().mo6893i(dFs)) {
            case 0:
                return null;
            case 2:
                return (Object[]) bFf().mo5606d(new aCE(this, dFs, new Object[]{new Integer(i), str, str2, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, dFs, new Object[]{new Integer(i), str, str2, new Boolean(z)}));
                break;
        }
        return m31696a(i, str, str2, z);
    }

    @C6143ahL
    public float bjB() {
        switch (bFf().mo6893i(dFQ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dFQ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFQ, new Object[0]));
                break;
        }
        return bjA();
    }

    @C6143ahL
    public float bjD() {
        switch (bFf().mo6893i(dFR)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dFR, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFR, new Object[0]));
                break;
        }
        return bjC();
    }

    public List<GatePass> bjF() {
        switch (bFf().mo6893i(dGc)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dGc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGc, new Object[0]));
                break;
        }
        return bjE();
    }

    @C6143ahL
    public float bjH() {
        switch (bFf().mo6893i(dGh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dGh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dGh, new Object[0]));
                break;
        }
        return bjG();
    }

    @C6580apg(cpn = true)
    public void bju() {
        switch (bFf().mo6893i(dEN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEN, new Object[0]));
                break;
        }
        bjt();
    }

    @C6143ahL
    public String bjz() {
        switch (bFf().mo6893i(dFk)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dFk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dFk, new Object[0]));
                break;
        }
        return bjy();
    }

    @C5566aOg
    /* renamed from: bl */
    public boolean mo97bl(String str) {
        switch (bFf().mo6893i(beF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                break;
        }
        return m31713bk(str);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo99c(Loot ael) {
        switch (bFf().mo6893i(byD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                break;
        }
        m31708b(ael);
    }

    @C6143ahL
    /* renamed from: c */
    public boolean mo18905c(String str, long j) {
        switch (bFf().mo6893i(dFL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dFL, new Object[]{str, new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFL, new Object[]{str, new Long(j)}));
                break;
        }
        return m31710b(str, j);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: cr */
    public void mo115cr(String str) {
        switch (bFf().mo6893i(byQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                break;
        }
        m31721cq(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public Object mo18906d(String str, Object... objArr) {
        switch (bFf().mo6893i(dEU)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, dEU, new Object[]{str, objArr}));
            case 3:
                bFf().mo5606d(new aCE(this, dEU, new Object[]{str, objArr}));
                break;
        }
        return m31714c(str, objArr);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo117d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m31716c(auq, aag);
    }

    /* renamed from: d */
    public void mo118d(ItemType jCVar, int i) {
        switch (bFf().mo6893i(dGa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGa, new Object[]{jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGa, new Object[]{jCVar, new Integer(i)}));
                break;
        }
        m31718c(jCVar, i);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: d */
    public void mo18907d(Runnable runnable) {
        switch (bFf().mo6893i(coD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                break;
        }
        m31719c(runnable);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo119d(String str, Ship fAVar) {
        switch (bFf().mo6893i(byG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                break;
        }
        m31720c(str, fAVar);
    }

    @C5566aOg
    /* renamed from: e */
    public void mo121e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m31725d(rPVar);
    }

    /* renamed from: eT */
    public void mo18908eT(String str) {
        switch (bFf().mo6893i(dEO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEO, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEO, new Object[]{str}));
                break;
        }
        m31731eS(str);
    }

    /* renamed from: eV */
    public void mo18909eV(String str) {
        switch (bFf().mo6893i(dEP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEP, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEP, new Object[]{str}));
                break;
        }
        m31732eU(str);
    }

    /* renamed from: eX */
    public void mo18910eX(String str) {
        switch (bFf().mo6893i(dER)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dER, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dER, new Object[]{str}));
                break;
        }
        m31733eW(str);
    }

    @C6143ahL
    /* renamed from: eZ */
    public void mo18911eZ(String str) {
        switch (bFf().mo6893i(dFd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFd, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFd, new Object[]{str}));
                break;
        }
        m31734eY(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public Object mo18912f(String str, Object... objArr) {
        switch (bFf().mo6893i(dFb)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, dFb, new Object[]{str, objArr}));
            case 3:
                bFf().mo5606d(new aCE(this, dFb, new Object[]{str, objArr}));
                break;
        }
        return m31729e(str, objArr);
    }

    @C6143ahL
    /* renamed from: fB */
    public int mo18913fB(String str) {
        switch (bFf().mo6893i(dFD)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dFD, new Object[]{str}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFD, new Object[]{str}));
                break;
        }
        return m31736fA(str);
    }

    @C6143ahL
    /* renamed from: fD */
    public void mo18914fD(String str) {
        switch (bFf().mo6893i(dFJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFJ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFJ, new Object[]{str}));
                break;
        }
        m31737fC(str);
    }

    @C6143ahL
    /* renamed from: fF */
    public boolean mo18915fF(String str) {
        switch (bFf().mo6893i(dFK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dFK, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFK, new Object[]{str}));
                break;
        }
        return m31738fE(str);
    }

    @C6143ahL
    /* renamed from: fH */
    public void mo18916fH(String str) {
        switch (bFf().mo6893i(dFM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFM, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFM, new Object[]{str}));
                break;
        }
        m31739fG(str);
    }

    @C6143ahL
    /* renamed from: fJ */
    public boolean mo18917fJ(String str) {
        switch (bFf().mo6893i(dFN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dFN, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFN, new Object[]{str}));
                break;
        }
        return m31740fI(str);
    }

    @C6143ahL
    /* renamed from: fL */
    public float mo18918fL(String str) {
        switch (bFf().mo6893i(dFO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dFO, new Object[]{str}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFO, new Object[]{str}));
                break;
        }
        return m31741fK(str);
    }

    @C6143ahL
    /* renamed from: fN */
    public float mo18919fN(String str) {
        switch (bFf().mo6893i(dFP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dFP, new Object[]{str}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFP, new Object[]{str}));
                break;
        }
        return m31742fM(str);
    }

    @C6143ahL
    /* renamed from: fP */
    public void mo18920fP(String str) {
        switch (bFf().mo6893i(dFS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFS, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFS, new Object[]{str}));
                break;
        }
        m31743fO(str);
    }

    @C6143ahL
    /* renamed from: fR */
    public void mo18921fR(String str) {
        switch (bFf().mo6893i(dFY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFY, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFY, new Object[]{str}));
                break;
        }
        m31744fQ(str);
    }

    /* renamed from: fT */
    public boolean mo123fT(String str) {
        switch (bFf().mo6893i(dFZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dFZ, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFZ, new Object[]{str}));
                break;
        }
        return m31745fS(str);
    }

    @C6143ahL
    /* renamed from: fV */
    public void mo18922fV(String str) {
        switch (bFf().mo6893i(dGb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGb, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGb, new Object[]{str}));
                break;
        }
        m31746fU(str);
    }

    @C6143ahL
    /* renamed from: fX */
    public void mo18923fX(String str) {
        switch (bFf().mo6893i(dGe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGe, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGe, new Object[]{str}));
                break;
        }
        m31747fW(str);
    }

    @C6143ahL
    /* renamed from: fZ */
    public boolean mo18924fZ(String str) {
        switch (bFf().mo6893i(dGf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dGf, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dGf, new Object[]{str}));
                break;
        }
        return m31748fY(str);
    }

    @C6143ahL
    /* renamed from: fd */
    public void mo18925fd(String str) {
        switch (bFf().mo6893i(dFf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFf, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFf, new Object[]{str}));
                break;
        }
        m31751fc(str);
    }

    @C6143ahL
    /* renamed from: ft */
    public void mo18926ft(String str) {
        switch (bFf().mo6893i(dFx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFx, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFx, new Object[]{str}));
                break;
        }
        m31766fs(str);
    }

    @C6143ahL
    /* renamed from: fv */
    public void mo18927fv(String str) {
        switch (bFf().mo6893i(dFz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFz, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFz, new Object[]{str}));
                break;
        }
        m31767fu(str);
    }

    @C6143ahL
    /* renamed from: fx */
    public void mo18928fx(String str) {
        switch (bFf().mo6893i(dFA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFA, new Object[]{str}));
                break;
        }
        m31768fw(str);
    }

    @C6143ahL
    /* renamed from: fz */
    public void mo18929fz(String str) {
        switch (bFf().mo6893i(dFB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFB, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFB, new Object[]{str}));
                break;
        }
        m31769fy(str);
    }

    @C6143ahL
    public String getParameter(String str) {
        switch (bFf().mo6893i(dFj)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dFj, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFj, new Object[]{str}));
                break;
        }
        return m31753ff(str);
    }

    public Object getValue(String str) {
        switch (bFf().mo6893i(dFh)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, dFh, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFh, new Object[]{str}));
                break;
        }
        return m31752fe(str);
    }

    @C6143ahL
    /* renamed from: gx */
    public void mo18932gx(float f) {
        switch (bFf().mo6893i(dGi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGi, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGi, new Object[]{new Float(f)}));
                break;
        }
        m31773gw(f);
    }

    @C5566aOg
    /* renamed from: h */
    public void mo125h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m31770g(fAVar);
    }

    @C6143ahL
    /* renamed from: h */
    public void mo18933h(String str, String str2, String str3) {
        switch (bFf().mo6893i(dFI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFI, new Object[]{str, str2, str3}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFI, new Object[]{str, str2, str3}));
                break;
        }
        m31771g(str, str2, str3);
    }

    @C5566aOg
    /* renamed from: j */
    public void mo130j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m31774i(fAVar);
    }

    @C5566aOg
    /* renamed from: l */
    public void mo131l(Gate fFVar) {
        switch (bFf().mo6893i(byM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                break;
        }
        m31776k(fFVar);
    }

    @C6143ahL
    /* renamed from: l */
    public void mo18934l(String str, boolean z) {
        switch (bFf().mo6893i(dGd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGd, new Object[]{str, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGd, new Object[]{str, new Boolean(z)}));
                break;
        }
        m31777k(str, z);
    }

    @C6143ahL
    /* renamed from: m */
    public int mo18935m(String str, int i) {
        switch (bFf().mo6893i(dFo)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dFo, new Object[]{str, new Integer(i)}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFo, new Object[]{str, new Integer(i)}));
                break;
        }
        return m31778l(str, i);
    }

    @C5566aOg
    /* renamed from: o */
    public void mo132o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m31779n(fAVar);
    }

    @C6143ahL
    /* renamed from: o */
    public boolean mo18936o(String str, int i) {
        switch (bFf().mo6893i(dFp)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dFp, new Object[]{str, new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dFp, new Object[]{str, new Integer(i)}));
                break;
        }
        return m31780n(str, i);
    }

    @C5566aOg
    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m31781p(bf);
    }

    @C6143ahL
    /* renamed from: q */
    public void mo18937q(String str, int i) {
        switch (bFf().mo6893i(dFC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFC, new Object[]{str, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFC, new Object[]{str, new Integer(i)}));
                break;
        }
        m31782p(str, i);
    }

    @C5566aOg
    /* renamed from: s */
    public void mo134s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m31784r(bf);
    }

    @C6143ahL
    public void setParameter(String str, String str2) {
        switch (bFf().mo6893i(dFi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFi, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFi, new Object[]{str, str2}));
                break;
        }
        m31659F(str, str2);
    }

    public void setValue(String str, Object obj) {
        switch (bFf().mo6893i(dFg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFg, new Object[]{str, obj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFg, new Object[]{str, obj}));
                break;
        }
        m31783p(str, obj);
    }

    @C5566aOg
    /* renamed from: t */
    public void mo135t(String str, String str2) {
        switch (bFf().mo6893i(byH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                break;
        }
        m31786s(str, str2);
    }

    @C5566aOg
    /* renamed from: u */
    public void mo136u(Station bf) {
        switch (bFf().mo6893i(byN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                break;
        }
        m31788t(bf);
    }

    @C5566aOg
    /* renamed from: x */
    public void mo138x(Ship fAVar) {
        switch (bFf().mo6893i(byI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                break;
        }
        m31789w(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "6fd7ce974fef7beb3d0fa9f46d4da41f", aum = 0)
    /* renamed from: eS */
    private void m31731eS(String str) {
        mo6317hy("Mission '" + ((ScriptableMissionTemplate) cdp()).getHandle() + "' " + str);
    }

    @C0064Am(aul = "ca714447c341dc28d1544c8a81613a45", aum = 0)
    /* renamed from: eU */
    private void m31732eU(String str) {
        mo8358lY("Mission '" + ((ScriptableMissionTemplate) cdp()).getHandle() + "' " + str);
    }

    @C0064Am(aul = "0574970a327181cf11d9451d5a599fe9", aum = 0)
    /* renamed from: a */
    private void m31695a(String str, Throwable th) {
        mo8354g("Mission '" + ((ScriptableMissionTemplate) cdp()).getHandle() + "' " + str, th);
    }

    @C0064Am(aul = "a633fd5903ee18619df8745629eadba4", aum = 0)
    /* renamed from: eW */
    private void m31733eW(String str) {
        mo8359ma("Mission '" + ((ScriptableMissionTemplate) cdp()).getHandle() + "' " + str);
    }

    @C0064Am(aul = "536d975a91521121207014d618146a24", aum = 0)
    private org.mozilla1.javascript.Context bjv() {
        org.mozilla1.javascript.Context bkP = org.mozilla1.javascript.Context.call();
        bkP.setOptimizationLevel(0);
        return bkP;
    }

    @C0064Am(aul = "fe3641a228bde0da2baaaa9d470044a5", aum = 0)
    private void bjw() {
        if (this.f7546TR == null) {
            try {
                this.daB = new C2516b();
                org.mozilla1.javascript.Context lhVar = this.daB.get();
                this.f7546TR = lhVar.initStandardObjects();
                this.f7546TR.put("mission", this.f7546TR, (Object) this);
                this.f7546TR.put("game/script", this.f7546TR, (Object) ((ScriptableMissionTemplate) cdp()).cAS());
                lhVar.evaluateReader(this.f7546TR, (Reader) new InputStreamReader(ClassLoader.getSystemResourceAsStream("taikodom/game/script/mission/scripting/ScriptableMissionInclude.js")), "<MissionScript_" + ((ScriptableMissionTemplate) cdp()).getHandle() + ">", 1, (Object) null);
            } catch (Exception e) {
                org.mozilla1.javascript.Context.exit();
                e.printStackTrace();
            }
        }
    }

    @C0064Am(aul = "b07e743db060154c610cbda51eb336c2", aum = 0)
    private List<MissionObjective> azj() {
        return azc();
    }

    @C0064Am(aul = "7c78c56912faba26ff28d46e04756d36", aum = 0)
    /* renamed from: c */
    private Object m31714c(String str, Object[] objArr) {
        Object obj = this.f7546TR.get(str, this.f7546TR);
        if (obj != org.mozilla1.javascript.Scriptable.NOT_FOUND) {
            try {
                return ((org.mozilla1.javascript.Function) obj).call(this.daB.get(), this.f7546TR, this.f7546TR, objArr);
            } catch (Exception e) {
                mo18898b("Error calling function '" + str + "' for mission '" + ((ScriptableMissionTemplate) cdp()).getHandle() + "'", (Throwable) e);
            }
        }
        return null;
    }

    @C0064Am(aul = "e09d4e446ee7821c065fbe704e4cfbd8", aum = 0)
    /* renamed from: a */
    private Object[] m31697a(org.mozilla1.javascript.NativeArray ayt, org.mozilla1.javascript.Scriptable avf) {
        long length = ayt.getLength();
        Object[] objArr = new Object[((int) length)];
        for (int i = 0; ((long) i) < length; i++) {
            objArr[i] = ayt.get(i, avf);
        }
        return objArr;
    }

    @C0064Am(aul = "3b0a1b52a9d58bca9835d4a1e332dc10", aum = 0)
    /* renamed from: a */
    private org.mozilla1.javascript.NativeArray m31672a(Object[] objArr, org.mozilla1.javascript.Scriptable avf) {
        return new org.mozilla1.javascript.NativeArray(objArr);
    }

    @C0064Am(aul = "5b88fc3a2a169978b8c36ac6137b1953", aum = 0)
    /* renamed from: a */
    private Map<String, Object> m31675a(org.mozilla1.javascript.NativeObject pd, org.mozilla1.javascript.Scriptable avf) {
        HashMap hashMap = new HashMap();
        for (Object obj : pd.getAllIds()) {
            Object obj2 = pd.get(obj.toString(), avf);
            if (obj2 instanceof org.mozilla1.javascript.NativeArray) {
                obj2 = m31711b((org.mozilla1.javascript.NativeArray) obj2, avf);
            } else if (obj2 instanceof org.mozilla1.javascript.NativeObject) {
                obj2 = m31707b((org.mozilla1.javascript.NativeObject) obj2, avf);
            }
            hashMap.put(obj.toString(), obj2);
        }
        return hashMap;
    }

    @C0064Am(aul = "a8a99cbc3ea55d9e9b4ff77871358b21", aum = 0)
    /* renamed from: a */
    private org.mozilla1.javascript.NativeObject m31671a(Map<String, Object> map, org.mozilla1.javascript.Scriptable avf) {
        if (map == null) {
            return null;
        }
        org.mozilla1.javascript.NativeObject pd = new org.mozilla1.javascript.NativeObject();
        for (Map.Entry next : map.entrySet()) {
            Object value = next.getValue();
            if (value instanceof Map) {
                value = m31702b((Map<String, Object>) (Map) value, avf);
            } else if (value.getClass().isArray()) {
                value = m31703b((Object[]) value, avf);
            }
            pd.put((String) next.getKey(), pd, value);
        }
        return pd;
    }

    @C0064Am(aul = "0ffe02d41a6cbedacbba8f60979ae67c", aum = 0)
    /* renamed from: d */
    private org.mozilla1.javascript.NativeObject m31722d(org.mozilla1.javascript.Scriptable avf) {
        return m31702b((Map<String, Object>) (Map) getValue(C0891Mw.DATA), avf);
    }

    @C0064Am(aul = "0c698580472f8aeb4b039b8242f4951c", aum = 0)
    /* renamed from: c */
    private void m31715c(org.mozilla1.javascript.NativeObject pd, org.mozilla1.javascript.Scriptable avf) {
        setValue(C0891Mw.DATA, m31707b(pd, avf));
    }

    @C0064Am(aul = "5a32572d70eef184af7bd538ed4fdc0c", aum = 0)
    /* renamed from: e */
    private Object m31729e(String str, Object[] objArr) {
        int i;
        bjx();
        if (this.daB.get() == null) {
            return null;
        }
        org.mozilla1.javascript.NativeObject e = m31728e(this.f7546TR);
        if (e != null) {
            mo18906d("setData", e);
        }
        if (objArr != null) {
            i = objArr.length;
        } else {
            i = 0;
        }
        String str2 = "invokeTrigger" + String.valueOf(i);
        Object[] objArr2 = new Object[(i + 1)];
        objArr2[0] = str;
        for (int i2 = 0; i2 < i; i2++) {
            objArr2[i2 + 1] = objArr[i2];
        }
        Object obj = this.f7546TR.get(str2, this.f7546TR);
        if (obj != org.mozilla1.javascript.Scriptable.NOT_FOUND) {
            try {
                Object call = ((org.mozilla1.javascript.Function) obj).call(this.daB.get(), this.f7546TR, this.f7546TR, objArr2);
                Object d = mo18906d("getData", new Object[0]);
                if (d != null) {
                    m31723d((org.mozilla1.javascript.NativeObject) d, this.f7546TR);
                }
                return call;
            } catch (Exception e2) {
                mo8354g("Error calling trigger '" + str + "' for mission '" + ((ScriptableMissionTemplate) cdp()).getHandle() + "'", (Throwable) e2);
            }
        }
        return null;
    }

    @C0064Am(aul = "371ef7da0be46e256f84098a613f6037", aum = 0)
    /* renamed from: a */
    private void m31676a(Actor cr, C5260aCm acm) {
        String str;
        Iterator it = bjq().entrySet().iterator();
        while (true) {
            if (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                NPC auf = (NPC) entry.getValue();
                if (auf != null && auf.bQx() == cr) {
                    str = (String) entry.getKey();
                    break;
                }
            } else {
                str = null;
                break;
            }
        }
        if (str != null) {
            mo18912f("onPlayerDamaged", str);
        }
    }

    @C0064Am(aul = "52a4bfc1cc25a04a1ff1e635462d8abf", aum = 0)
    private boolean arx() {
        return true;
    }

    @C0064Am(aul = "c769f7347842da22c259c53170382d93", aum = 0)
    /* renamed from: a */
    private void m31680a(Character acx, Loot ael) {
        String J = m31665J(acx);
        if (J != null) {
            mo18912f("onLootSpawned", J);
        }
    }

    @C0064Am(aul = "082aac475d1058d8192bbd66d5129780", aum = 0)
    /* renamed from: I */
    private String m31662I(Character acx) {
        if (!(acx instanceof NPC)) {
            return null;
        }
        String str = null;
        for (Map.Entry entry : bjq().entrySet()) {
            if (acx.equals(entry.getValue())) {
                str = (String) entry.getKey();
            }
        }
        return str;
    }

    @C0064Am(aul = "53328c7a267d7a63dbd07635b2373f2e", aum = 0)
    @C6143ahL
    /* renamed from: eY */
    private void m31734eY(String str) {
        mo8359ma("------> ScriptableMission '" + ((ScriptableMissionTemplate) cdp()).getHandle() + "' js debug: " + str);
    }

    @C0064Am(aul = "62fce9d80aa720457cd68ae39e8db56c", aum = 0)
    /* renamed from: fa */
    private Actor m31749fa(String str) {
        String handle;
        if (str == null) {
            return null;
        }
        for (StellarSystem nodes : ala().aKa()) {
            Iterator<Node> it = nodes.getNodes().iterator();
            while (true) {
                if (it.hasNext()) {
                    Iterator<Actor> it2 = it.next().mo21624Zw().iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            Actor next = it2.next();
                            if (next instanceof Station) {
                                handle = ((Station) next).getHandle();
                            } else if (next instanceof Gate) {
                                handle = ((Gate) next).getHandle();
                            } else if (next instanceof SpaceZone) {
                                handle = ((SpaceZone) next).getHandle();
                            } else if (next instanceof Scenery) {
                                handle = ((Scenery) next).getHandle();
                            } else {
                                handle = next instanceof HazardArea ? ((HazardArea) next).getHandle() : null;
                            }
                            if (str.equals(handle)) {
                                return next;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    @C0064Am(aul = "6f916665ac95053dd3ff831a45dff4cb", aum = 0)
    @C6143ahL
    /* renamed from: fc */
    private void m31751fc(String str) {
        Actor fb;
        if (str != null && (fb = m31750fb(str)) != null) {
            mo71aT(fb);
            cdg();
        }
    }

    @C0064Am(aul = "ce7b2c63cf3c04bf050210ef2779cca6", aum = 0)
    /* renamed from: p */
    private void m31783p(String str, Object obj) {
        if (str != null) {
            if (obj == null) {
                bjo().remove(str);
                return;
            }
            Class<?> cls = obj.getClass();
            if (obj instanceof org.mozilla1.javascript.NativeArray) {
                obj = m31711b((org.mozilla1.javascript.NativeArray) obj, this.f7546TR);
            } else if (!(Number.class.isAssignableFrom(cls) || cls == String.class || cls == Boolean.class || cls == java.lang.Character.class)) {
                mo6317hy("Not setting value of class " + cls.getName());
                return;
            }
            bjo().put(str, obj);
        }
    }

    @C0064Am(aul = "8ddc6e63e5c93e5cd2ec774982964732", aum = 0)
    /* renamed from: fe */
    private Object m31752fe(String str) {
        Object obj = bjo().get(str);
        if (obj instanceof Object[]) {
            return m31703b((Object[]) obj, this.f7546TR);
        }
        return obj;
    }

    @C0064Am(aul = "54e354337bd3fdd2f051af098100d4bc", aum = 0)
    @C6143ahL
    /* renamed from: F */
    private void m31659F(String str, String str2) {
        cdr().setParameter(str, str2);
    }

    @C0064Am(aul = "1ded28d61c9f1bc19df7a545546eed79", aum = 0)
    @C6143ahL
    /* renamed from: ff */
    private String m31753ff(String str) {
        return cdr().getParameter(str);
    }

    @C0064Am(aul = "644f784590a0b9e8aedbfc1c644893b1", aum = 0)
    @C6143ahL
    private String bjy() {
        ProgressionCareer Rn = mo64KY().mo12637Rn();
        if (Rn == null) {
            return null;
        }
        return Rn.getHandle();
    }

    @C0064Am(aul = "40b450c4699a70c02acd0d87d92a01ef", aum = 0)
    /* renamed from: fg */
    private ItemType m31754fg(String str) {
        if (str == null) {
            return null;
        }
        for (Map.Entry<Class<? extends E>, Collection<E>> value : ala().mo1543t(ItemType.class).entrySet()) {
            Iterator it = ((Collection) value.getValue()).iterator();
            while (true) {
                if (it.hasNext()) {
                    ItemType jCVar = (ItemType) it.next();
                    if (str.equals(jCVar.getHandle())) {
                        return jCVar;
                    }
                }
            }
        }
        return null;
    }

    @C0064Am(aul = "5912f0b8eeffd89391a840fb12643746", aum = 0)
    /* renamed from: a */
    private Asset m31673a(AssetGroup ne, String str) {
        if (str == null) {
            return null;
        }
        if (ne == null) {
            return null;
        }
        for (Asset next : ne.bjM()) {
            if (str.equals(next.getHandle())) {
                return next;
            }
        }
        for (AssetGroup b : ne.bjO()) {
            Asset b2 = m31705b(b, str);
            if (b2 != null) {
                return b2;
            }
        }
        return null;
    }

    @C0064Am(aul = "9673cb07c5483bdd3dbb15b72b0c7e64", aum = 0)
    /* renamed from: fi */
    private Asset m31756fi(String str) {
        return m31705b(ala().aJa(), str);
    }

    @C0064Am(aul = "0ec96e974491b5547a820020886587c6", aum = 0)
    @C6143ahL
    /* renamed from: l */
    private int m31778l(String str, int i) {
        ItemType fh = m31755fh(str);
        if (fh == null) {
            return 0;
        }
        return cdr().mo9417f(fh, i);
    }

    @C0064Am(aul = "f6f01c74b169dd1024a8f9f52cbcccc3", aum = 0)
    @C6143ahL
    /* renamed from: n */
    private boolean m31780n(String str, int i) {
        ItemType fh = m31755fh(str);
        if (fh == null) {
            return false;
        }
        return cdr().mo9406b(fh, i, this);
    }

    @C0064Am(aul = "3652a856f8670c931516adf73f1bd0cc", aum = 0)
    /* renamed from: fk */
    private NPCType m31758fk(String str) {
        if (str == null) {
            return null;
        }
        for (NPCType next : ala().aJY()) {
            if (str.equals(next.getHandle())) {
                return next;
            }
        }
        return null;
    }

    @C0064Am(aul = "e3c22b4bf5b5f50dfb98c1c8fd6eaa49", aum = 0)
    @C6143ahL
    /* renamed from: a */
    private void m31686a(String str, org.mozilla1.javascript.NativeObject pd) {
        TableNPCSpawn aoj = (TableNPCSpawn) bFf().mo6865M(TableNPCSpawn.class);
        aoj.mo10S();
        Object[] ids = pd.getIds();
        if (ids != null) {
            for (Object obj : ids) {
                if (obj instanceof String) {
                    NPCType fl = m31759fl((String) obj);
                    if (fl != null) {
                        try {
                            double parseDouble = Double.parseDouble(pd.get((String) obj, this.f7546TR).toString());
                            NPCSpawn ju = (NPCSpawn) bFf().mo6865M(NPCSpawn.class);
                            ju.mo10S();
                            ju.mo3118a(fl);
                            ju.mo3121lr((int) parseDouble);
                            aoj.mo15168b(ju);
                        } catch (Exception e) {
                        }
                    } else {
                        mo18908eT("couldn't find a NPC type for '" + ((String) obj) + "'");
                    }
                }
            }
        }
        if (aoj.coi().size() == 0) {
            mo18909eV("is trying to declare an empty spawn table");
        } else {
            bjo().put(str, aoj);
        }
    }

    @C0064Am(aul = "8bd3afc7c6c185b33299e15c23e0d88d", aum = 0)
    @C6143ahL
    /* renamed from: a */
    private Object[] m31696a(int i, String str, String str2, boolean z) {
        Vec3d bMT;
        long aMJ;
        Object obj = bjo().get(str2);
        ArrayList arrayList = new ArrayList();
        if (obj == null || !(obj instanceof MissionArea)) {
            mo18909eV("is trying to spawn hostile NPCs without a valid area (" + str2 + ")");
        }
        MissionArea aaw = (MissionArea) obj;
        Object obj2 = bjo().get(str);
        if (obj2 == null || !(obj2 instanceof TableNPCSpawn)) {
            mo18909eV("is trying to spawn hostile NPCs without a valid spawn table (" + str + ")");
            return new Object[0];
        }
        TableNPCSpawn aoj = (TableNPCSpawn) obj2;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= i) {
                break;
            }
            for (NPCType next : aoj.bvC()) {
                if (aaw.bMT() == null) {
                    bMT = aaw.mo12220zO();
                } else {
                    bMT = aaw.bMT();
                }
                if (aaw.bMT() == null) {
                    aMJ = aaw.mo12213Vg();
                } else {
                    aMJ = aaw.aMJ();
                }
                int i5 = i2 + 1;
                String str3 = String.valueOf(str) + i2;
                NPC b = mo82b(str3, next, aaw.mo12212Ve().mo21606Nc(), bMT, (float) aMJ);
                Controller hb = b.mo12657hb();
                C6491anv.m24341a(hb, aaw.mo12212Ve().getPosition(), aaw.mo12212Ve().getRadius());
                if (z) {
                    C6491anv.m24342a(hb, (Character) mo64KY());
                }
                C6491anv.m24344f(hb);
                arrayList.add(str3);
                bjq().put(str3, b);
                i2 = i5;
            }
            i3 = i4 + 1;
        }
        Object[] objArr = new Object[arrayList.size()];
        for (int i6 = 0; i6 < arrayList.size(); i6++) {
            objArr[i6] = org.mozilla1.javascript.Context.m35239b(arrayList.get(i6), this.f7546TR);
        }
        return objArr;
    }

    @C0064Am(aul = "7105945654b382e23b03255167683db2", aum = 0)
    /* renamed from: fm */
    private Node m31760fm(String str) {
        if (str == null) {
            return null;
        }
        for (StellarSystem nodes : ala().aKa()) {
            Iterator<Node> it = nodes.getNodes().iterator();
            while (true) {
                if (it.hasNext()) {
                    Node next = it.next();
                    if (str.equals(next.getHandle())) {
                        return next;
                    }
                }
            }
        }
        return null;
    }

    @C0064Am(aul = "187f4904fbd03eca63531bfca502fb80", aum = 0)
    @C6143ahL
    /* renamed from: a */
    private void m31691a(String str, String str2, double d, double d2, double d3, long j) {
        Node fn;
        if (str2 != null && str != null && (fn = m31761fn(str2)) != null) {
            MissionArea aaw = (MissionArea) bFf().mo6865M(MissionArea.class);
            aaw.mo10S();
            aaw.mo12216c(fn);
            aaw.mo12219h(new Vec3d(d, d2, d3).mo9504d((Tuple3d) aaw.mo12212Ve().getPosition()));
            aaw.mo12217cJ(j);
            bjo().put(str, aaw);
        }
    }

    @C0064Am(aul = "cea78836ec94059e0c04f4fea16ff2d3", aum = 0)
    /* renamed from: fo */
    private I18NString m31762fo(String str) {
        if (str == null) {
            return null;
        }
        I18NStringTable bIU = ((ScriptableMissionTemplate) cdp()).bIU();
        if (bIU != null) {
            for (I18NStringTableEntry next : bIU.bDF()) {
                if (str.equals(next.getKey())) {
                    return next.cHj();
                }
            }
        }
        return null;
    }

    @C0064Am(aul = "c8b8cb03281d6d84dbe5c0ba6d0eec73", aum = 0)
    /* renamed from: fq */
    private ScriptableMissionTemplateObjective m31764fq(String str) {
        if (str == null) {
            return null;
        }
        for (ScriptableMissionTemplateObjective next : ((ScriptableMissionTemplate) cdp()).azk()) {
            if (str.equals(next.getHandle())) {
                return next;
            }
        }
        return null;
    }

    @C0064Am(aul = "846f8276275a59dc027e2d1da362ad83", aum = 0)
    /* renamed from: dM */
    private MissionObjective m31726dM(String str) {
        if (str == null) {
            return null;
        }
        for (MissionObjective sGVar : azc()) {
            if (str.equals(sGVar.getHandle())) {
                return sGVar;
            }
        }
        return null;
    }

    @C0064Am(aul = "1f4de417f22cf1f49f4ffbd115ae093b", aum = 0)
    @C6143ahL
    /* renamed from: fs */
    private void m31766fs(String str) {
        if (m31727dN(str) != null) {
            mo18909eV("is trying to create an objective with a repeated handle (" + str + ")");
            return;
        }
        I18NString fp = m31763fp(str);
        if (fp == null) {
            mo18909eV("is trying to create an objective with null description (" + str + ")");
            return;
        }
        ScriptableMissionTemplateObjective fr = m31765fr(str);
        if (fr == null) {
            mo18909eV("is trying to create an objective with no quantity reference (" + str + ")");
            return;
        }
        MissionObjective sGVar = (MissionObjective) bFf().mo6865M(MissionObjective.class);
        sGVar.mo21866a(str, fp, fr.mo4241Iq(), false);
        sGVar.mo21870c(Mission.C0015a.ACTIVE);
        sGVar.mo21871eA(0);
        azc().add(sGVar);
        m31709b(sGVar);
        bju();
    }

    @C0064Am(aul = "520d885bff545ebe1d9d882e32759992", aum = 0)
    @C6143ahL
    /* renamed from: a */
    private void m31694a(String str, String str2, int i) {
        if (m31727dN(str) != null) {
            mo18909eV("is trying to create an objective with a repeated handle (" + str + ")");
            return;
        }
        I18NString fp = m31763fp(str2);
        if (fp == null) {
            mo18909eV("is trying to create an objective with null description (" + str + ")");
            return;
        }
        MissionObjective sGVar = (MissionObjective) bFf().mo6865M(MissionObjective.class);
        sGVar.mo21866a(str, fp, i, false);
        sGVar.mo21870c(Mission.C0015a.ACTIVE);
        sGVar.mo21871eA(0);
        azc().add(sGVar);
        m31709b(sGVar);
        bju();
    }

    @C0064Am(aul = "bf57a8bbbfbc8ac70d862bdcb333cb1f", aum = 0)
    @C6143ahL
    /* renamed from: fu */
    private void m31767fu(String str) {
        if (str != null) {
            MissionObjective dN = m31727dN(str);
            if (dN != null) {
                azc().remove(dN);
            }
            bju();
        }
    }

    @C0064Am(aul = "e551ad9d2342e4c5c0af81aca377446f", aum = 0)
    @C6143ahL
    /* renamed from: fw */
    private void m31768fw(String str) {
        MissionObjective dN = m31727dN(str);
        if (dN != null) {
            dN.mo21870c(Mission.C0015a.ACCOMPLISHED);
            dN.mo21871eA(dN.abR());
            m31709b(dN);
            bju();
        }
    }

    @C0064Am(aul = "a05550b7b636c661300c80adcb3949c2", aum = 0)
    @C6143ahL
    /* renamed from: fy */
    private void m31769fy(String str) {
        MissionObjective dN = m31727dN(str);
        if (dN != null) {
            dN.mo21870c(Mission.C0015a.FAILED);
            bju();
        }
    }

    @C0064Am(aul = "154c1de454d3ea6f3dc43ec907946916", aum = 0)
    @C6143ahL
    /* renamed from: p */
    private void m31782p(String str, int i) {
        MissionObjective dN = m31727dN(str);
        if (dN != null) {
            if (i >= dN.abR()) {
                i = dN.abR();
                dN.mo21870c(Mission.C0015a.ACCOMPLISHED);
            } else {
                dN.mo21870c(Mission.C0015a.ACTIVE);
            }
            dN.mo21871eA(i);
            m31709b(dN);
            bju();
        }
    }

    @C0064Am(aul = "b2cd6b54b264f8013f7ab88c61f16b6f", aum = 0)
    @C6143ahL
    /* renamed from: fA */
    private int m31736fA(String str) {
        ItemType fh;
        if (str == null || (fh = m31755fh(str)) == null) {
            return 0;
        }
        int i = 0;
        for (C5820abA next : mo64KY().dxW()) {
            if (next.mo12359XH().bAP() == fh) {
                if (fh instanceof BulkItemType) {
                    i = next.mo12359XH().mo302Iq() + i;
                } else {
                    i++;
                }
            }
        }
        return i;
    }

    @C0064Am(aul = "c9a11498321ae3b319b943defe6054b2", aum = 0)
    @C6143ahL
    /* renamed from: G */
    private int m31660G(String str, String str2) {
        ItemType fh;
        if (str == null || str2 == null || (fh = m31755fh(str)) == null) {
            return 0;
        }
        int i = 0;
        for (C5820abA next : mo64KY().dxW()) {
            if (next.mo12359XH().bAP() == fh && str2.equals(next.mo12361eT().getHandle())) {
                if (fh instanceof BulkItemType) {
                    i = next.mo12359XH().mo302Iq() + i;
                } else {
                    i++;
                }
            }
        }
        return i;
    }

    @C0064Am(aul = "f23184f58eb32be42d9c53cda3eb10f9", aum = 0)
    @C6143ahL
    /* renamed from: a */
    private void m31692a(String str, String str2, float f) {
        mo18896b(str, str2, f, (String) null);
    }

    @C0064Am(aul = "3c6dfe76e64770bc57b6532a6c1e6ef5", aum = 0)
    /* renamed from: a */
    private Asset m31674a(String str, AssetGroup ne) {
        if (str == null) {
            return null;
        }
        if (ne == null) {
            return null;
        }
        for (Asset next : ne.bjM()) {
            if (str.equals(next.getHandle())) {
                return next;
            }
        }
        for (AssetGroup b : ne.bjO()) {
            Asset b2 = m31706b(str, b);
            if (b2 != null) {
                return b2;
            }
        }
        return null;
    }

    @C0064Am(aul = "ba4bb9bae18c920f19f44e596562e557", aum = 0)
    @C6143ahL
    /* renamed from: a */
    private void m31693a(String str, String str2, float f, String str3) {
        if (str2 != null && str != null) {
            I18NString fp = m31763fp(str2);
            if (fp == null) {
                mo18908eT("is trying to show a NPC message with an invalid handle: " + str2);
                return;
            }
            NPCType fl = m31759fl(str);
            if (fl == null) {
                mo18908eT("is trying to show a NPC message with an invalid NPC: " + str);
                return;
            }
            long j = (long) (1000.0f * f);
            Asset tCVar = null;
            if (str3 != null) {
                tCVar = m31706b(str3, ala().aJa());
            }
            mo64KY().mo14419f((C1506WA) new C5311aEl(fl, fp, j, tCVar));
        }
    }

    @C0064Am(aul = "d8d66c747675ee1d516415ea9c06883d", aum = 0)
    @C6143ahL
    /* renamed from: g */
    private void m31771g(String str, String str2, String str3) {
        I18NString fp = m31763fp(str2);
        if (fp == null) {
            mo18908eT("is trying to show a recurrent message with an invalid header handle: " + str2);
            return;
        }
        mo64KY().mo14419f((C1506WA) new C5543aNj(str, fp, m31757fj(str3)));
    }

    @C0064Am(aul = "9efc0945bcf278c13f1792a1704fdbce", aum = 0)
    @C6143ahL
    /* renamed from: fC */
    private void m31737fC(String str) {
        if (str != null) {
            mo64KY().mo14419f((C1506WA) new C6559apL(str));
        }
    }

    @C0064Am(aul = "a6c31740948edec8be0c4dc9246df101", aum = 0)
    @C6143ahL
    /* renamed from: fE */
    private boolean m31738fE(String str) {
        Object obj = bjo().get(str);
        if (obj == null || !(obj instanceof MissionArea)) {
            return false;
        }
        MissionArea aaw = (MissionArea) obj;
        aaw.mo12211Q(aaw.mo12220zO());
        aaw.mo12218eo(aaw.mo12213Vg());
        mo92b(str, aaw.mo12220zO(), aaw.mo12212Ve().mo21606Nc(), aaw.mo12213Vg(), true, true);
        return true;
    }

    @C0064Am(aul = "3856ceddbb713358b16a30c51fe507b2", aum = 0)
    @C6143ahL
    /* renamed from: b */
    private boolean m31710b(String str, long j) {
        Object obj = bjo().get(str);
        if (obj == null || !(obj instanceof MissionArea)) {
            return false;
        }
        MissionArea aaw = (MissionArea) obj;
        aaw.mo12211Q(new Vec3d(aaw.mo12220zO()).mo9531q(new Vec3f((((float) Math.random()) * 2.0f) - 1.0f, (((float) Math.random()) * 2.0f) - 1.0f, (((float) Math.random()) * 2.0f) - 1.0f).dfO().mo23510mS(((float) Math.random()) * ((float) aaw.mo12213Vg()))));
        aaw.mo12218eo(j);
        mo92b(str, aaw.bMT(), aaw.mo12212Ve().mo21606Nc(), aaw.aMJ(), true, true);
        return true;
    }

    @C0064Am(aul = "ab07a0f9460d5f3ff513cddfbc59b7b7", aum = 0)
    @C6143ahL
    /* renamed from: fG */
    private void m31739fG(String str) {
        mo128iZ(str);
    }

    @C0064Am(aul = "920c6a87d5633b28ed23b2078fe49ad6", aum = 0)
    @C6143ahL
    /* renamed from: fI */
    private boolean m31740fI(String str) {
        NPC auf = (NPC) bjq().get(str);
        if (auf == null || auf.isDisposed() || auf.bQx() == null) {
            return false;
        }
        return auf.bQx().isAlive();
    }

    @C0064Am(aul = "7d82bab5ea395fe3f3d6c4402c51066b", aum = 0)
    @C6143ahL
    /* renamed from: fK */
    private float m31741fK(String str) {
        NPC auf = (NPC) bjq().get(str);
        if (auf != null) {
            Ship bQx = auf.bQx();
            if (!auf.isDisposed() && bQx != null) {
                Shield zv = bQx.mo8288zv();
                return (zv.mo19079Tx() / zv.mo19089hh()) * 100.0f;
            }
        }
        return 0.0f;
    }

    @C0064Am(aul = "5d6977b5d7d886f25432e804dffec4e0", aum = 0)
    @C6143ahL
    /* renamed from: fM */
    private float m31742fM(String str) {
        NPC auf = (NPC) bjq().get(str);
        if (auf != null) {
            Ship bQx = auf.bQx();
            if (!auf.isDisposed() && bQx != null) {
                Hull zt = bQx.mo8287zt();
                return (zt.mo19923Tx() / zt.mo19934hh()) * 100.0f;
            }
        }
        return 0.0f;
    }

    @C0064Am(aul = "5be17df50d49b2811ca06d2fd0651ff4", aum = 0)
    @C6143ahL
    private float bjA() {
        Ship bQx = mo64KY().bQx();
        if (bQx == null) {
            return 0.0f;
        }
        Shield zv = bQx.mo8288zv();
        return (zv.mo19079Tx() / zv.mo19089hh()) * 100.0f;
    }

    @C0064Am(aul = "afd4f766ceb4c7442107b06bfab5306e", aum = 0)
    @C6143ahL
    private float bjC() {
        Ship bQx = mo64KY().bQx();
        if (bQx == null) {
            return 0.0f;
        }
        Hull zt = bQx.mo8287zt();
        return (zt.mo19923Tx() / zt.mo19934hh()) * 100.0f;
    }

    @C0064Am(aul = "69d9b7d8bc1bd0bdccff65367e8ec51f", aum = 0)
    @C6143ahL
    /* renamed from: fO */
    private void m31743fO(String str) {
        Ship bQx;
        NPC auf = (NPC) bjq().get(str);
        if (auf != null && (bQx = auf.bQx()) != null) {
            bQx.mo8348d(C3981xi.C3982a.class, this);
            bjr().put(bQx, str);
        }
    }

    @C0064Am(aul = "1a12a8d5140ceceeac46fea2997ea59c", aum = 0)
    /* renamed from: a */
    private void m31682a(Ship fAVar, Actor cr) {
        String str = (String) bjr().get(fAVar);
        if (str != null) {
            mo18912f("onNPCDamaged", str);
        }
    }

    @C0064Am(aul = "911e71a8143fced83359a3336a23c64a", aum = 0)
    /* renamed from: a */
    private void m31679a(NPC auf, Character acx) {
        Controller hb = auf.mo12657hb();
        if (hb instanceof C6534aom) {
            C6534aom aom = (C6534aom) hb;
            if (acx != null) {
                aom.mo4588J(acx.bQx());
            } else if (aom.bUT() != null) {
                aom.mo4588J((Ship) null);
            }
        }
    }

    @C0064Am(aul = "6372ccf9f270eb89520eee819ac82f74", aum = 0)
    @C6143ahL
    /* renamed from: a */
    private void m31688a(String str, org.mozilla1.javascript.NativeArray ayt) {
        Set set;
        HashSet hashSet;
        try {
            set = (Set) bjo().get(str);
        } catch (Exception e) {
            set = null;
        }
        if (set == null) {
            HashSet hashSet2 = new HashSet();
            bjo().put(str, hashSet2);
            hashSet = hashSet2;
        } else {
            hashSet = set;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (((long) i2) < ayt.getLength()) {
                try {
                    hashSet.add((String) ayt.get(i2, this.f7546TR));
                } catch (Exception e2) {
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    @C0064Am(aul = "5b5ceda71a4c4775d8c31f4059f679d8", aum = 0)
    /* renamed from: e */
    private List<NPC> m31730e(Set<String> set) {
        ArrayList arrayList = new ArrayList();
        for (String str : set) {
            NPC auf = (NPC) bjq().get(str);
            if (auf != null && !auf.isDisposed()) {
                Ship bQx = auf.bQx();
                if (!bQx.isDisposed() && bQx.isAlive()) {
                    arrayList.add(auf);
                }
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "0f78895bc0cffacf4f07f764a1415031", aum = 0)
    /* renamed from: r */
    private void m31785r(List<NPC> list) {
        Iterator<NPC> it = list.iterator();
        while (it.hasNext()) {
            NPC next = it.next();
            if (next.isDisposed() || next.bQx() == null || next.bQx().isDead()) {
                it.remove();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:? A[ADDED_TO_REGION, ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0017 A[ADDED_TO_REGION] */
    @p001a.C0064Am(aul = "5e3cbb61d019ab523fd7eebe5100d516", aum = 0)
    @p001a.C6143ahL
    /* renamed from: I */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m31664I(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            r2 = 0
            a.Wo r0 = r5.bjo()     // Catch:{ Exception -> 0x001a }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ Exception -> 0x001a }
            java.util.Set r0 = (java.util.Set) r0     // Catch:{ Exception -> 0x001a }
            a.Wo r1 = r5.bjo()     // Catch:{ Exception -> 0x005d }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ Exception -> 0x005d }
            java.util.Set r1 = (java.util.Set) r1     // Catch:{ Exception -> 0x005d }
        L_0x0015:
            if (r0 == 0) goto L_0x0019
            if (r1 != 0) goto L_0x001e
        L_0x0019:
            return
        L_0x001a:
            r0 = move-exception
            r0 = r2
        L_0x001c:
            r1 = r2
            goto L_0x0015
        L_0x001e:
            java.util.List r2 = r5.m31735f(r0)
            r5.m31787s((java.util.List<p001a.C5721aUf>) r2)
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0019
            java.util.List r3 = r5.m31735f(r1)
            r5.m31787s((java.util.List<p001a.C5721aUf>) r3)
            boolean r0 = r3.isEmpty()
            if (r0 != 0) goto L_0x0019
            r0 = 0
            java.util.Iterator r4 = r2.iterator()
            r2 = r0
        L_0x003e:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0019
            java.lang.Object r0 = r4.next()
            a.aUf r0 = (p001a.C5721aUf) r0
            int r1 = r3.size()
            int r1 = r2 % r1
            java.lang.Object r1 = r3.get(r1)
            a.aUf r1 = (p001a.C5721aUf) r1
            r5.mo18891b((p001a.C5721aUf) r0, (p001a.C5895acX) r1)
            int r0 = r2 + 1
            r2 = r0
            goto L_0x003e
        L_0x005d:
            r1 = move-exception
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2514gE.m31664I(java.lang.String, java.lang.String):void");
    }

    @C0064Am(aul = "93e2901d2c8f19c8d228859a56481e84", aum = 0)
    @C6143ahL
    /* renamed from: fQ */
    private void m31744fQ(String str) {
        Set set;
        try {
            set = (Set) bjo().get(str);
        } catch (Exception e) {
            set = null;
        }
        if (set != null) {
            List<NPC> f = m31735f(set);
            if (!f.isEmpty()) {
                for (NPC hb : f) {
                    C6491anv.m24342a(hb.mo12657hb(), (Character) mo64KY());
                }
            }
        }
    }

    @C0064Am(aul = "e74a3b38f84138107511027e114d1814", aum = 0)
    /* renamed from: fS */
    private boolean m31745fS(String str) {
        Object f = mo18912f("lootSpawnPermission", str);
        if (f == null) {
            return false;
        }
        if (f instanceof Boolean) {
            return ((Boolean) f).booleanValue();
        }
        return false;
    }

    @C0064Am(aul = "8b5b128caae114ef476ebbab5eaf1770", aum = 0)
    /* renamed from: c */
    private void m31718c(ItemType jCVar, int i) {
        mo18912f("onItemLootSpawn", jCVar.getHandle(), Integer.valueOf(i));
    }

    @C0064Am(aul = "2f3f820d9efb1513d245277b70d87a50", aum = 0)
    @C6143ahL
    /* renamed from: fU */
    private void m31746fU(String str) {
        C3131oI.C3132a valueOf = C3131oI.C3132a.valueOf(str);
        if (valueOf == null) {
            mo18909eV("is trying to post a nursery event with a invalid transition name: '" + str + "'");
        } else {
            mo64KY().mo14419f((C1506WA) new C3131oI(valueOf));
        }
    }

    @C0064Am(aul = "da1e38e9364ff63392718152cc093e5a", aum = 0)
    private List<GatePass> bjE() {
        return bjp();
    }

    @C0064Am(aul = "0d2127df7543302c4e6a1b83457b17ba", aum = 0)
    @C6143ahL
    /* renamed from: k */
    private void m31777k(String str, boolean z) {
        Actor fb = m31750fb(str);
        if (fb instanceof Gate) {
            for (GatePass adb : bjp()) {
                if (adb.mo8293lb() == fb) {
                    adb.mo8292jb(z);
                    return;
                }
            }
            GatePass adb2 = (GatePass) bFf().mo6865M(GatePass.class);
            adb2.mo10S();
            adb2.mo8291f((Gate) fb);
            adb2.mo8292jb(z);
            bjp().add(adb2);
        }
    }

    @C0064Am(aul = "2662ae3abb1074a234ffdbe9c217d161", aum = 0)
    @C6143ahL
    /* renamed from: fW */
    private void m31747fW(String str) {
        if (str != null) {
            Iterator it = bjp().iterator();
            while (it.hasNext()) {
                if (str.equals(((GatePass) it.next()).mo8293lb().getHandle())) {
                    it.remove();
                    return;
                }
            }
        }
    }

    @C0064Am(aul = "36ba3b6330234a0d18bc65c43a720839", aum = 0)
    @C6143ahL
    /* renamed from: fY */
    private boolean m31748fY(String str) {
        for (GatePass lb : bjp()) {
            if (lb.mo8293lb().getHandle().equals(str)) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "4f959eda27dbf7ca8f9e58c56fc08a97", aum = 0)
    @C6143ahL
    private float bjG() {
        return bjs();
    }

    @C0064Am(aul = "de7d37b39e2d8b36b2921f0dcc06a5e7", aum = 0)
    @C6143ahL
    /* renamed from: gw */
    private void m31773gw(float f) {
        m31772gv(f);
    }

    @C4034yP
    @C0064Am(aul = "21d3d95e02743d87c67efb2810dbb11a", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: c */
    private void m31719c(Runnable runnable) {
        runnable.run();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.gE$a */
    public class SelectionCallback extends aDJ implements C1616Xf, C6681ard {
        /* renamed from: QS */
        public static final C5663aRz f7548QS = null;
        /* renamed from: QU */
        public static final C5663aRz f7550QU = null;
        /* renamed from: QW */
        public static final C5663aRz f7552QW = null;
        /* renamed from: QY */
        public static final C2491fm f7554QY = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7555aT = null;
        private static final long serialVersionUID = 928566213673285239L;
        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "8c58f0653e4231a56c5e1cec3474e234", aum = 3)

        /* renamed from: QX */
        static /* synthetic */ ScriptableMission f7553QX = null;
        @C0064Am(aul = "684b312a5cb01c94456df3d60f6757a4", aum = 0)

        /* renamed from: QR */
        private static Ship f7547QR = null;
        @C0064Am(aul = "1a5d0b8c6900235122da9e59c08eb0dc", aum = 1)

        /* renamed from: QT */
        private static Actor f7549QT = null;
        @C0064Am(aul = "0518b7d19f3be250d5b9d3e27cc5a5ba", aum = 2)

        /* renamed from: QV */
        private static float f7551QV = 0.0f;

        static {
            m31870V();
        }

        public SelectionCallback() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public SelectionCallback(C5540aNg ang) {
            super(ang);
        }

        public SelectionCallback(ScriptableMission gEVar, Ship fAVar, Actor cr, float f) {
            super((C5540aNg) null);
            super._m_script_init(gEVar, fAVar, cr, f);
        }

        /* renamed from: V */
        static void m31870V() {
            _m_fieldCount = aDJ._m_fieldCount + 4;
            _m_methodCount = aDJ._m_methodCount + 1;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 4)];
            C5663aRz b = C5640aRc.m17844b(ScriptableMission.SelectionCallback.class, "684b312a5cb01c94456df3d60f6757a4", i);
            f7548QS = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(ScriptableMission.SelectionCallback.class, "1a5d0b8c6900235122da9e59c08eb0dc", i2);
            f7550QU = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(ScriptableMission.SelectionCallback.class, "0518b7d19f3be250d5b9d3e27cc5a5ba", i3);
            f7552QW = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(ScriptableMission.SelectionCallback.class, "8c58f0653e4231a56c5e1cec3474e234", i4);
            f7555aT = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i6 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i6 + 1)];
            C2491fm a = C4105zY.m41624a(ScriptableMission.SelectionCallback.class, "3e389e9065f1497f559aba89f8413017", i6);
            f7554QY = a;
            fmVarArr[i6] = a;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ScriptableMission.SelectionCallback.class, C6248ajM.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31871a(ScriptableMission gEVar) {
            bFf().mo5608dq().mo3197f(f7555aT, gEVar);
        }

        /* renamed from: ad */
        private void m31872ad(float f) {
            bFf().mo5608dq().mo3150a(f7552QW, f);
        }

        /* renamed from: e */
        private void m31873e(Ship fAVar) {
            bFf().mo5608dq().mo3197f(f7548QS, fAVar);
        }

        /* renamed from: w */
        private void m31874w(Actor cr) {
            bFf().mo5608dq().mo3197f(f7550QU, cr);
        }

        /* renamed from: wc */
        private Ship m31875wc() {
            return (Ship) bFf().mo5608dq().mo3214p(f7548QS);
        }

        /* renamed from: wd */
        private Actor m31876wd() {
            return (Actor) bFf().mo5608dq().mo3214p(f7550QU);
        }

        /* renamed from: we */
        private float m31877we() {
            return bFf().mo5608dq().mo3211m(f7552QW);
        }

        /* renamed from: wf */
        private ScriptableMission m31878wf() {
            return (ScriptableMission) bFf().mo5608dq().mo3214p(f7555aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6248ajM(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    m31879wg();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public void run() {
            switch (bFf().mo6893i(f7554QY)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7554QY, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7554QY, new Object[0]));
                    break;
            }
            m31879wg();
        }

        /* renamed from: a */
        public void mo18940a(ScriptableMission gEVar, Ship fAVar, Actor cr, float f) {
            m31871a(gEVar);
            super.mo10S();
            m31873e(fAVar);
            m31874w(cr);
            m31872ad(f);
        }

        @C0064Am(aul = "3e389e9065f1497f559aba89f8413017", aum = 0)
        /* renamed from: wg */
        private void m31879wg() {
            if (m31875wc().mo1011bv(m31876wd()) <= m31877we()) {
                m31878wf().m31724d(m31875wc(), m31876wd());
            }
        }
    }

    /* renamed from: a.gE$b */
    /* compiled from: a */
    class C2516b extends ThreadLocal<org.mozilla1.javascript.Context> {
        C2516b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: bin */
        public org.mozilla1.javascript.Context initialValue() {
            return ScriptableMission.this.aRv();
        }
    }
}
