package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C5512aMe;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.TableNPCSpawn;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.ItemDeliveryMissionScriptTemplate;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C0176CC;
import logic.data.mbean.C6452anI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

@C5829abJ("1.1.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.mission.ItemDeliveryMission")
@C6485anp
@C5511aMd
/* renamed from: a.aBT */
/* compiled from: a */
public class ItemDeliveryMissionScript<T extends ItemDeliveryMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f2416Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cyj = null;
    public static final C5663aRz cyl = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f2417yH = null;
    private static final String iHd = "itemDeliveryNPC";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "749c1521b74b1150f35702defb836a24", aum = 0)
    private static C1556Wo<ItemType, Integer> cyi = null;
    @C0064Am(aul = "58f7abf1c0ef478c69465a98eb9f308e", aum = 1)
    private static C1556Wo<ItemType, Integer> cyk = null;

    static {
        m12942V();
    }

    public ItemDeliveryMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemDeliveryMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12942V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 2;
        _m_methodCount = MissionScript._m_methodCount + 2;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ItemDeliveryMissionScript.class, "749c1521b74b1150f35702defb836a24", i);
        cyj = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemDeliveryMissionScript.class, "58f7abf1c0ef478c69465a98eb9f308e", i2);
        cyl = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i4 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 2)];
        C2491fm a = C4105zY.m41624a(ItemDeliveryMissionScript.class, "e6f30ea198b557a3129b967215d993e7", i4);
        f2417yH = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemDeliveryMissionScript.class, "efda4dd2717899fe6bed0596c17a1d09", i5);
        f2416Do = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemDeliveryMissionScript.class, C6452anI.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m12940A(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cyj, wo);
    }

    /* renamed from: B */
    private void m12941B(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cyl, wo);
    }

    /* access modifiers changed from: private */
    public C1556Wo aDi() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cyj);
    }

    /* access modifiers changed from: private */
    public C1556Wo aDj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cyl);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6452anI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m12947jF();
                return null;
            case 1:
                m12943a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2416Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2416Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2416Do, new Object[]{jt}));
                break;
        }
        m12943a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f2417yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2417yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2417yH, new Object[0]));
                break;
        }
        m12947jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "e6f30ea198b557a3129b967215d993e7", aum = 0)
    /* renamed from: jF */
    private void m12947jF() {
        mo575p(StateA.class);
    }

    @C0064Am(aul = "efda4dd2717899fe6bed0596c17a1d09", aum = 0)
    /* renamed from: a */
    private void m12943a(C0665JT jt) {
        if ("".equals(jt.getVersion())) {
            if (azm() == null) {
                mo576r(StateA.class);
            }
        } else if ("1.0.0".equals(jt.getVersion())) {
            C0665JT eE = jt.mo3113eE("missionTemplate");
            ItemType jCVar = (ItemType) ((C5512aMe) eE.get("deliveryItemType")).baw();
            int intValue = ((Integer) eE.get("deliveryQuantity")).intValue();
            int intValue2 = ((Integer) jt.get("deliveredItems")).intValue();
            if (jCVar != null) {
                aDi().put(jCVar, new Integer(intValue - intValue2));
                if (intValue2 > 0) {
                    aDj().put(jCVar, Integer.valueOf(intValue2));
                    return;
                }
                return;
            }
            mo8358lY("DeliverItemType null!!");
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aBT$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C2491fm aLU = null;
        /* renamed from: aT */
        public static final C5663aRz f2418aT = null;
        public static final C2491fm beE = null;
        public static final C2491fm byQ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "4715e6977df2b665b9df2388da99dc11", aum = 0)
        static /* synthetic */ ItemDeliveryMissionScript cvY;

        static {
            m12957V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateA(ItemDeliveryMissionScript abt) {
            super((C5540aNg) null);
            super._m_script_init(abt);
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m12957V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 3;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ItemDeliveryMissionScript.StateA.class, "4715e6977df2b665b9df2388da99dc11", i);
            f2418aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
            C2491fm a = C4105zY.m41624a(ItemDeliveryMissionScript.StateA.class, "bc5ad5c80cf03f3a9ee9eccd6bd62924", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ItemDeliveryMissionScript.StateA.class, "343425d9b4c74291aee67a51f0cdd573", i4);
            aLU = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(ItemDeliveryMissionScript.StateA.class, "851c8229938273f8ff6bb0054ce66e56", i5);
            byQ = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ItemDeliveryMissionScript.StateA.class, C0176CC.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m12958a(ItemDeliveryMissionScript abt) {
            bFf().mo5608dq().mo3197f(f2418aT, abt);
        }

        private ItemDeliveryMissionScript cKJ() {
            return (ItemDeliveryMissionScript) bFf().mo5608dq().mo3214p(f2418aT);
        }

        /* renamed from: RM */
        public void mo6623RM() {
            switch (bFf().mo6893i(aLU)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                    break;
            }
            m12956RL();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0176CC(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m12956RL();
                    return null;
                case 2:
                    m12959cq((String) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: cr */
        public void mo6626cr(String str) {
            switch (bFf().mo6893i(byQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    break;
            }
            m12959cq(str);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo7918b(ItemDeliveryMissionScript abt) {
            m12958a(abt);
            super.mo10S();
        }

        @C0064Am(aul = "bc5ad5c80cf03f3a9ee9eccd6bd62924", aum = 0)
        private void aai() {
            WaypointDat brY;
            TableNPCSpawn bvw;
            int i = 0;
            for (ItemTypeTableItem next : ((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).ajG()) {
                ArrayList<ItemTypeTableItem> arrayList = new ArrayList<>();
                if (!cKJ().cdr().mo9406b(next.mo23385az(), next.getAmount(), cKJ())) {
                    for (ItemTypeTableItem zlVar : arrayList) {
                        cKJ().cdr().mo9417f(zlVar.mo23385az(), zlVar.getAmount());
                    }
                    cKJ().fail();
                } else {
                    arrayList.add(next);
                    cKJ().mo122f(((NLSSpeechActions) cKJ().ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEA(), next.mo23385az().mo19891ke());
                }
            }
            cKJ().aDi().clear();
            for (ItemTypeTableItem next2 : ((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).ajE()) {
                cKJ().aDi().put(next2.mo23385az(), new Integer(next2.getAmount()));
            }
            if (((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).mo10584Nc() != null && ((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).brY() != null && (bvw = brY.bvw()) != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i;
                    if (i2 < (brY = ((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).brY()).getAmount()) {
                        i = i3;
                        for (NPCType b : bvw.bvC()) {
                            cKJ().mo565b(ItemDeliveryMissionScript.iHd + i, b, brY, ((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).mo10584Nc());
                            i++;
                        }
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        }

        @C0064Am(aul = "343425d9b4c74291aee67a51f0cdd573", aum = 0)
        /* renamed from: RL */
        private void m12956RL() {
            for (ItemTypeTableItem next : ((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).ajG()) {
                cKJ().cdr().mo9417f(next.mo23385az(), next.getAmount());
                cKJ().mo122f(((NLSSpeechActions) cKJ().ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEy(), next.mo23385az().mo19891ke());
            }
        }

        @C0064Am(aul = "851c8229938273f8ff6bb0054ce66e56", aum = 0)
        /* renamed from: cq */
        private void m12959cq(String str) {
            if (cKJ().cdp() == null) {
                mo8358lY("Mission template not set in an instance of " + getClass().getSimpleName());
                return;
            }
            cKJ().mo566b(str, Mission.C0015a.ACCOMPLISHED);
            if (str.equals(((ItemDeliveryMissionScriptTemplate) cKJ().cdp()).bse())) {
                for (ItemType jCVar : new HashSet(cKJ().aDi().keySet())) {
                    Integer num = (Integer) cKJ().aDi().get(jCVar);
                    int f = cKJ().cdr().mo9417f(jCVar, num.intValue());
                    if (f != 0) {
                        Integer num2 = (Integer) cKJ().aDj().get(jCVar);
                        if (num2 == null) {
                            num2 = new Integer(0);
                        }
                        Integer valueOf = Integer.valueOf(num2.intValue() + f);
                        Integer valueOf2 = Integer.valueOf(num.intValue() - f);
                        cKJ().mo574k(jCVar.getHandle(), valueOf.intValue());
                        cKJ().aDi().put(jCVar, valueOf2);
                        cKJ().aDj().put(jCVar, valueOf);
                        if (valueOf2.intValue() == 0) {
                            cKJ().aDi().remove(jCVar);
                            cKJ().mo566b(jCVar.getHandle(), Mission.C0015a.ACCOMPLISHED);
                        }
                    }
                }
                if (cKJ().aDi().size() == 0) {
                    cKJ().cdk();
                }
            }
        }
    }
}
