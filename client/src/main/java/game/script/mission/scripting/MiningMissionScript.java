package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.missiontemplate.scripting.MiningMissionScriptTemplate;
import game.script.ship.Station;
import game.script.space.AsteroidLootType;
import game.script.space.LootType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0908NK;
import logic.data.mbean.C1509WD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.HashSet;

@C6485anp
@C5511aMd
/* renamed from: a.aEi  reason: case insensitive filesystem */
/* compiled from: a */
public class MiningMissionScript<T extends MiningMissionScriptTemplate> extends MissionScript<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cyl = null;
    public static final C5663aRz hFa = null;
    public static final C5663aRz hFb = null;
    public static final C5663aRz hFc = null;
    public static final C5663aRz hFd = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f2772yH = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d56d195d6e625bf6c61e4cd0031fc462", aum = 2)
    private static C1556Wo<ItemType, Integer> cyk;
    @C0064Am(aul = "19be3ead40d41187d28fe4211efa329b", aum = 0)
    private static C1556Wo<ItemType, Integer> dHL;
    @C0064Am(aul = "7640a7ffc4d46be109b2f6f2d14706ea", aum = 1)
    private static C1556Wo<ItemType, Integer> dHM;
    @C0064Am(aul = "6713d31aff3defcfd62eeef38288c166", aum = 3)
    private static boolean dHN;
    @C0064Am(aul = "306f24033cb4e9a2f51f7967754c21b9", aum = 4)
    private static boolean dHO;

    static {
        m14342V();
    }

    public MiningMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MiningMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14342V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 5;
        _m_methodCount = MissionScript._m_methodCount + 1;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(MiningMissionScript.class, "19be3ead40d41187d28fe4211efa329b", i);
        hFa = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MiningMissionScript.class, "7640a7ffc4d46be109b2f6f2d14706ea", i2);
        hFb = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MiningMissionScript.class, "d56d195d6e625bf6c61e4cd0031fc462", i3);
        cyl = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MiningMissionScript.class, "6713d31aff3defcfd62eeef38288c166", i4);
        hFc = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MiningMissionScript.class, "306f24033cb4e9a2f51f7967754c21b9", i5);
        hFd = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i7 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 1)];
        C2491fm a = C4105zY.m41624a(MiningMissionScript.class, "dcab86d7d70d12c4707b524d9518cff1", i7);
        f2772yH = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MiningMissionScript.class, C0908NK.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m14341B(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cyl, wo);
    }

    /* access modifiers changed from: private */
    public C1556Wo aDj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cyl);
    }

    /* renamed from: ah */
    private void m14344ah(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(hFa, wo);
    }

    /* renamed from: ai */
    private void m14345ai(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(hFb, wo);
    }

    /* access modifiers changed from: private */
    public C1556Wo cXn() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(hFa);
    }

    /* access modifiers changed from: private */
    public C1556Wo cXo() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(hFb);
    }

    /* access modifiers changed from: private */
    public boolean cXp() {
        return bFf().mo5608dq().mo3201h(hFc);
    }

    /* access modifiers changed from: private */
    public boolean cXq() {
        return bFf().mo5608dq().mo3201h(hFd);
    }

    /* renamed from: je */
    private void m14351je(boolean z) {
        bFf().mo5608dq().mo3153a(hFc, z);
    }

    /* renamed from: jf */
    private void m14352jf(boolean z) {
        bFf().mo5608dq().mo3153a(hFd, z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0908NK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m14350jF();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f2772yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2772yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2772yH, new Object[0]));
                break;
        }
        m14350jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "dcab86d7d70d12c4707b524d9518cff1", aum = 0)
    /* renamed from: jF */
    private void m14350jF() {
        boolean z;
        boolean z2 = true;
        if (((MiningMissionScriptTemplate) cdp()).alK() != null) {
            z = true;
        } else {
            z = false;
        }
        m14351je(z);
        if (((MiningMissionScriptTemplate) cdp()).ajI() == null) {
            z2 = false;
        }
        m14352jf(z2);
        mo575p(StateA.class);
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.aEi$a */
    class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f2773aT = null;
        public static final C2491fm beE = null;
        public static final C2491fm byJ = null;
        public static final C2491fm byQ = null;
        public static final C2491fm byR = null;
        public static final C2491fm hKN = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "52b48dda160d8c9df667f54b7b8a8800", aum = 0)
        static /* synthetic */ MiningMissionScript eyl;

        static {
            m14360V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateA(MiningMissionScript aei) {
            super((C5540aNg) null);
            super._m_script_init(aei);
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m14360V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 5;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(MiningMissionScript.StateA.class, "52b48dda160d8c9df667f54b7b8a8800", i);
            f2773aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(MiningMissionScript.StateA.class, "1ac6c5b339cf9f2c7cd086d0119b94aa", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(MiningMissionScript.StateA.class, "f4603aab50c3c41ada08ac45735d47f5", i4);
            byR = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(MiningMissionScript.StateA.class, "97c55a06ca9bca1126ea16b7f134fdb3", i5);
            byQ = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(MiningMissionScript.StateA.class, "ebed8dedb1cf05ddda7f9b7924f3e57b", i6);
            byJ = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(MiningMissionScript.StateA.class, "ef4e068ff17d405a9262f37a8581b698", i7);
            hKN = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(MiningMissionScript.StateA.class, C1509WD.class, _m_fields, _m_methods);
        }

        private MiningMissionScript cZr() {
            return (MiningMissionScript) bFf().mo5608dq().mo3214p(f2773aT);
        }

        private void cZt() {
            switch (bFf().mo6893i(hKN)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, hKN, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, hKN, new Object[0]));
                    break;
            }
            cZs();
        }

        /* renamed from: f */
        private void m14363f(MiningMissionScript aei) {
            bFf().mo5608dq().mo3197f(f2773aT, aei);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1509WD(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m14361a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                    return null;
                case 2:
                    m14362cq((String) args[0]);
                    return null;
                case 3:
                    m14364p((Station) args[0]);
                    return null;
                case 4:
                    cZs();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo8416b(LootType ahc, ItemType jCVar, int i) {
            switch (bFf().mo6893i(byR)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                    break;
            }
            m14361a(ahc, jCVar, i);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: cr */
        public void mo6626cr(String str) {
            switch (bFf().mo6893i(byQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    break;
            }
            m14362cq(str);
        }

        /* renamed from: q */
        public void mo4971q(Station bf) {
            switch (bFf().mo6893i(byJ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                    break;
            }
            m14364p(bf);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: g */
        public void mo8654g(MiningMissionScript aei) {
            m14363f(aei);
            super.mo10S();
        }

        @C0064Am(aul = "1ac6c5b339cf9f2c7cd086d0119b94aa", aum = 0)
        private void aai() {
            cZr().cXn().clear();
            for (ItemTypeTableItem next : ((MiningMissionScriptTemplate) cZr().cdp()).alI()) {
                cZr().cXn().put(next.mo23385az(), new Integer(next.getAmount()));
            }
        }

        @C0064Am(aul = "f4603aab50c3c41ada08ac45735d47f5", aum = 0)
        /* renamed from: a */
        private void m14361a(LootType ahc, ItemType jCVar, int i) {
            Integer num;
            if ((ahc instanceof AsteroidLootType) && (num = (Integer) cZr().cXn().get(jCVar)) != null) {
                Integer num2 = (Integer) cZr().cXo().get(jCVar);
                if (num2 == null) {
                    num2 = new Integer(0);
                }
                if (i > num.intValue()) {
                    i = num.intValue();
                }
                Integer valueOf = Integer.valueOf(num2.intValue() + i);
                Integer valueOf2 = Integer.valueOf(num.intValue() - i);
                cZr().cXn().put(jCVar, valueOf2);
                cZr().cXo().put(jCVar, valueOf);
                if (!cZr().cXq() && !cZr().cXp()) {
                    cZr().mo574k(jCVar.getHandle(), valueOf.intValue());
                }
                if (valueOf2.intValue() <= 0) {
                    cZr().cXn().remove(jCVar);
                    if (!cZr().cXq() && !cZr().cXp()) {
                        cZr().mo566b(jCVar.getHandle(), Mission.C0015a.ACCOMPLISHED);
                    }
                }
                if (cZr().cXn().size() == 0 && !cZr().cXq() && !cZr().cXp()) {
                    cZr().cdk();
                }
            }
        }

        @C0064Am(aul = "97c55a06ca9bca1126ea16b7f134fdb3", aum = 0)
        /* renamed from: cq */
        private void m14362cq(String str) {
            if (cZr().cXp() && str.equals(((MiningMissionScriptTemplate) cZr().cdp()).alK())) {
                cZt();
            }
        }

        @C0064Am(aul = "ebed8dedb1cf05ddda7f9b7924f3e57b", aum = 0)
        /* renamed from: p */
        private void m14364p(Station bf) {
            if (cZr().cXq() && bf == ((MiningMissionScriptTemplate) cZr().cdp()).ajI()) {
                cZt();
            }
        }

        @C0064Am(aul = "ef4e068ff17d405a9262f37a8581b698", aum = 0)
        private void cZs() {
            for (ItemType jCVar : new HashSet(cZr().cXo().keySet())) {
                Integer num = (Integer) cZr().cXo().get(jCVar);
                int f = cZr().cdr().mo9417f(jCVar, num.intValue());
                if (f != 0) {
                    Integer num2 = (Integer) cZr().aDj().get(jCVar);
                    if (num2 == null) {
                        num2 = new Integer(0);
                    }
                    Integer valueOf = Integer.valueOf(num2.intValue() + f);
                    Integer valueOf2 = Integer.valueOf(num.intValue() - f);
                    cZr().mo574k(jCVar.getHandle(), valueOf.intValue());
                    cZr().cXo().put(jCVar, valueOf2);
                    cZr().aDj().put(jCVar, valueOf);
                    Integer num3 = (Integer) cZr().cXn().get(jCVar);
                    if (valueOf2.intValue() == 0 && (num3 == null || num3.intValue() == 0)) {
                        cZr().cXo().remove(jCVar);
                        cZr().mo566b(jCVar.getHandle(), Mission.C0015a.ACCOMPLISHED);
                    }
                }
            }
            if (cZr().cXo().size() == 0 && cZr().cXn().size() == 0) {
                cZr().cdk();
            }
        }
    }
}
