package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.MissionTemplate;
import game.script.mission.MissionTemplateObjective;
import game.script.mission.actions.MissionAction;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0597IR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.aBQ */
/* compiled from: a */
public abstract class MissionScriptTemplate extends MissionTemplate implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aBr = null;
    public static final C5663aRz cnp = null;
    public static final C5663aRz hiD = null;
    public static final C5663aRz hiE = null;
    public static final C5663aRz hiF = null;
    public static final C2491fm hiG = null;
    public static final C2491fm hiH = null;
    public static final C2491fm hiI = null;
    public static final C2491fm hiJ = null;
    public static final C2491fm hiK = null;
    public static final C2491fm hiL = null;
    public static final C2491fm hiM = null;
    public static final C2491fm hiN = null;
    public static final C2491fm hiO = null;
    public static final C2491fm hiP = null;
    public static final C2491fm hiQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "331af5cefa51bdad623b42f5df96c243", aum = 0)
    private static C3438ra<MissionTemplateObjective> cno;
    @C0064Am(aul = "aa060333739c6fee7efcd3611cc9461b", aum = 1)
    private static C3438ra<MissionAction> dhh;
    @C0064Am(aul = "60c0e594ffe426eb57d326a959a6ed48", aum = 2)
    private static int dhi;
    @C0064Am(aul = "28131b9a2b0cc4601e69dc4fb4bd3a40", aum = 3)
    private static long dhj;

    static {
        m12888V();
    }

    public MissionScriptTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionScriptTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12888V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionTemplate._m_fieldCount + 4;
        _m_methodCount = MissionTemplate._m_methodCount + 13;
        int i = MissionTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(MissionScriptTemplate.class, "331af5cefa51bdad623b42f5df96c243", i);
        cnp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionScriptTemplate.class, "aa060333739c6fee7efcd3611cc9461b", i2);
        hiD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionScriptTemplate.class, "60c0e594ffe426eb57d326a959a6ed48", i3);
        hiE = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionScriptTemplate.class, "28131b9a2b0cc4601e69dc4fb4bd3a40", i4);
        hiF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionTemplate._m_fields, (Object[]) _m_fields);
        int i6 = MissionTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 13)];
        C2491fm a = C4105zY.m41624a(MissionScriptTemplate.class, "b1d44ff19dcbd67342d35592a5ecb843", i6);
        aBr = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionScriptTemplate.class, "26da9aab9b7100057febdcbeead6d9b5", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionScriptTemplate.class, "6826dd1345d5ab991e983e16badae492", i8);
        hiG = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionScriptTemplate.class, "943a096fb987b6345a5afcca719449e3", i9);
        hiH = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionScriptTemplate.class, "4ed75933a62098cd2d3fff14f54882ef", i10);
        hiI = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionScriptTemplate.class, "8bfad87981298f25727fc05d43646abf", i11);
        hiJ = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionScriptTemplate.class, "358227bec61369a521d41a658ef1b2c2", i12);
        hiK = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionScriptTemplate.class, "806c1e172170ad055a18e389dc45935c", i13);
        hiL = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionScriptTemplate.class, "cde790f4315d0222a2d8d7e2beec7320", i14);
        hiM = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionScriptTemplate.class, "9cb228e75ed00dfae877614c4c554247", i15);
        hiN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionScriptTemplate.class, "69136b6fe4d189f4ffc49b853e42b4a5", i16);
        hiO = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(MissionScriptTemplate.class, "775560460619d3dd92acdcf1fbe4652d", i17);
        hiP = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(MissionScriptTemplate.class, "5fa46086d25687832557a6d47cfb80cd", i18);
        hiQ = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionScriptTemplate.class, C0597IR.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "b1d44ff19dcbd67342d35592a5ecb843", aum = 0)
    /* renamed from: MZ */
    private void m12887MZ() {
        throw new aWi(new aCE(this, aBr, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Objectives")
    @C0064Am(aul = "6826dd1345d5ab991e983e16badae492", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m12889a(MissionTemplateObjective aqy) {
        throw new aWi(new aCE(this, hiG, new Object[]{aqy}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Actions")
    @C0064Am(aul = "8bfad87981298f25727fc05d43646abf", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m12890a(MissionAction cnVar) {
        throw new aWi(new aCE(this, hiJ, new Object[]{cnVar}));
    }

    /* renamed from: ad */
    private void m12891ad(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnp, raVar);
    }

    private C3438ra azc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnp);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Objectives")
    @C0064Am(aul = "943a096fb987b6345a5afcca719449e3", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m12893c(MissionTemplateObjective aqy) {
        throw new aWi(new aCE(this, hiH, new Object[]{aqy}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Actions")
    @C0064Am(aul = "358227bec61369a521d41a658ef1b2c2", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m12894c(MissionAction cnVar) {
        throw new aWi(new aCE(this, hiK, new Object[]{cnVar}));
    }

    private C3438ra cJX() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hiD);
    }

    private int cJY() {
        return bFf().mo5608dq().mo3212n(hiE);
    }

    private long cJZ() {
        return bFf().mo5608dq().mo3213o(hiF);
    }

    /* renamed from: cq */
    private void m12895cq(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hiD, raVar);
    }

    /* renamed from: jF */
    private void m12896jF(long j) {
        bFf().mo5608dq().mo3184b(hiF, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Cost")
    @C0064Am(aul = "775560460619d3dd92acdcf1fbe4652d", aum = 0)
    @C5566aOg
    /* renamed from: jG */
    private void m12897jG(long j) {
        throw new aWi(new aCE(this, hiP, new Object[]{new Long(j)}));
    }

    /* renamed from: wO */
    private void m12899wO(int i) {
        bFf().mo5608dq().mo3183b(hiE, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to expire (s)")
    @C0064Am(aul = "9cb228e75ed00dfae877614c4c554247", aum = 0)
    @C5566aOg
    /* renamed from: wP */
    private void m12900wP(int i) {
        throw new aWi(new aCE(this, hiN, new Object[]{new Integer(i)}));
    }

    /* renamed from: Na */
    public void mo1677Na() {
        switch (bFf().mo6893i(aBr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBr, new Object[0]));
                break;
        }
        m12887MZ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0597IR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionTemplate._m_methodCount) {
            case 0:
                m12887MZ();
                return null;
            case 1:
                return m12892au();
            case 2:
                m12889a((MissionTemplateObjective) args[0]);
                return null;
            case 3:
                m12893c((MissionTemplateObjective) args[0]);
                return null;
            case 4:
                return cKa();
            case 5:
                m12890a((MissionAction) args[0]);
                return null;
            case 6:
                m12894c((MissionAction) args[0]);
                return null;
            case 7:
                return cKc();
            case 8:
                return new Integer(cKe());
            case 9:
                m12900wP(((Integer) args[0]).intValue());
                return null;
            case 10:
                return new Long(cKg());
            case 11:
                m12897jG(((Long) args[0]).longValue());
                return null;
            case 12:
                return m12898lm((String) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Objectives")
    @C5566aOg
    /* renamed from: b */
    public void mo7885b(MissionTemplateObjective aqy) {
        switch (bFf().mo6893i(hiG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hiG, new Object[]{aqy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hiG, new Object[]{aqy}));
                break;
        }
        m12889a(aqy);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Actions")
    @C5566aOg
    /* renamed from: b */
    public void mo7886b(MissionAction cnVar) {
        switch (bFf().mo6893i(hiJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hiJ, new Object[]{cnVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hiJ, new Object[]{cnVar}));
                break;
        }
        m12890a(cnVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Objectives")
    public List<MissionTemplateObjective> cKb() {
        switch (bFf().mo6893i(hiI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hiI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hiI, new Object[0]));
                break;
        }
        return cKa();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Actions")
    public List<MissionAction> cKd() {
        switch (bFf().mo6893i(hiL)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hiL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hiL, new Object[0]));
                break;
        }
        return cKc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to expire (s)")
    public int cKf() {
        switch (bFf().mo6893i(hiM)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hiM, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hiM, new Object[0]));
                break;
        }
        return cKe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Cost")
    public long cKh() {
        switch (bFf().mo6893i(hiO)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hiO, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hiO, new Object[0]));
                break;
        }
        return cKg();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Objectives")
    @C5566aOg
    /* renamed from: d */
    public void mo7891d(MissionTemplateObjective aqy) {
        switch (bFf().mo6893i(hiH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hiH, new Object[]{aqy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hiH, new Object[]{aqy}));
                break;
        }
        m12893c(aqy);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Actions")
    @C5566aOg
    /* renamed from: d */
    public void mo7892d(MissionAction cnVar) {
        switch (bFf().mo6893i(hiK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hiK, new Object[]{cnVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hiK, new Object[]{cnVar}));
                break;
        }
        m12894c(cnVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Cost")
    @C5566aOg
    /* renamed from: jH */
    public void mo7893jH(long j) {
        switch (bFf().mo6893i(hiP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hiP, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hiP, new Object[]{new Long(j)}));
                break;
        }
        m12897jG(j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ln */
    public MissionTemplateObjective mo7894ln(String str) {
        switch (bFf().mo6893i(hiQ)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplateObjective) bFf().mo5606d(new aCE(this, hiQ, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, hiQ, new Object[]{str}));
                break;
        }
        return m12898lm(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m12892au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to expire (s)")
    @C5566aOg
    /* renamed from: wQ */
    public void mo7895wQ(int i) {
        switch (bFf().mo6893i(hiN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hiN, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hiN, new Object[]{new Integer(i)}));
                break;
        }
        m12900wP(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "26da9aab9b7100057febdcbeead6d9b5", aum = 0)
    /* renamed from: au */
    private String m12892au() {
        return "[" + getHandle() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Objectives")
    @C0064Am(aul = "4ed75933a62098cd2d3fff14f54882ef", aum = 0)
    private List<MissionTemplateObjective> cKa() {
        return Collections.unmodifiableList(azc());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Actions")
    @C0064Am(aul = "806c1e172170ad055a18e389dc45935c", aum = 0)
    private List<MissionAction> cKc() {
        return Collections.unmodifiableList(cJX());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to expire (s)")
    @C0064Am(aul = "cde790f4315d0222a2d8d7e2beec7320", aum = 0)
    private int cKe() {
        return cJY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Cost")
    @C0064Am(aul = "69136b6fe4d189f4ffc49b853e42b4a5", aum = 0)
    private long cKg() {
        return cJZ();
    }

    @C0064Am(aul = "5fa46086d25687832557a6d47cfb80cd", aum = 0)
    /* renamed from: lm */
    private MissionTemplateObjective m12898lm(String str) {
        if (str == null || "".equals(str)) {
            return null;
        }
        for (MissionTemplateObjective aqy : azc()) {
            if (str.equals(aqy.getHandle())) {
                return aqy;
            }
        }
        return null;
    }
}
