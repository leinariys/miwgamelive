package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.missiontemplate.scripting.BuyOutpostScriptTemplate;
import game.script.ship.Outpost;
import game.script.ship.OutpostActivationItem;
import game.script.ship.OutpostType;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3657tM;
import logic.data.mbean.aHR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;

@C6485anp
@C5511aMd
/* renamed from: a.Wm */
/* compiled from: a */
public class BuyOutpostMissionScript<T extends BuyOutpostScriptTemplate> extends MissionScript<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cyj = null;
    public static final C5663aRz izz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f2053yH = null;
    private static final long izx = 86400;
    private static final String izy = "RESERVATION_EXPIRING";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "65ff848687cf47b424628688828a40ba", aum = 0)
    private static Outpost hYU = null;
    @C0064Am(aul = "d54556d7b9b853dc7ada7092b0ce9481", aum = 1)
    private static C3438ra<OutpostActivationItem> hYV = null;

    static {
        m11319V();
    }

    public BuyOutpostMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BuyOutpostMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11319V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 2;
        _m_methodCount = MissionScript._m_methodCount + 1;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BuyOutpostMissionScript.class, "65ff848687cf47b424628688828a40ba", i);
        izz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BuyOutpostMissionScript.class, "d54556d7b9b853dc7ada7092b0ce9481", i2);
        cyj = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i4 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 1)];
        C2491fm a = C4105zY.m41624a(BuyOutpostMissionScript.class, "0c3db254c1c7374bcada7a4e1514aab6", i4);
        f2053yH = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BuyOutpostMissionScript.class, aHR.class, _m_fields, _m_methods);
    }

    /* renamed from: cx */
    private void m11322cx(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cyj, raVar);
    }

    /* access modifiers changed from: private */
    public Outpost dnC() {
        return (Outpost) bFf().mo5608dq().mo3214p(izz);
    }

    /* access modifiers changed from: private */
    public C3438ra dnD() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cyj);
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public void m11324h(Outpost qZVar) {
        bFf().mo5608dq().mo3197f(izz, qZVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aHR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m11325jF();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f2053yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2053yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2053yH, new Object[0]));
                break;
        }
        m11325jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "0c3db254c1c7374bcada7a4e1514aab6", aum = 0)
    /* renamed from: jF */
    private void m11325jF() {
        mo575p(StateA.class);
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.Wm$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C2491fm aLR = null;
        public static final C2491fm aLU = null;
        /* renamed from: aT */
        public static final C5663aRz f2054aT = null;
        public static final C2491fm beE = null;
        public static final C2491fm byQ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "a3cba93eeda4c45cdcf5cb7b209cf84d", aum = 0)
        static /* synthetic */ BuyOutpostMissionScript btd;

        static {
            m11334V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(BuyOutpostMissionScript wm) {
            super((C5540aNg) null);
            super._m_script_init(wm);
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m11334V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 4;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BuyOutpostMissionScript.StateA.class, "a3cba93eeda4c45cdcf5cb7b209cf84d", i);
            f2054aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
            C2491fm a = C4105zY.m41624a(BuyOutpostMissionScript.StateA.class, "2eb7a49ca7f5cc5ab80e7b207359d997", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BuyOutpostMissionScript.StateA.class, "a253b894f11df7e648d4c560427cd4bc", i4);
            aLR = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BuyOutpostMissionScript.StateA.class, "759814ad684c34e6e7524119c5e261f3", i5);
            aLU = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BuyOutpostMissionScript.StateA.class, "fa1c26aff6c02b639c023da0fc26835c", i6);
            byQ = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BuyOutpostMissionScript.StateA.class, C3657tM.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m11335a(BuyOutpostMissionScript wm) {
            bFf().mo5608dq().mo3197f(f2054aT, wm);
        }

        private BuyOutpostMissionScript bDo() {
            return (BuyOutpostMissionScript) bFf().mo5608dq().mo3214p(f2054aT);
        }

        /* renamed from: RM */
        public void mo6623RM() {
            switch (bFf().mo6893i(aLU)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                    break;
            }
            m11333RL();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3657tM(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m11336aV((String) args[0]);
                    return null;
                case 2:
                    m11333RL();
                    return null;
                case 3:
                    m11337cq((String) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: aW */
        public void mo6624aW(String str) {
            switch (bFf().mo6893i(aLR)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                    break;
            }
            m11336aV(str);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: cr */
        public void mo6626cr(String str) {
            switch (bFf().mo6893i(byQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    break;
            }
            m11337cq(str);
        }

        /* renamed from: b */
        public void mo6625b(BuyOutpostMissionScript wm) {
            m11335a(wm);
            super.mo10S();
        }

        @C0064Am(aul = "2eb7a49ca7f5cc5ab80e7b207359d997", aum = 0)
        private void aai() {
            Station cXi = bDo().cdr().cXi();
            if (!(cXi instanceof Outpost)) {
                bDo().fail();
                return;
            }
            bDo().m11324h((Outpost) cXi);
            OutpostType apo = ((BuyOutpostScriptTemplate) bDo().cdp()).apo();
            if (bDo().dnC().bXV() != Outpost.C3349b.FOR_SALE) {
                bDo().fail();
            } else if (bDo().dnC().bYh() != apo) {
                bDo().fail();
            } else if (!bDo().mo571eb(apo.mo3587rZ())) {
                bDo().fail();
            } else {
                bDo().dnC().mo21399m(bDo().cdr().bYd());
                bDo().mo101c(BuyOutpostMissionScript.izy, ((long) bDo().dnC().bYh().bex()) * BuyOutpostMissionScript.izx, 1);
                for (OutpostActivationItem uBVar : apo.bez()) {
                    OutpostActivationItem uBVar2 = (OutpostActivationItem) bFf().mo6865M(OutpostActivationItem.class);
                    uBVar2.mo10S();
                    uBVar2.setAmount(uBVar.getAmount());
                    uBVar2.mo22323c(uBVar.mo22322az());
                    uBVar2.mo22324eS(0);
                    bDo().dnD().add(uBVar2);
                }
            }
        }

        @C0064Am(aul = "a253b894f11df7e648d4c560427cd4bc", aum = 0)
        /* renamed from: aV */
        private void m11336aV(String str) {
            if (BuyOutpostMissionScript.izy.equals(str)) {
                bDo().dnC().bXZ();
                bDo().fail();
            }
        }

        @C0064Am(aul = "759814ad684c34e6e7524119c5e261f3", aum = 0)
        /* renamed from: RL */
        private void m11333RL() {
            bDo().dnC().bXZ();
        }

        @C0064Am(aul = "fa1c26aff6c02b639c023da0fc26835c", aum = 0)
        /* renamed from: cq */
        private void m11337cq(String str) {
            if (str.equals(((BuyOutpostScriptTemplate) bDo().cdp()).apq())) {
                Iterator it = bDo().dnD().iterator();
                while (it.hasNext()) {
                    OutpostActivationItem uBVar = (OutpostActivationItem) it.next();
                    int amount = uBVar.getAmount() - uBVar.ajr();
                    int f = bDo().cdr().mo9417f(uBVar.mo22322az(), amount);
                    uBVar.mo22324eS(uBVar.ajr() + f);
                    bDo().mo574k(uBVar.mo22322az().getHandle(), uBVar.ajr());
                    if (f == amount) {
                        bDo().mo566b(uBVar.mo22322az().getHandle(), Mission.C0015a.ACCOMPLISHED);
                        it.remove();
                    }
                }
                if (bDo().dnD().size() == 0) {
                    bDo().dnC().bXX();
                    bDo().cdk();
                }
            }
        }
    }
}
