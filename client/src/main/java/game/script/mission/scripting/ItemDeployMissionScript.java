package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C5512aMe;
import game.script.Actor;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.missiontemplate.scripting.ItemDeployMissionScriptTemplate;
import game.script.nls.NLSManager;
import game.script.nls.NLSSpeechActions;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C0860MU;
import logic.data.mbean.C5800aag;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

@C5829abJ("1.1.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.mission.ItemDeployMission")
@C6485anp
@C5511aMd
/* renamed from: a.CF */
/* compiled from: a */
public class ItemDeployMissionScript<T extends ItemDeployMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f275Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLU = null;
    public static final C2491fm byJ = null;
    public static final C5663aRz cyj = null;
    public static final C5663aRz cyl = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f276yH = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b0095fadbac0aebaf458babda6435727", aum = 0)
    private static C1556Wo<ItemType, Integer> cyi;
    @C0064Am(aul = "dac7d27ee9a97def08890fbcf5928a9a", aum = 1)
    private static C1556Wo<ItemType, Integer> cyk;

    static {
        m1485V();
    }

    public ItemDeployMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemDeployMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m1485V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 2;
        _m_methodCount = MissionScript._m_methodCount + 4;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ItemDeployMissionScript.class, "b0095fadbac0aebaf458babda6435727", i);
        cyj = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemDeployMissionScript.class, "dac7d27ee9a97def08890fbcf5928a9a", i2);
        cyl = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i4 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(ItemDeployMissionScript.class, "2ce5f635a2fb9f02cc477bbe9fd7e7e0", i4);
        aLU = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemDeployMissionScript.class, "9a92d27f38c7b8d69e09433042658827", i5);
        byJ = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemDeployMissionScript.class, "f76989d16919ee8785f47d616a232571", i6);
        f276yH = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemDeployMissionScript.class, "f052a619fae031443164f4a4d166c6af", i7);
        f275Do = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemDeployMissionScript.class, C0860MU.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m1482A(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cyj, wo);
    }

    /* renamed from: B */
    private void m1483B(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cyl, wo);
    }

    /* access modifiers changed from: private */
    public C1556Wo aDi() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cyj);
    }

    private C1556Wo aDj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cyl);
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m1484RL();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0860MU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m1484RL();
                return null;
            case 1:
                m1491p((Station) args[0]);
                return null;
            case 2:
                m1490jF();
                return null;
            case 3:
                m1489a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f275Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f275Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f275Do, new Object[]{jt}));
                break;
        }
        m1489a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f276yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f276yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f276yH, new Object[0]));
                break;
        }
        m1490jF();
    }

    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m1491p(bf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "2ce5f635a2fb9f02cc477bbe9fd7e7e0", aum = 0)
    /* renamed from: RL */
    private void m1484RL() {
        for (ItemTypeTableItem next : ((ItemDeployMissionScriptTemplate) cdp()).ajG()) {
            cdr().mo9417f(next.mo23385az(), next.getAmount());
            mo122f(((NLSSpeechActions) ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEy(), next.mo23385az().mo19891ke());
        }
    }

    @C0064Am(aul = "9a92d27f38c7b8d69e09433042658827", aum = 0)
    /* renamed from: p */
    private void m1491p(Station bf) {
        if (cdp() == null) {
            mo8358lY("Mission template not set in an instance of " + getClass().getSimpleName());
        } else if (bf == ((ItemDeployMissionScriptTemplate) cdp()).ajI()) {
            for (ItemType jCVar : new HashSet(aDi().keySet())) {
                Integer num = (Integer) aDi().get(jCVar);
                int f = cdr().mo9417f(jCVar, num.intValue());
                if (f != 0) {
                    Integer num2 = (Integer) aDj().get(jCVar);
                    if (num2 == null) {
                        num2 = new Integer(0);
                    }
                    Integer valueOf = Integer.valueOf(num2.intValue() + f);
                    Integer valueOf2 = Integer.valueOf(num.intValue() - f);
                    mo574k(jCVar.getHandle(), valueOf.intValue());
                    aDi().put(jCVar, valueOf2);
                    aDj().put(jCVar, valueOf);
                    if (valueOf2.intValue() == 0) {
                        aDi().remove(jCVar);
                        mo566b(jCVar.getHandle(), Mission.C0015a.ACCOMPLISHED);
                    }
                }
            }
            if (aDi().size() == 0) {
                cdk();
            }
        }
    }

    @C0064Am(aul = "f76989d16919ee8785f47d616a232571", aum = 0)
    /* renamed from: jF */
    private void m1490jF() {
        mo575p(StateA.class);
    }

    @C0064Am(aul = "f052a619fae031443164f4a4d166c6af", aum = 0)
    /* renamed from: a */
    private void m1489a(C0665JT jt) {
        if ("".equals(jt.getVersion())) {
            if (azm() == null) {
                mo576r(StateA.class);
            }
        } else if ("1.0.0".equals(jt.getVersion())) {
            C0665JT eE = jt.mo3113eE("missionTemplate");
            ItemType jCVar = (ItemType) ((C5512aMe) eE.get("deliveryItemType")).baw();
            int intValue = ((Integer) eE.get("deliveryQuantity")).intValue();
            int intValue2 = ((Integer) jt.get("cont")).intValue();
            if (jCVar != null) {
                aDi().put(jCVar, new Integer(intValue - intValue2));
                if (intValue2 > 0) {
                    aDj().put(jCVar, Integer.valueOf(intValue2));
                    return;
                }
                return;
            }
            mo8358lY("DeliverItemType null!!");
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.CF$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f277aT = null;
        public static final C2491fm beE = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "7be14e8df4ebe13620ffca8d1b2535e5", aum = 0)
        static /* synthetic */ ItemDeployMissionScript eSt;

        static {
            m1502V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateA(ItemDeployMissionScript cf) {
            super((C5540aNg) null);
            super._m_script_init(cf);
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m1502V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ItemDeployMissionScript.StateA.class, "7be14e8df4ebe13620ffca8d1b2535e5", i);
            f277aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(ItemDeployMissionScript.StateA.class, "e6b2cd5ecff33b3303f7a8e17cad1512", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ItemDeployMissionScript.StateA.class, C5800aag.class, _m_fields, _m_methods);
        }

        /* renamed from: b */
        private void m1503b(ItemDeployMissionScript cf) {
            bFf().mo5608dq().mo3197f(f277aT, cf);
        }

        private ItemDeployMissionScript cuU() {
            return (ItemDeployMissionScript) bFf().mo5608dq().mo3214p(f277aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5800aag(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public void mo946c(ItemDeployMissionScript cf) {
            m1503b(cf);
            super.mo10S();
        }

        @C0064Am(aul = "e6b2cd5ecff33b3303f7a8e17cad1512", aum = 0)
        private void aai() {
            for (ItemTypeTableItem next : ((ItemDeployMissionScriptTemplate) cuU().cdp()).ajG()) {
                ArrayList<ItemTypeTableItem> arrayList = new ArrayList<>();
                if (!cuU().cdr().mo9406b(next.mo23385az(), next.getAmount(), cuU())) {
                    for (ItemTypeTableItem zlVar : arrayList) {
                        cuU().cdr().mo9417f(zlVar.mo23385az(), zlVar.getAmount());
                    }
                    cuU().fail();
                } else {
                    arrayList.add(next);
                    cuU().mo122f(((NLSSpeechActions) cuU().ala().aIY().mo6310c(NLSManager.C1472a.SPEECHACTIONS)).bEA(), next.mo23385az().mo19891ke());
                }
            }
            cuU().aDi().clear();
            for (ItemTypeTableItem next2 : ((ItemDeployMissionScriptTemplate) cuU().cdp()).ajE()) {
                cuU().aDi().put(next2.mo23385az(), new Integer(next2.getAmount()));
            }
            cuU().mo72aV((Actor) ((ItemDeployMissionScriptTemplate) cuU().cdp()).ajI());
        }
    }
}
