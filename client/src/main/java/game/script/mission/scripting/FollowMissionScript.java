package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.WaypointHelper;
import game.script.missiontemplate.scripting.FollowMissionScriptTemplate;
import game.script.npc.NPC;
import game.script.ship.Ship;
import logic.baa.*;
import logic.data.mbean.C2128cK;
import logic.data.mbean.C3313qC;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.HashMap;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.mission.FollowMission")
@C6485anp
@C5511aMd
/* renamed from: a.jm */
/* compiled from: a */
public class FollowMissionScript<T extends FollowMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f8332Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aiM = null;
    public static final C5663aRz aiN = null;
    public static final C5663aRz aiO = null;
    public static final C5663aRz aiP = null;
    public static final C5663aRz aiQ = null;
    public static final C5663aRz aiR = null;
    public static final C5663aRz aiS = null;
    public static final C5663aRz aiT = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f8340yH = null;
    private static final String aiG = "wp_npc_spawn";
    private static final String aiH = "followed_npc_";
    private static final String aiI = "wp_npc_dest";
    private static final String aiJ = "wp_npc_attack_spot";
    private static final String aiK = "attacker_npc_";
    private static final String aiL = "secondary_npc_";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "e010b3dccd834ba842e64f8be594c12a", aum = 1)
    private static int counter;
    @C0064Am(aul = "2721a10e5e379849962f7e4108faff20", aum = 0)

    /* renamed from: vU */
    private static HashMap<String, NPC> f8333vU;
    @C0064Am(aul = "9f4eee3fdbb5f580235ca1cb80d1e4aa", aum = 2)

    /* renamed from: vV */
    private static int f8334vV;
    @C0064Am(aul = "ba674815d2581c8f81fd4bd08c55f321", aum = 3)

    /* renamed from: vW */
    private static Vec3d f8335vW;
    @C0064Am(aul = "2b6660085e592feab0d412f790c8925c", aum = 4)

    /* renamed from: vX */
    private static Vec3d f8336vX;
    @C0064Am(aul = "414b07f2b1e9b5e73c1c007d08953ced", aum = 5)

    /* renamed from: vY */
    private static Vec3d f8337vY;
    @C0064Am(aul = "5a2ae6b688c1b726f688b0c233787bb3", aum = 6)

    /* renamed from: vZ */
    private static int f8338vZ;
    @C0064Am(aul = "6bb31428ceaae180851d889e882d9035", aum = 7)

    /* renamed from: wa */
    private static C3438ra<NPC> f8339wa;

    static {
        m34137V();
    }

    public FollowMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FollowMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34137V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 8;
        _m_methodCount = MissionScript._m_methodCount + 2;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(FollowMissionScript.class, "2721a10e5e379849962f7e4108faff20", i);
        aiM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(FollowMissionScript.class, "e010b3dccd834ba842e64f8be594c12a", i2);
        aiN = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(FollowMissionScript.class, "9f4eee3fdbb5f580235ca1cb80d1e4aa", i3);
        aiO = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(FollowMissionScript.class, "ba674815d2581c8f81fd4bd08c55f321", i4);
        aiP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(FollowMissionScript.class, "2b6660085e592feab0d412f790c8925c", i5);
        aiQ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(FollowMissionScript.class, "414b07f2b1e9b5e73c1c007d08953ced", i6);
        aiR = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(FollowMissionScript.class, "5a2ae6b688c1b726f688b0c233787bb3", i7);
        aiS = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(FollowMissionScript.class, "6bb31428ceaae180851d889e882d9035", i8);
        aiT = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i10 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 2)];
        C2491fm a = C4105zY.m41624a(FollowMissionScript.class, "8cf739f23faf28cd98bac7b587f87753", i10);
        f8340yH = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(FollowMissionScript.class, "a22b9b0674f2068135e6c77c980ba497", i11);
        f8332Do = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FollowMissionScript.class, C2128cK.class, _m_fields, _m_methods);
    }

    /* access modifiers changed from: private */
    /* renamed from: ES */
    public HashMap m34129ES() {
        return (HashMap) bFf().mo5608dq().mo3214p(aiM);
    }

    /* access modifiers changed from: private */
    /* renamed from: ET */
    public int m34130ET() {
        return bFf().mo5608dq().mo3212n(aiN);
    }

    /* access modifiers changed from: private */
    /* renamed from: EU */
    public int m34131EU() {
        return bFf().mo5608dq().mo3212n(aiO);
    }

    /* access modifiers changed from: private */
    /* renamed from: EV */
    public Vec3d m34132EV() {
        return (Vec3d) bFf().mo5608dq().mo3214p(aiP);
    }

    /* access modifiers changed from: private */
    /* renamed from: EW */
    public Vec3d m34133EW() {
        return (Vec3d) bFf().mo5608dq().mo3214p(aiQ);
    }

    /* access modifiers changed from: private */
    /* renamed from: EX */
    public Vec3d m34134EX() {
        return (Vec3d) bFf().mo5608dq().mo3214p(aiR);
    }

    /* access modifiers changed from: private */
    /* renamed from: EY */
    public int m34135EY() {
        return bFf().mo5608dq().mo3212n(aiS);
    }

    /* access modifiers changed from: private */
    /* renamed from: EZ */
    public C3438ra m34136EZ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aiT);
    }

    /* renamed from: a */
    private void m34147a(HashMap hashMap) {
        bFf().mo5608dq().mo3197f(aiM, hashMap);
    }

    /* access modifiers changed from: private */
    /* renamed from: cc */
    public void m34154cc(int i) {
        bFf().mo5608dq().mo3183b(aiN, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: cd */
    public void m34155cd(int i) {
        bFf().mo5608dq().mo3183b(aiO, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: ce */
    public void m34156ce(int i) {
        bFf().mo5608dq().mo3183b(aiS, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public void m34163l(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(aiP, ajr);
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public void m34164m(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(aiQ, ajr);
    }

    /* access modifiers changed from: private */
    /* renamed from: n */
    public void m34165n(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(aiR, ajr);
    }

    /* renamed from: u */
    private void m34166u(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aiT, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2128cK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m34162jF();
                return null;
            case 1:
                m34142a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8332Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8332Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8332Do, new Object[]{jt}));
                break;
        }
        m34142a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f8340yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8340yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8340yH, new Object[0]));
                break;
        }
        m34162jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m34147a(new HashMap());
    }

    @C0064Am(aul = "8cf739f23faf28cd98bac7b587f87753", aum = 0)
    /* renamed from: jF */
    private void m34162jF() {
        m34155cd(0);
        mo575p(StateA.class);
    }

    @C0064Am(aul = "a22b9b0674f2068135e6c77c980ba497", aum = 0)
    /* renamed from: a */
    private void m34142a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && azm() == null) {
            mo576r(StateA.class);
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.jm$a */
    class StateA extends MissionScriptState implements C1616Xf {
        public static final C2491fm _f_tick_0020_0028F_0029V = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8341aT = null;
        public static final C2491fm beE = null;
        public static final C2491fm beF = null;
        public static final C2491fm beG = null;
        public static final C2491fm byH = null;
        public static final C2491fm gCr = null;
        public static final C2491fm gCs = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "51764bdd5c103f25870cd515bfe4fe92", aum = 0)
        static /* synthetic */ FollowMissionScript aWa;

        static {
            m34176V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        StateA(FollowMissionScript jmVar) {
            super((C5540aNg) null);
            super._m_script_init(jmVar);
        }

        /* renamed from: V */
        static void m34176V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 7;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(FollowMissionScript.StateA.class, "51764bdd5c103f25870cd515bfe4fe92", i);
            f8341aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
            C2491fm a = C4105zY.m41624a(FollowMissionScript.StateA.class, "4b9e9f28d630f7a74154342027b21f03", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(FollowMissionScript.StateA.class, "3efc42982b843995e5128111f3b35b07", i4);
            gCr = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(FollowMissionScript.StateA.class, "d7eddd9ea28977fbcf72ac2488134d73", i5);
            beF = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(FollowMissionScript.StateA.class, "a3aa75c269d60d28e8a1e2fc6e04caa3", i6);
            byH = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(FollowMissionScript.StateA.class, "d52f9aa786dba257fc000648d35dd039", i7);
            gCs = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(FollowMissionScript.StateA.class, "8f1b782a12d9cdac38652b0ba1549436", i8);
            beG = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(FollowMissionScript.StateA.class, "e57996e69e3a328eeba769e943f587d3", i9);
            _f_tick_0020_0028F_0029V = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(FollowMissionScript.StateA.class, C3313qC.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "d7eddd9ea28977fbcf72ac2488134d73", aum = 0)
        @C5566aOg
        /* renamed from: bk */
        private boolean m34178bk(String str) {
            throw new aWi(new aCE(this, beF, new Object[]{str}));
        }

        private FollowMissionScript cwM() {
            return (FollowMissionScript) bFf().mo5608dq().mo3214p(f8341aT);
        }

        @C0064Am(aul = "3efc42982b843995e5128111f3b35b07", aum = 0)
        @C5566aOg
        private void cwN() {
            throw new aWi(new aCE(this, gCr, new Object[0]));
        }

        @C5566aOg
        private void cwO() {
            switch (bFf().mo6893i(gCr)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gCr, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gCr, new Object[0]));
                    break;
            }
            cwN();
        }

        private void cwQ() {
            switch (bFf().mo6893i(gCs)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gCs, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gCs, new Object[0]));
                    break;
            }
            cwP();
        }

        /* renamed from: i */
        private void m34179i(FollowMissionScript jmVar) {
            bFf().mo5608dq().mo3197f(f8341aT, jmVar);
        }

        @C0064Am(aul = "a3aa75c269d60d28e8a1e2fc6e04caa3", aum = 0)
        @C5566aOg
        /* renamed from: s */
        private void m34180s(String str, String str2) {
            throw new aWi(new aCE(this, byH, new Object[]{str, str2}));
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: V */
        public void mo4709V(float f) {
            switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                    break;
            }
            m34175U(f);
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3313qC(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    cwN();
                    return null;
                case 2:
                    return new Boolean(m34178bk((String) args[0]));
                case 3:
                    m34180s((String) args[0], (String) args[1]);
                    return null;
                case 4:
                    cwP();
                    return null;
                case 5:
                    m34177a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                case 6:
                    m34175U(((Float) args[0]).floatValue());
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m34177a(str, fAVar, fAVar2);
        }

        @C5566aOg
        /* renamed from: bl */
        public boolean mo4962bl(String str) {
            switch (bFf().mo6893i(beF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                    break;
            }
            return m34178bk(str);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        @C5566aOg
        /* renamed from: t */
        public void mo12268t(String str, String str2) {
            switch (bFf().mo6893i(byH)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                    break;
            }
            m34180s(str, str2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: j */
        public void mo19987j(FollowMissionScript jmVar) {
            m34179i(jmVar);
            super.mo10S();
        }

        @C0064Am(aul = "4b9e9f28d630f7a74154342027b21f03", aum = 0)
        private void aai() {
            cwM().cdp();
            cwO();
        }

        @C0064Am(aul = "d52f9aa786dba257fc000648d35dd039", aum = 0)
        private void cwP() {
            WaypointDat cIt = ((FollowMissionScriptTemplate) cwM().cdp()).cIt();
            WaypointHelper.m2400a(cIt, cwM().m34133EW());
            cwM().mo92b(FollowMissionScript.aiJ, WaypointHelper.m2400a(cIt, (Vec3d) null), ((FollowMissionScriptTemplate) cwM().cdp()).mo7603Nc(), cIt.aMJ(), false, cIt.alW());
        }

        @C0064Am(aul = "8f1b782a12d9cdac38652b0ba1549436", aum = 0)
        /* renamed from: a */
        private void m34177a(String str, Ship fAVar, Ship fAVar2) {
            if (!str.startsWith(FollowMissionScript.aiL)) {
                if (str.contains(FollowMissionScript.aiH)) {
                    cwM().fail();
                }
                if (str.equals(FollowMissionScript.aiK)) {
                    FollowMissionScript cwM = cwM();
                    cwM.m34155cd(cwM.m34131EU() + 1);
                    if (cwM().m34131EU() == cwM().m34135EY()) {
                        cwM().mo128iZ(FollowMissionScript.aiJ);
                        cwM().mo128iZ(FollowMissionScript.aiI);
                        cwM().cdk();
                    }
                }
            }
        }

        @C0064Am(aul = "e57996e69e3a328eeba769e943f587d3", aum = 0)
        /* renamed from: U */
        private void m34175U(float f) {
            for (NPC bQx : cwM().m34136EZ()) {
                bQx.bQx().mo7599de(cVr());
            }
        }
    }
}
