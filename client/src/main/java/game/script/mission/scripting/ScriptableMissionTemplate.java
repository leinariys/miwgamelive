package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.mission.Mission;
import game.script.mission.MissionTemplate;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1154Qv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.awi  reason: case insensitive filesystem */
/* compiled from: a */
public class ScriptableMissionTemplate extends MissionTemplate implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cnJ = null;
    public static final C5663aRz cnp = null;
    public static final C5663aRz eOW = null;
    public static final C2491fm eOZ = null;
    public static final C2491fm ePa = null;
    public static final C5663aRz gLI = null;
    public static final C2491fm gLJ = null;
    public static final C2491fm gLK = null;
    public static final C2491fm gLL = null;
    public static final C2491fm gLM = null;
    public static final C2491fm gLN = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f5523xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fa819363a7af88bd0d5112dee297bea9", aum = 2)
    private static I18NStringTable bdX;
    @C0064Am(aul = "313f088aa57f3d6376f8d747de1fcf83", aum = 0)
    private static C3438ra<ScriptableMissionTemplateObjective> cno;
    @C0064Am(aul = "0c57b278ed0839073a44179dda3e044b", aum = 1)
    private static String dWf;

    static {
        m26916V();
    }

    public ScriptableMissionTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScriptableMissionTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26916V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionTemplate._m_fieldCount + 3;
        _m_methodCount = MissionTemplate._m_methodCount + 10;
        int i = MissionTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ScriptableMissionTemplate.class, "313f088aa57f3d6376f8d747de1fcf83", i);
        cnp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ScriptableMissionTemplate.class, "0c57b278ed0839073a44179dda3e044b", i2);
        gLI = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ScriptableMissionTemplate.class, "fa819363a7af88bd0d5112dee297bea9", i3);
        eOW = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionTemplate._m_fields, (Object[]) _m_fields);
        int i5 = MissionTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 10)];
        C2491fm a = C4105zY.m41624a(ScriptableMissionTemplate.class, "ce3d8d1bad003264c5aaa9b76ae06187", i5);
        gLJ = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ScriptableMissionTemplate.class, "27236091a2db0234a4faadfcca30a80d", i6);
        gLK = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ScriptableMissionTemplate.class, "62dd15dddb08d01cb8e27b9bebff7474", i7);
        cnJ = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ScriptableMissionTemplate.class, "e76a72986b1ed88cf3e1ff8d951d2f89", i8);
        gLL = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ScriptableMissionTemplate.class, "0380472986f06a949863d6f9a4b3551d", i9);
        gLM = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ScriptableMissionTemplate.class, "ea0ad252976de799e1256ceaee6feaba", i10);
        ePa = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ScriptableMissionTemplate.class, "7d8d28a2bdb9b8ee4dbb72b89d962bc1", i11);
        eOZ = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(ScriptableMissionTemplate.class, "9370c02b39c9835d81e57e05f8fb0199", i12);
        gLN = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(ScriptableMissionTemplate.class, "9a7e6faadcecc55f630f98f50e18cfae", i13);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(ScriptableMissionTemplate.class, "83ad26d29ad7ec0f12d7608c31392387", i14);
        f5523xF = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScriptableMissionTemplate.class, C1154Qv.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Objectives")
    @C0064Am(aul = "ce3d8d1bad003264c5aaa9b76ae06187", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m26917a(ScriptableMissionTemplateObjective nx) {
        throw new aWi(new aCE(this, gLJ, new Object[]{nx}));
    }

    /* renamed from: a */
    private void m26918a(I18NStringTable wz) {
        bFf().mo5608dq().mo3197f(eOW, wz);
    }

    /* renamed from: ad */
    private void m26919ad(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnp, raVar);
    }

    private C3438ra azc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnp);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "String Table")
    @C0064Am(aul = "ea0ad252976de799e1256ceaee6feaba", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m26921b(I18NStringTable wz) {
        throw new aWi(new aCE(this, ePa, new Object[]{wz}));
    }

    private I18NStringTable bIQ() {
        return (I18NStringTable) bFf().mo5608dq().mo3214p(eOW);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Objectives")
    @C0064Am(aul = "27236091a2db0234a4faadfcca30a80d", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m26922c(ScriptableMissionTemplateObjective nx) {
        throw new aWi(new aCE(this, gLK, new Object[]{nx}));
    }

    private String cAQ() {
        return (String) bFf().mo5608dq().mo3214p(gLI);
    }

    @C0064Am(aul = "83ad26d29ad7ec0f12d7608c31392387", aum = 0)
    /* renamed from: iR */
    private Mission m26923iR() {
        return cAU();
    }

    /* renamed from: ko */
    private void m26924ko(String str) {
        bFf().mo5608dq().mo3197f(gLI, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYT = C3248pc.C3249a.MISSION_SCRIPT, displayName = "Mission Script")
    @C0064Am(aul = "e76a72986b1ed88cf3e1ff8d951d2f89", aum = 0)
    @C5566aOg
    /* renamed from: kp */
    private void m26925kp(String str) {
        throw new aWi(new aCE(this, gLL, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1154Qv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionTemplate._m_methodCount) {
            case 0:
                m26917a((ScriptableMissionTemplateObjective) args[0]);
                return null;
            case 1:
                m26922c((ScriptableMissionTemplateObjective) args[0]);
                return null;
            case 2:
                return azj();
            case 3:
                m26925kp((String) args[0]);
                return null;
            case 4:
                return cAR();
            case 5:
                m26921b((I18NStringTable) args[0]);
                return null;
            case 6:
                return bIT();
            case 7:
                return cAT();
            case 8:
                return m26920au();
            case 9:
                return m26923iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Objectives")
    public List<ScriptableMissionTemplateObjective> azk() {
        switch (bFf().mo6893i(cnJ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cnJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cnJ, new Object[0]));
                break;
        }
        return azj();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Objectives")
    @C5566aOg
    /* renamed from: b */
    public void mo16713b(ScriptableMissionTemplateObjective nx) {
        switch (bFf().mo6893i(gLJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gLJ, new Object[]{nx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gLJ, new Object[]{nx}));
                break;
        }
        m26917a(nx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "String Table")
    public I18NStringTable bIU() {
        switch (bFf().mo6893i(eOZ)) {
            case 0:
                return null;
            case 2:
                return (I18NStringTable) bFf().mo5606d(new aCE(this, eOZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eOZ, new Object[0]));
                break;
        }
        return bIT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "String Table")
    @C5566aOg
    /* renamed from: c */
    public void mo16715c(I18NStringTable wz) {
        switch (bFf().mo6893i(ePa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePa, new Object[]{wz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePa, new Object[]{wz}));
                break;
        }
        m26921b(wz);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Script")
    public String cAS() {
        switch (bFf().mo6893i(gLM)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gLM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gLM, new Object[0]));
                break;
        }
        return cAR();
    }

    public ScriptableMission cAU() {
        switch (bFf().mo6893i(gLN)) {
            case 0:
                return null;
            case 2:
                return (ScriptableMission) bFf().mo5606d(new aCE(this, gLN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gLN, new Object[0]));
                break;
        }
        return cAT();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Objectives")
    @C5566aOg
    /* renamed from: d */
    public void mo16718d(ScriptableMissionTemplateObjective nx) {
        switch (bFf().mo6893i(gLK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gLK, new Object[]{nx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gLK, new Object[]{nx}));
                break;
        }
        m26922c(nx);
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f5523xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f5523xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5523xF, new Object[0]));
                break;
        }
        return m26923iR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYT = C3248pc.C3249a.MISSION_SCRIPT, displayName = "Mission Script")
    @C5566aOg
    /* renamed from: kq */
    public void mo16719kq(String str) {
        switch (bFf().mo6893i(gLL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gLL, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gLL, new Object[]{str}));
                break;
        }
        m26925kp(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m26920au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Objectives")
    @C0064Am(aul = "62dd15dddb08d01cb8e27b9bebff7474", aum = 0)
    private List<ScriptableMissionTemplateObjective> azj() {
        return Collections.unmodifiableList(azc());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Script")
    @C0064Am(aul = "0380472986f06a949863d6f9a4b3551d", aum = 0)
    private String cAR() {
        return cAQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "String Table")
    @C0064Am(aul = "7d8d28a2bdb9b8ee4dbb72b89d962bc1", aum = 0)
    private I18NStringTable bIT() {
        return bIQ();
    }

    @C0064Am(aul = "9370c02b39c9835d81e57e05f8fb0199", aum = 0)
    private ScriptableMission cAT() {
        ScriptableMission gEVar = (ScriptableMission) bFf().mo6865M(ScriptableMission.class);
        gEVar.mo10S();
        return gEVar;
    }

    @C0064Am(aul = "9a7e6faadcecc55f630f98f50e18cfae", aum = 0)
    /* renamed from: au */
    private String m26920au() {
        return "[" + getHandle() + "]";
    }
}
