package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.mission.Mission;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.WaypointHelper;
import game.script.missiontemplate.scripting.AssaultMissionScriptTemplate;
import game.script.missiontemplate.scripting.AssaultMissionWave;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0426Fu;
import logic.data.mbean.C0707KD;
import logic.data.mbean.C1160RA;
import logic.data.mbean.C7005ays;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@C6485anp
@C5511aMd
/* renamed from: a.xO */
/* compiled from: a */
public class AssaultMissionScript<T extends AssaultMissionScriptTemplate> extends MissionScript<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLU = null;
    public static final C5663aRz gFj = null;
    public static final C5663aRz haU = null;
    public static final C2491fm haV = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f9530yH = null;
    private static final String haQ = "assaulted_npc_handle";
    private static final String haR = "hostile_npc_handle";
    private static final String haS = "friendly_npc_handle";
    private static final String haT = "wave_timer_handle";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "dc3cfcaa11bfba2f1275036ededdaef5", aum = 0)
    private static Vec3d cCz = null;
    @C0064Am(aul = "ee0c0f836448be22e42bac1f5977af72", aum = 1)
    private static int cVv = 0;

    static {
        m40911V();
    }

    public AssaultMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AssaultMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40911V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 2;
        _m_methodCount = MissionScript._m_methodCount + 3;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(AssaultMissionScript.class, "dc3cfcaa11bfba2f1275036ededdaef5", i);
        gFj = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AssaultMissionScript.class, "ee0c0f836448be22e42bac1f5977af72", i2);
        haU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i4 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 3)];
        C2491fm a = C4105zY.m41624a(AssaultMissionScript.class, "38f4a8bc7f32f0ed29c40d5c3a55d84c", i4);
        aLU = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(AssaultMissionScript.class, "49d66ce8d498eb693e645fa4e48daa09", i5);
        haV = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(AssaultMissionScript.class, "8d19861d0e60428e00183c98372d88a7", i6);
        f9530yH = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AssaultMissionScript.class, C0426Fu.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "49d66ce8d498eb693e645fa4e48daa09", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private List<Character> m40912a(List<NPCType> list, int i, Vec3d ajr, float f, Vec3d ajr2, float f2, List<Character> list2, String str) {
        throw new aWi(new aCE(this, haV, new Object[]{list, new Integer(i), ajr, new Float(f), ajr2, new Float(f2), list2, str}));
    }

    /* access modifiers changed from: private */
    /* renamed from: af */
    public void m40915af(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(gFj, ajr);
    }

    /* access modifiers changed from: private */
    public Vec3d cEi() {
        return (Vec3d) bFf().mo5608dq().mo3214p(gFj);
    }

    private int cGv() {
        return bFf().mo5608dq().mo3212n(haU);
    }

    /* renamed from: wG */
    private void m40918wG(int i) {
        bFf().mo5608dq().mo3183b(haU, i);
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m40910RL();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0426Fu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m40910RL();
                return null;
            case 1:
                return m40912a((List) args[0], ((Integer) args[1]).intValue(), (Vec3d) args[2], ((Float) args[3]).floatValue(), (Vec3d) args[4], ((Float) args[5]).floatValue(), (List) args[6], (String) args[7]);
            case 2:
                m40917jF();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public List<Character> mo22909b(List<NPCType> list, int i, Vec3d ajr, float f, Vec3d ajr2, float f2, List<Character> list2, String str) {
        switch (bFf().mo6893i(haV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, haV, new Object[]{list, new Integer(i), ajr, new Float(f), ajr2, new Float(f2), list2, str}));
            case 3:
                bFf().mo5606d(new aCE(this, haV, new Object[]{list, new Integer(i), ajr, new Float(f), ajr2, new Float(f2), list2, str}));
                break;
        }
        return m40912a(list, i, ajr, f, ajr2, f2, list2, str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f9530yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9530yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9530yH, new Object[0]));
                break;
        }
        m40917jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "38f4a8bc7f32f0ed29c40d5c3a55d84c", aum = 0)
    /* renamed from: RL */
    private void m40910RL() {
        cdr().mo9427mm("WAYPOINT");
    }

    @C0064Am(aul = "8d19861d0e60428e00183c98372d88a7", aum = 0)
    /* renamed from: jF */
    private void m40917jF() {
        m40918wG(0);
        mo575p(StateA.class);
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.xO$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9531aT = null;
        public static final C2491fm beE = null;
        public static final C2491fm byQ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "9e0f115e6530c8fe5e501f06837af7d6", aum = 0)
        static /* synthetic */ AssaultMissionScript bHH;

        static {
            m40928V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        StateA(AssaultMissionScript xOVar) {
            super((C5540aNg) null);
            super._m_script_init(xOVar);
        }

        /* renamed from: V */
        static void m40928V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 2;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(AssaultMissionScript.StateA.class, "9e0f115e6530c8fe5e501f06837af7d6", i);
            f9531aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(AssaultMissionScript.StateA.class, "ea765a2dc00a0f7282475dd33cca2f55", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(AssaultMissionScript.StateA.class, "6fca6918262fac4033f9663147a08ce2", i4);
            byQ = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(AssaultMissionScript.StateA.class, C7005ays.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m40929a(AssaultMissionScript xOVar) {
            bFf().mo5608dq().mo3197f(f9531aT, xOVar);
        }

        private AssaultMissionScript aoI() {
            return (AssaultMissionScript) bFf().mo5608dq().mo3214p(f9531aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C7005ays(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m40930cq((String) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: cr */
        public void mo6626cr(String str) {
            switch (bFf().mo6893i(byQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    break;
            }
            m40930cq(str);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo22910b(AssaultMissionScript xOVar) {
            m40929a(xOVar);
            super.mo10S();
        }

        @C0064Am(aul = "ea765a2dc00a0f7282475dd33cca2f55", aum = 0)
        private void aai() {
            String ciD = ((AssaultMissionScriptTemplate) aoI().cdp()).ciD();
            if (ciD == null || "".equals(ciD)) {
                aoI().mo568dJ(AssaultMissionScriptTemplate.fZm);
                aoI().mo575p(StateB.class);
            }
        }

        @C0064Am(aul = "6fca6918262fac4033f9663147a08ce2", aum = 0)
        /* renamed from: cq */
        private void m40930cq(String str) {
            if (str.equals(((AssaultMissionScriptTemplate) aoI().cdp()).ciD())) {
                aoI().mo574k(AssaultMissionScriptTemplate.fZl, 1);
                aoI().mo566b(AssaultMissionScriptTemplate.fZl, Mission.C0015a.ACCOMPLISHED);
                aoI().mo575p(StateB.class);
            }
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.xO$c */
    /* compiled from: a */
    public class StateB extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9533aT = null;
        public static final C5663aRz bHZ = null;
        public static final C5663aRz bIb = null;
        public static final C5663aRz bId = null;
        public static final C2491fm bIe = null;
        public static final C2491fm beE = null;
        public static final C2491fm beF = null;
        public static final C2491fm byF = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "3c4d425997d1540c05bc0a9e0f489fbe", aum = 3)
        static /* synthetic */ AssaultMissionScript bHH;
        @C0064Am(aul = "8a677f4d701c183fd6ae9fc919aa5292", aum = 0)
        private static WaypointDat bHY;
        @C0064Am(aul = "024c25e609299a24d68749ba5c12c5e0", aum = 1)
        private static List<Character> bIa;
        @C0064Am(aul = "89f6576475ecb2d42f202c9af18e8050", aum = 2)
        private static List<Character> bIc;

        static {
            m40961V();
        }

        public StateB() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateB(C5540aNg ang) {
            super(ang);
        }

        StateB(AssaultMissionScript xOVar) {
            super((C5540aNg) null);
            super._m_script_init(xOVar);
        }

        /* renamed from: V */
        static void m40961V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 4;
            _m_methodCount = MissionScriptState._m_methodCount + 4;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 4)];
            C5663aRz b = C5640aRc.m17844b(AssaultMissionScript.StateB.class, "8a677f4d701c183fd6ae9fc919aa5292", i);
            bHZ = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(AssaultMissionScript.StateB.class, "024c25e609299a24d68749ba5c12c5e0", i2);
            bIb = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(AssaultMissionScript.StateB.class, "89f6576475ecb2d42f202c9af18e8050", i3);
            bId = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(AssaultMissionScript.StateB.class, "3c4d425997d1540c05bc0a9e0f489fbe", i4);
            f9533aT = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i6 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i6 + 4)];
            C2491fm a = C4105zY.m41624a(AssaultMissionScript.StateB.class, "0931161b92a5ab4b1d2d8e2227ee31ee", i6);
            beE = a;
            fmVarArr[i6] = a;
            int i7 = i6 + 1;
            C2491fm a2 = C4105zY.m41624a(AssaultMissionScript.StateB.class, "733f946606fe911ed170618bec4c4d6d", i7);
            beF = a2;
            fmVarArr[i7] = a2;
            int i8 = i7 + 1;
            C2491fm a3 = C4105zY.m41624a(AssaultMissionScript.StateB.class, "d6f1378c9c816d701b3dd52304e92fa9", i8);
            byF = a3;
            fmVarArr[i8] = a3;
            int i9 = i8 + 1;
            C2491fm a4 = C4105zY.m41624a(AssaultMissionScript.StateB.class, "cc53d96f43fd6e6f807fa129237164fc", i9);
            bIe = a4;
            fmVarArr[i9] = a4;
            int i10 = i9 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(AssaultMissionScript.StateB.class, C1160RA.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m40962a(AssaultMissionScript xOVar) {
            bFf().mo5608dq().mo3197f(f9533aT, xOVar);
        }

        private AssaultMissionScript aoI() {
            return (AssaultMissionScript) bFf().mo5608dq().mo3214p(f9533aT);
        }

        private WaypointDat aoQ() {
            return (WaypointDat) bFf().mo5608dq().mo3214p(bHZ);
        }

        private List aoR() {
            return (List) bFf().mo5608dq().mo3214p(bIb);
        }

        private List aoS() {
            return (List) bFf().mo5608dq().mo3214p(bId);
        }

        private void aoU() {
            switch (bFf().mo6893i(bIe)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, bIe, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, bIe, new Object[0]));
                    break;
            }
            aoT();
        }

        /* renamed from: f */
        private void m40965f(WaypointDat cDVar) {
            bFf().mo5608dq().mo3197f(bHZ, cDVar);
        }

        /* renamed from: l */
        private void m40966l(List list) {
            bFf().mo5608dq().mo3197f(bIb, list);
        }

        /* renamed from: m */
        private void m40967m(List list) {
            bFf().mo5608dq().mo3197f(bId, list);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1160RA(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    return new Boolean(m40964bk((String) args[0]));
                case 2:
                    m40963a((String) args[0], (Ship) args[1]);
                    return null;
                case 3:
                    aoT();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo12257b(String str, Ship fAVar) {
            switch (bFf().mo6893i(byF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                    break;
            }
            m40963a(str, fAVar);
        }

        /* renamed from: bl */
        public boolean mo4962bl(String str) {
            switch (bFf().mo6893i(beF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                    break;
            }
            return m40964bk(str);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo22912b(AssaultMissionScript xOVar) {
            m40962a(xOVar);
            super.mo10S();
        }

        @C0064Am(aul = "0931161b92a5ab4b1d2d8e2227ee31ee", aum = 0)
        private void aai() {
            List<WaypointDat> Ne = ((AssaultMissionScriptTemplate) aoI().cdp()).mo14863Ne();
            if (Ne == null || Ne.size() == 0) {
                aoI().mo568dJ(AssaultMissionScriptTemplate.fZn);
                aoI().mo575p(StateC.class);
                return;
            }
            aoU();
        }

        @C0064Am(aul = "733f946606fe911ed170618bec4c4d6d", aum = 0)
        /* renamed from: bk */
        private boolean m40964bk(String str) {
            ArrayList arrayList = new ArrayList();
            arrayList.add(aoI().cdr().bQx().agj());
            m40966l(aoI().mo22909b(aoQ().bvC(), aoQ().getAmount(), aoI().cEi(), (float) aoQ().aMJ(), aoI().cEi(), (float) aoQ().aMJ(), arrayList, AssaultMissionScript.haR));
            if (!((AssaultMissionScriptTemplate) aoI().cdp()).ciz()) {
                m40967m(aoI().mo22909b(aoQ().bvE(), aoQ().bvG(), aoI().cEi(), (float) aoQ().aMJ(), aoI().cEi(), (float) aoQ().aMJ(), aoR(), AssaultMissionScript.haS));
            }
            aoI().mo574k(AssaultMissionScriptTemplate.fZm, ((AssaultMissionScriptTemplate) aoI().cdp()).mo14863Ne().indexOf(aoQ()) + 1);
            if (aoR() != null && aoR().size() != 0) {
                return true;
            }
            aoU();
            return true;
        }

        @C0064Am(aul = "d6f1378c9c816d701b3dd52304e92fa9", aum = 0)
        /* renamed from: a */
        private void m40963a(String str, Ship fAVar) {
            NPC auf = (NPC) fAVar.agj();
            if (aoR().contains(auf)) {
                aoR().remove(auf);
                if (aoR().size() == 0) {
                    aoU();
                }
            }
        }

        @C0064Am(aul = "cc53d96f43fd6e6f807fa129237164fc", aum = 0)
        private void aoT() {
            List<WaypointDat> Ne = ((AssaultMissionScriptTemplate) aoI().cdp()).mo14863Ne();
            int indexOf = Ne.indexOf(aoQ()) + 1;
            if (indexOf >= Ne.size()) {
                aoI().mo574k(AssaultMissionScriptTemplate.fZm, Ne.size());
                aoI().mo566b(AssaultMissionScriptTemplate.fZm, Mission.C0015a.ACCOMPLISHED);
                aoI().mo568dJ(AssaultMissionScriptTemplate.fZn);
                aoI().mo575p(StateC.class);
                return;
            }
            m40965f(Ne.get(indexOf));
            aoI().m40915af(WaypointHelper.m2400a(aoQ(), aoI().cEi()));
            aoI().mo92b("assault_mission_waypoint", aoI().cEi(), ((AssaultMissionScriptTemplate) aoI().cdp()).mo14862Nc(), aoQ().aMJ(), true, aoQ().alW());
            if (((AssaultMissionScriptTemplate) aoI().cdp()).ciz()) {
                m40967m(aoI().mo22909b(aoQ().bvE(), aoQ().bvG(), aoI().cEi(), (float) aoQ().aMJ(), aoI().cEi(), (float) aoQ().aMJ(), (List<Character>) null, AssaultMissionScript.haS));
            }
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.xO$b */
    /* compiled from: a */
    public class StateC extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C2491fm aLR = null;
        /* renamed from: aT */
        public static final C5663aRz f9532aT = null;
        public static final C5663aRz bHJ = null;
        public static final C5663aRz bHL = null;
        public static final C5663aRz bHN = null;
        public static final C5663aRz bHP = null;
        public static final C5663aRz bHR = null;
        public static final C5663aRz bHT = null;
        public static final C5663aRz bHV = null;
        public static final C2491fm beE = null;
        public static final C2491fm beG = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "f8461afc84a28c2622183de0cde1a2e1", aum = 7)
        static /* synthetic */ AssaultMissionScript bHH;
        @C0064Am(aul = "3a35338d9b10916fceeb4fd867c1bb25", aum = 0)
        private static NPC bHI;
        @C0064Am(aul = "6ee3ffc95483e9e3ce52fe4391a6493d", aum = 1)
        private static WaypointDat bHK;
        @C0064Am(aul = "f375cb99377664139a51883a0f751317", aum = 2)
        private static AssaultMissionWave bHM;
        @C0064Am(aul = "68077e262a47266a471a09c7e2e5b471", aum = 3)
        private static C3438ra<Character> bHO;
        @C0064Am(aul = "ee34b160aa4f33e5ea6ede4194c400cb", aum = 4)
        private static C3438ra<Character> bHQ;
        @C0064Am(aul = "d05df105b03ddd860c3a01e47413a738", aum = 5)
        private static C3438ra<Character> bHS;
        @C0064Am(aul = "51a2dd48d530994e3ce5d7d3d200d9ca", aum = 6)
        private static C3438ra<Character> bHU;

        static {
            m40941V();
        }

        public StateC() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateC(C5540aNg ang) {
            super(ang);
        }

        StateC(AssaultMissionScript xOVar) {
            super((C5540aNg) null);
            super._m_script_init(xOVar);
        }

        /* renamed from: V */
        static void m40941V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 8;
            _m_methodCount = MissionScriptState._m_methodCount + 3;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 8)];
            C5663aRz b = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "3a35338d9b10916fceeb4fd867c1bb25", i);
            bHJ = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "6ee3ffc95483e9e3ce52fe4391a6493d", i2);
            bHL = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "f375cb99377664139a51883a0f751317", i3);
            bHN = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "68077e262a47266a471a09c7e2e5b471", i4);
            bHP = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            C5663aRz b5 = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "ee34b160aa4f33e5ea6ede4194c400cb", i5);
            bHR = b5;
            arzArr[i5] = b5;
            int i6 = i5 + 1;
            C5663aRz b6 = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "d05df105b03ddd860c3a01e47413a738", i6);
            bHT = b6;
            arzArr[i6] = b6;
            int i7 = i6 + 1;
            C5663aRz b7 = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "51a2dd48d530994e3ce5d7d3d200d9ca", i7);
            bHV = b7;
            arzArr[i7] = b7;
            int i8 = i7 + 1;
            C5663aRz b8 = C5640aRc.m17844b(AssaultMissionScript.StateC.class, "f8461afc84a28c2622183de0cde1a2e1", i8);
            f9532aT = b8;
            arzArr[i8] = b8;
            int i9 = i8 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i10 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i10 + 3)];
            C2491fm a = C4105zY.m41624a(AssaultMissionScript.StateC.class, "2f8dcf1d40db8ccec752a62a0d157fd9", i10);
            beE = a;
            fmVarArr[i10] = a;
            int i11 = i10 + 1;
            C2491fm a2 = C4105zY.m41624a(AssaultMissionScript.StateC.class, "f6e82ea9c65a7bcbf498ca8ecac6d2eb", i11);
            aLR = a2;
            fmVarArr[i11] = a2;
            int i12 = i11 + 1;
            C2491fm a3 = C4105zY.m41624a(AssaultMissionScript.StateC.class, "3bec9cfae5c687e97a3c78daaed35fba", i12);
            beG = a3;
            fmVarArr[i12] = a3;
            int i13 = i12 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(AssaultMissionScript.StateC.class, C0707KD.class, _m_fields, _m_methods);
        }

        /* renamed from: U */
        private void m40940U(C3438ra raVar) {
            bFf().mo5608dq().mo3197f(bHP, raVar);
        }

        /* renamed from: V */
        private void m40942V(C3438ra raVar) {
            bFf().mo5608dq().mo3197f(bHR, raVar);
        }

        /* renamed from: W */
        private void m40943W(C3438ra raVar) {
            bFf().mo5608dq().mo3197f(bHT, raVar);
        }

        /* renamed from: X */
        private void m40944X(C3438ra raVar) {
            bFf().mo5608dq().mo3197f(bHV, raVar);
        }

        /* renamed from: a */
        private void m40945a(AssaultMissionWave axg) {
            bFf().mo5608dq().mo3197f(bHN, axg);
        }

        /* renamed from: a */
        private void m40946a(AssaultMissionScript xOVar) {
            bFf().mo5608dq().mo3197f(f9532aT, xOVar);
        }

        private AssaultMissionScript aoI() {
            return (AssaultMissionScript) bFf().mo5608dq().mo3214p(f9532aT);
        }

        private NPC aoJ() {
            return (NPC) bFf().mo5608dq().mo3214p(bHJ);
        }

        private WaypointDat aoK() {
            return (WaypointDat) bFf().mo5608dq().mo3214p(bHL);
        }

        private AssaultMissionWave aoL() {
            return (AssaultMissionWave) bFf().mo5608dq().mo3214p(bHN);
        }

        private C3438ra aoM() {
            return (C3438ra) bFf().mo5608dq().mo3214p(bHP);
        }

        private C3438ra aoN() {
            return (C3438ra) bFf().mo5608dq().mo3214p(bHR);
        }

        private C3438ra aoO() {
            return (C3438ra) bFf().mo5608dq().mo3214p(bHT);
        }

        private C3438ra aoP() {
            return (C3438ra) bFf().mo5608dq().mo3214p(bHV);
        }

        /* renamed from: c */
        private void m40949c(NPC auf) {
            bFf().mo5608dq().mo3197f(bHJ, auf);
        }

        /* renamed from: e */
        private void m40950e(WaypointDat cDVar) {
            bFf().mo5608dq().mo3197f(bHL, cDVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0707KD(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m40948aV((String) args[0]);
                    return null;
                case 2:
                    m40947a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: aW */
        public void mo6624aW(String str) {
            switch (bFf().mo6893i(aLR)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                    break;
            }
            m40948aV(str);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m40947a(str, fAVar, fAVar2);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo22911b(AssaultMissionScript xOVar) {
            m40946a(xOVar);
            super.mo10S();
        }

        @C0064Am(aul = "2f8dcf1d40db8ccec752a62a0d157fd9", aum = 0)
        private void aai() {
            m40950e(((AssaultMissionScriptTemplate) aoI().cdp()).ciB());
            aoI().m40915af(WaypointHelper.m2400a(aoK(), aoI().cEi()));
            aoI().mo92b("assault_mission_waypoint", aoI().cEi(), ((AssaultMissionScriptTemplate) aoI().cdp()).mo14862Nc(), aoK().aMJ(), true, aoK().alW());
            m40949c(aoI().mo564b(AssaultMissionScript.haQ, ((AssaultMissionScriptTemplate) aoI().cdp()).ciF(), aoI().cEi(), (float) aoK().aMJ(), aoI().cEi(), (float) aoK().aMJ(), ((AssaultMissionScriptTemplate) aoI().cdp()).mo14862Nc(), (Ship) null));
            List<AssaultMissionWave> ciJ = ((AssaultMissionScriptTemplate) aoI().cdp()).ciJ();
            if (ciJ != null && ciJ.size() > 0) {
                m40945a(ciJ.get(0));
                aoI().mo101c(AssaultMissionScript.haT, aoL().cCT(), 1);
            }
        }

        @C0064Am(aul = "f6e82ea9c65a7bcbf498ca8ecac6d2eb", aum = 0)
        /* renamed from: aV */
        private void m40948aV(String str) {
            WaypointDat cCR = aoL().cCR();
            Vec3d a = WaypointHelper.m2400a(cCR, aoI().cEi());
            if (AssaultMissionScript.haT.equals(str)) {
                if (aoL().cCV() == AssaultMissionWave.C2007a.FRIEND_OFFENCE) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(aoJ());
                    aoN().addAll(aoI().mo22909b(cCR.bvC(), cCR.getAmount(), a, (float) cCR.aMJ(), aoI().cEi(), (float) aoK().aMJ(), arrayList, AssaultMissionScript.haS));
                } else if (aoL().cCV() == AssaultMissionWave.C2007a.FRIEND_DEFENCE) {
                    aoM().addAll(aoI().mo22909b(cCR.bvC(), cCR.getAmount(), a, (float) cCR.aMJ(), aoI().cEi(), (float) aoK().aMJ(), aoP(), AssaultMissionScript.haS));
                } else if (aoL().cCV() == AssaultMissionWave.C2007a.ENEMY_OFFENCE) {
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(aoI().cdr().bQx().agj());
                    aoP().addAll(aoI().mo22909b(cCR.bvC(), cCR.getAmount(), a, (float) cCR.aMJ(), aoI().cEi(), (float) aoK().aMJ(), arrayList2, AssaultMissionScript.haR));
                } else if (aoL().cCV() == AssaultMissionWave.C2007a.ENEMY_DEFENCE) {
                    aoO().addAll(aoI().mo22909b(cCR.bvC(), cCR.getAmount(), a, (float) cCR.aMJ(), aoI().cEi(), (float) aoK().aMJ(), aoN(), AssaultMissionScript.haS));
                }
                List<AssaultMissionWave> ciJ = ((AssaultMissionScriptTemplate) aoI().cdp()).ciJ();
                int indexOf = ciJ.indexOf(aoL()) + 1;
                if (indexOf < ciJ.size()) {
                    m40945a(ciJ.get(indexOf));
                    aoI().mo101c(AssaultMissionScript.haT, aoL().cCT(), 1);
                }
            }
        }

        @C0064Am(aul = "3bec9cfae5c687e97a3c78daaed35fba", aum = 0)
        /* renamed from: a */
        private void m40947a(String str, Ship fAVar, Ship fAVar2) {
            if (AssaultMissionScript.haQ.equals(str)) {
                aoI().mo574k(AssaultMissionScriptTemplate.fZn, 1);
                aoI().mo566b(AssaultMissionScriptTemplate.fZn, Mission.C0015a.ACCOMPLISHED);
                aoI().cdk();
            }
        }
    }
}
