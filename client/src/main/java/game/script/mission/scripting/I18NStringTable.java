package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.*;
import logic.data.mbean.C2635hq;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.Wz */
/* compiled from: a */
public class I18NStringTable extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f2065bL = null;
    /* renamed from: bM */
    public static final C5663aRz f2066bM = null;
    /* renamed from: bN */
    public static final C2491fm f2067bN = null;
    /* renamed from: bO */
    public static final C2491fm f2068bO = null;
    /* renamed from: bP */
    public static final C2491fm f2069bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2070bQ = null;
    public static final C5663aRz exX = null;
    public static final C2491fm exY = null;
    public static final C2491fm exZ = null;
    public static final C2491fm eya = null;
    public static final C2491fm eyb = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7d6b3dc563451c9d4948dcfc0777c4e1", aum = 1)

    /* renamed from: Uo */
    private static C3438ra<I18NStringTableEntry> f2063Uo;
    @C0064Am(aul = "988852520c4c79116be474a22eeb90e7", aum = 2)

    /* renamed from: bK */
    private static UUID f2064bK;
    @C0064Am(aul = "3df275f372343348d1b6d0824aad0124", aum = 0)
    private static String handle;

    static {
        m11379V();
    }

    public I18NStringTable() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public I18NStringTable(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11379V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(I18NStringTable.class, "3df275f372343348d1b6d0824aad0124", i);
        f2066bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(I18NStringTable.class, "7d6b3dc563451c9d4948dcfc0777c4e1", i2);
        exX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(I18NStringTable.class, "988852520c4c79116be474a22eeb90e7", i3);
        f2065bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 9)];
        C2491fm a = C4105zY.m41624a(I18NStringTable.class, "675e37db91d129f630fb47b010f44fc7", i5);
        f2067bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(I18NStringTable.class, "3294c1ba882664f7f1858c3195d5ce2b", i6);
        f2068bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(I18NStringTable.class, "64a5d8c17b87f2a3f73e4fb0ad3276c3", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(I18NStringTable.class, "1b4d69d9ecaf176dffc4e9a9d55c63d0", i8);
        f2069bP = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(I18NStringTable.class, "5dcd181f2bc8bf396904c01d7a289343", i9);
        f2070bQ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(I18NStringTable.class, "9b7be67f63337bd900f3a6475e420722", i10);
        exY = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(I18NStringTable.class, "4ac8b476e66fb64a6b1ca4d2bbfa4b52", i11);
        exZ = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(I18NStringTable.class, "2f7523152f9d23b124588bb114049ab3", i12);
        eya = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(I18NStringTable.class, "4566d82ad87564d49aad7d57f094b2f6", i13);
        eyb = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(I18NStringTable.class, C2635hq.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m11381a(String str) {
        bFf().mo5608dq().mo3197f(f2066bM, str);
    }

    /* renamed from: a */
    private void m11382a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2065bL, uuid);
    }

    /* renamed from: an */
    private UUID m11383an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2065bL);
    }

    /* renamed from: ao */
    private String m11384ao() {
        return (String) bFf().mo5608dq().mo3214p(f2066bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "5dcd181f2bc8bf396904c01d7a289343", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m11388b(String str) {
        throw new aWi(new aCE(this, f2070bQ, new Object[]{str}));
    }

    private C3438ra bDD() {
        return (C3438ra) bFf().mo5608dq().mo3214p(exX);
    }

    /* renamed from: bw */
    private void m11390bw(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(exX, raVar);
    }

    /* renamed from: c */
    private void m11392c(UUID uuid) {
        switch (bFf().mo6893i(f2068bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2068bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2068bO, new Object[]{uuid}));
                break;
        }
        m11389b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2635hq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m11385ap();
            case 1:
                m11389b((UUID) args[0]);
                return null;
            case 2:
                return m11387au();
            case 3:
                return m11386ar();
            case 4:
                m11388b((String) args[0]);
                return null;
            case 5:
                return bDE();
            case 6:
                m11380a((I18NStringTableEntry) args[0]);
                return null;
            case 7:
                m11391c((I18NStringTableEntry) args[0]);
                return null;
            case 8:
                return m11393hr((String) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2067bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2067bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2067bN, new Object[0]));
                break;
        }
        return m11385ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Message Table")
    /* renamed from: b */
    public void mo6665b(I18NStringTableEntry azy) {
        switch (bFf().mo6893i(exZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, exZ, new Object[]{azy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, exZ, new Object[]{azy}));
                break;
        }
        m11380a(azy);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Message Table")
    public List<I18NStringTableEntry> bDF() {
        switch (bFf().mo6893i(exY)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, exY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, exY, new Object[0]));
                break;
        }
        return bDE();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Message Table")
    /* renamed from: d */
    public void mo6667d(I18NStringTableEntry azy) {
        switch (bFf().mo6893i(eya)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eya, new Object[]{azy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eya, new Object[]{azy}));
                break;
        }
        m11391c(azy);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2069bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2069bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2069bP, new Object[0]));
                break;
        }
        return m11386ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2070bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2070bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2070bQ, new Object[]{str}));
                break;
        }
        m11388b(str);
    }

    /* renamed from: hs */
    public I18NString mo6668hs(String str) {
        switch (bFf().mo6893i(eyb)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, eyb, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, eyb, new Object[]{str}));
                break;
        }
        return m11393hr(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m11387au();
    }

    @C0064Am(aul = "675e37db91d129f630fb47b010f44fc7", aum = 0)
    /* renamed from: ap */
    private UUID m11385ap() {
        return m11383an();
    }

    @C0064Am(aul = "3294c1ba882664f7f1858c3195d5ce2b", aum = 0)
    /* renamed from: b */
    private void m11389b(UUID uuid) {
        m11382a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m11382a(UUID.randomUUID());
    }

    @C0064Am(aul = "64a5d8c17b87f2a3f73e4fb0ad3276c3", aum = 0)
    /* renamed from: au */
    private String m11387au() {
        return "I18NStringTable: [" + m11384ao() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "1b4d69d9ecaf176dffc4e9a9d55c63d0", aum = 0)
    /* renamed from: ar */
    private String m11386ar() {
        return m11384ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Message Table")
    @C0064Am(aul = "9b7be67f63337bd900f3a6475e420722", aum = 0)
    private List<I18NStringTableEntry> bDE() {
        return Collections.unmodifiableList(bDD());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Message Table")
    @C0064Am(aul = "4ac8b476e66fb64a6b1ca4d2bbfa4b52", aum = 0)
    /* renamed from: a */
    private void m11380a(I18NStringTableEntry azy) {
        bDD().add(azy);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Message Table")
    @C0064Am(aul = "2f7523152f9d23b124588bb114049ab3", aum = 0)
    /* renamed from: c */
    private void m11391c(I18NStringTableEntry azy) {
        bDD().remove(azy);
    }

    @C0064Am(aul = "4566d82ad87564d49aad7d57f094b2f6", aum = 0)
    /* renamed from: hr */
    private I18NString m11393hr(String str) {
        if (str == null) {
            return null;
        }
        for (I18NStringTableEntry azy : bDD()) {
            if (str.equals(azy.getKey())) {
                return azy.cHj();
            }
        }
        return null;
    }
}
