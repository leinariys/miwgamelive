package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Character;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.mission.MissionTrigger;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Gate;
import game.script.space.Loot;
import game.script.space.LootType;
import game.script.space.Node;
import logic.baa.*;
import logic.data.mbean.C2955mD;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aam  reason: case insensitive filesystem */
/* compiled from: a */
public class MissionScriptState extends aDJ implements C1616Xf {
    public static int _m_fieldCount = 0;
    public static C5663aRz[] _m_fields = null;
    public static int _m_methodCount = 0;
    public static C2491fm[] _m_methods = null;
    public static C2491fm aLR = null;
    public static C2491fm aLS = null;
    public static C2491fm aLU = null;
    public static C2491fm aLV = null;
    public static C2491fm aLW = null;
    public static C2491fm aLY = null;
    public static C2491fm aMe = null;
    public static C2491fm beE = null;
    public static C2491fm beF = null;
    public static C2491fm beG = null;
    public static C2491fm beH = null;
    public static C2491fm byC = null;
    public static C2491fm byD = null;
    public static C2491fm byE = null;
    public static C2491fm byF = null;
    public static C2491fm byG = null;
    public static C2491fm byH = null;
    public static C2491fm byI = null;
    public static C2491fm byJ = null;
    public static C2491fm byK = null;
    public static C2491fm byL = null;
    public static C2491fm byM = null;
    public static C2491fm byN = null;
    public static C2491fm byO = null;
    public static C2491fm byP = null;
    public static C2491fm byQ = null;
    public static C2491fm byR = null;
    public static C2491fm eTk = null;
    public static long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m19660V();
    }

    public MissionScriptState() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionScriptState(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19660V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 0;
        _m_methodCount = aDJ._m_methodCount + 28;
        _m_fields = new C5663aRz[(aDJ._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 28)];
        C2491fm a = C4105zY.m41624a(MissionScriptState.class, "a149b4aa62ab5d3294def89c99ed75d5", i);
        aLU = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(MissionScriptState.class, "f04c0c16485b8ffaced35feee6430578", i2);
        byI = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionScriptState.class, "c6a072fa3089409eb85575efdfbda2c3", i3);
        aLY = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionScriptState.class, "2c4ca6494a1fc038ad9577d6b33dce00", i4);
        beG = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionScriptState.class, "8fbefa35e355af76da002a8ce7fff541", i5);
        byJ = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionScriptState.class, "b6a577df335abb9ea5059e4d86d53f1e", i6);
        byK = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionScriptState.class, "07880379bf2ad06a7868c3781a14c7fe", i7);
        eTk = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionScriptState.class, "e792adeba125f8b340b8cf08f004214c", i8);
        byM = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionScriptState.class, "6edd1122da4d1b5b51ecb745e12253bc", i9);
        beH = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionScriptState.class, "d78205545c05f028a11d81d9c9308307", i10);
        byN = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionScriptState.class, "3ba2e426fc0caeacc131cc1137ff1f73", i11);
        byO = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(MissionScriptState.class, "aecb2ebe050c24401b68fdd7a6c68e1a", i12);
        byP = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(MissionScriptState.class, "c70616123a33f32390c66975d407f749", i13);
        byC = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        C2491fm a14 = C4105zY.m41624a(MissionScriptState.class, "60d769337f4d83f2d7e8f70b3f66ea9a", i14);
        byD = a14;
        fmVarArr[i14] = a14;
        int i15 = i14 + 1;
        C2491fm a15 = C4105zY.m41624a(MissionScriptState.class, "dd5edfbbc3bddf83b02bf76e1f9d5604", i15);
        byE = a15;
        fmVarArr[i15] = a15;
        int i16 = i15 + 1;
        C2491fm a16 = C4105zY.m41624a(MissionScriptState.class, "c3e8ea4291524c254539d871886d4e26", i16);
        byQ = a16;
        fmVarArr[i16] = a16;
        int i17 = i16 + 1;
        C2491fm a17 = C4105zY.m41624a(MissionScriptState.class, "6c466edbd8f960d800df4867016656f5", i17);
        aMe = a17;
        fmVarArr[i17] = a17;
        int i18 = i17 + 1;
        C2491fm a18 = C4105zY.m41624a(MissionScriptState.class, "68334b7a85bd80e61ae4a3627848e1aa", i18);
        byF = a18;
        fmVarArr[i18] = a18;
        int i19 = i18 + 1;
        C2491fm a19 = C4105zY.m41624a(MissionScriptState.class, "0716eb83715f202977c303a897c07b4f", i19);
        aLS = a19;
        fmVarArr[i19] = a19;
        int i20 = i19 + 1;
        C2491fm a20 = C4105zY.m41624a(MissionScriptState.class, "14caa3ffa920e98d48b6cab7716f4b15", i20);
        byG = a20;
        fmVarArr[i20] = a20;
        int i21 = i20 + 1;
        C2491fm a21 = C4105zY.m41624a(MissionScriptState.class, "b5f48650f908088f8e69471262fe4533", i21);
        aLR = a21;
        fmVarArr[i21] = a21;
        int i22 = i21 + 1;
        C2491fm a22 = C4105zY.m41624a(MissionScriptState.class, "382c30cd29c0525d1d59f61b08fef684", i22);
        beF = a22;
        fmVarArr[i22] = a22;
        int i23 = i22 + 1;
        C2491fm a23 = C4105zY.m41624a(MissionScriptState.class, "196d555e3db65491984fc5d50a4d4673", i23);
        byH = a23;
        fmVarArr[i23] = a23;
        int i24 = i23 + 1;
        C2491fm a24 = C4105zY.m41624a(MissionScriptState.class, "3bbc50cfd1484cbdd5c2d10ad25f76dd", i24);
        beE = a24;
        fmVarArr[i24] = a24;
        int i25 = i24 + 1;
        C2491fm a25 = C4105zY.m41624a(MissionScriptState.class, "a640b7b42947e5511ec45533a3f0ced3", i25);
        byR = a25;
        fmVarArr[i25] = a25;
        int i26 = i25 + 1;
        C2491fm a26 = C4105zY.m41624a(MissionScriptState.class, "b14b696ea54ca84009d29c4804b6b28e", i26);
        byL = a26;
        fmVarArr[i26] = a26;
        int i27 = i26 + 1;
        C2491fm a27 = C4105zY.m41624a(MissionScriptState.class, "c5be09d14c8f245544ec4512debd5a4f", i27);
        aLV = a27;
        fmVarArr[i27] = a27;
        int i28 = i27 + 1;
        C2491fm a28 = C4105zY.m41624a(MissionScriptState.class, "387ae7265d82d1f1f50ce4e7f2297ce4", i28);
        aLW = a28;
        fmVarArr[i28] = a28;
        int i29 = i28 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionScriptState.class, C2955mD.class, _m_fields, _m_methods);
    }

    /* renamed from: RM */
    public void mo6623RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m19659RL();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2955mD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m19659RL();
                return null;
            case 1:
                m19685w((Ship) args[0]);
                return null;
            case 2:
                m19661a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            case 3:
                m19668a((String) args[0], (Ship) args[1], (Ship) args[2]);
                return null;
            case 4:
                m19681p((Station) args[0]);
                return null;
            case 5:
                m19682r((Station) args[0]);
                return null;
            case 6:
                m19673c((String) args[0], (Station) args[1]);
                return null;
            case 7:
                m19679k((Gate) args[0]);
                return null;
            case 8:
                m19680n((Ship) args[0]);
                return null;
            case 9:
                m19684t((Station) args[0]);
                return null;
            case 10:
                m19663a((MissionTrigger) args[0]);
                return null;
            case 11:
                akJ();
                return null;
            case 12:
                m19666a((String) args[0], (Character) args[1]);
                return null;
            case 13:
                m19670b((Loot) args[0]);
                return null;
            case 14:
                m19664a((Character) args[0], (Loot) args[1]);
                return null;
            case 15:
                m19675cq((String) args[0]);
                return null;
            case 16:
                m19678i((Ship) args[0]);
                return null;
            case 17:
                m19667a((String) args[0], (Ship) args[1]);
                return null;
            case 18:
                m19677g((Ship) args[0]);
                return null;
            case 19:
                m19674c((String) args[0], (Ship) args[1]);
                return null;
            case 20:
                m19669aV((String) args[0]);
                return null;
            case 21:
                return new Boolean(m19671bk((String) args[0]));
            case 22:
                m19683s((String) args[0], (String) args[1]);
                return null;
            case 23:
                aai();
                return null;
            case 24:
                m19662a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 25:
                m19676d((Node) args[0]);
                return null;
            case 26:
                m19665a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 27:
                m19672c((Item) args[0], (ItemLocation) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aW */
    public void mo6624aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m19669aV(str);
    }

    public void aaj() {
        switch (bFf().mo6893i(beE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beE, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beE, new Object[0]));
                break;
        }
        aai();
    }

    public void akK() {
        switch (bFf().mo6893i(byP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                break;
        }
        akJ();
    }

    /* renamed from: b */
    public void mo12253b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m19661a(cr, acm);
    }

    /* renamed from: b */
    public void mo8416b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m19662a(ahc, jCVar, i);
    }

    /* renamed from: b */
    public void mo12254b(MissionTrigger aus) {
        switch (bFf().mo6893i(byO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                break;
        }
        m19663a(aus);
    }

    /* renamed from: b */
    public void mo12255b(Character acx, Loot ael) {
        switch (bFf().mo6893i(byE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                break;
        }
        m19664a(acx, ael);
    }

    /* renamed from: b */
    public void mo12256b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m19665a(auq, aag, aag2);
    }

    /* renamed from: b */
    public void mo8417b(String str, Character acx) {
        switch (bFf().mo6893i(byC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                break;
        }
        m19666a(str, acx);
    }

    /* renamed from: b */
    public void mo12257b(String str, Ship fAVar) {
        switch (bFf().mo6893i(byF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                break;
        }
        m19667a(str, fAVar);
    }

    /* renamed from: b */
    public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(beG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                break;
        }
        m19668a(str, fAVar, fAVar2);
    }

    /* renamed from: bl */
    public boolean mo4962bl(String str) {
        switch (bFf().mo6893i(beF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                break;
        }
        return m19671bk(str);
    }

    /* renamed from: c */
    public void mo12258c(Loot ael) {
        switch (bFf().mo6893i(byD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                break;
        }
        m19670b(ael);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cr */
    public void mo6626cr(String str) {
        switch (bFf().mo6893i(byQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                break;
        }
        m19675cq(str);
    }

    /* renamed from: d */
    public void mo12259d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m19672c(auq, aag);
    }

    /* renamed from: d */
    public void mo12260d(String str, Station bf) {
        switch (bFf().mo6893i(eTk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eTk, new Object[]{str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eTk, new Object[]{str, bf}));
                break;
        }
        m19673c(str, bf);
    }

    /* renamed from: d */
    public void mo12261d(String str, Ship fAVar) {
        switch (bFf().mo6893i(byG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                break;
        }
        m19674c(str, fAVar);
    }

    /* renamed from: e */
    public void mo12262e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m19676d(rPVar);
    }

    /* renamed from: h */
    public void mo12263h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m19677g(fAVar);
    }

    /* renamed from: j */
    public void mo12264j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m19678i(fAVar);
    }

    /* renamed from: l */
    public void mo12265l(Gate fFVar) {
        switch (bFf().mo6893i(byM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                break;
        }
        m19679k(fFVar);
    }

    /* renamed from: o */
    public void mo12266o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m19680n(fAVar);
    }

    /* renamed from: q */
    public void mo4971q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m19681p(bf);
    }

    /* renamed from: s */
    public void mo12267s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m19682r(bf);
    }

    /* renamed from: t */
    public void mo12268t(String str, String str2) {
        switch (bFf().mo6893i(byH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                break;
        }
        m19683s(str, str2);
    }

    /* renamed from: u */
    public void mo12269u(Station bf) {
        switch (bFf().mo6893i(byN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                break;
        }
        m19684t(bf);
    }

    /* renamed from: x */
    public void mo12270x(Ship fAVar) {
        switch (bFf().mo6893i(byI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                break;
        }
        m19685w(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a149b4aa62ab5d3294def89c99ed75d5", aum = 0)
    /* renamed from: RL */
    private void m19659RL() {
    }

    @C0064Am(aul = "f04c0c16485b8ffaced35feee6430578", aum = 0)
    /* renamed from: w */
    private void m19685w(Ship fAVar) {
    }

    @C0064Am(aul = "c6a072fa3089409eb85575efdfbda2c3", aum = 0)
    /* renamed from: a */
    private void m19661a(Actor cr, C5260aCm acm) {
    }

    @C0064Am(aul = "2c4ca6494a1fc038ad9577d6b33dce00", aum = 0)
    /* renamed from: a */
    private void m19668a(String str, Ship fAVar, Ship fAVar2) {
    }

    @C0064Am(aul = "8fbefa35e355af76da002a8ce7fff541", aum = 0)
    /* renamed from: p */
    private void m19681p(Station bf) {
    }

    @C0064Am(aul = "b6a577df335abb9ea5059e4d86d53f1e", aum = 0)
    /* renamed from: r */
    private void m19682r(Station bf) {
    }

    @C0064Am(aul = "07880379bf2ad06a7868c3781a14c7fe", aum = 0)
    /* renamed from: c */
    private void m19673c(String str, Station bf) {
    }

    @C0064Am(aul = "e792adeba125f8b340b8cf08f004214c", aum = 0)
    /* renamed from: k */
    private void m19679k(Gate fFVar) {
    }

    @C0064Am(aul = "6edd1122da4d1b5b51ecb745e12253bc", aum = 0)
    /* renamed from: n */
    private void m19680n(Ship fAVar) {
    }

    @C0064Am(aul = "d78205545c05f028a11d81d9c9308307", aum = 0)
    /* renamed from: t */
    private void m19684t(Station bf) {
    }

    @C0064Am(aul = "3ba2e426fc0caeacc131cc1137ff1f73", aum = 0)
    /* renamed from: a */
    private void m19663a(MissionTrigger aus) {
    }

    @C0064Am(aul = "aecb2ebe050c24401b68fdd7a6c68e1a", aum = 0)
    private void akJ() {
    }

    @C0064Am(aul = "c70616123a33f32390c66975d407f749", aum = 0)
    /* renamed from: a */
    private void m19666a(String str, Character acx) {
    }

    @C0064Am(aul = "60d769337f4d83f2d7e8f70b3f66ea9a", aum = 0)
    /* renamed from: b */
    private void m19670b(Loot ael) {
    }

    @C0064Am(aul = "dd5edfbbc3bddf83b02bf76e1f9d5604", aum = 0)
    /* renamed from: a */
    private void m19664a(Character acx, Loot ael) {
    }

    @C0064Am(aul = "c3e8ea4291524c254539d871886d4e26", aum = 0)
    /* renamed from: cq */
    private void m19675cq(String str) {
    }

    @C0064Am(aul = "6c466edbd8f960d800df4867016656f5", aum = 0)
    /* renamed from: i */
    private void m19678i(Ship fAVar) {
    }

    @C0064Am(aul = "68334b7a85bd80e61ae4a3627848e1aa", aum = 0)
    /* renamed from: a */
    private void m19667a(String str, Ship fAVar) {
    }

    @C0064Am(aul = "0716eb83715f202977c303a897c07b4f", aum = 0)
    /* renamed from: g */
    private void m19677g(Ship fAVar) {
    }

    @C0064Am(aul = "14caa3ffa920e98d48b6cab7716f4b15", aum = 0)
    /* renamed from: c */
    private void m19674c(String str, Ship fAVar) {
    }

    @C0064Am(aul = "b5f48650f908088f8e69471262fe4533", aum = 0)
    /* renamed from: aV */
    private void m19669aV(String str) {
    }

    @C0064Am(aul = "382c30cd29c0525d1d59f61b08fef684", aum = 0)
    /* renamed from: bk */
    private boolean m19671bk(String str) {
        return false;
    }

    @C0064Am(aul = "196d555e3db65491984fc5d50a4d4673", aum = 0)
    /* renamed from: s */
    private void m19683s(String str, String str2) {
    }

    @C0064Am(aul = "3bbc50cfd1484cbdd5c2d10ad25f76dd", aum = 0)
    private void aai() {
    }

    @C0064Am(aul = "a640b7b42947e5511ec45533a3f0ced3", aum = 0)
    /* renamed from: a */
    private void m19662a(LootType ahc, ItemType jCVar, int i) {
    }

    @C0064Am(aul = "b14b696ea54ca84009d29c4804b6b28e", aum = 0)
    /* renamed from: d */
    private void m19676d(Node rPVar) {
    }

    @C0064Am(aul = "c5be09d14c8f245544ec4512debd5a4f", aum = 0)
    /* renamed from: a */
    private void m19665a(Item auq, ItemLocation aag, ItemLocation aag2) {
    }

    @C0064Am(aul = "387ae7265d82d1f1f50ce4e7f2297ce4", aum = 0)
    /* renamed from: c */
    private void m19672c(Item auq, ItemLocation aag) {
    }
}
