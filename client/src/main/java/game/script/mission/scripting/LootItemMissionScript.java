package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.TableNPCSpawn;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.WaypointHelper;
import game.script.missiontemplate.scripting.LootItemMissionScriptTemplate;
import game.script.npc.NPCType;
import game.script.ship.Ship;
import game.script.space.LootType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.*;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;

@C6485anp
@C5511aMd
/* renamed from: a.aDZ */
/* compiled from: a */
public class LootItemMissionScript<T extends LootItemMissionScriptTemplate> extends MissionScript<T> implements C1616Xf {
    public static int _m_fieldCount = 0;
    public static C5663aRz[] _m_fields = null;
    public static int _m_methodCount = 0;
    public static C2491fm[] _m_methods = null;
    public static C5663aRz gPC = null;
    public static C5663aRz gPD = null;
    public static C5663aRz gPE = null;
    public static C5663aRz gPF = null;
    public static C5663aRz gPG = null;
    public static C5663aRz gPI = null;
    public static String gPz = "patrolMissionKilling";
    public static String hES = "patrolMissionWaypoint";
    public static long serialVersionUID = 0;
    /* renamed from: yH */
    public static C2491fm f2551yH = null;
    public static String gPA = "primarPatrolMissionCreepNpc";
    public static String gPB = "primaryPatrolMissionCreepNpc";
    public static String gPy = "lootMissionWaypoint";
    public static String hEQ = "lootMissionNpcListenLoot";
    public static String hER = "lootMissionCreepNpc";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "a00135834799d0a2978291a1d2eb76d6", aum = 0)
    private static int bCN = 0;
    @C0064Am(aul = "60fdbab44a205353a5de2aa8bccbe71d", aum = 3)
    private static boolean bCO = false;
    @C0064Am(aul = "d6991059bf43b03c0a172d4cfc69d876", aum = 5)
    private static int bCP = 0;
    @C0064Am(aul = "89821588f70b4139be3dcf9e1b326659", aum = 4)
    private static int bCQ = 0;
    @C0064Am(aul = "cf64021d3c037c7d3f129f12b1f5387d", aum = 1)
    private static int bCR = 0;
    @C0064Am(aul = "940da18ab8034ddbfb94ab65b33bc117", aum = 2)
    private static Vec3d bCT = null;

    static {
        m13614V();
    }

    public LootItemMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LootItemMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13614V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 6;
        _m_methodCount = MissionScript._m_methodCount + 1;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.class, "a00135834799d0a2978291a1d2eb76d6", i);
        gPC = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(LootItemMissionScript.class, "cf64021d3c037c7d3f129f12b1f5387d", i2);
        gPG = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(LootItemMissionScript.class, "940da18ab8034ddbfb94ab65b33bc117", i3);
        gPI = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(LootItemMissionScript.class, "60fdbab44a205353a5de2aa8bccbe71d", i4);
        gPD = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(LootItemMissionScript.class, "89821588f70b4139be3dcf9e1b326659", i5);
        gPF = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(LootItemMissionScript.class, "d6991059bf43b03c0a172d4cfc69d876", i6);
        gPE = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i8 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 1)];
        C2491fm a = C4105zY.m41624a(LootItemMissionScript.class, "7f7a855bae825709bca25f675be93abf", i8);
        f2551yH = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LootItemMissionScript.class, C6247ajL.class, _m_fields, _m_methods);
    }

    /* access modifiers changed from: private */
    /* renamed from: ae */
    public void m13621ae(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(gPI, ajr);
    }

    /* access modifiers changed from: private */
    public int cCA() {
        return bFf().mo5608dq().mo3212n(gPC);
    }

    /* access modifiers changed from: private */
    public boolean cCB() {
        return bFf().mo5608dq().mo3201h(gPD);
    }

    /* access modifiers changed from: private */
    public int cCC() {
        return bFf().mo5608dq().mo3212n(gPE);
    }

    /* access modifiers changed from: private */
    public int cCD() {
        return bFf().mo5608dq().mo3212n(gPF);
    }

    /* access modifiers changed from: private */
    public int cCE() {
        return bFf().mo5608dq().mo3212n(gPG);
    }

    /* access modifiers changed from: private */
    public Vec3d cCG() {
        return (Vec3d) bFf().mo5608dq().mo3214p(gPI);
    }

    /* access modifiers changed from: private */
    /* renamed from: hs */
    public void m13630hs(boolean z) {
        bFf().mo5608dq().mo3153a(gPD, z);
    }

    /* access modifiers changed from: private */
    /* renamed from: vO */
    public void m13632vO(int i) {
        bFf().mo5608dq().mo3183b(gPC, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: vP */
    public void m13633vP(int i) {
        bFf().mo5608dq().mo3183b(gPE, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: vQ */
    public void m13634vQ(int i) {
        bFf().mo5608dq().mo3183b(gPF, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: vR */
    public void m13635vR(int i) {
        bFf().mo5608dq().mo3183b(gPG, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6247ajL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m13631jF();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f2551yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2551yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2551yH, new Object[0]));
                break;
        }
        m13631jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "7f7a855bae825709bca25f675be93abf", aum = 0)
    /* renamed from: jF */
    private void m13631jF() {
        m13632vO(-1);
        m13634vQ(0);
        mo575p(State0.class);
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aDZ$f */
    /* compiled from: a */
    public static class State0 extends MissionScriptState implements C1616Xf {
        public static int _m_fieldCount = 0;
        public static C5663aRz[] _m_fields = null;
        public static int _m_methodCount = 0;
        public static C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static C5663aRz f2557aT = null;
        public static C2491fm beE = null;
        public static long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "93c2d489c23bfce9ab3420ad975987c5", aum = 0)
        static /* synthetic */ LootItemMissionScript awH;

        static {
            m13703V();
        }

        public State0() {
            super((C5540aNg) null);
            super.mo10S();
        }

        State0(LootItemMissionScript adz) {
            super((C5540aNg) null);
            super._m_script_init(adz);
        }

        public State0(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m13703V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.State0.class, "93c2d489c23bfce9ab3420ad975987c5", i);
            f2557aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(LootItemMissionScript.State0.class, "0c82ac10ebfc0f4cb2628c7c034c5cde", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(LootItemMissionScript.State0.class, C6228ais.class, _m_fields, _m_methods);
        }

        private LootItemMissionScript dpO() {
            return (LootItemMissionScript) bFf().mo5608dq().mo3214p(f2557aT);
        }

        /* renamed from: g */
        private void m13704g(LootItemMissionScript adz) {
            bFf().mo5608dq().mo3197f(f2557aT, adz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6228ais(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo8422h(LootItemMissionScript adz) {
            m13704g(adz);
            super.mo10S();
        }

        @C0064Am(aul = "0c82ac10ebfc0f4cb2628c7c034c5cde", aum = 0)
        private void aai() {
            if (dpO().cCA() >= 0) {
                dpO().mo128iZ("patrolMissionWaypoint" + dpO().cCA());
            }
            LootItemMissionScript dpO = dpO();
            dpO.m13632vO(dpO.cCA() + 1);
            if (dpO().cCA() == ((LootItemMissionScriptTemplate) dpO().cdp()).bsi().size()) {
                dpO().mo575p(StateA.class);
                return;
            }
            dpO().m13635vR(0);
            dpO().m13633vP(0);
            WaypointDat cDVar = ((LootItemMissionScriptTemplate) dpO().cdp()).bsi().get(dpO().cCA());
            if (dpO().cCG() == null) {
                dpO().m13621ae(cDVar.getPosition());
            } else {
                dpO().m13621ae(WaypointHelper.m2400a(cDVar, dpO().cCG()));
            }
            dpO().mo92b("patrolMissionWaypoint" + dpO().cCA(), dpO().cCG(), ((LootItemMissionScriptTemplate) dpO().cdp()).mo5178Nc(), cDVar.aMJ(), true, cDVar.alW());
            dpO().m13630hs(false);
            dpO().mo575p(State1.class);
        }
    }

    @C6485anp
    @C5511aMd
    static
            /* renamed from: a.aDZ$g */
            /* compiled from: a */
    class State1 extends MissionScriptState implements C1616Xf {
        public static int _m_fieldCount = 0;
        public static C5663aRz[] _m_fields = null;
        public static int _m_methodCount = 0;
        public static C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static C5663aRz f2558aT = null;
        public static C2491fm beF = null;
        public static C2491fm beG = null;
        public static long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "7a716c25a6f5feee0b16a0c1e9256bcc", aum = 0)
        static /* synthetic */ LootItemMissionScript awH;

        static {
            m13713V();
        }

        public State1() {
            super((C5540aNg) null);
            super.mo10S();
        }

        State1(LootItemMissionScript adz) {
            super((C5540aNg) null);
            super._m_script_init(adz);
        }

        public State1(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m13713V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 2;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.State1.class, "7a716c25a6f5feee0b16a0c1e9256bcc", i);
            f2558aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(LootItemMissionScript.State1.class, "c045f045b4580f4ed90333394a8801d4", i3);
            beF = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(LootItemMissionScript.State1.class, "c7bdd99b02088352ac3add555671217b", i4);
            beG = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(LootItemMissionScript.State1.class, C5525aMr.class, _m_fields, _m_methods);
        }

        private LootItemMissionScript dpO() {
            return (LootItemMissionScript) bFf().mo5608dq().mo3214p(f2558aT);
        }

        /* renamed from: g */
        private void m13716g(LootItemMissionScript adz) {
            bFf().mo5608dq().mo3197f(f2558aT, adz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5525aMr(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    return new Boolean(m13715bk((String) args[0]));
                case 1:
                    m13714a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m13714a(str, fAVar, fAVar2);
        }

        /* renamed from: bl */
        public boolean mo4962bl(String str) {
            switch (bFf().mo6893i(beF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                    break;
            }
            return m13715bk(str);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo8423h(LootItemMissionScript adz) {
            m13716g(adz);
            super.mo10S();
        }

        @C0064Am(aul = "c045f045b4580f4ed90333394a8801d4", aum = 0)
        /* renamed from: bk */
        private boolean m13715bk(String str) {
            if (!dpO().cCB() && ("patrolMissionWaypoint" + dpO().cCA()).equals(str)) {
                dpO().m13630hs(true);
                dpO().mo574k("patrolMissionWaypoint", dpO().cCA() + 1);
                if (dpO().cCA() + 1 >= ((LootItemMissionScriptTemplate) dpO().cdp()).bsi().size()) {
                    dpO().mo566b("patrolMissionWaypoint", Mission.C0015a.ACCOMPLISHED);
                    dpO().mo568dJ("patrolMissionKilling");
                }
                WaypointDat cDVar = ((LootItemMissionScriptTemplate) dpO().cdp()).bsi().get(dpO().cCA());
                TableNPCSpawn bvw = cDVar.bvw();
                if (bvw != null) {
                    for (int i = 0; i < cDVar.getAmount(); i++) {
                        for (NPCType b : bvw.bvC()) {
                            LootItemMissionScript dpO = dpO();
                            StringBuilder sb = new StringBuilder(LootItemMissionScript.gPA);
                            LootItemMissionScript dpO2 = dpO();
                            int d = dpO2.cCD();
                            dpO2.m13634vQ(d + 1);
                            dpO.mo563b(sb.append(d).toString(), b, dpO().cCG(), (float) cDVar.aMJ(), dpO().cCG(), (float) cDVar.aMJ(), ((LootItemMissionScriptTemplate) dpO().cdp()).mo5178Nc(), dpO().cdr().bQx().agj());
                            LootItemMissionScript dpO3 = dpO();
                            dpO3.m13635vR(dpO3.cCE() + 1);
                        }
                    }
                }
                TableNPCSpawn bvI = cDVar.bvI();
                if (bvI != null) {
                    for (int i2 = 0; i2 < cDVar.getAmount(); i2++) {
                        for (NPCType b2 : bvI.bvC()) {
                            LootItemMissionScript dpO4 = dpO();
                            StringBuilder sb2 = new StringBuilder(LootItemMissionScript.gPB);
                            LootItemMissionScript dpO5 = dpO();
                            int d2 = dpO5.cCD();
                            dpO5.m13634vQ(d2 + 1);
                            dpO4.mo563b(sb2.append(d2).toString(), b2, dpO().cCG(), (float) cDVar.aMJ(), dpO().cCG(), (float) cDVar.aMJ(), ((LootItemMissionScriptTemplate) dpO().cdp()).mo5178Nc(), dpO().cdr().bQx().agj());
                        }
                    }
                }
                if (dpO().cCE() == 0) {
                    dpO().mo575p(State0.class);
                }
            }
            return false;
        }

        @C0064Am(aul = "c7bdd99b02088352ac3add555671217b", aum = 0)
        /* renamed from: a */
        private void m13714a(String str, Ship fAVar, Ship fAVar2) {
            if (!str.startsWith(LootItemMissionScript.gPB) && str.startsWith(LootItemMissionScript.gPA)) {
                WaypointDat cDVar = ((LootItemMissionScriptTemplate) dpO().cdp()).bsi().get(dpO().cCA());
                LootItemMissionScript dpO = dpO();
                dpO.m13633vP(dpO.cCC() + 1);
                if (dpO().cCC() == dpO().cCE()) {
                    dpO().mo575p(State0.class);
                }
            }
        }
    }

    @C6485anp
    @C5511aMd
    static
            /* renamed from: a.aDZ$a */
    class StateA extends MissionScriptState implements C1616Xf {
        public static int _m_fieldCount = 0;
        public static C5663aRz[] _m_fields = null;
        public static int _m_methodCount = 0;
        public static C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static C5663aRz f2552aT = null;
        public static C2491fm beE = null;
        public static long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "cea97a5b014a3dbc4c0ef35733cfe769", aum = 0)
        static /* synthetic */ LootItemMissionScript awH;

        static {
            m13643V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateA(LootItemMissionScript adz) {
            super((C5540aNg) null);
            super._m_script_init(adz);
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m13643V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.StateA.class, "cea97a5b014a3dbc4c0ef35733cfe769", i);
            f2552aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(LootItemMissionScript.StateA.class, "7170f0b2e80096f06d08e164e771e172", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(LootItemMissionScript.StateA.class, C5494aLm.class, _m_fields, _m_methods);
        }

        private LootItemMissionScript dpO() {
            return (LootItemMissionScript) bFf().mo5608dq().mo3214p(f2552aT);
        }

        /* renamed from: g */
        private void m13644g(LootItemMissionScript adz) {
            bFf().mo5608dq().mo3197f(f2552aT, adz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5494aLm(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo8414h(LootItemMissionScript adz) {
            m13644g(adz);
            super.mo10S();
        }

        @C0064Am(aul = "7170f0b2e80096f06d08e164e771e172", aum = 0)
        private void aai() {
            WaypointDat brY = ((LootItemMissionScriptTemplate) dpO().cdp()).brY();
            dpO().mo92b(LootItemMissionScript.gPy, brY.getPosition(), ((LootItemMissionScriptTemplate) dpO().cdp()).mo5178Nc(), brY.aMJ(), true, brY.alW());
            if (((LootItemMissionScriptTemplate) dpO().cdp()).bsc()) {
                dpO().mo575p(StateC.class);
            } else {
                dpO().mo575p(StateB.class);
            }
        }
    }

    @C6485anp
    @C5511aMd
    static
            /* renamed from: a.aDZ$e */
            /* compiled from: a */
    class StateB extends MissionScriptState implements C1616Xf {
        public static int _m_fieldCount = 0;
        public static C5663aRz[] _m_fields = null;
        public static int _m_methodCount = 0;
        public static C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static C5663aRz f2556aT = null;
        public static C2491fm beF = null;
        public static long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "5496bafddd8baf7e78243425181cad62", aum = 0)
        static /* synthetic */ LootItemMissionScript awH;

        static {
            m13691V();
        }

        public StateB() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateB(LootItemMissionScript adz) {
            super((C5540aNg) null);
            super._m_script_init(adz);
        }

        public StateB(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m13691V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.StateB.class, "5496bafddd8baf7e78243425181cad62", i);
            f2556aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(LootItemMissionScript.StateB.class, "c60756d37d34a257690ca4e4153eed9b", i3);
            beF = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(LootItemMissionScript.StateB.class, C5955adf.class, _m_fields, _m_methods);
        }

        private LootItemMissionScript dpO() {
            return (LootItemMissionScript) bFf().mo5608dq().mo3214p(f2556aT);
        }

        /* renamed from: g */
        private void m13693g(LootItemMissionScript adz) {
            bFf().mo5608dq().mo3197f(f2556aT, adz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5955adf(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    return new Boolean(m13692bk((String) args[0]));
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: bl */
        public boolean mo4962bl(String str) {
            switch (bFf().mo6893i(beF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                    break;
            }
            return m13692bk(str);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo8421h(LootItemMissionScript adz) {
            m13693g(adz);
            super.mo10S();
        }

        @C0064Am(aul = "c60756d37d34a257690ca4e4153eed9b", aum = 0)
        /* renamed from: bk */
        private boolean m13692bk(String str) {
            if (!LootItemMissionScript.gPy.equals(str)) {
                return true;
            }
            dpO().mo575p(StateC.class);
            return false;
        }
    }

    @C6485anp
    @C5511aMd
    static
            /* renamed from: a.aDZ$d */
            /* compiled from: a */
    class StateC extends MissionScriptState implements C1616Xf {
        public static int _m_fieldCount = 0;
        public static C5663aRz[] _m_fields = null;
        public static int _m_methodCount = 0;
        public static C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static C5663aRz f2555aT = null;
        public static C2491fm beE = null;
        public static C2491fm beG = null;
        public static long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "e9927e3ce39c7a6dbb51f68c3a68995a", aum = 0)
        static /* synthetic */ LootItemMissionScript awH;

        static {
            m13679V();
        }

        public StateC() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateC(LootItemMissionScript adz) {
            super((C5540aNg) null);
            super._m_script_init(adz);
        }

        public StateC(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m13679V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 2;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.StateC.class, "e9927e3ce39c7a6dbb51f68c3a68995a", i);
            f2555aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(LootItemMissionScript.StateC.class, "54abc5c59faac4f4bba960e618d36543", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(LootItemMissionScript.StateC.class, "ab05654345a9e1ad5de2bf3bc3191bf8", i4);
            beG = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(LootItemMissionScript.StateC.class, C2906le.class, _m_fields, _m_methods);
        }

        private LootItemMissionScript dpO() {
            return (LootItemMissionScript) bFf().mo5608dq().mo3214p(f2555aT);
        }

        /* renamed from: g */
        private void m13681g(LootItemMissionScript adz) {
            bFf().mo5608dq().mo3197f(f2555aT, adz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2906le(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m13680a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m13680a(str, fAVar, fAVar2);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo8420h(LootItemMissionScript adz) {
            m13681g(adz);
            super.mo10S();
        }

        @C0064Am(aul = "54abc5c59faac4f4bba960e618d36543", aum = 0)
        private void aai() {
            int i = 0;
            dpO().mo574k(LootItemMissionScriptTemplate.ebh, 1);
            dpO().mo566b(LootItemMissionScriptTemplate.ebh, Mission.C0015a.ACCOMPLISHED);
            dpO().mo565b(LootItemMissionScript.hEQ, ((LootItemMissionScriptTemplate) dpO().cdp()).brW().bvA(), ((LootItemMissionScriptTemplate) dpO().cdp()).brY(), ((LootItemMissionScriptTemplate) dpO().cdp()).mo5178Nc());
            TableNPCSpawn bvw = ((LootItemMissionScriptTemplate) dpO().cdp()).brY().bvw();
            if (bvw != null) {
                int i2 = 0;
                while (true) {
                    int i3 = i;
                    if (i2 < ((LootItemMissionScriptTemplate) dpO().cdp()).brY().getAmount()) {
                        Iterator<NPCType> it = bvw.bvC().iterator();
                        while (true) {
                            i = i3;
                            if (!it.hasNext()) {
                                break;
                            }
                            i3 = i + 1;
                            dpO().mo565b(LootItemMissionScript.hER + i, it.next(), ((LootItemMissionScriptTemplate) dpO().cdp()).brY(), ((LootItemMissionScriptTemplate) dpO().cdp()).mo5178Nc());
                        }
                        i2++;
                    } else {
                        return;
                    }
                }
            }
        }

        @C0064Am(aul = "ab05654345a9e1ad5de2bf3bc3191bf8", aum = 0)
        /* renamed from: a */
        private void m13680a(String str, Ship fAVar, Ship fAVar2) {
            if (!LootItemMissionScript.hEQ.equals(str)) {
                return;
            }
            if (!dpO().mo559E(fAVar)) {
                dpO().fail();
            } else {
                dpO().mo575p(StateD.class);
            }
        }
    }

    @C6485anp
    @C5511aMd
    static
            /* renamed from: a.aDZ$c */
            /* compiled from: a */
    class StateD extends MissionScriptState implements C1616Xf {
        public static int _m_fieldCount = 0;
        public static C5663aRz[] _m_fields = null;
        public static int _m_methodCount = 0;
        public static C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static C5663aRz f2554aT = null;
        public static C2491fm beE = null;
        public static C2491fm byC = null;
        public static C2491fm byR = null;
        public static long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "102a7015c2ad78b009e9cedceeb036cd", aum = 0)
        static /* synthetic */ LootItemMissionScript awH;

        static {
            m13665V();
        }

        public StateD() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateD(LootItemMissionScript adz) {
            super((C5540aNg) null);
            super._m_script_init(adz);
        }

        public StateD(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m13665V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 3;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.StateD.class, "102a7015c2ad78b009e9cedceeb036cd", i);
            f2554aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
            C2491fm a = C4105zY.m41624a(LootItemMissionScript.StateD.class, "2211759c3224aa928f6707e4452dc171", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(LootItemMissionScript.StateD.class, "36f17e188a4c9445500abca200e95c3a", i4);
            byR = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(LootItemMissionScript.StateD.class, "e05f24229318fdcf9633e3db6e27a28a", i5);
            byC = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(LootItemMissionScript.StateD.class, C5579aOt.class, _m_fields, _m_methods);
        }

        private LootItemMissionScript dpO() {
            return (LootItemMissionScript) bFf().mo5608dq().mo3214p(f2554aT);
        }

        /* renamed from: g */
        private void m13668g(LootItemMissionScript adz) {
            bFf().mo5608dq().mo3197f(f2554aT, adz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5579aOt(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m13666a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                    return null;
                case 2:
                    m13667a((String) args[0], (Character) args[1]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: b */
        public void mo8416b(LootType ahc, ItemType jCVar, int i) {
            switch (bFf().mo6893i(byR)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                    break;
            }
            m13666a(ahc, jCVar, i);
        }

        /* renamed from: b */
        public void mo8417b(String str, Character acx) {
            switch (bFf().mo6893i(byC)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                    break;
            }
            m13667a(str, acx);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo8418h(LootItemMissionScript adz) {
            m13668g(adz);
            super.mo10S();
        }

        @C0064Am(aul = "2211759c3224aa928f6707e4452dc171", aum = 0)
        private void aai() {
            dpO().mo574k(LootItemMissionScriptTemplate.ebi, 1);
            dpO().mo566b(LootItemMissionScriptTemplate.ebi, Mission.C0015a.ACCOMPLISHED);
        }

        @C0064Am(aul = "36f17e188a4c9445500abca200e95c3a", aum = 0)
        /* renamed from: a */
        private void m13666a(LootType ahc, ItemType jCVar, int i) {
            if (jCVar == ((LootItemMissionScriptTemplate) dpO().cdp()).brU()) {
                dpO().mo575p(StateE.class);
            }
        }

        @C0064Am(aul = "e05f24229318fdcf9633e3db6e27a28a", aum = 0)
        /* renamed from: a */
        private void m13667a(String str, Character acx) {
            if (LootItemMissionScript.hEQ.equals(str) && !dpO().mo577r(((LootItemMissionScriptTemplate) dpO().cdp()).brU())) {
                dpO().fail();
            }
        }
    }

    @C6485anp
    @C5511aMd

    /* renamed from: a.aDZ$b */
    /* compiled from: a */
    public static class StateE extends MissionScriptState implements C1616Xf {
        public static int _m_fieldCount = 0;
        public static C5663aRz[] _m_fields = null;
        public static int _m_methodCount = 0;
        public static C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static C5663aRz f2553aT = null;
        public static C2491fm beE = null;
        public static C2491fm byQ = null;
        public static long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "239062c42c5a89ab244833f948d68f20", aum = 0)
        static /* synthetic */ LootItemMissionScript awH;

        static {
            m13653V();
        }

        public StateE() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateE(LootItemMissionScript adz) {
            super((C5540aNg) null);
            super._m_script_init(adz);
        }

        public StateE(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m13653V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 2;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(LootItemMissionScript.StateE.class, "239062c42c5a89ab244833f948d68f20", i);
            f2553aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(LootItemMissionScript.StateE.class, "2f1fe7586ddc26695e01c6e49bf588f0", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(LootItemMissionScript.StateE.class, "b71ff02fdf8dd6db86830cb46b7c3083", i4);
            byQ = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(LootItemMissionScript.StateE.class, C0305Dy.class, _m_fields, _m_methods);
        }

        private LootItemMissionScript dpO() {
            return (LootItemMissionScript) bFf().mo5608dq().mo3214p(f2553aT);
        }

        /* renamed from: g */
        private void m13655g(LootItemMissionScript adz) {
            bFf().mo5608dq().mo3197f(f2553aT, adz);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0305Dy(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    m13654cq((String) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: cr */
        public void mo6626cr(String str) {
            switch (bFf().mo6893i(byQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    break;
            }
            m13654cq(str);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo8415h(LootItemMissionScript adz) {
            m13655g(adz);
            super.mo10S();
        }

        @C0064Am(aul = "2f1fe7586ddc26695e01c6e49bf588f0", aum = 0)
        private void aai() {
            dpO().mo574k(LootItemMissionScriptTemplate.ebj, 1);
            dpO().mo566b(LootItemMissionScriptTemplate.ebj, Mission.C0015a.ACCOMPLISHED);
        }

        @C0064Am(aul = "b71ff02fdf8dd6db86830cb46b7c3083", aum = 0)
        /* renamed from: cq */
        private void m13654cq(String str) {
            if (str.equals(((LootItemMissionScriptTemplate) dpO().cdp()).bse())) {
                ItemType bsg = ((LootItemMissionScriptTemplate) dpO().cdp()).bsg();
                if (bsg == null) {
                    bsg = ((LootItemMissionScriptTemplate) dpO().cdp()).brU();
                }
                if (dpO().cdr().mo9417f(bsg, 1) > 0) {
                    dpO().mo574k(LootItemMissionScriptTemplate.ebk, 1);
                    dpO().mo566b(LootItemMissionScriptTemplate.ebk, Mission.C0015a.ACCOMPLISHED);
                    dpO().cdk();
                }
            }
        }
    }
}
