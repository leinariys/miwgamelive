package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.script.missiontemplate.scripting.TalkToNPCMissionScriptTemplate;
import game.script.npcchat.PlayerSpeech;
import logic.baa.*;
import logic.data.mbean.C0346Eb;
import logic.data.mbean.C5632aQu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.mission.TalkToNPCMission")
@C6485anp
@C5511aMd
/* renamed from: a.XS */
/* compiled from: a */
public class TalkToNPCMissionScript<T extends TalkToNPCMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f2114Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f2115yH = null;
    public static C6494any ___iScriptClass;

    static {
        m11494V();
    }

    public TalkToNPCMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TalkToNPCMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11494V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 0;
        _m_methodCount = MissionScript._m_methodCount + 2;
        _m_fields = new C5663aRz[(MissionScript._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(TalkToNPCMissionScript.class, "ecfb4c5a856928ff10eeef732edddc68", i);
        f2115yH = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(TalkToNPCMissionScript.class, "18d9230a88b75f41aeea449279f68496", i2);
        f2114Do = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TalkToNPCMissionScript.class, C0346Eb.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0346Eb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m11496jF();
                return null;
            case 1:
                m11495a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2114Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2114Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2114Do, new Object[]{jt}));
                break;
        }
        m11495a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f2115yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2115yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2115yH, new Object[0]));
                break;
        }
        m11496jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ecfb4c5a856928ff10eeef732edddc68", aum = 0)
    /* renamed from: jF */
    private void m11496jF() {
        mo575p(StateA.class);
    }

    @C0064Am(aul = "18d9230a88b75f41aeea449279f68496", aum = 0)
    /* renamed from: a */
    private void m11495a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && azm() == null) {
            mo576r(StateA.class);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.XS$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f2116aT = null;
        public static final C2491fm byQ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "8266c01c29065807cf263f604f7526fa", aum = 0)
        static /* synthetic */ TalkToNPCMissionScript eJR;

        static {
            m11505V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        StateA(TalkToNPCMissionScript xs) {
            super((C5540aNg) null);
            super._m_script_init(xs);
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m11505V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(TalkToNPCMissionScript.StateA.class, "8266c01c29065807cf263f604f7526fa", i);
            f2116aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(TalkToNPCMissionScript.StateA.class, "ade0d31a8cfcdd60faf503962f70ec1b", i3);
            byQ = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(TalkToNPCMissionScript.StateA.class, C5632aQu.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m11506a(TalkToNPCMissionScript xs) {
            bFf().mo5608dq().mo3197f(f2116aT, xs);
        }

        private TalkToNPCMissionScript bGl() {
            return (TalkToNPCMissionScript) bFf().mo5608dq().mo3214p(f2116aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5632aQu(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    m11507cq((String) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: cr */
        public void mo6626cr(String str) {
            switch (bFf().mo6893i(byQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                    break;
            }
            m11507cq(str);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo6730b(TalkToNPCMissionScript xs) {
            m11506a(xs);
            super.mo10S();
        }

        @C0064Am(aul = "ade0d31a8cfcdd60faf503962f70ec1b", aum = 0)
        /* renamed from: cq */
        private void m11507cq(String str) {
            for (PlayerSpeech handle : ((TalkToNPCMissionScriptTemplate) bGl().cdp()).btQ()) {
                if (handle.getHandle().equals(str)) {
                    bGl().cdk();
                    return;
                }
            }
        }
    }
}
