package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1621Xk;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.azY */
/* compiled from: a */
public class I18NStringTableEntry extends aDJ implements C0468GU, C1616Xf {

    /* renamed from: CR */
    public static final C5663aRz f5687CR = null;

    /* renamed from: CS */
    public static final C5663aRz f5688CS = null;

    /* renamed from: CU */
    public static final C2491fm f5689CU = null;

    /* renamed from: CV */
    public static final C2491fm f5690CV = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5692bL = null;
    /* renamed from: bM */
    public static final C5663aRz f5693bM = null;
    /* renamed from: bN */
    public static final C2491fm f5694bN = null;
    /* renamed from: bO */
    public static final C2491fm f5695bO = null;
    /* renamed from: bP */
    public static final C2491fm f5696bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5697bQ = null;
    public static final C2491fm hcv = null;
    public static final C2491fm hcw = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b80316dc2f1980b967b96df354a289e4", aum = 2)

    /* renamed from: bK */
    private static UUID f5691bK;
    @C0064Am(aul = "31c90e18e15521e4c4a7c4c72fdf5228", aum = 1)
    private static I18NString eIv;
    @C0064Am(aul = "5c67dcfb5394145ebbe9fcb3a22ef218", aum = 3)
    private static String handle;
    @C0064Am(aul = "a773798ee452130c0bf17b61af44b057", aum = 0)
    private static String key;

    static {
        m27692V();
    }

    public I18NStringTableEntry() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public I18NStringTableEntry(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27692V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(I18NStringTableEntry.class, "a773798ee452130c0bf17b61af44b057", i);
        f5687CR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(I18NStringTableEntry.class, "31c90e18e15521e4c4a7c4c72fdf5228", i2);
        f5688CS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(I18NStringTableEntry.class, "b80316dc2f1980b967b96df354a289e4", i3);
        f5692bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(I18NStringTableEntry.class, "5c67dcfb5394145ebbe9fcb3a22ef218", i4);
        f5693bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(I18NStringTableEntry.class, "419fbd451ad54100c06bc56e210169ae", i6);
        f5694bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(I18NStringTableEntry.class, "f85b0db9f3beb83b3c8a808c8890a51b", i7);
        f5695bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(I18NStringTableEntry.class, "19bc42b4be79ebf6e55176d9df70561d", i8);
        f5696bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(I18NStringTableEntry.class, "2f58ee9be2dcf2b0d221c09654fe80b6", i9);
        f5697bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(I18NStringTableEntry.class, "b970c4f8d2527fd7d31fb00d56bd9d76", i10);
        f5689CU = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(I18NStringTableEntry.class, "67f41b2b133dc4c7b9920a15eb1f8673", i11);
        f5690CV = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(I18NStringTableEntry.class, "34051a72bd3ccd3e1556b8659beb6965", i12);
        hcv = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(I18NStringTableEntry.class, "c2944f01b068c8117513abd129735e3d", i13);
        hcw = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(I18NStringTableEntry.class, "5890c3059b9db6bf2f3f2435de0cdc66", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(I18NStringTableEntry.class, C1621Xk.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m27691U(String str) {
        bFf().mo5608dq().mo3197f(f5687CR, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "67f41b2b133dc4c7b9920a15eb1f8673", aum = 0)
    @C5566aOg
    /* renamed from: W */
    private void m27693W(String str) {
        throw new aWi(new aCE(this, f5690CV, new Object[]{str}));
    }

    /* renamed from: a */
    private void m27694a(String str) {
        bFf().mo5608dq().mo3197f(f5693bM, str);
    }

    /* renamed from: a */
    private void m27695a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5692bL, uuid);
    }

    /* renamed from: an */
    private UUID m27696an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5692bL);
    }

    /* renamed from: ao */
    private String m27697ao() {
        return (String) bFf().mo5608dq().mo3214p(f5693bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "2f58ee9be2dcf2b0d221c09654fe80b6", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m27701b(String str) {
        throw new aWi(new aCE(this, f5697bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m27703c(UUID uuid) {
        switch (bFf().mo6893i(f5695bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5695bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5695bO, new Object[]{uuid}));
                break;
        }
        m27702b(uuid);
    }

    private I18NString cHh() {
        return (I18NString) bFf().mo5608dq().mo3214p(f5688CS);
    }

    /* renamed from: lM */
    private String m27704lM() {
        return (String) bFf().mo5608dq().mo3214p(f5687CR);
    }

    /* renamed from: nw */
    private void m27706nw(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f5688CS, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C0064Am(aul = "c2944f01b068c8117513abd129735e3d", aum = 0)
    @C5566aOg
    /* renamed from: nx */
    private void m27707nx(I18NString i18NString) {
        throw new aWi(new aCE(this, hcw, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1621Xk(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m27698ap();
            case 1:
                m27702b((UUID) args[0]);
                return null;
            case 2:
                return m27699ar();
            case 3:
                m27701b((String) args[0]);
                return null;
            case 4:
                return m27705lO();
            case 5:
                m27693W((String) args[0]);
                return null;
            case 6:
                return cHi();
            case 7:
                m27707nx((I18NString) args[0]);
                return null;
            case 8:
                return m27700au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5694bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5694bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5694bN, new Object[0]));
                break;
        }
        return m27698ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    public I18NString cHj() {
        switch (bFf().mo6893i(hcv)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hcv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hcv, new Object[0]));
                break;
        }
        return cHi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5696bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5696bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5696bP, new Object[0]));
                break;
        }
        return m27699ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5697bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5697bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5697bQ, new Object[]{str}));
                break;
        }
        m27701b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getKey() {
        switch (bFf().mo6893i(f5689CU)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5689CU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5689CU, new Object[0]));
                break;
        }
        return m27705lO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setKey(String str) {
        switch (bFf().mo6893i(f5690CV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5690CV, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5690CV, new Object[]{str}));
                break;
        }
        m27693W(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Value")
    @C5566aOg
    /* renamed from: ny */
    public void mo17217ny(I18NString i18NString) {
        switch (bFf().mo6893i(hcw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hcw, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hcw, new Object[]{i18NString}));
                break;
        }
        m27707nx(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m27700au();
    }

    @C0064Am(aul = "419fbd451ad54100c06bc56e210169ae", aum = 0)
    /* renamed from: ap */
    private UUID m27698ap() {
        return m27696an();
    }

    @C0064Am(aul = "f85b0db9f3beb83b3c8a808c8890a51b", aum = 0)
    /* renamed from: b */
    private void m27702b(UUID uuid) {
        m27695a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m27695a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "19bc42b4be79ebf6e55176d9df70561d", aum = 0)
    /* renamed from: ar */
    private String m27699ar() {
        return m27697ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "b970c4f8d2527fd7d31fb00d56bd9d76", aum = 0)
    /* renamed from: lO */
    private String m27705lO() {
        return m27704lM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Value")
    @C0064Am(aul = "34051a72bd3ccd3e1556b8659beb6965", aum = 0)
    private I18NString cHi() {
        return cHh();
    }

    @C0064Am(aul = "5890c3059b9db6bf2f3f2435de0cdc66", aum = 0)
    /* renamed from: au */
    private String m27700au() {
        return String.valueOf(m27704lM()) + " (" + getClass().getSimpleName() + ")";
    }
}
