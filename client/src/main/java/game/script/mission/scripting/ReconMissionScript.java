package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.missiontemplate.ReconWaypointDat;
import game.script.missiontemplate.scripting.ReconMissionScriptTemplate;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.ship.Ship;
import logic.baa.*;
import logic.data.mbean.C0989OZ;
import logic.data.mbean.aMZ;
import logic.data.mbean.aWr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.mission.ReconMission")
@C6485anp
@C5511aMd
/* renamed from: a.rY */
/* compiled from: a */
public class ReconMissionScript<T extends ReconMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f9040Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aiO = null;
    public static final C5663aRz bHZ = null;
    public static final C2491fm bYI = null;
    public static final C2491fm coD = null;
    public static final C2491fm dWw = null;
    public static final C5663aRz ddM = null;
    public static final C5663aRz gFj = null;
    public static final C5663aRz gWS = null;
    public static final C5663aRz gWT = null;
    public static final C5663aRz gWW = null;
    public static final C5663aRz gWY = null;
    public static final C5663aRz gXa = null;
    public static final C2491fm gXb = null;
    public static final C2491fm gXc = null;
    public static final C2491fm gXd = null;
    public static final C2491fm gXe = null;
    public static final C2491fm gXf = null;
    public static final C2491fm gXg = null;
    public static final C2491fm gXh = null;
    public static final C2491fm gXi = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f9043yH = null;
    private static final String gWQ = "reconMission_selectableNPCHandle";
    /* renamed from: yB */
    private static final String f9042yB = "reconMission_attackerNPCHandle";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "93c58966bbba2bbe3d320877c70966a7", aum = 1)
    private static int cCx = 0;
    @C0064Am(aul = "10efdf8c540e8dd8a0c7159c439eb2f4", aum = 3)
    private static Vec3d cCz = null;
    @C0064Am(aul = "8565d8c61f962da2f687ff9e42f0a64b", aum = 6)
    private static int ddL = 0;
    @C0064Am(aul = "9be145538101ec508a5dafc0cbab708f", aum = 0)
    private static ReconWaypointDat gWR = null;
    @C0064Am(aul = "25021a262fa714df31d48b746ec01254", aum = 2)
    private static ReconWaypointDat gWU = null;
    @C0064Am(aul = "e278a7465b44cc346f952c5908165ff9", aum = 5)
    private static int gWV = 0;
    @C0064Am(aul = "eb200098a5302c2cf9fc7fd99564853d", aum = 7)
    private static C3438ra<Ship> gWX = null;
    @C0064Am(aul = "0ccaf0816e734892060b6edf0375dff6", aum = 8)
    private static int gWZ = 0;
    @C0064Am(aul = "ca92e3bccce713df87fa393e6e7b5a9d", aum = 4)

    /* renamed from: vV */
    private static int f9041vV = 0;

    static {
        m38397V();
    }

    public ReconMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ReconMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38397V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 9;
        _m_methodCount = MissionScript._m_methodCount + 13;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(ReconMissionScript.class, "9be145538101ec508a5dafc0cbab708f", i);
        gWS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ReconMissionScript.class, "93c58966bbba2bbe3d320877c70966a7", i2);
        gWT = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ReconMissionScript.class, "25021a262fa714df31d48b746ec01254", i3);
        bHZ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ReconMissionScript.class, "10efdf8c540e8dd8a0c7159c439eb2f4", i4);
        gFj = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ReconMissionScript.class, "ca92e3bccce713df87fa393e6e7b5a9d", i5);
        aiO = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ReconMissionScript.class, "e278a7465b44cc346f952c5908165ff9", i6);
        gWW = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ReconMissionScript.class, "8565d8c61f962da2f687ff9e42f0a64b", i7);
        ddM = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ReconMissionScript.class, "eb200098a5302c2cf9fc7fd99564853d", i8);
        gWY = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ReconMissionScript.class, "0ccaf0816e734892060b6edf0375dff6", i9);
        gXa = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i11 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 13)];
        C2491fm a = C4105zY.m41624a(ReconMissionScript.class, "f8116228f075a587a0f4cd194891abb1", i11);
        bYI = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(ReconMissionScript.class, "bc2d3085324222dfa230f88fdb2294ad", i12);
        coD = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(ReconMissionScript.class, "13ef5018af794e67a4bf291a0412414f", i13);
        gXb = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(ReconMissionScript.class, "417c6a0197c8f2ea72ae424ba255bc81", i14);
        gXc = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(ReconMissionScript.class, "372dcaca120c2ceb3f90a74cd3511eb5", i15);
        f9043yH = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(ReconMissionScript.class, "40d120803aa77b8527ca8cf314430690", i16);
        gXd = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(ReconMissionScript.class, "c5bca59eeb5589c187eb1a4a8eeaba18", i17);
        gXe = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(ReconMissionScript.class, "06b340d4fc769ee9eeb4ed0b73fe9f69", i18);
        dWw = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(ReconMissionScript.class, "589d2fab6674604ccd947e56249cd2e7", i19);
        gXf = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(ReconMissionScript.class, "68aa633eded6e6f6e1be142ac1381e8b", i20);
        gXg = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(ReconMissionScript.class, "a43308876ec901cf897db58c99f0d3d5", i21);
        gXh = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(ReconMissionScript.class, "9dba8db08e0c71cc724b35bd04942a2a", i22);
        gXi = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        C2491fm a13 = C4105zY.m41624a(ReconMissionScript.class, "ccc684f6632772e704820add5b4aaea1", i23);
        f9040Do = a13;
        fmVarArr[i23] = a13;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ReconMissionScript.class, aWr.class, _m_fields, _m_methods);
    }

    /* access modifiers changed from: private */
    /* renamed from: EU */
    public int m38396EU() {
        return bFf().mo5608dq().mo3212n(aiO);
    }

    @C0064Am(aul = "a43308876ec901cf897db58c99f0d3d5", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private NPC m38398a(NPCType aed, String str, ReconWaypointDat ht) {
        throw new aWi(new aCE(this, gXh, new Object[]{aed, str, ht}));
    }

    private int aVm() {
        return bFf().mo5608dq().mo3212n(ddM);
    }

    /* renamed from: af */
    private void m38404af(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(gFj, ajr);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "13ef5018af794e67a4bf291a0412414f", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: ai */
    private void m38405ai(Ship fAVar) {
        throw new aWi(new aCE(this, gXb, new Object[]{fAVar}));
    }

    /* access modifiers changed from: private */
    @C5566aOg
    /* renamed from: b */
    public NPC m38406b(NPCType aed, String str, ReconWaypointDat ht) {
        switch (bFf().mo6893i(gXh)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, gXh, new Object[]{aed, str, ht}));
            case 3:
                bFf().mo5606d(new aCE(this, gXh, new Object[]{aed, str, ht}));
                break;
        }
        return m38398a(aed, str, ht);
    }

    /* access modifiers changed from: private */
    public ReconWaypointDat cEf() {
        return (ReconWaypointDat) bFf().mo5608dq().mo3214p(gWS);
    }

    private int cEg() {
        return bFf().mo5608dq().mo3212n(gWT);
    }

    /* access modifiers changed from: private */
    public ReconWaypointDat cEh() {
        return (ReconWaypointDat) bFf().mo5608dq().mo3214p(bHZ);
    }

    private Vec3d cEi() {
        return (Vec3d) bFf().mo5608dq().mo3214p(gFj);
    }

    private int cEj() {
        return bFf().mo5608dq().mo3212n(gWW);
    }

    /* access modifiers changed from: private */
    public C3438ra cEk() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gWY);
    }

    private int cEl() {
        return bFf().mo5608dq().mo3212n(gXa);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "417c6a0197c8f2ea72ae424ba255bc81", aum = 0)
    @C2499fr(mo18857qh = 2)
    private void cEm() {
        throw new aWi(new aCE(this, gXc, new Object[0]));
    }

    @C0064Am(aul = "40d120803aa77b8527ca8cf314430690", aum = 0)
    @C5566aOg
    private void cEo() {
        throw new aWi(new aCE(this, gXd, new Object[0]));
    }

    @C0064Am(aul = "589d2fab6674604ccd947e56249cd2e7", aum = 0)
    @C5566aOg
    private void cEq() {
        throw new aWi(new aCE(this, gXf, new Object[0]));
    }

    @C0064Am(aul = "9dba8db08e0c71cc724b35bd04942a2a", aum = 0)
    @C5566aOg
    private ReconWaypointDat cEs() {
        throw new aWi(new aCE(this, gXi, new Object[0]));
    }

    /* access modifiers changed from: private */
    /* renamed from: cd */
    public void m38410cd(int i) {
        bFf().mo5608dq().mo3183b(aiO, i);
    }

    /* renamed from: ck */
    private void m38411ck(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gWY, raVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m38414e(ReconWaypointDat ht) {
        bFf().mo5608dq().mo3197f(gWS, ht);
    }

    /* renamed from: f */
    private void m38416f(ReconWaypointDat ht) {
        bFf().mo5608dq().mo3197f(bHZ, ht);
    }

    @C0064Am(aul = "c5bca59eeb5589c187eb1a4a8eeaba18", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private String m38417g(ReconWaypointDat ht) {
        throw new aWi(new aCE(this, gXe, new Object[]{ht}));
    }

    @C0064Am(aul = "68aa633eded6e6f6e1be142ac1381e8b", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private boolean m38418i(ReconWaypointDat ht) {
        throw new aWi(new aCE(this, gXg, new Object[]{ht}));
    }

    /* renamed from: kC */
    private void m38420kC(int i) {
        bFf().mo5608dq().mo3183b(ddM, i);
    }

    @C0064Am(aul = "06b340d4fc769ee9eeb4ed0b73fe9f69", aum = 0)
    @C5566aOg
    /* renamed from: mX */
    private String m38421mX(int i) {
        throw new aWi(new aCE(this, dWw, new Object[]{new Integer(i)}));
    }

    /* renamed from: wk */
    private void m38422wk(int i) {
        bFf().mo5608dq().mo3183b(gWT, i);
    }

    /* renamed from: wl */
    private void m38423wl(int i) {
        bFf().mo5608dq().mo3183b(gWW, i);
    }

    /* access modifiers changed from: private */
    /* renamed from: wm */
    public void m38424wm(int i) {
        bFf().mo5608dq().mo3183b(gXa, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aWr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                return new Boolean(arx());
            case 1:
                m38409c((Runnable) args[0]);
                return null;
            case 2:
                m38405ai((Ship) args[0]);
                return null;
            case 3:
                cEm();
                return null;
            case 4:
                m38419jF();
                return null;
            case 5:
                cEo();
                return null;
            case 6:
                return m38417g((ReconWaypointDat) args[0]);
            case 7:
                return m38421mX(((Integer) args[0]).intValue());
            case 8:
                cEq();
                return null;
            case 9:
                return new Boolean(m38418i((ReconWaypointDat) args[0]));
            case 10:
                return m38398a((NPCType) args[0], (String) args[1], (ReconWaypointDat) args[2]);
            case 11:
                return cEs();
            case 12:
                m38400a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: aj */
    public void mo21682aj(Ship fAVar) {
        switch (bFf().mo6893i(gXb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXb, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXb, new Object[]{fAVar}));
                break;
        }
        m38405ai(fAVar);
    }

    public boolean ary() {
        switch (bFf().mo6893i(bYI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bYI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bYI, new Object[0]));
                break;
        }
        return arx();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9040Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9040Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9040Do, new Object[]{jt}));
                break;
        }
        m38400a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    public void cEn() {
        switch (bFf().mo6893i(gXc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXc, new Object[0]));
                break;
        }
        cEm();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public void cEp() {
        switch (bFf().mo6893i(gXd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXd, new Object[0]));
                break;
        }
        cEo();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    public void cEr() {
        switch (bFf().mo6893i(gXf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXf, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXf, new Object[0]));
                break;
        }
        cEq();
    }

    @C5566aOg
    public ReconWaypointDat cEt() {
        switch (bFf().mo6893i(gXi)) {
            case 0:
                return null;
            case 2:
                return (ReconWaypointDat) bFf().mo5606d(new aCE(this, gXi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gXi, new Object[0]));
                break;
        }
        return cEs();
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: d */
    public void mo21687d(Runnable runnable) {
        switch (bFf().mo6893i(coD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                break;
        }
        m38409c(runnable);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: h */
    public String mo21688h(ReconWaypointDat ht) {
        switch (bFf().mo6893i(gXe)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gXe, new Object[]{ht}));
            case 3:
                bFf().mo5606d(new aCE(this, gXe, new Object[]{ht}));
                break;
        }
        return m38417g(ht);
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f9043yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9043yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9043yH, new Object[0]));
                break;
        }
        m38419jF();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: j */
    public boolean mo21689j(ReconWaypointDat ht) {
        switch (bFf().mo6893i(gXg)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gXg, new Object[]{ht}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gXg, new Object[]{ht}));
                break;
        }
        return m38418i(ht);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: mY */
    public String mo21690mY(int i) {
        switch (bFf().mo6893i(dWw)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dWw, new Object[]{new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, dWw, new Object[]{new Integer(i)}));
                break;
        }
        return m38421mX(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f8116228f075a587a0f4cd194891abb1", aum = 0)
    private boolean arx() {
        return true;
    }

    @C4034yP
    @C0064Am(aul = "bc2d3085324222dfa230f88fdb2294ad", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: c */
    private void m38409c(Runnable runnable) {
        runnable.run();
    }

    @C0064Am(aul = "372dcaca120c2ceb3f90a74cd3511eb5", aum = 0)
    /* renamed from: jF */
    private void m38419jF() {
        m38422wk(-1);
        m38414e((ReconWaypointDat) null);
        mo575p(StateA.class);
    }

    @C0064Am(aul = "ccc684f6632772e704820add5b4aaea1", aum = 0)
    /* renamed from: a */
    private void m38400a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && azm() == null) {
            mo576r(StateA.class);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.rY$b */
    /* compiled from: a */
    public class SelectionCallback extends aDJ implements C1616Xf, C6681ard {
        /* renamed from: QS */
        public static final C5663aRz f9046QS = null;
        /* renamed from: QW */
        public static final C5663aRz f9048QW = null;
        /* renamed from: QY */
        public static final C2491fm f9049QY = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9050aT = null;
        public static final C5663aRz bwx = null;

        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "e37f8e57eb617948677ecba4f7221dd3", aum = 3)
        static /* synthetic */ ReconMissionScript beD = null;
        @C0064Am(aul = "fb3a4ea3d622e49690e85e250f9b9a5c", aum = 0)

        /* renamed from: QR */
        private static Ship f9045QR = null;
        @C0064Am(aul = "987eb182b42c718180404495fed50e82", aum = 2)

        /* renamed from: QV */
        private static float f9047QV = 0.0f;
        @C0064Am(aul = "c454aa992baf3a14d0cd1e04d7cd1fde", aum = 1)
        private static Ship bww = null;

        static {
            m38454V();
        }

        public SelectionCallback() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public SelectionCallback(C5540aNg ang) {
            super(ang);
        }

        public SelectionCallback(ReconMissionScript rYVar, Ship fAVar, Ship fAVar2, float f) {
            super((C5540aNg) null);
            super._m_script_init(rYVar, fAVar, fAVar2, f);
        }

        /* renamed from: V */
        static void m38454V() {
            _m_fieldCount = aDJ._m_fieldCount + 4;
            _m_methodCount = aDJ._m_methodCount + 1;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 4)];
            C5663aRz b = C5640aRc.m17844b(ReconMissionScript.SelectionCallback.class, "fb3a4ea3d622e49690e85e250f9b9a5c", i);
            f9046QS = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(ReconMissionScript.SelectionCallback.class, "c454aa992baf3a14d0cd1e04d7cd1fde", i2);
            bwx = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(ReconMissionScript.SelectionCallback.class, "987eb182b42c718180404495fed50e82", i3);
            f9048QW = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(ReconMissionScript.SelectionCallback.class, "e37f8e57eb617948677ecba4f7221dd3", i4);
            f9050aT = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i6 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i6 + 1)];
            C2491fm a = C4105zY.m41624a(ReconMissionScript.SelectionCallback.class, "9f512fb75d9fe68b7a92fd0351cbdc43", i6);
            f9049QY = a;
            fmVarArr[i6] = a;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ReconMissionScript.SelectionCallback.class, C0989OZ.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m38455a(ReconMissionScript rYVar) {
            bFf().mo5608dq().mo3197f(f9050aT, rYVar);
        }

        private ReconMissionScript aah() {
            return (ReconMissionScript) bFf().mo5608dq().mo3214p(f9050aT);
        }

        /* renamed from: ad */
        private void m38456ad(float f) {
            bFf().mo5608dq().mo3150a(f9048QW, f);
        }

        private Ship ajw() {
            return (Ship) bFf().mo5608dq().mo3214p(bwx);
        }

        /* renamed from: e */
        private void m38457e(Ship fAVar) {
            bFf().mo5608dq().mo3197f(f9046QS, fAVar);
        }

        /* renamed from: v */
        private void m38458v(Ship fAVar) {
            bFf().mo5608dq().mo3197f(bwx, fAVar);
        }

        /* renamed from: wc */
        private Ship m38459wc() {
            return (Ship) bFf().mo5608dq().mo3214p(f9046QS);
        }

        /* renamed from: we */
        private float m38460we() {
            return bFf().mo5608dq().mo3211m(f9048QW);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0989OZ(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    m38461wg();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public void run() {
            switch (bFf().mo6893i(f9049QY)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9049QY, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9049QY, new Object[0]));
                    break;
            }
            m38461wg();
        }

        /* renamed from: a */
        public void mo21692a(ReconMissionScript rYVar, Ship fAVar, Ship fAVar2, float f) {
            m38455a(rYVar);
            super.mo10S();
            m38457e(fAVar);
            m38458v(fAVar2);
            m38456ad(f);
        }

        @C0064Am(aul = "9f512fb75d9fe68b7a92fd0351cbdc43", aum = 0)
        /* renamed from: wg */
        private void m38461wg() {
            if (m38459wc().mo1011bv((Actor) ajw()) <= m38460we()) {
                aah().mo21682aj(ajw());
            } else {
                aah().cEn();
            }
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.rY$a */
    public class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9044aT = null;
        public static final C2491fm beE = null;
        public static final C2491fm beF = null;
        public static final C2491fm beG = null;
        public static final C2491fm beH = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "a2a77f08f6a066ef43cbae1c14e77c5a", aum = 0)
        static /* synthetic */ ReconMissionScript beD;

        static {
            m38438V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        StateA(ReconMissionScript rYVar) {
            super((C5540aNg) null);
            super._m_script_init(rYVar);
        }

        /* renamed from: V */
        static void m38438V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 4;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ReconMissionScript.StateA.class, "a2a77f08f6a066ef43cbae1c14e77c5a", i);
            f9044aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
            C2491fm a = C4105zY.m41624a(ReconMissionScript.StateA.class, "9f25c19fc3a13622f2816f62f788c29b", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ReconMissionScript.StateA.class, "ac877b3d552e76fb49373b917b8662c8", i4);
            beF = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(ReconMissionScript.StateA.class, "99251805d103593fcd687f2b1e8cae56", i5);
            beG = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(ReconMissionScript.StateA.class, "3ab076f05abf8e5d5b2c8d7800e98369", i6);
            beH = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ReconMissionScript.StateA.class, aMZ.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m38439a(ReconMissionScript rYVar) {
            bFf().mo5608dq().mo3197f(f9044aT, rYVar);
        }

        @C0064Am(aul = "99251805d103593fcd687f2b1e8cae56", aum = 0)
        @C5566aOg
        /* renamed from: a */
        private void m38440a(String str, Ship fAVar, Ship fAVar2) {
            throw new aWi(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
        }

        private ReconMissionScript aah() {
            return (ReconMissionScript) bFf().mo5608dq().mo3214p(f9044aT);
        }

        @C0064Am(aul = "ac877b3d552e76fb49373b917b8662c8", aum = 0)
        @C5566aOg
        /* renamed from: bk */
        private boolean m38441bk(String str) {
            throw new aWi(new aCE(this, beF, new Object[]{str}));
        }

        @C0064Am(aul = "3ab076f05abf8e5d5b2c8d7800e98369", aum = 0)
        @C5566aOg
        /* renamed from: n */
        private void m38442n(Ship fAVar) {
            throw new aWi(new aCE(this, beH, new Object[]{fAVar}));
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new aMZ(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                case 1:
                    return new Boolean(m38441bk((String) args[0]));
                case 2:
                    m38440a((String) args[0], (Ship) args[1], (Ship) args[2]);
                    return null;
                case 3:
                    m38442n((Ship) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        @C5566aOg
        /* renamed from: b */
        public void mo8419b(String str, Ship fAVar, Ship fAVar2) {
            switch (bFf().mo6893i(beG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                    break;
            }
            m38440a(str, fAVar, fAVar2);
        }

        @C5566aOg
        /* renamed from: bl */
        public boolean mo4962bl(String str) {
            switch (bFf().mo6893i(beF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                    break;
            }
            return m38441bk(str);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        @C5566aOg
        /* renamed from: o */
        public void mo12266o(Ship fAVar) {
            switch (bFf().mo6893i(beH)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                    break;
            }
            m38442n(fAVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo21691b(ReconMissionScript rYVar) {
            m38439a(rYVar);
            super.mo10S();
        }

        @C0064Am(aul = "9f25c19fc3a13622f2816f62f788c29b", aum = 0)
        private void aai() {
            if (aah().cdp() == null) {
                mo8358lY("Mission template not set in an instance of " + getClass().getSimpleName());
            } else if (((ReconMissionScriptTemplate) aah().cdp()).mo20577Ne().isEmpty()) {
                mo8358lY("Recon mission template has no waypoints");
            } else {
                aah().m38424wm(0);
                aah().cEp();
            }
        }
    }
}
