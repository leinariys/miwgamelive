package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.script.missiontemplate.PositionDat;
import game.script.missiontemplate.scripting.KillingNpcMissionScriptTemplate;
import game.script.ship.Ship;
import logic.baa.*;
import logic.data.mbean.C2880lO;
import logic.data.mbean.C6310akW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C3437rZ(aak = "", aal = "taikodom.game.script.mission.KillingNpcMission")
@C6485anp
@C5511aMd
/* renamed from: a.nV */
/* compiled from: a */
public class KillingNpcMissionScript<T extends KillingNpcMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f8715Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMd = null;
    public static final C2491fm aMe = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f8716yH = null;
    private static final String aMb = "KILLING_NPC_SPOT";
    private static final long aMc = 1000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "fec24a1cd8314e940d45099069902f26", aum = 0)
    private static int aAt = 0;

    static {
        m36115V();
    }

    public KillingNpcMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public KillingNpcMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36115V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionScript._m_fieldCount + 1;
        _m_methodCount = MissionScript._m_methodCount + 3;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(KillingNpcMissionScript.class, "fec24a1cd8314e940d45099069902f26", i);
        aMd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i3 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(KillingNpcMissionScript.class, "62ac39f29295f7222a16c708e6fb9220", i3);
        aMe = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(KillingNpcMissionScript.class, "7ebfdd502187ac5f2abfbaf5d231e449", i4);
        f8716yH = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(KillingNpcMissionScript.class, "30ab315a700848b6685844052f1f958a", i5);
        f8715Do = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(KillingNpcMissionScript.class, C2880lO.class, _m_fields, _m_methods);
    }

    /* renamed from: RP */
    private int m36114RP() {
        return bFf().mo5608dq().mo3212n(aMd);
    }

    /* renamed from: dm */
    private void m36118dm(int i) {
        bFf().mo5608dq().mo3183b(aMd, i);
    }

    @C0064Am(aul = "62ac39f29295f7222a16c708e6fb9220", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m36119i(Ship fAVar) {
        throw new aWi(new aCE(this, aMe, new Object[]{fAVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2880lO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m36119i((Ship) args[0]);
                return null;
            case 1:
                m36120jF();
                return null;
            case 2:
                m36116a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8715Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8715Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8715Do, new Object[]{jt}));
                break;
        }
        m36116a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f8716yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8716yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8716yH, new Object[0]));
                break;
        }
        m36120jF();
    }

    @C5566aOg
    /* renamed from: j */
    public void mo130j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m36119i(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "7ebfdd502187ac5f2abfbaf5d231e449", aum = 0)
    /* renamed from: jF */
    private void m36120jF() {
        m36118dm(0);
        mo575p(StateA.class);
    }

    @C0064Am(aul = "30ab315a700848b6685844052f1f958a", aum = 0)
    /* renamed from: a */
    private void m36116a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && azm() == null) {
            mo576r(StateA.class);
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.nV$a */
    class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8717aT = null;
        public static final C2491fm beE = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "8950200f8342a95047c8801fcff17eda", aum = 0)
        static /* synthetic */ KillingNpcMissionScript fUT;

        static {
            m36130V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        StateA(KillingNpcMissionScript nVVar) {
            super((C5540aNg) null);
            super._m_script_init(nVVar);
        }

        /* renamed from: V */
        static void m36130V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(KillingNpcMissionScript.StateA.class, "8950200f8342a95047c8801fcff17eda", i);
            f8717aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(KillingNpcMissionScript.StateA.class, "326efc246aeb4c1bf03ae42084275610", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(KillingNpcMissionScript.StateA.class, C6310akW.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m36131a(KillingNpcMissionScript nVVar) {
            bFf().mo5608dq().mo3197f(f8717aT, nVVar);
        }

        private KillingNpcMissionScript dnm() {
            return (KillingNpcMissionScript) bFf().mo5608dq().mo3214p(f8717aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6310akW(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo20755b(KillingNpcMissionScript nVVar) {
            m36131a(nVVar);
            super.mo10S();
        }

        @C0064Am(aul = "326efc246aeb4c1bf03ae42084275610", aum = 0)
        private void aai() {
            for (PositionDat position : ((KillingNpcMissionScriptTemplate) dnm().cdp()).alY()) {
                dnm().mo92b(KillingNpcMissionScript.aMb, position.getPosition(), ((KillingNpcMissionScriptTemplate) dnm().cdp()).mo22778Nc(), KillingNpcMissionScript.aMc, true, ((KillingNpcMissionScriptTemplate) dnm().cdp()).alW());
            }
        }
    }
}
