package game.script.mission.scripting;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.space.Node;
import logic.baa.*;
import logic.data.mbean.C6933awV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.io.Serializable;
import java.util.Collection;

@C5511aMd
@C6485anp
        /* renamed from: a.aaW  reason: case insensitive filesystem */
        /* compiled from: a */
class MissionArea extends aDJ implements C1616Xf, Serializable {
    public static final C5663aRz _f_center = null;
    /* renamed from: _f_getCenter_0020_0028_0029Lcom_002fhoplon_002fgeometry_002fVec3d_003b */
    public static final C2491fm f4075xdc7de0cc = null;
    /* renamed from: _f_setCenter_0020_0028Lcom_002fhoplon_002fgeometry_002fVec3d_003b_0029V */
    public static final C2491fm f4076x6107980 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aSi = null;
    public static final C2491fm aSj = null;
    public static final C2491fm aSk = null;
    public static final C2491fm aSl = null;
    public static final C5663aRz awh = null;
    public static final C2491fm cQA = null;
    public static final C2491fm cQB = null;
    public static final C5663aRz cQw = null;
    public static final C2491fm eXm = null;
    public static final C2491fm eXn = null;
    public static final C5663aRz ebp = null;
    /* renamed from: ui */
    public static final C5663aRz f4077ui = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "058fcb8d6a3993958b5f7a2d1d814ec1", aum = 2)

    /* renamed from: Dw */
    private static long f4073Dw = 0;
    @C0064Am(aul = "b6142c1f6de8fb026cfee5c594f8c1a0", aum = 4)

    /* renamed from: Dx */
    private static long f4074Dx = 0;
    @C0064Am(aul = "8043227a5237dcf0bb5a060450152cad", aum = 0)
    private static Node aSh = null;
    @C0064Am(aul = "51c61043d5febcab02b0a38940cf7f34", aum = 1)
    private static Vec3d center = null;
    @C0064Am(aul = "c60efd4628b9d8bac293ac1ecb83658a", aum = 3)
    private static Vec3d eXl = null;

    static {
        m19498V();
    }

    MissionArea() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionArea(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19498V() {
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 10;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(MissionArea.class, "8043227a5237dcf0bb5a060450152cad", i);
        f4077ui = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionArea.class, "51c61043d5febcab02b0a38940cf7f34", i2);
        _f_center = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionArea.class, "058fcb8d6a3993958b5f7a2d1d814ec1", i3);
        awh = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionArea.class, "c60efd4628b9d8bac293ac1ecb83658a", i4);
        ebp = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MissionArea.class, "b6142c1f6de8fb026cfee5c594f8c1a0", i5);
        cQw = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(MissionArea.class, "731355a64bfc3250aaff0d9ebbc7e9af", i7);
        aSi = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionArea.class, "fe19857be9ac1df5ab693adf002cdad5", i8);
        aSj = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionArea.class, "b918a5200bb69cc779f75de3b6db8e31", i9);
        f4075xdc7de0cc = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionArea.class, "6c994c5114e3cf0dd2c39634867c5627", i10);
        f4076x6107980 = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionArea.class, "a02b956f20e2aa52836761bf61352167", i11);
        aSk = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionArea.class, "cdd9c50d5816aa65f5faaab49a2b2032", i12);
        aSl = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionArea.class, "d0ba4f0698a7a2987aed965d0031d990", i13);
        eXm = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionArea.class, "9f5cc4cdc6a88a04f871cacfadd4644f", i14);
        eXn = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionArea.class, "d7490a46544e5df3f32ab1f13c80e3f4", i15);
        cQA = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionArea.class, "623b27cde0f8b72e3d798691c93c6e88", i16);
        cQB = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionArea.class, C6933awV.class, _m_fields, _m_methods);
    }

    /* renamed from: O */
    private void m19496O(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(ebp, ajr);
    }

    /* renamed from: Vb */
    private Node m19499Vb() {
        return (Node) bFf().mo5608dq().mo3214p(f4077ui);
    }

    /* renamed from: Vc */
    private long m19500Vc() {
        return bFf().mo5608dq().mo3213o(awh);
    }

    /* renamed from: a */
    private void m19503a(Node rPVar) {
        bFf().mo5608dq().mo3197f(f4077ui, rPVar);
    }

    private long aMF() {
        return bFf().mo5608dq().mo3213o(cQw);
    }

    private Vec3d bMR() {
        return (Vec3d) bFf().mo5608dq().mo3214p(ebp);
    }

    /* renamed from: cH */
    private void m19505cH(long j) {
        bFf().mo5608dq().mo3184b(awh, j);
    }

    /* renamed from: em */
    private void m19507em(long j) {
        bFf().mo5608dq().mo3184b(cQw, j);
    }

    /* renamed from: f */
    private void m19509f(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(_f_center, ajr);
    }

    /* renamed from: zL */
    private Vec3d m19511zL() {
        return (Vec3d) bFf().mo5608dq().mo3214p(_f_center);
    }

    /* renamed from: Q */
    public void mo12211Q(Vec3d ajr) {
        switch (bFf().mo6893i(eXn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eXn, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eXn, new Object[]{ajr}));
                break;
        }
        m19497P(ajr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: Ve */
    public Node mo12212Ve() {
        switch (bFf().mo6893i(aSi)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, aSi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aSi, new Object[0]));
                break;
        }
        return m19501Vd();
    }

    /* renamed from: Vg */
    public long mo12213Vg() {
        switch (bFf().mo6893i(aSk)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, aSk, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, aSk, new Object[0]));
                break;
        }
        return m19502Vf();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6933awV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m19501Vd();
            case 1:
                m19504b((Node) args[0]);
                return null;
            case 2:
                return m19512zN();
            case 3:
                m19510g((Vec3d) args[0]);
                return null;
            case 4:
                return new Long(m19502Vf());
            case 5:
                m19506cI(((Long) args[0]).longValue());
                return null;
            case 6:
                return bMS();
            case 7:
                m19497P((Vec3d) args[0]);
                return null;
            case 8:
                return new Long(aMI());
            case 9:
                m19508en(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public long aMJ() {
        switch (bFf().mo6893i(cQA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cQA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cQA, new Object[0]));
                break;
        }
        return aMI();
    }

    public Vec3d bMT() {
        switch (bFf().mo6893i(eXm)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, eXm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eXm, new Object[0]));
                break;
        }
        return bMS();
    }

    /* renamed from: c */
    public void mo12216c(Node rPVar) {
        switch (bFf().mo6893i(aSj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                break;
        }
        m19504b(rPVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cJ */
    public void mo12217cJ(long j) {
        switch (bFf().mo6893i(aSl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSl, new Object[]{new Long(j)}));
                break;
        }
        m19506cI(j);
    }

    /* renamed from: eo */
    public void mo12218eo(long j) {
        switch (bFf().mo6893i(cQB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQB, new Object[]{new Long(j)}));
                break;
        }
        m19508en(j);
    }

    /* renamed from: h */
    public void mo12219h(Vec3d ajr) {
        switch (bFf().mo6893i(f4076x6107980)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4076x6107980, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4076x6107980, new Object[]{ajr}));
                break;
        }
        m19510g(ajr);
    }

    /* renamed from: zO */
    public Vec3d mo12220zO() {
        switch (bFf().mo6893i(f4075xdc7de0cc)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, f4075xdc7de0cc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4075xdc7de0cc, new Object[0]));
                break;
        }
        return m19512zN();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "731355a64bfc3250aaff0d9ebbc7e9af", aum = 0)
    /* renamed from: Vd */
    private Node m19501Vd() {
        return m19499Vb();
    }

    @C0064Am(aul = "fe19857be9ac1df5ab693adf002cdad5", aum = 0)
    /* renamed from: b */
    private void m19504b(Node rPVar) {
        m19503a(rPVar);
    }

    @C0064Am(aul = "b918a5200bb69cc779f75de3b6db8e31", aum = 0)
    /* renamed from: zN */
    private Vec3d m19512zN() {
        return m19511zL();
    }

    @C0064Am(aul = "6c994c5114e3cf0dd2c39634867c5627", aum = 0)
    /* renamed from: g */
    private void m19510g(Vec3d ajr) {
        m19509f(ajr);
    }

    @C0064Am(aul = "a02b956f20e2aa52836761bf61352167", aum = 0)
    /* renamed from: Vf */
    private long m19502Vf() {
        return m19500Vc();
    }

    @C0064Am(aul = "cdd9c50d5816aa65f5faaab49a2b2032", aum = 0)
    /* renamed from: cI */
    private void m19506cI(long j) {
        m19505cH(j);
    }

    @C0064Am(aul = "d0ba4f0698a7a2987aed965d0031d990", aum = 0)
    private Vec3d bMS() {
        return bMR();
    }

    @C0064Am(aul = "9f5cc4cdc6a88a04f871cacfadd4644f", aum = 0)
    /* renamed from: P */
    private void m19497P(Vec3d ajr) {
        m19496O(ajr);
    }

    @C0064Am(aul = "d7490a46544e5df3f32ab1f13c80e3f4", aum = 0)
    private long aMI() {
        return aMF();
    }

    @C0064Am(aul = "623b27cde0f8b72e3d798691c93c6e88", aum = 0)
    /* renamed from: en */
    private void m19508en(long j) {
        m19507em(j);
    }
}
