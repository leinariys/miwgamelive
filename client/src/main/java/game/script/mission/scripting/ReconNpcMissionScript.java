package game.script.mission.scripting;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.missiontemplate.PositionDat;
import game.script.missiontemplate.scripting.ReconNpcMissionScriptTemplate;
import game.script.npc.NPC;
import game.script.ship.Ship;
import logic.baa.*;
import logic.data.mbean.C2292dg;
import logic.data.mbean.C5497aLp;
import logic.data.mbean.C6816auI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.uG */
/* compiled from: a */
public class ReconNpcMissionScript<T extends ReconNpcMissionScriptTemplate> extends MissionScript<T> implements aOW, aOW {

    /* renamed from: Do */
    public static final C2491fm f9329Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm beH = null;
    public static final C2491fm coD = null;
    public static final C5663aRz gWY = null;
    public static final C2491fm gXb = null;
    public static final C2491fm gXc = null;
    public static final C2491fm iBJ = null;
    /* renamed from: yH */
    public static final C2491fm f9330yH = null;
    private static final String gPy = "recon_npc_spot";
    private static final long iBI = 1000;
    private static final long serialVersionUID = 1804041777897082216L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "4f2904991db6ec42ac8b36030b9e9454", aum = 0)
    private static C3438ra<Ship> gWX = null;

    static {
        m39868V();
    }

    public ReconNpcMissionScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ReconNpcMissionScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39868V() {
        _m_fieldCount = MissionScript._m_fieldCount + 1;
        _m_methodCount = MissionScript._m_methodCount + 7;
        int i = MissionScript._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ReconNpcMissionScript.class, "4f2904991db6ec42ac8b36030b9e9454", i);
        gWY = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_fields, (Object[]) _m_fields);
        int i3 = MissionScript._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
        C2491fm a = C4105zY.m41624a(ReconNpcMissionScript.class, "78dc4e9708edef72c969ca98ea2e1e1e", i3);
        gXb = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ReconNpcMissionScript.class, "1dfa6e1af2937255c1301946a21764eb", i4);
        iBJ = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ReconNpcMissionScript.class, "b6fc071e8412d0dec05a5925320ee49f", i5);
        gXc = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ReconNpcMissionScript.class, "cb5b9b8b296cfda41cac6c016d534bd2", i6);
        coD = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(ReconNpcMissionScript.class, "d61ec3f027354ecc29b35a2e0cea9811", i7);
        beH = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(ReconNpcMissionScript.class, "7fd756d3dd67fd04d48e4c6d1c9d274b", i8);
        f9330yH = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(ReconNpcMissionScript.class, "9a2e2ad6b0d24da7041d49b2025f61a9", i9);
        f9329Do = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionScript._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ReconNpcMissionScript.class, C5497aLp.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "78dc4e9708edef72c969ca98ea2e1e1e", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: ai */
    private void m39871ai(Ship fAVar) {
        throw new aWi(new aCE(this, gXb, new Object[]{fAVar}));
    }

    private C3438ra cEk() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gWY);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "b6fc071e8412d0dec05a5925320ee49f", aum = 0)
    @C2499fr(mo18857qh = 2)
    private void cEm() {
        throw new aWi(new aCE(this, gXc, new Object[0]));
    }

    /* renamed from: ck */
    private void m39873ck(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gWY, raVar);
    }

    @C0064Am(aul = "1dfa6e1af2937255c1301946a21764eb", aum = 0)
    @C5566aOg
    private void dor() {
        throw new aWi(new aCE(this, iBJ, new Object[0]));
    }

    @C5566aOg
    private void dos() {
        switch (bFf().mo6893i(iBJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iBJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iBJ, new Object[0]));
                break;
        }
        dor();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5497aLp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MissionScript._m_methodCount) {
            case 0:
                m39871ai((Ship) args[0]);
                return null;
            case 1:
                dor();
                return null;
            case 2:
                cEm();
                return null;
            case 3:
                m39872c((Runnable) args[0]);
                return null;
            case 4:
                m39875n((Ship) args[0]);
                return null;
            case 5:
                m39874jF();
                return null;
            case 6:
                m39869a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: aj */
    public void mo22335aj(Ship fAVar) {
        switch (bFf().mo6893i(gXb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXb, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXb, new Object[]{fAVar}));
                break;
        }
        m39871ai(fAVar);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9329Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9329Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9329Do, new Object[]{jt}));
                break;
        }
        m39869a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    public void cEn() {
        switch (bFf().mo6893i(gXc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXc, new Object[0]));
                break;
        }
        cEm();
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: d */
    public void mo22337d(Runnable runnable) {
        switch (bFf().mo6893i(coD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                break;
        }
        m39872c(runnable);
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f9330yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9330yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9330yH, new Object[0]));
                break;
        }
        m39874jF();
    }

    /* renamed from: o */
    public void mo132o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m39875n(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C4034yP
    @C0064Am(aul = "cb5b9b8b296cfda41cac6c016d534bd2", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: c */
    private void m39872c(Runnable runnable) {
        runnable.run();
    }

    @C0064Am(aul = "d61ec3f027354ecc29b35a2e0cea9811", aum = 0)
    /* renamed from: n */
    private void m39875n(Ship fAVar) {
        if ((fAVar.agj() instanceof NPC) && !cEk().contains(fAVar)) {
            ReconNpcMissionScriptTemplate aup = (ReconNpcMissionScriptTemplate) cdp();
            if (aup.alS().contains(((NPC) fAVar.agj()).mo11654Fs())) {
                SelectionCallback aVar = (SelectionCallback) bFf().mo6865M(SelectionCallback.class);
                aVar.mo22338a(this, cdr().bQx(), fAVar, (float) aup.cyt());
                mo22337d(aVar);
            }
        }
    }

    @C0064Am(aul = "7fd756d3dd67fd04d48e4c6d1c9d274b", aum = 0)
    /* renamed from: jF */
    private void m39874jF() {
        cEk().clear();
        mo575p(StateA.class);
    }

    @C0064Am(aul = "9a2e2ad6b0d24da7041d49b2025f61a9", aum = 0)
    /* renamed from: a */
    private void m39869a(C0665JT jt) {
        if ("".equals(jt.getVersion()) && azm() == null) {
            mo576r(StateA.class);
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.uG$a */
    class SelectionCallback extends aDJ implements C1616Xf, C6681ard {
        /* renamed from: QS */
        public static final C5663aRz f9332QS = null;
        /* renamed from: QW */
        public static final C5663aRz f9334QW = null;
        /* renamed from: QY */
        public static final C2491fm f9335QY = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9336aT = null;
        public static final C5663aRz bwx = null;
        private static final long serialVersionUID = 928566213673285239L;
        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "0a671c592b2f15389508806401402ddf", aum = 3)

        /* renamed from: ym */
        static /* synthetic */ ReconNpcMissionScript f9337ym;
        @C0064Am(aul = "80ba0190da18e0895910246a0eec5bff", aum = 0)

        /* renamed from: QR */
        private static Ship f9331QR = null;
        @C0064Am(aul = "39620c7064f3002287c18e82587c9e5f", aum = 2)

        /* renamed from: QV */
        private static float f9333QV = 0.0f;
        @C0064Am(aul = "195824867636a172932b4cc1e216a128", aum = 1)
        private static Ship bww = null;

        static {
            m39887V();
        }

        public SelectionCallback() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public SelectionCallback(C5540aNg ang) {
            super(ang);
        }

        public SelectionCallback(ReconNpcMissionScript uGVar, Ship fAVar, Ship fAVar2, float f) {
            super((C5540aNg) null);
            super._m_script_init(uGVar, fAVar, fAVar2, f);
        }

        /* renamed from: V */
        static void m39887V() {
            _m_fieldCount = aDJ._m_fieldCount + 4;
            _m_methodCount = aDJ._m_methodCount + 1;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 4)];
            C5663aRz b = C5640aRc.m17844b(ReconNpcMissionScript.SelectionCallback.class, "80ba0190da18e0895910246a0eec5bff", i);
            f9332QS = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(ReconNpcMissionScript.SelectionCallback.class, "195824867636a172932b4cc1e216a128", i2);
            bwx = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(ReconNpcMissionScript.SelectionCallback.class, "39620c7064f3002287c18e82587c9e5f", i3);
            f9334QW = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(ReconNpcMissionScript.SelectionCallback.class, "0a671c592b2f15389508806401402ddf", i4);
            f9336aT = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i6 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i6 + 1)];
            C2491fm a = C4105zY.m41624a(ReconNpcMissionScript.SelectionCallback.class, "9f16bc72ad50a7cf6642cf9a2c52e9a9", i6);
            f9335QY = a;
            fmVarArr[i6] = a;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ReconNpcMissionScript.SelectionCallback.class, C6816auI.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m39888a(ReconNpcMissionScript uGVar) {
            bFf().mo5608dq().mo3197f(f9336aT, uGVar);
        }

        /* renamed from: ad */
        private void m39889ad(float f) {
            bFf().mo5608dq().mo3150a(f9334QW, f);
        }

        private Ship ajw() {
            return (Ship) bFf().mo5608dq().mo3214p(bwx);
        }

        private ReconNpcMissionScript ajx() {
            return (ReconNpcMissionScript) bFf().mo5608dq().mo3214p(f9336aT);
        }

        /* renamed from: e */
        private void m39890e(Ship fAVar) {
            bFf().mo5608dq().mo3197f(f9332QS, fAVar);
        }

        /* renamed from: v */
        private void m39891v(Ship fAVar) {
            bFf().mo5608dq().mo3197f(bwx, fAVar);
        }

        /* renamed from: wc */
        private Ship m39892wc() {
            return (Ship) bFf().mo5608dq().mo3214p(f9332QS);
        }

        /* renamed from: we */
        private float m39893we() {
            return bFf().mo5608dq().mo3211m(f9334QW);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6816auI(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    m39894wg();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public void run() {
            switch (bFf().mo6893i(f9335QY)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9335QY, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9335QY, new Object[0]));
                    break;
            }
            m39894wg();
        }

        /* renamed from: a */
        public void mo22338a(ReconNpcMissionScript uGVar, Ship fAVar, Ship fAVar2, float f) {
            m39888a(uGVar);
            super.mo10S();
            m39890e(fAVar);
            m39891v(fAVar2);
            m39889ad(f);
        }

        @C0064Am(aul = "9f16bc72ad50a7cf6642cf9a2c52e9a9", aum = 0)
        /* renamed from: wg */
        private void m39894wg() {
            if (m39892wc().mo1011bv((Actor) ajw()) <= m39893we()) {
                ajx().mo22335aj(ajw());
            } else {
                ajx().cEn();
            }
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.uG$b */
            /* compiled from: a */
    class StateA extends MissionScriptState implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9338aT = null;
        public static final C2491fm beE = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "927d66e0e3372fe648c1544a617b5ef8", aum = 0)

        /* renamed from: ym */
        static /* synthetic */ ReconNpcMissionScript f9339ym;

        static {
            m39903V();
        }

        public StateA() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StateA(C5540aNg ang) {
            super(ang);
        }

        StateA(ReconNpcMissionScript uGVar) {
            super((C5540aNg) null);
            super._m_script_init(uGVar);
        }

        /* renamed from: V */
        static void m39903V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = MissionScriptState._m_fieldCount + 1;
            _m_methodCount = MissionScriptState._m_methodCount + 1;
            int i = MissionScriptState._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ReconNpcMissionScript.StateA.class, "927d66e0e3372fe648c1544a617b5ef8", i);
            f9338aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_fields, (Object[]) _m_fields);
            int i3 = MissionScriptState._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(ReconNpcMissionScript.StateA.class, "ba20e096e914cfbbad1245a88a8fc17a", i3);
            beE = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) MissionScriptState._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ReconNpcMissionScript.StateA.class, C2292dg.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m39904a(ReconNpcMissionScript uGVar) {
            bFf().mo5608dq().mo3197f(f9338aT, uGVar);
        }

        private ReconNpcMissionScript ajx() {
            return (ReconNpcMissionScript) bFf().mo5608dq().mo3214p(f9338aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2292dg(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - MissionScriptState._m_methodCount) {
                case 0:
                    aai();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void aaj() {
            switch (bFf().mo6893i(beE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, beE, new Object[0]));
                    break;
            }
            aai();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo22340b(ReconNpcMissionScript uGVar) {
            m39904a(uGVar);
            super.mo10S();
        }

        @C0064Am(aul = "ba20e096e914cfbbad1245a88a8fc17a", aum = 0)
        private void aai() {
            PositionDat cyv = ((ReconNpcMissionScriptTemplate) ajx().cdp()).cyv();
            if (cyv != null) {
                ajx().mo92b(ReconNpcMissionScript.gPy, cyv.getPosition(), ((ReconNpcMissionScriptTemplate) ajx().cdp()).mo16343Nc(), ReconNpcMissionScript.iBI, true, ((ReconNpcMissionScriptTemplate) ajx().cdp()).alW());
            }
        }
    }
}
