package game.script.mission;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Character;
import game.script.LDScriptingController;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.npc.NPC;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Gate;
import game.script.space.Loot;
import game.script.space.LootType;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5796aac;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.BP */
/* compiled from: a */
public final class NPCEventDispatcher<T extends C4045yZ> extends Mission<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLS = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLX = null;
    public static final C2491fm aLY = null;
    public static final C2491fm aMe = null;
    public static final C2491fm beF = null;
    public static final C2491fm beG = null;
    public static final C2491fm beH = null;
    public static final C2491fm byB = null;
    public static final C2491fm byC = null;
    public static final C2491fm byD = null;
    public static final C2491fm byE = null;
    public static final C2491fm byF = null;
    public static final C2491fm byG = null;
    public static final C2491fm byH = null;
    public static final C2491fm byI = null;
    public static final C2491fm byJ = null;
    public static final C2491fm byK = null;
    public static final C2491fm byL = null;
    public static final C2491fm byM = null;
    public static final C2491fm byN = null;
    public static final C2491fm byO = null;
    public static final C2491fm byP = null;
    public static final C2491fm byQ = null;
    public static final C2491fm byR = null;
    public static final C2491fm byS = null;
    public static final C2491fm cqA = null;
    public static final C5663aRz cqr = null;
    public static final C5663aRz cqt = null;
    public static final C5663aRz cqv = null;
    public static final C5663aRz cqx = null;
    public static final C2491fm cqy = null;
    public static final C2491fm cqz = null;
    private static final long serialVersionUID = -4628696445167179416L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "34a446d737a26c91a8e58fa4b61eb126", aum = 0)
    private static LDScriptingController cqq = null;
    @C0064Am(aul = "93bad94381bb7872853964bd300ecd44", aum = 1)
    private static Mission<? extends C4045yZ> cqs = null;
    @C0064Am(aul = "7f50807254e75697e0a4405c2fcb4745", aum = 2)
    private static String cqu = null;
    @C0064Am(aul = "623617a0113d303214083282038cab16", aum = 3)
    private static NPC cqw = null;

    static {
        m1145V();
    }

    public NPCEventDispatcher() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCEventDispatcher(C5540aNg ang) {
        super(ang);
    }

    NPCEventDispatcher(LDScriptingController lYVar, Mission<? extends C4045yZ> af, String str, NPC auf) {
        super((C5540aNg) null);
        super._m_script_init(lYVar, af, str, auf);
    }

    /* renamed from: V */
    static void m1145V() {
        _m_fieldCount = Mission._m_fieldCount + 4;
        _m_methodCount = Mission._m_methodCount + 33;
        int i = Mission._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(NPCEventDispatcher.class, "34a446d737a26c91a8e58fa4b61eb126", i);
        cqr = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCEventDispatcher.class, "93bad94381bb7872853964bd300ecd44", i2);
        cqt = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPCEventDispatcher.class, "7f50807254e75697e0a4405c2fcb4745", i3);
        cqv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NPCEventDispatcher.class, "623617a0113d303214083282038cab16", i4);
        cqx = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Mission._m_fields, (Object[]) _m_fields);
        int i6 = Mission._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 33)];
        C2491fm a = C4105zY.m41624a(NPCEventDispatcher.class, "a436fb07ef660651933160749c94ec5a", i6);
        cqy = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCEventDispatcher.class, "4b276177483a91c87d664bd89b05679c", i7);
        aLU = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCEventDispatcher.class, "4c04395c6d8a145dafd9611a315657f4", i8);
        aLT = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCEventDispatcher.class, "455c0a46e116205faddf9c4ae1320326", i9);
        byI = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCEventDispatcher.class, "0c1327d2bb1d22e573e4519439a3f9ee", i10);
        byJ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCEventDispatcher.class, "885294b78da48b013a97994f94448d72", i11);
        byK = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCEventDispatcher.class, "6c6cbac2958f68e2df5dfce8869239c4", i12);
        aMe = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCEventDispatcher.class, "5b8ad2e7e0515b3257b83ce0550c7ace", i13);
        aLS = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(NPCEventDispatcher.class, "1bd1713c844f2427aa39ea40590d16bc", i14);
        beF = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(NPCEventDispatcher.class, "f3dca077365ba7794b838db013a98969", i15);
        cqz = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(NPCEventDispatcher.class, "58b2b0c281816a743b2219f43bfd4757", i16);
        byM = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(NPCEventDispatcher.class, "6a8b20ec19cc9b970fbba392ccecb008", i17);
        beH = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(NPCEventDispatcher.class, "b55f165f17b77e6bc0652764a04b9d90", i18);
        byN = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(NPCEventDispatcher.class, "317426f2288e180da1076bb9ed5e9b69", i19);
        byO = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(NPCEventDispatcher.class, "2054a1656b1be84ecc3f77703d42ba37", i20);
        byP = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(NPCEventDispatcher.class, "8032e9ca7b579e94daabfa08cf88d513", i21);
        byQ = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(NPCEventDispatcher.class, "50f948503dcd48dcae248b94aca9d880", i22);
        aLR = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(NPCEventDispatcher.class, "a4d6035d4113deb8a5d954f7e5855de9", i23);
        beG = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        C2491fm a19 = C4105zY.m41624a(NPCEventDispatcher.class, "d484bf3a62038eeef4425c31600ba6d5", i24);
        byB = a19;
        fmVarArr[i24] = a19;
        int i25 = i24 + 1;
        C2491fm a20 = C4105zY.m41624a(NPCEventDispatcher.class, "9d99f9da3e89ba01b6f2de9a10afdb70", i25);
        byC = a20;
        fmVarArr[i25] = a20;
        int i26 = i25 + 1;
        C2491fm a21 = C4105zY.m41624a(NPCEventDispatcher.class, "36791a79962ec224bf6c2035a7895ed1", i26);
        byF = a21;
        fmVarArr[i26] = a21;
        int i27 = i26 + 1;
        C2491fm a22 = C4105zY.m41624a(NPCEventDispatcher.class, "71ac44d9b73aea582725be08484478bb", i27);
        byG = a22;
        fmVarArr[i27] = a22;
        int i28 = i27 + 1;
        C2491fm a23 = C4105zY.m41624a(NPCEventDispatcher.class, "4df42a2a844e5b52d304ced9bd8b6f8f", i28);
        byH = a23;
        fmVarArr[i28] = a23;
        int i29 = i28 + 1;
        C2491fm a24 = C4105zY.m41624a(NPCEventDispatcher.class, "ab7360104c8fdb765356a93614a22bd2", i29);
        byR = a24;
        fmVarArr[i29] = a24;
        int i30 = i29 + 1;
        C2491fm a25 = C4105zY.m41624a(NPCEventDispatcher.class, "24d9592326da6a84b54657521ced5433", i30);
        byS = a25;
        fmVarArr[i30] = a25;
        int i31 = i30 + 1;
        C2491fm a26 = C4105zY.m41624a(NPCEventDispatcher.class, "063787f108b2a4cdc08c78fe5c878e82", i31);
        byD = a26;
        fmVarArr[i31] = a26;
        int i32 = i31 + 1;
        C2491fm a27 = C4105zY.m41624a(NPCEventDispatcher.class, "f5091c6c732fed3bcfdde7a51a84de6a", i32);
        byL = a27;
        fmVarArr[i32] = a27;
        int i33 = i32 + 1;
        C2491fm a28 = C4105zY.m41624a(NPCEventDispatcher.class, "a49c5e603038fec94bb901fa0b9575de", i33);
        cqA = a28;
        fmVarArr[i33] = a28;
        int i34 = i33 + 1;
        C2491fm a29 = C4105zY.m41624a(NPCEventDispatcher.class, "b6cecb02da0c0e1c8bfee205a42bd72a", i34);
        aLV = a29;
        fmVarArr[i34] = a29;
        int i35 = i34 + 1;
        C2491fm a30 = C4105zY.m41624a(NPCEventDispatcher.class, "2dbd66bcee188fbc634892681e5449d1", i35);
        aLW = a30;
        fmVarArr[i35] = a30;
        int i36 = i35 + 1;
        C2491fm a31 = C4105zY.m41624a(NPCEventDispatcher.class, "11dc0d0972bde6182ce3e73bfba64e7a", i36);
        aLX = a31;
        fmVarArr[i36] = a31;
        int i37 = i36 + 1;
        C2491fm a32 = C4105zY.m41624a(NPCEventDispatcher.class, "01356ea60ce39168c67377571fdb620e", i37);
        aLY = a32;
        fmVarArr[i37] = a32;
        int i38 = i37 + 1;
        C2491fm a33 = C4105zY.m41624a(NPCEventDispatcher.class, "e130f85f52991f04abd16c3833125106", i38);
        byE = a33;
        fmVarArr[i38] = a33;
        int i39 = i38 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Mission._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCEventDispatcher.class, C5796aac.class, _m_fields, _m_methods);
    }

    private LDScriptingController aAU() {
        return (LDScriptingController) bFf().mo5608dq().mo3214p(cqr);
    }

    private Mission aAV() {
        return (Mission) bFf().mo5608dq().mo3214p(cqt);
    }

    private String aAW() {
        return (String) bFf().mo5608dq().mo3214p(cqv);
    }

    private NPC aAX() {
        return (NPC) bFf().mo5608dq().mo3214p(cqx);
    }

    /* renamed from: b */
    private void m1158b(LDScriptingController lYVar) {
        bFf().mo5608dq().mo3197f(cqr, lYVar);
    }

    /* renamed from: c */
    private void m1160c(Mission af) {
        bFf().mo5608dq().mo3197f(cqt, af);
    }

    /* renamed from: d */
    private void m1164d(NPC auf) {
        bFf().mo5608dq().mo3197f(cqx, auf);
    }

    /* renamed from: dO */
    private void m1166dO(String str) {
        bFf().mo5608dq().mo3197f(cqv, str);
    }

    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m1142RJ();
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m1143RL();
    }

    /* renamed from: RO */
    public void mo67RO() {
        switch (bFf().mo6893i(aLX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                break;
        }
        m1144RN();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5796aac(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Mission._m_methodCount) {
            case 0:
                return aAY();
            case 1:
                m1143RL();
                return null;
            case 2:
                m1142RJ();
                return null;
            case 3:
                m1176w((Ship) args[0]);
                return null;
            case 4:
                m1171p((Station) args[0]);
                return null;
            case 5:
                m1172r((Station) args[0]);
                return null;
            case 6:
                m1168i((Ship) args[0]);
                return null;
            case 7:
                m1167g((Ship) args[0]);
                return null;
            case 8:
                return new Boolean(m1159bk((String) args[0]));
            case 9:
                m1175t((Character) args[0]);
                return null;
            case 10:
                m1169k((Gate) args[0]);
                return null;
            case 11:
                m1170n((Ship) args[0]);
                return null;
            case 12:
                m1174t((Station) args[0]);
                return null;
            case 13:
                m1148a((MissionTrigger) args[0]);
                return null;
            case 14:
                akJ();
                return null;
            case 15:
                m1163cq((String) args[0]);
                return null;
            case 16:
                m1156aV((String) args[0]);
                return null;
            case 17:
                m1154a((String) args[0], (Ship) args[1], (Ship) args[2]);
                return null;
            case 18:
                m1151a((String) args[0], (Station) args[1]);
                return null;
            case 19:
                m1152a((String) args[0], (Character) args[1]);
                return null;
            case 20:
                m1153a((String) args[0], (Ship) args[1]);
                return null;
            case 21:
                m1162c((String) args[0], (Ship) args[1]);
                return null;
            case 22:
                m1173s((String) args[0], (String) args[1]);
                return null;
            case 23:
                m1147a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 24:
                akL();
                return null;
            case 25:
                m1157b((Loot) args[0]);
                return null;
            case 26:
                m1165d((Node) args[0]);
                return null;
            case 27:
                return new Boolean(m1155aD((Player) args[0]));
            case 28:
                m1150a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 29:
                m1161c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 30:
                m1144RN();
                return null;
            case 31:
                m1146a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            case 32:
                m1149a((Character) args[0], (Loot) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public NPC aAZ() {
        switch (bFf().mo6893i(cqy)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, cqy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqy, new Object[0]));
                break;
        }
        return aAY();
    }

    /* renamed from: aE */
    public boolean mo731aE(Player aku) {
        switch (bFf().mo6893i(cqA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cqA, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cqA, new Object[]{aku}));
                break;
        }
        return m1155aD(aku);
    }

    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m1156aV(str);
    }

    public void akK() {
        switch (bFf().mo6893i(byP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                break;
        }
        akJ();
    }

    public void akM() {
        switch (bFf().mo6893i(byS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                break;
        }
        akL();
    }

    /* renamed from: b */
    public void mo85b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m1146a(cr, acm);
    }

    /* renamed from: b */
    public void mo86b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m1147a(ahc, jCVar, i);
    }

    /* renamed from: b */
    public void mo87b(MissionTrigger aus) {
        switch (bFf().mo6893i(byO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                break;
        }
        m1148a(aus);
    }

    /* renamed from: b */
    public void mo88b(Character acx, Loot ael) {
        switch (bFf().mo6893i(byE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                break;
        }
        m1149a(acx, ael);
    }

    /* renamed from: b */
    public void mo90b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m1150a(auq, aag, aag2);
    }

    /* renamed from: b */
    public void mo91b(String str, Station bf) {
        switch (bFf().mo6893i(byB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                break;
        }
        m1151a(str, bf);
    }

    /* renamed from: b */
    public void mo93b(String str, Character acx) {
        switch (bFf().mo6893i(byC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                break;
        }
        m1152a(str, acx);
    }

    /* renamed from: b */
    public void mo94b(String str, Ship fAVar) {
        switch (bFf().mo6893i(byF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                break;
        }
        m1153a(str, fAVar);
    }

    /* renamed from: b */
    public void mo95b(String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(beG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                break;
        }
        m1154a(str, fAVar, fAVar2);
    }

    /* renamed from: bl */
    public boolean mo97bl(String str) {
        switch (bFf().mo6893i(beF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                break;
        }
        return m1159bk(str);
    }

    /* renamed from: c */
    public void mo99c(Loot ael) {
        switch (bFf().mo6893i(byD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                break;
        }
        m1157b(ael);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cr */
    public void mo115cr(String str) {
        switch (bFf().mo6893i(byQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                break;
        }
        m1163cq(str);
    }

    /* renamed from: d */
    public void mo117d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m1161c(auq, aag);
    }

    /* renamed from: d */
    public void mo119d(String str, Ship fAVar) {
        switch (bFf().mo6893i(byG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                break;
        }
        m1162c(str, fAVar);
    }

    /* renamed from: e */
    public void mo121e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m1165d(rPVar);
    }

    /* renamed from: h */
    public void mo125h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m1167g(fAVar);
    }

    /* renamed from: j */
    public void mo130j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m1168i(fAVar);
    }

    /* renamed from: l */
    public void mo131l(Gate fFVar) {
        switch (bFf().mo6893i(byM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                break;
        }
        m1169k(fFVar);
    }

    /* renamed from: o */
    public void mo132o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m1170n(fAVar);
    }

    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m1171p(bf);
    }

    /* renamed from: s */
    public void mo134s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m1172r(bf);
    }

    /* renamed from: t */
    public void mo135t(String str, String str2) {
        switch (bFf().mo6893i(byH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                break;
        }
        m1173s(str, str2);
    }

    /* renamed from: u */
    public void mo136u(Station bf) {
        switch (bFf().mo6893i(byN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                break;
        }
        m1174t(bf);
    }

    /* renamed from: u */
    public void mo732u(Character acx) {
        switch (bFf().mo6893i(cqz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqz, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqz, new Object[]{acx}));
                break;
        }
        m1175t(acx);
    }

    /* renamed from: x */
    public void mo138x(Ship fAVar) {
        switch (bFf().mo6893i(byI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                break;
        }
        m1176w(fAVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo729a(LDScriptingController lYVar, Mission<? extends C4045yZ> af, String str, NPC auf) {
        super.mo10S();
        m1158b(lYVar);
        m1160c((Mission) af);
        m1166dO(str);
        m1164d(auf);
    }

    @C0064Am(aul = "a436fb07ef660651933160749c94ec5a", aum = 0)
    private NPC aAY() {
        return aAX();
    }

    @C0064Am(aul = "4b276177483a91c87d664bd89b05679c", aum = 0)
    /* renamed from: RL */
    private void m1143RL() {
        throw new UnsupportedOperationException("This mission should never have be aborted");
    }

    @C0064Am(aul = "4c04395c6d8a145dafd9611a315657f4", aum = 0)
    /* renamed from: RJ */
    private void m1142RJ() {
        throw new UnsupportedOperationException("This mission should never be accepted");
    }

    @C0064Am(aul = "455c0a46e116205faddf9c4ae1320326", aum = 0)
    /* renamed from: w */
    private void m1176w(Ship fAVar) {
        if (aAU() == null) {
            System.err.println("**** Inconsistent npc temporary NPCEventDispatcher has been found");
        } else {
            aAU().mo20257b(aAV(), aAW(), fAVar, aAX().bQx());
        }
    }

    @C0064Am(aul = "0c1327d2bb1d22e573e4519439a3f9ee", aum = 0)
    /* renamed from: p */
    private void m1171p(Station bf) {
        if (aAU() == null) {
            System.err.println("**** Inconsistent npc temporary NPCEventDispatcher has been found");
        } else {
            aAU().mo20254b(aAV(), aAW(), bf);
        }
    }

    @C0064Am(aul = "885294b78da48b013a97994f94448d72", aum = 0)
    /* renamed from: r */
    private void m1172r(Station bf) {
    }

    @C0064Am(aul = "6c6cbac2958f68e2df5dfce8869239c4", aum = 0)
    /* renamed from: i */
    private void m1168i(Ship fAVar) {
        if (aAU() == null) {
            System.err.println("**** Inconsistent npc temporary NPCEventDispatcher has been found");
        } else {
            aAU().mo20256b(aAV(), aAW(), fAVar);
        }
    }

    @C0064Am(aul = "5b8ad2e7e0515b3257b83ce0550c7ace", aum = 0)
    /* renamed from: g */
    private void m1167g(Ship fAVar) {
        if (aAU() == null) {
            System.err.println("**** Inconsistent npc temporary NPCEventDispatcher has been found");
        } else {
            aAU().mo20273d(aAV(), aAW(), fAVar);
        }
    }

    @C0064Am(aul = "1bd1713c844f2427aa39ea40590d16bc", aum = 0)
    /* renamed from: bk */
    private boolean m1159bk(String str) {
        if (aAU() == null) {
            System.err.println("**** Inconsistent npc temporary NPCEventDispatcher has been found");
        } else {
            aAU().mo20258b(aAV(), aAW(), str);
        }
        return true;
    }

    @C0064Am(aul = "f3dca077365ba7794b838db013a98969", aum = 0)
    /* renamed from: t */
    private void m1175t(Character acx) {
        if (aAU() == null) {
            System.err.println("**** Inconsistent npc temporary NPCEventDispatcher has been found");
        } else {
            aAU().mo20255b(aAV(), aAW(), acx);
        }
    }

    @C0064Am(aul = "58b2b0c281816a743b2219f43bfd4757", aum = 0)
    /* renamed from: k */
    private void m1169k(Gate fFVar) {
        throw new UnsupportedOperationException("This mission should never call hud target events");
    }

    @C0064Am(aul = "6a8b20ec19cc9b970fbba392ccecb008", aum = 0)
    /* renamed from: n */
    private void m1170n(Ship fAVar) {
        throw new UnsupportedOperationException("This mission should never call hud target events");
    }

    @C0064Am(aul = "b55f165f17b77e6bc0652764a04b9d90", aum = 0)
    /* renamed from: t */
    private void m1174t(Station bf) {
        throw new UnsupportedOperationException("This mission should never call hud target events");
    }

    @C0064Am(aul = "317426f2288e180da1076bb9ed5e9b69", aum = 0)
    /* renamed from: a */
    private void m1148a(MissionTrigger aus) {
        throw new UnsupportedOperationException("This mission should never call hud target events");
    }

    @C0064Am(aul = "2054a1656b1be84ecc3f77703d42ba37", aum = 0)
    private void akJ() {
        throw new UnsupportedOperationException("This mission should never call hud target events");
    }

    @C0064Am(aul = "8032e9ca7b579e94daabfa08cf88d513", aum = 0)
    /* renamed from: cq */
    private void m1163cq(String str) {
        throw new UnsupportedOperationException("This mission should never call npc chat events");
    }

    @C0064Am(aul = "50f948503dcd48dcae248b94aca9d880", aum = 0)
    /* renamed from: aV */
    private void m1156aV(String str) {
        throw new UnsupportedOperationException("This mission should never call timer events");
    }

    @C0064Am(aul = "a4d6035d4113deb8a5d954f7e5855de9", aum = 0)
    /* renamed from: a */
    private void m1154a(String str, Ship fAVar, Ship fAVar2) {
        throw new UnsupportedOperationException("This mission should never call death events");
    }

    @C0064Am(aul = "d484bf3a62038eeef4425c31600ba6d5", aum = 0)
    /* renamed from: a */
    private void m1151a(String str, Station bf) {
        throw new UnsupportedOperationException("This mission should never call dock events");
    }

    @C0064Am(aul = "9d99f9da3e89ba01b6f2de9a10afdb70", aum = 0)
    /* renamed from: a */
    private void m1152a(String str, Character acx) {
        throw new UnsupportedOperationException("This mission should never call loot events");
    }

    @C0064Am(aul = "36791a79962ec224bf6c2035a7895ed1", aum = 0)
    /* renamed from: a */
    private void m1153a(String str, Ship fAVar) {
        throw new UnsupportedOperationException("This mission should never call npc kill events");
    }

    @C0064Am(aul = "71ac44d9b73aea582725be08484478bb", aum = 0)
    /* renamed from: c */
    private void m1162c(String str, Ship fAVar) {
        throw new UnsupportedOperationException("This mission should never call player kill events");
    }

    @C0064Am(aul = "4df42a2a844e5b52d304ced9bd8b6f8f", aum = 0)
    /* renamed from: s */
    private void m1173s(String str, String str2) {
        throw new UnsupportedOperationException("This mission should never call waypoint events");
    }

    @C0064Am(aul = "ab7360104c8fdb765356a93614a22bd2", aum = 0)
    /* renamed from: a */
    private void m1147a(LootType ahc, ItemType jCVar, int i) {
        throw new UnsupportedOperationException("This mission should never call loot events");
    }

    @C0064Am(aul = "24d9592326da6a84b54657521ced5433", aum = 0)
    private void akL() {
        throw new UnsupportedOperationException("This mission should never call accomplishment events");
    }

    @C0064Am(aul = "063787f108b2a4cdc08c78fe5c878e82", aum = 0)
    /* renamed from: b */
    private void m1157b(Loot ael) {
    }

    @C0064Am(aul = "f5091c6c732fed3bcfdde7a51a84de6a", aum = 0)
    /* renamed from: d */
    private void m1165d(Node rPVar) {
    }

    @C0064Am(aul = "a49c5e603038fec94bb901fa0b9575de", aum = 0)
    /* renamed from: aD */
    private boolean m1155aD(Player aku) {
        return aAV().mo64KY() == aku;
    }

    @C0064Am(aul = "b6cecb02da0c0e1c8bfee205a42bd72a", aum = 0)
    /* renamed from: a */
    private void m1150a(Item auq, ItemLocation aag, ItemLocation aag2) {
    }

    @C0064Am(aul = "2dbd66bcee188fbc634892681e5449d1", aum = 0)
    /* renamed from: c */
    private void m1161c(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "11dc0d0972bde6182ce3e73bfba64e7a", aum = 0)
    /* renamed from: RN */
    private void m1144RN() {
        throw new UnsupportedOperationException("This mission should never call accomplishment events");
    }

    @C0064Am(aul = "01356ea60ce39168c67377571fdb620e", aum = 0)
    /* renamed from: a */
    private void m1146a(Actor cr, C5260aCm acm) {
    }

    @C0064Am(aul = "e130f85f52991f04abd16c3833125106", aum = 0)
    /* renamed from: a */
    private void m1149a(Character acx, Loot ael) {
    }
}
