package game.script.mission;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aAN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aFD */
/* compiled from: a */
public class MissionBeaconType extends aDJ implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f2781LR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    /* renamed from: bL */
    public static final C5663aRz f2783bL = null;
    /* renamed from: bM */
    public static final C5663aRz f2784bM = null;
    /* renamed from: bN */
    public static final C2491fm f2785bN = null;
    /* renamed from: bO */
    public static final C2491fm f2786bO = null;
    /* renamed from: bP */
    public static final C2491fm f2787bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2788bQ = null;
    public static final C5663aRz hJJ = null;
    public static final C2491fm hJK = null;
    public static final C2491fm hJL = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0286726eb5931dbc54b2e4dc0c04e327", aum = 1)

    /* renamed from: LQ */
    private static Asset f2780LQ;
    @C0064Am(aul = "1d5075f9b1b829fec6459a455c78ffa3", aum = 3)

    /* renamed from: bK */
    private static UUID f2782bK;
    @C0064Am(aul = "489767550f2764b1d083451d7bcfc517", aum = 2)
    private static I18NString grv;
    @C0064Am(aul = "1468da888231ac76b37eaf059eb98a0b", aum = 0)
    private static String handle;

    static {
        m14495V();
    }

    public MissionBeaconType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionBeaconType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14495V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 8;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(MissionBeaconType.class, "1468da888231ac76b37eaf059eb98a0b", i);
        f2784bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionBeaconType.class, "0286726eb5931dbc54b2e4dc0c04e327", i2);
        f2781LR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionBeaconType.class, "489767550f2764b1d083451d7bcfc517", i3);
        hJJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionBeaconType.class, "1d5075f9b1b829fec6459a455c78ffa3", i4);
        f2783bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(MissionBeaconType.class, "e3176bb7e96225235440152c34d0328f", i6);
        f2785bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionBeaconType.class, "735ace7cf3c7aae99155d972f3162fa8", i7);
        f2786bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionBeaconType.class, "eb40e8897be09627f3d7a37ba97f6562", i8);
        aDk = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionBeaconType.class, "b7d01501be018e2307ceed70ca603e46", i9);
        aDl = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionBeaconType.class, "0c3776adf0598688212739b63614aacc", i10);
        hJK = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionBeaconType.class, "ff33dcd23f369d01d4cc67364588ae57", i11);
        hJL = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionBeaconType.class, "d3d4457b675ca441f380c111b8779f28", i12);
        f2787bP = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionBeaconType.class, "c9d437f7d11ce91da04ab79460685365", i13);
        f2788bQ = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionBeaconType.class, aAN.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Render")
    @C0064Am(aul = "b7d01501be018e2307ceed70ca603e46", aum = 0)
    @C5566aOg
    /* renamed from: H */
    private void m14493H(Asset tCVar) {
        throw new aWi(new aCE(this, aDl, new Object[]{tCVar}));
    }

    /* renamed from: a */
    private void m14496a(String str) {
        bFf().mo5608dq().mo3197f(f2784bM, str);
    }

    /* renamed from: a */
    private void m14497a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2783bL, uuid);
    }

    /* renamed from: an */
    private UUID m14498an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2783bL);
    }

    /* renamed from: ao */
    private String m14499ao() {
        return (String) bFf().mo5608dq().mo3214p(f2784bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "c9d437f7d11ce91da04ab79460685365", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m14502b(String str) {
        throw new aWi(new aCE(this, f2788bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m14504c(UUID uuid) {
        switch (bFf().mo6893i(f2786bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2786bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2786bO, new Object[]{uuid}));
                break;
        }
        m14503b(uuid);
    }

    private I18NString cZc() {
        return (I18NString) bFf().mo5608dq().mo3214p(hJJ);
    }

    /* renamed from: nK */
    private void m14505nK(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(hJJ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Name")
    @C0064Am(aul = "0c3776adf0598688212739b63614aacc", aum = 0)
    @C5566aOg
    /* renamed from: nL */
    private void m14506nL(I18NString i18NString) {
        throw new aWi(new aCE(this, hJK, new Object[]{i18NString}));
    }

    /* renamed from: r */
    private void m14507r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f2781LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m14508rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f2781LR);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Render")
    @C5566aOg
    /* renamed from: I */
    public void mo8766I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m14493H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Render")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m14494Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aAN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m14500ap();
            case 1:
                m14503b((UUID) args[0]);
                return null;
            case 2:
                return m14494Nt();
            case 3:
                m14493H((Asset) args[0]);
                return null;
            case 4:
                m14506nL((I18NString) args[0]);
                return null;
            case 5:
                return cZd();
            case 6:
                return m14501ar();
            case 7:
                m14502b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2785bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2785bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2785bN, new Object[0]));
                break;
        }
        return m14500ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Name")
    public I18NString cZe() {
        switch (bFf().mo6893i(hJL)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hJL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hJL, new Object[0]));
                break;
        }
        return cZd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2787bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2787bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2787bP, new Object[0]));
                break;
        }
        return m14501ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2788bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2788bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2788bQ, new Object[]{str}));
                break;
        }
        m14502b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Name")
    @C5566aOg
    /* renamed from: nM */
    public void mo8768nM(I18NString i18NString) {
        switch (bFf().mo6893i(hJK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hJK, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hJK, new Object[]{i18NString}));
                break;
        }
        m14506nL(i18NString);
    }

    @C0064Am(aul = "e3176bb7e96225235440152c34d0328f", aum = 0)
    /* renamed from: ap */
    private UUID m14500ap() {
        return m14498an();
    }

    @C0064Am(aul = "735ace7cf3c7aae99155d972f3162fa8", aum = 0)
    /* renamed from: b */
    private void m14503b(UUID uuid) {
        m14497a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m14497a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Render")
    @C0064Am(aul = "eb40e8897be09627f3d7a37ba97f6562", aum = 0)
    /* renamed from: Nt */
    private Asset m14494Nt() {
        return m14508rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Name")
    @C0064Am(aul = "ff33dcd23f369d01d4cc67364588ae57", aum = 0)
    private I18NString cZd() {
        return cZc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "d3d4457b675ca441f380c111b8779f28", aum = 0)
    /* renamed from: ar */
    private String m14501ar() {
        return m14499ao();
    }
}
