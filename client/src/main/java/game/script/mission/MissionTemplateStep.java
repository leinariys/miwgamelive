package game.script.mission;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1579XD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aQB */
/* compiled from: a */
public class MissionTemplateStep extends aDJ implements C0468GU, C1616Xf {

    /* renamed from: QA */
    public static final C5663aRz f3577QA = null;

    /* renamed from: QE */
    public static final C2491fm f3578QE = null;

    /* renamed from: QF */
    public static final C2491fm f3579QF = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f3581bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3582bM = null;
    /* renamed from: bN */
    public static final C2491fm f3583bN = null;
    /* renamed from: bO */
    public static final C2491fm f3584bO = null;
    /* renamed from: bP */
    public static final C2491fm f3585bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3586bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ef8b1dc82414d3cbe1884e4be917d88e", aum = 2)

    /* renamed from: bK */
    private static UUID f3580bK;
    @C0064Am(aul = "f5c45b64a7b1fc182d9672175a601d0e", aum = 0)
    private static String handle;
    @C0064Am(aul = "29fec800b5d4edd51472a81968b1456a", aum = 1)

    /* renamed from: nh */
    private static I18NString f3587nh;

    static {
        m17408V();
    }

    public MissionTemplateStep() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionTemplateStep(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17408V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 6;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(MissionTemplateStep.class, "f5c45b64a7b1fc182d9672175a601d0e", i);
        f3582bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionTemplateStep.class, "29fec800b5d4edd51472a81968b1456a", i2);
        f3577QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionTemplateStep.class, "ef8b1dc82414d3cbe1884e4be917d88e", i3);
        f3581bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(MissionTemplateStep.class, "57d706ac061bf6cbcc74bf244d85e6e7", i5);
        f3583bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionTemplateStep.class, "ca563ce5b3645177752a97f91aa83da8", i6);
        f3584bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionTemplateStep.class, "5add2fd05a55d7940749a1a592faaac9", i7);
        f3586bQ = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionTemplateStep.class, "6a8ab815337b116d724535f45139b397", i8);
        f3585bP = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionTemplateStep.class, "fed165f4704bc8e55a95f5345ec6cabf", i9);
        f3578QE = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionTemplateStep.class, "356be6b6fba4016cf49947a81dd1225d", i10);
        f3579QF = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionTemplateStep.class, C1579XD.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m17409a(String str) {
        bFf().mo5608dq().mo3197f(f3582bM, str);
    }

    /* renamed from: a */
    private void m17410a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3581bL, uuid);
    }

    /* renamed from: an */
    private UUID m17411an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3581bL);
    }

    /* renamed from: ao */
    private String m17412ao() {
        return (String) bFf().mo5608dq().mo3214p(f3582bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "6a8ab815337b116d724535f45139b397", aum = 0)
    @C5566aOg
    /* renamed from: ar */
    private String m17414ar() {
        throw new aWi(new aCE(this, f3585bP, new Object[0]));
    }

    /* renamed from: br */
    private void m17417br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3577QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "356be6b6fba4016cf49947a81dd1225d", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m17418bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f3579QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m17419c(UUID uuid) {
        switch (bFf().mo6893i(f3584bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3584bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3584bO, new Object[]{uuid}));
                break;
        }
        m17416b(uuid);
    }

    /* renamed from: vT */
    private I18NString m17420vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3577QA);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1579XD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m17413ap();
            case 1:
                m17416b((UUID) args[0]);
                return null;
            case 2:
                m17415b((String) args[0]);
                return null;
            case 3:
                return m17414ar();
            case 4:
                return m17421vV();
            case 5:
                m17418bu((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3583bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3583bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3583bN, new Object[0]));
                break;
        }
        return m17413ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo10858bv(I18NString i18NString) {
        switch (bFf().mo6893i(f3579QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3579QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3579QF, new Object[]{i18NString}));
                break;
        }
        m17418bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public String getHandle() {
        switch (bFf().mo6893i(f3585bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3585bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3585bP, new Object[0]));
                break;
        }
        return m17414ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3586bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3586bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3586bQ, new Object[]{str}));
                break;
        }
        m17415b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo10860vW() {
        switch (bFf().mo6893i(f3578QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3578QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3578QE, new Object[0]));
                break;
        }
        return m17421vV();
    }

    @C0064Am(aul = "57d706ac061bf6cbcc74bf244d85e6e7", aum = 0)
    /* renamed from: ap */
    private UUID m17413ap() {
        return m17411an();
    }

    @C0064Am(aul = "ca563ce5b3645177752a97f91aa83da8", aum = 0)
    /* renamed from: b */
    private void m17416b(UUID uuid) {
        m17410a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m17410a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "5add2fd05a55d7940749a1a592faaac9", aum = 0)
    /* renamed from: b */
    private void m17415b(String str) {
        m17409a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "fed165f4704bc8e55a95f5345ec6cabf", aum = 0)
    /* renamed from: vV */
    private I18NString m17421vV() {
        return m17420vT();
    }
}
