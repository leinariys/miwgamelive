package game.script.mission;

import game.network.message.externalizable.aCE;
import game.script.LDScriptingController;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6390aly;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.apY  reason: case insensitive filesystem */
/* compiled from: a */
public class MissionTimer extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aeP = null;
    public static final C5663aRz avF = null;
    public static final C2491fm bLp = null;
    /* renamed from: bM */
    public static final C5663aRz f5105bM = null;
    /* renamed from: bP */
    public static final C2491fm f5106bP = null;
    public static final C5663aRz gom = null;
    public static final C5663aRz gon = null;
    public static final C5663aRz goo = null;
    public static final C5663aRz gop = null;
    public static final C5663aRz goq = null;
    /* renamed from: pp */
    public static final C2491fm f5107pp = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "78f4609055a4fec1fd7f63fb629d3dd2", aum = 2)
    private static LDScriptingController fXo;
    @C0064Am(aul = "42741c9c78a11d7a45ec5d22f27b9a3a", aum = 3)
    private static boolean fXp;
    @C0064Am(aul = "ee61d0783ff2c6f7001714053c5989af", aum = 4)
    private static int fXq;
    @C0064Am(aul = "7802a8871959100cb8678c8f74dc16b0", aum = 5)
    private static float fXr;
    @C0064Am(aul = "46a8f0d4db7353a2fdcc851c612fdeba", aum = 6)
    private static boolean fXs;
    @C0064Am(aul = "515297e4703b00107693fca7abdd3bd2", aum = 7)
    private static long fXt;
    @C0064Am(aul = "377b306562f612978b2bfcc8ef2302de", aum = 1)
    private static String handle;
    @C0064Am(aul = "0b6acb8bc885192a634ffe8b5e1d6f70", aum = 0)

    /* renamed from: wx */
    private static Mission<? extends C4045yZ> f5108wx;

    static {
        m24904V();
    }

    public MissionTimer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    MissionTimer(Mission<? extends C4045yZ> af, String str, LDScriptingController lYVar, long j, int i, boolean z) {
        super((C5540aNg) null);
        super._m_script_init(af, str, lYVar, j, i, z);
    }

    public MissionTimer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24904V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 8;
        _m_methodCount = TaikodomObject._m_methodCount + 5;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(MissionTimer.class, "0b6acb8bc885192a634ffe8b5e1d6f70", i);
        aeP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionTimer.class, "377b306562f612978b2bfcc8ef2302de", i2);
        f5105bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionTimer.class, "78f4609055a4fec1fd7f63fb629d3dd2", i3);
        avF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionTimer.class, "42741c9c78a11d7a45ec5d22f27b9a3a", i4);
        gom = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MissionTimer.class, "ee61d0783ff2c6f7001714053c5989af", i5);
        gon = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(MissionTimer.class, "7802a8871959100cb8678c8f74dc16b0", i6);
        goo = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(MissionTimer.class, "46a8f0d4db7353a2fdcc851c612fdeba", i7);
        gop = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(MissionTimer.class, "515297e4703b00107693fca7abdd3bd2", i8);
        goq = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i10 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 5)];
        C2491fm a = C4105zY.m41624a(MissionTimer.class, "d53535470ab14c5007cbe8774824737e", i10);
        f5107pp = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionTimer.class, "ead9bcf15c286deb506efa7f857f79fc", i11);
        f5106bP = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionTimer.class, "08c15f645e09aed6762143d83f323652", i12);
        bLp = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionTimer.class, "e1d35d01285cd9838e7b41e14c6c4001", i13);
        _f_onResurrect_0020_0028_0029V = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionTimer.class, "2bab25f6e65482f13790215ab514fb06", i14);
        _f_dispose_0020_0028_0029V = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionTimer.class, C6390aly.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m24905a(String str) {
        bFf().mo5608dq().mo3197f(f5105bM, str);
    }

    /* renamed from: ao */
    private String m24907ao() {
        return (String) bFf().mo5608dq().mo3214p(f5105bM);
    }

    private LDScriptingController cqA() {
        return (LDScriptingController) bFf().mo5608dq().mo3214p(avF);
    }

    private boolean cqB() {
        return bFf().mo5608dq().mo3201h(gom);
    }

    private int cqC() {
        return bFf().mo5608dq().mo3212n(gon);
    }

    private float cqD() {
        return bFf().mo5608dq().mo3211m(goo);
    }

    private boolean cqE() {
        return bFf().mo5608dq().mo3201h(gop);
    }

    private long cqF() {
        return bFf().mo5608dq().mo3213o(goq);
    }

    private Mission cqz() {
        return (Mission) bFf().mo5608dq().mo3214p(aeP);
    }

    /* renamed from: d */
    private void m24909d(Mission af) {
        bFf().mo5608dq().mo3197f(aeP, af);
    }

    /* renamed from: fZ */
    private void m24910fZ(boolean z) {
        bFf().mo5608dq().mo3153a(gom, z);
    }

    @C0064Am(aul = "d53535470ab14c5007cbe8774824737e", aum = 0)
    @C5566aOg
    /* renamed from: gY */
    private void m24912gY() {
        throw new aWi(new aCE(this, f5107pp, new Object[0]));
    }

    /* renamed from: ga */
    private void m24913ga(boolean z) {
        bFf().mo5608dq().mo3153a(gop, z);
    }

    /* renamed from: i */
    private void m24914i(LDScriptingController lYVar) {
        bFf().mo5608dq().mo3197f(avF, lYVar);
    }

    /* renamed from: in */
    private void m24915in(long j) {
        bFf().mo5608dq().mo3184b(goq, j);
    }

    /* renamed from: kr */
    private void m24916kr(float f) {
        bFf().mo5608dq().mo3150a(goo, f);
    }

    /* renamed from: tD */
    private void m24917tD(int i) {
        bFf().mo5608dq().mo3183b(gon, i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6390aly(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m24912gY();
                return null;
            case 1:
                return m24908ar();
            case 2:
                apP();
                return null;
            case 3:
                m24906aG();
                return null;
            case 4:
                m24911fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m24906aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: package-private */
    public void cancel() {
        switch (bFf().mo6893i(bLp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                break;
        }
        apP();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m24911fg();
    }

    @C5566aOg
    /* renamed from: gZ */
    public void mo15430gZ() {
        switch (bFf().mo6893i(f5107pp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5107pp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5107pp, new Object[0]));
                break;
        }
        m24912gY();
    }

    public String getHandle() {
        switch (bFf().mo6893i(f5106bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5106bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5106bP, new Object[0]));
                break;
        }
        return m24908ar();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo15428a(Mission<? extends C4045yZ> af, String str, LDScriptingController lYVar, long j, int i, boolean z) {
        super.mo10S();
        if (i < 1) {
            throw new IllegalArgumentException("Invalid number of steps for MissionTimer");
        }
        m24909d(af);
        m24905a(str);
        m24914i(lYVar);
        m24910fZ(false);
        m24913ga(z);
        m24916kr(((float) j) / ((float) i));
        m24917tD(i);
        m24915in(cVr() + (1000 * j));
        ((MissionTimer) mo8362mt(cqD())).mo15430gZ();
    }

    @C0064Am(aul = "ead9bcf15c286deb506efa7f857f79fc", aum = 0)
    /* renamed from: ar */
    private String m24908ar() {
        return m24907ao();
    }

    @C0064Am(aul = "08c15f645e09aed6762143d83f323652", aum = 0)
    private void apP() {
        m24910fZ(true);
    }

    @C0064Am(aul = "e1d35d01285cd9838e7b41e14c6c4001", aum = 0)
    /* renamed from: aG */
    private void m24906aG() {
        super.mo70aH();
        int i = 1;
        long j = 0;
        long cVr = cVr();
        long cqF = cqF();
        if (cqD() > 0.0f) {
            while (cVr < cqF) {
                j = cqF - cVr;
                cqF -= (long) (cqD() * 1000.0f);
                i++;
            }
        }
        m24917tD(i);
        ((MissionTimer) mo8362mt(((float) j) / 1000.0f)).mo15430gZ();
    }

    @C0064Am(aul = "2bab25f6e65482f13790215ab514fb06", aum = 0)
    /* renamed from: fg */
    private void m24911fg() {
        cancel();
        super.dispose();
    }
}
