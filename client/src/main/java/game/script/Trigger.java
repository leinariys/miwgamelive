package game.script;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.simulation.Space;
import game.script.space.StellarSystem;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C6348alI;
import logic.data.mbean.C6151ahT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aJX */
/* compiled from: a */
public abstract class Trigger extends Actor implements C1616Xf {

    /* renamed from: Mq */
    public static final C2491fm f3205Mq = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_setRadius_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm awC = null;
    public static final C2491fm awD = null;
    public static final C5663aRz awh = null;
    public static final C2491fm bcF = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uY */
    public static final C2491fm f3206uY = null;
    /* renamed from: vc */
    public static final C2491fm f3207vc = null;
    /* renamed from: ve */
    public static final C2491fm f3208ve = null;
    /* renamed from: vi */
    public static final C2491fm f3209vi = null;
    /* renamed from: vm */
    public static final C2491fm f3210vm = null;
    /* renamed from: vn */
    public static final C2491fm f3211vn = null;
    /* renamed from: vo */
    public static final C2491fm f3212vo = null;
    /* renamed from: vs */
    public static final C2491fm f3213vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8e3d856ab1b2aa14b9eeea104d74c87c", aum = 0)
    private static float radius;

    static {
        m15752V();
    }

    public Trigger() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Trigger(C5540aNg ang) {
        super(ang);
    }

    public Trigger(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m15752V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 1;
        _m_methodCount = Actor._m_methodCount + 14;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(Trigger.class, "8e3d856ab1b2aa14b9eeea104d74c87c", i);
        awh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i3 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 14)];
        C2491fm a = C4105zY.m41624a(Trigger.class, "e2b374623fdd3c2ba86bfe7d370661f9", i3);
        bcF = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(Trigger.class, "99d14b3acb8949d3268bb51b5c9c796b", i4);
        f3213vs = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(Trigger.class, "18c9adefa503d21bfbcc0c0a50b0f505", i5);
        _f_setRadius_0020_0028F_0029V = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(Trigger.class, "a4d18d6ca90b04f75fe38401c7f65aa6", i6);
        f3206uY = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(Trigger.class, "91fed8b6aeedededfa25c894b06564ed", i7);
        f3205Mq = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(Trigger.class, "9314145f8398bb3499c06fa27fd4c3bd", i8);
        f3210vm = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(Trigger.class, "9bc12fb64675e1d6744629da1639033c", i9);
        f3212vo = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(Trigger.class, "3fb6b349cf226d4dc4fab3baf81e3b2e", i10);
        f3211vn = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(Trigger.class, "59f5159fee95b8ba8ac8185be7a02043", i11);
        f3207vc = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(Trigger.class, "7178269e21ae01d63c5bfd8def6c8b86", i12);
        f3208ve = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(Trigger.class, "26a812265100c31b61b4c1d508b6df6c", i13);
        awC = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(Trigger.class, "6c0126aeea4f970fae4e104182529cbc", i14);
        awD = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(Trigger.class, "e7d9398cccac4e05da2df900d8047a58", i15);
        _f_onResurrect_0020_0028_0029V = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        C2491fm a14 = C4105zY.m41624a(Trigger.class, "d03d9628d9045d618d762d1e0f749076", i16);
        f3209vi = a14;
        fmVarArr[i16] = a14;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Trigger.class, C6151ahT.class, _m_fields, _m_methods);
    }

    /* renamed from: Ln */
    private float m15751Ln() {
        return bFf().mo5608dq().mo3211m(awh);
    }

    /* renamed from: aU */
    private void m15759aU(float f) {
        bFf().mo5608dq().mo3150a(awh, f);
    }

    @C4034yP
    /* renamed from: B */
    public void mo7552B(Actor cr) {
        switch (bFf().mo6893i(awD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                break;
        }
        m15750A(cr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6151ahT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return new Float(m15753Zg());
            case 1:
                m15754a((Actor.C0200h) args[0]);
                return null;
            case 2:
                m15757aA(((Float) args[0]).floatValue());
                return null;
            case 3:
                return m15760c((Space) args[0], ((Long) args[1]).longValue());
            case 4:
                m15756a((StellarSystem) args[0]);
                return null;
            case 5:
                m15755a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 6:
                return m15765iz();
            case 7:
                m15761c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 8:
                return m15762io();
            case 9:
                return m15763iq();
            case 10:
                m15766y((Actor) args[0]);
                return null;
            case 11:
                m15750A((Actor) args[0]);
                return null;
            case 12:
                m15758aG();
                return null;
            case 13:
                return m15764it();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m15758aG();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f3213vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3213vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3213vs, new Object[]{hVar}));
                break;
        }
        m15754a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f3210vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3210vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3210vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m15755a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f3205Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3205Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3205Mq, new Object[]{jj}));
                break;
        }
        m15756a(jj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f3206uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f3206uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f3206uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m15760c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f3211vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3211vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3211vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m15761c(cr, acm, vec3f, vec3f2);
    }

    public float getRadius() {
        switch (bFf().mo6893i(bcF)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bcF, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcF, new Object[0]));
                break;
        }
        return m15753Zg();
    }

    public void setRadius(float f) {
        switch (bFf().mo6893i(_f_setRadius_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m15757aA(f);
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f3212vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3212vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3212vo, new Object[0]));
                break;
        }
        return m15765iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f3207vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3207vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3207vc, new Object[0]));
                break;
        }
        return m15762io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f3208ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3208ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3208ve, new Object[0]));
                break;
        }
        return m15763iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f3209vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3209vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3209vi, new Object[0]));
                break;
        }
        return m15764it();
    }

    @C4034yP
    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m15766y(cr);
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        mo1054cO(true);
    }

    @C0064Am(aul = "e2b374623fdd3c2ba86bfe7d370661f9", aum = 0)
    /* renamed from: Zg */
    private float m15753Zg() {
        return m15751Ln();
    }

    @C0064Am(aul = "99d14b3acb8949d3268bb51b5c9c796b", aum = 0)
    /* renamed from: a */
    private void m15754a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(m15751Ln()));
    }

    @C0064Am(aul = "18c9adefa503d21bfbcc0c0a50b0f505", aum = 0)
    /* renamed from: aA */
    private void m15757aA(float f) {
        m15759aU(f);
    }

    @C0064Am(aul = "a4d18d6ca90b04f75fe38401c7f65aa6", aum = 0)
    /* renamed from: c */
    private C0520HN m15760c(Space ea, long j) {
        C2750jU jUVar = new C2750jU(ea, this, mo958IL());
        jUVar.mo2449a(ea.cKn().mo5725a(j, (C2235dB) jUVar, 13));
        return jUVar;
    }

    @C0064Am(aul = "91fed8b6aeedededfa25c894b06564ed", aum = 0)
    /* renamed from: a */
    private void m15756a(StellarSystem jj) {
        super.mo993b(jj);
        if (bHa()) {
            mo1054cO(true);
        }
    }

    @C0064Am(aul = "9314145f8398bb3499c06fa27fd4c3bd", aum = 0)
    /* renamed from: a */
    private void m15755a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "9bc12fb64675e1d6744629da1639033c", aum = 0)
    /* renamed from: iz */
    private String m15765iz() {
        return null;
    }

    @C0064Am(aul = "3fb6b349cf226d4dc4fab3baf81e3b2e", aum = 0)
    /* renamed from: c */
    private void m15761c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "59f5159fee95b8ba8ac8185be7a02043", aum = 0)
    /* renamed from: io */
    private String m15762io() {
        return null;
    }

    @C0064Am(aul = "7178269e21ae01d63c5bfd8def6c8b86", aum = 0)
    /* renamed from: iq */
    private String m15763iq() {
        return null;
    }

    @C4034yP
    @C0064Am(aul = "26a812265100c31b61b4c1d508b6df6c", aum = 0)
    /* renamed from: y */
    private void m15766y(Actor cr) {
    }

    @C4034yP
    @C0064Am(aul = "6c0126aeea4f970fae4e104182529cbc", aum = 0)
    /* renamed from: A */
    private void m15750A(Actor cr) {
    }

    @C0064Am(aul = "e7d9398cccac4e05da2df900d8047a58", aum = 0)
    /* renamed from: aG */
    private void m15758aG() {
        super.mo70aH();
        mo1054cO(true);
    }

    @C0064Am(aul = "d03d9628d9045d618d762d1e0f749076", aum = 0)
    /* renamed from: it */
    private String m15764it() {
        return null;
    }
}
