package game.script.util;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.aRJ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.atO  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class GameObjectAdapter<T extends GameObjectAdapter> extends TaikodomObject implements C1616Xf {

    /* renamed from: PF */
    public static final C2491fm f5312PF = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm apF = null;
    public static final C2491fm apG = null;
    public static final C5663aRz bZL = null;
    public static final C5663aRz gBV = null;
    public static final C5663aRz gBX = null;
    public static final C2491fm gBY = null;
    public static final C2491fm gBZ = null;
    public static final C2491fm gCa = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d382d7e1b66bd46a18445b17f3827c66", aum = 0)
    private static GameObjectAdapter bZK;
    @C0064Am(aul = "58352868fe777fb1e180969d77f3d62e", aum = 1)
    private static GameObjectAdapter gBU;
    @C0064Am(aul = "745281c2842db8f232e75c8b2418de9b", aum = 2)
    private static GameObjectAdapter gBW;

    static {
        m25844V();
    }

    public GameObjectAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GameObjectAdapter(C5540aNg ang) {
        super(ang);
    }

    public GameObjectAdapter(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m25844V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(GameObjectAdapter.class, "d382d7e1b66bd46a18445b17f3827c66", i);
        bZL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(GameObjectAdapter.class, "58352868fe777fb1e180969d77f3d62e", i2);
        gBV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(GameObjectAdapter.class, "745281c2842db8f232e75c8b2418de9b", i3);
        gBX = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(GameObjectAdapter.class, "aafc18fd32a91f94316ac69b5ffbef10", i5);
        f5312PF = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(GameObjectAdapter.class, "109a6caa5efb04c977d36d899ebedc74", i6);
        gBY = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(GameObjectAdapter.class, "5ee27f7a3b371ba536575d57f11c8461", i7);
        apG = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(GameObjectAdapter.class, "8664d0de8ced11409431b4bcb5b60e68", i8);
        gBZ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(GameObjectAdapter.class, "b0ecf14a803190c0e78da72b652c646d", i9);
        apF = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(GameObjectAdapter.class, "a6b80319ac20c4a4840c95e759c01584", i10);
        gCa = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GameObjectAdapter.class, aRJ.class, _m_fields, _m_methods);
    }

    private GameObjectAdapter asa() {
        return (GameObjectAdapter) bFf().mo5608dq().mo3214p(bZL);
    }

    /* renamed from: b */
    private void m25847b(GameObjectAdapter ato) {
        bFf().mo5608dq().mo3197f(bZL, ato);
    }

    private GameObjectAdapter cwG() {
        return (GameObjectAdapter) bFf().mo5608dq().mo3214p(gBV);
    }

    private GameObjectAdapter cwH() {
        return (GameObjectAdapter) bFf().mo5608dq().mo3214p(gBX);
    }

    /* renamed from: j */
    private void m25848j(GameObjectAdapter ato) {
        bFf().mo5608dq().mo3197f(gBV, ato);
    }

    /* renamed from: k */
    private void m25849k(GameObjectAdapter ato) {
        bFf().mo5608dq().mo3197f(gBX, ato);
    }

    /* renamed from: IE */
    public T mo16070IE() {
        switch (bFf().mo6893i(apF)) {
            case 0:
                return null;
            case 2:
                return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, apF, new Object[0]));
                break;
        }
        return m25842ID();
    }

    /* renamed from: IG */
    public T mo16071IG() {
        switch (bFf().mo6893i(apG)) {
            case 0:
                return null;
            case 2:
                return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, apG, new Object[0]));
                break;
        }
        return m25843IF();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aRJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m25845a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                return null;
            case 1:
                cwI();
                return null;
            case 2:
                return m25843IF();
            case 3:
                return cwK();
            case 4:
                return m25842ID();
            case 5:
                return new Boolean(m25846ax((Class) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: ay */
    public boolean mo16072ay(Class cls) {
        switch (bFf().mo6893i(gCa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gCa, new Object[]{cls}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gCa, new Object[]{cls}));
                break;
        }
        return m25846ax(cls);
    }

    /* renamed from: b */
    public void mo12950b(T t, T t2, T t3) {
        switch (bFf().mo6893i(f5312PF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5312PF, new Object[]{t, t2, t3}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5312PF, new Object[]{t, t2, t3}));
                break;
        }
        m25845a(t, t2, t3);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cwJ() {
        switch (bFf().mo6893i(gBY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gBY, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gBY, new Object[0]));
                break;
        }
        cwI();
    }

    public T cwL() {
        switch (bFf().mo6893i(gBZ)) {
            case 0:
                return null;
            case 2:
                return (GameObjectAdapter) bFf().mo5606d(new aCE(this, gBZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gBZ, new Object[0]));
                break;
        }
        return cwK();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "aafc18fd32a91f94316ac69b5ffbef10", aum = 0)
    /* renamed from: a */
    private void m25845a(T t, T t2, T t3) {
        m25849k(t2);
        m25847b(t3);
        if (asa() == null) {
            throw new IllegalArgumentException("base can not be null");
        }
        m25848j(t);
    }

    @C0064Am(aul = "109a6caa5efb04c977d36d899ebedc74", aum = 0)
    private void cwI() {
        m25849k((GameObjectAdapter) null);
        m25847b((GameObjectAdapter) null);
        m25848j((GameObjectAdapter) null);
    }

    @C0064Am(aul = "5ee27f7a3b371ba536575d57f11c8461", aum = 0)
    /* renamed from: IF */
    private T m25843IF() {
        return cwG();
    }

    @C0064Am(aul = "8664d0de8ced11409431b4bcb5b60e68", aum = 0)
    private T cwK() {
        return cwH();
    }

    @C0064Am(aul = "b0ecf14a803190c0e78da72b652c646d", aum = 0)
    /* renamed from: ID */
    private T m25842ID() {
        return asa();
    }

    @C0064Am(aul = "a6b80319ac20c4a4840c95e759c01584", aum = 0)
    /* renamed from: ax */
    private boolean m25846ax(Class cls) {
        if (cls == null) {
            return false;
        }
        return cls.isAssignableFrom(getClass());
    }
}
