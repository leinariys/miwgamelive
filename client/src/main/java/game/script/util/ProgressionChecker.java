package game.script.util;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.progression.ProgressionAbility;
import game.script.progression.ProgressionCareer;
import game.script.progression.ProgressionCharacterTemplate;
import logic.baa.*;
import logic.data.mbean.C2239dF;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.atS  reason: case insensitive filesystem */
/* compiled from: a */
public class ProgressionChecker extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aHH = null;
    public static final C2491fm gCo = null;
    public static final C2491fm gCp = null;
    public static final C2491fm gCq = null;
    /* renamed from: pQ */
    public static final C2491fm f5317pQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m25865V();
    }

    public ProgressionChecker() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProgressionChecker(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25865V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 5;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 5)];
        C2491fm a = C4105zY.m41624a(ProgressionChecker.class, "9baae64eba19c9dd9fae1fc061f76878", i);
        f5317pQ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ProgressionChecker.class, "40fa426b0e17cffbe6f3d5b0256c31b0", i2);
        aHH = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ProgressionChecker.class, "8daea6dd78f11f1f6bfb3a548236be66", i3);
        gCo = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ProgressionChecker.class, "0fdfc73e0b01aa92488e1dfaec599d49", i4);
        gCp = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ProgressionChecker.class, "cb34fe8a6b5e55cdc928d19aeffa38dc", i5);
        gCq = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProgressionChecker.class, C2239dF.class, _m_fields, _m_methods);
    }

    /* renamed from: b */
    private void m25869b(C3438ra<ProgressionCharacterTemplate> raVar, aKD akd, Set<String> set, boolean z) {
        switch (bFf().mo6893i(gCo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gCo, new Object[]{raVar, akd, set, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gCo, new Object[]{raVar, akd, set, new Boolean(z)}));
                break;
        }
        m25866a(raVar, akd, set, z);
    }

    /* renamed from: b */
    private void m25870b(Collection<ProgressionAbility> collection, aKD akd, Set<String> set, boolean z) {
        switch (bFf().mo6893i(gCq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gCq, new Object[]{collection, akd, set, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gCq, new Object[]{collection, akd, set, new Boolean(z)}));
                break;
        }
        m25867a(collection, akd, set, z);
    }

    /* renamed from: b */
    private boolean m25871b(C0468GU gu, aKD akd, Set<String> set, C5663aRz... arzArr) {
        switch (bFf().mo6893i(aHH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aHH, new Object[]{gu, akd, set, arzArr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aHH, new Object[]{gu, akd, set, arzArr}));
                break;
        }
        return m25868a(gu, akd, set, arzArr);
    }

    /* renamed from: d */
    private void m25873d(C3438ra<ProgressionCareer> raVar, aKD akd, Set<String> set, boolean z) {
        switch (bFf().mo6893i(gCp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gCp, new Object[]{raVar, akd, set, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gCp, new Object[]{raVar, akd, set, new Boolean(z)}));
                break;
        }
        m25872c(raVar, akd, set, z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2239dF(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m25874y(((Boolean) args[0]).booleanValue());
            case 1:
                return new Boolean(m25868a((C0468GU) args[0], (aKD) args[1], (Set<String>) (Set) args[2], (C5663aRz[]) args[3]));
            case 2:
                m25866a((C3438ra<ProgressionCharacterTemplate>) (C3438ra) args[0], (aKD) args[1], (Set<String>) (Set) args[2], ((Boolean) args[3]).booleanValue());
                return null;
            case 3:
                m25872c((C3438ra) args[0], (aKD) args[1], (Set) args[2], ((Boolean) args[3]).booleanValue());
                return null;
            case 4:
                m25867a((Collection<ProgressionAbility>) (Collection) args[0], (aKD) args[1], (Set<String>) (Set) args[2], ((Boolean) args[3]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: z */
    public String mo16075z(boolean z) {
        switch (bFf().mo6893i(f5317pQ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5317pQ, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, f5317pQ, new Object[]{new Boolean(z)}));
                break;
        }
        return m25874y(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "9baae64eba19c9dd9fae1fc061f76878", aum = 0)
    /* renamed from: y */
    private String m25874y(boolean z) {
        aKD akd = new aKD();
        HashSet hashSet = new HashSet();
        m25870b((Collection<ProgressionAbility>) ala().aJk().cqR(), akd, (Set<String>) hashSet, z);
        m25873d(ala().aJk().mo15612aD(), akd, hashSet, z);
        m25869b(ala().aJk().cqV(), akd, (Set<String>) hashSet, z);
        StringBuffer stringBuffer = new StringBuffer();
        if (z) {
            stringBuffer.append("*** WARNING: I18N errors are being ignored (" + akd.mo9662a(C6117agl.I18N).size() + ") ***\n\n");
        }
        if (akd.isEmpty()) {
            stringBuffer.append("*** Progression Consistency Check: No errors found ***");
        } else {
            stringBuffer.append("*** Progression Consistency Check: Errors found (" + akd.mo9669jJ(z) + ") ***\n\n");
            for (String append : akd.mo9670jK(z)) {
                stringBuffer.append(append);
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    @C0064Am(aul = "40fa426b0e17cffbe6f3d5b0256c31b0", aum = 0)
    /* renamed from: a */
    private boolean m25868a(C0468GU gu, aKD akd, Set<String> set, C5663aRz[] arzArr) {
        if (gu.getHandle() == null) {
            akd.mo9668j(gu);
            return false;
        }
        if (gu.getHandle().equals(C0468GU.dac)) {
            akd.mo9671k(gu);
        } else if (set.contains(gu.getHandle())) {
            akd.mo9672l(gu);
            return false;
        } else {
            set.add(gu.getHandle());
        }
        for (String a : C2520gI.m31894a(gu, arzArr)) {
            akd.mo9664a(gu, a);
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b  */
    @p001a.C0064Am(aul = "8daea6dd78f11f1f6bfb3a548236be66", aum = 0)
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m25866a(C3438ra<ProgressionCharacterTemplate> r4, p001a.aKD r5, java.util.Set<java.lang.String> r6, boolean r7) {
        /*
            r3 = this;
            java.util.Iterator r1 = r4.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            a.aof r0 = (p001a.C6527aof) r0
            r2 = 0
            a.aRz[] r2 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r3.m25871b((logic.baa.C0468GU) r0, (p001a.aKD) r5, (java.util.Set<java.lang.String>) r6, (logic.res.code.C5663aRz[]) r2)
            if (r0 != 0) goto L_0x0004
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6774atS.m25866a(a.ra, a.aKD, java.util.Set, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b  */
    @p001a.C0064Am(aul = "0fdfc73e0b01aa92488e1dfaec599d49", aum = 0)
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m25872c(C3438ra<ProgressionCareer> r4, p001a.aKD r5, java.util.Set<java.lang.String> r6, boolean r7) {
        /*
            r3 = this;
            java.util.Iterator r1 = r4.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            a.sb r0 = (p001a.C3579sb) r0
            r2 = 0
            a.aRz[] r2 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r3.m25871b((logic.baa.C0468GU) r0, (p001a.aKD) r5, (java.util.Set<java.lang.String>) r6, (logic.res.code.C5663aRz[]) r2)
            if (r0 != 0) goto L_0x0004
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6774atS.m25872c(a.ra, a.aKD, java.util.Set, boolean):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b  */
    @p001a.C0064Am(aul = "cb34fe8a6b5e55cdc928d19aeffa38dc", aum = 0)
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m25867a(java.util.Collection<ProgressionAbility> r4, p001a.aKD r5, java.util.Set<java.lang.String> r6, boolean r7) {
        /*
            r3 = this;
            java.util.Iterator r1 = r4.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            a.LK r0 = (p001a.C0782LK) r0
            r2 = 0
            a.aRz[] r2 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r3.m25871b((logic.baa.C0468GU) r0, (p001a.aKD) r5, (java.util.Set<java.lang.String>) r6, (logic.res.code.C5663aRz[]) r2)
            if (r0 != 0) goto L_0x0004
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C6774atS.m25867a(java.util.Collection, a.aKD, java.util.Set, boolean):void");
    }
}
