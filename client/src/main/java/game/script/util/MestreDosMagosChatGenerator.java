package game.script.util;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.mission.MissionTemplate;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.ayH;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Comparator;

@C5511aMd
@C6485anp
/* renamed from: a.aDN */
/* compiled from: a */
public class MestreDosMagosChatGenerator extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cDU = null;
    public static final C2491fm iOg = null;
    public static final C2491fm iOh = null;
    public static final C2491fm iOi = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m13583V();
    }

    public MestreDosMagosChatGenerator() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MestreDosMagosChatGenerator(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13583V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 4;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 4)];
        C2491fm a = C4105zY.m41624a(MestreDosMagosChatGenerator.class, "f1d0253ac536d503244c49dc92cb18de", i);
        cDU = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(MestreDosMagosChatGenerator.class, "a2b1ee7413f1bd2722d6947c5ed933ee", i2);
        iOg = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(MestreDosMagosChatGenerator.class, "fa681e829659ca42c3dce6acb22bc91b", i3);
        iOh = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(MestreDosMagosChatGenerator.class, "ea334bd477c7a214cab4f3a07b67dc5d", i4);
        iOi = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MestreDosMagosChatGenerator.class, ayH.class, _m_fields, _m_methods);
    }

    /* renamed from: G */
    private I18NString m13582G(MissionTemplate avh) {
        switch (bFf().mo6893i(iOi)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iOi, new Object[]{avh}));
            case 3:
                bFf().mo5606d(new aCE(this, iOi, new Object[]{avh}));
                break;
        }
        return m13581F(avh);
    }

    @C0064Am(aul = "f1d0253ac536d503244c49dc92cb18de", aum = 0)
    @C5566aOg
    private void aEb() {
        throw new aWi(new aCE(this, cDU, new Object[0]));
    }

    /* renamed from: bc */
    private I18NString m13585bc(String str, String str2) {
        switch (bFf().mo6893i(iOh)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iOh, new Object[]{str, str2}));
            case 3:
                bFf().mo5606d(new aCE(this, iOh, new Object[]{str, str2}));
                break;
        }
        return m13584bb(str, str2);
    }

    /* renamed from: o */
    private I18NString m13587o(String... strArr) {
        switch (bFf().mo6893i(iOg)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, iOg, new Object[]{strArr}));
            case 3:
                bFf().mo5606d(new aCE(this, iOg, new Object[]{strArr}));
                break;
        }
        return m13586n(strArr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new ayH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                aEb();
                return null;
            case 1:
                return m13586n((String[]) args[0]);
            case 2:
                return m13584bb((String) args[0], (String) args[1]);
            case 3:
                return m13581F((MissionTemplate) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    public void aEc() {
        switch (bFf().mo6893i(cDU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cDU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cDU, new Object[0]));
                break;
        }
        aEb();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a2b1ee7413f1bd2722d6947c5ed933ee", aum = 0)
    /* renamed from: n */
    private I18NString m13586n(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("Invalid null arg");
        } else if (strArr.length != C1584XI.values().length) {
            throw new IllegalArgumentException("Can't crate I18NString cause the number of incoming texts is not the same as the supported languages");
        } else {
            I18NString i18NString = new I18NString();
            C1584XI[] values = C1584XI.values();
            for (int i = 0; i < values.length; i++) {
                i18NString.set(values[i].getCode(), strArr[i]);
            }
            return i18NString;
        }
    }

    @C0064Am(aul = "fa681e829659ca42c3dce6acb22bc91b", aum = 0)
    /* renamed from: bb */
    private I18NString m13584bb(String str, String str2) {
        I18NString i18NString = new I18NString();
        String str3 = String.valueOf(str) + " - " + str2;
        for (C1584XI code : C1584XI.values()) {
            i18NString.set(code.getCode(), str3);
        }
        return i18NString;
    }

    @C0064Am(aul = "ea334bd477c7a214cab4f3a07b67dc5d", aum = 0)
    /* renamed from: F */
    private I18NString m13581F(MissionTemplate avh) {
        I18NString i18NString = new I18NString();
        for (C1584XI xi : C1584XI.values()) {
            i18NString.set(xi.getCode(), String.valueOf(avh.getHandle()) + " (" + avh.mo711rP().get(xi.getCode()) + ")");
        }
        return i18NString;
    }

    /* renamed from: a.aDN$a */
    class C1779a implements Comparator<MissionTemplate> {
        C1779a() {
        }

        /* renamed from: a */
        public int compare(MissionTemplate avh, MissionTemplate avh2) {
            return avh.getHandle().compareToIgnoreCase(avh2.getHandle());
        }
    }
}
