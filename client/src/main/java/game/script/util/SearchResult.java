package game.script.util;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2271dU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.fu */
/* compiled from: a */
public class SearchResult extends aDJ implements C1616Xf {
    /* renamed from: Kj */
    public static final C5663aRz f7478Kj = null;
    /* renamed from: Kk */
    public static final C5663aRz f7479Kk = null;
    /* renamed from: Kl */
    public static final C2491fm f7480Kl = null;
    /* renamed from: Km */
    public static final C2491fm f7481Km = null;
    /* renamed from: Kn */
    public static final C2491fm f7482Kn = null;
    /* renamed from: Ko */
    public static final C2491fm f7483Ko = null;
    /* renamed from: Kp */
    public static final C2491fm f7484Kp = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bbed11532d6db310f9632ae2b4a5f05b", aum = 1)

    /* renamed from: AT */
    private static aDJ f7477AT;
    @C0064Am(aul = "6ba5c50764748d90f07d3cee8d1cac43", aum = 0)
    private static String fieldName;

    static {
        m31516V();
    }

    public SearchResult() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SearchResult(C5540aNg ang) {
        super(ang);
    }

    public SearchResult(String str, aDJ adj) {
        super((C5540aNg) null);
        super._m_script_init(str, adj);
    }

    /* renamed from: V */
    static void m31516V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 6;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(SearchResult.class, "6ba5c50764748d90f07d3cee8d1cac43", i);
        f7478Kj = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SearchResult.class, "bbed11532d6db310f9632ae2b4a5f05b", i2);
        f7479Kk = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(SearchResult.class, "c87722cc148dadc635fdff6771e3eca8", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(SearchResult.class, "11dbf496eb66e8effa1d91b2ddfe6628", i5);
        f7480Kl = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(SearchResult.class, "dcc92d9789d54b9284507e4e79adcde1", i6);
        f7481Km = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(SearchResult.class, "7232881d61513df89539668a9dec936d", i7);
        f7482Kn = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(SearchResult.class, "920b37b7d03f2377dec9e56b8ca24c77", i8);
        f7483Ko = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(SearchResult.class, "c823afcbefc97a9dd82b53b4b4256316", i9);
        f7484Kp = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SearchResult.class, C2271dU.class, _m_fields, _m_methods);
    }

    /* renamed from: ag */
    private void m31517ag(String str) {
        bFf().mo5608dq().mo3197f(f7478Kj, str);
    }

    /* renamed from: d */
    private void m31519d(aDJ adj) {
        bFf().mo5608dq().mo3197f(f7479Kk, adj);
    }

    /* renamed from: qi */
    private String m31520qi() {
        return (String) bFf().mo5608dq().mo3214p(f7478Kj);
    }

    /* renamed from: qj */
    private aDJ m31521qj() {
        return (aDJ) bFf().mo5608dq().mo3214p(f7479Kk);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2271dU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m31518au();
            case 1:
                return new Integer(m31522qk());
            case 2:
                return new Boolean(m31526v(args[0]));
            case 3:
                return m31523ql();
            case 4:
                return m31524qm();
            case 5:
                return m31525qo();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public boolean equals(Object obj) {
        switch (bFf().mo6893i(f7481Km)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7481Km, new Object[]{obj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7481Km, new Object[]{obj}));
                break;
        }
        return m31526v(obj);
    }

    public String getFieldName() {
        switch (bFf().mo6893i(f7482Kn)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7482Kn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7482Kn, new Object[0]));
                break;
        }
        return m31523ql();
    }

    public int hashCode() {
        switch (bFf().mo6893i(f7480Kl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f7480Kl, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7480Kl, new Object[0]));
                break;
        }
        return m31522qk();
    }

    /* renamed from: qn */
    public String mo18860qn() {
        switch (bFf().mo6893i(f7483Ko)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7483Ko, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7483Ko, new Object[0]));
                break;
        }
        return m31524qm();
    }

    /* renamed from: qp */
    public String mo18861qp() {
        switch (bFf().mo6893i(f7484Kp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7484Kp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7484Kp, new Object[0]));
                break;
        }
        return m31525qo();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m31518au();
    }

    /* renamed from: a */
    public void mo18858a(String str, aDJ adj) {
        super.mo10S();
        m31517ag(str);
        m31519d(adj);
    }

    @C0064Am(aul = "c87722cc148dadc635fdff6771e3eca8", aum = 0)
    /* renamed from: au */
    private String m31518au() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("* From field [");
        stringBuffer.append(m31520qi() != null ? m31520qi() : "<null>");
        stringBuffer.append("] of ScriptObject [");
        stringBuffer.append(m31521qj() != null ? m31521qj().getClass().getName() : "<null>");
        stringBuffer.append("], ");
        stringBuffer.append(m31521qj() != null ? m31521qj().bFf().getObjectId() : "<null>");
        return stringBuffer.toString();
    }

    @C0064Am(aul = "11dbf496eb66e8effa1d91b2ddfe6628", aum = 0)
    /* renamed from: qk */
    private int m31522qk() {
        int i = 0;
        int hashCode = m31520qi() == null ? 0 : m31520qi().hashCode();
        if (m31521qj() != null) {
            i = m31521qj().hashCode();
        }
        return hashCode + i;
    }

    @C0064Am(aul = "dcc92d9789d54b9284507e4e79adcde1", aum = 0)
    /* renamed from: v */
    private boolean m31526v(Object obj) {
        if (!(obj instanceof SearchResult)) {
            return false;
        }
        SearchResult fuVar = (SearchResult) obj;
        if (m31520qi() == null || m31521qj() == null || !m31520qi().equals(fuVar.m31520qi()) || !m31521qj().equals(fuVar.m31521qj())) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "7232881d61513df89539668a9dec936d", aum = 0)
    /* renamed from: ql */
    private String m31523ql() {
        return m31520qi() == null ? "<null>" : m31520qi();
    }

    @C0064Am(aul = "920b37b7d03f2377dec9e56b8ca24c77", aum = 0)
    /* renamed from: qm */
    private String m31524qm() {
        return m31521qj() == null ? "<null>" : m31521qj().getClass().getName();
    }

    @C0064Am(aul = "c823afcbefc97a9dd82b53b4b4256316", aum = 0)
    /* renamed from: qo */
    private String m31525qo() {
        return m31521qj() == null ? "<null>" : String.valueOf(m31521qj().bFf().getObjectId().getId());
    }
}
