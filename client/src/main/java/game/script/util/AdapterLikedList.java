package game.script.util;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5305aEf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.HashSet;

@C5511aMd
@C6485anp
/* renamed from: a.zA */
/* compiled from: a */
public class AdapterLikedList<T extends GameObjectAdapter> extends TaikodomObject implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f9641Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJH = null;
    public static final C5663aRz bNh = null;
    public static final C5663aRz bZJ = null;
    public static final C5663aRz bZL = null;
    public static final C2491fm bZM = null;
    public static final C2491fm bZN = null;
    public static final C2491fm bZO = null;
    public static final C2491fm bZP = null;
    public static final C2491fm bZQ = null;
    public static final C2491fm bZR = null;
    public static final C2491fm bZS = null;
    public static final C2491fm bZT = null;
    public static final C2491fm bZU = null;
    public static final C2491fm bZV = null;
    public static final C2491fm bZW = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1b9fd33043acd5de1d33f4a34115c7a5", aum = 0)
    @C5566aOg
    private static C2686iZ<C6872avM> bNg;
    @C0064Am(aul = "4f115a53d058a1e3ae7ca605992d0cc4", aum = 1)
    private static GameObjectAdapter bZI;
    @C0064Am(aul = "a4e6c2a002b40a8f5ab228dbd4bb31ba", aum = 2)
    private static GameObjectAdapter bZK;

    static {
        m41482V();
    }

    public AdapterLikedList() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AdapterLikedList(C5540aNg ang) {
        super(ang);
    }

    public AdapterLikedList(T t) {
        super((C5540aNg) null);
        super._m_script_init(t);
    }

    /* renamed from: V */
    static void m41482V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 15;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(AdapterLikedList.class, "1b9fd33043acd5de1d33f4a34115c7a5", i);
        bNh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AdapterLikedList.class, "4f115a53d058a1e3ae7ca605992d0cc4", i2);
        bZJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AdapterLikedList.class, "a4e6c2a002b40a8f5ab228dbd4bb31ba", i3);
        bZL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 15)];
        C2491fm a = C4105zY.m41624a(AdapterLikedList.class, "9a74df6d82ecab4873254cf929c1eaef", i5);
        bZM = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(AdapterLikedList.class, "aab34a7979420144c2c73fc4f3b9c743", i6);
        bZN = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(AdapterLikedList.class, "9b80bc7fc29e1d1c52a8f058d1570471", i7);
        bZO = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(AdapterLikedList.class, "fd5e4e26800b0ba4588e943056bc918e", i8);
        bZP = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(AdapterLikedList.class, "a51e46eade9f0a5990d27a9257bb00c0", i9);
        bZQ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(AdapterLikedList.class, "a55f851fe0b226e13927ab05ae0bdc90", i10);
        aJH = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(AdapterLikedList.class, "6721ae19b055159a6c49951c99edbdf1", i11);
        f9641Lm = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(AdapterLikedList.class, "3e74e0fdb554d2a27dbcc6e75d0c5fe2", i12);
        bZR = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(AdapterLikedList.class, "a8ef066e311a295c5e23800b211050dc", i13);
        bZS = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(AdapterLikedList.class, "a86b7ca7b1c2986eb1c74202000b2e09", i14);
        bZT = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(AdapterLikedList.class, "8c28c432f76b9b4658815f58b51cc3c9", i15);
        bZU = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(AdapterLikedList.class, "1cae750f734002f456f28772081893b5", i16);
        bZV = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(AdapterLikedList.class, "6345f5008a8afe184418030fd336d6fa", i17);
        _f_onResurrect_0020_0028_0029V = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(AdapterLikedList.class, "da640dd3668f7576aad7445973fa77c6", i18);
        bZW = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(AdapterLikedList.class, "6ad52532d7d11cccacef1304e2692ba0", i19);
        _f_dispose_0020_0028_0029V = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AdapterLikedList.class, C5305aEf.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m41483a(GameObjectAdapter ato) {
        bFf().mo5608dq().mo3197f(bZJ, ato);
    }

    private C2686iZ aqU() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(bNh);
    }

    private GameObjectAdapter arZ() {
        return (GameObjectAdapter) bFf().mo5608dq().mo3214p(bZJ);
    }

    private GameObjectAdapter asa() {
        return (GameObjectAdapter) bFf().mo5608dq().mo3214p(bZL);
    }

    @C0064Am(aul = "9a74df6d82ecab4873254cf929c1eaef", aum = 0)
    @C5566aOg
    private T asb() {
        throw new aWi(new aCE(this, bZM, new Object[0]));
    }

    private void asg() {
        switch (bFf().mo6893i(bZS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bZS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bZS, new Object[0]));
                break;
        }
        asf();
    }

    private void asi() {
        switch (bFf().mo6893i(bZT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bZT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bZT, new Object[0]));
                break;
        }
        ash();
    }

    private void aso() {
        switch (bFf().mo6893i(bZW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bZW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bZW, new Object[0]));
                break;
        }
        asn();
    }

    /* renamed from: b */
    private void m41486b(GameObjectAdapter ato) {
        bFf().mo5608dq().mo3197f(bZL, ato);
    }

    @C0064Am(aul = "9b80bc7fc29e1d1c52a8f058d1570471", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m41487d(GameObjectAdapter ato) {
        throw new aWi(new aCE(this, bZO, new Object[]{ato}));
    }

    @C0064Am(aul = "fd5e4e26800b0ba4588e943056bc918e", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m41488f(GameObjectAdapter ato) {
        throw new aWi(new aCE(this, bZP, new Object[]{ato}));
    }

    /* renamed from: o */
    private void m41491o(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(bNh, iZVar);
    }

    @C0064Am(aul = "6721ae19b055159a6c49951c99edbdf1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m41492qU() {
        throw new aWi(new aCE(this, f9641Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5305aEf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return asb();
            case 1:
                return asd();
            case 2:
                m41487d((GameObjectAdapter) args[0]);
                return null;
            case 3:
                m41488f((GameObjectAdapter) args[0]);
                return null;
            case 4:
                return new Boolean(m41490h((GameObjectAdapter) args[0]));
            case 5:
                return new Integer(m41481QD());
            case 6:
                m41492qU();
                return null;
            case 7:
                m41484a((C6872avM) args[0]);
                return null;
            case 8:
                asf();
                return null;
            case 9:
                ash();
                return null;
            case 10:
                return new Boolean(asj());
            case 11:
                asl();
                return null;
            case 12:
                m41485aG();
                return null;
            case 13:
                asn();
                return null;
            case 14:
                m41489fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m41485aG();
    }

    @C5566aOg
    public T asc() {
        switch (bFf().mo6893i(bZM)) {
            case 0:
                return null;
            case 2:
                return (GameObjectAdapter) bFf().mo5606d(new aCE(this, bZM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bZM, new Object[0]));
                break;
        }
        return asb();
    }

    public T ase() {
        switch (bFf().mo6893i(bZN)) {
            case 0:
                return null;
            case 2:
                return (GameObjectAdapter) bFf().mo5606d(new aCE(this, bZN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bZN, new Object[0]));
                break;
        }
        return asd();
    }

    public boolean ask() {
        switch (bFf().mo6893i(bZU)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bZU, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bZU, new Object[0]));
                break;
        }
        return asj();
    }

    public void asm() {
        switch (bFf().mo6893i(bZV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bZV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bZV, new Object[0]));
                break;
        }
        asl();
    }

    /* renamed from: b */
    public void mo23259b(C6872avM avm) {
        switch (bFf().mo6893i(bZR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bZR, new Object[]{avm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bZR, new Object[]{avm}));
                break;
        }
        m41484a(avm);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m41489fg();
    }

    @C5566aOg
    /* renamed from: e */
    public void mo23261e(GameObjectAdapter ato) {
        switch (bFf().mo6893i(bZO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bZO, new Object[]{ato}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bZO, new Object[]{ato}));
                break;
        }
        m41487d(ato);
    }

    @C5566aOg
    /* renamed from: g */
    public void mo23262g(GameObjectAdapter ato) {
        switch (bFf().mo6893i(bZP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bZP, new Object[]{ato}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bZP, new Object[]{ato}));
                break;
        }
        m41488f(ato);
    }

    /* renamed from: i */
    public boolean mo23263i(GameObjectAdapter ato) {
        switch (bFf().mo6893i(bZQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bZQ, new Object[]{ato}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bZQ, new Object[]{ato}));
                break;
        }
        return m41490h(ato);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo23264qV() {
        switch (bFf().mo6893i(f9641Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9641Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9641Lm, new Object[0]));
                break;
        }
        m41492qU();
    }

    public int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m41481QD();
    }

    /* renamed from: c */
    public void mo23260c(T t) {
        super.mo10S();
        if (t.mo16071IG() != null) {
            throw new IllegalArgumentException("base should be the first element on the queue");
        }
        m41486b((GameObjectAdapter) t);
        m41483a((GameObjectAdapter) t);
    }

    @C0064Am(aul = "aab34a7979420144c2c73fc4f3b9c743", aum = 0)
    private T asd() {
        return arZ();
    }

    @C0064Am(aul = "a51e46eade9f0a5990d27a9257bb00c0", aum = 0)
    /* renamed from: h */
    private boolean m41490h(GameObjectAdapter ato) {
        if (ato != null) {
            for (GameObjectAdapter asa = asa(); asa != null; asa = asa.cwL()) {
                if (ato.mo8317Ej() == asa.mo8317Ej()) {
                    return true;
                }
            }
        }
        return false;
    }

    @C0064Am(aul = "a55f851fe0b226e13927ab05ae0bdc90", aum = 0)
    /* renamed from: QD */
    private int m41481QD() {
        int i = 0;
        for (GameObjectAdapter cwL = asa().cwL(); cwL != null; cwL = cwL.cwL()) {
            i++;
        }
        return i;
    }

    @C0064Am(aul = "3e74e0fdb554d2a27dbcc6e75d0c5fe2", aum = 0)
    /* renamed from: a */
    private void m41484a(C6872avM avm) {
        if (!aqU().contains(avm)) {
            aqU().add(avm);
        }
    }

    @C0064Am(aul = "a8ef066e311a295c5e23800b211050dc", aum = 0)
    private void asf() {
        for (C6872avM TN : aqU()) {
            TN.mo12906TN();
        }
    }

    @C0064Am(aul = "a86b7ca7b1c2986eb1c74202000b2e09", aum = 0)
    private void ash() {
        for (C6872avM TL : aqU()) {
            TL.mo12905TL();
        }
    }

    @C0064Am(aul = "8c28c432f76b9b4658815f58b51cc3c9", aum = 0)
    private boolean asj() {
        HashSet hashSet = new HashSet();
        for (GameObjectAdapter asa = asa(); asa != null; asa = asa.cwL()) {
            if (hashSet.contains(Long.valueOf(asa.mo8317Ej()))) {
                mo8358lY("LOOPBUG: Next check : Module/Adapter/Special hability adapter sanity failed. Base is  " + asa().bFY() + " adapter is " + asa.bFY());
                return true;
            }
            hashSet.add(Long.valueOf(asa.mo8317Ej()));
        }
        hashSet.clear();
        for (GameObjectAdapter ase = ase(); ase != null; ase = ase.mo16071IG()) {
            if (hashSet.contains(Long.valueOf(ase.mo8317Ej()))) {
                mo8358lY("LOOPBUG: Previous check : Module/Adapter/Special hability adapter sanity failed. Base is  " + asa().bFY() + " adapter is " + ase.bFY());
                return true;
            }
            hashSet.add(Long.valueOf(ase.mo8317Ej()));
        }
        return false;
    }

    @C0064Am(aul = "1cae750f734002f456f28772081893b5", aum = 0)
    private void asl() {
        System.out.println("Size is " + size() + " + base, total " + size() + 1);
        for (GameObjectAdapter asa = asa(); asa != null; asa = asa.cwL()) {
            System.out.println(" si " + asa.mo8317Ej() + ", prev " + asa.mo16071IG().mo8317Ej() + ", next " + asa.cwL().mo8317Ej());
        }
    }

    @C0064Am(aul = "6345f5008a8afe184418030fd336d6fa", aum = 0)
    /* renamed from: aG */
    private void m41485aG() {
        super.mo70aH();
        if (ask()) {
            aso();
        }
    }

    @C0064Am(aul = "da640dd3668f7576aad7445973fa77c6", aum = 0)
    private void asn() {
        asa().mo12950b(null, null, asa());
        m41483a(asa());
    }

    @C0064Am(aul = "6ad52532d7d11cccacef1304e2692ba0", aum = 0)
    /* renamed from: fg */
    private void m41489fg() {
        super.dispose();
        GameObjectAdapter asa = asa();
        while (asa != null) {
            GameObjectAdapter cwL = asa.cwL();
            asa.dispose();
            asa = cwL;
        }
        m41486b((GameObjectAdapter) null);
        m41483a((GameObjectAdapter) null);
    }
}
