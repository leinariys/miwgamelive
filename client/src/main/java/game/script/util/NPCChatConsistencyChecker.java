package game.script.util;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.agent.Agent;
import game.script.faction.Faction;
import game.script.item.BlueprintType;
import game.script.item.ItemType;
import game.script.mission.MissionTemplate;
import game.script.npc.NPCType;
import game.script.npcchat.*;
import game.script.npcchat.actions.*;
import game.script.npcchat.requirement.*;
import game.script.progression.ProgressionCareer;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C6804atw;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.acq  reason: case insensitive filesystem */
/* compiled from: a */
public class NPCChatConsistencyChecker extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm fbA = null;
    public static final C2491fm fbB = null;
    public static final C2491fm fbC = null;
    public static final C2491fm fbt = null;
    public static final C2491fm fbu = null;
    public static final C2491fm fbv = null;
    public static final C2491fm fbw = null;
    public static final C2491fm fbx = null;
    public static final C2491fm fby = null;
    public static final C2491fm fbz = null;
    /* renamed from: pQ */
    public static final C2491fm f4275pQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m20595V();
    }

    public NPCChatConsistencyChecker() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCChatConsistencyChecker(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20595V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 11)];
        C2491fm a = C4105zY.m41624a(NPCChatConsistencyChecker.class, "2cc3a0fd1406466828bbce5b4065339a", i);
        f4275pQ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "1d1d5cd75231cb69a24d350dc737b1a1", i2);
        fbt = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "e3a297042e09d81bc07928b227eb74b5", i3);
        fbu = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "d34ae0e10179e4ed9222566f5c2dae93", i4);
        fbv = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "696e99d4425a82ed8f46d33231ab4d8c", i5);
        fbw = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "714987cf3cdf41ffdb332aa2c520c155", i6);
        fbx = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "73f7f77f2dc70a7e980614cff512e4ca", i7);
        fby = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "c64bdc24afab1ca5a7412debf05924f3", i8);
        fbz = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "a2d66e32d9d205d04fbbb5c05263115c", i9);
        fbA = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "a32b4f65da09081f2451fecabbfebbd0", i10);
        fbB = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(NPCChatConsistencyChecker.class, "8df1595c5f9f16d6f67d5d387e11f535", i11);
        fbC = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCChatConsistencyChecker.class, C6804atw.class, _m_fields, _m_methods);
    }

    /* renamed from: b */
    private List<C1122QV> m20602b(PlayerSpeech av, List<AbstractSpeech> list, boolean z) {
        switch (bFf().mo6893i(fbx)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbx, new Object[]{av, list, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, fbx, new Object[]{av, list, new Boolean(z)}));
                break;
        }
        return m20596a(av, list, z);
    }

    /* renamed from: b */
    private List<C1122QV> m20603b(AbstractSpeech aht, boolean z) {
        switch (bFf().mo6893i(fbv)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbv, new Object[]{aht, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, fbv, new Object[]{aht, new Boolean(z)}));
                break;
        }
        return m20597a(aht, z);
    }

    /* renamed from: b */
    private List<C1122QV> m20604b(NPCSpeech agl, List<AbstractSpeech> list, boolean z) {
        switch (bFf().mo6893i(fbw)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbw, new Object[]{agl, list, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, fbw, new Object[]{agl, list, new Boolean(z)}));
                break;
        }
        return m20598a(agl, list, z);
    }

    /* renamed from: b */
    private List<SpeechRequirement> m20605b(SpeechRequirementCollection ant) {
        switch (bFf().mo6893i(fbz)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbz, new Object[]{ant}));
            case 3:
                bFf().mo5606d(new aCE(this, fbz, new Object[]{ant}));
                break;
        }
        return m20599a(ant);
    }

    /* renamed from: b */
    private List<C1122QV> m20606b(SpeechRequirement xVar, AbstractSpeech aht) {
        switch (bFf().mo6893i(fbC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbC, new Object[]{xVar, aht}));
            case 3:
                bFf().mo5606d(new aCE(this, fbC, new Object[]{xVar, aht}));
                break;
        }
        return m20600a(xVar, aht);
    }

    /* renamed from: b */
    private List<C1122QV> m20607b(SpeechAction zkVar, PlayerSpeech av) {
        switch (bFf().mo6893i(fbB)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbB, new Object[]{zkVar, av}));
            case 3:
                bFf().mo5606d(new aCE(this, fbB, new Object[]{zkVar, av}));
                break;
        }
        return m20601a(zkVar, av);
    }

    /* renamed from: ea */
    private List<C1122QV> m20610ea(boolean z) {
        switch (bFf().mo6893i(fbu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbu, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, fbu, new Object[]{new Boolean(z)}));
                break;
        }
        return m20608dZ(z);
    }

    /* renamed from: f */
    private boolean m20611f(AbstractSpeech aht) {
        switch (bFf().mo6893i(fbt)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fbt, new Object[]{aht}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fbt, new Object[]{aht}));
                break;
        }
        return m20609e(aht);
    }

    /* renamed from: h */
    private List<C1122QV> m20613h(AbstractSpeech aht) {
        switch (bFf().mo6893i(fbA)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fbA, new Object[]{aht}));
            case 3:
                bFf().mo5606d(new aCE(this, fbA, new Object[]{aht}));
                break;
        }
        return m20612g(aht);
    }

    /* renamed from: l */
    private List<C1122QV> m20615l(PlayerSpeech av) {
        switch (bFf().mo6893i(fby)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fby, new Object[]{av}));
            case 3:
                bFf().mo5606d(new aCE(this, fby, new Object[]{av}));
                break;
        }
        return m20614k(av);
    }

    @C0064Am(aul = "2cc3a0fd1406466828bbce5b4065339a", aum = 0)
    @C5566aOg
    /* renamed from: y */
    private String m20616y(boolean z) {
        throw new aWi(new aCE(this, f4275pQ, new Object[]{new Boolean(z)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6804atw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m20616y(((Boolean) args[0]).booleanValue());
            case 1:
                return new Boolean(m20609e((AbstractSpeech) args[0]));
            case 2:
                return m20608dZ(((Boolean) args[0]).booleanValue());
            case 3:
                return m20597a((AbstractSpeech) args[0], ((Boolean) args[1]).booleanValue());
            case 4:
                return m20598a((NPCSpeech) args[0], (List<AbstractSpeech>) (List) args[1], ((Boolean) args[2]).booleanValue());
            case 5:
                return m20596a((PlayerSpeech) args[0], (List<AbstractSpeech>) (List) args[1], ((Boolean) args[2]).booleanValue());
            case 6:
                return m20614k((PlayerSpeech) args[0]);
            case 7:
                return m20599a((SpeechRequirementCollection) args[0]);
            case 8:
                return m20612g((AbstractSpeech) args[0]);
            case 9:
                return m20601a((SpeechAction) args[0], (PlayerSpeech) args[1]);
            case 10:
                return m20600a((SpeechRequirement) args[0], (AbstractSpeech) args[1]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: z */
    public String mo12705z(boolean z) {
        switch (bFf().mo6893i(f4275pQ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4275pQ, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, f4275pQ, new Object[]{new Boolean(z)}));
                break;
        }
        return m20616y(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "1d1d5cd75231cb69a24d350dc737b1a1", aum = 0)
    /* renamed from: e */
    private boolean m20609e(AbstractSpeech aht) {
        SpeechRequirementCollection ddV = aht.ddV();
        if (ddV == null || (ddV.cme().size() == 0 && ddV.cmc().size() == 0)) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "e3a297042e09d81bc07928b227eb74b5", aum = 0)
    /* renamed from: dZ */
    private List<C1122QV> m20608dZ(boolean z) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList<NPCType> arrayList3 = new ArrayList<>();
        arrayList3.addAll(ala().aJY());
        arrayList3.add(ala().aKM());
        for (NPCType aed : arrayList3) {
            NPCChat bTK = aed.bTK();
            if (bTK != null) {
                if (bTK.clF().isEmpty()) {
                    C1122QV qv = new C1122QV("NPCChat '" + aed.mo11470ke() + "' has no root speech at all");
                    qv.mo5010q(aed);
                    arrayList2.add(qv);
                } else {
                    HashSet hashSet = new HashSet();
                    boolean z2 = false;
                    for (NPCSpeech next : bTK.clF()) {
                        if (hashSet.contains(Integer.valueOf(next.getPriority()))) {
                            arrayList2.add(new C1122QV("NPCSpeech has same priority of another one at the same level", next, aed));
                        }
                        hashSet.add(Integer.valueOf(next.getPriority()));
                        for (C1122QV next2 : m20604b(next, (List<AbstractSpeech>) arrayList, z)) {
                            next2.mo5010q(aed);
                            arrayList2.add(next2);
                        }
                        if (!z2 && m20611f(next)) {
                            z2 = true;
                        }
                    }
                    if (!z2) {
                        C1122QV qv2 = new C1122QV("NPCChat '" + aed.mo11470ke() + "' has no default root speech");
                        qv2.mo5010q(aed);
                        arrayList2.add(qv2);
                    }
                }
            }
        }
        return arrayList2;
    }

    @C0064Am(aul = "d34ae0e10179e4ed9222566f5c2dae93", aum = 0)
    /* renamed from: a */
    private List<C1122QV> m20597a(AbstractSpeech aht, boolean z) {
        if (z) {
            return Collections.emptyList();
        }
        String simpleName = aht.getClass().getSimpleName();
        ArrayList arrayList = new ArrayList();
        for (String str : C2520gI.m31894a(aht, new C5663aRz[0])) {
            arrayList.add(new C1122QV(String.valueOf(simpleName) + ", " + str, aht));
        }
        return arrayList;
    }

    @C0064Am(aul = "696e99d4425a82ed8f46d33231ab4d8c", aum = 0)
    /* renamed from: a */
    private List<C1122QV> m20598a(NPCSpeech agl, List<AbstractSpeech> list, boolean z) {
        if (list.contains(agl)) {
            return Collections.emptyList();
        }
        list.add(agl);
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(m20603b((AbstractSpeech) agl, z));
        arrayList.addAll(m20613h(agl));
        if (agl.bXl().size() == 0) {
            arrayList.add(new C1122QV("NPCSpeech has no children", agl));
        } else {
            boolean z2 = false;
            for (PlayerSpeech next : agl.bXl()) {
                for (C1122QV next2 : m20602b(next, list, z)) {
                    next2.mo5009d(agl);
                    arrayList.add(next2);
                }
                if (!z2 && m20611f(next)) {
                    z2 = true;
                }
            }
            if (!z2) {
                arrayList.add(new C1122QV("NPCSpeech has no default child", agl));
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "714987cf3cdf41ffdb332aa2c520c155", aum = 0)
    /* renamed from: a */
    private List<C1122QV> m20596a(PlayerSpeech av, List<AbstractSpeech> list, boolean z) {
        if (list.contains(av)) {
            return Collections.emptyList();
        }
        list.add(av);
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(m20603b((AbstractSpeech) av, z));
        arrayList.addAll(m20615l(av));
        arrayList.addAll(m20613h(av));
        HashSet hashSet = new HashSet();
        boolean z2 = false;
        for (NPCSpeech next : av.avN()) {
            if (hashSet.contains(Integer.valueOf(next.getPriority()))) {
                arrayList.add(new C1122QV("NPCSpeech has same priority of another one at the same level", av));
            }
            hashSet.add(Integer.valueOf(next.getPriority()));
            for (C1122QV next2 : m20604b(next, list, z)) {
                next2.mo5009d(av);
                arrayList.add(next2);
            }
            if (!z2 && m20611f(next)) {
                z2 = true;
            }
        }
        if (av.avN().size() > 0 && !z2) {
            arrayList.add(new C1122QV("PlayerSpeech has no default child", av));
        }
        return arrayList;
    }

    @C0064Am(aul = "73f7f77f2dc70a7e980614cff512e4ca", aum = 0)
    /* renamed from: k */
    private List<C1122QV> m20614k(PlayerSpeech av) {
        ArrayList arrayList = new ArrayList();
        for (SpeechAction next : av.avP()) {
            arrayList.addAll(m20607b(next, av));
            if (next.arP() != av) {
                arrayList.add(new C1122QV("SpeechAction " + next.getClass().getSimpleName() + " does not belong to this speech. " + "It may have been set by a drag and drop. " + "Delete it and create a new instance", av));
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "c64bdc24afab1ca5a7412debf05924f3", aum = 0)
    /* renamed from: a */
    private List<SpeechRequirement> m20599a(SpeechRequirementCollection ant) {
        ArrayList arrayList = new ArrayList();
        if (ant != null) {
            arrayList.addAll(ant.cmc());
            for (SpeechRequirementCollection b : ant.cme()) {
                arrayList.addAll(m20605b(b));
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "a2d66e32d9d205d04fbbb5c05263115c", aum = 0)
    /* renamed from: g */
    private List<C1122QV> m20612g(AbstractSpeech aht) {
        ArrayList arrayList = new ArrayList();
        for (SpeechRequirement next : m20605b(aht.ddV())) {
            arrayList.addAll(m20606b(next, aht));
            if (next.mo22847at() != aht) {
                arrayList.add(new C1122QV("SpeechRequirement " + next.getClass().getSimpleName() + " does not belong to this speech. " + "It may have been set by a drag and drop. " + "Delete it and create a new instance", aht));
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "a32b4f65da09081f2451fecabbfebbd0", aum = 0)
    /* renamed from: a */
    private List<C1122QV> m20601a(SpeechAction zkVar, PlayerSpeech av) {
        List<MissionTemplate> aJU = ala().aJU();
        List<ItemType> cD = ala().mo1458cD(true);
        ArrayList arrayList = new ArrayList();
        if (zkVar == null) {
            arrayList.add(new C1122QV("SpeechAction is null", av));
        } else if (zkVar instanceof CraftItemSpeechAction) {
            CraftItemSpeechAction auc = (CraftItemSpeechAction) zkVar;
            if (!ala().aKu().contains(auc.cwT())) {
                arrayList.add(new C1122QV("CraftItemSpeechAction has a BluePrintType unrelated to Taikodom: " + auc.cwT(), av));
            }
        } else if (zkVar instanceof GiveItemSpeechAction) {
            GiveItemSpeechAction anc = (GiveItemSpeechAction) zkVar;
            if (!cD.contains(anc.mo10492Sv())) {
                arrayList.add(new C1122QV("GiveItemSpeechAction has a ItemType unrelated to Taikodom: " + anc.mo10492Sv(), av));
            }
        } else if (!(zkVar instanceof GiveMoneySpeechAction)) {
            if (zkVar instanceof OpenMissionBriefingSpeechAction) {
                OpenMissionBriefingSpeechAction zu = (OpenMissionBriefingSpeechAction) zkVar;
                if (!aJU.contains(zu.bfA())) {
                    arrayList.add(new C1122QV("OpenMissionBriefingSpeechAction has a MissionTemplate unrelated to Taikodom: " + zu.bfA(), av));
                }
            } else if (!(zkVar instanceof PostNPCChatEventSpeechAction) && !(zkVar instanceof RepairActiveShipSpeechAction) && !(zkVar instanceof SetParamSpeechAction)) {
                if (zkVar instanceof TakeItemSpeechAction) {
                    TakeItemSpeechAction air = (TakeItemSpeechAction) zkVar;
                    if (!cD.contains(air.cbU())) {
                        arrayList.add(new C1122QV("TakeItemSpeechAction has a ItemType unrelated to Taikodom: " + air.cbU(), av));
                    }
                } else if (!(zkVar instanceof TakeMoneySpeechAction) && !(zkVar instanceof RemoveParamSpeechAction)) {
                    if (zkVar instanceof ChangePlayerFactionSpeechAction) {
                        ChangePlayerFactionSpeechAction le = (ChangePlayerFactionSpeechAction) zkVar;
                        if (le.azN() == null) {
                            arrayList.add(new C1122QV("ChangePlayerFactionSpeechAction has a null Faction", av));
                        } else if (!ala().mo1321Uv().contains(le.azN())) {
                            arrayList.add(new C1122QV("ChangePlayerFactionSpeechAction has Faction unrelated to Taikodom: " + le.azN().getHandle(), av));
                        }
                    } else if (!(zkVar instanceof OpenCorpCreationWindowSpeechAction) && !(zkVar instanceof FormatOniSpeechAction) && !(zkVar instanceof GiveOniMemorySpeechAction) && !(zkVar instanceof azR)) {
                        if (zkVar instanceof NurseryTutorialSpeechAction) {
                            if (((NurseryTutorialSpeechAction) zkVar).mo6605Us() == null) {
                                arrayList.add(new C1122QV("NurseryTutorialSpeechAction has null transition", av));
                            }
                        } else if (zkVar instanceof RecurrentMessageRequestSpeechAction) {
                            if (((RecurrentMessageRequestSpeechAction) zkVar).mo21578YR() == null) {
                                arrayList.add(new C1122QV("RecurrentMessageRequestSpeechAction has null header", av));
                            }
                        } else if (zkVar instanceof AddToKnownAgentsSpeechAction) {
                            Agent bHm = ((AddToKnownAgentsSpeechAction) zkVar).bHm();
                            if (bHm == null) {
                                arrayList.add(new C1122QV("AddToKnownAgentsSpeechAction has null agent", av));
                            } else {
                                Actor bhE = bHm.bhE();
                                if (bhE instanceof Station) {
                                    Station bf = (Station) bhE;
                                    if (!bf.azz().contains(bHm)) {
                                        arrayList.add(new C1122QV("AddToKnownAgentsSpeechAction has an inconsistent Agent (" + bHm.getName() + "). He is at a Station " + bf.getHandle() + ", but the Station does not has it.", av));
                                    }
                                }
                            }
                        } else {
                            arrayList.add(new C1122QV("Unknown SpeechAction subclass: '" + zkVar.getClass().getName() + "', it is not going to be checked. Editor must be updated.", av));
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "8df1595c5f9f16d6f67d5d387e11f535", aum = 0)
    /* renamed from: a */
    private List<C1122QV> m20600a(SpeechRequirement xVar, AbstractSpeech aht) {
        List<MissionTemplate> aJU = ala().aJU();
        List<ItemType> cD = ala().mo1458cD(true);
        ArrayList arrayList = new ArrayList();
        if (xVar == null) {
            arrayList.add(new C1122QV("SpeechRequirement is null", aht));
        } else if (xVar instanceof ActiveMissionSpeechRequirement) {
            ActiveMissionSpeechRequirement apc = (ActiveMissionSpeechRequirement) xVar;
            if (!aJU.contains(apc.cpX())) {
                arrayList.add(new C1122QV("ActiveMissionSpeechRequirement has a MissionTemplate unrelated to Taikodom: " + apc.cpX(), aht));
            }
        } else if (xVar instanceof HasAccomplishMissionSpeechRequirement) {
            HasAccomplishMissionSpeechRequirement ami = (HasAccomplishMissionSpeechRequirement) xVar;
            if (!aJU.contains(ami.cpX())) {
                arrayList.add(new C1122QV("HasAccomplishMissionSpeechRequirement has a MissionTemplate unrelated to Taikodom: " + ami.cpX(), aht));
            }
            if (ami.cpX() != null && ami.cpX().mo709lZ()) {
                arrayList.add(new C1122QV("HasAccomplishMissionSpeechRequirement must not check a recurrent MissionTemplate: " + ami.cpX(), aht));
            }
        } else if (!(xVar instanceof HasActiveShipSpeechRequirement)) {
            if (xVar instanceof HasItemSpeechRequirement) {
                HasItemSpeechRequirement sn = (HasItemSpeechRequirement) xVar;
                if (!cD.contains(sn.mo5467az())) {
                    arrayList.add(new C1122QV("HasItemSpeechRequirement has an ItemType unrelated to Taikodom: " + sn.mo5467az(), aht));
                }
            } else if (!(xVar instanceof HasMoneySpeechRequeriment)) {
                if (xVar instanceof HasNotAccomplishMissionRequirement) {
                    HasNotAccomplishMissionRequirement arn = (HasNotAccomplishMissionRequirement) xVar;
                    if (!aJU.contains(arn.cpX())) {
                        arrayList.add(new C1122QV("HasNotAccomplishMissionSpeechRequirement has a MissionTemplate unrelated to Taikodom: " + arn.cpX(), aht));
                    }
                    if (arn.cpX() != null && arn.cpX().mo709lZ()) {
                        arrayList.add(new C1122QV("HasNotAccomplishMissionSpeechRequirement must not check a recurrent MissionTemplate: " + arn.cpX(), aht));
                    }
                } else if (xVar instanceof HasRoomForItemSpeechAction) {
                    HasRoomForItemSpeechAction are = (HasRoomForItemSpeechAction) xVar;
                    if (!cD.contains(are.mo15845az())) {
                        arrayList.add(new C1122QV("HasRoomForItemSpeechAction has an ItemType unrelated to Taikodom: " + are.mo15845az(), aht));
                    }
                } else if (xVar instanceof NotActiveMissionSpeechRequirement) {
                    NotActiveMissionSpeechRequirement aqj = (NotActiveMissionSpeechRequirement) xVar;
                    if (!aJU.contains(aqj.cpX())) {
                        arrayList.add(new C1122QV("NotActiveMissionSpeechRequirement has a MissionTemplate unrelated to Taikodom: " + aqj.cpX(), aht));
                    }
                } else if (xVar instanceof NotHasItemSpeechRequirement) {
                    NotHasItemSpeechRequirement aqr = (NotHasItemSpeechRequirement) xVar;
                    if (!cD.contains(aqr.mo15669az())) {
                        arrayList.add(new C1122QV("NotHasItemSpeechRequirement has an ItemType unrelated to Taikodom: " + aqr.mo15669az(), aht));
                    }
                } else if (!(xVar instanceof ParamSpeechRequirement)) {
                    if (xVar instanceof HasBlueprintItemSpeechRequirement) {
                        for (BlueprintType next : ((HasBlueprintItemSpeechRequirement) xVar).aKu()) {
                            if (!cD.contains(next)) {
                                arrayList.add(new C1122QV("NotBluePrintItemSpeechRequirement has an ItemType unrelated to Taikodom: " + next, aht));
                            }
                        }
                    } else if (!(xVar instanceof ExpeditionParamSpeechRequirement)) {
                        if (xVar instanceof IsFromFactionSpeechRequirement) {
                            IsFromFactionSpeechRequirement uWVar = (IsFromFactionSpeechRequirement) xVar;
                            if (uWVar.mo22416Uv().isEmpty()) {
                                arrayList.add(new C1122QV("IsFromFactionSpeechRequirement has no Faction. This check is never going to be true", aht));
                            } else {
                                for (Faction next2 : uWVar.mo22416Uv()) {
                                    if (!ala().mo1321Uv().contains(next2)) {
                                        arrayList.add(new C1122QV("IsFromFactionSpeechRequirement has a Faction unrelated to Taikodom: " + next2.getHandle(), aht));
                                    }
                                }
                            }
                        } else if (xVar instanceof IsNotFromFactionSpeechRequirement) {
                            IsNotFromFactionSpeechRequirement oNVar = (IsNotFromFactionSpeechRequirement) xVar;
                            if (oNVar.mo20976Uv().isEmpty()) {
                                arrayList.add(new C1122QV("IsNotFromFactionSpeechRequirement has no Faction. This check is never going to be true", aht));
                            } else {
                                for (Faction next3 : oNVar.mo20976Uv()) {
                                    if (!ala().mo1321Uv().contains(next3)) {
                                        arrayList.add(new C1122QV("IsNotFromFactionSpeechRequirement has a Faction unrelated to Taikodom: " + next3.getHandle(), aht));
                                    }
                                }
                            }
                        } else if (!(xVar instanceof HasCorporationSpeechRequirement) && !(xVar instanceof HasNotCorporationSpeechRequirement) && !(xVar instanceof IsCEOOfACorporation)) {
                            if (xVar instanceof IsDockedInOwnOutpostWithLevelSpeechRequirement) {
                                if (((IsDockedInOwnOutpostWithLevelSpeechRequirement) xVar).brI() == null) {
                                    arrayList.add(new C1122QV("There is no defined required level", aht));
                                }
                            } else if (!(xVar instanceof IsDockedInOwnOutpostSpeechRequirement) && !(xVar instanceof CorporationHasOutpostSpeechRequirement) && !(xVar instanceof CorporationHasNotOutpostSpeechRequirement) && !(xVar instanceof IsAllowedToReserveAnotherOutpostSpeechRequirement)) {
                                if (xVar instanceof NurseryTutorialCompletion) {
                                    if (((NurseryTutorialCompletion) xVar).mo17729ka() == null) {
                                        arrayList.add(new C1122QV("NurseryTutorialCompletion has null completion", aht));
                                    }
                                } else if (xVar instanceof ProgressionCareerSpeechRequeriment) {
                                    ProgressionCareerSpeechRequeriment h = (ProgressionCareerSpeechRequeriment) xVar;
                                    if (h.mo2480aD().isEmpty()) {
                                        arrayList.add(new C1122QV("ProgressionCareerSpeechRequirement has no progression career", aht));
                                    } else {
                                        for (ProgressionCareer sbVar : h.mo2480aD()) {
                                            if (!ala().aJk().mo15612aD().contains(sbVar)) {
                                                arrayList.add(new C1122QV("ProgressionCareerSpeechRequirement has a ProgressionCareer unrelated to Taikodom:" + sbVar.getHandle(), aht));
                                            }
                                        }
                                    }
                                } else {
                                    arrayList.add(new C1122QV("Unknown SpeechRequirement subclass: '" + xVar.getClass().getName() + "', it is not going to be checked. Editor must be updated. Call a developer", aht));
                                }
                            }
                        }
                    }
                }
            }
        }
        return arrayList;
    }
}
