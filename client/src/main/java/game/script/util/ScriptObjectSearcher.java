package game.script.util;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C4036yR;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C0029AO;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.Ay */
/* compiled from: a */
public class ScriptObjectSearcher extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm chO = null;
    public static final C2491fm chP = null;
    public static final C2491fm chQ = null;
    public static final C2491fm chR = null;
    public static final C2491fm chS = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m688V();
    }

    public ScriptObjectSearcher() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScriptObjectSearcher(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m688V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 5;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 5)];
        C2491fm a = C4105zY.m41624a(ScriptObjectSearcher.class, "8624ef45a386fbf54fb078ea7643f7eb", i);
        chO = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ScriptObjectSearcher.class, "913c1494ad4c39db6b8360885271a5df", i2);
        chP = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ScriptObjectSearcher.class, "de32eae1769fe5df6e60657ab3dc7806", i3);
        chQ = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ScriptObjectSearcher.class, "f0df037553b2a4224a4760e3518347fb", i4);
        chR = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ScriptObjectSearcher.class, "8a481cbdc87144f7c6f4b2b78bf3c5e2", i5);
        chS = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScriptObjectSearcher.class, C4036yR.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "de32eae1769fe5df6e60657ab3dc7806", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private Map<aDJ, Set<SearchResult>> m689a(C5983aeH aeh) {
        throw new aWi(new aCE(this, chQ, new Object[]{aeh}));
    }

    @C0064Am(aul = "8a481cbdc87144f7c6f4b2b78bf3c5e2", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m690a(aDJ adj, C5663aRz arz, aDJ adj2, Map<aDJ, Set<SearchResult>> map, C5983aeH aeh) {
        throw new aWi(new aCE(this, chS, new Object[]{adj, arz, adj2, map, aeh}));
    }

    @C0064Am(aul = "f0df037553b2a4224a4760e3518347fb", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m691a(aDJ adj, C5663aRz arz, aDJ adj2, Set<aDJ> set, Map<aDJ, Set<SearchResult>> map, C5983aeH aeh) {
        throw new aWi(new aCE(this, chR, new Object[]{adj, arz, adj2, set, map, aeh}));
    }

    @C5566aOg
    /* renamed from: b */
    private void m693b(aDJ adj, C5663aRz arz, aDJ adj2, Map<aDJ, Set<SearchResult>> map, C5983aeH aeh) {
        switch (bFf().mo6893i(chS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chS, new Object[]{adj, arz, adj2, map, aeh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chS, new Object[]{adj, arz, adj2, map, aeh}));
                break;
        }
        m690a(adj, arz, adj2, map, aeh);
    }

    @C5566aOg
    /* renamed from: b */
    private void m694b(aDJ adj, C5663aRz arz, aDJ adj2, Set<aDJ> set, Map<aDJ, Set<SearchResult>> map, C5983aeH aeh) {
        switch (bFf().mo6893i(chR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chR, new Object[]{adj, arz, adj2, set, map, aeh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chR, new Object[]{adj, arz, adj2, set, map, aeh}));
                break;
        }
        m691a(adj, arz, adj2, set, map, aeh);
    }

    /* renamed from: b */
    private void m695b(aDJ adj, Set<aDJ> set, Map<Class<? extends aDJ>, Integer> map) {
        switch (bFf().mo6893i(chP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, chP, new Object[]{adj, set, map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, chP, new Object[]{adj, set, map}));
                break;
        }
        m692a(adj, set, map);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4036yR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return avU();
            case 1:
                m692a((aDJ) args[0], (Set) args[1], (Map) args[2]);
                return null;
            case 2:
                return m689a((C5983aeH) args[0]);
            case 3:
                m691a((aDJ) args[0], (C5663aRz) args[1], (aDJ) args[2], (Set) args[3], (Map) args[4], (C5983aeH) args[5]);
                return null;
            case 4:
                m690a((aDJ) args[0], (C5663aRz) args[1], (aDJ) args[2], (Map) args[3], (C5983aeH) args[4]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Map<Class<? extends aDJ>, Integer> avV() {
        switch (bFf().mo6893i(chO)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, chO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, chO, new Object[0]));
                break;
        }
        return avU();
    }

    @C5566aOg
    /* renamed from: b */
    public final Map<aDJ, Set<SearchResult>> mo553b(C5983aeH aeh) {
        switch (bFf().mo6893i(chQ)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, chQ, new Object[]{aeh}));
            case 3:
                bFf().mo5606d(new aCE(this, chQ, new Object[]{aeh}));
                break;
        }
        return m689a(aeh);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "8624ef45a386fbf54fb078ea7643f7eb", aum = 0)
    private Map<Class<? extends aDJ>, Integer> avU() {
        HashMap hashMap = new HashMap();
        m695b(ala(), new HashSet(), hashMap);
        return hashMap;
    }

    @C0064Am(aul = "913c1494ad4c39db6b8360885271a5df", aum = 0)
    /* renamed from: a */
    private void m692a(aDJ adj, Set<aDJ> set, Map<Class<? extends aDJ>, Integer> map) {
        if (!set.contains(adj)) {
            set.add(adj);
            Integer num = map.get(adj.getClass());
            if (num == null) {
                map.put(adj.getClass(), 1);
            } else {
                map.put(adj.getClass(), Integer.valueOf(num.intValue() + 1));
            }
            C1616Xf xf = adj;
            for (C5663aRz w : xf.mo25c()) {
                Object w2 = xf.mo6767w(w);
                if (w2 instanceof C0029AO) {
                    for (Object next : (C0029AO) w2) {
                        if (next instanceof aDJ) {
                            m695b((aDJ) next, set, map);
                        }
                    }
                } else if (w2 instanceof C1556Wo) {
                    for (Map.Entry entry : ((C1556Wo) w2).entrySet()) {
                        if (entry.getKey() instanceof aDJ) {
                            m695b((aDJ) entry.getKey(), set, map);
                        }
                        if (entry.getValue() instanceof aDJ) {
                            m695b((aDJ) entry.getValue(), set, map);
                        }
                    }
                } else if (w2 instanceof aDJ) {
                    m695b((aDJ) w2, set, map);
                }
            }
        }
    }
}
