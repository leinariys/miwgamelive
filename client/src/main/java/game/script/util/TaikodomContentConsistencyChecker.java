package game.script.util;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.TaikodomDefaultContents;
import game.script.TaikodomObject;
import game.script.WanderAndShootAIControllerType;
import game.script.agent.Agent;
import game.script.ai.npc.AIControllerType;
import game.script.ai.npc.WanderAndShootAIController;
import game.script.avatar.*;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.body.BodyPieceBank;
import game.script.avatar.body.TextureScheme;
import game.script.avatar.cloth.Cloth;
import game.script.avatar.cloth.ClothCategory;
import game.script.avatar.cloth.CompoundCategory;
import game.script.chat.ChatDefaults;
import game.script.citizenship.CitizenImprovementType;
import game.script.citizenship.CitizenshipPack;
import game.script.citizenship.CitizenshipReward;
import game.script.citizenship.CitizenshipType;
import game.script.cloning.CloningDefaults;
import game.script.consignment.ConsignmentFeeManager;
import game.script.consignment.ConsignmentFeeRange;
import game.script.corporation.CorporationDefaults;
import game.script.corporation.CorporationPermissionDefaults;
import game.script.corporation.CorporationRoleDefaults;
import game.script.crafting.CraftingRecipe;
import game.script.damage.DamageType;
import game.script.faction.Faction;
import game.script.hazardarea.HazardArea.HazardAreaType;
import game.script.item.*;
import game.script.item.buff.module.AttributeBuffType;
import game.script.itembilling.ItemBilling;
import game.script.itembilling.ItemBillingOrder;
import game.script.itemgen.ItemGenEntry;
import game.script.itemgen.ItemGenSet;
import game.script.itemgen.ItemGenTable;
import game.script.mines.MineThrowerWeaponType;
import game.script.mines.MineType;
import game.script.missile.MissileType;
import game.script.missile.MissileWeaponType;
import game.script.mission.MissionTemplate;
import game.script.newmarket.store.Store;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.ship.*;
import game.script.ship.categories.FreighterType;
import game.script.space.*;
import game.script.spacezone.*;
import game.script.spacezone.population.BossNestPopulationControl;
import game.script.spacezone.population.BossNestPopulationControlType;
import game.script.spacezone.population.LinearlyDistribuitedPopulationControlType;
import game.script.spacezone.population.NPCSpawn;
import logic.baa.*;
import logic.data.link.C3040nL;
import logic.data.link.C3599sm;
import logic.data.link.C6990ayd;
import logic.data.mbean.C6828auU;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.classfile.C0147Bi;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.addon.neo.preferences.GUIPrefAddon;
import taikodom.game.script.p003ai.npc.DroneAIController;
import taikodom.infra.script.I18NString;

import java.lang.reflect.Method;
import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.ne */
/* compiled from: a */
public class TaikodomContentConsistencyChecker extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aGS = null;
    public static final C2491fm aGT = null;
    public static final C2491fm aGU = null;
    public static final C2491fm aGV = null;
    public static final C2491fm aGW = null;
    public static final C2491fm aGX = null;
    public static final C2491fm aGY = null;
    public static final C2491fm aGZ = null;
    public static final C2491fm aHA = null;
    public static final C2491fm aHB = null;
    public static final C2491fm aHC = null;
    public static final C2491fm aHD = null;
    public static final C2491fm aHE = null;
    public static final C2491fm aHF = null;
    public static final C2491fm aHG = null;
    public static final C2491fm aHH = null;
    public static final C2491fm aHI = null;
    public static final C2491fm aHJ = null;
    public static final C2491fm aHK = null;
    public static final C2491fm aHL = null;
    public static final C2491fm aHM = null;
    public static final C2491fm aHN = null;
    public static final C2491fm aHO = null;
    public static final C2491fm aHP = null;
    public static final C2491fm aHQ = null;
    public static final C2491fm aHR = null;
    public static final C2491fm aHS = null;
    public static final C2491fm aHT = null;
    public static final C2491fm aHU = null;
    public static final C2491fm aHV = null;
    public static final C2491fm aHW = null;
    public static final C2491fm aHX = null;
    public static final C2491fm aHY = null;
    public static final C2491fm aHZ = null;
    public static final C2491fm aHa = null;
    public static final C2491fm aHb = null;
    public static final C2491fm aHc = null;
    public static final C2491fm aHd = null;
    public static final C2491fm aHe = null;
    public static final C2491fm aHf = null;
    public static final C2491fm aHg = null;
    public static final C2491fm aHh = null;
    public static final C2491fm aHi = null;
    public static final C2491fm aHj = null;
    public static final C2491fm aHk = null;
    public static final C2491fm aHl = null;
    public static final C2491fm aHm = null;
    public static final C2491fm aHn = null;
    public static final C2491fm aHo = null;
    public static final C2491fm aHp = null;
    public static final C2491fm aHq = null;
    public static final C2491fm aHr = null;
    public static final C2491fm aHs = null;
    public static final C2491fm aHt = null;
    public static final C2491fm aHu = null;
    public static final C2491fm aHv = null;
    public static final C2491fm aHw = null;
    public static final C2491fm aHx = null;
    public static final C2491fm aHy = null;
    public static final C2491fm aHz = null;
    public static final C2491fm aIa = null;
    public static final C2491fm aIb = null;
    public static final C2491fm aIc = null;
    public static final C2491fm aId = null;
    public static final C2491fm aIe = null;
    public static final C2491fm aIf = null;
    public static final C2491fm aIg = null;
    public static final C2491fm aIh = null;
    public static final C2491fm aIi = null;
    public static final C2491fm aIj = null;
    /* renamed from: pQ */
    public static final C2491fm f8746pQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m36350V();
    }

    public TaikodomContentConsistencyChecker() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TaikodomContentConsistencyChecker(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36350V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 71;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 71)];
        C2491fm a = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "d40f21890f445ae29ab38e391998e320", i);
        f8746pQ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "b710ce93db2dc22bc2b6d1129b1be607", i2);
        aGS = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "aaf68377504436dfad23661e186e7acc", i3);
        aGT = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "5eaa72068d0fc757a26d3acd8ab5f2ce", i4);
        aGU = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "cfc209321519c60af8efbc343aa5221f", i5);
        aGV = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "9c80d383c4fb85bb76a861880a8f3233", i6);
        aGW = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "8548a7785958a219cbd0aa2cfcfde61f", i7);
        aGX = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "c0a3cb716c65e06ddaf6e46f2ec22e49", i8);
        aGY = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "b9ba181996d15ebfc632bfb3c88e774d", i9);
        aGZ = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "6e42d37e649fac50ffa16521e26a7aa1", i10);
        aHa = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "bd5678364288e485ca2a9b364f8b9852", i11);
        aHb = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "e8914f0b1a1922e8f57bb442f59d2c81", i12);
        aHc = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "2bed5fe10852c0484dcecb8f46c0b3e9", i13);
        aHd = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        C2491fm a14 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "ee954f2a038fc2dcd563d534b1a6d506", i14);
        aHe = a14;
        fmVarArr[i14] = a14;
        int i15 = i14 + 1;
        C2491fm a15 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "b283989db25a5eca66f63cb68311265e", i15);
        aHf = a15;
        fmVarArr[i15] = a15;
        int i16 = i15 + 1;
        C2491fm a16 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "f9ebde751853bef25bda88b282599f6a", i16);
        aHg = a16;
        fmVarArr[i16] = a16;
        int i17 = i16 + 1;
        C2491fm a17 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "4b56d6cadeddccae03e6f7a1b062fcdb", i17);
        aHh = a17;
        fmVarArr[i17] = a17;
        int i18 = i17 + 1;
        C2491fm a18 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "a8ae507f093218d1ef62f2037d3eedfc", i18);
        aHi = a18;
        fmVarArr[i18] = a18;
        int i19 = i18 + 1;
        C2491fm a19 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "14ceb02c9ef020ac059875fd62c7016e", i19);
        aHj = a19;
        fmVarArr[i19] = a19;
        int i20 = i19 + 1;
        C2491fm a20 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "e38960d61ba315f559f28577c6e9f165", i20);
        aHk = a20;
        fmVarArr[i20] = a20;
        int i21 = i20 + 1;
        C2491fm a21 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "9a333ed1041fa42c52f42f4036ea3adf", i21);
        aHl = a21;
        fmVarArr[i21] = a21;
        int i22 = i21 + 1;
        C2491fm a22 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "aa8af26b2903c454096e0315900aa01f", i22);
        aHm = a22;
        fmVarArr[i22] = a22;
        int i23 = i22 + 1;
        C2491fm a23 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "9ac5bade960e75b334dc2859a1160990", i23);
        aHn = a23;
        fmVarArr[i23] = a23;
        int i24 = i23 + 1;
        C2491fm a24 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "d1fa2e06806766ee2e0618f7c6648a76", i24);
        aHo = a24;
        fmVarArr[i24] = a24;
        int i25 = i24 + 1;
        C2491fm a25 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "c8480a7dca14a74ad0df331eff663bd2", i25);
        aHp = a25;
        fmVarArr[i25] = a25;
        int i26 = i25 + 1;
        C2491fm a26 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "9f709a8a8d27461f17af4749afb7a190", i26);
        aHq = a26;
        fmVarArr[i26] = a26;
        int i27 = i26 + 1;
        C2491fm a27 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "32edc4e30c87211eacae060312583135", i27);
        aHr = a27;
        fmVarArr[i27] = a27;
        int i28 = i27 + 1;
        C2491fm a28 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "5762b877e67b00a711fd9e5627eaf991", i28);
        aHs = a28;
        fmVarArr[i28] = a28;
        int i29 = i28 + 1;
        C2491fm a29 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "84076bb0189ce346b7983f2be61bf3b9", i29);
        aHt = a29;
        fmVarArr[i29] = a29;
        int i30 = i29 + 1;
        C2491fm a30 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "e6d1dd362ab55e69b456f730870c4994", i30);
        aHu = a30;
        fmVarArr[i30] = a30;
        int i31 = i30 + 1;
        C2491fm a31 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "1279c9d77cd487ba86e25a58beef2d18", i31);
        aHv = a31;
        fmVarArr[i31] = a31;
        int i32 = i31 + 1;
        C2491fm a32 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "73786287c6d84df13f2fe022053a1d90", i32);
        aHw = a32;
        fmVarArr[i32] = a32;
        int i33 = i32 + 1;
        C2491fm a33 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "4d0b07f078081d4034c50d079fab7a8c", i33);
        aHx = a33;
        fmVarArr[i33] = a33;
        int i34 = i33 + 1;
        C2491fm a34 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "1f9855a54d8d05fa5c67b10e74effa07", i34);
        aHy = a34;
        fmVarArr[i34] = a34;
        int i35 = i34 + 1;
        C2491fm a35 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "c027795f4c20be6cb5dc0f9d6a556f8b", i35);
        aHz = a35;
        fmVarArr[i35] = a35;
        int i36 = i35 + 1;
        C2491fm a36 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "445c2f68ad1f40f49d1718297bd573d6", i36);
        aHA = a36;
        fmVarArr[i36] = a36;
        int i37 = i36 + 1;
        C2491fm a37 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "abdc27a06d0f37fac270be985411cf90", i37);
        aHB = a37;
        fmVarArr[i37] = a37;
        int i38 = i37 + 1;
        C2491fm a38 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "e42a661621b8161201d599377caad2ea", i38);
        aHC = a38;
        fmVarArr[i38] = a38;
        int i39 = i38 + 1;
        C2491fm a39 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "2a48f5dfcd5a5cfcc2c7a5e4562a5f25", i39);
        aHD = a39;
        fmVarArr[i39] = a39;
        int i40 = i39 + 1;
        C2491fm a40 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "cb153c3c37bd8880e60aa82eb3eb5ece", i40);
        aHE = a40;
        fmVarArr[i40] = a40;
        int i41 = i40 + 1;
        C2491fm a41 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "c5d25dba4bc9140ec33e57911451cf86", i41);
        aHF = a41;
        fmVarArr[i41] = a41;
        int i42 = i41 + 1;
        C2491fm a42 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "4dafacd393270a702464e592b893b05a", i42);
        aHG = a42;
        fmVarArr[i42] = a42;
        int i43 = i42 + 1;
        C2491fm a43 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "adf21fe12ecbca5275017d0ccc1cd646", i43);
        aHH = a43;
        fmVarArr[i43] = a43;
        int i44 = i43 + 1;
        C2491fm a44 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "5ccca5c198f7d30f9ba856cb1eee6626", i44);
        aHI = a44;
        fmVarArr[i44] = a44;
        int i45 = i44 + 1;
        C2491fm a45 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "e6771ed087370de054bd243c4bdffb7a", i45);
        aHJ = a45;
        fmVarArr[i45] = a45;
        int i46 = i45 + 1;
        C2491fm a46 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "17b558aa04218c4b1d28f476be0ed5b0", i46);
        aHK = a46;
        fmVarArr[i46] = a46;
        int i47 = i46 + 1;
        C2491fm a47 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "0533c48e9f8e98a91819e0746ba98dc3", i47);
        aHL = a47;
        fmVarArr[i47] = a47;
        int i48 = i47 + 1;
        C2491fm a48 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "2f0931a230c5ad194974fd64202ddcb8", i48);
        aHM = a48;
        fmVarArr[i48] = a48;
        int i49 = i48 + 1;
        C2491fm a49 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "ec35af8f6d2df44f8ad64df00b811c27", i49);
        aHN = a49;
        fmVarArr[i49] = a49;
        int i50 = i49 + 1;
        C2491fm a50 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "293a7b0f0e4b36297140f1a48261d6ad", i50);
        aHO = a50;
        fmVarArr[i50] = a50;
        int i51 = i50 + 1;
        C2491fm a51 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "8069c23c628ce978f879d2fc860e5871", i51);
        aHP = a51;
        fmVarArr[i51] = a51;
        int i52 = i51 + 1;
        C2491fm a52 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "20911db44f7df527728179c59265412f", i52);
        aHQ = a52;
        fmVarArr[i52] = a52;
        int i53 = i52 + 1;
        C2491fm a53 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "88da5641deb2dc473961697c4cb763e0", i53);
        aHR = a53;
        fmVarArr[i53] = a53;
        int i54 = i53 + 1;
        C2491fm a54 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "284edafa3d13368b557986e00b0a6d6b", i54);
        aHS = a54;
        fmVarArr[i54] = a54;
        int i55 = i54 + 1;
        C2491fm a55 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "d0c010a1cee434822458d6e84f9946e1", i55);
        aHT = a55;
        fmVarArr[i55] = a55;
        int i56 = i55 + 1;
        C2491fm a56 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "b836580b661feb18a9b32afd3a18e371", i56);
        aHU = a56;
        fmVarArr[i56] = a56;
        int i57 = i56 + 1;
        C2491fm a57 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "e5e6520dc65e82dff46d207ef89a1e32", i57);
        aHV = a57;
        fmVarArr[i57] = a57;
        int i58 = i57 + 1;
        C2491fm a58 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "36e7ec61ddc54c40d48f0bcc991ad1a5", i58);
        aHW = a58;
        fmVarArr[i58] = a58;
        int i59 = i58 + 1;
        C2491fm a59 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "b70c9dbc45c2d1567a78179f58ce1d0b", i59);
        aHX = a59;
        fmVarArr[i59] = a59;
        int i60 = i59 + 1;
        C2491fm a60 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "2b8ddf11d5b100ab2bbd16c26586d371", i60);
        aHY = a60;
        fmVarArr[i60] = a60;
        int i61 = i60 + 1;
        C2491fm a61 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "2e7ded279c212efff7fe2e4f880551ad", i61);
        aHZ = a61;
        fmVarArr[i61] = a61;
        int i62 = i61 + 1;
        C2491fm a62 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "04198473d658faab96011eeb6a67671c", i62);
        aIa = a62;
        fmVarArr[i62] = a62;
        int i63 = i62 + 1;
        C2491fm a63 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "868db8f6f422ac0680246f6aa60bd595", i63);
        aIb = a63;
        fmVarArr[i63] = a63;
        int i64 = i63 + 1;
        C2491fm a64 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "7aec8848ca595ef2f60f99759d7541c5", i64);
        aIc = a64;
        fmVarArr[i64] = a64;
        int i65 = i64 + 1;
        C2491fm a65 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "9e1cd9235348b9a0018003888bed9054", i65);
        aId = a65;
        fmVarArr[i65] = a65;
        int i66 = i65 + 1;
        C2491fm a66 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "a0a12e09b5017652effa1806c06fd2a4", i66);
        aIe = a66;
        fmVarArr[i66] = a66;
        int i67 = i66 + 1;
        C2491fm a67 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "122573aaa9f4b502321a368c2a0e0cf2", i67);
        aIf = a67;
        fmVarArr[i67] = a67;
        int i68 = i67 + 1;
        C2491fm a68 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "1dab3ce8bf4e7eeddf7eeb3fddeda3fb", i68);
        aIg = a68;
        fmVarArr[i68] = a68;
        int i69 = i68 + 1;
        C2491fm a69 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "eba2e6106e50a741e3d7b84c0d2e9ce6", i69);
        aIh = a69;
        fmVarArr[i69] = a69;
        int i70 = i69 + 1;
        C2491fm a70 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "2ff6c951988351eb174f4f5ee624483a", i70);
        aIi = a70;
        fmVarArr[i70] = a70;
        int i71 = i70 + 1;
        C2491fm a71 = C4105zY.m41624a(TaikodomContentConsistencyChecker.class, "8202386176c3339e9f9b9367fe2062e7", i71);
        aIj = a71;
        fmVarArr[i71] = a71;
        int i72 = i71 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TaikodomContentConsistencyChecker.class, C6828auU.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private static void m36367a(aKD akd, Set<BodyPieceBank> set, AvatarAppearanceType t, Set<BodyPiece> set2, Collection<BodyPiece> collection) {
        boolean z;
        for (BodyPiece next : collection) {
            if (!set2.add(next)) {
                akd.mo9665b(t, "more than one instance of some bodypiece assigned to it [handle: " + next.getHandle() + "]");
            }
            Iterator<BodyPieceBank> it = set.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().mo11214R(next)) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                akd.mo9663a(t, next, "BodyPiece");
            }
        }
    }

    /* renamed from: B */
    private void m36330B(Collection<SceneryType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHj, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHj, new Object[]{collection, akd, set}));
                break;
        }
        m36329A(collection, akd, set);
    }

    /* renamed from: D */
    private void m36332D(Collection<NPCType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHk, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHk, new Object[]{collection, akd, set}));
                break;
        }
        m36331C(collection, akd, set);
    }

    /* renamed from: F */
    private void m36334F(Collection<ItemTypeCategory> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHl, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHl, new Object[]{collection, akd, set}));
                break;
        }
        m36333E(collection, akd, set);
    }

    /* renamed from: H */
    private void m36336H(Collection<DatabaseCategory> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHm, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHm, new Object[]{collection, akd, set}));
                break;
        }
        m36335G(collection, akd, set);
    }

    /* renamed from: J */
    private void m36338J(Collection<TaikopediaEntry> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHn, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHn, new Object[]{collection, akd, set}));
                break;
        }
        m36337I(collection, akd, set);
    }

    /* renamed from: L */
    private void m36340L(Collection<HullType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHp, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHp, new Object[]{collection, akd, set}));
                break;
        }
        m36339K(collection, akd, set);
    }

    /* renamed from: N */
    private void m36342N(Collection<EquippedShipType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHq, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHq, new Object[]{collection, akd, set}));
                break;
        }
        m36341M(collection, akd, set);
    }

    /* renamed from: P */
    private void m36344P(Collection<GateType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHr, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHr, new Object[]{collection, akd, set}));
                break;
        }
        m36343O(collection, akd, set);
    }

    /* renamed from: R */
    private void m36346R(Collection<AsteroidType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHs, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHs, new Object[]{collection, akd, set}));
                break;
        }
        m36345Q(collection, akd, set);
    }

    /* renamed from: T */
    private void m36348T(Collection<AsteroidZoneCategory> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHt, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHt, new Object[]{collection, akd, set}));
                break;
        }
        m36347S(collection, akd, set);
    }

    /* renamed from: V */
    private void m36351V(Collection<SpaceCategory> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHu, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHu, new Object[]{collection, akd, set}));
                break;
        }
        m36349U(collection, akd, set);
    }

    /* renamed from: X */
    private void m36353X(Collection<CargoHoldType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHv, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHv, new Object[]{collection, akd, set}));
                break;
        }
        m36352W(collection, akd, set);
    }

    /* renamed from: Z */
    private void m36355Z(Collection<AdvertiseType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHw, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHw, new Object[]{collection, akd, set}));
                break;
        }
        m36354Y(collection, akd, set);
    }

    /* renamed from: aB */
    private void m36384aB(Collection<BagItemType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aId)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aId, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aId, new Object[]{collection, akd, set}));
                break;
        }
        m36383aA(collection, akd, set);
    }

    /* renamed from: aD */
    private void m36386aD(Collection<HazardAreaType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIe, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIe, new Object[]{collection, akd, set}));
                break;
        }
        m36385aC(collection, akd, set);
    }

    /* renamed from: aF */
    private void m36388aF(Collection<ModuleType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIf, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIf, new Object[]{collection, akd, set}));
                break;
        }
        m36387aE(collection, akd, set);
    }

    /* renamed from: aH */
    private void m36390aH(Collection<AttributeBuffType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIg, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIg, new Object[]{collection, akd, set}));
                break;
        }
        m36389aG(collection, akd, set);
    }

    /* renamed from: aJ */
    private void m36392aJ(Collection<DamageType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIh, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIh, new Object[]{collection, akd, set}));
                break;
        }
        m36391aI(collection, akd, set);
    }

    /* renamed from: ab */
    private void m36394ab(Collection<ClipBoxType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHx, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHx, new Object[]{collection, akd, set}));
                break;
        }
        m36393aa(collection, akd, set);
    }

    /* renamed from: ad */
    private void m36396ad(Collection<ClipType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHy, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHy, new Object[]{collection, akd, set}));
                break;
        }
        m36395ac(collection, akd, set);
    }

    /* renamed from: af */
    private void m36398af(Collection<MerchandiseType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHB, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHB, new Object[]{collection, akd, set}));
                break;
        }
        m36397ae(collection, akd, set);
    }

    /* renamed from: ah */
    private void m36400ah(Collection<BlueprintType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHC, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHC, new Object[]{collection, akd, set}));
                break;
        }
        m36399ag(collection, akd, set);
    }

    /* renamed from: aj */
    private void m36402aj(Collection<ShipType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHD, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHD, new Object[]{collection, akd, set}));
                break;
        }
        m36401ai(collection, akd, set);
    }

    /* renamed from: al */
    private void m36404al(Collection<CruiseSpeedType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHE, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHE, new Object[]{collection, akd, set}));
                break;
        }
        m36403ak(collection, akd, set);
    }

    /* renamed from: an */
    private void m36406an(Collection<RawMaterialType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHF, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHF, new Object[]{collection, akd, set}));
                break;
        }
        m36405am(collection, akd, set);
    }

    /* renamed from: ap */
    private void m36408ap(Collection<WeaponType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHG, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHG, new Object[]{collection, akd, set}));
                break;
        }
        m36407ao(collection, akd, set);
    }

    /* renamed from: ar */
    private void m36410ar(Collection<StellarSystem> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHL, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHL, new Object[]{collection, akd, set}));
                break;
        }
        m36409aq(collection, akd, set);
    }

    /* renamed from: at */
    private void m36412at(Collection<ItemGenSet> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHV, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHV, new Object[]{collection, akd, set}));
                break;
        }
        m36411as(collection, akd, set);
    }

    /* renamed from: av */
    private void m36414av(Collection<LootType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHW, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHW, new Object[]{collection, akd, set}));
                break;
        }
        m36413au(collection, akd, set);
    }

    /* renamed from: ax */
    private void m36416ax(Collection<AIControllerType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIa, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIa, new Object[]{collection, akd, set}));
                break;
        }
        m36415aw(collection, akd, set);
    }

    /* renamed from: az */
    private void m36418az(Collection<C6257ajV> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIb, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIb, new Object[]{collection, akd, set}));
                break;
        }
        m36417ay(collection, akd, set);
    }

    /* renamed from: b */
    private void m36419b(Station bf, aKD akd, Set<String> set, Map<NPC, Station> map) {
        switch (bFf().mo6893i(aHT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHT, new Object[]{bf, akd, set, map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHT, new Object[]{bf, akd, set, map}));
                break;
        }
        m36356a(bf, akd, set, map);
    }

    /* renamed from: b */
    private void m36420b(Actor cr, StellarSystem jj, Node rPVar, aKD akd) {
        switch (bFf().mo6893i(aHS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHS, new Object[]{cr, jj, rPVar, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHS, new Object[]{cr, jj, rPVar, akd}));
                break;
        }
        m36357a(cr, jj, rPVar, akd);
    }

    /* renamed from: b */
    private void m36421b(AvatarManager dq, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIj, new Object[]{dq, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIj, new Object[]{dq, akd, set}));
                break;
        }
        m36358a(dq, akd, set);
    }

    /* renamed from: b */
    private void m36422b(C0405Fe fe, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHX, new Object[]{fe, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHX, new Object[]{fe, akd, set}));
                break;
        }
        m36359a(fe, akd, set);
    }

    /* renamed from: b */
    private void m36423b(StellarSystem jj, aKD akd, Set<String> set, Map<NPC, Station> map) {
        switch (bFf().mo6893i(aHM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHM, new Object[]{jj, akd, set, map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHM, new Object[]{jj, akd, set, map}));
                break;
        }
        m36360a(jj, akd, set, map);
    }

    /* renamed from: b */
    private void m36424b(OutpostType kz, aKD akd) {
        switch (bFf().mo6893i(aHf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHf, new Object[]{kz, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHf, new Object[]{kz, akd}));
                break;
        }
        m36361a(kz, akd);
    }

    /* renamed from: b */
    private void m36425b(CruiseSpeedType nf, aKD akd) {
        switch (bFf().mo6893i(aHP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHP, new Object[]{nf, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHP, new Object[]{nf, akd}));
                break;
        }
        m36362a(nf, akd);
    }

    /* renamed from: b */
    private void m36426b(ShipType ng, aKD akd) {
        switch (bFf().mo6893i(aHN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHN, new Object[]{ng, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHN, new Object[]{ng, akd}));
                break;
        }
        m36363a(ng, akd);
    }

    /* renamed from: b */
    private void m36427b(SpaceZoneTypesManager vt, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHd, new Object[]{vt, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHd, new Object[]{vt, akd, set}));
                break;
        }
        m36364a(vt, akd, set);
    }

    /* renamed from: b */
    private void m36428b(ComponentType aef, aKD akd) {
        switch (bFf().mo6893i(aHK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHK, new Object[]{aef, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHK, new Object[]{aef, akd}));
                break;
        }
        m36365a(aef, akd);
    }

    /* renamed from: b */
    private void m36429b(aKD akd, MissileType avg) {
        switch (bFf().mo6893i(aHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHz, new Object[]{akd, avg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHz, new Object[]{akd, avg}));
                break;
        }
        m36366a(akd, avg);
    }

    /* renamed from: b */
    private void m36430b(MineType amx, aKD akd) {
        switch (bFf().mo6893i(aHA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHA, new Object[]{amx, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHA, new Object[]{amx, akd}));
                break;
        }
        m36368a(amx, akd);
    }

    /* renamed from: b */
    private void m36431b(ItemBilling acVar, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aIi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIi, new Object[]{acVar, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIi, new Object[]{acVar, akd, set}));
                break;
        }
        m36369a(acVar, akd, set);
    }

    /* renamed from: b */
    private void m36432b(FreighterType afn, aKD akd) {
        switch (bFf().mo6893i(aHO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHO, new Object[]{afn, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHO, new Object[]{afn, akd}));
                break;
        }
        m36370a(afn, akd);
    }

    /* renamed from: b */
    private void m36433b(C6257ajV ajv, C6278ajq ajq, aKD akd, boolean z) {
        switch (bFf().mo6893i(aIc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aIc, new Object[]{ajv, ajq, akd, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aIc, new Object[]{ajv, ajq, akd, new Boolean(z)}));
                break;
        }
        m36371a(ajv, ajq, akd, z);
    }

    /* renamed from: b */
    private void m36434b(BulkItemType amh, aKD akd) {
        switch (bFf().mo6893i(aHI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHI, new Object[]{amh, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHI, new Object[]{amh, akd}));
                break;
        }
        m36372a(amh, akd);
    }

    /* renamed from: b */
    private void m36435b(AsteroidZone btVar, aKD akd) {
        switch (bFf().mo6893i(aHY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHY, new Object[]{btVar, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHY, new Object[]{btVar, akd}));
                break;
        }
        m36373a(btVar, akd);
    }

    /* renamed from: b */
    private void m36436b(Gate fFVar, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHR, new Object[]{fFVar, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHR, new Object[]{fFVar, akd, set}));
                break;
        }
        m36374a(fFVar, akd, set);
    }

    /* renamed from: b */
    private void m36437b(NPCZone fwVar, aKD akd) {
        switch (bFf().mo6893i(aHZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHZ, new Object[]{fwVar, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHZ, new Object[]{fwVar, akd}));
                break;
        }
        m36375a(fwVar, akd);
    }

    /* renamed from: b */
    private void m36438b(TaikodomDefaultContents gVVar, aKD akd) {
        switch (bFf().mo6893i(aHa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHa, new Object[]{gVVar, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHa, new Object[]{gVVar, akd}));
                break;
        }
        m36376a(gVVar, akd);
    }

    /* renamed from: b */
    private void m36439b(Store hfVar, aKD akd, Set<String> set, Station bf) {
        switch (bFf().mo6893i(aHU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHU, new Object[]{hfVar, akd, set, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHU, new Object[]{hfVar, akd, set, bf}));
                break;
        }
        m36377a(hfVar, akd, set, bf);
    }

    /* renamed from: b */
    private void m36440b(ItemType jCVar, aKD akd) {
        switch (bFf().mo6893i(aHJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHJ, new Object[]{jCVar, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHJ, new Object[]{jCVar, akd}));
                break;
        }
        m36378a(jCVar, akd);
    }

    /* renamed from: b */
    private void m36441b(Node rPVar, StellarSystem jj, aKD akd, Set<String> set, Map<NPC, Station> map) {
        switch (bFf().mo6893i(aHQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHQ, new Object[]{rPVar, jj, akd, set, map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHQ, new Object[]{rPVar, jj, akd, set, map}));
                break;
        }
        m36379a(rPVar, jj, akd, set, map);
    }

    /* renamed from: b */
    private void m36442b(C4068yr yrVar, aKD akd) {
        switch (bFf().mo6893i(aHo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHo, new Object[]{yrVar, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHo, new Object[]{yrVar, akd}));
                break;
        }
        m36380a(yrVar, akd);
    }

    /* renamed from: b */
    private void m36443b(Collection<CitizenshipPack> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aGT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGT, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGT, new Object[]{collection, akd, set}));
                break;
        }
        m36381a(collection, akd, set);
    }

    /* renamed from: b */
    private boolean m36444b(C0468GU gu, aKD akd, Set<String> set, C5663aRz... arzArr) {
        switch (bFf().mo6893i(aHH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aHH, new Object[]{gu, akd, set, arzArr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aHH, new Object[]{gu, akd, set, arzArr}));
                break;
        }
        return m36382a(gu, akd, set, arzArr);
    }

    /* renamed from: d */
    private void m36446d(Collection<CitizenshipType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aGU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGU, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGU, new Object[]{collection, akd, set}));
                break;
        }
        m36445c(collection, akd, set);
    }

    /* renamed from: f */
    private void m36448f(Collection<CitizenImprovementType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aGV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGV, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGV, new Object[]{collection, akd, set}));
                break;
        }
        m36447e(collection, akd, set);
    }

    /* renamed from: h */
    private void m36450h(Collection<CitizenshipReward> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aGW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGW, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGW, new Object[]{collection, akd, set}));
                break;
        }
        m36449g(collection, akd, set);
    }

    /* renamed from: j */
    private Method m36453j(Class cls) {
        switch (bFf().mo6893i(aGS)) {
            case 0:
                return null;
            case 2:
                return (Method) bFf().mo5606d(new aCE(this, aGS, new Object[]{cls}));
            case 3:
                bFf().mo5606d(new aCE(this, aGS, new Object[]{cls}));
                break;
        }
        return m36451i(cls);
    }

    /* renamed from: j */
    private void m36454j(Collection<AmplifierType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aGX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGX, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGX, new Object[]{collection, akd, set}));
                break;
        }
        m36452i(collection, akd, set);
    }

    /* renamed from: l */
    private void m36456l(Collection<SectorCategory> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aGY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGY, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGY, new Object[]{collection, akd, set}));
                break;
        }
        m36455k(collection, akd, set);
    }

    /* renamed from: n */
    private void m36458n(Collection<ConsignmentFeeManager> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aGZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGZ, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGZ, new Object[]{collection, akd, set}));
                break;
        }
        m36457m(collection, akd, set);
    }

    /* renamed from: p */
    private void m36460p(Collection<Faction> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHb, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHb, new Object[]{collection, akd, set}));
                break;
        }
        m36459o(collection, akd, set);
    }

    /* renamed from: r */
    private void m36462r(Collection<C6544aow> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHc, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHc, new Object[]{collection, akd, set}));
                break;
        }
        m36461q(collection, akd, set);
    }

    /* renamed from: t */
    private void m36464t(Collection<StationType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHe, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHe, new Object[]{collection, akd, set}));
                break;
        }
        m36463s(collection, akd, set);
    }

    /* renamed from: v */
    private void m36466v(Collection<ShipStructureType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHg, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHg, new Object[]{collection, akd, set}));
                break;
        }
        m36465u(collection, akd, set);
    }

    /* renamed from: x */
    private void m36468x(Collection<ShipSectorType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHh, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHh, new Object[]{collection, akd, set}));
                break;
        }
        m36467w(collection, akd, set);
    }

    @C0064Am(aul = "d40f21890f445ae29ab38e391998e320", aum = 0)
    @C5566aOg
    /* renamed from: y */
    private String m36469y(boolean z) {
        throw new aWi(new aCE(this, f8746pQ, new Object[]{new Boolean(z)}));
    }

    /* renamed from: z */
    private void m36471z(Collection<ShieldType> collection, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(aHi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aHi, new Object[]{collection, akd, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aHi, new Object[]{collection, akd, set}));
                break;
        }
        m36470y(collection, akd, set);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6828auU(this);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v8, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v13, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v23, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v15, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v26, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v17, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v17, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v29, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v19, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v19, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v32, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v21, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v35, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v23, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v38, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v25, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v21, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v41, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v27, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v23, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v44, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v29, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v25, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v47, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v31, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v27, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v4, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v50, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v33, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v29, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v53, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v35, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v31, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v8, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v56, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v37, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v33, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v39, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v35, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v10, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v62, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v41, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v65, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v43, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v68, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v45, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v71, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v47, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v37, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v12, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v74, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v49, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v39, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v77, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v51, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v80, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v53, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v83, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v55, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v86, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v57, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v41, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v14, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v90, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v59, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v43, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v93, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v61, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v45, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v96, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v63, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v47, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v99, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v65, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v49, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v102, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v67, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v51, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v105, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v69, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v53, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v108, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v71, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v111, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v73, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v114, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v75, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v55, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v117, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v77, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v57, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v120, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v79, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v59, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v123, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v81, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v61, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v126, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v83, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v63, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v129, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v85, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v65, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v132, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v87, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v67, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v135, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v89, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v69, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v138, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v91, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v71, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v141, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v93, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v73, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v144, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v95, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v147, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v97, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v75, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v150, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v99, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v77, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v153, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v101, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v79, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v156, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v103, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v81, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v159, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v105, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v83, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v162, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v107, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v85, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v165, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v109, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v87, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v168, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v111, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v89, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v171, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v113, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v174, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v115, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v91, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v177, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v117, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v93, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v180, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v119, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v95, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v183, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v121, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v97, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v186, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v123, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v189, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v125, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v99, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v192, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v127, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v101, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v195, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v129, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v103, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v198, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v131, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v105, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v201, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v133, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v107, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v204, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v135, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v109, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v207, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v137, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v111, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v210, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v213, resolved type: java.lang.Object[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object mo14a(p001a.C0495Gr r10) {
        /*
            r9 = this;
            r8 = 3
            r7 = 2
            r6 = 0
            r3 = 1
            r2 = 0
            java.lang.Object[] r5 = r10.getArgs()
            int r0 = r10.mo2417hq()
            int r1 = p001a.C0377FC._m_methodCount
            int r0 = r0 - r1
            switch(r0) {
                case 0: goto L_0x0018;
                case 1: goto L_0x0025;
                case 2: goto L_0x002e;
                case 3: goto L_0x003f;
                case 4: goto L_0x0050;
                case 5: goto L_0x0061;
                case 6: goto L_0x0072;
                case 7: goto L_0x0083;
                case 8: goto L_0x0094;
                case 9: goto L_0x00a6;
                case 10: goto L_0x00b4;
                case 11: goto L_0x00c6;
                case 12: goto L_0x00d8;
                case 13: goto L_0x00ea;
                case 14: goto L_0x00fc;
                case 15: goto L_0x010a;
                case 16: goto L_0x011c;
                case 17: goto L_0x012e;
                case 18: goto L_0x0140;
                case 19: goto L_0x0152;
                case 20: goto L_0x0164;
                case 21: goto L_0x0176;
                case 22: goto L_0x0188;
                case 23: goto L_0x019a;
                case 24: goto L_0x01a8;
                case 25: goto L_0x01ba;
                case 26: goto L_0x01cc;
                case 27: goto L_0x01de;
                case 28: goto L_0x01f0;
                case 29: goto L_0x0202;
                case 30: goto L_0x0214;
                case 31: goto L_0x0226;
                case 32: goto L_0x0238;
                case 33: goto L_0x024a;
                case 34: goto L_0x025c;
                case 35: goto L_0x026a;
                case 36: goto L_0x0278;
                case 37: goto L_0x028a;
                case 38: goto L_0x029c;
                case 39: goto L_0x02ae;
                case 40: goto L_0x02c0;
                case 41: goto L_0x02d2;
                case 42: goto L_0x02e4;
                case 43: goto L_0x0300;
                case 44: goto L_0x030e;
                case 45: goto L_0x031c;
                case 46: goto L_0x032a;
                case 47: goto L_0x033c;
                case 48: goto L_0x0352;
                case 49: goto L_0x0360;
                case 50: goto L_0x036e;
                case 51: goto L_0x037c;
                case 52: goto L_0x0398;
                case 53: goto L_0x03aa;
                case 54: goto L_0x03c0;
                case 55: goto L_0x03d6;
                case 56: goto L_0x03ec;
                case 57: goto L_0x03fe;
                case 58: goto L_0x0410;
                case 59: goto L_0x0422;
                case 60: goto L_0x0430;
                case 61: goto L_0x043e;
                case 62: goto L_0x0450;
                case 63: goto L_0x0462;
                case 64: goto L_0x047c;
                case 65: goto L_0x048e;
                case 66: goto L_0x04a0;
                case 67: goto L_0x04b2;
                case 68: goto L_0x04c4;
                case 69: goto L_0x04d6;
                case 70: goto L_0x04e8;
                default: goto L_0x0013;
            }
        L_0x0013:
            java.lang.Object r0 = super.mo14a((p001a.C0495Gr) r10)
        L_0x0017:
            return r0
        L_0x0018:
            r0 = r5[r2]
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            java.lang.String r0 = r9.m36469y(r0)
            goto L_0x0017
        L_0x0025:
            r0 = r5[r2]
            java.lang.Class r0 = (java.lang.Class) r0
            java.lang.reflect.Method r0 = r9.m36451i(r0)
            goto L_0x0017
        L_0x002e:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36381a((java.util.Collection<p001a.C5918acu>) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2)
            r0 = r6
            goto L_0x0017
        L_0x003f:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36445c(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0050:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36447e(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0061:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36449g(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0072:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36452i(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0083:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36455k(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0094:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36457m(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x00a6:
            r0 = r5[r2]
            a.gV r0 = (p001a.C2536gV) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36376a((p001a.C2536gV) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x00b4:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36459o(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x00c6:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36461q(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x00d8:
            r0 = r5[r2]
            a.Vt r0 = (p001a.C1487Vt) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36364a((p001a.C1487Vt) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2)
            r0 = r6
            goto L_0x0017
        L_0x00ea:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36463s(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x00fc:
            r0 = r5[r2]
            a.KZ r0 = (p001a.C0740KZ) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36361a((p001a.C0740KZ) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x010a:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36465u(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x011c:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36467w(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x012e:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36470y(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0140:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36329A(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0152:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36331C(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0164:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36333E(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0176:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36335G(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0188:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36337I(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x019a:
            r0 = r5[r2]
            a.yr r0 = (logic.baa.C4068yr) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36380a((logic.baa.C4068yr) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x01a8:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36339K(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x01ba:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36341M(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x01cc:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36343O(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x01de:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36345Q(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x01f0:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36347S(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0202:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36349U(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0214:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36352W(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0226:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36354Y(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0238:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36393aa(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x024a:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36395ac(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x025c:
            r0 = r5[r2]
            a.aKD r0 = (p001a.aKD) r0
            r1 = r5[r3]
            a.aVg r1 = (p001a.C5748aVg) r1
            r9.m36366a((p001a.aKD) r0, (p001a.C5748aVg) r1)
            r0 = r6
            goto L_0x0017
        L_0x026a:
            r0 = r5[r2]
            a.aMx r0 = (p001a.C5531aMx) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36368a((p001a.C5531aMx) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x0278:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36397ae(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x028a:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36399ag(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x029c:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36401ai(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x02ae:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36403ak(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x02c0:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36405am(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x02d2:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36407ao(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x02e4:
            java.lang.Boolean r4 = new java.lang.Boolean
            r0 = r5[r2]
            a.GU r0 = (logic.baa.C0468GU) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r3 = r5[r8]
            a.aRz[] r3 = (logic.res.code.C5663aRz[]) r3
            boolean r0 = r9.m36382a((logic.baa.C0468GU) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2, (logic.res.code.C5663aRz[]) r3)
            r4.<init>(r0)
            r0 = r4
            goto L_0x0017
        L_0x0300:
            r0 = r5[r2]
            a.amh r0 = (p001a.C6425amh) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36372a((p001a.C6425amh) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x030e:
            r0 = r5[r2]
            a.jC r0 = (p001a.C2722jC) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36378a((p001a.C2722jC) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x031c:
            r0 = r5[r2]
            a.aEF r0 = (p001a.aEF) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36365a((p001a.aEF) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x032a:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36409aq(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x033c:
            r0 = r5[r2]
            a.Jj r0 = (p001a.C0683Jj) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r3 = r5[r8]
            java.util.Map r3 = (java.util.Map) r3
            r9.m36360a((p001a.C0683Jj) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2, (java.util.Map<p001a.C5721aUf, p001a.C0096BF>) r3)
            r0 = r6
            goto L_0x0017
        L_0x0352:
            r0 = r5[r2]
            a.NG r0 = (p001a.C0902NG) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36363a((p001a.C0902NG) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x0360:
            r0 = r5[r2]
            a.afN r0 = (p001a.C6041afN) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36370a((p001a.C6041afN) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x036e:
            r0 = r5[r2]
            a.NF r0 = (p001a.C0901NF) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36362a((p001a.C0901NF) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x037c:
            r1 = r5[r2]
            a.rP r1 = (p001a.C3422rP) r1
            r2 = r5[r3]
            a.Jj r2 = (p001a.C0683Jj) r2
            r3 = r5[r7]
            a.aKD r3 = (p001a.aKD) r3
            r4 = r5[r8]
            java.util.Set r4 = (java.util.Set) r4
            r0 = 4
            r5 = r5[r0]
            java.util.Map r5 = (java.util.Map) r5
            r0 = r9
            r0.m36379a((p001a.C3422rP) r1, (p001a.C0683Jj) r2, (p001a.aKD) r3, (java.util.Set<java.lang.String>) r4, (java.util.Map<p001a.C5721aUf, p001a.C0096BF>) r5)
            r0 = r6
            goto L_0x0017
        L_0x0398:
            r0 = r5[r2]
            a.fF r0 = (p001a.C2418fF) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36374a((p001a.C2418fF) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2)
            r0 = r6
            goto L_0x0017
        L_0x03aa:
            r0 = r5[r2]
            a.CR r0 = (p001a.C0192CR) r0
            r1 = r5[r3]
            a.Jj r1 = (p001a.C0683Jj) r1
            r2 = r5[r7]
            a.rP r2 = (p001a.C3422rP) r2
            r3 = r5[r8]
            a.aKD r3 = (p001a.aKD) r3
            r9.m36357a((p001a.C0192CR) r0, (p001a.C0683Jj) r1, (p001a.C3422rP) r2, (p001a.aKD) r3)
            r0 = r6
            goto L_0x0017
        L_0x03c0:
            r0 = r5[r2]
            a.BF r0 = (p001a.C0096BF) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r3 = r5[r8]
            java.util.Map r3 = (java.util.Map) r3
            r9.m36356a((p001a.C0096BF) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2, (java.util.Map<p001a.C5721aUf, p001a.C0096BF>) r3)
            r0 = r6
            goto L_0x0017
        L_0x03d6:
            r0 = r5[r2]
            a.hf r0 = (p001a.C2617hf) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r3 = r5[r8]
            a.BF r3 = (p001a.C0096BF) r3
            r9.m36377a((p001a.C2617hf) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2, (p001a.C0096BF) r3)
            r0 = r6
            goto L_0x0017
        L_0x03ec:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36411as(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x03fe:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36413au(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0410:
            r0 = r5[r2]
            a.Fe r0 = (logic.baa.C0405Fe) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36359a((logic.baa.C0405Fe) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2)
            r0 = r6
            goto L_0x0017
        L_0x0422:
            r0 = r5[r2]
            a.bt r0 = (p001a.C2087bt) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36373a((p001a.C2087bt) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x0430:
            r0 = r5[r2]
            a.fw r0 = (p001a.C2504fw) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r9.m36375a((p001a.C2504fw) r0, (p001a.aKD) r1)
            r0 = r6
            goto L_0x0017
        L_0x043e:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36415aw(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0450:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36417ay(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x0462:
            r0 = r5[r2]
            a.ajV r0 = (p001a.C6257ajV) r0
            r1 = r5[r3]
            a.ajq r1 = (p001a.C6278ajq) r1
            r2 = r5[r7]
            a.aKD r2 = (p001a.aKD) r2
            r3 = r5[r8]
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            r9.m36371a((p001a.C6257ajV) r0, (p001a.C6278ajq) r1, (p001a.aKD) r2, (boolean) r3)
            r0 = r6
            goto L_0x0017
        L_0x047c:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36383aA(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x048e:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36385aC(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x04a0:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36387aE(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x04b2:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36389aG(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x04c4:
            r0 = r5[r2]
            java.util.Collection r0 = (java.util.Collection) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36391aI(r0, r1, r2)
            r0 = r6
            goto L_0x0017
        L_0x04d6:
            r0 = r5[r2]
            a.ac r0 = (p001a.C1859ac) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36369a((p001a.C1859ac) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2)
            r0 = r6
            goto L_0x0017
        L_0x04e8:
            r0 = r5[r2]
            a.Dq r0 = (p001a.C0296Dq) r0
            r1 = r5[r3]
            a.aKD r1 = (p001a.aKD) r1
            r2 = r5[r7]
            java.util.Set r2 = (java.util.Set) r2
            r9.m36358a((p001a.C0296Dq) r0, (p001a.aKD) r1, (java.util.Set<java.lang.String>) r2)
            r0 = r6
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3090ne.mo14a(a.Gr):java.lang.Object");
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: z */
    public String mo20835z(boolean z) {
        switch (bFf().mo6893i(f8746pQ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8746pQ, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, f8746pQ, new Object[]{new Boolean(z)}));
                break;
        }
        return m36469y(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "b710ce93db2dc22bc2b6d1129b1be607", aum = 0)
    /* renamed from: i */
    private Method m36451i(Class cls) {
        String str;
        String simpleName = cls.getSimpleName();
        if (simpleName.endsWith(GUIPrefAddon.C4817c.Y)) {
            str = "check" + simpleName.substring(0, simpleName.length() - 1) + "ies";
        } else {
            str = "check" + simpleName + "s";
        }
        for (Method method : TaikodomContentConsistencyChecker.class.getDeclaredMethods()) {
            if (str.equals(method.getName())) {
                return method;
            }
        }
        return null;
    }

    @C0064Am(aul = "aaf68377504436dfad23661e186e7acc", aum = 0)
    /* renamed from: a */
    private void m36381a(Collection<CitizenshipPack> collection, aKD akd, Set<String> set) {
        for (CitizenshipPack next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.mo12738rZ() <= 0) {
                    akd.mo9665b(next, "price equal or less than zero");
                }
                if (next.bOT() <= 0.0f) {
                    akd.mo9665b(next, "days equal or less than zero");
                }
                if (next.bOV() == null) {
                    akd.mo9665b(next, "null citizenshiptype");
                }
            }
        }
    }

    @C0064Am(aul = "5eaa72068d0fc757a26d3acd8ab5f2ce", aum = 0)
    /* renamed from: c */
    private void m36445c(Collection<CitizenshipType> collection, aKD akd, Set<String> set) {
        for (CitizenshipType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.bRJ() == null) {
                    akd.mo9665b(next, "no background image");
                }
                if (next.mo12811sK() == null) {
                    akd.mo9665b(next, "no icon");
                }
                if (next.bRL().size() == 0) {
                    akd.mo9665b(next, "no improvement.");
                } else {
                    for (CitizenImprovementType kt : next.bRL()) {
                        if (!ala().aLI().contains(kt)) {
                            akd.mo9663a(next, kt, "improvement");
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b  */
    @p001a.C0064Am(aul = "cfc209321519c60af8efbc343aa5221f", aum = 0)
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m36447e(java.util.Collection<CitizenImprovementType> r4, p001a.aKD r5, java.util.Set<java.lang.String> r6) {
        /*
            r3 = this;
            java.util.Iterator r1 = r4.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            a.KT r0 = (p001a.C0728KT) r0
            r2 = 0
            a.aRz[] r2 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r3.m36444b((logic.baa.C0468GU) r0, (p001a.aKD) r5, (java.util.Set<java.lang.String>) r6, (logic.res.code.C5663aRz[]) r2)
            if (r0 != 0) goto L_0x0004
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3090ne.m36447e(java.util.Collection, a.aKD, java.util.Set):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b  */
    @p001a.C0064Am(aul = "9c80d383c4fb85bb76a861880a8f3233", aum = 0)
    /* renamed from: g */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m36449g(java.util.Collection<CitizenshipReward> r4, p001a.aKD r5, java.util.Set<java.lang.String> r6) {
        /*
            r3 = this;
            java.util.Iterator r1 = r4.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            a.lQ r0 = (p001a.C2882lQ) r0
            r2 = 0
            a.aRz[] r2 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r3.m36444b((logic.baa.C0468GU) r0, (p001a.aKD) r5, (java.util.Set<java.lang.String>) r6, (logic.res.code.C5663aRz[]) r2)
            if (r0 != 0) goto L_0x0004
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3090ne.m36449g(java.util.Collection, a.aKD, java.util.Set):void");
    }

    @C0064Am(aul = "8548a7785958a219cbd0aa2cfcfde61f", aum = 0)
    /* renamed from: i */
    private void m36452i(Collection<AmplifierType> collection, aKD akd, Set<String> set) {
        for (AmplifierType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36428b((ComponentType) next, akd);
            }
        }
    }

    @C0064Am(aul = "c0a3cb716c65e06ddaf6e46f2ec22e49", aum = 0)
    /* renamed from: k */
    private void m36455k(Collection<SectorCategory> collection, aKD akd, Set<String> set) {
        for (SectorCategory next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.mo18440sK() == null) {
                akd.mo9665b(next, "Sector Category is missing icon.");
            }
        }
    }

    @C0064Am(aul = "b9ba181996d15ebfc632bfb3c88e774d", aum = 0)
    /* renamed from: m */
    private void m36457m(Collection<ConsignmentFeeManager> collection, aKD akd, Set<String> set) {
        for (ConsignmentFeeManager next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.bVz().isEmpty()) {
                    akd.mo9665b(next, "an empty list of fee ranges");
                } else {
                    for (ConsignmentFeeRange next2 : next.bVz()) {
                        if (next2.cIN() < 0) {
                            akd.mo9665b(next, "a fee range with invalid lower bound (" + next2.cIN() + "). It must be non-negative");
                        }
                    }
                }
            }
        }
    }

    @C0064Am(aul = "6e42d37e649fac50ffa16521e26a7aa1", aum = 0)
    /* renamed from: a */
    private void m36376a(TaikodomDefaultContents gVVar, aKD akd) {
        boolean z;
        boolean z2;
        if (gVVar.mo19005xD() <= 0.0f) {
            akd.mo9674mZ("invalid undocked trade range limit: " + gVVar.mo19005xD() + ". It must be positive");
        }
        Faction xf = gVVar.mo19011xf();
        if (xf == null) {
            akd.mo9674mZ("null Faction");
        } else if (!ala().mo1321Uv().contains(xf)) {
            akd.mo9673m(xf);
        }
        CorporationDefaults xb = gVVar.mo19009xb();
        if (xb.awh().isEmpty()) {
            akd.mo9674mZ("no Asset to compose default Corporation logos list");
        }
        for (CorporationRoleDefaults next : xb.awb()) {
            for (String str : C2520gI.m31894a(next, new C5663aRz[0])) {
                akd.mo9674mZ("an error in CorporationRoleDefaults for role '" + next.cyU() + "': " + str);
            }
            if (next.mo16626sK() == null) {
                akd.mo9674mZ("an error in CorporationRoleDefaults for role '" + next.cyU() + "': No icon");
            }
        }
        for (CorporationPermissionDefaults next2 : xb.awd()) {
            for (String str2 : C2520gI.m31894a(next2, new C5663aRz[0])) {
                akd.mo9674mZ("an error in CorporationPermissionDefaults for permission '" + next2.czh() + "': " + str2);
            }
        }
        if (xb.awf() <= 0) {
            akd.mo9674mZ("an error in CorporationDefaults: base max members must is invalid (" + xb.awf() + "). It must be positive");
        }
        CloningDefaults xd = gVVar.mo19010xd();
        if (xd.bSK() == null) {
            akd.mo9674mZ("a CloningDefaults error: null default starting Station 1");
        } else if (xd.bSK().azL() != null) {
            akd.mo9674mZ("a CloningDefaults error: Default station 1 must not have a CloningCenter");
        }
        if (xd.bSM() == null) {
            akd.mo9674mZ("a CloningDefaults error: null default starting Station 2");
        } else if (xd.bSM().azL() != null) {
            akd.mo9674mZ("a CloningDefaults error: Default station 2 must not have a CloningCenter");
        }
        if (xd.bSO() == null) {
            akd.mo9674mZ("a CloningDefaults error: null img asset for starting Station 1");
        }
        if (xd.bSQ() == null) {
            akd.mo9674mZ("a CloningDefaults error: null img asset for starting Station 2");
        }
        if (xd.bTc() == null) {
            akd.mo9674mZ("a CloningDefaults error: null ship type for starting Station 1");
        } else if (!ala().aKC().contains(xd.bTc())) {
            akd.mo9674mZ("a CloningDefaults error: default ship type for Station 1 is not in Ship Types list");
        }
        if (xd.bTe() == null) {
            akd.mo9674mZ("a CloningDefaults error: null ship type for starting Station 2");
        } else if (!ala().aKC().contains(xd.bTe())) {
            akd.mo9674mZ("a CloningDefaults error: default ship type for Station 2 is not in Ship Types list");
        }
        if (xd.bSS() <= 0) {
            akd.mo9674mZ("a CloningDefaults error: invalid max clones number. It must be positive.");
        }
        if (xd.bSY() == null) {
            akd.mo9674mZ("a CloningDefaults error: null default agent 1");
        } else {
            Iterator<Agent> it = xd.bSK().azz().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().equals(xd.bSY())) {
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                    break;
                }
            }
            if (!z) {
                akd.mo9674mZ("a CloningDefaults error: The station " + xd.bSK() + " not contains the agent " + xd.bSY());
            }
        }
        if (xd.bTa() == null) {
            akd.mo9674mZ("a CloningDefaults error: null default agent 2");
        } else {
            Iterator<Agent> it2 = xd.bSM().azz().iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (it2.next().equals(xd.bTa())) {
                        z2 = true;
                        break;
                    }
                } else {
                    z2 = false;
                    break;
                }
            }
            if (!z2) {
                akd.mo9674mZ("a CloningDefaults error: The station " + xd.bSM() + " not contains the agent " + xd.bTa());
            }
        }
        MissionTemplate bSU = xd.bSU();
        if (bSU != null && !ala().aJU().contains(bSU)) {
            akd.mo9674mZ("a CloningDefaults error: default mission template 1 is not in Mission Templates");
        }
        MissionTemplate bSW = xd.bSW();
        if (bSW != null && !ala().aJU().contains(bSW)) {
            akd.mo9674mZ("a CloningDefaults error: default mission template 2 is not in Mission Templates");
        }
        OutpostDefaults wZ = gVVar.mo19003wZ();
        if (wZ.cRl() <= 0) {
            akd.mo9674mZ("an OutpostDefaults error: invalid upkeep period. It must be positive");
        }
        if (wZ.cRn() <= 0) {
            akd.mo9674mZ("an OutpostDefaults error: invalid upkeep penalty period. It must be positive");
        }
        for (Map.Entry next3 : gVVar.mo19004xB().mo20117JG().entrySet()) {
            I18NString i18NString = (I18NString) next3.getValue();
            if (i18NString.get().equals(ChatDefaults.EMPTY)) {
                akd.mo9674mZ("some error on ChatDefaults, context " + next3.getKey() + ": content is still default");
            }
            for (String str3 : C2520gI.m31894a(i18NString, new C5663aRz[0])) {
                akd.mo9674mZ("some error on ChatDefaults, context " + next3.getKey() + ": " + str3);
            }
        }
        if (gVVar.mo19017xr() <= 0.0f) {
            akd.mo9674mZ("invalid storage volume (" + gVVar.mo19017xr() + "): it must be positive");
        }
        if (gVVar.mo19015xn().bmr() <= 0) {
            akd.mo9674mZ("invalid item billing volume (" + gVVar.mo19015xn().bmr() + "): it must be positive");
        }
    }

    @C0064Am(aul = "bd5678364288e485ca2a9b364f8b9852", aum = 0)
    /* renamed from: o */
    private void m36459o(Collection<Faction> collection, aKD akd, Set<String> set) {
        for (Faction next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36442b((C4068yr) next, akd);
                for (Faction next2 : next.bVf()) {
                    if (!ala().mo1321Uv().contains(next)) {
                        akd.mo9663a(next, next2, "enemy faction");
                    }
                    if (next2 == next) {
                        akd.mo9665b(next, "an enemy equal to itself");
                    }
                }
            }
        }
    }

    @C0064Am(aul = "e8914f0b1a1922e8f57bb442f59d2c81", aum = 0)
    /* renamed from: q */
    private void m36461q(Collection<C6544aow> collection, aKD akd, Set<String> set) {
        for (C6544aow next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.clO() > next.getMaxPitch()) {
                    akd.mo9665b(next, "min pitch greater than max pitch");
                }
                if (next.mo14929VH() <= 0.0f) {
                    akd.mo9665b(next, "max angular velocity <= 0");
                }
                if (next.mo14930VT() == null) {
                    akd.mo9665b(next, "no hull");
                }
                if (next.mo14931VX() == null) {
                    akd.mo9665b(next, "no shield");
                }
                if (next.mo14928Nu() == null) {
                    akd.mo9665b(next, "no render asset");
                }
                if (next.clS() == null) {
                    akd.mo9665b(next, "no weapon");
                } else if (!ala().mo1545uZ().contains(next.clS())) {
                    akd.mo9663a(next, next.clS(), "weapon");
                }
                if (next.cny() == null) {
                    akd.mo9666c(next, "for pitch");
                }
                if (next.cnw() == null) {
                    akd.mo9666c(next, "for yaw");
                }
                if (next.cnA() == null) {
                    akd.mo9666c(next, "for shot spawn");
                }
                if (next.clL() == null) {
                    akd.mo9665b(next, "no AI controller class");
                }
            }
        }
    }

    @C0064Am(aul = "2bed5fe10852c0484dcecb8f46c0b3e9", aum = 0)
    /* renamed from: a */
    private void m36364a(SpaceZoneTypesManager vt, aKD akd, Set<String> set) {
        if (vt != null) {
            List<NPCZoneType> bAU = vt.bAU();
            List<PopulationControlType> bAW = vt.bAW();
            List<AsteroidZoneType> bAY = vt.bAY();
            for (NPCZoneType next : bAU) {
                m36444b((C0468GU) next, akd, set, new C5663aRz[0]);
                PopulationControlType aVD = next.aVD();
                if (aVD == null) {
                    akd.mo9665b(next, "has null population control");
                } else if (!bAW.contains(aVD)) {
                    akd.mo9663a(next, aVD, "population type");
                }
            }
            for (PopulationControlType next2 : bAW) {
                m36444b((C0468GU) next2, akd, set, new C5663aRz[0]);
                if (next2 instanceof BossNestPopulationControlType) {
                    if (((BossNestPopulationControlType) next2).dqX().size() == 0) {
                        akd.mo9665b(next2, "an empty boss spawn table");
                    }
                } else if (next2 instanceof LinearlyDistribuitedPopulationControlType) {
                    LinearlyDistribuitedPopulationControlType kx = (LinearlyDistribuitedPopulationControlType) next2;
                    if (kx.bek().size() == 0) {
                        akd.mo9665b(next2, "an empty spawn table");
                    }
                    int i = 0;
                    for (NPCSpawn dfh : kx.bek()) {
                        i = dfh.dfh() + i;
                    }
                    if (i != 100) {
                        akd.mo9665b(next2, "spawnTable dont sum 100%");
                    }
                }
            }
            for (AsteroidZoneType next3 : bAY) {
                m36444b((C0468GU) next3, akd, set, new C5663aRz[0]);
                AsteroidSpawnTable dvi = next3.dvi();
                if (dvi == null || dvi.aKW() == null || dvi.aKW().size() == 0) {
                    akd.mo9665b(next3, "an empty or null spawn table");
                }
                if (next3.dva() == 0.0f) {
                    akd.mo9665b(next3, "the distribution width equals to zero");
                }
                if (next3.dvc() == 0) {
                    akd.mo9665b(next3, "the number of asteroids equals to zero");
                }
                if (((float) next3.dve()) < 5.0f) {
                    akd.mo9665b(next3, "the reposition period shorter than 5.0");
                }
                AsteroidZone.C2089b duS = next3.duS();
                if (duS == AsteroidZone.C2089b.SPIRAL) {
                    if (next3.duY() == 0.0f) {
                        akd.mo9665b(next3, "the number of turns equals to zero");
                    }
                } else if (duS == AsteroidZone.C2089b.ARCH) {
                    if (next3.duW() == 0.0f) {
                        akd.mo9665b(next3, "the distribution angle equals to zero");
                    }
                } else if (duS == null) {
                    akd.mo9665b(next3, "the distribution shape is NULL");
                }
            }
        }
    }

    @C0064Am(aul = "ee954f2a038fc2dcd563d534b1a6d506", aum = 0)
    /* renamed from: s */
    private void m36463s(Collection<StationType> collection, aKD akd, Set<String> set) {
        for (StationType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.bhj() == null) {
                    akd.mo9665b(next, "no AmbienceSFX");
                }
                if (next.mo3521Nu() == null) {
                    akd.mo9665b(next, "no RenderAsset");
                }
                if (next.mo4108rH() == null) {
                    akd.mo9665b(next, "no spawn orientation");
                }
                if (next.mo4109rJ() == null) {
                    akd.mo9665b(next, "no spawn relative position");
                }
                if (next instanceof OutpostType) {
                    m36424b((OutpostType) next, akd);
                }
            }
        }
    }

    @C0064Am(aul = "b283989db25a5eca66f63cb68311265e", aum = 0)
    /* renamed from: a */
    private void m36361a(OutpostType kz, aKD akd) {
        if (kz.mo3587rZ() < 0) {
            akd.mo9665b(kz, "an invalid price value (" + kz.mo3587rZ() + "). It must be non-negative");
        }
        if (kz.bex() <= 0) {
            akd.mo9665b(kz, "an invalid reservation length value (" + kz.bex() + "). It must be positive");
        }
        if (kz.bez().isEmpty()) {
            akd.mo9665b(kz, "no activation material");
        } else {
            for (OutpostActivationItem uBVar : kz.bez()) {
                if (uBVar.getAmount() <= 0) {
                    akd.mo9665b(kz, "some activation item with invalid amount (" + uBVar.getAmount() + "). It must be positive");
                }
                if (!ala().mo1458cD(false).contains(uBVar.mo22322az())) {
                    akd.mo9665b(kz, "some activation item with an itemtype unknown to Taikodom: " + uBVar.mo22322az().getHandle());
                }
            }
        }
        if (kz.beD() == null) {
            akd.mo9665b(kz, "null Category");
        }
        for (OutpostUpgrade axr : kz.beH().values()) {
            if (axr.cDh() <= 0 && axr.mo16752Mb() != C2611hZ.m32750Ac()) {
                akd.mo9665b(kz, "upgrade for level " + axr.mo16752Mb() + " with invalid cost (" + axr.cDh() + "). It must be positive");
            }
            if (axr.cDd() <= 0.0f) {
                akd.mo9665b(kz, "upgrade for level " + axr.mo16752Mb() + " with invalid max storage capacity (" + axr.cDd() + "). It must be positive");
            }
            if (axr.cDj() <= 0) {
                akd.mo9665b(kz, "upgrade for level " + axr.mo16752Mb() + " with invalid time (" + axr.cDj() + "). It must be positive");
            }
        }
        for (OutpostLevelInfo an : kz.beJ().values()) {
            if (an.cEQ() <= 0) {
                akd.mo9665b(kz, an.mo449Mb() + " with invalid upkeep cost (" + an.cEQ() + "). It must be positive");
            }
            if (an.get().isEmpty()) {
                akd.mo9665b(kz, "empty list of upkeep items");
            } else {
                for (OutpostLevelInfo.OutpostUpkeepItem next : an.get()) {
                    if (next.getAmount() <= 0) {
                        akd.mo9665b(kz, an.mo449Mb() + " with invalid upkeep item amount (" + next.getAmount() + "). It must be positive");
                    }
                    if (next.mo463eP() == null) {
                        akd.mo9665b(kz, an.mo449Mb() + " with null upkeep item");
                    } else if (!ala().mo1458cD(false).contains(next.mo463eP())) {
                        akd.mo9665b(kz, an.mo449Mb() + " with upkeep item unknown to Taikodom: " + next.mo463eP().getHandle());
                    }
                }
            }
            if (an.cEO() <= 0) {
                akd.mo9665b(kz, an.mo449Mb() + " with invalid max corp members add (" + an.cEO() + "). It must be positive");
            }
            for (String str : C2520gI.m31894a(an, new C5663aRz[0])) {
                akd.mo9665b(kz, an.mo449Mb() + " with invalid I18NContent: " + str);
            }
        }
    }

    @C0064Am(aul = "f9ebde751853bef25bda88b282599f6a", aum = 0)
    /* renamed from: u */
    private void m36465u(Collection<ShipStructureType> collection, aKD akd, Set<String> set) {
        for (ShipStructureType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.mo9731VX() == null) {
                    akd.mo9665b(next, "a null ShieldType");
                } else if (!ala().aKm().contains(next.mo9731VX())) {
                    akd.mo9663a(next, next.mo9731VX(), "shield");
                }
                for (ShipSectorType aux : next.bVG()) {
                    if (!ala().aKy().contains(aux)) {
                        akd.mo9663a(next, aux, "sector");
                    }
                }
            }
        }
    }

    @C0064Am(aul = "4b56d6cadeddccae03e6f7a1b062fcdb", aum = 0)
    /* renamed from: w */
    private void m36467w(Collection<ShipSectorType> collection, aKD akd, Set<String> set) {
        for (ShipSectorType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.bmW() == null) {
                    akd.mo9665b(next, "no SectorCategory");
                } else if (!ala().aLo().contains(next.bmW())) {
                    akd.mo9663a(next, next.bmW(), "SectorCategory");
                }
            }
        }
    }

    @C0064Am(aul = "a8ae507f093218d1ef62f2037d3eedfc", aum = 0)
    /* renamed from: y */
    private void m36470y(Collection<ShieldType> collection, aKD akd, Set<String> set) {
        for (ShieldType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.cpZ() < 0.0f) {
                    akd.mo9665b(next, "invalid damage reduction (" + next.cpZ() + "). It must be non-negative");
                }
                if (next.mo15373hh() < 0.0f) {
                    akd.mo9665b(next, "invalid max HP (" + next.mo15373hh() + "). It must be non-negative");
                }
                if (next.mo15374ic() < 0.0f) {
                    akd.mo9665b(next, "invalid recovery time (" + next.mo15374ic() + "). It must be non-negative");
                }
                if (next.cqb() < 0) {
                    akd.mo9665b(next, "invalid regeneration (" + next.cqb() + "). It must be non-negative");
                }
            }
        }
    }

    @C0064Am(aul = "14ceb02c9ef020ac059875fd62c7016e", aum = 0)
    /* renamed from: A */
    private void m36329A(Collection<SceneryType> collection, aKD akd, Set<String> set) {
        for (SceneryType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.mo3521Nu() == null) {
                akd.mo9665b(next, "no RenderAsset");
            }
        }
    }

    @C0064Am(aul = "e38960d61ba315f559f28577c6e9f165", aum = 0)
    /* renamed from: C */
    private void m36331C(Collection<NPCType> collection, aKD akd, Set<String> set) {
        for (NPCType next : collection) {
            C5663aRz[] arzArr = null;
            if (m36444b((C0468GU) next, akd, set, next.bTK() == null ? new C5663aRz[]{C3599sm.bgp} : new C5663aRz[0])) {
                if (next.bTI() == null && next.bTK() != null) {
                    akd.mo9665b(next, "no avatar handle");
                }
                if (next.bTC() != null) {
                    if (!ala().aJW().contains(next.bTC())) {
                        akd.mo9663a(next, next.bTC(), "equipped ship type");
                    }
                    if (next.bTE() == null) {
                        akd.mo9665b(next, "an EquippedShipType but does not have an AIControllerClass");
                    }
                    if (next.bTE() != null && next.bTG() == null && (next.bTE() == DroneAIController.class || next.bTE() == WanderAndShootAIController.class)) {
                        akd.mo9665b(next, "an AIControllerClass " + next.bTE() + " but does not have an AIControllerType");
                    }
                    m36422b((C0405Fe) next, akd, set);
                }
            }
        }
    }

    @C0064Am(aul = "9a333ed1041fa42c52f42f4036ea3adf", aum = 0)
    /* renamed from: E */
    private void m36333E(Collection<ItemTypeCategory> collection, aKD akd, Set<String> set) {
        for (ItemTypeCategory next : collection) {
            if (!next.bJU()) {
                if (!m36444b((C0468GU) next, akd, set, C6990ayd.f5631vE)) {
                }
            } else if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36442b((C4068yr) next, akd);
            }
            if (next.bJS() != null && !ala().aKI().contains(next.bJS())) {
                akd.mo9663a(next, next.bJS(), "parent");
            }
            if (next.mo12100sK() == null) {
                akd.mo9665b(next, "no icon");
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b  */
    @p001a.C0064Am(aul = "aa8af26b2903c454096e0315900aa01f", aum = 0)
    /* renamed from: G */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m36335G(java.util.Collection<DatabaseCategory> r4, p001a.aKD r5, java.util.Set<java.lang.String> r6) {
        /*
            r3 = this;
            java.util.Iterator r1 = r4.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            a.aiK r0 = (p001a.C6194aiK) r0
            r2 = 0
            a.aRz[] r2 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r3.m36444b((logic.baa.C0468GU) r0, (p001a.aKD) r5, (java.util.Set<java.lang.String>) r6, (logic.res.code.C5663aRz[]) r2)
            if (r0 != 0) goto L_0x0004
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3090ne.m36335G(java.util.Collection, a.aKD, java.util.Set):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000b  */
    @p001a.C0064Am(aul = "9ac5bade960e75b334dc2859a1160990", aum = 0)
    /* renamed from: I */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m36337I(java.util.Collection<TaikopediaEntry> r4, p001a.aKD r5, java.util.Set<java.lang.String> r6) {
        /*
            r3 = this;
            java.util.Iterator r1 = r4.iterator()
        L_0x0004:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            java.lang.Object r0 = r1.next()
            a.aiZ r0 = (p001a.C6209aiZ) r0
            r2 = 0
            a.aRz[] r2 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r3.m36444b((logic.baa.C0468GU) r0, (p001a.aKD) r5, (java.util.Set<java.lang.String>) r6, (logic.res.code.C5663aRz[]) r2)
            if (r0 != 0) goto L_0x0004
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3090ne.m36337I(java.util.Collection, a.aKD, java.util.Set):void");
    }

    @C0064Am(aul = "d1fa2e06806766ee2e0618f7c6648a76", aum = 0)
    /* renamed from: a */
    private void m36380a(C4068yr yrVar, aKD akd) {
        if (yrVar.mo184RY() == null) {
            akd.mo9665b(yrVar, "no DatabaseCategory");
        }
        if (yrVar.mo185Sa() == null && yrVar.mo187Se() == null) {
            akd.mo9665b(yrVar, "no PDA asset handle");
        }
    }

    @C0064Am(aul = "c8480a7dca14a74ad0df331eff663bd2", aum = 0)
    /* renamed from: K */
    private void m36339K(Collection<HullType> collection, aKD akd, Set<String> set) {
        for (HullType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.mo22727hh() <= 0.0f) {
                akd.mo9665b(next, "invalid max HP (" + next.mo22727hh() + "). It must be positive");
            }
        }
    }

    @C0064Am(aul = "9f709a8a8d27461f17af4749afb7a190", aum = 0)
    /* renamed from: M */
    private void m36341M(Collection<EquippedShipType> collection, aKD akd, Set<String> set) {
        boolean z;
        for (EquippedShipType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.mo19050uX() == null) {
                    akd.mo9665b(next, "no ShipType");
                    z = true;
                } else {
                    if (!ala().mo1458cD(false).contains(next.mo19050uX())) {
                        akd.mo9663a(next, next.mo19050uX(), "ship type");
                    }
                    z = false;
                }
                for (ClipType next2 : next.mo19053vd()) {
                    if (!ala().mo1458cD(false).contains(next2)) {
                        akd.mo9663a(next, next2, "clip");
                    }
                }
                for (WeaponType next3 : next.mo19051uZ()) {
                    if (!ala().mo1458cD(false).contains(next3)) {
                        akd.mo9663a(next, next3, "weapon");
                    }
                }
                if (!z) {
                    HashMap hashMap = new HashMap();
                    for (WeaponType next4 : next.mo19051uZ()) {
                        for (ShipStructureType bVG : next.mo19050uX().agt()) {
                            for (ShipSectorType aux : bVG.bVG()) {
                                if (next4.cXV() == aux.bmW()) {
                                    Integer num = (Integer) hashMap.get(aux);
                                    if (num == null) {
                                        num = 0;
                                    }
                                    int intValue = num.intValue() + next4.cuJ();
                                    if (intValue > aux.dAF()) {
                                        akd.mo9665b(next, "an WeaponType '" + next4.getHandle() + "' that are not going to be placed anywhere");
                                    } else {
                                        hashMap.put(aux, Integer.valueOf(intValue));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @C0064Am(aul = "32edc4e30c87211eacae060312583135", aum = 0)
    /* renamed from: O */
    private void m36343O(Collection<GateType> collection, aKD akd, Set<String> set) {
        for (GateType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.mo3521Nu() == null) {
                akd.mo9665b(next, "has no RenderAsset");
            }
        }
    }

    @C0064Am(aul = "5762b877e67b00a711fd9e5627eaf991", aum = 0)
    /* renamed from: Q */
    private void m36345Q(Collection<AsteroidType> collection, aKD akd, Set<String> set) {
        for (AsteroidType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36422b((C0405Fe) next, akd, set);
                if (next.mo3521Nu() == null) {
                    akd.mo9665b(next, "no RenderAsset");
                }
                if (next.dpL() == null) {
                    akd.mo9665b(next, "no AsteroidLootType");
                } else if (!ala().aKK().contains(next.dpL())) {
                    akd.mo9663a(next, next.dpL(), "loot");
                }
                if (next.mo11071VT() == null) {
                    akd.mo9665b(next, "no HullType");
                } else if (!ala().aKk().contains(next.mo11071VT())) {
                    akd.mo9663a(next, next.mo11071VT(), "hull");
                }
            }
        }
    }

    @C0064Am(aul = "84076bb0189ce346b7983f2be61bf3b9", aum = 0)
    /* renamed from: S */
    private void m36347S(Collection<AsteroidZoneCategory> collection, aKD akd, Set<String> set) {
        for (AsteroidZoneCategory next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36442b((C4068yr) next, akd);
            }
        }
    }

    @C0064Am(aul = "e6d1dd362ab55e69b456f730870c4994", aum = 0)
    /* renamed from: U */
    private void m36349U(Collection<SpaceCategory> collection, aKD akd, Set<String> set) {
        for (SpaceCategory next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36442b((C4068yr) next, akd);
            }
        }
    }

    @C0064Am(aul = "1279c9d77cd487ba86e25a58beef2d18", aum = 0)
    /* renamed from: W */
    private void m36352W(Collection<CargoHoldType> collection, aKD akd, Set<String> set) {
        for (CargoHoldType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.mo11172wE() <= 0.0f) {
                akd.mo9665b(next, "invalid max volume (" + next.mo11172wE() + "). It must be positive");
            }
        }
    }

    @C0064Am(aul = "73786287c6d84df13f2fe022053a1d90", aum = 0)
    /* renamed from: Y */
    private void m36354Y(Collection<AdvertiseType> collection, aKD akd, Set<String> set) {
        for (AdvertiseType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.mo3521Nu() == null) {
                akd.mo9665b(next, "no RenderAsset");
            }
        }
    }

    @C0064Am(aul = "4d0b07f078081d4034c50d079fab7a8c", aum = 0)
    /* renamed from: aa */
    private void m36393aa(Collection<ClipBoxType> collection, aKD akd, Set<String> set) {
        for (ClipBoxType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36440b((ItemType) next, akd);
                m36434b((BulkItemType) next, akd);
                if (next.dvX() <= 0) {
                    akd.mo9665b(next, "zero or less bullets to generate");
                }
                if (next.dvZ() == null) {
                    akd.mo9665b(next, "no related ammo");
                } else {
                    if (!ala().aKc().contains(next.dvZ())) {
                        akd.mo9663a(next, next.dvZ(), "related clip");
                    }
                    if (next.dvZ().mo19876Hy() != 0) {
                        akd.mo9665b(next, "invalid base unit price for the related clip: " + next.dvZ().mo19876Hy() + ". It must be zero");
                    }
                    float HA = next.dvZ().mo19865HA() * ((float) next.dvX());
                    if (next.mo19865HA() < HA) {
                        akd.mo9665b(next, "lower volume (" + next.mo19865HA() + ") than its related clips (" + HA + "). Its volume must be at least the same or greater than.");
                    }
                }
            }
        }
    }

    @C0064Am(aul = "1f9855a54d8d05fa5c67b10e74effa07", aum = 0)
    /* renamed from: ac */
    private void m36395ac(Collection<ClipType> collection, aKD akd, Set<String> set) {
        for (ClipType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36440b((ItemType) next, akd);
                m36434b((BulkItemType) next, akd);
                if (next.mo3521Nu() == null) {
                    akd.mo9665b(next, "no RenderAsset");
                }
                if (next instanceof MineType) {
                    m36430b((MineType) next, akd);
                }
                if (next instanceof MissileType) {
                    m36429b(akd, (MissileType) next);
                }
            }
        }
    }

    @C0064Am(aul = "c027795f4c20be6cb5dc0f9d6a556f8b", aum = 0)
    /* renamed from: a */
    private void m36366a(aKD akd, MissileType avg) {
        if (avg.mo3521Nu() == null) {
            akd.mo9665b(avg, "no RenderAsset");
        }
        if (avg.bkh() == null) {
            akd.mo9665b(avg, "no ExplosionGFX");
        }
        if (avg.bkj() == null) {
            akd.mo9665b(avg, "no ExplosionSFX");
        }
        if (avg.mo11871zD() <= 0.0f) {
            akd.mo9665b(avg, "invalid explosion radius. It must be positive");
        }
        if (avg.dAP() <= 0) {
            akd.mo9665b(avg, "invalid life time. It must be positive");
        }
        if (avg.mo11853VT() == null) {
            akd.mo9665b(avg, "no HullType");
        } else if (!ala().aKk().contains(avg.mo11853VT())) {
            akd.mo9663a(avg, avg.mo11853VT(), "hull");
        }
        if (avg.mo11851VF() < 0.0f) {
            akd.mo9665b(avg, "invalid max angular acceleration. It must non-negative");
        }
        if (avg.mo11852VH() < 0.0f) {
            akd.mo9665b(avg, "invalid max angular velocity. It must non-negative");
        }
        if (avg.mo11870rb() <= 0.0f) {
            akd.mo9665b(avg, "invalid max linear acceleration. It must be positive");
        }
        if (avg.mo11869ra() <= 0.0f) {
            akd.mo9665b(avg, "invalid max linear velocity. It must be positive");
        }
    }

    @C0064Am(aul = "445c2f68ad1f40f49d1718297bd573d6", aum = 0)
    /* renamed from: a */
    private void m36368a(MineType amx, aKD akd) {
        if (amx.bkh() == null) {
            akd.mo9665b(amx, "no ExplosionGFX");
        }
        if (amx.bkj() == null) {
            akd.mo9665b(amx, "no ExplosionSFX");
        }
        if (amx.mo10198zD() <= 0.0f) {
            akd.mo9665b(amx, "invalid explosion radius. It must be positive");
        }
        if (amx.dju() <= 0.0f) {
            akd.mo9665b(amx, "invalid detonation radius. It must be positive");
        }
        if (amx.mo10187VT() == null) {
            akd.mo9665b(amx, "no HullType");
        } else if (!ala().aKk().contains(amx.mo10187VT())) {
            akd.mo9663a(amx, amx.mo10187VT(), "hull");
        }
        if (amx.getLifeTime() <= 0) {
            akd.mo9665b(amx, "invalid life time. It must be positive");
        }
    }

    @C0064Am(aul = "abdc27a06d0f37fac270be985411cf90", aum = 0)
    /* renamed from: ae */
    private void m36397ae(Collection<MerchandiseType> collection, aKD akd, Set<String> set) {
        for (MerchandiseType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36440b((ItemType) next, akd);
                m36434b((BulkItemType) next, akd);
            }
        }
    }

    @C0064Am(aul = "e42a661621b8161201d599377caad2ea", aum = 0)
    /* renamed from: ag */
    private void m36399ag(Collection<BlueprintType> collection, aKD akd, Set<String> set) {
        for (BlueprintType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36440b((ItemType) next, akd);
                m36434b((BulkItemType) next, akd);
                if (next.bhW().isEmpty()) {
                    akd.mo9665b(next, "no ingredients");
                }
                boolean z = false;
                float f = 0.0f;
                for (IngredientsCategory xVVar : next.bhW()) {
                    for (String str : C2520gI.m31894a(xVVar, new C5663aRz[0])) {
                        akd.mo9664a((C0468GU) next, "a I18n error with some ingredient category: " + str);
                    }
                    if (xVVar.apd().isEmpty()) {
                        akd.mo9665b(next, "some ingredient category with no recipes");
                    } else {
                        boolean z2 = z;
                        for (CraftingRecipe next2 : xVVar.apd()) {
                            if (next2.getAmount() == 0) {
                                akd.mo9665b(next, "some recipe with amount zero");
                            }
                            if (next2.mo554az() == null) {
                                akd.mo9665b(next, "some recipe with null ItemType");
                                z2 = true;
                            } else {
                                if (!ala().mo1458cD(false).contains(next2.mo554az())) {
                                    akd.mo9663a(next, next2.mo554az(), "recipe");
                                }
                                f += ((float) next2.getAmount()) * next2.mo554az().mo19865HA();
                            }
                        }
                        z = z2;
                    }
                }
                CraftingRecipe bia = next.bia();
                if (bia == null) {
                    akd.mo9665b(next, "no result");
                } else {
                    if (bia.getAmount() == 0) {
                        akd.mo9665b(next, "result with amount zero");
                    }
                    if (bia.mo554az() == null) {
                        akd.mo9665b(next, "crafting result with null ItemType");
                        z = true;
                    } else {
                        if (!ala().mo1458cD(false).contains(bia.mo554az())) {
                            akd.mo9663a(next, bia.mo554az(), "item type result");
                        }
                        float HA = bia.mo554az().mo19865HA() * ((float) bia.getAmount());
                    }
                    if (!z) {
                    }
                }
            }
        }
    }

    @C0064Am(aul = "2a48f5dfcd5a5cfcc2c7a5e4562a5f25", aum = 0)
    /* renamed from: ai */
    private void m36401ai(Collection<ShipType> collection, aKD akd, Set<String> set) {
        for (ShipType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36440b((ItemType) next, akd);
                m36426b(next, akd);
            }
        }
    }

    @C0064Am(aul = "cb153c3c37bd8880e60aa82eb3eb5ece", aum = 0)
    /* renamed from: ak */
    private void m36403ak(Collection<CruiseSpeedType> collection, aKD akd, Set<String> set) {
        for (CruiseSpeedType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36425b(next, akd);
            }
        }
    }

    @C0064Am(aul = "c5d25dba4bc9140ec33e57911451cf86", aum = 0)
    /* renamed from: am */
    private void m36405am(Collection<RawMaterialType> collection, aKD akd, Set<String> set) {
        for (RawMaterialType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36440b((ItemType) next, akd);
                m36434b((BulkItemType) next, akd);
            }
        }
    }

    @C0064Am(aul = "4dafacd393270a702464e592b893b05a", aum = 0)
    /* renamed from: ao */
    private void m36407ao(Collection<WeaponType> collection, aKD akd, Set<String> set) {
        for (WeaponType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36428b((ComponentType) next, akd);
                if (next instanceof ProjectileWeaponType) {
                    ProjectileWeaponType agj = (ProjectileWeaponType) next;
                    ClipType cZZ = agj.cZZ();
                    if (cZZ == null) {
                        akd.mo9665b(next, "null default ammo");
                    } else {
                        if (!ala().aKc().contains(cZZ)) {
                            akd.mo9663a(next, cZZ, "default ammo");
                        }
                        if (agj.bun() != cZZ.bun()) {
                            akd.mo9665b(next, "an uncompatible default ammo: " + cZZ.getHandle());
                        }
                    }
                }
                if (next instanceof MissileWeaponType) {
                    if (next.getSpeed() < 0.0f) {
                        akd.mo9665b(next, "invalid speed: (" + next.getSpeed() + "). It must be >= 0");
                    }
                } else if (next instanceof MineThrowerWeaponType) {
                    if (next.getSpeed() != 0.0f) {
                        akd.mo9665b(next, "invalid speed: (" + next.getSpeed() + "). It must be 0");
                    }
                } else if (next.getSpeed() <= 0.0f) {
                    akd.mo9665b(next, "invalid speed: (" + next.getSpeed() + "). It must be > 0");
                }
            }
        }
    }

    @C0064Am(aul = "adf21fe12ecbca5275017d0ccc1cd646", aum = 0)
    /* renamed from: a */
    private boolean m36382a(C0468GU gu, aKD akd, Set<String> set, C5663aRz[] arzArr) {
        if (gu.getHandle() == null) {
            akd.mo9668j(gu);
            return false;
        }
        if (gu.getHandle().equals(C0468GU.dac)) {
            akd.mo9671k(gu);
        } else if (set.contains(gu.getHandle())) {
            akd.mo9672l(gu);
            return false;
        } else {
            set.add(gu.getHandle());
        }
        for (String a : C2520gI.m31894a(gu, arzArr)) {
            akd.mo9664a(gu, a);
        }
        return true;
    }

    @C0064Am(aul = "5ccca5c198f7d30f9ba856cb1eee6626", aum = 0)
    /* renamed from: a */
    private void m36372a(BulkItemType amh, aKD akd) {
        if (amh.mo13880dX() <= 0) {
            akd.mo9665b(amh, "invalid max stack size: " + amh.mo13880dX());
        }
    }

    @C0064Am(aul = "e6771ed087370de054bd243c4bdffb7a", aum = 0)
    /* renamed from: a */
    private void m36378a(ItemType jCVar, aKD akd) {
        if ((jCVar instanceof C4068yr) && (!(jCVar instanceof MerchandiseType) || ((MerchandiseType) jCVar).bgJ())) {
            m36442b((C4068yr) jCVar, akd);
        }
        if (jCVar.mo12100sK() == null) {
            akd.mo9665b(jCVar, "no Icon");
        }
        if (jCVar.mo19868HG() < 0) {
            akd.mo9665b(jCVar, "invalid avg unit price: (" + jCVar.mo19868HG() + "). It must be non-negative");
        }
        if (jCVar.mo19870HK() < 0) {
            akd.mo9665b(jCVar, "invalid avg max unit price: (" + jCVar.mo19870HK() + "). It must be non-negative");
        }
        if (jCVar.mo19869HI() < 0) {
            akd.mo9665b(jCVar, "invalid avg min unit price: (" + jCVar.mo19869HI() + "). It must be non-negative");
        }
        if (jCVar.mo19871HM() <= 0 || jCVar.mo19871HM() > 9999) {
            akd.mo9665b(jCVar, "invalid max trading units: (" + jCVar.mo19871HM() + "). It must be greater than zero and less than 9999");
        }
        if (jCVar.mo19874HS() < 1) {
            akd.mo9665b(jCVar, "invalid min lot active buy: (" + jCVar.mo19874HS() + "). It must be greater than 0");
        }
        if (jCVar.mo19875HU() < 1) {
            akd.mo9665b(jCVar, "invalid min lot any buy: (" + jCVar.mo19875HU() + "). It must be greater than 0");
        }
        if (jCVar.mo19872HO() < 1) {
            akd.mo9665b(jCVar, "invalid min lot cheap sell: (" + jCVar.mo19872HO() + "). It must be greater than 0");
        }
        if (jCVar.mo19873HQ() < 1) {
            akd.mo9665b(jCVar, "invalid min lot expansive sell: (" + jCVar.mo19873HQ() + "). It must be greater than 0");
        }
        if (jCVar.mo19865HA() < 0.0f) {
            akd.mo9665b(jCVar, "invalid unit volume: (" + jCVar.mo19865HA() + "). It must be non-negative");
        }
        if (jCVar.mo19866HC() == null) {
            akd.mo9665b(jCVar, "null ItemTypeCategory");
            return;
        }
        if (!ala().aKI().contains(jCVar.mo19866HC())) {
            akd.mo9663a(jCVar, jCVar.mo19866HC(), "ItemTypeCategory");
        }
        ItemTypeCategory bJS = jCVar.mo19866HC().bJS();
        if (bJS == null) {
            akd.mo9665b(jCVar, "null ItemTypeCategoy's parent");
        } else if (!ala().aKI().contains(bJS)) {
            akd.mo9663a(jCVar, bJS, "ItemTypeCategory's parent");
        }
    }

    @C0064Am(aul = "17b558aa04218c4b1d28f476be0ed5b0", aum = 0)
    /* renamed from: a */
    private void m36365a(ComponentType aef, aKD akd) {
        m36440b((ItemType) aef, akd);
        if (aef.cXV() == null) {
            akd.mo9665b(aef, "no Category");
        } else if (!ala().aLo().contains(aef.cXV())) {
            akd.mo9663a(aef, aef.cXV(), "Category");
        }
        if (aef.cuH() == null) {
            akd.mo9665b(aef, "null Size");
        }
    }

    @C0064Am(aul = "0533c48e9f8e98a91819e0746ba98dc3", aum = 0)
    /* renamed from: aq */
    private void m36409aq(Collection<StellarSystem> collection, aKD akd, Set<String> set) {
        HashMap hashMap = new HashMap();
        for (StellarSystem next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36423b(next, akd, set, (Map<NPC, Station>) hashMap);
            }
        }
    }

    @C0064Am(aul = "2f0931a230c5ad194974fd64202ddcb8", aum = 0)
    /* renamed from: a */
    private void m36360a(StellarSystem jj, aKD akd, Set<String> set, Map<NPC, Station> map) {
        for (Node next : jj.getNodes()) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36441b(next, jj, akd, set, map);
            }
        }
    }

    @C0064Am(aul = "ec35af8f6d2df44f8ad64df00b811c27", aum = 0)
    /* renamed from: a */
    private void m36363a(ShipType ng, aKD akd) {
        boolean z;
        if (ng instanceof FreighterType) {
            m36432b((FreighterType) ng, akd);
        }
        if (ng.bkx() == null) {
            akd.mo9665b(ng, "no PhysicalAsset");
        }
        if (ng.mo4151Nu() == null) {
            akd.mo9665b(ng, "no RenderAsset");
            z = false;
        } else {
            z = true;
        }
        if (ng.bkl() == null) {
            akd.mo9665b(ng, "no LootType");
        } else if (!ala().aKK().contains(ng.bkl())) {
            akd.mo9663a(ng, ng.bkl(), "loot");
        }
        if (ng.bkF() < 0.0f) {
            akd.mo9665b(ng, "invalid explosion delay (" + ng.bkF() + "). It must be non-negative");
        }
        if (!ng.getHandle().contains("sce_")) {
            if (ng.mo4218rb() <= 0.0f) {
                akd.mo9665b(ng, "invalid max linear acceleration. It must be positive");
            }
            if (ng.mo4217ra() <= 0.0f) {
                akd.mo9665b(ng, "invalid max linear velocity. It must be positive");
            }
            if (ng.mo4152VF() <= 0.0f) {
                akd.mo9665b(ng, "invalid max angular acceleration. It must be positive");
            }
            if (ng.mo4153VH() <= 0.0f) {
                akd.mo9665b(ng, "invalid max angular velocity. It must be positive");
            }
            if (z) {
                if (ng.mo21127bc(C5791aaX.eXt) == null) {
                    akd.mo9666c(ng, "for left weapon");
                }
                if (ng.mo21127bc(C5791aaX.eXu) == null) {
                    akd.mo9666c(ng, "for right weapon");
                }
                if (ng.mo21127bc(C5791aaX.eXv) == null) {
                    akd.mo9666c(ng, "for middle weapon");
                }
            }
            if (ng.bkf() == null) {
                akd.mo9665b(ng, "no EngineSFX");
            }
            if (ng.bkh() == null) {
                akd.mo9665b(ng, "no ExplosionGFX");
            }
            if (ng.bkj() == null) {
                akd.mo9665b(ng, "no ExplosionSFX");
            }
            if (ng.mo4155VT() == null) {
                akd.mo9665b(ng, "no HullType");
            } else if (!ala().aKk().contains(ng.mo4155VT())) {
                akd.mo9663a(ng, ng.mo4155VT(), "hull");
            }
            if (ng.mo4156Wb() == null) {
                akd.mo9665b(ng, "no CargoHoldType");
            } else if (!ala().aKg().contains(ng.mo4156Wb())) {
                akd.mo9663a(ng, ng.mo4156Wb(), "cargo hold");
            }
            if (ng.agt().isEmpty()) {
                akd.mo9665b(ng, "no ShipStructureTypes");
            } else {
                for (ShipStructureType aks : ng.agt()) {
                    if (!ala().aKA().contains(aks)) {
                        akd.mo9663a(ng, aks, "ship structure");
                    }
                }
            }
            if (ng.bkd() == null) {
                akd.mo9665b(ng, "null cruise speed type");
            } else if (!ala().aLu().contains(ng.bkd())) {
                akd.mo9663a(ng, ng.bkd(), "cruise speed");
            }
            for (C3315qE qEVar : ng.mo4157Wl()) {
                if (qEVar.mo21300Xv() == null) {
                    akd.mo9665b(ng, "null turretType");
                } else if (!ala().aLi().contains(qEVar.mo21300Xv())) {
                    akd.mo9663a(ng, qEVar.mo21300Xv(), "turret");
                } else if (ng.mo21127bc(C5791aaX.eXw + qEVar.mo21301Xx()) == null) {
                    akd.mo9666c(ng, "for turret " + qEVar.mo21301Xx());
                }
            }
        }
    }

    @C0064Am(aul = "293a7b0f0e4b36297140f1a48261d6ad", aum = 0)
    /* renamed from: a */
    private void m36370a(FreighterType afn, aKD akd) {
        if (afn.bVL() == null) {
            akd.mo9665b(afn, "freighter with null vault");
        }
    }

    @C0064Am(aul = "8069c23c628ce978f879d2fc860e5871", aum = 0)
    /* renamed from: a */
    private void m36362a(CruiseSpeedType nf, aKD akd) {
        if (nf.mo4143rb() < 0.0f) {
            akd.mo9665b(nf, "negative max linear acceleration");
        }
        if (nf.mo4142ra() < 0.0f) {
            akd.mo9665b(nf, "negative max linear velocity");
        }
        if (nf.mo4127VF() < 0.0f) {
            akd.mo9665b(nf, "negative max angular acceleration");
        }
        if (nf.mo4128VH() < 0.0f) {
            akd.mo9665b(nf, "negative max angular velocity");
        }
        if (nf.anr() < 0.0f) {
            akd.mo9665b(nf, "negative warmup time");
        }
        if (nf.ant() < 0.0f) {
            akd.mo9665b(nf, "negative cooldown time");
        }
    }

    @C0064Am(aul = "20911db44f7df527728179c59265412f", aum = 0)
    /* renamed from: a */
    private void m36379a(Node rPVar, StellarSystem jj, aKD akd, Set<String> set, Map<NPC, Station> map) {
        m36442b((C4068yr) rPVar, akd);
        if (rPVar.mo21606Nc() != jj) {
            akd.mo9665b(rPVar, "incorrect owner StellarSystem. '" + rPVar.mo21606Nc() + "'. It should be '" + jj + "'");
        }
        if (rPVar.mo21607Nu() == null) {
            akd.mo9665b(rPVar, "no render asset");
        }
        if (rPVar.getPosition() == null) {
            akd.mo9665b(rPVar, "null position. Edit and apply position field");
        }
        if (rPVar.getRadius() <= 0.0f) {
            akd.mo9665b(rPVar, "invalid radius (" + rPVar.getRadius() + "). It must be positive");
        }
        for (Actor next : rPVar.mo21624Zw()) {
            m36420b(next, jj, rPVar, akd);
            if (next instanceof Station) {
                m36419b((Station) next, akd, set, map);
            } else if (next instanceof Gate) {
                m36436b((Gate) next, akd, set);
            }
        }
        for (Advertise b : rPVar.aac()) {
            m36420b((Actor) b, jj, rPVar, akd);
        }
        for (NPCZone next2 : rPVar.mo21616ZU()) {
            m36420b((Actor) next2, jj, rPVar, akd);
            m36437b(next2, akd);
        }
        for (AsteroidZone next3 : rPVar.mo21615ZQ()) {
            m36420b((Actor) next3, jj, rPVar, akd);
            m36435b(next3, akd);
        }
    }

    @C0064Am(aul = "88da5641deb2dc473961697c4cb763e0", aum = 0)
    /* renamed from: a */
    private void m36374a(Gate fFVar, aKD akd, Set<String> set) {
        if (fFVar.mo18391rR() == null) {
            akd.mo9665b(fFVar, "no space category");
        } else if (!ala().aLG().contains(fFVar.mo18391rR())) {
            akd.mo9663a(fFVar, fFVar.mo18391rR(), "space category");
        }
        if (fFVar.mo18392ru() == null) {
            akd.mo9665b(fFVar, "no type");
        }
        if (fFVar.mo18386rF() == null) {
            akd.mo9665b(fFVar, "no destiny");
        }
        if (fFVar.mo18387rH() == null) {
            akd.mo9665b(fFVar, "no orientation");
        }
        if (fFVar.mo18388rJ() == null) {
            akd.mo9665b(fFVar, "no spawn relative position");
        }
    }

    @C0064Am(aul = "284edafa3d13368b557986e00b0a6d6b", aum = 0)
    /* renamed from: a */
    private void m36357a(Actor cr, StellarSystem jj, Node rPVar, aKD akd) {
        if (cr.bae() && !jj.aXN().contains(cr)) {
            akd.mo9665b(rPVar, "a spawned " + cr.getClass().getSimpleName() + " unknown to its owner StellarSystem '" + jj + "': " + cr);
        }
        if (!cr.bae() && jj.aXN().contains(cr)) {
            akd.mo9665b(rPVar, "an unspawned " + cr.getClass().getSimpleName() + " known to its owner StellarSystem '" + jj + "': " + cr);
        }
        if (cr.cLl() == null) {
            akd.mo9665b(rPVar, "a " + cr.getClass().getSimpleName() + " with null position: " + cr);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @p001a.C0064Am(aul = "d0c010a1cee434822458d6e84f9946e1", aum = 0)
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m36356a(Station r9, p001a.aKD r10, java.util.Set<java.lang.String> r11, java.util.Map<NPC, Station> r12) {
        /*
            r8 = this;
            r2 = 0
            r8.m36442b((logic.baa.C4068yr) r9, (p001a.aKD) r10)
            a.aRz[] r1 = new logic.res.code.C5663aRz[r2]
            boolean r0 = r9 instanceof p001a.C3347qZ
            if (r0 == 0) goto L_0x0015
            r0 = r9
            a.Xf r0 = (logic.baa.C1616Xf) r0
            a.aRz[] r3 = r0.mo25c()
            int r4 = r3.length
            r0 = r2
        L_0x0013:
            if (r0 < r4) goto L_0x001d
        L_0x0015:
            r0 = r1
        L_0x0016:
            boolean r0 = r8.m36444b((logic.baa.C0468GU) r9, (p001a.aKD) r10, (java.util.Set<java.lang.String>) r11, (logic.res.code.C5663aRz[]) r0)
            if (r0 != 0) goto L_0x0034
        L_0x001c:
            return
        L_0x001d:
            r5 = r3[r0]
            java.lang.String r6 = r5.name()
            java.lang.String r7 = "typeName"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x0031
            r0 = 1
            a.aRz[] r0 = new logic.res.code.C5663aRz[r0]
            r0[r2] = r5
            goto L_0x0016
        L_0x0031:
            int r0 = r0 + 1
            goto L_0x0013
        L_0x0034:
            a.DN r0 = r8.ala()
            java.util.List r0 = r0.aKE()
            a.Mx r1 = r9.azP()
            boolean r0 = r0.contains(r1)
            if (r0 != 0) goto L_0x004f
            a.Mx r0 = r9.azP()
            java.lang.String r1 = "StationType"
            r10.mo9663a(r9, r0, r1)
        L_0x004f:
            a.ko r0 = r9.azL()
            if (r0 == 0) goto L_0x007b
            long r2 = r0.mo20141JL()
            r4 = 0
            int r1 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r1 > 0) goto L_0x007b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "a CloningCenter with invalid price ("
            r1.<init>(r2)
            long r2 = r0.mo20141JL()
            java.lang.StringBuilder r0 = r1.append(r2)
            java.lang.String r1 = "). It must be positive"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.mo9665b(r9, r0)
        L_0x007b:
            a.hf r0 = r9.azJ()
            if (r0 == 0) goto L_0x0084
            r8.m36439b((p001a.C2617hf) r0, (p001a.aKD) r10, (java.util.Set<java.lang.String>) r11, (p001a.C0096BF) r9)
        L_0x0084:
            java.util.List r0 = r9.azz()
            java.util.Iterator r2 = r0.iterator()
        L_0x008c:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x00e1
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>()
            java.util.Map r0 = r9.azB()
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x00a3:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x001c
            java.lang.Object r0 = r3.next()
            r1 = r0
            a.abk r1 = (p001a.C5856abk) r1
            java.util.Map r0 = r9.azB()
            java.lang.Object r0 = r0.get(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r4 = r0.intValue()
            if (r4 >= 0) goto L_0x01ef
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r4 = "an agent NPC ("
            r0.<init>(r4)
            a.aed r1 = r1.mo11654Fs()
            java.lang.String r1 = r1.getHandle()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ") with a negative index"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.mo9665b(r9, r0)
            goto L_0x00a3
        L_0x00e1:
            java.lang.Object r0 = r2.next()
            a.abk r0 = (p001a.C5856abk) r0
            boolean r1 = r12.containsKey(r0)
            if (r1 == 0) goto L_0x01ab
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "an agent NPC instance ("
            r1.<init>(r3)
            a.aed r3 = r0.mo11654Fs()
            java.lang.String r3 = r3.getHandle()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = ", "
            java.lang.StringBuilder r1 = r1.append(r3)
            long r4 = r0.mo8317Ej()
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r3 = ") already located in another Station: "
            java.lang.StringBuilder r3 = r1.append(r3)
            java.lang.Object r1 = r12.get(r0)
            a.BF r1 = (p001a.C0096BF) r1
            java.lang.String r1 = r1.getName()
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r3 = ". You must remove this agent NPC and drag the NPCType to create a new instance"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r10.mo9665b(r9, r1)
        L_0x012f:
            a.aed r1 = r0.mo11654Fs()
            a.aaQ r1 = r1.bTK()
            if (r1 != 0) goto L_0x0159
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "an agent NPC with no chat: '"
            r1.<init>(r3)
            a.aed r3 = r0.mo11654Fs()
            taikodom.infra.game.script.I18NString r3 = r3.mo11470ke()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "'"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r10.mo9665b(r9, r1)
        L_0x0159:
            a.aed r1 = r0.mo11654Fs()
            java.lang.String r1 = r1.bTI()
            if (r1 != 0) goto L_0x0183
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "an agent NPC with no avatar handle: '"
            r1.<init>(r3)
            a.aed r3 = r0.mo11654Fs()
            taikodom.infra.game.script.I18NString r3 = r3.mo11470ke()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "'"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r10.mo9665b(r9, r1)
        L_0x0183:
            a.CR r1 = r0.bhE()
            if (r1 != 0) goto L_0x01af
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "an agent NPC with null location: "
            r1.<init>(r3)
            a.aed r0 = r0.mo11654Fs()
            java.lang.String r0 = r0.getHandle()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = ". Delete him and drag again"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.mo9665b(r9, r0)
            goto L_0x008c
        L_0x01ab:
            r12.put(r0, r9)
            goto L_0x012f
        L_0x01af:
            a.CR r1 = r0.bhE()
            if (r1 == r9) goto L_0x008c
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "an agent NPC ("
            r1.<init>(r3)
            a.aed r3 = r0.mo11654Fs()
            java.lang.String r3 = r3.getHandle()
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = ") whose location is not the Station itself. "
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "NPC's location is "
            java.lang.StringBuilder r1 = r1.append(r3)
            a.CR r0 = r0.bhE()
            java.lang.String r0 = r0.getName()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = ". You must remove this agent NPC and drag the NPCType to create a new instance"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.mo9665b(r9, r0)
            goto L_0x008c
        L_0x01ef:
            boolean r4 = r2.contains(r0)
            if (r4 == 0) goto L_0x021b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = "an agent NPC ("
            r4.<init>(r5)
            a.aed r1 = r1.mo11654Fs()
            java.lang.String r1 = r1.getHandle()
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r4 = ") with the same index as another: "
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            r10.mo9665b(r9, r0)
            goto L_0x00a3
        L_0x021b:
            r2.add(r0)
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3090ne.m36356a(a.BF, a.aKD, java.util.Set, java.util.Map):void");
    }

    @C0064Am(aul = "b836580b661feb18a9b32afd3a18e371", aum = 0)
    /* renamed from: a */
    private void m36377a(Store hfVar, aKD akd, Set<String> set, Station bf) {
        if (m36444b((C0468GU) hfVar, akd, set, new C5663aRz[0])) {
            HashSet<ItemType> hashSet = new HashSet<>();
            hashSet.addAll(hfVar.dCz());
            hashSet.addAll(hfVar.dCv());
            hashSet.addAll(hfVar.dCx());
            for (ItemType jCVar : hashSet) {
                if (!ala().aKe().contains(jCVar) && !ala().aKc().contains(jCVar) && !ala().aKG().contains(jCVar) && !ala().aKu().contains(jCVar) && !ala().aKC().contains(jCVar) && !ala().mo1546vb().contains(jCVar) && !ala().aKq().contains(jCVar) && !ala().mo1545uZ().contains(jCVar)) {
                    String str = "";
                    if (hfVar.dCz().contains(jCVar)) {
                        str = String.valueOf(str) + " > activeBuy";
                    }
                    if (hfVar.dCv().contains(jCVar)) {
                        str = String.valueOf(str) + " > cheapSell";
                    }
                    if (hfVar.dCx().contains(jCVar)) {
                        str = String.valueOf(str) + " > expensiveSell";
                    }
                    akd.mo9663a(hfVar, jCVar, "marketed item on" + str + " from station " + bf + ",");
                }
            }
        }
    }

    @C0064Am(aul = "e5e6520dc65e82dff46d207ef89a1e32", aum = 0)
    /* renamed from: as */
    private void m36411as(Collection<ItemGenSet> collection, aKD akd, Set<String> set) {
        for (ItemGenSet next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.diC().isEmpty()) {
                    akd.mo9665b(next, "no entries. It's empty");
                } else {
                    double d = ScriptRuntime.NaN;
                    boolean z = false;
                    for (ItemGenEntry next2 : next.diC()) {
                        if (next2.mo20232MN() <= 0) {
                            akd.mo9665b(next, "an ItemGenEntry with invalid min quantity: " + next2.mo20232MN());
                        }
                        if (next2.mo20233MP() < next2.mo20232MN()) {
                            akd.mo9665b(next, "an ItemGenEntry with invalid max quantity: " + next2.mo20233MP() + ". It must be at least " + next2.mo20232MN());
                        }
                        if (next2.mo20234MR() <= 0.0f) {
                            akd.mo9665b(next, "an ItemGenEntry with invalid percentage: " + next2.mo20234MR());
                        } else {
                            d += (double) next2.mo20234MR();
                        }
                        if (next2.mo20235az() == null) {
                            akd.mo9665b(next, "an ItemGenEntry with a null ItemType");
                            z = true;
                        } else if (!ala().mo1458cD(false).contains(next2.mo20235az())) {
                            akd.mo9665b(next, "an ItemGenEntry with an ItemType unrelated to Taikodom: " + next2.mo20235az().getHandle());
                        }
                    }
                    if (!z && d > 100.0d) {
                        akd.mo9665b(next, "a set of ItemGenEntries with invalid total percentage (" + d + "). It must be equal or lower than 100.0");
                    }
                }
            }
        }
    }

    @C0064Am(aul = "36e7ec61ddc54c40d48f0bcc991ad1a5", aum = 0)
    /* renamed from: au */
    private void m36413au(Collection<LootType> collection, aKD akd, Set<String> set) {
        for (LootType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.mo3521Nu() == null) {
                    akd.mo9665b(next, "no RenderAsset");
                }
                if (next.getLifeTime() < 0.0f) {
                    akd.mo9665b(next, "invalid life time (" + next.getLifeTime() + "). It must be non-negative");
                }
            }
        }
    }

    @C0064Am(aul = "b70c9dbc45c2d1567a78179f58ce1d0b", aum = 0)
    /* renamed from: a */
    private void m36359a(C0405Fe fe, aKD akd, Set<String> set) {
        if (fe.aPl() != null) {
            List<ItemType> cD = ala().mo1458cD(false);
            for (ItemGenTable next : fe.aPl()) {
                if (next.mo21071TV() == 0) {
                    akd.mo9665b(fe, "zero multiplier. No item shall be generated");
                }
                if (!next.mo21072TX().isEmpty()) {
                    for (ItemGenSet next2 : next.mo21072TX()) {
                        if (!ala().mo1320TX().contains(next2)) {
                            akd.mo9663a(fe, next2, "set");
                        } else {
                            for (ItemGenEntry next3 : next2.diC()) {
                                if (next3.mo20235az() == null) {
                                    akd.mo9665b(fe, "item gen set (" + next2.getHandle() + ") with an entry with null itemtype");
                                } else if (!cD.contains(next3.mo20235az())) {
                                    akd.mo9663a(fe, next3.mo20235az(), "item gen set (" + next2.getHandle() + ") with an entry with itemtype");
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @C0064Am(aul = "2b8ddf11d5b100ab2bbd16c26586d371", aum = 0)
    /* renamed from: a */
    private void m36373a(AsteroidZone btVar, aKD akd) {
        if (btVar.bAi() == null) {
            akd.mo9665b(btVar, "no AsteroidZoneCategory");
        } else if (!ala().aLE().contains(btVar.bAi())) {
            akd.mo9663a(btVar, btVar.bAi(), "asteroid zone category");
        }
        if (btVar.mo20316Lu() <= 0.0f) {
            akd.mo9665b(btVar, "a zero or negative radius");
        }
        if (!ala().aJc().bAY().contains(btVar.bAe())) {
            akd.mo9665b(btVar, "has a type which not is in Space Zone Types Manager");
        }
    }

    @C0064Am(aul = "2e7ded279c212efff7fe2e4f880551ad", aum = 0)
    /* renamed from: a */
    private void m36375a(NPCZone fwVar, aKD akd) {
        if (fwVar.mo20316Lu() <= 0.0f) {
            akd.mo9665b(fwVar, "a zero or negative radius");
        }
        if (fwVar.mo20317Lw()) {
            for (String a : C2520gI.m31894a(fwVar, new C5663aRz[0])) {
                akd.mo9664a((C0468GU) fwVar, a);
            }
        }
        if (fwVar.mo18874qM() == null) {
            akd.mo9665b(fwVar, "null population behaviour");
        }
        PopulationControl qK = fwVar.mo18873qK();
        if (qK == null) {
            akd.mo9665b(fwVar, "null population control");
        } else if (qK instanceof BossNestPopulationControl) {
            BossNestPopulationControl aod = (BossNestPopulationControl) qK;
            if (aod.cmH()) {
                for (String str : C2520gI.m31894a(aod, new C5663aRz[0])) {
                    akd.mo9664a((C0468GU) fwVar, "has I18N error in BossNestPopulationControl: " + str);
                }
            }
            if (aod.cmB() <= 0) {
                akd.mo9665b(fwVar, "has invalid boss reposition period");
            }
        }
    }

    @C0064Am(aul = "04198473d658faab96011eeb6a67671c", aum = 0)
    /* renamed from: aw */
    private void m36415aw(Collection<AIControllerType> collection, aKD akd, Set<String> set) {
        for (AIControllerType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && (next instanceof WanderAndShootAIControllerType)) {
                WanderAndShootAIControllerType k = (WanderAndShootAIControllerType) next;
                if (k.mo3460aQ() < 0.0f || k.mo3460aQ() > 1.0f) {
                    akd.mo9665b(next, "an error: factor beetween going ahead and turning must be beetween 0 and 1");
                }
            }
        }
    }

    @C0064Am(aul = "868db8f6f422ac0680246f6aa60bd595", aum = 0)
    /* renamed from: ay */
    private void m36417ay(Collection<C6257ajV> collection, aKD akd, Set<String> set) {
        for (C6257ajV next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (!next.cjp().cea() && !next.cjr().cea() && next.cjb().size() < 0) {
                    akd.mo9665b(next, "an error: its an empty tutorial.");
                }
                if (next.cjd().isEmpty()) {
                    akd.mo9665b(next, "an empty rule to start");
                } else {
                    for (C6123agr next2 : next.cjd()) {
                        if (next2.getEvent().equals("") || next2.getEvent().equals(C6123agr.DEFAULT)) {
                            akd.mo9665b(next, "an empty or invalid rule event");
                        }
                        if (next2.bUu().equals(C6123agr.DEFAULT)) {
                            akd.mo9665b(next, "an invalid rule parameter");
                        }
                    }
                }
                for (C6278ajq next3 : next.cjb()) {
                    for (C6123agr next4 : next.cjd()) {
                        if (next4.getEvent().equals("") || next4.getEvent().equals(C6123agr.DEFAULT)) {
                            akd.mo9665b(next, "an empty or invalid rule event");
                        }
                        if (next4.bUu().equals(C6123agr.DEFAULT)) {
                            akd.mo9665b(next, "an invalid rule parameter");
                        }
                    }
                    m36433b(next, next3, akd, false);
                }
                m36433b(next, next.cjp(), akd, true);
                m36433b(next, next.cjr(), akd, true);
            }
        }
    }

    @C0064Am(aul = "7aec8848ca595ef2f60f99759d7541c5", aum = 0)
    /* renamed from: a */
    private void m36371a(C6257ajV ajv, C6278ajq ajq, aKD akd, boolean z) {
        for (String str : C2520gI.m31894a(ajq, z ? new C5663aRz[]{C3040nL.aLs} : new C5663aRz[0])) {
            akd.mo9664a((C0468GU) ajv, "some page has error (" + ajq.cWm() + "): " + str);
        }
        I18NString rP = ajq.mo14215rP();
        for (String next : rP.getMap().keySet()) {
            if (!z && ajq.cdT().get(next).equals("")) {
                if (ajq.cdX().get(next).equals("")) {
                    akd.mo9665b(ajv, "an empty page. Page: " + ajq.cWm() + ". Locale '" + next + "'.");
                } else {
                    akd.mo9665b(ajv, "a success message for an empty page content. Page: " + ajq.cWm() + ". Locale: '" + next + "'.");
                }
            }
            if (rP.get(next).length() > 36) {
                akd.mo9665b(ajv, "a big title (greater than 36 chars). Page OID: " + ajq.cWm() + ". Locale: '" + next + "'.");
            }
        }
        if (!z) {
            for (C6123agr next2 : ajq.cdV()) {
                if (next2.getEvent().equals("") || next2.getEvent().equals(C6123agr.DEFAULT)) {
                    akd.mo9665b(ajv, "a page with an empty or invalid rule event (" + ajq.cWm() + ")");
                }
                if (next2.bUu().equals(C6123agr.DEFAULT)) {
                    akd.mo9665b(ajv, "a page with an invalid rule parameter (" + ajq.cWm() + ")");
                }
            }
        }
    }

    @C0064Am(aul = "9e1cd9235348b9a0018003888bed9054", aum = 0)
    /* renamed from: aA */
    private void m36383aA(Collection<BagItemType> collection, aKD akd, Set<String> set) {
        for (BagItemType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36440b((ItemType) next, akd);
                if (next.ayd() <= 0.0f) {
                    akd.mo9665b(next, "invalid capacity (" + next.ayd() + "). It must be positive");
                }
            }
        }
    }

    @C0064Am(aul = "a0a12e09b5017652effa1806c06fd2a4", aum = 0)
    /* renamed from: aC */
    private void m36385aC(Collection<HazardAreaType> collection, aKD akd, Set<String> set) {
        for (HazardAreaType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.cue().isEmpty()) {
                    akd.mo9665b(next, "no AttributeBuffTypes");
                } else {
                    for (AttributeBuffType api : next.cue()) {
                        if (!ala().aLg().contains(api)) {
                            akd.mo9663a(next, api, "AttributeBuffType");
                        }
                    }
                }
                if (next.cui() <= 0) {
                    akd.mo9665b(next, "an invalid tick in sec (" + next.cui() + "). It must be positive");
                }
                if (next.mo15984Lu() <= 0.0f) {
                    akd.mo9665b(next, "an invalid zone radius (" + next.mo15984Lu() + "). It must be positive");
                }
            }
        }
    }

    @C0064Am(aul = "122573aaa9f4b502321a368c2a0e0cf2", aum = 0)
    /* renamed from: aE */
    private void m36387aE(Collection<ModuleType> collection, aKD akd, Set<String> set) {
        for (ModuleType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                m36428b((ComponentType) next, akd);
                next.bdD();
                next.bdB();
                next.bdz();
                boolean z = true;
                if (next.bdH().isEmpty()) {
                    akd.mo9665b(next, "no AttributeBuffTypes");
                } else {
                    boolean z2 = true;
                    for (AttributeBuffType api : next.bdH()) {
                        if (api.dod() != Module.C4010b.SELF) {
                            z2 = false;
                        }
                        if (!ala().aLg().contains(api)) {
                            akd.mo9663a(next, api, "AttributeBuffType");
                        }
                    }
                    z = z2;
                }
                if (z) {
                    if (next.bdF() < 0.0f) {
                        akd.mo9665b(next, "invalid range (" + next.bdF() + "). It must be non-negative");
                    }
                } else if (next.bdF() <= 0.0f) {
                    akd.mo9665b(next, "invalid range (" + next.bdF() + "). It must be positive");
                }
                if (next.bdx() < 0.0f) {
                    akd.mo9665b(next, "invalid cooldown (" + next.bdx() + "). It must be non-negative");
                }
                if (next.bdt() < 0.0f) {
                    akd.mo9665b(next, "invalid duration (" + next.bdt() + "). It must be non-negative");
                }
                if (next.bdv() < 0.0f) {
                    akd.mo9665b(next, "invalid warmup (" + next.bdv() + "). It must be non-negative");
                }
            }
        }
    }

    @C0064Am(aul = "1dab3ce8bf4e7eeddf7eeb3fddeda3fb", aum = 0)
    /* renamed from: aG */
    private void m36389aG(Collection<AttributeBuffType> collection, aKD akd, Set<String> set) {
        for (AttributeBuffType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.dod() == null) {
                akd.mo9665b(next, "no ModuleTarget");
            }
        }
    }

    @C0064Am(aul = "eba2e6106e50a741e3d7b84c0d2e9ce6", aum = 0)
    /* renamed from: aI */
    private void m36391aI(Collection<DamageType> collection, aKD akd, Set<String> set) {
        for (DamageType next : collection) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0]) && next.mo2145sK() == null) {
                akd.mo9665b(next, "no icon");
            }
        }
    }

    @C0064Am(aul = "2ff6c951988351eb174f4f5ee624483a", aum = 0)
    /* renamed from: a */
    private void m36369a(ItemBilling acVar, aKD akd, Set<String> set) {
        for (ItemBillingOrder next : acVar.mo12552cV()) {
            if (m36444b((C0468GU) next, akd, set, new C5663aRz[0])) {
                if (next.mo22367az() == null) {
                    akd.mo9665b(next, "ItemType is null.");
                }
                if (next.ajU() <= 0) {
                    akd.mo9665b(next, "Invalid Hoplons price. Must be positive.");
                }
                if (next.ajW() < 0) {
                    akd.mo9665b(next, "Invalid Durability. Must be zero (without depreciation) or positive.");
                }
            }
        }
    }

    @C0064Am(aul = "8202386176c3339e9f9b9367fe2062e7", aum = 0)
    /* renamed from: a */
    private void m36358a(AvatarManager dq, aKD akd, Set<String> set) {
        boolean z;
        if (dq == null) {
            System.err.println("No avatar manager defined");
            return;
        }
        if (dq.aFW() == null || dq.aFW().length() == 0) {
            akd.mo9665b(dq, "no mesh resource path defined!!");
        } else if (!dq.aFW().endsWith(C0147Bi.SEPARATOR)) {
            akd.mo9665b(dq, "mesh resource path not ending with '/' (lame but needed)");
        }
        if (dq.aFY() == null || dq.aFY().length() == 0) {
            akd.mo9665b(dq, "no texture resource path defined!!");
        } else if (!dq.aFW().endsWith(C0147Bi.SEPARATOR)) {
            akd.mo9665b(dq, "texture resource path not ending with '/' (lame but needed)");
        }
        Collection<AvatarSector> aFI = dq.aFI();
        for (AvatarSector next : aFI) {
            m36444b((C0468GU) next, akd, set, new C5663aRz[0]);
            if (next.getPrefix() == null || next.getPrefix().length() == 0) {
                akd.mo9665b(next, " prefix null or empty (cannot control mesh visibility)");
            }
        }
        AvatarManager.ColorManager aFK = dq.aFK();
        Set<ColorWrapper> cGf = aFK.cGf();
        for (ColorWrapper b : cGf) {
            m36444b((C0468GU) b, akd, set, new C5663aRz[0]);
        }
        Set<TextureGridType> cGb = aFK.cGb();
        TextureGridType fn = null;
        for (TextureGridType next2 : cGb) {
            m36444b((C0468GU) next2, akd, set, new C5663aRz[0]);
            if (next2.aPN() == null) {
                if (fn != null) {
                    akd.mo9665b(next2, "been declared (another empty grid definition have been already declared [handle: " + fn.getHandle() + "])");
                } else {
                    fn = next2;
                }
            }
        }
        Set<TextureGrid> cGd = aFK.cGd();
        for (TextureGrid next3 : cGd) {
            m36444b((C0468GU) next3, akd, set, new C5663aRz[0]);
            if (next3.byh() == null) {
                akd.mo9665b(next3, "null grid type");
            } else if (!cGb.contains(next3.byh())) {
                akd.mo9663a(next3, next3.byh(), "Grid definition");
            }
        }
        Set<TextureScheme> cGh = aFK.cGh();
        for (TextureScheme next4 : cGh) {
            m36444b((C0468GU) next4, akd, set, new C5663aRz[0]);
            if (next4.byr() == null) {
                akd.mo9665b(next4, "null color");
            } else if (!cGf.contains(next4.byr())) {
                akd.mo9663a(next4, next4.byr(), "color (ColorWrapper) ");
            }
            if (next4.crX() == null) {
                akd.mo9665b(next4, "no body texture defined");
            }
            if (next4.crV() == null) {
                akd.mo9665b(next4, "no head texture defined");
            }
        }
        Set<BodyPieceBank> aFE = dq.aFE();
        for (BodyPieceBank next5 : aFE) {
            for (BodyPiece next6 : next5.dry()) {
                m36444b((C0468GU) next6, akd, set, new C5663aRz[0]);
                if (next6.bpk() == null) {
                    akd.mo9665b(next6, "null sector");
                } else if (!aFI.contains(next6.bpk())) {
                    akd.mo9663a(next6, next6.bpk(), "sector");
                }
                for (BodyPieceBank next7 : aFE) {
                    if (!next7.equals(next5) && next7.mo11214R(next6)) {
                        System.err.println("uniqueness violation");
                    }
                }
                for (TextureGrid next8 : next6.bpm()) {
                    if (!cGd.contains(next8)) {
                        akd.mo9663a(next6, next8, "color");
                    }
                }
            }
        }
        if (!(dq.aFQ() != null)) {
            akd.mo9665b(dq, "No facial hair sector defined");
        }
        if (!(dq.aFO() != null)) {
            akd.mo9665b(dq, "No hair sector defined");
        }
        if (!(dq.aFM() != null)) {
            akd.mo9665b(dq, "No eyeballs defined");
        }
        List<AvatarAppearanceType> aFG = dq.aFG();
        for (AvatarAppearanceType next9 : aFG) {
            m36444b((C0468GU) next9, akd, set, new C5663aRz[0]);
            if (next9.mo5548cH() <= 0.0f) {
                akd.mo9665b(next9, "model height lesser than Zero (probably expecting a value greater than 1,50 and lesser than 2,00)");
            }
            if (next9.getPrefix() == null || next9.getPrefix().length() == 0) {
                akd.mo9665b(next9, "no resource path prefix defined");
            }
            if (next9.mo5532bF() == null) {
                akd.mo9665b(next9, "no mesh asset defined");
            }
            if (next9.mo5533bH() == null) {
                akd.mo9665b(next9, "no Model asset defined");
            }
            if (next9.mo5534bJ() == null) {
                akd.mo9665b(next9, "no Body Normal asset defined");
            }
            if (next9.mo5549cJ() == null) {
                akd.mo9665b(next9, "no idle animation asset defined");
            }
            if (next9.mo5546cD().isEmpty()) {
                akd.mo9665b(next9, "no face skin detail defined");
            }
            for (TextureScheme ark : next9.mo5559cr()) {
                if (!cGh.contains(ark)) {
                    akd.mo9663a(next9, ark, "skin color");
                }
            }
            if (!next9.mo5561cv().isEmpty()) {
                if (next9.mo5563cz().isEmpty()) {
                    akd.mo9665b(next9, "no color definitions to be used on skin features");
                }
            } else if (!next9.mo5563cz().isEmpty()) {
                akd.mo9665b(next9, "colors definitions but no skin features to be applied on");
            }
            for (ColorWrapper apf : next9.mo5563cz()) {
                if (!cGf.contains(apf)) {
                    akd.mo9663a(next9, apf, "color");
                }
            }
            HashSet hashSet = new HashSet();
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5557cn());
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5535bL());
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5537bP());
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5551cb());
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5555cj());
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5553cf());
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5541bX());
            m36367a(akd, aFE, next9, (Set<BodyPiece>) hashSet, (Collection<BodyPiece>) next9.mo5539bT());
        }
        for (ClothCategory next10 : dq.aFC()) {
            m36444b((C0468GU) next10, akd, set, new C5663aRz[0]);
            if (next10 instanceof CompoundCategory) {
                CompoundCategory ape = (CompoundCategory) next10;
                for (ClothCategory next11 : ape.cpg()) {
                    if (!dq.aFC().contains(next11)) {
                        akd.mo9663a(ape, next11, "sub category");
                    }
                }
            }
        }
        for (Cloth next12 : dq.aFA()) {
            m36444b((C0468GU) next12, akd, set, new C5663aRz[0]);
            if (next12.bEb() == null) {
                akd.mo9665b(next12, "null category");
            } else if (!dq.aFC().contains(next12.bEb())) {
                akd.mo9663a(next12, next12.bEb(), "category");
            }
            for (BodyPiece next13 : next12.bEj()) {
                Iterator<BodyPieceBank> it = aFE.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (it.next().mo11214R(next13)) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    akd.mo9663a(next12, next13, "part");
                }
            }
            for (AvatarAppearanceType next14 : next12.bEf()) {
                if (!aFG.contains(next14)) {
                    akd.mo9663a(next12, next14, "forbidden appearance");
                }
            }
        }
    }
}
