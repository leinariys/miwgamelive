package game.script.util;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.mission.MissionTemplate;
import game.script.mission.NPCSpawn;
import game.script.mission.TableNPCSpawn;
import game.script.mission.actions.*;
import game.script.mission.scripting.MissionScriptTemplate;
import game.script.mission.scripting.ScriptableMissionTemplate;
import game.script.missiontemplate.ReconWaypointDat;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.*;
import logic.baa.*;
import logic.data.link.C0743Kc;
import logic.data.link.C6516aoU;
import logic.data.mbean.C6593apt;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.bY */
/* compiled from: a */
public class MissionTemplateConsistencyChecker extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: pQ */
    public static final C2491fm f5810pQ = null;
    /* renamed from: pR */
    public static final C2491fm f5811pR = null;
    /* renamed from: pS */
    public static final C2491fm f5812pS = null;
    /* renamed from: pT */
    public static final C2491fm f5813pT = null;
    /* renamed from: pU */
    public static final C2491fm f5814pU = null;
    /* renamed from: pV */
    public static final C2491fm f5815pV = null;
    public static final long serialVersionUID = 0;
    /* renamed from: pP */
    private static final Class[] f5809pP = {ProtectMissionScriptTemplate.class};
    public static C6494any ___iScriptClass;

    static {
        m27853V();
    }

    public MissionTemplateConsistencyChecker() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionTemplateConsistencyChecker(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27853V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 6)];
        C2491fm a = C4105zY.m41624a(MissionTemplateConsistencyChecker.class, "52177d6d06288f9029371dcb06a0bd17", i);
        f5810pQ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(MissionTemplateConsistencyChecker.class, "515b3b2463472a00168a2ea2388bedfa", i2);
        f5811pR = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionTemplateConsistencyChecker.class, "121d8dd2b0ec3b1f36254cad905e029a", i3);
        f5812pS = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionTemplateConsistencyChecker.class, "9db88677452a0c1d80546c732ac3339e", i4);
        f5813pT = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionTemplateConsistencyChecker.class, "abf66ac326d0b49f0b9bd3bcaf4fbd0c", i5);
        f5814pU = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionTemplateConsistencyChecker.class, "c3e393f348bb7755ca7eb18045224f90", i6);
        f5815pV = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionTemplateConsistencyChecker.class, C6593apt.class, _m_fields, _m_methods);
    }

    /* renamed from: b */
    private List<String> m27858b(String str, WaypointDat cDVar, boolean z, boolean z2) {
        switch (bFf().mo6893i(f5814pU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f5814pU, new Object[]{str, cDVar, new Boolean(z), new Boolean(z2)}));
            case 3:
                bFf().mo5606d(new aCE(this, f5814pU, new Object[]{str, cDVar, new Boolean(z), new Boolean(z2)}));
                break;
        }
        return m27854a(str, cDVar, z, z2);
    }

    /* renamed from: b */
    private void m27859b(MissionScriptTemplate abq, aKD akd) {
        switch (bFf().mo6893i(f5813pT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5813pT, new Object[]{abq, akd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5813pT, new Object[]{abq, akd}));
                break;
        }
        m27855a(abq, akd);
    }

    /* renamed from: b */
    private void m27860b(MissionTemplate avh, aKD akd, boolean z) {
        switch (bFf().mo6893i(f5812pS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5812pS, new Object[]{avh, akd, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5812pS, new Object[]{avh, akd, new Boolean(z)}));
                break;
        }
        m27856a(avh, akd, z);
    }

    /* renamed from: b */
    private boolean m27861b(MissionTemplate avh, aKD akd, Set<String> set) {
        switch (bFf().mo6893i(f5811pR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5811pR, new Object[]{avh, akd, set}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5811pR, new Object[]{avh, akd, set}));
                break;
        }
        return m27857a(avh, akd, set);
    }

    /* renamed from: d */
    private void m27863d(MissionTemplate avh, aKD akd, boolean z) {
        switch (bFf().mo6893i(f5815pV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5815pV, new Object[]{avh, akd, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5815pV, new Object[]{avh, akd, new Boolean(z)}));
                break;
        }
        m27862c(avh, akd, z);
    }

    @C0064Am(aul = "52177d6d06288f9029371dcb06a0bd17", aum = 0)
    @C5566aOg
    /* renamed from: y */
    private String m27864y(boolean z) {
        throw new aWi(new aCE(this, f5810pQ, new Object[]{new Boolean(z)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6593apt(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m27864y(((Boolean) args[0]).booleanValue());
            case 1:
                return new Boolean(m27857a((MissionTemplate) args[0], (aKD) args[1], (Set<String>) (Set) args[2]));
            case 2:
                m27856a((MissionTemplate) args[0], (aKD) args[1], ((Boolean) args[2]).booleanValue());
                return null;
            case 3:
                m27855a((MissionScriptTemplate) args[0], (aKD) args[1]);
                return null;
            case 4:
                return m27854a((String) args[0], (WaypointDat) args[1], ((Boolean) args[2]).booleanValue(), ((Boolean) args[3]).booleanValue());
            case 5:
                m27862c((MissionTemplate) args[0], (aKD) args[1], ((Boolean) args[2]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: z */
    public String mo17301z(boolean z) {
        switch (bFf().mo6893i(f5810pQ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5810pQ, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, f5810pQ, new Object[]{new Boolean(z)}));
                break;
        }
        return m27864y(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "515b3b2463472a00168a2ea2388bedfa", aum = 0)
    /* renamed from: a */
    private boolean m27857a(MissionTemplate avh, aKD akd, Set<String> set) {
        if (avh.getHandle() == null) {
            akd.mo9668j(avh);
            return false;
        }
        if (avh.getHandle().equals(C0468GU.dac)) {
            akd.mo9671k(avh);
        } else if (set.contains(avh.getHandle())) {
            akd.mo9672l(avh);
            return false;
        } else {
            set.add(avh.getHandle());
        }
        return true;
    }

    @C0064Am(aul = "121d8dd2b0ec3b1f36254cad905e029a", aum = 0)
    /* renamed from: a */
    private void m27856a(MissionTemplate avh, aKD akd, boolean z) {
        if (avh.dBi() == null) {
            akd.mo9665b(avh, "no Giver Agent");
        } else if (!ala().aJY().contains(avh.dBi())) {
            akd.mo9663a(avh, avh.dBi(), "giver agent");
        }
        if (avh instanceof MissionScriptTemplate) {
            m27859b((MissionScriptTemplate) avh, akd);
        }
        if (!z && avh.aqJ()) {
            ArrayList arrayList = new ArrayList();
            for (C5663aRz arz : avh.mo25c()) {
                if (arz != C6516aoU.gjJ) {
                    arrayList.add(arz);
                }
            }
            for (String str : C2520gI.m31894a(avh, (C5663aRz[]) arrayList.toArray(new C5663aRz[0]))) {
                akd.mo9665b(avh, "Mission template '" + avh.getHandle() + "' with invalid I18NContent: " + str);
            }
        }
        if (avh instanceof MissionScriptTemplate) {
            if (((MissionScriptTemplate) avh).cKb().size() <= 0) {
                akd.mo9665b(avh, "no objectives");
            }
        } else if ((avh instanceof ScriptableMissionTemplate) && ((ScriptableMissionTemplate) avh).azk().size() <= 0) {
            akd.mo9665b(avh, "no objectives");
        }
    }

    @C0064Am(aul = "9db88677452a0c1d80546c732ac3339e", aum = 0)
    /* renamed from: a */
    private void m27855a(MissionScriptTemplate abq, aKD akd) {
        String handle;
        String lW;
        for (MissionAction next : abq.cKd()) {
            MissionAction.C2210a aDO = next.aDO();
            if (aDO == null) {
                akd.mo9665b(abq, "mission action with null event");
            } else if ((aDO == MissionAction.C2210a.NPC_CHAT || aDO == MissionAction.C2210a.NPC_KILLED || aDO == MissionAction.C2210a.STATE_CHANGE || aDO == MissionAction.C2210a.TIMER || aDO == MissionAction.C2210a.UNDOCK || aDO == MissionAction.C2210a.WAYPOINT_REACHED) && ((handle = next.getHandle()) == null || "".equals(handle.trim()))) {
                akd.mo9665b(abq, "mission action with null handle");
            }
            if (next instanceof AccomplishObjectiveMissionAction) {
                String cuR = ((AccomplishObjectiveMissionAction) next).cuR();
                if (cuR == null || "".equals(cuR.trim())) {
                    akd.mo9665b(abq, "accomplish objective mission action with null handle");
                }
            } else if (next instanceof CreateBeaconPathToActor) {
                if (((CreateBeaconPathToActor) next).cAP() == null) {
                    akd.mo9665b(abq, "create beacon mission action with null destination");
                }
            } else if (next instanceof GiveItemMissionAction) {
                GiveItemMissionAction oiVar = (GiveItemMissionAction) next;
                if (oiVar.mo21027Sv() == null) {
                    akd.mo9665b(abq, "give item mission action with null item type");
                }
                if (oiVar.mo21028Sx() < 1) {
                    akd.mo9665b(abq, "give item mission action with invalid quantity");
                }
            } else if (next instanceof SetParamMissionAction) {
                SetParamMissionAction apc = (SetParamMissionAction) next;
                String key = apc.getKey();
                String value = apc.getValue();
                if (key == null || "".equals(key.trim())) {
                    akd.mo9665b(abq, "set parameter mission action with invalid key");
                }
                if (value == null || "".equals(value.trim())) {
                    akd.mo9665b(abq, "set parameter mission action with invalid value");
                }
            } else if (next instanceof SetTimer) {
                SetTimer eyVar = (SetTimer) next;
                String lW2 = eyVar.mo18247lW();
                if (lW2 == null || "".equals(lW2.trim())) {
                    akd.mo9665b(abq, "set timer mission action with invalid timer handle");
                }
                if (eyVar.getTime() < 1) {
                    akd.mo9665b(abq, "set timer mission action with invalid timeout");
                }
            } else if (next instanceof ShowNPCMessage) {
                ShowNPCMessage sgVar = (ShowNPCMessage) next;
                if (sgVar.abs() == null) {
                    akd.mo9665b(abq, "NPC chat mission action with null NPC type");
                }
                if (sgVar.abv() < 0.0f) {
                    akd.mo9665b(abq, "NPC chat mission action with invalid timeout");
                }
                if (sgVar.mo22014KI() == null) {
                    akd.mo9665b(abq, "NPC chat mission action with invalid message");
                }
            } else if ((next instanceof UnsetTimer) && ((lW = ((UnsetTimer) next).mo15272lW()) == null || "".equals(lW.trim()))) {
                akd.mo9665b(abq, "unset timer mission action with invalid timer handle");
            }
        }
    }

    @C0064Am(aul = "abf66ac326d0b49f0b9bd3bcaf4fbd0c", aum = 0)
    /* renamed from: a */
    private List<String> m27854a(String str, WaypointDat cDVar, boolean z, boolean z2) {
        boolean z3;
        if (str == null || !str.startsWith("MissionE")) {
            z3 = false;
        } else {
            z3 = true;
        }
        Vec3d position = cDVar.getPosition();
        ArrayList arrayList = new ArrayList();
        if (cDVar == null) {
            arrayList.add("waypoint is null");
            return arrayList;
        }
        if (position == null) {
            arrayList.add("null position");
        } else if (position.x == ScriptRuntime.NaN && position.y == ScriptRuntime.NaN && position.z == ScriptRuntime.NaN && cDVar.bvu() != WaypointDat.C2121a.RELATIVE) {
            arrayList.add("invalid position");
        }
        if ((cDVar.mo17519Vg() <= 0 && cDVar.bvu() != WaypointDat.C2121a.STATIC) || cDVar.aMJ() <= 0) {
            arrayList.add("zero or negative radius");
        }
        if (z && cDVar.getAmount() <= 0 && (cDVar.bvw() == null || cDVar.bvw().coi() == null || cDVar.bvw().coi().size() == 0)) {
            arrayList.add("missing primary spawn info");
        }
        TableNPCSpawn bvw = cDVar.bvw();
        if (bvw != null && bvw.coi().size() > 0) {
            boolean z4 = false;
            for (NPCSpawn next : bvw.coi()) {
                if (next.baz() == null || next.baB() <= 0 || next.baB() > 100) {
                    z4 = true;
                }
                if (!(next.baz() == null || z3 || next.baz().bTU().size() == 0)) {
                    arrayList.add("NPC '" + next.baz().mo11470ke() + "' has loot and mission is not expedition");
                }
                if (next.baz() != null && !ala().aJY().contains(next.baz())) {
                    arrayList.add("NPC '" + next.baz().mo11470ke() + "' in mission '" + str + "' is not related to Taikodom");
                }
            }
            if (z4) {
                arrayList.add("illegal primary spawn info");
            }
        }
        if (z2 && cDVar.bvG() <= 0 && (cDVar.bvI() == null || cDVar.bvI().coi() == null || cDVar.bvI().coi().size() == 0)) {
            arrayList.add("missing secondary spawn info");
        }
        TableNPCSpawn bvI = cDVar.bvI();
        if (bvI != null && bvI.coi().size() > 0) {
            boolean z5 = false;
            for (NPCSpawn next2 : bvI.coi()) {
                if (next2.baz() == null || next2.baB() <= 0 || next2.baB() > 100) {
                    z5 = true;
                }
                if (!(next2.baz() == null || z3 || next2.baz().bTU().size() == 0)) {
                    arrayList.add("NPC '" + next2.baz().mo11470ke() + "' has loot and mission is not expedition");
                }
            }
            if (z5) {
                arrayList.add("illegal primary spawn info");
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "c3e393f348bb7755ca7eb18045224f90", aum = 0)
    /* renamed from: c */
    private void m27862c(MissionTemplate avh, aKD akd, boolean z) {
        boolean z2;
        int i = 1;
        int i2 = 0;
        if (!z) {
            Class<?>[] clsArr = f5809pP;
            int length = clsArr.length;
            int i3 = 0;
            while (true) {
                if (i3 >= length) {
                    z2 = false;
                    break;
                }
                if (avh.getClass() == clsArr[i3]) {
                    z2 = true;
                    break;
                }
                i3++;
            }
            if (!z2) {
                for (String str : C2520gI.m31894a(avh, C6516aoU.gjJ)) {
                    akd.mo9665b(avh, "Mission template '" + avh.getHandle() + "' with invalid I18NContent: " + str);
                }
            }
        }
        if (!(avh instanceof MissionScriptTemplate) && !(avh instanceof ScriptableMissionTemplate)) {
            akd.mo9665b(avh, "mission template '" + avh.getHandle() + "' is not game.script and should be ported or rewritten");
        }
        if (avh instanceof AssaultMissionScriptTemplate) {
            AssaultMissionScriptTemplate amd = (AssaultMissionScriptTemplate) avh;
            if (amd.mo14862Nc() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
            }
            if (amd.ciF() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null assaulted NPC");
            }
            if (amd.ciB() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null assaulted waypoint");
            } else {
                for (String str2 : m27858b(avh.getHandle(), amd.ciB(), false, false)) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', assaulted waypoint, error:" + str2);
                }
            }
            int i4 = 1;
            for (AssaultMissionWave next : amd.ciJ()) {
                for (String str3 : m27858b(avh.getHandle(), next.cCR(), true, false)) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', wave waypoint (order " + i4 + "), error:" + str3);
                }
                if (next.cCV() == null) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', assault wave (order " + i4 + ") has a null wave type");
                }
                if (next.cCT() < 0) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', assault wave (order " + i4 + ") has a negative time offset");
                }
                i4++;
            }
        } else if (avh instanceof AsteroidLootItemMissionScriptTemplate) {
            AsteroidLootItemMissionScriptTemplate ang = (AsteroidLootItemMissionScriptTemplate) avh;
            if (ang.mo15009Nc() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
            }
            if (ang.mo15010Ne().size() == 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no waypoints");
            } else {
                for (WaypointDat b : ang.mo15010Ne()) {
                    for (String str4 : m27858b(avh.getHandle(), b, false, false)) {
                        akd.mo9665b(avh, "mission '" + avh.getHandle() + "', waypoint #" + i + ", error:" + str4);
                    }
                    i++;
                }
            }
            if (ang.cii() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null asteroid type");
            }
            if (ang.brU() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null item to be looted");
            }
            if (ang.cky() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null destination station");
            }
        } else if (avh instanceof BuyOutpostScriptTemplate) {
            BuyOutpostScriptTemplate xZVar = (BuyOutpostScriptTemplate) avh;
            if (xZVar.apo() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null outpost type");
            }
            String apq = xZVar.apq();
            if (apq == null || "".equals(apq)) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no delivery speech handle");
            }
        } else if (avh instanceof FollowMissionScriptTemplate) {
            FollowMissionScriptTemplate aab = (FollowMissionScriptTemplate) avh;
            if (aab.mo7603Nc() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
            }
            for (String str5 : m27858b(avh.getHandle(), aab.cIt(), false, false)) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "', NPC attack spot waypoint, error:" + str5);
            }
            for (String str6 : m27858b(avh.getHandle(), aab.cIr(), false, false)) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "', NPC destination waypoint, error:" + str6);
            }
            for (String str7 : m27858b(avh.getHandle(), aab.cIp(), false, false)) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "', NPC spawn waypoint, error:" + str7);
            }
        } else if (avh instanceof ItemDeliveryMissionScriptTemplate) {
            ItemDeliveryMissionScriptTemplate aoo = (ItemDeliveryMissionScriptTemplate) avh;
            if (aoo.brY() != null) {
                if (aoo.mo10584Nc() == null) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
                }
                for (String str8 : m27858b(avh.getHandle(), aoo.brY(), true, false)) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', conflict waypoint, error:" + str8);
                }
            }
            int i5 = 0;
            for (ItemTypeTableItem next2 : aoo.ajE()) {
                if (next2.mo23385az() == null) {
                    i5++;
                } else if (next2.getAmount() <= 0) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', delivery item '" + next2.mo23385az().mo19891ke().get() + "' has zero or negative ammount");
                }
            }
            if (i5 > 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has " + i5 + " null delivery item item types");
            }
            for (ItemTypeTableItem next3 : aoo.ajG()) {
                if (next3.mo23385az() == null) {
                    i2++;
                } else if (next3.getAmount() <= 0) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', receive item '" + next3.mo23385az().mo19891ke().get() + "' has zero or negative ammount");
                }
            }
            if (i2 > 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has " + i2 + " null receive item types");
            }
            String bse = aoo.bse();
            if (bse == null || "".equals(bse)) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no delivery speech handle");
            }
        } else if (avh instanceof ItemDeployMissionScriptTemplate) {
            ItemDeployMissionScriptTemplate uLVar = (ItemDeployMissionScriptTemplate) avh;
            int i6 = 0;
            for (ItemTypeTableItem next4 : uLVar.ajE()) {
                if (next4.mo23385az() == null) {
                    i6++;
                } else if (next4.getAmount() <= 0) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', delivery item '" + next4.mo23385az().mo19891ke().get() + "' has zero or negative ammount");
                }
            }
            if (i6 > 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has " + i6 + " null delivery item item types");
            }
            for (ItemTypeTableItem next5 : uLVar.ajG()) {
                if (next5.mo23385az() == null) {
                    i2++;
                } else if (next5.getAmount() <= 0) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', receive item '" + next5.mo23385az().mo19891ke().get() + "' has zero or negative ammount");
                }
            }
            if (i2 > 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has " + i2 + " null receive item types");
            }
            if (uLVar.ajI() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has " + i2 + " null destination station");
            }
        } else if (avh instanceof KillingNpcMissionScriptTemplate) {
            KillingNpcMissionScriptTemplate wgVar = (KillingNpcMissionScriptTemplate) avh;
            if (wgVar.mo22778Nc() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
            }
            if (wgVar.alS().size() == 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no NPC types to kill");
            }
            if (wgVar.alU() <= 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has zero or negative ammount to kill");
            }
            if (wgVar.alY().size() == 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no position data");
            }
        } else if (avh instanceof LootItemMissionScriptTemplate) {
            LootItemMissionScriptTemplate rq = (LootItemMissionScriptTemplate) avh;
            if (rq.mo5178Nc() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
            }
            if (rq.brW() == null || rq.brW().coi() == null || rq.brW().coi().size() == 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no lootable NPCs");
            }
            if (rq.brU() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null item to be looted");
            }
            if (rq.brY() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null waypoint");
                return;
            }
            for (String str9 : m27858b(avh.getHandle(), rq.brY(), false, false)) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "', waypoint error:" + str9);
            }
        } else if (avh instanceof PatrolMissionScriptTemplate) {
            PatrolMissionScriptTemplate mRVar = (PatrolMissionScriptTemplate) avh;
            if (mRVar.mo20543Nc() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
            }
            if (mRVar.mo20544Ne().size() == 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no waypoints");
                return;
            }
            for (WaypointDat b2 : mRVar.mo20544Ne()) {
                for (String str10 : m27858b(avh.getHandle(), b2, false, false)) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', waypoint #" + i + ", error:" + str10);
                }
                i++;
            }
        } else if (avh instanceof ProtectMissionScriptTemplate) {
            ProtectMissionScriptTemplate dp = (ProtectMissionScriptTemplate) avh;
            if (dp.mo1678Nc() == null) {
                akd.mo9665b(avh, "mission '" + dp.getHandle() + "' has a null stellar system");
            }
            if (dp.aEQ() == null) {
                akd.mo9665b(avh, "mission '" + dp.getHandle() + "' has a null protected waypoint");
            }
            if (dp.aEI().size() == 0) {
                akd.mo9665b(avh, "mission '" + dp.getHandle() + "' has no attackers");
            } else {
                int i7 = 1;
                for (WaypointDat b3 : dp.aEI()) {
                    for (String str11 : m27858b(avh.getHandle(), b3, true, false)) {
                        akd.mo9665b(avh, "mission '" + avh.getHandle() + "', waypoint #" + i7 + ", error:" + str11);
                    }
                    i7++;
                }
            }
            if (!z) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(C6516aoU.gjJ);
                for (C5663aRz arz : avh.mo25c()) {
                    if (arz == C0743Kc.dnD && !dp.aEC()) {
                        arrayList.add(arz);
                    } else if (arz == C0743Kc.dnF && !dp.aEy()) {
                        arrayList.add(arz);
                    } else if (arz == C0743Kc.dnx && !dp.aFe()) {
                        arrayList.add(arz);
                    } else if (arz == C0743Kc.dny && !dp.aFg()) {
                        arrayList.add(arz);
                    } else if (arz == C0743Kc.dnz && !dp.aFa()) {
                        arrayList.add(arz);
                    } else if (arz == C0743Kc.dnA && !dp.aFc()) {
                        arrayList.add(arz);
                    }
                }
                for (String str12 : C2520gI.m31894a(avh, (C5663aRz[]) arrayList.toArray(new C5663aRz[0]))) {
                    akd.mo9665b(avh, "Mission template '" + avh.getHandle() + "' with invalid I18NContent: " + str12);
                }
            }
        } else if (avh instanceof ReconMissionScriptTemplate) {
            ReconMissionScriptTemplate mdVar = (ReconMissionScriptTemplate) avh;
            if (mdVar.mo20576Nc() == null) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has a null stellar system");
            }
            if (mdVar.mo20577Ne().size() == 0) {
                akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no waypoints");
                return;
            }
            for (ReconWaypointDat b4 : mdVar.mo20577Ne()) {
                for (String str13 : m27858b(avh.getHandle(), b4, false, false)) {
                    akd.mo9665b(avh, "mission '" + avh.getHandle() + "', waypoint #" + i + ", error:" + str13);
                }
                i++;
            }
        } else if ((avh instanceof TalkToNPCMissionScriptTemplate) && ((TalkToNPCMissionScriptTemplate) avh).btQ().size() == 0) {
            akd.mo9665b(avh, "mission '" + avh.getHandle() + "' has no target speeches");
        }
    }
}
