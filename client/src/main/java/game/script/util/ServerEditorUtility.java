package game.script.util;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.axL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Dk */
/* compiled from: a */
public class ServerEditorUtility extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cDI = null;
    public static final C5663aRz cDK = null;
    public static final C5663aRz cDM = null;
    public static final C5663aRz cDO = null;
    public static final C5663aRz cDQ = null;
    public static final C2491fm cDR = null;
    public static final C2491fm cDS = null;
    public static final C2491fm cDT = null;
    public static final C2491fm cDU = null;
    public static final C2491fm cDV = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f429yH = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5b96ad577a42bdb4fb8403594700678c", aum = 0)
    private static TaikodomContentConsistencyChecker cDH;
    @C0064Am(aul = "87d81f87fdd2fd853b2539e18790d1bb", aum = 1)
    private static NPCChatConsistencyChecker cDJ;
    @C0064Am(aul = "5b56e3db324a53ed82a8939aae8dfdab", aum = 2)
    private static MissionTemplateConsistencyChecker cDL;
    @C0064Am(aul = "bbab64539f7970b7b0ab55fc634bb1d2", aum = 3)
    private static MestreDosMagosChatGenerator cDN;
    @C0064Am(aul = "af809639b892f20e298cc480bf5b62d7", aum = 4)
    private static ProgressionChecker cDP;

    static {
        m2488V();
    }

    public ServerEditorUtility() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ServerEditorUtility(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2488V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ServerEditorUtility.class, "5b96ad577a42bdb4fb8403594700678c", i);
        cDI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ServerEditorUtility.class, "87d81f87fdd2fd853b2539e18790d1bb", i2);
        cDK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ServerEditorUtility.class, "5b56e3db324a53ed82a8939aae8dfdab", i3);
        cDM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ServerEditorUtility.class, "bbab64539f7970b7b0ab55fc634bb1d2", i4);
        cDO = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ServerEditorUtility.class, "af809639b892f20e298cc480bf5b62d7", i5);
        cDQ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 6)];
        C2491fm a = C4105zY.m41624a(ServerEditorUtility.class, "930d759677d06c7dcdf404d19a1a2efc", i7);
        f429yH = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ServerEditorUtility.class, "5e58c2d57ec8dc8ee92820823f00a677", i8);
        cDR = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ServerEditorUtility.class, "86aaaa8907702694fa41171bf0bfb319", i9);
        cDS = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ServerEditorUtility.class, "24d056959a28280ea558ad1380d13cb0", i10);
        cDT = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ServerEditorUtility.class, "827e611b446912937ff824b0456d4a6d", i11);
        cDU = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ServerEditorUtility.class, "975fb425045025cf5e8d2307a56e041e", i12);
        cDV = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ServerEditorUtility.class, axL.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m2489a(MestreDosMagosChatGenerator adn) {
        bFf().mo5608dq().mo3197f(cDO, adn);
    }

    /* renamed from: a */
    private void m2490a(NPCChatConsistencyChecker acq) {
        bFf().mo5608dq().mo3197f(cDK, acq);
    }

    /* renamed from: a */
    private void m2491a(ProgressionChecker ats) {
        bFf().mo5608dq().mo3197f(cDQ, ats);
    }

    /* renamed from: a */
    private void m2492a(MissionTemplateConsistencyChecker bYVar) {
        bFf().mo5608dq().mo3197f(cDM, bYVar);
    }

    /* renamed from: a */
    private void m2493a(TaikodomContentConsistencyChecker neVar) {
        bFf().mo5608dq().mo3197f(cDI, neVar);
    }

    private TaikodomContentConsistencyChecker aDW() {
        return (TaikodomContentConsistencyChecker) bFf().mo5608dq().mo3214p(cDI);
    }

    private NPCChatConsistencyChecker aDX() {
        return (NPCChatConsistencyChecker) bFf().mo5608dq().mo3214p(cDK);
    }

    private MissionTemplateConsistencyChecker aDY() {
        return (MissionTemplateConsistencyChecker) bFf().mo5608dq().mo3214p(cDM);
    }

    private MestreDosMagosChatGenerator aDZ() {
        return (MestreDosMagosChatGenerator) bFf().mo5608dq().mo3214p(cDO);
    }

    private ProgressionChecker aEa() {
        return (ProgressionChecker) bFf().mo5608dq().mo3214p(cDQ);
    }

    @C0064Am(aul = "827e611b446912937ff824b0456d4a6d", aum = 0)
    @C5566aOg
    @C2499fr
    private void aEb() {
        throw new aWi(new aCE(this, cDU, new Object[0]));
    }

    @C0064Am(aul = "5e58c2d57ec8dc8ee92820823f00a677", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: bW */
    private String m2494bW(boolean z) {
        throw new aWi(new aCE(this, cDR, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "86aaaa8907702694fa41171bf0bfb319", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: bY */
    private String m2495bY(boolean z) {
        throw new aWi(new aCE(this, cDS, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "24d056959a28280ea558ad1380d13cb0", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ca */
    private String m2496ca(boolean z) {
        throw new aWi(new aCE(this, cDT, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "975fb425045025cf5e8d2307a56e041e", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: cc */
    private String m2497cc(boolean z) {
        throw new aWi(new aCE(this, cDV, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "930d759677d06c7dcdf404d19a1a2efc", aum = 0)
    @C5566aOg
    /* renamed from: jF */
    private void m2498jF() {
        throw new aWi(new aCE(this, f429yH, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new axL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m2498jF();
                return null;
            case 1:
                return m2494bW(((Boolean) args[0]).booleanValue());
            case 2:
                return m2495bY(((Boolean) args[0]).booleanValue());
            case 3:
                return m2496ca(((Boolean) args[0]).booleanValue());
            case 4:
                aEb();
                return null;
            case 5:
                return m2497cc(((Boolean) args[0]).booleanValue());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    public void aEc() {
        switch (bFf().mo6893i(cDU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cDU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cDU, new Object[0]));
                break;
        }
        aEb();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: bX */
    public String mo1668bX(boolean z) {
        switch (bFf().mo6893i(cDR)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cDR, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, cDR, new Object[]{new Boolean(z)}));
                break;
        }
        return m2494bW(z);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: bZ */
    public String mo1669bZ(boolean z) {
        switch (bFf().mo6893i(cDS)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cDS, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, cDS, new Object[]{new Boolean(z)}));
                break;
        }
        return m2495bY(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: cb */
    public String mo1670cb(boolean z) {
        switch (bFf().mo6893i(cDT)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cDT, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, cDT, new Object[]{new Boolean(z)}));
                break;
        }
        return m2496ca(z);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: cd */
    public String mo1671cd(boolean z) {
        switch (bFf().mo6893i(cDV)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cDV, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, cDV, new Object[]{new Boolean(z)}));
                break;
        }
        return m2497cc(z);
    }

    @C5566aOg
    public void init() {
        switch (bFf().mo6893i(f429yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f429yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f429yH, new Object[0]));
                break;
        }
        m2498jF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
