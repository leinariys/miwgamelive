package game.script.support;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.login.UserConnection;
import game.script.player.Player;
import logic.aaa.C0786LO;
import logic.aaa.C1506WA;
import logic.aaa.C3950xG;
import logic.aaa.C5429aIz;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.link.C0772LA;
import logic.data.mbean.C5331aFf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ayL */
/* compiled from: a */
public class TicketManager extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm gVA = null;
    public static final C2491fm gVB = null;
    public static final C2491fm gVC = null;
    public static final C2491fm gVD = null;
    public static final C2491fm gVE = null;
    public static final C2491fm gVF = null;
    public static final C2491fm gVG = null;
    public static final C2491fm gVH = null;
    public static final C2491fm gVI = null;
    public static final C2491fm gVJ = null;
    public static final C2491fm gVK = null;
    public static final C2491fm gVL = null;
    public static final C2491fm gVM = null;
    public static final C2491fm gVN = null;
    public static final C2491fm gVO = null;
    public static final C2491fm gVP = null;
    public static final C2491fm gVQ = null;
    public static final C2491fm gVR = null;
    public static final C2491fm gVS = null;
    public static final C2491fm gVT = null;
    public static final C2491fm gVU = null;
    public static final C2491fm gVV = null;
    public static final C5663aRz gVv = null;
    public static final C2491fm gVx = null;
    public static final C2491fm gVy = null;
    public static final C2491fm gVz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C5566aOg
    public static long gVw = 30000;
    @C0064Am(aul = "7298c6c4647231199606e2a1766d11d0", aum = 0)
    @C5566aOg
    private static C3438ra<Ticket> gVu;

    static {
        m27342V();
    }

    public TicketManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TicketManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27342V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 1;
        _m_methodCount = TaikodomObject._m_methodCount + 25;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(TicketManager.class, "7298c6c4647231199606e2a1766d11d0", i);
        gVv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i3 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 25)];
        C2491fm a = C4105zY.m41624a(TicketManager.class, "54e60cb43f73e83ced5ec7d1bcc31bf0", i3);
        gVx = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(TicketManager.class, "6bae779b8612c1dab6d00fdf2bc640f7", i4);
        gVy = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(TicketManager.class, C0772LA.dvu, i5);
        gVz = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(TicketManager.class, "0fbb368a2d6fdb68eb4d1079fa09adc3", i6);
        gVA = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(TicketManager.class, "6dd207954c8aea2811a194e619927f65", i7);
        gVB = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(TicketManager.class, "f5b5cb03da2142a20bc55f78a24d06e1", i8);
        gVC = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(TicketManager.class, "c9075cd15e0a410a94f6261ee10f2852", i9);
        gVD = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(TicketManager.class, "cd8ba7eb95de6c1dbbe80079c13b3931", i10);
        gVE = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(TicketManager.class, "5fe06f212b759e0c434666a8fb7ae22b", i11);
        gVF = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(TicketManager.class, "62e8b9774f5aed365d6bbd75bf01cf2c", i12);
        gVG = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(TicketManager.class, "1b7fdf46cb10d91c8e6c372e7b3584d4", i13);
        gVH = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(TicketManager.class, "ee5aedf92c5dddbc1889254ac7c578ca", i14);
        gVI = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(TicketManager.class, "9be301fd9c9b0ad7610183429ff097f1", i15);
        gVJ = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        C2491fm a14 = C4105zY.m41624a(TicketManager.class, "f068f2aa6991101d2007bec1dd24dedb", i16);
        gVK = a14;
        fmVarArr[i16] = a14;
        int i17 = i16 + 1;
        C2491fm a15 = C4105zY.m41624a(TicketManager.class, "3e3109f988e004bdef8af56eca83e653", i17);
        gVL = a15;
        fmVarArr[i17] = a15;
        int i18 = i17 + 1;
        C2491fm a16 = C4105zY.m41624a(TicketManager.class, "1c23c4fc21a25824f565d359e6df6af9", i18);
        gVM = a16;
        fmVarArr[i18] = a16;
        int i19 = i18 + 1;
        C2491fm a17 = C4105zY.m41624a(TicketManager.class, "bd8bd97e967c616c1e985702d0a214af", i19);
        gVN = a17;
        fmVarArr[i19] = a17;
        int i20 = i19 + 1;
        C2491fm a18 = C4105zY.m41624a(TicketManager.class, "44e9902c1dc60414243f64f7f6c888f3", i20);
        gVO = a18;
        fmVarArr[i20] = a18;
        int i21 = i20 + 1;
        C2491fm a19 = C4105zY.m41624a(TicketManager.class, "e2d7b555336b71e8e0e23c5b505c8947", i21);
        gVP = a19;
        fmVarArr[i21] = a19;
        int i22 = i21 + 1;
        C2491fm a20 = C4105zY.m41624a(TicketManager.class, "2a7c0b48b8e7880032c208f0ff2c76e7", i22);
        gVQ = a20;
        fmVarArr[i22] = a20;
        int i23 = i22 + 1;
        C2491fm a21 = C4105zY.m41624a(TicketManager.class, "56bbebb1121eb9b624e4d130737fc143", i23);
        gVR = a21;
        fmVarArr[i23] = a21;
        int i24 = i23 + 1;
        C2491fm a22 = C4105zY.m41624a(TicketManager.class, "809ea9b58eb524e7a5771b1760d4e83f", i24);
        gVS = a22;
        fmVarArr[i24] = a22;
        int i25 = i24 + 1;
        C2491fm a23 = C4105zY.m41624a(TicketManager.class, "f09d0201c41877074b249a659688a8aa", i25);
        gVT = a23;
        fmVarArr[i25] = a23;
        int i26 = i25 + 1;
        C2491fm a24 = C4105zY.m41624a(TicketManager.class, "9cce7205d81a5b6e839f941d42c0d777", i26);
        gVU = a24;
        fmVarArr[i26] = a24;
        int i27 = i26 + 1;
        C2491fm a25 = C4105zY.m41624a(TicketManager.class, "82cd95247161b5bd5eab13a12c45104f", i27);
        gVV = a25;
        fmVarArr[i27] = a25;
        int i28 = i27 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TicketManager.class, C5331aFf.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "56bbebb1121eb9b624e4d130737fc143", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m27343a(Ticket fLVar, String str) {
        throw new aWi(new aCE(this, gVR, new Object[]{fLVar, str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "62e8b9774f5aed365d6bbd75bf01cf2c", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m27344a(Ticket fLVar, String str, Player aku) {
        throw new aWi(new aCE(this, gVG, new Object[]{fLVar, str, aku}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "54e60cb43f73e83ced5ec7d1bcc31bf0", aum = 0)
    @C2499fr
    /* renamed from: aF */
    private void m27345aF(String str, String str2) {
        throw new aWi(new aCE(this, gVx, new Object[]{str, str2}));
    }

    @C0064Am(aul = "9be301fd9c9b0ad7610183429ff097f1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: bX */
    private boolean m27347bX(Player aku) {
        throw new aWi(new aCE(this, gVJ, new Object[]{aku}));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: bY */
    private boolean m27348bY(Player aku) {
        switch (bFf().mo6893i(gVJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVJ, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVJ, new Object[]{aku}));
                break;
        }
        return m27347bX(aku);
    }

    @C0064Am(aul = "f068f2aa6991101d2007bec1dd24dedb", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: bZ */
    private boolean m27349bZ(Player aku) {
        throw new aWi(new aCE(this, gVK, new Object[]{aku}));
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: c */
    private void m27350c(Ticket fLVar) {
        switch (bFf().mo6893i(gVy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVy, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVy, new Object[]{fLVar}));
                break;
        }
        m27346b(fLVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "f09d0201c41877074b249a659688a8aa", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m27351c(Ticket fLVar, String str) {
        throw new aWi(new aCE(this, gVT, new Object[]{fLVar, str}));
    }

    private C3438ra cDR() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gVv);
    }

    @C5566aOg
    @C0064Am(aul = "1b7fdf46cb10d91c8e6c372e7b3584d4", aum = 0)
    @C2499fr
    private ArrayList<Ticket> cDU() {
        throw new aWi(new aCE(this, gVH, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "ee5aedf92c5dddbc1889254ac7c578ca", aum = 0)
    @C2499fr
    private ArrayList<Ticket> cDW() {
        throw new aWi(new aCE(this, gVI, new Object[0]));
    }

    @C0064Am(aul = "e2d7b555336b71e8e0e23c5b505c8947", aum = 0)
    @C5566aOg
    @C2499fr
    private boolean cEa() {
        throw new aWi(new aCE(this, gVP, new Object[0]));
    }

    @C0064Am(aul = "9cce7205d81a5b6e839f941d42c0d777", aum = 0)
    @C5566aOg
    @C2499fr
    private boolean cEc() {
        throw new aWi(new aCE(this, gVU, new Object[0]));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ca */
    private boolean m27352ca(Player aku) {
        switch (bFf().mo6893i(gVK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVK, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVK, new Object[]{aku}));
                break;
        }
        return m27349bZ(aku);
    }

    @C0064Am(aul = "809ea9b58eb524e7a5771b1760d4e83f", aum = 0)
    @C5566aOg
    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    /* renamed from: cb */
    private boolean m27353cb(Player aku) {
        throw new aWi(new aCE(this, gVS, new Object[]{aku}));
    }

    /* renamed from: cj */
    private void m27354cj(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gVv, raVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "f5b5cb03da2142a20bc55f78a24d06e1", aum = 0)
    @C2499fr
    /* renamed from: f */
    private void m27356f(Ticket fLVar) {
        throw new aWi(new aCE(this, gVC, new Object[]{fLVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "c9075cd15e0a410a94f6261ee10f2852", aum = 0)
    @C2499fr
    /* renamed from: h */
    private void m27357h(Ticket fLVar) {
        throw new aWi(new aCE(this, gVD, new Object[]{fLVar}));
    }

    @C5566aOg
    @C0064Am(aul = "3e3109f988e004bdef8af56eca83e653", aum = 0)
    @C2499fr
    /* renamed from: hu */
    private ArrayList<Ticket> m27358hu(boolean z) {
        throw new aWi(new aCE(this, gVL, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "cd8ba7eb95de6c1dbbe80079c13b3931", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: j */
    private void m27359j(Ticket fLVar) {
        throw new aWi(new aCE(this, gVE, new Object[]{fLVar}));
    }

    @C4034yP
    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    @ClientOnly
    /* renamed from: m */
    private void m27362m(Ticket fLVar) {
        switch (bFf().mo6893i(gVF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVF, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVF, new Object[]{fLVar}));
                break;
        }
        m27361l(fLVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "1c23c4fc21a25824f565d359e6df6af9", aum = 0)
    @C2499fr
    /* renamed from: n */
    private void m27363n(Ticket fLVar) {
        throw new aWi(new aCE(this, gVM, new Object[]{fLVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "bd8bd97e967c616c1e985702d0a214af", aum = 0)
    @C2499fr
    /* renamed from: p */
    private void m27364p(Ticket fLVar) {
        throw new aWi(new aCE(this, gVN, new Object[]{fLVar}));
    }

    @C0064Am(aul = "2a7c0b48b8e7880032c208f0ff2c76e7", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: r */
    private void m27365r(Ticket fLVar) {
        throw new aWi(new aCE(this, gVQ, new Object[]{fLVar}));
    }

    @C0064Am(aul = "82cd95247161b5bd5eab13a12c45104f", aum = 0)
    @C5566aOg
    /* renamed from: t */
    private void m27366t(Ticket fLVar) {
        throw new aWi(new aCE(this, gVV, new Object[]{fLVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5331aFf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m27345aF((String) args[0], (String) args[1]);
                return null;
            case 1:
                m27346b((Ticket) args[0]);
                return null;
            case 2:
                return new Boolean(m27360k((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 3:
                m27355d((Ticket) args[0]);
                return null;
            case 4:
                return cDS();
            case 5:
                m27356f((Ticket) args[0]);
                return null;
            case 6:
                m27357h((Ticket) args[0]);
                return null;
            case 7:
                m27359j((Ticket) args[0]);
                return null;
            case 8:
                m27361l((Ticket) args[0]);
                return null;
            case 9:
                m27344a((Ticket) args[0], (String) args[1], (Player) args[2]);
                return null;
            case 10:
                return cDU();
            case 11:
                return cDW();
            case 12:
                return new Boolean(m27347bX((Player) args[0]));
            case 13:
                return new Boolean(m27349bZ((Player) args[0]));
            case 14:
                return m27358hu(((Boolean) args[0]).booleanValue());
            case 15:
                m27363n((Ticket) args[0]);
                return null;
            case 16:
                m27364p((Ticket) args[0]);
                return null;
            case 17:
                return new Boolean(cDY());
            case 18:
                return new Boolean(cEa());
            case 19:
                m27365r((Ticket) args[0]);
                return null;
            case 20:
                m27343a((Ticket) args[0], (String) args[1]);
                return null;
            case 21:
                return new Boolean(m27353cb((Player) args[0]));
            case 22:
                m27351c((Ticket) args[0], (String) args[1]);
                return null;
            case 23:
                return new Boolean(cEc());
            case 24:
                m27366t((Ticket) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: aG */
    public void mo16938aG(String str, String str2) {
        switch (bFf().mo6893i(gVx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVx, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVx, new Object[]{str, str2}));
                break;
        }
        m27345aF(str, str2);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo16939b(Ticket fLVar, String str) {
        switch (bFf().mo6893i(gVR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVR, new Object[]{fLVar, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVR, new Object[]{fLVar, str}));
                break;
        }
        m27343a(fLVar, str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo16940b(Ticket fLVar, String str, Player aku) {
        switch (bFf().mo6893i(gVG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVG, new Object[]{fLVar, str, aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVG, new Object[]{fLVar, str, aku}));
                break;
        }
        m27344a(fLVar, str, aku);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C3438ra<Ticket> cDT() {
        switch (bFf().mo6893i(gVB)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, gVB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gVB, new Object[0]));
                break;
        }
        return cDS();
    }

    @C5566aOg
    @C2499fr
    public ArrayList<Ticket> cDV() {
        switch (bFf().mo6893i(gVH)) {
            case 0:
                return null;
            case 2:
                return (ArrayList) bFf().mo5606d(new aCE(this, gVH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gVH, new Object[0]));
                break;
        }
        return cDU();
    }

    @C5566aOg
    @C2499fr
    public ArrayList<Ticket> cDX() {
        switch (bFf().mo6893i(gVI)) {
            case 0:
                return null;
            case 2:
                return (ArrayList) bFf().mo5606d(new aCE(this, gVI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gVI, new Object[0]));
                break;
        }
        return cDW();
    }

    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    public boolean cDZ() {
        switch (bFf().mo6893i(gVO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVO, new Object[0]));
                break;
        }
        return cDY();
    }

    @C5566aOg
    @C2499fr
    public boolean cEb() {
        switch (bFf().mo6893i(gVP)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVP, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVP, new Object[0]));
                break;
        }
        return cEa();
    }

    @C5566aOg
    @C2499fr
    public boolean cEd() {
        switch (bFf().mo6893i(gVU)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVU, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVU, new Object[0]));
                break;
        }
        return cEc();
    }

    @C5566aOg
    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    /* renamed from: cc */
    public boolean mo16947cc(Player aku) {
        switch (bFf().mo6893i(gVS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVS, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVS, new Object[]{aku}));
                break;
        }
        return m27353cb(aku);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo16948d(Ticket fLVar, String str) {
        switch (bFf().mo6893i(gVT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVT, new Object[]{fLVar, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVT, new Object[]{fLVar, str}));
                break;
        }
        m27351c(fLVar, str);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    @ClientOnly
    /* renamed from: e */
    public void mo16949e(Ticket fLVar) {
        switch (bFf().mo6893i(gVA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVA, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVA, new Object[]{fLVar}));
                break;
        }
        m27355d(fLVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: g */
    public void mo16950g(Ticket fLVar) {
        switch (bFf().mo6893i(gVC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVC, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVC, new Object[]{fLVar}));
                break;
        }
        m27356f(fLVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: hv */
    public ArrayList<Ticket> mo16951hv(boolean z) {
        switch (bFf().mo6893i(gVL)) {
            case 0:
                return null;
            case 2:
                return (ArrayList) bFf().mo5606d(new aCE(this, gVL, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, gVL, new Object[]{new Boolean(z)}));
                break;
        }
        return m27358hu(z);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: i */
    public void mo16952i(Ticket fLVar) {
        switch (bFf().mo6893i(gVD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVD, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVD, new Object[]{fLVar}));
                break;
        }
        m27357h(fLVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: k */
    public void mo16953k(Ticket fLVar) {
        switch (bFf().mo6893i(gVE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVE, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVE, new Object[]{fLVar}));
                break;
        }
        m27359j(fLVar);
    }

    @C5472aKq
    /* renamed from: l */
    public boolean mo16954l(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(gVz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVz, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVz, new Object[]{zt}));
                break;
        }
        return m27360k(zt);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: o */
    public void mo16955o(Ticket fLVar) {
        switch (bFf().mo6893i(gVM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVM, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVM, new Object[]{fLVar}));
                break;
        }
        m27363n(fLVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: q */
    public void mo16956q(Ticket fLVar) {
        switch (bFf().mo6893i(gVN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVN, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVN, new Object[]{fLVar}));
                break;
        }
        m27364p(fLVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: s */
    public void mo16957s(Ticket fLVar) {
        switch (bFf().mo6893i(gVQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVQ, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVQ, new Object[]{fLVar}));
                break;
        }
        m27365r(fLVar);
    }

    @C5566aOg
    /* renamed from: u */
    public void mo16958u(Ticket fLVar) {
        switch (bFf().mo6893i(gVV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gVV, new Object[]{fLVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gVV, new Object[]{fLVar}));
                break;
        }
        m27366t(fLVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "6bae779b8612c1dab6d00fdf2bc640f7", aum = 0)
    @C2499fr
    /* renamed from: b */
    private void m27346b(Ticket fLVar) {
        getPlayer().mo14419f((C1506WA) new C5429aIz());
    }

    @C0064Am(aul = "1ec6d2d64e8938a9b6e04abefbb30129", aum = 0)
    @C5472aKq
    /* renamed from: k */
    private boolean m27360k(C1722ZT<UserConnection> zt) {
        if (zt != null) {
            return zt.bFq().getUser().cEb();
        }
        mo8358lY("Null replication context. Ignoring replication rule");
        return false;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "0fbb368a2d6fdb68eb4d1079fa09adc3", aum = 0)
    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    /* renamed from: d */
    private void m27355d(Ticket fLVar) {
        getPlayer().mo14419f((C1506WA) new C3950xG(fLVar));
    }

    @C0064Am(aul = "6dd207954c8aea2811a194e619927f65", aum = 0)
    private C3438ra<Ticket> cDS() {
        return cDR();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "5fe06f212b759e0c434666a8fb7ae22b", aum = 0)
    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    /* renamed from: l */
    private void m27361l(Ticket fLVar) {
        getPlayer().mo14419f((C1506WA) new C0786LO(fLVar));
    }

    @C0064Am(aul = "44e9902c1dc60414243f64f7f6c888f3", aum = 0)
    @C2499fr(mo18855qf = {"1ec6d2d64e8938a9b6e04abefbb30129"})
    private boolean cDY() {
        for (Ticket fLVar : cDT()) {
            if (fLVar.mo18426sl() == null && fLVar.mo18430st() && !fLVar.mo18433sz()) {
                return true;
            }
        }
        return false;
    }
}
