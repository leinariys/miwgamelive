package game.script.support;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5558aNy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.fL */
/* compiled from: a */
public class Ticket extends TaikodomObject implements C1616Xf {

    /* renamed from: NA */
    public static final C2491fm f7284NA = null;

    /* renamed from: NB */
    public static final C2491fm f7285NB = null;

    /* renamed from: NC */
    public static final C2491fm f7286NC = null;

    /* renamed from: ND */
    public static final C2491fm f7287ND = null;

    /* renamed from: NF */
    public static final C2491fm f7288NF = null;

    /* renamed from: NG */
    public static final C2491fm f7289NG = null;

    /* renamed from: NH */
    public static final C2491fm f7290NH = null;

    /* renamed from: NI */
    public static final C2491fm f7291NI = null;

    /* renamed from: NJ */
    public static final C2491fm f7292NJ = null;

    /* renamed from: NK */
    public static final C2491fm f7293NK = null;

    /* renamed from: NM */
    public static final C2491fm f7294NM = null;

    /* renamed from: NN */
    public static final C2491fm f7295NN = null;

    /* renamed from: NO */
    public static final C2491fm f7296NO = null;

    /* renamed from: NP */
    public static final C2491fm f7297NP = null;

    /* renamed from: NQ */
    public static final C2491fm f7298NQ = null;

    /* renamed from: NR */
    public static final C2491fm f7299NR = null;

    /* renamed from: NS */
    public static final C2491fm f7300NS = null;

    /* renamed from: NT */
    public static final C2491fm f7301NT = null;

    /* renamed from: NU */
    public static final C2491fm f7302NU = null;

    /* renamed from: Nj */
    public static final C5663aRz f7303Nj = null;
    /* renamed from: Nl */
    public static final C5663aRz f7305Nl = null;
    /* renamed from: Nn */
    public static final C5663aRz f7307Nn = null;
    /* renamed from: Np */
    public static final C5663aRz f7309Np = null;
    /* renamed from: Nr */
    public static final C5663aRz f7311Nr = null;
    /* renamed from: Nt */
    public static final C5663aRz f7313Nt = null;
    /* renamed from: Nv */
    public static final C5663aRz f7315Nv = null;
    /* renamed from: Nx */
    public static final C5663aRz f7317Nx = null;
    /* renamed from: Ny */
    public static final C2491fm f7318Ny = null;
    /* renamed from: Nz */
    public static final C2491fm f7319Nz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: hF */
    public static final C2491fm f7322hF = null;
    /* renamed from: hz */
    public static final C5663aRz f7323hz = null;
    /* renamed from: la */
    public static final C5663aRz f7324la = null;
    /* renamed from: lo */
    public static final C2491fm f7325lo = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "684cd3586fa34844fcafba6ecb470852", aum = 2)

    /* renamed from: Nk */
    private static String f7304Nk;
    @C0064Am(aul = "bb352fa9335b7d9b8d28908474884eeb", aum = 3)

    /* renamed from: Nm */
    private static Player f7306Nm;
    @C0064Am(aul = "858b1e90d99c9fd0db4bc0307b875da5", aum = 4)

    /* renamed from: No */
    private static TicketChat f7308No;
    @C0064Am(aul = "92fab5664c7bc1cf8ae08572fa9c99f6", aum = 5)

    /* renamed from: Nq */
    private static boolean f7310Nq;
    @C0064Am(aul = "07a9c351a6320637b8ad032cc93f431b", aum = 7)

    /* renamed from: Ns */
    private static long f7312Ns;
    @C0064Am(aul = "2cb5841df1f346eb98ce7cccddb5caa8", aum = 8)

    /* renamed from: Nu */
    private static String f7314Nu;
    @C0064Am(aul = "969b91ccf3832c1292d023733bacea78", aum = 9)

    /* renamed from: Nw */
    private static long f7316Nw;
    @C0064Am(aul = "45d1953441ca63619808a19eb3b65b06", aum = 0)

    /* renamed from: P */
    private static Player f7320P;
    @C0064Am(aul = "8c421f4424c4c4856ef7b7d9c6fb1aec", aum = 6)

    /* renamed from: Q */
    private static long f7321Q;
    @C0064Am(aul = "85117edee70fdbd5ab6bc0b1c4d1310a", aum = 1)
    private static String subject;

    static {
        m30727V();
    }

    public Ticket() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Ticket(C5540aNg ang) {
        super(ang);
    }

    public Ticket(Player aku, String str, String str2) {
        super((C5540aNg) null);
        super._m_script_init(aku, str, str2);
    }

    /* renamed from: V */
    static void m30727V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 10;
        _m_methodCount = TaikodomObject._m_methodCount + 23;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(Ticket.class, "45d1953441ca63619808a19eb3b65b06", i);
        f7323hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Ticket.class, "85117edee70fdbd5ab6bc0b1c4d1310a", i2);
        f7303Nj = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Ticket.class, "684cd3586fa34844fcafba6ecb470852", i3);
        f7305Nl = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Ticket.class, "bb352fa9335b7d9b8d28908474884eeb", i4);
        f7307Nn = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Ticket.class, "858b1e90d99c9fd0db4bc0307b875da5", i5);
        f7309Np = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Ticket.class, "92fab5664c7bc1cf8ae08572fa9c99f6", i6);
        f7311Nr = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Ticket.class, "8c421f4424c4c4856ef7b7d9c6fb1aec", i7);
        f7324la = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Ticket.class, "07a9c351a6320637b8ad032cc93f431b", i8);
        f7313Nt = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Ticket.class, "2cb5841df1f346eb98ce7cccddb5caa8", i9);
        f7315Nv = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Ticket.class, "969b91ccf3832c1292d023733bacea78", i10);
        f7317Nx = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i12 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 23)];
        C2491fm a = C4105zY.m41624a(Ticket.class, "8649b6eca8ed8ecdbfc557edec9f2468", i12);
        f7322hF = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(Ticket.class, "81819a9e93a4d737ff56b848a0460633", i13);
        f7318Ny = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(Ticket.class, "71b7ce67f7715bf48d474aa8437815e0", i14);
        f7319Nz = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(Ticket.class, "32b84f5703a8a792ebca6a5a14dd7a5a", i15);
        f7284NA = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(Ticket.class, "4ab13d9f1f5001a295f5db6e162f808c", i16);
        f7285NB = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(Ticket.class, "394dbcc3fb0d1c248d2f0b6d5ee8d65b", i17);
        f7286NC = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(Ticket.class, "57e0b75d19ef3326ea11430b72692503", i18);
        f7325lo = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(Ticket.class, "2fe2a0d8543079b4dcc3c9ca16afaaa6", i19);
        f7287ND = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(Ticket.class, "3d48ae713174f48ccbc995591013935f", i20);
        f7288NF = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(Ticket.class, "529f3217b72ea9caffb518d9d9b5f8ad", i21);
        f7289NG = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(Ticket.class, "5224be673fcf5b8ce73acaa9a22cd8c5", i22);
        f7290NH = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(Ticket.class, "30374248f390b940c1c8066910c9548f", i23);
        f7291NI = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(Ticket.class, "cb2f460ebb04fcc2a0e0449c4b0c261e", i24);
        f7292NJ = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(Ticket.class, "bca794e5dcb96e790b7289f60905c925", i25);
        f7293NK = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(Ticket.class, "1097dd5af3c70364a8badc3ea5ffb6ef", i26);
        f7294NM = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(Ticket.class, "aa7cf2f35c66827538ba35b5a55f4587", i27);
        f7295NN = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(Ticket.class, "76640f67a6314dd3800752f2a347b8ac", i28);
        f7296NO = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(Ticket.class, "52980e630de9c51615cf2372809a2d6e", i29);
        f7297NP = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(Ticket.class, "8eb4fdbca5437a8ad40bdd47c2c57723", i30);
        f7298NQ = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(Ticket.class, "9895db2cd83c96e2d7ec62e623e6a2e3", i31);
        f7299NR = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(Ticket.class, "7a65086a7e189fa117974416a81bd6fd", i32);
        f7300NS = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(Ticket.class, "904594d3d1180843b8f438d1335e63d5", i33);
        f7301NT = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(Ticket.class, "f9aca4b1357c26f745163794c7dedefd", i34);
        f7302NU = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Ticket.class, C5558aNy.class, _m_fields, _m_methods);
    }

    /* renamed from: M */
    private void m30721M(long j) {
        bFf().mo5608dq().mo3184b(f7313Nt, j);
    }

    /* renamed from: N */
    private void m30722N(long j) {
        bFf().mo5608dq().mo3184b(f7317Nx, j);
    }

    /* renamed from: P */
    private void m30724P(boolean z) {
        bFf().mo5608dq().mo3153a(f7311Nr, z);
    }

    /* renamed from: a */
    private void m30728a(TicketChat ahm) {
        bFf().mo5608dq().mo3197f(f7309Np, ahm);
    }

    /* renamed from: a */
    private void m30729a(Player aku) {
        bFf().mo5608dq().mo3197f(f7323hz, aku);
    }

    /* renamed from: ah */
    private void m30730ah(String str) {
        bFf().mo5608dq().mo3197f(f7303Nj, str);
    }

    /* renamed from: ai */
    private void m30731ai(String str) {
        bFf().mo5608dq().mo3197f(f7305Nl, str);
    }

    /* renamed from: aj */
    private void m30732aj(String str) {
        bFf().mo5608dq().mo3197f(f7315Nv, str);
    }

    /* renamed from: dG */
    private Player m30736dG() {
        return (Player) bFf().mo5608dq().mo3214p(f7323hz);
    }

    /* renamed from: eH */
    private long m30738eH() {
        return bFf().mo5608dq().mo3213o(f7324la);
    }

    /* renamed from: m */
    private void m30740m(long j) {
        bFf().mo5608dq().mo3184b(f7324la, j);
    }

    /* renamed from: n */
    private void m30741n(Player aku) {
        bFf().mo5608dq().mo3197f(f7307Nn, aku);
    }

    /* renamed from: sa */
    private String m30747sa() {
        return (String) bFf().mo5608dq().mo3214p(f7303Nj);
    }

    /* renamed from: sb */
    private String m30748sb() {
        return (String) bFf().mo5608dq().mo3214p(f7305Nl);
    }

    /* renamed from: sc */
    private Player m30749sc() {
        return (Player) bFf().mo5608dq().mo3214p(f7307Nn);
    }

    /* renamed from: sd */
    private TicketChat m30750sd() {
        return (TicketChat) bFf().mo5608dq().mo3214p(f7309Np);
    }

    /* renamed from: se */
    private boolean m30751se() {
        return bFf().mo5608dq().mo3201h(f7311Nr);
    }

    /* renamed from: sf */
    private long m30752sf() {
        return bFf().mo5608dq().mo3213o(f7313Nt);
    }

    /* renamed from: sg */
    private String m30753sg() {
        return (String) bFf().mo5608dq().mo3214p(f7315Nv);
    }

    /* renamed from: sh */
    private long m30754sh() {
        return bFf().mo5608dq().mo3213o(f7317Nx);
    }

    /* renamed from: P */
    public void mo18411P(long j) {
        switch (bFf().mo6893i(f7296NO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7296NO, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7296NO, new Object[]{new Long(j)}));
                break;
        }
        m30723O(j);
    }

    /* renamed from: R */
    public void mo18412R(long j) {
        switch (bFf().mo6893i(f7302NU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7302NU, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7302NU, new Object[]{new Long(j)}));
                break;
        }
        m30725Q(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5558aNy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m30737dK();
            case 1:
                m30742o((Player) args[0]);
                return null;
            case 2:
                return m30755si();
            case 3:
                m30733ak((String) args[0]);
                return null;
            case 4:
                return m30756sj();
            case 5:
                m30734al((String) args[0]);
                return null;
            case 6:
                return new Long(m30739eW());
            case 7:
                return m30757sk();
            case 8:
                m30743q((Player) args[0]);
                return null;
            case 9:
                return new Boolean(m30744s((Player) args[0]));
            case 10:
                return m30758sm();
            case 11:
                m30759so();
                return null;
            case 12:
                m30760sq();
                return null;
            case 13:
                return new Boolean(m30761ss());
            case 14:
                m30762su();
                return null;
            case 15:
                return new Long(m30763sw());
            case 16:
                m30723O(((Long) args[0]).longValue());
                return null;
            case 17:
                return new Boolean(m30764sy());
            case 18:
                m30726Q(((Boolean) args[0]).booleanValue());
                return null;
            case 19:
                m30735am((String) args[0]);
                return null;
            case 20:
                return m30745sA();
            case 21:
                return new Long(m30746sC());
            case 22:
                m30725Q(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: an */
    public void mo18414an(String str) {
        switch (bFf().mo6893i(f7299NR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7299NR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7299NR, new Object[]{str}));
                break;
        }
        m30735am(str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: dL */
    public Player mo18415dL() {
        switch (bFf().mo6893i(f7322hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f7322hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7322hF, new Object[0]));
                break;
        }
        return m30737dK();
    }

    public String getContent() {
        switch (bFf().mo6893i(f7285NB)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7285NB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7285NB, new Object[0]));
                break;
        }
        return m30756sj();
    }

    public void setContent(String str) {
        switch (bFf().mo6893i(f7286NC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7286NC, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7286NC, new Object[]{str}));
                break;
        }
        m30734al(str);
    }

    public long getCreationTime() {
        switch (bFf().mo6893i(f7325lo)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7325lo, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7325lo, new Object[0]));
                break;
        }
        return m30739eW();
    }

    public String getSubject() {
        switch (bFf().mo6893i(f7319Nz)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7319Nz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7319Nz, new Object[0]));
                break;
        }
        return m30755si();
    }

    public void setSubject(String str) {
        switch (bFf().mo6893i(f7284NA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7284NA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7284NA, new Object[]{str}));
                break;
        }
        m30733ak(str);
    }

    /* renamed from: p */
    public void mo18419p(Player aku) {
        switch (bFf().mo6893i(f7318Ny)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7318Ny, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7318Ny, new Object[]{aku}));
                break;
        }
        m30742o(aku);
    }

    /* renamed from: r */
    public void mo18420r(Player aku) {
        switch (bFf().mo6893i(f7288NF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7288NF, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7288NF, new Object[]{aku}));
                break;
        }
        m30743q(aku);
    }

    /* renamed from: sB */
    public String mo18421sB() {
        switch (bFf().mo6893i(f7300NS)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7300NS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7300NS, new Object[0]));
                break;
        }
        return m30745sA();
    }

    /* renamed from: sD */
    public long mo18422sD() {
        switch (bFf().mo6893i(f7301NT)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7301NT, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7301NT, new Object[0]));
                break;
        }
        return m30746sC();
    }

    public void setDump(boolean z) {
        switch (bFf().mo6893i(f7298NQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7298NQ, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7298NQ, new Object[]{new Boolean(z)}));
                break;
        }
        m30726Q(z);
    }

    /* renamed from: sl */
    public Player mo18426sl() {
        switch (bFf().mo6893i(f7287ND)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f7287ND, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7287ND, new Object[0]));
                break;
        }
        return m30757sk();
    }

    /* renamed from: sn */
    public TicketChat mo18427sn() {
        switch (bFf().mo6893i(f7290NH)) {
            case 0:
                return null;
            case 2:
                return (TicketChat) bFf().mo5606d(new aCE(this, f7290NH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7290NH, new Object[0]));
                break;
        }
        return m30758sm();
    }

    /* renamed from: sp */
    public void mo18428sp() {
        switch (bFf().mo6893i(f7291NI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7291NI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7291NI, new Object[0]));
                break;
        }
        m30759so();
    }

    /* renamed from: sr */
    public void mo18429sr() {
        switch (bFf().mo6893i(f7292NJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7292NJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7292NJ, new Object[0]));
                break;
        }
        m30760sq();
    }

    /* renamed from: st */
    public boolean mo18430st() {
        switch (bFf().mo6893i(f7293NK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7293NK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7293NK, new Object[0]));
                break;
        }
        return m30761ss();
    }

    /* renamed from: sv */
    public void mo18431sv() {
        switch (bFf().mo6893i(f7294NM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7294NM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7294NM, new Object[0]));
                break;
        }
        m30762su();
    }

    /* renamed from: sx */
    public long mo18432sx() {
        switch (bFf().mo6893i(f7295NN)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7295NN, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7295NN, new Object[0]));
                break;
        }
        return m30763sw();
    }

    /* renamed from: sz */
    public boolean mo18433sz() {
        switch (bFf().mo6893i(f7297NP)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7297NP, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7297NP, new Object[0]));
                break;
        }
        return m30764sy();
    }

    /* renamed from: t */
    public boolean mo18434t(Player aku) {
        switch (bFf().mo6893i(f7289NG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7289NG, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7289NG, new Object[]{aku}));
                break;
        }
        return m30744s(aku);
    }

    /* renamed from: a */
    public void mo18413a(Player aku, String str, String str2) {
        super.mo10S();
        m30729a(aku);
        m30730ah(str);
        m30731ai(str2);
        m30740m(currentTimeMillis());
        TicketChat ahm = (TicketChat) bFf().mo6865M(TicketChat.class);
        ahm.mo10S();
        m30728a(ahm);
    }

    @C0064Am(aul = "8649b6eca8ed8ecdbfc557edec9f2468", aum = 0)
    /* renamed from: dK */
    private Player m30737dK() {
        return m30736dG();
    }

    @C0064Am(aul = "81819a9e93a4d737ff56b848a0460633", aum = 0)
    /* renamed from: o */
    private void m30742o(Player aku) {
        m30729a(aku);
    }

    @C0064Am(aul = "71b7ce67f7715bf48d474aa8437815e0", aum = 0)
    /* renamed from: si */
    private String m30755si() {
        return m30747sa();
    }

    @C0064Am(aul = "32b84f5703a8a792ebca6a5a14dd7a5a", aum = 0)
    /* renamed from: ak */
    private void m30733ak(String str) {
        m30730ah(str);
    }

    @C0064Am(aul = "4ab13d9f1f5001a295f5db6e162f808c", aum = 0)
    /* renamed from: sj */
    private String m30756sj() {
        return m30748sb();
    }

    @C0064Am(aul = "394dbcc3fb0d1c248d2f0b6d5ee8d65b", aum = 0)
    /* renamed from: al */
    private void m30734al(String str) {
        m30731ai(str);
    }

    @C0064Am(aul = "57e0b75d19ef3326ea11430b72692503", aum = 0)
    /* renamed from: eW */
    private long m30739eW() {
        return m30738eH();
    }

    @C0064Am(aul = "2fe2a0d8543079b4dcc3c9ca16afaaa6", aum = 0)
    /* renamed from: sk */
    private Player m30757sk() {
        return m30749sc();
    }

    @C0064Am(aul = "3d48ae713174f48ccbc995591013935f", aum = 0)
    /* renamed from: q */
    private void m30743q(Player aku) {
        m30741n(aku);
    }

    @C0064Am(aul = "529f3217b72ea9caffb518d9d9b5f8ad", aum = 0)
    /* renamed from: s */
    private boolean m30744s(Player aku) {
        if (m30749sc() != null) {
            return false;
        }
        mo18420r(aku);
        return true;
    }

    @C0064Am(aul = "5224be673fcf5b8ce73acaa9a22cd8c5", aum = 0)
    /* renamed from: sm */
    private TicketChat m30758sm() {
        return m30750sd();
    }

    @C0064Am(aul = "30374248f390b940c1c8066910c9548f", aum = 0)
    /* renamed from: so */
    private void m30759so() {
        mo18427sn().mo9260sp();
    }

    @C0064Am(aul = "cb2f460ebb04fcc2a0e0449c4b0c261e", aum = 0)
    /* renamed from: sq */
    private void m30760sq() {
        mo18427sn().mo9261sr();
    }

    @C0064Am(aul = "bca794e5dcb96e790b7289f60905c925", aum = 0)
    /* renamed from: ss */
    private boolean m30761ss() {
        if (mo18432sx() == 0) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "1097dd5af3c70364a8badc3ea5ffb6ef", aum = 0)
    /* renamed from: su */
    private void m30762su() {
        mo18411P(currentTimeMillis());
    }

    @C0064Am(aul = "aa7cf2f35c66827538ba35b5a55f4587", aum = 0)
    /* renamed from: sw */
    private long m30763sw() {
        return m30752sf();
    }

    @C0064Am(aul = "76640f67a6314dd3800752f2a347b8ac", aum = 0)
    /* renamed from: O */
    private void m30723O(long j) {
        m30721M(j);
    }

    @C0064Am(aul = "52980e630de9c51615cf2372809a2d6e", aum = 0)
    /* renamed from: sy */
    private boolean m30764sy() {
        return m30751se();
    }

    @C0064Am(aul = "8eb4fdbca5437a8ad40bdd47c2c57723", aum = 0)
    /* renamed from: Q */
    private void m30726Q(boolean z) {
        m30724P(z);
    }

    @C0064Am(aul = "9895db2cd83c96e2d7ec62e623e6a2e3", aum = 0)
    /* renamed from: am */
    private void m30735am(String str) {
        m30732aj(str);
        mo18412R(currentTimeMillis());
    }

    @C0064Am(aul = "7a65086a7e189fa117974416a81bd6fd", aum = 0)
    /* renamed from: sA */
    private String m30745sA() {
        return m30753sg();
    }

    @C0064Am(aul = "904594d3d1180843b8f438d1335e63d5", aum = 0)
    /* renamed from: sC */
    private long m30746sC() {
        return m30754sh();
    }

    @C0064Am(aul = "f9aca4b1357c26f745163794c7dedefd", aum = 0)
    /* renamed from: Q */
    private void m30725Q(long j) {
        m30722N(j);
    }
}
