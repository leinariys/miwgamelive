package game.script.support;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5920acw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;

@C5511aMd
@C6485anp
/* renamed from: a.aHm  reason: case insensitive filesystem */
/* compiled from: a */
public class TicketChat extends TaikodomObject implements C1616Xf {

    /* renamed from: NI */
    public static final C2491fm f3001NI = null;

    /* renamed from: NJ */
    public static final C2491fm f3002NJ = null;

    /* renamed from: NK */
    public static final C2491fm f3003NK = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aVu = null;
    public static final C5663aRz cnX = null;
    public static final C5663aRz hWC = null;
    public static final C2491fm hWD = null;
    public static final C2491fm hWE = null;
    public static final C2491fm hWF = null;
    public static final C2491fm hza = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7851337a8d3ba3e9c9619e8b0f261e64", aum = 1)
    private static C3438ra<Player> cYy;
    @C0064Am(aul = "74750b3fe66ccb9dff2ace8e9a8049e5", aum = 0)
    private static C3438ra<String> fbS;
    @C0064Am(aul = "cb250e1335d138d110f51581b4d5028d", aum = 2)
    private static boolean fbT;

    static {
        m15284V();
    }

    public TicketChat() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TicketChat(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15284V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 7;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(TicketChat.class, "74750b3fe66ccb9dff2ace8e9a8049e5", i);
        aVu = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TicketChat.class, "7851337a8d3ba3e9c9619e8b0f261e64", i2);
        cnX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TicketChat.class, "cb250e1335d138d110f51581b4d5028d", i3);
        hWC = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(TicketChat.class, "bdda28095885a57e64b04305759aed91", i5);
        hWD = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(TicketChat.class, "59d531b90a815adb7c8b1bceb71d5969", i6);
        hWE = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(TicketChat.class, "8f6f945d2c203bd124701f16d62e6c59", i7);
        hza = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(TicketChat.class, "62b0b5b34d27599cf0e3721f5b6818c5", i8);
        f3001NI = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(TicketChat.class, "985fe9141af3a3049a639b76dc72bfd3", i9);
        f3003NK = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(TicketChat.class, "2425c269e577fe50a65cd90c52f82b1c", i10);
        f3002NJ = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(TicketChat.class, "653981f9357669bfb544bbeda3443146", i11);
        hWF = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TicketChat.class, C5920acw.class, _m_fields, _m_methods);
    }

    private C3438ra aQN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnX);
    }

    /* renamed from: aZ */
    private void m15285aZ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnX, raVar);
    }

    /* renamed from: cv */
    private void m15286cv(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aVu, raVar);
    }

    private C3438ra dcV() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aVu);
    }

    private boolean dcW() {
        return bFf().mo5608dq().mo3201h(hWC);
    }

    /* renamed from: jy */
    private void m15287jy(boolean z) {
        bFf().mo5608dq().mo3153a(hWC, z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5920acw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return dcX();
            case 1:
                m15288mJ((String) args[0]);
                return null;
            case 2:
                return cSP();
            case 3:
                m15289so();
                return null;
            case 4:
                return new Boolean(m15291ss());
            case 5:
                m15290sq();
                return null;
            case 6:
                return dcZ();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C3438ra<Player> cSQ() {
        switch (bFf().mo6893i(hza)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, hza, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hza, new Object[0]));
                break;
        }
        return cSP();
    }

    public C3438ra<String> dcY() {
        switch (bFf().mo6893i(hWD)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, hWD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hWD, new Object[0]));
                break;
        }
        return dcX();
    }

    public String dda() {
        switch (bFf().mo6893i(hWF)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hWF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hWF, new Object[0]));
                break;
        }
        return dcZ();
    }

    public void logMsg(String str) {
        switch (bFf().mo6893i(hWE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hWE, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hWE, new Object[]{str}));
                break;
        }
        m15288mJ(str);
    }

    /* renamed from: sp */
    public void mo9260sp() {
        switch (bFf().mo6893i(f3001NI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3001NI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3001NI, new Object[0]));
                break;
        }
        m15289so();
    }

    /* renamed from: sr */
    public void mo9261sr() {
        switch (bFf().mo6893i(f3002NJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3002NJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3002NJ, new Object[0]));
                break;
        }
        m15290sq();
    }

    /* renamed from: st */
    public boolean mo9262st() {
        switch (bFf().mo6893i(f3003NK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f3003NK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3003NK, new Object[0]));
                break;
        }
        return m15291ss();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "bdda28095885a57e64b04305759aed91", aum = 0)
    private C3438ra<String> dcX() {
        return dcV();
    }

    @C0064Am(aul = "59d531b90a815adb7c8b1bceb71d5969", aum = 0)
    /* renamed from: mJ */
    private void m15288mJ(String str) {
        dcY().add(str);
    }

    @C0064Am(aul = "8f6f945d2c203bd124701f16d62e6c59", aum = 0)
    private C3438ra<Player> cSP() {
        return aQN();
    }

    @C0064Am(aul = "62b0b5b34d27599cf0e3721f5b6818c5", aum = 0)
    /* renamed from: so */
    private void m15289so() {
        m15287jy(true);
    }

    @C0064Am(aul = "985fe9141af3a3049a639b76dc72bfd3", aum = 0)
    /* renamed from: ss */
    private boolean m15291ss() {
        return dcW();
    }

    @C0064Am(aul = "2425c269e577fe50a65cd90c52f82b1c", aum = 0)
    /* renamed from: sq */
    private void m15290sq() {
        m15287jy(false);
    }

    @C0064Am(aul = "653981f9357669bfb544bbeda3443146", aum = 0)
    private String dcZ() {
        String str = "";
        Iterator it = dcV().iterator();
        while (true) {
            String str2 = str;
            if (!it.hasNext()) {
                return str2;
            }
            str = String.valueOf(str2) + ((String) it.next()).replace("|", C0147Bi.SEPARATOR) + "||";
        }
    }
}
