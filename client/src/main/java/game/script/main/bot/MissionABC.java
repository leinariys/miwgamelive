package game.script.main.bot;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.ai.npc.DroneAIControllerType;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.MissionTrigger;
import game.script.npc.NPCType;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2060bj;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.game.script.p003ai.npc.DroneAIController;

import java.util.Collection;
import java.util.Random;

@C5511aMd
@C6485anp
/* renamed from: a.zj */
/* compiled from: a */
public class MissionABC extends Mission<MissionABCTemplate> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLS = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLX = null;
    public static final C2491fm aLY = null;
    public static final C2491fm aMe = null;
    public static final C5663aRz bYF = null;
    public static final C5663aRz bYG = null;
    public static final C2491fm bYI = null;
    public static final C2491fm bYJ = null;
    public static final C2491fm bYK = null;
    public static final C2491fm bYL = null;
    public static final C2491fm bYM = null;
    public static final C2491fm bYN = null;
    public static final C2491fm bYO = null;
    public static final C2491fm bYP = null;
    public static final C2491fm bYQ = null;
    public static final C2491fm bYR = null;
    public static final C2491fm beF = null;
    public static final C2491fm beG = null;
    public static final C2491fm beH = null;
    public static final C5663aRz bfK = null;
    public static final C2491fm byB = null;
    public static final C2491fm byC = null;
    public static final C2491fm byD = null;
    public static final C2491fm byE = null;
    public static final C2491fm byF = null;
    public static final C2491fm byG = null;
    public static final C2491fm byH = null;
    public static final C2491fm byI = null;
    public static final C2491fm byJ = null;
    public static final C2491fm byK = null;
    public static final C2491fm byL = null;
    public static final C2491fm byM = null;
    public static final C2491fm byN = null;
    public static final C2491fm byO = null;
    public static final C2491fm byP = null;
    public static final C2491fm byQ = null;
    public static final C2491fm byR = null;
    public static final C2491fm byS = null;
    /* renamed from: xD */
    public static final C5663aRz f9714xD = null;
    private static final String bYH = "botTestWayPoint";
    private static final Random random = new Random();
    private static final long serialVersionUID = -2595947865417351791L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "f1efdf24e4438e39b8c7451ecce1f554", aum = 1)

    /* renamed from: nA */
    private static Vec3d f9710nA = null;
    @C0064Am(aul = "5868fe2c3a1da17d0380dfba46b408e3", aum = 2)

    /* renamed from: nB */
    private static StellarSystem f9711nB = null;
    @C0064Am(aul = "7325bffbd5e0945faa894b178c8e8d06", aum = 3)

    /* renamed from: nC */
    private static NPCType f9712nC = null;
    @C0064Am(aul = "a0127fc136703002284131966bc8e8fd", aum = 0)

    /* renamed from: nz */
    private static BotScript f9713nz = null;

    static {
        m41711V();
    }

    public MissionABC() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionABC(C5540aNg ang) {
        super(ang);
    }

    public MissionABC(BotScript ati) {
        super((C5540aNg) null);
        super._m_script_init(ati);
    }

    /* renamed from: V */
    static void m41711V() {
        _m_fieldCount = Mission._m_fieldCount + 4;
        _m_methodCount = Mission._m_methodCount + 40;
        int i = Mission._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(MissionABC.class, "a0127fc136703002284131966bc8e8fd", i);
        f9714xD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionABC.class, "f1efdf24e4438e39b8c7451ecce1f554", i2);
        bYF = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissionABC.class, "5868fe2c3a1da17d0380dfba46b408e3", i3);
        bYG = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissionABC.class, "7325bffbd5e0945faa894b178c8e8d06", i4);
        bfK = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Mission._m_fields, (Object[]) _m_fields);
        int i6 = Mission._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 40)];
        C2491fm a = C4105zY.m41624a(MissionABC.class, "a115f8c3bbcd0d7492e2f2492e6d63a0", i6);
        bYI = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionABC.class, "8c8001d190acee97c60a29d83defa9a0", i7);
        bYJ = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(MissionABC.class, "6c0347dc8a370909da73ec7801b3b0b5", i8);
        bYK = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(MissionABC.class, "eeafab5351cce260661974d2ef835881", i9);
        bYL = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(MissionABC.class, "a7e32f17b227d3eef010dfac407b3eb2", i10);
        bYM = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(MissionABC.class, "82e5d4ce6e19885585c7ac66090c4dc6", i11);
        bYN = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(MissionABC.class, "d17220f9b8d1e57090d9a2b5ed5ba2c7", i12);
        bYO = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(MissionABC.class, "e232f19182dffbac8871b4503e4291d2", i13);
        aMe = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(MissionABC.class, "97ba3b6ac91ec2a38e32080f9fa12582", i14);
        aLS = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(MissionABC.class, "e3ea6578f2eb31ffdec7ddb174ad72d6", i15);
        beF = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(MissionABC.class, "adfe92aa6c87298eaa4e2a2694641bc4", i16);
        bYP = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(MissionABC.class, "cbac3bcd641d7f637d5636203d12eeda", i17);
        bYQ = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(MissionABC.class, "be7d7ff3f2e0a2e0bf3e178868ec84e6", i18);
        bYR = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(MissionABC.class, "0e88042793a576caa0ec5bff5dfb7afd", i19);
        aLU = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(MissionABC.class, "c89040f8f32e887697c7d018b4c4f22b", i20);
        aLT = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(MissionABC.class, "5a5edbc76294db530d2d5ded9e14495a", i21);
        byI = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(MissionABC.class, "0ddf1e1ac6f9840bb81b2b6693abdadb", i22);
        byJ = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(MissionABC.class, "d112561d24eaa486849fcb8442ebf0a1", i23);
        byM = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        C2491fm a19 = C4105zY.m41624a(MissionABC.class, "845c76457afff068cbb5d682e86cbd6d", i24);
        beH = a19;
        fmVarArr[i24] = a19;
        int i25 = i24 + 1;
        C2491fm a20 = C4105zY.m41624a(MissionABC.class, "e2107edd2f9764f81b2921055faad2d2", i25);
        byN = a20;
        fmVarArr[i25] = a20;
        int i26 = i25 + 1;
        C2491fm a21 = C4105zY.m41624a(MissionABC.class, "cd08115f7bd4840aeb29361d3ccb3b4f", i26);
        byO = a21;
        fmVarArr[i26] = a21;
        int i27 = i26 + 1;
        C2491fm a22 = C4105zY.m41624a(MissionABC.class, "60d9696484257bb615a117af1b869ed3", i27);
        byP = a22;
        fmVarArr[i27] = a22;
        int i28 = i27 + 1;
        C2491fm a23 = C4105zY.m41624a(MissionABC.class, "113c4a9a254cec9adc531cd7a4d37ab8", i28);
        byQ = a23;
        fmVarArr[i28] = a23;
        int i29 = i28 + 1;
        C2491fm a24 = C4105zY.m41624a(MissionABC.class, "755491129b7e09f58b0b3bae3a7299ba", i29);
        aLR = a24;
        fmVarArr[i29] = a24;
        int i30 = i29 + 1;
        C2491fm a25 = C4105zY.m41624a(MissionABC.class, "c1997b44166e76efccd022fea3b9b7af", i30);
        beG = a25;
        fmVarArr[i30] = a25;
        int i31 = i30 + 1;
        C2491fm a26 = C4105zY.m41624a(MissionABC.class, "0348ff5db7736101b27b90a920e53eb6", i31);
        byB = a26;
        fmVarArr[i31] = a26;
        int i32 = i31 + 1;
        C2491fm a27 = C4105zY.m41624a(MissionABC.class, "d61e81b6d11bd0e7fda8d92012663f5b", i32);
        byC = a27;
        fmVarArr[i32] = a27;
        int i33 = i32 + 1;
        C2491fm a28 = C4105zY.m41624a(MissionABC.class, "de658674318001cea71623174f3415a8", i33);
        byF = a28;
        fmVarArr[i33] = a28;
        int i34 = i33 + 1;
        C2491fm a29 = C4105zY.m41624a(MissionABC.class, "e858b10d67a09949ff41c7a9717cf009", i34);
        byG = a29;
        fmVarArr[i34] = a29;
        int i35 = i34 + 1;
        C2491fm a30 = C4105zY.m41624a(MissionABC.class, "0fbedcfe6836b097e0196dcc174f99e2", i35);
        byH = a30;
        fmVarArr[i35] = a30;
        int i36 = i35 + 1;
        C2491fm a31 = C4105zY.m41624a(MissionABC.class, "ac18d985920788f1da72d278be8abac0", i36);
        byR = a31;
        fmVarArr[i36] = a31;
        int i37 = i36 + 1;
        C2491fm a32 = C4105zY.m41624a(MissionABC.class, "26e0e83559e9415a64f0458d8d03f8ff", i37);
        byS = a32;
        fmVarArr[i37] = a32;
        int i38 = i37 + 1;
        C2491fm a33 = C4105zY.m41624a(MissionABC.class, "e64218b3757350a13e737de7b845521f", i38);
        byD = a33;
        fmVarArr[i38] = a33;
        int i39 = i38 + 1;
        C2491fm a34 = C4105zY.m41624a(MissionABC.class, "c0d2bb15cd54db6a2442b73cf93df715", i39);
        byL = a34;
        fmVarArr[i39] = a34;
        int i40 = i39 + 1;
        C2491fm a35 = C4105zY.m41624a(MissionABC.class, "7b646b06187352df2f60d8426e57a86a", i40);
        byK = a35;
        fmVarArr[i40] = a35;
        int i41 = i40 + 1;
        C2491fm a36 = C4105zY.m41624a(MissionABC.class, "001d32b528cc201684301768b468f3a1", i41);
        aLV = a36;
        fmVarArr[i41] = a36;
        int i42 = i41 + 1;
        C2491fm a37 = C4105zY.m41624a(MissionABC.class, "73551fec28e9980dc093bd28b61cfdbd", i42);
        aLW = a37;
        fmVarArr[i42] = a37;
        int i43 = i42 + 1;
        C2491fm a38 = C4105zY.m41624a(MissionABC.class, "cbe338a1f1c07105d73cae83ae85d8a2", i43);
        aLX = a38;
        fmVarArr[i43] = a38;
        int i44 = i43 + 1;
        C2491fm a39 = C4105zY.m41624a(MissionABC.class, "44e71cc3d3caea93c6bfd1f43e1013a0", i44);
        aLY = a39;
        fmVarArr[i44] = a39;
        int i45 = i44 + 1;
        C2491fm a40 = C4105zY.m41624a(MissionABC.class, "0ed738e6c9b75c152bc93d801d6330c0", i45);
        byE = a40;
        fmVarArr[i45] = a40;
        int i46 = i45 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Mission._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionABC.class, C2060bj.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "8c8001d190acee97c60a29d83defa9a0", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m41713a(Station bf, Taikodom dn) {
        throw new aWi(new aCE(this, bYJ, new Object[]{bf, dn}));
    }

    /* renamed from: a */
    private void m41718a(BotScript ati) {
        bFf().mo5608dq().mo3197f(f9714xD, ati);
    }

    private NPCType abn() {
        return (NPCType) bFf().mo5608dq().mo3214p(bfK);
    }

    @C0064Am(aul = "a7e32f17b227d3eef010dfac407b3eb2", aum = 0)
    @C5566aOg
    @C2499fr
    private void arB() {
        throw new aWi(new aCE(this, bYM, new Object[0]));
    }

    private Vec3d arE() {
        switch (bFf().mo6893i(bYN)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bYN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bYN, new Object[0]));
                break;
        }
        return arD();
    }

    @C0064Am(aul = "d17220f9b8d1e57090d9a2b5ed5ba2c7", aum = 0)
    @C5566aOg
    @C2499fr
    private void arF() {
        throw new aWi(new aCE(this, bYO, new Object[0]));
    }

    @C0064Am(aul = "adfe92aa6c87298eaa4e2a2694641bc4", aum = 0)
    @C5566aOg
    @C2499fr
    private Ship arH() {
        throw new aWi(new aCE(this, bYP, new Object[0]));
    }

    @C0064Am(aul = "cbac3bcd641d7f637d5636203d12eeda", aum = 0)
    @C5566aOg
    @C2499fr
    private boolean arJ() {
        throw new aWi(new aCE(this, bYQ, new Object[0]));
    }

    @C0064Am(aul = "be7d7ff3f2e0a2e0bf3e178868ec84e6", aum = 0)
    @C5566aOg
    @C2499fr
    private void arL() {
        throw new aWi(new aCE(this, bYR, new Object[0]));
    }

    private Vec3d arv() {
        return (Vec3d) bFf().mo5608dq().mo3214p(bYF);
    }

    private StellarSystem arw() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(bYG);
    }

    @C0064Am(aul = "6c0347dc8a370909da73ec7801b3b0b5", aum = 0)
    @C5566aOg
    @C2499fr
    private Vec3d arz() {
        throw new aWi(new aCE(this, bYK, new Object[0]));
    }

    /* renamed from: b */
    private NPCType m41725b(Taikodom dn) {
        switch (bFf().mo6893i(bYL)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, bYL, new Object[]{dn}));
            case 3:
                bFf().mo5606d(new aCE(this, bYL, new Object[]{dn}));
                break;
        }
        return m41712a(dn);
    }

    /* renamed from: b */
    private void m41727b(NPCType aed) {
        bFf().mo5608dq().mo3197f(bfK, aed);
    }

    /* renamed from: h */
    private void m41734h(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(bYG, jj);
    }

    /* renamed from: iO */
    private BotScript m41736iO() {
        return (BotScript) bFf().mo5608dq().mo3214p(f9714xD);
    }

    /* renamed from: x */
    private void m41744x(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(bYF, ajr);
    }

    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m41708RJ();
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m41709RL();
    }

    /* renamed from: RO */
    public void mo67RO() {
        switch (bFf().mo6893i(aLX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                break;
        }
        m41710RN();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2060bj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Mission._m_methodCount) {
            case 0:
                return new Boolean(arx());
            case 1:
                m41713a((Station) args[0], (Taikodom) args[1]);
                return null;
            case 2:
                return arz();
            case 3:
                return m41712a((Taikodom) args[0]);
            case 4:
                arB();
                return null;
            case 5:
                return arD();
            case 6:
                arF();
                return null;
            case 7:
                m41735i((Ship) args[0]);
                return null;
            case 8:
                m41733g((Ship) args[0]);
                return null;
            case 9:
                return new Boolean(m41728bk((String) args[0]));
            case 10:
                return arH();
            case 11:
                return new Boolean(arJ());
            case 12:
                arL();
                return null;
            case 13:
                m41709RL();
                return null;
            case 14:
                m41708RJ();
                return null;
            case 15:
                m41743w((Ship) args[0]);
                return null;
            case 16:
                m41739p((Station) args[0]);
                return null;
            case 17:
                m41737k((Gate) args[0]);
                return null;
            case 18:
                m41738n((Ship) args[0]);
                return null;
            case 19:
                m41742t((Station) args[0]);
                return null;
            case 20:
                m41716a((MissionTrigger) args[0]);
                return null;
            case 21:
                akJ();
                return null;
            case 22:
                m41731cq((String) args[0]);
                return null;
            case 23:
                m41724aV((String) args[0]);
                return null;
            case 24:
                m41723a((String) args[0], (Ship) args[1], (Ship) args[2]);
                return null;
            case 25:
                m41720a((String) args[0], (Station) args[1]);
                return null;
            case 26:
                m41721a((String) args[0], (Character) args[1]);
                return null;
            case 27:
                m41722a((String) args[0], (Ship) args[1]);
                return null;
            case 28:
                m41730c((String) args[0], (Ship) args[1]);
                return null;
            case 29:
                m41741s((String) args[0], (String) args[1]);
                return null;
            case 30:
                m41715a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 31:
                akL();
                return null;
            case 32:
                m41726b((Loot) args[0]);
                return null;
            case 33:
                m41732d((Node) args[0]);
                return null;
            case 34:
                m41740r((Station) args[0]);
                return null;
            case 35:
                m41719a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 36:
                m41729c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 37:
                m41710RN();
                return null;
            case 38:
                m41714a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            case 39:
                m41717a((Character) args[0], (Loot) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m41724aV(str);
    }

    public void akK() {
        switch (bFf().mo6893i(byP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                break;
        }
        akJ();
    }

    public void akM() {
        switch (bFf().mo6893i(byS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                break;
        }
        akL();
    }

    @C5566aOg
    @C2499fr
    public Vec3d arA() {
        switch (bFf().mo6893i(bYK)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bYK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bYK, new Object[0]));
                break;
        }
        return arz();
    }

    @C5566aOg
    @C2499fr
    public void arC() {
        switch (bFf().mo6893i(bYM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYM, new Object[0]));
                break;
        }
        arB();
    }

    @C5566aOg
    @C2499fr
    public void arG() {
        switch (bFf().mo6893i(bYO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYO, new Object[0]));
                break;
        }
        arF();
    }

    @C5566aOg
    @C2499fr
    public Ship arI() {
        switch (bFf().mo6893i(bYP)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, bYP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bYP, new Object[0]));
                break;
        }
        return arH();
    }

    @C5566aOg
    @C2499fr
    public boolean arK() {
        switch (bFf().mo6893i(bYQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bYQ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bYQ, new Object[0]));
                break;
        }
        return arJ();
    }

    @C5566aOg
    @C2499fr
    public void arM() {
        switch (bFf().mo6893i(bYR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYR, new Object[0]));
                break;
        }
        arL();
    }

    public boolean ary() {
        switch (bFf().mo6893i(bYI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bYI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bYI, new Object[0]));
                break;
        }
        return arx();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo23376b(Station bf, Taikodom dn) {
        switch (bFf().mo6893i(bYJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bYJ, new Object[]{bf, dn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bYJ, new Object[]{bf, dn}));
                break;
        }
        m41713a(bf, dn);
    }

    /* renamed from: b */
    public void mo85b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m41714a(cr, acm);
    }

    /* renamed from: b */
    public void mo86b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m41715a(ahc, jCVar, i);
    }

    /* renamed from: b */
    public void mo87b(MissionTrigger aus) {
        switch (bFf().mo6893i(byO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                break;
        }
        m41716a(aus);
    }

    /* renamed from: b */
    public void mo88b(Character acx, Loot ael) {
        switch (bFf().mo6893i(byE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                break;
        }
        m41717a(acx, ael);
    }

    /* renamed from: b */
    public void mo90b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m41719a(auq, aag, aag2);
    }

    /* renamed from: b */
    public void mo91b(String str, Station bf) {
        switch (bFf().mo6893i(byB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                break;
        }
        m41720a(str, bf);
    }

    /* renamed from: b */
    public void mo93b(String str, Character acx) {
        switch (bFf().mo6893i(byC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                break;
        }
        m41721a(str, acx);
    }

    /* renamed from: b */
    public void mo94b(String str, Ship fAVar) {
        switch (bFf().mo6893i(byF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                break;
        }
        m41722a(str, fAVar);
    }

    /* renamed from: b */
    public void mo95b(String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(beG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                break;
        }
        m41723a(str, fAVar, fAVar2);
    }

    /* renamed from: bl */
    public boolean mo97bl(String str) {
        switch (bFf().mo6893i(beF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                break;
        }
        return m41728bk(str);
    }

    /* renamed from: c */
    public void mo99c(Loot ael) {
        switch (bFf().mo6893i(byD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                break;
        }
        m41726b(ael);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cr */
    public void mo115cr(String str) {
        switch (bFf().mo6893i(byQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                break;
        }
        m41731cq(str);
    }

    /* renamed from: d */
    public void mo117d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m41729c(auq, aag);
    }

    /* renamed from: d */
    public void mo119d(String str, Ship fAVar) {
        switch (bFf().mo6893i(byG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                break;
        }
        m41730c(str, fAVar);
    }

    /* renamed from: e */
    public void mo121e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m41732d(rPVar);
    }

    /* renamed from: h */
    public void mo125h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m41733g(fAVar);
    }

    /* renamed from: j */
    public void mo130j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m41735i(fAVar);
    }

    /* renamed from: l */
    public void mo131l(Gate fFVar) {
        switch (bFf().mo6893i(byM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                break;
        }
        m41737k(fFVar);
    }

    /* renamed from: o */
    public void mo132o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m41738n(fAVar);
    }

    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m41739p(bf);
    }

    /* renamed from: s */
    public void mo134s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m41740r(bf);
    }

    /* renamed from: t */
    public void mo135t(String str, String str2) {
        switch (bFf().mo6893i(byH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                break;
        }
        m41741s(str, str2);
    }

    /* renamed from: u */
    public void mo136u(Station bf) {
        switch (bFf().mo6893i(byN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                break;
        }
        m41742t(bf);
    }

    /* renamed from: x */
    public void mo138x(Ship fAVar) {
        switch (bFf().mo6893i(byI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                break;
        }
        m41743w(fAVar);
    }

    /* renamed from: b */
    public void mo23377b(BotScript ati) {
        super.mo10S();
        m41718a(ati);
    }

    @C0064Am(aul = "a115f8c3bbcd0d7492e2f2492e6d63a0", aum = 0)
    private boolean arx() {
        return true;
    }

    @C0064Am(aul = "eeafab5351cce260661974d2ef835881", aum = 0)
    /* renamed from: a */
    private NPCType m41712a(Taikodom dn) {
        for (NPCType next : dn.aJY()) {
            if (!(next.bTC() == null || next.bTC().mo19050uX() == null || next.bTC().mo19050uX().getHandle().indexOf("bullf") < 0)) {
                if ((next.bTG() instanceof DroneAIControllerType) || next.bTE() == DroneAIController.class) {
                    return next;
                }
            }
        }
        throw new IllegalStateException("Could not find a drone type");
    }

    @C0064Am(aul = "82e5d4ce6e19885585c7ac66090c4dc6", aum = 0)
    private Vec3d arD() {
        return arv().mo9531q(new Vec3f(random.nextFloat() * 200.0f, random.nextFloat() * 200.0f, random.nextFloat() * 200.0f));
    }

    @C0064Am(aul = "e232f19182dffbac8871b4503e4291d2", aum = 0)
    /* renamed from: i */
    private void m41735i(Ship fAVar) {
        cdk();
    }

    @C0064Am(aul = "97ba3b6ac91ec2a38e32080f9fa12582", aum = 0)
    /* renamed from: g */
    private void m41733g(Ship fAVar) {
        cdk();
    }

    @C0064Am(aul = "e3ea6578f2eb31ffdec7ddb174ad72d6", aum = 0)
    /* renamed from: bk */
    private boolean m41728bk(String str) {
        if (!bYH.equals(str)) {
            return false;
        }
        arC();
        return true;
    }

    @C0064Am(aul = "0e88042793a576caa0ec5bff5dfb7afd", aum = 0)
    /* renamed from: RL */
    private void m41709RL() {
    }

    @C0064Am(aul = "c89040f8f32e887697c7d018b4c4f22b", aum = 0)
    /* renamed from: RJ */
    private void m41708RJ() {
    }

    @C0064Am(aul = "5a5edbc76294db530d2d5ded9e14495a", aum = 0)
    /* renamed from: w */
    private void m41743w(Ship fAVar) {
    }

    @C0064Am(aul = "0ddf1e1ac6f9840bb81b2b6693abdadb", aum = 0)
    /* renamed from: p */
    private void m41739p(Station bf) {
    }

    @C0064Am(aul = "d112561d24eaa486849fcb8442ebf0a1", aum = 0)
    /* renamed from: k */
    private void m41737k(Gate fFVar) {
    }

    @C0064Am(aul = "845c76457afff068cbb5d682e86cbd6d", aum = 0)
    /* renamed from: n */
    private void m41738n(Ship fAVar) {
    }

    @C0064Am(aul = "e2107edd2f9764f81b2921055faad2d2", aum = 0)
    /* renamed from: t */
    private void m41742t(Station bf) {
    }

    @C0064Am(aul = "cd08115f7bd4840aeb29361d3ccb3b4f", aum = 0)
    /* renamed from: a */
    private void m41716a(MissionTrigger aus) {
    }

    @C0064Am(aul = "60d9696484257bb615a117af1b869ed3", aum = 0)
    private void akJ() {
    }

    @C0064Am(aul = "113c4a9a254cec9adc531cd7a4d37ab8", aum = 0)
    /* renamed from: cq */
    private void m41731cq(String str) {
    }

    @C0064Am(aul = "755491129b7e09f58b0b3bae3a7299ba", aum = 0)
    /* renamed from: aV */
    private void m41724aV(String str) {
    }

    @C0064Am(aul = "c1997b44166e76efccd022fea3b9b7af", aum = 0)
    /* renamed from: a */
    private void m41723a(String str, Ship fAVar, Ship fAVar2) {
    }

    @C0064Am(aul = "0348ff5db7736101b27b90a920e53eb6", aum = 0)
    /* renamed from: a */
    private void m41720a(String str, Station bf) {
    }

    @C0064Am(aul = "d61e81b6d11bd0e7fda8d92012663f5b", aum = 0)
    /* renamed from: a */
    private void m41721a(String str, Character acx) {
    }

    @C0064Am(aul = "de658674318001cea71623174f3415a8", aum = 0)
    /* renamed from: a */
    private void m41722a(String str, Ship fAVar) {
    }

    @C0064Am(aul = "e858b10d67a09949ff41c7a9717cf009", aum = 0)
    /* renamed from: c */
    private void m41730c(String str, Ship fAVar) {
    }

    @C0064Am(aul = "0fbedcfe6836b097e0196dcc174f99e2", aum = 0)
    /* renamed from: s */
    private void m41741s(String str, String str2) {
    }

    @C0064Am(aul = "ac18d985920788f1da72d278be8abac0", aum = 0)
    /* renamed from: a */
    private void m41715a(LootType ahc, ItemType jCVar, int i) {
    }

    @C0064Am(aul = "26e0e83559e9415a64f0458d8d03f8ff", aum = 0)
    private void akL() {
    }

    @C0064Am(aul = "e64218b3757350a13e737de7b845521f", aum = 0)
    /* renamed from: b */
    private void m41726b(Loot ael) {
    }

    @C0064Am(aul = "c0d2bb15cd54db6a2442b73cf93df715", aum = 0)
    /* renamed from: d */
    private void m41732d(Node rPVar) {
    }

    @C0064Am(aul = "7b646b06187352df2f60d8426e57a86a", aum = 0)
    /* renamed from: r */
    private void m41740r(Station bf) {
    }

    @C0064Am(aul = "001d32b528cc201684301768b468f3a1", aum = 0)
    /* renamed from: a */
    private void m41719a(Item auq, ItemLocation aag, ItemLocation aag2) {
    }

    @C0064Am(aul = "73551fec28e9980dc093bd28b61cfdbd", aum = 0)
    /* renamed from: c */
    private void m41729c(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "cbe338a1f1c07105d73cae83ae85d8a2", aum = 0)
    /* renamed from: RN */
    private void m41710RN() {
    }

    @C0064Am(aul = "44e71cc3d3caea93c6bfd1f43e1013a0", aum = 0)
    /* renamed from: a */
    private void m41714a(Actor cr, C5260aCm acm) {
    }

    @C0064Am(aul = "0ed738e6c9b75c152bc93d801d6330c0", aum = 0)
    /* renamed from: a */
    private void m41717a(Character acx, Loot ael) {
    }
}
