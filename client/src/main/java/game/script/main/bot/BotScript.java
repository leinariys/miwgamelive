package game.script.main.bot;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.item.EnergyWeapon;
import game.script.item.Weapon;
import game.script.item.WeaponType;
import game.script.player.Player;
import game.script.player.User;
import game.script.ship.Outpost;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.Station;
import game.script.space.Asteroid;
import game.script.space.Node;
import game.script.spacezone.AsteroidZone;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0313EE;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import p001a.*;
import taikodom.game.main.bot.BotScript$BotScriptConsole;
import taikodom.geom.Orientation;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.ati  reason: case insensitive filesystem */
/* compiled from: a */
public class BotScript extends TaikodomObject implements C1616Xf {

    /* renamed from: Ny */
    public static final C2491fm f5333Ny = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPi = null;
    public static final C2491fm avL = null;
    public static final C2491fm gyD = null;
    public static final C2491fm gyE = null;
    public static final C2491fm gyF = null;
    public static final C2491fm gyG = null;
    public static final C2491fm gyH = null;
    public static final C2491fm gyI = null;
    public static final C2491fm gyJ = null;
    public static final C2491fm gyK = null;
    public static final C2491fm gyL = null;
    public static final C2491fm gyM = null;
    public static final C2491fm gyN = null;
    public static final C2491fm gyO = null;
    public static final C2491fm gyP = null;
    public static final C2491fm gyQ = null;
    public static final C2491fm gyR = null;
    public static final C2491fm gyS = null;
    public static final C2491fm gyT = null;
    public static final C2491fm gyU = null;
    public static final C2491fm gyV = null;
    public static final C2491fm gyW = null;
    public static final C2491fm gyX = null;
    public static final C2491fm gyY = null;
    public static final C2491fm gyZ = null;
    public static final C5663aRz gyn = null;
    /* access modifiers changed from: private */
    public static final C5445aJp gyq = new C5445aJp();
    /* access modifiers changed from: private */
    public static final C5445aJp gyr = new C5445aJp();
    /* access modifiers changed from: private */
    public static final Object gyx = new Object();
    public static final C2491fm gza = null;
    public static final C2491fm gzb = null;
    public static final C2491fm gzc = null;
    public static final C2491fm gzd = null;
    public static final C2491fm gze = null;
    public static final C2491fm gzf = null;
    public static final C2491fm gzg = null;
    public static final C2491fm gzh = null;
    public static final C2491fm gzi = null;
    /* renamed from: hF */
    public static final C2491fm f5334hF = null;
    /* renamed from: kX */
    public static final C5663aRz f5336kX = null;
    /* renamed from: lm */
    public static final C2491fm f5337lm = null;
    public static final long serialVersionUID = 0;
    private static final Object gyg = new Object();
    public static C6494any ___iScriptClass;
    /* access modifiers changed from: private */
    public static long gyA = 0;
    /* access modifiers changed from: private */
    public static long gyB = 0;
    /* access modifiers changed from: private */
    public static long gyC = 0;
    public static boolean gyl = false;
    public static Map<String, C6605aqF> gym = new HashMap();
    public static Map<String, Integer> gyo = new HashMap();
    /* access modifiers changed from: private */
    public static float gys;
    /* access modifiers changed from: private */
    public static long gyv = Long.MIN_VALUE;
    /* access modifiers changed from: private */
    public static long gyw = Long.MAX_VALUE;
    /* access modifiers changed from: private */
    public static long gyz = 0;
    @C0064Am(aul = "c2d32955cf33da809fd0f7f7cef01d1e", aum = 1)
    private static MissionABCTemplate cTg;
    private static List<Station> gyh;
    private static List<Station> gyi;
    private static Station gyj;
    private static Station gyk;
    private static Map<Station, Set<Asteroid>> gyp;
    private static long gyt = 0;
    private static long gyu = 0;
    private static boolean gyy = false;
    @C0064Am(aul = "2234eb23931860e6c4c4636853aced0c", aum = 0)

    /* renamed from: iH */
    private static Station f5335iH;

    static {
        m25936V();
    }

    /* renamed from: P */
    private transient Player f5338P;
    private transient Ship aOO;

    public BotScript() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BotScript(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25936V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 37;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BotScript.class, "2234eb23931860e6c4c4636853aced0c", i);
        f5336kX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BotScript.class, "c2d32955cf33da809fd0f7f7cef01d1e", i2);
        gyn = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 37)];
        C2491fm a = C4105zY.m41624a(BotScript.class, "42fdfed24a1816ea4039c002095e0bec", i4);
        gyD = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BotScript.class, "4c72dd04f3d9fd2e70cf0e9cf95186e6", i5);
        f5333Ny = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BotScript.class, "d604981746f177cba550c9c49a3a9491", i6);
        f5337lm = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BotScript.class, "b58d43d16c446ade3924bc1b2f255ed9", i7);
        avL = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BotScript.class, "18673ec9f0db0ee2b0f9d2d20dea779f", i8);
        gyE = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BotScript.class, "8b70d496fe9bc383f6f7a3c6ab1f2c09", i9);
        gyF = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(BotScript.class, "a8f48e724aafb49a4cc137372234596f", i10);
        aPi = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(BotScript.class, "7e53a1ed03c056ba76cd8a75cb70238b", i11);
        f5334hF = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(BotScript.class, "735e0554d39b3ff8a430bb4156abe7fc", i12);
        gyG = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(BotScript.class, "e77dd15d5b32c8bd32ba6984b18d05ae", i13);
        gyH = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        C2491fm a11 = C4105zY.m41624a(BotScript.class, "ee3e9ae6fda99b7dff5a645fbffc4473", i14);
        gyI = a11;
        fmVarArr[i14] = a11;
        int i15 = i14 + 1;
        C2491fm a12 = C4105zY.m41624a(BotScript.class, "c0ced0e58efde0eb071876d41af1a675", i15);
        gyJ = a12;
        fmVarArr[i15] = a12;
        int i16 = i15 + 1;
        C2491fm a13 = C4105zY.m41624a(BotScript.class, "ad56305e15f619c052b581e09606d9eb", i16);
        gyK = a13;
        fmVarArr[i16] = a13;
        int i17 = i16 + 1;
        C2491fm a14 = C4105zY.m41624a(BotScript.class, "7030a7c6f5a4908cdd162d417aafbc02", i17);
        gyL = a14;
        fmVarArr[i17] = a14;
        int i18 = i17 + 1;
        C2491fm a15 = C4105zY.m41624a(BotScript.class, "def39df76facd88515bfdf7b6f896591", i18);
        gyM = a15;
        fmVarArr[i18] = a15;
        int i19 = i18 + 1;
        C2491fm a16 = C4105zY.m41624a(BotScript.class, "54559072181a321c8b43e9c6f5412c8e", i19);
        gyN = a16;
        fmVarArr[i19] = a16;
        int i20 = i19 + 1;
        C2491fm a17 = C4105zY.m41624a(BotScript.class, "9df89a209790efbb3369ceb2318f8acf", i20);
        gyO = a17;
        fmVarArr[i20] = a17;
        int i21 = i20 + 1;
        C2491fm a18 = C4105zY.m41624a(BotScript.class, "dd765f162b796b29e82ba2c13bf5895d", i21);
        gyP = a18;
        fmVarArr[i21] = a18;
        int i22 = i21 + 1;
        C2491fm a19 = C4105zY.m41624a(BotScript.class, "5fff139c4a4f6bfca3bcc38239b8b24b", i22);
        gyQ = a19;
        fmVarArr[i22] = a19;
        int i23 = i22 + 1;
        C2491fm a20 = C4105zY.m41624a(BotScript.class, "8d0203b1750504bbeb8d1e2ab9b09b68", i23);
        gyR = a20;
        fmVarArr[i23] = a20;
        int i24 = i23 + 1;
        C2491fm a21 = C4105zY.m41624a(BotScript.class, "710404ceffa66f17ac147c3d7f362559", i24);
        gyS = a21;
        fmVarArr[i24] = a21;
        int i25 = i24 + 1;
        C2491fm a22 = C4105zY.m41624a(BotScript.class, "48e6199980b859512e28f87f73a951d1", i25);
        gyT = a22;
        fmVarArr[i25] = a22;
        int i26 = i25 + 1;
        C2491fm a23 = C4105zY.m41624a(BotScript.class, "5b663a9356f3210676f2b4efb14e45b1", i26);
        gyU = a23;
        fmVarArr[i26] = a23;
        int i27 = i26 + 1;
        C2491fm a24 = C4105zY.m41624a(BotScript.class, "0e14aac87db092e83f62f7f8138015a3", i27);
        gyV = a24;
        fmVarArr[i27] = a24;
        int i28 = i27 + 1;
        C2491fm a25 = C4105zY.m41624a(BotScript.class, "78d5538e289331e8d7893598ed173f37", i28);
        gyW = a25;
        fmVarArr[i28] = a25;
        int i29 = i28 + 1;
        C2491fm a26 = C4105zY.m41624a(BotScript.class, "1358ec74f19ee04bb4524241eef99a4d", i29);
        gyX = a26;
        fmVarArr[i29] = a26;
        int i30 = i29 + 1;
        C2491fm a27 = C4105zY.m41624a(BotScript.class, "a87be9308abb3a9b82e24476ff25c40d", i30);
        gyY = a27;
        fmVarArr[i30] = a27;
        int i31 = i30 + 1;
        C2491fm a28 = C4105zY.m41624a(BotScript.class, "e44ead59a08897e7758e1e4b170668b5", i31);
        gyZ = a28;
        fmVarArr[i31] = a28;
        int i32 = i31 + 1;
        C2491fm a29 = C4105zY.m41624a(BotScript.class, "1c7a6659db54cc5b1077a950ca657f1b", i32);
        gza = a29;
        fmVarArr[i32] = a29;
        int i33 = i32 + 1;
        C2491fm a30 = C4105zY.m41624a(BotScript.class, "a7a21f752fe6681a2dc74b9dc2399e7d", i33);
        gzb = a30;
        fmVarArr[i33] = a30;
        int i34 = i33 + 1;
        C2491fm a31 = C4105zY.m41624a(BotScript.class, "680253e3028b461f2183a80d31a0f2fe", i34);
        gzc = a31;
        fmVarArr[i34] = a31;
        int i35 = i34 + 1;
        C2491fm a32 = C4105zY.m41624a(BotScript.class, "afbd5b55a79c1b30090d3f8323b83bb4", i35);
        gzd = a32;
        fmVarArr[i35] = a32;
        int i36 = i35 + 1;
        C2491fm a33 = C4105zY.m41624a(BotScript.class, "c096be0e59d356b60c51398296d5f32d", i36);
        gze = a33;
        fmVarArr[i36] = a33;
        int i37 = i36 + 1;
        C2491fm a34 = C4105zY.m41624a(BotScript.class, "fedaf418b35dc06cb87428b144f74561", i37);
        gzf = a34;
        fmVarArr[i37] = a34;
        int i38 = i37 + 1;
        C2491fm a35 = C4105zY.m41624a(BotScript.class, "023263ead7bf70a5f286f661222681a2", i38);
        gzg = a35;
        fmVarArr[i38] = a35;
        int i39 = i38 + 1;
        C2491fm a36 = C4105zY.m41624a(BotScript.class, "030c32acc9e7fda46cf8d21ee882af26", i39);
        gzh = a36;
        fmVarArr[i39] = a36;
        int i40 = i39 + 1;
        C2491fm a37 = C4105zY.m41624a(BotScript.class, "1b5c05b2ef799994a4eff1d48c0026f3", i40);
        gzi = a37;
        fmVarArr[i40] = a37;
        int i41 = i40 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BotScript.class, C0313EE.class, _m_fields, _m_methods);
    }

    public static long[] cvp() {
        long[] jArr;
        synchronized (gyx) {
            jArr = new long[]{gyu, gyt};
            gyu = 0;
            gyt = 0;
        }
        return jArr;
    }

    @C0064Am(aul = "735e0554d39b3ff8a430bb4156abe7fc", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private Player m25937a(User adk, String str, Station bf) {
        throw new aWi(new aCE(this, gyG, new Object[]{adk, str, bf}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "680253e3028b461f2183a80d31a0f2fe", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m25938a(long j, C1285Sv.C1286a aVar, int i, long j2) {
        throw new aWi(new aCE(this, gzc, new Object[]{new Long(j), aVar, new Integer(i), new Long(j2)}));
    }

    /* renamed from: a */
    private void m25939a(Station bf) {
        bFf().mo5608dq().mo3197f(f5336kX, bf);
    }

    /* renamed from: a */
    private void m25940a(MissionABCTemplate cXVar) {
        bFf().mo5608dq().mo3197f(gyn, cXVar);
    }

    @C0064Am(aul = "78d5538e289331e8d7893598ed173f37", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m25941a(Ship fAVar, Vec3d ajr) {
        throw new aWi(new aCE(this, gyW, new Object[]{fAVar, ajr}));
    }

    @C0064Am(aul = "ee3e9ae6fda99b7dff5a645fbffc4473", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m25942a(Ship fAVar, Vec3d ajr, Vec3d ajr2) {
        throw new aWi(new aCE(this, gyI, new Object[]{fAVar, ajr, ajr2}));
    }

    @C0064Am(aul = "1358ec74f19ee04bb4524241eef99a4d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m25943a(Ship fAVar, Weapon adv) {
        throw new aWi(new aCE(this, gyX, new Object[]{fAVar, adv}));
    }

    @C0064Am(aul = "fedaf418b35dc06cb87428b144f74561", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: aX */
    private float m25947aX(Actor cr) {
        throw new aWi(new aCE(this, gzf, new Object[]{cr}));
    }

    @C0064Am(aul = "0e14aac87db092e83f62f7f8138015a3", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ab */
    private void m25948ab(Ship fAVar) {
        throw new aWi(new aCE(this, gyV, new Object[]{fAVar}));
    }

    @C0064Am(aul = "a7a21f752fe6681a2dc74b9dc2399e7d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ad */
    private void m25949ad(Ship fAVar) {
        throw new aWi(new aCE(this, gzb, new Object[]{fAVar}));
    }

    @C0064Am(aul = "710404ceffa66f17ac147c3d7f362559", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: am */
    private Asteroid m25950am(Station bf) {
        throw new aWi(new aCE(this, gyS, new Object[]{bf}));
    }

    @C0064Am(aul = "030c32acc9e7fda46cf8d21ee882af26", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ao */
    private String m25951ao(Station bf) {
        throw new aWi(new aCE(this, gzh, new Object[]{bf}));
    }

    private void aqT() {
        switch (bFf().mo6893i(gyD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyD, new Object[0]));
                break;
        }
        cva();
    }

    /* renamed from: b */
    private void m25952b(Node rPVar, List<Station> list, List<Station> list2) {
        switch (bFf().mo6893i(gyM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyM, new Object[]{rPVar, list, list2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyM, new Object[]{rPVar, list, list2}));
                break;
        }
        m25944a(rPVar, list, list2);
    }

    /* renamed from: b */
    private void m25953b(Node rPVar, List<Station> list, Map<Station, Set<Asteroid>> map) {
        switch (bFf().mo6893i(gyN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyN, new Object[]{rPVar, list, map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyN, new Object[]{rPVar, list, map}));
                break;
        }
        m25945a(rPVar, list, map);
    }

    /* renamed from: b */
    private void m25954b(Node rPVar, Set<Asteroid> set, List<Station> list, Map<Station, Set<Asteroid>> map) {
        switch (bFf().mo6893i(gyO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyO, new Object[]{rPVar, set, list, map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyO, new Object[]{rPVar, set, list, map}));
                break;
        }
        m25946a(rPVar, set, list, map);
    }

    @C0064Am(aul = "c096be0e59d356b60c51398296d5f32d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: bT */
    private void m25956bT(Player aku) {
        throw new aWi(new aCE(this, gze, new Object[]{aku}));
    }

    @C0064Am(aul = "a87be9308abb3a9b82e24476ff25c40d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private void m25957c(Ship fAVar, Vec3d ajr) {
        throw new aWi(new aCE(this, gyY, new Object[]{fAVar, ajr}));
    }

    private MissionABCTemplate cuZ() {
        return (MissionABCTemplate) bFf().mo5608dq().mo3214p(gyn);
    }

    @C0064Am(aul = "18673ec9f0db0ee2b0f9d2d20dea779f", aum = 0)
    @C5566aOg
    @C2499fr
    private MissionABC cvb() {
        throw new aWi(new aCE(this, gyE, new Object[0]));
    }

    @C0064Am(aul = "8b70d496fe9bc383f6f7a3c6ab1f2c09", aum = 0)
    @C5566aOg
    @C2499fr
    private Actor cvd() {
        throw new aWi(new aCE(this, gyF, new Object[0]));
    }

    private void cvg() {
        switch (bFf().mo6893i(gyK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyK, new Object[0]));
                break;
        }
        cvf();
    }

    @C5566aOg
    @C0064Am(aul = "dd765f162b796b29e82ba2c13bf5895d", aum = 0)
    @C2499fr
    private List<Station> cvh() {
        throw new aWi(new aCE(this, gyP, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "5fff139c4a4f6bfca3bcc38239b8b24b", aum = 0)
    @C2499fr
    private List<Station> cvj() {
        throw new aWi(new aCE(this, gyQ, new Object[0]));
    }

    @C0064Am(aul = "48e6199980b859512e28f87f73a951d1", aum = 0)
    @C5566aOg
    @C2499fr
    private Station cvl() {
        throw new aWi(new aCE(this, gyT, new Object[0]));
    }

    @C0064Am(aul = "5b663a9356f3210676f2b4efb14e45b1", aum = 0)
    @C5566aOg
    @C2499fr
    private Station cvn() {
        throw new aWi(new aCE(this, gyU, new Object[0]));
    }

    @C0064Am(aul = "afbd5b55a79c1b30090d3f8323b83bb4", aum = 0)
    @C5566aOg
    @C2499fr
    private Vec3d cvq() {
        throw new aWi(new aCE(this, gzd, new Object[0]));
    }

    @C0064Am(aul = "7e53a1ed03c056ba76cd8a75cb70238b", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: dK */
    private Player m25958dK() {
        throw new aWi(new aCE(this, f5334hF, new Object[0]));
    }

    /* renamed from: eF */
    private Station m25959eF() {
        return (Station) bFf().mo5608dq().mo3214p(f5336kX);
    }

    @C0064Am(aul = "d604981746f177cba550c9c49a3a9491", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: eS */
    private Station m25960eS() {
        throw new aWi(new aCE(this, f5337lm, new Object[0]));
    }

    @C0064Am(aul = "b58d43d16c446ade3924bc1b2f255ed9", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: g */
    private void m25961g(Station bf) {
        throw new aWi(new aCE(this, avL, new Object[]{bf}));
    }

    /* renamed from: gz */
    private synchronized void m25963gz(boolean z) {
        switch (bFf().mo6893i(gyL)) {
            case 0:
                break;
            case 2:
                bFf().mo5606d(new aCE(this, gyL, new Object[]{new Boolean(z)}));
                break;
            case 3:
                bFf().mo5606d(new aCE(this, gyL, new Object[]{new Boolean(z)}));
                break;
        }
        m25962gy(z);
    }

    /* renamed from: i */
    private Orientation m25965i(Vec3d ajr, Vec3d ajr2) {
        switch (bFf().mo6893i(gyJ)) {
            case 0:
                return null;
            case 2:
                return (Orientation) bFf().mo5606d(new aCE(this, gyJ, new Object[]{ajr, ajr2}));
            case 3:
                bFf().mo5606d(new aCE(this, gyJ, new Object[]{ajr, ajr2}));
                break;
        }
        return m25964h(ajr, ajr2);
    }

    @C0064Am(aul = "023263ead7bf70a5f286f661222681a2", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: iK */
    private long m25966iK(long j) {
        throw new aWi(new aCE(this, gzg, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "e44ead59a08897e7758e1e4b170668b5", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: j */
    private void m25967j(Node rPVar) {
        throw new aWi(new aCE(this, gyZ, new Object[]{rPVar}));
    }

    @C0064Am(aul = "8d0203b1750504bbeb8d1e2ab9b09b68", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: jU */
    private Station m25968jU(String str) {
        throw new aWi(new aCE(this, gyR, new Object[]{str}));
    }

    @C0064Am(aul = "a8f48e724aafb49a4cc137372234596f", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: l */
    private void m25970l(Ship fAVar) {
        throw new aWi(new aCE(this, aPi, new Object[]{fAVar}));
    }

    @C0064Am(aul = "4c72dd04f3d9fd2e70cf0e9cf95186e6", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: o */
    private void m25971o(Player aku) {
        throw new aWi(new aCE(this, f5333Ny, new Object[]{aku}));
    }

    @C0064Am(aul = "1b5c05b2ef799994a4eff1d48c0026f3", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: u */
    private EnergyWeapon m25972u(WeaponType apt) {
        throw new aWi(new aCE(this, gzi, new Object[]{apt}));
    }

    @C0064Am(aul = "e77dd15d5b32c8bd32ba6984b18d05ae", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: x */
    private Ship m25974x(ShipType ng) {
        throw new aWi(new aCE(this, gyH, new Object[]{ng}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0313EE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                cva();
                return null;
            case 1:
                m25971o((Player) args[0]);
                return null;
            case 2:
                return m25960eS();
            case 3:
                m25961g((Station) args[0]);
                return null;
            case 4:
                return cvb();
            case 5:
                return cvd();
            case 6:
                m25970l((Ship) args[0]);
                return null;
            case 7:
                return m25958dK();
            case 8:
                return m25937a((User) args[0], (String) args[1], (Station) args[2]);
            case 9:
                return m25974x((ShipType) args[0]);
            case 10:
                m25942a((Ship) args[0], (Vec3d) args[1], (Vec3d) args[2]);
                return null;
            case 11:
                return m25964h((Vec3d) args[0], (Vec3d) args[1]);
            case 12:
                cvf();
                return null;
            case 13:
                m25962gy(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                m25944a((Node) args[0], (List<Station>) (List) args[1], (List<Station>) (List) args[2]);
                return null;
            case 15:
                m25945a((Node) args[0], (List<Station>) (List) args[1], (Map<Station, Set<Asteroid>>) (Map) args[2]);
                return null;
            case 16:
                m25946a((Node) args[0], (Set<Asteroid>) (Set) args[1], (List<Station>) (List) args[2], (Map<Station, Set<Asteroid>>) (Map) args[3]);
                return null;
            case 17:
                return cvh();
            case 18:
                return cvj();
            case 19:
                return m25968jU((String) args[0]);
            case 20:
                return m25950am((Station) args[0]);
            case 21:
                return cvl();
            case 22:
                return cvn();
            case 23:
                m25948ab((Ship) args[0]);
                return null;
            case 24:
                m25941a((Ship) args[0], (Vec3d) args[1]);
                return null;
            case 25:
                m25943a((Ship) args[0], (Weapon) args[1]);
                return null;
            case 26:
                m25957c((Ship) args[0], (Vec3d) args[1]);
                return null;
            case 27:
                m25967j((Node) args[0]);
                return null;
            case 28:
                m25955bR((Player) args[0]);
                return null;
            case 29:
                m25949ad((Ship) args[0]);
                return null;
            case 30:
                m25938a(((Long) args[0]).longValue(), (C1285Sv.C1286a) args[1], ((Integer) args[2]).intValue(), ((Long) args[3]).longValue());
                return null;
            case 31:
                return cvq();
            case 32:
                m25956bT((Player) args[0]);
                return null;
            case 33:
                return new Float(m25947aX((Actor) args[0]));
            case 34:
                return new Long(m25966iK(((Long) args[0]).longValue()));
            case 35:
                return m25951ao((Station) args[0]);
            case 36:
                return m25972u((WeaponType) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: aY */
    public float mo16105aY(Actor cr) {
        switch (bFf().mo6893i(gzf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gzf, new Object[]{cr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gzf, new Object[]{cr}));
                break;
        }
        return m25947aX(cr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ac */
    public void mo16106ac(Ship fAVar) {
        switch (bFf().mo6893i(gyV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyV, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyV, new Object[]{fAVar}));
                break;
        }
        m25948ab(fAVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ae */
    public void mo16107ae(Ship fAVar) {
        switch (bFf().mo6893i(gzb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gzb, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gzb, new Object[]{fAVar}));
                break;
        }
        m25949ad(fAVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: an */
    public Asteroid mo16108an(Station bf) {
        switch (bFf().mo6893i(gyS)) {
            case 0:
                return null;
            case 2:
                return (Asteroid) bFf().mo5606d(new aCE(this, gyS, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, gyS, new Object[]{bf}));
                break;
        }
        return m25950am(bf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ap */
    public String mo16109ap(Station bf) {
        switch (bFf().mo6893i(gzh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gzh, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, gzh, new Object[]{bf}));
                break;
        }
        return m25951ao(bf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public Player mo16110b(User adk, String str, Station bf) {
        switch (bFf().mo6893i(gyG)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, gyG, new Object[]{adk, str, bf}));
            case 3:
                bFf().mo5606d(new aCE(this, gyG, new Object[]{adk, str, bf}));
                break;
        }
        return m25937a(adk, str, bf);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo16111b(long j, C1285Sv.C1286a aVar, int i, long j2) {
        switch (bFf().mo6893i(gzc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gzc, new Object[]{new Long(j), aVar, new Integer(i), new Long(j2)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gzc, new Object[]{new Long(j), aVar, new Integer(i), new Long(j2)}));
                break;
        }
        m25938a(j, aVar, i, j2);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo16112b(Ship fAVar, Vec3d ajr) {
        switch (bFf().mo6893i(gyW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyW, new Object[]{fAVar, ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyW, new Object[]{fAVar, ajr}));
                break;
        }
        m25941a(fAVar, ajr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo16113b(Ship fAVar, Vec3d ajr, Vec3d ajr2) {
        switch (bFf().mo6893i(gyI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyI, new Object[]{fAVar, ajr, ajr2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyI, new Object[]{fAVar, ajr, ajr2}));
                break;
        }
        m25942a(fAVar, ajr, ajr2);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo16114b(Ship fAVar, Weapon adv) {
        switch (bFf().mo6893i(gyX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyX, new Object[]{fAVar, adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyX, new Object[]{fAVar, adv}));
                break;
        }
        m25943a(fAVar, adv);
    }

    /* renamed from: bS */
    public void mo16115bS(Player aku) {
        switch (bFf().mo6893i(gza)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gza, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gza, new Object[]{aku}));
                break;
        }
        m25955bR(aku);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: bU */
    public void mo16116bU(Player aku) {
        switch (bFf().mo6893i(gze)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gze, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gze, new Object[]{aku}));
                break;
        }
        m25956bT(aku);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    public MissionABC cvc() {
        switch (bFf().mo6893i(gyE)) {
            case 0:
                return null;
            case 2:
                return (MissionABC) bFf().mo5606d(new aCE(this, gyE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gyE, new Object[0]));
                break;
        }
        return cvb();
    }

    @C5566aOg
    @C2499fr
    public Actor cve() {
        switch (bFf().mo6893i(gyF)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, gyF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gyF, new Object[0]));
                break;
        }
        return cvd();
    }

    @C5566aOg
    @C2499fr
    public List<Station> cvi() {
        switch (bFf().mo6893i(gyP)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gyP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gyP, new Object[0]));
                break;
        }
        return cvh();
    }

    @C5566aOg
    @C2499fr
    public List<Station> cvk() {
        switch (bFf().mo6893i(gyQ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gyQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gyQ, new Object[0]));
                break;
        }
        return cvj();
    }

    @C5566aOg
    @C2499fr
    public synchronized Station cvm() {
        Station bf;
        switch (bFf().mo6893i(gyT)) {
            case 0:
                bf = null;
                break;
            case 2:
                bf = (Station) bFf().mo5606d(new aCE(this, gyT, new Object[0]));
                break;
            case 3:
                bFf().mo5606d(new aCE(this, gyT, new Object[0]));
                break;
        }
        bf = cvl();
        return bf;
    }

    @C5566aOg
    @C2499fr
    public synchronized Station cvo() {
        Station bf;
        switch (bFf().mo6893i(gyU)) {
            case 0:
                bf = null;
                break;
            case 2:
                bf = (Station) bFf().mo5606d(new aCE(this, gyU, new Object[0]));
                break;
            case 3:
                bFf().mo5606d(new aCE(this, gyU, new Object[0]));
                break;
        }
        bf = cvn();
        return bf;
    }

    @C5566aOg
    @C2499fr
    public Vec3d cvr() {
        switch (bFf().mo6893i(gzd)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, gzd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gzd, new Object[0]));
                break;
        }
        return cvq();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo16124d(Ship fAVar, Vec3d ajr) {
        switch (bFf().mo6893i(gyY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyY, new Object[]{fAVar, ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyY, new Object[]{fAVar, ajr}));
                break;
        }
        m25957c(fAVar, ajr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: dL */
    public Player mo16125dL() {
        switch (bFf().mo6893i(f5334hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f5334hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5334hF, new Object[0]));
                break;
        }
        return m25958dK();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: eT */
    public Station mo16126eT() {
        switch (bFf().mo6893i(f5337lm)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f5337lm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5337lm, new Object[0]));
                break;
        }
        return m25960eS();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: h */
    public void mo16127h(Station bf) {
        switch (bFf().mo6893i(avL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avL, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avL, new Object[]{bf}));
                break;
        }
        m25961g(bf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: iL */
    public long mo16128iL(long j) {
        switch (bFf().mo6893i(gzg)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gzg, new Object[]{new Long(j)}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gzg, new Object[]{new Long(j)}));
                break;
        }
        return m25966iK(j);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: jV */
    public Station mo16129jV(String str) {
        switch (bFf().mo6893i(gyR)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, gyR, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, gyR, new Object[]{str}));
                break;
        }
        return m25968jU(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: k */
    public void mo16130k(Node rPVar) {
        switch (bFf().mo6893i(gyZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gyZ, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gyZ, new Object[]{rPVar}));
                break;
        }
        m25967j(rPVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: m */
    public void mo16131m(Ship fAVar) {
        switch (bFf().mo6893i(aPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                break;
        }
        m25970l(fAVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: p */
    public void mo16132p(Player aku) {
        switch (bFf().mo6893i(f5333Ny)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5333Ny, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5333Ny, new Object[]{aku}));
                break;
        }
        m25971o(aku);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: v */
    public EnergyWeapon mo16133v(WeaponType apt) {
        switch (bFf().mo6893i(gzi)) {
            case 0:
                return null;
            case 2:
                return (EnergyWeapon) bFf().mo5606d(new aCE(this, gzi, new Object[]{apt}));
            case 3:
                bFf().mo5606d(new aCE(this, gzi, new Object[]{apt}));
                break;
        }
        return m25972u(apt);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: y */
    public Ship mo16134y(ShipType ng) {
        switch (bFf().mo6893i(gyH)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, gyH, new Object[]{ng}));
            case 3:
                bFf().mo5606d(new aCE(this, gyH, new Object[]{ng}));
                break;
        }
        return m25974x(ng);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        synchronized (gyx) {
            if (!gyy) {
                gyy = true;
                aqT();
            }
        }
        gyC++;
    }

    @C0064Am(aul = "42fdfed24a1816ea4039c002095e0bec", aum = 0)
    private void cva() {
        MBeanServer platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
        if (platformMBeanServer != null) {
            try {
                platformMBeanServer.registerMBean(new BotScript$BotScriptConsole(this), new ObjectName("Bots:name=Statistics"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @C0064Am(aul = "c0ced0e58efde0eb071876d41af1a675", aum = 0)
    /* renamed from: h */
    private Orientation m25964h(Vec3d ajr, Vec3d ajr2) {
        Vec3f aB = ajr2.mo9485aB(ajr);
        aB.normalize();
        aB.negate();
        Vec3f b = new Vec3f(0.0f, 1.0f, 0.0f).mo23476b(aB);
        b.normalize();
        Vec3f b2 = aB.mo23476b(b);
        b2.normalize();
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.mo13988aD(b);
        ajk.mo13989aE(b2);
        ajk.mo13990aF(aB);
        return new Orientation(ajk);
    }

    @C0064Am(aul = "ad56305e15f619c052b581e09606d9eb", aum = 0)
    private void cvf() {
        synchronized (gyg) {
            if (gyh == null) {
                m25963gz(false);
            }
        }
    }

    @C0064Am(aul = "7030a7c6f5a4908cdd162d417aafbc02", aum = 0)
    /* renamed from: gy */
    private void m25962gy(boolean z) {
        synchronized (gyg) {
            if (!z) {
                if (gyh != null) {
                    return;
                }
            }
            List<Station> dgK = MessageContainer.init();
            List<Station> dgK2 = MessageContainer.init();
            Map<Station, Set<Asteroid>> newMap = MessageContainer.newMap();
            for (Node next : ala().aKa().get(0).getNodes()) {
                m25953b(next, dgK2, newMap);
                m25952b(next, dgK2, dgK);
            }
            gyi = dgK;
            gyh = dgK2;
            gyp = newMap;
            System.out.println("Miner Data Loaded...");
        }
    }

    @C0064Am(aul = "def39df76facd88515bfdf7b6f896591", aum = 0)
    /* renamed from: a */
    private void m25944a(Node rPVar, List<Station> list, List<Station> list2) {
        for (Actor next : rPVar.mo21624Zw()) {
            if ((next instanceof Station) && !list.contains(next) && !(next instanceof Outpost)) {
                list2.add((Station) next);
            }
        }
    }

    @C0064Am(aul = "54559072181a321c8b43e9c6f5412c8e", aum = 0)
    /* renamed from: a */
    private void m25945a(Node rPVar, List<Station> list, Map<Station, Set<Asteroid>> map) {
        Set<AsteroidZone> ZQ = rPVar.mo21615ZQ();
        Set dgI = MessageContainer.dgI();
        if (ZQ != null) {
            for (AsteroidZone btVar : ZQ) {
                if (btVar != null) {
                    m25954b(rPVar, (Set<Asteroid>) dgI, list, map);
                }
            }
        }
    }

    @C0064Am(aul = "9df89a209790efbb3369ceb2318f8acf", aum = 0)
    /* renamed from: a */
    private void m25946a(Node rPVar, Set<Asteroid> set, List<Station> list, Map<Station, Set<Asteroid>> map) {
        for (Actor next : rPVar.mo21624Zw()) {
            if ((next instanceof Station) && !(next instanceof Outpost)) {
                list.add((Station) next);
                map.put((Station) next, set);
            }
            if (next instanceof Asteroid) {
                set.add((Asteroid) next);
            }
        }
    }

    @C0064Am(aul = "1c7a6659db54cc5b1077a950ca657f1b", aum = 0)
    /* renamed from: bR */
    private void m25955bR(Player aku) {
        aku.dxc().cOp();
        aku.dxc().cOi();
    }
}
