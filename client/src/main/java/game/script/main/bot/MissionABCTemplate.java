package game.script.main.bot;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.mission.MissionTemplate;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6392amA;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.cX */
/* compiled from: a */
public class MissionABCTemplate extends MissionTemplate implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: kX */
    public static final C5663aRz f6143kX = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xD */
    public static final C5663aRz f6145xD = null;
    /* renamed from: xE */
    public static final C2491fm f6146xE = null;
    /* renamed from: xF */
    public static final C2491fm f6147xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ab5abe8e8f52630e7412b455824e4227", aum = 1)

    /* renamed from: iH */
    private static Station f6142iH;
    @C0064Am(aul = "5fdcfc591899779b5098648b5f65ac70", aum = 0)

    /* renamed from: nz */
    private static BotScript f6144nz;

    static {
        m28362V();
    }

    public MissionABCTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissionABCTemplate(C5540aNg ang) {
        super(ang);
    }

    public MissionABCTemplate(BotScript ati, Station bf) {
        super((C5540aNg) null);
        super._m_script_init(ati, bf);
    }

    /* renamed from: V */
    static void m28362V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MissionTemplate._m_fieldCount + 2;
        _m_methodCount = MissionTemplate._m_methodCount + 2;
        int i = MissionTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(MissionABCTemplate.class, "5fdcfc591899779b5098648b5f65ac70", i);
        f6145xD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissionABCTemplate.class, "ab5abe8e8f52630e7412b455824e4227", i2);
        f6143kX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MissionTemplate._m_fields, (Object[]) _m_fields);
        int i4 = MissionTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 2)];
        C2491fm a = C4105zY.m41624a(MissionABCTemplate.class, "d123e574ebf528dbba9957deb9b1740a", i4);
        f6146xE = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(MissionABCTemplate.class, "214e54a7b81aae111380cddf6063a92b", i5);
        f6147xF = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MissionTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissionABCTemplate.class, C6392amA.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m28363a(Station bf) {
        bFf().mo5608dq().mo3197f(f6143kX, bf);
    }

    /* renamed from: a */
    private void m28364a(BotScript ati) {
        bFf().mo5608dq().mo3197f(f6145xD, ati);
    }

    /* renamed from: eF */
    private Station m28365eF() {
        return (Station) bFf().mo5608dq().mo3214p(f6143kX);
    }

    /* renamed from: iO */
    private BotScript m28366iO() {
        return (BotScript) bFf().mo5608dq().mo3214p(f6145xD);
    }

    @C0064Am(aul = "214e54a7b81aae111380cddf6063a92b", aum = 0)
    /* renamed from: iR */
    private Mission m28368iR() {
        return mo17615iQ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6392amA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - MissionTemplate._m_methodCount) {
            case 0:
                return m28367iP();
            case 1:
                return m28368iR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: iQ */
    public MissionABC mo17615iQ() {
        switch (bFf().mo6893i(f6146xE)) {
            case 0:
                return null;
            case 2:
                return (MissionABC) bFf().mo5606d(new aCE(this, f6146xE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6146xE, new Object[0]));
                break;
        }
        return m28367iP();
    }

    /* renamed from: iS */
    public /* bridge */ /* synthetic */ Mission mo1716iS() {
        switch (bFf().mo6893i(f6147xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f6147xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6147xF, new Object[0]));
                break;
        }
        return m28368iR();
    }

    /* renamed from: a */
    public void mo17614a(BotScript ati, Station bf) {
        super.mo10S();
        m28364a(ati);
        m28363a(bf);
    }

    @C0064Am(aul = "d123e574ebf528dbba9957deb9b1740a", aum = 0)
    /* renamed from: iP */
    private MissionABC m28367iP() {
        MissionABC zjVar = (MissionABC) bFf().mo6865M(MissionABC.class);
        zjVar.mo23377b(m28366iO());
        zjVar.mo23376b(m28365eF(), m28366iO().ala());
        return zjVar;
    }
}
