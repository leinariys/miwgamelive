package game.script.aggro;

import game.geometry.Vec3d;
import game.network.message.C0474GZ;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import logic.baa.*;
import logic.data.mbean.C0586IH;
import logic.data.mbean.aPA;
import logic.data.mbean.ayN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.dq */
/* compiled from: a */
public class AggroTable extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zf */
    public static final C5663aRz f6632zf = null;
    /* renamed from: zh */
    public static final C5663aRz f6634zh = null;
    /* renamed from: zj */
    public static final C5663aRz f6636zj = null;
    /* renamed from: zl */
    public static final C5663aRz f6638zl = null;
    /* renamed from: zn */
    public static final C5663aRz f6640zn = null;
    /* renamed from: zp */
    public static final C2491fm f6641zp = null;
    /* renamed from: zq */
    public static final C2491fm f6642zq = null;
    /* renamed from: zr */
    public static final C2491fm f6643zr = null;
    /* renamed from: zs */
    public static final C2491fm f6644zs = null;
    /* renamed from: zt */
    public static final C2491fm f6645zt = null;
    /* renamed from: zu */
    public static final C2491fm f6646zu = null;
    /* renamed from: zv */
    public static final C2491fm f6647zv = null;
    /* renamed from: zw */
    public static final C2491fm f6648zw = null;
    /* renamed from: zx */
    public static final C2491fm f6649zx = null;
    static final Comparator<C2308a> comparator = new C4061yk();
    /* renamed from: zd */
    private static final long f6630zd = 600000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "cb0150bc58838f1007e8b4877bc62799", aum = 0)

    /* renamed from: ze */
    private static Pawn f6631ze;
    @C0064Am(aul = "4d1bb4cc062e486f235ec211e3f362af", aum = 1)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: zg */
    private static C3438ra<C2308a> f6633zg;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "2f420e4fee487d81777543c07b4259d6", aum = 2)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: zi */
    private static C1556Wo<Pawn, C2308a> f6635zi;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "e198f567edf5448c7dc4c7b62e3bb8d1", aum = 3)

    /* renamed from: zk */
    private static AggroTableCleanup f6637zk;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "5d4bf8bfb02a52a8149d6336306e0f40", aum = 4)

    /* renamed from: zm */
    private static AggroTableValidator f6639zm;

    static {
        m29308V();
    }

    /* renamed from: zo */
    private transient ArrayList<C2308a> f6650zo;

    public AggroTable() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AggroTable(C5540aNg ang) {
        super(ang);
    }

    public AggroTable(Pawn avi) {
        super((C5540aNg) null);
        super._m_script_init(avi);
    }

    /* renamed from: V */
    static void m29308V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(AggroTable.class, "cb0150bc58838f1007e8b4877bc62799", i);
        f6632zf = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AggroTable.class, "4d1bb4cc062e486f235ec211e3f362af", i2);
        f6634zh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AggroTable.class, "2f420e4fee487d81777543c07b4259d6", i3);
        f6636zj = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AggroTable.class, "e198f567edf5448c7dc4c7b62e3bb8d1", i4);
        f6638zl = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AggroTable.class, "5d4bf8bfb02a52a8149d6336306e0f40", i5);
        f6640zn = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(AggroTable.class, "a7ab7b0c599469e8891e68e42f10bea4", i7);
        f6641zp = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(AggroTable.class, "aaf07cc2c7f1d0094a06431ebe34b86d", i8);
        f6642zq = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(AggroTable.class, "7a5a65bdaa761e3a095b7d7f820165d7", i9);
        f6643zr = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(AggroTable.class, "b858d1bda02608b89adc751605919293", i10);
        f6644zs = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(AggroTable.class, "0d2613072b947280e4b16711e85c57a7", i11);
        f6645zt = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(AggroTable.class, "041bcb946fc1a1473c3c647f1a13c977", i12);
        f6646zu = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(AggroTable.class, "02eb1819a40a117ec588c98e50b4a2d7", i13);
        f6647zv = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(AggroTable.class, "73b1ca4d5dd733911533cc301d566269", i14);
        f6648zw = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(AggroTable.class, "1320f4cb0c24e01019efe931d5f05a56", i15);
        f6649zx = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(AggroTable.class, "52bbb79e851c02643b4b3e1e6b87516b", i16);
        _f_dispose_0020_0028_0029V = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AggroTable.class, C0586IH.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "7a5a65bdaa761e3a095b7d7f820165d7", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m29310a(Actor cr, float f) {
        throw new aWi(new aCE(this, f6643zr, new Object[]{cr, new Float(f)}));
    }

    /* renamed from: a */
    private void m29311a(Pawn avi) {
        bFf().mo5608dq().mo3197f(f6632zf, avi);
    }

    /* renamed from: a */
    private void m29313a(AggroTableCleanup bVar) {
        bFf().mo5608dq().mo3197f(f6638zl, bVar);
    }

    /* renamed from: a */
    private void m29314a(AggroTableValidator cVar) {
        bFf().mo5608dq().mo3197f(f6640zn, cVar);
    }

    /* renamed from: b */
    private void m29316b(Pawn avi, float f) {
        switch (bFf().mo6893i(f6644zs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6644zs, new Object[]{avi, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6644zs, new Object[]{avi, new Float(f)}));
                break;
        }
        m29312a(avi, f);
    }

    /* renamed from: f */
    private void m29317f(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f6636zj, wo);
    }

    @C0064Am(aul = "a7ab7b0c599469e8891e68e42f10bea4", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m29319h(Actor cr) {
        throw new aWi(new aCE(this, f6641zp, new Object[]{cr}));
    }

    @C0064Am(aul = "aaf07cc2c7f1d0094a06431ebe34b86d", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m29320j(Actor cr) {
        throw new aWi(new aCE(this, f6642zq, new Object[]{cr}));
    }

    /* renamed from: jO */
    private Pawn m29321jO() {
        return (Pawn) bFf().mo5608dq().mo3214p(f6632zf);
    }

    /* renamed from: jP */
    private C3438ra m29322jP() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f6634zh);
    }

    /* renamed from: jQ */
    private C1556Wo m29323jQ() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f6636zj);
    }

    /* renamed from: jR */
    private AggroTableCleanup m29324jR() {
        return (AggroTableCleanup) bFf().mo5608dq().mo3214p(f6638zl);
    }

    /* renamed from: jS */
    private AggroTableValidator m29325jS() {
        return (AggroTableValidator) bFf().mo5608dq().mo3214p(f6640zn);
    }

    /* renamed from: jU */
    private void m29327jU() {
        switch (bFf().mo6893i(f6645zt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6645zt, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6645zt, new Object[0]));
                break;
        }
        m29326jT();
    }

    /* access modifiers changed from: private */
    /* renamed from: jX */
    public void m29330jX() {
        switch (bFf().mo6893i(f6649zx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6649zx, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6649zx, new Object[0]));
                break;
        }
        m29329jW();
    }

    /* renamed from: p */
    private void m29331p(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f6634zh, raVar);
    }

    /* renamed from: K */
    public Pawn mo17921K(float f) {
        switch (bFf().mo6893i(f6647zv)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, f6647zv, new Object[]{new Float(f)}));
            case 3:
                bFf().mo5606d(new aCE(this, f6647zv, new Object[]{new Float(f)}));
                break;
        }
        return m29307J(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0586IH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m29319h((Actor) args[0]);
                return null;
            case 1:
                m29320j((Actor) args[0]);
                return null;
            case 2:
                m29310a((Actor) args[0], ((Float) args[1]).floatValue());
                return null;
            case 3:
                m29312a((Pawn) args[0], ((Float) args[1]).floatValue());
                return null;
            case 4:
                m29326jT();
                return null;
            case 5:
                m29328jV();
                return null;
            case 6:
                return m29307J(((Float) args[0]).floatValue());
            case 7:
                return m29309a(((Float) args[0]).floatValue(), (Vec3d) args[1]);
            case 8:
                m29329jW();
                return null;
            case 9:
                m29318fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public Pawn mo17922b(float f, Vec3d ajr) {
        switch (bFf().mo6893i(f6648zw)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, f6648zw, new Object[]{new Float(f), ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, f6648zw, new Object[]{new Float(f), ajr}));
                break;
        }
        return m29309a(f, ajr);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo17923b(Actor cr, float f) {
        switch (bFf().mo6893i(f6643zr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6643zr, new Object[]{cr, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6643zr, new Object[]{cr, new Float(f)}));
                break;
        }
        m29310a(cr, f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cleanup() {
        switch (bFf().mo6893i(f6646zu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6646zu, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6646zu, new Object[0]));
                break;
        }
        m29328jV();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m29318fg();
    }

    @C5566aOg
    /* renamed from: i */
    public void mo17926i(Actor cr) {
        switch (bFf().mo6893i(f6641zp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6641zp, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6641zp, new Object[]{cr}));
                break;
        }
        m29319h(cr);
    }

    @C5566aOg
    /* renamed from: k */
    public void mo17927k(Actor cr) {
        switch (bFf().mo6893i(f6642zq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6642zq, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6642zq, new Object[]{cr}));
                break;
        }
        m29320j(cr);
    }

    /* renamed from: b */
    public void mo17924b(Pawn avi) {
        super.mo10S();
        m29311a(avi);
        AggroTableCleanup bVar = (AggroTableCleanup) bFf().mo6865M(AggroTableCleanup.class);
        bVar.mo17931a(this, (aDJ) this);
        m29313a(bVar);
        AggroTableValidator cVar = (AggroTableValidator) bFf().mo6865M(AggroTableValidator.class);
        cVar.mo17932a(this, (aDJ) this);
        m29314a(cVar);
    }

    @C0064Am(aul = "b858d1bda02608b89adc751605919293", aum = 0)
    /* renamed from: a */
    private void m29312a(Pawn avi, float f) {
        if (!isDisposed()) {
            C2308a aVar = (C2308a) m29323jQ().get(avi);
            if (aVar == null) {
                aVar = new C2308a();
                aVar.bDI = avi;
                m29323jQ().put(avi, aVar);
            }
            aVar.bDJ += f;
            aVar.bDK = currentTimeMillis();
            m29327jU();
        }
    }

    @C0064Am(aul = "0d2613072b947280e4b16711e85c57a7", aum = 0)
    /* renamed from: jT */
    private void m29326jT() {
        if (m29325jS() == null) {
            AggroTableValidator cVar = (AggroTableValidator) bFf().mo6865M(AggroTableValidator.class);
            cVar.mo17932a(this, (aDJ) this);
            m29314a(cVar);
        }
        if (m29324jR() == null) {
            AggroTableCleanup bVar = (AggroTableCleanup) bFf().mo6865M(AggroTableCleanup.class);
            bVar.mo17931a(this, (aDJ) this);
            m29313a(bVar);
        }
        if (!m29325jS().isActive()) {
            m29325jS().mo4702d(2.0f, 1);
        }
        if (!m29324jR().isActive()) {
            m29324jR().mo4704hk(120.0f);
        }
    }

    @C0064Am(aul = "041bcb946fc1a1473c3c647f1a13c977", aum = 0)
    /* renamed from: jV */
    private void m29328jV() {
        Iterator it = new ArrayList(m29323jQ().entrySet()).iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            if (currentTimeMillis() - ((C2308a) entry.getValue()).bDK > f6630zd) {
                m29323jQ().remove(entry.getKey());
            }
        }
        if (m29323jQ().isEmpty()) {
            m29324jR().stop();
            this.f6650zo = null;
            m29322jP().clear();
        }
    }

    @C0064Am(aul = "02eb1819a40a117ec588c98e50b4a2d7", aum = 0)
    /* renamed from: J */
    private Pawn m29307J(float f) {
        if (m29322jP().isEmpty()) {
            return null;
        }
        float f2 = f * f;
        for (C2308a aVar : m29322jP()) {
            Pawn avi = aVar.bDI;
            if (avi != null && m29321jO().mo1013bx((Actor) avi) < f2 && !avi.isDisposed() && avi.bae() && avi.isAlive()) {
                return avi;
            }
        }
        return null;
    }

    @C0064Am(aul = "73b1ca4d5dd733911533cc301d566269", aum = 0)
    /* renamed from: a */
    private Pawn m29309a(float f, Vec3d ajr) {
        if (m29322jP().isEmpty()) {
            return null;
        }
        float f2 = f * f;
        Pawn avi = null;
        for (C2308a aVar : m29322jP()) {
            if (aVar.bDI != null && ((!bGX() || !C1298TD.m9830t(aVar.bDI).bFT()) && !aVar.bDI.isDisposed() && aVar.bDI.bae() && !aVar.bDI.isDead())) {
                float aC = aVar.bDI.getPosition().mo9486aC(ajr);
                if (aC < f2) {
                    avi = aVar.bDI;
                    f2 = aC;
                }
            }
        }
        return avi;
    }

    @C0064Am(aul = "1320f4cb0c24e01019efe931d5f05a56", aum = 0)
    /* renamed from: jW */
    private void m29329jW() {
        ArrayList<C2308a> arrayList = this.f6650zo;
        if (arrayList == null) {
            arrayList = new ArrayList<>();
            this.f6650zo = arrayList;
        }
        arrayList.clear();
        arrayList.addAll(m29323jQ().values());
        Collections.sort(arrayList, comparator);
        if (!arrayList.equals(m29322jP())) {
            m29322jP().clear();
            m29322jP().addAll(arrayList);
        }
    }

    @C0064Am(aul = "52bbb79e851c02643b4b3e1e6b87516b", aum = 0)
    /* renamed from: fg */
    private void m29318fg() {
        m29311a((Pawn) null);
        if (m29325jS() != null) {
            m29325jS().dispose();
        }
        if (m29324jR() != null) {
            m29324jR().dispose();
        }
        if (m29322jP() != null) {
            m29322jP().clear();
        }
        if (m29323jQ() != null) {
            m29323jQ().clear();
        }
        if (this.f6650zo != null) {
            this.f6650zo.clear();
        }
        super.dispose();
    }

    /* renamed from: a.dq$a */
    public static class C2308a implements Externalizable {
        private static final long serialVersionUID = 2898183244563484796L;
        Pawn bDI;
        float bDJ;
        long bDK;

        public void readExternal(ObjectInput objectInput) {
            this.bDI = (Pawn) C0474GZ.dah.inputReadObject(objectInput);
            this.bDJ = objectInput.readFloat();
            this.bDK = objectInput.readLong();
        }

        public void writeExternal(ObjectOutput objectOutput) {
            C0474GZ.dah.serializeData(objectOutput, (Object) this.bDI);
            objectOutput.writeFloat(this.bDJ);
            objectOutput.writeLong(this.bDK);
        }

        public String toString() {
            return "AggroEntry[enemy=" + this.bDI + ",damage=" + this.bDJ + "]";
        }
    }

    @C6485anp
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C5511aMd
            /* renamed from: a.dq$b */
            /* compiled from: a */
    class AggroTableCleanup extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f6651JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6652aT = null;

        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "6cb04383d284e366bdb781902c204de9", aum = 0)
        static /* synthetic */ AggroTable bDZ = null;

        static {
            m29345V();
        }

        public AggroTableCleanup() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public AggroTableCleanup(C5540aNg ang) {
            super(ang);
        }

        public AggroTableCleanup(AggroTable dqVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(dqVar, adj);
        }

        /* renamed from: V */
        static void m29345V() {
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(AggroTableCleanup.class, "6cb04383d284e366bdb781902c204de9", i);
            f6652aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(AggroTableCleanup.class, "c3cce0053162a01d83e0fd326877dce6", i3);
            f6651JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(AggroTableCleanup.class, ayN.class, _m_fields, _m_methods);
        }

        private AggroTable amD() {
            return (AggroTable) bFf().mo5608dq().mo3214p(f6652aT);
        }

        /* renamed from: b */
        private void m29346b(AggroTable dqVar) {
            bFf().mo5608dq().mo3197f(f6652aT, dqVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new ayN(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m29347pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f6651JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f6651JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f6651JD, new Object[0]));
                    break;
            }
            m29347pi();
        }

        /* renamed from: a */
        public void mo17931a(AggroTable dqVar, aDJ adj) {
            m29346b(dqVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "c3cce0053162a01d83e0fd326877dce6", aum = 0)
        /* renamed from: pi */
        private void m29347pi() {
            amD().cleanup();
        }
    }

    @C6485anp
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C5511aMd
            /* renamed from: a.dq$c */
            /* compiled from: a */
    class AggroTableValidator extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f6653JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6654aT = null;

        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "b9c15132626ee9a51be19227ecdf3a97", aum = 0)
        static /* synthetic */ AggroTable bDZ = null;

        static {
            m29357V();
        }

        public AggroTableValidator() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public AggroTableValidator(C5540aNg ang) {
            super(ang);
        }

        public AggroTableValidator(AggroTable dqVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(dqVar, adj);
        }

        /* renamed from: V */
        static void m29357V() {
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(AggroTableValidator.class, "b9c15132626ee9a51be19227ecdf3a97", i);
            f6654aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(AggroTableValidator.class, "a994fd9532d5fdffc86253f039b12d10", i3);
            f6653JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(AggroTableValidator.class, aPA.class, _m_fields, _m_methods);
        }

        private AggroTable amD() {
            return (AggroTable) bFf().mo5608dq().mo3214p(f6654aT);
        }

        /* renamed from: b */
        private void m29358b(AggroTable dqVar) {
            bFf().mo5608dq().mo3197f(f6654aT, dqVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new aPA(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m29359pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f6653JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f6653JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f6653JD, new Object[0]));
                    break;
            }
            m29359pi();
        }

        /* renamed from: a */
        public void mo17932a(AggroTable dqVar, aDJ adj) {
            m29358b(dqVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "a994fd9532d5fdffc86253f039b12d10", aum = 0)
        /* renamed from: pi */
        private void m29359pi() {
            amD().m29330jX();
        }
    }
}
