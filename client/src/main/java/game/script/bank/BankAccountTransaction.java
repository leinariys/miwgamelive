package game.script.bank;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3975xc;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ani  reason: case insensitive filesystem */
/* compiled from: a */
public class BankAccountTransaction extends TaikodomObject implements C1616Xf {

    /* renamed from: QA */
    public static final C5663aRz f4984QA = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJO = null;
    public static final C2491fm aJP = null;
    /* renamed from: cz */
    public static final C5663aRz f4985cz = null;
    public static final C5663aRz gcN = null;
    public static final C2491fm gcO = null;
    public static final C2491fm gcP = null;
    public static final C2491fm gcQ = null;
    public static final C2491fm gcR = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C3248pc
    @C0064Am(aul = "26aa8ca0e67f3ec42e38ffebdba4aa29", aum = 1)
    private static float amount;
    @C3248pc
    @C0064Am(aul = "5a312b0a304d0a63c9094babc58e78a4", aum = 2)
    private static Player bFB;
    @C3248pc
    @C0064Am(aul = "bcdf1195da687546fd4e4cc8f8d2ec49", aum = 0)
    private static String description;

    static {
        m24264V();
    }

    public BankAccountTransaction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BankAccountTransaction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24264V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(BankAccountTransaction.class, "bcdf1195da687546fd4e4cc8f8d2ec49", i);
        f4984QA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BankAccountTransaction.class, "26aa8ca0e67f3ec42e38ffebdba4aa29", i2);
        f4985cz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BankAccountTransaction.class, "5a312b0a304d0a63c9094babc58e78a4", i3);
        gcN = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(BankAccountTransaction.class, "30e26be31bfb68a7e5b2e5726b2a4b62", i5);
        gcO = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(BankAccountTransaction.class, "83f8d07ea9d51414f5e543e05a631266", i6);
        gcP = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(BankAccountTransaction.class, "98577e20555ea95d404659f9db1be945", i7);
        aJP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(BankAccountTransaction.class, "53247d4307babb682b6b27af300cb181", i8);
        aJO = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(BankAccountTransaction.class, "9bc211c27d239a80e5631564cf0842a4", i9);
        gcQ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(BankAccountTransaction.class, "bc6eb2a16bd3bee4ed5b9387dbdae8df", i10);
        gcR = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BankAccountTransaction.class, C3975xc.class, _m_fields, _m_methods);
    }

    /* renamed from: Qj */
    private String m24263Qj() {
        return (String) bFf().mo5608dq().mo3214p(f4984QA);
    }

    /* renamed from: aM */
    private void m24265aM(String str) {
        bFf().mo5608dq().mo3197f(f4984QA, str);
    }

    /* renamed from: bC */
    private void m24267bC(Player aku) {
        bFf().mo5608dq().mo3197f(gcN, aku);
    }

    private float ckG() {
        return bFf().mo5608dq().mo3211m(f4985cz);
    }

    private Player ckH() {
        return (Player) bFf().mo5608dq().mo3214p(gcN);
    }

    /* renamed from: jS */
    private void m24269jS(float f) {
        bFf().mo5608dq().mo3150a(f4985cz, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3975xc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Float(ckI());
            case 1:
                m24270jT(((Float) args[0]).floatValue());
                return null;
            case 2:
                return m24262QE();
            case 3:
                m24266aQ((String) args[0]);
                return null;
            case 4:
                return ckJ();
            case 5:
                m24268bD((Player) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: bE */
    public void mo15022bE(Player aku) {
        switch (bFf().mo6893i(gcR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gcR, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gcR, new Object[]{aku}));
                break;
        }
        m24268bD(aku);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Player ckK() {
        switch (bFf().mo6893i(gcQ)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, gcQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gcQ, new Object[0]));
                break;
        }
        return ckJ();
    }

    public float getAmount() {
        switch (bFf().mo6893i(gcO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gcO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gcO, new Object[0]));
                break;
        }
        return ckI();
    }

    public void setAmount(float f) {
        switch (bFf().mo6893i(gcP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gcP, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gcP, new Object[]{new Float(f)}));
                break;
        }
        m24270jT(f);
    }

    public String getDescription() {
        switch (bFf().mo6893i(aJP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aJP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJP, new Object[0]));
                break;
        }
        return m24262QE();
    }

    public void setDescription(String str) {
        switch (bFf().mo6893i(aJO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJO, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJO, new Object[]{str}));
                break;
        }
        m24266aQ(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "30e26be31bfb68a7e5b2e5726b2a4b62", aum = 0)
    private float ckI() {
        return ckG();
    }

    @C0064Am(aul = "83f8d07ea9d51414f5e543e05a631266", aum = 0)
    /* renamed from: jT */
    private void m24270jT(float f) {
        m24269jS(f);
    }

    @C0064Am(aul = "98577e20555ea95d404659f9db1be945", aum = 0)
    /* renamed from: QE */
    private String m24262QE() {
        return m24263Qj();
    }

    @C0064Am(aul = "53247d4307babb682b6b27af300cb181", aum = 0)
    /* renamed from: aQ */
    private void m24266aQ(String str) {
        m24265aM(str);
    }

    @C0064Am(aul = "9bc211c27d239a80e5631564cf0842a4", aum = 0)
    private Player ckJ() {
        return ckH();
    }

    @C0064Am(aul = "bc6eb2a16bd3bee4ed5b9387dbdae8df", aum = 0)
    /* renamed from: bD */
    private void m24268bD(Player aku) {
        m24267bC(aku);
    }
}
