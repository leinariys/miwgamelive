package game.script.bank;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2389ej;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.adO  reason: case insensitive filesystem */
/* compiled from: a */
public class BankAccount extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz flo = null;
    public static final C5663aRz flp = null;
    public static final C2491fm flq = null;
    public static final C2491fm flr = null;
    public static final C2491fm fls = null;
    public static final C2491fm flt = null;
    public static final C2491fm flu = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9a69e6e3e8ad0a07bbddb590c3bfab91", aum = 0)

    /* renamed from: BF */
    private static long f4316BF;
    @C0064Am(aul = "e33bc6daa3057d078fa60cefc2760bb9", aum = 1)

    /* renamed from: BG */
    private static C3438ra<BankStatementEntry> f4317BG;

    static {
        m20821V();
    }

    public BankAccount() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BankAccount(long j) {
        super((C5540aNg) null);
        super._m_script_init(j);
    }

    public BankAccount(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20821V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BankAccount.class, "9a69e6e3e8ad0a07bbddb590c3bfab91", i);
        flo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BankAccount.class, "e33bc6daa3057d078fa60cefc2760bb9", i2);
        flp = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(BankAccount.class, "1d7d1b3d5ba1d3cf36ee012f18bd4db1", i4);
        flq = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BankAccount.class, "be6f54b4f74d6e1e13fb279a3e5332cf", i5);
        flr = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BankAccount.class, "cd8d5279287f14d73eba5a2ae264761a", i6);
        fls = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BankAccount.class, "3fd110bfa00ff9c751c6ce3472521622", i7);
        flt = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BankAccount.class, "6c5c5cdc7ad0a6d3a7c73e34a96e8e1a", i8);
        _f_dispose_0020_0028_0029V = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BankAccount.class, "f623b16754817665527f5ccc531f9dbd", i9);
        flu = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BankAccount.class, C2389ej.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "3fd110bfa00ff9c751c6ce3472521622", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m20822a(BankStatementEntry arp) {
        throw new aWi(new aCE(this, flt, new Object[]{arp}));
    }

    /* renamed from: bK */
    private void m20823bK(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(flp, raVar);
    }

    private long bSp() {
        return bFf().mo5608dq().mo3213o(flo);
    }

    private C3438ra bSq() {
        return (C3438ra) bFf().mo5608dq().mo3214p(flp);
    }

    /* renamed from: gQ */
    private void m20825gQ(long j) {
        bFf().mo5608dq().mo3184b(flo, j);
    }

    @C0064Am(aul = "be6f54b4f74d6e1e13fb279a3e5332cf", aum = 0)
    @C5566aOg
    /* renamed from: gS */
    private void m20826gS(long j) {
        throw new aWi(new aCE(this, flr, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "f623b16754817665527f5ccc531f9dbd", aum = 0)
    @C5566aOg
    /* renamed from: gU */
    private void m20827gU(long j) {
        throw new aWi(new aCE(this, flu, new Object[]{new Long(j)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2389ej(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Long(bSr());
            case 1:
                m20826gS(((Long) args[0]).longValue());
                return null;
            case 2:
                return bSt();
            case 3:
                m20822a((BankStatementEntry) args[0]);
                return null;
            case 4:
                m20824fg();
                return null;
            case 5:
                m20827gU(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo12831b(BankStatementEntry arp) {
        switch (bFf().mo6893i(flt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flt, new Object[]{arp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flt, new Object[]{arp}));
                break;
        }
        m20822a(arp);
    }

    public long bSs() {
        switch (bFf().mo6893i(flq)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, flq, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, flq, new Object[0]));
                break;
        }
        return bSr();
    }

    public C3438ra<BankStatementEntry> bSu() {
        switch (bFf().mo6893i(fls)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fls, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fls, new Object[0]));
                break;
        }
        return bSt();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m20824fg();
    }

    @C5566aOg
    /* renamed from: gT */
    public void mo12835gT(long j) {
        switch (bFf().mo6893i(flr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flr, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flr, new Object[]{new Long(j)}));
                break;
        }
        m20826gS(j);
    }

    @C5566aOg
    /* renamed from: gV */
    public void mo12836gV(long j) {
        switch (bFf().mo6893i(flu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flu, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flu, new Object[]{new Long(j)}));
                break;
        }
        m20827gU(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        if (C5877acF.fel) {
            mo12835gT(9999999);
        } else {
            mo12835gT(ala().aIS().bQc());
        }
    }

    /* renamed from: gR */
    public void mo12834gR(long j) {
        super.mo10S();
        if (C5877acF.fel) {
            mo12835gT(9999999);
        } else {
            mo12835gT(j);
        }
    }

    @C0064Am(aul = "1d7d1b3d5ba1d3cf36ee012f18bd4db1", aum = 0)
    private long bSr() {
        return bSp();
    }

    @C0064Am(aul = "cd8d5279287f14d73eba5a2ae264761a", aum = 0)
    private C3438ra<BankStatementEntry> bSt() {
        return bSq();
    }

    @C0064Am(aul = "6c5c5cdc7ad0a6d3a7c73e34a96e8e1a", aum = 0)
    /* renamed from: fg */
    private void m20824fg() {
        for (BankStatementEntry dispose : bSq()) {
            dispose.dispose();
        }
        bSq().clear();
        super.dispose();
    }
}
