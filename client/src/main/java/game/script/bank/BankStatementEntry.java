package game.script.bank;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6722asS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C5878acG;
import logic.sql.C6401amJ;
import p001a.*;

import java.util.Collection;
import java.util.Date;

@C5511aMd
@C6485anp
/* renamed from: a.arP  reason: case insensitive filesystem */
/* compiled from: a */
public class BankStatementEntry extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aRA = null;
    /* renamed from: cz */
    public static final C5663aRz f5229cz = null;
    public static final C5663aRz gtA = null;
    public static final C5663aRz gtC = null;
    public static final C5663aRz gtE = null;
    public static final C2491fm gtF = null;
    public static final C2491fm gtG = null;
    public static final C2491fm gtH = null;
    public static final C2491fm gtI = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "cf42d542f6468d867882fae70a39c46e", aum = 0)
    private static long axi;
    @C0064Am(aul = "53687070015d56b1b614ec3eda5dbec4", aum = 2)
    private static boolean gtB;
    @C0064Am(aul = "3acc348214af3ae6b768afbe66a0f519", aum = 3)
    private static Date gtD;
    @C0064Am(aul = "b97768f872b882d9d27d14c5d8aa0acf", aum = 1)
    private static Bank.C1861a gtz;

    static {
        m25429V();
    }

    public BankStatementEntry() {
        super((C5540aNg) null);
        super.mo10S();
    }

    private BankStatementEntry(long j, Bank.C1861a aVar, boolean z, Date date) {
        super((C5540aNg) null);
        super._m_script_init(j, aVar, z, date);
    }

    public BankStatementEntry(long j, Bank.C1861a aVar, boolean z, Date date, String str, int i, C1165RF rf) {
        super((C5540aNg) null);
        super._m_script_init(j, aVar, z, date, str, i, rf);
    }

    public BankStatementEntry(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25429V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 5;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(BankStatementEntry.class, "cf42d542f6468d867882fae70a39c46e", i);
        f5229cz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BankStatementEntry.class, "b97768f872b882d9d27d14c5d8aa0acf", i2);
        gtA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BankStatementEntry.class, "53687070015d56b1b614ec3eda5dbec4", i3);
        gtC = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(BankStatementEntry.class, "3acc348214af3ae6b768afbe66a0f519", i4);
        gtE = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 5)];
        C2491fm a = C4105zY.m41624a(BankStatementEntry.class, "7036b5837320cfe85dbcbcfab3e5dab9", i6);
        gtF = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(BankStatementEntry.class, "f5da02aefd82d852ba57bf16e03a8f26", i7);
        gtG = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(BankStatementEntry.class, "44cd573b5948df7f208296d467b6061a", i8);
        gtH = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(BankStatementEntry.class, "bc9b578ad480533ae7636f51aef724b7", i9);
        gtI = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(BankStatementEntry.class, "40094bb8acf09631e87c6609a03953d0", i10);
        aRA = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BankStatementEntry.class, C6722asS.class, _m_fields, _m_methods);
    }

    /* renamed from: LS */
    private long m25427LS() {
        return bFf().mo5608dq().mo3213o(f5229cz);
    }

    /* renamed from: a */
    private void m25431a(Bank.C1861a aVar) {
        bFf().mo5608dq().mo3197f(gtA, aVar);
    }

    /* renamed from: b */
    private void m25432b(Date date) {
        bFf().mo5608dq().mo3197f(gtE, date);
    }

    /* renamed from: cj */
    private void m25433cj(long j) {
        bFf().mo5608dq().mo3184b(f5229cz, j);
    }

    private Bank.C1861a csu() {
        return (Bank.C1861a) bFf().mo5608dq().mo3214p(gtA);
    }

    private boolean csv() {
        return bFf().mo5608dq().mo3201h(gtC);
    }

    private Date csw() {
        return (Date) bFf().mo5608dq().mo3214p(gtE);
    }

    /* renamed from: gg */
    private void m25434gg(boolean z) {
        bFf().mo5608dq().mo3153a(gtC, z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: Sh */
    public long mo15736Sh() {
        switch (bFf().mo6893i(gtF)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gtF, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gtF, new Object[0]));
                break;
        }
        return csx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @ClientOnly
    /* renamed from: UY */
    public String mo15737UY() {
        switch (bFf().mo6893i(aRA)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aRA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aRA, new Object[0]));
                break;
        }
        return m25428UX();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6722asS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Long(csx());
            case 1:
                return new Boolean(csy());
            case 2:
                return csA();
            case 3:
                return csC();
            case 4:
                return m25428UX();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Date csB() {
        switch (bFf().mo6893i(gtH)) {
            case 0:
                return null;
            case 2:
                return (Date) bFf().mo5606d(new aCE(this, gtH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gtH, new Object[0]));
                break;
        }
        return csA();
    }

    public Bank.C1861a csD() {
        switch (bFf().mo6893i(gtI)) {
            case 0:
                return null;
            case 2:
                return (Bank.C1861a) bFf().mo5606d(new aCE(this, gtI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gtI, new Object[0]));
                break;
        }
        return csC();
    }

    public boolean csz() {
        switch (bFf().mo6893i(gtG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gtG, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gtG, new Object[0]));
                break;
        }
        return csy();
    }

    /* renamed from: a */
    public void mo15738a(long j, Bank.C1861a aVar, boolean z, Date date, String str, int i, C1165RF rf) {
        m25430a(j, aVar, z, date);
        if (rf instanceof Player) {
            mo2066a((C5878acG) new C6401amJ((Player) rf, aVar != null ? aVar.toString() : "<<bank transfer type undefined>>", j, str != null ? str : "<<undefined>>", i));
        }
    }

    /* renamed from: a */
    private void m25430a(long j, Bank.C1861a aVar, boolean z, Date date) {
        super.mo10S();
        m25433cj(j);
        m25431a(aVar);
        m25434gg(z);
        m25432b(date);
    }

    @C0064Am(aul = "7036b5837320cfe85dbcbcfab3e5dab9", aum = 0)
    private long csx() {
        return m25427LS();
    }

    @C0064Am(aul = "f5da02aefd82d852ba57bf16e03a8f26", aum = 0)
    private boolean csy() {
        return csv();
    }

    @C0064Am(aul = "44cd573b5948df7f208296d467b6061a", aum = 0)
    private Date csA() {
        return csw();
    }

    @C0064Am(aul = "bc9b578ad480533ae7636f51aef724b7", aum = 0)
    private Bank.C1861a csC() {
        return csu();
    }

    @C0064Am(aul = "40094bb8acf09631e87c6609a03953d0", aum = 0)
    @ClientOnly
    /* renamed from: UX */
    private String m25428UX() {
        return "";
    }
}
