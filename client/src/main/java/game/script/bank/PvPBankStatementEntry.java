package game.script.bank;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aFE;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Date;

@C5511aMd
@C6485anp
/* renamed from: a.pl */
/* compiled from: a */
public class PvPBankStatementEntry extends BankStatementEntry implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aRA = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m37287V();
    }

    public PvPBankStatementEntry() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PvPBankStatementEntry(long j, Bank.C1861a aVar, boolean z, Date date, String str, int i, C1165RF rf) {
        super((C5540aNg) null);
        super.mo15738a(j, aVar, z, date, str, i, rf);
    }

    public PvPBankStatementEntry(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37287V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BankStatementEntry._m_fieldCount + 0;
        _m_methodCount = BankStatementEntry._m_methodCount + 1;
        _m_fields = new C5663aRz[(BankStatementEntry._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BankStatementEntry._m_fields, (Object[]) _m_fields);
        int i = BankStatementEntry._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(PvPBankStatementEntry.class, "210a7e91d449e4cae177fed6fa14e31c", i);
        aRA = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BankStatementEntry._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PvPBankStatementEntry.class, aFE.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: UY */
    public String mo15737UY() {
        switch (bFf().mo6893i(aRA)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aRA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aRA, new Object[0]));
                break;
        }
        return m37286UX();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aFE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - BankStatementEntry._m_methodCount) {
            case 0:
                return m37286UX();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo15738a(long j, Bank.C1861a aVar, boolean z, Date date, String str, int i, C1165RF rf) {
        super.mo15738a(j, aVar, z, date, str, i, rf);
    }

    @C0064Am(aul = "210a7e91d449e4cae177fed6fa14e31c", aum = 0)
    /* renamed from: UX */
    private String m37286UX() {
        return super.mo15737UY();
    }
}
