package game.script.bank;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.aAA;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

@C5829abJ("1.10.1")
@C6485anp
@C5511aMd
/* renamed from: a.acR  reason: case insensitive filesystem */
/* compiled from: a */
public class Bank extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4219bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4220bM = null;
    /* renamed from: bN */
    public static final C2491fm f4221bN = null;
    /* renamed from: bO */
    public static final C2491fm f4222bO = null;
    /* renamed from: bP */
    public static final C2491fm f4223bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4224bQ = null;
    public static final C5663aRz ffh = null;
    public static final C5663aRz ffj = null;
    public static final C2491fm ffk = null;
    public static final C2491fm ffl = null;
    public static final C2491fm ffm = null;
    public static final C2491fm ffn = null;
    public static final C2491fm ffo = null;
    public static final C2491fm ffp = null;
    public static final C2491fm ffq = null;
    public static final C2491fm ffr = null;
    public static final C2491fm ffs = null;
    public static final C2491fm fft = null;
    public static final C2491fm ffu = null;
    public static final C2491fm ffv = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f41a7330b4d33e199ff47a3e419558da", aum = 2)

    /* renamed from: bK */
    private static UUID f4218bK;
    @C0064Am(aul = "8bb4f85388ef2561be0572ec97b44198", aum = 0)
    private static long ffg;
    @C0064Am(aul = "cd98bbc9c12e72d038ad8fe9234a3031", aum = 1)
    private static long ffi;
    @C0064Am(aul = "8cad1902f426ad0c072f0b62e4491a40", aum = 3)
    private static String handle;

    static {
        m20333V();
    }

    public Bank() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Bank(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20333V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 17;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(Bank.class, "8bb4f85388ef2561be0572ec97b44198", i);
        ffh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Bank.class, "cd98bbc9c12e72d038ad8fe9234a3031", i2);
        ffj = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Bank.class, "f41a7330b4d33e199ff47a3e419558da", i3);
        f4219bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Bank.class, "8cad1902f426ad0c072f0b62e4491a40", i4);
        f4220bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 17)];
        C2491fm a = C4105zY.m41624a(Bank.class, "74e63dd4213b106ba898e972431f64fe", i6);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(Bank.class, "5d29dda7af428de71a21ddf7a03bfab8", i7);
        f4221bN = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(Bank.class, "d234e0157279f4a1f9a41b88a3ecbbf2", i8);
        f4222bO = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(Bank.class, "7a6ddf700ad8be1bba37db2c03891e28", i9);
        f4223bP = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(Bank.class, "5e465d02a1db5e83742f2c8be4bf5815", i10);
        f4224bQ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(Bank.class, "1fe45651d2f30867e4ef801edfb0d81a", i11);
        ffk = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(Bank.class, "bee8501d226fa61a58866b34b1ec30f3", i12);
        ffl = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(Bank.class, "3a5918e528425cbf0b45bcb38a96757d", i13);
        ffm = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(Bank.class, "42660fe1c21e0f22e2140da7b5b144d7", i14);
        ffn = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(Bank.class, "7bfa715cfcab5c805621d009aaf865d4", i15);
        ffo = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(Bank.class, "0a3aa5d359441d51b962f599649cc1fa", i16);
        ffp = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(Bank.class, "4d8aac35812f41c856efa83bd8c64e7a", i17);
        ffq = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(Bank.class, "afac92baa464de53f28ee36558248657", i18);
        ffr = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(Bank.class, "7b7f51231aa50aa555061b8d6ed53e30", i19);
        ffs = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(Bank.class, "f20341bbd72b85b2005f0d27960dbfe8", i20);
        fft = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(Bank.class, "907c415f60fdb7edf0a644e08f8b8483", i21);
        ffu = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(Bank.class, "c2a19569e51f53efcb7ef251221b4fdb", i22);
        ffv = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Bank.class, aAA.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "7bfa715cfcab5c805621d009aaf865d4", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m20334a(C1165RF rf, long j, C1861a aVar, String str, int i) {
        throw new aWi(new aCE(this, ffo, new Object[]{rf, new Long(j), aVar, str, new Integer(i)}));
    }

    @C0064Am(aul = "0a3aa5d359441d51b962f599649cc1fa", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m20335a(C1165RF rf, long j, C1861a aVar, Date date, String str, int i) {
        throw new aWi(new aCE(this, ffp, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i)}));
    }

    @C0064Am(aul = "c2a19569e51f53efcb7ef251221b4fdb", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m20336a(Player aku, long j) {
        throw new aWi(new aCE(this, ffv, new Object[]{aku, new Long(j)}));
    }

    /* renamed from: a */
    private void m20337a(String str) {
        bFf().mo5608dq().mo3197f(f4220bM, str);
    }

    /* renamed from: a */
    private void m20338a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4219bL, uuid);
    }

    @C0064Am(aul = "907c415f60fdb7edf0a644e08f8b8483", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m20339a(long j, C1165RF rf, C1165RF rf2, C1861a aVar, String str, int i) {
        throw new aWi(new aCE(this, ffu, new Object[]{new Long(j), rf, rf2, aVar, str, new Integer(i)}));
    }

    @C0064Am(aul = "4d8aac35812f41c856efa83bd8c64e7a", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m20340a(C1165RF rf, long j, C1861a aVar, String str, int i, boolean z) {
        throw new aWi(new aCE(this, ffq, new Object[]{rf, new Long(j), aVar, str, new Integer(i), new Boolean(z)}));
    }

    @C0064Am(aul = "f20341bbd72b85b2005f0d27960dbfe8", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m20341a(C1165RF rf, long j, C1861a aVar, Date date, String str, int i, boolean z) {
        throw new aWi(new aCE(this, fft, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i), new Boolean(z)}));
    }

    /* renamed from: an */
    private UUID m20342an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4219bL);
    }

    /* renamed from: ao */
    private String m20343ao() {
        return (String) bFf().mo5608dq().mo3214p(f4220bM);
    }

    @C5566aOg
    /* renamed from: b */
    private void m20347b(C1165RF rf, long j, C1861a aVar, Date date, String str, int i) {
        switch (bFf().mo6893i(ffp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffp, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffp, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i)}));
                break;
        }
        m20335a(rf, j, aVar, date, str, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "5e465d02a1db5e83742f2c8be4bf5815", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m20348b(String str) {
        throw new aWi(new aCE(this, f4224bQ, new Object[]{str}));
    }

    @C5566aOg
    /* renamed from: b */
    private boolean m20350b(C1165RF rf, long j, C1861a aVar, Date date, String str, int i, boolean z) {
        switch (bFf().mo6893i(fft)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fft, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i), new Boolean(z)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fft, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i), new Boolean(z)}));
                break;
        }
        return m20341a(rf, j, aVar, date, str, i, z);
    }

    private long bPZ() {
        return bFf().mo5608dq().mo3213o(ffh);
    }

    private long bQa() {
        return bFf().mo5608dq().mo3213o(ffj);
    }

    /* renamed from: c */
    private void m20351c(UUID uuid) {
        switch (bFf().mo6893i(f4222bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4222bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4222bO, new Object[]{uuid}));
                break;
        }
        m20349b(uuid);
    }

    @C0064Am(aul = "afac92baa464de53f28ee36558248657", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private boolean m20352c(C1165RF rf, long j, C1861a aVar, String str, int i) {
        throw new aWi(new aCE(this, ffr, new Object[]{rf, new Long(j), aVar, str, new Integer(i)}));
    }

    @C0064Am(aul = "7b7f51231aa50aa555061b8d6ed53e30", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private boolean m20353c(C1165RF rf, long j, C1861a aVar, Date date, String str, int i) {
        throw new aWi(new aCE(this, ffs, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i)}));
    }

    @C5566aOg
    /* renamed from: d */
    private boolean m20354d(C1165RF rf, long j, C1861a aVar, Date date, String str, int i) {
        switch (bFf().mo6893i(ffs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ffs, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ffs, new Object[]{rf, new Long(j), aVar, date, str, new Integer(i)}));
                break;
        }
        return m20353c(rf, j, aVar, date, str, i);
    }

    /* renamed from: gF */
    private void m20355gF(long j) {
        bFf().mo5608dq().mo3184b(ffh, j);
    }

    /* renamed from: gG */
    private void m20356gG(long j) {
        bFf().mo5608dq().mo3184b(ffj, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Player Balance")
    @C0064Am(aul = "1fe45651d2f30867e4ef801edfb0d81a", aum = 0)
    @C5566aOg
    /* renamed from: gH */
    private void m20357gH(long j) {
        throw new aWi(new aCE(this, ffk, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Corporation Creation Fee")
    @C0064Am(aul = "3a5918e528425cbf0b45bcb38a96757d", aum = 0)
    @C5566aOg
    /* renamed from: gJ */
    private void m20358gJ(long j) {
        throw new aWi(new aCE(this, ffm, new Object[]{new Long(j)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aAA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m20346au();
            case 1:
                return m20344ap();
            case 2:
                m20349b((UUID) args[0]);
                return null;
            case 3:
                return m20345ar();
            case 4:
                m20348b((String) args[0]);
                return null;
            case 5:
                m20357gH(((Long) args[0]).longValue());
                return null;
            case 6:
                return new Long(bQb());
            case 7:
                m20358gJ(((Long) args[0]).longValue());
                return null;
            case 8:
                return new Long(bQd());
            case 9:
                m20334a((C1165RF) args[0], ((Long) args[1]).longValue(), (C1861a) args[2], (String) args[3], ((Integer) args[4]).intValue());
                return null;
            case 10:
                m20335a((C1165RF) args[0], ((Long) args[1]).longValue(), (C1861a) args[2], (Date) args[3], (String) args[4], ((Integer) args[5]).intValue());
                return null;
            case 11:
                return new Boolean(m20340a((C1165RF) args[0], ((Long) args[1]).longValue(), (C1861a) args[2], (String) args[3], ((Integer) args[4]).intValue(), ((Boolean) args[5]).booleanValue()));
            case 12:
                return new Boolean(m20352c((C1165RF) args[0], ((Long) args[1]).longValue(), (C1861a) args[2], (String) args[3], ((Integer) args[4]).intValue()));
            case 13:
                return new Boolean(m20353c((C1165RF) args[0], ((Long) args[1]).longValue(), (C1861a) args[2], (Date) args[3], (String) args[4], ((Integer) args[5]).intValue()));
            case 14:
                return new Boolean(m20341a((C1165RF) args[0], ((Long) args[1]).longValue(), (C1861a) args[2], (Date) args[3], (String) args[4], ((Integer) args[5]).intValue(), ((Boolean) args[6]).booleanValue()));
            case 15:
                return new Boolean(m20339a(((Long) args[0]).longValue(), (C1165RF) args[1], (C1165RF) args[2], (C1861a) args[3], (String) args[4], ((Integer) args[5]).intValue()));
            case 16:
                m20336a((Player) args[0], ((Long) args[1]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4221bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4221bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4221bN, new Object[0]));
                break;
        }
        return m20344ap();
    }

    @C5566aOg
    /* renamed from: b */
    public void mo12602b(C1165RF rf, long j, C1861a aVar, String str, int i) {
        switch (bFf().mo6893i(ffo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffo, new Object[]{rf, new Long(j), aVar, str, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffo, new Object[]{rf, new Long(j), aVar, str, new Integer(i)}));
                break;
        }
        m20334a(rf, j, aVar, str, i);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo12603b(Player aku, long j) {
        switch (bFf().mo6893i(ffv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffv, new Object[]{aku, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffv, new Object[]{aku, new Long(j)}));
                break;
        }
        m20336a(aku, j);
    }

    @C5566aOg
    /* renamed from: b */
    public boolean mo12604b(long j, C1165RF rf, C1165RF rf2, C1861a aVar, String str, int i) {
        switch (bFf().mo6893i(ffu)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ffu, new Object[]{new Long(j), rf, rf2, aVar, str, new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ffu, new Object[]{new Long(j), rf, rf2, aVar, str, new Integer(i)}));
                break;
        }
        return m20339a(j, rf, rf2, aVar, str, i);
    }

    @C5566aOg
    /* renamed from: b */
    public boolean mo12605b(C1165RF rf, long j, C1861a aVar, String str, int i, boolean z) {
        switch (bFf().mo6893i(ffq)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ffq, new Object[]{rf, new Long(j), aVar, str, new Integer(i), new Boolean(z)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ffq, new Object[]{rf, new Long(j), aVar, str, new Integer(i), new Boolean(z)}));
                break;
        }
        return m20340a(rf, j, aVar, str, i, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Player Balance")
    public long bQc() {
        switch (bFf().mo6893i(ffl)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ffl, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ffl, new Object[0]));
                break;
        }
        return bQb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corporation Creation Fee")
    public long bQe() {
        switch (bFf().mo6893i(ffn)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ffn, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ffn, new Object[0]));
                break;
        }
        return bQd();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public boolean mo12608d(C1165RF rf, long j, C1861a aVar, String str, int i) {
        switch (bFf().mo6893i(ffr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ffr, new Object[]{rf, new Long(j), aVar, str, new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ffr, new Object[]{rf, new Long(j), aVar, str, new Integer(i)}));
                break;
        }
        return m20352c(rf, j, aVar, str, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Player Balance")
    @C5566aOg
    /* renamed from: gI */
    public void mo12609gI(long j) {
        switch (bFf().mo6893i(ffk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffk, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffk, new Object[]{new Long(j)}));
                break;
        }
        m20357gH(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Corporation Creation Fee")
    @C5566aOg
    /* renamed from: gK */
    public void mo12610gK(long j) {
        switch (bFf().mo6893i(ffm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ffm, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ffm, new Object[]{new Long(j)}));
                break;
        }
        m20358gJ(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4223bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4223bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4223bP, new Object[0]));
                break;
        }
        return m20345ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4224bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4224bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4224bQ, new Object[]{str}));
                break;
        }
        m20348b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m20346au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "74e63dd4213b106ba898e972431f64fe", aum = 0)
    /* renamed from: au */
    private String m20346au() {
        return "[Taikodom Bank]";
    }

    @C0064Am(aul = "5d29dda7af428de71a21ddf7a03bfab8", aum = 0)
    /* renamed from: ap */
    private UUID m20344ap() {
        return m20342an();
    }

    @C0064Am(aul = "d234e0157279f4a1f9a41b88a3ecbbf2", aum = 0)
    /* renamed from: b */
    private void m20349b(UUID uuid) {
        m20338a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "7a6ddf700ad8be1bba37db2c03891e28", aum = 0)
    /* renamed from: ar */
    private String m20345ar() {
        return m20343ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Player Balance")
    @C0064Am(aul = "bee8501d226fa61a58866b34b1ec30f3", aum = 0)
    private long bQb() {
        return bPZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corporation Creation Fee")
    @C0064Am(aul = "42660fe1c21e0f22e2140da7b5b144d7", aum = 0)
    private long bQd() {
        return bQa();
    }

    /* renamed from: a.acR$a */
    public enum C1861a {
        MARKET_BUY,
        MARKET_SELL,
        TRADE,
        MISSION,
        CRAFTING,
        DEATH_PENALITY,
        CREEP_KILL_REWARD,
        CREEP_KILL_FINE,
        MARKET_BUY_ORDER_CREATE,
        MARKET_BUY_ORDER_SUPPLIED,
        MARKET_BUY_ORDER_RETURN,
        MARKET_SELL_ORDER_SUPPLIED,
        CORPORATION_CREATION,
        CORPORATION_TRANSFER,
        CORPORATION_DRAW,
        AUCTION_FEE,
        AUCTION_BUY,
        AUCTION_SELL,
        CONSIGNMENT_FEE,
        CONSIGNMENT_BUY,
        CONSIGNMENT_SELL,
        CLONING,
        CONTRACT_CREATION_FEE,
        INSURANCE_FEE_COSTS,
        CONTRACT_REWARD_DEPOSIT,
        CONTRACT_REWARD_WITHDRAW,
        CONTRACT_REWARD_RETURN,
        CONTRACT_SAFEGUARD_DEPOSIT,
        CONTRACT_SAFEGUARD_WITHDRAW,
        CONTRACT_SAFEGUARD_RETURN,
        OUTPOST_UPGRADE,
        OUTPOST_UPKEEP,
        ONI_OVERLOAD,
        ONI_SLOT_EDIT,
        CITIZENSHIP_WAGE,
        HOPLONS_ACQUIRING_CITIZENSHIP,
        HOPLONS_CONVERSION_DEBIT,
        HOPLONS_CONVERSION_CREDIT,
        HOPLONS_CREDIT
    }
}
