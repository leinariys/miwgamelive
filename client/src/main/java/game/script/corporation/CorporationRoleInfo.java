package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0603IX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aWt */
/* compiled from: a */
public class CorporationRoleInfo extends TaikodomObject implements C1616Xf {

    /* renamed from: NY */
    public static final C5663aRz f4025NY = null;

    /* renamed from: Ob */
    public static final C2491fm f4026Ob = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gGA = null;
    public static final C2491fm gGB = null;
    public static final C5663aRz gGy = null;
    public static final C5663aRz jgl = null;
    public static final C5663aRz jgm = null;
    public static final C2491fm jgn = null;
    public static final C2491fm jgo = null;
    public static final C2491fm jgp = null;
    public static final C2491fm jgq = null;
    public static final C2491fm jgr = null;
    public static final C2491fm jgs = null;
    public static final C2491fm jgt = null;
    public static final C2491fm jgu = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e2d98fbe7521c97c307de5a9deff27d6", aum = 0)
    private static C5741aUz dhu;
    @C0064Am(aul = "a1e735011615b30fb4e7f2c4cc3cdb52", aum = 1)
    private static C1556Wo<C6704asA, Boolean> dhv;
    @C0064Am(aul = "983aa28baf9096e9ce47d1adc40d2a1b", aum = 2)
    private static String dhw;
    @C0064Am(aul = "956ac6394e685fb0736db48fb9715515", aum = 3)
    private static String dhx;
    @C0064Am(aul = "f52271b4369675afc494b8f8379f88d0", aum = 4)

    /* renamed from: jn */
    private static Asset f4027jn;

    static {
        m19305V();
    }

    public CorporationRoleInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationRoleInfo(C5540aNg ang) {
        super(ang);
    }

    public CorporationRoleInfo(C5741aUz auz) {
        super((C5540aNg) null);
        super._m_script_init(auz);
    }

    /* renamed from: V */
    static void m19305V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(CorporationRoleInfo.class, "e2d98fbe7521c97c307de5a9deff27d6", i);
        gGy = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CorporationRoleInfo.class, "a1e735011615b30fb4e7f2c4cc3cdb52", i2);
        gGA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CorporationRoleInfo.class, "983aa28baf9096e9ce47d1adc40d2a1b", i3);
        jgl = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CorporationRoleInfo.class, "956ac6394e685fb0736db48fb9715515", i4);
        jgm = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CorporationRoleInfo.class, "f52271b4369675afc494b8f8379f88d0", i5);
        f4025NY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(CorporationRoleInfo.class, "a5d31dd4e5cb0cec008d828b54e85f11", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationRoleInfo.class, "211232f0d0305eb45d0c31470a1915bb", i8);
        _f_onResurrect_0020_0028_0029V = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationRoleInfo.class, "d24b0396d413a6c46edcfe5cf70ef394", i9);
        jgn = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationRoleInfo.class, "808631abdfa212589951735fde71fe74", i10);
        jgo = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(CorporationRoleInfo.class, "0f5f8c7c25467e06bdd65c5b093ee095", i11);
        f4026Ob = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(CorporationRoleInfo.class, "d1dc34ae2d7cbaad27ba5e6051889ff6", i12);
        gGB = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(CorporationRoleInfo.class, "d62b41d0e21bb7b93ab23f571f20a89f", i13);
        jgp = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(CorporationRoleInfo.class, "90ecffccd5d1fa364a3ea97bab115050", i14);
        jgq = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(CorporationRoleInfo.class, "7a00432b4456f2892d1781c2f89a7e88", i15);
        jgr = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(CorporationRoleInfo.class, "30870af5aab1bfc998b394fa3713f506", i16);
        jgs = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(CorporationRoleInfo.class, "0331617222ac8b01bfaa7db256fba9f4", i17);
        jgt = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(CorporationRoleInfo.class, "2a4c01c83e7e3757d3d493dfe4ed8679", i18);
        jgu = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationRoleInfo.class, C0603IX.class, _m_fields, _m_methods);
    }

    /* renamed from: Z */
    private void m19306Z(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(gGA, wo);
    }

    @C0064Am(aul = "7a00432b4456f2892d1781c2f89a7e88", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m19307a(C6704asA asa, boolean z) {
        throw new aWi(new aCE(this, jgr, new Object[]{asa, new Boolean(z)}));
    }

    private C5741aUz cyQ() {
        return (C5741aUz) bFf().mo5608dq().mo3214p(gGy);
    }

    private C1556Wo cyS() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(gGA);
    }

    private String dDW() {
        return (String) bFf().mo5608dq().mo3214p(jgl);
    }

    private String dDX() {
        return (String) bFf().mo5608dq().mo3214p(jgm);
    }

    @C0064Am(aul = "d24b0396d413a6c46edcfe5cf70ef394", aum = 0)
    @C5566aOg
    @C2499fr
    private void dDY() {
        throw new aWi(new aCE(this, jgn, new Object[0]));
    }

    @C0064Am(aul = "808631abdfa212589951735fde71fe74", aum = 0)
    @C5566aOg
    @C2499fr
    private void dEa() {
        throw new aWi(new aCE(this, jgo, new Object[0]));
    }

    /* renamed from: e */
    private void m19310e(C5741aUz auz) {
        bFf().mo5608dq().mo3197f(gGy, auz);
    }

    /* renamed from: nT */
    private void m19312nT(String str) {
        bFf().mo5608dq().mo3197f(jgl, str);
    }

    /* renamed from: nU */
    private void m19313nU(String str) {
        bFf().mo5608dq().mo3197f(jgm, str);
    }

    @C0064Am(aul = "d62b41d0e21bb7b93ab23f571f20a89f", aum = 0)
    @C5566aOg
    /* renamed from: nV */
    private void m19314nV(String str) {
        throw new aWi(new aCE(this, jgp, new Object[]{str}));
    }

    @C0064Am(aul = "90ecffccd5d1fa364a3ea97bab115050", aum = 0)
    @C5566aOg
    /* renamed from: nW */
    private void m19315nW(String str) {
        throw new aWi(new aCE(this, jgq, new Object[]{str}));
    }

    /* renamed from: sG */
    private Asset m19316sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f4025NY);
    }

    /* renamed from: t */
    private void m19318t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f4025NY, tCVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0603IX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m19309au();
            case 1:
                m19308aG();
                return null;
            case 2:
                dDY();
                return null;
            case 3:
                dEa();
                return null;
            case 4:
                return m19317sJ();
            case 5:
                return cyT();
            case 6:
                m19314nV((String) args[0]);
                return null;
            case 7:
                m19315nW((String) args[0]);
                return null;
            case 8:
                m19307a((C6704asA) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 9:
                return dEc();
            case 10:
                return dEd();
            case 11:
                return new Boolean(m19311e((C6704asA) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m19308aG();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: b */
    public void mo12089b(C6704asA asa, boolean z) {
        switch (bFf().mo6893i(jgr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jgr, new Object[]{asa, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jgr, new Object[]{asa, new Boolean(z)}));
                break;
        }
        m19307a(asa, z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C5741aUz cyU() {
        switch (bFf().mo6893i(gGB)) {
            case 0:
                return null;
            case 2:
                return (C5741aUz) bFf().mo5606d(new aCE(this, gGB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gGB, new Object[0]));
                break;
        }
        return cyT();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    @C2499fr
    public void dDZ() {
        switch (bFf().mo6893i(jgn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jgn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jgn, new Object[0]));
                break;
        }
        dDY();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    @C2499fr
    public void dEb() {
        switch (bFf().mo6893i(jgo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jgo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jgo, new Object[0]));
                break;
        }
        dEa();
    }

    @ClientOnly
    public String dEe() {
        switch (bFf().mo6893i(jgt)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, jgt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jgt, new Object[0]));
                break;
        }
        return dEd();
    }

    /* renamed from: f */
    public boolean mo12095f(C6704asA asa) {
        switch (bFf().mo6893i(jgu)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, jgu, new Object[]{asa}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, jgu, new Object[]{asa}));
                break;
        }
        return m19311e(asa);
    }

    @ClientOnly
    public String getRoleName() {
        switch (bFf().mo6893i(jgs)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, jgs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jgs, new Object[0]));
                break;
        }
        return dEc();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void setRoleName(String str) {
        switch (bFf().mo6893i(jgp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jgp, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jgp, new Object[]{str}));
                break;
        }
        m19314nV(str);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: nX */
    public void mo12097nX(String str) {
        switch (bFf().mo6893i(jgq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jgq, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jgq, new Object[]{str}));
                break;
        }
        m19315nW(str);
    }

    /* renamed from: sK */
    public Asset mo12098sK() {
        switch (bFf().mo6893i(f4026Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f4026Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4026Ob, new Object[0]));
                break;
        }
        return m19317sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m19309au();
    }

    @C0064Am(aul = "a5d31dd4e5cb0cec008d828b54e85f11", aum = 0)
    /* renamed from: au */
    private String m19309au() {
        return "Role [" + cyQ() + "], Custom name [" + dDW() + "]";
    }

    /* renamed from: f */
    public void mo12094f(C5741aUz auz) {
        super.mo10S();
        m19310e(auz);
        dDZ();
        dEb();
    }

    @C0064Am(aul = "211232f0d0305eb45d0c31470a1915bb", aum = 0)
    /* renamed from: aG */
    private void m19308aG() {
        for (C6704asA asa : C6704asA.values()) {
            if (!cyS().containsKey(asa)) {
                if (cyQ() == C5741aUz.dAr()) {
                    cyS().put(asa, Boolean.TRUE);
                } else {
                    cyS().put(asa, Boolean.FALSE);
                }
            } else if (cyQ() == C5741aUz.dAr() && !((Boolean) cyS().get(asa)).booleanValue()) {
                mo8358lY("CorporationRole had false permission for CEO level for permission '" + asa + "'. Fixing...");
                cyS().put(asa, Boolean.TRUE);
            }
        }
        super.mo70aH();
    }

    @C0064Am(aul = "0f5f8c7c25467e06bdd65c5b093ee095", aum = 0)
    /* renamed from: sJ */
    private Asset m19317sJ() {
        if (m19316sG() != null) {
            return m19316sG();
        }
        return ala().aJe().mo19009xb().mo26d(cyQ()).mo16626sK();
    }

    @C0064Am(aul = "d1dc34ae2d7cbaad27ba5e6051889ff6", aum = 0)
    private C5741aUz cyT() {
        return cyQ();
    }

    @C0064Am(aul = "30870af5aab1bfc998b394fa3713f506", aum = 0)
    @ClientOnly
    private String dEc() {
        if (dDW() != null) {
            return dDW();
        }
        return ala().aJe().mo19009xb().mo26d(cyQ()).mo16624ke().get();
    }

    @C0064Am(aul = "0331617222ac8b01bfaa7db256fba9f4", aum = 0)
    @ClientOnly
    private String dEd() {
        if (dDX() != null) {
            return dDX();
        }
        return ala().aJe().mo19009xb().mo26d(cyQ()).cyW().get();
    }

    @C0064Am(aul = "2a4c01c83e7e3757d3d493dfe4ed8679", aum = 0)
    /* renamed from: e */
    private boolean m19311e(C6704asA asa) {
        return ((Boolean) cyS().get(asa)).booleanValue();
    }
}
