package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C1272So;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aq */
/* compiled from: a */
public class CorporationInvitationStatus extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz axd = null;
    public static final C2491fm axe = null;
    public static final C2491fm axf = null;
    public static final C2491fm axg = null;
    public static final C2491fm axh = null;
    /* renamed from: hF */
    public static final C2491fm f5141hF = null;
    /* renamed from: hz */
    public static final C5663aRz f5142hz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a00ae8508d7629490cf1c4c181b6d689", aum = 0)

    /* renamed from: P */
    private static Player f5140P;
    @C0064Am(aul = "28742ec0b3454bbb9877fa404d843b95", aum = 1)
    private static C1962a axc;

    static {
        m25046V();
    }

    public CorporationInvitationStatus() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationInvitationStatus(C5540aNg ang) {
        super(ang);
    }

    public CorporationInvitationStatus(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m25046V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 5;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(CorporationInvitationStatus.class, "a00ae8508d7629490cf1c4c181b6d689", i);
        f5142hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CorporationInvitationStatus.class, "28742ec0b3454bbb9877fa404d843b95", i2);
        axd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(CorporationInvitationStatus.class, "100b5b52fdf26c0e0b4b548e2616c6f2", i4);
        f5141hF = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationInvitationStatus.class, "ea875126b90d9e3aa239d420acddbff1", i5);
        axe = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationInvitationStatus.class, "2f71709befacab9189236d17bbb2fe96", i6);
        axf = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationInvitationStatus.class, "919bb08b43ec8521a233edc05c1d8ba0", i7);
        axg = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(CorporationInvitationStatus.class, "3d236af52f5938a577ad7d5ee64d5a6c", i8);
        axh = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationInvitationStatus.class, C1272So.class, _m_fields, _m_methods);
    }

    /* renamed from: LK */
    private C1962a m25041LK() {
        return (C1962a) bFf().mo5608dq().mo3214p(axd);
    }

    /* renamed from: a */
    private void m25047a(Player aku) {
        bFf().mo5608dq().mo3197f(f5142hz, aku);
    }

    /* renamed from: a */
    private void m25048a(C1962a aVar) {
        bFf().mo5608dq().mo3197f(axd, aVar);
    }

    /* renamed from: dG */
    private Player m25049dG() {
        return (Player) bFf().mo5608dq().mo3214p(f5142hz);
    }

    /* renamed from: LM */
    public C1962a mo15534LM() {
        switch (bFf().mo6893i(axe)) {
            case 0:
                return null;
            case 2:
                return (C1962a) bFf().mo5606d(new aCE(this, axe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, axe, new Object[0]));
                break;
        }
        return m25042LL();
    }

    /* renamed from: LO */
    public void mo15535LO() {
        switch (bFf().mo6893i(axf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, axf, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, axf, new Object[0]));
                break;
        }
        m25043LN();
    }

    /* renamed from: LR */
    public void mo15536LR() {
        switch (bFf().mo6893i(axh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, axh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, axh, new Object[0]));
                break;
        }
        m25045LQ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1272So(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m25050dK();
            case 1:
                return m25042LL();
            case 2:
                m25043LN();
                return null;
            case 3:
                m25044LP();
                return null;
            case 4:
                m25045LQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void accept() {
        switch (bFf().mo6893i(axg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, axg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, axg, new Object[0]));
                break;
        }
        m25044LP();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: dL */
    public Player mo15539dL() {
        switch (bFf().mo6893i(f5141hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f5141hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5141hF, new Object[0]));
                break;
        }
        return m25050dK();
    }

    /* renamed from: b */
    public void mo15538b(Player aku) {
        super.mo10S();
        m25047a(aku);
        m25048a(C1962a.UNSENT);
    }

    @C0064Am(aul = "100b5b52fdf26c0e0b4b548e2616c6f2", aum = 0)
    /* renamed from: dK */
    private Player m25050dK() {
        return m25049dG();
    }

    @C0064Am(aul = "ea875126b90d9e3aa239d420acddbff1", aum = 0)
    /* renamed from: LL */
    private C1962a m25042LL() {
        return m25041LK();
    }

    @C0064Am(aul = "2f71709befacab9189236d17bbb2fe96", aum = 0)
    /* renamed from: LN */
    private void m25043LN() {
        m25048a(C1962a.REFUSED);
    }

    @C0064Am(aul = "919bb08b43ec8521a233edc05c1d8ba0", aum = 0)
    /* renamed from: LP */
    private void m25044LP() {
        m25048a(C1962a.ACCEPTED);
    }

    @C0064Am(aul = "3d236af52f5938a577ad7d5ee64d5a6c", aum = 0)
    /* renamed from: LQ */
    private void m25045LQ() {
        m25048a(C1962a.WAITING);
    }

    /* renamed from: a.aq$a */
    public enum C1962a {
        UNSENT,
        WAITING,
        ACCEPTED,
        REFUSED
    }
}
