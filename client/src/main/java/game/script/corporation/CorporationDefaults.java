package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C6653arB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.AB */
/* compiled from: a */
public class CorporationDefaults extends aDJ implements C0468GU, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f2Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4bL = null;
    /* renamed from: bN */
    public static final C2491fm f5bN = null;
    /* renamed from: bO */
    public static final C2491fm f6bO = null;
    /* renamed from: bP */
    public static final C2491fm f7bP = null;
    public static final C5663aRz chU = null;
    public static final C5663aRz chW = null;
    public static final C5663aRz chY = null;
    public static final C5663aRz cia = null;
    public static final C2491fm cib = null;
    public static final C2491fm cic = null;
    public static final C2491fm cid = null;
    public static final C2491fm cie = null;
    public static final C2491fm cif = null;
    public static final C2491fm cig = null;
    public static final C2491fm cih = null;
    public static final C2491fm cii = null;
    public static final C2491fm cij = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "243c3d518bfc72a4ca1908ce55f31fed", aum = 4)

    /* renamed from: bK */
    private static UUID f3bK;
    @C0064Am(aul = "d95b92d54e7d30d786a6700e379e2f79", aum = 0)
    private static C3438ra<Asset> chT;
    @C0064Am(aul = "029920bf9e6649221499fd6767c187a5", aum = 1)
    private static C1556Wo<C5741aUz, CorporationRoleDefaults> chV;
    @C0064Am(aul = "a067fa3de8b3945d06c333cee9fc56dc", aum = 2)
    private static C1556Wo<C6704asA, CorporationPermissionDefaults> chX;
    @C0064Am(aul = "bc14787892595c4c52bfe578f4d9f228", aum = 3)
    private static int chZ;

    static {
        m8V();
    }

    public CorporationDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 13;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(CorporationDefaults.class, "d95b92d54e7d30d786a6700e379e2f79", i);
        chU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CorporationDefaults.class, "029920bf9e6649221499fd6767c187a5", i2);
        chW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CorporationDefaults.class, "a067fa3de8b3945d06c333cee9fc56dc", i3);
        chY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CorporationDefaults.class, "bc14787892595c4c52bfe578f4d9f228", i4);
        cia = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CorporationDefaults.class, "243c3d518bfc72a4ca1908ce55f31fed", i5);
        f4bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 13)];
        C2491fm a = C4105zY.m41624a(CorporationDefaults.class, "eb017362843025ab1d51c67be76e654f", i7);
        f5bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationDefaults.class, "64e48107e74ef598a9414d89f43aae30", i8);
        f6bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationDefaults.class, "a6a582f8a8c5ae3ba094187f8c15ae5d", i9);
        f7bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationDefaults.class, "5396e86a3f596369d4c5f6039be30b5a", i10);
        cib = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(CorporationDefaults.class, "82f0dcd4dd6fb3ae54fc85ef1c6f464b", i11);
        cic = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(CorporationDefaults.class, "c31d671302ba34c0cdcd75a44acbab5c", i12);
        cid = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(CorporationDefaults.class, "6f859acea913ccd9223dcb09ada1ac76", i13);
        cie = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(CorporationDefaults.class, "b65c180167440e2e90e4c916c73a4331", i14);
        f2Do = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(CorporationDefaults.class, "ae85f8abe5292d32a617f58673ce1baa", i15);
        cif = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(CorporationDefaults.class, "7d9ea4bb02b38eb6509d47a885dbf5bb", i16);
        cig = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(CorporationDefaults.class, "6feb32b18b9b2ffd9a57f979c0e08e96", i17);
        cih = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(CorporationDefaults.class, "a5fb7eac31754b6da51dd1c29652f7fd", i18);
        cii = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(CorporationDefaults.class, "3836d9991fb1b68d96fce04bf474d748", i19);
        cij = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationDefaults.class, C6653arB.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m11a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4bL, uuid);
    }

    /* renamed from: ac */
    private void m12ac(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(chU, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Corporation Logos")
    @C0064Am(aul = "6feb32b18b9b2ffd9a57f979c0e08e96", aum = 0)
    @C5566aOg
    /* renamed from: ae */
    private void m13ae(Asset tCVar) {
        throw new aWi(new aCE(this, cih, new Object[]{tCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Corporation Logos")
    @C0064Am(aul = "a5fb7eac31754b6da51dd1c29652f7fd", aum = 0)
    @C5566aOg
    /* renamed from: ag */
    private void m14ag(Asset tCVar) {
        throw new aWi(new aCE(this, cii, new Object[]{tCVar}));
    }

    /* renamed from: an */
    private UUID m15an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4bL);
    }

    private C3438ra avW() {
        return (C3438ra) bFf().mo5608dq().mo3214p(chU);
    }

    private C1556Wo avX() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(chW);
    }

    private C1556Wo avY() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(chY);
    }

    private int avZ() {
        return bFf().mo5608dq().mo3212n(cia);
    }

    /* renamed from: c */
    private void m20c(UUID uuid) {
        switch (bFf().mo6893i(f6bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6bO, new Object[]{uuid}));
                break;
        }
        m18b(uuid);
    }

    /* renamed from: fZ */
    private void m21fZ(int i) {
        bFf().mo5608dq().mo3183b(cia, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Base (default) max members number for any Corporation")
    @C0064Am(aul = "7d9ea4bb02b38eb6509d47a885dbf5bb", aum = 0)
    @C5566aOg
    /* renamed from: ga */
    private void m22ga(int i) {
        throw new aWi(new aCE(this, cig, new Object[]{new Integer(i)}));
    }

    /* renamed from: w */
    private void m23w(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(chW, wo);
    }

    /* renamed from: x */
    private void m24x(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(chY, wo);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6653arB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m16ap();
            case 1:
                m18b((UUID) args[0]);
                return null;
            case 2:
                return m17ar();
            case 3:
                return awa();
            case 4:
                return awc();
            case 5:
                return m19c((C5741aUz) args[0]);
            case 6:
                return m9a((C6704asA) args[0]);
            case 7:
                m10a((C0665JT) args[0]);
                return null;
            case 8:
                return new Integer(awe());
            case 9:
                m22ga(((Integer) args[0]).intValue());
                return null;
            case 10:
                m13ae((Asset) args[0]);
                return null;
            case 11:
                m14ag((Asset) args[0]);
                return null;
            case 12:
                return awg();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Corporation Logos")
    @C5566aOg
    /* renamed from: af */
    public void mo16af(Asset tCVar) {
        switch (bFf().mo6893i(cih)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cih, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cih, new Object[]{tCVar}));
                break;
        }
        m13ae(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Corporation Logos")
    @C5566aOg
    /* renamed from: ah */
    public void mo17ah(Asset tCVar) {
        switch (bFf().mo6893i(cii)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cii, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cii, new Object[]{tCVar}));
                break;
        }
        m14ag(tCVar);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5bN, new Object[0]));
                break;
        }
        return m16ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Roles Info")
    public List<CorporationRoleDefaults> awb() {
        switch (bFf().mo6893i(cib)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cib, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cib, new Object[0]));
                break;
        }
        return awa();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Permissions Info")
    public List<CorporationPermissionDefaults> awd() {
        switch (bFf().mo6893i(cic)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cic, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cic, new Object[0]));
                break;
        }
        return awc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Base (default) max members number for any Corporation")
    public int awf() {
        switch (bFf().mo6893i(cif)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cif, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cif, new Object[0]));
                break;
        }
        return awe();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Corporation Logos")
    public List<Asset> awh() {
        switch (bFf().mo6893i(cij)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cij, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cij, new Object[0]));
                break;
        }
        return awg();
    }

    /* renamed from: b */
    public CorporationPermissionDefaults mo23b(C6704asA asa) {
        switch (bFf().mo6893i(cie)) {
            case 0:
                return null;
            case 2:
                return (CorporationPermissionDefaults) bFf().mo5606d(new aCE(this, cie, new Object[]{asa}));
            case 3:
                bFf().mo5606d(new aCE(this, cie, new Object[]{asa}));
                break;
        }
        return m9a(asa);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2Do, new Object[]{jt}));
                break;
        }
        m10a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public CorporationRoleDefaults mo26d(C5741aUz auz) {
        switch (bFf().mo6893i(cid)) {
            case 0:
                return null;
            case 2:
                return (CorporationRoleDefaults) bFf().mo5606d(new aCE(this, cid, new Object[]{auz}));
            case 3:
                bFf().mo5606d(new aCE(this, cid, new Object[]{auz}));
                break;
        }
        return m19c(auz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Base (default) max members number for any Corporation")
    @C5566aOg
    /* renamed from: gb */
    public void mo27gb(int i) {
        switch (bFf().mo6893i(cig)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cig, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cig, new Object[]{new Integer(i)}));
                break;
        }
        m22ga(i);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f7bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7bP, new Object[0]));
                break;
        }
        return m17ar();
    }

    @C0064Am(aul = "eb017362843025ab1d51c67be76e654f", aum = 0)
    /* renamed from: ap */
    private UUID m16ap() {
        return m15an();
    }

    @C0064Am(aul = "64e48107e74ef598a9414d89f43aae30", aum = 0)
    /* renamed from: b */
    private void m18b(UUID uuid) {
        m11a(uuid);
    }

    @C0064Am(aul = "a6a582f8a8c5ae3ba094187f8c15ae5d", aum = 0)
    /* renamed from: ar */
    private String m17ar() {
        return "corporation_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        for (C5741aUz auz : C5741aUz.values()) {
            CorporationRoleDefaults avj = (CorporationRoleDefaults) bFf().mo6865M(CorporationRoleDefaults.class);
            avj.mo16622f(auz);
            avX().put(auz, avj);
        }
        for (C6704asA asa : C6704asA.values()) {
            CorporationPermissionDefaults avv = (CorporationPermissionDefaults) bFf().mo6865M(CorporationPermissionDefaults.class);
            avv.mo16643d(asa);
            avY().put(asa, avv);
        }
        m11a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Roles Info")
    @C0064Am(aul = "5396e86a3f596369d4c5f6039be30b5a", aum = 0)
    private List<CorporationRoleDefaults> awa() {
        return Collections.unmodifiableList(new ArrayList(avX().values()));
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Permissions Info")
    @C0064Am(aul = "82f0dcd4dd6fb3ae54fc85ef1c6f464b", aum = 0)
    private List<CorporationPermissionDefaults> awc() {
        return Collections.unmodifiableList(new ArrayList(avY().values()));
    }

    @C0064Am(aul = "c31d671302ba34c0cdcd75a44acbab5c", aum = 0)
    /* renamed from: c */
    private CorporationRoleDefaults m19c(C5741aUz auz) {
        if (avX().containsKey(auz)) {
            return (CorporationRoleDefaults) avX().get(auz);
        }
        throw new IllegalArgumentException("Unknown corporation role in defaults: " + auz);
    }

    @C0064Am(aul = "6f859acea913ccd9223dcb09ada1ac76", aum = 0)
    /* renamed from: a */
    private CorporationPermissionDefaults m9a(C6704asA asa) {
        if (avY().containsKey(asa)) {
            return (CorporationPermissionDefaults) avY().get(asa);
        }
        throw new IllegalArgumentException("Unknown corporation permission in defaults: " + asa);
    }

    @C0064Am(aul = "b65c180167440e2e90e4c916c73a4331", aum = 0)
    /* renamed from: a */
    private void m10a(C0665JT jt) {
        for (C5741aUz auz : C5741aUz.values()) {
            if (!avX().containsKey(auz)) {
                CorporationRoleDefaults avj = (CorporationRoleDefaults) bFf().mo6865M(CorporationRoleDefaults.class);
                avj.mo16622f(auz);
                avX().put(auz, avj);
            }
        }
        for (C6704asA asa : C6704asA.values()) {
            if (!avY().containsKey(asa)) {
                CorporationPermissionDefaults avv = (CorporationPermissionDefaults) bFf().mo6865M(CorporationPermissionDefaults.class);
                avv.mo16643d(asa);
                avY().put(asa, avv);
            }
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Base (default) max members number for any Corporation")
    @C0064Am(aul = "ae85f8abe5292d32a617f58673ce1baa", aum = 0)
    private int awe() {
        return avZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Corporation Logos")
    @C0064Am(aul = "3836d9991fb1b68d96fce04bf474d748", aum = 0)
    private List<Asset> awg() {
        return Collections.unmodifiableList(avW());
    }
}
