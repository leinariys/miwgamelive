package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.associates.Associate;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0595IP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.ba */
/* compiled from: a */
public class CorporationCreationStatus extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: mB */
    public static final C5663aRz f5820mB = null;
    /* renamed from: mC */
    public static final C2491fm f5821mC = null;
    /* renamed from: mD */
    public static final C2491fm f5822mD = null;
    /* renamed from: mE */
    public static final C2491fm f5823mE = null;
    /* renamed from: mF */
    public static final C2491fm f5824mF = null;
    /* renamed from: mG */
    public static final C2491fm f5825mG = null;
    /* renamed from: mH */
    public static final C2491fm f5826mH = null;
    /* renamed from: mI */
    public static final C2491fm f5827mI = null;
    /* renamed from: mJ */
    public static final C2491fm f5828mJ = null;
    /* renamed from: mK */
    public static final C2491fm f5829mK = null;
    /* renamed from: mL */
    public static final C2491fm f5830mL = null;
    /* renamed from: mv */
    public static final C5663aRz f5832mv = null;
    /* renamed from: mx */
    public static final C5663aRz f5834mx = null;
    /* renamed from: mz */
    public static final C5663aRz f5836mz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "630ba63c616e541b0f61fa3806b3027b", aum = 3)

    /* renamed from: mA */
    private static boolean f5819mA;
    @C0064Am(aul = "7c9e9b52562973eaebce573a32442eb3", aum = 0)

    /* renamed from: mu */
    private static Player f5831mu;
    @C0064Am(aul = "c2ea795dc5e2081602d0e91dba9642cd", aum = 1)

    /* renamed from: mw */
    private static C2686iZ<CorporationInvitationStatus> f5833mw;
    @C0064Am(aul = "ad9909dcf0ee618890da4714ecdceb16", aum = 2)

    /* renamed from: my */
    private static boolean f5835my;

    static {
        m27879V();
    }

    public CorporationCreationStatus() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationCreationStatus(C5540aNg ang) {
        super(ang);
    }

    public CorporationCreationStatus(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m27879V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CorporationCreationStatus.class, "7c9e9b52562973eaebce573a32442eb3", i);
        f5832mv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CorporationCreationStatus.class, "c2ea795dc5e2081602d0e91dba9642cd", i2);
        f5834mx = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CorporationCreationStatus.class, "ad9909dcf0ee618890da4714ecdceb16", i3);
        f5836mz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CorporationCreationStatus.class, "630ba63c616e541b0f61fa3806b3027b", i4);
        f5820mB = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(CorporationCreationStatus.class, "89280aa44fd09c26a25fdbcb1065f896", i6);
        f5821mC = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationCreationStatus.class, "9fc861c9fd9618c493f2a5d55681d72d", i7);
        f5822mD = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationCreationStatus.class, "2799423b6d554139737c5d916e1427fe", i8);
        f5823mE = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationCreationStatus.class, "d8284985275ed1db8da494f011972349", i9);
        f5824mF = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CorporationCreationStatus.class, "0040031e3b9cac79a24b745c2c3e0492", i10);
        f5825mG = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CorporationCreationStatus.class, "e2de3219b5a495afebffd0df079c2444", i11);
        f5826mH = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CorporationCreationStatus.class, "c35a11ba3df5cd939430f2c6bc8c6daa", i12);
        f5827mI = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CorporationCreationStatus.class, "2a8a37cb6e3c8366fee92e1616bce32f", i13);
        f5828mJ = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CorporationCreationStatus.class, "a03ec49360c859447406ee79e6c2ab9d", i14);
        f5829mK = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(CorporationCreationStatus.class, "2cf48fe3d96346047d055417b4ccfd12", i15);
        f5830mL = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationCreationStatus.class, C0595IP.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "0040031e3b9cac79a24b745c2c3e0492", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private Corporation m27880a(String str, Asset tCVar) {
        throw new aWi(new aCE(this, f5825mG, new Object[]{str, tCVar}));
    }

    /* renamed from: c */
    private void m27881c(Player aku) {
        bFf().mo5608dq().mo3197f(f5832mv, aku);
    }

    /* renamed from: e */
    private void m27883e(Player aku) {
        switch (bFf().mo6893i(f5821mC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5821mC, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5821mC, new Object[]{aku}));
                break;
        }
        m27882d(aku);
    }

    /* renamed from: fZ */
    private Player m27885fZ() {
        return (Player) bFf().mo5608dq().mo3214p(f5832mv);
    }

    /* renamed from: ga */
    private C2686iZ m27886ga() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(f5834mx);
    }

    /* renamed from: gb */
    private boolean m27887gb() {
        return bFf().mo5608dq().mo3201h(f5836mz);
    }

    /* renamed from: gc */
    private boolean m27888gc() {
        return bFf().mo5608dq().mo3201h(f5820mB);
    }

    /* renamed from: i */
    private void m27894i(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(f5834mx, iZVar);
    }

    /* renamed from: t */
    private void m27897t(boolean z) {
        bFf().mo5608dq().mo3153a(f5836mz, z);
    }

    /* renamed from: u */
    private void m27898u(boolean z) {
        bFf().mo5608dq().mo3153a(f5820mB, z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0595IP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m27882d((Player) args[0]);
                return null;
            case 1:
                m27884f((Player) args[0]);
                return null;
            case 2:
                m27893h((Player) args[0]);
                return null;
            case 3:
                m27895j((Player) args[0]);
                return null;
            case 4:
                return m27880a((String) args[0], (Asset) args[1]);
            case 5:
                return m27889gd();
            case 6:
                return new Boolean(m27890gf());
            case 7:
                return new Boolean(m27896l((Player) args[0]));
            case 8:
                return new Boolean(m27891gg());
            case 9:
                m27892gi();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: b */
    public Corporation mo17306b(String str, Asset tCVar) {
        switch (bFf().mo6893i(f5825mG)) {
            case 0:
                return null;
            case 2:
                return (Corporation) bFf().mo5606d(new aCE(this, f5825mG, new Object[]{str, tCVar}));
            case 3:
                bFf().mo5606d(new aCE(this, f5825mG, new Object[]{str, tCVar}));
                break;
        }
        return m27880a(str, tCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: g */
    public void mo17308g(Player aku) {
        switch (bFf().mo6893i(f5822mD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5822mD, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5822mD, new Object[]{aku}));
                break;
        }
        m27884f(aku);
    }

    /* renamed from: ge */
    public Set<CorporationInvitationStatus> mo17309ge() {
        switch (bFf().mo6893i(f5826mH)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, f5826mH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5826mH, new Object[0]));
                break;
        }
        return m27889gd();
    }

    /* renamed from: gh */
    public boolean mo17310gh() {
        switch (bFf().mo6893i(f5829mK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5829mK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5829mK, new Object[0]));
                break;
        }
        return m27891gg();
    }

    /* renamed from: gj */
    public void mo17311gj() {
        switch (bFf().mo6893i(f5830mL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5830mL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5830mL, new Object[0]));
                break;
        }
        m27892gi();
    }

    /* renamed from: i */
    public void mo17312i(Player aku) {
        switch (bFf().mo6893i(f5823mE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5823mE, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5823mE, new Object[]{aku}));
                break;
        }
        m27893h(aku);
    }

    public boolean isDone() {
        switch (bFf().mo6893i(f5827mI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5827mI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5827mI, new Object[0]));
                break;
        }
        return m27890gf();
    }

    /* renamed from: k */
    public void mo17314k(Player aku) {
        switch (bFf().mo6893i(f5824mF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5824mF, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5824mF, new Object[]{aku}));
                break;
        }
        m27895j(aku);
    }

    /* renamed from: m */
    public boolean mo17315m(Player aku) {
        switch (bFf().mo6893i(f5828mJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5828mJ, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5828mJ, new Object[]{aku}));
                break;
        }
        return m27896l(aku);
    }

    /* renamed from: b */
    public void mo17307b(Player aku) {
        super.mo10S();
        m27881c(aku);
        m27897t(false);
        m27898u(false);
        for (Associate next : aku.dxu().cUP()) {
            if (next.aPI()) {
                m27883e(next.mo2077dL());
            }
        }
    }

    @C0064Am(aul = "89280aa44fd09c26a25fdbcb1065f896", aum = 0)
    /* renamed from: d */
    private void m27882d(Player aku) {
        if (aku.dxC() == null) {
            CorporationInvitationStatus aqVar = (CorporationInvitationStatus) bFf().mo6865M(CorporationInvitationStatus.class);
            aqVar.mo15538b(aku);
            m27886ga().add(aqVar);
        }
    }

    @C0064Am(aul = "9fc861c9fd9618c493f2a5d55681d72d", aum = 0)
    /* renamed from: f */
    private void m27884f(Player aku) {
        for (CorporationInvitationStatus aqVar : m27886ga()) {
            if (aqVar.mo15539dL() == aku) {
                aqVar.accept();
                return;
            }
        }
        throw new IllegalArgumentException("Player '" + aku.getName() + "' accepted a Corporation invitation but he wasn't invited!");
    }

    @C0064Am(aul = "2799423b6d554139737c5d916e1427fe", aum = 0)
    /* renamed from: h */
    private void m27893h(Player aku) {
        for (CorporationInvitationStatus aqVar : m27886ga()) {
            if (aqVar.mo15539dL() == aku) {
                aqVar.mo15535LO();
                return;
            }
        }
        throw new IllegalArgumentException("Player '" + aku.getName() + "' refused a Corporation invitation but he wasn't invited!");
    }

    @C0064Am(aul = "d8284985275ed1db8da494f011972349", aum = 0)
    /* renamed from: j */
    private void m27895j(Player aku) {
        for (CorporationInvitationStatus aqVar : m27886ga()) {
            if (aqVar.mo15539dL() == aku) {
                aqVar.mo15536LR();
                return;
            }
        }
        throw new IllegalArgumentException("Player '" + aku.getName() + "' is not in my invitation list");
    }

    @C0064Am(aul = "e2de3219b5a495afebffd0df079c2444", aum = 0)
    /* renamed from: gd */
    private Set<CorporationInvitationStatus> m27889gd() {
        return Collections.unmodifiableSet(m27886ga());
    }

    @C0064Am(aul = "c35a11ba3df5cd939430f2c6bc8c6daa", aum = 0)
    /* renamed from: gf */
    private boolean m27890gf() {
        if (!m27887gb()) {
            return false;
        }
        for (CorporationInvitationStatus LM : m27886ga()) {
            if (LM.mo15534LM() == CorporationInvitationStatus.C1962a.WAITING) {
                return false;
            }
        }
        return true;
    }

    @C0064Am(aul = "2a8a37cb6e3c8366fee92e1616bce32f", aum = 0)
    /* renamed from: l */
    private boolean m27896l(Player aku) {
        return m27885fZ() == aku;
    }

    @C0064Am(aul = "a03ec49360c859447406ee79e6c2ab9d", aum = 0)
    /* renamed from: gg */
    private boolean m27891gg() {
        return m27888gc();
    }

    @C0064Am(aul = "2cf48fe3d96346047d055417b4ccfd12", aum = 0)
    /* renamed from: gi */
    private void m27892gi() {
        m27897t(true);
        Iterator it = m27886ga().iterator();
        while (it.hasNext()) {
            if (((CorporationInvitationStatus) it.next()).mo15534LM() == CorporationInvitationStatus.C1962a.UNSENT) {
                it.remove();
            }
        }
    }
}
