package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C6755asz;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.avj  reason: case insensitive filesystem */
/* compiled from: a */
public class CorporationRoleDefaults extends aDJ implements C0468GU, C1616Xf {

    /* renamed from: NY */
    public static final C5663aRz f5457NY = null;

    /* renamed from: Ob */
    public static final C2491fm f5458Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f5459Oc = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5461bL = null;
    /* renamed from: bN */
    public static final C2491fm f5462bN = null;
    /* renamed from: bO */
    public static final C2491fm f5463bO = null;
    /* renamed from: bP */
    public static final C2491fm f5464bP = null;
    public static final C5663aRz gGA = null;
    public static final C2491fm gGB = null;
    public static final C2491fm gGC = null;
    public static final C2491fm gGD = null;
    public static final C2491fm gGE = null;
    public static final C2491fm gGF = null;
    public static final C5663aRz gGy = null;
    public static final C5663aRz gGz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f5467zQ = null;
    /* renamed from: zT */
    public static final C2491fm f5468zT = null;
    /* renamed from: zU */
    public static final C2491fm f5469zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bb0292d7e3a6f3b29de44272a994e117", aum = 5)

    /* renamed from: bK */
    private static UUID f5460bK;
    @C0064Am(aul = "61d6a70f63b2953b41f59aefd7b4366c", aum = 0)
    private static C5741aUz dhu;
    @C0064Am(aul = "a22544b587ced2832ba57f2ab443ca33", aum = 4)
    private static C1556Wo<C6704asA, Boolean> dhv;
    @C0064Am(aul = "ffe46553f485383a8ea86acce862ac0b", aum = 2)
    private static I18NString gvd;
    @C0064Am(aul = "5452ac66a14c7515b8f4200d2c5e2262", aum = 3)

    /* renamed from: jn */
    private static Asset f5465jn;
    @C0064Am(aul = "905b6405d36d12476238d22a65b280c8", aum = 1)

    /* renamed from: zP */
    private static I18NString f5466zP;

    static {
        m26690V();
    }

    public CorporationRoleDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationRoleDefaults(C5540aNg ang) {
        super(ang);
    }

    public CorporationRoleDefaults(C5741aUz auz) {
        super((C5540aNg) null);
        super._m_script_init(auz);
    }

    /* renamed from: V */
    static void m26690V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 6;
        _m_methodCount = aDJ._m_methodCount + 14;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(CorporationRoleDefaults.class, "61d6a70f63b2953b41f59aefd7b4366c", i);
        gGy = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CorporationRoleDefaults.class, "905b6405d36d12476238d22a65b280c8", i2);
        f5467zQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CorporationRoleDefaults.class, "ffe46553f485383a8ea86acce862ac0b", i3);
        gGz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CorporationRoleDefaults.class, "5452ac66a14c7515b8f4200d2c5e2262", i4);
        f5457NY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CorporationRoleDefaults.class, "a22544b587ced2832ba57f2ab443ca33", i5);
        gGA = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CorporationRoleDefaults.class, "bb0292d7e3a6f3b29de44272a994e117", i6);
        f5461bL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i8 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 14)];
        C2491fm a = C4105zY.m41624a(CorporationRoleDefaults.class, "75e3c5bd7d62b9056772152d6cc51ae6", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationRoleDefaults.class, "0965e162c857127e858db3e92da01925", i9);
        f5462bN = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationRoleDefaults.class, "93bcefa4a293c3d6b5e74ef7e5439928", i10);
        f5463bO = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationRoleDefaults.class, "dc1f38622bca192096358199fcc84e49", i11);
        f5464bP = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(CorporationRoleDefaults.class, "2c9608fd1c814efa9ec38f667923be04", i12);
        _f_onResurrect_0020_0028_0029V = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(CorporationRoleDefaults.class, "8a44eb102751c3127fb2b70e61e5b911", i13);
        gGB = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(CorporationRoleDefaults.class, "dd038d10555eed79ccfb0f68fb317dbf", i14);
        f5468zT = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(CorporationRoleDefaults.class, "8c0fc7d017727dc0e821b55343913d6e", i15);
        f5469zU = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(CorporationRoleDefaults.class, "aaf6290d8124f994e131ffe7bdddae54", i16);
        gGC = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(CorporationRoleDefaults.class, "7eb30dcfc416fda4c71c6742a3a5d70c", i17);
        gGD = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(CorporationRoleDefaults.class, "b7c94e246413fd1f04943a7b1d26d80d", i18);
        f5458Ob = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(CorporationRoleDefaults.class, "79b31351ab25f96ec479ab0099f1c2c2", i19);
        f5459Oc = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(CorporationRoleDefaults.class, "df5bb72831804b97ef2cd5d5699fab4f", i20);
        gGE = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(CorporationRoleDefaults.class, "5fdde6b080e6f2ad0a6e6efdc9a70a4d", i21);
        gGF = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationRoleDefaults.class, C6755asz.class, _m_fields, _m_methods);
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Permissions")
    @C0064Am(aul = "5fdde6b080e6f2ad0a6e6efdc9a70a4d", aum = 0)
    /* renamed from: D */
    private void m26689D(Map<C6704asA, Boolean> map) {
        throw new aWi(new aCE(this, gGF, new Object[]{map}));
    }

    /* renamed from: Z */
    private void m26691Z(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(gGA, wo);
    }

    /* renamed from: a */
    private void m26692a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5461bL, uuid);
    }

    /* renamed from: an */
    private UUID m26694an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5461bL);
    }

    /* renamed from: c */
    private void m26699c(UUID uuid) {
        switch (bFf().mo6893i(f5463bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5463bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5463bO, new Object[]{uuid}));
                break;
        }
        m26698b(uuid);
    }

    private C5741aUz cyQ() {
        return (C5741aUz) bFf().mo5608dq().mo3214p(gGy);
    }

    private I18NString cyR() {
        return (I18NString) bFf().mo5608dq().mo3214p(gGz);
    }

    private C1556Wo cyS() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(gGA);
    }

    /* renamed from: d */
    private void m26700d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f5467zQ, i18NString);
    }

    /* renamed from: e */
    private void m26701e(C5741aUz auz) {
        bFf().mo5608dq().mo3197f(gGy, auz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "8c0fc7d017727dc0e821b55343913d6e", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m26702e(I18NString i18NString) {
        throw new aWi(new aCE(this, f5469zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m26703kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f5467zQ);
    }

    /* renamed from: nq */
    private void m26705nq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(gGz, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Function")
    @C0064Am(aul = "7eb30dcfc416fda4c71c6742a3a5d70c", aum = 0)
    @C5566aOg
    /* renamed from: nr */
    private void m26706nr(I18NString i18NString) {
        throw new aWi(new aCE(this, gGD, new Object[]{i18NString}));
    }

    /* renamed from: sG */
    private Asset m26707sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f5457NY);
    }

    /* renamed from: t */
    private void m26709t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f5457NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "79b31351ab25f96ec479ab0099f1c2c2", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m26710u(Asset tCVar) {
        throw new aWi(new aCE(this, f5459Oc, new Object[]{tCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Permissions")
    @C5566aOg
    /* renamed from: E */
    public void mo16618E(Map<C6704asA, Boolean> map) {
        switch (bFf().mo6893i(gGF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGF, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGF, new Object[]{map}));
                break;
        }
        m26689D(map);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6755asz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m26697au();
            case 1:
                return m26695ap();
            case 2:
                m26698b((UUID) args[0]);
                return null;
            case 3:
                return m26696ar();
            case 4:
                m26693aG();
                return null;
            case 5:
                return cyT();
            case 6:
                return m26704kd();
            case 7:
                m26702e((I18NString) args[0]);
                return null;
            case 8:
                return cyV();
            case 9:
                m26706nr((I18NString) args[0]);
                return null;
            case 10:
                return m26708sJ();
            case 11:
                m26710u((Asset) args[0]);
                return null;
            case 12:
                return cyX();
            case 13:
                m26689D((Map) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m26693aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5462bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5462bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5462bN, new Object[0]));
                break;
        }
        return m26695ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Role")
    public C5741aUz cyU() {
        switch (bFf().mo6893i(gGB)) {
            case 0:
                return null;
            case 2:
                return (C5741aUz) bFf().mo5606d(new aCE(this, gGB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gGB, new Object[0]));
                break;
        }
        return cyT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Function")
    public I18NString cyW() {
        switch (bFf().mo6893i(gGC)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gGC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gGC, new Object[0]));
                break;
        }
        return cyV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Permissions")
    public Map<C6704asA, Boolean> cyY() {
        switch (bFf().mo6893i(gGE)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, gGE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gGE, new Object[0]));
                break;
        }
        return cyX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo16623f(I18NString i18NString) {
        switch (bFf().mo6893i(f5469zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5469zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5469zU, new Object[]{i18NString}));
                break;
        }
        m26702e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5464bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5464bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5464bP, new Object[0]));
                break;
        }
        return m26696ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo16624ke() {
        switch (bFf().mo6893i(f5468zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f5468zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5468zT, new Object[0]));
                break;
        }
        return m26704kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Function")
    @C5566aOg
    /* renamed from: ns */
    public void mo16625ns(I18NString i18NString) {
        switch (bFf().mo6893i(gGD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gGD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gGD, new Object[]{i18NString}));
                break;
        }
        m26706nr(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo16626sK() {
        switch (bFf().mo6893i(f5458Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f5458Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5458Ob, new Object[0]));
                break;
        }
        return m26708sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m26697au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo16627v(Asset tCVar) {
        switch (bFf().mo6893i(f5459Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5459Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5459Oc, new Object[]{tCVar}));
                break;
        }
        m26710u(tCVar);
    }

    @C0064Am(aul = "75e3c5bd7d62b9056772152d6cc51ae6", aum = 0)
    /* renamed from: au */
    private String m26697au() {
        return cyQ() + " [" + m26703kb().get() + "]";
    }

    @C0064Am(aul = "0965e162c857127e858db3e92da01925", aum = 0)
    /* renamed from: ap */
    private UUID m26695ap() {
        return m26694an();
    }

    @C0064Am(aul = "93bcefa4a293c3d6b5e74ef7e5439928", aum = 0)
    /* renamed from: b */
    private void m26698b(UUID uuid) {
        m26692a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "dc1f38622bca192096358199fcc84e49", aum = 0)
    /* renamed from: ar */
    private String m26696ar() {
        return "corporation_role_defaults";
    }

    /* renamed from: f */
    public void mo16622f(C5741aUz auz) {
        super.mo10S();
        m26701e(auz);
        for (C6704asA asa : C6704asA.values()) {
            if (auz == C5741aUz.dAr()) {
                cyS().put(asa, Boolean.TRUE);
            } else {
                cyS().put(asa, Boolean.FALSE);
            }
        }
        m26692a(UUID.randomUUID());
    }

    @C0064Am(aul = "2c9608fd1c814efa9ec38f667923be04", aum = 0)
    /* renamed from: aG */
    private void m26693aG() {
        for (C6704asA asa : C6704asA.values()) {
            if (!cyS().containsKey(asa)) {
                if (cyQ() == C5741aUz.dAr()) {
                    cyS().put(asa, Boolean.TRUE);
                } else {
                    cyS().put(asa, Boolean.FALSE);
                }
            }
        }
        super.mo70aH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Role")
    @C0064Am(aul = "8a44eb102751c3127fb2b70e61e5b911", aum = 0)
    private C5741aUz cyT() {
        return cyQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "dd038d10555eed79ccfb0f68fb317dbf", aum = 0)
    /* renamed from: kd */
    private I18NString m26704kd() {
        return m26703kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Function")
    @C0064Am(aul = "aaf6290d8124f994e131ffe7bdddae54", aum = 0)
    private I18NString cyV() {
        return cyR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "b7c94e246413fd1f04943a7b1d26d80d", aum = 0)
    /* renamed from: sJ */
    private Asset m26708sJ() {
        return m26707sG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Permissions")
    @C0064Am(aul = "df5bb72831804b97ef2cd5d5699fab4f", aum = 0)
    private Map<C6704asA, Boolean> cyX() {
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(cyS());
        return Collections.unmodifiableMap(treeMap);
    }
}
