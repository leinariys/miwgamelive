package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6554apG;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.azK */
/* compiled from: a */
public class CorporationMembership extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm gGB = null;
    public static final C5663aRz gGy = null;
    /* renamed from: hF */
    public static final C2491fm f5672hF = null;
    public static final C5663aRz haW = null;
    public static final C5663aRz haX = null;
    public static final C5663aRz haY = null;
    public static final C2491fm haZ = null;
    public static final C2491fm hba = null;
    public static final C2491fm hbb = null;
    public static final C2491fm hbc = null;
    public static final C2491fm hbd = null;
    public static final C2491fm hbe = null;
    public static final C2491fm hbf = null;
    public static final C2491fm hbg = null;
    public static final C2491fm hbh = null;
    /* renamed from: hz */
    public static final C5663aRz f5673hz = null;
    public static final long serialVersionUID = 0;
    private static final long dXJ = 86400000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "57571eaa14447ecffef20b61fc314b5b", aum = 0)

    /* renamed from: P */
    private static Player f5671P = null;
    @C0064Am(aul = "37d825ae74ec7d49350f71790137b87e", aum = 4)
    private static C5741aUz dhu;
    @C0064Am(aul = "810363ac49c268a238ca2591fdcd0388", aum = 1)
    private static long gmS;
    @C0064Am(aul = "83359e7e7e49ec8468a4e93bf199e878", aum = 2)
    private static long gmT;
    @C0064Am(aul = "4c32f293751a8a05aab7db31d5453762", aum = 3)
    private static long gmU;

    static {
        m27602V();
    }

    public CorporationMembership() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationMembership(C5540aNg ang) {
        super(ang);
    }

    public CorporationMembership(Player aku, C5741aUz auz) {
        super((C5540aNg) null);
        super._m_script_init(aku, auz);
    }

    /* renamed from: V */
    static void m27602V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(CorporationMembership.class, "57571eaa14447ecffef20b61fc314b5b", i);
        f5673hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CorporationMembership.class, "810363ac49c268a238ca2591fdcd0388", i2);
        haW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CorporationMembership.class, "83359e7e7e49ec8468a4e93bf199e878", i3);
        haX = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CorporationMembership.class, "4c32f293751a8a05aab7db31d5453762", i4);
        haY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CorporationMembership.class, "37d825ae74ec7d49350f71790137b87e", i5);
        gGy = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 13)];
        C2491fm a = C4105zY.m41624a(CorporationMembership.class, "cf7fe9db85e8888d4fb110bd723b4189", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationMembership.class, "e480a2a178f359179e5078bed52066c1", i8);
        _f_onResurrect_0020_0028_0029V = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationMembership.class, "f06a5bc3cf6e0b4bceded9fc1667d11a", i9);
        f5672hF = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationMembership.class, "b976de03f59717ffe9459feba64c2264", i10);
        haZ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(CorporationMembership.class, "e480edaf3258fd0b5d7df3a11bf0ac4b", i11);
        hba = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(CorporationMembership.class, "fe151ff4a1667640f71c93bf0a1aa76a", i12);
        hbb = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(CorporationMembership.class, "cc2bbf20fa2a5022e66c3e6c2c71062b", i13);
        hbc = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(CorporationMembership.class, "54ccb68ef4b495d5351e32b19879878b", i14);
        hbd = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(CorporationMembership.class, "a5565db07f20f266e01784dbadf1c86e", i15);
        hbe = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(CorporationMembership.class, "b953fd790f9a3a0098eaa2185e0a3b12", i16);
        gGB = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(CorporationMembership.class, "148fdec9604c22551c28b5a098725368", i17);
        hbf = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(CorporationMembership.class, "c627f469fe9a9eff5c59862ccacf9b1a", i18);
        hbg = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(CorporationMembership.class, "b9401b2456622f07f412f9af7df8385f", i19);
        hbh = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationMembership.class, C6554apG.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m27603a(Player aku) {
        bFf().mo5608dq().mo3197f(f5673hz, aku);
    }

    @C0064Am(aul = "e480edaf3258fd0b5d7df3a11bf0ac4b", aum = 0)
    @C5566aOg
    private void cGB() {
        throw new aWi(new aCE(this, hba, new Object[0]));
    }

    @C0064Am(aul = "148fdec9604c22551c28b5a098725368", aum = 0)
    @C5566aOg
    private void cGJ() {
        throw new aWi(new aCE(this, hbf, new Object[0]));
    }

    @C0064Am(aul = "c627f469fe9a9eff5c59862ccacf9b1a", aum = 0)
    @C5566aOg
    private void cGL() {
        throw new aWi(new aCE(this, hbg, new Object[0]));
    }

    @C0064Am(aul = "b9401b2456622f07f412f9af7df8385f", aum = 0)
    @C5566aOg
    private void cGN() {
        throw new aWi(new aCE(this, hbh, new Object[0]));
    }

    private long cGw() {
        return bFf().mo5608dq().mo3213o(haW);
    }

    private long cGx() {
        return bFf().mo5608dq().mo3213o(haX);
    }

    private long cGy() {
        return bFf().mo5608dq().mo3213o(haY);
    }

    private C5741aUz cyQ() {
        return (C5741aUz) bFf().mo5608dq().mo3214p(gGy);
    }

    /* renamed from: dG */
    private Player m27606dG() {
        return (Player) bFf().mo5608dq().mo3214p(f5673hz);
    }

    /* renamed from: e */
    private void m27608e(C5741aUz auz) {
        bFf().mo5608dq().mo3197f(gGy, auz);
    }

    /* renamed from: jp */
    private void m27609jp(long j) {
        bFf().mo5608dq().mo3184b(haW, j);
    }

    /* renamed from: jq */
    private void m27610jq(long j) {
        bFf().mo5608dq().mo3184b(haX, j);
    }

    /* renamed from: jr */
    private void m27611jr(long j) {
        bFf().mo5608dq().mo3184b(haY, j);
    }

    @C0064Am(aul = "54ccb68ef4b495d5351e32b19879878b", aum = 0)
    @C5566aOg
    /* renamed from: js */
    private void m27612js(long j) {
        throw new aWi(new aCE(this, hbd, new Object[]{new Long(j)}));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6554apG(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m27605au();
            case 1:
                m27604aG();
                return null;
            case 2:
                return m27607dK();
            case 3:
                return new Long(cGz());
            case 4:
                cGB();
                return null;
            case 5:
                return new Long(cGD());
            case 6:
                return new Integer(cGF());
            case 7:
                m27612js(((Long) args[0]).longValue());
                return null;
            case 8:
                return new Long(cGH());
            case 9:
                return cyT();
            case 10:
                cGJ();
                return null;
            case 11:
                cGL();
                return null;
            case 12:
                cGN();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m27604aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public long cGA() {
        switch (bFf().mo6893i(haZ)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, haZ, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, haZ, new Object[0]));
                break;
        }
        return cGz();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cGC() {
        switch (bFf().mo6893i(hba)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hba, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hba, new Object[0]));
                break;
        }
        cGB();
    }

    public long cGE() {
        switch (bFf().mo6893i(hbb)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hbb, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbb, new Object[0]));
                break;
        }
        return cGD();
    }

    public int cGG() {
        switch (bFf().mo6893i(hbc)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hbc, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbc, new Object[0]));
                break;
        }
        return cGF();
    }

    public long cGI() {
        switch (bFf().mo6893i(hbe)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hbe, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbe, new Object[0]));
                break;
        }
        return cGH();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cGK() {
        switch (bFf().mo6893i(hbf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hbf, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hbf, new Object[0]));
                break;
        }
        cGJ();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cGM() {
        switch (bFf().mo6893i(hbg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hbg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hbg, new Object[0]));
                break;
        }
        cGL();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cGO() {
        switch (bFf().mo6893i(hbh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hbh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hbh, new Object[0]));
                break;
        }
        cGN();
    }

    public C5741aUz cyU() {
        switch (bFf().mo6893i(gGB)) {
            case 0:
                return null;
            case 2:
                return (C5741aUz) bFf().mo5606d(new aCE(this, gGB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gGB, new Object[0]));
                break;
        }
        return cyT();
    }

    /* renamed from: dL */
    public Player mo17177dL() {
        switch (bFf().mo6893i(f5672hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f5672hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5672hF, new Object[0]));
                break;
        }
        return m27607dK();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: jt */
    public void mo17178jt(long j) {
        switch (bFf().mo6893i(hbd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hbd, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hbd, new Object[]{new Long(j)}));
                break;
        }
        m27612js(j);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m27605au();
    }

    @C0064Am(aul = "cf7fe9db85e8888d4fb110bd723b4189", aum = 0)
    /* renamed from: au */
    private String m27605au() {
        return "Role [" + cyQ() + "], Player [" + m27606dG().getName() + "], Join time: " + cGw();
    }

    /* renamed from: a */
    public void mo17167a(Player aku, C5741aUz auz) {
        super.mo10S();
        m27608e(auz);
        m27603a(aku);
        m27609jp(cVr());
        m27610jq(cVr());
        m27611jr(0);
    }

    @C0064Am(aul = "e480a2a178f359179e5078bed52066c1", aum = 0)
    /* renamed from: aG */
    private void m27604aG() {
        if (cGw() == 0) {
            m27609jp(cVr());
        }
        super.mo70aH();
    }

    @C0064Am(aul = "f06a5bc3cf6e0b4bceded9fc1667d11a", aum = 0)
    /* renamed from: dK */
    private Player m27607dK() {
        return m27606dG();
    }

    @C0064Am(aul = "b976de03f59717ffe9459feba64c2264", aum = 0)
    private long cGz() {
        return cGw();
    }

    @C0064Am(aul = "fe151ff4a1667640f71c93bf0a1aa76a", aum = 0)
    private long cGD() {
        return cGx();
    }

    @C0064Am(aul = "cc2bbf20fa2a5022e66c3e6c2c71062b", aum = 0)
    private int cGF() {
        return (int) Math.floor(((double) (cVr() - cGx())) / 8.64E7d);
    }

    @C0064Am(aul = "a5565db07f20f266e01784dbadf1c86e", aum = 0)
    private long cGH() {
        return cGy();
    }

    @C0064Am(aul = "b953fd790f9a3a0098eaa2185e0a3b12", aum = 0)
    private C5741aUz cyT() {
        return cyQ();
    }
}
