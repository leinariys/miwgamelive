package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.aGM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.GD */
/* compiled from: a */
public class CorporationReservedNames extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cZo = null;
    public static final C2491fm cZp = null;
    public static final C2491fm cZq = null;
    public static final C2491fm cZr = null;
    public static final C2491fm cZs = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "cb5c17e1f65a04a20ed93c83830a91ed", aum = 0)
    private static C1556Wo<String, Player> cZn;

    static {
        m3328V();
    }

    public CorporationReservedNames() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationReservedNames(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3328V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 4;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(CorporationReservedNames.class, "cb5c17e1f65a04a20ed93c83830a91ed", i);
        cZo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(CorporationReservedNames.class, "f6a4db50ef74a420ed483b9ee0170e84", i3);
        cZp = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationReservedNames.class, "690aaf97f855828c70bfc2b4d63c30e8", i4);
        cZq = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationReservedNames.class, "33c3a256ad2eb7ece722ebf3a6222a87", i5);
        cZr = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationReservedNames.class, "1e83ad058ffbcaf56ea10015573d944c", i6);
        cZs = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationReservedNames.class, aGM.class, _m_fields, _m_methods);
    }

    /* renamed from: D */
    private void m3327D(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cZo, wo);
    }

    private C1556Wo aRj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cZo);
    }

    /* renamed from: eu */
    private Player m3332eu(String str) {
        switch (bFf().mo6893i(cZs)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, cZs, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, cZs, new Object[]{str}));
                break;
        }
        return m3331et(str);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aGM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return aRk();
            case 1:
                return m3329c((String) args[0], (Player) args[1]);
            case 2:
                return new Boolean(m3330e((String) args[0], (Player) args[1]));
            case 3:
                return m3331et((String) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C1556Wo<String, Player> aRl() {
        switch (bFf().mo6893i(cZp)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, cZp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cZp, new Object[0]));
                break;
        }
        return aRk();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public Player mo2283d(String str, Player aku) {
        switch (bFf().mo6893i(cZq)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, cZq, new Object[]{str, aku}));
            case 3:
                bFf().mo5606d(new aCE(this, cZq, new Object[]{str, aku}));
                break;
        }
        return m3329c(str, aku);
    }

    /* renamed from: f */
    public boolean mo2284f(String str, Player aku) {
        switch (bFf().mo6893i(cZr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cZr, new Object[]{str, aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cZr, new Object[]{str, aku}));
                break;
        }
        return m3330e(str, aku);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f6a4db50ef74a420ed483b9ee0170e84", aum = 0)
    private C1556Wo<String, Player> aRk() {
        return aRj();
    }

    @C0064Am(aul = "690aaf97f855828c70bfc2b4d63c30e8", aum = 0)
    /* renamed from: c */
    private Player m3329c(String str, Player aku) {
        Player eu = m3332eu(str);
        if (eu != null) {
            return eu;
        }
        aRj().put(str, aku);
        return aku;
    }

    @C0064Am(aul = "33c3a256ad2eb7ece722ebf3a6222a87", aum = 0)
    /* renamed from: e */
    private boolean m3330e(String str, Player aku) {
        Player eu = m3332eu(str);
        if (eu != null && !eu.equals(aku)) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "1e83ad058ffbcaf56ea10015573d944c", aum = 0)
    /* renamed from: et */
    private Player m3331et(String str) {
        return (Player) aRl().get(str);
    }
}
