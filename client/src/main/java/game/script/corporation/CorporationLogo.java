package game.script.corporation;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C5423aIt;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.EC */
/* compiled from: a */
public class CorporationLogo extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bP */
    public static final C2491fm f474bP = null;
    public static final C5663aRz cTb = null;
    public static final C2491fm cTc = null;
    public static final C2491fm cTd = null;
    public static final C2491fm cTe = null;
    public static final long serialVersionUID = 0;
    private static final String cSX = "_big";
    private static final String cSY = "_medium";
    private static final String cSZ = "_small";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "df4160234fc9c35fee35492cc7bcb767", aum = 0)
    private static Asset cTa;

    static {
        m2779V();
    }

    public CorporationLogo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationLogo(C5540aNg ang) {
        super(ang);
    }

    public CorporationLogo(Asset tCVar) {
        super((C5540aNg) null);
        super._m_script_init(tCVar);
    }

    /* renamed from: V */
    static void m2779V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 4;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(CorporationLogo.class, "df4160234fc9c35fee35492cc7bcb767", i);
        cTb = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(CorporationLogo.class, "c62948777bf1b961f0533376428045a3", i3);
        f474bP = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationLogo.class, "e8fe94b57cbccc9957fef7c97a1ef2f5", i4);
        cTc = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationLogo.class, "44bbba6e638ac4b1ce3a497914d68eb0", i5);
        cTd = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationLogo.class, "9165792e688f4bb21ae2c69d50699fab", i6);
        cTe = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationLogo.class, C5423aIt.class, _m_fields, _m_methods);
    }

    private Asset aOK() {
        return (Asset) bFf().mo5608dq().mo3214p(cTb);
    }

    /* renamed from: al */
    private void m2780al(Asset tCVar) {
        bFf().mo5608dq().mo3197f(cTb, tCVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5423aIt(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m2781ar();
            case 1:
                return aOL();
            case 2:
                return aON();
            case 3:
                return aOP();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public String aOM() {
        switch (bFf().mo6893i(cTc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cTc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cTc, new Object[0]));
                break;
        }
        return aOL();
    }

    public String aOO() {
        switch (bFf().mo6893i(cTd)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cTd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cTd, new Object[0]));
                break;
        }
        return aON();
    }

    public String aOQ() {
        switch (bFf().mo6893i(cTe)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cTe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cTe, new Object[0]));
                break;
        }
        return aOP();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String getHandle() {
        switch (bFf().mo6893i(f474bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f474bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f474bP, new Object[0]));
                break;
        }
        return m2781ar();
    }

    /* renamed from: am */
    public void mo1807am(Asset tCVar) {
        super.mo10S();
        m2780al(tCVar);
    }

    @C0064Am(aul = "c62948777bf1b961f0533376428045a3", aum = 0)
    /* renamed from: ar */
    private String m2781ar() {
        if (aOK() != null) {
            return aOK().getHandle();
        }
        throw new IllegalStateException("Some CorporationLogo has null asset");
    }

    @C0064Am(aul = "e8fe94b57cbccc9957fef7c97a1ef2f5", aum = 0)
    private String aOL() {
        return String.valueOf(getHandle()) + cSX;
    }

    @C0064Am(aul = "44bbba6e638ac4b1ce3a497914d68eb0", aum = 0)
    private String aON() {
        return String.valueOf(getHandle()) + cSY;
    }

    @C0064Am(aul = "9165792e688f4bb21ae2c69d50699fab", aum = 0)
    private String aOP() {
        return String.valueOf(getHandle()) + cSZ;
    }
}
