package game.script.corporation;

import game.network.message.externalizable.C0352Eg;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.bank.BankAccount;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Outpost;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.mbean.C5607aPv;
import logic.data.mbean.C5904acg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import p001a.*;

import java.util.*;

@C5829abJ("1.10.2")
@C6485anp
@C5511aMd
/* renamed from: a.aP */
/* compiled from: a */
public class Corporation extends TaikodomObject implements C1165RF, C1616Xf {

    /* renamed from: GZ */
    public static final C5663aRz f3494GZ = null;

    /* renamed from: Lm */
    public static final C2491fm f3495Lm = null;

    /* renamed from: Pf */
    public static final C2491fm f3496Pf = null;

    /* renamed from: QA */
    public static final C5663aRz f3497QA = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJA = null;
    public static final C2491fm aJB = null;
    public static final C2491fm aJC = null;
    public static final C2491fm aJD = null;
    public static final C2491fm aJE = null;
    public static final C2491fm aJF = null;
    public static final C2491fm aJG = null;
    public static final C2491fm aJH = null;
    public static final C2491fm aJI = null;
    public static final C2491fm aJJ = null;
    public static final C2491fm aJK = null;
    public static final C2491fm aJL = null;
    public static final C2491fm aJM = null;
    public static final C2491fm aJN = null;
    public static final C2491fm aJO = null;
    public static final C2491fm aJP = null;
    public static final C2491fm aJQ = null;
    public static final C2491fm aJR = null;
    public static final C2491fm aJS = null;
    public static final C2491fm aJT = null;
    public static final C2491fm aJU = null;
    public static final C2491fm aJV = null;
    public static final C2491fm aJW = null;
    public static final C2491fm aJX = null;
    public static final C2491fm aJY = null;
    public static final C2491fm aJZ = null;
    public static final int aJi = 5;
    public static final int aJj = 60;
    public static final C5663aRz aJl = null;
    public static final C5663aRz aJn = null;
    public static final C5663aRz aJp = null;
    public static final C5663aRz aJs = null;
    public static final C5663aRz aJu = null;
    public static final C5663aRz aJw = null;
    public static final C2491fm aJx = null;
    public static final C2491fm aJy = null;
    public static final C2491fm aJz = null;
    public static final C2491fm aKa = null;
    public static final C2491fm aKb = null;
    public static final C2491fm aKc = null;
    public static final C2491fm aKd = null;
    public static final C2491fm aKe = null;
    public static final C2491fm aKf = null;
    public static final C2491fm aKg = null;
    public static final C2491fm aKh = null;
    public static final C2491fm aKi = null;
    public static final C2491fm aKj = null;
    public static final C2491fm aKk = null;
    public static final C2491fm aKl = null;
    public static final C2491fm aKm = null;
    public static final C2491fm aKn = null;
    /* renamed from: mJ */
    public static final C2491fm f3498mJ = null;
    /* renamed from: mv */
    public static final C5663aRz f3500mv = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f3501zQ = null;
    private static final int aJh = 30;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "2998c75b712df5097fe66536ec818a53", aum = 3)
    private static C1556Wo<Player, CorporationMembership> aJk;
    @C0064Am(aul = "2fc076d302c859a629604c63a630ec8d", aum = 4)
    private static C1556Wo<C5741aUz, CorporationRoleInfo> aJm;
    @C0064Am(aul = "7489f2f8aa4e218ba6fd37fe4aefd095", aum = 5)
    private static CorporationLogo aJo;
    @C0064Am(aul = "13d8adc2d97e15c54faba289b1fc0e4a", aum = 6)
    private static String aJq;
    @C0064Am(aul = "d60f61f56ad4e6db23bd79c2ff97efbd", aum = 7)
    private static BankAccount aJr;
    @C0064Am(aul = "c3d7cce26edbfc491971509b9f265442", aum = 8)
    private static C2686iZ<Outpost> aJt;
    @C0064Am(aul = "9c459228bae19ecc10b720b05297bc18", aum = 9)
    private static PlayerDisposedListener aJv;
    @C0064Am(aul = "14bf994e536b7e115b8a101e6111e8bd", aum = 1)
    private static String description;
    @C0064Am(aul = "abe0ec7adff0c681b2caa88325978fc4", aum = 2)

    /* renamed from: mu */
    private static Player f3499mu;
    @C0064Am(aul = "8dc0162435255d85f31cd5be21912cbb", aum = 0)
    private static String name;

    static {
        m17044V();
    }

    public Corporation() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Corporation(C5540aNg ang) {
        super(ang);
    }

    public Corporation(String str, Player aku, List<Player> list, Asset tCVar) {
        super((C5540aNg) null);
        super._m_script_init(str, aku, list, tCVar);
    }

    /* renamed from: V */
    static void m17044V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 10;
        _m_methodCount = TaikodomObject._m_methodCount + 49;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(Corporation.class, "8dc0162435255d85f31cd5be21912cbb", i);
        f3501zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Corporation.class, "14bf994e536b7e115b8a101e6111e8bd", i2);
        f3497QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Corporation.class, "abe0ec7adff0c681b2caa88325978fc4", i3);
        f3500mv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Corporation.class, "2998c75b712df5097fe66536ec818a53", i4);
        aJl = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Corporation.class, "2fc076d302c859a629604c63a630ec8d", i5);
        aJn = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Corporation.class, "7489f2f8aa4e218ba6fd37fe4aefd095", i6);
        aJp = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Corporation.class, "13d8adc2d97e15c54faba289b1fc0e4a", i7);
        f3494GZ = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Corporation.class, "d60f61f56ad4e6db23bd79c2ff97efbd", i8);
        aJs = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Corporation.class, "c3d7cce26edbfc491971509b9f265442", i9);
        aJu = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Corporation.class, "9c459228bae19ecc10b720b05297bc18", i10);
        aJw = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i12 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 49)];
        C2491fm a = C4105zY.m41624a(Corporation.class, "712f10dd0c4ea326b27ed262fa6faa9f", i12);
        _f_dispose_0020_0028_0029V = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(Corporation.class, "d64b0746398c5a8b9eb7ab140807bc82", i13);
        _f_onResurrect_0020_0028_0029V = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(Corporation.class, "2050a1e4c1795d36ba1d717922f55579", i14);
        aJx = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(Corporation.class, "80a7b200716ff8141e1225de50030849", i15);
        f3496Pf = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(Corporation.class, "f3437c6886f776ca26e52806e37516cc", i16);
        aJy = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(Corporation.class, "9359fbd47a22da351c51786f885bc140", i17);
        aJz = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(Corporation.class, "37e040480e4819d16be3c89e94a116e6", i18);
        aJA = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(Corporation.class, "5884495fa968165805e2f0da3dbcdc33", i19);
        aJB = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(Corporation.class, "584b4f82b5463e464a3014326a7acffc", i20);
        aJC = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(Corporation.class, "ba50d99de36d286b3936642f675c07dc", i21);
        aJD = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(Corporation.class, "7a2d87349d8be7fa44930ffdacd679b7", i22);
        aJE = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(Corporation.class, "666b05633fb2e1318aee9ddc0293b161", i23);
        aJF = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(Corporation.class, "0a81e2961290092d0185d887f935bcc4", i24);
        aJG = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(Corporation.class, "92f4f6a12a5bcb1b347de6aa721a15b6", i25);
        aJH = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(Corporation.class, "25d115a2300dabd7c8278c15ff0f2587", i26);
        aJI = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(Corporation.class, "24bf2901c55f4967d8bdae2f9aae3f2d", i27);
        aJJ = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(Corporation.class, "45e164c42354cc2f7ae5efd74c7f39e3", i28);
        aJK = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(Corporation.class, "958b903e09f7790f1b17c11a1b8d2d41", i29);
        aJL = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(Corporation.class, "7609925cd25b4d09d83f76048a3b01e5", i30);
        aJM = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(Corporation.class, "dc926fccb02c4639be7156707d36f388", i31);
        aJN = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(Corporation.class, "1ffcc5042546c13842066a610aa6a331", i32);
        aJO = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(Corporation.class, "3e16f17227b438e521113493daf55ea9", i33);
        aJP = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(Corporation.class, "48e3c1821b72ae655c0b0d516d769647", i34);
        aJQ = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        C2491fm a24 = C4105zY.m41624a(Corporation.class, "dca1c75e88720429db5604afed61e2d7", i35);
        aJR = a24;
        fmVarArr[i35] = a24;
        int i36 = i35 + 1;
        C2491fm a25 = C4105zY.m41624a(Corporation.class, "f7211f4a83d6c86c8fa1799be3d38950", i36);
        aJS = a25;
        fmVarArr[i36] = a25;
        int i37 = i36 + 1;
        C2491fm a26 = C4105zY.m41624a(Corporation.class, "5386a97597086f00703ba782c46bd640", i37);
        f3498mJ = a26;
        fmVarArr[i37] = a26;
        int i38 = i37 + 1;
        C2491fm a27 = C4105zY.m41624a(Corporation.class, "5a1545f666aff5cb7d872c804d5b4126", i38);
        aJT = a27;
        fmVarArr[i38] = a27;
        int i39 = i38 + 1;
        C2491fm a28 = C4105zY.m41624a(Corporation.class, "279b7a5c166afc953115d401bfdbe8a0", i39);
        aJU = a28;
        fmVarArr[i39] = a28;
        int i40 = i39 + 1;
        C2491fm a29 = C4105zY.m41624a(Corporation.class, "bc3c8ea3f726cff11540e9335e103c86", i40);
        aJV = a29;
        fmVarArr[i40] = a29;
        int i41 = i40 + 1;
        C2491fm a30 = C4105zY.m41624a(Corporation.class, "2d45d8af68999815c7ef9053a987d8c7", i41);
        aJW = a30;
        fmVarArr[i41] = a30;
        int i42 = i41 + 1;
        C2491fm a31 = C4105zY.m41624a(Corporation.class, "7804c3ae9c7fb9dcebaf6b6411782a20", i42);
        aJX = a31;
        fmVarArr[i42] = a31;
        int i43 = i42 + 1;
        C2491fm a32 = C4105zY.m41624a(Corporation.class, "343b7cdf237333a5e89b886f10a7b4e6", i43);
        aJY = a32;
        fmVarArr[i43] = a32;
        int i44 = i43 + 1;
        C2491fm a33 = C4105zY.m41624a(Corporation.class, "c274121b6653b92d058977f4131554df", i44);
        aJZ = a33;
        fmVarArr[i44] = a33;
        int i45 = i44 + 1;
        C2491fm a34 = C4105zY.m41624a(Corporation.class, "dce2965ff4406593ad356aba3f251d36", i45);
        aKa = a34;
        fmVarArr[i45] = a34;
        int i46 = i45 + 1;
        C2491fm a35 = C4105zY.m41624a(Corporation.class, "caace9fdd377a37d159732d7a3b8c5cb", i46);
        aKb = a35;
        fmVarArr[i46] = a35;
        int i47 = i46 + 1;
        C2491fm a36 = C4105zY.m41624a(Corporation.class, "56c7ed5415e00e6a0172e9f0327fe63b", i47);
        aKc = a36;
        fmVarArr[i47] = a36;
        int i48 = i47 + 1;
        C2491fm a37 = C4105zY.m41624a(Corporation.class, "04823bd5a8b92a17c41d9cb0a95eee94", i48);
        aKd = a37;
        fmVarArr[i48] = a37;
        int i49 = i48 + 1;
        C2491fm a38 = C4105zY.m41624a(Corporation.class, "fc61e6246a7b468d78c37d99da660945", i49);
        aKe = a38;
        fmVarArr[i49] = a38;
        int i50 = i49 + 1;
        C2491fm a39 = C4105zY.m41624a(Corporation.class, "fb72a71b91b9426e02ce7422d4b4752c", i50);
        aKf = a39;
        fmVarArr[i50] = a39;
        int i51 = i50 + 1;
        C2491fm a40 = C4105zY.m41624a(Corporation.class, "67ddf3cc0dfb5c2219cda31114f6dab1", i51);
        aKg = a40;
        fmVarArr[i51] = a40;
        int i52 = i51 + 1;
        C2491fm a41 = C4105zY.m41624a(Corporation.class, "7a7bcf915c154940c3ddc8a18488ff33", i52);
        aKh = a41;
        fmVarArr[i52] = a41;
        int i53 = i52 + 1;
        C2491fm a42 = C4105zY.m41624a(Corporation.class, "2779391ef2be1aa74c5650a1a4ac17ed", i53);
        aKi = a42;
        fmVarArr[i53] = a42;
        int i54 = i53 + 1;
        C2491fm a43 = C4105zY.m41624a(Corporation.class, "0c2ab58eb50d7d0d8bcfe0c0d53e0f79", i54);
        aKj = a43;
        fmVarArr[i54] = a43;
        int i55 = i54 + 1;
        C2491fm a44 = C4105zY.m41624a(Corporation.class, "087d016b8f70f3d5096302a14b4f8e9d", i55);
        aKk = a44;
        fmVarArr[i55] = a44;
        int i56 = i55 + 1;
        C2491fm a45 = C4105zY.m41624a(Corporation.class, "fce9e57f69afa174a0a3781811d1aa34", i56);
        aKl = a45;
        fmVarArr[i56] = a45;
        int i57 = i56 + 1;
        C2491fm a46 = C4105zY.m41624a(Corporation.class, "69b313a92d93dcb1e6c9f50c2f2a9b32", i57);
        f3495Lm = a46;
        fmVarArr[i57] = a46;
        int i58 = i57 + 1;
        C2491fm a47 = C4105zY.m41624a(Corporation.class, "e7019f40ebf5b8c091dd8610d6a0b323", i58);
        aKm = a47;
        fmVarArr[i58] = a47;
        int i59 = i58 + 1;
        C2491fm a48 = C4105zY.m41624a(Corporation.class, "eb60092480faa6066b804f77f022d678", i59);
        aKn = a48;
        fmVarArr[i59] = a48;
        int i60 = i59 + 1;
        C2491fm a49 = C4105zY.m41624a(Corporation.class, "cbc45c15005b0ddf7b3c1d48aa1b4c1d", i60);
        _f_hasHollowField_0020_0028_0029Z = a49;
        fmVarArr[i60] = a49;
        int i61 = i60 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Corporation.class, C5904acg.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    public static int m17016L(Player aku) {
        if (aku.ala().aLS().ado()) {
            return 2;
        }
        return 5;
    }

    @C0064Am(aul = "dc926fccb02c4639be7156707d36f388", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: B */
    private boolean m17011B(Player aku) {
        throw new aWi(new aCE(this, aJN, new Object[]{aku}));
    }

    @C0064Am(aul = "2779391ef2be1aa74c5650a1a4ac17ed", aum = 0)
    @C5566aOg
    /* renamed from: J */
    private void m17015J(Player aku) {
        throw new aWi(new aCE(this, aKi, new Object[]{aku}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "7804c3ae9c7fb9dcebaf6b6411782a20", aum = 0)
    @C2499fr
    /* renamed from: QH */
    private void m17021QH() {
        throw new aWi(new aCE(this, aJX, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "c274121b6653b92d058977f4131554df", aum = 0)
    @C2499fr
    /* renamed from: QJ */
    private void m17022QJ() {
        throw new aWi(new aCE(this, aJZ, new Object[0]));
    }

    /* renamed from: QM */
    private void m17024QM() {
        switch (bFf().mo6893i(aKj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKj, new Object[0]));
                break;
        }
        m17023QL();
    }

    @C0064Am(aul = "087d016b8f70f3d5096302a14b4f8e9d", aum = 0)
    @C5566aOg
    /* renamed from: QN */
    private void m17025QN() {
        throw new aWi(new aCE(this, aKk, new Object[0]));
    }

    @C5566aOg
    /* renamed from: QO */
    private void m17026QO() {
        switch (bFf().mo6893i(aKk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKk, new Object[0]));
                break;
        }
        m17025QN();
    }

    @C0064Am(aul = "e7019f40ebf5b8c091dd8610d6a0b323", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m17028QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    @C0064Am(aul = "eb60092480faa6066b804f77f022d678", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: QT */
    private void m17029QT() {
        throw new aWi(new aCE(this, aKn, new Object[0]));
    }

    /* renamed from: Qj */
    private String m17031Qj() {
        return (String) bFf().mo5608dq().mo3214p(f3497QA);
    }

    /* access modifiers changed from: private */
    /* renamed from: Qk */
    public C1556Wo m17032Qk() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(aJl);
    }

    /* renamed from: Ql */
    private C1556Wo m17033Ql() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(aJn);
    }

    /* renamed from: Qm */
    private CorporationLogo m17034Qm() {
        return (CorporationLogo) bFf().mo5608dq().mo3214p(aJp);
    }

    /* renamed from: Qn */
    private String m17035Qn() {
        return (String) bFf().mo5608dq().mo3214p(f3494GZ);
    }

    /* renamed from: Qo */
    private BankAccount m17036Qo() {
        return (BankAccount) bFf().mo5608dq().mo3214p(aJs);
    }

    /* renamed from: Qp */
    private C2686iZ m17037Qp() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(aJu);
    }

    /* access modifiers changed from: private */
    /* renamed from: Qq */
    public PlayerDisposedListener m17038Qq() {
        return (PlayerDisposedListener) bFf().mo5608dq().mo3214p(aJw);
    }

    /* renamed from: a */
    private void m17046a(CorporationLogo ec) {
        bFf().mo5608dq().mo3197f(aJp, ec);
    }

    /* renamed from: a */
    private void m17047a(PlayerDisposedListener aVar) {
        bFf().mo5608dq().mo3197f(aJw, aVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "56c7ed5415e00e6a0172e9f0327fe63b", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m17048a(C5741aUz auz, C6704asA asa, boolean z) {
        throw new aWi(new aCE(this, aKc, new Object[]{auz, asa, new Boolean(z)}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "dce2965ff4406593ad356aba3f251d36", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m17049a(C5741aUz auz, String str) {
        throw new aWi(new aCE(this, aKa, new Object[]{auz, str}));
    }

    /* renamed from: a */
    private void m17050a(BankAccount ado) {
        bFf().mo5608dq().mo3197f(aJs, ado);
    }

    @C0064Am(aul = "ba50d99de36d286b3936642f675c07dc", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m17052a(Outpost qZVar) {
        throw new aWi(new aCE(this, aJD, new Object[]{qZVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "bc3c8ea3f726cff11540e9335e103c86", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m17053a(String str, Player aku) {
        throw new aWi(new aCE(this, aJV, new Object[]{str, aku}));
    }

    @C0064Am(aul = "24bf2901c55f4967d8bdae2f9aae3f2d", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m17054a(Player aku, Player aku2) {
        throw new aWi(new aCE(this, aJJ, new Object[]{aku, aku2}));
    }

    @C0064Am(aul = "45e164c42354cc2f7ae5efd74c7f39e3", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private boolean m17056a(Player aku, String str) {
        throw new aWi(new aCE(this, aJK, new Object[]{aku, str}));
    }

    @C0064Am(aul = "2d45d8af68999815c7ef9053a987d8c7", aum = 0)
    @C5566aOg
    /* renamed from: aG */
    private void m17058aG(boolean z) {
        throw new aWi(new aCE(this, aJW, new Object[]{new Boolean(z)}));
    }

    @C5566aOg
    /* renamed from: aH */
    private void m17059aH(boolean z) {
        switch (bFf().mo6893i(aJW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJW, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJW, new Object[]{new Boolean(z)}));
                break;
        }
        m17058aG(z);
    }

    @C0064Am(aul = "343b7cdf237333a5e89b886f10a7b4e6", aum = 0)
    @C5566aOg
    /* renamed from: aI */
    private void m17060aI(boolean z) {
        throw new aWi(new aCE(this, aJY, new Object[]{new Boolean(z)}));
    }

    @C5566aOg
    /* renamed from: aJ */
    private void m17061aJ(boolean z) {
        switch (bFf().mo6893i(aJY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJY, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJY, new Object[]{new Boolean(z)}));
                break;
        }
        m17060aI(z);
    }

    /* renamed from: aM */
    private void m17062aM(String str) {
        bFf().mo5608dq().mo3197f(f3497QA, str);
    }

    /* renamed from: aN */
    private void m17063aN(String str) {
        bFf().mo5608dq().mo3197f(f3494GZ, str);
    }

    @C0064Am(aul = "25d115a2300dabd7c8278c15ff0f2587", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: aO */
    private boolean m17064aO(String str) {
        throw new aWi(new aCE(this, aJI, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "1ffcc5042546c13842066a610aa6a331", aum = 0)
    @C2499fr
    /* renamed from: aQ */
    private void m17065aQ(String str) {
        throw new aWi(new aCE(this, aJO, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "48e3c1821b72ae655c0b0d516d769647", aum = 0)
    @C2499fr
    /* renamed from: aR */
    private void m17066aR(String str) {
        throw new aWi(new aCE(this, aJQ, new Object[]{str}));
    }

    /* renamed from: ap */
    private void m17067ap(String str) {
        bFf().mo5608dq().mo3197f(f3501zQ, str);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "caace9fdd377a37d159732d7a3b8c5cb", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m17069c(C5741aUz auz, String str) {
        throw new aWi(new aCE(this, aKb, new Object[]{auz, str}));
    }

    /* renamed from: c */
    private void m17070c(Player aku) {
        bFf().mo5608dq().mo3197f(f3500mv, aku);
    }

    @C0064Am(aul = "7a2d87349d8be7fa44930ffdacd679b7", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m17071c(Outpost qZVar) {
        throw new aWi(new aCE(this, aJE, new Object[]{qZVar}));
    }

    @C0064Am(aul = "958b903e09f7790f1b17c11a1b8d2d41", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private boolean m17072c(Player aku, Player aku2) {
        throw new aWi(new aCE(this, aJL, new Object[]{aku, aku2}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "7a7bcf915c154940c3ddc8a18488ff33", aum = 0)
    @C2499fr
    /* renamed from: cA */
    private void m17073cA(long j) {
        throw new aWi(new aCE(this, aKh, new Object[]{new Long(j)}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "67ddf3cc0dfb5c2219cda31114f6dab1", aum = 0)
    @C2499fr
    /* renamed from: cy */
    private void m17076cy(long j) {
        throw new aWi(new aCE(this, aKg, new Object[]{new Long(j)}));
    }

    /* renamed from: fZ */
    private Player m17078fZ() {
        return (Player) bFf().mo5608dq().mo3214p(f3500mv);
    }

    /* renamed from: k */
    private void m17080k(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(aJl, wo);
    }

    /* renamed from: k */
    private void m17081k(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(aJu, iZVar);
    }

    /* renamed from: l */
    private void m17082l(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(aJn, wo);
    }

    @C0064Am(aul = "69b313a92d93dcb1e6c9f50c2f2a9b32", aum = 0)
    @C5566aOg
    @Deprecated
    /* renamed from: qU */
    private void m17084qU() {
        throw new aWi(new aCE(this, f3495Lm, new Object[0]));
    }

    /* renamed from: uU */
    private String m17085uU() {
        return (String) bFf().mo5608dq().mo3214p(f3501zQ);
    }

    @C0064Am(aul = "7609925cd25b4d09d83f76048a3b01e5", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: z */
    private boolean m17087z(Player aku) {
        throw new aWi(new aCE(this, aJM, new Object[]{aku}));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: A */
    public boolean mo10691A(Player aku) {
        switch (bFf().mo6893i(aJM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJM, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJM, new Object[]{aku}));
                break;
        }
        return m17087z(aku);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: C */
    public boolean mo10692C(Player aku) {
        switch (bFf().mo6893i(aJN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJN, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJN, new Object[]{aku}));
                break;
        }
        return m17011B(aku);
    }

    /* renamed from: E */
    public boolean mo10693E(Player aku) {
        switch (bFf().mo6893i(aJT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJT, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJT, new Object[]{aku}));
                break;
        }
        return m17012D(aku);
    }

    /* renamed from: G */
    public boolean mo10694G(Player aku) {
        switch (bFf().mo6893i(aJU)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJU, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJU, new Object[]{aku}));
                break;
        }
        return m17013F(aku);
    }

    /* renamed from: I */
    public CorporationRoleInfo mo10695I(Player aku) {
        switch (bFf().mo6893i(aKd)) {
            case 0:
                return null;
            case 2:
                return (CorporationRoleInfo) bFf().mo5606d(new aCE(this, aKd, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, aKd, new Object[]{aku}));
                break;
        }
        return m17014H(aku);
    }

    @C5566aOg
    /* renamed from: K */
    public void mo10696K(Player aku) {
        switch (bFf().mo6893i(aKi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKi, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKi, new Object[]{aku}));
                break;
        }
        m17015J(aku);
    }

    /* renamed from: QA */
    public Set<Player> mo10697QA() {
        switch (bFf().mo6893i(aJF)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, aJF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJF, new Object[0]));
                break;
        }
        return m17043Qz();
    }

    /* renamed from: QC */
    public Set<CorporationMembership> mo10698QC() {
        switch (bFf().mo6893i(aJG)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, aJG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJG, new Object[0]));
                break;
        }
        return m17017QB();
    }

    /* renamed from: QG */
    public String mo10699QG() {
        switch (bFf().mo6893i(aJR)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aJR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJR, new Object[0]));
                break;
        }
        return m17020QF();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: QI */
    public void mo10700QI() {
        switch (bFf().mo6893i(aJX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJX, new Object[0]));
                break;
        }
        m17021QH();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: QK */
    public void mo10701QK() {
        switch (bFf().mo6893i(aJZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJZ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJZ, new Object[0]));
                break;
        }
        m17022QJ();
    }

    /* renamed from: QQ */
    public int mo10702QQ() {
        switch (bFf().mo6893i(aKl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aKl, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aKl, new Object[0]));
                break;
        }
        return m17027QP();
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo10703QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m17028QR();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: QU */
    public void mo10704QU() {
        switch (bFf().mo6893i(aKn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKn, new Object[0]));
                break;
        }
        m17029QT();
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m17030QV();
    }

    /* renamed from: Qs */
    public Player mo10705Qs() {
        switch (bFf().mo6893i(aJx)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, aJx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJx, new Object[0]));
                break;
        }
        return m17039Qr();
    }

    /* renamed from: Qu */
    public CorporationLogo mo10706Qu() {
        switch (bFf().mo6893i(aJy)) {
            case 0:
                return null;
            case 2:
                return (CorporationLogo) bFf().mo5606d(new aCE(this, aJy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJy, new Object[0]));
                break;
        }
        return m17040Qt();
    }

    /* renamed from: Qw */
    public BankAccount mo5156Qw() {
        switch (bFf().mo6893i(aJz)) {
            case 0:
                return null;
            case 2:
                return (BankAccount) bFf().mo5606d(new aCE(this, aJz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJz, new Object[0]));
                break;
        }
        return m17041Qv();
    }

    /* renamed from: Qy */
    public Set<Outpost> mo10707Qy() {
        switch (bFf().mo6893i(aJC)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, aJC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJC, new Object[0]));
                break;
        }
        return m17042Qx();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5904acg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m17079fg();
                return null;
            case 1:
                m17057aG();
                return null;
            case 2:
                return m17039Qr();
            case 3:
                return m17086uV();
            case 4:
                return m17040Qt();
            case 5:
                return m17041Qv();
            case 6:
                m17074cu(((Long) args[0]).longValue());
                return null;
            case 7:
                m17075cw(((Long) args[0]).longValue());
                return null;
            case 8:
                return m17042Qx();
            case 9:
                m17052a((Outpost) args[0]);
                return null;
            case 10:
                m17071c((Outpost) args[0]);
                return null;
            case 11:
                return m17043Qz();
            case 12:
                return m17017QB();
            case 13:
                return new Integer(m17018QD());
            case 14:
                return new Boolean(m17064aO((String) args[0]));
            case 15:
                return new Boolean(m17054a((Player) args[0], (Player) args[1]));
            case 16:
                return new Boolean(m17056a((Player) args[0], (String) args[1]));
            case 17:
                return new Boolean(m17072c((Player) args[0], (Player) args[1]));
            case 18:
                return new Boolean(m17087z((Player) args[0]));
            case 19:
                return new Boolean(m17011B((Player) args[0]));
            case 20:
                m17065aQ((String) args[0]);
                return null;
            case 21:
                return m17019QE();
            case 22:
                m17066aR((String) args[0]);
                return null;
            case 23:
                return m17020QF();
            case 24:
                m17051a((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 25:
                return new Boolean(m17083l((Player) args[0]));
            case 26:
                return new Boolean(m17012D((Player) args[0]));
            case 27:
                return new Boolean(m17013F((Player) args[0]));
            case 28:
                m17053a((String) args[0], (Player) args[1]);
                return null;
            case 29:
                m17058aG(((Boolean) args[0]).booleanValue());
                return null;
            case 30:
                m17021QH();
                return null;
            case 31:
                m17060aI(((Boolean) args[0]).booleanValue());
                return null;
            case 32:
                m17022QJ();
                return null;
            case 33:
                m17049a((C5741aUz) args[0], (String) args[1]);
                return null;
            case 34:
                m17069c((C5741aUz) args[0], (String) args[1]);
                return null;
            case 35:
                m17048a((C5741aUz) args[0], (C6704asA) args[1], ((Boolean) args[2]).booleanValue());
                return null;
            case 36:
                return m17014H((Player) args[0]);
            case 37:
                return m17045a((C5741aUz) args[0]);
            case 38:
                return new Boolean(m17055a((Player) args[0], (C6704asA) args[1]));
            case 39:
                m17076cy(((Long) args[0]).longValue());
                return null;
            case 40:
                m17073cA(((Long) args[0]).longValue());
                return null;
            case 41:
                m17015J((Player) args[0]);
                return null;
            case 42:
                m17023QL();
                return null;
            case 43:
                m17025QN();
                return null;
            case 44:
                return new Integer(m17027QP());
            case 45:
                m17084qU();
                return null;
            case 46:
                m17028QR();
                return null;
            case 47:
                m17029QT();
                return null;
            case 48:
                return new Boolean(m17030QV());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m17057aG();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: aP */
    public boolean mo10709aP(String str) {
        switch (bFf().mo6893i(aJI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJI, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJI, new Object[]{str}));
                break;
        }
        return m17064aO(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: aS */
    public void mo10710aS(String str) {
        switch (bFf().mo6893i(aJQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJQ, new Object[]{str}));
                break;
        }
        m17066aR(str);
    }

    /* renamed from: b */
    public CorporationRoleInfo mo10711b(C5741aUz auz) {
        switch (bFf().mo6893i(aKe)) {
            case 0:
                return null;
            case 2:
                return (CorporationRoleInfo) bFf().mo5606d(new aCE(this, aKe, new Object[]{auz}));
            case 3:
                bFf().mo5606d(new aCE(this, aKe, new Object[]{auz}));
                break;
        }
        return m17045a(auz);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo10712b(C5741aUz auz, C6704asA asa, boolean z) {
        switch (bFf().mo6893i(aKc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKc, new Object[]{auz, asa, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKc, new Object[]{auz, asa, new Boolean(z)}));
                break;
        }
        m17048a(auz, asa, z);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo10713b(C5741aUz auz, String str) {
        switch (bFf().mo6893i(aKa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKa, new Object[]{auz, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKa, new Object[]{auz, str}));
                break;
        }
        m17049a(auz, str);
    }

    /* renamed from: b */
    public void mo10714b(Player aku, boolean z) {
        switch (bFf().mo6893i(aJS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJS, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJS, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m17051a(aku, z);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo10715b(Outpost qZVar) {
        switch (bFf().mo6893i(aJD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJD, new Object[]{qZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJD, new Object[]{qZVar}));
                break;
        }
        m17052a(qZVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo10716b(String str, Player aku) {
        switch (bFf().mo6893i(aJV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJV, new Object[]{str, aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJV, new Object[]{str, aku}));
                break;
        }
        m17053a(str, aku);
    }

    @C5566aOg
    /* renamed from: b */
    public boolean mo10717b(Player aku, Player aku2) {
        switch (bFf().mo6893i(aJJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJJ, new Object[]{aku, aku2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJJ, new Object[]{aku, aku2}));
                break;
        }
        return m17054a(aku, aku2);
    }

    /* renamed from: b */
    public boolean mo10718b(Player aku, C6704asA asa) {
        switch (bFf().mo6893i(aKf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aKf, new Object[]{aku, asa}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aKf, new Object[]{aku, asa}));
                break;
        }
        return m17055a(aku, asa);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public boolean mo10719b(Player aku, String str) {
        switch (bFf().mo6893i(aJK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJK, new Object[]{aku, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJK, new Object[]{aku, str}));
                break;
        }
        return m17056a(aku, str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: cB */
    public void mo10720cB(long j) {
        switch (bFf().mo6893i(aKh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKh, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKh, new Object[]{new Long(j)}));
                break;
        }
        m17073cA(j);
    }

    /* renamed from: cv */
    public void mo5157cv(long j) {
        switch (bFf().mo6893i(aJA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJA, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJA, new Object[]{new Long(j)}));
                break;
        }
        m17074cu(j);
    }

    /* renamed from: cx */
    public void mo5158cx(long j) {
        switch (bFf().mo6893i(aJB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJB, new Object[]{new Long(j)}));
                break;
        }
        m17075cw(j);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: cz */
    public void mo10721cz(long j) {
        switch (bFf().mo6893i(aKg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKg, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKg, new Object[]{new Long(j)}));
                break;
        }
        m17076cy(j);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo10722d(C5741aUz auz, String str) {
        switch (bFf().mo6893i(aKb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKb, new Object[]{auz, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKb, new Object[]{auz, str}));
                break;
        }
        m17069c(auz, str);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo10723d(Outpost qZVar) {
        switch (bFf().mo6893i(aJE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJE, new Object[]{qZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJE, new Object[]{qZVar}));
                break;
        }
        m17071c(qZVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public boolean mo10724d(Player aku, Player aku2) {
        switch (bFf().mo6893i(aJL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aJL, new Object[]{aku, aku2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJL, new Object[]{aku, aku2}));
                break;
        }
        return m17072c(aku, aku2);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m17079fg();
    }

    public String getDescription() {
        switch (bFf().mo6893i(aJP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aJP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJP, new Object[0]));
                break;
        }
        return m17019QE();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void setDescription(String str) {
        switch (bFf().mo6893i(aJO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJO, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJO, new Object[]{str}));
                break;
        }
        m17065aQ(str);
    }

    public String getName() {
        switch (bFf().mo6893i(f3496Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3496Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3496Pf, new Object[0]));
                break;
        }
        return m17086uV();
    }

    /* renamed from: m */
    public boolean mo10726m(Player aku) {
        switch (bFf().mo6893i(f3498mJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f3498mJ, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3498mJ, new Object[]{aku}));
                break;
        }
        return m17083l(aku);
    }

    @C5566aOg
    @Deprecated
    /* renamed from: qV */
    public void mo10727qV() {
        switch (bFf().mo6893i(f3495Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3495Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3495Lm, new Object[0]));
                break;
        }
        m17084qU();
    }

    public int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m17018QD();
    }

    /* renamed from: a */
    public void mo10708a(String str, Player aku, List<Player> list, Asset tCVar) {
        super.mo10S();
        m17067ap(String.valueOf(aku.bOx().cEb() ? "@" : "") + str);
        m17062aM("");
        m17070c(aku);
        CorporationLogo ec = (CorporationLogo) bFf().mo6865M(CorporationLogo.class);
        ec.mo1807am(tCVar);
        m17046a(ec);
        BankAccount ado = (BankAccount) bFf().mo6865M(BankAccount.class);
        ado.mo12834gR(0);
        m17050a(ado);
        PlayerDisposedListener aVar = (PlayerDisposedListener) bFf().mo6865M(PlayerDisposedListener.class);
        aVar.mo10730b(this);
        m17047a(aVar);
        m17078fZ().mo14437p(this);
        CorporationMembership azk = (CorporationMembership) bFf().mo6865M(CorporationMembership.class);
        azk.mo17167a(aku, C5741aUz.LEVEL_5);
        m17032Qk().put(m17078fZ(), azk);
        aku.mo8348d(C3161oY.C3162a.class, m17038Qq());
        for (Player next : list) {
            next.mo14437p(this);
            CorporationMembership azk2 = (CorporationMembership) bFf().mo6865M(CorporationMembership.class);
            azk2.mo17167a(next, C5741aUz.LEVEL_1);
            m17032Qk().put(next, azk2);
            next.mo8348d(C3161oY.C3162a.class, m17038Qq());
        }
        ala().mo1503f(this);
        for (C5741aUz auz : C5741aUz.values()) {
            CorporationRoleInfo awt = (CorporationRoleInfo) bFf().mo6865M(CorporationRoleInfo.class);
            awt.mo12094f(auz);
            m17033Ql().put(auz, awt);
        }
        mo10700QI();
        mo10701QK();
        mo10705Qs().dxy().mo6853hC(m17085uU());
        mo10705Qs().dxy().mo6850d(m17034Qm());
    }

    @C0064Am(aul = "712f10dd0c4ea326b27ed262fa6faa9f", aum = 0)
    /* renamed from: fg */
    private void m17079fg() {
        Iterator it = new ArrayList(m17032Qk().keySet()).iterator();
        while (it.hasNext()) {
            Player aku = (Player) it.next();
            aku.mo14374d(true, false);
            aku.mo8355h(C3161oY.C3162a.class, m17038Qq());
        }
        if (!ala().mo1523h(this)) {
            mo6317hy("Corporation " + getName() + " is being destroyed but it hasn't been in Taikodom's list");
        }
        for (Outpost bYb : new HashSet(m17037Qp())) {
            bYb.bYb();
        }
        m17038Qq().dispose();
        super.dispose();
    }

    @C0064Am(aul = "d64b0746398c5a8b9eb7ab140807bc82", aum = 0)
    /* renamed from: aG */
    private void m17057aG() {
        for (C5741aUz auz : C5741aUz.values()) {
            if (!m17033Ql().containsKey(auz)) {
                CorporationRoleInfo awt = (CorporationRoleInfo) bFf().mo6865M(CorporationRoleInfo.class);
                awt.mo12094f(auz);
                m17033Ql().put(auz, awt);
            }
        }
        super.mo70aH();
    }

    @C0064Am(aul = "2050a1e4c1795d36ba1d717922f55579", aum = 0)
    /* renamed from: Qr */
    private Player m17039Qr() {
        return m17078fZ();
    }

    @C0064Am(aul = "80a7b200716ff8141e1225de50030849", aum = 0)
    /* renamed from: uV */
    private String m17086uV() {
        return m17085uU();
    }

    @C0064Am(aul = "f3437c6886f776ca26e52806e37516cc", aum = 0)
    /* renamed from: Qt */
    private CorporationLogo m17040Qt() {
        return m17034Qm();
    }

    @C0064Am(aul = "9359fbd47a22da351c51786f885bc140", aum = 0)
    /* renamed from: Qv */
    private BankAccount m17041Qv() {
        return m17036Qo();
    }

    @C0064Am(aul = "37e040480e4819d16be3c89e94a116e6", aum = 0)
    /* renamed from: cu */
    private void m17074cu(long j) {
    }

    @C0064Am(aul = "5884495fa968165805e2f0da3dbcdc33", aum = 0)
    /* renamed from: cw */
    private void m17075cw(long j) {
    }

    @C0064Am(aul = "584b4f82b5463e464a3014326a7acffc", aum = 0)
    /* renamed from: Qx */
    private Set<Outpost> m17042Qx() {
        return MessageContainer.m16003v(m17037Qp());
    }

    @C0064Am(aul = "666b05633fb2e1318aee9ddc0293b161", aum = 0)
    /* renamed from: Qz */
    private Set<Player> m17043Qz() {
        return MessageContainer.m16003v(m17032Qk().keySet());
    }

    @C0064Am(aul = "0a81e2961290092d0185d887f935bcc4", aum = 0)
    /* renamed from: QB */
    private Set<CorporationMembership> m17017QB() {
        return MessageContainer.m16003v(m17032Qk().values());
    }

    @C0064Am(aul = "92f4f6a12a5bcb1b347de6aa721a15b6", aum = 0)
    /* renamed from: QD */
    private int m17018QD() {
        return m17032Qk().size();
    }

    @C0064Am(aul = "3e16f17227b438e521113493daf55ea9", aum = 0)
    /* renamed from: QE */
    private String m17019QE() {
        return m17031Qj();
    }

    @C0064Am(aul = "dca1c75e88720429db5604afed61e2d7", aum = 0)
    /* renamed from: QF */
    private String m17020QF() {
        return m17035Qn();
    }

    @C0064Am(aul = "f7211f4a83d6c86c8fa1799be3d38950", aum = 0)
    /* renamed from: a */
    private void m17051a(Player aku, boolean z) {
        if (m17032Qk().containsKey(aku)) {
            m17032Qk().remove(aku);
            if (z) {
                NLSCorporation eXVar = (NLSCorporation) ala().aIY().mo6310c(NLSManager.C1472a.CORPORATION);
                for (Player aku2 : m17032Qk().keySet()) {
                    aku2.mo14419f((C1506WA) new C0352Eg(aku2, eXVar.mo18098or(), aku.getName(), getName()));
                }
            }
        }
    }

    @C0064Am(aul = "5386a97597086f00703ba782c46bd640", aum = 0)
    /* renamed from: l */
    private boolean m17083l(Player aku) {
        return m17078fZ() == aku;
    }

    @C0064Am(aul = "5a1545f666aff5cb7d872c804d5b4126", aum = 0)
    /* renamed from: D */
    private boolean m17012D(Player aku) {
        return m17032Qk().containsKey(aku) && !mo10726m(aku);
    }

    @C0064Am(aul = "279b7a5c166afc953115d401bfdbe8a0", aum = 0)
    /* renamed from: F */
    private boolean m17013F(Player aku) {
        return m17032Qk().containsKey(aku);
    }

    @C0064Am(aul = "04823bd5a8b92a17c41d9cb0a95eee94", aum = 0)
    /* renamed from: H */
    private CorporationRoleInfo m17014H(Player aku) {
        if (m17032Qk().containsKey(aku)) {
            return (CorporationRoleInfo) m17033Ql().get(((CorporationMembership) m17032Qk().get(aku)).cyU());
        }
        throw new IllegalArgumentException("Player '" + aku.getName() + "' does not belong to Corporation '" + m17085uU() + "'");
    }

    @C0064Am(aul = "fc61e6246a7b468d78c37d99da660945", aum = 0)
    /* renamed from: a */
    private CorporationRoleInfo m17045a(C5741aUz auz) {
        return (CorporationRoleInfo) m17033Ql().get(auz);
    }

    @C0064Am(aul = "fb72a71b91b9426e02ce7422d4b4752c", aum = 0)
    /* renamed from: a */
    private boolean m17055a(Player aku, C6704asA asa) {
        CorporationMembership azk = (CorporationMembership) m17032Qk().get(aku);
        if (azk == null) {
            return false;
        }
        return ((CorporationRoleInfo) m17033Ql().get(azk.cyU())).mo12095f(asa);
    }

    @C0064Am(aul = "0c2ab58eb50d7d0d8bcfe0c0d53e0f79", aum = 0)
    /* renamed from: QL */
    private void m17023QL() {
        CorporationMembership azk = (CorporationMembership) m17032Qk().get(m17078fZ());
        if (azk.cGG() > 30) {
            ArrayList arrayList = new ArrayList(m17032Qk().values());
            arrayList.remove(azk);
            if (arrayList.size() != 0) {
                Collections.sort(arrayList, new C1816b());
                CorporationMembership azk2 = (CorporationMembership) arrayList.get(0);
                azk2.cGM();
                azk.cGO();
                m17070c(azk2.mo17177dL());
            }
        }
    }

    @C0064Am(aul = "fce9e57f69afa174a0a3781811d1aa34", aum = 0)
    /* renamed from: QP */
    private int m17027QP() {
        int awf = ala().aJe().mo19009xb().awf();
        Iterator it = m17037Qp().iterator();
        while (true) {
            int i = awf;
            if (!it.hasNext()) {
                return i;
            }
            awf = ((Outpost) it.next()).bYf() + i;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x00fa  */
    @p001a.C0064Am(aul = "cbc45c15005b0ddf7b3c1d48aa1b4c1d", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m17030QV() {
        /*
            r4 = this;
            r1 = 1
            boolean r0 = super.mo961QW()
            if (r0 == 0) goto L_0x0009
            r0 = r1
        L_0x0008:
            return r0
        L_0x0009:
            a.EC r0 = r4.mo10706Qu()
            if (r0 == 0) goto L_0x001f
            a.EC r0 = r4.mo10706Qu()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x001f
            r0 = r1
            goto L_0x0008
        L_0x001f:
            a.adO r0 = r4.mo5156Qw()
            if (r0 == 0) goto L_0x0035
            a.adO r0 = r4.mo5156Qw()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x0035
            r0 = r1
            goto L_0x0008
        L_0x0035:
            a.Wo r0 = r4.m17033Ql()
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x0041:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x0092
            a.DN r0 = r4.ala()
            a.gV r0 = r0.aJe()
            a.AB r2 = r0.mo19009xb()
            java.util.List r0 = r2.awb()
            java.util.Iterator r3 = r0.iterator()
        L_0x005b:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x00a5
            java.util.List r0 = r2.awd()
            java.util.Iterator r2 = r0.iterator()
        L_0x0069:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x00b8
            a.iZ r0 = r4.m17037Qp()
            java.util.Iterator r2 = r0.iterator()
        L_0x0077:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x00cb
            a.Wo r0 = r4.m17032Qk()
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x0089:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x00fa
            r0 = 0
            goto L_0x0008
        L_0x0092:
            java.lang.Object r0 = r2.next()
            a.aWt r0 = (p001a.aWt) r0
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x0041
            r0 = r1
            goto L_0x0008
        L_0x00a5:
            java.lang.Object r0 = r3.next()
            a.avj r0 = (p001a.C6895avj) r0
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x005b
            r0 = r1
            goto L_0x0008
        L_0x00b8:
            java.lang.Object r0 = r2.next()
            a.avv r0 = (p001a.C6907avv) r0
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x0069
            r0 = r1
            goto L_0x0008
        L_0x00cb:
            java.lang.Object r0 = r2.next()
            a.qZ r0 = (p001a.C3347qZ) r0
            a.KZ r0 = r0.bYh()
            a.Wo r0 = r0.beJ()
            java.util.Collection r0 = r0.values()
            java.util.Iterator r3 = r0.iterator()
        L_0x00e1:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0077
            java.lang.Object r0 = r3.next()
            a.An r0 = (p001a.C0065An) r0
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x00e1
            r0 = r1
            goto L_0x0008
        L_0x00fa:
            java.lang.Object r0 = r2.next()
            a.azK r0 = (p001a.azK) r0
            a.TD r3 = p001a.C1298TD.m9830t(r0)
            boolean r3 = r3.bFT()
            if (r3 != 0) goto L_0x0118
            a.akU r0 = r0.mo17177dL()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x0089
        L_0x0118:
            r0 = r1
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1814aP.m17030QV():boolean");
    }

    /* renamed from: a.aP$b */
    /* compiled from: a */
    class C1816b implements Comparator<CorporationMembership> {
        C1816b() {
        }

        /* renamed from: a */
        public int compare(CorporationMembership azk, CorporationMembership azk2) {
            if (azk.cyU() != azk2.cyU()) {
                return azk2.cyU().ordinal() - azk.cyU().ordinal();
            }
            long cGA = azk.cGA();
            long cGA2 = azk2.cGA();
            if (cGA == cGA2) {
                return azk.mo17177dL().getName().compareTo(azk2.mo17177dL().getName());
            }
            if (cGA < cGA2) {
                return -1;
            }
            return cGA == cGA2 ? 0 : 1;
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.aP$a */
    class PlayerDisposedListener extends aDJ implements C1616Xf, C3161oY.C3162a {
        /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
        public static final C2491fm f3502x13860637 = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f3503aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "8a2090c940b6e86dd4de6a235b8f2de3", aum = 0)

        /* renamed from: lJ */
        static /* synthetic */ Corporation f3504lJ;

        static {
            m17136V();
        }

        public PlayerDisposedListener() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public PlayerDisposedListener(C5540aNg ang) {
            super(ang);
        }

        PlayerDisposedListener(Corporation aPVar) {
            super((C5540aNg) null);
            super._m_script_init(aPVar);
        }

        /* renamed from: V */
        static void m17136V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 1;
            _m_methodCount = aDJ._m_methodCount + 1;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(PlayerDisposedListener.class, "8a2090c940b6e86dd4de6a235b8f2de3", i);
            f3503aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i3 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(PlayerDisposedListener.class, "d1ec577b8a99719e7ce5c41adf4fad6c", i3);
            f3502x13860637 = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(PlayerDisposedListener.class, C5607aPv.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m17137a(Corporation aPVar) {
            bFf().mo5608dq().mo3197f(f3503aT, aPVar);
        }

        @C0064Am(aul = "d1ec577b8a99719e7ce5c41adf4fad6c", aum = 0)
        @C5566aOg
        /* renamed from: b */
        private void m17138b(aDJ adj) {
            throw new aWi(new aCE(this, f3502x13860637, new Object[]{adj}));
        }

        /* renamed from: fu */
        private Corporation m17139fu() {
            return (Corporation) bFf().mo5608dq().mo3214p(f3503aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5607aPv(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    m17138b((aDJ) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        @C5566aOg
        /* renamed from: c */
        public void mo98c(aDJ adj) {
            switch (bFf().mo6893i(f3502x13860637)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f3502x13860637, new Object[]{adj}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f3502x13860637, new Object[]{adj}));
                    break;
            }
            m17138b(adj);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo10730b(Corporation aPVar) {
            m17137a(aPVar);
            super.mo10S();
        }
    }
}
