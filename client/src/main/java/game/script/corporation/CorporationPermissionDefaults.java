package game.script.corporation;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5289aDp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.avv  reason: case insensitive filesystem */
/* compiled from: a */
public class CorporationPermissionDefaults extends aDJ implements C0468GU, C1616Xf {

    /* renamed from: QA */
    public static final C5663aRz f5472QA = null;

    /* renamed from: QE */
    public static final C2491fm f5473QE = null;

    /* renamed from: QF */
    public static final C2491fm f5474QF = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5476bL = null;
    /* renamed from: bN */
    public static final C2491fm f5477bN = null;
    /* renamed from: bO */
    public static final C2491fm f5478bO = null;
    /* renamed from: bP */
    public static final C2491fm f5479bP = null;
    public static final C5663aRz gIP = null;
    public static final C2491fm gIQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f5482zQ = null;
    /* renamed from: zT */
    public static final C2491fm f5483zT = null;
    /* renamed from: zU */
    public static final C2491fm f5484zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0fc397812ced210bf6ea0b258849ee77", aum = 3)

    /* renamed from: bK */
    private static UUID f5475bK;
    @C0064Am(aul = "e1d26e0bab4fd120dd3e5c0ca5ad751f", aum = 0)
    private static C6704asA gIO;
    @C0064Am(aul = "fc570ea7c674f16a8f8db2b010d5ecf9", aum = 2)

    /* renamed from: nh */
    private static I18NString f5480nh;
    @C0064Am(aul = "c4c908c10f07f08348912d1389c63e4f", aum = 1)

    /* renamed from: zP */
    private static I18NString f5481zP;

    static {
        m26763V();
    }

    public CorporationPermissionDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CorporationPermissionDefaults(C5540aNg ang) {
        super(ang);
    }

    public CorporationPermissionDefaults(C6704asA asa) {
        super((C5540aNg) null);
        super._m_script_init(asa);
    }

    /* renamed from: V */
    static void m26763V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CorporationPermissionDefaults.class, "e1d26e0bab4fd120dd3e5c0ca5ad751f", i);
        gIP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CorporationPermissionDefaults.class, "c4c908c10f07f08348912d1389c63e4f", i2);
        f5482zQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CorporationPermissionDefaults.class, "fc570ea7c674f16a8f8db2b010d5ecf9", i3);
        f5472QA = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CorporationPermissionDefaults.class, "0fc397812ced210bf6ea0b258849ee77", i4);
        f5476bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(CorporationPermissionDefaults.class, "b6aa0c4d9ef4f0048ebd86f367f1dc95", i6);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CorporationPermissionDefaults.class, "741f2af111d9404795405327c4bac157", i7);
        f5477bN = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CorporationPermissionDefaults.class, "9a4e8048abd9eaf502aa32359dad4806", i8);
        f5478bO = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CorporationPermissionDefaults.class, "bda62a6268c67dd37a570a7501e4b4a1", i9);
        f5479bP = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CorporationPermissionDefaults.class, "e51af671f709055cf05aba6d0cda5b4c", i10);
        f5483zT = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CorporationPermissionDefaults.class, "003119bbeda8242850413bd797e173c7", i11);
        f5484zU = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CorporationPermissionDefaults.class, "0c0d57fd215ddf5273cfa13855fe19a7", i12);
        f5473QE = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CorporationPermissionDefaults.class, "b449bc0f2dce9bc4a204ee4d8274b0b8", i13);
        f5474QF = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CorporationPermissionDefaults.class, "f6609db53d2856396d678dabb5f197c4", i14);
        gIQ = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CorporationPermissionDefaults.class, C5289aDp.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m26764a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5476bL, uuid);
    }

    /* renamed from: an */
    private UUID m26765an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5476bL);
    }

    /* renamed from: br */
    private void m26770br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f5472QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "b449bc0f2dce9bc4a204ee4d8274b0b8", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m26771bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f5474QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m26772c(C6704asA asa) {
        bFf().mo5608dq().mo3197f(gIP, asa);
    }

    /* renamed from: c */
    private void m26773c(UUID uuid) {
        switch (bFf().mo6893i(f5478bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5478bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5478bO, new Object[]{uuid}));
                break;
        }
        m26769b(uuid);
    }

    private C6704asA czf() {
        return (C6704asA) bFf().mo5608dq().mo3214p(gIP);
    }

    /* renamed from: d */
    private void m26774d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f5482zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "003119bbeda8242850413bd797e173c7", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m26775e(I18NString i18NString) {
        throw new aWi(new aCE(this, f5484zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m26776kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f5482zQ);
    }

    /* renamed from: vT */
    private I18NString m26778vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f5472QA);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5289aDp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m26768au();
            case 1:
                return m26766ap();
            case 2:
                m26769b((UUID) args[0]);
                return null;
            case 3:
                return m26767ar();
            case 4:
                return m26777kd();
            case 5:
                m26775e((I18NString) args[0]);
                return null;
            case 6:
                return m26779vV();
            case 7:
                m26771bu((I18NString) args[0]);
                return null;
            case 8:
                return czg();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5477bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5477bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5477bN, new Object[0]));
                break;
        }
        return m26766ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo16641bv(I18NString i18NString) {
        switch (bFf().mo6893i(f5474QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5474QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5474QF, new Object[]{i18NString}));
                break;
        }
        m26771bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Permission")
    public C6704asA czh() {
        switch (bFf().mo6893i(gIQ)) {
            case 0:
                return null;
            case 2:
                return (C6704asA) bFf().mo5606d(new aCE(this, gIQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gIQ, new Object[0]));
                break;
        }
        return czg();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo16644f(I18NString i18NString) {
        switch (bFf().mo6893i(f5484zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5484zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5484zU, new Object[]{i18NString}));
                break;
        }
        m26775e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5479bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5479bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5479bP, new Object[0]));
                break;
        }
        return m26767ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo16645ke() {
        switch (bFf().mo6893i(f5483zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f5483zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5483zT, new Object[0]));
                break;
        }
        return m26777kd();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m26768au();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo16646vW() {
        switch (bFf().mo6893i(f5473QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f5473QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5473QE, new Object[0]));
                break;
        }
        return m26779vV();
    }

    @C0064Am(aul = "b6aa0c4d9ef4f0048ebd86f367f1dc95", aum = 0)
    /* renamed from: au */
    private String m26768au() {
        return czf() + " [" + m26776kb().get() + "]";
    }

    @C0064Am(aul = "741f2af111d9404795405327c4bac157", aum = 0)
    /* renamed from: ap */
    private UUID m26766ap() {
        return m26765an();
    }

    @C0064Am(aul = "9a4e8048abd9eaf502aa32359dad4806", aum = 0)
    /* renamed from: b */
    private void m26769b(UUID uuid) {
        m26764a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "bda62a6268c67dd37a570a7501e4b4a1", aum = 0)
    /* renamed from: ar */
    private String m26767ar() {
        return "corporation_permission_defaults";
    }

    /* renamed from: d */
    public void mo16643d(C6704asA asa) {
        super.mo10S();
        m26772c(asa);
        m26764a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "e51af671f709055cf05aba6d0cda5b4c", aum = 0)
    /* renamed from: kd */
    private I18NString m26777kd() {
        return m26776kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "0c0d57fd215ddf5273cfa13855fe19a7", aum = 0)
    /* renamed from: vV */
    private I18NString m26779vV() {
        return m26778vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Permission")
    @C0064Am(aul = "f6609db53d2856396d678dabb5f197c4", aum = 0)
    private C6704asA czg() {
        return czf();
    }
}
