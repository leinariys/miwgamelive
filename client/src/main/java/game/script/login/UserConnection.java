package game.script.login;

import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.player.Player;
import game.script.player.PlayerSessionLog;
import game.script.player.User;
import logic.WrapRunnable;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.mbean.axN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6968axh
@C5511aMd
@C6485anp
/* renamed from: a.apK  reason: case insensitive filesystem */
/* compiled from: a */
public class UserConnection extends TaikodomObject implements C1616Xf {

    /* renamed from: Ny */
    public static final C2491fm f5092Ny = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aKm = null;
    /* renamed from: gQ */
    public static final C2491fm f5094gQ = null;
    public static final C5663aRz gnj = null;
    public static final C5663aRz gnk = null;
    public static final C2491fm gnm = null;
    public static final C2491fm gnn = null;
    public static final C2491fm gno = null;
    public static final C2491fm gnp = null;
    public static final C2491fm gnq = null;
    public static final C2491fm gnr = null;
    public static final C2491fm gns = null;
    public static final C2491fm gnt = null;
    public static final C2491fm gnu = null;
    /* renamed from: hF */
    public static final C2491fm f5095hF = null;
    /* renamed from: hz */
    public static final C5663aRz f5096hz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6c9bea8c1eb17a7efca25e154039e272", aum = 1)

    /* renamed from: P */
    private static Player f5093P;
    @C0064Am(aul = "118769a95b4be74a7bdf141e22f6d9fa", aum = 0)
    private static User dtL;
    @C0064Am(aul = "e33d291604be449e3a8ae7ca72d2ac68", aum = 2)
    @C5566aOg
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static aDR eIu;

    static {
        m24844V();
    }

    @ClientOnly
    private WrapRunnable gnl;

    public UserConnection() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public UserConnection(User adk, aDR adr) {
        super((C5540aNg) null);
        super._m_script_init(adk, adr);
    }

    public UserConnection(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24844V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 14;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(UserConnection.class, "118769a95b4be74a7bdf141e22f6d9fa", i);
        gnj = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(UserConnection.class, "6c9bea8c1eb17a7efca25e154039e272", i2);
        f5096hz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(UserConnection.class, "e33d291604be449e3a8ae7ca72d2ac68", i3);
        gnk = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 14)];
        C2491fm a = C4105zY.m41624a(UserConnection.class, "9e933ad49d21f76206d1722abad6efff", i5);
        gnm = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(UserConnection.class, "cad66e9cd8992fd1281f2eb205e341da", i6);
        f5095hF = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(UserConnection.class, "ff9195fd058313953c81640b2908810c", i7);
        gnn = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(UserConnection.class, "d5e897f8a48a0cb3e37a42a89608b96a", i8);
        gno = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(UserConnection.class, "11fbe1a5ef980b64f1dd9173631f3772", i9);
        gnp = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(UserConnection.class, "f63ef2bde731ce9c8f8a52db39f732b1", i10);
        f5092Ny = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(UserConnection.class, "a15d6f449ea11ed44572879dc3df56af", i11);
        f5094gQ = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(UserConnection.class, "5d645d1950316e58e28c8342521624dc", i12);
        gnq = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(UserConnection.class, "1dddb7776f1a2b0d3fc72a297f1a7be5", i13);
        gnr = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(UserConnection.class, "9f692d760fac337f4c116239698de34a", i14);
        _f_dispose_0020_0028_0029V = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(UserConnection.class, "2b50736932f27cf3ea98e134c3c6f7af", i15);
        gns = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(UserConnection.class, "960bd8945cd39ba1b8372e6216b35877", i16);
        gnt = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(UserConnection.class, "700a5b66c3c48e029576ae6de1fcc440", i17);
        gnu = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(UserConnection.class, "e1103165034842389bc32afccf9fa363", i18);
        aKm = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(UserConnection.class, axN.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "e1103165034842389bc32afccf9fa363", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m24843QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    /* renamed from: a */
    private void m24845a(Player aku) {
        bFf().mo5608dq().mo3197f(f5096hz, aku);
    }

    @C0064Am(aul = "11fbe1a5ef980b64f1dd9173631f3772", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: bM */
    private void m24847bM(Player aku) {
        throw new aWi(new aCE(this, gnp, new Object[]{aku}));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: bN */
    private void m24848bN(Player aku) {
        switch (bFf().mo6893i(gnp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gnp, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gnp, new Object[]{aku}));
                break;
        }
        m24847bM(aku);
    }

    private User cqe() {
        return (User) bFf().mo5608dq().mo3214p(gnj);
    }

    private aDR cqf() {
        return (aDR) bFf().mo5608dq().mo3214p(gnk);
    }

    @C0064Am(aul = "ff9195fd058313953c81640b2908810c", aum = 0)
    @C5566aOg
    @C2499fr
    private Player cqh() {
        throw new aWi(new aCE(this, gnn, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "5d645d1950316e58e28c8342521624dc", aum = 0)
    @C2499fr
    private void cqj() {
        throw new aWi(new aCE(this, gnq, new Object[0]));
    }

    /* renamed from: dG */
    private Player m24849dG() {
        return (Player) bFf().mo5608dq().mo3214p(f5096hz);
    }

    @C0064Am(aul = "a15d6f449ea11ed44572879dc3df56af", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m24851dd() {
        throw new aWi(new aCE(this, f5094gQ, new Object[0]));
    }

    /* renamed from: l */
    private void m24854l(aDR adr) {
        bFf().mo5608dq().mo3197f(gnk, adr);
    }

    /* renamed from: x */
    private void m24857x(User adk) {
        bFf().mo5608dq().mo3197f(gnj, adk);
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo15380QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m24843QR();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new axN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cqg();
            case 1:
                return m24850dK();
            case 2:
                return cqh();
            case 3:
                m24846bK((Player) args[0]);
                return null;
            case 4:
                m24847bM((Player) args[0]);
                return null;
            case 5:
                m24856o((Player) args[0]);
                return null;
            case 6:
                m24851dd();
                return null;
            case 7:
                cqj();
                return null;
            case 8:
                return cql();
            case 9:
                m24853fg();
                return null;
            case 10:
                return new Boolean(m24852fX(((Boolean) args[0]).booleanValue()));
            case 11:
                return cqm();
            case 12:
                m24855m((aDR) args[0]);
                return null;
            case 13:
                m24843QR();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public aDR bFs() {
        switch (bFf().mo6893i(gnt)) {
            case 0:
                return null;
            case 2:
                return (aDR) bFf().mo5606d(new aCE(this, gnt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gnt, new Object[0]));
                break;
        }
        return cqm();
    }

    @ClientOnly
    /* renamed from: bL */
    public void mo15383bL(Player aku) {
        switch (bFf().mo6893i(gno)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gno, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gno, new Object[]{aku}));
                break;
        }
        m24846bK(aku);
    }

    public ClientConnect bgt() {
        switch (bFf().mo6893i(gnr)) {
            case 0:
                return null;
            case 2:
                return (ClientConnect) bFf().mo5606d(new aCE(this, gnr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gnr, new Object[0]));
                break;
        }
        return cql();
    }

    //bhe
    public User getUser() {
        switch (bFf().mo6893i(gnm)) {
            case 0:
                return null;
            case 2:
                return (User) bFf().mo5606d(new aCE(this, gnm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gnm, new Object[0]));
                break;
        }
        return cqg();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    public Player cqi() {
        switch (bFf().mo6893i(gnn)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, gnn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gnn, new Object[0]));
                break;
        }
        return cqh();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void cqk() {
        switch (bFf().mo6893i(gnq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gnq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gnq, new Object[0]));
                break;
        }
        cqj();
    }

    /* renamed from: dL */
    public Player mo15388dL() {
        switch (bFf().mo6893i(f5095hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f5095hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5095hF, new Object[0]));
                break;
        }
        return m24850dK();
    }

    @C5566aOg
    /* renamed from: de */
    public void mo15389de() {
        switch (bFf().mo6893i(f5094gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5094gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5094gQ, new Object[0]));
                break;
        }
        m24851dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m24853fg();
    }

    /* renamed from: fY */
    public boolean mo15390fY(boolean z) {
        switch (bFf().mo6893i(gns)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gns, new Object[]{new Boolean(z)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gns, new Object[]{new Boolean(z)}));
                break;
        }
        return m24852fX(z);
    }

    /* renamed from: j */
    public void mo15391j(aDR adr) {
        switch (bFf().mo6893i(gnu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gnu, new Object[]{adr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gnu, new Object[]{adr}));
                break;
        }
        m24855m(adr);
    }

    @C2198cg
    /* renamed from: p */
    public void mo15392p(Player aku) {
        switch (bFf().mo6893i(f5092Ny)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5092Ny, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5092Ny, new Object[]{aku}));
                break;
        }
        m24856o(aku);
    }

    /* renamed from: a */
    public void mo15381a(User adk, aDR adr) {
        super.mo10S();
        m24857x(adk);
        mo15391j(adr);
    }

    @C0064Am(aul = "9e933ad49d21f76206d1722abad6efff", aum = 0)
    private User cqg() {
        return cqe();
    }

    @C0064Am(aul = "cad66e9cd8992fd1281f2eb205e341da", aum = 0)
    /* renamed from: dK */
    private Player m24850dK() {
        return m24849dG();
    }

    @C0064Am(aul = "d5e897f8a48a0cb3e37a42a89608b96a", aum = 0)
    @ClientOnly
    /* renamed from: bK */
    private void m24846bK(Player aku) {
        if (this.gnl != null) {
            ald().alf().mo7963a(this.gnl);
        }
        m24848bN(aku);
        this.gnl = new C1958a(aku);
        ald().alf().addTask("Last Alive Time Updater", this.gnl, 60000);
    }

    @C0064Am(aul = "f63ef2bde731ce9c8f8a52db39f732b1", aum = 0)
    @C2198cg
    /* renamed from: o */
    private void m24856o(Player aku) {
        if (!cqe().cSQ().contains(aku)) {
            throw new IllegalArgumentException("Player " + aku + " does not correspond to the user " + cqe().getUsername());
        }
        m24845a(aku);
        m24849dG().dxI();
    }

    @C0064Am(aul = "1dddb7776f1a2b0d3fc72a297f1a7be5", aum = 0)
    private ClientConnect cql() {
        if (bFs() != null) {
            return bFs().bgt();
        }
        return null;
    }

    @C0064Am(aul = "9f692d760fac337f4c116239698de34a", aum = 0)
    /* renamed from: fg */
    private void m24853fg() {
        mo15391j((aDR) null);
        super.dispose();
    }

    @C0064Am(aul = "2b50736932f27cf3ea98e134c3c6f7af", aum = 0)
    /* renamed from: fX */
    private boolean m24852fX(boolean z) {
        PlayerSessionLog.C0024b bVar;
        ClientConnect bgt = bgt();
        if (bgt == null) {
            return false;
        }
        if (mo15388dL() != null) {
            if (z) {
                bVar = PlayerSessionLog.C0024b.SERVER_SHUTDOWN;
            } else {
                bVar = PlayerSessionLog.C0024b.PLAYER_KICKED;
            }
            mo15388dL().dxS().mo210d(bVar);
        }
        bFf().mo6866PM().bGU().authorizationChannelClose(bgt);
        return true;
    }

    @C0064Am(aul = "960bd8945cd39ba1b8372e6216b35877", aum = 0)
    private aDR cqm() {
        return cqf();
    }

    @C0064Am(aul = "700a5b66c3c48e029576ae6de1fcc440", aum = 0)
    /* renamed from: m */
    private void m24855m(aDR adr) {
        m24854l(adr);
    }

    /* renamed from: a.apK$a */
    class C1958a extends WrapRunnable {
        private final /* synthetic */ Player jdn;

        C1958a(Player aku) {
            this.jdn = aku;
        }

        public void run() {
            this.jdn.dxS().cfs();
        }
    }
}
