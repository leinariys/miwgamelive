package game.script.consignment;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6660arI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aAX */
/* compiled from: a */
public class ConsignmentFeeRange extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f2330bL = null;
    /* renamed from: bM */
    public static final C5663aRz f2331bM = null;
    /* renamed from: bN */
    public static final C2491fm f2332bN = null;
    /* renamed from: bO */
    public static final C2491fm f2333bO = null;
    /* renamed from: bP */
    public static final C2491fm f2334bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2335bQ = null;
    public static final C2491fm hfA = null;
    public static final C2491fm hfB = null;
    public static final C2491fm hfC = null;
    public static final C2491fm hfD = null;
    public static final C2491fm hfE = null;
    public static final C5663aRz hfw = null;
    public static final C5663aRz hfx = null;
    public static final C5663aRz hfy = null;
    public static final C2491fm hfz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f87e27a750325dd0301b5104a90abe39", aum = 3)

    /* renamed from: bK */
    private static UUID f2329bK;
    @C0064Am(aul = "b7435afe0de2dd9c942dfa760d7b8753", aum = 0)
    private static long gtg;
    @C0064Am(aul = "2cf1d965eb04dddec53ed18136bd754f", aum = 1)
    private static float gth;
    @C0064Am(aul = "464c8b5948192375ae5d7483361bc07b", aum = 2)
    private static long gti;
    @C0064Am(aul = "cafa6010317e8f158d2bee8e13956153", aum = 4)
    private static String handle;

    static {
        m12577V();
    }

    public ConsignmentFeeRange() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ConsignmentFeeRange(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12577V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ConsignmentFeeRange.class, "b7435afe0de2dd9c942dfa760d7b8753", i);
        hfw = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ConsignmentFeeRange.class, "2cf1d965eb04dddec53ed18136bd754f", i2);
        hfx = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ConsignmentFeeRange.class, "464c8b5948192375ae5d7483361bc07b", i3);
        hfy = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ConsignmentFeeRange.class, "f87e27a750325dd0301b5104a90abe39", i4);
        f2330bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ConsignmentFeeRange.class, "cafa6010317e8f158d2bee8e13956153", i5);
        f2331bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 11)];
        C2491fm a = C4105zY.m41624a(ConsignmentFeeRange.class, "fed880cda34b8a93b8519e80b232a609", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ConsignmentFeeRange.class, "1630e67db766996ca3471b61c1988407", i8);
        f2332bN = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ConsignmentFeeRange.class, "57e95a0b668c3218daa6d1e6afa5025d", i9);
        f2333bO = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ConsignmentFeeRange.class, "26bcae6349f155fe4241e36245e5524a", i10);
        f2334bP = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ConsignmentFeeRange.class, "70efc1ab52352f01b5b65ffe01c68d6b", i11);
        f2335bQ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ConsignmentFeeRange.class, "0828e4a85505c89929d3217f10828acb", i12);
        hfz = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ConsignmentFeeRange.class, "e8904a6aec9c9cfa3fb109f0ba9ecb6e", i13);
        hfA = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ConsignmentFeeRange.class, "18f124091b2ff8e09a49d69f5e0d2107", i14);
        hfB = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ConsignmentFeeRange.class, "53227c4f519595c258730704dabe188d", i15);
        hfC = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ConsignmentFeeRange.class, "f26eb5936ee1d3cc65616eb64ffdd0e9", i16);
        hfD = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(ConsignmentFeeRange.class, "d6b3066292bf94741f22c1a9c320ece7", i17);
        hfE = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ConsignmentFeeRange.class, C6660arI.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m12578a(String str) {
        bFf().mo5608dq().mo3197f(f2331bM, str);
    }

    /* renamed from: a */
    private void m12579a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2330bL, uuid);
    }

    /* renamed from: an */
    private UUID m12580an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2330bL);
    }

    /* renamed from: ao */
    private String m12581ao() {
        return (String) bFf().mo5608dq().mo3214p(f2331bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "70efc1ab52352f01b5b65ffe01c68d6b", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m12585b(String str) {
        throw new aWi(new aCE(this, f2335bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m12587c(UUID uuid) {
        switch (bFf().mo6893i(f2333bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2333bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2333bO, new Object[]{uuid}));
                break;
        }
        m12586b(uuid);
    }

    private long cIJ() {
        return bFf().mo5608dq().mo3213o(hfw);
    }

    private float cIK() {
        return bFf().mo5608dq().mo3211m(hfx);
    }

    private long cIL() {
        return bFf().mo5608dq().mo3213o(hfy);
    }

    /* renamed from: jv */
    private void m12588jv(long j) {
        bFf().mo5608dq().mo3184b(hfw, j);
    }

    /* renamed from: jw */
    private void m12589jw(long j) {
        bFf().mo5608dq().mo3184b(hfy, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lower bound")
    @C0064Am(aul = "e8904a6aec9c9cfa3fb109f0ba9ecb6e", aum = 0)
    @C5566aOg
    /* renamed from: jx */
    private void m12590jx(long j) {
        throw new aWi(new aCE(this, hfA, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min absolute fee")
    @C0064Am(aul = "d6b3066292bf94741f22c1a9c320ece7", aum = 0)
    @C5566aOg
    /* renamed from: jz */
    private void m12591jz(long j) {
        throw new aWi(new aCE(this, hfE, new Object[]{new Long(j)}));
    }

    /* renamed from: lj */
    private void m12592lj(float f) {
        bFf().mo5608dq().mo3150a(hfx, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fee (% of the value)")
    @C0064Am(aul = "53227c4f519595c258730704dabe188d", aum = 0)
    @C5566aOg
    /* renamed from: lk */
    private void m12593lk(float f) {
        throw new aWi(new aCE(this, hfC, new Object[]{new Float(f)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6660arI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m12584au();
            case 1:
                return m12582ap();
            case 2:
                m12586b((UUID) args[0]);
                return null;
            case 3:
                return m12583ar();
            case 4:
                m12585b((String) args[0]);
                return null;
            case 5:
                return new Long(cIM());
            case 6:
                m12590jx(((Long) args[0]).longValue());
                return null;
            case 7:
                return new Float(cIO());
            case 8:
                m12593lk(((Float) args[0]).floatValue());
                return null;
            case 9:
                return new Long(cIQ());
            case 10:
                m12591jz(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2332bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2332bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2332bN, new Object[0]));
                break;
        }
        return m12582ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lower bound")
    public long cIN() {
        switch (bFf().mo6893i(hfz)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hfz, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hfz, new Object[0]));
                break;
        }
        return cIM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fee (% of the value)")
    public float cIP() {
        switch (bFf().mo6893i(hfB)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hfB, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hfB, new Object[0]));
                break;
        }
        return cIO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min absolute fee")
    public long cIR() {
        switch (bFf().mo6893i(hfD)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hfD, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hfD, new Object[0]));
                break;
        }
        return cIQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2334bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2334bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2334bP, new Object[0]));
                break;
        }
        return m12583ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2335bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2335bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2335bQ, new Object[]{str}));
                break;
        }
        m12585b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min absolute fee")
    @C5566aOg
    /* renamed from: jA */
    public void mo7653jA(long j) {
        switch (bFf().mo6893i(hfE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hfE, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hfE, new Object[]{new Long(j)}));
                break;
        }
        m12591jz(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lower bound")
    @C5566aOg
    /* renamed from: jy */
    public void mo7654jy(long j) {
        switch (bFf().mo6893i(hfA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hfA, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hfA, new Object[]{new Long(j)}));
                break;
        }
        m12590jx(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fee (% of the value)")
    @C5566aOg
    /* renamed from: ll */
    public void mo7655ll(float f) {
        switch (bFf().mo6893i(hfC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hfC, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hfC, new Object[]{new Float(f)}));
                break;
        }
        m12593lk(f);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m12584au();
    }

    @C0064Am(aul = "fed880cda34b8a93b8519e80b232a609", aum = 0)
    /* renamed from: au */
    private String m12584au() {
        return "[" + cIJ() + "...], fee [" + cIK() + "%], abs min [" + cIL() + "]";
    }

    @C0064Am(aul = "1630e67db766996ca3471b61c1988407", aum = 0)
    /* renamed from: ap */
    private UUID m12582ap() {
        return m12580an();
    }

    @C0064Am(aul = "57e95a0b668c3218daa6d1e6afa5025d", aum = 0)
    /* renamed from: b */
    private void m12586b(UUID uuid) {
        m12579a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m12579a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "26bcae6349f155fe4241e36245e5524a", aum = 0)
    /* renamed from: ar */
    private String m12583ar() {
        return m12581ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lower bound")
    @C0064Am(aul = "0828e4a85505c89929d3217f10828acb", aum = 0)
    private long cIM() {
        return cIJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fee (% of the value)")
    @C0064Am(aul = "18f124091b2ff8e09a49d69f5e0d2107", aum = 0)
    private float cIO() {
        return cIK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min absolute fee")
    @C0064Am(aul = "f26eb5936ee1d3cc65616eb64ffdd0e9", aum = 0)
    private long cIQ() {
        return cIL();
    }
}
