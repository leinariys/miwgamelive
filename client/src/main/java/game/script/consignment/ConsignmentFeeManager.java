package game.script.consignment;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.*;
import logic.data.mbean.C4056yf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.au */
/* compiled from: a */
public class ConsignmentFeeManager extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5345bL = null;
    /* renamed from: bM */
    public static final C5663aRz f5346bM = null;
    /* renamed from: bN */
    public static final C2491fm f5347bN = null;
    /* renamed from: bO */
    public static final C2491fm f5348bO = null;
    /* renamed from: bP */
    public static final C2491fm f5349bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5350bQ = null;
    public static final C2491fm dOc = null;
    public static final C2491fm dxI = null;
    public static final C2491fm fuA = null;
    public static final C2491fm fuB = null;
    public static final C5663aRz fuy = null;
    public static final C2491fm fuz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3492c59e28974aec08a1d0b2f09013b7", aum = 0)
    private static C3438ra<ConsignmentFeeRange> bJC;
    @C0064Am(aul = "b00d699aeb94aad65f0e7baf336ec480", aum = 2)

    /* renamed from: bK */
    private static UUID f5344bK;
    @C0064Am(aul = "4609bcf788553ee77446e8ffccf5bc99", aum = 1)
    private static String handle;

    static {
        m26129V();
    }

    public ConsignmentFeeManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ConsignmentFeeManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26129V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 9;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ConsignmentFeeManager.class, "3492c59e28974aec08a1d0b2f09013b7", i);
        fuy = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ConsignmentFeeManager.class, "4609bcf788553ee77446e8ffccf5bc99", i2);
        f5346bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ConsignmentFeeManager.class, "b00d699aeb94aad65f0e7baf336ec480", i3);
        f5345bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 9)];
        C2491fm a = C4105zY.m41624a(ConsignmentFeeManager.class, "0dc507a419b6d237d26514b9fc5ad2a6", i5);
        f5347bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ConsignmentFeeManager.class, "e2632c7d813671e76f2376646439d79c", i6);
        f5348bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ConsignmentFeeManager.class, "bf49b4caa52131c585305ddb9cca205e", i7);
        fuz = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ConsignmentFeeManager.class, "f1fe8e6ee25a8b5f1bf4840025f4f1f5", i8);
        fuA = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ConsignmentFeeManager.class, "5c8160b450a939918497a644ba14d8ab", i9);
        dxI = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ConsignmentFeeManager.class, "903677bd5eb0c6b7beeac4de3d7c283e", i10);
        fuB = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ConsignmentFeeManager.class, "9023a3baa89348e666f78e07e8e8cdea", i11);
        f5349bP = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(ConsignmentFeeManager.class, "ee8c4e41f225582b1d21cb7849522a57", i12);
        f5350bQ = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(ConsignmentFeeManager.class, "4e2e621071c55c0a6bc1d16fe734dd8c", i13);
        dOc = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ConsignmentFeeManager.class, C4056yf.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m26130a(String str) {
        bFf().mo5608dq().mo3197f(f5346bM, str);
    }

    /* renamed from: a */
    private void m26131a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5345bL, uuid);
    }

    /* renamed from: an */
    private UUID m26133an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5345bL);
    }

    /* renamed from: ao */
    private String m26134ao() {
        return (String) bFf().mo5608dq().mo3214p(f5346bM);
    }

    /* renamed from: bO */
    private void m26139bO(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fuy, raVar);
    }

    private C3438ra bVx() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fuy);
    }

    /* renamed from: c */
    private void m26140c(UUID uuid) {
        switch (bFf().mo6893i(f5348bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5348bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5348bO, new Object[]{uuid}));
                break;
        }
        m26138b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4056yf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m26135ap();
            case 1:
                m26138b((UUID) args[0]);
                return null;
            case 2:
                return new Boolean(m26132a((ConsignmentFeeRange) args[0]));
            case 3:
                return new Boolean(m26141c((ConsignmentFeeRange) args[0]));
            case 4:
                return bgf();
            case 5:
                return bVy();
            case 6:
                return m26136ar();
            case 7:
                m26137b((String) args[0]);
                return null;
            case 8:
                return new Long(m26142fj(((Long) args[0]).longValue()));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5347bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5347bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5347bN, new Object[0]));
                break;
        }
        return m26135ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ranges")
    /* renamed from: b */
    public boolean mo16283b(ConsignmentFeeRange aax) {
        switch (bFf().mo6893i(fuz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fuz, new Object[]{aax}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fuz, new Object[]{aax}));
                break;
        }
        return m26132a(aax);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ranges")
    public List<ConsignmentFeeRange> bVz() {
        switch (bFf().mo6893i(fuB)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fuB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fuB, new Object[0]));
                break;
        }
        return bVy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ranges")
    /* renamed from: d */
    public boolean mo16285d(ConsignmentFeeRange aax) {
        switch (bFf().mo6893i(fuA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fuA, new Object[]{aax}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fuA, new Object[]{aax}));
                break;
        }
        return m26141c(aax);
    }

    /* renamed from: fk */
    public long mo16286fk(long j) {
        switch (bFf().mo6893i(dOc)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dOc, new Object[]{new Long(j)}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dOc, new Object[]{new Long(j)}));
                break;
        }
        return m26142fj(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5349bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5349bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5349bP, new Object[0]));
                break;
        }
        return m26136ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5350bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5350bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5350bQ, new Object[]{str}));
                break;
        }
        m26137b(str);
    }

    public Iterator<ConsignmentFeeRange> iterator() {
        switch (bFf().mo6893i(dxI)) {
            case 0:
                return null;
            case 2:
                return (Iterator) bFf().mo5606d(new aCE(this, dxI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dxI, new Object[0]));
                break;
        }
        return bgf();
    }

    @C0064Am(aul = "0dc507a419b6d237d26514b9fc5ad2a6", aum = 0)
    /* renamed from: ap */
    private UUID m26135ap() {
        return m26133an();
    }

    @C0064Am(aul = "e2632c7d813671e76f2376646439d79c", aum = 0)
    /* renamed from: b */
    private void m26138b(UUID uuid) {
        m26131a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m26131a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ranges")
    @C0064Am(aul = "bf49b4caa52131c585305ddb9cca205e", aum = 0)
    /* renamed from: a */
    private boolean m26132a(ConsignmentFeeRange aax) {
        return bVx().add(aax);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ranges")
    @C0064Am(aul = "f1fe8e6ee25a8b5f1bf4840025f4f1f5", aum = 0)
    /* renamed from: c */
    private boolean m26141c(ConsignmentFeeRange aax) {
        return bVx().remove(aax);
    }

    @C0064Am(aul = "5c8160b450a939918497a644ba14d8ab", aum = 0)
    private Iterator<ConsignmentFeeRange> bgf() {
        return bVx().iterator();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ranges")
    @C0064Am(aul = "903677bd5eb0c6b7beeac4de3d7c283e", aum = 0)
    private List<ConsignmentFeeRange> bVy() {
        return Collections.unmodifiableList(bVx());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "9023a3baa89348e666f78e07e8e8cdea", aum = 0)
    /* renamed from: ar */
    private String m26136ar() {
        return m26134ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "ee8c4e41f225582b1d21cb7849522a57", aum = 0)
    /* renamed from: b */
    private void m26137b(String str) {
        m26130a(str);
    }

    @C0064Am(aul = "4e2e621071c55c0a6bc1d16fe734dd8c", aum = 0)
    /* renamed from: fj */
    private long m26142fj(long j) {
        long j2;
        List w = MessageContainer.m16004w(bVx());
        Collections.sort(w, new C1995a());
        float f = 0.0f;
        long j3 = 0;
        Iterator it = w.iterator();
        while (true) {
            j2 = j3;
            if (it.hasNext()) {
                ConsignmentFeeRange aax = (ConsignmentFeeRange) it.next();
                if (aax.cIN() > j) {
                    break;
                }
                f = aax.cIP();
                j3 = aax.cIR();
            } else {
                break;
            }
        }
        return Math.max((long) Math.floor((double) ((((float) j) * f) / 100.0f)), j2);
    }

    /* renamed from: a.au$a */
    class C1995a implements Comparator<ConsignmentFeeRange> {
        C1995a() {
        }

        /* renamed from: a */
        public int compare(ConsignmentFeeRange aax, ConsignmentFeeRange aax2) {
            if (aax.cIN() < aax2.cIN()) {
                return -1;
            }
            if (aax.cIN() == aax2.cIN()) {
                return 0;
            }
            return 1;
        }
    }
}
