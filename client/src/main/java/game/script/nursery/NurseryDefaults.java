package game.script.nursery;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.agent.Agent;
import game.script.item.ItemType;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.mission.actions.RecurrentMessageRequestMissionAction;
import game.script.progression.ProgressionCareer;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C0781LJ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.ahp  reason: case insensitive filesystem */
/* compiled from: a */
public class NurseryDefaults extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4571bL = null;
    /* renamed from: bN */
    public static final C2491fm f4572bN = null;
    /* renamed from: bO */
    public static final C2491fm f4573bO = null;
    /* renamed from: bP */
    public static final C2491fm f4574bP = null;
    public static final C2491fm fJA = null;
    public static final C2491fm fJB = null;
    public static final C2491fm fJC = null;
    public static final C2491fm fJD = null;
    public static final C2491fm fJE = null;
    public static final C2491fm fJF = null;
    public static final C2491fm fJG = null;
    public static final C2491fm fJH = null;
    public static final C2491fm fJI = null;
    public static final C2491fm fJJ = null;
    public static final C2491fm fJK = null;
    public static final C2491fm fJL = null;
    public static final C2491fm fJM = null;
    public static final C2491fm fJN = null;
    public static final C2491fm fJO = null;
    public static final C2491fm fJP = null;
    public static final C2491fm fJQ = null;
    public static final C2491fm fJR = null;
    public static final C2491fm fJS = null;
    public static final C2491fm fJT = null;
    public static final C2491fm fJU = null;
    public static final C2491fm fJV = null;
    public static final C2491fm fJW = null;
    public static final C2491fm fJX = null;
    public static final C2491fm fJY = null;
    public static final C2491fm fJZ = null;
    public static final C5663aRz fJa = null;
    public static final C5663aRz fJb = null;
    public static final C5663aRz fJc = null;
    public static final C5663aRz fJd = null;
    public static final C5663aRz fJe = null;
    public static final C5663aRz fJf = null;
    public static final C5663aRz fJg = null;
    public static final C5663aRz fJh = null;
    public static final C5663aRz fJi = null;
    public static final C5663aRz fJj = null;
    public static final C5663aRz fJk = null;
    public static final C5663aRz fJl = null;
    public static final C5663aRz fJm = null;
    public static final C5663aRz fJn = null;
    public static final C5663aRz fJo = null;
    public static final C5663aRz fJp = null;
    public static final C5663aRz fJq = null;
    public static final C5663aRz fJr = null;
    public static final C5663aRz fJs = null;
    public static final C5663aRz fJt = null;
    public static final C5663aRz fJu = null;
    public static final C2491fm fJv = null;
    public static final C2491fm fJw = null;
    public static final C2491fm fJx = null;
    public static final C2491fm fJy = null;
    public static final C2491fm fJz = null;
    public static final C2491fm fKa = null;
    public static final C2491fm fKb = null;
    public static final C2491fm fKc = null;
    public static final C2491fm fKd = null;
    public static final C2491fm fKe = null;
    public static final C2491fm fKf = null;
    public static final C2491fm fKg = null;
    public static final C2491fm fKh = null;
    public static final C2491fm fKi = null;
    public static final C2491fm fKj = null;
    public static final C2491fm fKk = null;
    public static final C2491fm fKl = null;
    public static final C2491fm fKm = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "448c89d50fca8b9e8c801f0c59dbb667", aum = 21)

    /* renamed from: bK */
    private static UUID f4570bK;
    @C0064Am(aul = "fc0592326b81e5fb6a3c5d4e01bc78c6", aum = 0)
    private static C3438ra<ProgressionCareer> dwf;
    @C0064Am(aul = "c3e2ad5406f827a11909ed92147940c0", aum = 1)
    private static C3438ra<ProgressionCareer> dwg;
    @C0064Am(aul = "6a58a2768dc6e1dc62fd5f58f33d3ef9", aum = 2)
    private static ShipType dwh;
    @C0064Am(aul = "880748544fa3e535bc90b372259685a4", aum = 3)
    private static WeaponType dwi;
    @C0064Am(aul = "5457b86abf4938ee757c934f3168250b", aum = 4)
    private static MissionTemplate dwj;
    @C0064Am(aul = "8c0c6da97eb02e9fa46d01b6fe6fe544", aum = 5)
    private static MissionTemplate dwk;
    @C0064Am(aul = "5ff597398b13418039e8f43507b6097f", aum = 6)
    private static ItemType dwl;
    @C0064Am(aul = "06145e07ed3c49daf404210cc791e24e", aum = 7)
    private static MissionTemplate dwm;
    @C0064Am(aul = "8a55edc1efc79735c794154914b4d0f0", aum = 8)
    private static MissionTemplate dwn;
    @C0064Am(aul = "411f0650ecc37f71461a3bf0ed1067f0", aum = 9)
    private static RecurrentMessageRequestMissionAction dwo;
    @C0064Am(aul = "14a58d1400d226ff05f357d234d2e2ce", aum = 10)
    private static I18NString dwp;
    @C0064Am(aul = "a44b2ceb63ea125e270d73f0b7c78d98", aum = 11)
    private static I18NString dwq;
    @C0064Am(aul = "72ac4b19a02338b9177fad95a088bd4d", aum = 12)
    private static I18NString dwr;
    @C0064Am(aul = "af5f5fdc9cea3b0b2f8f80875f1c8556", aum = 13)
    private static I18NString dws;
    @C0064Am(aul = "556d75cdf9cd67a14449f0cfb1e427d7", aum = 14)
    private static RecurrentMessageRequestMissionAction dwt;
    @C0064Am(aul = "40f2a4315b2d4a38188f9f294b74558f", aum = 15)
    private static RecurrentMessageRequestMissionAction dwu;
    @C0064Am(aul = "bacddc2f1a9d3eab387df50299f08c0a", aum = 16)
    private static Station dwv;
    @C0064Am(aul = "21035d11294a83f805c040d1d9cfeae5", aum = 17)
    private static Station dww;
    @C0064Am(aul = "ec6f3fab91e7ff2ab8e77bc216c9f003", aum = 18)
    private static Station dwx;
    @C0064Am(aul = "d981deae72294e332f678f81154a6064", aum = 19)
    private static Station dwy;
    @C0064Am(aul = "b4e6ae40d60710ab36b3e96f560f6f0c", aum = 20)
    private static Agent dwz;

    static {
        m22306V();
    }

    public NurseryDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NurseryDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22306V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 22;
        _m_methodCount = aDJ._m_methodCount + 47;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 22)];
        C5663aRz b = C5640aRc.m17844b(NurseryDefaults.class, "fc0592326b81e5fb6a3c5d4e01bc78c6", i);
        fJa = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NurseryDefaults.class, "c3e2ad5406f827a11909ed92147940c0", i2);
        fJb = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NurseryDefaults.class, "6a58a2768dc6e1dc62fd5f58f33d3ef9", i3);
        fJc = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NurseryDefaults.class, "880748544fa3e535bc90b372259685a4", i4);
        fJd = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NurseryDefaults.class, "5457b86abf4938ee757c934f3168250b", i5);
        fJe = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NurseryDefaults.class, "8c0c6da97eb02e9fa46d01b6fe6fe544", i6);
        fJf = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NurseryDefaults.class, "5ff597398b13418039e8f43507b6097f", i7);
        fJg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NurseryDefaults.class, "06145e07ed3c49daf404210cc791e24e", i8);
        fJh = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NurseryDefaults.class, "8a55edc1efc79735c794154914b4d0f0", i9);
        fJi = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(NurseryDefaults.class, "411f0650ecc37f71461a3bf0ed1067f0", i10);
        fJj = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(NurseryDefaults.class, "14a58d1400d226ff05f357d234d2e2ce", i11);
        fJk = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(NurseryDefaults.class, "a44b2ceb63ea125e270d73f0b7c78d98", i12);
        fJl = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(NurseryDefaults.class, "72ac4b19a02338b9177fad95a088bd4d", i13);
        fJm = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(NurseryDefaults.class, "af5f5fdc9cea3b0b2f8f80875f1c8556", i14);
        fJn = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(NurseryDefaults.class, "556d75cdf9cd67a14449f0cfb1e427d7", i15);
        fJo = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(NurseryDefaults.class, "40f2a4315b2d4a38188f9f294b74558f", i16);
        fJp = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(NurseryDefaults.class, "bacddc2f1a9d3eab387df50299f08c0a", i17);
        fJq = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(NurseryDefaults.class, "21035d11294a83f805c040d1d9cfeae5", i18);
        fJr = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(NurseryDefaults.class, "ec6f3fab91e7ff2ab8e77bc216c9f003", i19);
        fJs = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(NurseryDefaults.class, "d981deae72294e332f678f81154a6064", i20);
        fJt = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(NurseryDefaults.class, "b4e6ae40d60710ab36b3e96f560f6f0c", i21);
        fJu = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(NurseryDefaults.class, "448c89d50fca8b9e8c801f0c59dbb667", i22);
        f4571bL = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i24 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i24 + 47)];
        C2491fm a = C4105zY.m41624a(NurseryDefaults.class, "b4d00f0b93a0fa31b7d8efb1d922408e", i24);
        f4572bN = a;
        fmVarArr[i24] = a;
        int i25 = i24 + 1;
        C2491fm a2 = C4105zY.m41624a(NurseryDefaults.class, "ad6bf91a18316ebd8f65497ff611774b", i25);
        f4573bO = a2;
        fmVarArr[i25] = a2;
        int i26 = i25 + 1;
        C2491fm a3 = C4105zY.m41624a(NurseryDefaults.class, "2c2866f17a4a0706409ebecb8db38ddb", i26);
        f4574bP = a3;
        fmVarArr[i26] = a3;
        int i27 = i26 + 1;
        C2491fm a4 = C4105zY.m41624a(NurseryDefaults.class, "9918b423beb3b2569efcc4efaa1ac1de", i27);
        fJv = a4;
        fmVarArr[i27] = a4;
        int i28 = i27 + 1;
        C2491fm a5 = C4105zY.m41624a(NurseryDefaults.class, "e623323d83c42690ede1898f5dc6c5b9", i28);
        fJw = a5;
        fmVarArr[i28] = a5;
        int i29 = i28 + 1;
        C2491fm a6 = C4105zY.m41624a(NurseryDefaults.class, "c53d2605542d8e97345cdb2b17ce71f8", i29);
        fJx = a6;
        fmVarArr[i29] = a6;
        int i30 = i29 + 1;
        C2491fm a7 = C4105zY.m41624a(NurseryDefaults.class, "5c7b25465087c15cc7973af23bfe6f98", i30);
        fJy = a7;
        fmVarArr[i30] = a7;
        int i31 = i30 + 1;
        C2491fm a8 = C4105zY.m41624a(NurseryDefaults.class, "6e036876cc4cdcb8ed43690101d80560", i31);
        fJz = a8;
        fmVarArr[i31] = a8;
        int i32 = i31 + 1;
        C2491fm a9 = C4105zY.m41624a(NurseryDefaults.class, "aa5d8c209e10517c8ef7a1115c186de6", i32);
        fJA = a9;
        fmVarArr[i32] = a9;
        int i33 = i32 + 1;
        C2491fm a10 = C4105zY.m41624a(NurseryDefaults.class, "050700ee79c25dfecd7e9fda1f4d69a3", i33);
        fJB = a10;
        fmVarArr[i33] = a10;
        int i34 = i33 + 1;
        C2491fm a11 = C4105zY.m41624a(NurseryDefaults.class, "3e32ec6314a75af92adc1c9176e5df8c", i34);
        fJC = a11;
        fmVarArr[i34] = a11;
        int i35 = i34 + 1;
        C2491fm a12 = C4105zY.m41624a(NurseryDefaults.class, "46deb4240b57e45371534add7e209e9a", i35);
        fJD = a12;
        fmVarArr[i35] = a12;
        int i36 = i35 + 1;
        C2491fm a13 = C4105zY.m41624a(NurseryDefaults.class, "079397da782539e2af4e822d10302fc9", i36);
        fJE = a13;
        fmVarArr[i36] = a13;
        int i37 = i36 + 1;
        C2491fm a14 = C4105zY.m41624a(NurseryDefaults.class, "ec34c3010a61177462fb675c15783b56", i37);
        fJF = a14;
        fmVarArr[i37] = a14;
        int i38 = i37 + 1;
        C2491fm a15 = C4105zY.m41624a(NurseryDefaults.class, "bf65467347ec59f4da712a7dcf8cce7a", i38);
        fJG = a15;
        fmVarArr[i38] = a15;
        int i39 = i38 + 1;
        C2491fm a16 = C4105zY.m41624a(NurseryDefaults.class, "c4513ea40d4bb7f022ca1024b4d05f59", i39);
        fJH = a16;
        fmVarArr[i39] = a16;
        int i40 = i39 + 1;
        C2491fm a17 = C4105zY.m41624a(NurseryDefaults.class, "5f9552671e3bc4fce0759c6a8c81f2a1", i40);
        fJI = a17;
        fmVarArr[i40] = a17;
        int i41 = i40 + 1;
        C2491fm a18 = C4105zY.m41624a(NurseryDefaults.class, "3bbb73d7ded271d4552c5e4d55b38932", i41);
        fJJ = a18;
        fmVarArr[i41] = a18;
        int i42 = i41 + 1;
        C2491fm a19 = C4105zY.m41624a(NurseryDefaults.class, "1f831bbd5847ff0132cfbdeab60d0621", i42);
        fJK = a19;
        fmVarArr[i42] = a19;
        int i43 = i42 + 1;
        C2491fm a20 = C4105zY.m41624a(NurseryDefaults.class, "45df55749ecfc7829f5e1e37859efb50", i43);
        fJL = a20;
        fmVarArr[i43] = a20;
        int i44 = i43 + 1;
        C2491fm a21 = C4105zY.m41624a(NurseryDefaults.class, "81623ca92e4126bfb65c0eafb3fed7b2", i44);
        fJM = a21;
        fmVarArr[i44] = a21;
        int i45 = i44 + 1;
        C2491fm a22 = C4105zY.m41624a(NurseryDefaults.class, "acd299e371da766d37d46e6e75336b30", i45);
        fJN = a22;
        fmVarArr[i45] = a22;
        int i46 = i45 + 1;
        C2491fm a23 = C4105zY.m41624a(NurseryDefaults.class, "0248606a6fa522915887d39541c8a807", i46);
        fJO = a23;
        fmVarArr[i46] = a23;
        int i47 = i46 + 1;
        C2491fm a24 = C4105zY.m41624a(NurseryDefaults.class, "061cd7dfcd9c22adbf3a003386b252a4", i47);
        fJP = a24;
        fmVarArr[i47] = a24;
        int i48 = i47 + 1;
        C2491fm a25 = C4105zY.m41624a(NurseryDefaults.class, "830d75e4379c2b83db35c836ab6eb9cd", i48);
        fJQ = a25;
        fmVarArr[i48] = a25;
        int i49 = i48 + 1;
        C2491fm a26 = C4105zY.m41624a(NurseryDefaults.class, "79df0a4f2168cb0fbc01625729520a01", i49);
        fJR = a26;
        fmVarArr[i49] = a26;
        int i50 = i49 + 1;
        C2491fm a27 = C4105zY.m41624a(NurseryDefaults.class, "88e4e7ef752f958707d25f87d74d08ec", i50);
        fJS = a27;
        fmVarArr[i50] = a27;
        int i51 = i50 + 1;
        C2491fm a28 = C4105zY.m41624a(NurseryDefaults.class, "a8965dd2d31fecda0d41a815d358aaf3", i51);
        fJT = a28;
        fmVarArr[i51] = a28;
        int i52 = i51 + 1;
        C2491fm a29 = C4105zY.m41624a(NurseryDefaults.class, "bc5d35aea72d9a5e1e837cfd700e5ad7", i52);
        fJU = a29;
        fmVarArr[i52] = a29;
        int i53 = i52 + 1;
        C2491fm a30 = C4105zY.m41624a(NurseryDefaults.class, "1c54ddf26a7b3fbd0b085f8ad33515cd", i53);
        fJV = a30;
        fmVarArr[i53] = a30;
        int i54 = i53 + 1;
        C2491fm a31 = C4105zY.m41624a(NurseryDefaults.class, "6b1e5edce88440e9ffccddcda2d354e2", i54);
        fJW = a31;
        fmVarArr[i54] = a31;
        int i55 = i54 + 1;
        C2491fm a32 = C4105zY.m41624a(NurseryDefaults.class, "bb16cf352bc8996bdb2a041d3d7e4f3b", i55);
        fJX = a32;
        fmVarArr[i55] = a32;
        int i56 = i55 + 1;
        C2491fm a33 = C4105zY.m41624a(NurseryDefaults.class, "8d75c5cd18a606b57d7f69acbbc8c331", i56);
        fJY = a33;
        fmVarArr[i56] = a33;
        int i57 = i56 + 1;
        C2491fm a34 = C4105zY.m41624a(NurseryDefaults.class, "a6b610297c039490859f685dc591c962", i57);
        fJZ = a34;
        fmVarArr[i57] = a34;
        int i58 = i57 + 1;
        C2491fm a35 = C4105zY.m41624a(NurseryDefaults.class, "3d7da11f20f0648a53169820439374a0", i58);
        fKa = a35;
        fmVarArr[i58] = a35;
        int i59 = i58 + 1;
        C2491fm a36 = C4105zY.m41624a(NurseryDefaults.class, "42369acdc8fe46b76bd60f436df37d55", i59);
        fKb = a36;
        fmVarArr[i59] = a36;
        int i60 = i59 + 1;
        C2491fm a37 = C4105zY.m41624a(NurseryDefaults.class, "365347068f89b5a2de7608cd16debcc7", i60);
        fKc = a37;
        fmVarArr[i60] = a37;
        int i61 = i60 + 1;
        C2491fm a38 = C4105zY.m41624a(NurseryDefaults.class, "e4e459d04e00977e6d91549c8f4e9c31", i61);
        fKd = a38;
        fmVarArr[i61] = a38;
        int i62 = i61 + 1;
        C2491fm a39 = C4105zY.m41624a(NurseryDefaults.class, "460cd0fca5d96f427bc824a3e6a7ba10", i62);
        fKe = a39;
        fmVarArr[i62] = a39;
        int i63 = i62 + 1;
        C2491fm a40 = C4105zY.m41624a(NurseryDefaults.class, "039e08c0fe32249e17ba60134cf560ee", i63);
        fKf = a40;
        fmVarArr[i63] = a40;
        int i64 = i63 + 1;
        C2491fm a41 = C4105zY.m41624a(NurseryDefaults.class, "db002ed1cfb0318f38244918cbff727a", i64);
        fKg = a41;
        fmVarArr[i64] = a41;
        int i65 = i64 + 1;
        C2491fm a42 = C4105zY.m41624a(NurseryDefaults.class, "cdbfc1d9d729a8c7f8067e9478b9d901", i65);
        fKh = a42;
        fmVarArr[i65] = a42;
        int i66 = i65 + 1;
        C2491fm a43 = C4105zY.m41624a(NurseryDefaults.class, "b7d7ca718e9d92d0bfb05c273ec4d581", i66);
        fKi = a43;
        fmVarArr[i66] = a43;
        int i67 = i66 + 1;
        C2491fm a44 = C4105zY.m41624a(NurseryDefaults.class, "8d993b9729eec02125be01e596b214cd", i67);
        fKj = a44;
        fmVarArr[i67] = a44;
        int i68 = i67 + 1;
        C2491fm a45 = C4105zY.m41624a(NurseryDefaults.class, "706e6c72bc616377b07b4a03bb9e7882", i68);
        fKk = a45;
        fmVarArr[i68] = a45;
        int i69 = i68 + 1;
        C2491fm a46 = C4105zY.m41624a(NurseryDefaults.class, "4796088a399d58b556b1373f3bfece48", i69);
        fKl = a46;
        fmVarArr[i69] = a46;
        int i70 = i69 + 1;
        C2491fm a47 = C4105zY.m41624a(NurseryDefaults.class, "4446f1aad0eb8341b528d3840f38bbf9", i70);
        fKm = a47;
        fmVarArr[i70] = a47;
        int i71 = i70 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NurseryDefaults.class, C0781LJ.class, _m_fields, _m_methods);
    }

    /* renamed from: N */
    private void m22304N(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(fJg, jCVar);
    }

    /* renamed from: W */
    private void m22307W(Station bf) {
        bFf().mo5608dq().mo3197f(fJq, bf);
    }

    /* renamed from: X */
    private void m22308X(Station bf) {
        bFf().mo5608dq().mo3197f(fJr, bf);
    }

    /* renamed from: Y */
    private void m22309Y(Station bf) {
        bFf().mo5608dq().mo3197f(fJs, bf);
    }

    /* renamed from: Z */
    private void m22310Z(Station bf) {
        bFf().mo5608dq().mo3197f(fJt, bf);
    }

    /* renamed from: a */
    private void m22311a(RecurrentMessageRequestMissionAction aul) {
        bFf().mo5608dq().mo3197f(fJj, aul);
    }

    /* renamed from: a */
    private void m22312a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4571bL, uuid);
    }

    /* renamed from: an */
    private UUID m22317an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4571bL);
    }

    /* renamed from: b */
    private void m22320b(RecurrentMessageRequestMissionAction aul) {
        bFf().mo5608dq().mo3197f(fJo, aul);
    }

    /* renamed from: bR */
    private void m22322bR(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fJa, raVar);
    }

    /* renamed from: bS */
    private void m22323bS(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fJb, raVar);
    }

    private C3438ra bYA() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fJa);
    }

    private C3438ra bYB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fJb);
    }

    private ShipType bYC() {
        return (ShipType) bFf().mo5608dq().mo3214p(fJc);
    }

    private WeaponType bYD() {
        return (WeaponType) bFf().mo5608dq().mo3214p(fJd);
    }

    private MissionTemplate bYE() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(fJe);
    }

    private MissionTemplate bYF() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(fJf);
    }

    private ItemType bYG() {
        return (ItemType) bFf().mo5608dq().mo3214p(fJg);
    }

    private MissionTemplate bYH() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(fJh);
    }

    private MissionTemplate bYI() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(fJi);
    }

    private RecurrentMessageRequestMissionAction bYJ() {
        return (RecurrentMessageRequestMissionAction) bFf().mo5608dq().mo3214p(fJj);
    }

    private I18NString bYK() {
        return (I18NString) bFf().mo5608dq().mo3214p(fJk);
    }

    private I18NString bYL() {
        return (I18NString) bFf().mo5608dq().mo3214p(fJl);
    }

    private I18NString bYM() {
        return (I18NString) bFf().mo5608dq().mo3214p(fJm);
    }

    private I18NString bYN() {
        return (I18NString) bFf().mo5608dq().mo3214p(fJn);
    }

    private RecurrentMessageRequestMissionAction bYO() {
        return (RecurrentMessageRequestMissionAction) bFf().mo5608dq().mo3214p(fJo);
    }

    private RecurrentMessageRequestMissionAction bYP() {
        return (RecurrentMessageRequestMissionAction) bFf().mo5608dq().mo3214p(fJp);
    }

    private Station bYQ() {
        return (Station) bFf().mo5608dq().mo3214p(fJq);
    }

    private Station bYR() {
        return (Station) bFf().mo5608dq().mo3214p(fJr);
    }

    private Station bYS() {
        return (Station) bFf().mo5608dq().mo3214p(fJs);
    }

    private Station bYT() {
        return (Station) bFf().mo5608dq().mo3214p(fJt);
    }

    private Agent bYU() {
        return (Agent) bFf().mo5608dq().mo3214p(fJu);
    }

    /* renamed from: c */
    private void m22324c(RecurrentMessageRequestMissionAction aul) {
        bFf().mo5608dq().mo3197f(fJp, aul);
    }

    /* renamed from: c */
    private void m22325c(UUID uuid) {
        switch (bFf().mo6893i(f4573bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4573bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4573bO, new Object[]{uuid}));
                break;
        }
        m22321b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Career path 1")
    @C0064Am(aul = "9918b423beb3b2569efcc4efaa1ac1de", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m22329h(ProgressionCareer sbVar) {
        throw new aWi(new aCE(this, fJv, new Object[]{sbVar}));
    }

    /* renamed from: i */
    private void m22330i(Agent abk) {
        bFf().mo5608dq().mo3197f(fJu, abk);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Career path 2")
    @C0064Am(aul = "e623323d83c42690ede1898f5dc6c5b9", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m22332j(ProgressionCareer sbVar) {
        throw new aWi(new aCE(this, fJw, new Object[]{sbVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Career path 1")
    @C0064Am(aul = "79df0a4f2168cb0fbc01625729520a01", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m22333l(ProgressionCareer sbVar) {
        throw new aWi(new aCE(this, fJR, new Object[]{sbVar}));
    }

    /* renamed from: ml */
    private void m22334ml(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(fJk, i18NString);
    }

    /* renamed from: mm */
    private void m22335mm(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(fJl, i18NString);
    }

    /* renamed from: mn */
    private void m22336mn(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(fJm, i18NString);
    }

    /* renamed from: mo */
    private void m22337mo(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(fJn, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Career path 2")
    @C0064Am(aul = "88e4e7ef752f958707d25f87d74d08ec", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m22342n(ProgressionCareer sbVar) {
        throw new aWi(new aCE(this, fJS, new Object[]{sbVar}));
    }

    /* renamed from: o */
    private void m22343o(WeaponType apt) {
        bFf().mo5608dq().mo3197f(fJd, apt);
    }

    /* renamed from: p */
    private void m22345p(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(fJe, avh);
    }

    /* renamed from: q */
    private void m22346q(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(fJf, avh);
    }

    /* renamed from: r */
    private void m22347r(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(fJh, avh);
    }

    /* renamed from: s */
    private void m22348s(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(fJi, avh);
    }

    /* renamed from: u */
    private void m22350u(ShipType ng) {
        bFf().mo5608dq().mo3197f(fJc, ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 3 (career path 2, go to last station)")
    /* renamed from: A */
    public void mo13580A(MissionTemplate avh) {
        switch (bFf().mo6893i(fJZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJZ, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJZ, new Object[]{avh}));
                break;
        }
        m22354z(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 2 ItemType")
    /* renamed from: P */
    public void mo13581P(ItemType jCVar) {
        switch (bFf().mo6893i(fJX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJX, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJX, new Object[]{jCVar}));
                break;
        }
        m22305O(jCVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0781LJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m22318ap();
            case 1:
                m22321b((UUID) args[0]);
                return null;
            case 2:
                return m22319ar();
            case 3:
                m22329h((ProgressionCareer) args[0]);
                return null;
            case 4:
                m22332j((ProgressionCareer) args[0]);
                return null;
            case 5:
                return bYV();
            case 6:
                return bYX();
            case 7:
                return bYZ();
            case 8:
                return bZb();
            case 9:
                return bZd();
            case 10:
                return bZf();
            case 11:
                return bZh();
            case 12:
                return bZj();
            case 13:
                return bZl();
            case 14:
                return bZn();
            case 15:
                return bZp();
            case 16:
                return bZr();
            case 17:
                return bZt();
            case 18:
                return bZv();
            case 19:
                return bZx();
            case 20:
                return bZz();
            case 21:
                return bZB();
            case 22:
                return bZD();
            case 23:
                return bZF();
            case 24:
                return bZH();
            case 25:
                m22333l((ProgressionCareer) args[0]);
                return null;
            case 26:
                m22342n((ProgressionCareer) args[0]);
                return null;
            case 27:
                m22351v((ShipType) args[0]);
                return null;
            case 28:
                m22344p((WeaponType) args[0]);
                return null;
            case 29:
                m22349t((MissionTemplate) args[0]);
                return null;
            case 30:
                m22352v((MissionTemplate) args[0]);
                return null;
            case 31:
                m22305O((ItemType) args[0]);
                return null;
            case 32:
                m22353x((MissionTemplate) args[0]);
                return null;
            case 33:
                m22354z((MissionTemplate) args[0]);
                return null;
            case 34:
                m22326d((RecurrentMessageRequestMissionAction) args[0]);
                return null;
            case 35:
                m22338mp((I18NString) args[0]);
                return null;
            case 36:
                m22339mr((I18NString) args[0]);
                return null;
            case 37:
                m22340mt((I18NString) args[0]);
                return null;
            case 38:
                m22341mv((I18NString) args[0]);
                return null;
            case 39:
                m22327f((RecurrentMessageRequestMissionAction) args[0]);
                return null;
            case 40:
                m22328h((RecurrentMessageRequestMissionAction) args[0]);
                return null;
            case 41:
                m22313aa((Station) args[0]);
                return null;
            case 42:
                m22314ac((Station) args[0]);
                return null;
            case 43:
                m22315ae((Station) args[0]);
                return null;
            case 44:
                m22316ag((Station) args[0]);
                return null;
            case 45:
                return bZJ();
            case 46:
                m22331j((Agent) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 1 (initial)")
    /* renamed from: ab */
    public void mo13582ab(Station bf) {
        switch (bFf().mo6893i(fKh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKh, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKh, new Object[]{bf}));
                break;
        }
        m22313aa(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 2 (outpost)")
    /* renamed from: ad */
    public void mo13583ad(Station bf) {
        switch (bFf().mo6893i(fKi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKi, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKi, new Object[]{bf}));
                break;
        }
        m22314ac(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 3 (career path 1, final station)")
    /* renamed from: af */
    public void mo13584af(Station bf) {
        switch (bFf().mo6893i(fKj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKj, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKj, new Object[]{bf}));
                break;
        }
        m22315ae(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 3 (career path 2, final station)")
    /* renamed from: ah */
    public void mo13585ah(Station bf) {
        switch (bFf().mo6893i(fKk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKk, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKk, new Object[]{bf}));
                break;
        }
        m22316ag(bf);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4572bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4572bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4572bN, new Object[0]));
                break;
        }
        return m22318ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Career path 1")
    public List<ProgressionCareer> bYW() {
        switch (bFf().mo6893i(fJx)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fJx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJx, new Object[0]));
                break;
        }
        return bYV();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Career path 2")
    public List<ProgressionCareer> bYY() {
        switch (bFf().mo6893i(fJy)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fJy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJy, new Object[0]));
                break;
        }
        return bYX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 7 (send to deposit)")
    public RecurrentMessageRequestMissionAction bZA() {
        switch (bFf().mo6893i(fJM)) {
            case 0:
                return null;
            case 2:
                return (RecurrentMessageRequestMissionAction) bFf().mo5606d(new aCE(this, fJM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJM, new Object[0]));
                break;
        }
        return bZz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 1 (initial)")
    public Station bZC() {
        switch (bFf().mo6893i(fJN)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, fJN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJN, new Object[0]));
                break;
        }
        return bZB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 2 (outpost)")
    public Station bZE() {
        switch (bFf().mo6893i(fJO)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, fJO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJO, new Object[0]));
                break;
        }
        return bZD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 3 (career path 1, final station)")
    public Station bZG() {
        switch (bFf().mo6893i(fJP)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, fJP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJP, new Object[0]));
                break;
        }
        return bZF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 3 (career path 2, final station)")
    public Station bZI() {
        switch (bFf().mo6893i(fJQ)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, fJQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJQ, new Object[0]));
                break;
        }
        return bZH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutor Agent")
    public Agent bZK() {
        switch (bFf().mo6893i(fKl)) {
            case 0:
                return null;
            case 2:
                return (Agent) bFf().mo5606d(new aCE(this, fKl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fKl, new Object[0]));
                break;
        }
        return bZJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ship Type")
    public ShipType bZa() {
        switch (bFf().mo6893i(fJz)) {
            case 0:
                return null;
            case 2:
                return (ShipType) bFf().mo5606d(new aCE(this, fJz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJz, new Object[0]));
                break;
        }
        return bYZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Weapon Type")
    public WeaponType bZc() {
        switch (bFf().mo6893i(fJA)) {
            case 0:
                return null;
            case 2:
                return (WeaponType) bFf().mo5606d(new aCE(this, fJA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJA, new Object[0]));
                break;
        }
        return bZb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 1 (destroy ships)")
    public MissionTemplate bZe() {
        switch (bFf().mo6893i(fJB)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, fJB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJB, new Object[0]));
                break;
        }
        return bZd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 2 (buy and transport)")
    public MissionTemplate bZg() {
        switch (bFf().mo6893i(fJC)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, fJC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJC, new Object[0]));
                break;
        }
        return bZf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 2 ItemType")
    public ItemType bZi() {
        switch (bFf().mo6893i(fJD)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, fJD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJD, new Object[0]));
                break;
        }
        return bZh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 3 (career path 1, go to last station)")
    public MissionTemplate bZk() {
        switch (bFf().mo6893i(fJE)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, fJE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJE, new Object[0]));
                break;
        }
        return bZj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 3 (career path 2, go to last station)")
    public MissionTemplate bZm() {
        switch (bFf().mo6893i(fJF)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, fJF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJF, new Object[0]));
                break;
        }
        return bZl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 1 (click on undock)")
    public RecurrentMessageRequestMissionAction bZo() {
        switch (bFf().mo6893i(fJG)) {
            case 0:
                return null;
            case 2:
                return (RecurrentMessageRequestMissionAction) bFf().mo5606d(new aCE(this, fJG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJG, new Object[0]));
                break;
        }
        return bZn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 2 (open market)")
    public I18NString bZq() {
        switch (bFf().mo6893i(fJH)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fJH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJH, new Object[0]));
                break;
        }
        return bZp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 3 (select mission item)")
    public I18NString bZs() {
        switch (bFf().mo6893i(fJI)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fJI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJI, new Object[0]));
                break;
        }
        return bZr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 4 (destination container)")
    public I18NString bZu() {
        switch (bFf().mo6893i(fJJ)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fJJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJJ, new Object[0]));
                break;
        }
        return bZt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 5 (buy item)")
    public I18NString bZw() {
        switch (bFf().mo6893i(fJK)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fJK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJK, new Object[0]));
                break;
        }
        return bZv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 6 (open ship window)")
    public RecurrentMessageRequestMissionAction bZy() {
        switch (bFf().mo6893i(fJL)) {
            case 0:
                return null;
            case 2:
                return (RecurrentMessageRequestMissionAction) bFf().mo5606d(new aCE(this, fJL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fJL, new Object[0]));
                break;
        }
        return bZx();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 1 (click on undock)")
    /* renamed from: e */
    public void mo13607e(RecurrentMessageRequestMissionAction aul) {
        switch (bFf().mo6893i(fKa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKa, new Object[]{aul}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKa, new Object[]{aul}));
                break;
        }
        m22326d(aul);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 6 (open ship window)")
    /* renamed from: g */
    public void mo13608g(RecurrentMessageRequestMissionAction aul) {
        switch (bFf().mo6893i(fKf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKf, new Object[]{aul}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKf, new Object[]{aul}));
                break;
        }
        m22327f(aul);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4574bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4574bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4574bP, new Object[0]));
                break;
        }
        return m22319ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 7 (send to deposit)")
    /* renamed from: i */
    public void mo13609i(RecurrentMessageRequestMissionAction aul) {
        switch (bFf().mo6893i(fKg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKg, new Object[]{aul}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKg, new Object[]{aul}));
                break;
        }
        m22328h(aul);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Career path 1")
    @C5566aOg
    /* renamed from: i */
    public void mo13610i(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(fJv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJv, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJv, new Object[]{sbVar}));
                break;
        }
        m22329h(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutor Agent")
    /* renamed from: k */
    public void mo13611k(Agent abk) {
        switch (bFf().mo6893i(fKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKm, new Object[]{abk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKm, new Object[]{abk}));
                break;
        }
        m22331j(abk);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Career path 2")
    @C5566aOg
    /* renamed from: k */
    public void mo13612k(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(fJw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJw, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJw, new Object[]{sbVar}));
                break;
        }
        m22332j(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Career path 1")
    @C5566aOg
    /* renamed from: m */
    public void mo13613m(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(fJR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJR, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJR, new Object[]{sbVar}));
                break;
        }
        m22333l(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 2 (open market)")
    /* renamed from: mq */
    public void mo13614mq(I18NString i18NString) {
        switch (bFf().mo6893i(fKb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKb, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKb, new Object[]{i18NString}));
                break;
        }
        m22338mp(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 3 (select mission item)")
    /* renamed from: ms */
    public void mo13615ms(I18NString i18NString) {
        switch (bFf().mo6893i(fKc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKc, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKc, new Object[]{i18NString}));
                break;
        }
        m22339mr(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 4 (destination container)")
    /* renamed from: mu */
    public void mo13616mu(I18NString i18NString) {
        switch (bFf().mo6893i(fKd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKd, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKd, new Object[]{i18NString}));
                break;
        }
        m22340mt(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 5 (buy item)")
    /* renamed from: mw */
    public void mo13617mw(I18NString i18NString) {
        switch (bFf().mo6893i(fKe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fKe, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fKe, new Object[]{i18NString}));
                break;
        }
        m22341mv(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Career path 2")
    @C5566aOg
    /* renamed from: o */
    public void mo13618o(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(fJS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJS, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJS, new Object[]{sbVar}));
                break;
        }
        m22342n(sbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Weapon Type")
    /* renamed from: q */
    public void mo13619q(WeaponType apt) {
        switch (bFf().mo6893i(fJU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJU, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJU, new Object[]{apt}));
                break;
        }
        m22344p(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 1 (destroy ships)")
    /* renamed from: u */
    public void mo13620u(MissionTemplate avh) {
        switch (bFf().mo6893i(fJV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJV, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJV, new Object[]{avh}));
                break;
        }
        m22349t(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ship Type")
    /* renamed from: w */
    public void mo13621w(ShipType ng) {
        switch (bFf().mo6893i(fJT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJT, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJT, new Object[]{ng}));
                break;
        }
        m22351v(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 2 (buy and transport)")
    /* renamed from: w */
    public void mo13622w(MissionTemplate avh) {
        switch (bFf().mo6893i(fJW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJW, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJW, new Object[]{avh}));
                break;
        }
        m22352v(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 3 (career path 1, go to last station)")
    /* renamed from: y */
    public void mo13623y(MissionTemplate avh) {
        switch (bFf().mo6893i(fJY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fJY, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fJY, new Object[]{avh}));
                break;
        }
        m22353x(avh);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "b4d00f0b93a0fa31b7d8efb1d922408e", aum = 0)
    /* renamed from: ap */
    private UUID m22318ap() {
        return m22317an();
    }

    @C0064Am(aul = "ad6bf91a18316ebd8f65497ff611774b", aum = 0)
    /* renamed from: b */
    private void m22321b(UUID uuid) {
        m22312a(uuid);
    }

    @C0064Am(aul = "2c2866f17a4a0706409ebecb8db38ddb", aum = 0)
    /* renamed from: ar */
    private String m22319ar() {
        return "nursery_defaults";
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Career path 1")
    @C0064Am(aul = "c53d2605542d8e97345cdb2b17ce71f8", aum = 0)
    private List<ProgressionCareer> bYV() {
        return Collections.unmodifiableList(bYA());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Career path 2")
    @C0064Am(aul = "5c7b25465087c15cc7973af23bfe6f98", aum = 0)
    private List<ProgressionCareer> bYX() {
        return Collections.unmodifiableList(bYB());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ship Type")
    @C0064Am(aul = "6e036876cc4cdcb8ed43690101d80560", aum = 0)
    private ShipType bYZ() {
        return bYC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Weapon Type")
    @C0064Am(aul = "aa5d8c209e10517c8ef7a1115c186de6", aum = 0)
    private WeaponType bZb() {
        return bYD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 1 (destroy ships)")
    @C0064Am(aul = "050700ee79c25dfecd7e9fda1f4d69a3", aum = 0)
    private MissionTemplate bZd() {
        return bYE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 2 (buy and transport)")
    @C0064Am(aul = "3e32ec6314a75af92adc1c9176e5df8c", aum = 0)
    private MissionTemplate bZf() {
        return bYF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 2 ItemType")
    @C0064Am(aul = "46deb4240b57e45371534add7e209e9a", aum = 0)
    private ItemType bZh() {
        return bYG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 3 (career path 1, go to last station)")
    @C0064Am(aul = "079397da782539e2af4e822d10302fc9", aum = 0)
    private MissionTemplate bZj() {
        return bYH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission 3 (career path 2, go to last station)")
    @C0064Am(aul = "ec34c3010a61177462fb675c15783b56", aum = 0)
    private MissionTemplate bZl() {
        return bYI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 1 (click on undock)")
    @C0064Am(aul = "bf65467347ec59f4da712a7dcf8cce7a", aum = 0)
    private RecurrentMessageRequestMissionAction bZn() {
        return bYJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 2 (open market)")
    @C0064Am(aul = "c4513ea40d4bb7f022ca1024b4d05f59", aum = 0)
    private I18NString bZp() {
        return bYK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 3 (select mission item)")
    @C0064Am(aul = "5f9552671e3bc4fce0759c6a8c81f2a1", aum = 0)
    private I18NString bZr() {
        return bYL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 4 (destination container)")
    @C0064Am(aul = "3bbb73d7ded271d4552c5e4d55b38932", aum = 0)
    private I18NString bZt() {
        return bYM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 5 (buy item)")
    @C0064Am(aul = "1f831bbd5847ff0132cfbdeab60d0621", aum = 0)
    private I18NString bZv() {
        return bYN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 6 (open ship window)")
    @C0064Am(aul = "45df55749ecfc7829f5e1e37859efb50", aum = 0)
    private RecurrentMessageRequestMissionAction bZx() {
        return bYO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recurrent Message 7 (send to deposit)")
    @C0064Am(aul = "81623ca92e4126bfb65c0eafb3fed7b2", aum = 0)
    private RecurrentMessageRequestMissionAction bZz() {
        return bYP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 1 (initial)")
    @C0064Am(aul = "acd299e371da766d37d46e6e75336b30", aum = 0)
    private Station bZB() {
        return bYQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 2 (outpost)")
    @C0064Am(aul = "0248606a6fa522915887d39541c8a807", aum = 0)
    private Station bZD() {
        return bYR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 3 (career path 1, final station)")
    @C0064Am(aul = "061cd7dfcd9c22adbf3a003386b252a4", aum = 0)
    private Station bZF() {
        return bYS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station 3 (career path 2, final station)")
    @C0064Am(aul = "830d75e4379c2b83db35c836ab6eb9cd", aum = 0)
    private Station bZH() {
        return bYT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ship Type")
    @C0064Am(aul = "a8965dd2d31fecda0d41a815d358aaf3", aum = 0)
    /* renamed from: v */
    private void m22351v(ShipType ng) {
        m22350u(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Weapon Type")
    @C0064Am(aul = "bc5d35aea72d9a5e1e837cfd700e5ad7", aum = 0)
    /* renamed from: p */
    private void m22344p(WeaponType apt) {
        m22343o(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 1 (destroy ships)")
    @C0064Am(aul = "1c54ddf26a7b3fbd0b085f8ad33515cd", aum = 0)
    /* renamed from: t */
    private void m22349t(MissionTemplate avh) {
        m22345p(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 2 (buy and transport)")
    @C0064Am(aul = "6b1e5edce88440e9ffccddcda2d354e2", aum = 0)
    /* renamed from: v */
    private void m22352v(MissionTemplate avh) {
        m22346q(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 2 ItemType")
    @C0064Am(aul = "bb16cf352bc8996bdb2a041d3d7e4f3b", aum = 0)
    /* renamed from: O */
    private void m22305O(ItemType jCVar) {
        m22304N(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 3 (career path 1, go to last station)")
    @C0064Am(aul = "8d75c5cd18a606b57d7f69acbbc8c331", aum = 0)
    /* renamed from: x */
    private void m22353x(MissionTemplate avh) {
        m22347r(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission 3 (career path 2, go to last station)")
    @C0064Am(aul = "a6b610297c039490859f685dc591c962", aum = 0)
    /* renamed from: z */
    private void m22354z(MissionTemplate avh) {
        m22348s(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 1 (click on undock)")
    @C0064Am(aul = "3d7da11f20f0648a53169820439374a0", aum = 0)
    /* renamed from: d */
    private void m22326d(RecurrentMessageRequestMissionAction aul) {
        m22311a(aul);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 2 (open market)")
    @C0064Am(aul = "42369acdc8fe46b76bd60f436df37d55", aum = 0)
    /* renamed from: mp */
    private void m22338mp(I18NString i18NString) {
        m22334ml(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 3 (select mission item)")
    @C0064Am(aul = "365347068f89b5a2de7608cd16debcc7", aum = 0)
    /* renamed from: mr */
    private void m22339mr(I18NString i18NString) {
        m22335mm(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 4 (destination container)")
    @C0064Am(aul = "e4e459d04e00977e6d91549c8f4e9c31", aum = 0)
    /* renamed from: mt */
    private void m22340mt(I18NString i18NString) {
        m22336mn(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 5 (buy item)")
    @C0064Am(aul = "460cd0fca5d96f427bc824a3e6a7ba10", aum = 0)
    /* renamed from: mv */
    private void m22341mv(I18NString i18NString) {
        m22337mo(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 6 (open ship window)")
    @C0064Am(aul = "039e08c0fe32249e17ba60134cf560ee", aum = 0)
    /* renamed from: f */
    private void m22327f(RecurrentMessageRequestMissionAction aul) {
        m22320b(aul);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recurrent Message 7 (send to deposit)")
    @C0064Am(aul = "db002ed1cfb0318f38244918cbff727a", aum = 0)
    /* renamed from: h */
    private void m22328h(RecurrentMessageRequestMissionAction aul) {
        m22324c(aul);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 1 (initial)")
    @C0064Am(aul = "cdbfc1d9d729a8c7f8067e9478b9d901", aum = 0)
    /* renamed from: aa */
    private void m22313aa(Station bf) {
        m22307W(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 2 (outpost)")
    @C0064Am(aul = "b7d7ca718e9d92d0bfb05c273ec4d581", aum = 0)
    /* renamed from: ac */
    private void m22314ac(Station bf) {
        m22308X(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 3 (career path 1, final station)")
    @C0064Am(aul = "8d993b9729eec02125be01e596b214cd", aum = 0)
    /* renamed from: ae */
    private void m22315ae(Station bf) {
        m22309Y(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station 3 (career path 2, final station)")
    @C0064Am(aul = "706e6c72bc616377b07b4a03bb9e7882", aum = 0)
    /* renamed from: ag */
    private void m22316ag(Station bf) {
        m22310Z(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tutor Agent")
    @C0064Am(aul = "4796088a399d58b556b1373f3bfece48", aum = 0)
    private Agent bZJ() {
        return bYU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Tutor Agent")
    @C0064Am(aul = "4446f1aad0eb8341b528d3840f38bbf9", aum = 0)
    /* renamed from: j */
    private void m22331j(Agent abk) {
        m22330i(abk);
    }
}
