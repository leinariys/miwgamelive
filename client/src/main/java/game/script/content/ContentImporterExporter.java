package game.script.content;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.ayP;
import logic.res.XmlNode;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.geom.Orientation;
import taikodom.infra.script.I18NString;

import java.io.PrintWriter;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.util.*;

@C3437rZ(aak = "", aal = "taikodom.game.script.ContentImporterExporter")
@C6485anp
@C5511aMd
/* renamed from: a.Zn */
/* compiled from: a */
public class ContentImporterExporter extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm hqd = null;
    public static final C2491fm hqe = null;
    public static final C2491fm hqf = null;
    public static final C2491fm hqg = null;
    public static final C2491fm hqh = null;
    public static final C2491fm hqi = null;
    public static final C2491fm hqj = null;
    public static final C2491fm hqk = null;
    public static final C2491fm hql = null;
    public static final C2491fm hqm = null;
    public static final C2491fm hqn = null;
    public static final C2491fm hqo = null;
    public static final C2491fm hqp = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m12180V();
    }

    private transient Set<Class<?>> hqa;
    private transient Map<UUID, C0468GU> hqb;
    private transient C1746a hqc;

    public ContentImporterExporter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ContentImporterExporter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12180V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 13)];
        C2491fm a = C4105zY.m41624a(ContentImporterExporter.class, "f2a885930143f018e6b3587d44a681fa", i);
        hqd = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ContentImporterExporter.class, "51857d02ea4e341f202a4825ec8e1628", i2);
        hqe = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ContentImporterExporter.class, "67078b981de484767d38756c68cf0ce9", i3);
        hqf = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ContentImporterExporter.class, "90a77a35c4fb57847053959df226cb53", i4);
        hqg = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ContentImporterExporter.class, "a09f274d4063df7c2df91eb358592850", i5);
        hqh = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(ContentImporterExporter.class, "ff0be0baf20d7691c1da9e8f5a3c0a1a", i6);
        hqi = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(ContentImporterExporter.class, "0c961fc73fc7bbd8865899a6cb22335d", i7);
        hqj = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(ContentImporterExporter.class, "3ef29773a3d195815fb853228bf136d0", i8);
        hqk = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(ContentImporterExporter.class, "d5712e1d42f5bf36afeccb58a9d3d47f", i9);
        hql = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(ContentImporterExporter.class, "e65f9eab8501e9b49591f0236f565ae3", i10);
        hqm = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(ContentImporterExporter.class, "4f15f66049d38160b1539cbf192d44d6", i11);
        hqn = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(ContentImporterExporter.class, "ba2acd51170b15864c5add19063631fd", i12);
        hqo = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(ContentImporterExporter.class, "04aa1fb685a281bee50ecc195688e258", i13);
        hqp = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ContentImporterExporter.class, ayP.class, _m_fields, _m_methods);
    }

    /* renamed from: aC */
    private Method m12184aC(Class cls) {
        switch (bFf().mo6893i(hql)) {
            case 0:
                return null;
            case 2:
                return (Method) bFf().mo5606d(new aCE(this, hql, new Object[]{cls}));
            case 3:
                bFf().mo5606d(new aCE(this, hql, new Object[]{cls}));
                break;
        }
        return m12183aB(cls);
    }

    /* renamed from: aE */
    private String m12187aE(Object obj) {
        switch (bFf().mo6893i(hqe)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hqe, new Object[]{obj}));
            case 3:
                bFf().mo5606d(new aCE(this, hqe, new Object[]{obj}));
                break;
        }
        return m12185aD(obj);
    }

    /* renamed from: aE */
    private Method m12188aE(Class cls) {
        switch (bFf().mo6893i(hqm)) {
            case 0:
                return null;
            case 2:
                return (Method) bFf().mo5606d(new aCE(this, hqm, new Object[]{cls}));
            case 3:
                bFf().mo5606d(new aCE(this, hqm, new Object[]{cls}));
                break;
        }
        return m12186aD(cls);
    }

    /* renamed from: b */
    private boolean m12189b(PrintWriter printWriter, String str, Object obj, String str2) {
        switch (bFf().mo6893i(hqi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hqi, new Object[]{printWriter, str, obj, str2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hqi, new Object[]{printWriter, str, obj, str2}));
                break;
        }
        return m12182a(printWriter, str, obj, str2);
    }

    private void cPA() {
        switch (bFf().mo6893i(hqf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hqf, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hqf, new Object[0]));
                break;
        }
        cPz();
    }

    @C5566aOg
    @C5794aaa
    @C0064Am(aul = "3ef29773a3d195815fb853228bf136d0", aum = 0)
    @C2499fr
    private String cPD() {
        throw new aWi(new aCE(this, hqk, new Object[0]));
    }

    /* renamed from: g */
    private Map<String, C1566Wy> m12191g(C0468GU gu) {
        switch (bFf().mo6893i(hqg)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, hqg, new Object[]{gu}));
            case 3:
                bFf().mo5606d(new aCE(this, hqg, new Object[]{gu}));
                break;
        }
        return m12190f(gu);
    }

    /* renamed from: i */
    private Map<String, C1566Wy> m12194i(C0468GU gu) {
        switch (bFf().mo6893i(hqh)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, hqh, new Object[]{gu}));
            case 3:
                bFf().mo5606d(new aCE(this, hqh, new Object[]{gu}));
                break;
        }
        return m12192h(gu);
    }

    /* renamed from: j */
    private Object m12195j(XmlNode agy) {
        switch (bFf().mo6893i(hqn)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, hqn, new Object[]{agy}));
            case 3:
                bFf().mo5606d(new aCE(this, hqn, new Object[]{agy}));
                break;
        }
        return m12193i(agy);
    }

    @C5566aOg
    @C5794aaa
    @C0064Am(aul = "04aa1fb685a281bee50ecc195688e258", aum = 0)
    @C2499fr
    /* renamed from: k */
    private void m12196k(byte[] bArr) {
        throw new aWi(new aCE(this, hqp, new Object[]{bArr}));
    }

    /* renamed from: lu */
    private String m12198lu(String str) {
        switch (bFf().mo6893i(hqd)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hqd, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, hqd, new Object[]{str}));
                break;
        }
        return m12197lt(str);
    }

    /* renamed from: lw */
    private void m12200lw(String str) {
        switch (bFf().mo6893i(hqo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hqo, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hqo, new Object[]{str}));
                break;
        }
        m12199lv(str);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new ayP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m12197lt((String) args[0]);
            case 1:
                return m12185aD(args[0]);
            case 2:
                cPz();
                return null;
            case 3:
                return m12190f((C0468GU) args[0]);
            case 4:
                return m12192h((C0468GU) args[0]);
            case 5:
                return new Boolean(m12182a((PrintWriter) args[0], (String) args[1], args[2], (String) args[3]));
            case 6:
                return cPB();
            case 7:
                return cPD();
            case 8:
                return m12183aB((Class) args[0]);
            case 9:
                return m12186aD((Class) args[0]);
            case 10:
                return m12193i((XmlNode) args[0]);
            case 11:
                m12199lv((String) args[0]);
                return null;
            case 12:
                m12196k((byte[]) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public StringBuffer cPC() {
        switch (bFf().mo6893i(hqj)) {
            case 0:
                return null;
            case 2:
                return (StringBuffer) bFf().mo5606d(new aCE(this, hqj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hqj, new Object[0]));
                break;
        }
        return cPB();
    }

    @C5566aOg
    @C5794aaa
    @C2499fr
    public String cPE() {
        switch (bFf().mo6893i(hqk)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hqk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hqk, new Object[0]));
                break;
        }
        return cPD();
    }

    @C5566aOg
    @C5794aaa
    @C2499fr
    /* renamed from: l */
    public void mo7469l(byte[] bArr) {
        switch (bFf().mo6893i(hqp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hqp, new Object[]{bArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hqp, new Object[]{bArr}));
                break;
        }
        m12196k(bArr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f2a885930143f018e6b3587d44a681fa", aum = 0)
    /* renamed from: lt */
    private String m12197lt(String str) {
        return str.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;").replace("'", "&apos;");
    }

    @C0064Am(aul = "51857d02ea4e341f202a4825ec8e1628", aum = 0)
    /* renamed from: aD */
    private String m12185aD(Object obj) {
        if (obj instanceof Vec3d) {
            return C3803vB.m40160w((Vec3d) obj);
        }
        if (obj instanceof Vec3f) {
            return C0928Nc.m7696aa((Vec3f) obj);
        }
        if (obj instanceof Color) {
            return aTO.m18351Y((Color) obj);
        }
        if (obj instanceof Quat4fWrap) {
            return C3352qa.m37726c((Quat4fWrap) obj);
        }
        if (obj instanceof Class) {
            return C3360qg.m37769k((Class) obj);
        }
        if (obj instanceof C3892wO) {
            return C3614sy.m39218A((C3892wO) obj);
        }
        if (obj instanceof C1649YI) {
            return C0987OX.m8085a((C1649YI) obj);
        }
        return obj.toString();
    }

    @C0064Am(aul = "67078b981de484767d38756c68cf0ce9", aum = 0)
    private void cPz() {
        this.hqb = new HashMap();
        int i = 0;
        int i2 = 0;
        for (C3582se yn : ala().bFf().mo6866PM().bGA()) {
            C1616Xf yn2 = yn.mo6901yn();
            if (yn2 instanceof C0468GU) {
                C0468GU gu = (C0468GU) yn2;
                UUID aq = gu.mo18aq();
                if (aq == null) {
                    i++;
                } else if (this.hqb.keySet().contains(aq)) {
                    i2++;
                }
                this.hqb.put(aq, gu);
                i2 = i2;
            }
        }
    }

    @C0064Am(aul = "90a77a35c4fb57847053959df226cb53", aum = 0)
    /* renamed from: f */
    private Map<String, C1566Wy> m12190f(C0468GU gu) {
        SortedMap<String, C1566Wy> a = C1133Qd.m8961a(this.hqc, (C1616Xf) gu);
        HashMap hashMap = new HashMap();
        for (String next : a.keySet()) {
            C1566Wy wy = (C1566Wy) a.get(next);
            if (wy.bDt() != null) {
                hashMap.put(next, wy);
            }
        }
        return hashMap;
    }

    @C0064Am(aul = "a09f274d4063df7c2df91eb358592850", aum = 0)
    /* renamed from: h */
    private Map<String, C1566Wy> m12192h(C0468GU gu) {
        SortedMap<String, C1566Wy> a = C1133Qd.m8961a(this.hqc, (C1616Xf) gu);
        HashMap hashMap = new HashMap();
        for (String next : a.keySet()) {
            C1566Wy wy = (C1566Wy) a.get(next);
            if (wy.bDx() != null) {
                hashMap.put(next, wy);
            }
        }
        return hashMap;
    }

    @C0064Am(aul = "ff0be0baf20d7691c1da9e8f5a3c0a1a", aum = 0)
    /* renamed from: a */
    private boolean m12182a(PrintWriter printWriter, String str, Object obj, String str2) {
        Class<?> cls = obj.getClass();
        if (C0468GU.class.isAssignableFrom(cls)) {
            printWriter.print(str2);
            printWriter.print("<");
            printWriter.print(str);
            printWriter.print(" type=\"");
            printWriter.print(cls.getName());
            printWriter.print("\" UUID=\"");
            printWriter.print(((C0468GU) obj).mo18aq().toString());
            printWriter.println("\" />");
        } else if (I18NString.class == cls) {
            printWriter.print(str2);
            printWriter.print("<");
            printWriter.print(str);
            printWriter.print(" type=\"");
            printWriter.print(cls.getName());
            printWriter.println("\">");
            for (Map.Entry next : ((I18NString) obj).getMap().entrySet()) {
                printWriter.print(str2);
                printWriter.print("   <i18n lang=\"");
                printWriter.print((String) next.getKey());
                printWriter.print("\">");
                printWriter.print(m12198lu((String) next.getValue()));
                printWriter.println("</i18n>");
            }
            printWriter.print(str2);
            printWriter.print("</");
            printWriter.print(str);
            printWriter.println(">");
        } else if (!this.hqa.contains(cls) && !cls.isPrimitive() && !cls.isEnum()) {
            return false;
        } else {
            printWriter.print(str2);
            printWriter.print("<");
            printWriter.print(str);
            printWriter.print(" type=\"");
            printWriter.print(cls.getName());
            printWriter.print("\">");
            printWriter.print(m12198lu(m12187aE(obj)));
            printWriter.print("</");
            printWriter.print(str);
            printWriter.println(">");
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0205  */
    @p001a.C0064Am(aul = "0c961fc73fc7bbd8865899a6cb22335d", aum = 0)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.StringBuffer cPB() {
        /*
            r15 = this;
            r4 = 0
            r3 = 1
            r0 = 0
            java.util.HashSet r1 = new java.util.HashSet
            r1.<init>()
            r15.hqa = r1
            r1 = 14
            java.lang.Class[] r1 = new java.lang.Class[r1]
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            r1[r0] = r2
            java.lang.Class<a.aJR> r2 = p001a.aJR.class
            r1[r3] = r2
            r2 = 2
            java.lang.Class<com.hoplon.geometry.Vec3f> r3 = com.hoplon.geometry.Vec3f.class
            r1[r2] = r3
            r2 = 3
            java.lang.Class<a.aJF> r3 = p001a.aJF.class
            r1[r2] = r3
            r2 = 4
            java.lang.Class<a.aoY> r3 = p001a.C6520aoY.class
            r1[r2] = r3
            r2 = 5
            java.lang.Class<taikodom.geom.Orientation> r3 = taikodom.geom.Orientation.class
            r1[r2] = r3
            r2 = 6
            java.lang.Class<java.lang.Class> r3 = java.lang.Class.class
            r1[r2] = r3
            r2 = 7
            java.lang.Class<a.wO> r3 = p001a.C3892wO.class
            r1[r2] = r3
            r2 = 8
            java.lang.Class<java.lang.Integer> r3 = java.lang.Integer.class
            r1[r2] = r3
            r2 = 9
            java.lang.Class<a.YI> r3 = p001a.C1649YI.class
            r1[r2] = r3
            r2 = 10
            java.lang.Class<com.hoplon.geometry.Color> r3 = com.hoplon.geometry.Color.class
            r1[r2] = r3
            r2 = 11
            java.lang.Class<java.lang.Float> r3 = java.lang.Float.class
            r1[r2] = r3
            r2 = 12
            java.lang.Class<java.lang.Double> r3 = java.lang.Double.class
            r1[r2] = r3
            r2 = 13
            java.lang.Class<java.lang.Boolean> r3 = java.lang.Boolean.class
            r1[r2] = r3
            int r2 = r1.length
        L_0x0059:
            if (r0 < r2) goto L_0x00a0
            a.Zn$a r0 = new a.Zn$a
            r0.<init>(r15, r4)
            r15.hqc = r0
            r15.cPA()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.io.StringWriter r7 = new java.io.StringWriter
            r7.<init>()
            java.io.PrintWriter r8 = new java.io.PrintWriter
            r8.<init>(r7)
            java.lang.String r0 = "<contents>"
            r8.println(r0)
            java.util.Map<java.util.UUID, a.GU> r0 = r15.hqb
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r9 = r0.iterator()
        L_0x0083:
            boolean r0 = r9.hasNext()
            if (r0 != 0) goto L_0x00aa
            java.lang.String r0 = "</contents>"
            r8.println(r0)
            r8.close()
            java.util.Iterator r1 = r6.iterator()
        L_0x0095:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x048a
            java.lang.StringBuffer r0 = r7.getBuffer()
            return r0
        L_0x00a0:
            r3 = r1[r0]
            java.util.Set<java.lang.Class<?>> r5 = r15.hqa
            r5.add(r3)
            int r0 = r0 + 1
            goto L_0x0059
        L_0x00aa:
            java.lang.Object r0 = r9.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getValue()
            a.GU r1 = (logic.baa.C0468GU) r1
            java.lang.Class r3 = r1.getClass()
            java.lang.reflect.Method r2 = r15.m12188aE((java.lang.Class) r3)
            if (r2 == 0) goto L_0x0132
            r5 = 1
            r2.setAccessible(r5)     // Catch:{ Exception -> 0x0131 }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0131 }
            java.lang.Object r2 = r2.invoke(r1, r5)     // Catch:{ Exception -> 0x0131 }
            boolean r5 = r2 instanceof logic.baa.C0468GU     // Catch:{ Exception -> 0x0131 }
            if (r5 == 0) goto L_0x0132
            a.GU r2 = (logic.baa.C0468GU) r2     // Catch:{ Exception -> 0x0131 }
            java.util.UUID r2 = r2.mo18aq()     // Catch:{ Exception -> 0x0131 }
        L_0x00d5:
            java.lang.String r5 = "   <content class=\""
            r8.print(r5)
            java.lang.String r3 = r3.getName()
            r8.print(r3)
            java.lang.String r3 = "\" UUID=\""
            r8.print(r3)
            java.lang.Object r0 = r0.getKey()
            java.util.UUID r0 = (java.util.UUID) r0
            java.lang.String r0 = r0.toString()
            r8.print(r0)
            if (r2 == 0) goto L_0x0101
            java.lang.String r0 = "\" typeUUID=\""
            r8.print(r0)
            java.lang.String r0 = r2.toString()
            r8.print(r0)
        L_0x0101:
            java.lang.String r0 = "\">"
            r8.println(r0)
            java.util.Map r0 = r15.m12194i((logic.baa.C0468GU) r1)
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x0112:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x0134
            java.util.Map r0 = r15.m12191g(r1)
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r10 = r0.iterator()
        L_0x0124:
            boolean r0 = r10.hasNext()
            if (r0 != 0) goto L_0x0205
            java.lang.String r0 = "   </content>"
            r8.println(r0)
            goto L_0x0083
        L_0x0131:
            r2 = move-exception
        L_0x0132:
            r2 = r4
            goto L_0x00d5
        L_0x0134:
            java.lang.Object r0 = r3.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getValue()
            a.Wy r2 = (p001a.C1566Wy) r2
            java.util.Iterator r5 = r2.getIterator()
            java.lang.String r2 = "      <list name=\""
            r8.print(r2)
            java.lang.Object r2 = r0.getKey()
            java.lang.String r2 = (java.lang.String) r2
            r8.print(r2)
            java.lang.String r2 = "\">"
            r8.println(r2)
        L_0x0157:
            boolean r2 = r5.hasNext()
            if (r2 != 0) goto L_0x0163
            java.lang.String r0 = "      </list>"
            r8.println(r0)
            goto L_0x0112
        L_0x0163:
            java.lang.Object r2 = r5.next()
            java.lang.Class r10 = r2.getClass()
            java.lang.Class<a.GU> r11 = logic.baa.C0468GU.class
            boolean r11 = r11.isAssignableFrom(r10)
            if (r11 == 0) goto L_0x018b
            java.lang.String r10 = "         <entry UUID=\""
            r8.print(r10)
            a.GU r2 = (logic.baa.C0468GU) r2
            java.util.UUID r2 = r2.mo18aq()
            java.lang.String r2 = r2.toString()
            r8.print(r2)
            java.lang.String r2 = "\" />"
            r8.println(r2)
            goto L_0x0157
        L_0x018b:
            java.util.Set<java.lang.Class<?>> r11 = r15.hqa
            boolean r11 = r11.contains(r10)
            if (r11 != 0) goto L_0x019f
            boolean r11 = r10.isPrimitive()
            if (r11 != 0) goto L_0x019f
            boolean r11 = r10.isEnum()
            if (r11 == 0) goto L_0x01c1
        L_0x019f:
            java.lang.String r11 = "         <entry type=\""
            r8.print(r11)
            java.lang.String r10 = r10.getName()
            r8.print(r10)
            java.lang.String r10 = "\">"
            r8.print(r10)
            java.lang.String r2 = r15.m12187aE((java.lang.Object) r2)
            java.lang.String r2 = r15.m12198lu(r2)
            r8.print(r2)
            java.lang.String r2 = "</entry>"
            r8.println(r2)
            goto L_0x0157
        L_0x01c1:
            java.lang.Class<taikodom.infra.game.script.I18NString> r2 = taikodom.infra.game.script.I18NString.class
            if (r10 == r2) goto L_0x0157
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r11 = "Class '"
            r2.<init>(r11)
            java.lang.String r10 = r10.getName()
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r10 = "' não reconhecida! (class = '"
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.Class r10 = r1.getClass()
            java.lang.String r10 = r10.getName()
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r10 = "', field = '"
            java.lang.StringBuilder r10 = r2.append(r10)
            java.lang.Object r2 = r0.getKey()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.StringBuilder r2 = r10.append(r2)
            java.lang.String r10 = "')"
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r2 = r2.toString()
            r6.add(r2)
            goto L_0x0157
        L_0x0205:
            java.lang.Object r0 = r10.next()
            r3 = r0
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r0 = r3.getValue()
            a.Wy r0 = (p001a.C1566Wy) r0
            a.fm r2 = r0.bDt()
            java.lang.Class r5 = r2.getReturnType()
            a.aCE r11 = new a.aCE
            r0 = r1
            a.Xf r0 = (logic.baa.C1616Xf) r0
            r11.<init>(r0, r2)
            r0 = r1
            a.Xf r0 = (logic.baa.C1616Xf) r0
            java.lang.Object r2 = r0.mo14a((p001a.C0495Gr) r11)
            java.lang.Class<a.GU> r0 = logic.baa.C0468GU.class
            boolean r0 = r0.isAssignableFrom(r5)
            if (r0 == 0) goto L_0x0261
            java.lang.String r0 = "      <field name=\""
            r8.print(r0)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            r8.print(r0)
            java.lang.String r0 = "\" UUID=\""
            r8.print(r0)
            if (r2 != 0) goto L_0x0252
            java.lang.String r0 = "null"
            r8.print(r0)
        L_0x024b:
            java.lang.String r0 = "\" />"
            r8.println(r0)
            goto L_0x0124
        L_0x0252:
            r0 = r2
            a.GU r0 = (logic.baa.C0468GU) r0
            java.util.UUID r0 = r0.mo18aq()
            java.lang.String r0 = r0.toString()
            r8.print(r0)
            goto L_0x024b
        L_0x0261:
            java.util.Set<java.lang.Class<?>> r0 = r15.hqa
            boolean r0 = r0.contains(r5)
            if (r0 != 0) goto L_0x0275
            boolean r0 = r5.isPrimitive()
            if (r0 != 0) goto L_0x0275
            boolean r0 = r5.isEnum()
            if (r0 == 0) goto L_0x02a8
        L_0x0275:
            java.lang.String r0 = "      <field name=\""
            r8.print(r0)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            r8.print(r0)
            java.lang.String r0 = "\" type=\""
            r8.print(r0)
            java.lang.String r0 = r5.getName()
            r8.print(r0)
            java.lang.String r0 = "\">"
            r8.print(r0)
            if (r2 == 0) goto L_0x02a1
            java.lang.String r0 = r15.m12187aE((java.lang.Object) r2)
            java.lang.String r0 = r15.m12198lu(r0)
            r8.print(r0)
        L_0x02a1:
            java.lang.String r0 = "</field>"
            r8.println(r0)
            goto L_0x0124
        L_0x02a8:
            java.lang.Class<taikodom.infra.game.script.I18NString> r0 = taikodom.infra.game.script.I18NString.class
            if (r5 != r0) goto L_0x0312
            java.lang.String r0 = "      <field name=\""
            r8.print(r0)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            r8.print(r0)
            java.lang.String r0 = "\" type=\""
            r8.print(r0)
            java.lang.String r0 = r5.getName()
            r8.print(r0)
            java.lang.String r0 = "\">"
            r8.println(r0)
            taikodom.infra.game.script.I18NString r2 = (taikodom.infra.game.script.I18NString) r2
            java.util.Map r0 = r2.getMap()
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r3 = r0.iterator()
        L_0x02d9:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x02e6
            java.lang.String r0 = "      </field>"
            r8.println(r0)
            goto L_0x0124
        L_0x02e6:
            java.lang.Object r0 = r3.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.String r2 = "         <i18n lang=\""
            r8.print(r2)
            java.lang.Object r2 = r0.getKey()
            java.lang.String r2 = (java.lang.String) r2
            r8.print(r2)
            java.lang.String r2 = "\">"
            r8.print(r2)
            java.lang.Object r0 = r0.getValue()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r0 = r15.m12198lu(r0)
            r8.print(r0)
            java.lang.String r0 = "</i18n>"
            r8.println(r0)
            goto L_0x02d9
        L_0x0312:
            java.lang.Class<java.util.Map> r0 = java.util.Map.class
            boolean r0 = r0.isAssignableFrom(r5)
            if (r0 == 0) goto L_0x044a
            java.lang.String r0 = "      <map name=\""
            r8.print(r0)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            r8.print(r0)
            java.lang.String r0 = "\">"
            r8.println(r0)
            java.util.Map r2 = (java.util.Map) r2
            java.util.Set r0 = r2.entrySet()
            java.util.Iterator r11 = r0.iterator()
        L_0x0337:
            boolean r0 = r11.hasNext()
            if (r0 != 0) goto L_0x0344
            java.lang.String r0 = "      </map>"
            r8.println(r0)
            goto L_0x0124
        L_0x0344:
            java.lang.Object r0 = r11.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.String r2 = "         <mapEntry>"
            r8.println(r2)
            java.lang.Object r12 = r0.getKey()
            java.lang.Object r13 = r0.getValue()
            if (r12 != 0) goto L_0x0434
            java.lang.String r0 = "null"
            r5 = r0
        L_0x035c:
            if (r13 != 0) goto L_0x043f
            java.lang.String r0 = "null"
            r2 = r0
        L_0x0361:
            java.lang.String r0 = "key"
            java.lang.String r14 = "            "
            boolean r0 = r15.m12189b(r8, r0, r12, r14)
            if (r0 != 0) goto L_0x03c7
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r12 = "Map Key Class '"
            r0.<init>(r12)
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r5 = "' não reconhecida! (class = '"
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.Class r5 = r1.getClass()
            java.lang.String r5 = r5.getName()
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r5 = "', field = '"
            java.lang.StringBuilder r5 = r0.append(r5)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r5 = "') (class = '"
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.Class r5 = r1.getClass()
            java.lang.String r5 = r5.getName()
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r5 = "', field = '"
            java.lang.StringBuilder r5 = r0.append(r5)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r5 = "')"
            java.lang.StringBuilder r0 = r0.append(r5)
            java.lang.String r0 = r0.toString()
            r6.add(r0)
        L_0x03c7:
            java.lang.String r0 = "value"
            java.lang.String r5 = "            "
            boolean r0 = r15.m12189b(r8, r0, r13, r5)
            if (r0 != 0) goto L_0x042d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r5 = "Map Value Class '"
            r0.<init>(r5)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "' não reconhecida! (class = '"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.Class r2 = r1.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "', field = '"
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "') (class = '"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.Class r2 = r1.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "', field = '"
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "')"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r6.add(r0)
        L_0x042d:
            java.lang.String r0 = "         </mapEntry>"
            r8.println(r0)
            goto L_0x0337
        L_0x0434:
            java.lang.Class r0 = r12.getClass()
            java.lang.String r0 = r0.getName()
            r5 = r0
            goto L_0x035c
        L_0x043f:
            java.lang.Class r0 = r13.getClass()
            java.lang.String r0 = r0.getName()
            r2 = r0
            goto L_0x0361
        L_0x044a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "Class '"
            r0.<init>(r2)
            java.lang.String r2 = r5.getName()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "' não reconhecida! (class = '"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.Class r2 = r1.getClass()
            java.lang.String r2 = r2.getName()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = "', field = '"
            java.lang.StringBuilder r2 = r0.append(r2)
            java.lang.Object r0 = r3.getKey()
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "')"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r6.add(r0)
            goto L_0x0124
        L_0x048a:
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            r15.mo6317hy(r0)
            goto L_0x0095
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1745Zn.cPB():java.lang.StringBuffer");
    }

    @C0064Am(aul = "d5712e1d42f5bf36afeccb58a9d3d47f", aum = 0)
    /* renamed from: aB */
    private Method m12183aB(Class cls) {
        Method method = null;
        try {
            method = cls.getDeclaredMethod("setUUID", new Class[]{UUID.class});
        } catch (Exception e) {
        }
        if (method != null || cls.getSuperclass() == null) {
            return method;
        }
        return m12184aC(cls.getSuperclass());
    }

    @C0064Am(aul = "e65f9eab8501e9b49591f0236f565ae3", aum = 0)
    /* renamed from: aD */
    private Method m12186aD(Class cls) {
        Method method = null;
        try {
            method = cls.getDeclaredMethod("getType", new Class[0]);
        } catch (Exception e) {
        }
        if (method != null || cls.getSuperclass() == null) {
            return method;
        }
        return m12188aE(cls.getSuperclass());
    }

    @C0064Am(aul = "4f15f66049d38160b1539cbf192d44d6", aum = 0)
    /* renamed from: i */
    private Object m12193i(XmlNode agy) {
        boolean z;
        Object obj;
        Object obj2 = null;
        String attribute = agy.getAttribute("type");
        if (I18NString.class.getName().equals(attribute)) {
            List<XmlNode> children = agy.getListChildrenTag();
            I18NString i18NString = new I18NString();
            for (XmlNode next : children) {
                if ("i18n".equals(next.getTagName())) {
                    i18NString.set(next.getAttribute("lang"), next.getContent());
                }
            }
            return i18NString;
        }
        String content = agy.getContent();
        if (String.class.getName().equals(attribute)) {
            return content == null ? "" : content;
        }
        if (Integer.class.getName().equals(attribute) || "int".equals(attribute)) {
            return Integer.valueOf(Integer.parseInt(content));
        }
        if (Long.class.getName().equals(attribute) || "long".equals(attribute)) {
            return Long.valueOf(Long.parseLong(content));
        }
        if (Boolean.class.getName().equals(attribute) || "boolean".equals(attribute)) {
            return Boolean.valueOf(Boolean.parseBoolean(content));
        }
        if (Float.class.getName().equals(attribute) || "float".equals(attribute)) {
            return Float.valueOf(Float.parseFloat(content));
        }
        if (Short.class.getName().equals(attribute) || "short".equals(attribute)) {
            return Short.valueOf(Short.parseShort(content));
        }
        if (Double.class.getName().equals(attribute) || "double".equals(attribute)) {
            return Double.valueOf(Double.parseDouble(content));
        }
        if (Vec3d.class.getName().equals(attribute)) {
            return C3803vB.m40159cs(content);
        }
        if (Vec3f.class.getName().equals(attribute)) {
            return C0928Nc.m7697eP(content);
        }
        if (Color.class.getName().equals(attribute)) {
            return aTO.m18352nG(content);
        }
        if (Quat4fWrap.class.getName().equals(attribute)) {
            return C3352qa.m37725be(content);
        }
        if (Orientation.class.getName().equals(attribute)) {
            Quat4fWrap be = C3352qa.m37725be(content);
            if (be != null) {
                return new Orientation(be);
            }
            return null;
        } else if (Class.class.getName().equals(attribute)) {
            return C3360qg.m37768bf(content);
        } else {
            if (C3892wO.class.getName().equals(attribute)) {
                return C3614sy.m39219br(content);
            }
            if (C1649YI.class.getName().equals(attribute)) {
                return C0987OX.m8086gh(content);
            }
            try {
                Class<?> cls = Class.forName(attribute);
                if (!cls.isEnum()) {
                    z = false;
                    obj = null;
                } else if ("null".equals(content) || "".equals(content) || content == null) {
                    z = true;
                    obj = null;
                } else {
                    Object[] enumConstants = cls.getEnumConstants();
                    int length = enumConstants.length;
                    int i = 0;
                    while (true) {
                        if (i >= length) {
                            break;
                        }
                        Object obj3 = enumConstants[i];
                        if (((Enum) obj3).name().equals(content)) {
                            obj2 = obj3;
                            break;
                        }
                        i++;
                    }
                    if (obj2 == null) {
                        z = false;
                        obj = obj2;
                    } else {
                        z = true;
                        obj = obj2;
                    }
                }
            } catch (Exception e) {
                z = false;
                obj = null;
            }
            if (z) {
                return obj;
            }
            mo8358lY("Unrecognized type: " + attribute + " (value = " + content + ")");
            return obj;
        }
    }

    @C0064Am(aul = "ba2acd51170b15864c5add19063631fd", aum = 0)
    /* renamed from: lv */
    private void m12199lv(String str) {
        C2491fm bDu;
        C0468GU gu;
        Class<?> cls;
        mo8359ma("==========================");
        mo8359ma("");
        mo8359ma("CONTENT IMPORTING STARTING");
        mo8359ma("");
        mo8359ma("==========================");
        try {
            cPA();
            C1492Vy vy = new C1492Vy(new StringReader(str));
            vy.bBf().getTagName();
            ArrayList<XmlNode> arrayList = new ArrayList<>();
            while (true) {
                XmlNode bBg = vy.bBg();
                if (bBg == null) {
                    break;
                }
                arrayList.add(bBg);
            }
            new HashMap();
            HashSet hashSet = new HashSet();
            for (XmlNode agy : arrayList) {
                if ("content".equals(agy.getTagName())) {
                    UUID fromString = UUID.fromString(agy.getAttribute("UUID"));
                    if (this.hqb.get(fromString) == null) {
                        hashSet.add(fromString);
                    }
                }
            }
            while (hashSet.size() > 0) {
                mo8359ma("Missing content count: " + hashSet.size());
                for (XmlNode agy2 : arrayList) {
                    if ("content".equals(agy2.getTagName())) {
                        UUID fromString2 = UUID.fromString(agy2.getAttribute("UUID"));
                        if (hashSet.contains(fromString2)) {
                            String attribute = agy2.getAttribute("typeUUID");
                            UUID uuid = null;
                            if (attribute != null) {
                                uuid = UUID.fromString(attribute);
                            }
                            if (uuid == null || this.hqb.get(uuid) != null) {
                                String attribute2 = agy2.getAttribute("class");
                                try {
                                    cls = Class.forName(attribute2);
                                } catch (Exception e) {
                                    cls = null;
                                }
                                if (cls == null) {
                                    mo8358lY("Class '" + attribute2 + "' is not present in this server");
                                    hashSet.remove(fromString2);
                                } else if (C0468GU.class.isAssignableFrom(cls)) {
                                    C0468GU gu2 = (C0468GU) ala().bFf().mo6866PM().mo3340T(cls);
                                    if (uuid != null) {
                                        C0468GU gu3 = this.hqb.get(uuid);
                                        Method declaredMethod = cls.getDeclaredMethod(C2821kb.arY, new Class[]{gu3.getClass()});
                                        declaredMethod.setAccessible(true);
                                        declaredMethod.invoke(gu2, new Object[]{gu3});
                                    } else {
                                        Method declaredMethod2 = cls.getDeclaredMethod(C2821kb.arY, new Class[0]);
                                        declaredMethod2.setAccessible(true);
                                        declaredMethod2.invoke(gu2, new Object[0]);
                                    }
                                    hashSet.remove(fromString2);
                                    Method aC = m12184aC(cls);
                                    try {
                                        aC.setAccessible(true);
                                        aC.invoke(gu2, new Object[]{fromString2});
                                    } catch (Exception e2) {
                                    }
                                    this.hqb.put(fromString2, gu2);
                                } else {
                                    mo8358lY("Class '" + attribute2 + "' is not TaikodomContent in this server");
                                    hashSet.remove(fromString2);
                                }
                            }
                        }
                    } else {
                        mo8358lY("Unrecognized tag: " + agy2.getTagName());
                    }
                }
            }
            this.hqc = new C1746a(this, (C1746a) null);
            for (XmlNode agy3 : arrayList) {
                if ("content".equals(agy3.getTagName())) {
                    String attribute3 = agy3.getAttribute("class");
                    String attribute4 = agy3.getAttribute("UUID");
                    C0468GU gu4 = this.hqb.get(UUID.fromString(attribute4));
                    if (gu4 != null) {
                        Map<String, C1566Wy> g = m12191g(gu4);
                        Map<String, C1566Wy> i = m12194i(gu4);
                        String name = gu4.getClass().getName();
                        if (name.equals(attribute3)) {
                            for (XmlNode next : agy3.getListChildrenTag()) {
                                String name2 = next.getTagName();
                                if ("field".equals(name2)) {
                                    String attribute5 = next.getAttribute("name");
                                    C1566Wy wy = g.get(attribute5);
                                    if (!(wy == null || (bDu = wy.bDu()) == null)) {
                                        String attribute6 = next.getAttribute("UUID");
                                        if (attribute6 != null) {
                                            boolean z = true;
                                            if (!"null".equals(attribute6)) {
                                                C0468GU gu5 = this.hqb.get(UUID.fromString(attribute6));
                                                if (gu5 == null) {
                                                    z = false;
                                                    mo6317hy("field not found: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute5);
                                                }
                                                gu = gu5;
                                            } else {
                                                gu = null;
                                            }
                                            if (z) {
                                                ((C1616Xf) gu4).mo14a((C0495Gr) new aCE((C1616Xf) gu4, bDu, new Object[]{gu}));
                                            }
                                        } else {
                                            try {
                                                ((C1616Xf) gu4).mo14a((C0495Gr) new aCE((C1616Xf) gu4, bDu, new Object[]{m12195j(next)}));
                                            } catch (Exception e3) {
                                            }
                                        }
                                    }
                                } else if ("map".equals(name2)) {
                                    String attribute7 = next.getAttribute("name");
                                    C1566Wy wy2 = g.get(attribute7);
                                    if (wy2 == null) {
                                        mo6317hy("No soField found: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute7);
                                    } else if (wy2.bDu() != null) {
                                        HashMap hashMap = new HashMap();
                                        Object obj = null;
                                        Object obj2 = null;
                                        for (XmlNode children : next.getListChildrenTag()) {
                                            for (XmlNode next2 : children.getListChildrenTag()) {
                                                if ("key".equals(next2.getTagName())) {
                                                    String attribute8 = next2.getAttribute("UUID");
                                                    if (attribute8 == null) {
                                                        obj = m12195j(next2);
                                                    } else if (!"null".equals(attribute8)) {
                                                        obj = this.hqb.get(UUID.fromString(attribute8));
                                                    }
                                                } else if ("value".equals(next2.getTagName())) {
                                                    String attribute9 = next2.getAttribute("UUID");
                                                    if (attribute9 == null) {
                                                        obj2 = m12195j(next2);
                                                    } else if (!"null".equals(attribute9)) {
                                                        obj2 = this.hqb.get(UUID.fromString(attribute9));
                                                    }
                                                }
                                            }
                                            if (obj == null || obj2 == null) {
                                                if (obj == null) {
                                                    mo6317hy("Null map key: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute7);
                                                }
                                                if (obj2 == null) {
                                                    mo6317hy("Null map value: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute7);
                                                }
                                            } else {
                                                hashMap.put(obj, obj2);
                                            }
                                        }
                                        ((C1616Xf) gu4).mo14a((C0495Gr) new aCE((C1616Xf) gu4, wy2.bDu(), new Object[]{hashMap}));
                                    } else {
                                        mo6317hy("No setter for map: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute7);
                                    }
                                } else if ("list".equals(name2)) {
                                    String attribute10 = next.getAttribute("name");
                                    C1566Wy wy3 = i.get(attribute10);
                                    if (wy3 != null) {
                                        C2491fm bDx = wy3.bDx();
                                        C2491fm bDy = wy3.bDy();
                                        Iterator<?> iterator = wy3.getIterator();
                                        if (bDx == null || bDy == null || iterator == null) {
                                            mo6317hy("Adder/remover/iterator missing: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute10);
                                        } else {
                                            ArrayList<Object> arrayList2 = new ArrayList<>();
                                            while (iterator.hasNext()) {
                                                arrayList2.add(iterator.next());
                                            }
                                            for (Object obj3 : arrayList2) {
                                                ((C1616Xf) gu4).mo14a((C0495Gr) new aCE((C1616Xf) gu4, bDy, new Object[]{obj3}));
                                            }
                                            for (XmlNode next3 : next.getListChildrenTag()) {
                                                if ("entry".equals(next3.getTagName())) {
                                                    String attribute11 = next3.getAttribute("UUID");
                                                    C0468GU gu6 = null;
                                                    if (attribute11 == null) {
                                                        gu6 = m12195j(next3);
                                                    } else if (!"null".equals(attribute11)) {
                                                        gu6 = this.hqb.get(UUID.fromString(attribute11));
                                                    }
                                                    if (gu6 != null) {
                                                        ((C1616Xf) gu4).mo14a((C0495Gr) new aCE((C1616Xf) gu4, bDx, new Object[]{gu6}));
                                                    } else {
                                                        mo6317hy("Null list entry: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute10);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        mo6317hy("No soField found: UUID = " + attribute4 + ", class = " + attribute3 + ", field = " + attribute10);
                                    }
                                }
                            }
                        } else {
                            mo8358lY("Object found, but wrong class; expected = '" + attribute3 + "', found = '" + name + "'");
                        }
                    }
                } else {
                    mo8358lY("Unrecognized tag: " + agy3.getTagName());
                }
            }
        } catch (Exception e4) {
            e4.printStackTrace();
        }
        mo8359ma("=====================");
        mo8359ma("");
        mo8359ma("CONTENT IMPORTING END");
        mo8359ma("");
        mo8359ma("=====================");
    }

    /* renamed from: a.Zn$a */
    private class C1746a implements C5437aJh {
        private C1746a() {
        }

        /* synthetic */ C1746a(ContentImporterExporter zn, C1746a aVar) {
            this();
        }

        /* renamed from: b */
        public Object mo7471b(C1616Xf xf, C0495Gr gr) {
            return mo7470a(xf, gr, false);
        }

        /* renamed from: a */
        public Object mo7470a(C1616Xf xf, C0495Gr gr, boolean z) {
            if (!z) {
                try {
                    if (!gr.aQK().mo7393ho()) {
                        return xf.mo14a(gr);
                    }
                } catch (Exception e) {
                    ContentImporterExporter.this.mo8354g("error calling " + gr, (Throwable) e);
                    return null;
                }
            }
            return ContentImporterExporter.this.ala().aJs().mo20025b(xf, gr);
        }

        /* renamed from: c */
        public Object mo7473c(C1616Xf xf, C0495Gr gr) {
            try {
                return ContentImporterExporter.this.ala().aJs().mo20025b(xf, gr);
            } catch (Exception e) {
                ContentImporterExporter.this.mo8354g("error calling " + gr, (Throwable) e);
                return null;
            }
        }

        /* renamed from: c */
        public aDJ mo7472c(C1616Xf xf, C2491fm fmVar, Class<? extends aDJ> cls) {
            try {
                return ContentImporterExporter.this.ala().aJs().mo20024b(xf, fmVar, cls);
            } catch (Exception e) {
                ContentImporterExporter.this.mo8354g("Error calling " + fmVar, (Throwable) e);
                return null;
            }
        }

        /* renamed from: c */
        public List<aDJ> mo7474c(C1616Xf xf, C2491fm fmVar, Class<? extends aDJ> cls, int i) {
            try {
                return ContentImporterExporter.this.ala().aJs().mo20026b(xf, fmVar, cls, i);
            } catch (Exception e) {
                ContentImporterExporter.this.mo8354g("Error calling " + fmVar, (Throwable) e);
                return null;
            }
        }
    }
}
