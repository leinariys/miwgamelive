package game.script;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1081Pp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aaZ  reason: case insensitive filesystem */
/* compiled from: a */
public class TemporaryFeatureBlock extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bkF = null;
    public static final C5663aRz eXA = null;
    public static final C2491fm eXB = null;
    public static final C2491fm eXC = null;
    public static final C5663aRz eXz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "83c09fc78ea61633fb95095e55a57ee0", aum = 0)
    private static boolean dQc;
    @C0064Am(aul = "3894f51eef4ff170a58313a94d979719", aum = 1)
    private static boolean dQd;

    static {
        m19528V();
    }

    public TemporaryFeatureBlock() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TemporaryFeatureBlock(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19528V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 3;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(TemporaryFeatureBlock.class, "83c09fc78ea61633fb95095e55a57ee0", i);
        eXz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TemporaryFeatureBlock.class, "3894f51eef4ff170a58313a94d979719", i2);
        eXA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 3)];
        C2491fm a = C4105zY.m41624a(TemporaryFeatureBlock.class, "356d85b95b0083029941032f8e6a9a3c", i4);
        bkF = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(TemporaryFeatureBlock.class, "6e59120ad13ae6c6735c772224cd9a13", i5);
        eXB = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(TemporaryFeatureBlock.class, "41f212f5dd692d243e726348528dc11b", i6);
        eXC = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TemporaryFeatureBlock.class, C1081Pp.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "356d85b95b0083029941032f8e6a9a3c", aum = 0)
    @C5566aOg
    private void adk() {
        throw new aWi(new aCE(this, bkF, new Object[0]));
    }

    private boolean bMU() {
        return bFf().mo5608dq().mo3201h(eXz);
    }

    private boolean bMV() {
        return bFf().mo5608dq().mo3201h(eXA);
    }

    /* renamed from: dT */
    private void m19529dT(boolean z) {
        bFf().mo5608dq().mo3153a(eXz, z);
    }

    /* renamed from: dU */
    private void m19530dU(boolean z) {
        bFf().mo5608dq().mo3153a(eXA, z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1081Pp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                adk();
                return null;
            case 1:
                return new Boolean(bMW());
            case 2:
                return new Boolean(bMY());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean bMX() {
        switch (bFf().mo6893i(eXB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eXB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eXB, new Object[0]));
                break;
        }
        return bMW();
    }

    public boolean bMZ() {
        switch (bFf().mo6893i(eXC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eXC, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eXC, new Object[0]));
                break;
        }
        return bMY();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public void reset() {
        switch (bFf().mo6893i(bkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                break;
        }
        adk();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "6e59120ad13ae6c6735c772224cd9a13", aum = 0)
    private boolean bMW() {
        return bMU();
    }

    @C0064Am(aul = "41f212f5dd692d243e726348528dc11b", aum = 0)
    private boolean bMY() {
        return bMV();
    }
}
