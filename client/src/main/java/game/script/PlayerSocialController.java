package game.script;

import game.network.message.externalizable.C6205aiV;
import game.network.message.externalizable.aCE;
import game.script.login.UserConnection;
import game.script.mail.MailBox;
import game.script.mail.MailMessage;
import game.script.player.Player;
import logic.aaa.C1506WA;
import logic.aaa.C2733jJ;
import logic.baa.*;
import logic.data.link.C1883ag;
import logic.data.mbean.C6334aku;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.List;

@C6968axh
@C6485anp
@C2499fr(mo18855qf = {"a728927d50d2b6c39992267b216644d0"})
@C5511aMd
/* renamed from: a.qh */
/* compiled from: a */
public class PlayerSocialController extends Controller implements C1616Xf {

    /* renamed from: Ny */
    public static final C2491fm f8945Ny = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: hF */
    public static final C2491fm f8947hF = null;
    public static final C2491fm hnO = null;
    public static final C2491fm hov = null;
    /* renamed from: hz */
    public static final C5663aRz f8948hz = null;
    public static final C2491fm ibA = null;
    public static final C2491fm ibB = null;
    public static final C2491fm ibC = null;
    public static final C2491fm ibD = null;
    public static final C2491fm ibE = null;
    public static final C2491fm ibF = null;
    public static final C2491fm ibG = null;
    public static final C5663aRz ibq = null;
    public static final C5663aRz ibr = null;
    public static final C2491fm ibs = null;
    public static final C2491fm ibt = null;
    public static final C2491fm ibu = null;
    public static final C2491fm ibv = null;
    public static final C2491fm ibw = null;
    public static final C2491fm ibx = null;
    public static final C2491fm iby = null;
    public static final C2491fm ibz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "75e36be5574842e10671efec78cdb62c", aum = 2)

    /* renamed from: P */
    private static Player f8946P;
    @C0064Am(aul = "85e86a9c0070f94f813aad8600da3e7c", aum = 0)
    private static MailBox ath;
    @C0064Am(aul = "e4487c6581eb85c13b12d2005df0f97e", aum = 1)
    private static MailBox atj;

    static {
        m37770V();
    }

    public PlayerSocialController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerSocialController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37770V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Controller._m_fieldCount + 3;
        _m_methodCount = Controller._m_methodCount + 22;
        int i = Controller._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(PlayerSocialController.class, "85e86a9c0070f94f813aad8600da3e7c", i);
        ibq = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerSocialController.class, "e4487c6581eb85c13b12d2005df0f97e", i2);
        ibr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerSocialController.class, "75e36be5574842e10671efec78cdb62c", i3);
        f8948hz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Controller._m_fields, (Object[]) _m_fields);
        int i5 = Controller._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 22)];
        C2491fm a = C4105zY.m41624a(PlayerSocialController.class, "072f0e3f90f20fb60fef46124e723598", i5);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerSocialController.class, "c20c60a613742f8c03b871bbaa11918c", i6);
        ibs = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerSocialController.class, "13229e3216c0bf8a1387ff4c9e829f3e", i7);
        ibt = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerSocialController.class, "4245e6184765401bd4cb7116e24dd6ea", i8);
        ibu = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerSocialController.class, "f23a2568f2ed341235f24e267c5f0d51", i9);
        ibv = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerSocialController.class, "ac8aef90890bd8736dc4bc8305fc25d5", i10);
        ibw = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerSocialController.class, "f8778dc24ac552fb9d140645e6b7ef0a", i11);
        ibx = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerSocialController.class, C1883ag.f4488hv, i12);
        hnO = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerSocialController.class, "38bfdb04772bbe65f5684ef18ccdb254", i13);
        iby = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerSocialController.class, "8a2a7ce20c28a52f632fca65da02ba45", i14);
        f8947hF = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerSocialController.class, "ba9172351c96aaac6237ca515d0ff546", i15);
        f8945Ny = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerSocialController.class, "3b7bdc41bdb7aba3a9121f4f9225b14a", i16);
        ibz = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerSocialController.class, "5826dd6f511d641685ca4f9b9b107318", i17);
        ibA = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerSocialController.class, "0e402f93173c4fee312c55e5c5d00105", i18);
        ibB = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(PlayerSocialController.class, "c8e5996720b225deebf3f7105fae953b", i19);
        ibC = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(PlayerSocialController.class, "4e4b979e089761be5e6a173d801fd1f1", i20);
        ibD = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(PlayerSocialController.class, "253345410e1c0672b1b863291ed1f85e", i21);
        ibE = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        C2491fm a18 = C4105zY.m41624a(PlayerSocialController.class, "be81068457aca8cd98330fde73cd8de4", i22);
        ibF = a18;
        fmVarArr[i22] = a18;
        int i23 = i22 + 1;
        C2491fm a19 = C4105zY.m41624a(PlayerSocialController.class, "3f0c58e6fa53e12c45d1851e3707e566", i23);
        _f_dispose_0020_0028_0029V = a19;
        fmVarArr[i23] = a19;
        int i24 = i23 + 1;
        C2491fm a20 = C4105zY.m41624a(PlayerSocialController.class, "ca47f3c6c0a801e31ca81969620e2447", i24);
        ibG = a20;
        fmVarArr[i24] = a20;
        int i25 = i24 + 1;
        C2491fm a21 = C4105zY.m41624a(PlayerSocialController.class, "509daa4f91088cb9b179bc71b1f4f01f", i25);
        hov = a21;
        fmVarArr[i25] = a21;
        int i26 = i25 + 1;
        C2491fm a22 = C4105zY.m41624a(PlayerSocialController.class, "2efe4c3f65ec76e18823e0ab26c0283b", i26);
        _f_onResurrect_0020_0028_0029V = a22;
        fmVarArr[i26] = a22;
        int i27 = i26 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Controller._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerSocialController.class, C6334aku.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m37771a(Player aku) {
        bFf().mo5608dq().mo3197f(f8948hz, aku);
    }

    @C5566aOg
    @C0064Am(aul = "ac8aef90890bd8736dc4bc8305fc25d5", aum = 0)
    @C2499fr
    /* renamed from: a */
    private boolean m37772a(List<Player> list, String str, String str2) {
        throw new aWi(new aCE(this, ibw, new Object[]{list, str, str2}));
    }

    @C0064Am(aul = "f23a2568f2ed341235f24e267c5f0d51", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private boolean m37775b(Player aku, String str, String str2) {
        throw new aWi(new aCE(this, ibv, new Object[]{aku, str, str2}));
    }

    /* renamed from: c */
    private void m37776c(MailBox afg) {
        bFf().mo5608dq().mo3197f(ibq, afg);
    }

    /* renamed from: d */
    private void m37778d(MailBox afg) {
        bFf().mo5608dq().mo3197f(ibr, afg);
    }

    /* renamed from: dG */
    private Player m37779dG() {
        return (Player) bFf().mo5608dq().mo3214p(f8948hz);
    }

    private MailBox dej() {
        return (MailBox) bFf().mo5608dq().mo3214p(ibq);
    }

    private MailBox dek() {
        return (MailBox) bFf().mo5608dq().mo3214p(ibr);
    }

    /* renamed from: j */
    private boolean m37784j(MailMessage asf) {
        switch (bFf().mo6893i(ibu)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ibu, new Object[]{asf}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ibu, new Object[]{asf}));
                break;
        }
        return m37783i(asf);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "3b7bdc41bdb7aba3a9121f4f9225b14a", aum = 0)
    @C2499fr
    /* renamed from: mM */
    private void m37785mM(String str) {
        throw new aWi(new aCE(this, ibz, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "5826dd6f511d641685ca4f9b9b107318", aum = 0)
    @C2499fr
    /* renamed from: mO */
    private void m37786mO(String str) {
        throw new aWi(new aCE(this, ibA, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "0e402f93173c4fee312c55e5c5d00105", aum = 0)
    @C2499fr
    /* renamed from: mQ */
    private void m37787mQ(String str) {
        throw new aWi(new aCE(this, ibB, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "c8e5996720b225deebf3f7105fae953b", aum = 0)
    @C2499fr
    /* renamed from: mS */
    private void m37788mS(String str) {
        throw new aWi(new aCE(this, ibC, new Object[]{str}));
    }

    @C0064Am(aul = "4e4b979e089761be5e6a173d801fd1f1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: mU */
    private C3362a m37789mU(String str) {
        throw new aWi(new aCE(this, ibD, new Object[]{str}));
    }

    @C0064Am(aul = "253345410e1c0672b1b863291ed1f85e", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: mW */
    private C3362a m37790mW(String str) {
        throw new aWi(new aCE(this, ibE, new Object[]{str}));
    }

    @C0064Am(aul = "f8778dc24ac552fb9d140645e6b7ef0a", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: r */
    private boolean m37793r(String str, String str2, String str3) {
        throw new aWi(new aCE(this, ibx, new Object[]{str, str2, str3}));
    }

    @C0064Am(aul = "be81068457aca8cd98330fde73cd8de4", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: t */
    private C3362a m37794t(String str, boolean z) {
        throw new aWi(new aCE(this, ibF, new Object[]{str, new Boolean(z)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6334aku(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Controller._m_methodCount) {
            case 0:
                return m37774au();
            case 1:
                return del();
            case 2:
                return den();
            case 3:
                return new Boolean(m37783i((MailMessage) args[0]));
            case 4:
                return new Boolean(m37775b((Player) args[0], (String) args[1], (String) args[2]));
            case 5:
                return new Boolean(m37772a((List) args[0], (String) args[1], (String) args[2]));
            case 6:
                return new Boolean(m37793r((String) args[0], (String) args[1], (String) args[2]));
            case 7:
                return new Boolean(m37792o((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 8:
                m37782i((I18NString) args[0], (Object[]) args[1]);
                return null;
            case 9:
                return m37780dK();
            case 10:
                m37791o((Player) args[0]);
                return null;
            case 11:
                m37785mM((String) args[0]);
                return null;
            case 12:
                m37786mO((String) args[0]);
                return null;
            case 13:
                m37787mQ((String) args[0]);
                return null;
            case 14:
                m37788mS((String) args[0]);
                return null;
            case 15:
                return m37789mU((String) args[0]);
            case 16:
                return m37790mW((String) args[0]);
            case 17:
                return m37794t((String) args[0], ((Boolean) args[1]).booleanValue());
            case 18:
                m37781fg();
                return null;
            case 19:
                m37777cz((Player) args[0]);
                return null;
            case 20:
                cNR();
                return null;
            case 21:
                m37773aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m37773aG();
    }

    @ClientOnly
    public void asy() {
        switch (bFf().mo6893i(hov)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hov, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hov, new Object[0]));
                break;
        }
        cNR();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public boolean mo21440b(List<Player> list, String str, String str2) {
        switch (bFf().mo6893i(ibw)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ibw, new Object[]{list, str, str2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ibw, new Object[]{list, str, str2}));
                break;
        }
        return m37772a(list, str, str2);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"a728927d50d2b6c39992267b216644d0"})
    @ClientOnly
    /* renamed from: bG */
    public void mo21441bG(Player aku) {
        switch (bFf().mo6893i(ibG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ibG, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ibG, new Object[]{aku}));
                break;
        }
        m37777cz(aku);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: c */
    public boolean mo21442c(Player aku, String str, String str2) {
        switch (bFf().mo6893i(ibv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ibv, new Object[]{aku, str, str2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ibv, new Object[]{aku, str, str2}));
                break;
        }
        return m37775b(aku, str, str2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: dL */
    public Player mo21443dL() {
        switch (bFf().mo6893i(f8947hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f8947hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8947hF, new Object[0]));
                break;
        }
        return m37780dK();
    }

    public MailBox dem() {
        switch (bFf().mo6893i(ibs)) {
            case 0:
                return null;
            case 2:
                return (MailBox) bFf().mo5606d(new aCE(this, ibs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ibs, new Object[0]));
                break;
        }
        return del();
    }

    public MailBox deo() {
        switch (bFf().mo6893i(ibt)) {
            case 0:
                return null;
            case 2:
                return (MailBox) bFf().mo5606d(new aCE(this, ibt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ibt, new Object[0]));
                break;
        }
        return den();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m37781fg();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: j */
    public void mo21446j(I18NString i18NString, Object... objArr) {
        switch (bFf().mo6893i(iby)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iby, new Object[]{i18NString, objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iby, new Object[]{i18NString, objArr}));
                break;
        }
        m37782i(i18NString, objArr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: mN */
    public void mo21447mN(String str) {
        switch (bFf().mo6893i(ibz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ibz, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ibz, new Object[]{str}));
                break;
        }
        m37785mM(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: mP */
    public void mo21448mP(String str) {
        switch (bFf().mo6893i(ibA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ibA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ibA, new Object[]{str}));
                break;
        }
        m37786mO(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: mR */
    public void mo21449mR(String str) {
        switch (bFf().mo6893i(ibB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ibB, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ibB, new Object[]{str}));
                break;
        }
        m37787mQ(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: mT */
    public void mo21450mT(String str) {
        switch (bFf().mo6893i(ibC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ibC, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ibC, new Object[]{str}));
                break;
        }
        m37788mS(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: mV */
    public C3362a mo21451mV(String str) {
        switch (bFf().mo6893i(ibD)) {
            case 0:
                return null;
            case 2:
                return (C3362a) bFf().mo5606d(new aCE(this, ibD, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, ibD, new Object[]{str}));
                break;
        }
        return m37789mU(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: mX */
    public C3362a mo21452mX(String str) {
        switch (bFf().mo6893i(ibE)) {
            case 0:
                return null;
            case 2:
                return (C3362a) bFf().mo5606d(new aCE(this, ibE, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, ibE, new Object[]{str}));
                break;
        }
        return m37790mW(str);
    }

    /* renamed from: p */
    public void mo21453p(Player aku) {
        switch (bFf().mo6893i(f8945Ny)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8945Ny, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8945Ny, new Object[]{aku}));
                break;
        }
        m37791o(aku);
    }

    @C5472aKq
    /* renamed from: p */
    public boolean mo21454p(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(hnO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hnO, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hnO, new Object[]{zt}));
                break;
        }
        return m37792o(zt);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: s */
    public boolean mo21455s(String str, String str2, String str3) {
        switch (bFf().mo6893i(ibx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ibx, new Object[]{str, str2, str3}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ibx, new Object[]{str, str2, str3}));
                break;
        }
        return m37793r(str, str2, str3);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m37774au();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: u */
    public C3362a mo21456u(String str, boolean z) {
        switch (bFf().mo6893i(ibF)) {
            case 0:
                return null;
            case 2:
                return (C3362a) bFf().mo5606d(new aCE(this, ibF, new Object[]{str, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, ibF, new Object[]{str, new Boolean(z)}));
                break;
        }
        return m37794t(str, z);
    }

    @C0064Am(aul = "072f0e3f90f20fb60fef46124e723598", aum = 0)
    /* renamed from: au */
    private String m37774au() {
        return "PlayerController: [" + mo21443dL().getName() + "]";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        MailBox afg = (MailBox) bFf().mo6865M(MailBox.class);
        afg.mo8774lo("inbox");
        m37776c(afg);
        MailBox afg2 = (MailBox) bFf().mo6865M(MailBox.class);
        afg2.mo8774lo("outbox");
        m37778d(afg2);
    }

    @C0064Am(aul = "c20c60a613742f8c03b871bbaa11918c", aum = 0)
    private MailBox del() {
        return dej();
    }

    @C0064Am(aul = "13229e3216c0bf8a1387ff4c9e829f3e", aum = 0)
    private MailBox den() {
        return dek();
    }

    @C0064Am(aul = "4245e6184765401bd4cb7116e24dd6ea", aum = 0)
    /* renamed from: i */
    private boolean m37783i(MailMessage asf) {
        for (Player dwU : asf.ctO()) {
            dwU.dwU().dem().mo8771f(asf);
        }
        dek().mo8771f(asf);
        return true;
    }

    @C0064Am(aul = "a728927d50d2b6c39992267b216644d0", aum = 0)
    @C5472aKq
    /* renamed from: o */
    private boolean m37792o(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            return false;
        }
        if (zt.bFq() == null) {
            return false;
        }
        if (zt.bFq().mo15388dL() == null) {
            return false;
        }
        return zt.bFq().mo15388dL() == mo21443dL();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "38bfdb04772bbe65f5684ef18ccdb254", aum = 0)
    @C2499fr
    /* renamed from: i */
    private void m37782i(I18NString i18NString, Object[] objArr) {
        ald().getEventManager().mo13975h(new C2733jJ(i18NString.get(), false, true, objArr));
    }

    @C0064Am(aul = "8a2a7ce20c28a52f632fca65da02ba45", aum = 0)
    /* renamed from: dK */
    private Player m37780dK() {
        return m37779dG();
    }

    @C0064Am(aul = "ba9172351c96aaac6237ca515d0ff546", aum = 0)
    /* renamed from: o */
    private void m37791o(Player aku) {
        m37771a(aku);
    }

    @C0064Am(aul = "3f0c58e6fa53e12c45d1851e3707e566", aum = 0)
    /* renamed from: fg */
    private void m37781fg() {
        if (bGX()) {
            try {
                asy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "ca47f3c6c0a801e31ca81969620e2447", aum = 0)
    @C2499fr(mo18855qf = {"a728927d50d2b6c39992267b216644d0"})
    /* renamed from: cz */
    private void m37777cz(Player aku) {
        getPlayer().mo14419f((C1506WA) new C6205aiV(C6205aiV.C1909a.CORP_HIRE, aku));
    }

    @C0064Am(aul = "509daa4f91088cb9b179bc71b1f4f01f", aum = 0)
    @ClientOnly
    private void cNR() {
    }

    @C0064Am(aul = "2efe4c3f65ec76e18823e0ab26c0283b", aum = 0)
    /* renamed from: aG */
    private void m37773aG() {
        super.mo70aH();
        if (dej() == null) {
            MailBox afg = (MailBox) bFf().mo6865M(MailBox.class);
            afg.mo8774lo("inbox");
            m37776c(afg);
        }
        if (dek() == null) {
            MailBox afg2 = (MailBox) bFf().mo6865M(MailBox.class);
            afg2.mo8774lo("outbox");
            m37778d(afg2);
        }
    }

    /* renamed from: a.qh$a */
    public enum C3362a {
        SUCCESS,
        PLAYER_NOT_FOUND_OR_DISCONNECTED,
        PLAYER_NOT_FOUND,
        PLAYER_IS_ALREADY_FRIEND,
        PLAYER_IS_YOURSELF
    }
}
