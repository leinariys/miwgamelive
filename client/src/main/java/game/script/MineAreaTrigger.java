package game.script;

import game.network.message.externalizable.aCE;
import game.script.mines.Mine;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.simulation.Space;
import logic.aaa.C2235dB;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6007aef;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;

@C6485anp
@C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
@C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
@C5511aMd
@C0909NL
/* renamed from: a.ok */
/* compiled from: a */
public class MineAreaTrigger extends Trigger implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aNm = null;
    public static final C2491fm aNn = null;
    public static final C2491fm aNo = null;
    public static final C2491fm awC = null;
    /* renamed from: uY */
    public static final C2491fm f8782uY = null;
    /* renamed from: vi */
    public static final C2491fm f8783vi = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "9b01b07deb2b19ca94dae10df2b241d4", aum = 0)
    private static Mine aNl = null;

    static {
        m36816V();
    }

    public MineAreaTrigger() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MineAreaTrigger(C5540aNg ang) {
        super(ang);
    }

    public MineAreaTrigger(Mine hSVar) {
        super((C5540aNg) null);
        super._m_script_init(hSVar);
    }

    /* renamed from: V */
    static void m36816V() {
        _m_fieldCount = Trigger._m_fieldCount + 1;
        _m_methodCount = Trigger._m_methodCount + 7;
        int i = Trigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(MineAreaTrigger.class, "9b01b07deb2b19ca94dae10df2b241d4", i);
        aNm = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Trigger._m_fields, (Object[]) _m_fields);
        int i3 = Trigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
        C2491fm a = C4105zY.m41624a(MineAreaTrigger.class, "379cd17d713b7a1a0b09989ae8c6d60e", i3);
        f8782uY = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(MineAreaTrigger.class, "794bfff851272149c472a6dec4571bbd", i4);
        awC = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(MineAreaTrigger.class, "c43b18f39971a31fb895c91aeb08a2da", i5);
        aNn = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(MineAreaTrigger.class, "78e1a16bb3b084ed85d9ead3c3eb883f", i6);
        aNo = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(MineAreaTrigger.class, "68ee1b3966bdab5e085f5723aef7cdcd", i7);
        f8783vi = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(MineAreaTrigger.class, "c42dbfbbf0d28bf6ec952ae2b4beb0dd", i8);
        _f_dispose_0020_0028_0029V = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(MineAreaTrigger.class, "425461d36bad647b95ef6bdfa2419665", i9);
        _f_onResurrect_0020_0028_0029V = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Trigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MineAreaTrigger.class, C6007aef.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private boolean m36813F(Actor cr) {
        switch (bFf().mo6893i(aNn)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aNn, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aNn, new Object[]{cr}));
                break;
        }
        return m36812E(cr);
    }

    /* renamed from: Sy */
    private Mine m36814Sy() {
        return (Mine) bFf().mo5608dq().mo3214p(aNm);
    }

    /* renamed from: a */
    private void m36817a(Mine hSVar) {
        bFf().mo5608dq().mo3197f(aNm, hSVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* access modifiers changed from: protected */
    /* renamed from: SA */
    public List<Actor> mo21033SA() {
        switch (bFf().mo6893i(aNo)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aNo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aNo, new Object[0]));
                break;
        }
        return m36815Sz();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6007aef(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Trigger._m_methodCount) {
            case 0:
                return m36819c((Space) args[0], ((Long) args[1]).longValue());
            case 1:
                m36822y((Actor) args[0]);
                return null;
            case 2:
                return new Boolean(m36812E((Actor) args[0]));
            case 3:
                return m36815Sz();
            case 4:
                return m36821it();
            case 5:
                m36820fg();
                return null;
            case 6:
                m36818aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m36818aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f8782uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f8782uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f8782uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m36819c(ea, j);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m36820fg();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f8783vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8783vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8783vi, new Object[0]));
                break;
        }
        return m36821it();
    }

    /* renamed from: z */
    public void mo7590z(Actor cr) {
        switch (bFf().mo6893i(awC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awC, new Object[]{cr}));
                break;
        }
        m36822y(cr);
    }

    /* renamed from: b */
    public void mo21034b(Mine hSVar) {
        super.mo10S();
        m36817a(hSVar);
    }

    @C0064Am(aul = "379cd17d713b7a1a0b09989ae8c6d60e", aum = 0)
    /* renamed from: c */
    private C0520HN m36819c(Space ea, long j) {
        if (bGY()) {
            mo988aj(m36814Sy().cLl());
        }
        C2750jU jUVar = new C2750jU(ea, this, mo958IL());
        jUVar.mo2449a(ea.cKn().mo5725a(j, (C2235dB) jUVar, 13));
        return jUVar;
    }

    @C0064Am(aul = "794bfff851272149c472a6dec4571bbd", aum = 0)
    /* renamed from: y */
    private void m36822y(Actor cr) {
        if (!isDisposed() && bGY() && !m36813F(cr) && !(cr instanceof Mine) && (cr instanceof aDA) && !(cr instanceof Turret) && !m36814Sy().mo19265zH()) {
            m36814Sy().mo8696f(mo21033SA());
        }
    }

    @C0064Am(aul = "c43b18f39971a31fb895c91aeb08a2da", aum = 0)
    /* renamed from: E */
    private boolean m36812E(Actor cr) {
        Ship al = m36814Sy().mo19266zJ().mo7855al();
        if (cr == al || al == null) {
            return true;
        }
        if ((al.agj() instanceof Player) && (cr instanceof Ship) && (((Ship) cr).agj() instanceof Player)) {
            Player aku = (Player) al.agj();
            if (aku.dxk() && aku.dxi().mo2426aV((Player) ((Ship) cr).agj())) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "78e1a16bb3b084ed85d9ead3c3eb883f", aum = 0)
    /* renamed from: Sz */
    private List<Actor> m36815Sz() {
        return mo965Zc().mo1900c(getPosition(), m36814Sy().mo19263zD());
    }

    @C0064Am(aul = "68ee1b3966bdab5e085f5723aef7cdcd", aum = 0)
    /* renamed from: it */
    private String m36821it() {
        return null;
    }

    @C0064Am(aul = "c42dbfbbf0d28bf6ec952ae2b4beb0dd", aum = 0)
    /* renamed from: fg */
    private void m36820fg() {
        m36817a((Mine) null);
        super.dispose();
    }

    @C0064Am(aul = "425461d36bad647b95ef6bdfa2419665", aum = 0)
    /* renamed from: aG */
    private void m36818aG() {
        super.mo70aH();
        if ((m36814Sy() != null && m36814Sy().isDisposed()) || m36814Sy() == null) {
            dispose();
        }
    }
}
