package game.script.map3d;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0884Mp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.dR */
/* compiled from: a */
public class ZoomLevel extends TaikodomObject implements C1616Xf {
    /* renamed from: AK */
    public static final C5663aRz f6518AK = null;
    /* renamed from: AM */
    public static final C5663aRz f6520AM = null;
    /* renamed from: AN */
    public static final C2491fm f6521AN = null;
    /* renamed from: AO */
    public static final C2491fm f6522AO = null;
    /* renamed from: AP */
    public static final C2491fm f6523AP = null;
    /* renamed from: AQ */
    public static final C2491fm f6524AQ = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c39f798bcd9377416bd9c4749a43235a", aum = 0)

    /* renamed from: AJ */
    private static C6785atd f6517AJ;
    @C0064Am(aul = "a8000c8db7b9c383ba486d5b859d7f4c", aum = 1)

    /* renamed from: AL */
    private static float f6519AL;

    static {
        m28949V();
    }

    public ZoomLevel() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ZoomLevel(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28949V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 4;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ZoomLevel.class, "c39f798bcd9377416bd9c4749a43235a", i);
        f6518AK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ZoomLevel.class, "a8000c8db7b9c383ba486d5b859d7f4c", i2);
        f6520AM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(ZoomLevel.class, "b261248c2f168bc8f6b2ed2273e2cfd4", i4);
        f6521AN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ZoomLevel.class, "121b55b420319cf8d16969da72c6f0a1", i5);
        f6522AO = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ZoomLevel.class, "8041d4a7a98f411cb83a4344bd11fd2f", i6);
        f6523AP = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ZoomLevel.class, "477bb91e76552b75c62b4da07852af9e", i7);
        f6524AQ = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ZoomLevel.class, C0884Mp.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m28947L(float f) {
        bFf().mo5608dq().mo3150a(f6520AM, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zoom")
    @C0064Am(aul = "477bb91e76552b75c62b4da07852af9e", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m28948M(float f) {
        throw new aWi(new aCE(this, f6524AQ, new Object[]{new Float(f)}));
    }

    /* renamed from: a */
    private void m28950a(C6785atd atd) {
        bFf().mo5608dq().mo3197f(f6518AK, atd);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "View Type")
    @C0064Am(aul = "121b55b420319cf8d16969da72c6f0a1", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m28951b(C6785atd atd) {
        throw new aWi(new aCE(this, f6522AO, new Object[]{atd}));
    }

    /* renamed from: km */
    private C6785atd m28952km() {
        return (C6785atd) bFf().mo5608dq().mo3214p(f6518AK);
    }

    /* renamed from: kn */
    private float m28953kn() {
        return bFf().mo5608dq().mo3211m(f6520AM);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0884Mp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m28954ko();
            case 1:
                m28951b((C6785atd) args[0]);
                return null;
            case 2:
                return new Float(m28955kq());
            case 3:
                m28948M(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "View Type")
    @C5566aOg
    /* renamed from: c */
    public void mo17805c(C6785atd atd) {
        switch (bFf().mo6893i(f6522AO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6522AO, new Object[]{atd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6522AO, new Object[]{atd}));
                break;
        }
        m28951b(atd);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zoom")
    public float getZoom() {
        switch (bFf().mo6893i(f6523AP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6523AP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6523AP, new Object[0]));
                break;
        }
        return m28955kq();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Zoom")
    @C5566aOg
    public void setZoom(float f) {
        switch (bFf().mo6893i(f6524AQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6524AQ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6524AQ, new Object[]{new Float(f)}));
                break;
        }
        m28948M(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "View Type")
    /* renamed from: kp */
    public C6785atd mo17807kp() {
        switch (bFf().mo6893i(f6521AN)) {
            case 0:
                return null;
            case 2:
                return (C6785atd) bFf().mo5606d(new aCE(this, f6521AN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6521AN, new Object[0]));
                break;
        }
        return m28954ko();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "View Type")
    @C0064Am(aul = "b261248c2f168bc8f6b2ed2273e2cfd4", aum = 0)
    /* renamed from: ko */
    private C6785atd m28954ko() {
        return m28952km();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Zoom")
    @C0064Am(aul = "8041d4a7a98f411cb83a4344bd11fd2f", aum = 0)
    /* renamed from: kq */
    private float m28955kq() {
        return m28953kn();
    }
}
