package game.script.map3d;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.aGZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import p001a.*;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.abb  reason: case insensitive filesystem */
/* compiled from: a */
public class Map3DDefaults extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4142bL = null;
    /* renamed from: bN */
    public static final C2491fm f4143bN = null;
    /* renamed from: bO */
    public static final C2491fm f4144bO = null;
    /* renamed from: bP */
    public static final C2491fm f4145bP = null;
    public static final C5663aRz eXF = null;
    public static final C2491fm eXG = null;
    public static final C2491fm eXH = null;
    public static final C2491fm eXI = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "69ddc0f3e46a6ca807f49808a5771109", aum = 1)

    /* renamed from: bK */
    private static UUID f4141bK;
    @C0064Am(aul = "065b1bd3642d98e938f073bb601577ff", aum = 0)
    private static C3438ra<ZoomLevel> eXE;

    static {
        m20026V();
    }

    public Map3DDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Map3DDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20026V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Map3DDefaults.class, "065b1bd3642d98e938f073bb601577ff", i);
        eXF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Map3DDefaults.class, "69ddc0f3e46a6ca807f49808a5771109", i2);
        f4142bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(Map3DDefaults.class, "d3394206df8306095a638e90f3227428", i4);
        f4143bN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Map3DDefaults.class, "4dfcd2cacec5072b6ca11b33e2ca2a9f", i5);
        f4144bO = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Map3DDefaults.class, "884dff5601af2a5ea3f7ae79fe256088", i6);
        f4145bP = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Map3DDefaults.class, "e343a56339d78f2469acfcf715dbf545", i7);
        eXG = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Map3DDefaults.class, "f609004c0dbfb97041cdb79edc3cda14", i8);
        eXH = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(Map3DDefaults.class, "f15e018911a3819e9c2e93453f1aa2a6", i9);
        eXI = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Map3DDefaults.class, aGZ.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m20028a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4142bL, uuid);
    }

    /* renamed from: an */
    private UUID m20029an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4142bL);
    }

    /* renamed from: bJ */
    private void m20033bJ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eXF, raVar);
    }

    private C3438ra bNa() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eXF);
    }

    /* renamed from: c */
    private void m20035c(UUID uuid) {
        switch (bFf().mo6893i(f4144bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4144bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4144bO, new Object[]{uuid}));
                break;
        }
        m20032b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aGZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m20030ap();
            case 1:
                m20032b((UUID) args[0]);
                return null;
            case 2:
                return m20031ar();
            case 3:
                m20027a((ZoomLevel) args[0]);
                return null;
            case 4:
                m20034c((ZoomLevel) args[0]);
                return null;
            case 5:
                return bNb();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4143bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4143bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4143bN, new Object[0]));
                break;
        }
        return m20030ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Zoom Levels")
    /* renamed from: b */
    public void mo12491b(ZoomLevel dRVar) {
        switch (bFf().mo6893i(eXG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eXG, new Object[]{dRVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eXG, new Object[]{dRVar}));
                break;
        }
        m20027a(dRVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Zoom Levels")
    public List<ZoomLevel> bsn() {
        switch (bFf().mo6893i(eXI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, eXI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eXI, new Object[0]));
                break;
        }
        return bNb();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Zoom Levels")
    /* renamed from: d */
    public void mo12493d(ZoomLevel dRVar) {
        switch (bFf().mo6893i(eXH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eXH, new Object[]{dRVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eXH, new Object[]{dRVar}));
                break;
        }
        m20034c(dRVar);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4145bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4145bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4145bP, new Object[0]));
                break;
        }
        return m20031ar();
    }

    @C0064Am(aul = "d3394206df8306095a638e90f3227428", aum = 0)
    /* renamed from: ap */
    private UUID m20030ap() {
        return m20029an();
    }

    @C0064Am(aul = "4dfcd2cacec5072b6ca11b33e2ca2a9f", aum = 0)
    /* renamed from: b */
    private void m20032b(UUID uuid) {
        m20028a(uuid);
    }

    @C0064Am(aul = "884dff5601af2a5ea3f7ae79fe256088", aum = 0)
    /* renamed from: ar */
    private String m20031ar() {
        return "map_3d_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m20028a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Zoom Levels")
    @C0064Am(aul = "e343a56339d78f2469acfcf715dbf545", aum = 0)
    /* renamed from: a */
    private void m20027a(ZoomLevel dRVar) {
        bNa().add(dRVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Zoom Levels")
    @C0064Am(aul = "f609004c0dbfb97041cdb79edc3cda14", aum = 0)
    /* renamed from: c */
    private void m20034c(ZoomLevel dRVar) {
        bNa().remove(dRVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Zoom Levels")
    @C0064Am(aul = "f15e018911a3819e9c2e93453f1aa2a6", aum = 0)
    private List<ZoomLevel> bNb() {
        return MessageContainer.m16004w(bNa());
    }
}
