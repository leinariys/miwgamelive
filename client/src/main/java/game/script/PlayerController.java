package game.script;

import com.hoplon.geometry.Vec3f;
import game.engine.IEngineGame;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.*;
import game.script.advertise.AdvertiseManager;
import game.script.ai.PlayerArrivalController;
import game.script.ai.PlayerDirectionalController;
import game.script.ai.PlayerOrbitalController;
import game.script.citizenship.inprovements.InsuranceImprovement;
import game.script.hazardarea.HazardArea.HazardArea;
import game.script.item.Item;
import game.script.item.Shot;
import game.script.item.Weapon;
import game.script.login.UserConnection;
import game.script.nls.NLSInteraction;
import game.script.nls.NLSManager;
import game.script.nls.NLSWindowAlert;
import game.script.player.Player;
import game.script.player.PlayerSessionLog;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.Station;
import game.script.space.*;
import logic.EngineGame;
import logic.WrapRunnable;
import logic.aaa.*;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.link.C3875wA;
import logic.data.mbean.C0942Np;
import logic.data.mbean.C5480aKy;
import logic.render.IEngineGraphics;
import logic.res.ConfigManager;
import logic.res.ConfigManagerSection;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.sound.C0907NJ;
import logic.res.sound.SoundPlayer;
import logic.sql.C5878acG;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;
import taikodom.infra.script.I18NString;
import taikodom.render.camera.Camera;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RSpaceDust;
import taikodom.render.scene.SPitchedSet;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@C6485anp
@C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
@C5511aMd
/* renamed from: a.t */
/* compiled from: a */
public class PlayerController extends Controller implements C1616Xf, C3161oY.C3162a {

    /* renamed from: Ny */
    public static final C2491fm f9169Ny = null;
    /* renamed from: _f_appLog_0020_0028Ltaikodom_002fgame_002fscript_002flog_002fLogRecord_003b_0029V */
    public static final C2491fm f9171x75159c47 = null;
    public static final C2491fm _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_enterGate_0020_0028Ltaikodom_002fgame_002fscript_002fspace_002fGate_003b_0029V */
    public static final C2491fm f9172xcedc4f07 = null;
    public static final C2491fm _f_exitGateServer_0020_0028_0029V = null;
    public static final C2491fm _f_forceMaxSpeedOnClient_0020_0028_0029V = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f9173x851d656b = null;
    /* renamed from: _f_onActorSpawn_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029V */
    public static final C2491fm f9174x6b6d29da = null;
    /* renamed from: _f_onActorUnspawn_0020_0028Ltaikodom_002fgame_002fscript_002fActor_003b_0029V */
    public static final C2491fm f9175xa1dc95e1 = null;
    public static final C2491fm _f_onEnterCruiseSpeed_0020_0028_0029V = null;
    public static final C2491fm _f_onExitCruiseSpeed_0020_0028_0029V = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f9176x13860637 = null;
    public static final C2491fm _f_onPawnExplode_0020_0028_0029V = null;
    public static final C2491fm _f_onSimulationStatusUpdate_0020_0028_0029V = null;
    public static final C2491fm _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V = null;
    /* renamed from: _f_setPawn_0020_0028Ltaikodom_002fgame_002fscript_002fPawn_003b_0029V */
    public static final C2491fm f9177xbccafd70 = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aSi = null;
    public static final C2491fm aSj = null;
    public static final C2491fm brG = null;
    public static final C2491fm bsf = null;
    public static final C2491fm dTP = null;
    /* renamed from: gQ */
    public static final C2491fm f9178gQ = null;
    /* renamed from: hF */
    public static final C2491fm f9179hF = null;
    public static final C5663aRz hmZ = null;
    public static final C5663aRz hnB = null;
    public static final C2491fm hnG = null;
    public static final C2491fm hnH = null;
    public static final C2491fm hnI = null;
    public static final C2491fm hnJ = null;
    public static final C2491fm hnK = null;
    public static final C2491fm hnL = null;
    public static final C2491fm hnM = null;
    public static final C2491fm hnN = null;
    public static final C2491fm hnO = null;
    public static final C2491fm hnP = null;
    public static final C2491fm hnQ = null;
    public static final C2491fm hnR = null;
    public static final C2491fm hnS = null;
    public static final C2491fm hnT = null;
    public static final C2491fm hnU = null;
    public static final C2491fm hnV = null;
    public static final C2491fm hnW = null;
    public static final C2491fm hnX = null;
    public static final C2491fm hnY = null;
    public static final C2491fm hnZ = null;
    public static final C5663aRz hnl = null;
    public static final C2491fm hoA = null;
    public static final C2491fm hoB = null;
    public static final C2491fm hoC = null;
    public static final C2491fm hoD = null;
    public static final C2491fm hoE = null;
    public static final C2491fm hoF = null;
    public static final C2491fm hoG = null;
    public static final C2491fm hoH = null;
    public static final C2491fm hoI = null;
    public static final C2491fm hoJ = null;
    public static final C2491fm hoK = null;
    public static final C2491fm hoL = null;
    public static final C2491fm hoM = null;
    public static final C2491fm hoN = null;
    public static final C2491fm hoO = null;
    public static final C2491fm hoP = null;
    public static final C2491fm hoQ = null;
    public static final C2491fm hoR = null;
    public static final C2491fm hoS = null;
    public static final C2491fm hoT = null;
    public static final C2491fm hoU = null;
    public static final C2491fm hoV = null;
    public static final C2491fm hoW = null;
    public static final C2491fm hoX = null;
    public static final C2491fm hoY = null;
    public static final C2491fm hoZ = null;
    public static final C2491fm hoa = null;
    public static final C2491fm hob = null;
    public static final C2491fm hoc = null;
    public static final C2491fm hod = null;
    public static final C2491fm hoe = null;
    public static final C2491fm hof = null;
    public static final C2491fm hog = null;
    public static final C2491fm hoh = null;
    public static final C2491fm hoi = null;
    public static final C2491fm hoj = null;
    public static final C2491fm hok = null;
    public static final C2491fm hol = null;
    public static final C2491fm hom = null;
    public static final C2491fm hon = null;
    public static final C2491fm hoo = null;
    public static final C2491fm hop = null;
    public static final C2491fm hoq = null;
    public static final C2491fm hor = null;
    public static final C2491fm hos = null;
    public static final C2491fm hot = null;
    public static final C2491fm hou = null;
    public static final C2491fm hov = null;
    public static final C2491fm how = null;
    public static final C2491fm hox = null;
    public static final C2491fm hoy = null;
    public static final C2491fm hoz = null;
    public static final C2491fm hpA = null;
    public static final C2491fm hpB = null;
    public static final C2491fm hpC = null;
    public static final C2491fm hpD = null;
    public static final C2491fm hpE = null;
    public static final C2491fm hpF = null;
    public static final C2491fm hpG = null;
    public static final C2491fm hpH = null;
    public static final C2491fm hpI = null;
    public static final C2491fm hpJ = null;
    public static final C2491fm hpK = null;
    public static final C2491fm hpL = null;
    public static final C2491fm hpM = null;
    public static final C2491fm hpN = null;
    public static final C2491fm hpO = null;
    public static final C2491fm hpP = null;
    public static final C2491fm hpQ = null;
    public static final C2491fm hpR = null;
    public static final C2491fm hpS = null;
    public static final C2491fm hpT = null;
    public static final C2491fm hpU = null;
    public static final C2491fm hpV = null;
    public static final C2491fm hpW = null;
    public static final C2491fm hpX = null;
    public static final C2491fm hpY = null;
    public static final C2491fm hpZ = null;
    public static final C2491fm hpa = null;
    public static final C2491fm hpb = null;
    public static final C2491fm hpc = null;
    public static final C2491fm hpd = null;
    public static final C2491fm hpe = null;
    public static final C2491fm hpf = null;
    public static final C2491fm hpg = null;
    public static final C2491fm hph = null;
    public static final C2491fm hpi = null;
    public static final C2491fm hpj = null;
    public static final C2491fm hpk = null;
    public static final C2491fm hpl = null;
    public static final C2491fm hpm = null;
    public static final C2491fm hpn = null;
    public static final C2491fm hpo = null;
    public static final C2491fm hpp = null;
    public static final C2491fm hpq = null;
    public static final C2491fm hpr = null;
    public static final C2491fm hps = null;
    public static final C2491fm hpt = null;
    public static final C2491fm hpu = null;
    public static final C2491fm hpv = null;
    public static final C2491fm hpw = null;
    public static final C2491fm hpx = null;
    public static final C2491fm hpy = null;
    public static final C2491fm hpz = null;
    /* renamed from: hz */
    public static final C5663aRz f9180hz = null;
    /* access modifiers changed from: private */
    public static final Log logger = LogPrinter.setClass(PlayerController.class);
    public static final long serialVersionUID = 0;
    /* renamed from: ui */
    public static final C5663aRz f9181ui = null;
    static final transient float hmW = 394.0f;
    private static final float hmR = 2000.0f;
    private static final float hmS = 4000000.0f;
    private static final float hmT = 1000000.0f;
    private static final float hmU = 4000000.0f;
    private static final float hmV = 20.0f;
    private static final float hmX = 0.1f;
    private static final long hnC = 3000;
    private static final long hnD = 200;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "9d5a50210ebb737266b0441da9a2d3b4", aum = 3)

    /* renamed from: P */
    private static Player f9170P = null;
    @C0064Am(aul = "6b10335f32b8bbfe548286ab72710eb8", aum = 2)
    private static Node aSh = null;
    @C0064Am(aul = "e0f480d9e616b3f406a40316ee7f1449", aum = 0)
    @C5256aCi
    private static boolean hmY = false;
    @C0064Am(aul = "9a173376cd982cbc2c205ac4e20f7678", aum = 4)
    @C5566aOg
    private static boolean hnA = false;
    @C0064Am(aul = "9e5deb0b7a8f45a429f32cf09c596ab1", aum = 1)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static Actor hnk;

    static {
        m39226V();
    }

    private final transient long hnh;
    /* access modifiers changed from: private */
    @ClientOnly
    public transient RSpaceDust hnp;
    /* access modifiers changed from: private */
    @ClientOnly
    public transient SceneObject hnt;
    transient float fjY;
    transient float fjZ;
    transient int fkb;
    transient int fkc;
    @ClientOnly
    transient WrapRunnable fkk;
    transient float hnn;
    transient float hno;
    @ClientOnly
    transient C5964ado hnq;
    transient int screenHeight;
    transient int screenWidth;
    @ClientOnly
    private boolean bHv;
    @ClientOnly
    private transient SPitchedSet boA;
    @ClientOnly
    private boolean hnE;
    @ClientOnly
    private long hnF;
    @ClientOnly
    private transient AdvertiseManager hna;
    private transient boolean hnb;
    private transient boolean hnc;
    @ClientOnly
    private C6483ann hnd;
    @ClientOnly
    private C6483ann hne;
    @ClientOnly
    private C6483ann hnf;
    @ClientOnly
    private C6483ann hng;
    private transient long hni;
    private transient boolean hnj;
    @ClientOnly
    private transient Node hnm;
    @ClientOnly
    private int hnr;
    @ClientOnly
    private float hns;
    @ClientOnly
    private transient boolean hnu;
    @ClientOnly
    private transient boolean hnv;
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @ClientOnly
    private boolean hnw;
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @ClientOnly
    private boolean hnx;
    @ClientOnly
    private C2540gZ hny;
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @ClientOnly
    private RulesOverride hnz;

    public PlayerController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39226V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Controller._m_fieldCount + 5;
        _m_methodCount = Controller._m_methodCount + 150;
        int i = Controller._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(PlayerController.class, "e0f480d9e616b3f406a40316ee7f1449", i);
        hmZ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerController.class, "9e5deb0b7a8f45a429f32cf09c596ab1", i2);
        hnl = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerController.class, "6b10335f32b8bbfe548286ab72710eb8", i3);
        f9181ui = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PlayerController.class, "9d5a50210ebb737266b0441da9a2d3b4", i4);
        f9180hz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PlayerController.class, "9a173376cd982cbc2c205ac4e20f7678", i5);
        hnB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Controller._m_fields, (Object[]) _m_fields);
        int i7 = Controller._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 150)];
        C2491fm a = C4105zY.m41624a(PlayerController.class, "13685d499dd2450b0e5756084b583f69", i7);
        hnG = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerController.class, "9c58c508beff58b705a902978ef0eae5", i8);
        hnH = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerController.class, "c5a02f8741f6f295ab380d1611554d0f", i9);
        hnI = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerController.class, "50558df5f67dc30e1b9ff309221b5a37", i10);
        hnJ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerController.class, "49a8d63134faa5da456382923dc53c65", i11);
        hnK = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerController.class, "56376a9daa04254e66c5134bdf21dd4a", i12);
        hnL = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerController.class, "9413a73feac2c363619925bd7d4515f5", i13);
        hnM = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerController.class, "3ac07d8b71b592a42d648b43671f1d04", i14);
        _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerController.class, "101398c3106eea3e2497ba756a5439be", i15);
        hnN = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerController.class, C3875wA.f9460hv, i16);
        hnO = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerController.class, "fb74d92435ab1ef93ad152f9136c304b", i17);
        hnP = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerController.class, "d707d6da0a5a52d0f565241632249959", i18);
        hnQ = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerController.class, "6fb798fde5dce3dd948ab3cdd7fa80f2", i19);
        hnR = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerController.class, "2ecc620d9438a4a9e00da27dfc3d1392", i20);
        hnS = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(PlayerController.class, "0d670a1a46c7aa271e77d7b68319c83e", i21);
        hnT = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(PlayerController.class, "0977464b14a61adaec59e6cec3ad6df4", i22);
        hnU = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        C2491fm a17 = C4105zY.m41624a(PlayerController.class, "5cc27447c4718e5d2a02b24945428d6d", i23);
        _f_dispose_0020_0028_0029V = a17;
        fmVarArr[i23] = a17;
        int i24 = i23 + 1;
        C2491fm a18 = C4105zY.m41624a(PlayerController.class, "7ac0f0b15eed0f9292ef9bbe32621c78", i24);
        hnV = a18;
        fmVarArr[i24] = a18;
        int i25 = i24 + 1;
        C2491fm a19 = C4105zY.m41624a(PlayerController.class, "21a75effd38a8f32e48fd73f1224cfc3", i25);
        hnW = a19;
        fmVarArr[i25] = a19;
        int i26 = i25 + 1;
        C2491fm a20 = C4105zY.m41624a(PlayerController.class, "7c4156f5d04fdd5bee896da912d40dcc", i26);
        f9172xcedc4f07 = a20;
        fmVarArr[i26] = a20;
        int i27 = i26 + 1;
        C2491fm a21 = C4105zY.m41624a(PlayerController.class, "0ef35c4b927e1479ee784a227a70b8c6", i27);
        hnX = a21;
        fmVarArr[i27] = a21;
        int i28 = i27 + 1;
        C2491fm a22 = C4105zY.m41624a(PlayerController.class, "1bcac4df1490e16de993f058133859a8", i28);
        _f_exitGateServer_0020_0028_0029V = a22;
        fmVarArr[i28] = a22;
        int i29 = i28 + 1;
        C2491fm a23 = C4105zY.m41624a(PlayerController.class, "afb032f3547cfc131fce2e28efb1267b", i29);
        _f_forceMaxSpeedOnClient_0020_0028_0029V = a23;
        fmVarArr[i29] = a23;
        int i30 = i29 + 1;
        C2491fm a24 = C4105zY.m41624a(PlayerController.class, "17ef11877911147d73511411ba1b2970", i30);
        hnY = a24;
        fmVarArr[i30] = a24;
        int i31 = i30 + 1;
        C2491fm a25 = C4105zY.m41624a(PlayerController.class, "8684cfc74a01d2bdfec068878590e3d2", i31);
        hnZ = a25;
        fmVarArr[i31] = a25;
        int i32 = i31 + 1;
        C2491fm a26 = C4105zY.m41624a(PlayerController.class, "a29fc7b1066b6f533a1eb6a2e6917499", i32);
        hoa = a26;
        fmVarArr[i32] = a26;
        int i33 = i32 + 1;
        C2491fm a27 = C4105zY.m41624a(PlayerController.class, "32ff6a45801486b47d78182625418825", i33);
        hob = a27;
        fmVarArr[i33] = a27;
        int i34 = i33 + 1;
        C2491fm a28 = C4105zY.m41624a(PlayerController.class, "7acd6087f7974fdfc46ad419760c1539", i34);
        hoc = a28;
        fmVarArr[i34] = a28;
        int i35 = i34 + 1;
        C2491fm a29 = C4105zY.m41624a(PlayerController.class, "3fc33da54e63baa85be6f832d822690b", i35);
        hod = a29;
        fmVarArr[i35] = a29;
        int i36 = i35 + 1;
        C2491fm a30 = C4105zY.m41624a(PlayerController.class, "0237384706d6bb54fca554fec9a20173", i36);
        hoe = a30;
        fmVarArr[i36] = a30;
        int i37 = i36 + 1;
        C2491fm a31 = C4105zY.m41624a(PlayerController.class, "eb98826b586c828ee2e8738869aa088c", i37);
        hof = a31;
        fmVarArr[i37] = a31;
        int i38 = i37 + 1;
        C2491fm a32 = C4105zY.m41624a(PlayerController.class, "9d238bc22a55efb67e64257e24c1a311", i38);
        hog = a32;
        fmVarArr[i38] = a32;
        int i39 = i38 + 1;
        C2491fm a33 = C4105zY.m41624a(PlayerController.class, "88e28253330607cd79253070489f6995", i39);
        hoh = a33;
        fmVarArr[i39] = a33;
        int i40 = i39 + 1;
        C2491fm a34 = C4105zY.m41624a(PlayerController.class, "2d91c85fcb3a7b0868edfa1c2c1ecabc", i40);
        hoi = a34;
        fmVarArr[i40] = a34;
        int i41 = i40 + 1;
        C2491fm a35 = C4105zY.m41624a(PlayerController.class, "810a4bf6aeccc6cafb802c1428a2850b", i41);
        aSi = a35;
        fmVarArr[i41] = a35;
        int i42 = i41 + 1;
        C2491fm a36 = C4105zY.m41624a(PlayerController.class, "b2c093192588e5e28163ae0a8424b8ac", i42);
        f9179hF = a36;
        fmVarArr[i42] = a36;
        int i43 = i42 + 1;
        C2491fm a37 = C4105zY.m41624a(PlayerController.class, "bde89fae20cffbe30056d76609528fc0", i43);
        hoj = a37;
        fmVarArr[i43] = a37;
        int i44 = i43 + 1;
        C2491fm a38 = C4105zY.m41624a(PlayerController.class, "6bdb12ed0634a8d0f164bedd23413846", i44);
        hok = a38;
        fmVarArr[i44] = a38;
        int i45 = i44 + 1;
        C2491fm a39 = C4105zY.m41624a(PlayerController.class, "410e0f656784cb7913f05d4043ec8471", i45);
        f9173x851d656b = a39;
        fmVarArr[i45] = a39;
        int i46 = i45 + 1;
        C2491fm a40 = C4105zY.m41624a(PlayerController.class, "31bce84ad56bb0046db1646e9daf85ef", i46);
        hol = a40;
        fmVarArr[i46] = a40;
        int i47 = i46 + 1;
        C2491fm a41 = C4105zY.m41624a(PlayerController.class, "b5c0efc64c12454a287bac1456ba6155", i47);
        hom = a41;
        fmVarArr[i47] = a41;
        int i48 = i47 + 1;
        C2491fm a42 = C4105zY.m41624a(PlayerController.class, "f044ef2f747b694e5d047f330eb98b76", i48);
        hon = a42;
        fmVarArr[i48] = a42;
        int i49 = i48 + 1;
        C2491fm a43 = C4105zY.m41624a(PlayerController.class, "c68802a8e0e6db9d5d07ec7f9f475930", i49);
        hoo = a43;
        fmVarArr[i49] = a43;
        int i50 = i49 + 1;
        C2491fm a44 = C4105zY.m41624a(PlayerController.class, "524254350f21130222a97dafbaf1ad08", i50);
        hop = a44;
        fmVarArr[i50] = a44;
        int i51 = i50 + 1;
        C2491fm a45 = C4105zY.m41624a(PlayerController.class, "2adc6eb22b101533516fea06750d6ac5", i51);
        hoq = a45;
        fmVarArr[i51] = a45;
        int i52 = i51 + 1;
        C2491fm a46 = C4105zY.m41624a(PlayerController.class, "0b7a490b284a06fabddcb8efac04f6b9", i52);
        hor = a46;
        fmVarArr[i52] = a46;
        int i53 = i52 + 1;
        C2491fm a47 = C4105zY.m41624a(PlayerController.class, "858b50fefa288f2a24509d1d75df2b3e", i53);
        hos = a47;
        fmVarArr[i53] = a47;
        int i54 = i53 + 1;
        C2491fm a48 = C4105zY.m41624a(PlayerController.class, "addce871821ccfde79f6a84700e53c7c", i54);
        hot = a48;
        fmVarArr[i54] = a48;
        int i55 = i54 + 1;
        C2491fm a49 = C4105zY.m41624a(PlayerController.class, "32fb4379a37dd11ec441ac0b9b6cad30", i55);
        hou = a49;
        fmVarArr[i55] = a49;
        int i56 = i55 + 1;
        C2491fm a50 = C4105zY.m41624a(PlayerController.class, "464546dd6b5aebbabe07b6ff957b1fd2", i56);
        hov = a50;
        fmVarArr[i56] = a50;
        int i57 = i56 + 1;
        C2491fm a51 = C4105zY.m41624a(PlayerController.class, "8902bbd4e0a2696a4e81294e17ff5853", i57);
        how = a51;
        fmVarArr[i57] = a51;
        int i58 = i57 + 1;
        C2491fm a52 = C4105zY.m41624a(PlayerController.class, "b56d17042ed1cd5324fd7f8ce52d72d0", i58);
        hox = a52;
        fmVarArr[i58] = a52;
        int i59 = i58 + 1;
        C2491fm a53 = C4105zY.m41624a(PlayerController.class, "db21022b600db4ec5c54d322d509734b", i59);
        hoy = a53;
        fmVarArr[i59] = a53;
        int i60 = i59 + 1;
        C2491fm a54 = C4105zY.m41624a(PlayerController.class, "38cad9bfdcaa1d8567146d9fa8d50e22", i60);
        hoz = a54;
        fmVarArr[i60] = a54;
        int i61 = i60 + 1;
        C2491fm a55 = C4105zY.m41624a(PlayerController.class, "4d1cdc4d86b30a9e95cde7f1b684fc30", i61);
        hoA = a55;
        fmVarArr[i61] = a55;
        int i62 = i61 + 1;
        C2491fm a56 = C4105zY.m41624a(PlayerController.class, "66f654279a2245f161dbbc3375b10518", i62);
        hoB = a56;
        fmVarArr[i62] = a56;
        int i63 = i62 + 1;
        C2491fm a57 = C4105zY.m41624a(PlayerController.class, "f82ef2a5905ffde005fc34751068d84b", i63);
        f9174x6b6d29da = a57;
        fmVarArr[i63] = a57;
        int i64 = i63 + 1;
        C2491fm a58 = C4105zY.m41624a(PlayerController.class, "77f6f14468d8e8911dd54e2ea36a16c1", i64);
        f9175xa1dc95e1 = a58;
        fmVarArr[i64] = a58;
        int i65 = i64 + 1;
        C2491fm a59 = C4105zY.m41624a(PlayerController.class, "9d2b903b41ef6421ed610f5e41283a7e", i65);
        _f_onEnterCruiseSpeed_0020_0028_0029V = a59;
        fmVarArr[i65] = a59;
        int i66 = i65 + 1;
        C2491fm a60 = C4105zY.m41624a(PlayerController.class, "16a2d109509f438ff032716f68ad924d", i66);
        _f_onExitCruiseSpeed_0020_0028_0029V = a60;
        fmVarArr[i66] = a60;
        int i67 = i66 + 1;
        C2491fm a61 = C4105zY.m41624a(PlayerController.class, "152543439f187b3f0090fa538f4878c1", i67);
        hoC = a61;
        fmVarArr[i67] = a61;
        int i68 = i67 + 1;
        C2491fm a62 = C4105zY.m41624a(PlayerController.class, "512d27aecbed742113737bc50b179ba3", i68);
        _f_onPawnExplode_0020_0028_0029V = a62;
        fmVarArr[i68] = a62;
        int i69 = i68 + 1;
        C2491fm a63 = C4105zY.m41624a(PlayerController.class, "920f13c5871b1e665dd867bc9b900d95", i69);
        hoD = a63;
        fmVarArr[i69] = a63;
        int i70 = i69 + 1;
        C2491fm a64 = C4105zY.m41624a(PlayerController.class, "635a83ac2a042a9f8baddb748909c818", i70);
        hoE = a64;
        fmVarArr[i70] = a64;
        int i71 = i70 + 1;
        C2491fm a65 = C4105zY.m41624a(PlayerController.class, "63faa4d137abae7a69b9d568fa3b1c4d", i71);
        hoF = a65;
        fmVarArr[i71] = a65;
        int i72 = i71 + 1;
        C2491fm a66 = C4105zY.m41624a(PlayerController.class, "e6c2d0ffb4e94c21e7c4205388c83faf", i72);
        hoG = a66;
        fmVarArr[i72] = a66;
        int i73 = i72 + 1;
        C2491fm a67 = C4105zY.m41624a(PlayerController.class, "906b371745dc10aaaba9daae6629f158", i73);
        _f_onSimulationStatusUpdate_0020_0028_0029V = a67;
        fmVarArr[i73] = a67;
        int i74 = i73 + 1;
        C2491fm a68 = C4105zY.m41624a(PlayerController.class, "6b64fc13ae769aa2ff388d7258e1a260", i74);
        hoH = a68;
        fmVarArr[i74] = a68;
        int i75 = i74 + 1;
        C2491fm a69 = C4105zY.m41624a(PlayerController.class, "bd92a675f138fcb27c554a19d50f395f", i75);
        hoI = a69;
        fmVarArr[i75] = a69;
        int i76 = i75 + 1;
        C2491fm a70 = C4105zY.m41624a(PlayerController.class, "f7f4c25db72a9dd284b668e30b43fa4b", i76);
        hoJ = a70;
        fmVarArr[i76] = a70;
        int i77 = i76 + 1;
        C2491fm a71 = C4105zY.m41624a(PlayerController.class, "6ec98293e31be4f2f452ffca959c1896", i77);
        hoK = a71;
        fmVarArr[i77] = a71;
        int i78 = i77 + 1;
        C2491fm a72 = C4105zY.m41624a(PlayerController.class, "c33f8a2877f319e3a90040fc498990c4", i78);
        hoL = a72;
        fmVarArr[i78] = a72;
        int i79 = i78 + 1;
        C2491fm a73 = C4105zY.m41624a(PlayerController.class, "47049f32419ca6583e90c2a5a80f2167", i79);
        hoM = a73;
        fmVarArr[i79] = a73;
        int i80 = i79 + 1;
        C2491fm a74 = C4105zY.m41624a(PlayerController.class, "78122c2d22378605860a06a5ca838964", i80);
        hoN = a74;
        fmVarArr[i80] = a74;
        int i81 = i80 + 1;
        C2491fm a75 = C4105zY.m41624a(PlayerController.class, "5b6fb11ac6d49398d2dbe9ec0cb999b0", i81);
        hoO = a75;
        fmVarArr[i81] = a75;
        int i82 = i81 + 1;
        C2491fm a76 = C4105zY.m41624a(PlayerController.class, "ed0693fb8a06c24e4aad1fc852afe329", i82);
        hoP = a76;
        fmVarArr[i82] = a76;
        int i83 = i82 + 1;
        C2491fm a77 = C4105zY.m41624a(PlayerController.class, "4da52270230355d67b4601342a42c763", i83);
        hoQ = a77;
        fmVarArr[i83] = a77;
        int i84 = i83 + 1;
        C2491fm a78 = C4105zY.m41624a(PlayerController.class, "d07622762b2c8b3ef2bc7dd1db27b90e", i84);
        hoR = a78;
        fmVarArr[i84] = a78;
        int i85 = i84 + 1;
        C2491fm a79 = C4105zY.m41624a(PlayerController.class, "a4e64e231ea6a68b5519e1b7950cd124", i85);
        hoS = a79;
        fmVarArr[i85] = a79;
        int i86 = i85 + 1;
        C2491fm a80 = C4105zY.m41624a(PlayerController.class, "78d500bf03e56827d6bcf9da742a696c", i86);
        hoT = a80;
        fmVarArr[i86] = a80;
        int i87 = i86 + 1;
        C2491fm a81 = C4105zY.m41624a(PlayerController.class, "699277008ea4507a50848224e36fd99d", i87);
        hoU = a81;
        fmVarArr[i87] = a81;
        int i88 = i87 + 1;
        C2491fm a82 = C4105zY.m41624a(PlayerController.class, "799158d14507087fc12d60824b1f0098", i88);
        dTP = a82;
        fmVarArr[i88] = a82;
        int i89 = i88 + 1;
        C2491fm a83 = C4105zY.m41624a(PlayerController.class, "a240298661de77eb0146d1d8fbd347b0", i89);
        hoV = a83;
        fmVarArr[i89] = a83;
        int i90 = i89 + 1;
        C2491fm a84 = C4105zY.m41624a(PlayerController.class, "822a3bc90244191296024cc5ab2ee3d6", i90);
        hoW = a84;
        fmVarArr[i90] = a84;
        int i91 = i90 + 1;
        C2491fm a85 = C4105zY.m41624a(PlayerController.class, "bc45bd5497dbeed516c8f347c9aa90e2", i91);
        hoX = a85;
        fmVarArr[i91] = a85;
        int i92 = i91 + 1;
        C2491fm a86 = C4105zY.m41624a(PlayerController.class, "ce722566ac00b0dfcb16f7e6d667f209", i92);
        bsf = a86;
        fmVarArr[i92] = a86;
        int i93 = i92 + 1;
        C2491fm a87 = C4105zY.m41624a(PlayerController.class, "8d470ffa1fabc7dc275a571fcad9ba09", i93);
        hoY = a87;
        fmVarArr[i93] = a87;
        int i94 = i93 + 1;
        C2491fm a88 = C4105zY.m41624a(PlayerController.class, "9f08615fb6d1c7a17f9e6bc0c2d481f9", i94);
        hoZ = a88;
        fmVarArr[i94] = a88;
        int i95 = i94 + 1;
        C2491fm a89 = C4105zY.m41624a(PlayerController.class, "8ae6c946a9786deae4a83b6f79c07064", i95);
        hpa = a89;
        fmVarArr[i95] = a89;
        int i96 = i95 + 1;
        C2491fm a90 = C4105zY.m41624a(PlayerController.class, "548966f5c9d86b025ac0a2cb661bcd1f", i96);
        hpb = a90;
        fmVarArr[i96] = a90;
        int i97 = i96 + 1;
        C2491fm a91 = C4105zY.m41624a(PlayerController.class, "b7f58d624156b4afb5921598669880ab", i97);
        hpc = a91;
        fmVarArr[i97] = a91;
        int i98 = i97 + 1;
        C2491fm a92 = C4105zY.m41624a(PlayerController.class, "9f621a2dcb9b25830f99f33df0d3768a", i98);
        hpd = a92;
        fmVarArr[i98] = a92;
        int i99 = i98 + 1;
        C2491fm a93 = C4105zY.m41624a(PlayerController.class, "3a6f3039434058ccd0e7a3c029b6f290", i99);
        hpe = a93;
        fmVarArr[i99] = a93;
        int i100 = i99 + 1;
        C2491fm a94 = C4105zY.m41624a(PlayerController.class, "bcfc92323c026a828709f00c0abbac18", i100);
        hpf = a94;
        fmVarArr[i100] = a94;
        int i101 = i100 + 1;
        C2491fm a95 = C4105zY.m41624a(PlayerController.class, "ee1780b2dd39853506feffb5c0c08219", i101);
        hpg = a95;
        fmVarArr[i101] = a95;
        int i102 = i101 + 1;
        C2491fm a96 = C4105zY.m41624a(PlayerController.class, "a60796202eb2065ae5dac731d9c78a0c", i102);
        hph = a96;
        fmVarArr[i102] = a96;
        int i103 = i102 + 1;
        C2491fm a97 = C4105zY.m41624a(PlayerController.class, "4da59d4083ec8f3c1aecbd88c876dae2", i103);
        hpi = a97;
        fmVarArr[i103] = a97;
        int i104 = i103 + 1;
        C2491fm a98 = C4105zY.m41624a(PlayerController.class, "d8158954b06a7a9460e56c3955d63a3d", i104);
        hpj = a98;
        fmVarArr[i104] = a98;
        int i105 = i104 + 1;
        C2491fm a99 = C4105zY.m41624a(PlayerController.class, "e04446120bb12c0e0049be25b78c31a2", i105);
        hpk = a99;
        fmVarArr[i105] = a99;
        int i106 = i105 + 1;
        C2491fm a100 = C4105zY.m41624a(PlayerController.class, "e84d7bfcea17c04b9242b66085021945", i106);
        _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V = a100;
        fmVarArr[i106] = a100;
        int i107 = i106 + 1;
        C2491fm a101 = C4105zY.m41624a(PlayerController.class, "b9a2dc320eb49a64a532199afb3ec435", i107);
        hpl = a101;
        fmVarArr[i107] = a101;
        int i108 = i107 + 1;
        C2491fm a102 = C4105zY.m41624a(PlayerController.class, "e8f8b142d2fb197bcbadf2515955f3af", i108);
        aSj = a102;
        fmVarArr[i108] = a102;
        int i109 = i108 + 1;
        C2491fm a103 = C4105zY.m41624a(PlayerController.class, "563962de7e16a464cb1db9d147da670a", i109);
        f9177xbccafd70 = a103;
        fmVarArr[i109] = a103;
        int i110 = i109 + 1;
        C2491fm a104 = C4105zY.m41624a(PlayerController.class, "2135bec9bd3c51f6ed17af9b01529fda", i110);
        f9169Ny = a104;
        fmVarArr[i110] = a104;
        int i111 = i110 + 1;
        C2491fm a105 = C4105zY.m41624a(PlayerController.class, "ce2b4c31123600646c94def5bdd60103", i111);
        hpm = a105;
        fmVarArr[i111] = a105;
        int i112 = i111 + 1;
        C2491fm a106 = C4105zY.m41624a(PlayerController.class, "6c8a865257117c8171d0b18a5fa9f7d1", i112);
        hpn = a106;
        fmVarArr[i112] = a106;
        int i113 = i112 + 1;
        C2491fm a107 = C4105zY.m41624a(PlayerController.class, "e8d62ea71ce5e58cb96b53f4593f5a2f", i113);
        hpo = a107;
        fmVarArr[i113] = a107;
        int i114 = i113 + 1;
        C2491fm a108 = C4105zY.m41624a(PlayerController.class, "cc4d651469130702938d9563e84d413d", i114);
        hpp = a108;
        fmVarArr[i114] = a108;
        int i115 = i114 + 1;
        C2491fm a109 = C4105zY.m41624a(PlayerController.class, "b767efced38bf4bf827b09b3f0eb54f8", i115);
        hpq = a109;
        fmVarArr[i115] = a109;
        int i116 = i115 + 1;
        C2491fm a110 = C4105zY.m41624a(PlayerController.class, "618af9472b64bc091f8f7a2d4e131167", i116);
        hpr = a110;
        fmVarArr[i116] = a110;
        int i117 = i116 + 1;
        C2491fm a111 = C4105zY.m41624a(PlayerController.class, "0c5a99e6a657a505c5baf49aca3289ff", i117);
        hps = a111;
        fmVarArr[i117] = a111;
        int i118 = i117 + 1;
        C2491fm a112 = C4105zY.m41624a(PlayerController.class, "323203c279108e32fd89111664495fd0", i118);
        hpt = a112;
        fmVarArr[i118] = a112;
        int i119 = i118 + 1;
        C2491fm a113 = C4105zY.m41624a(PlayerController.class, "44294607fe9b0e8bc50e98c9105d4dce", i119);
        hpu = a113;
        fmVarArr[i119] = a113;
        int i120 = i119 + 1;
        C2491fm a114 = C4105zY.m41624a(PlayerController.class, "0e1d510043b7d217295392dec4cb46ae", i120);
        hpv = a114;
        fmVarArr[i120] = a114;
        int i121 = i120 + 1;
        C2491fm a115 = C4105zY.m41624a(PlayerController.class, "8511d4c9d2acd1cd9ab9fcced7aa2ca8", i121);
        hpw = a115;
        fmVarArr[i121] = a115;
        int i122 = i121 + 1;
        C2491fm a116 = C4105zY.m41624a(PlayerController.class, "e0b5b2d39a411cf6d72997a2d7862772", i122);
        hpx = a116;
        fmVarArr[i122] = a116;
        int i123 = i122 + 1;
        C2491fm a117 = C4105zY.m41624a(PlayerController.class, "c5ff8061833c0e462b150d17e20e7a8f", i123);
        hpy = a117;
        fmVarArr[i123] = a117;
        int i124 = i123 + 1;
        C2491fm a118 = C4105zY.m41624a(PlayerController.class, "0cdd10594c0f12d017c17b976c6a4c8a", i124);
        hpz = a118;
        fmVarArr[i124] = a118;
        int i125 = i124 + 1;
        C2491fm a119 = C4105zY.m41624a(PlayerController.class, "6c7076709f42f142541245881075c8a0", i125);
        hpA = a119;
        fmVarArr[i125] = a119;
        int i126 = i125 + 1;
        C2491fm a120 = C4105zY.m41624a(PlayerController.class, "f6b88353f829510f4d3d65c02d3e8f1f", i126);
        hpB = a120;
        fmVarArr[i126] = a120;
        int i127 = i126 + 1;
        C2491fm a121 = C4105zY.m41624a(PlayerController.class, "18db031867a6d52ca21cfbc104b6a2f2", i127);
        hpC = a121;
        fmVarArr[i127] = a121;
        int i128 = i127 + 1;
        C2491fm a122 = C4105zY.m41624a(PlayerController.class, "948bee6ca53b7cb1dfcccfb1e32d82aa", i128);
        hpD = a122;
        fmVarArr[i128] = a122;
        int i129 = i128 + 1;
        C2491fm a123 = C4105zY.m41624a(PlayerController.class, "4766a2812a11db86dfb00eb5060ddc60", i129);
        _f_tick_0020_0028F_0029V = a123;
        fmVarArr[i129] = a123;
        int i130 = i129 + 1;
        C2491fm a124 = C4105zY.m41624a(PlayerController.class, "1e8356bee8310dde17ea184f7c432769", i130);
        hpE = a124;
        fmVarArr[i130] = a124;
        int i131 = i130 + 1;
        C2491fm a125 = C4105zY.m41624a(PlayerController.class, "99910e9537a616d9fd99e6630aa8b4ba", i131);
        hpF = a125;
        fmVarArr[i131] = a125;
        int i132 = i131 + 1;
        C2491fm a126 = C4105zY.m41624a(PlayerController.class, "727bece61d941b734ff1f53e7ad37e16", i132);
        brG = a126;
        fmVarArr[i132] = a126;
        int i133 = i132 + 1;
        C2491fm a127 = C4105zY.m41624a(PlayerController.class, "5fe4e3da22fafb77df927738d1dff639", i133);
        hpG = a127;
        fmVarArr[i133] = a127;
        int i134 = i133 + 1;
        C2491fm a128 = C4105zY.m41624a(PlayerController.class, "d3d5702838fe5f16e12639debaeb62e4", i134);
        hpH = a128;
        fmVarArr[i134] = a128;
        int i135 = i134 + 1;
        C2491fm a129 = C4105zY.m41624a(PlayerController.class, "57660ff21d48a97a587fecdbd34494e5", i135);
        f9171x75159c47 = a129;
        fmVarArr[i135] = a129;
        int i136 = i135 + 1;
        C2491fm a130 = C4105zY.m41624a(PlayerController.class, "224c990cb639ef075b36cdf01c079818", i136);
        hpI = a130;
        fmVarArr[i136] = a130;
        int i137 = i136 + 1;
        C2491fm a131 = C4105zY.m41624a(PlayerController.class, "ceace58088355300fedfc14b459d9869", i137);
        hpJ = a131;
        fmVarArr[i137] = a131;
        int i138 = i137 + 1;
        C2491fm a132 = C4105zY.m41624a(PlayerController.class, "bae4caf0d37c65472797702c9bfb50e6", i138);
        hpK = a132;
        fmVarArr[i138] = a132;
        int i139 = i138 + 1;
        C2491fm a133 = C4105zY.m41624a(PlayerController.class, "f6f7911c671d8482671385cf80edeb05", i139);
        _f_step_0020_0028F_0029V = a133;
        fmVarArr[i139] = a133;
        int i140 = i139 + 1;
        C2491fm a134 = C4105zY.m41624a(PlayerController.class, "acceda36772ecea65f9557919925ff6a", i140);
        hpL = a134;
        fmVarArr[i140] = a134;
        int i141 = i140 + 1;
        C2491fm a135 = C4105zY.m41624a(PlayerController.class, "405124c7ef4d8599fcb1ec2eecfa998f", i141);
        hpM = a135;
        fmVarArr[i141] = a135;
        int i142 = i141 + 1;
        C2491fm a136 = C4105zY.m41624a(PlayerController.class, "34bf72b5d1e7f0395946cb4bd77cfee9", i142);
        hpN = a136;
        fmVarArr[i142] = a136;
        int i143 = i142 + 1;
        C2491fm a137 = C4105zY.m41624a(PlayerController.class, "887cb678a58d908b2fe1f175944150c4", i143);
        hpO = a137;
        fmVarArr[i143] = a137;
        int i144 = i143 + 1;
        C2491fm a138 = C4105zY.m41624a(PlayerController.class, "c9975d1fe5bd7f69968bf68910249164", i144);
        hpP = a138;
        fmVarArr[i144] = a138;
        int i145 = i144 + 1;
        C2491fm a139 = C4105zY.m41624a(PlayerController.class, "aee9d258161e08dc0224805938af2e2a", i145);
        hpQ = a139;
        fmVarArr[i145] = a139;
        int i146 = i145 + 1;
        C2491fm a140 = C4105zY.m41624a(PlayerController.class, "6c4e8b9f4e8b240eca189c613a02065c", i146);
        hpR = a140;
        fmVarArr[i146] = a140;
        int i147 = i146 + 1;
        C2491fm a141 = C4105zY.m41624a(PlayerController.class, "d3d01a18a3ec3ef8a9ca4cf0d3e22101", i147);
        hpS = a141;
        fmVarArr[i147] = a141;
        int i148 = i147 + 1;
        C2491fm a142 = C4105zY.m41624a(PlayerController.class, "4c6872e5e2298e71b344de5711592fa7", i148);
        hpT = a142;
        fmVarArr[i148] = a142;
        int i149 = i148 + 1;
        C2491fm a143 = C4105zY.m41624a(PlayerController.class, "b20c2284486caa227ca7549ec0dd113b", i149);
        hpU = a143;
        fmVarArr[i149] = a143;
        int i150 = i149 + 1;
        C2491fm a144 = C4105zY.m41624a(PlayerController.class, "d9ae51c83f5f92181e459fd6a60151da", i150);
        hpV = a144;
        fmVarArr[i150] = a144;
        int i151 = i150 + 1;
        C2491fm a145 = C4105zY.m41624a(PlayerController.class, "659e603489830330deddf24791cad4d0", i151);
        hpW = a145;
        fmVarArr[i151] = a145;
        int i152 = i151 + 1;
        C2491fm a146 = C4105zY.m41624a(PlayerController.class, "ea9d85d3398de2cad94206f23436956b", i152);
        hpX = a146;
        fmVarArr[i152] = a146;
        int i153 = i152 + 1;
        C2491fm a147 = C4105zY.m41624a(PlayerController.class, "d9f9cf41dc2b1d3035cb71b50a9f55d7", i153);
        hpY = a147;
        fmVarArr[i153] = a147;
        int i154 = i153 + 1;
        C2491fm a148 = C4105zY.m41624a(PlayerController.class, "f42dc548bc4139ab1b4535f3f9501828", i154);
        hpZ = a148;
        fmVarArr[i154] = a148;
        int i155 = i154 + 1;
        C2491fm a149 = C4105zY.m41624a(PlayerController.class, "32cfa77d669b61333cb1d5220b5dd1a7", i155);
        f9176x13860637 = a149;
        fmVarArr[i155] = a149;
        int i156 = i155 + 1;
        C2491fm a150 = C4105zY.m41624a(PlayerController.class, "cf95a282cd5675f1898db5dae2278546", i156);
        f9178gQ = a150;
        fmVarArr[i156] = a150;
        int i157 = i156 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Controller._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerController.class, C5480aKy.class, _m_fields, _m_methods);
    }

    /* renamed from: c */
    public static final float m39266c(C0286Dh dh) {
        if (C5877acF.fdU) {
            return Float.MAX_VALUE;
        }
        if (dh instanceof Gate) {
            return hmT;
        }
        if (dh instanceof Station) {
        }
        return 4000000.0f;
    }

    /* renamed from: Vb */
    private Node m39227Vb() {
        return (Node) bFf().mo5608dq().mo3214p(f9181ui);
    }

    /* renamed from: a */
    private void m39232a(Player aku) {
        bFf().mo5608dq().mo3197f(f9180hz, aku);
    }

    /* renamed from: a */
    private void m39234a(Node rPVar) {
        bFf().mo5608dq().mo3197f(f9181ui, rPVar);
    }

    @C4034yP
    @C0064Am(aul = "512d27aecbed742113737bc50b179ba3", aum = 0)
    @C5566aOg
    private void aYf() {
        throw new aWi(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "1bcac4df1490e16de993f058133859a8", aum = 0)
    @C5566aOg
    private void aYh() {
        throw new aWi(new aCE(this, _f_exitGateServer_0020_0028_0029V, new Object[0]));
    }

    /* renamed from: ap */
    private void m39244ap(Vec3d ajr) {
        switch (bFf().mo6893i(hpy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpy, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpy, new Object[]{ajr}));
                break;
        }
        m39243ao(ajr);
    }

    @ClientOnly
    private void asy() {
        switch (bFf().mo6893i(hov)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hov, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hov, new Object[0]));
                break;
        }
        cNR();
    }

    /* renamed from: b */
    private void m39250b(C3627j jVar) {
        switch (bFf().mo6893i(hoz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoz, new Object[]{jVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoz, new Object[]{jVar}));
                break;
        }
        m39235a(jVar);
    }

    /* renamed from: bF */
    private void m39252bF(Actor cr) {
        bFf().mo5608dq().mo3197f(hnl, cr);
    }

    /* renamed from: bH */
    private void m39254bH(Actor cr) {
        switch (bFf().mo6893i(hnK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnK, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnK, new Object[]{cr}));
                break;
        }
        m39253bG(cr);
    }

    /* renamed from: bP */
    private void m39259bP(Actor cr) {
        switch (bFf().mo6893i(hoT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoT, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoT, new Object[]{cr}));
                break;
        }
        m39258bO(cr);
    }

    @ClientOnly
    /* renamed from: bX */
    private void m39265bX(Actor cr) {
        switch (bFf().mo6893i(hpP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpP, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpP, new Object[]{cr}));
                break;
        }
        m39264bW(cr);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "57660ff21d48a97a587fecdbd34494e5", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m39267c(C5878acG acg) {
        throw new aWi(new aCE(this, f9171x75159c47, new Object[]{acg}));
    }

    private boolean cMT() {
        return bFf().mo5608dq().mo3201h(hmZ);
    }

    private Actor cMU() {
        return (Actor) bFf().mo5608dq().mo3214p(hnl);
    }

    private boolean cMV() {
        return bFf().mo5608dq().mo3201h(hnB);
    }

    private void cNQ() {
        switch (bFf().mo6893i(hou)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hou, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hou, new Object[0]));
                break;
        }
        cNP();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "4d1cdc4d86b30a9e95cde7f1b684fc30", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cNX() {
        throw new aWi(new aCE(this, hoA, new Object[0]));
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    private void cNk() {
        switch (bFf().mo6893i(hnX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnX, new Object[0]));
                break;
        }
        cNj();
    }

    @C0064Am(aul = "17ef11877911147d73511411ba1b2970", aum = 0)
    @C5566aOg
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private String cNl() {
        throw new aWi(new aCE(this, hnY, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "32ff6a45801486b47d78182625418825", aum = 0)
    @C2499fr
    private List<Actor> cNp() {
        throw new aWi(new aCE(this, hob, new Object[0]));
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    private void cOU() {
        switch (bFf().mo6893i(hpC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpC, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpC, new Object[0]));
                break;
        }
        cOT();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "63faa4d137abae7a69b9d568fa3b1c4d", aum = 0)
    @C2499fr
    private void cOf() {
        throw new aWi(new aCE(this, hoF, new Object[0]));
    }

    private boolean cOr() {
        switch (bFf().mo6893i(hoQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hoQ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hoQ, new Object[0]));
                break;
        }
        return cOq();
    }

    @C0064Am(aul = "d07622762b2c8b3ef2bc7dd1db27b90e", aum = 0)
    @C5566aOg
    private void cOs() {
        throw new aWi(new aCE(this, hoR, new Object[0]));
    }

    @C0064Am(aul = "a4e64e231ea6a68b5519e1b7950cd124", aum = 0)
    @C5566aOg
    private void cOu() {
        throw new aWi(new aCE(this, hoS, new Object[0]));
    }

    @C0064Am(aul = "9f08615fb6d1c7a17f9e6bc0c2d481f9", aum = 0)
    @C5566aOg
    @C2499fr
    private void cOx() {
        throw new aWi(new aCE(this, hoZ, new Object[0]));
    }

    /* access modifiers changed from: private */
    @C5566aOg
    @C2499fr
    public void cOy() {
        switch (bFf().mo6893i(hoZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoZ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoZ, new Object[0]));
                break;
        }
        cOx();
    }

    @C0064Am(aul = "659e603489830330deddf24791cad4d0", aum = 0)
    @C5566aOg
    @C2499fr
    private boolean cPq() {
        throw new aWi(new aCE(this, hpW, new Object[0]));
    }

    private long cPt() {
        switch (bFf().mo6893i(hpX)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hpX, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hpX, new Object[0]));
                break;
        }
        return cPs();
    }

    private long cPv() {
        switch (bFf().mo6893i(hpY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hpY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hpY, new Object[0]));
                break;
        }
        return cPu();
    }

    /* access modifiers changed from: private */
    public PlayerController cuN() {
        switch (bFf().mo6893i(hoJ)) {
            case 0:
                return null;
            case 2:
                return (PlayerController) bFf().mo5606d(new aCE(this, hoJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hoJ, new Object[0]));
                break;
        }
        return cOl();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "bc45bd5497dbeed516c8f347c9aa90e2", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    /* renamed from: d */
    private void m39271d(C0286Dh dh) {
        throw new aWi(new aCE(this, hoX, new Object[]{dh}));
    }

    @ClientOnly
    /* renamed from: d */
    private void m39273d(C3627j jVar) {
        switch (bFf().mo6893i(hpu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpu, new Object[]{jVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpu, new Object[]{jVar}));
                break;
        }
        m39269c(jVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: dG */
    public Player m39274dG() {
        return (Player) bFf().mo5608dq().mo3214p(f9180hz);
    }

    @C0064Am(aul = "cf95a282cd5675f1898db5dae2278546", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m39276dd() {
        throw new aWi(new aCE(this, f9178gQ, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "727bece61d941b734ff1f53e7ad37e16", aum = 0)
    @C2499fr
    /* renamed from: dd */
    private void m39277dd(long j) {
        throw new aWi(new aCE(this, brG, new Object[]{new Long(j)}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: de */
    private void m39278de(long j) {
        switch (bFf().mo6893i(brG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brG, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brG, new Object[]{new Long(j)}));
                break;
        }
        m39277dd(j);
    }

    @ClientOnly
    /* renamed from: f */
    private void m39281f(C3627j jVar) {
        switch (bFf().mo6893i(hpv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpv, new Object[]{jVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpv, new Object[]{jVar}));
                break;
        }
        m39280e(jVar);
    }

    @ClientOnly
    /* renamed from: h */
    private void m39288h(C3627j jVar) {
        switch (bFf().mo6893i(hpA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpA, new Object[]{jVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpA, new Object[]{jVar}));
                break;
        }
        m39286g(jVar);
    }

    /* renamed from: ie */
    private void m39291ie(boolean z) {
        bFf().mo5608dq().mo3153a(hmZ, z);
    }

    /* renamed from: if */
    private void m39292if(boolean z) {
        bFf().mo5608dq().mo3153a(hnB, z);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: ii */
    private void m39295ii(boolean z) {
        switch (bFf().mo6893i(hnP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnP, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnP, new Object[]{new Boolean(z)}));
                break;
        }
        m39294ih(z);
    }

    @ClientOnly
    /* renamed from: in */
    private void m39299in(boolean z) {
        switch (bFf().mo6893i(hop)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hop, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hop, new Object[]{new Boolean(z)}));
                break;
        }
        m39298im(z);
    }

    @ClientOnly
    /* renamed from: ip */
    private void m39301ip(boolean z) {
        switch (bFf().mo6893i(hoq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoq, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoq, new Object[]{new Boolean(z)}));
                break;
        }
        m39300io(z);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "8d470ffa1fabc7dc275a571fcad9ba09", aum = 0)
    @C2499fr
    /* renamed from: is */
    private void m39303is(boolean z) {
        throw new aWi(new aCE(this, hoY, new Object[]{new Boolean(z)}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: it */
    private void m39304it(boolean z) {
        switch (bFf().mo6893i(hoY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoY, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoY, new Object[]{new Boolean(z)}));
                break;
        }
        m39303is(z);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "6c4e8b9f4e8b240eca189c613a02065c", aum = 0)
    @C2499fr
    /* renamed from: ix */
    private void m39307ix(boolean z) {
        throw new aWi(new aCE(this, hpR, new Object[]{new Boolean(z)}));
    }

    /* renamed from: ls */
    private ConfigManagerSection m39317ls(String str) {
        switch (bFf().mo6893i(hnQ)) {
            case 0:
                return null;
            case 2:
                return (ConfigManagerSection) bFf().mo5606d(new aCE(this, hnQ, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, hnQ, new Object[]{str}));
                break;
        }
        return m39316lr(str);
    }

    /* renamed from: lx */
    private void m39319lx(float f) {
        switch (bFf().mo6893i(hnJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnJ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnJ, new Object[]{new Float(f)}));
                break;
        }
        m39318lw(f);
    }

    @ClientOnly
    /* renamed from: lz */
    private void m39321lz(float f) {
        switch (bFf().mo6893i(hoI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoI, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoI, new Object[]{new Float(f)}));
                break;
        }
        m39320ly(f);
    }

    /* access modifiers changed from: private */
    @ClientOnly
    /* renamed from: o */
    public void m39326o(Pawn avi) {
        switch (bFf().mo6893i(hoL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoL, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoL, new Object[]{avi}));
                break;
        }
        m39323n(avi);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "948bee6ca53b7cb1dfcccfb1e32d82aa", aum = 0)
    @C2499fr
    /* renamed from: w */
    private void m39329w(Weapon adv) {
        throw new aWi(new aCE(this, hpD, new Object[]{adv}));
    }

    @ClientOnly
    /* renamed from: Ff */
    public void mo22054Ff() {
        switch (bFf().mo6893i(hpt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpt, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpt, new Object[0]));
                break;
        }
        cOL();
    }

    @ClientOnly
    /* renamed from: Fg */
    public void mo22055Fg() {
        switch (bFf().mo6893i(hpz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpz, new Object[0]));
                break;
        }
        cOQ();
    }

    /* renamed from: Fk */
    public void mo22056Fk() {
        switch (bFf().mo6893i(hnW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnW, new Object[0]));
                break;
        }
        cNi();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m39225U(f);
    }

    /* renamed from: Ve */
    public Node mo22057Ve() {
        switch (bFf().mo6893i(aSi)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, aSi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aSi, new Object[0]));
                break;
        }
        return m39228Vd();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5480aKy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Controller._m_methodCount) {
            case 0:
                cMW();
                return null;
            case 1:
                cMY();
                return null;
            case 2:
                m39293ig(((Boolean) args[0]).booleanValue());
                return null;
            case 3:
                m39318lw(((Float) args[0]).floatValue());
                return null;
            case 4:
                m39253bG((Actor) args[0]);
                return null;
            case 5:
                m39255bI((Actor) args[0]);
                return null;
            case 6:
                cNa();
                return null;
            case 7:
                m39240a(((Boolean) args[0]).booleanValue(), (String) args[1]);
                return null;
            case 8:
                cNc();
                return null;
            case 9:
                return new Boolean(m39327o((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 10:
                m39294ih(((Boolean) args[0]).booleanValue());
                return null;
            case 11:
                return m39316lr((String) args[0]);
            case 12:
                cNe();
                return null;
            case 13:
                m39296ij(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                m39239a(((Boolean) args[0]).booleanValue(), (aDA) args[1]);
                return null;
            case 15:
                m39297il(((Boolean) args[0]).booleanValue());
                return null;
            case 16:
                m39283fg();
                return null;
            case 17:
                return cNg();
            case 18:
                cNi();
                return null;
            case 19:
                m39322m((Gate) args[0]);
                return null;
            case 20:
                cNj();
                return null;
            case 21:
                aYh();
                return null;
            case 22:
                aYl();
                return null;
            case 23:
                return cNl();
            case 24:
                return m39324o((SceneObject) args[0]);
            case 25:
                return cNn();
            case 26:
                return cNp();
            case 27:
                return new Boolean(cNr());
            case 28:
                return cNt();
            case 29:
                return cNv();
            case 30:
                return cNx();
            case 31:
                return cNz();
            case 32:
                return cNB();
            case 33:
                return cND();
            case 34:
                return m39228Vd();
            case 35:
                return m39275dK();
            case 36:
                return m39256bK((Actor) args[0]);
            case 37:
                return new Float(m39229a((SceneObject) args[0], (ShipType) args[1], ((Boolean) args[2]).booleanValue()));
            case 38:
                return m39224Mo();
            case 39:
                return m39328w(((Float) args[0]).floatValue(), ((Float) args[1]).floatValue());
            case 40:
                cNF();
                return null;
            case 41:
                cNH();
                return null;
            case 42:
                cNJ();
                return null;
            case 43:
                m39298im(((Boolean) args[0]).booleanValue());
                return null;
            case 44:
                m39300io(((Boolean) args[0]).booleanValue());
                return null;
            case 45:
                return new Boolean(cNL());
            case 46:
                return new Boolean(cNN());
            case 47:
                m39302iq(((Boolean) args[0]).booleanValue());
                return null;
            case 48:
                cNP();
                return null;
            case 49:
                cNR();
                return null;
            case 50:
                return new Boolean(cNS());
            case 51:
                return new Boolean(cNU());
            case 52:
                return new Boolean(cNV());
            case 53:
                m39235a((C3627j) args[0]);
                return null;
            case 54:
                cNX();
                return null;
            case 55:
                m39257bM((Actor) args[0]);
                return null;
            case 56:
                m39245aq((Actor) args[0]);
                return null;
            case 57:
                m39246as((Actor) args[0]);
                return null;
            case 58:
                aYj();
                return null;
            case 59:
                aYk();
                return null;
            case 60:
                cNZ();
                return null;
            case 61:
                aYf();
                return null;
            case 62:
                cOb();
                return null;
            case 63:
                cOd();
                return null;
            case 64:
                cOf();
                return null;
            case 65:
                cOh();
                return null;
            case 66:
                aYn();
                return null;
            case 67:
                cOj();
                return null;
            case 68:
                m39320ly(((Float) args[0]).floatValue());
                return null;
            case 69:
                return cOl();
            case 70:
                m39309l((Pawn) args[0]);
                return null;
            case 71:
                m39323n((Pawn) args[0]);
                return null;
            case 72:
                m39241aI((String) args[0], (String) args[1]);
                return null;
            case 73:
                m39261bQ((Asset) args[0]);
                return null;
            case 74:
                cOm();
                return null;
            case 75:
                cOo();
                return null;
            case 76:
                return new Boolean(cOq());
            case 77:
                cOs();
                return null;
            case 78:
                cOu();
                return null;
            case 79:
                m39258bO((Actor) args[0]);
                return null;
            case 80:
                m39260bQ((Actor) args[0]);
                return null;
            case 81:
                m39289hn(((Float) args[0]).floatValue());
                return null;
            case 82:
                cOw();
                return null;
            case 83:
                return new Float(m39330y(((Float) args[0]).floatValue(), ((Float) args[1]).floatValue()));
            case 84:
                m39271d((C0286Dh) args[0]);
                return null;
            case 85:
                m39231a((C0286Dh) args[0], ((Float) args[1]).floatValue());
                return null;
            case 86:
                m39303is(((Boolean) args[0]).booleanValue());
                return null;
            case 87:
                cOx();
                return null;
            case 88:
                m39305iu(((Boolean) args[0]).booleanValue());
                return null;
            case 89:
                cOz();
                return null;
            case 90:
                m39310lA(((Float) args[0]).floatValue());
                return null;
            case 91:
                m39311lC(((Float) args[0]).floatValue());
                return null;
            case 92:
                m39312lE(((Float) args[0]).floatValue());
                return null;
            case 93:
                m39313lG(((Float) args[0]).floatValue());
                return null;
            case 94:
                m39233a((C6483ann) args[0]);
                return null;
            case 95:
                m39268c((C6483ann) args[0]);
                return null;
            case 96:
                m39279e((C6483ann) args[0]);
                return null;
            case 97:
                m39314lI(((Float) args[0]).floatValue());
                return null;
            case 98:
                m39285g((C6483ann) args[0]);
                return null;
            case 99:
                m39282fI(((Float) args[0]).floatValue());
                return null;
            case 100:
                m39262bS((Actor) args[0]);
                return null;
            case 101:
                m39249b((Node) args[0]);
                return null;
            case 102:
                m39272d((Pawn) args[0]);
                return null;
            case 103:
                m39325o((Player) args[0]);
                return null;
            case 104:
                m39315lK(((Float) args[0]).floatValue());
                return null;
            case 105:
                cOB();
                return null;
            case 106:
                cOD();
                return null;
            case 107:
                cOF();
                return null;
            case 108:
                cOH();
                return null;
            case 109:
                m39230a((C0286Dh.C0287a) args[0], (C0286Dh) args[1]);
                return null;
            case 110:
                cOJ();
                return null;
            case 111:
                cOL();
                return null;
            case 112:
                m39269c((C3627j) args[0]);
                return null;
            case 113:
                m39280e((C3627j) args[0]);
                return null;
            case 114:
                cOM();
                return null;
            case 115:
                cOO();
                return null;
            case 116:
                m39243ao((Vec3d) args[0]);
                return null;
            case 117:
                cOQ();
                return null;
            case 118:
                m39286g((C3627j) args[0]);
                return null;
            case 119:
                cOR();
                return null;
            case 120:
                cOT();
                return null;
            case 121:
                m39329w((Weapon) args[0]);
                return null;
            case 122:
                m39225U(((Float) args[0]).floatValue());
                return null;
            case 123:
                cOV();
                return null;
            case 124:
                m39306iw(((Boolean) args[0]).booleanValue());
                return null;
            case 125:
                m39277dd(((Long) args[0]).longValue());
                return null;
            case 126:
                cOX();
                return null;
            case 127:
                cOY();
                return null;
            case 128:
                m39267c((C5878acG) args[0]);
                return null;
            case 129:
                return new Boolean(cPa());
            case 130:
                m39263bU((Actor) args[0]);
                return null;
            case 131:
                m39270d((Actor) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 132:
                m39247ay(((Float) args[0]).floatValue());
                return null;
            case 133:
                cPc();
                return null;
            case 134:
                m39251bC((Vec3f) args[0]);
                return null;
            case 135:
                return cPe();
            case 136:
                cPg();
                return null;
            case 137:
                m39264bW((Actor) args[0]);
                return null;
            case 138:
                cPi();
                return null;
            case 139:
                m39307ix(((Boolean) args[0]).booleanValue());
                return null;
            case 140:
                cPk();
                return null;
            case 141:
                cPm();
                return null;
            case 142:
                m39242ak((Ship) args[0]);
                return null;
            case 143:
                cPo();
                return null;
            case 144:
                return new Boolean(cPq());
            case 145:
                return new Long(cPs());
            case 146:
                return new Long(cPu());
            case 147:
                cPw();
                return null;
            case 148:
                m39248b((aDJ) args[0]);
                return null;
            case 149:
                m39276dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    public void mo2066a(C5878acG acg) {
        switch (bFf().mo6893i(f9171x75159c47)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9171x75159c47, new Object[]{acg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9171x75159c47, new Object[]{acg}));
                break;
        }
        m39267c(acg);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    /* renamed from: aJ */
    public void mo22058aJ(String str, String str2) {
        switch (bFf().mo6893i(hoM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoM, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoM, new Object[]{str, str2}));
                break;
        }
        m39241aI(str, str2);
    }

    /* renamed from: aR */
    public void mo22059aR(boolean z) {
        switch (bFf().mo6893i(hpF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpF, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpF, new Object[]{new Boolean(z)}));
                break;
        }
        m39306iw(z);
    }

    @C4034yP
    @C5566aOg
    public void aYg() {
        switch (bFf().mo6893i(_f_onPawnExplode_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onPawnExplode_0020_0028_0029V, new Object[0]));
                break;
        }
        aYf();
    }

    @C5566aOg
    public void aYi() {
        switch (bFf().mo6893i(_f_exitGateServer_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_exitGateServer_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_exitGateServer_0020_0028_0029V, new Object[0]));
                break;
        }
        aYh();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void aYm() {
        switch (bFf().mo6893i(_f_forceMaxSpeedOnClient_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_forceMaxSpeedOnClient_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_forceMaxSpeedOnClient_0020_0028_0029V, new Object[0]));
                break;
        }
        aYl();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void aYo() {
        switch (bFf().mo6893i(_f_onSimulationStatusUpdate_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onSimulationStatusUpdate_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onSimulationStatusUpdate_0020_0028_0029V, new Object[0]));
                break;
        }
        aYn();
    }

    @ClientOnly
    public void ajM() {
        switch (bFf().mo6893i(hpG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpG, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpG, new Object[0]));
                break;
        }
        cOX();
    }

    /* renamed from: al */
    public Ship mo22061al() {
        switch (bFf().mo6893i(f9173x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f9173x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9173x851d656b, new Object[0]));
                break;
        }
        return m39224Mo();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    /* renamed from: al */
    public void mo22062al(Ship fAVar) {
        switch (bFf().mo6893i(hpU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpU, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpU, new Object[]{fAVar}));
                break;
        }
        m39242ak(fAVar);
    }

    @ClientOnly
    public boolean anX() {
        switch (bFf().mo6893i(hox)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hox, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hox, new Object[0]));
                break;
        }
        return cNU();
    }

    @ClientOnly
    /* renamed from: ar */
    public void mo3287ar(Actor cr) {
        switch (bFf().mo6893i(f9174x6b6d29da)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9174x6b6d29da, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9174x6b6d29da, new Object[]{cr}));
                break;
        }
        m39245aq(cr);
    }

    @ClientOnly
    /* renamed from: at */
    public void mo3288at(Actor cr) {
        switch (bFf().mo6893i(f9175xa1dc95e1)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9175xa1dc95e1, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9175xa1dc95e1, new Object[]{cr}));
                break;
        }
        m39246as(cr);
    }

    /* renamed from: b */
    public float mo22064b(SceneObject sceneObject, ShipType ng, boolean z) {
        switch (bFf().mo6893i(hok)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hok, new Object[]{sceneObject, ng, new Boolean(z)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hok, new Object[]{sceneObject, ng, new Boolean(z)}));
                break;
        }
        return m39229a(sceneObject, ng, z);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: b */
    public void mo22065b(C0286Dh.C0287a aVar, C0286Dh dh) {
        switch (bFf().mo6893i(hpr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpr, new Object[]{aVar, dh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpr, new Object[]{aVar, dh}));
                break;
        }
        m39230a(aVar, dh);
    }

    /* renamed from: b */
    public void mo22066b(C0286Dh dh, float f) {
        switch (bFf().mo6893i(bsf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bsf, new Object[]{dh, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bsf, new Object[]{dh, new Float(f)}));
                break;
        }
        m39231a(dh, f);
    }

    /* renamed from: b */
    public void mo22067b(C6483ann ann) {
        switch (bFf().mo6893i(hpg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpg, new Object[]{ann}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpg, new Object[]{ann}));
                break;
        }
        m39233a(ann);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: b */
    public void mo22068b(boolean z, aDA ada) {
        switch (bFf().mo6893i(hnT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnT, new Object[]{new Boolean(z), ada}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnT, new Object[]{new Boolean(z), ada}));
                break;
        }
        m39239a(z, ada);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo22069b(boolean z, String str) {
        switch (bFf().mo6893i(_f_check_0020_0028ZLjava_002flang_002fString_003b_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V, new Object[]{new Boolean(z), str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_check_0020_0028ZLjava_002flang_002fString_003b_0029V, new Object[]{new Boolean(z), str}));
                break;
        }
        m39240a(z, str);
    }

    @ClientOnly
    /* renamed from: bD */
    public void mo22070bD(Vec3f vec3f) {
        switch (bFf().mo6893i(hpM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpM, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpM, new Object[]{vec3f}));
                break;
        }
        m39251bC(vec3f);
    }

    @ClientOnly
    /* renamed from: bJ */
    public void mo22071bJ(Actor cr) {
        switch (bFf().mo6893i(hnL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnL, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnL, new Object[]{cr}));
                break;
        }
        m39255bI(cr);
    }

    @Deprecated
    /* renamed from: bL */
    public SceneObject mo22072bL(Actor cr) {
        switch (bFf().mo6893i(hoj)) {
            case 0:
                return null;
            case 2:
                return (SceneObject) bFf().mo5606d(new aCE(this, hoj, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, hoj, new Object[]{cr}));
                break;
        }
        return m39256bK(cr);
    }

    @ClientOnly
    /* renamed from: bN */
    public void mo22073bN(Actor cr) {
        switch (bFf().mo6893i(hoB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoB, new Object[]{cr}));
                break;
        }
        m39257bM(cr);
    }

    @ClientOnly
    /* renamed from: bR */
    public void mo22074bR(Actor cr) {
        switch (bFf().mo6893i(hoU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoU, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoU, new Object[]{cr}));
                break;
        }
        m39260bQ(cr);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    /* renamed from: bR */
    public void mo22075bR(Asset tCVar) {
        switch (bFf().mo6893i(hoN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoN, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoN, new Object[]{tCVar}));
                break;
        }
        m39261bQ(tCVar);
    }

    /* renamed from: bT */
    public void mo22076bT(Actor cr) {
        switch (bFf().mo6893i(hpl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpl, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpl, new Object[]{cr}));
                break;
        }
        m39262bS(cr);
    }

    @C4034yP
    @ClientOnly
    /* renamed from: bV */
    public void mo22077bV(Actor cr) {
        switch (bFf().mo6893i(hpJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpJ, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpJ, new Object[]{cr}));
                break;
        }
        m39263bU(cr);
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f9176x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9176x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9176x13860637, new Object[]{adj}));
                break;
        }
        m39248b(adj);
    }

    /* renamed from: c */
    public void mo22078c(Node rPVar) {
        switch (bFf().mo6893i(aSj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aSj, new Object[]{rPVar}));
                break;
        }
        m39249b(rPVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cMX() {
        switch (bFf().mo6893i(hnG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnG, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnG, new Object[0]));
                break;
        }
        cMW();
    }

    public void cMZ() {
        switch (bFf().mo6893i(hnH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnH, new Object[0]));
                break;
        }
        cMY();
    }

    public C6483ann cNA() {
        switch (bFf().mo6893i(hog)) {
            case 0:
                return null;
            case 2:
                return (C6483ann) bFf().mo5606d(new aCE(this, hog, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hog, new Object[0]));
                break;
        }
        return cNz();
    }

    public C6483ann cNC() {
        switch (bFf().mo6893i(hoh)) {
            case 0:
                return null;
            case 2:
                return (C6483ann) bFf().mo5606d(new aCE(this, hoh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hoh, new Object[0]));
                break;
        }
        return cNB();
    }

    @ClientOnly
    public EngineGame cNE() {
        switch (bFf().mo6893i(hoi)) {
            case 0:
                return null;
            case 2:
                return (EngineGame) bFf().mo5606d(new aCE(this, hoi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hoi, new Object[0]));
                break;
        }
        return cND();
    }

    @ClientOnly
    public void cNG() {
        switch (bFf().mo6893i(hom)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hom, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hom, new Object[0]));
                break;
        }
        cNF();
    }

    @ClientOnly
    public void cNI() {
        switch (bFf().mo6893i(hon)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hon, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hon, new Object[0]));
                break;
        }
        cNH();
    }

    @ClientOnly
    public void cNK() {
        switch (bFf().mo6893i(hoo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoo, new Object[0]));
                break;
        }
        cNJ();
    }

    @ClientOnly
    public boolean cNM() {
        switch (bFf().mo6893i(hor)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hor, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hor, new Object[0]));
                break;
        }
        return cNL();
    }

    @ClientOnly
    public boolean cNO() {
        switch (bFf().mo6893i(hos)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hos, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hos, new Object[0]));
                break;
        }
        return cNN();
    }

    public boolean cNT() {
        switch (bFf().mo6893i(how)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, how, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, how, new Object[0]));
                break;
        }
        return cNS();
    }

    @ClientOnly
    public boolean cNW() {
        switch (bFf().mo6893i(hoy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hoy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hoy, new Object[0]));
                break;
        }
        return cNV();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    public void cNY() {
        switch (bFf().mo6893i(hoA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoA, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoA, new Object[0]));
                break;
        }
        cNX();
    }

    @ClientOnly
    public void cNb() {
        switch (bFf().mo6893i(hnM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnM, new Object[0]));
                break;
        }
        cNa();
    }

    @ClientOnly
    public void cNd() {
        switch (bFf().mo6893i(hnN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnN, new Object[0]));
                break;
        }
        cNc();
    }

    @ClientOnly
    public void cNf() {
        switch (bFf().mo6893i(hnR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnR, new Object[0]));
                break;
        }
        cNe();
    }

    /* access modifiers changed from: package-private */
    public List<C1540Wd<C0286Dh, Float>> cNh() {
        switch (bFf().mo6893i(hnV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hnV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hnV, new Object[0]));
                break;
        }
        return cNg();
    }

    @C5566aOg
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    public String cNm() {
        switch (bFf().mo6893i(hnY)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hnY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hnY, new Object[0]));
                break;
        }
        return cNl();
    }

    @ClientOnly
    public AdvertiseManager cNo() {
        switch (bFf().mo6893i(hoa)) {
            case 0:
                return null;
            case 2:
                return (AdvertiseManager) bFf().mo5606d(new aCE(this, hoa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hoa, new Object[0]));
                break;
        }
        return cNn();
    }

    @C5566aOg
    @C2499fr
    public List<Actor> cNq() {
        switch (bFf().mo6893i(hob)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hob, new Object[0]));
                break;
        }
        return cNp();
    }

    @ClientOnly
    public boolean cNs() {
        switch (bFf().mo6893i(hoc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hoc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hoc, new Object[0]));
                break;
        }
        return cNr();
    }

    public C6483ann cNu() {
        switch (bFf().mo6893i(hod)) {
            case 0:
                return null;
            case 2:
                return (C6483ann) bFf().mo5606d(new aCE(this, hod, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hod, new Object[0]));
                break;
        }
        return cNt();
    }

    public C6483ann cNw() {
        switch (bFf().mo6893i(hoe)) {
            case 0:
                return null;
            case 2:
                return (C6483ann) bFf().mo5606d(new aCE(this, hoe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hoe, new Object[0]));
                break;
        }
        return cNv();
    }

    public ConfigManagerSection cNy() {
        switch (bFf().mo6893i(hof)) {
            case 0:
                return null;
            case 2:
                return (ConfigManagerSection) bFf().mo5606d(new aCE(this, hof, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hof, new Object[0]));
                break;
        }
        return cNx();
    }

    @ClientOnly
    public void cOA() {
        switch (bFf().mo6893i(hpb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpb, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpb, new Object[0]));
                break;
        }
        cOz();
    }

    @ClientOnly
    public void cOC() {
        switch (bFf().mo6893i(hpn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpn, new Object[0]));
                break;
        }
        cOB();
    }

    @ClientOnly
    public void cOE() {
        switch (bFf().mo6893i(hpo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpo, new Object[0]));
                break;
        }
        cOD();
    }

    @ClientOnly
    public void cOG() {
        switch (bFf().mo6893i(hpp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpp, new Object[0]));
                break;
        }
        cOF();
    }

    @ClientOnly
    public void cOI() {
        switch (bFf().mo6893i(hpq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpq, new Object[0]));
                break;
        }
        cOH();
    }

    @ClientOnly
    public void cOK() {
        switch (bFf().mo6893i(hps)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hps, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hps, new Object[0]));
                break;
        }
        cOJ();
    }

    @ClientOnly
    public void cON() {
        switch (bFf().mo6893i(hpw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpw, new Object[0]));
                break;
        }
        cOM();
    }

    @ClientOnly
    public void cOP() {
        switch (bFf().mo6893i(hpx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpx, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpx, new Object[0]));
                break;
        }
        cOO();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void cOS() {
        switch (bFf().mo6893i(hpB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpB, new Object[0]));
                break;
        }
        cOR();
    }

    public void cOW() {
        switch (bFf().mo6893i(hpE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpE, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpE, new Object[0]));
                break;
        }
        cOV();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void cOZ() {
        switch (bFf().mo6893i(hpH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpH, new Object[0]));
                break;
        }
        cOY();
    }

    public void cOa() {
        switch (bFf().mo6893i(hoC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoC, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoC, new Object[0]));
                break;
        }
        cNZ();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void cOc() {
        switch (bFf().mo6893i(hoD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoD, new Object[0]));
                break;
        }
        cOb();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void cOe() {
        switch (bFf().mo6893i(hoE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoE, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoE, new Object[0]));
                break;
        }
        cOd();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void cOg() {
        switch (bFf().mo6893i(hoF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoF, new Object[0]));
                break;
        }
        cOf();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    public void cOi() {
        switch (bFf().mo6893i(hoG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoG, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoG, new Object[0]));
                break;
        }
        cOh();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void cOk() {
        switch (bFf().mo6893i(hoH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoH, new Object[0]));
                break;
        }
        cOj();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    public void cOn() {
        switch (bFf().mo6893i(hoO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoO, new Object[0]));
                break;
        }
        cOm();
    }

    @ClientOnly
    public void cOp() {
        switch (bFf().mo6893i(hoP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoP, new Object[0]));
                break;
        }
        cOo();
    }

    @C5566aOg
    public void cOt() {
        switch (bFf().mo6893i(hoR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoR, new Object[0]));
                break;
        }
        cOs();
    }

    @C5566aOg
    public void cOv() {
        switch (bFf().mo6893i(hoS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoS, new Object[0]));
                break;
        }
        cOu();
    }

    @ClientOnly
    public boolean cPb() {
        switch (bFf().mo6893i(hpI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hpI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hpI, new Object[0]));
                break;
        }
        return cPa();
    }

    @ClientOnly
    public void cPd() {
        switch (bFf().mo6893i(hpL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpL, new Object[0]));
                break;
        }
        cPc();
    }

    @ClientOnly
    public RulesOverride cPf() {
        switch (bFf().mo6893i(hpN)) {
            case 0:
                return null;
            case 2:
                return (RulesOverride) bFf().mo5606d(new aCE(this, hpN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hpN, new Object[0]));
                break;
        }
        return cPe();
    }

    @ClientOnly
    public void cPh() {
        switch (bFf().mo6893i(hpO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpO, new Object[0]));
                break;
        }
        cPg();
    }

    @ClientOnly
    public void cPj() {
        switch (bFf().mo6893i(hpQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpQ, new Object[0]));
                break;
        }
        cPi();
    }

    @ClientOnly
    public void cPl() {
        switch (bFf().mo6893i(hpS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpS, new Object[0]));
                break;
        }
        cPk();
    }

    @ClientOnly
    public void cPn() {
        switch (bFf().mo6893i(hpT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpT, new Object[0]));
                break;
        }
        cPm();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void cPp() {
        switch (bFf().mo6893i(hpV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpV, new Object[0]));
                break;
        }
        cPo();
    }

    @C5566aOg
    @C2499fr
    public boolean cPr() {
        switch (bFf().mo6893i(hpW)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hpW, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hpW, new Object[0]));
                break;
        }
        return cPq();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    public void cPx() {
        switch (bFf().mo6893i(hpZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpZ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpZ, new Object[0]));
                break;
        }
        cPw();
    }

    /* renamed from: d */
    public void mo22134d(C6483ann ann) {
        switch (bFf().mo6893i(hph)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hph, new Object[]{ann}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hph, new Object[]{ann}));
                break;
        }
        m39268c(ann);
    }

    /* renamed from: dL */
    public Player mo22135dL() {
        switch (bFf().mo6893i(f9179hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f9179hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9179hF, new Object[0]));
                break;
        }
        return m39275dK();
    }

    @C5566aOg
    /* renamed from: de */
    public void mo22136de() {
        switch (bFf().mo6893i(f9178gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9178gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9178gQ, new Object[0]));
                break;
        }
        m39276dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m39283fg();
    }

    @C4034yP
    @ClientOnly
    /* renamed from: e */
    public void mo22137e(Actor cr, boolean z) {
        switch (bFf().mo6893i(hpK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpK, new Object[]{cr, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpK, new Object[]{cr, new Boolean(z)}));
                break;
        }
        m39270d(cr, z);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    /* renamed from: e */
    public void mo22138e(C0286Dh dh) {
        switch (bFf().mo6893i(hoX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoX, new Object[]{dh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoX, new Object[]{dh}));
                break;
        }
        m39271d(dh);
    }

    /* renamed from: e */
    public void mo3293e(Pawn avi) {
        switch (bFf().mo6893i(f9177xbccafd70)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9177xbccafd70, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9177xbccafd70, new Object[]{avi}));
                break;
        }
        m39272d(avi);
    }

    /* renamed from: f */
    public void mo22139f(C6483ann ann) {
        switch (bFf().mo6893i(hpi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpi, new Object[]{ann}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpi, new Object[]{ann}));
                break;
        }
        m39279e(ann);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    /* renamed from: fJ */
    public void mo3294fJ(float f) {
        switch (bFf().mo6893i(_f_setDesiredLinearVelocityAtClient_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setDesiredLinearVelocityAtClient_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m39282fI(f);
    }

    /* renamed from: fv */
    public void mo22140fv(boolean z) {
        switch (bFf().mo6893i(hnU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnU, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnU, new Object[]{new Boolean(z)}));
                break;
        }
        m39297il(z);
    }

    /* renamed from: fx */
    public void mo22141fx(boolean z) {
        switch (bFf().mo6893i(hnI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnI, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnI, new Object[]{new Boolean(z)}));
                break;
        }
        m39293ig(z);
    }

    /* renamed from: h */
    public void mo22142h(C6483ann ann) {
        switch (bFf().mo6893i(hpk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpk, new Object[]{ann}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpk, new Object[]{ann}));
                break;
        }
        m39285g(ann);
    }

    @ClientOnly
    /* renamed from: ho */
    public void mo22143ho(float f) {
        switch (bFf().mo6893i(dTP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTP, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTP, new Object[]{new Float(f)}));
                break;
        }
        m39289hn(f);
    }

    @ClientOnly
    /* renamed from: ik */
    public void mo22144ik(boolean z) {
        switch (bFf().mo6893i(hnS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hnS, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hnS, new Object[]{new Boolean(z)}));
                break;
        }
        m39296ij(z);
    }

    @ClientOnly
    /* renamed from: ir */
    public void mo22145ir(boolean z) {
        switch (bFf().mo6893i(hot)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hot, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hot, new Object[]{new Boolean(z)}));
                break;
        }
        m39302iq(z);
    }

    /* renamed from: iv */
    public void mo22146iv(boolean z) {
        switch (bFf().mo6893i(hpa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpa, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpa, new Object[]{new Boolean(z)}));
                break;
        }
        m39305iu(z);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: iy */
    public void mo22147iy(boolean z) {
        switch (bFf().mo6893i(hpR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpR, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpR, new Object[]{new Boolean(z)}));
                break;
        }
        m39307ix(z);
    }

    /* renamed from: lB */
    public void mo22148lB(float f) {
        switch (bFf().mo6893i(hpc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpc, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpc, new Object[]{new Float(f)}));
                break;
        }
        m39310lA(f);
    }

    /* renamed from: lD */
    public void mo22149lD(float f) {
        switch (bFf().mo6893i(hpd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpd, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpd, new Object[]{new Float(f)}));
                break;
        }
        m39311lC(f);
    }

    /* renamed from: lF */
    public void mo22150lF(float f) {
        switch (bFf().mo6893i(hpe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpe, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpe, new Object[]{new Float(f)}));
                break;
        }
        m39312lE(f);
    }

    /* renamed from: lH */
    public void mo22151lH(float f) {
        switch (bFf().mo6893i(hpf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpf, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpf, new Object[]{new Float(f)}));
                break;
        }
        m39313lG(f);
    }

    @ClientOnly
    /* renamed from: lJ */
    public void mo22152lJ(float f) {
        switch (bFf().mo6893i(hpj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpj, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpj, new Object[]{new Float(f)}));
                break;
        }
        m39314lI(f);
    }

    /* renamed from: lL */
    public void mo22153lL(float f) {
        switch (bFf().mo6893i(hpm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpm, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpm, new Object[]{new Float(f)}));
                break;
        }
        m39315lK(f);
    }

    @ClientOnly
    /* renamed from: m */
    public void mo22154m(Pawn avi) {
        switch (bFf().mo6893i(hoK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoK, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoK, new Object[]{avi}));
                break;
        }
        m39309l(avi);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    /* renamed from: n */
    public void mo3295n(Gate fFVar) {
        switch (bFf().mo6893i(f9172xcedc4f07)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9172xcedc4f07, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9172xcedc4f07, new Object[]{fFVar}));
                break;
        }
        m39322m(fFVar);
    }

    @ClientOnly
    /* renamed from: p */
    public Actor mo22155p(SceneObject sceneObject) {
        switch (bFf().mo6893i(hnZ)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, hnZ, new Object[]{sceneObject}));
            case 3:
                bFf().mo5606d(new aCE(this, hnZ, new Object[]{sceneObject}));
                break;
        }
        return m39324o(sceneObject);
    }

    /* renamed from: p */
    public void mo22156p(Player aku) {
        switch (bFf().mo6893i(f9169Ny)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9169Ny, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9169Ny, new Object[]{aku}));
                break;
        }
        m39325o(aku);
    }

    @C5472aKq
    /* renamed from: p */
    public boolean mo22157p(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(hnO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hnO, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hnO, new Object[]{zt}));
                break;
        }
        return m39327o(zt);
    }

    @ClientOnly
    public void resetView() {
        switch (bFf().mo6893i(hoV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hoV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hoV, new Object[0]));
                break;
        }
        cOw();
    }

    @ClientOnly
    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m39247ay(f);
    }

    /* renamed from: x */
    public Vec3f mo22160x(float f, float f2) {
        switch (bFf().mo6893i(hol)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hol, new Object[]{new Float(f), new Float(f2)}));
            case 3:
                bFf().mo5606d(new aCE(this, hol, new Object[]{new Float(f), new Float(f2)}));
                break;
        }
        return m39328w(f, f2);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: x */
    public void mo22161x(Weapon adv) {
        switch (bFf().mo6893i(hpD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hpD, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hpD, new Object[]{adv}));
                break;
        }
        m39329w(adv);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    /* renamed from: xO */
    public void mo3296xO() {
        switch (bFf().mo6893i(_f_onEnterCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onEnterCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYj();
    }

    @C4034yP
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    @ClientOnly
    /* renamed from: xP */
    public void mo3297xP() {
        switch (bFf().mo6893i(_f_onExitCruiseSpeed_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onExitCruiseSpeed_0020_0028_0029V, new Object[0]));
                break;
        }
        aYk();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: z */
    public float mo22162z(float f, float f2) {
        switch (bFf().mo6893i(hoW)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hoW, new Object[]{new Float(f), new Float(f2)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hoW, new Object[]{new Float(f), new Float(f2)}));
                break;
        }
        return m39330y(f, f2);
    }

    @C0064Am(aul = "13685d499dd2450b0e5756084b583f69", aum = 0)
    private void cMW() {
        this.hnE = true;
    }

    @C0064Am(aul = "9c58c508beff58b705a902978ef0eae5", aum = 0)
    private void cMY() {
        long currentTimeMillis = SingletonCurrentTimeMilliImpl.currentTimeMillis();
        if (currentTimeMillis >= this.hnF + hnD) {
            if (this.hnE || currentTimeMillis > this.hnF + hnC) {
                this.hnF = currentTimeMillis;
                this.hnE = !mo22135dL().bhE().mo965Zc().cKz();
            }
        }
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m39291ie(true);
        this.hnb = false;
        this.hnc = false;
        this.hnd = C6483ann.UNDEFINED;
        this.hne = C6483ann.UNDEFINED;
        this.hnf = C6483ann.UNDEFINED;
        this.hng = C6483ann.UNDEFINED;
        this.hnh = 10000;
        this.hni = 0;
        this.hnn = 0.0f;
        this.hno = 0.0f;
        this.fkk = null;
        this.hnu = false;
        this.hnv = false;
        this.hnw = false;
        this.hnx = false;
        this.hns = 0.0f;
        this.hnr = 0;
        this.bHv = false;
    }

    @C0064Am(aul = "c5a02f8741f6f295ab380d1611554d0f", aum = 0)
    /* renamed from: ig */
    private void m39293ig(boolean z) {
        if (z) {
            mo22067b(C6483ann.ACCELERATE);
            this.hnr = 1;
            mo8361ms(hmX);
            m39274dG().mo14348am("accelerate", "");
        } else if (cNu() == C6483ann.ACCELERATE) {
            this.hnr = 0;
        } else {
            return;
        }
        cMX();
    }

    @C0064Am(aul = "50558df5f67dc30e1b9ff309221b5a37", aum = 0)
    /* renamed from: lw */
    private void m39318lw(float f) {
        if (aYa() != null && aYa().cLZ() && !cNW() && !mo22061al().ahb()) {
            boolean z = true;
            if (cNs()) {
                z = false;
            }
            if (this.hnr != 0) {
                this.hns = ((Math.min(hmX, f) / ((aYa().mo1091ra() + hmV) / aYa().mo1092rb())) * 2.0f * ((float) this.hnr)) + this.hns;
                if (Math.abs(this.hns) > 1.0f) {
                    this.hns = Math.signum(this.hns);
                }
            }
            mo22148lB(this.hns);
            if (z) {
                mo8361ms(hmX);
            }
        }
    }

    @C0064Am(aul = "49a8d63134faa5da456382923dc53c65", aum = 0)
    /* renamed from: bG */
    private void m39253bG(Actor cr) {
        SceneObject bL;
        if (!(cr instanceof Shot) && (bL = mo22072bL(cr)) != null) {
            bL.setAllowSelection(true);
            bL.putClientProperty("actor", cr);
        }
    }

    @C0064Am(aul = "56376a9daa04254e66c5134bdf21dd4a", aum = 0)
    @ClientOnly
    /* renamed from: bI */
    private void m39255bI(Actor cr) {
        m39254bH(cr);
    }

    @C0064Am(aul = "9413a73feac2c363619925bd7d4515f5", aum = 0)
    @ClientOnly
    private void cNa() {
    }

    @C0064Am(aul = "3ac07d8b71b592a42d648b43671f1d04", aum = 0)
    /* renamed from: a */
    private void m39240a(boolean z, String str) {
        if (!z) {
            throw new RuntimeException(str);
        }
    }

    @C0064Am(aul = "101398c3106eea3e2497ba756a5439be", aum = 0)
    @ClientOnly
    private void cNc() {
        if (cNM()) {
            if (!anX()) {
                ald().bhh().aRR();
            } else {
                ald().bhh().aRS();
            }
        } else if (anX()) {
            cPd();
        }
    }

    @C0064Am(aul = "9df22fec0eef587e59c9c7882829432d", aum = 0)
    @C5472aKq
    /* renamed from: o */
    private boolean m39327o(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            return false;
        }
        if (zt.bFq() == null) {
            return false;
        }
        if (zt.bFq().mo15388dL() == null) {
            return false;
        }
        return zt.bFq().mo15388dL() == mo22135dL();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "fb74d92435ab1ef93ad152f9136c304b", aum = 0)
    @C2499fr
    /* renamed from: ih */
    private void m39294ih(boolean z) {
        if (z) {
            this.hnr = 0;
            mo22152lJ(aYa().mo1090qZ().length());
        }
        m39291ie(z);
    }

    @C0064Am(aul = "d707d6da0a5a52d0f565241632249959", aum = 0)
    /* renamed from: lr */
    private ConfigManagerSection m39316lr(String str) {
        ConfigManager bhb = C5916acs.getSingolton().getConfigManager();
        ConfigManagerSection yr = new ConfigManagerSection();
        bhb.sectionOptions.put(str, yr);
        return yr;
    }

    @C0064Am(aul = "6fb798fde5dce3dd948ab3cdd7fa80f2", aum = 0)
    @ClientOnly
    private void cNe() {
        if (!cPf().mo22169P() && !cPf().mo22170R()) {
            if (mo22061al().afL().acZ() == C6809auB.C1996a.COOLDOWN) {
                mo22135dL().mo14419f((C1506WA) new C0284Df(((NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT)).dcw(), false, new Object[0]));
                return;
            }
            m39278de(cVr());
            mo22152lJ(0.0f);
        }
    }

    @C0064Am(aul = "2ecc620d9438a4a9e00da27dfc3d1392", aum = 0)
    @ClientOnly
    /* renamed from: ij */
    private void m39296ij(boolean z) {
        boolean z2 = false;
        if (!cPf().mo22169P() && !cPf().mo22170R()) {
            boolean z3 = (mo22061al().agZ() || mo22061al().ahh()) && !z;
            if ((mo22061al().ahd() || mo22061al().ahf()) && z) {
                z2 = true;
            }
            if (z3 || z2) {
                m39278de(cVr());
                mo22152lJ(0.0f);
            }
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "0d670a1a46c7aa271e77d7b68319c83e", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m39239a(boolean z, aDA ada) {
        if (mo22061al().agB() == ada && !z) {
            mo22135dL().dwU().mo21446j(((NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT)).dbE(), new Object[0]);
        }
    }

    @C0064Am(aul = "0977464b14a61adaec59e6cec3ad6df4", aum = 0)
    /* renamed from: il */
    private void m39297il(boolean z) {
        if (z) {
            m39274dG().dxc().mo22067b(C6483ann.DEACCELERATE);
            this.hnr = -1;
            mo8361ms(hmX);
            m39274dG().mo14348am("deaccelerate", "");
        } else if (cNu() == C6483ann.DEACCELERATE) {
            this.hnr = 0;
        } else {
            return;
        }
        cMX();
    }

    @C0064Am(aul = "5cc27447c4718e5d2a02b24945428d6d", aum = 0)
    /* renamed from: fg */
    private void m39283fg() {
        if (!(cNE() == null || cNE().getEventManager() == null)) {
            cNE().getEventManager().mo13975h(new C5268aCu());
        }
        mo22135dL().dxS().mo210d(PlayerSessionLog.C0024b.PLAYER_LOGOUT);
        if (mo22061al() != null) {
            mo22061al().ahI();
            mo22061al().ahK();
        }
        if (!bGZ()) {
            try {
                asy();
            } catch (Exception e) {
                e.printStackTrace();
            }
            cOU();
            cNE().cnp();
        }
        mo22135dL().dtM();
    }

    @C0064Am(aul = "7ac0f0b15eed0f9292ef9bbe32621c78", aum = 0)
    private List<C1540Wd<C0286Dh, Float>> cNg() {
        Pawn aYa = aYa();
        float lengthSquared = aYa.mo958IL().getBoundingBox().lengthSquared();
        List<Actor> c = aYa.mo965Zc().mo1900c(aYa.getPosition(), hmR + aYa.mo958IL().getBoundingBox().length());
        ArrayList arrayList = new ArrayList();
        for (Actor next : c) {
            if (next instanceof C0286Dh) {
                arrayList.add(new C1540Wd((C0286Dh) next, Float.valueOf((aYa.mo1013bx(next) - lengthSquared) - next.mo958IL().getBoundingBox().lengthSquared())));
            }
        }
        Collections.sort(arrayList, new C3626i());
        return arrayList;
    }

    @C0064Am(aul = "21a75effd38a8f32e48fd73f1224cfc3", aum = 0)
    private void cNi() {
        cuN().cOp();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "7c4156f5d04fdd5bee896da912d40dcc", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    /* renamed from: m */
    private void m39322m(Gate fFVar) {
        try {
            ald().getEventManager().mo13975h(new C1274Sq(C1274Sq.C1275a.ENTER_GATE));
        } catch (Exception e) {
            mo8354g("Ignoring exception during enter gate", (Throwable) e);
        }
        mo22135dL().mo14419f((C1506WA) new C5783aaP(C5783aaP.C1841a.JUMPING));
        mo22148lB(0.0f);
        m39288h(C3627j.JUMP);
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "0ef35c4b927e1479ee784a227a70b8c6", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cNj() {
        m39273d(C3627j.JUMP);
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "afb032f3547cfc131fce2e28efb1267b", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void aYl() {
        if (mo22061al().ahO() != null) {
            mo22152lJ(mo22061al().ahO().mo2572ra());
        } else {
            mo22152lJ(mo22061al().mo1091ra());
        }
    }

    @C0064Am(aul = "8684cfc74a01d2bdfec068878590e3d2", aum = 0)
    @ClientOnly
    /* renamed from: o */
    private Actor m39324o(SceneObject sceneObject) {
        if (sceneObject != null) {
            return (Actor) sceneObject.getClientProperty("actor");
        }
        return null;
    }

    @C0064Am(aul = "a29fc7b1066b6f533a1eb6a2e6917499", aum = 0)
    @ClientOnly
    private AdvertiseManager cNn() {
        if (this.hna == null) {
            AdvertiseManager py = (AdvertiseManager) bFf().mo6865M(AdvertiseManager.class);
            py.mo10S();
            this.hna = py;
        }
        return this.hna;
    }

    @C0064Am(aul = "7acd6087f7974fdfc46ad419760c1539", aum = 0)
    @ClientOnly
    private boolean cNr() {
        return this.hnc;
    }

    @C0064Am(aul = "3fc33da54e63baa85be6f832d822690b", aum = 0)
    private C6483ann cNt() {
        return this.hnd;
    }

    @C0064Am(aul = "0237384706d6bb54fca554fec9a20173", aum = 0)
    private C6483ann cNv() {
        return this.hne;
    }

    @C0064Am(aul = "eb98826b586c828ee2e8738869aa088c", aum = 0)
    private ConfigManagerSection cNx() {
        IEngineGame ald = C5916acs.getSingolton();
        ConfigManager bhb = ald.getConfigManager();
        String name = ald.mo4089dL().getName();
        try {
            ConfigManagerSection kW = bhb.getSection(name);
            if (kW == null) {
                return m39317ls(name);
            }
            return kW;
        } catch (Exception e) {
            System.out.println("Creating config section for player " + name);
            if (0 == 0) {
                return m39317ls(name);
            }
            return null;
        } catch (Throwable th) {
            if (0 == 0) {
                m39317ls(name);
            }
            throw th;
        }
    }

    @C0064Am(aul = "9d238bc22a55efb67e64257e24c1a311", aum = 0)
    private C6483ann cNz() {
        return this.hnf;
    }

    @C0064Am(aul = "88e28253330607cd79253070489f6995", aum = 0)
    private C6483ann cNB() {
        return this.hng;
    }

    @C0064Am(aul = "2d91c85fcb3a7b0868edfa1c2c1ecabc", aum = 0)
    @ClientOnly
    private EngineGame cND() {
        return (EngineGame) ald();
    }

    @C0064Am(aul = "810a4bf6aeccc6cafb802c1428a2850b", aum = 0)
    /* renamed from: Vd */
    private Node m39228Vd() {
        return m39227Vb();
    }

    @C0064Am(aul = "b2c093192588e5e28163ae0a8424b8ac", aum = 0)
    /* renamed from: dK */
    private Player m39275dK() {
        return m39274dG();
    }

    @C0064Am(aul = "bde89fae20cffbe30056d76609528fc0", aum = 0)
    @Deprecated
    /* renamed from: bK */
    private SceneObject m39256bK(Actor cr) {
        return cr.cHg();
    }

    @C0064Am(aul = "6bdb12ed0634a8d0f164bedd23413846", aum = 0)
    /* renamed from: a */
    private float m39229a(SceneObject sceneObject, ShipType ng, boolean z) {
        sceneObject.computeWorldSpaceAABB();
        float dir = sceneObject.computeLocalSpaceAABB().dir();
        IEngineGraphics ale = ald().getEngineGraphics();
        return ((float) (((double) dir) / ((Math.tan((double) ((float) ((((double) ale.adY().getFovY()) * 0.017453292519943295d) * 0.5d))) * 2.0d) * ((double) hmW)))) * ((float) ale.aet());
    }

    @C0064Am(aul = "410e0f656784cb7913f05d4043ec8471", aum = 0)
    /* renamed from: Mo */
    private Ship m39224Mo() {
        return (Ship) aYa();
    }

    @C0064Am(aul = "31bce84ad56bb0046db1646e9daf85ef", aum = 0)
    /* renamed from: w */
    private Vec3f m39328w(float f, float f2) {
        return null;
    }

    @C0064Am(aul = "b5c0efc64c12454a287bac1456ba6155", aum = 0)
    @ClientOnly
    private void cNF() {
        ald().getEngineGraphics();
        if (this.hnb) {
            this.hnb = false;
            this.hnc = true;
            mo22143ho(0.03f);
        }
    }

    @C0064Am(aul = "f044ef2f747b694e5d047f330eb98b76", aum = 0)
    @ClientOnly
    private void cNH() {
        m39274dG().ald().getEventManager().mo13975h(new C5839abT());
    }

    @C0064Am(aul = "c68802a8e0e6db9d5d07ec7f9f475930", aum = 0)
    @ClientOnly
    private void cNJ() {
        m39274dG().ald().getEventManager().mo13975h(new C2327eB(false));
    }

    @C0064Am(aul = "524254350f21130222a97dafbaf1ad08", aum = 0)
    @ClientOnly
    /* renamed from: im */
    private void m39298im(boolean z) {
        cNQ();
        this.hnv = true;
        if (!bGX() || ald().getEngineGraphics() != null) {
            if (z) {
                m39274dG().ald().getEventManager().mo13975h(new C3949xF(this.bHv));
            }
            ald().bhh().aRT();
        }
    }

    @C0064Am(aul = "2adc6eb22b101533516fea06750d6ac5", aum = 0)
    @ClientOnly
    /* renamed from: io */
    private void m39300io(boolean z) {
        cNQ();
        this.hnv = false;
        if (z) {
            m39274dG().ald().getEventManager().mo13975h(new C3949xF(this.bHv));
        }
        ald().bhh().aRR();
    }

    @C0064Am(aul = "0b7a490b284a06fabddcb8efac04f6b9", aum = 0)
    @ClientOnly
    private boolean cNL() {
        Collection<C3173oj> b = C5916acs.getSingolton().getEventManager().mo13967b(C3173oj.class);
        if (b == null || b.size() <= 0) {
            return false;
        }
        if (((C3173oj) b.toArray()[0]).ddW() == C3173oj.C3174a.ORBITAL) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "858b50fefa288f2a24509d1d75df2b3e", aum = 0)
    @ClientOnly
    private boolean cNN() {
        return this.hnu;
    }

    @C0064Am(aul = "addce871821ccfde79f6a84700e53c7c", aum = 0)
    @ClientOnly
    /* renamed from: iq */
    private void m39302iq(boolean z) {
        this.hnu = z;
    }

    @C0064Am(aul = "32fb4379a37dd11ec441ac0b9b6cad30", aum = 0)
    private void cNP() {
        PlayerController alb;
        if (bGX() && (alb = ald().alb()) != null && cuN() != alb) {
            //ВЗАИМОДЕЙСТВИЕ С ДРУГОМ КОНТРОЛЛЕРОМ ИГРОКА
            throw new RuntimeException("############ INTERACTING WITH SOMEONE ELSE PLAYER CONTROLLER #############");
        }
    }

    @C0064Am(aul = "464546dd6b5aebbabe07b6ff957b1fd2", aum = 0)
    @ClientOnly
    private void cNR() {
        cNQ();
        this.fjY = 0.0f;
        this.fjZ = 0.0f;
        this.hnv = false;
        this.hnm = null;
        if (this.hnt != null) {
            this.hnt.dispose();
            this.hnt = null;
        }
        this.hnb = false;
        this.hnc = false;
    }

    @C0064Am(aul = "8902bbd4e0a2696a4e81294e17ff5853", aum = 0)
    private boolean cNS() {
        return cMT();
    }

    @C0064Am(aul = "b56d17042ed1cd5324fd7f8ce52d72d0", aum = 0)
    @ClientOnly
    private boolean cNU() {
        return this.hnv;
    }

    @C0064Am(aul = "db21022b600db4ec5c54d322d509734b", aum = 0)
    @ClientOnly
    private boolean cNV() {
        return this.hnj;
    }

    @C0064Am(aul = "38cad9bfdcaa1d8567146d9fa8d50e22", aum = 0)
    /* renamed from: a */
    private void m39235a(C3627j jVar) {
        Node rPVar;
        if (jVar == C3627j.DOCK) {
            this.hnq = new C3625h();
            ald().getLoaderTrail().mo4993a(this.hnq);
        }
        cNQ();
        if (bGX()) {
            ((Ship) cuN().aYa()).mo18306bt(true);
            IEngineGraphics ale = ald().getEngineGraphics();
            if (ale != null) {
                Asset bkf = ((Ship) cuN().aYa()).agH().bkf();
                if (bkf != null) {
                    ale.mo3049bV(bkf.getFile());
                    this.boA = (SPitchedSet) ale.mo3047bT(bkf.getHandle());
                    if (this.boA != null) {
                        cuN().mo22061al().mo18305b(this.boA);
                        this.boA.play();
                        ale.adZ().addChild(this.boA);
                    }
                }
                Node azW = cuN().aYa().azW();
                if (azW != null) {
                    this.hnm = azW;
                    rPVar = azW;
                } else {
                    System.err.println(" WARNING: Player is not on a node, using last node scenario! ");
                    rPVar = this.hnm;
                }
                ald().getLoaderTrail().mo4996a(rPVar.mo21663ir(), rPVar.mo21662ip(), ald().getEngineGraphics().adZ(), new C3621d(), "PlayerControllerInfra:LoadSpace", 5);
            }
        }
    }

    @C0064Am(aul = "66f654279a2245f161dbbc3375b10518", aum = 0)
    @ClientOnly
    /* renamed from: bM */
    private void m39257bM(Actor cr) {
        mo22071bJ(cr);
    }

    @C0064Am(aul = "f82ef2a5905ffde005fc34751068d84b", aum = 0)
    @ClientOnly
    /* renamed from: aq */
    private void m39245aq(Actor cr) {
        if (!(cr instanceof Shot)) {
            mo22135dL().mo14419f((C1506WA) new C6898avm(cr));
            if ((cr instanceof Loot) && mo22061al() != null && mo22061al().agB() == null && ((Loot) cr).dtC() == mo22135dL()) {
                mo22135dL().mo14419f((C1506WA) new C5893acV(cr));
            }
        }
    }

    @C0064Am(aul = "77f6f14468d8e8911dd54e2ea36a16c1", aum = 0)
    @ClientOnly
    /* renamed from: as */
    private void m39246as(Actor cr) {
        if (!(cr instanceof Shot)) {
            mo22135dL().mo14419f((C1506WA) new C2041bS(cr));
            mo22074bR(cr);
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "9d2b903b41ef6421ed610f5e41283a7e", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void aYj() {
        if (!aYa().isDead()) {
            cOC();
        }
        mo22135dL().mo14419f((C1506WA) new C6126agu(true));
        mo22152lJ(mo22061al().mo1091ra());
        if (this.hny != null) {
            this.hny.mo3296xO();
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "16a2d109509f438ff032716f68ad924d", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void aYk() {
        cNG();
        mo22152lJ(mo22061al().mo1091ra());
        mo22135dL().mo14419f((C1506WA) new C6126agu(false));
        mo8361ms(hmX);
        if (this.hny != null) {
            this.hny.mo3297xP();
        }
    }

    @C0064Am(aul = "152543439f187b3f0090fa538f4878c1", aum = 0)
    private void cNZ() {
        mo22061al().ahI();
        mo22061al().ahK();
        mo22149lD(0.0f);
        mo22150lF(0.0f);
        mo22151lH(0.0f);
        m39274dG().mo14348am(this.bHv ? "combat" : "piloting", "");
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "920f13c5871b1e665dd867bc9b900d95", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cOb() {
        if (mo22061al().agZ()) {
            cNG();
        }
        cOE();
        mo22135dL().mo14419f((C1506WA) new C2996mm());
        this.hni = cVr();
        this.hnx = true;
        this.hnw = true;
        mo8361ms(hmX);
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "635a83ac2a042a9f8baddb748909c818", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cOd() {
        m39288h(C3627j.DOCK);
        resetView();
        cOI();
        mo22135dL().mo14419f((C1506WA) new C5783aaP(C5783aaP.C1841a.DOCKED));
        m39274dG().mo14348am("dock", m39274dG().bhE().getName());
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "e6c2d0ffb4e94c21e7c4205388c83faf", aum = 0)
    @C2499fr
    private void cOh() {
        m39273d(C3627j.DOCK);
        ald().getEventManager().mo13975h(new C1274Sq(C1274Sq.C1275a.UNDOCK));
        mo22146iv(true);
        this.hnr = 0;
        mo22152lJ(mo22061al().agH().mo4217ra() * 0.5f);
        m39274dG().mo14348am("undock", "");
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "906b371745dc10aaaba9daae6629f158", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void aYn() {
        mo22135dL().mo14419f((C1506WA) new C1278St());
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "6b64fc13ae769aa2ff388d7258e1a260", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cOj() {
    }

    @C0064Am(aul = "bd92a675f138fcb27c554a19d50f395f", aum = 0)
    @ClientOnly
    /* renamed from: ly */
    private void m39320ly(float f) {
        String str;
        String str2;
        if (cVr() - this.hni < 10000) {
            if (this.hnx) {
                NLSWindowAlert ags = (NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT);
                if (cMU() != null) {
                    str = cMU().getName();
                } else {
                    str = "";
                }
                if (cMU() instanceof HazardArea) {
                    str2 = ags.dbu().get();
                } else {
                    str2 = ags.dbs().get();
                }
                C5850abe abe = new C5850abe();
                abe.mo12495d(new C2733jJ(str2, true, false, C6128agw.C1890a.RED, str));
                C0575Hy hy = new C0575Hy(new C3622e(), ags.dcm().get(), 0, 1000, C6128agw.C1890a.WHITE, 10000);
                hy.start();
                abe.mo12495d(hy);
                mo22135dL().mo14419f((C1506WA) abe);
            }
            mo8361ms(1.0f);
        } else {
            this.hni = 0;
            this.hnw = false;
            cNY();
        }
        this.hnx = false;
    }

    @C0064Am(aul = "f7f4c25db72a9dd284b668e30b43fa4b", aum = 0)
    private PlayerController cOl() {
        return this;
    }

    @C0064Am(aul = "6ec98293e31be4f2f452ffca959c1896", aum = 0)
    @ClientOnly
    /* renamed from: l */
    private void m39309l(Pawn avi) {
        if (this.hnp == null) {
            ald().getLoaderTrail().mo4995a("data/scene/defaults.pro", "default_spacedust_tracer", (Scene) null, new C3620c(avi), "PlayerController:playerShipReady ");
        } else {
            m39326o(avi);
        }
    }

    @C0064Am(aul = "c33f8a2877f319e3a90040fc498990c4", aum = 0)
    @ClientOnly
    /* renamed from: n */
    private void m39323n(Pawn avi) {
        if (this.hnp != null) {
            this.hnp.setReferenceObject(avi.cHg());
            C5916acs.getSingolton().getEngineGraphics().adZ().addChild(this.hnp);
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "47049f32419ca6583e90c2a5a80f2167", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    /* renamed from: aI */
    private void m39241aI(String str, String str2) {
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "78122c2d22378605860a06a5ca838964", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    /* renamed from: bQ */
    private void m39261bQ(Asset tCVar) {
        SoundPlayer.aWp().mo2917eC(tCVar.getHandle());
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "5b6fb11ac6d49398d2dbe9ec0cb999b0", aum = 0)
    @C2499fr
    private void cOm() {
        m39274dG().mo14419f((C1506WA) new C3002mq(0));
    }

    @C0064Am(aul = "ed0693fb8a06c24e4aad1fc852afe329", aum = 0)
    @ClientOnly
    private void cOo() {
        m39274dG().mo14419f((C1506WA) new C3002mq(0, new C3618b(), false));
    }

    @C0064Am(aul = "4da52270230355d67b4601342a42c763", aum = 0)
    private boolean cOq() {
        return mo22061al().agr().mo16383bW(mo22135dL());
    }

    @C0064Am(aul = "78d500bf03e56827d6bcf9da742a696c", aum = 0)
    /* renamed from: bO */
    private void m39258bO(Actor cr) {
        SceneObject bL = mo22072bL(cr);
        if (bL != null) {
            bL.putClientProperty("actor", null);
        }
    }

    @C0064Am(aul = "699277008ea4507a50848224e36fd99d", aum = 0)
    @ClientOnly
    /* renamed from: bQ */
    private void m39260bQ(Actor cr) {
        cNQ();
        m39259bP(cr);
    }

    @C0064Am(aul = "799158d14507087fc12d60824b1f0098", aum = 0)
    @ClientOnly
    /* renamed from: hn */
    private void m39289hn(float f) {
        mo8361ms(f);
    }

    @C0064Am(aul = "a240298661de77eb0146d1d8fbd347b0", aum = 0)
    @ClientOnly
    private void cOw() {
    }

    @C0064Am(aul = "822a3bc90244191296024cc5ab2ee3d6", aum = 0)
    /* renamed from: y */
    private float m39330y(float f, float f2) {
        return f;
    }

    @C0064Am(aul = "ce722566ac00b0dfcb16f7e6d667f209", aum = 0)
    /* renamed from: a */
    private void m39231a(C0286Dh dh, float f) {
        C0286Dh.C0287a aVar = C0286Dh.C0287a.ERROR_OUT_OF_RANGE;
        if (dh.mo599Y(f)) {
            try {
                if (!cMV() || !(dh instanceof Loot)) {
                    aVar = dh.mo634d(mo22135dL());
                } else {
                    mo22147iy(false);
                    aVar = ((Loot) dh).mo8570ah(mo22135dL());
                }
            } catch (C5475aKt e) {
                C5475aKt akt = e;
                aVar = C0286Dh.C0287a.ERROR_UNKNOWN;
                mo8352e("Error while " + aYa() + " was trying to interact with " + dh, (Throwable) akt);
            }
        }
        if (aVar != C0286Dh.C0287a.OK) {
            mo22065b(aVar, dh);
        }
        cOS();
    }

    @C0064Am(aul = "8ae6c946a9786deae4a83b6f79c07064", aum = 0)
    /* renamed from: iu */
    private void m39305iu(boolean z) {
        m39304it(z);
        m39295ii(z);
    }

    @C0064Am(aul = "548966f5c9d86b025ac0a2cb661bcd1f", aum = 0)
    @ClientOnly
    private void cOz() {
        if (!C5877acF.RUNNING_CLIENT_BOT) {
            m39274dG().ald().bhh().aRT();
        }
        this.bHv = true;
        m39299in(true);
    }

    @C0064Am(aul = "b7f58d624156b4afb5921598669880ab", aum = 0)
    /* renamed from: lA */
    private void m39310lA(float f) {
        if (cNT()) {
            float ra = (((1.0f + f) * (aYa().mo1091ra() + hmV)) - 40.0f) / 2.0f;
            if (Math.abs(ra) <= 3.0f) {
                ra = 0.0f;
            }
            if (ra != aYa().aSj()) {
                aYa().mo964W(ra);
                mo22135dL().mo14419f((C1506WA) new C2946lw(ra));
                cMX();
            }
        }
    }

    @C0064Am(aul = "9f621a2dcb9b25830f99f33df0d3768a", aum = 0)
    /* renamed from: lC */
    private void m39311lC(float f) {
        if (cNT()) {
            Ship fAVar = (Ship) aYa();
            float VH = fAVar.mo963VH() * fAVar.afZ() * f;
            if (VH != fAVar.aSg()) {
                fAVar.mo1063f(fAVar.aSh(), fAVar.aSi(), VH);
                cMX();
            }
        }
    }

    @C0064Am(aul = "3a6f3039434058ccd0e7a3c029b6f290", aum = 0)
    /* renamed from: lE */
    private void m39312lE(float f) {
        if (cNT()) {
            float agb = ((Ship) aYa()).agb() * aYa().mo963VH() * f;
            if (agb != aYa().aSh()) {
                aYa().mo1063f(agb, aYa().aSi(), aYa().aSg());
                cMX();
            }
        }
    }

    @C0064Am(aul = "bcfc92323c026a828709f00c0abbac18", aum = 0)
    /* renamed from: lG */
    private void m39313lG(float f) {
        if (cNT()) {
            Ship fAVar = (Ship) aYa();
            float VH = fAVar.mo963VH() * f;
            if (VH != fAVar.aSi()) {
                fAVar.mo1063f(fAVar.aSh(), VH, fAVar.aSg());
                cMX();
            }
        }
    }

    @C0064Am(aul = "ee1780b2dd39853506feffb5c0c08219", aum = 0)
    /* renamed from: a */
    private void m39233a(C6483ann ann) {
        this.hnd = ann;
    }

    @C0064Am(aul = "a60796202eb2065ae5dac731d9c78a0c", aum = 0)
    /* renamed from: c */
    private void m39268c(C6483ann ann) {
        this.hne = ann;
    }

    @C0064Am(aul = "4da59d4083ec8f3c1aecbd88c876dae2", aum = 0)
    /* renamed from: e */
    private void m39279e(C6483ann ann) {
        this.hnf = ann;
    }

    @C0064Am(aul = "d8158954b06a7a9460e56c3955d63a3d", aum = 0)
    @ClientOnly
    /* renamed from: lI */
    private void m39314lI(float f) {
        this.hns = ((2.0f * (f + hmV)) / (aYa().mo1091ra() + hmV)) - 1.0f;
        mo22148lB(this.hns);
    }

    @C0064Am(aul = "e04446120bb12c0e0049be25b78c31a2", aum = 0)
    /* renamed from: g */
    private void m39285g(C6483ann ann) {
        this.hng = ann;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "e84d7bfcea17c04b9242b66085021945", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    /* renamed from: fI */
    private void m39282fI(float f) {
        mo22061al().mo964W(f);
    }

    @C0064Am(aul = "b9a2dc320eb49a64a532199afb3ec435", aum = 0)
    /* renamed from: bS */
    private void m39262bS(Actor cr) {
        if (cMU() != null) {
            cMU().mo8355h(C3161oY.C3162a.class, this);
        }
        m39252bF(cr);
        if (cMU() != null) {
            cMU().mo8348d(C3161oY.C3162a.class, this);
        }
    }

    @C0064Am(aul = "e8f8b142d2fb197bcbadf2515955f3af", aum = 0)
    /* renamed from: b */
    private void m39249b(Node rPVar) {
        m39234a(rPVar);
    }

    @C0064Am(aul = "563962de7e16a464cb1db9d147da670a", aum = 0)
    /* renamed from: d */
    private void m39272d(Pawn avi) {
        if (!(avi == aYa() || mo22135dL() == null)) {
            mo22135dL().mo14419f((C1506WA) new C2651i((Ship) aYa(), (Ship) avi));
        }
        super.mo3293e(avi);
    }

    @C0064Am(aul = "2135bec9bd3c51f6ed17af9b01529fda", aum = 0)
    /* renamed from: o */
    private void m39325o(Player aku) {
        m39232a(aku);
    }

    @C0064Am(aul = "ce2b4c31123600646c94def5bdd60103", aum = 0)
    /* renamed from: lK */
    private void m39315lK(float f) {
        this.hns = f;
        mo8361ms(hmX);
    }

    @C0064Am(aul = "6c8a865257117c8171d0b18a5fa9f7d1", aum = 0)
    @ClientOnly
    private void cOB() {
        this.hnb = true;
        this.hnc = false;
        mo22143ho(0.03f);
    }

    @C0064Am(aul = "e8d62ea71ce5e58cb96b53f4593f5a2f", aum = 0)
    @ClientOnly
    private void cOD() {
        float f = 100.0f;
        if (mo22135dL().bQx().cHg() != null) {
            f = (float) mo22135dL().bQx().cHg().getAabbLSLenght();
        }
        ald().getEventManager().mo13975h(new C6060afg(mo22135dL().bQx().cHg(), (Vec3d) null, 1, false, f));
    }

    @C0064Am(aul = "cc4d651469130702938d9563e84d413d", aum = 0)
    @ClientOnly
    private void cOF() {
        if (mo22135dL().bQB()) {
            m39274dG().ald().getEventManager().mo13975h(new C2327eB(true));
        }
    }

    @C0064Am(aul = "b767efced38bf4bf827b09b3f0eb54f8", aum = 0)
    @ClientOnly
    private void cOH() {
        Station bf = (Station) cuN().mo22135dL().bhE();
        NLSWindowAlert ags = (NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT);
        if (!aMU.CREATED_PLAYER.equals(m39274dG().cXm())) {
            m39274dG().mo14419f((C1506WA) new C2733jJ(ags.dck().get(), false, bf.getName()));
        }
        ald().getEventManager().mo13975h(new C1274Sq(C1274Sq.C1275a.DOCK));
        mo22054Ff();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "618af9472b64bc091f8f7a2d4e131167", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m39230a(C0286Dh.C0287a aVar, C0286Dh dh) {
        I18NString i18NString = null;
        NLSInteraction ale = (NLSInteraction) ala().aIY().mo6310c(NLSManager.C1472a.INTERACTION);
        if (aVar == C0286Dh.C0287a.ERROR_BANNED_FACTION) {
            i18NString = ale.dhh();
        } else if (aVar == C0286Dh.C0287a.ERROR_CORP_EXCLUSIVE) {
            i18NString = ale.dhf();
        } else if (aVar == C0286Dh.C0287a.ERROR_CANT_GET_LOOT) {
            i18NString = ale.dhj();
        } else if (aVar == C0286Dh.C0287a.ERROR_CRUISE_SPEED) {
            i18NString = ale.dhl();
        } else if (aVar == C0286Dh.C0287a.ERROR_DEACTIVATED) {
            i18NString = ale.dhn();
        } else if (aVar == C0286Dh.C0287a.ERROR_OUT_OF_RANGE) {
            i18NString = ale.dhr();
        } else if (aVar == C0286Dh.C0287a.ERROR_OBJECT_IN_USE) {
            i18NString = ale.dhp();
        } else if (aVar == C0286Dh.C0287a.ERROR_NOT_ENOUGH_SPACE) {
            i18NString = ale.dht();
        } else if (aVar == C0286Dh.C0287a.ERROR_NOT_ENOUGH_MONEY) {
            i18NString = ale.dhv();
        } else if (aVar == C0286Dh.C0287a.CITIZENSHIP_RESTRICTED_ACCESS) {
            i18NString = ale.dhx();
        } else if (aVar == C0286Dh.C0287a.CITIZENSHIP_ONLY) {
            i18NString = ale.dhz();
        } else if (aVar == C0286Dh.C0287a.PASS_UNAUTHORIZED) {
            i18NString = ale.dhB();
        }
        if (i18NString == null) {
            return;
        }
        if (dh == null || dh.getName() == null) {
            mo22135dL().mo14419f((C1506WA) new C2733jJ(i18NString.get(), false, true, C6128agw.C1890a.RED, new Object[0]));
            return;
        }
        mo22135dL().mo14419f((C1506WA) new C2733jJ(i18NString.get(), false, true, C6128agw.C1890a.RED, dh.getName()));
    }

    @C0064Am(aul = "0c5a99e6a657a505c5baf49aca3289ff", aum = 0)
    @ClientOnly
    private void cOJ() {
        if (ald().getEngineGraphics() != null) {
            cNQ();
            float f = 100.0f;
            if (mo22135dL().bQx().cHg() != null) {
                f = (float) mo22135dL().bQx().cHg().getAabbLSLenght();
            }
            if (m39274dG().bQB()) {
                ald().getEventManager().mo13975h(new C6531aoj());
            } else {
                ald().getEventManager().mo13975h(new C6060afg(mo22135dL().bQx().cHg(), (Vec3d) null, 0, false, f));
            }
        }
    }

    @C0064Am(aul = "323203c279108e32fd89111664495fd0", aum = 0)
    @ClientOnly
    private void cOL() {
        this.hnb = false;
        this.hnc = false;
        ald().getLoaderTrail().mo5007vr();
    }

    @C0064Am(aul = "44294607fe9b0e8bc50e98c9105d4dce", aum = 0)
    @ClientOnly
    /* renamed from: c */
    private void m39269c(C3627j jVar) {
        ald().getLoaderTrail().mo5007vr();
        if (!(jVar == C3627j.JUMP || ald().getEngineGraphics() == null)) {
            boolean brL = ((EngineGame) ald()).cnr().cYc().brL();
            try {
                synchronized (ald().getEngineGraphics().aee().getTreeLock()) {
                    ald().getEngineGraphics().aee().getTreeLock().wait(2000);
                }
            } catch (InterruptedException e) {
                try {
                    e.printStackTrace();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    if (brL) {
                        try {
                            ((EngineGame) ald()).cnr().cYc().brK();
                        } catch (InterruptedException e2) {
                            e2.printStackTrace();
                        }
                    }
                    throw th2;
                }
            }
            if (brL) {
                try {
                    ((EngineGame) ald()).cnr().cYc().brK();
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
        }
        ald().getEventManager().mo13975h(new aIQ(0.4f));
        m39281f(jVar);
    }

    @C0064Am(aul = "0e1d510043b7d217295392dec4cb46ae", aum = 0)
    @ClientOnly
    /* renamed from: e */
    private void m39280e(C3627j jVar) {
        ald().getEventManager().mo13975h(new aIQ(0.5f));
        if (jVar == C3627j.DOCK) {
            ald().getEventManager().mo13975h(new C1274Sq(C1274Sq.C1275a.UNDOCK));
            mo22055Fg();
        }
        ald().getEventManager().mo13965a(C2640ht.class, new C3623f());
        ald().getEventManager().mo13965a(C3002mq.class, new C3624g());
        m39250b(jVar);
        cOP();
        mo22135dL().dxo();
        mo22135dL().mo14419f((C1506WA) new C5783aaP(C5783aaP.C1841a.FLYING));
        if (jVar == C3627j.DOCK) {
            cOA();
        }
        if (jVar == C3627j.JUMP) {
            cPn();
        }
        ald().getEventManager().mo13975h(new aIQ(0.8f));
        mo22061al().mo965Zc().mo1893b(cVr(), (Actor) mo22061al());
    }

    @C0064Am(aul = "8511d4c9d2acd1cd9ab9fcced7aa2ca8", aum = 0)
    @ClientOnly
    private void cOM() {
        if (isDebugEnabled()) {
            mo8360mc("Start interacting");
        }
        this.hnj = true;
    }

    @C0064Am(aul = "e0b5b2d39a411cf6d72997a2d7862772", aum = 0)
    @ClientOnly
    private void cOO() {
        ala().aLW().mo11535cw(aYa());
        m39244ap(aYa().getPosition());
    }

    @C0064Am(aul = "c5ff8061833c0e462b150d17e20e7a8f", aum = 0)
    /* renamed from: ao */
    private void m39243ao(Vec3d ajr) {
        cNQ();
        IEngineGraphics ale = ald().getEngineGraphics();
        if (ale != null) {
            this.screenHeight = ale.aet();
            this.screenWidth = ale.aes();
            this.fkb = (int) (((float) this.screenHeight) * 0.5f);
            this.fkc = (int) (((float) this.screenWidth) * 0.5f);
        }
        if (ala().aIW().avs() != null) {
            if (ale != null) {
                ale.mo3049bV(ala().aIW().avs());
            }
            bGX();
            cuN().cNo().mo4713r(cuN().mo22061al().azW().aac());
            this.hnc = false;
            return;
        }
        throw new NullPointerException("MarkFile is null in MarkManager");
    }

    @C0064Am(aul = "0cdd10594c0f12d017c17b976c6a4c8a", aum = 0)
    @ClientOnly
    private void cOQ() {
        IEngineGraphics ale = ald().getEngineGraphics();
        if (ale != null) {
            ale.adV();
        }
    }

    @C0064Am(aul = "6c7076709f42f142541245881075c8a0", aum = 0)
    @ClientOnly
    /* renamed from: g */
    private void m39286g(C3627j jVar) {
        if (jVar == C3627j.JUMP) {
            cPl();
        }
        cOZ();
        cOU();
        mo22135dL().dxo();
        if (mo22061al() != null) {
            mo22061al().ahI();
        }
        if (!C5877acF.RUNNING_CLIENT_BOT && jVar == C3627j.DOCK) {
            ald().bhh().aRR();
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "f6b88353f829510f4d3d65c02d3e8f1f", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cOR() {
        if (isDebugEnabled()) {
            mo8360mc("Stopped interacting");
        }
        this.hnj = false;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "18db031867a6d52ca21cfbc104b6a2f2", aum = 0)
    @C2499fr
    private void cOT() {
        cNQ();
        if (this.hnp != null) {
            this.hnp.setRender(false);
            ald().getEngineGraphics().adZ().removeChild(this.hnp);
            this.hnp.dispose();
            this.hnp = null;
        }
        if (!bGX() || ald().getEngineGraphics() != null) {
            cuN().cNo().mo4713r((Collection<Advertise>) null);
            ala().aLW().dvM();
        }
    }

    @C0064Am(aul = "4766a2812a11db86dfb00eb5060ddc60", aum = 0)
    /* renamed from: U */
    private void m39225U(float f) {
        super.mo4709V(f);
        IEngineGraphics ale = ald().getEngineGraphics();
        if (!bGX() || ale != null) {
            if (!cNO() && cuN().aYa() != null && !cuN().aYa().isDead()) {
                if (cuN().aYa().isDead() || !cuN().aYa().bae()) {
                    this.hnc = false;
                }
                mo22143ho(0.03f);
            }
            if (aYa() == null || !this.hnw) {
                m39319lx(f);
            } else {
                m39321lz(f);
            }
        }
    }

    @C0064Am(aul = "1e8356bee8310dde17ea184f7c432769", aum = 0)
    private void cOV() {
        mo22059aR(true);
    }

    @C0064Am(aul = "99910e9537a616d9fd99e6630aa8b4ba", aum = 0)
    /* renamed from: iw */
    private void m39306iw(boolean z) {
        this.bHv = !this.bHv;
        if (this.bHv) {
            m39299in(z);
        } else {
            m39301ip(z);
        }
        cOa();
        cNd();
    }

    @C0064Am(aul = "5fe4e3da22fafb77df927738d1dff639", aum = 0)
    @ClientOnly
    private void cOX() {
        mo22069b(aYa() != null, "The controller must be controlling a paw to be able to undock");
        if (cOr()) {
            cNI();
            cNK();
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "d3d5702838fe5f16e12639debaeb62e4", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cOY() {
        if (this.hnt != null) {
            cNQ();
            if (bGX()) {
                if (this.boA != null) {
                    this.boA.stop();
                }
                this.hnt.dispose();
                this.hnt = null;
                if (ald().getEngineGraphics() != null) {
                    ald().getEngineGraphics().adV();
                }
            }
        }
    }

    @C0064Am(aul = "224c990cb639ef075b36cdf01c079818", aum = 0)
    @ClientOnly
    private boolean cPa() {
        return this.hny != null;
    }

    @C4034yP
    @C0064Am(aul = "ceace58088355300fedfc14b459d9869", aum = 0)
    @ClientOnly
    /* renamed from: bU */
    private void m39263bU(Actor cr) {
        if (this.hny != null) {
            cPd();
        }
        if (!anX()) {
            PlayerOrbitalController vNVar = (PlayerOrbitalController) bFf().mo6865M(PlayerOrbitalController.class);
            vNVar.mo10S();
            vNVar.mo3293e(mo22061al());
            vNVar.mo22574ba(cr);
            this.hny = vNVar;
            mo22135dL().mo14419f((C1506WA) new C4135zz(C4135zz.C4136a.ORBIT, true));
            String str = "other";
            if (cr instanceof Station) {
                str = "station";
            } else if (cr instanceof Asteroid) {
                str = "asteroid";
            } else if (cr instanceof Gate) {
                str = "gate";
            }
            mo22135dL().mo14419f((C1506WA) new C6018aeq("autoPilot.orbit", str));
        }
    }

    @C4034yP
    @C0064Am(aul = "bae4caf0d37c65472797702c9bfb50e6", aum = 0)
    @ClientOnly
    /* renamed from: d */
    private void m39270d(Actor cr, boolean z) {
        if (this.hny != null) {
            cPd();
        }
        if (cr != null && !anX()) {
            if (!z || !(cr instanceof C0286Dh) || m39274dG().bQx() == null || m39274dG().bQx().mo1013bx(cr) >= m39266c((C0286Dh) cr)) {
                PlayerArrivalController on = (PlayerArrivalController) bFf().mo6865M(PlayerArrivalController.class);
                on.mo10S();
                on.mo3293e(mo22061al());
                on.mo4400c(cr, z);
                this.hny = on;
                if (z && (cr instanceof C0286Dh)) {
                    mo22135dL().mo14419f((C1506WA) new C4135zz(C4135zz.C4136a.GO_TO_INTERACT, true));
                    String str = "other";
                    if (cr instanceof Station) {
                        str = "station";
                    } else if (cr instanceof Gate) {
                        str = "gate";
                    } else if (cr instanceof Loot) {
                        str = "loot";
                    }
                    mo22135dL().mo14419f((C1506WA) new C6018aeq("autoPilot.gotoInteract", str));
                } else if (cr instanceof Ship) {
                    mo22135dL().mo14419f((C1506WA) new C4135zz(C4135zz.C4136a.FOLLOW, true));
                    mo22135dL().mo14419f((C1506WA) new C6018aeq("autoPilot.follow", ""));
                } else {
                    mo22135dL().mo14419f((C1506WA) new C4135zz(C4135zz.C4136a.GO_TO, true));
                    String str2 = "other";
                    if (cr instanceof Asteroid) {
                        str2 = "asteroid";
                    }
                    mo22135dL().mo14419f((C1506WA) new C6018aeq("autoPilot.goto", str2));
                }
            } else {
                m39265bX(cr);
            }
        }
    }

    @C0064Am(aul = "f6f7911c671d8482671385cf80edeb05", aum = 0)
    @ClientOnly
    /* renamed from: ay */
    private void m39247ay(float f) {
        if (mo22135dL() == getPlayer()) {
            if (this.hny != null && bGX()) {
                if (!this.hny.isRunning()) {
                    if ((this.hny instanceof PlayerArrivalController) && ((PlayerArrivalController) this.hny).cBz()) {
                        m39265bX(((PlayerArrivalController) this.hny).agB());
                    }
                    if ((this.hny instanceof PlayerArrivalController) && !((PlayerArrivalController) this.hny).cBz()) {
                        mo22135dL().mo14419f((C1506WA) new C5667aSd("SHUTDOWN", true));
                    }
                    cPd();
                } else {
                    this.hny.step(f);
                }
            }
            if (ald().getEngineGraphics() != null) {
                Camera camera = ald().getEngineGraphics().aea().getCamera();
                float determinant = camera.getTransform().orientation.determinant();
                if (Float.isNaN(determinant) || Float.isInfinite(determinant)) {
                    logger.error(String.valueOf(determinant) + " at camera: " + camera.getName() + " transform: " + camera.getTransform());
                    camera.setTransform(new Matrix4fWrap());
                    camera.setOrientation(mo22061al().getOrientation());
                    camera.setPosition(mo22061al().getPosition());
                    float f2 = 100.0f;
                    if (mo22135dL().bQx().cHg() != null) {
                        f2 = (float) mo22135dL().bQx().cHg().getAabbLSLenght();
                    }
                    ald().getEventManager().mo13975h(new C6060afg(mo22135dL().bQx().cHg(), (Vec3d) null, 0, false, f2));
                }
            }
        }
    }

    @C0064Am(aul = "acceda36772ecea65f9557919925ff6a", aum = 0)
    @ClientOnly
    private void cPc() {
        if (this.hny != null) {
            mo22135dL().mo14419f((C1506WA) new C4135zz());
            mo22135dL().mo14419f((C1506WA) new C6018aeq("autoPilot.cancel", ""));
            this.hny.stop();
            this.hny = null;
            mo22149lD(0.0f);
            mo22151lH(0.0f);
            mo22150lF(0.0f);
            if (mo22061al().agZ() || mo22061al().ahh()) {
                mo22135dL().mo14419f((C1506WA) new C5667aSd("SPECIAL1", true));
            }
        }
    }

    @C0064Am(aul = "405124c7ef4d8599fcb1ec2eecfa998f", aum = 0)
    @ClientOnly
    /* renamed from: bC */
    private void m39251bC(Vec3f vec3f) {
        if (this.hny != null) {
            cPd();
        }
        if (vec3f != null) {
            PlayerDirectionalController lEVar = (PlayerDirectionalController) bFf().mo6865M(PlayerDirectionalController.class);
            lEVar.mo10S();
            lEVar.mo20181b(mo22061al(), vec3f);
            this.hny = lEVar;
            mo22135dL().mo14419f((C1506WA) new C4135zz(C4135zz.C4136a.DIRECTION_CHANGE, true));
            mo22135dL().mo14419f((C1506WA) new C6018aeq("autoPilot.changeDirection", ""));
        }
    }

    @C0064Am(aul = "34bf72b5d1e7f0395946cb4bd77cfee9", aum = 0)
    @ClientOnly
    private RulesOverride cPe() {
        if (this.hnz == null) {
            RulesOverride aVar = (RulesOverride) bFf().mo6865M(RulesOverride.class);
            aVar.mo22171b(this);
            this.hnz = aVar;
            this.hnz.create();
        }
        return this.hnz;
    }

    @C0064Am(aul = "887cb678a58d908b2fe1f175944150c4", aum = 0)
    @ClientOnly
    private void cPg() {
        Ship bQx;
        if (!cPf().mo22169P() && (bQx = m39274dG().bQx()) != null) {
            m39265bX(bQx.agB());
        }
    }

    @C0064Am(aul = "c9975d1fe5bd7f69968bf68910249164", aum = 0)
    @ClientOnly
    /* renamed from: bW */
    private void m39264bW(Actor cr) {
        Ship bQx = m39274dG().bQx();
        if (bQx != null && cr != null && (cr instanceof C0286Dh)) {
            if (cr instanceof Gate) {
                Gate fFVar = (Gate) cr;
                if (fFVar.isEnabled() && fFVar.mo18393rw() > 0 && bQx.mo1013bx((Actor) fFVar) <= m39266c((C0286Dh) fFVar) && (m39274dG().bQG().mo20290r(fFVar) == null || !m39274dG().bQG().mo20290r(fFVar).cVv())) {
                    m39274dG().mo14419f((C1506WA) new C2393em(fFVar));
                    return;
                }
            }
            cON();
            try {
                mo22138e((C0286Dh) cr);
            } catch (C5475aKt e) {
                cOS();
            }
        }
    }

    @C0064Am(aul = "aee9d258161e08dc0224805938af2e2a", aum = 0)
    @ClientOnly
    private void cPi() {
        if (mo22061al().agB() instanceof Loot) {
            mo22147iy(true);
            cPh();
        }
    }

    @C0064Am(aul = "d3d01a18a3ec3ef8a9ca4cf0d3e22101", aum = 0)
    @ClientOnly
    private void cPk() {
        ald().bhh().aRS();
        cPf().mo22164F();
        cPf().mo22166J();
    }

    @C0064Am(aul = "4c6872e5e2298e71b344de5711592fa7", aum = 0)
    @ClientOnly
    private void cPm() {
        if (this.bHv) {
            ald().bhh().aRT();
        } else {
            ald().bhh().aRR();
        }
        ald().getEventManager().mo13975h(new C1274Sq(C1274Sq.C1275a.EXIT_GATE));
        cuN().cPf().mo22165H();
        cuN().cPf().mo22167L();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "b20c2284486caa227ca7549ec0dd113b", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    /* renamed from: ak */
    private void m39242ak(Ship fAVar) {
        try {
            ala().aJA().mo11896c(mo22135dL(), fAVar);
        } catch (C3979xg e) {
            e.printStackTrace();
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "d9ae51c83f5f92181e459fd6a60151da", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cPo() {
        if (mo22135dL().bQB() && ala().aJA().mo11897dj(mo22135dL())) {
            mo22135dL().mo14419f((C1506WA) new C0186CL(mo22135dL(), ala().aJA().mo11898dl(mo22135dL())));
        }
    }

    @C0064Am(aul = "ea9d85d3398de2cad94206f23436956b", aum = 0)
    private long cPs() {
        long awt = ala().aJe().mo19019xv().awt();
        InsuranceImprovement bqB = m39274dG().cXk().bqB();
        if (bqB == null || bqB.bPR() <= awt) {
            return awt;
        }
        return bqB.bPR();
    }

    @C0064Am(aul = "d9f9cf41dc2b1d3035cb71b50a9f55d7", aum = 0)
    private long cPu() {
        Ship bQx = m39274dG().bQx();
        if (bQx == null) {
            return 0;
        }
        long HG = bQx.agr().bAP().mo19868HG();
        long j = HG;
        for (Item next : bQx.aiB()) {
            if (!next.cxn()) {
                j = (((long) next.mo302Iq()) * next.bAP().mo19868HG()) + j;
            }
        }
        return j;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "f42dc548bc4139ab1b4535f3f9501828", aum = 0)
    @C2499fr(mo18855qf = {"9df22fec0eef587e59c9c7882829432d"})
    private void cPw() {
        if (m39274dG().mo12659ly() != null) {
            m39274dG().mo12659ly().mo20723Rr();
        }
    }

    @C0064Am(aul = "32cfa77d669b61333cb1d5220b5dd1a7", aum = 0)
    /* renamed from: b */
    private void m39248b(aDJ adj) {
        if (cMU() == adj) {
            mo22076bT((Actor) null);
        }
    }

    /* renamed from: a.t$j */
    /* compiled from: a */
    public enum C3627j {
        DOCK,
        JUMP
    }

    /* renamed from: a.t$i */
    /* compiled from: a */
    class C3626i implements Comparator<C1540Wd<C0286Dh, Float>> {
        C3626i() {
        }

        /* renamed from: a */
        public int compare(C1540Wd<C0286Dh, Float> wd, C1540Wd<C0286Dh, Float> wd2) {
            return (int) (((Float) wd.dpH).floatValue() - ((Float) wd2.dpH).floatValue());
        }
    }

    /* renamed from: a.t$h */
    /* compiled from: a */
    class C3625h implements C5964ado {
        C3625h() {
        }

        /* renamed from: Xy */
        public void mo12895Xy() {
            PlayerController.this.ald().getLoaderTrail().mo5001b((C5964ado) this);
            NLSWindowAlert ags = (NLSWindowAlert) PlayerController.this.ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT);
            Node azW = PlayerController.this.cuN().aYa().azW();
            StellarSystem Nc = PlayerController.this.cuN().aYa().mo960Nc();
            if (!(Nc == null || azW == null || ags == null || PlayerController.this.cuN().mo22135dL() == null)) {
                PlayerController.this.cuN().mo22135dL().mo14419f((C1506WA) new C2733jJ(ags.dbA().get(), true, true, azW.mo21665ke(), Nc.mo3250ke()));
            }
            PlayerController.this.cuN().cNb();
        }
    }

    /* renamed from: a.t$d */
    /* compiled from: a */
    class C3621d implements C0907NJ {
        C3621d() {
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            if (renderAsset instanceof SceneObject) {
                PlayerController.this.hnt = (SceneObject) renderAsset;
                if (PlayerController.this.hnt != null) {
                    PlayerController.this.hnt.setAllowSelection(false);
                    PlayerController.logger.debug("Loaded Scene " + PlayerController.this.hnt.getName());
                    PlayerController.this.ald().getEventManager().mo13975h(new aQA());
                    return;
                }
                PlayerController.logger.error("Missing content: " + PlayerController.this.cuN().aYa().azW().mo21662ip());
                throw new IllegalStateException("Missing content: " + PlayerController.this.cuN().aYa().azW().mo21662ip());
            }
        }
    }

    /* renamed from: a.t$e */
    /* compiled from: a */
    class C3622e implements C1459VT {
        C3622e() {
        }

        /* renamed from: a */
        public void mo6145a(int i, Object... objArr) {
            Integer num = objArr[0];
            if (num.intValue() >= 1000) {
                num = Integer.valueOf(num.intValue() / 1000);
            }
            objArr[0] = Integer.valueOf(num.intValue() - (i / 1000));
        }

        /* renamed from: b */
        public boolean mo6146b(Object... objArr) {
            return objArr[0].intValue() < 1;
        }
    }

    /* renamed from: a.t$c */
    /* compiled from: a */
    class C3620c implements C0907NJ {
        private final /* synthetic */ Pawn aVE;

        C3620c(Pawn avi) {
            this.aVE = avi;
        }

        /* renamed from: a */
        public void mo968a(RenderAsset renderAsset) {
            PlayerController.this.hnp = (RSpaceDust) renderAsset;
            PlayerController.this.m39326o(this.aVE);
        }
    }

    /* renamed from: a.t$b */
    /* compiled from: a */
    class C3618b implements C3002mq.C3003a {
        C3618b() {
        }

        public void execute() {
            PlayerController.this.ald().mo4091e(new C3619a());
        }

        /* renamed from: a.t$b$a */
        class C3619a implements Runnable {
            C3619a() {
            }

            public void run() {
                PlayerController.this.cOy();
            }
        }
    }

    /* renamed from: a.t$f */
    /* compiled from: a */
    class C3623f extends C6124ags<C2640ht> {
        C3623f() {
        }

        /* renamed from: a */
        public boolean updateInfo(C2640ht htVar) {
            PlayerController.this.ald().getEventManager().mo13975h(new C3002mq(1));
            PlayerController.this.ald().getEventManager().mo13968b(C2640ht.class, this);
            return false;
        }
    }

    /* renamed from: a.t$g */
    /* compiled from: a */
    class C3624g extends C6124ags<C3002mq> {
        C3624g() {
        }

        /* renamed from: a */
        public boolean updateInfo(C3002mq mqVar) {
            if (mqVar.aiH() != 1) {
                return false;
            }
            PlayerController.this.mo22135dL().dyC();
            PlayerController.this.ald().getEventManager().mo13968b(C3002mq.class, this);
            return false;
        }
    }

    @C6485anp
    @ClientOnly
    @C5511aMd
    /* renamed from: a.t$a */
    public class RulesOverride extends TaikodomObject implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9183aT = null;
        /* renamed from: aU */
        public static final C2491fm f9184aU = null;
        /* renamed from: aV */
        public static final C2491fm f9185aV = null;
        /* renamed from: aW */
        public static final C2491fm f9186aW = null;
        /* renamed from: aX */
        public static final C2491fm f9187aX = null;
        /* renamed from: aY */
        public static final C2491fm f9188aY = null;
        /* renamed from: aZ */
        public static final C2491fm f9189aZ = null;
        /* renamed from: ba */
        public static final C2491fm f9190ba = null;
        /* renamed from: bb */
        public static final C2491fm f9191bb = null;
        /* renamed from: bc */
        public static final C2491fm f9192bc = null;
        /* renamed from: bd */
        public static final C2491fm f9193bd = null;
        /* renamed from: be */
        public static final C2491fm f9194be = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "d7f5d3ba2a84790cf04866724aa00fd9", aum = 0)

        /* renamed from: aS */
        static /* synthetic */ PlayerController f9182aS;

        static {
            m39408V();
        }

        @ClientOnly

        /* renamed from: aQ */
        private AtomicInteger f9195aQ;
        @ClientOnly

        /* renamed from: aR */
        private AtomicInteger f9196aR;

        public RulesOverride() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public RulesOverride(C5540aNg ang) {
            super(ang);
        }

        public RulesOverride(PlayerController tVar) {
            super((C5540aNg) null);
            super._m_script_init(tVar);
        }

        /* renamed from: V */
        static void m39408V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaikodomObject._m_fieldCount + 1;
            _m_methodCount = TaikodomObject._m_methodCount + 11;
            int i = TaikodomObject._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(RulesOverride.class, "d7f5d3ba2a84790cf04866724aa00fd9", i);
            f9183aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
            int i3 = TaikodomObject._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 11)];
            C2491fm a = C4105zY.m41624a(RulesOverride.class, "f40cdd3560b0f89991547c59836360a4", i3);
            f9184aU = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(RulesOverride.class, "7dbf8a9e938ad71d309620682fe29c35", i4);
            f9185aV = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(RulesOverride.class, "d1110d13274790c50412747e43b5d8af", i5);
            f9186aW = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(RulesOverride.class, "e0bc12d934cddcb08424875162b29c7d", i6);
            f9187aX = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(RulesOverride.class, "a7d83c51a0fc1c2c51a4301957a3b7d7", i7);
            f9188aY = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(RulesOverride.class, "48a0e42da7f30f9e9ddfe4a26f9821cd", i8);
            f9189aZ = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(RulesOverride.class, "fef2771510420a0c335ead5b37daeec1", i9);
            f9190ba = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            C2491fm a8 = C4105zY.m41624a(RulesOverride.class, "2f268e44a676c30e9a90ce20b2f85a30", i10);
            f9191bb = a8;
            fmVarArr[i10] = a8;
            int i11 = i10 + 1;
            C2491fm a9 = C4105zY.m41624a(RulesOverride.class, "24dea6eef1035c7f3a5a66be5dd8c3a0", i11);
            f9192bc = a9;
            fmVarArr[i11] = a9;
            int i12 = i11 + 1;
            C2491fm a10 = C4105zY.m41624a(RulesOverride.class, "5f1a3e6f5f824713b54b5d21b5e8ae83", i12);
            f9193bd = a10;
            fmVarArr[i12] = a10;
            int i13 = i12 + 1;
            C2491fm a11 = C4105zY.m41624a(RulesOverride.class, "6acbd42b928fbec14e3f56f97a97b0d3", i13);
            f9194be = a11;
            fmVarArr[i13] = a11;
            int i14 = i13 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(RulesOverride.class, C0942Np.class, _m_fields, _m_methods);
        }

        /* renamed from: A */
        private PlayerController m39398A() {
            return (PlayerController) bFf().mo5608dq().mo3214p(f9183aT);
        }

        /* renamed from: a */
        private void m39409a(PlayerController tVar) {
            bFf().mo5608dq().mo3197f(f9183aT, tVar);
        }

        @ClientOnly
        /* renamed from: D */
        public boolean mo22163D() {
            switch (bFf().mo6893i(f9185aV)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f9185aV, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9185aV, new Object[0]));
                    break;
            }
            return m39400C();
        }

        @ClientOnly
        /* renamed from: F */
        public void mo22164F() {
            switch (bFf().mo6893i(f9186aW)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9186aW, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9186aW, new Object[0]));
                    break;
            }
            m39401E();
        }

        @ClientOnly
        /* renamed from: H */
        public void mo22165H() {
            switch (bFf().mo6893i(f9187aX)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9187aX, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9187aX, new Object[0]));
                    break;
            }
            m39402G();
        }

        @ClientOnly
        /* renamed from: J */
        public void mo22166J() {
            switch (bFf().mo6893i(f9188aY)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9188aY, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9188aY, new Object[0]));
                    break;
            }
            m39403I();
        }

        @ClientOnly
        /* renamed from: L */
        public void mo22167L() {
            switch (bFf().mo6893i(f9189aZ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9189aZ, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9189aZ, new Object[0]));
                    break;
            }
            m39404K();
        }

        @ClientOnly
        /* renamed from: N */
        public boolean mo22168N() {
            switch (bFf().mo6893i(f9190ba)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f9190ba, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9190ba, new Object[0]));
                    break;
            }
            return m39405M();
        }

        @ClientOnly
        /* renamed from: P */
        public boolean mo22169P() {
            switch (bFf().mo6893i(f9191bb)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f9191bb, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9191bb, new Object[0]));
                    break;
            }
            return m39406O();
        }

        @ClientOnly
        /* renamed from: R */
        public boolean mo22170R() {
            switch (bFf().mo6893i(f9192bc)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f9192bc, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9192bc, new Object[0]));
                    break;
            }
            return m39407Q();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0942Np(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
                case 0:
                    m39399B();
                    return null;
                case 1:
                    return new Boolean(m39400C());
                case 2:
                    m39401E();
                    return null;
                case 3:
                    m39402G();
                    return null;
                case 4:
                    m39403I();
                    return null;
                case 5:
                    m39404K();
                    return null;
                case 6:
                    return new Boolean(m39405M());
                case 7:
                    return new Boolean(m39406O());
                case 8:
                    return new Boolean(m39407Q());
                case 9:
                    m39410a((AtomicInteger) args[0]);
                    return null;
                case 10:
                    m39411c((AtomicInteger) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        @ClientOnly
        /* renamed from: b */
        public void mo22172b(AtomicInteger atomicInteger) {
            switch (bFf().mo6893i(f9193bd)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9193bd, new Object[]{atomicInteger}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9193bd, new Object[]{atomicInteger}));
                    break;
            }
            m39410a(atomicInteger);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        @ClientOnly
        public void create() {
            switch (bFf().mo6893i(f9184aU)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9184aU, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9184aU, new Object[0]));
                    break;
            }
            m39399B();
        }

        @ClientOnly
        /* renamed from: d */
        public void mo22174d(AtomicInteger atomicInteger) {
            switch (bFf().mo6893i(f9194be)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9194be, new Object[]{atomicInteger}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9194be, new Object[]{atomicInteger}));
                    break;
            }
            m39411c(atomicInteger);
        }

        /* renamed from: b */
        public void mo22171b(PlayerController tVar) {
            m39409a(tVar);
            super.mo10S();
        }

        @C0064Am(aul = "f40cdd3560b0f89991547c59836360a4", aum = 0)
        @ClientOnly
        /* renamed from: B */
        private void m39399B() {
            this.f9195aQ = new AtomicInteger();
            this.f9196aR = new AtomicInteger();
        }

        @C0064Am(aul = "7dbf8a9e938ad71d309620682fe29c35", aum = 0)
        @ClientOnly
        /* renamed from: C */
        private boolean m39400C() {
            return m39398A().m39274dG().bQx().ahb();
        }

        @C0064Am(aul = "d1110d13274790c50412747e43b5d8af", aum = 0)
        @ClientOnly
        /* renamed from: E */
        private void m39401E() {
            mo22172b(this.f9195aQ);
        }

        @C0064Am(aul = "e0bc12d934cddcb08424875162b29c7d", aum = 0)
        @ClientOnly
        /* renamed from: G */
        private void m39402G() {
            mo22174d(this.f9195aQ);
        }

        @C0064Am(aul = "a7d83c51a0fc1c2c51a4301957a3b7d7", aum = 0)
        @ClientOnly
        /* renamed from: I */
        private void m39403I() {
            mo22172b(this.f9196aR);
        }

        @C0064Am(aul = "48a0e42da7f30f9e9ddfe4a26f9821cd", aum = 0)
        @ClientOnly
        /* renamed from: K */
        private void m39404K() {
            mo22174d(this.f9196aR);
        }

        @C0064Am(aul = "fef2771510420a0c335ead5b37daeec1", aum = 0)
        @ClientOnly
        /* renamed from: M */
        private boolean m39405M() {
            return m39398A().m39274dG().bQB() || m39398A().m39274dG().bQx() == null || m39398A().m39274dG().bQx().isDead() || !m39398A().m39274dG().bQx().cLZ();
        }

        @C0064Am(aul = "2f268e44a676c30e9a90ce20b2f85a30", aum = 0)
        @ClientOnly
        /* renamed from: O */
        private boolean m39406O() {
            return this.f9195aQ.get() > 0 || mo22168N() || !m39398A().cNT();
        }

        @C0064Am(aul = "24dea6eef1035c7f3a5a66be5dd8c3a0", aum = 0)
        @ClientOnly
        /* renamed from: Q */
        private boolean m39407Q() {
            return this.f9196aR.get() > 0 || mo22168N();
        }

        @C0064Am(aul = "5f1a3e6f5f824713b54b5d21b5e8ae83", aum = 0)
        @ClientOnly
        /* renamed from: a */
        private void m39410a(AtomicInteger atomicInteger) {
            atomicInteger.set(atomicInteger.get() + 1);
        }

        @C0064Am(aul = "6acbd42b928fbec14e3f56f97a97b0d3", aum = 0)
        @ClientOnly
        /* renamed from: c */
        private void m39411c(AtomicInteger atomicInteger) {
            int i = atomicInteger.get() - 1;
            if (i >= 0) {
                atomicInteger.set(i);
            }
        }
    }
}
