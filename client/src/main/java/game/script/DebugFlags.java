package game.script;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2414fB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.th */
/* compiled from: a */
public class DebugFlags extends aDJ implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bkA = null;
    public static final C5663aRz bkB = null;
    public static final C5663aRz bkC = null;
    public static final C5663aRz bkD = null;
    public static final C5663aRz bkE = null;
    public static final C2491fm bkF = null;
    public static final C2491fm bkG = null;
    public static final C2491fm bkH = null;
    public static final C2491fm bkI = null;
    public static final C2491fm bkJ = null;
    public static final C2491fm bkK = null;
    public static final C2491fm bkL = null;
    public static final C5663aRz bkz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fe7665dee2e3d606fd1472d549c44d59", aum = 0)

    /* renamed from: Ls */
    private static boolean f9264Ls;
    @C0064Am(aul = "25a64144b4526610a94d539a86f44464", aum = 1)

    /* renamed from: Lt */
    private static boolean f9265Lt;
    @C0064Am(aul = "0c26f68e239831e0e6a8f14395edec23", aum = 2)

    /* renamed from: Lu */
    private static boolean f9266Lu;
    @C0064Am(aul = "b4d5613fcb5084a18f35ddae6b5c48c4", aum = 3)

    /* renamed from: Lv */
    private static boolean f9267Lv;
    @C0064Am(aul = "56be6876a22a2120219348d13ad207ff", aum = 4)

    /* renamed from: Lw */
    private static boolean f9268Lw;
    @C0064Am(aul = "eaa2a052767726f453eca09dc19b507b", aum = 5)

    /* renamed from: Lx */
    private static boolean f9269Lx;

    static {
        m39708V();
    }

    public DebugFlags() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public DebugFlags(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39708V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 6;
        _m_methodCount = aDJ._m_methodCount + 8;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(DebugFlags.class, "fe7665dee2e3d606fd1472d549c44d59", i);
        bkz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(DebugFlags.class, "25a64144b4526610a94d539a86f44464", i2);
        bkA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(DebugFlags.class, "0c26f68e239831e0e6a8f14395edec23", i3);
        bkB = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(DebugFlags.class, "b4d5613fcb5084a18f35ddae6b5c48c4", i4);
        bkC = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(DebugFlags.class, "56be6876a22a2120219348d13ad207ff", i5);
        bkD = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(DebugFlags.class, "eaa2a052767726f453eca09dc19b507b", i6);
        bkE = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i8 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 8)];
        C2491fm a = C4105zY.m41624a(DebugFlags.class, "ff9518c8c4cf23a03f0e81e4bce629d5", i8);
        _f_onResurrect_0020_0028_0029V = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(DebugFlags.class, "c6de6f7118c699bf12afa8a03d586d1c", i9);
        bkF = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(DebugFlags.class, "52065348bb597582fa16aabf697f1c06", i10);
        bkG = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(DebugFlags.class, "858e92da019c85068431bfb660dfca1b", i11);
        bkH = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(DebugFlags.class, "03d453d6108826b1d360a075f390d64c", i12);
        bkI = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(DebugFlags.class, "62c3242e6bf77353acc156b895b48c67", i13);
        bkJ = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(DebugFlags.class, "99179cdf48d861fadcfac9ed15085747", i14);
        bkK = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(DebugFlags.class, "bb96ad63d5e53aa289f6a9f69ce69e7f", i15);
        bkL = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(DebugFlags.class, C2414fB.class, _m_fields, _m_methods);
    }

    private boolean ade() {
        return bFf().mo5608dq().mo3201h(bkz);
    }

    private boolean adf() {
        return bFf().mo5608dq().mo3201h(bkA);
    }

    private boolean adg() {
        return bFf().mo5608dq().mo3201h(bkB);
    }

    private boolean adh() {
        return bFf().mo5608dq().mo3201h(bkC);
    }

    private boolean adi() {
        return bFf().mo5608dq().mo3201h(bkD);
    }

    private boolean adj() {
        return bFf().mo5608dq().mo3201h(bkE);
    }

    @C0064Am(aul = "c6de6f7118c699bf12afa8a03d586d1c", aum = 0)
    @C5566aOg
    private void adk() {
        throw new aWi(new aCE(this, bkF, new Object[0]));
    }

    /* renamed from: bk */
    private void m39710bk(boolean z) {
        bFf().mo5608dq().mo3153a(bkz, z);
    }

    /* renamed from: bl */
    private void m39711bl(boolean z) {
        bFf().mo5608dq().mo3153a(bkA, z);
    }

    /* renamed from: bm */
    private void m39712bm(boolean z) {
        bFf().mo5608dq().mo3153a(bkB, z);
    }

    /* renamed from: bn */
    private void m39713bn(boolean z) {
        bFf().mo5608dq().mo3153a(bkC, z);
    }

    /* renamed from: bo */
    private void m39714bo(boolean z) {
        bFf().mo5608dq().mo3153a(bkD, z);
    }

    /* renamed from: bp */
    private void m39715bp(boolean z) {
        bFf().mo5608dq().mo3153a(bkE, z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2414fB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m39709aG();
                return null;
            case 1:
                adk();
                return null;
            case 2:
                return new Boolean(adl());
            case 3:
                return new Boolean(adn());
            case 4:
                return new Boolean(adp());
            case 5:
                return new Boolean(adr());
            case 6:
                return new Boolean(adt());
            case 7:
                return new Boolean(adv());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m39709aG();
    }

    public boolean adm() {
        switch (bFf().mo6893i(bkG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bkG, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bkG, new Object[0]));
                break;
        }
        return adl();
    }

    public boolean ado() {
        switch (bFf().mo6893i(bkH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bkH, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bkH, new Object[0]));
                break;
        }
        return adn();
    }

    public boolean adq() {
        switch (bFf().mo6893i(bkI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bkI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bkI, new Object[0]));
                break;
        }
        return adp();
    }

    public boolean ads() {
        switch (bFf().mo6893i(bkJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bkJ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bkJ, new Object[0]));
                break;
        }
        return adr();
    }

    public boolean adu() {
        switch (bFf().mo6893i(bkK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bkK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bkK, new Object[0]));
                break;
        }
        return adt();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: iL */
    public boolean mo22273iL() {
        switch (bFf().mo6893i(bkL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bkL, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bkL, new Object[0]));
                break;
        }
        return adv();
    }

    @C5566aOg
    public void reset() {
        switch (bFf().mo6893i(bkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                break;
        }
        adk();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ff9518c8c4cf23a03f0e81e4bce629d5", aum = 0)
    /* renamed from: aG */
    private void m39709aG() {
        reset();
        super.mo70aH();
    }

    @C0064Am(aul = "52065348bb597582fa16aabf697f1c06", aum = 0)
    private boolean adl() {
        return ade();
    }

    @C0064Am(aul = "858e92da019c85068431bfb660dfca1b", aum = 0)
    private boolean adn() {
        return adf();
    }

    @C0064Am(aul = "03d453d6108826b1d360a075f390d64c", aum = 0)
    private boolean adp() {
        return adg();
    }

    @C0064Am(aul = "62c3242e6bf77353acc156b895b48c67", aum = 0)
    private boolean adr() {
        return adh();
    }

    @C0064Am(aul = "99179cdf48d861fadcfac9ed15085747", aum = 0)
    private boolean adt() {
        return adi();
    }

    @C0064Am(aul = "bb96ad63d5e53aa289f6a9f69ce69e7f", aum = 0)
    private boolean adv() {
        return adj();
    }
}
