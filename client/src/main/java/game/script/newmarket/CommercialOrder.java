package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.TaikodomObject;
import game.script.TaskletImpl;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C2767ji;
import logic.data.mbean.C6191aiH;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aJ */
/* compiled from: a */
public abstract class CommercialOrder extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: kO */
    public static final C5663aRz f3115kO = null;
    /* renamed from: kQ */
    public static final C5663aRz f3117kQ = null;
    /* renamed from: kS */
    public static final C5663aRz f3119kS = null;
    /* renamed from: kU */
    public static final C5663aRz f3121kU = null;
    /* renamed from: kW */
    public static final C5663aRz f3123kW = null;
    /* renamed from: kX */
    public static final C5663aRz f3124kX = null;
    /* renamed from: kZ */
    public static final C5663aRz f3126kZ = null;
    /* renamed from: lA */
    public static final C2491fm f3127lA = null;
    /* renamed from: lB */
    public static final C2491fm f3128lB = null;
    /* renamed from: lC */
    public static final C2491fm f3129lC = null;
    /* renamed from: lD */
    public static final C2491fm f3130lD = null;
    /* renamed from: lE */
    public static final C2491fm f3131lE = null;
    /* renamed from: lF */
    public static final C2491fm f3132lF = null;
    /* renamed from: lG */
    public static final C2491fm f3133lG = null;
    /* renamed from: la */
    public static final C5663aRz f3134la = null;
    /* renamed from: lb */
    public static final C5663aRz f3135lb = null;
    /* renamed from: ld */
    public static final C5663aRz f3137ld = null;
    /* renamed from: lf */
    public static final C5663aRz f3139lf = null;
    /* renamed from: lh */
    public static final C5663aRz f3141lh = null;
    /* renamed from: li */
    public static final C2491fm f3142li = null;
    /* renamed from: lj */
    public static final C2491fm f3143lj = null;
    /* renamed from: lk */
    public static final C2491fm f3144lk = null;
    /* renamed from: ll */
    public static final C2491fm f3145ll = null;
    /* renamed from: lm */
    public static final C2491fm f3146lm = null;
    /* renamed from: ln */
    public static final C2491fm f3147ln = null;
    /* renamed from: lo */
    public static final C2491fm f3148lo = null;
    /* renamed from: lp */
    public static final C2491fm f3149lp = null;
    /* renamed from: lq */
    public static final C2491fm f3150lq = null;
    /* renamed from: lr */
    public static final C2491fm f3151lr = null;
    /* renamed from: ls */
    public static final C2491fm f3152ls = null;
    /* renamed from: lt */
    public static final C2491fm f3153lt = null;
    /* renamed from: lu */
    public static final C2491fm f3154lu = null;
    /* renamed from: lv */
    public static final C2491fm f3155lv = null;
    /* renamed from: lw */
    public static final C2491fm f3156lw = null;
    /* renamed from: lx */
    public static final C2491fm f3157lx = null;
    /* renamed from: ly */
    public static final C2491fm f3158ly = null;
    /* renamed from: lz */
    public static final C2491fm f3159lz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c73e12770407d7f24f341f03c9460186", aum = 7)

    /* renamed from: Q */
    private static long f3112Q;
    @C0064Am(aul = "f98d8224f7fe0be1289d96bf35b5df7f", aum = 0)

    /* renamed from: cY */
    private static ItemType f3113cY;
    @C0064Am(aul = "2c2124d2e4622d17ab06b75c34e0d0a5", aum = 8)
    private static boolean dirty;
    @C0064Am(aul = "d2075e797e32a0cc431168f4f99fe177", aum = 5)

    /* renamed from: iH */
    private static Station f3114iH;
    @C0064Am(aul = "dd8d1f49ac815c817f41c0b893765fa1", aum = 1)

    /* renamed from: kP */
    private static int f3116kP;
    @C0064Am(aul = "c29a034e3d5a425bc0cb92bca6d6f8db", aum = 2)

    /* renamed from: kR */
    private static int f3118kR;
    @C0064Am(aul = "1c6cb85faf5226eb295d6457804bb0af", aum = 3)

    /* renamed from: kT */
    private static int f3120kT;
    @C0064Am(aul = "75ab7dce6ebda52194babae7df7b9c31", aum = 4)

    /* renamed from: kV */
    private static long f3122kV;
    @C0064Am(aul = "24d2a1c08a7c00e5733ffd2a88b416d6", aum = 6)

    /* renamed from: kY */
    private static long f3125kY;
    @C0064Am(aul = "f91b8617656aaa9f7294b5aaef90fece", aum = 9)

    /* renamed from: lc */
    private static CommercialOrderInfo f3136lc;
    @C0064Am(aul = "a076b8316350fcce54c06cfeaa9fa39f", aum = 10)

    /* renamed from: le */
    private static C2686iZ<CommercialOrderInfo> f3138le;
    @C0064Am(aul = "0dd1c43162958a07f8579687c7f7f016", aum = 11)

    /* renamed from: lg */
    private static CommercialOrderTicker f3140lg;

    static {
        m15526V();
    }

    public CommercialOrder() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CommercialOrder(C5540aNg ang) {
        super(ang);
    }

    public CommercialOrder(ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
        super((C5540aNg) null);
        super._m_script_init(jCVar, i, i2, j, bf, j2);
    }

    /* renamed from: V */
    static void m15526V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 12;
        _m_methodCount = TaikodomObject._m_methodCount + 26;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(CommercialOrder.class, "f98d8224f7fe0be1289d96bf35b5df7f", i);
        f3115kO = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CommercialOrder.class, "dd8d1f49ac815c817f41c0b893765fa1", i2);
        f3117kQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CommercialOrder.class, "c29a034e3d5a425bc0cb92bca6d6f8db", i3);
        f3119kS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CommercialOrder.class, "1c6cb85faf5226eb295d6457804bb0af", i4);
        f3121kU = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CommercialOrder.class, "75ab7dce6ebda52194babae7df7b9c31", i5);
        f3123kW = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CommercialOrder.class, "d2075e797e32a0cc431168f4f99fe177", i6);
        f3124kX = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CommercialOrder.class, "24d2a1c08a7c00e5733ffd2a88b416d6", i7);
        f3126kZ = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(CommercialOrder.class, "c73e12770407d7f24f341f03c9460186", i8);
        f3134la = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(CommercialOrder.class, "2c2124d2e4622d17ab06b75c34e0d0a5", i9);
        f3135lb = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(CommercialOrder.class, "f91b8617656aaa9f7294b5aaef90fece", i10);
        f3137ld = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(CommercialOrder.class, "a076b8316350fcce54c06cfeaa9fa39f", i11);
        f3139lf = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(CommercialOrder.class, "0dd1c43162958a07f8579687c7f7f016", i12);
        f3141lh = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i14 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 26)];
        C2491fm a = C4105zY.m41624a(CommercialOrder.class, "7bcd013041fe35706308519f4b5f761d", i14);
        f3142li = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(CommercialOrder.class, "a5e4d1607e11ad007ae2eee62fd6fdc8", i15);
        f3143lj = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(CommercialOrder.class, "6f49bec3b59bcd9989811d939527ab6a", i16);
        f3144lk = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(CommercialOrder.class, "f4aa3866cd7b2939c1037e99fef8f8d8", i17);
        f3145ll = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(CommercialOrder.class, "1ce11c91a874e0e7ff921e523ae9ecd3", i18);
        f3146lm = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(CommercialOrder.class, "8ed584edd5b56e35be115204edd969a9", i19);
        f3147ln = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(CommercialOrder.class, "5f8cf454b0c4814942c2fe0015b677ef", i20);
        f3148lo = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(CommercialOrder.class, "e6c3204c2140a0facb81687ba644077c", i21);
        f3149lp = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(CommercialOrder.class, "1b716317f85f37cfc913f55b572f72fb", i22);
        f3150lq = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(CommercialOrder.class, "7341ca859ad4e4bf276d45bad26c2bbf", i23);
        f3151lr = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(CommercialOrder.class, "45ef49c6203ce27752cf2388df6ec9c2", i24);
        f3152ls = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(CommercialOrder.class, "011131bc36386e7769b5950e32f6f4b1", i25);
        f3153lt = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(CommercialOrder.class, "da907a985cce83a2a2bb418e52b91316", i26);
        f3154lu = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(CommercialOrder.class, "4bdf882abd8f157e16c790c26987df04", i27);
        f3155lv = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(CommercialOrder.class, "5fed15878c1699a4960eced514d4a7ca", i28);
        _f_dispose_0020_0028_0029V = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(CommercialOrder.class, "4ba88358fa84638fb1bc6f68e73cba4b", i29);
        f3156lw = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(CommercialOrder.class, "6e23d8ea538410e2896807bafd1750d0", i30);
        f3157lx = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(CommercialOrder.class, "dc1958ad83627ade29e018e2f4f5a247", i31);
        f3158ly = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(CommercialOrder.class, "602fae6f5a6d23461153dc160cddcd2b", i32);
        f3159lz = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(CommercialOrder.class, "b52132c051a371e72b45131811a2a5ec", i33);
        f3127lA = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(CommercialOrder.class, "0ee6b63453f18683543945c55e01ffd1", i34);
        f3128lB = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(CommercialOrder.class, "8807319eace829b47660642afe861b0d", i35);
        f3129lC = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(CommercialOrder.class, "b789dc7c147cc92cb5bccc76c0222739", i36);
        f3130lD = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        C2491fm a24 = C4105zY.m41624a(CommercialOrder.class, "2a6e58fb302e2e6be1e77146d072a8c0", i37);
        f3131lE = a24;
        fmVarArr[i37] = a24;
        int i38 = i37 + 1;
        C2491fm a25 = C4105zY.m41624a(CommercialOrder.class, "4d47d23ff315332f540f0c9537baf2bd", i38);
        f3132lF = a25;
        fmVarArr[i38] = a25;
        int i39 = i38 + 1;
        C2491fm a26 = C4105zY.m41624a(CommercialOrder.class, "fb317398c8ce4e0144c99b441695998d", i39);
        f3133lG = a26;
        fmVarArr[i39] = a26;
        int i40 = i39 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CommercialOrder.class, C2767ji.class, _m_fields, _m_methods);
    }

    /* renamed from: i */
    public static boolean m15566i(Item auq) {
        if (auq.aNP() && !auq.isDisposed()) {
            return true;
        }
        return false;
    }

    /* renamed from: C */
    private void m15520C(int i) {
        bFf().mo5608dq().mo3183b(f3117kQ, i);
    }

    /* renamed from: D */
    private void m15521D(int i) {
        bFf().mo5608dq().mo3183b(f3119kS, i);
    }

    /* renamed from: E */
    private void m15522E(int i) {
        bFf().mo5608dq().mo3183b(f3121kU, i);
    }

    /* renamed from: a */
    private void m15527a(Station bf) {
        bFf().mo5608dq().mo3197f(f3124kX, bf);
    }

    /* renamed from: a */
    private void m15528a(CommercialOrderInfo aeu) {
        bFf().mo5608dq().mo3197f(f3137ld, aeu);
    }

    /* renamed from: a */
    private void m15529a(CommercialOrderTicker aVar) {
        bFf().mo5608dq().mo3197f(f3141lh, aVar);
    }

    /* renamed from: eA */
    private ItemType m15532eA() {
        return (ItemType) bFf().mo5608dq().mo3214p(f3115kO);
    }

    /* renamed from: eB */
    private int m15533eB() {
        return bFf().mo5608dq().mo3212n(f3117kQ);
    }

    /* renamed from: eC */
    private int m15534eC() {
        return bFf().mo5608dq().mo3212n(f3119kS);
    }

    /* renamed from: eD */
    private int m15535eD() {
        return bFf().mo5608dq().mo3212n(f3121kU);
    }

    /* renamed from: eE */
    private long m15536eE() {
        return bFf().mo5608dq().mo3213o(f3123kW);
    }

    /* renamed from: eF */
    private Station m15537eF() {
        return (Station) bFf().mo5608dq().mo3214p(f3124kX);
    }

    /* renamed from: eG */
    private long m15538eG() {
        return bFf().mo5608dq().mo3213o(f3126kZ);
    }

    /* renamed from: eH */
    private long m15539eH() {
        return bFf().mo5608dq().mo3213o(f3134la);
    }

    /* renamed from: eI */
    private boolean m15540eI() {
        return bFf().mo5608dq().mo3201h(f3135lb);
    }

    /* renamed from: eJ */
    private CommercialOrderInfo m15541eJ() {
        return (CommercialOrderInfo) bFf().mo5608dq().mo3214p(f3137ld);
    }

    /* renamed from: eK */
    private C2686iZ m15542eK() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(f3139lf);
    }

    /* renamed from: eL */
    private CommercialOrderTicker m15543eL() {
        return (CommercialOrderTicker) bFf().mo5608dq().mo3214p(f3141lh);
    }

    @C0064Am(aul = "7bcd013041fe35706308519f4b5f761d", aum = 0)
    /* renamed from: eM */
    private void m15544eM() {
        throw new aWi(new aCE(this, f3142li, new Object[0]));
    }

    /* renamed from: f */
    private void m15553f(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f3115kO, jCVar);
    }

    @C0064Am(aul = "6e23d8ea538410e2896807bafd1750d0", aum = 0)
    @C5566aOg
    /* renamed from: fj */
    private void m15559fj() {
        throw new aWi(new aCE(this, f3157lx, new Object[0]));
    }

    @C0064Am(aul = "dc1958ad83627ade29e018e2f4f5a247", aum = 0)
    /* renamed from: fl */
    private void m15560fl() {
        throw new aWi(new aCE(this, f3158ly, new Object[0]));
    }

    @C0064Am(aul = "b789dc7c147cc92cb5bccc76c0222739", aum = 0)
    /* renamed from: fp */
    private CommercialOrderInfo m15562fp() {
        throw new aWi(new aCE(this, f3130lD, new Object[0]));
    }

    /* renamed from: h */
    private void m15565h(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(f3139lf, iZVar);
    }

    /* renamed from: k */
    private void m15567k(long j) {
        bFf().mo5608dq().mo3184b(f3123kW, j);
    }

    /* renamed from: l */
    private void m15568l(long j) {
        bFf().mo5608dq().mo3184b(f3126kZ, j);
    }

    /* renamed from: m */
    private void m15569m(long j) {
        bFf().mo5608dq().mo3184b(f3134la, j);
    }

    /* renamed from: p */
    private void m15570p(boolean z) {
        bFf().mo5608dq().mo3153a(f3135lb, z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: G */
    public void mo9431G(int i) {
        switch (bFf().mo6893i(f3143lj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3143lj, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3143lj, new Object[]{new Integer(i)}));
                break;
        }
        m15523F(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: I */
    public void mo9432I(int i) {
        switch (bFf().mo6893i(f3152ls)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3152ls, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3152ls, new Object[]{new Integer(i)}));
                break;
        }
        m15524H(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: K */
    public void mo9433K(int i) {
        switch (bFf().mo6893i(f3154lu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3154lu, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3154lu, new Object[]{new Integer(i)}));
                break;
        }
        m15525J(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2767ji(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m15544eM();
                return null;
            case 1:
                m15523F(((Integer) args[0]).intValue());
                return null;
            case 2:
                return m15545eO();
            case 3:
                return new Long(m15546eQ());
            case 4:
                return m15547eS();
            case 5:
                return new Long(m15548eU());
            case 6:
                return new Long(m15549eW());
            case 7:
                return new Long(m15550eX());
            case 8:
                return new Integer(m15551eY());
            case 9:
                return new Integer(m15554fa());
            case 10:
                m15524H(((Integer) args[0]).intValue());
                return null;
            case 11:
                return new Integer(m15555fc());
            case 12:
                m15525J(((Integer) args[0]).intValue());
                return null;
            case 13:
                return new Integer(m15556fe());
            case 14:
                m15557fg();
                return null;
            case 15:
                return m15558fh();
            case 16:
                m15559fj();
                return null;
            case 17:
                m15560fl();
                return null;
            case 18:
                return m15561fn();
            case 19:
                m15530b((CommercialOrderInfo) args[0]);
                return null;
            case 20:
                return new Boolean(m15531d((CommercialOrderInfo) args[0]));
            case 21:
                m15552f((CommercialOrderInfo) args[0]);
                return null;
            case 22:
                return m15562fp();
            case 23:
                return new Boolean(m15563fr());
            case 24:
                m15571q(((Boolean) args[0]).booleanValue());
                return null;
            case 25:
                m15564h((CommercialOrderInfo) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public void mo9435c(CommercialOrderInfo aeu) {
        switch (bFf().mo6893i(f3127lA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3127lA, new Object[]{aeu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3127lA, new Object[]{aeu}));
                break;
        }
        m15530b(aeu);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m15557fg();
    }

    /* renamed from: e */
    public boolean mo9436e(CommercialOrderInfo aeu) {
        switch (bFf().mo6893i(f3128lB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f3128lB, new Object[]{aeu}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3128lB, new Object[]{aeu}));
                break;
        }
        return m15531d(aeu);
    }

    /* access modifiers changed from: protected */
    /* renamed from: eN */
    public void mo3667eN() {
        switch (bFf().mo6893i(f3142li)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3142li, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3142li, new Object[0]));
                break;
        }
        m15544eM();
    }

    /* renamed from: eP */
    public ItemType mo9437eP() {
        switch (bFf().mo6893i(f3144lk)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f3144lk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3144lk, new Object[0]));
                break;
        }
        return m15545eO();
    }

    /* renamed from: eR */
    public long mo9438eR() {
        switch (bFf().mo6893i(f3145ll)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f3145ll, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3145ll, new Object[0]));
                break;
        }
        return m15546eQ();
    }

    /* renamed from: eT */
    public Station mo9439eT() {
        switch (bFf().mo6893i(f3146lm)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f3146lm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3146lm, new Object[0]));
                break;
        }
        return m15547eS();
    }

    /* renamed from: eV */
    public long mo9440eV() {
        switch (bFf().mo6893i(f3147ln)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f3147ln, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3147ln, new Object[0]));
                break;
        }
        return m15548eU();
    }

    /* renamed from: eZ */
    public int mo9441eZ() {
        switch (bFf().mo6893i(f3150lq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f3150lq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3150lq, new Object[0]));
                break;
        }
        return m15551eY();
    }

    /* renamed from: fb */
    public int mo9442fb() {
        switch (bFf().mo6893i(f3151lr)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f3151lr, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3151lr, new Object[0]));
                break;
        }
        return m15554fa();
    }

    /* renamed from: fd */
    public int mo9443fd() {
        switch (bFf().mo6893i(f3153lt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f3153lt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3153lt, new Object[0]));
                break;
        }
        return m15555fc();
    }

    /* renamed from: ff */
    public int mo9444ff() {
        switch (bFf().mo6893i(f3155lv)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f3155lv, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3155lv, new Object[0]));
                break;
        }
        return m15556fe();
    }

    /* renamed from: fi */
    public Market mo9445fi() {
        switch (bFf().mo6893i(f3156lw)) {
            case 0:
                return null;
            case 2:
                return (Market) bFf().mo5606d(new aCE(this, f3156lw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3156lw, new Object[0]));
                break;
        }
        return m15558fh();
    }

    @C5566aOg
    /* renamed from: fk */
    public void mo9446fk() {
        switch (bFf().mo6893i(f3157lx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3157lx, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3157lx, new Object[0]));
                break;
        }
        m15559fj();
    }

    /* renamed from: fm */
    public void mo3668fm() {
        switch (bFf().mo6893i(f3158ly)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3158ly, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3158ly, new Object[0]));
                break;
        }
        m15560fl();
    }

    /* renamed from: fo */
    public CommercialOrderInfo mo9447fo() {
        switch (bFf().mo6893i(f3159lz)) {
            case 0:
                return null;
            case 2:
                return (CommercialOrderInfo) bFf().mo5606d(new aCE(this, f3159lz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3159lz, new Object[0]));
                break;
        }
        return m15561fn();
    }

    /* access modifiers changed from: protected */
    /* renamed from: fq */
    public CommercialOrderInfo mo3669fq() {
        switch (bFf().mo6893i(f3130lD)) {
            case 0:
                return null;
            case 2:
                return (CommercialOrderInfo) bFf().mo5606d(new aCE(this, f3130lD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3130lD, new Object[0]));
                break;
        }
        return m15562fp();
    }

    /* renamed from: g */
    public void mo9448g(CommercialOrderInfo aeu) {
        switch (bFf().mo6893i(f3129lC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3129lC, new Object[]{aeu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3129lC, new Object[]{aeu}));
                break;
        }
        m15552f(aeu);
    }

    public long getCreationTime() {
        switch (bFf().mo6893i(f3148lo)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f3148lo, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3148lo, new Object[0]));
                break;
        }
        return m15549eW();
    }

    public long getTimeout() {
        switch (bFf().mo6893i(f3149lp)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f3149lp, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3149lp, new Object[0]));
                break;
        }
        return m15550eX();
    }

    /* renamed from: i */
    public void mo9451i(CommercialOrderInfo aeu) {
        switch (bFf().mo6893i(f3133lG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3133lG, new Object[]{aeu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3133lG, new Object[]{aeu}));
                break;
        }
        m15564h(aeu);
    }

    public boolean isDirty() {
        switch (bFf().mo6893i(f3131lE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f3131lE, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3131lE, new Object[0]));
                break;
        }
        return m15563fr();
    }

    public void setDirty(boolean z) {
        switch (bFf().mo6893i(f3132lF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3132lF, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3132lF, new Object[]{new Boolean(z)}));
                break;
        }
        m15571q(z);
    }

    /* renamed from: a */
    public void mo9434a(ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
        super.mo10S();
        m15553f(jCVar);
        m15520C(i);
        m15521D(i2);
        m15567k(j);
        m15527a(bf);
        m15568l(j2);
        CommercialOrderTicker aVar = (CommercialOrderTicker) bFf().mo6865M(CommercialOrderTicker.class);
        aVar.mo9454a(this, (aDJ) this);
        m15529a(aVar);
        m15543eL().mo4702d((float) (j2 / 1000), 1);
        m15569m(cVr());
    }

    @C0064Am(aul = "a5e4d1607e11ad007ae2eee62fd6fdc8", aum = 0)
    /* renamed from: F */
    private void m15523F(int i) {
        mo9433K(m15535eD() + i);
        if (m15533eB() == m15535eD()) {
            mo9446fk();
        }
        if (m15533eB() - m15535eD() < m15534eC()) {
            m15521D(m15533eB() - m15535eD());
        }
    }

    @C0064Am(aul = "6f49bec3b59bcd9989811d939527ab6a", aum = 0)
    /* renamed from: eO */
    private ItemType m15545eO() {
        return m15532eA();
    }

    @C0064Am(aul = "f4aa3866cd7b2939c1037e99fef8f8d8", aum = 0)
    /* renamed from: eQ */
    private long m15546eQ() {
        return m15536eE();
    }

    @C0064Am(aul = "1ce11c91a874e0e7ff921e523ae9ecd3", aum = 0)
    /* renamed from: eS */
    private Station m15547eS() {
        return m15537eF();
    }

    @C0064Am(aul = "8ed584edd5b56e35be115204edd969a9", aum = 0)
    /* renamed from: eU */
    private long m15548eU() {
        return m15538eG();
    }

    @C0064Am(aul = "5f8cf454b0c4814942c2fe0015b677ef", aum = 0)
    /* renamed from: eW */
    private long m15549eW() {
        return m15539eH();
    }

    @C0064Am(aul = "e6c3204c2140a0facb81687ba644077c", aum = 0)
    /* renamed from: eX */
    private long m15550eX() {
        return (m15539eH() + m15538eG()) - cVr();
    }

    @C0064Am(aul = "1b716317f85f37cfc913f55b572f72fb", aum = 0)
    /* renamed from: eY */
    private int m15551eY() {
        return m15533eB();
    }

    @C0064Am(aul = "7341ca859ad4e4bf276d45bad26c2bbf", aum = 0)
    /* renamed from: fa */
    private int m15554fa() {
        return m15534eC();
    }

    @C0064Am(aul = "45ef49c6203ce27752cf2388df6ec9c2", aum = 0)
    /* renamed from: H */
    private void m15524H(int i) {
        m15521D(i);
    }

    @C0064Am(aul = "011131bc36386e7769b5950e32f6f4b1", aum = 0)
    /* renamed from: fc */
    private int m15555fc() {
        return m15535eD();
    }

    @C0064Am(aul = "da907a985cce83a2a2bb418e52b91316", aum = 0)
    /* renamed from: J */
    private void m15525J(int i) {
        m15522E(i);
    }

    @C0064Am(aul = "4bdf882abd8f157e16c790c26987df04", aum = 0)
    /* renamed from: fe */
    private int m15556fe() {
        return mo9441eZ() - mo9443fd();
    }

    @C0064Am(aul = "5fed15878c1699a4960eced514d4a7ca", aum = 0)
    /* renamed from: fg */
    private void m15557fg() {
        super.dispose();
        if (m15543eL() != null) {
            m15543eL().stop();
            m15543eL().dispose();
        }
        m15529a((CommercialOrderTicker) null);
        m15527a((Station) null);
        m15553f((ItemType) null);
        m15528a((CommercialOrderInfo) null);
        for (CommercialOrderInfo abk : m15542eK()) {
            abk.abk();
        }
        m15542eK().clear();
    }

    @C0064Am(aul = "4ba88358fa84638fb1bc6f68e73cba4b", aum = 0)
    /* renamed from: fh */
    private Market m15558fh() {
        return ala().mo1508fi();
    }

    @C0064Am(aul = "602fae6f5a6d23461153dc160cddcd2b", aum = 0)
    /* renamed from: fn */
    private CommercialOrderInfo m15561fn() {
        if (m15540eI() || m15541eJ() == null) {
            m15528a(mo3669fq());
            m15570p(false);
            m15542eK().add(m15541eJ());
        }
        return m15541eJ();
    }

    @C0064Am(aul = "b52132c051a371e72b45131811a2a5ec", aum = 0)
    /* renamed from: b */
    private void m15530b(CommercialOrderInfo aeu) {
        m15542eK().add(aeu);
    }

    @C0064Am(aul = "0ee6b63453f18683543945c55e01ffd1", aum = 0)
    /* renamed from: d */
    private boolean m15531d(CommercialOrderInfo aeu) {
        return m15542eK().contains(aeu);
    }

    @C0064Am(aul = "8807319eace829b47660642afe861b0d", aum = 0)
    /* renamed from: f */
    private void m15552f(CommercialOrderInfo aeu) {
        m15542eK().remove(aeu);
    }

    @C0064Am(aul = "2a6e58fb302e2e6be1e77146d072a8c0", aum = 0)
    /* renamed from: fr */
    private boolean m15563fr() {
        return m15540eI();
    }

    @C0064Am(aul = "4d47d23ff315332f540f0c9537baf2bd", aum = 0)
    /* renamed from: q */
    private void m15571q(boolean z) {
        m15570p(z);
    }

    @C0064Am(aul = "fb317398c8ce4e0144c99b441695998d", aum = 0)
    /* renamed from: h */
    private void m15564h(CommercialOrderInfo aeu) {
        m15528a(aeu);
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aJ$a */
    public class CommercialOrderTicker extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f3160JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f3161aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "6f0be86d61863ad6c9b5f9dcaf24081d", aum = 0)
        static /* synthetic */ CommercialOrder fOf;

        static {
            m15601V();
        }

        public CommercialOrderTicker() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CommercialOrderTicker(CommercialOrder aJVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(aJVar, adj);
        }

        public CommercialOrderTicker(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m15601V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CommercialOrderTicker.class, "6f0be86d61863ad6c9b5f9dcaf24081d", i);
            f3161aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(CommercialOrderTicker.class, "f366e45d56b5931f4a1cdc8f135417e2", i3);
            f3160JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CommercialOrderTicker.class, C6191aiH.class, _m_fields, _m_methods);
        }

        private CommercialOrder dhF() {
            return (CommercialOrder) bFf().mo5608dq().mo3214p(f3161aT);
        }

        /* renamed from: e */
        private void m15602e(CommercialOrder aJVar) {
            bFf().mo5608dq().mo3197f(f3161aT, aJVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6191aiH(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m15603pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f3160JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f3160JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f3160JD, new Object[0]));
                    break;
            }
            m15603pi();
        }

        /* renamed from: a */
        public void mo9454a(CommercialOrder aJVar, aDJ adj) {
            m15602e(aJVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "f366e45d56b5931f4a1cdc8f135417e2", aum = 0)
        /* renamed from: pi */
        private void m15603pi() {
            dhF().mo3667eN();
        }
    }
}
