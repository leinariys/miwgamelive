package game.script.newmarket.economy;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.item.ItemType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C7030azr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.aOR */
/* compiled from: a */
public class VirtualWarehouse extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aeR = null;
    /* renamed from: bM */
    public static final C5663aRz f3463bM = null;
    /* renamed from: bP */
    public static final C2491fm f3464bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3465bQ = null;
    public static final C2491fm cof = null;
    public static final C5663aRz iAf = null;
    public static final C2491fm iAg = null;
    public static final C2491fm iAh = null;
    public static final C2491fm iAi = null;
    public static final C2491fm iAj = null;
    public static final C2491fm iAk = null;
    public static final C2491fm iAl = null;
    public static final C2491fm iAm = null;
    public static final C2491fm iAn = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b081c193fd2ae1ca5d5211245bb443fb", aum = 0)
    private static C3438ra<WarehouseAgent> gYP;
    @C0064Am(aul = "6fe1828387aad1429c82b900456afae1", aum = 1)
    private static C1556Wo<ItemType, WarehouseInventoryItem> gYQ;
    @C0064Am(aul = "04362157d8c1581a06a717fadd5e7ab8", aum = 2)
    private static String handle;

    static {
        m16876V();
    }

    public VirtualWarehouse() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public VirtualWarehouse(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16876V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(VirtualWarehouse.class, "b081c193fd2ae1ca5d5211245bb443fb", i);
        aeR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(VirtualWarehouse.class, "6fe1828387aad1429c82b900456afae1", i2);
        iAf = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(VirtualWarehouse.class, "04362157d8c1581a06a717fadd5e7ab8", i3);
        f3463bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 11)];
        C2491fm a = C4105zY.m41624a(VirtualWarehouse.class, "720dc9c9fa6bf4dbad60f4e0a8800bf0", i5);
        f3464bP = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(VirtualWarehouse.class, "d3abe80e3b2f004d227ccedb89482c91", i6);
        f3465bQ = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(VirtualWarehouse.class, "062a5dec500eb333eb5b51b1c0f0ea83", i7);
        iAg = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(VirtualWarehouse.class, "2ad7de4d52b156533ca5d4712587736c", i8);
        iAh = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(VirtualWarehouse.class, "d2ac20f558171ac1e841c051c9045241", i9);
        iAi = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(VirtualWarehouse.class, "df7b109612c881ea008ff97d45958af5", i10);
        iAj = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(VirtualWarehouse.class, "2286a35538f5fad988f6cbaeff6a544b", i11);
        cof = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(VirtualWarehouse.class, "28b2735348f3aabf56e4224b429ae8da", i12);
        iAk = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(VirtualWarehouse.class, "c285b2ec5659171088744e451d54f385", i13);
        iAl = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(VirtualWarehouse.class, "c8ab2bf842dc010d01a4daec75c16f7e", i14);
        iAm = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(VirtualWarehouse.class, "220164bedca329b440641ff3ca8ccad1", i15);
        iAn = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(VirtualWarehouse.class, C7030azr.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m16879a(String str) {
        bFf().mo5608dq().mo3197f(f3463bM, str);
    }

    /* renamed from: an */
    private void m16880an(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iAf, wo);
    }

    /* renamed from: ao */
    private String m16881ao() {
        return (String) bFf().mo5608dq().mo3214p(f3463bM);
    }

    /* renamed from: cy */
    private void m16885cy(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aeR, raVar);
    }

    private C3438ra dnU() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aeR);
    }

    private C1556Wo dnV() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iAf);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C7030azr(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Inventory")
    /* renamed from: X */
    public void mo10597X(ItemType jCVar) {
        switch (bFf().mo6893i(iAg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iAg, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iAg, new Object[]{jCVar}));
                break;
        }
        m16877W(jCVar);
    }

    /* renamed from: Z */
    public WarehouseInventoryItem mo10598Z(ItemType jCVar) {
        switch (bFf().mo6893i(iAm)) {
            case 0:
                return null;
            case 2:
                return (WarehouseInventoryItem) bFf().mo5606d(new aCE(this, iAm, new Object[]{jCVar}));
            case 3:
                bFf().mo5606d(new aCE(this, iAm, new Object[]{jCVar}));
                break;
        }
        return m16878Y(jCVar);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m16882ar();
            case 1:
                m16884b((String) args[0]);
                return null;
            case 2:
                m16877W((ItemType) args[0]);
                return null;
            case 3:
                m16887e((WarehouseInventoryItem) args[0]);
                return null;
            case 4:
                m16888g((WarehouseInventoryItem) args[0]);
                return null;
            case 5:
                return dnW();
            case 6:
                return azy();
            case 7:
                m16883b((WarehouseAgent) args[0]);
                return null;
            case 8:
                m16886d((WarehouseAgent) args[0]);
                return null;
            case 9:
                return m16878Y((ItemType) args[0]);
            case 10:
                m16889i((WarehouseInventoryItem) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Agents")
    public List<WarehouseAgent> azz() {
        switch (bFf().mo6893i(cof)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cof, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cof, new Object[0]));
                break;
        }
        return azy();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Agents")
    /* renamed from: c */
    public void mo10600c(WarehouseAgent akn) {
        switch (bFf().mo6893i(iAk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iAk, new Object[]{akn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iAk, new Object[]{akn}));
                break;
        }
        m16883b(akn);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Inventory")
    public List<WarehouseInventoryItem> dnX() {
        switch (bFf().mo6893i(iAj)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iAj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iAj, new Object[0]));
                break;
        }
        return dnW();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Agents")
    /* renamed from: e */
    public void mo10602e(WarehouseAgent akn) {
        switch (bFf().mo6893i(iAl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iAl, new Object[]{akn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iAl, new Object[]{akn}));
                break;
        }
        m16886d(akn);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Inventory")
    /* renamed from: f */
    public void mo10603f(WarehouseInventoryItem fIVar) {
        switch (bFf().mo6893i(iAh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iAh, new Object[]{fIVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iAh, new Object[]{fIVar}));
                break;
        }
        m16887e(fIVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3464bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3464bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3464bP, new Object[0]));
                break;
        }
        return m16882ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3465bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3465bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3465bQ, new Object[]{str}));
                break;
        }
        m16884b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Inventory")
    /* renamed from: h */
    public void mo10605h(WarehouseInventoryItem fIVar) {
        switch (bFf().mo6893i(iAi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iAi, new Object[]{fIVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iAi, new Object[]{fIVar}));
                break;
        }
        m16888g(fIVar);
    }

    /* renamed from: j */
    public void mo10606j(WarehouseInventoryItem fIVar) {
        switch (bFf().mo6893i(iAn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iAn, new Object[]{fIVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iAn, new Object[]{fIVar}));
                break;
        }
        m16889i(fIVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "handle")
    @C0064Am(aul = "720dc9c9fa6bf4dbad60f4e0a8800bf0", aum = 0)
    /* renamed from: ar */
    private String m16882ar() {
        return m16881ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "handle")
    @C0064Am(aul = "d3abe80e3b2f004d227ccedb89482c91", aum = 0)
    /* renamed from: b */
    private void m16884b(String str) {
        m16879a(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Inventory")
    @C0064Am(aul = "062a5dec500eb333eb5b51b1c0f0ea83", aum = 0)
    /* renamed from: W */
    private void m16877W(ItemType jCVar) {
        if (!dnV().containsKey(jCVar)) {
            C1556Wo dnV = dnV();
            WarehouseInventoryItem fIVar = (WarehouseInventoryItem) bFf().mo6865M(WarehouseInventoryItem.class);
            fIVar.mo18400a(this, jCVar);
            dnV.put(jCVar, fIVar);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Inventory")
    @C0064Am(aul = "2ad7de4d52b156533ca5d4712587736c", aum = 0)
    /* renamed from: e */
    private void m16887e(WarehouseInventoryItem fIVar) {
        if (!dnV().containsKey(fIVar.mo18403az())) {
            C1556Wo dnV = dnV();
            ItemType az = fIVar.mo18403az();
            WarehouseInventoryItem fIVar2 = (WarehouseInventoryItem) bFf().mo6865M(WarehouseInventoryItem.class);
            fIVar2.mo18399a(this, fIVar);
            dnV.put(az, fIVar2);
            return;
        }
        ((WarehouseInventoryItem) dnV().get(fIVar.mo18403az())).mo18397I(fIVar.mo18404rX());
        ((WarehouseInventoryItem) dnV().get(fIVar.mo18403az())).mo18398K(fIVar.mo18405rZ());
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Inventory")
    @C0064Am(aul = "d2ac20f558171ac1e841c051c9045241", aum = 0)
    /* renamed from: g */
    private void m16888g(WarehouseInventoryItem fIVar) {
        if (dnV().containsKey(fIVar.mo18403az())) {
            dnV().remove(fIVar.mo18403az());
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Inventory")
    @C0064Am(aul = "df7b109612c881ea008ff97d45958af5", aum = 0)
    private List<WarehouseInventoryItem> dnW() {
        return Collections.unmodifiableList(new ArrayList(dnV().values()));
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Agents")
    @C0064Am(aul = "2286a35538f5fad988f6cbaeff6a544b", aum = 0)
    private List<WarehouseAgent> azy() {
        return Collections.unmodifiableList(dnU());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Agents")
    @C0064Am(aul = "28b2735348f3aabf56e4224b429ae8da", aum = 0)
    /* renamed from: b */
    private void m16883b(WarehouseAgent akn) {
        dnU().add(akn);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Agents")
    @C0064Am(aul = "c285b2ec5659171088744e451d54f385", aum = 0)
    /* renamed from: d */
    private void m16886d(WarehouseAgent akn) {
        dnU().remove(akn);
    }

    @C0064Am(aul = "c8ab2bf842dc010d01a4daec75c16f7e", aum = 0)
    /* renamed from: Y */
    private WarehouseInventoryItem m16878Y(ItemType jCVar) {
        if (dnV().containsKey(jCVar)) {
            return (WarehouseInventoryItem) dnV().get(jCVar);
        }
        mo10597X(jCVar);
        return (WarehouseInventoryItem) dnV().get(jCVar);
    }

    @C0064Am(aul = "220164bedca329b440641ff3ca8ccad1", aum = 0)
    /* renamed from: i */
    private void m16889i(WarehouseInventoryItem fIVar) {
    }
}
