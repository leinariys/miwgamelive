package game.script.newmarket.economy;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3857vs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.aBp  reason: case insensitive filesystem */
/* compiled from: a */
public class SinkAgent extends WarehouseAgent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz hhg = null;
    public static final C2491fm hhh = null;
    public static final C2491fm hhi = null;
    public static final C2491fm hhj = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5d813f08f8792d296288be524d6a222f", aum = 0)
    private static long bzj;

    static {
        m13087V();
    }

    public SinkAgent() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SinkAgent(SinkAgent abp) {
        super((C5540aNg) null);
        super._m_script_init(abp);
    }

    public SinkAgent(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13087V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = WarehouseAgent._m_fieldCount + 1;
        _m_methodCount = WarehouseAgent._m_methodCount + 3;
        int i = WarehouseAgent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(SinkAgent.class, "5d813f08f8792d296288be524d6a222f", i);
        hhg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) WarehouseAgent._m_fields, (Object[]) _m_fields);
        int i3 = WarehouseAgent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(SinkAgent.class, "bf73728093cd84c966cc37177a6b151f", i3);
        hhh = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(SinkAgent.class, "124239861d1469e8def29b37c46b402f", i4);
        hhi = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(SinkAgent.class, "0f8adaf1b8a447958ba70816e71d08b7", i5);
        hhj = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) WarehouseAgent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SinkAgent.class, C3857vs.class, _m_fields, _m_methods);
    }

    private long cJb() {
        return bFf().mo5608dq().mo3213o(hhg);
    }

    /* renamed from: jC */
    private void m13088jC(long j) {
        bFf().mo5608dq().mo3184b(hhg, j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3857vs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - WarehouseAgent._m_methodCount) {
            case 0:
                return new Boolean(cJc());
            case 1:
                return new Long(cJe());
            case 2:
                m13089jD(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public boolean cJd() {
        switch (bFf().mo6893i(hhh)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hhh, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hhh, new Object[0]));
                break;
        }
        return cJc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Maximum Ammount")
    public long cJf() {
        switch (bFf().mo6893i(hhi)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hhi, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hhi, new Object[0]));
                break;
        }
        return cJe();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Maximum Ammount")
    /* renamed from: jE */
    public void mo7982jE(long j) {
        switch (bFf().mo6893i(hhj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hhj, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hhj, new Object[]{new Long(j)}));
                break;
        }
        m13089jD(j);
    }

    @C0064Am(aul = "bf73728093cd84c966cc37177a6b151f", aum = 0)
    private boolean cJc() {
        if (dgB().mo18404rX() <= cJb()) {
            return false;
        }
        dgB().mo18397I(cJb());
        return true;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Maximum Ammount")
    @C0064Am(aul = "124239861d1469e8def29b37c46b402f", aum = 0)
    private long cJe() {
        return cJb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Maximum Ammount")
    @C0064Am(aul = "0f8adaf1b8a447958ba70816e71d08b7", aum = 0)
    /* renamed from: jD */
    private void m13089jD(long j) {
        m13088jC(j);
    }

    /* renamed from: a */
    public void mo7979a(SinkAgent abp) {
        super.mo9710a((WarehouseAgent) abp);
        m13088jC(abp.cJb());
    }
}
