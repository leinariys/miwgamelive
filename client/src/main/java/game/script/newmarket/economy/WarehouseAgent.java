package game.script.newmarket.economy;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0187CM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C6485anp
@C5511aMd
/* renamed from: a.aKN */
/* compiled from: a */
public abstract class WarehouseAgent extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eyu = null;
    public static final C2491fm hhh = null;
    public static final C5663aRz ijs = null;
    public static final C2491fm ijt = null;
    public static final C2491fm iju = null;
    public static final C2491fm ijv = null;
    public static final C2491fm ijw = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5b8681d4cd71d7ffa049e4737f4cf5ee", aum = 0)
    private static WarehouseInventoryItem cAg;
    @C0064Am(aul = "970239ca8b3a91a6664ce70798479414", aum = 1)
    private static C1808a cAh;

    static {
        m15950V();
    }

    public WarehouseAgent() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WarehouseAgent(WarehouseAgent akn) {
        super((C5540aNg) null);
        super._m_script_init(akn);
    }

    public WarehouseAgent(C5540aNg ang) {
        super(ang);
    }

    public WarehouseAgent(WarehouseInventoryItem fIVar) {
        super((C5540aNg) null);
        super._m_script_init(fIVar);
    }

    /* renamed from: V */
    static void m15950V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 5;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(WarehouseAgent.class, "5b8681d4cd71d7ffa049e4737f4cf5ee", i);
        ijs = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WarehouseAgent.class, "970239ca8b3a91a6664ce70798479414", i2);
        eyu = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(WarehouseAgent.class, "7009310fc021e6b9a2edde95b617bcb5", i4);
        hhh = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(WarehouseAgent.class, "36b02a1ed05a73c989a329472bf5adbb", i5);
        ijt = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(WarehouseAgent.class, "5dbf667a2c94626f048c7a035a22cb59", i6);
        iju = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(WarehouseAgent.class, "171d14b30e080cfdcabfc546292903a1", i7);
        ijv = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(WarehouseAgent.class, "d4ce4163049cae643294a0db50ba455d", i8);
        ijw = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WarehouseAgent.class, C0187CM.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m15951a(C1808a aVar) {
        bFf().mo5608dq().mo3197f(eyu, aVar);
    }

    /* renamed from: a */
    private void m15952a(WarehouseInventoryItem fIVar) {
        bFf().mo5608dq().mo3197f(ijs, fIVar);
    }

    @C4034yP
    @C0064Am(aul = "7009310fc021e6b9a2edde95b617bcb5", aum = 0)
    private boolean cJc() {
        throw new aWi(new aCE(this, hhh, new Object[0]));
    }

    private WarehouseInventoryItem dgy() {
        return (WarehouseInventoryItem) bFf().mo5608dq().mo3214p(ijs);
    }

    private C1808a dgz() {
        return (C1808a) bFf().mo5608dq().mo3214p(eyu);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0187CM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Boolean(cJc());
            case 1:
                return dgA();
            case 2:
                m15954c((WarehouseInventoryItem) args[0]);
                return null;
            case 3:
                return dgC();
            case 4:
                m15953b((C1808a) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Priority")
    /* renamed from: c */
    public void mo9712c(C1808a aVar) {
        switch (bFf().mo6893i(ijw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ijw, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ijw, new Object[]{aVar}));
                break;
        }
        m15953b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C4034yP
    public boolean cJd() {
        switch (bFf().mo6893i(hhh)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hhh, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hhh, new Object[0]));
                break;
        }
        return cJc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delegated Item")
    /* renamed from: d */
    public void mo9713d(WarehouseInventoryItem fIVar) {
        switch (bFf().mo6893i(iju)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iju, new Object[]{fIVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iju, new Object[]{fIVar}));
                break;
        }
        m15954c(fIVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delegated Item")
    public WarehouseInventoryItem dgB() {
        switch (bFf().mo6893i(ijt)) {
            case 0:
                return null;
            case 2:
                return (WarehouseInventoryItem) bFf().mo5606d(new aCE(this, ijt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ijt, new Object[0]));
                break;
        }
        return dgA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Priority")
    public C1808a dgD() {
        switch (bFf().mo6893i(ijv)) {
            case 0:
                return null;
            case 2:
                return (C1808a) bFf().mo5606d(new aCE(this, ijv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ijv, new Object[0]));
                break;
        }
        return dgC();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m15952a((WarehouseInventoryItem) null);
    }

    /* renamed from: a */
    public void mo9710a(WarehouseAgent akn) {
        super.mo10S();
        m15952a(akn.dgy());
    }

    /* renamed from: b */
    public void mo9711b(WarehouseInventoryItem fIVar) {
        super.mo10S();
        m15952a(fIVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Delegated Item")
    @C0064Am(aul = "36b02a1ed05a73c989a329472bf5adbb", aum = 0)
    private WarehouseInventoryItem dgA() {
        return dgy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Delegated Item")
    @C0064Am(aul = "5dbf667a2c94626f048c7a035a22cb59", aum = 0)
    /* renamed from: c */
    private void m15954c(WarehouseInventoryItem fIVar) {
        m15952a(fIVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Priority")
    @C0064Am(aul = "171d14b30e080cfdcabfc546292903a1", aum = 0)
    private C1808a dgC() {
        return dgz();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Priority")
    @C0064Am(aul = "d4ce4163049cae643294a0db50ba455d", aum = 0)
    /* renamed from: b */
    private void m15953b(C1808a aVar) {
        m15951a(aVar);
    }

    /* renamed from: a.aKN$a */
    public enum C1808a {
        HIGHEST,
        HIGH,
        NORMAL,
        LOW,
        LOWEST
    }
}
