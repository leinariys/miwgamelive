package game.script.newmarket.economy;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6584apk;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.aSu  reason: case insensitive filesystem */
/* compiled from: a */
public class SourceAgent extends WarehouseAgent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm hhh = null;
    public static final C5663aRz iMK = null;
    public static final C2491fm iML = null;
    public static final C2491fm iMM = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0b086c1c401ef44a510d9b7a2b61f833", aum = 0)
    private static long glF;

    static {
        m18274V();
    }

    public SourceAgent() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SourceAgent(C5540aNg ang) {
        super(ang);
    }

    public SourceAgent(SourceAgent asu) {
        super((C5540aNg) null);
        super._m_script_init(asu);
    }

    /* renamed from: V */
    static void m18274V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = WarehouseAgent._m_fieldCount + 1;
        _m_methodCount = WarehouseAgent._m_methodCount + 3;
        int i = WarehouseAgent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(SourceAgent.class, "0b086c1c401ef44a510d9b7a2b61f833", i);
        iMK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) WarehouseAgent._m_fields, (Object[]) _m_fields);
        int i3 = WarehouseAgent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(SourceAgent.class, "fccadc41e7edc5d9d6a0d9df6535de7a", i3);
        hhh = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(SourceAgent.class, "b557c68837a99b948a52f810e5e541b4", i4);
        iML = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(SourceAgent.class, "164a731596886f7a12e901a2dd9163ec", i5);
        iMM = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) WarehouseAgent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SourceAgent.class, C6584apk.class, _m_fields, _m_methods);
    }

    private long duo() {
        return bFf().mo5608dq().mo3213o(iMK);
    }

    /* renamed from: lT */
    private void m18275lT(long j) {
        bFf().mo5608dq().mo3184b(iMK, j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6584apk(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - WarehouseAgent._m_methodCount) {
            case 0:
                return new Boolean(cJc());
            case 1:
                return new Long(dup());
            case 2:
                m18276lU(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public boolean cJd() {
        switch (bFf().mo6893i(hhh)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hhh, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hhh, new Object[0]));
                break;
        }
        return cJc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Maximum Ammount")
    public long duq() {
        switch (bFf().mo6893i(iML)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iML, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iML, new Object[0]));
                break;
        }
        return dup();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Maximum Ammount")
    /* renamed from: lV */
    public void mo11452lV(long j) {
        switch (bFf().mo6893i(iMM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iMM, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iMM, new Object[]{new Long(j)}));
                break;
        }
        m18276lU(j);
    }

    @C0064Am(aul = "fccadc41e7edc5d9d6a0d9df6535de7a", aum = 0)
    private boolean cJc() {
        if (dgB().mo18404rX() >= duo()) {
            return false;
        }
        dgB().mo18397I(duo());
        return true;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Maximum Ammount")
    @C0064Am(aul = "b557c68837a99b948a52f810e5e541b4", aum = 0)
    private long dup() {
        return duo();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Maximum Ammount")
    @C0064Am(aul = "164a731596886f7a12e901a2dd9163ec", aum = 0)
    /* renamed from: lU */
    private void m18276lU(long j) {
        m18275lT(j);
    }

    /* renamed from: a */
    public void mo11450a(SourceAgent asu) {
        super.mo9710a((WarehouseAgent) asu);
        m18275lT(asu.duo());
    }
}
