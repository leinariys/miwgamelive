package game.script.newmarket.economy;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import logic.baa.*;
import logic.data.mbean.aFR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.fI */
/* compiled from: a */
public class WarehouseInventoryItem extends aDJ implements C1616Xf {
    /* renamed from: MS */
    public static final C5663aRz f7264MS = null;
    /* renamed from: MT */
    public static final C5663aRz f7265MT = null;
    /* renamed from: MV */
    public static final C5663aRz f7267MV = null;
    /* renamed from: MW */
    public static final C2491fm f7268MW = null;
    /* renamed from: MX */
    public static final C2491fm f7269MX = null;
    /* renamed from: MY */
    public static final C2491fm f7270MY = null;
    /* renamed from: MZ */
    public static final C2491fm f7271MZ = null;
    /* renamed from: Na */
    public static final C2491fm f7272Na = null;
    /* renamed from: Nb */
    public static final C2491fm f7273Nb = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final C5663aRz _f_type = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: cC */
    public static final C2491fm f7274cC = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9bbd5edd13b89f5b4b9cbd9cc93ac2e0", aum = 0)

    /* renamed from: MQ */
    private static ItemType f7262MQ;
    @C0064Am(aul = "9fc6d39e3cbe4e7f9fe60224f5b3e079", aum = 1)

    /* renamed from: MR */
    private static long f7263MR;
    @C0064Am(aul = "73b3c0b24d39fa98cf4b0e0c8023f86e", aum = 3)

    /* renamed from: MU */
    private static VirtualWarehouse f7266MU;
    @C0064Am(aul = "a11965f955d6dbd414b8750abe694eae", aum = 2)

    /* renamed from: db */
    private static long f7275db;

    static {
        m30690V();
    }

    public WarehouseInventoryItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WarehouseInventoryItem(C5540aNg ang) {
        super(ang);
    }

    public WarehouseInventoryItem(VirtualWarehouse aor, WarehouseInventoryItem fIVar) {
        super((C5540aNg) null);
        super._m_script_init(aor, fIVar);
    }

    public WarehouseInventoryItem(VirtualWarehouse aor, ItemType jCVar) {
        super((C5540aNg) null);
        super._m_script_init(aor, jCVar);
    }

    public WarehouseInventoryItem(VirtualWarehouse aor, ItemType jCVar, long j, long j2) {
        super((C5540aNg) null);
        super._m_script_init(aor, jCVar, j, j2);
    }

    /* renamed from: V */
    static void m30690V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 8;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(WarehouseInventoryItem.class, "9bbd5edd13b89f5b4b9cbd9cc93ac2e0", i);
        _f_type = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WarehouseInventoryItem.class, "9fc6d39e3cbe4e7f9fe60224f5b3e079", i2);
        f7264MS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WarehouseInventoryItem.class, "a11965f955d6dbd414b8750abe694eae", i3);
        f7265MT = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WarehouseInventoryItem.class, "73b3c0b24d39fa98cf4b0e0c8023f86e", i4);
        f7267MV = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(WarehouseInventoryItem.class, "62f383cf85c7b0ca22dd659d2ca8dfb4", i6);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(WarehouseInventoryItem.class, "2f4c1b7f43ccbf64e6551b8b9bf03c68", i7);
        f7268MW = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(WarehouseInventoryItem.class, "c2c727740720af54abf64aa29c2d3548", i8);
        f7269MX = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(WarehouseInventoryItem.class, "a1b61443a593972c346352201afcb0a8", i9);
        f7270MY = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(WarehouseInventoryItem.class, "aa3b5afe8ddff51626871483039e0e82", i10);
        f7271MZ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(WarehouseInventoryItem.class, "c8acd1fe0b0543b17c42a09e269f4ea9", i11);
        f7274cC = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(WarehouseInventoryItem.class, "217ca19b452d0382f72b191cde580f1f", i12);
        f7272Na = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(WarehouseInventoryItem.class, "8a3b3c51ba2da8b41bd567eeebe00ce3", i13);
        f7273Nb = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WarehouseInventoryItem.class, aFR.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m30685F(long j) {
        bFf().mo5608dq().mo3184b(f7264MS, j);
    }

    /* renamed from: G */
    private void m30686G(long j) {
        bFf().mo5608dq().mo3184b(f7265MT, j);
    }

    /* renamed from: a */
    private void m30692a(VirtualWarehouse aor) {
        bFf().mo5608dq().mo3197f(f7267MV, aor);
    }

    /* renamed from: g */
    private void m30695g(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(_f_type, jCVar);
    }

    /* renamed from: rS */
    private ItemType m30696rS() {
        return (ItemType) bFf().mo5608dq().mo3214p(_f_type);
    }

    /* renamed from: rT */
    private long m30697rT() {
        return bFf().mo5608dq().mo3213o(f7264MS);
    }

    /* renamed from: rU */
    private long m30698rU() {
        return bFf().mo5608dq().mo3213o(f7265MT);
    }

    /* renamed from: rV */
    private VirtualWarehouse m30699rV() {
        return (VirtualWarehouse) bFf().mo5608dq().mo3214p(f7267MV);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ammount")
    /* renamed from: I */
    public void mo18397I(long j) {
        switch (bFf().mo6893i(f7269MX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7269MX, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7269MX, new Object[]{new Long(j)}));
                break;
        }
        m30687H(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price")
    /* renamed from: K */
    public void mo18398K(long j) {
        switch (bFf().mo6893i(f7271MZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7271MZ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7271MZ, new Object[]{new Long(j)}));
                break;
        }
        m30688J(j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aFR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m30693au();
            case 1:
                return new Long(m30700rW());
            case 2:
                m30687H(((Long) args[0]).longValue());
                return null;
            case 3:
                return new Long(m30701rY());
            case 4:
                m30688J(((Long) args[0]).longValue());
                return null;
            case 5:
                return m30694ay();
            case 6:
                m30691a(((Long) args[0]).longValue(), ((Long) args[1]).longValue());
                return null;
            case 7:
                return new Long(m30689L(((Long) args[0]).longValue()));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void add(long j, long j2) {
        switch (bFf().mo6893i(f7272Na)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7272Na, new Object[]{new Long(j), new Long(j2)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7272Na, new Object[]{new Long(j), new Long(j2)}));
                break;
        }
        m30691a(j, j2);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    /* renamed from: az */
    public ItemType mo18403az() {
        switch (bFf().mo6893i(f7274cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f7274cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7274cC, new Object[0]));
                break;
        }
        return m30694ay();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ammount")
    /* renamed from: rX */
    public long mo18404rX() {
        switch (bFf().mo6893i(f7268MW)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7268MW, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7268MW, new Object[0]));
                break;
        }
        return m30700rW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price")
    /* renamed from: rZ */
    public long mo18405rZ() {
        switch (bFf().mo6893i(f7270MY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7270MY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7270MY, new Object[0]));
                break;
        }
        return m30701rY();
    }

    public long remove(long j) {
        switch (bFf().mo6893i(f7273Nb)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7273Nb, new Object[]{new Long(j)}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7273Nb, new Object[]{new Long(j)}));
                break;
        }
        return m30689L(j);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m30693au();
    }

    @C0064Am(aul = "62f383cf85c7b0ca22dd659d2ca8dfb4", aum = 0)
    /* renamed from: au */
    private String m30693au() {
        return m30696rS() + " [ " + m30697rT() + " units @ T$ " + m30698rU() + " ]";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo18400a(VirtualWarehouse aor, ItemType jCVar) {
        super.mo10S();
        m30692a(aor);
        m30695g(jCVar);
        m30685F(0);
        m30686G(0);
    }

    /* renamed from: a */
    public void mo18401a(VirtualWarehouse aor, ItemType jCVar, long j, long j2) {
        super.mo10S();
        m30692a(aor);
        m30695g(jCVar);
        m30685F(j);
        m30686G(j2);
    }

    /* renamed from: a */
    public void mo18399a(VirtualWarehouse aor, WarehouseInventoryItem fIVar) {
        super.mo10S();
        m30692a(aor);
        m30695g(fIVar.m30696rS());
        m30685F(fIVar.m30697rT());
        m30686G(fIVar.m30698rU());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ammount")
    @C0064Am(aul = "2f4c1b7f43ccbf64e6551b8b9bf03c68", aum = 0)
    /* renamed from: rW */
    private long m30700rW() {
        return m30697rT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ammount")
    @C0064Am(aul = "c2c727740720af54abf64aa29c2d3548", aum = 0)
    /* renamed from: H */
    private void m30687H(long j) {
        m30685F(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price")
    @C0064Am(aul = "a1b61443a593972c346352201afcb0a8", aum = 0)
    /* renamed from: rY */
    private long m30701rY() {
        return m30698rU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price")
    @C0064Am(aul = "aa3b5afe8ddff51626871483039e0e82", aum = 0)
    /* renamed from: J */
    private void m30688J(long j) {
        m30686G(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    @C0064Am(aul = "c8acd1fe0b0543b17c42a09e269f4ea9", aum = 0)
    /* renamed from: ay */
    private ItemType m30694ay() {
        return m30696rS();
    }

    @C0064Am(aul = "217ca19b452d0382f72b191cde580f1f", aum = 0)
    /* renamed from: a */
    private void m30691a(long j, long j2) {
        long rT = m30697rT();
        if (j >= 0) {
            if (j2 >= 0) {
                m30686G((long) (((double) ((m30697rT() * m30698rU()) + (j * j2))) / ((double) (m30697rT() + j))));
            }
            m30685F(m30697rT() + j);
            if (m30697rT() != rT) {
                m30699rV().mo10606j(this);
            }
        }
    }

    @C0064Am(aul = "8a3b3c51ba2da8b41bd567eeebe00ce3", aum = 0)
    /* renamed from: L */
    private long m30689L(long j) {
        long rT = m30697rT();
        m30685F(m30697rT() - j);
        if (m30697rT() < 0) {
            j += m30697rT();
            m30685F(0);
            if (m30697rT() != rT) {
                m30699rV().mo10606j(this);
            }
        } else if (m30697rT() != rT) {
            m30699rV().mo10606j(this);
        }
        return j;
    }
}
