package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.script.TaskletImpl;
import game.script.item.ItemType;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C0134Bf;
import logic.data.mbean.C1079Pn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aEU */
/* compiled from: a */
public abstract class CommercialOrderInfo extends aDJ implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bfH = null;
    public static final C2491fm bfI = null;
    public static final C2491fm iYu = null;
    public static final C5663aRz iZc = null;
    public static final C2491fm iZd = null;
    public static final C2491fm iZe = null;
    public static final C2491fm iZf = null;
    /* renamed from: kO */
    public static final C5663aRz f2692kO = null;
    /* renamed from: kQ */
    public static final C5663aRz f2694kQ = null;
    /* renamed from: kS */
    public static final C5663aRz f2696kS = null;
    /* renamed from: kU */
    public static final C5663aRz f2698kU = null;
    /* renamed from: kW */
    public static final C5663aRz f2700kW = null;
    /* renamed from: kX */
    public static final C5663aRz f2701kX = null;
    /* renamed from: kZ */
    public static final C5663aRz f2703kZ = null;
    /* renamed from: la */
    public static final C5663aRz f2704la = null;
    /* renamed from: lh */
    public static final C5663aRz f2705lh = null;
    /* renamed from: lk */
    public static final C2491fm f2706lk = null;
    /* renamed from: ll */
    public static final C2491fm f2707ll = null;
    /* renamed from: lm */
    public static final C2491fm f2708lm = null;
    /* renamed from: lo */
    public static final C2491fm f2709lo = null;
    /* renamed from: lp */
    public static final C2491fm f2710lp = null;
    /* renamed from: lq */
    public static final C2491fm f2711lq = null;
    /* renamed from: lr */
    public static final C2491fm f2712lr = null;
    /* renamed from: lt */
    public static final C2491fm f2713lt = null;
    /* renamed from: lu */
    public static final C2491fm f2714lu = null;
    /* renamed from: lv */
    public static final C2491fm f2715lv = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "68d8c553902e61e39739b8db26aa83ef", aum = 7)

    /* renamed from: Q */
    private static long f2689Q;
    @C0064Am(aul = "f29ae217c69908067936eddbbd40436d", aum = 0)

    /* renamed from: cY */
    private static ItemType f2690cY;
    @C0064Am(aul = "1ded91cc9fc6ee0340ce958fdad88c07", aum = 8)
    private static long ckO;
    @C0064Am(aul = "7ad536d3cc7393d7f00ca31dff9b6777", aum = 9)
    private static CommercialOrderTicker ckP;
    @C0064Am(aul = "68087d1fcd79ea8060a74128eefb92f0", aum = 5)

    /* renamed from: iH */
    private static Station f2691iH;
    @C0064Am(aul = "213a081a87af0427a43f6a8cece90c35", aum = 1)

    /* renamed from: kP */
    private static int f2693kP;
    @C0064Am(aul = "b5b3077527fa07efba201f341eff5abc", aum = 2)

    /* renamed from: kR */
    private static int f2695kR;
    @C0064Am(aul = "078c9b4407456b2c13a0c2f541ec6feb", aum = 3)

    /* renamed from: kT */
    private static int f2697kT;
    @C0064Am(aul = "a3dee6875881bd5eca931ef5afa45d73", aum = 4)

    /* renamed from: kV */
    private static long f2699kV;
    @C0064Am(aul = "feb9d9dc2c2309eec8aa2cf24f2a4311", aum = 6)

    /* renamed from: kY */
    private static long f2702kY;

    static {
        m14187V();
    }

    @ClientOnly
    private int iZb;

    public CommercialOrderInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CommercialOrderInfo(CommercialOrder aJVar) {
        super((C5540aNg) null);
        super._m_script_init(aJVar);
    }

    public CommercialOrderInfo(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14187V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 10;
        _m_methodCount = aDJ._m_methodCount + 17;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(CommercialOrderInfo.class, "f29ae217c69908067936eddbbd40436d", i);
        f2692kO = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CommercialOrderInfo.class, "213a081a87af0427a43f6a8cece90c35", i2);
        f2694kQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CommercialOrderInfo.class, "b5b3077527fa07efba201f341eff5abc", i3);
        f2696kS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CommercialOrderInfo.class, "078c9b4407456b2c13a0c2f541ec6feb", i4);
        f2698kU = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CommercialOrderInfo.class, "a3dee6875881bd5eca931ef5afa45d73", i5);
        f2700kW = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CommercialOrderInfo.class, "68087d1fcd79ea8060a74128eefb92f0", i6);
        f2701kX = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CommercialOrderInfo.class, "feb9d9dc2c2309eec8aa2cf24f2a4311", i7);
        f2703kZ = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(CommercialOrderInfo.class, "68d8c553902e61e39739b8db26aa83ef", i8);
        f2704la = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(CommercialOrderInfo.class, "1ded91cc9fc6ee0340ce958fdad88c07", i9);
        iZc = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(CommercialOrderInfo.class, "7ad536d3cc7393d7f00ca31dff9b6777", i10);
        f2705lh = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i12 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 17)];
        C2491fm a = C4105zY.m41624a(CommercialOrderInfo.class, "6df038105dde7177fb92c955940d11f6", i12);
        bfH = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(CommercialOrderInfo.class, "d5068b805f048b133b4c7a350a2b7763", i13);
        f2715lv = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(CommercialOrderInfo.class, "4389ce2838d71c79abbd04c2feee9e47", i14);
        f2706lk = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(CommercialOrderInfo.class, "d85c8478dfc925187c6a10a110817142", i15);
        f2711lq = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(CommercialOrderInfo.class, "2dc853d6ef43748a69b85074b8ccbb4a", i16);
        f2712lr = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(CommercialOrderInfo.class, "c4473ad4179be6ea760c4b6ff9f49af8", i17);
        f2713lt = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(CommercialOrderInfo.class, "074f01ae975ec16d109aa3dd2376db46", i18);
        f2707ll = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(CommercialOrderInfo.class, "67655f6f96e11790864a52a262814eb4", i19);
        f2708lm = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(CommercialOrderInfo.class, "3d8fb03a7ed53df57d1ebd0bfc1a8679", i20);
        iYu = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(CommercialOrderInfo.class, "f8c78d2ae942442c9fa759e38633be5e", i21);
        f2709lo = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(CommercialOrderInfo.class, "0268dc4d523fb6a7f12b9f5a35009965", i22);
        f2710lp = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(CommercialOrderInfo.class, "dcbf9493a79c220d34c63aca6e608248", i23);
        iZd = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(CommercialOrderInfo.class, "e0437ef5c50ebf644540ade8cd770139", i24);
        iZe = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(CommercialOrderInfo.class, "4e54eee0051b058880c6ba7f1cc08b08", i25);
        f2714lu = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(CommercialOrderInfo.class, "939895a67ffd802a467b2263489ffda2", i26);
        iZf = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(CommercialOrderInfo.class, "b5791ca4db6e1efb86d1a705c811570b", i27);
        _f_dispose_0020_0028_0029V = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(CommercialOrderInfo.class, "1c25548b386ed9633a4c7b1f30311196", i28);
        bfI = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CommercialOrderInfo.class, C0134Bf.class, _m_fields, _m_methods);
    }

    /* renamed from: C */
    private void m14183C(int i) {
        bFf().mo5608dq().mo3183b(f2694kQ, i);
    }

    /* renamed from: D */
    private void m14184D(int i) {
        bFf().mo5608dq().mo3183b(f2696kS, i);
    }

    /* renamed from: E */
    private void m14185E(int i) {
        bFf().mo5608dq().mo3183b(f2698kU, i);
    }

    /* renamed from: a */
    private void m14188a(Station bf) {
        bFf().mo5608dq().mo3197f(f2701kX, bf);
    }

    /* renamed from: a */
    private void m14189a(CommercialOrderTicker aVar) {
        bFf().mo5608dq().mo3197f(f2705lh, aVar);
    }

    @C0064Am(aul = "6df038105dde7177fb92c955940d11f6", aum = 0)
    private C0847MM abh() {
        throw new aWi(new aCE(this, bfH, new Object[0]));
    }

    @C0064Am(aul = "1c25548b386ed9633a4c7b1f30311196", aum = 0)
    private void abj() {
        throw new aWi(new aCE(this, bfI, new Object[0]));
    }

    private long dBn() {
        return bFf().mo5608dq().mo3213o(iZc);
    }

    private CommercialOrderTicker dBo() {
        return (CommercialOrderTicker) bFf().mo5608dq().mo3214p(f2705lh);
    }

    /* renamed from: eA */
    private ItemType m14190eA() {
        return (ItemType) bFf().mo5608dq().mo3214p(f2692kO);
    }

    /* renamed from: eB */
    private int m14191eB() {
        return bFf().mo5608dq().mo3212n(f2694kQ);
    }

    /* renamed from: eC */
    private int m14192eC() {
        return bFf().mo5608dq().mo3212n(f2696kS);
    }

    /* renamed from: eD */
    private int m14193eD() {
        return bFf().mo5608dq().mo3212n(f2698kU);
    }

    /* renamed from: eE */
    private long m14194eE() {
        return bFf().mo5608dq().mo3213o(f2700kW);
    }

    /* renamed from: eF */
    private Station m14195eF() {
        return (Station) bFf().mo5608dq().mo3214p(f2701kX);
    }

    /* renamed from: eG */
    private long m14196eG() {
        return bFf().mo5608dq().mo3213o(f2703kZ);
    }

    /* renamed from: eH */
    private long m14197eH() {
        return bFf().mo5608dq().mo3213o(f2704la);
    }

    /* renamed from: f */
    private void m14204f(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f2692kO, jCVar);
    }

    /* renamed from: k */
    private void m14209k(long j) {
        bFf().mo5608dq().mo3184b(f2700kW, j);
    }

    /* renamed from: l */
    private void m14210l(long j) {
        bFf().mo5608dq().mo3184b(f2703kZ, j);
    }

    /* renamed from: m */
    private void m14211m(long j) {
        bFf().mo5608dq().mo3184b(f2704la, j);
    }

    /* renamed from: ml */
    private void m14212ml(long j) {
        bFf().mo5608dq().mo3184b(iZc, j);
    }

    /* renamed from: K */
    public void mo8613K(int i) {
        switch (bFf().mo6893i(f2714lu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2714lu, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2714lu, new Object[]{new Integer(i)}));
                break;
        }
        m14186J(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0134Bf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return abh();
            case 1:
                return new Integer(m14207fe());
            case 2:
                return m14198eO();
            case 3:
                return new Integer(m14203eY());
            case 4:
                return new Integer(m14205fa());
            case 5:
                return new Integer(m14206fc());
            case 6:
                return new Long(m14199eQ());
            case 7:
                return m14200eS();
            case 8:
                return new Long(dAO());
            case 9:
                return new Long(m14201eW());
            case 10:
                return new Long(m14202eX());
            case 11:
                return new Long(dBp());
            case 12:
                return new Long(dBr());
            case 13:
                m14186J(((Integer) args[0]).intValue());
                return null;
            case 14:
                return new Integer(dBt());
            case 15:
                m14208fg();
                return null;
            case 16:
                abj();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C0847MM abi() {
        switch (bFf().mo6893i(bfH)) {
            case 0:
                return null;
            case 2:
                return (C0847MM) bFf().mo5606d(new aCE(this, bfH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfH, new Object[0]));
                break;
        }
        return abh();
    }

    public void abk() {
        switch (bFf().mo6893i(bfI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfI, new Object[0]));
                break;
        }
        abj();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public long dAP() {
        switch (bFf().mo6893i(iYu)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iYu, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iYu, new Object[0]));
                break;
        }
        return dAO();
    }

    public long dBq() {
        switch (bFf().mo6893i(iZd)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iZd, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZd, new Object[0]));
                break;
        }
        return dBp();
    }

    public long dBs() {
        switch (bFf().mo6893i(iZe)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iZe, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZe, new Object[0]));
                break;
        }
        return dBr();
    }

    public int dBu() {
        switch (bFf().mo6893i(iZf)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, iZf, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZf, new Object[0]));
                break;
        }
        return dBt();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m14208fg();
    }

    /* renamed from: eP */
    public ItemType mo8620eP() {
        switch (bFf().mo6893i(f2706lk)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f2706lk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2706lk, new Object[0]));
                break;
        }
        return m14198eO();
    }

    /* renamed from: eR */
    public long mo8621eR() {
        switch (bFf().mo6893i(f2707ll)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f2707ll, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2707ll, new Object[0]));
                break;
        }
        return m14199eQ();
    }

    /* renamed from: eT */
    public Station mo8622eT() {
        switch (bFf().mo6893i(f2708lm)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f2708lm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2708lm, new Object[0]));
                break;
        }
        return m14200eS();
    }

    /* renamed from: eZ */
    public int mo8623eZ() {
        switch (bFf().mo6893i(f2711lq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f2711lq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2711lq, new Object[0]));
                break;
        }
        return m14203eY();
    }

    /* renamed from: fb */
    public int mo8625fb() {
        switch (bFf().mo6893i(f2712lr)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f2712lr, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2712lr, new Object[0]));
                break;
        }
        return m14205fa();
    }

    /* renamed from: fd */
    public int mo8626fd() {
        switch (bFf().mo6893i(f2713lt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f2713lt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2713lt, new Object[0]));
                break;
        }
        return m14206fc();
    }

    /* renamed from: ff */
    public int mo8627ff() {
        switch (bFf().mo6893i(f2715lv)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f2715lv, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2715lv, new Object[0]));
                break;
        }
        return m14207fe();
    }

    public long getCreationTime() {
        switch (bFf().mo6893i(f2709lo)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f2709lo, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2709lo, new Object[0]));
                break;
        }
        return m14201eW();
    }

    public long getTimeout() {
        switch (bFf().mo6893i(f2710lp)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f2710lp, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2710lp, new Object[0]));
                break;
        }
        return m14202eX();
    }

    /* renamed from: f */
    public void mo8624f(CommercialOrder aJVar) {
        super.mo10S();
        m14204f(aJVar.mo9437eP());
        m14183C(aJVar.mo9441eZ());
        m14184D(aJVar.mo9442fb());
        m14185E(aJVar.mo9443fd());
        m14209k(aJVar.mo9438eR());
        m14188a(aJVar.mo9439eT());
        m14210l(aJVar.mo9440eV());
        m14211m(aJVar.getCreationTime());
        m14212ml(cVr());
    }

    @C0064Am(aul = "d5068b805f048b133b4c7a350a2b7763", aum = 0)
    /* renamed from: fe */
    private int m14207fe() {
        return mo8623eZ() - mo8626fd();
    }

    @C0064Am(aul = "4389ce2838d71c79abbd04c2feee9e47", aum = 0)
    /* renamed from: eO */
    private ItemType m14198eO() {
        return m14190eA();
    }

    @C0064Am(aul = "d85c8478dfc925187c6a10a110817142", aum = 0)
    /* renamed from: eY */
    private int m14203eY() {
        return m14191eB();
    }

    @C0064Am(aul = "2dc853d6ef43748a69b85074b8ccbb4a", aum = 0)
    /* renamed from: fa */
    private int m14205fa() {
        return m14192eC();
    }

    @C0064Am(aul = "c4473ad4179be6ea760c4b6ff9f49af8", aum = 0)
    /* renamed from: fc */
    private int m14206fc() {
        return m14193eD() + this.iZb;
    }

    @C0064Am(aul = "074f01ae975ec16d109aa3dd2376db46", aum = 0)
    /* renamed from: eQ */
    private long m14199eQ() {
        return m14194eE();
    }

    @C0064Am(aul = "67655f6f96e11790864a52a262814eb4", aum = 0)
    /* renamed from: eS */
    private Station m14200eS() {
        return m14195eF();
    }

    @C0064Am(aul = "3d8fb03a7ed53df57d1ebd0bfc1a8679", aum = 0)
    private long dAO() {
        return m14196eG();
    }

    @C0064Am(aul = "f8c78d2ae942442c9fa759e38633be5e", aum = 0)
    /* renamed from: eW */
    private long m14201eW() {
        return m14197eH();
    }

    @C0064Am(aul = "0268dc4d523fb6a7f12b9f5a35009965", aum = 0)
    /* renamed from: eX */
    private long m14202eX() {
        return (m14197eH() + m14196eG()) - cVr();
    }

    @C0064Am(aul = "dcbf9493a79c220d34c63aca6e608248", aum = 0)
    private long dBp() {
        return dBn();
    }

    @C0064Am(aul = "e0437ef5c50ebf644540ade8cd770139", aum = 0)
    private long dBr() {
        return cVr() - dBn();
    }

    @C0064Am(aul = "4e54eee0051b058880c6ba7f1cc08b08", aum = 0)
    /* renamed from: J */
    private void m14186J(int i) {
        this.iZb = i;
    }

    @C0064Am(aul = "939895a67ffd802a467b2263489ffda2", aum = 0)
    private int dBt() {
        return this.iZb;
    }

    @C0064Am(aul = "b5791ca4db6e1efb86d1a705c811570b", aum = 0)
    /* renamed from: fg */
    private void m14208fg() {
        super.dispose();
        if (dBo() != null) {
            dBo().stop();
            dBo().dispose();
            m14189a((CommercialOrderTicker) null);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aEU$a */
    public class CommercialOrderTicker extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f2716JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f2717aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "497b9233f1e5836cb816c91751292a52", aum = 0)
        static /* synthetic */ CommercialOrderInfo dPW;

        static {
            m14229V();
        }

        public CommercialOrderTicker() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CommercialOrderTicker(CommercialOrderInfo aeu, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(aeu, adj);
        }

        public CommercialOrderTicker(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m14229V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CommercialOrderTicker.class, "497b9233f1e5836cb816c91751292a52", i);
            f2717aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(CommercialOrderTicker.class, "2bf8fea002f388e13e589df06a757259", i3);
            f2716JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CommercialOrderTicker.class, C1079Pn.class, _m_fields, _m_methods);
        }

        private CommercialOrderInfo cYf() {
            return (CommercialOrderInfo) bFf().mo5608dq().mo3214p(f2717aT);
        }

        /* renamed from: j */
        private void m14230j(CommercialOrderInfo aeu) {
            bFf().mo5608dq().mo3197f(f2717aT, aeu);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1079Pn(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m14231pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f2716JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f2716JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f2716JD, new Object[0]));
                    break;
            }
            m14231pi();
        }

        /* renamed from: a */
        public void mo8630a(CommercialOrderInfo aeu, aDJ adj) {
            m14230j(aeu);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "2bf8fea002f388e13e589df06a757259", aum = 0)
        /* renamed from: pi */
        private void m14231pi() {
        }
    }
}
