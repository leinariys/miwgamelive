package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.consignment.ConsignmentFeeManager;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C5612aQa;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.OI */
/* compiled from: a */
public class Market extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1285bL = null;
    /* renamed from: bN */
    public static final C2491fm f1286bN = null;
    /* renamed from: bO */
    public static final C2491fm f1287bO = null;
    /* renamed from: bP */
    public static final C2491fm f1288bP = null;
    public static final C5663aRz dNJ = null;
    public static final C5663aRz dNK = null;
    public static final C5663aRz dNL = null;
    public static final C5663aRz dNN = null;
    public static final C2491fm dNO = null;
    public static final C2491fm dNP = null;
    public static final C2491fm dNQ = null;
    public static final C2491fm dNR = null;
    public static final C2491fm dNS = null;
    public static final C2491fm dNT = null;
    public static final C2491fm dNU = null;
    public static final C2491fm dNV = null;
    public static final C2491fm dNW = null;
    public static final C2491fm dNX = null;
    public static final C2491fm dNY = null;
    public static final C2491fm dNZ = null;
    public static final C2491fm dOa = null;
    public static final C2491fm dOb = null;
    public static final C2491fm dOc = null;
    public static final C2491fm dOd = null;
    public static final C2491fm dOe = null;
    public static final C2491fm dOf = null;
    public static final C2491fm dOg = null;
    public static final C2491fm dOh = null;
    public static final C2491fm dOi = null;
    public static final long serialVersionUID = 0;
    private static final String dNG = "Creating Buy Order";
    private static final String dNH = "Creaing Sell Order";
    private static final Log logger = LogPrinter.setClass(Market.class);
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "c39e2bc3b195f5fa9db3f7a6fbd288e5", aum = 4)

    /* renamed from: bK */
    private static UUID f1284bK = null;
    @C0064Am(aul = "469c7c51b6f78f9d0995cfcb88d8f35f", aum = 1)
    private static C1556Wo<Station, ListContainer<BuyOrder>> bvA = null;
    @C0064Am(aul = "27ba290036d4413ae0582424e0fd588e", aum = 2)
    private static C1556Wo<Station, ListContainer<SellOrder>> bvB = null;
    @C0064Am(aul = "2e5af13e3531dab8d4d59793e469817f", aum = 0)
    private static C3438ra<ItemType> dNI;
    @C0064Am(aul = "5ab4055974bea91c599e5dbdabaf76a8", aum = 3)
    private static ConsignmentFeeManager dNM;

    static {
        m7845V();
    }

    public Market() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Market(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7845V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 24;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(Market.class, "2e5af13e3531dab8d4d59793e469817f", i);
        dNJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Market.class, "469c7c51b6f78f9d0995cfcb88d8f35f", i2);
        dNK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Market.class, "27ba290036d4413ae0582424e0fd588e", i3);
        dNL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Market.class, "5ab4055974bea91c599e5dbdabaf76a8", i4);
        dNN = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Market.class, "c39e2bc3b195f5fa9db3f7a6fbd288e5", i5);
        f1285bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 24)];
        C2491fm a = C4105zY.m41624a(Market.class, "5ff43cda2e8fafad51eeee61ae971f58", i7);
        f1286bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(Market.class, "8d3848f3895252d632e65cd08efe3a8b", i8);
        f1287bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(Market.class, "2268daa94218c92904d720a18d9c0f82", i9);
        f1288bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(Market.class, "5ab81b14ad65dfab8ad425ebc6a1080c", i10);
        dNO = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(Market.class, "c709fd2e8fe06bc3d29c56cf2262b4e1", i11);
        dNP = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(Market.class, "31906229eacce3ed15e7375722d68724", i12);
        dNQ = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(Market.class, "b6b2d10d5d0a187e95fe46db8efe3e80", i13);
        dNR = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(Market.class, "61ab2f5c6f014856fef4b38f837b808d", i14);
        dNS = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(Market.class, "687daceaba62f2eaa0a2997c8f0ef8e6", i15);
        dNT = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(Market.class, "84d5950bc7bee5ca4d2b16ba49ce65fe", i16);
        dNU = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(Market.class, "5b4a8a0d7b8e62d72de16c77331b3d9a", i17);
        dNV = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(Market.class, "eb4493dc931593ed456596fb8290a5d7", i18);
        dNW = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(Market.class, "9e575c8ad91620b123dc51edae329cb1", i19);
        dNX = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(Market.class, "f1143a4f42695e89e8d481fb3b2b506f", i20);
        dNY = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(Market.class, "26c1828ddeb12bda01a91b8a921ea29a", i21);
        dNZ = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(Market.class, "c4b5a76f1245659a99960097c81b1581", i22);
        dOa = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        C2491fm a17 = C4105zY.m41624a(Market.class, "adfe787e1f503461fc5b84293e7691e1", i23);
        dOb = a17;
        fmVarArr[i23] = a17;
        int i24 = i23 + 1;
        C2491fm a18 = C4105zY.m41624a(Market.class, "6212b22d7ad593023e88b3ac16d241d5", i24);
        dOc = a18;
        fmVarArr[i24] = a18;
        int i25 = i24 + 1;
        C2491fm a19 = C4105zY.m41624a(Market.class, "11b581bd837ea91999f3faf9758304be", i25);
        dOd = a19;
        fmVarArr[i25] = a19;
        int i26 = i25 + 1;
        C2491fm a20 = C4105zY.m41624a(Market.class, "a8c5b07943068faa35b38d91cb6e078e", i26);
        dOe = a20;
        fmVarArr[i26] = a20;
        int i27 = i26 + 1;
        C2491fm a21 = C4105zY.m41624a(Market.class, "bcbd4b9e764758b9e7c51d42ac221b2d", i27);
        dOf = a21;
        fmVarArr[i27] = a21;
        int i28 = i27 + 1;
        C2491fm a22 = C4105zY.m41624a(Market.class, "45b1141f7e4f38be4a95a15c7edee790", i28);
        dOg = a22;
        fmVarArr[i28] = a22;
        int i29 = i28 + 1;
        C2491fm a23 = C4105zY.m41624a(Market.class, "48db36ffcf1ca660e1f3249109744aa2", i29);
        dOh = a23;
        fmVarArr[i29] = a23;
        int i30 = i29 + 1;
        C2491fm a24 = C4105zY.m41624a(Market.class, "a01098b3aba5f4c2411933025ae16013", i30);
        dOi = a24;
        fmVarArr[i30] = a24;
        int i31 = i30 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Market.class, C5612aQa.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "9e575c8ad91620b123dc51edae329cb1", aum = 0)
    @C5566aOg
    /* renamed from: K */
    private List<BuyOrder> m7841K(Station bf) {
        throw new aWi(new aCE(this, dNX, new Object[]{bf}));
    }

    /* renamed from: K */
    private void m7842K(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dNK, wo);
    }

    /* renamed from: L */
    private void m7843L(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dNL, wo);
    }

    @C0064Am(aul = "f1143a4f42695e89e8d481fb3b2b506f", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private List<SellOrder> m7844M(Station bf) {
        throw new aWi(new aCE(this, dNY, new Object[]{bf}));
    }

    @C0064Am(aul = "31906229eacce3ed15e7375722d68724", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private BuyOrder m7846a(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2) {
        throw new aWi(new aCE(this, dNQ, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
    }

    @C0064Am(aul = "b6b2d10d5d0a187e95fe46db8efe3e80", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private BuyOrder m7847a(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2, Station bf) {
        throw new aWi(new aCE(this, dNR, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), bf}));
    }

    @C0064Am(aul = "687daceaba62f2eaa0a2997c8f0ef8e6", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private SellOrder m7848a(C0847MM mm, Item auq, int i, long j, long j2) {
        throw new aWi(new aCE(this, dNT, new Object[]{mm, auq, new Integer(i), new Long(j), new Long(j2)}));
    }

    /* renamed from: a */
    private void m7850a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1285bL, uuid);
    }

    /* renamed from: an */
    private UUID m7851an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1285bL);
    }

    @C0064Am(aul = "adfe787e1f503461fc5b84293e7691e1", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m7854b(CommercialOrder aJVar) {
        throw new aWi(new aCE(this, dOb, new Object[]{aJVar}));
    }

    /* renamed from: bm */
    private void m7857bm(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dNJ, raVar);
    }

    private C3438ra bmu() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dNJ);
    }

    private C1556Wo bmv() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dNK);
    }

    private C1556Wo bmw() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dNL);
    }

    private ConsignmentFeeManager bmx() {
        return (ConsignmentFeeManager) bFf().mo5608dq().mo3214p(dNN);
    }

    @C0064Am(aul = "84d5950bc7bee5ca4d2b16ba49ce65fe", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private SellOrder m7858c(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2) {
        throw new aWi(new aCE(this, dNU, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
    }

    @C0064Am(aul = "5b4a8a0d7b8e62d72de16c77331b3d9a", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private SellOrder m7859c(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2, Station bf) {
        throw new aWi(new aCE(this, dNV, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), bf}));
    }

    @C0064Am(aul = "c4b5a76f1245659a99960097c81b1581", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m7860c(BuyOrder kl) {
        throw new aWi(new aCE(this, dOa, new Object[]{kl}));
    }

    @C0064Am(aul = "26c1828ddeb12bda01a91b8a921ea29a", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m7862c(SellOrder awh) {
        throw new aWi(new aCE(this, dNZ, new Object[]{awh}));
    }

    /* renamed from: c */
    private void m7863c(UUID uuid) {
        switch (bFf().mo6893i(f1287bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1287bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1287bO, new Object[]{uuid}));
                break;
        }
        m7855b(uuid);
    }

    /* renamed from: d */
    private void m7864d(C0847MM mm, int i, long j) {
        switch (bFf().mo6893i(dNW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dNW, new Object[]{mm, new Integer(i), new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dNW, new Object[]{mm, new Integer(i), new Long(j)}));
                break;
        }
        m7861c(mm, i, j);
    }

    /* renamed from: e */
    private void m7865e(ConsignmentFeeManager auVar) {
        bFf().mo5608dq().mo3197f(dNN, auVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Player Item Types (BuyOrder)")
    @C0064Am(aul = "45b1141f7e4f38be4a95a15c7edee790", aum = 0)
    @C5566aOg
    /* renamed from: w */
    private void m7868w(ItemType jCVar) {
        throw new aWi(new aCE(this, dOg, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Player Item Types (BuyOrder)")
    @C0064Am(aul = "48db36ffcf1ca660e1f3249109744aa2", aum = 0)
    @C5566aOg
    /* renamed from: y */
    private void m7869y(ItemType jCVar) {
        throw new aWi(new aCE(this, dOh, new Object[]{jCVar}));
    }

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public ListContainer<BuyOrder> mo4365H(Station bf) {
        switch (bFf().mo6893i(dNO)) {
            case 0:
                return null;
            case 2:
                return (ListContainer) bFf().mo5606d(new aCE(this, dNO, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, dNO, new Object[]{bf}));
                break;
        }
        return m7839G(bf);
    }

    /* access modifiers changed from: protected */
    /* renamed from: J */
    public ListContainer<SellOrder> mo4366J(Station bf) {
        switch (bFf().mo6893i(dNP)) {
            case 0:
                return null;
            case 2:
                return (ListContainer) bFf().mo5606d(new aCE(this, dNP, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, dNP, new Object[]{bf}));
                break;
        }
        return m7840I(bf);
    }

    @C5566aOg
    /* renamed from: L */
    public List<BuyOrder> mo4367L(Station bf) {
        switch (bFf().mo6893i(dNX)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dNX, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, dNX, new Object[]{bf}));
                break;
        }
        return m7841K(bf);
    }

    @C5566aOg
    /* renamed from: N */
    public List<SellOrder> mo4368N(Station bf) {
        switch (bFf().mo6893i(dNY)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dNY, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, dNY, new Object[]{bf}));
                break;
        }
        return m7844M(bf);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5612aQa(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m7852ap();
            case 1:
                m7855b((UUID) args[0]);
                return null;
            case 2:
                return m7853ar();
            case 3:
                return m7839G((Station) args[0]);
            case 4:
                return m7840I((Station) args[0]);
            case 5:
                return m7846a((C0847MM) args[0], (ItemType) args[1], ((Integer) args[2]).intValue(), ((Integer) args[3]).intValue(), ((Long) args[4]).longValue(), ((Long) args[5]).longValue());
            case 6:
                return m7847a((C0847MM) args[0], (ItemType) args[1], ((Integer) args[2]).intValue(), ((Integer) args[3]).intValue(), ((Long) args[4]).longValue(), ((Long) args[5]).longValue(), (Station) args[6]);
            case 7:
                m7849a((C0847MM) args[0], ((Integer) args[1]).intValue(), ((Long) args[2]).longValue());
                return null;
            case 8:
                return m7848a((C0847MM) args[0], (Item) args[1], ((Integer) args[2]).intValue(), ((Long) args[3]).longValue(), ((Long) args[4]).longValue());
            case 9:
                return m7858c((C0847MM) args[0], (ItemType) args[1], ((Integer) args[2]).intValue(), ((Integer) args[3]).intValue(), ((Long) args[4]).longValue(), ((Long) args[5]).longValue());
            case 10:
                return m7859c((C0847MM) args[0], (ItemType) args[1], ((Integer) args[2]).intValue(), ((Integer) args[3]).intValue(), ((Long) args[4]).longValue(), ((Long) args[5]).longValue(), (Station) args[6]);
            case 11:
                m7861c((C0847MM) args[0], ((Integer) args[1]).intValue(), ((Long) args[2]).longValue());
                return null;
            case 12:
                return m7841K((Station) args[0]);
            case 13:
                return m7844M((Station) args[0]);
            case 14:
                m7862c((SellOrder) args[0]);
                return null;
            case 15:
                m7860c((BuyOrder) args[0]);
                return null;
            case 16:
                m7854b((CommercialOrder) args[0]);
                return null;
            case 17:
                return new Long(m7867fj(((Long) args[0]).longValue()));
            case 18:
                return bmy();
            case 19:
                m7866f((ConsignmentFeeManager) args[0]);
                return null;
            case 20:
                return bmA();
            case 21:
                m7868w((ItemType) args[0]);
                return null;
            case 22:
                m7869y((ItemType) args[0]);
                return null;
            case 23:
                m7856bj((Player) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1286bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1286bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1286bN, new Object[0]));
                break;
        }
        return m7852ap();
    }

    @C5566aOg
    /* renamed from: b */
    public BuyOrder mo4369b(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2) {
        switch (bFf().mo6893i(dNQ)) {
            case 0:
                return null;
            case 2:
                return (BuyOrder) bFf().mo5606d(new aCE(this, dNQ, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
            case 3:
                bFf().mo5606d(new aCE(this, dNQ, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
                break;
        }
        return m7846a(mm, jCVar, i, i2, j, j2);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: b */
    public BuyOrder mo4370b(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2, Station bf) {
        switch (bFf().mo6893i(dNR)) {
            case 0:
                return null;
            case 2:
                return (BuyOrder) bFf().mo5606d(new aCE(this, dNR, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), bf}));
            case 3:
                bFf().mo5606d(new aCE(this, dNR, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), bf}));
                break;
        }
        return m7847a(mm, jCVar, i, i2, j, j2, bf);
    }

    @C5566aOg
    /* renamed from: b */
    public SellOrder mo4371b(C0847MM mm, Item auq, int i, long j, long j2) {
        switch (bFf().mo6893i(dNT)) {
            case 0:
                return null;
            case 2:
                return (SellOrder) bFf().mo5606d(new aCE(this, dNT, new Object[]{mm, auq, new Integer(i), new Long(j), new Long(j2)}));
            case 3:
                bFf().mo5606d(new aCE(this, dNT, new Object[]{mm, auq, new Integer(i), new Long(j), new Long(j2)}));
                break;
        }
        return m7848a(mm, auq, i, j, j2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo4372b(C0847MM mm, int i, long j) {
        switch (bFf().mo6893i(dNS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dNS, new Object[]{mm, new Integer(i), new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dNS, new Object[]{mm, new Integer(i), new Long(j)}));
                break;
        }
        m7849a(mm, i, j);
    }

    /* renamed from: bk */
    public void mo4373bk(Player aku) {
        switch (bFf().mo6893i(dOi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOi, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOi, new Object[]{aku}));
                break;
        }
        m7856bj(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Player Item Types (BuyOrder)")
    public List<ItemType> bmB() {
        switch (bFf().mo6893i(dOf)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dOf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dOf, new Object[0]));
                break;
        }
        return bmA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fee manager")
    public ConsignmentFeeManager bmz() {
        switch (bFf().mo6893i(dOd)) {
            case 0:
                return null;
            case 2:
                return (ConsignmentFeeManager) bFf().mo5606d(new aCE(this, dOd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dOd, new Object[0]));
                break;
        }
        return bmy();
    }

    @C5566aOg
    /* renamed from: c */
    public void mo4376c(CommercialOrder aJVar) {
        switch (bFf().mo6893i(dOb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOb, new Object[]{aJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOb, new Object[]{aJVar}));
                break;
        }
        m7854b(aJVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public SellOrder mo4377d(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2) {
        switch (bFf().mo6893i(dNU)) {
            case 0:
                return null;
            case 2:
                return (SellOrder) bFf().mo5606d(new aCE(this, dNU, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
            case 3:
                bFf().mo5606d(new aCE(this, dNU, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
                break;
        }
        return m7858c(mm, jCVar, i, i2, j, j2);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: d */
    public SellOrder mo4378d(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2, Station bf) {
        switch (bFf().mo6893i(dNV)) {
            case 0:
                return null;
            case 2:
                return (SellOrder) bFf().mo5606d(new aCE(this, dNV, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), bf}));
            case 3:
                bFf().mo5606d(new aCE(this, dNV, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), bf}));
                break;
        }
        return m7859c(mm, jCVar, i, i2, j, j2, bf);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo4379d(BuyOrder kl) {
        switch (bFf().mo6893i(dOa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOa, new Object[]{kl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOa, new Object[]{kl}));
                break;
        }
        m7860c(kl);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo4380d(SellOrder awh) {
        switch (bFf().mo6893i(dNZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dNZ, new Object[]{awh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dNZ, new Object[]{awh}));
                break;
        }
        m7862c(awh);
    }

    /* renamed from: fk */
    public long mo4381fk(long j) {
        switch (bFf().mo6893i(dOc)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dOc, new Object[]{new Long(j)}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dOc, new Object[]{new Long(j)}));
                break;
        }
        return m7867fj(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fee manager")
    /* renamed from: g */
    public void mo4382g(ConsignmentFeeManager auVar) {
        switch (bFf().mo6893i(dOe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOe, new Object[]{auVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOe, new Object[]{auVar}));
                break;
        }
        m7866f(auVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1288bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1288bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1288bP, new Object[0]));
                break;
        }
        return m7853ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Player Item Types (BuyOrder)")
    @C5566aOg
    /* renamed from: x */
    public void mo4383x(ItemType jCVar) {
        switch (bFf().mo6893i(dOg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOg, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOg, new Object[]{jCVar}));
                break;
        }
        m7868w(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Player Item Types (BuyOrder)")
    @C5566aOg
    /* renamed from: z */
    public void mo4384z(ItemType jCVar) {
        switch (bFf().mo6893i(dOh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOh, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOh, new Object[]{jCVar}));
                break;
        }
        m7869y(jCVar);
    }

    @C0064Am(aul = "5ff43cda2e8fafad51eeee61ae971f58", aum = 0)
    /* renamed from: ap */
    private UUID m7852ap() {
        return m7851an();
    }

    @C0064Am(aul = "8d3848f3895252d632e65cd08efe3a8b", aum = 0)
    /* renamed from: b */
    private void m7855b(UUID uuid) {
        m7850a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m7850a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "2268daa94218c92904d720a18d9c0f82", aum = 0)
    /* renamed from: ar */
    private String m7853ar() {
        return "market";
    }

    @C0064Am(aul = "5ab81b14ad65dfab8ad425ebc6a1080c", aum = 0)
    /* renamed from: G */
    private ListContainer<BuyOrder> m7839G(Station bf) {
        ListContainer<BuyOrder> zPVar = (ListContainer) bmv().get(bf);
        if (zPVar != null) {
            return zPVar;
        }
        ListContainer<BuyOrder> zPVar2 = (ListContainer) bFf().mo6865M(ListContainer.class);
        zPVar2.mo10S();
        bmv().put(bf, zPVar2);
        return zPVar2;
    }

    @C0064Am(aul = "c709fd2e8fe06bc3d29c56cf2262b4e1", aum = 0)
    /* renamed from: I */
    private ListContainer<SellOrder> m7840I(Station bf) {
        ListContainer<SellOrder> zPVar = (ListContainer) bmw().get(bf);
        if (zPVar != null) {
            return zPVar;
        }
        ListContainer<SellOrder> zPVar2 = (ListContainer) bFf().mo6865M(ListContainer.class);
        zPVar2.mo10S();
        bmw().put(bf, zPVar2);
        return zPVar2;
    }

    @C0064Am(aul = "61ab2f5c6f014856fef4b38f837b808d", aum = 0)
    /* renamed from: a */
    private void m7849a(C0847MM mm, int i, long j) {
        if (i <= 0 || j < 0 || !(mm.bhE() instanceof Station)) {
            throw new C6172aho(C6172aho.C1894a.INVALID_PLAYER_INPUT);
        }
    }

    @C0064Am(aul = "eb4493dc931593ed456596fb8290a5d7", aum = 0)
    /* renamed from: c */
    private void m7861c(C0847MM mm, int i, long j) {
        if (i <= 0 || j < 0 || !(mm.bhE() instanceof Station)) {
            throw new C6172aho(C6172aho.C1894a.INVALID_PLAYER_INPUT);
        }
    }

    @C0064Am(aul = "6212b22d7ad593023e88b3ac16d241d5", aum = 0)
    /* renamed from: fj */
    private long m7867fj(long j) {
        if (bmx() != null) {
            return bmx().mo16286fk(j);
        }
        throw new IllegalStateException("Market has no fee manager");
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fee manager")
    @C0064Am(aul = "11b581bd837ea91999f3faf9758304be", aum = 0)
    private ConsignmentFeeManager bmy() {
        return bmx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fee manager")
    @C0064Am(aul = "a8c5b07943068faa35b38d91cb6e078e", aum = 0)
    /* renamed from: f */
    private void m7866f(ConsignmentFeeManager auVar) {
        m7865e(auVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Player Item Types (BuyOrder)")
    @C0064Am(aul = "bcbd4b9e764758b9e7c51d42ac221b2d", aum = 0)
    private List<ItemType> bmA() {
        return Collections.unmodifiableList(bmu());
    }

    @C0064Am(aul = "a01098b3aba5f4c2411933025ae16013", aum = 0)
    /* renamed from: bj */
    private void m7856bj(Player aku) {
        ArrayList<BuyOrder> arrayList = new ArrayList<>();
        ArrayList<SellOrder> arrayList2 = new ArrayList<>();
        for (ListContainer list : bmv().values()) {
            for (BuyOrder kl : list.getList()) {
                if (kl.abg() == aku) {
                    arrayList.add(kl);
                }
            }
        }
        for (ListContainer list2 : bmw().values()) {
            for (SellOrder awh : list2.getList()) {
                if (awh.apa() == aku) {
                    arrayList2.add(awh);
                }
            }
        }
        for (BuyOrder eN : arrayList) {
            eN.mo3667eN();
        }
        for (SellOrder eN2 : arrayList2) {
            eN2.mo3667eN();
        }
    }
}
