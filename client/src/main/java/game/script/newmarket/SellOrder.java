package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.script.item.*;
import game.script.player.Player;
import game.script.ship.ShipType;
import game.script.ship.Station;
import game.script.storage.Storage;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5746aVe;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.aWh */
/* compiled from: a */
public class SellOrder extends CommercialOrder implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bIq = null;
    public static final C2491fm bIs = null;
    /* renamed from: gN */
    public static final C2491fm f4008gN = null;
    public static final C2491fm jft = null;
    public static final C2491fm jfu = null;
    /* renamed from: lD */
    public static final C2491fm f4009lD = null;
    /* renamed from: li */
    public static final C2491fm f4010li = null;
    /* renamed from: ly */
    public static final C2491fm f4011ly = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a17063d59eb2f60df581ccd3bea5a7b7", aum = 0)

    /* renamed from: zF */
    private static C0847MM f4012zF;

    static {
        m19251V();
    }

    public SellOrder() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SellOrder(C0847MM mm, ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
        super((C5540aNg) null);
        super._m_script_init(mm, jCVar, i, i2, j, bf, j2);
    }

    public SellOrder(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19251V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CommercialOrder._m_fieldCount + 1;
        _m_methodCount = CommercialOrder._m_methodCount + 8;
        int i = CommercialOrder._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(SellOrder.class, "a17063d59eb2f60df581ccd3bea5a7b7", i);
        bIq = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CommercialOrder._m_fields, (Object[]) _m_fields);
        int i3 = CommercialOrder._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 8)];
        C2491fm a = C4105zY.m41624a(SellOrder.class, "e0ae1b3a85a34611f1b57864d36bbbeb", i3);
        f4011ly = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(SellOrder.class, "bc4b5d5e6b92735af5319a39d9e1fda3", i4);
        jft = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(SellOrder.class, "f7702070ee1865ed88f292768ca46860", i5);
        f4010li = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(SellOrder.class, "18d08e23047d2ac9ca8631147707cb5e", i6);
        jfu = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(SellOrder.class, "ba7a9556b9d01e19ae636c2a84baaf41", i7);
        f4008gN = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(SellOrder.class, "2c87857c221564f716ce8599e05b1d00", i8);
        _f_dispose_0020_0028_0029V = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(SellOrder.class, "cf9f800fbc1d681d5b95ad0dfb4bdf73", i9);
        bIs = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(SellOrder.class, "c7d31d57578320fd32f144f94b2c93ba", i10);
        f4009lD = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CommercialOrder._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SellOrder.class, C5746aVe.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "18d08e23047d2ac9ca8631147707cb5e", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m19252a(C0847MM mm, int i, C0314EF ef) {
        throw new aWi(new aCE(this, jfu, new Object[]{mm, new Integer(i), ef}));
    }

    @C0064Am(aul = "ba7a9556b9d01e19ae636c2a84baaf41", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m19253a(BulkItemType amh, int i, ItemLocation aag) {
        throw new aWi(new aCE(this, f4008gN, new Object[]{amh, new Integer(i), aag}));
    }

    private C0847MM aoW() {
        return (C0847MM) bFf().mo5608dq().mo3214p(bIq);
    }

    /* renamed from: b */
    private void m19254b(C0847MM mm) {
        bFf().mo5608dq().mo3197f(bIq, mm);
    }

    @C5566aOg
    /* renamed from: b */
    private void m19255b(BulkItemType amh, int i, ItemLocation aag) {
        switch (bFf().mo6893i(f4008gN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4008gN, new Object[]{amh, new Integer(i), aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4008gN, new Object[]{amh, new Integer(i), aag}));
                break;
        }
        m19253a(amh, i, aag);
    }

    private boolean dDv() {
        switch (bFf().mo6893i(jft)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, jft, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, jft, new Object[0]));
                break;
        }
        return dDu();
    }

    @C0064Am(aul = "e0ae1b3a85a34611f1b57864d36bbbeb", aum = 0)
    @C5566aOg
    /* renamed from: fl */
    private void m19258fl() {
        throw new aWi(new aCE(this, f4011ly, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5746aVe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CommercialOrder._m_methodCount) {
            case 0:
                m19258fl();
                return null;
            case 1:
                return new Boolean(dDu());
            case 2:
                m19256eM();
                return null;
            case 3:
                m19252a((C0847MM) args[0], ((Integer) args[1]).intValue(), (C0314EF) args[2]);
                return null;
            case 4:
                m19253a((BulkItemType) args[0], ((Integer) args[1]).intValue(), (ItemLocation) args[2]);
                return null;
            case 5:
                m19257fg();
                return null;
            case 6:
                return aoZ();
            case 7:
                return m19259fp();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C0847MM apa() {
        switch (bFf().mo6893i(bIs)) {
            case 0:
                return null;
            case 2:
                return (C0847MM) bFf().mo5606d(new aCE(this, bIs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bIs, new Object[0]));
                break;
        }
        return aoZ();
    }

    @C5566aOg
    /* renamed from: b */
    public void mo12022b(C0847MM mm, int i, C0314EF ef) {
        switch (bFf().mo6893i(jfu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jfu, new Object[]{mm, new Integer(i), ef}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jfu, new Object[]{mm, new Integer(i), ef}));
                break;
        }
        m19252a(mm, i, ef);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m19257fg();
    }

    /* access modifiers changed from: protected */
    /* renamed from: eN */
    public void mo3667eN() {
        switch (bFf().mo6893i(f4010li)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4010li, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4010li, new Object[0]));
                break;
        }
        m19256eM();
    }

    @C5566aOg
    /* renamed from: fm */
    public void mo3668fm() {
        switch (bFf().mo6893i(f4011ly)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4011ly, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4011ly, new Object[0]));
                break;
        }
        m19258fl();
    }

    /* renamed from: fq */
    public CommercialOrderInfo mo3669fq() {
        switch (bFf().mo6893i(f4009lD)) {
            case 0:
                return null;
            case 2:
                return (CommercialOrderInfo) bFf().mo5606d(new aCE(this, f4009lD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4009lD, new Object[0]));
                break;
        }
        return m19259fp();
    }

    /* renamed from: a */
    public void mo12020a(C0847MM mm, ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
        super.mo9434a(jCVar, i, i2, j, bf, j2);
        m19254b(mm);
    }

    @C0064Am(aul = "bc4b5d5e6b92735af5319a39d9e1fda3", aum = 0)
    private boolean dDu() {
        if (!(aoW() instanceof Player)) {
            return false;
        }
        Player aku = (Player) aoW();
        int i = 2;
        if (aku.bhE() == mo9439eT()) {
            i = 1;
        }
        if (aku.mo14346aD(mo9439eT()).mo20060Lc() >= i) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "f7702070ee1865ed88f292768ca46860", aum = 0)
    /* renamed from: eM */
    private void m19256eM() {
        ItemLocation aag;
        if (!isDisposed()) {
            if (mo9444ff() > 0 && (aoW() instanceof Player) && !((Player) aoW()).isDisposed()) {
                Player aku = (Player) aoW();
                if (mo9437eP() instanceof BulkItemType) {
                    BulkItem atv = (BulkItem) mo9437eP().mo7459NK();
                    atv.mo11564db(mo9444ff());
                    Storage am = mo9439eT().mo602am(aku);
                    if (am != null) {
                        am.mo7615N(atv);
                    }
                } else {
                    ItemLocation am2 = mo9439eT().mo602am(aku);
                    if (!(mo9437eP() instanceof ShipType) || !dDv()) {
                        aag = am2;
                    } else {
                        aag = aku.mo14346aD(mo9439eT()).mo20057KW();
                    }
                    if (aag != null) {
                        int i = 0;
                        while (true) {
                            int i2 = i;
                            if (i2 >= mo9444ff()) {
                                break;
                            }
                            aag.mo7615N((Item) mo9437eP().mo7459NK());
                            i = i2 + 1;
                        }
                    }
                }
            }
            mo9445fi().mo4380d(this);
            aoW().mo3918a(this);
            dispose();
        }
    }

    @C0064Am(aul = "2c87857c221564f716ce8599e05b1d00", aum = 0)
    /* renamed from: fg */
    private void m19257fg() {
        super.dispose();
        m19254b((C0847MM) null);
    }

    @C0064Am(aul = "cf9f800fbc1d681d5b95ad0dfb4bdf73", aum = 0)
    private C0847MM aoZ() {
        return aoW();
    }

    @C0064Am(aul = "c7d31d57578320fd32f144f94b2c93ba", aum = 0)
    /* renamed from: fp */
    private CommercialOrderInfo m19259fp() {
        SellOrderInfo xTVar = (SellOrderInfo) bFf().mo6865M(SellOrderInfo.class);
        xTVar.mo22924b(this);
        return xTVar;
    }
}
