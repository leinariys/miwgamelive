package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.mbean.C0619Im;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.zP */
/* compiled from: a */
public class ListContainer<T> extends aDJ implements C3161oY.C3162a, aOW, C3161oY.C3162a {

    /* renamed from: Do */
    public static final C2491fm f9655Do = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f9656x13860637 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cbj = null;
    public static final C2491fm cbk = null;
    public static final C2491fm cbl = null;
    public static final C2491fm cbm = null;
    public static final C2491fm cbn = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zh */
    public static final C5663aRz f9658zh = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c08f3bb4b9e909da50adc8242bf4b771", aum = 0)

    /* renamed from: zg */
    private static C3438ra<T> f9657zg;

    static {
        m41545V();
    }

    public ListContainer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ListContainer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41545V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 7;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ListContainer.class, "c08f3bb4b9e909da50adc8242bf4b771", i);
        f9658zh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
        C2491fm a = C4105zY.m41624a(ListContainer.class, "18e72a91492b38c630d2828f6c4fb0d2", i3);
        cbj = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ListContainer.class, "cbf8cf7b34d5169e0d521e436e64e070", i4);
        cbk = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ListContainer.class, "d520ae92204a9b61237d6aecee54831a", i5);
        cbl = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ListContainer.class, "61d9b11cb0b4e07382ddb7de2aa2c2c4", i6);
        cbm = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(ListContainer.class, "113601f20eec09a23a0e9f1692ecdab3", i7);
        f9656x13860637 = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(ListContainer.class, "8ebffbba63ae8ddcb5b4605b810a9366", i8);
        f9655Do = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(ListContainer.class, "af22ab5f8e4e881a8afb7098a5debd70", i9);
        cbn = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ListContainer.class, C0619Im.class, _m_fields, _m_methods);
    }

    /* renamed from: jP */
    private C3438ra m41548jP() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f9658zh);
    }

    /* renamed from: p */
    private void m41549p(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f9658zh, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0619Im(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return asB();
            case 1:
                m41543I(args[0]);
                return null;
            case 2:
                return new Boolean(m41544J(args[0]));
            case 3:
                asC();
                return null;
            case 4:
                m41547b((aDJ) args[0]);
                return null;
            case 5:
                m41546a((C0665JT) args[0]);
                return null;
            case 6:
                asD();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void add(T t) {
        switch (bFf().mo6893i(cbk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cbk, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cbk, new Object[]{t}));
                break;
        }
        m41543I(t);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9655Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9655Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9655Do, new Object[]{jt}));
                break;
        }
        m41546a(jt);
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f9656x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9656x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9656x13860637, new Object[]{adj}));
                break;
        }
        m41547b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void clear() {
        switch (bFf().mo6893i(cbm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cbm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cbm, new Object[0]));
                break;
        }
        asC();
    }

    public List<T> getList() {
        switch (bFf().mo6893i(cbj)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cbj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cbj, new Object[0]));
                break;
        }
        return asB();
    }

    public boolean remove(T t) {
        switch (bFf().mo6893i(cbl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cbl, new Object[]{t}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cbl, new Object[]{t}));
                break;
        }
        return m41544J(t);
    }

    public void removeListeners() {
        switch (bFf().mo6893i(cbn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cbn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cbn, new Object[0]));
                break;
        }
        asD();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "18e72a91492b38c630d2828f6c4fb0d2", aum = 0)
    private List<T> asB() {
        return Collections.unmodifiableList(m41548jP());
    }

    @C0064Am(aul = "cbf8cf7b34d5169e0d521e436e64e070", aum = 0)
    /* renamed from: I */
    private void m41543I(T t) {
        m41548jP().add(t);
    }

    @C0064Am(aul = "d520ae92204a9b61237d6aecee54831a", aum = 0)
    /* renamed from: J */
    private boolean m41544J(T t) {
        return m41548jP().remove(t);
    }

    @C0064Am(aul = "61d9b11cb0b4e07382ddb7de2aa2c2c4", aum = 0)
    private void asC() {
        m41548jP().clear();
    }

    @C0064Am(aul = "113601f20eec09a23a0e9f1692ecdab3", aum = 0)
    /* renamed from: b */
    private void m41547b(aDJ adj) {
        m41548jP().remove(adj);
    }

    @C0064Am(aul = "8ebffbba63ae8ddcb5b4605b810a9366", aum = 0)
    /* renamed from: a */
    private void m41546a(C0665JT jt) {
        if (jt.mo3117j(1, 0, 0)) {
            ArrayList arrayList = new ArrayList();
            for (Object next : m41548jP()) {
                if (next instanceof aDJ) {
                    aDJ adj = (aDJ) next;
                    if (adj.isDisposed()) {
                        arrayList.add(next);
                    } else {
                        adj.mo8348d(C3161oY.C3162a.class, this);
                    }
                }
            }
            for (Object next2 : arrayList) {
                mo8359ma("Removing disposed object " + next2);
                m41548jP().remove(next2);
            }
        }
    }

    @C0064Am(aul = "af22ab5f8e4e881a8afb7098a5debd70", aum = 0)
    private void asD() {
        for (Object next : m41548jP()) {
            if (next instanceof aDJ) {
                ((aDJ) next).mo8355h(C3161oY.C3162a.class, this);
            }
        }
    }
}
