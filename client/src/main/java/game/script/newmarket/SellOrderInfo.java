package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C5512aMe;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C2707ip;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.1")
@C5511aMd
@C6485anp
/* renamed from: a.xT */
/* compiled from: a */
public class SellOrderInfo extends CommercialOrderInfo implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f9542Do = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bIp = null;
    public static final C5663aRz bIq = null;
    public static final C2491fm bIr = null;
    public static final C2491fm bIs = null;
    public static final C2491fm bfH = null;
    public static final C2491fm bfI = null;
    public static final C2491fm bfJ = null;
    /* renamed from: gQ */
    public static final C2491fm f9544gQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a92bc5ddcd6046e881fccfc64dc2ba9e", aum = 0)
    @C5566aOg

    /* renamed from: XQ */
    private static SellOrder f9543XQ;
    @C0064Am(aul = "45a652b70437426ca914093c366ed674", aum = 1)

    /* renamed from: zF */
    private static C0847MM f9545zF;

    static {
        m40985V();
    }

    public SellOrderInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SellOrderInfo(C5540aNg ang) {
        super(ang);
    }

    public SellOrderInfo(SellOrder awh) {
        super((C5540aNg) null);
        super._m_script_init(awh);
    }

    /* renamed from: V */
    static void m40985V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CommercialOrderInfo._m_fieldCount + 2;
        _m_methodCount = CommercialOrderInfo._m_methodCount + 8;
        int i = CommercialOrderInfo._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(SellOrderInfo.class, "a92bc5ddcd6046e881fccfc64dc2ba9e", i);
        bIp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SellOrderInfo.class, "45a652b70437426ca914093c366ed674", i2);
        bIq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CommercialOrderInfo._m_fields, (Object[]) _m_fields);
        int i4 = CommercialOrderInfo._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 8)];
        C2491fm a = C4105zY.m41624a(SellOrderInfo.class, "ccc7743040cbdac7eb3ec158b8b435b0", i4);
        bIr = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(SellOrderInfo.class, "7b631d6d6147c519120218550cb50baa", i5);
        bIs = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(SellOrderInfo.class, "e248d5247486e8a34db236fcee47d457", i6);
        _f_dispose_0020_0028_0029V = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(SellOrderInfo.class, "0d924875699da2bfcf13d4e2662cd7ab", i7);
        bfH = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(SellOrderInfo.class, "be1555c1202e32ad0bbd75f042b197b8", i8);
        f9542Do = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(SellOrderInfo.class, "20aadc1374aa1ef18f6a312ab927febd", i9);
        bfI = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(SellOrderInfo.class, "23b35f3255c285d8e041aeb304ecb0ef", i10);
        f9544gQ = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(SellOrderInfo.class, "bc5b0a31282469e30474c8357ab93794", i11);
        bfJ = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CommercialOrderInfo._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SellOrderInfo.class, C2707ip.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m40987a(SellOrder awh) {
        bFf().mo5608dq().mo3197f(bIp, awh);
    }

    @C0064Am(aul = "bc5b0a31282469e30474c8357ab93794", aum = 0)
    @C5566aOg
    private void abl() {
        throw new aWi(new aCE(this, bfJ, new Object[0]));
    }

    private SellOrder aoV() {
        return (SellOrder) bFf().mo5608dq().mo3214p(bIp);
    }

    private C0847MM aoW() {
        return (C0847MM) bFf().mo5608dq().mo3214p(bIq);
    }

    /* renamed from: b */
    private void m40988b(C0847MM mm) {
        bFf().mo5608dq().mo3197f(bIq, mm);
    }

    @C0064Am(aul = "23b35f3255c285d8e041aeb304ecb0ef", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m40989dd() {
        throw new aWi(new aCE(this, f9544gQ, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2707ip(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CommercialOrderInfo._m_methodCount) {
            case 0:
                return aoX();
            case 1:
                return aoZ();
            case 2:
                m40990fg();
                return null;
            case 3:
                return abh();
            case 4:
                m40986a((C0665JT) args[0]);
                return null;
            case 5:
                abj();
                return null;
            case 6:
                m40989dd();
                return null;
            case 7:
                abl();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C0847MM abi() {
        switch (bFf().mo6893i(bfH)) {
            case 0:
                return null;
            case 2:
                return (C0847MM) bFf().mo5606d(new aCE(this, bfH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfH, new Object[0]));
                break;
        }
        return abh();
    }

    public void abk() {
        switch (bFf().mo6893i(bfI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfI, new Object[0]));
                break;
        }
        abj();
    }

    @C5566aOg
    public void abm() {
        switch (bFf().mo6893i(bfJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfJ, new Object[0]));
                break;
        }
        abl();
    }

    public SellOrder aoY() {
        switch (bFf().mo6893i(bIr)) {
            case 0:
                return null;
            case 2:
                return (SellOrder) bFf().mo5606d(new aCE(this, bIr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bIr, new Object[0]));
                break;
        }
        return aoX();
    }

    public C0847MM apa() {
        switch (bFf().mo6893i(bIs)) {
            case 0:
                return null;
            case 2:
                return (C0847MM) bFf().mo5606d(new aCE(this, bIs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bIs, new Object[0]));
                break;
        }
        return aoZ();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9542Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9542Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9542Do, new Object[]{jt}));
                break;
        }
        m40986a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: de */
    public void mo22925de() {
        switch (bFf().mo6893i(f9544gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9544gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9544gQ, new Object[0]));
                break;
        }
        m40989dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m40990fg();
    }

    /* renamed from: b */
    public void mo22924b(SellOrder awh) {
        super.mo8624f((CommercialOrder) awh);
        m40987a(awh);
        m40988b(awh.apa());
    }

    @C0064Am(aul = "ccc7743040cbdac7eb3ec158b8b435b0", aum = 0)
    private SellOrder aoX() {
        return aoV();
    }

    @C0064Am(aul = "7b631d6d6147c519120218550cb50baa", aum = 0)
    private C0847MM aoZ() {
        return aoW();
    }

    @C0064Am(aul = "e248d5247486e8a34db236fcee47d457", aum = 0)
    /* renamed from: fg */
    private void m40990fg() {
        super.dispose();
        aoV().mo9448g(this);
        m40987a((SellOrder) null);
        m40988b((C0847MM) null);
    }

    @C0064Am(aul = "0d924875699da2bfcf13d4e2662cd7ab", aum = 0)
    private C0847MM abh() {
        return apa();
    }

    @C0064Am(aul = "be1555c1202e32ad0bbd75f042b197b8", aum = 0)
    /* renamed from: a */
    private void m40986a(C0665JT jt) {
        if (!jt.mo3117j(1, 0, 1)) {
            return;
        }
        if (((C5512aMe) jt.get("SellOrder")) != null) {
            m40987a((SellOrder) ((C5512aMe) jt.get("SellOrder")).mo10098yz());
        } else if (jt.get("sellOrder") == null) {
            mo8358lY("On Load version failed to recover sell order info.");
        }
    }

    @C0064Am(aul = "20aadc1374aa1ef18f6a312ab927febd", aum = 0)
    private void abj() {
        m40987a((SellOrder) null);
        if (aoW() instanceof Player) {
            m40988b((C0847MM) null);
        }
    }
}
