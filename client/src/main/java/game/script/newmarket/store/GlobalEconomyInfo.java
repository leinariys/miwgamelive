package game.script.newmarket.store;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C6275ajn;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.crafting.CraftingRecipe;
import game.script.item.BaseItemType;
import game.script.item.BlueprintType;
import game.script.item.IngredientsCategory;
import game.script.item.ItemType;
import game.script.newmarket.BuyOrder;
import game.script.newmarket.ListContainer;
import game.script.newmarket.SellOrder;
import game.script.ship.Station;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.mbean.C6104agY;
import logic.data.mbean.C7000ayn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@C5566aOg
@C6485anp
@C5511aMd
/* renamed from: a.mK */
/* compiled from: a */
public class GlobalEconomyInfo extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aDU = null;
    public static final C5663aRz aDW = null;
    public static final C5663aRz aDY = null;
    public static final C5663aRz aEA = null;
    public static final C5663aRz aEC = null;
    public static final C5663aRz aEE = null;
    public static final C5663aRz aEG = null;
    public static final C5663aRz aEI = null;
    public static final C2491fm aEJ = null;
    public static final C2491fm aEK = null;
    public static final C2491fm aEL = null;
    public static final C2491fm aEM = null;
    public static final C2491fm aEN = null;
    public static final C2491fm aEO = null;
    public static final C2491fm aEP = null;
    public static final C2491fm aEQ = null;
    public static final C2491fm aER = null;
    public static final C2491fm aES = null;
    public static final C2491fm aET = null;
    public static final C2491fm aEU = null;
    public static final C2491fm aEV = null;
    public static final C2491fm aEW = null;
    public static final C2491fm aEX = null;
    public static final C2491fm aEY = null;
    public static final C2491fm aEZ = null;
    public static final C5663aRz aEa = null;
    public static final C5663aRz aEc = null;
    public static final C5663aRz aEe = null;
    public static final C5663aRz aEg = null;
    public static final C5663aRz aEi = null;
    public static final C5663aRz aEk = null;
    public static final C5663aRz aEm = null;
    public static final C5663aRz aEo = null;
    public static final C5663aRz aEq = null;
    public static final C5663aRz aEs = null;
    public static final C5663aRz aEu = null;
    public static final C5663aRz aEw = null;
    public static final C5663aRz aEy = null;
    public static final C2491fm aFA = null;
    public static final C2491fm aFB = null;
    public static final C2491fm aFC = null;
    public static final C2491fm aFD = null;
    public static final C2491fm aFE = null;
    public static final C2491fm aFF = null;
    public static final C2491fm aFa = null;
    public static final C2491fm aFb = null;
    public static final C2491fm aFc = null;
    public static final C2491fm aFd = null;
    public static final C2491fm aFe = null;
    public static final C2491fm aFf = null;
    public static final C2491fm aFg = null;
    public static final C2491fm aFh = null;
    public static final C2491fm aFi = null;
    public static final C2491fm aFj = null;
    public static final C2491fm aFk = null;
    public static final C2491fm aFl = null;
    public static final C2491fm aFm = null;
    public static final C2491fm aFn = null;
    public static final C2491fm aFo = null;
    public static final C2491fm aFp = null;
    public static final C2491fm aFq = null;
    public static final C2491fm aFr = null;
    public static final C2491fm aFs = null;
    public static final C2491fm aFt = null;
    public static final C2491fm aFu = null;
    public static final C2491fm aFv = null;
    public static final C2491fm aFw = null;
    public static final C2491fm aFx = null;
    public static final C2491fm aFy = null;
    public static final C2491fm aFz = null;
    /* renamed from: bL */
    public static final C5663aRz f8642bL = null;
    /* renamed from: bN */
    public static final C2491fm f8643bN = null;
    /* renamed from: bO */
    public static final C2491fm f8644bO = null;
    /* renamed from: bP */
    public static final C2491fm f8645bP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "79daf6ae744bd6ca7a6a57144ee6f83c", aum = 0)
    private static float aDT;
    @C0064Am(aul = "cbeacf9fb206f8a618ec42b73872a32b", aum = 1)
    private static float aDV;
    @C0064Am(aul = "663daa90d85748ba745a32faa7e36cd5", aum = 2)
    private static float aDX;
    @C0064Am(aul = "c774da56a2f63ff8f1d338d7dafed0ae", aum = 3)
    private static float aDZ;
    @C0064Am(aul = "213e3f82281c83584d7cf2f901a095cf", aum = 17)
    private static float aEB;
    @C0064Am(aul = "0268e5f2b0060bf9be3ea1d099d05b6e", aum = 18)
    private static float aED;
    @C0064Am(aul = "6716e9298a752325203f0297bb3d6bb4", aum = 19)
    private static float aEF;
    @C0064Am(aul = "29107569333344597a5a09145b603f43", aum = 21)
    private static C1556Wo<ItemType, TransactionLog> aEH;
    @C0064Am(aul = "9b9b07fad2600b103cb44d25ef8cc86d", aum = 4)
    private static float aEb;
    @C0064Am(aul = "d457710da5e8c900091919a8358ed815", aum = 5)
    private static float aEd;
    @C0064Am(aul = "5badc547988bbd5a227d9ccafe7100af", aum = 6)
    private static float aEf;
    @C0064Am(aul = "510272fd151be90d93179e35304deac8", aum = 7)
    private static float aEh;
    @C0064Am(aul = "3c41dc5101e4814b42602b23ab3ee203", aum = 8)
    private static float aEj;
    @C0064Am(aul = "0dd77f576ba1c5e2680b8879ad0a0832", aum = 9)
    private static float aEl;
    @C0064Am(aul = "2d5d4e0271de6927747f341497311b60", aum = 10)
    private static float aEn;
    @C0064Am(aul = "919249f8421ea0efba7d10b8166f5ecc", aum = 11)
    private static float aEp;
    @C0064Am(aul = "39f7dccc234a838fca418447cb38daaa", aum = 12)
    private static float aEr;
    @C0064Am(aul = "b51922909e5c2a000ead3dd7bc378562", aum = 13)
    private static float aEt;
    @C0064Am(aul = "e401d08f4b497961d1340e0b92d7537b", aum = 14)
    private static float aEv;
    @C0064Am(aul = "371fb0e6d7369806820bd9ff5caa4a32", aum = 15)
    private static float aEx;
    @C0064Am(aul = "5d941ee7de60f357d1d39231385ae928", aum = 16)
    private static float aEz;
    @C0064Am(aul = "e0974087d526452441a3f9e916700a4a", aum = 20)

    /* renamed from: bK */
    private static UUID f8641bK;

    static {
        m35543V();
    }

    public GlobalEconomyInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GlobalEconomyInfo(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35543V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 22;
        _m_methodCount = TaikodomObject._m_methodCount + 52;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 22)];
        C5663aRz b = C5640aRc.m17844b(GlobalEconomyInfo.class, "79daf6ae744bd6ca7a6a57144ee6f83c", i);
        aDU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(GlobalEconomyInfo.class, "cbeacf9fb206f8a618ec42b73872a32b", i2);
        aDW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(GlobalEconomyInfo.class, "663daa90d85748ba745a32faa7e36cd5", i3);
        aDY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(GlobalEconomyInfo.class, "c774da56a2f63ff8f1d338d7dafed0ae", i4);
        aEa = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(GlobalEconomyInfo.class, "9b9b07fad2600b103cb44d25ef8cc86d", i5);
        aEc = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(GlobalEconomyInfo.class, "d457710da5e8c900091919a8358ed815", i6);
        aEe = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(GlobalEconomyInfo.class, "5badc547988bbd5a227d9ccafe7100af", i7);
        aEg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(GlobalEconomyInfo.class, "510272fd151be90d93179e35304deac8", i8);
        aEi = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(GlobalEconomyInfo.class, "3c41dc5101e4814b42602b23ab3ee203", i9);
        aEk = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(GlobalEconomyInfo.class, "0dd77f576ba1c5e2680b8879ad0a0832", i10);
        aEm = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(GlobalEconomyInfo.class, "2d5d4e0271de6927747f341497311b60", i11);
        aEo = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(GlobalEconomyInfo.class, "919249f8421ea0efba7d10b8166f5ecc", i12);
        aEq = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(GlobalEconomyInfo.class, "39f7dccc234a838fca418447cb38daaa", i13);
        aEs = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(GlobalEconomyInfo.class, "b51922909e5c2a000ead3dd7bc378562", i14);
        aEu = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(GlobalEconomyInfo.class, "e401d08f4b497961d1340e0b92d7537b", i15);
        aEw = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(GlobalEconomyInfo.class, "371fb0e6d7369806820bd9ff5caa4a32", i16);
        aEy = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(GlobalEconomyInfo.class, "5d941ee7de60f357d1d39231385ae928", i17);
        aEA = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(GlobalEconomyInfo.class, "213e3f82281c83584d7cf2f901a095cf", i18);
        aEC = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(GlobalEconomyInfo.class, "0268e5f2b0060bf9be3ea1d099d05b6e", i19);
        aEE = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(GlobalEconomyInfo.class, "6716e9298a752325203f0297bb3d6bb4", i20);
        aEG = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(GlobalEconomyInfo.class, "e0974087d526452441a3f9e916700a4a", i21);
        f8642bL = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(GlobalEconomyInfo.class, "29107569333344597a5a09145b603f43", i22);
        aEI = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i24 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i24 + 52)];
        C2491fm a = C4105zY.m41624a(GlobalEconomyInfo.class, "368ff267ccc9f7997c182ac8f486a61d", i24);
        f8643bN = a;
        fmVarArr[i24] = a;
        int i25 = i24 + 1;
        C2491fm a2 = C4105zY.m41624a(GlobalEconomyInfo.class, "006b4f1285453d02d46459b77fd20e29", i25);
        f8644bO = a2;
        fmVarArr[i25] = a2;
        int i26 = i25 + 1;
        C2491fm a3 = C4105zY.m41624a(GlobalEconomyInfo.class, "f2b88e383591c01132b0166e7560b884", i26);
        f8645bP = a3;
        fmVarArr[i26] = a3;
        int i27 = i26 + 1;
        C2491fm a4 = C4105zY.m41624a(GlobalEconomyInfo.class, "46a2e7d5934b3a8dc4d5ff091ff9065e", i27);
        aEJ = a4;
        fmVarArr[i27] = a4;
        int i28 = i27 + 1;
        C2491fm a5 = C4105zY.m41624a(GlobalEconomyInfo.class, "6780760eea23c858e564317692810239", i28);
        aEK = a5;
        fmVarArr[i28] = a5;
        int i29 = i28 + 1;
        C2491fm a6 = C4105zY.m41624a(GlobalEconomyInfo.class, "3e541737c75fff3d1c2527584b0cadc4", i29);
        aEL = a6;
        fmVarArr[i29] = a6;
        int i30 = i29 + 1;
        C2491fm a7 = C4105zY.m41624a(GlobalEconomyInfo.class, "59c84b4d057573532ff69882bca306e1", i30);
        aEM = a7;
        fmVarArr[i30] = a7;
        int i31 = i30 + 1;
        C2491fm a8 = C4105zY.m41624a(GlobalEconomyInfo.class, "7e10656ffbd8b1e2bc8a9be962e0b8d7", i31);
        aEN = a8;
        fmVarArr[i31] = a8;
        int i32 = i31 + 1;
        C2491fm a9 = C4105zY.m41624a(GlobalEconomyInfo.class, "7acd6d145d3c406aa27f2a505fb2927b", i32);
        aEO = a9;
        fmVarArr[i32] = a9;
        int i33 = i32 + 1;
        C2491fm a10 = C4105zY.m41624a(GlobalEconomyInfo.class, "5b1c19a5fe3ba45d5bff9d5ff0f7e392", i33);
        aEP = a10;
        fmVarArr[i33] = a10;
        int i34 = i33 + 1;
        C2491fm a11 = C4105zY.m41624a(GlobalEconomyInfo.class, "9ad2b94667840557821ce2c9df4b1099", i34);
        aEQ = a11;
        fmVarArr[i34] = a11;
        int i35 = i34 + 1;
        C2491fm a12 = C4105zY.m41624a(GlobalEconomyInfo.class, "0f1299558317de3cda6502d389e5c3d1", i35);
        aER = a12;
        fmVarArr[i35] = a12;
        int i36 = i35 + 1;
        C2491fm a13 = C4105zY.m41624a(GlobalEconomyInfo.class, "d7f7d43fa9afd53cdb939e088fa16391", i36);
        aES = a13;
        fmVarArr[i36] = a13;
        int i37 = i36 + 1;
        C2491fm a14 = C4105zY.m41624a(GlobalEconomyInfo.class, "7cb3672899a06f8b6f7d583d83871283", i37);
        aET = a14;
        fmVarArr[i37] = a14;
        int i38 = i37 + 1;
        C2491fm a15 = C4105zY.m41624a(GlobalEconomyInfo.class, "bf415aaae3d2b6c968fbf6fde078ca66", i38);
        aEU = a15;
        fmVarArr[i38] = a15;
        int i39 = i38 + 1;
        C2491fm a16 = C4105zY.m41624a(GlobalEconomyInfo.class, "3b3366ce5aba7a403cacd5f71ae989f8", i39);
        aEV = a16;
        fmVarArr[i39] = a16;
        int i40 = i39 + 1;
        C2491fm a17 = C4105zY.m41624a(GlobalEconomyInfo.class, "23758569560557e7c804df6289ce7a1f", i40);
        aEW = a17;
        fmVarArr[i40] = a17;
        int i41 = i40 + 1;
        C2491fm a18 = C4105zY.m41624a(GlobalEconomyInfo.class, "3ae55b41d8752b104986755cbf342f54", i41);
        aEX = a18;
        fmVarArr[i41] = a18;
        int i42 = i41 + 1;
        C2491fm a19 = C4105zY.m41624a(GlobalEconomyInfo.class, "97215d43c9bc9cccfefaa61f35293dac", i42);
        aEY = a19;
        fmVarArr[i42] = a19;
        int i43 = i42 + 1;
        C2491fm a20 = C4105zY.m41624a(GlobalEconomyInfo.class, "cae389845529737bfd92cbd02c3d97ef", i43);
        aEZ = a20;
        fmVarArr[i43] = a20;
        int i44 = i43 + 1;
        C2491fm a21 = C4105zY.m41624a(GlobalEconomyInfo.class, "13b51e118253af1273338948bf88d433", i44);
        aFa = a21;
        fmVarArr[i44] = a21;
        int i45 = i44 + 1;
        C2491fm a22 = C4105zY.m41624a(GlobalEconomyInfo.class, "fcdea0011e9d501588423eb0c7850c9f", i45);
        aFb = a22;
        fmVarArr[i45] = a22;
        int i46 = i45 + 1;
        C2491fm a23 = C4105zY.m41624a(GlobalEconomyInfo.class, "242027e23ce972eb391e9574887d6f5d", i46);
        aFc = a23;
        fmVarArr[i46] = a23;
        int i47 = i46 + 1;
        C2491fm a24 = C4105zY.m41624a(GlobalEconomyInfo.class, "32be396f56b129a7c3ffbe22f87bcf8b", i47);
        aFd = a24;
        fmVarArr[i47] = a24;
        int i48 = i47 + 1;
        C2491fm a25 = C4105zY.m41624a(GlobalEconomyInfo.class, "354eeaea44d0a1b1773add5f1d057fae", i48);
        aFe = a25;
        fmVarArr[i48] = a25;
        int i49 = i48 + 1;
        C2491fm a26 = C4105zY.m41624a(GlobalEconomyInfo.class, "8f3db251498360b522881638ef24042c", i49);
        aFf = a26;
        fmVarArr[i49] = a26;
        int i50 = i49 + 1;
        C2491fm a27 = C4105zY.m41624a(GlobalEconomyInfo.class, "0c19174c2d3b99840d3a5e3fac8fe248", i50);
        aFg = a27;
        fmVarArr[i50] = a27;
        int i51 = i50 + 1;
        C2491fm a28 = C4105zY.m41624a(GlobalEconomyInfo.class, "b52873a89b3abd6befef6de09a278d36", i51);
        aFh = a28;
        fmVarArr[i51] = a28;
        int i52 = i51 + 1;
        C2491fm a29 = C4105zY.m41624a(GlobalEconomyInfo.class, "2bdddea94876b64e1b4cafa4bf9f1ad1", i52);
        aFi = a29;
        fmVarArr[i52] = a29;
        int i53 = i52 + 1;
        C2491fm a30 = C4105zY.m41624a(GlobalEconomyInfo.class, "80e0e40bccaf275e11bce201152fec2c", i53);
        aFj = a30;
        fmVarArr[i53] = a30;
        int i54 = i53 + 1;
        C2491fm a31 = C4105zY.m41624a(GlobalEconomyInfo.class, "6bed4e202006693c0ee0742e34b0efe1", i54);
        aFk = a31;
        fmVarArr[i54] = a31;
        int i55 = i54 + 1;
        C2491fm a32 = C4105zY.m41624a(GlobalEconomyInfo.class, "e579ef4b17e35106915027d9cd3f12e4", i55);
        aFl = a32;
        fmVarArr[i55] = a32;
        int i56 = i55 + 1;
        C2491fm a33 = C4105zY.m41624a(GlobalEconomyInfo.class, "6f94faf21d66252da419d1718aa1ff24", i56);
        aFm = a33;
        fmVarArr[i56] = a33;
        int i57 = i56 + 1;
        C2491fm a34 = C4105zY.m41624a(GlobalEconomyInfo.class, "73a5b83a37217f27a39e92096ad0ec8a", i57);
        aFn = a34;
        fmVarArr[i57] = a34;
        int i58 = i57 + 1;
        C2491fm a35 = C4105zY.m41624a(GlobalEconomyInfo.class, "225b311ae734ecd8954bbfb4c9184d6b", i58);
        aFo = a35;
        fmVarArr[i58] = a35;
        int i59 = i58 + 1;
        C2491fm a36 = C4105zY.m41624a(GlobalEconomyInfo.class, "eac36d9338978c863705ff7a045b98fa", i59);
        aFp = a36;
        fmVarArr[i59] = a36;
        int i60 = i59 + 1;
        C2491fm a37 = C4105zY.m41624a(GlobalEconomyInfo.class, "d631728c3e921108cebf3e939519329f", i60);
        aFq = a37;
        fmVarArr[i60] = a37;
        int i61 = i60 + 1;
        C2491fm a38 = C4105zY.m41624a(GlobalEconomyInfo.class, "6d1b55953db28f9d9bae691339f24425", i61);
        aFr = a38;
        fmVarArr[i61] = a38;
        int i62 = i61 + 1;
        C2491fm a39 = C4105zY.m41624a(GlobalEconomyInfo.class, "3868f8f028910badee594161e4320cae", i62);
        aFs = a39;
        fmVarArr[i62] = a39;
        int i63 = i62 + 1;
        C2491fm a40 = C4105zY.m41624a(GlobalEconomyInfo.class, "a2077d60370b169edff32341336b759d", i63);
        aFt = a40;
        fmVarArr[i63] = a40;
        int i64 = i63 + 1;
        C2491fm a41 = C4105zY.m41624a(GlobalEconomyInfo.class, "93805669d4670b577fe2cd7a85a983fa", i64);
        aFu = a41;
        fmVarArr[i64] = a41;
        int i65 = i64 + 1;
        C2491fm a42 = C4105zY.m41624a(GlobalEconomyInfo.class, "eade5f287abda27d95d901304c729768", i65);
        aFv = a42;
        fmVarArr[i65] = a42;
        int i66 = i65 + 1;
        C2491fm a43 = C4105zY.m41624a(GlobalEconomyInfo.class, "68c239af815254e574116ea8fe277839", i66);
        aFw = a43;
        fmVarArr[i66] = a43;
        int i67 = i66 + 1;
        C2491fm a44 = C4105zY.m41624a(GlobalEconomyInfo.class, "8f02de58dee0e6c17a323de19f5a4b65", i67);
        aFx = a44;
        fmVarArr[i67] = a44;
        int i68 = i67 + 1;
        C2491fm a45 = C4105zY.m41624a(GlobalEconomyInfo.class, "97d789036c3ae674be342a4521f78566", i68);
        aFy = a45;
        fmVarArr[i68] = a45;
        int i69 = i68 + 1;
        C2491fm a46 = C4105zY.m41624a(GlobalEconomyInfo.class, "9786cd9f49bfc19da37c6fda932c6c0b", i69);
        aFz = a46;
        fmVarArr[i69] = a46;
        int i70 = i69 + 1;
        C2491fm a47 = C4105zY.m41624a(GlobalEconomyInfo.class, "9bf89d99fc18f9bd585058eb12a34023", i70);
        aFA = a47;
        fmVarArr[i70] = a47;
        int i71 = i70 + 1;
        C2491fm a48 = C4105zY.m41624a(GlobalEconomyInfo.class, "8065f814c78d423c2885bfcb20e4eb08", i71);
        aFB = a48;
        fmVarArr[i71] = a48;
        int i72 = i71 + 1;
        C2491fm a49 = C4105zY.m41624a(GlobalEconomyInfo.class, "0a2db3b883a903fbdd47222fc40551a6", i72);
        aFC = a49;
        fmVarArr[i72] = a49;
        int i73 = i72 + 1;
        C2491fm a50 = C4105zY.m41624a(GlobalEconomyInfo.class, "52fe586d2df9b710cc0823b2e71e97ba", i73);
        aFD = a50;
        fmVarArr[i73] = a50;
        int i74 = i73 + 1;
        C2491fm a51 = C4105zY.m41624a(GlobalEconomyInfo.class, "5e1fd298d399839cfddd9904e1983ec4", i74);
        aFE = a51;
        fmVarArr[i74] = a51;
        int i75 = i74 + 1;
        C2491fm a52 = C4105zY.m41624a(GlobalEconomyInfo.class, "93fb549a981623f827a2eebf34f6a050", i75);
        aFF = a52;
        fmVarArr[i75] = a52;
        int i76 = i75 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GlobalEconomyInfo.class, C6104agY.class, _m_fields, _m_methods);
    }

    /* renamed from: NL */
    private float m35495NL() {
        return bFf().mo5608dq().mo3211m(aDU);
    }

    /* renamed from: NM */
    private float m35496NM() {
        return bFf().mo5608dq().mo3211m(aDW);
    }

    /* renamed from: NN */
    private float m35497NN() {
        return bFf().mo5608dq().mo3211m(aDY);
    }

    /* renamed from: NO */
    private float m35498NO() {
        return bFf().mo5608dq().mo3211m(aEa);
    }

    /* renamed from: NP */
    private float m35499NP() {
        return bFf().mo5608dq().mo3211m(aEc);
    }

    /* renamed from: NQ */
    private float m35500NQ() {
        return bFf().mo5608dq().mo3211m(aEe);
    }

    /* renamed from: NR */
    private float m35501NR() {
        return bFf().mo5608dq().mo3211m(aEg);
    }

    /* renamed from: NS */
    private float m35502NS() {
        return bFf().mo5608dq().mo3211m(aEi);
    }

    /* renamed from: NT */
    private float m35503NT() {
        return bFf().mo5608dq().mo3211m(aEk);
    }

    /* renamed from: NU */
    private float m35504NU() {
        return bFf().mo5608dq().mo3211m(aEm);
    }

    /* renamed from: NV */
    private float m35505NV() {
        return bFf().mo5608dq().mo3211m(aEo);
    }

    /* renamed from: NW */
    private float m35506NW() {
        return bFf().mo5608dq().mo3211m(aEq);
    }

    /* renamed from: NX */
    private float m35507NX() {
        return bFf().mo5608dq().mo3211m(aEs);
    }

    /* renamed from: NY */
    private float m35508NY() {
        return bFf().mo5608dq().mo3211m(aEu);
    }

    /* renamed from: NZ */
    private float m35509NZ() {
        return bFf().mo5608dq().mo3211m(aEw);
    }

    /* renamed from: Oa */
    private float m35523Oa() {
        return bFf().mo5608dq().mo3211m(aEy);
    }

    /* renamed from: Ob */
    private float m35524Ob() {
        return bFf().mo5608dq().mo3211m(aEA);
    }

    /* renamed from: Oc */
    private float m35525Oc() {
        return bFf().mo5608dq().mo3211m(aEC);
    }

    /* renamed from: Od */
    private float m35526Od() {
        return bFf().mo5608dq().mo3211m(aEE);
    }

    /* renamed from: Oe */
    private float m35527Oe() {
        return bFf().mo5608dq().mo3211m(aEG);
    }

    /* renamed from: Of */
    private C1556Wo m35528Of() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(aEI);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "6780760eea23c858e564317692810239", aum = 0)
    @C2499fr
    /* renamed from: Oi */
    private void m35530Oi() {
        throw new aWi(new aCE(this, aEK, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: Oj */
    private void m35531Oj() {
        switch (bFf().mo6893i(aEK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEK, new Object[0]));
                break;
        }
        m35530Oi();
    }

    @C0064Am(aul = "5e1fd298d399839cfddd9904e1983ec4", aum = 0)
    @C5566aOg
    /* renamed from: Pc */
    private void m35541Pc() {
        throw new aWi(new aCE(this, aFE, new Object[0]));
    }

    /* renamed from: a */
    private void m35546a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8642bL, uuid);
    }

    /* renamed from: an */
    private UUID m35547an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8642bL);
    }

    /* renamed from: bg */
    private void m35564bg(float f) {
        bFf().mo5608dq().mo3150a(aDU, f);
    }

    /* renamed from: bh */
    private void m35565bh(float f) {
        bFf().mo5608dq().mo3150a(aDW, f);
    }

    /* renamed from: bi */
    private void m35566bi(float f) {
        bFf().mo5608dq().mo3150a(aDY, f);
    }

    /* renamed from: bj */
    private void m35567bj(float f) {
        bFf().mo5608dq().mo3150a(aEa, f);
    }

    /* renamed from: bk */
    private void m35568bk(float f) {
        bFf().mo5608dq().mo3150a(aEc, f);
    }

    /* renamed from: bl */
    private void m35569bl(float f) {
        bFf().mo5608dq().mo3150a(aEe, f);
    }

    /* renamed from: bm */
    private void m35570bm(float f) {
        bFf().mo5608dq().mo3150a(aEg, f);
    }

    /* renamed from: bn */
    private void m35571bn(float f) {
        bFf().mo5608dq().mo3150a(aEi, f);
    }

    /* renamed from: bo */
    private void m35572bo(float f) {
        bFf().mo5608dq().mo3150a(aEk, f);
    }

    /* renamed from: bp */
    private void m35573bp(float f) {
        bFf().mo5608dq().mo3150a(aEm, f);
    }

    /* renamed from: bq */
    private void m35574bq(float f) {
        bFf().mo5608dq().mo3150a(aEo, f);
    }

    /* renamed from: br */
    private void m35575br(float f) {
        bFf().mo5608dq().mo3150a(aEq, f);
    }

    /* renamed from: bs */
    private void m35576bs(float f) {
        bFf().mo5608dq().mo3150a(aEs, f);
    }

    /* renamed from: bt */
    private void m35577bt(float f) {
        bFf().mo5608dq().mo3150a(aEu, f);
    }

    /* renamed from: bu */
    private void m35578bu(float f) {
        bFf().mo5608dq().mo3150a(aEw, f);
    }

    /* renamed from: bv */
    private void m35579bv(float f) {
        bFf().mo5608dq().mo3150a(aEy, f);
    }

    /* renamed from: bw */
    private void m35580bw(float f) {
        bFf().mo5608dq().mo3150a(aEA, f);
    }

    /* renamed from: bx */
    private void m35581bx(float f) {
        bFf().mo5608dq().mo3150a(aEC, f);
    }

    /* renamed from: by */
    private void m35582by(float f) {
        bFf().mo5608dq().mo3150a(aEE, f);
    }

    /* renamed from: bz */
    private void m35583bz(float f) {
        bFf().mo5608dq().mo3150a(aEG, f);
    }

    /* renamed from: c */
    private void m35584c(UUID uuid) {
        switch (bFf().mo6893i(f8644bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8644bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8644bO, new Object[]{uuid}));
                break;
        }
        m35550b(uuid);
    }

    /* renamed from: j */
    private void m35593j(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(aEI, wo);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Increase")
    /* renamed from: OB */
    public float mo20460OB() {
        switch (bFf().mo6893i(aFb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFb, new Object[0]));
                break;
        }
        return m35510OA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Decrease")
    /* renamed from: OD */
    public float mo20461OD() {
        switch (bFf().mo6893i(aFd)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFd, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFd, new Object[0]));
                break;
        }
        return m35511OC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Increase")
    /* renamed from: OF */
    public float mo20462OF() {
        switch (bFf().mo6893i(aFf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFf, new Object[0]));
                break;
        }
        return m35512OE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reposition Time (s)")
    /* renamed from: OH */
    public float mo20463OH() {
        switch (bFf().mo6893i(aFh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFh, new Object[0]));
                break;
        }
        return m35513OG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Expensive, Lower Bound")
    /* renamed from: OJ */
    public float mo20464OJ() {
        switch (bFf().mo6893i(aFj)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFj, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFj, new Object[0]));
                break;
        }
        return m35514OI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Expensive, Higher Bound")
    /* renamed from: OL */
    public float mo20465OL() {
        switch (bFf().mo6893i(aFl)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFl, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFl, new Object[0]));
                break;
        }
        return m35515OK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Cheap, Lower Bound")
    /* renamed from: ON */
    public float mo20466ON() {
        switch (bFf().mo6893i(aFn)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFn, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFn, new Object[0]));
                break;
        }
        return m35516OM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Cheap, Higher Bound")
    /* renamed from: OP */
    public float mo20467OP() {
        switch (bFf().mo6893i(aFp)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFp, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFp, new Object[0]));
                break;
        }
        return m35517OO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Active, Lower Bound")
    /* renamed from: OR */
    public float mo20468OR() {
        switch (bFf().mo6893i(aFr)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFr, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFr, new Object[0]));
                break;
        }
        return m35518OQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Active, Higher Bound")
    /* renamed from: OT */
    public float mo20469OT() {
        switch (bFf().mo6893i(aFt)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFt, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFt, new Object[0]));
                break;
        }
        return m35519OS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Any, Lower Bound")
    /* renamed from: OV */
    public float mo20470OV() {
        switch (bFf().mo6893i(aFv)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFv, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFv, new Object[0]));
                break;
        }
        return m35520OU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Any, Higher Bound")
    /* renamed from: OX */
    public float mo20471OX() {
        switch (bFf().mo6893i(aFx)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aFx, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFx, new Object[0]));
                break;
        }
        return m35521OW();
    }

    /* renamed from: OZ */
    public void mo20472OZ() {
        switch (bFf().mo6893i(aFz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFz, new Object[0]));
                break;
        }
        m35522OY();
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "_Load Defaults_")
    /* renamed from: Oh */
    public void mo20473Oh() {
        switch (bFf().mo6893i(aEJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEJ, new Object[0]));
                break;
        }
        m35529Og();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Cheap, Expiration Time (s)")
    /* renamed from: Ol */
    public float mo20474Ol() {
        switch (bFf().mo6893i(aEL)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aEL, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aEL, new Object[0]));
                break;
        }
        return m35532Ok();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Expensive, Expiration Time (s)")
    /* renamed from: On */
    public float mo20475On() {
        switch (bFf().mo6893i(aEN)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aEN, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aEN, new Object[0]));
                break;
        }
        return m35533Om();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Active, Expiration Time (s)")
    /* renamed from: Op */
    public float mo20476Op() {
        switch (bFf().mo6893i(aEP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aEP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aEP, new Object[0]));
                break;
        }
        return m35534Oo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Any, Expiration Time (s)")
    /* renamed from: Or */
    public float mo20477Or() {
        switch (bFf().mo6893i(aER)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aER, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aER, new Object[0]));
                break;
        }
        return m35535Oq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Expiration Time Max Variation")
    /* renamed from: Ot */
    public float mo20478Ot() {
        switch (bFf().mo6893i(aET)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aET, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aET, new Object[0]));
                break;
        }
        return m35536Os();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Minimum Lot Max Variation")
    /* renamed from: Ov */
    public float mo20479Ov() {
        switch (bFf().mo6893i(aEV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aEV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aEV, new Object[0]));
                break;
        }
        return m35537Ou();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Median Price Max Daily Variation")
    /* renamed from: Ox */
    public float mo20480Ox() {
        switch (bFf().mo6893i(aEX)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aEX, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aEX, new Object[0]));
                break;
        }
        return m35538Ow();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Decrease")
    /* renamed from: Oz */
    public float mo20481Oz() {
        switch (bFf().mo6893i(aEZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aEZ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aEZ, new Object[0]));
                break;
        }
        return m35539Oy();
    }

    /* renamed from: Pb */
    public void mo20482Pb() {
        switch (bFf().mo6893i(aFA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFA, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFA, new Object[0]));
                break;
        }
        m35540Pa();
    }

    @C5566aOg
    /* renamed from: Pd */
    public void mo20483Pd() {
        switch (bFf().mo6893i(aFE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFE, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFE, new Object[0]));
                break;
        }
        m35541Pc();
    }

    /* renamed from: Pf */
    public void mo20484Pf() {
        switch (bFf().mo6893i(aFF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFF, new Object[0]));
                break;
        }
        m35542Pe();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6104agY(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m35548ap();
            case 1:
                m35550b((UUID) args[0]);
                return null;
            case 2:
                return m35549ar();
            case 3:
                m35529Og();
                return null;
            case 4:
                m35530Oi();
                return null;
            case 5:
                return new Float(m35532Ok());
            case 6:
                m35551bA(((Float) args[0]).floatValue());
                return null;
            case 7:
                return new Float(m35533Om());
            case 8:
                m35552bC(((Float) args[0]).floatValue());
                return null;
            case 9:
                return new Float(m35534Oo());
            case 10:
                m35553bE(((Float) args[0]).floatValue());
                return null;
            case 11:
                return new Float(m35535Oq());
            case 12:
                m35554bG(((Float) args[0]).floatValue());
                return null;
            case 13:
                return new Float(m35536Os());
            case 14:
                m35555bI(((Float) args[0]).floatValue());
                return null;
            case 15:
                return new Float(m35537Ou());
            case 16:
                m35556bK(((Float) args[0]).floatValue());
                return null;
            case 17:
                return new Float(m35538Ow());
            case 18:
                m35557bM(((Float) args[0]).floatValue());
                return null;
            case 19:
                return new Float(m35539Oy());
            case 20:
                m35558bO(((Float) args[0]).floatValue());
                return null;
            case 21:
                return new Float(m35510OA());
            case 22:
                m35559bQ(((Float) args[0]).floatValue());
                return null;
            case 23:
                return new Float(m35511OC());
            case 24:
                m35560bS(((Float) args[0]).floatValue());
                return null;
            case 25:
                return new Float(m35512OE());
            case 26:
                m35561bU(((Float) args[0]).floatValue());
                return null;
            case 27:
                return new Float(m35513OG());
            case 28:
                m35562bW(((Float) args[0]).floatValue());
                return null;
            case 29:
                return new Float(m35514OI());
            case 30:
                m35563bY(((Float) args[0]).floatValue());
                return null;
            case 31:
                return new Float(m35515OK());
            case 32:
                m35585ca(((Float) args[0]).floatValue());
                return null;
            case 33:
                return new Float(m35516OM());
            case 34:
                m35586cc(((Float) args[0]).floatValue());
                return null;
            case 35:
                return new Float(m35517OO());
            case 36:
                m35587ce(((Float) args[0]).floatValue());
                return null;
            case 37:
                return new Float(m35518OQ());
            case 38:
                m35588cg(((Float) args[0]).floatValue());
                return null;
            case 39:
                return new Float(m35519OS());
            case 40:
                m35589ci(((Float) args[0]).floatValue());
                return null;
            case 41:
                return new Float(m35520OU());
            case 42:
                m35590ck(((Float) args[0]).floatValue());
                return null;
            case 43:
                return new Float(m35521OW());
            case 44:
                m35591cm(((Float) args[0]).floatValue());
                return null;
            case 45:
                m35522OY();
                return null;
            case 46:
                m35540Pa();
                return null;
            case 47:
                m35545a((ItemType) args[0], ((Long) args[1]).longValue(), ((Integer) args[2]).intValue());
                return null;
            case 48:
                m35592i((ItemType) args[0]);
                return null;
            case 49:
                return new Long(m35544a(((Long) args[0]).longValue(), (BaseItemType) args[1]));
            case 50:
                m35541Pc();
                return null;
            case 51:
                m35542Pe();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8643bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8643bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8643bN, new Object[0]));
                break;
        }
        return m35548ap();
    }

    /* renamed from: b */
    public long mo20485b(long j, BaseItemType xBVar) {
        switch (bFf().mo6893i(aFD)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, aFD, new Object[]{new Long(j), xBVar}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, aFD, new Object[]{new Long(j), xBVar}));
                break;
        }
        return m35544a(j, xBVar);
    }

    /* renamed from: b */
    public void mo20486b(ItemType jCVar, long j, int i) {
        switch (bFf().mo6893i(aFB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFB, new Object[]{jCVar, new Long(j), new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFB, new Object[]{jCVar, new Long(j), new Integer(i)}));
                break;
        }
        m35545a(jCVar, j, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Cheap, Expiration Time (s)")
    /* renamed from: bB */
    public void mo20487bB(float f) {
        switch (bFf().mo6893i(aEM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEM, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEM, new Object[]{new Float(f)}));
                break;
        }
        m35551bA(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Expensive, Expiration Time (s)")
    /* renamed from: bD */
    public void mo20488bD(float f) {
        switch (bFf().mo6893i(aEO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEO, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEO, new Object[]{new Float(f)}));
                break;
        }
        m35552bC(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Active, Expiration Time (s)")
    /* renamed from: bF */
    public void mo20489bF(float f) {
        switch (bFf().mo6893i(aEQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEQ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEQ, new Object[]{new Float(f)}));
                break;
        }
        m35553bE(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Any, Expiration Time (s)")
    /* renamed from: bH */
    public void mo20490bH(float f) {
        switch (bFf().mo6893i(aES)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aES, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aES, new Object[]{new Float(f)}));
                break;
        }
        m35554bG(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Expiration Time Max Variation")
    /* renamed from: bJ */
    public void mo20491bJ(float f) {
        switch (bFf().mo6893i(aEU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEU, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEU, new Object[]{new Float(f)}));
                break;
        }
        m35555bI(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Minimum Lot Max Variation")
    /* renamed from: bL */
    public void mo20492bL(float f) {
        switch (bFf().mo6893i(aEW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEW, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEW, new Object[]{new Float(f)}));
                break;
        }
        m35556bK(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Median Price Max Daily Variation")
    /* renamed from: bN */
    public void mo20493bN(float f) {
        switch (bFf().mo6893i(aEY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEY, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEY, new Object[]{new Float(f)}));
                break;
        }
        m35557bM(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Decrease")
    /* renamed from: bP */
    public void mo20494bP(float f) {
        switch (bFf().mo6893i(aFa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFa, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFa, new Object[]{new Float(f)}));
                break;
        }
        m35558bO(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Increase")
    /* renamed from: bR */
    public void mo20495bR(float f) {
        switch (bFf().mo6893i(aFc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFc, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFc, new Object[]{new Float(f)}));
                break;
        }
        m35559bQ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Decrease")
    /* renamed from: bT */
    public void mo20496bT(float f) {
        switch (bFf().mo6893i(aFe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFe, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFe, new Object[]{new Float(f)}));
                break;
        }
        m35560bS(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Increase")
    /* renamed from: bV */
    public void mo20497bV(float f) {
        switch (bFf().mo6893i(aFg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFg, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFg, new Object[]{new Float(f)}));
                break;
        }
        m35561bU(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reposition Time (s)")
    /* renamed from: bX */
    public void mo20498bX(float f) {
        switch (bFf().mo6893i(aFi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFi, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFi, new Object[]{new Float(f)}));
                break;
        }
        m35562bW(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Expensive, Lower Bound")
    /* renamed from: bZ */
    public void mo20499bZ(float f) {
        switch (bFf().mo6893i(aFk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFk, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFk, new Object[]{new Float(f)}));
                break;
        }
        m35563bY(f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Expensive, Higher Bound")
    /* renamed from: cb */
    public void mo20500cb(float f) {
        switch (bFf().mo6893i(aFm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFm, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFm, new Object[]{new Float(f)}));
                break;
        }
        m35585ca(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Cheap, Lower Bound")
    /* renamed from: cd */
    public void mo20501cd(float f) {
        switch (bFf().mo6893i(aFo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFo, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFo, new Object[]{new Float(f)}));
                break;
        }
        m35586cc(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Cheap, Higher Bound")
    /* renamed from: cf */
    public void mo20502cf(float f) {
        switch (bFf().mo6893i(aFq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFq, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFq, new Object[]{new Float(f)}));
                break;
        }
        m35587ce(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Active, Lower Bound")
    /* renamed from: ch */
    public void mo20503ch(float f) {
        switch (bFf().mo6893i(aFs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFs, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFs, new Object[]{new Float(f)}));
                break;
        }
        m35588cg(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Active, Higher Bound")
    /* renamed from: cj */
    public void mo20504cj(float f) {
        switch (bFf().mo6893i(aFu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFu, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFu, new Object[]{new Float(f)}));
                break;
        }
        m35589ci(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Any, Lower Bound")
    /* renamed from: cl */
    public void mo20505cl(float f) {
        switch (bFf().mo6893i(aFw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFw, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFw, new Object[]{new Float(f)}));
                break;
        }
        m35590ck(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Any, Higher Bound")
    /* renamed from: cn */
    public void mo20506cn(float f) {
        switch (bFf().mo6893i(aFy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFy, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFy, new Object[]{new Float(f)}));
                break;
        }
        m35591cm(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8645bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8645bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8645bP, new Object[0]));
                break;
        }
        return m35549ar();
    }

    /* renamed from: j */
    public void mo20507j(ItemType jCVar) {
        switch (bFf().mo6893i(aFC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aFC, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aFC, new Object[]{jCVar}));
                break;
        }
        m35592i(jCVar);
    }

    @C0064Am(aul = "368ff267ccc9f7997c182ac8f486a61d", aum = 0)
    /* renamed from: ap */
    private UUID m35548ap() {
        return m35547an();
    }

    @C0064Am(aul = "006b4f1285453d02d46459b77fd20e29", aum = 0)
    /* renamed from: b */
    private void m35550b(UUID uuid) {
        m35546a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "f2b88e383591c01132b0166e7560b884", aum = 0)
    /* renamed from: ar */
    private String m35549ar() {
        return "global_economy_info";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m35546a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "_Load Defaults_")
    @C0064Am(aul = "46a2e7d5934b3a8dc4d5ff091ff9065e", aum = 0)
    /* renamed from: Og */
    private void m35529Og() {
        m35531Oj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Cheap, Expiration Time (s)")
    @C0064Am(aul = "3e541737c75fff3d1c2527584b0cadc4", aum = 0)
    /* renamed from: Ok */
    private float m35532Ok() {
        return m35495NL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Cheap, Expiration Time (s)")
    @C0064Am(aul = "59c84b4d057573532ff69882bca306e1", aum = 0)
    /* renamed from: bA */
    private void m35551bA(float f) {
        m35564bg(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Expensive, Expiration Time (s)")
    @C0064Am(aul = "7e10656ffbd8b1e2bc8a9be962e0b8d7", aum = 0)
    /* renamed from: Om */
    private float m35533Om() {
        return m35496NM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Expensive, Expiration Time (s)")
    @C0064Am(aul = "7acd6d145d3c406aa27f2a505fb2927b", aum = 0)
    /* renamed from: bC */
    private void m35552bC(float f) {
        m35565bh(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Active, Expiration Time (s)")
    @C0064Am(aul = "5b1c19a5fe3ba45d5bff9d5ff0f7e392", aum = 0)
    /* renamed from: Oo */
    private float m35534Oo() {
        return m35497NN();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Active, Expiration Time (s)")
    @C0064Am(aul = "9ad2b94667840557821ce2c9df4b1099", aum = 0)
    /* renamed from: bE */
    private void m35553bE(float f) {
        m35566bi(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Any, Expiration Time (s)")
    @C0064Am(aul = "0f1299558317de3cda6502d389e5c3d1", aum = 0)
    /* renamed from: Oq */
    private float m35535Oq() {
        return m35498NO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Any, Expiration Time (s)")
    @C0064Am(aul = "d7f7d43fa9afd53cdb939e088fa16391", aum = 0)
    /* renamed from: bG */
    private void m35554bG(float f) {
        m35567bj(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Expiration Time Max Variation")
    @C0064Am(aul = "7cb3672899a06f8b6f7d583d83871283", aum = 0)
    /* renamed from: Os */
    private float m35536Os() {
        return m35499NP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Expiration Time Max Variation")
    @C0064Am(aul = "bf415aaae3d2b6c968fbf6fde078ca66", aum = 0)
    /* renamed from: bI */
    private void m35555bI(float f) {
        m35568bk(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Minimum Lot Max Variation")
    @C0064Am(aul = "3b3366ce5aba7a403cacd5f71ae989f8", aum = 0)
    /* renamed from: Ou */
    private float m35537Ou() {
        return m35500NQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Minimum Lot Max Variation")
    @C0064Am(aul = "23758569560557e7c804df6289ce7a1f", aum = 0)
    /* renamed from: bK */
    private void m35556bK(float f) {
        m35569bl(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Median Price Max Daily Variation")
    @C0064Am(aul = "3ae55b41d8752b104986755cbf342f54", aum = 0)
    /* renamed from: Ow */
    private float m35538Ow() {
        return m35501NR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Median Price Max Daily Variation")
    @C0064Am(aul = "97215d43c9bc9cccfefaa61f35293dac", aum = 0)
    /* renamed from: bM */
    private void m35557bM(float f) {
        m35570bm(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Decrease")
    @C0064Am(aul = "cae389845529737bfd92cbd02c3d97ef", aum = 0)
    /* renamed from: Oy */
    private float m35539Oy() {
        return m35502NS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Decrease")
    @C0064Am(aul = "13b51e118253af1273338948bf88d433", aum = 0)
    /* renamed from: bO */
    private void m35558bO(float f) {
        m35571bn(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Increase")
    @C0064Am(aul = "fcdea0011e9d501588423eb0c7850c9f", aum = 0)
    /* renamed from: OA */
    private float m35510OA() {
        return m35503NT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Increase")
    @C0064Am(aul = "242027e23ce972eb391e9574887d6f5d", aum = 0)
    /* renamed from: bQ */
    private void m35559bQ(float f) {
        m35572bo(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Decrease")
    @C0064Am(aul = "32be396f56b129a7c3ffbe22f87bcf8b", aum = 0)
    /* renamed from: OC */
    private float m35511OC() {
        return m35504NU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Decrease")
    @C0064Am(aul = "354eeaea44d0a1b1773add5f1d057fae", aum = 0)
    /* renamed from: bS */
    private void m35560bS(float f) {
        m35573bp(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Increase")
    @C0064Am(aul = "8f3db251498360b522881638ef24042c", aum = 0)
    /* renamed from: OE */
    private float m35512OE() {
        return m35505NV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Increase")
    @C0064Am(aul = "0c19174c2d3b99840d3a5e3fac8fe248", aum = 0)
    /* renamed from: bU */
    private void m35561bU(float f) {
        m35574bq(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reposition Time (s)")
    @C0064Am(aul = "b52873a89b3abd6befef6de09a278d36", aum = 0)
    /* renamed from: OG */
    private float m35513OG() {
        return m35506NW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reposition Time (s)")
    @C0064Am(aul = "2bdddea94876b64e1b4cafa4bf9f1ad1", aum = 0)
    /* renamed from: bW */
    private void m35562bW(float f) {
        m35575br(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Expensive, Lower Bound")
    @C0064Am(aul = "80e0e40bccaf275e11bce201152fec2c", aum = 0)
    /* renamed from: OI */
    private float m35514OI() {
        return m35507NX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Expensive, Lower Bound")
    @C0064Am(aul = "6bed4e202006693c0ee0742e34b0efe1", aum = 0)
    /* renamed from: bY */
    private void m35563bY(float f) {
        m35576bs(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Expensive, Higher Bound")
    @C0064Am(aul = "e579ef4b17e35106915027d9cd3f12e4", aum = 0)
    /* renamed from: OK */
    private float m35515OK() {
        return m35508NY();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Expensive, Higher Bound")
    @C0064Am(aul = "6f94faf21d66252da419d1718aa1ff24", aum = 0)
    /* renamed from: ca */
    private void m35585ca(float f) {
        m35577bt(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Cheap, Lower Bound")
    @C0064Am(aul = "73a5b83a37217f27a39e92096ad0ec8a", aum = 0)
    /* renamed from: OM */
    private float m35516OM() {
        return m35509NZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Cheap, Lower Bound")
    @C0064Am(aul = "225b311ae734ecd8954bbfb4c9184d6b", aum = 0)
    /* renamed from: cc */
    private void m35586cc(float f) {
        m35578bu(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sell, Cheap, Higher Bound")
    @C0064Am(aul = "eac36d9338978c863705ff7a045b98fa", aum = 0)
    /* renamed from: OO */
    private float m35517OO() {
        return m35523Oa();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sell, Cheap, Higher Bound")
    @C0064Am(aul = "d631728c3e921108cebf3e939519329f", aum = 0)
    /* renamed from: ce */
    private void m35587ce(float f) {
        m35579bv(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Active, Lower Bound")
    @C0064Am(aul = "6d1b55953db28f9d9bae691339f24425", aum = 0)
    /* renamed from: OQ */
    private float m35518OQ() {
        return m35524Ob();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Active, Lower Bound")
    @C0064Am(aul = "3868f8f028910badee594161e4320cae", aum = 0)
    /* renamed from: cg */
    private void m35588cg(float f) {
        m35580bw(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Active, Higher Bound")
    @C0064Am(aul = "a2077d60370b169edff32341336b759d", aum = 0)
    /* renamed from: OS */
    private float m35519OS() {
        return m35525Oc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Active, Higher Bound")
    @C0064Am(aul = "93805669d4670b577fe2cd7a85a983fa", aum = 0)
    /* renamed from: ci */
    private void m35589ci(float f) {
        m35581bx(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Any, Lower Bound")
    @C0064Am(aul = "eade5f287abda27d95d901304c729768", aum = 0)
    /* renamed from: OU */
    private float m35520OU() {
        return m35526Od();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Any, Lower Bound")
    @C0064Am(aul = "68c239af815254e574116ea8fe277839", aum = 0)
    /* renamed from: ck */
    private void m35590ck(float f) {
        m35582by(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Buy, Any, Higher Bound")
    @C0064Am(aul = "8f02de58dee0e6c17a323de19f5a4b65", aum = 0)
    /* renamed from: OW */
    private float m35521OW() {
        return m35527Oe();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Buy, Any, Higher Bound")
    @C0064Am(aul = "97d789036c3ae674be342a4521f78566", aum = 0)
    /* renamed from: cm */
    private void m35591cm(float f) {
        m35583bz(f);
    }

    @C0064Am(aul = "9786cd9f49bfc19da37c6fda932c6c0b", aum = 0)
    /* renamed from: OY */
    private void m35522OY() {
        Store azJ;
        for (ItemType anP : ala().mo1458cD(true)) {
            anP.anP();
        }
        for (StellarSystem aXN : ala().aKa()) {
            for (Actor next : aXN.aXN()) {
                if ((next instanceof Station) && (azJ = ((Station) next).azJ()) != null) {
                    azJ.anP();
                }
            }
        }
    }

    @C0064Am(aul = "9bf89d99fc18f9bd585058eb12a34023", aum = 0)
    /* renamed from: Pa */
    private void m35540Pa() {
        Store azJ;
        mo20484Pf();
        for (ItemType reset : ala().mo1458cD(true)) {
            reset.reset();
        }
        for (StellarSystem aXN : ala().aKa()) {
            for (Actor next : aXN.aXN()) {
                if ((next instanceof Station) && (azJ = ((Station) next).azJ()) != null) {
                    azJ.reset();
                }
            }
        }
    }

    @C0064Am(aul = "8065f814c78d423c2885bfcb20e4eb08", aum = 0)
    /* renamed from: a */
    private void m35545a(ItemType jCVar, long j, int i) {
        if (!m35528Of().containsKey(jCVar)) {
            C1556Wo Of = m35528Of();
            TransactionLog aVar = (TransactionLog) bFf().mo6865M(TransactionLog.class);
            aVar.mo10S();
            Of.put(jCVar, aVar);
        }
        ((TransactionLog) m35528Of().get(jCVar)).mo20510b(j, i);
    }

    @C0064Am(aul = "0a2db3b883a903fbdd47222fc40551a6", aum = 0)
    /* renamed from: i */
    private void m35592i(ItemType jCVar) {
        if (!m35528Of().containsKey(jCVar)) {
            C1556Wo Of = m35528Of();
            TransactionLog aVar = (TransactionLog) bFf().mo6865M(TransactionLog.class);
            aVar.mo10S();
            Of.put(jCVar, aVar);
        }
        TransactionLog aVar2 = (TransactionLog) m35528Of().get(jCVar);
        jCVar.mo22856d(aVar2.mo20508WX(), aVar2.mo20509WZ());
    }

    @C0064Am(aul = "52fe586d2df9b710cc0823b2e71e97ba", aum = 0)
    /* renamed from: a */
    private long m35544a(long j, BaseItemType xBVar) {
        Store azJ;
        long j2;
        long j3;
        if (!(xBVar instanceof ItemType)) {
            mo8358lY("BaseItemType not an itemType?!? " + xBVar);
            return j;
        }
        ItemType jCVar = (ItemType) xBVar;
        for (StellarSystem aXN : ala().aKa()) {
            for (Actor next : aXN.aXN()) {
                if ((next instanceof Station) && (azJ = ((Station) next).azJ()) != null) {
                    if (azJ.dCv().contains(jCVar) || azJ.dCx().contains(jCVar)) {
                        j2 = Long.MAX_VALUE;
                        for (SellOrder next2 : azJ.dCD().getList()) {
                            if (next2.mo9437eP() == jCVar && next2.mo9438eR() < j2) {
                                j2 = next2.mo9438eR();
                            }
                        }
                    } else {
                        j2 = Long.MAX_VALUE;
                    }
                    if (azJ.dCz().contains(jCVar)) {
                        j3 = Long.MIN_VALUE;
                        for (BuyOrder next3 : azJ.dCB().getList()) {
                            if (next3.mo9437eP() == jCVar && next3.mo9438eR() > j3) {
                                j3 = next3.mo9438eR();
                            }
                        }
                    } else {
                        j3 = Long.MIN_VALUE;
                    }
                    for (Map.Entry<C0847MM, ListContainer<BuyOrder>> value : azJ.dCH().entrySet()) {
                        for (BuyOrder kl : ((ListContainer) value.getValue()).getList()) {
                            if (kl.mo9437eP() == jCVar && kl.mo9438eR() > j3) {
                                j3 = kl.mo9438eR();
                            }
                        }
                    }
                    if (j3 > j2) {
                        mo8358lY("ECONOMY ERROR: " + jCVar + " was refreshing price, lowestSell = " + j2 + " highestBuy = " + j3);
                        mo8358lY(" new price would be " + j + " old price was " + jCVar.mo19868HG());
                    }
                    if (j > j2) {
                        j = j2;
                    } else if (j < j3) {
                        j = j3;
                    }
                }
            }
        }
        double HK = (double) jCVar.mo19870HK();
        double HI = (double) jCVar.mo19869HI();
        if (Math.max(((double) j) / HK, HI / ((double) j)) > 0.9d) {
            mo8358lY("ECONOMY ERROR: " + jCVar + " getting too close to limits. Price = " + j + " Max = " + HK + " Min = " + HI);
        }
        for (BlueprintType next4 : ala().aKu()) {
            if (next4.bia().mo554az() == jCVar) {
                long j4 = 0;
                for (IngredientsCategory apd : next4.bhW()) {
                    for (CraftingRecipe next5 : apd.apd()) {
                        j4 = (long) ((((float) (next5.mo554az().mo19868HG() * ((long) next5.getAmount()))) * mo20466ON()) + ((float) j4));
                    }
                }
                long amount = j4 / ((long) next4.bia().getAmount());
                if (((long) (((float) j) * mo20469OT())) > amount) {
                    return (long) (((float) amount) / mo20469OT());
                }
                return j;
            }
        }
        return j;
    }

    @C0064Am(aul = "93fb549a981623f827a2eebf34f6a050", aum = 0)
    /* renamed from: Pe */
    private void m35542Pe() {
        m35528Of().clear();
    }

    @C5566aOg
    @C6485anp
    @C5511aMd
    /* renamed from: a.mK$a */
    public static class TransactionLog extends aDJ implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C2491fm aVA = null;
        public static final C2491fm aVB = null;
        public static final C2491fm aVC = null;
        public static final C2491fm aVD = null;
        public static final C5663aRz aVu = null;
        public static final C5663aRz aVw = null;
        public static final C5663aRz aVx = null;
        public static final C5663aRz aVz = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "07b68bcff73491348924bc6676b127b5", aum = 0)
        private static C1556Wo<Long, C6275ajn> aVt;
        @C0064Am(aul = "17ffcb150a3f6ca59b32d6f07909e0bb", aum = 1)
        private static long aVv;
        @C0064Am(aul = "286a339327cf97b68fc92f825606a7b1", aum = 3)
        private static long aVy;
        @C0064Am(aul = "c4ba1dd02ebe6d09389cd7e39d92b3e7", aum = 2)

        /* renamed from: n */
        private static int f8646n;

        static {
            m35650V();
        }

        public TransactionLog() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public TransactionLog(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m35650V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 4;
            _m_methodCount = aDJ._m_methodCount + 4;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 4)];
            C5663aRz b = C5640aRc.m17844b(TransactionLog.class, "07b68bcff73491348924bc6676b127b5", i);
            aVu = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(TransactionLog.class, "17ffcb150a3f6ca59b32d6f07909e0bb", i2);
            aVw = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(TransactionLog.class, "c4ba1dd02ebe6d09389cd7e39d92b3e7", i3);
            aVx = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(TransactionLog.class, "286a339327cf97b68fc92f825606a7b1", i4);
            aVz = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i6 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i6 + 4)];
            C2491fm a = C4105zY.m41624a(TransactionLog.class, "2c0ad939e1296199779423ff04f4ea39", i6);
            aVA = a;
            fmVarArr[i6] = a;
            int i7 = i6 + 1;
            C2491fm a2 = C4105zY.m41624a(TransactionLog.class, "3954d5c9d1e3bc7216279c7f6342272d", i7);
            aVB = a2;
            fmVarArr[i7] = a2;
            int i8 = i7 + 1;
            C2491fm a3 = C4105zY.m41624a(TransactionLog.class, "b0bcb2a5d031509c387d6f0ce42f56f6", i8);
            aVC = a3;
            fmVarArr[i8] = a3;
            int i9 = i8 + 1;
            C2491fm a4 = C4105zY.m41624a(TransactionLog.class, "7b126b0cd72c657245caeaeead6a517f", i9);
            aVD = a4;
            fmVarArr[i9] = a4;
            int i10 = i9 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(TransactionLog.class, C7000ayn.class, _m_fields, _m_methods);
        }

        /* access modifiers changed from: private */
        /* renamed from: WS */
        public C1556Wo m35651WS() {
            return (C1556Wo) bFf().mo5608dq().mo3214p(aVu);
        }

        /* renamed from: WT */
        private long m35652WT() {
            return bFf().mo5608dq().mo3213o(aVw);
        }

        /* renamed from: WU */
        private int m35653WU() {
            return bFf().mo5608dq().mo3212n(aVx);
        }

        /* renamed from: WV */
        private long m35654WV() {
            return bFf().mo5608dq().mo3213o(aVz);
        }

        /* renamed from: Xb */
        private void m35658Xb() {
            switch (bFf().mo6893i(aVD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aVD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aVD, new Object[0]));
                    break;
            }
            m35657Xa();
        }

        /* renamed from: cL */
        private void m35661cL(long j) {
            bFf().mo5608dq().mo3184b(aVw, j);
        }

        /* renamed from: cM */
        private void m35662cM(long j) {
            bFf().mo5608dq().mo3184b(aVz, j);
        }

        /* renamed from: dW */
        private void m35663dW(int i) {
            bFf().mo5608dq().mo3183b(aVx, i);
        }

        /* renamed from: o */
        private void m35664o(C1556Wo wo) {
            bFf().mo5608dq().mo3197f(aVu, wo);
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C7000ayn(this);
        }

        /* renamed from: WX */
        public long mo20508WX() {
            switch (bFf().mo6893i(aVB)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, aVB, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aVB, new Object[0]));
                    break;
            }
            return m35655WW();
        }

        /* renamed from: WZ */
        public int mo20509WZ() {
            switch (bFf().mo6893i(aVC)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, aVC, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aVC, new Object[0]));
                    break;
            }
            return m35656WY();
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    m35660a(((Long) args[0]).longValue(), ((Integer) args[1]).intValue());
                    return null;
                case 1:
                    return new Long(m35655WW());
                case 2:
                    return new Integer(m35656WY());
                case 3:
                    m35657Xa();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public void mo20510b(long j, int i) {
            switch (bFf().mo6893i(aVA)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aVA, new Object[]{new Long(j), new Integer(i)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aVA, new Object[]{new Long(j), new Integer(i)}));
                    break;
            }
            m35660a(j, i);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        @C0064Am(aul = "2c0ad939e1296199779423ff04f4ea39", aum = 0)
        /* renamed from: a */
        private void m35660a(long j, int i) {
            long currentTimeMillis = currentTimeMillis();
            m35651WS().put(Long.valueOf(currentTimeMillis), new C6275ajn(j, i));
            m35661cL((long) (((float) ((m35652WT() * ((long) m35653WU())) + (((long) i) * j))) / ((float) (m35653WU() + i))));
            m35663dW(m35653WU() + i);
            if (m35654WV() <= 0) {
                m35662cM(currentTimeMillis);
            }
        }

        @C0064Am(aul = "3954d5c9d1e3bc7216279c7f6342272d", aum = 0)
        /* renamed from: WW */
        private long m35655WW() {
            m35658Xb();
            return m35652WT();
        }

        @C0064Am(aul = "b0bcb2a5d031509c387d6f0ce42f56f6", aum = 0)
        /* renamed from: WY */
        private int m35656WY() {
            m35658Xb();
            return m35653WU();
        }

        @C0064Am(aul = "7b126b0cd72c657245caeaeead6a517f", aum = 0)
        /* renamed from: Xa */
        private void m35657Xa() {
            long currentTimeMillis = currentTimeMillis();
            if (m35654WV() <= currentTimeMillis - 43200000) {
                m35663dW(0);
                m35662cM(currentTimeMillis);
                long j = 0;
                for (Map.Entry entry : new HashMap(m35651WS()).entrySet()) {
                    if (((Long) entry.getKey()).longValue() < currentTimeMillis - 43200000) {
                        m35651WS().remove(entry.getKey());
                    } else {
                        if (((Long) entry.getKey()).longValue() < m35654WV()) {
                            m35662cM(((Long) entry.getKey()).longValue());
                        }
                        j += ((C6275ajn) entry.getValue()).mo14177rZ() * ((long) ((C6275ajn) entry.getValue()).getAmount());
                        m35663dW(((C6275ajn) entry.getValue()).getAmount() + m35653WU());
                    }
                }
                if (m35653WU() != 0) {
                    m35661cL(j / ((long) m35653WU()));
                } else {
                    m35661cL(0);
                }
            }
        }
    }
}
